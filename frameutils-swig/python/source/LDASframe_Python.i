/*
 * LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo Frame
 * Files
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools Frame Utilities is free software; you may redistribute it and/or
 * modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools Frame Utilities is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef LDASframePythonI
#define LDASframePythonI

#if defined(SWIGPYTHON)

%{
  #include <wchar.h>

  #include "ldastoolsal/MemChecker.hh"

  extern "C" {
    void frameCPP_on_exit( )
    {
      LDASTools::AL::MemChecker::Trigger t( true );

      t.DoGarbageCollection( );
    }
  } /* extern "C" */
%}

%init %{
  Py_AtExit( frameCPP_on_exit );
%}

%exception
{
  //---------------------------------------------------------------------
  // Ensure that no exceptions leave the C++ layer
  //---------------------------------------------------------------------
  try
  {
    $action;
  }
  catch( const std::exception& Exception )
  {
    //-------------------------------------------------------------------
    // Handle exceptions derived from std::exception
    //-------------------------------------------------------------------
    PyErr_SetString( PyExc_IndexError,
		     const_cast<char*>( Exception.what( ) ) );
    goto fail;
  }
  catch( ... )
  {
    //-------------------------------------------------------------------
    // Catch everything else to make sure they do not leak outside
    //-------------------------------------------------------------------
    PyErr_SetString( PyExc_IndexError,
		     const_cast<char*>( "unknown error" ) );
    goto fail;
  }
}

%define LDAS_PROPERTY_INIT()
  %insert("python") %{
    try:
      __swig_getmethods__
    except NameError:
      __swig_getmethods__ = {}
    try:
      __swig_setmethods__
    except NameError:
      __swig_setmethods__ = {}
  %}
%enddef /* LDAS_PROPERTY_READ_ONLY(Name,ReadFunc) */

%define LDAS_PROPERTY_READ_ONLY(Name,ReadFunc)
  %insert("python") %{
     __swig_getmethods__[Name] = ReadFunc
  %}
%enddef /* LDAS_PROPERTY_READ_ONLY(Name,ReadFunc) */

%define LDAS_PROPERTY_READ_WRITE(Name,ReadFunc,WriteFunc)
  %insert("python") %{
    __swig_getmethods__[Name] = ReadFunc
    __swig_setmethods__[Name] = WriteFunc
  %}
%enddef /* LDAS_PROPERTY_READ_ONLY(Name,ReadFunc) */

%typemap(in) swig_list_string_type {
  /* Check if is a list */
  if ( PyTuple_Check( $input ) )
  {
    const int size = PyTuple_Size( $input );

    for ( int i = 0; i < size; i++ )
    {
      PyObject *o = PyTuple_GetItem( $input, i );
      if ( PyUnicode_Check( o ) )
      {
	$1.push_back( PyUnicode_AsUTF8( PyTuple_GetItem( $input, i ) ) );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "tuple must contain strings" );
	return NULL;
      }
    }
  }
  else if ( PyList_Check( $input ) )
  {
    const int size = PyList_Size( $input );

    for ( int i = 0; i < size; i++ )
    {
      PyObject *o = PyList_GetItem( $input, i );
      if ( PyUnicode_Check( o ) )
      {
	$1.push_back( PyUnicode_AsUTF8( PyList_GetItem( $input, i ) ) );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "list must contain strings" );
	return NULL;
      }
    }
  }
  else if ( PyUnicode_Check( $input ) )
  {
    $1.push_back( PyUnicode_AsUTF8( $input ) );
  }
  else
  {
    PyErr_SetString( PyExc_TypeError, "not a list or a string" );
    return NULL;
  }
}

%typemap(in) swig_vector_string_type {
  /* Check if is a list */
  if ( PyTuple_Check( $input ) )
  {
    const int size = PyTuple_Size( $input );

    for ( int i = 0; i < size; i++ )
    {
      PyObject *o = PyTuple_GetItem( $input, i );
      if ( PyUnicode_Check( o ) )
      {
	$1.push_back( PyUnicode_AsUTF8( PyTuple_GetItem( $input, i ) ) );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "tuple must contain strings" );
	return NULL;
      }
    }
  }
  else if ( PyList_Check( $input ) )
  {
    const int size = PyList_Size( $input );

    for ( int i = 0; i < size; i++ )
    {
      PyObject *o = PyList_GetItem( $input, i );
      if ( PyUnicode_Check( o ) )
      {
	$1.push_back( PyUnicode_AsUTF8( PyList_GetItem( $input, i ) ) );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "list must contain strings" );
	return NULL;
      }
    }
  }
  else if ( PyUnicode_Check( $input ) )
  {
    $1.push_back( PyUnicode_AsUTF8( $input ) );
  }
  else
  {
    PyErr_SetString( PyExc_TypeError, "not a tuple, list or a string" );
    return NULL;
  }
}

%typemap(in) channel_container_type {
 /* Check if is a list */
  if ( PyTuple_Check( $input ) )
  {
    const int size = PyTuple_Size( $input );

    for ( int i = 0; i < size; i++ )
    {
      PyObject *o = PyTuple_GetItem( $input, i );
      if ( PyUnicode_Check( o ) )
      {
	$1.names.push_back( PyUnicode_AsUTF8( PyTuple_GetItem( $input, i ) ) );
	$1.resampling.push_back( 1 );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "tuple must contain strings" );
	return NULL;
      }
    }
  }
  else if ( PyList_Check( $input ) )
  {
    const int size = PyList_Size( $input );

    for ( int i = 0; i < size; i++ )
    {
      PyObject *o = PyList_GetItem( $input, i );
      if ( PyUnicode_Check( o ) )
      {
	$1.names.push_back( PyUnicode_AsUTF8( PyList_GetItem( $input, i ) ) );
	$1.resampling.push_back( 1 );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "list must contain strings" );
	return NULL;
      }
    }
  }
  else if ( PyUnicode_Check( $input ) )
  {
    $1.names.push_back( PyUnicode_AsUTF8( $input ) );
    $1.resampling.push_back( 1 );
  }
  else
  {
    PyErr_SetString( PyExc_TypeError, "not a tuple, list or a string" );
    return NULL;
  }
}

%import frameAPI/LDASframe.i

#endif /* defined(SWIGPYTHON) */

#endif /* LDASframePythonI */
