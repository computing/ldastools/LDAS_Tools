/*
 * LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo Frame
 * Files
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools Frame Utilities is free software; you may redistribute it and/or
 * modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools Frame Utilities is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

/** \cond IGNORE_BY_DOXYGEN */
%define DOCSTRING
"The LDASframe package for Python allows for Python
developers to interface with the frameAPI library create by the LDAS team."
"The specification on which this interface is based can be found at:"
"https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=329"
%enddef

%module(docstring=DOCSTRING,module="LDAStools") LDASframe
%feature("autodoc","1");

%{
#include "frameAPI/Channel.hh"

using namespace FrameAPI;
%}

%include "std_string.i"
%include "std_vector.i"

typedef std::vector< std::string > swig_vector_string_type;

%include "boost_shared_ptr.i"
/* %include "ldastoolsal/SharedArray.i" */

%shared_ptr(Channel)
%shared_ptr(ChannelDict)
%shared_ptr(DataDictionary)
%shared_ptr(Frame)

%include "Channel.i"
%include "Frame.i"
%include "Catalog.i"
%include "createRDS.i"
