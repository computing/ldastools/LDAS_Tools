/*
 * LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo Frame
 * Files
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools Frame Utilities is free software; you may redistribute it and/or
 * modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools Frame Utilities is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


%module(package="LDAStools") LDASframe

%{
#include "framecpp/FrVect.hh"

#include "frameAPI/Channel.hh"

  using FrameCPP::FrVect;
  using FrameCPP::Common::FrameSpec;

%}

#if 0
%import "framecpp/frameCPPPython.i"
%import "framecpp/DataTypes.i"
%import "framecpp/GPSTime.i"
%import "framecpp/STRING.i"
%import "framecpp/Dimension.i"
#endif /* 0 */

%include "boost_shared_ptr.i"
%import(module="frameCPP") "framecpp/FrVect.hh"

 // %import "framecpp/FrAdcData.i"
%import "framecpp/FrProcData.i"
%import "framecpp/FrSimData.i"

%import "frameAPI/Channel.hh"

%shared_ptr(FrVect)

%nodefaultctor Channel;

class Channel
{
 public:
  typedef CHANNEL_NAME_TYPE	name_type;
  typedef CHANNEL_UNIT_Y_TYPE	unitY_type;

#ifdef SWIGPYTHON
  typedef Container< FrVect > data_type;
#else /* SWIGPYTHON */
  using FrameCPP::FrProcData::data_type;
#endif /* SWIGPYTHON */
  

  name_type GetName( ) const;
  unitY_type GetUnitY( ) const;

  data_type& RefData( );

  LDAS_PROPERTY_INIT( )

  LDAS_PROPERTY_READ_ONLY("name",GetName)
  LDAS_PROPERTY_READ_ONLY("unitY",GetUnitY)
  LDAS_PROPERTY_READ_ONLY("data",RefData)
};

#if ! SWIGIMPORTED
%{
  Channel::data_type&
  Channel_RefData( Channel* Self )
  {
    return Self->RefData( );
  }
%}
#endif /* ! SWIGIMPORTED */
