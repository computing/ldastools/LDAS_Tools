/*
 * LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo Frame
 * Files
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools Frame Utilities is free software; you may redistribute it and/or
 * modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools Frame Utilities is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


%module(package="LDAStools") LDASframe

%{
#include <string>
#include <list>

#include "frameAPI/Catalog.hh"

using namespace FrameAPI;
using LDASTools::AL::GPSTime;
using FrameAPI::Catalog;
%}

%include "std_list.i"
%include "std_string.i"

%import "ldastoolsal/gpstime.i"
%import "ldastoolsal/unordered_map.hh"

%apply swig_list_string_type { Catalog::stream_source_type };

%rename(DataDictionary) FrameAPI::Catalog::DataDictionary;
%template(ChannelDict) LDASTools::AL::unordered_map< std::string, Catalog::ChannelMetaData >;

%{
  //---------------------------------------------------------------------
  // Expose the inner class to appear as a global class for SWIG
  //---------------------------------------------------------------------
  typedef FrameAPI::Catalog::DataDictionary DataDictionary;
%}

class DataDictionary
{
public:
  typedef boost::shared_ptr< Channel > channel_type;
};

%extend DataDictionary  {
#if SWIGPYTHON
  DataDictionary::channel_type __getitem__( const char* Name )
  {
    DataDictionary::channel_type
      retval = (*self)[ std::string( Name ) ];

    return retval;
  }
#endif /* SWIGPYTHON */
};

class Catalog
{
 public:
  typedef INT_4U gps_seconds_type;
  typedef channel_container_type channel_name_type;
  typedef boost::shared_ptr< DataDictionary> data_dict_type;

  Catalog( Catalog::stream_source_type Sources );
  GPSTime GPSStartTime( ) const;
  GPSTime GPSEndTime( ) const;

  data_dict_type Fetch( gps_seconds_type Start,
			gps_seconds_type Stop,
			channel_container_type ChannelNames );

  LDAS_PROPERTY_INIT( )

  LDAS_PROPERTY_READ_ONLY("GpsStartTime",GPSStartTime);
  LDAS_PROPERTY_READ_ONLY("GpsEndTime",GPSEndTime);
};
