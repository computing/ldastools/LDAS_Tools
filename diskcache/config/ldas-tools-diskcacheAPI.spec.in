# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define tarbasename @PROJECT_NAME@
%define _docdir %{_datadir}/doc/%{tarbasename}-%{version}

%define check_boost169 ( 0%{?rhel} && 0%{?rhel} <= 8 )
%define check_cmake3  ( 0%{?rhel} && 0%{?rhel} <= 7 )

%if 0%{?rhl} <= 7 || 0%{?sl7} <= 7
%define boost_configure_options_ -DBoost_NO_SYSTEM_PATHS=True -DBOOST_INCLUDEDIR="%{_includedir}/boost169;NO_CMAKE_PATH;NO_CMAKE_ENVIRONMENT_PATH" -DBOOST_LIBRARYDIR=%{_libdir}/boost169
%else
%define boost_configure_options_ %{nil}
%endif

Summary: LDAS tools libdiskcacheAPI toolkit runtime files
Name: ldas-tools-diskcacheAPI
Version: @PROJECT_VERSION@
Release: 1%{?dist}
License: GPLv2+
URL: @PACKAGE_URL@
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: @LDAS_TOOLS_SOURCE_URL@/ldas-tools-diskcacheAPI-%{version}.tar.gz
Requires: ldas-tools-al >= @LDAS_TOOLS_AL_VERSION@
Requires: ldas-tools-ldasgen >= @LDAS_TOOLS_LDASGEN_VERSION@
Requires(pre): shadow-utils
%{?systemd_requires}
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: make
BuildRequires: rpm-build
BuildRequires: openssl-devel
%if %{check_cmake3}
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
BuildRequires: boost169-devel
BuildRequires: boost169-regex
%else
BuildRequires: cmake >= 3.6
BuildRequires: boost-devel
BuildRequires: boost-regex
%endif
Buildrequires: doxygen
Buildrequires: pkgconfig
Buildrequires: zlib-devel
BuildRequires: ldas-tools-cmake >= @LDAS_TOOLS_CMAKE_VERSION@
Buildrequires: ldas-tools-ldasgen-devel >= @LDAS_TOOLS_LDASGEN_VERSION@
Buildrequires: systemd

%description
This provides the runtime libraries for the diskcacheAPI library.

%package devel
Group: Development/Scientific
Summary: LDAS tools libdiskcacheAPI toolkit development files
Requires: ldas-tools-ldasgen-devel >= @LDAS_TOOLS_LDASGEN_VERSION@
Requires: ldas-tools-diskcacheAPI = %{version}
%description devel
This provides the develpement files the diskcacheAPI library.

%prep

%setup -c -T -D -a 0 -n %{name}-%{version}

%build
#------------------------------------------------------------------------
# This works around a bug in the current rpmbuild system whereby the
#   PKG_CONFIG_PATH is set by the system and does not allow for
#   user preference.
# This work around should be fixed in RH 7.1 or so
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%if %{check_boost169}
export BOOST_CONFIGURE_OPTIONS="-DBoost_NO_SYSTEM_PATHS=True -DBOOST_INCLUDEDIR=%{_includedir}/boost169/ -DBOOST_LIBRARYDIR=%{_libdir}/boost169/"
%else
export BOOST_CONFIGURE_OPTIONS="-DBOOST_INCLUDEDIR=%{_includedir}/boost/ -DBOOST_LIBRARYDIR=%{_libdir}"
%endif
%if %{check_cmake3}
export CMAKE_PROGRAM=cmake3
%else
export CMAKE_PROGRAM=cmake
%endif

${CMAKE_PROGRAM} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    ${BOOST_CONFIGURE_OPTIONS} \
    %{tarbasename}-%{version}

make V=1 %{?_smp_mflags}

%if %{check_cmake3}
export CTEST_PROGRAM=ctest3
%else
export CTEST_PROGRAM=ctest
%endif

${CTEST_PROGRAM} -V %{?_smp_mflags}

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;

mkdir -p %{buildroot}%{_sharedstatedir}/diskcache
mkdir -p %{buildroot}/var/log/diskcache

# Remove the init script that has been replaced by a systemd unit file
rm -f %{buildroot}/etc/initd.d/diskcached

%pre
getent group diskcache >/dev/null || groupadd -r diskcache
getent passwd diskcache >/dev/null || \
    useradd -r -g diskcache -d %{_sharedstatedir}/diskcache -s /sbin/nologin \
    -c "Dedicated diskcache service account" diskcache
exit 0

%post
/sbin/ldconfig
%systemd_post diskcache.service

%preun
%systemd_preun diskcache.service

%postun
/sbin/ldconfig
%systemd_postun diskcache.service

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_sysconfdir}/diskcache
%attr(0755,diskcache,diskcache) %dir /var/log/diskcache
%attr(0755,diskcache,diskcache) %dir %{_sharedstatedir}/diskcache

%{_bindir}/diskcache
%{_bindir}/ldas-cache-dump-verify
%{_libdir}/libdiskcache*.so.*
%{_unitdir}/diskcache.service
%{_sysconfdir}/diskcache/diskcache.rsc.sample
%{_docdir}/diskcache_poller
%{_docdir}/diskcache_server

%files devel
%defattr(-,root,root)
%{_includedir}/diskcacheAPI
%{_libdir}/libdiskcache.*a
%{_libdir}/libdiskcache*.so
%{_docdir}
%{_libdir}/pkgconfig/ldastools-diskcache.pc

%changelog
# date +'%a %b %d %Y'
* Mon Dec 16 2024 Edward Maros <ed.maros@ligo.org> - 2.7.8-1
- Built for new release

* Mon May 08 2023 Edward Maros <ed.maros@ligo.org> - 2.7.7-1
- Built for new release

* Fri May 05 2023 Edward Maros <ed.maros@ligo.org> - 2.7.6-1
- Built for new release

* Wed Apr 19 2023 Edward Maros <ed.maros@ligo.org> - 2.7.5-1
- Built for new release

* Thu Apr 13 2023 Edward Maros <ed.maros@ligo.org> - 2.7.4-1
- Built for new release

* Thu Dec 08 2022 Edward Maros <ed.maros@ligo.org> - 2.7.3-1
- Built for new release

* Wed Sep 29 2021 Edward Maros <ed.maros@ligo.org> - 2.7.2-1
- Built for new release

* Tue Mar 09 2021 Edward Maros <ed.maros@ligo.org> - 2.7.1-1
- Built for new release

* Wed Aug 14 2019 Edward Maros <ed.maros@ligo.org> - 2.7.0-1
- Built for new release

* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.5-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Sep 26 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Fixed diskcache to recover from devices going offline (#4645)

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <ed.maros@ligo.org> - 1.19.13-1
- Initial build.
