# Release 2.7.8 - December 16, 2024
    - Corrected issues revealed by address sanitizer

# Release 2.7.6 - May 5, 2023
    - Fixed setting of parameter values for diskcache daemon subcommand
    - Added signal handler to trap SIGSEGV when cleaning up after main exits

# Release 2.7.5 - April 19, 2023
    - Fixed race condition when setting options for diskcache scan subcommand

# Release 2.7.4 - April 13, 2023
    - Added --configuration-file option to scan subcommand (Closes #150)

# Release 2.7.3 - December 8, 2022
    - Added --configuration-file option to scan subcommand (Closes #150)

# Release 2.7.2 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.7.0 - August 14, 2019
  - Converted to using CMake
  - Corrected Debian installation rules for diskcached
  - Added description text to Portfile (Closes #55)
  - Corrected Portfile to have proper description (Closes #55)
  - Corrected installation directory for Doxygen derived documenation (Closes #73)
  - Updated shared library versions
    - libdiskcache - 12:0:0

# Release 2.6.3 - December 6, 2018
  - Addressed packaging issues

# Release 2.6.2 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Standardize source code format by using clang-format
  - Replaced debugging messages going to std::cerr with queue_log_message (fixes #40)
  - Removed building of TCL components from diskcache (fixes #41)
  - Removed detection of bison as no targets have a dependency on the application (fixes #42)
  - Updated shared library versions
    - libdiskcache - 11:1:0

# Release 2.6.1 - June 22, 2018
  - Updated packaging rules to have build time dependency on specific
    versions of LDAS Tools packages

# Release 2.6.0 - June 19, 2018
  - Removed hand rolled smart pointers in favor of boost smart pointers
  - Changed version of diskcache library to 11:0:0

# Release 2.5.2 - September 15, 2016
  - Added option RWLOCK_INTERVAL_MS (#3120)
  - Added option RWLOCK_TIMEOUT_MS (#3121)
  - Added option SCAN_INTERVAL (#3119)

# Release 2.5.1 - September 9, 2016
  - Added --disable-warnings-as-errors to allow compilation on systems
    where warning messages have not yet been addressed
  - Added conditional setting of DESTDIR in python.mk to prevent install
    issues.
  - Modified diskcache's algorithm to pad with extra frame file on both sides
    of a resampling query to handle situations where the start and end of a
    query occurs on non frame file boundries.
  - Converted some debugging messages to use LDAS Logging instead of standard
    error.
  - Enhanced the createRDS algorithm to take a padding option to minimize the
    amount of data needed to be retrieved for a request requiring down
    sampling (#3130).
  - Continued effort to remove dead code.
  - Added systemd support (#1483)
  - Added support to handle padded queries (#3132)

# Release 2.5.0 - April 7, 2016
  Official release of splitting LDASTools into separate source packages

# Release 2.4.99.1 - March 11, 2016

The following problems have been resolved in this release:
  - Corrections to spec files.

# Release 2.4.99.0 - March 3, 2016

The following problems have been resolved in this release:
  - Separated code into independant source distribution
