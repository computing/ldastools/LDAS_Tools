//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__MOUNT_POINT_SCANNER__HH
#define DISKCACHE_API__MOUNT_POINT_SCANNER__HH

#include <string>
#include <vector>

#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/Task.hh"
#include "ldastoolsal/TaskThread.hh"
#include "ldastoolsal/ThreadPool.hh"
#include "ldastoolsal/types.hh"

#include "diskcacheAPI.hh"
#include "diskcache.hh"
#include "MountPointManagerSingleton.hh"

namespace diskCache
{
    class MountPointScanner : public LDASTools::AL::Task
    {
    public:
        typedef MountPointManagerSingleton::mount_point_name_container_type
                       mount_point_container_type;
        typedef INT_4U concurrency_type;
        typedef bool   scanner_sync_type;
        typedef LDASTools::AL::ReadWriteLockVariable<
            LDASTools::AL::ReadWriteLock::WRITE,
            scanner_sync_type >
            scanner_sync_rw_type;
        typedef LDASTools::AL::ReadWriteLockVariable<
            LDASTools::AL::ReadWriteLock::READ,
            scanner_sync_type >
            scanner_sync_ro_type;

        typedef LDASTools::AL::TaskThread controller_type;
        class SyncRequest
        {
        public:
            typedef INT_4U request_count_type;
            SyncRequest( );
            ~SyncRequest( );

            static request_count_type PendingRequests( );

        private:
            SyncRequest( const SyncRequest& );
            const SyncRequest& operator=( const SyncRequest& );

            static LDASTools::AL::MutexLock::baton_type p_request_count_baton;
            static request_count_type                   p_request_count;
        };

        class ScanResults
        {
        public:
            typedef INT_4U directory_count_type;
            typedef INT_4U file_count_type;
            typedef INT_4U mount_point_count_type;
            typedef double timer_delta_type;

            //-----------------------------------------------------------------
            /// \brief Constructor
            //-----------------------------------------------------------------
            ScanResults( );

            //-----------------------------------------------------------------
            /// \brief Retrieve the number of directories scanned.
            //-----------------------------------------------------------------
            directory_count_type DirectoryCount( ) const;

            //-----------------------------------------------------------------
            /// \brief Incriment the number of directories scanned by one.
            //-----------------------------------------------------------------
            void DirectoryCountInc( );

            //-----------------------------------------------------------------
            /// \brief Incriment the number of directories scanned by one.
            ///
            /// \param[in] Value
            ///     The number by which to increment the number of
            ///     directories scanned.
            //-----------------------------------------------------------------
            void DirectoryCountInc( directory_count_type Value );

            //-----------------------------------------------------------------
            /// \brief Retrieve the number of directories scanned.
            //-----------------------------------------------------------------
            file_count_type FileCount( ) const;

            //-----------------------------------------------------------------
            /// \brief Incriment the number of files scanned by one.
            ///
            /// \param[in] Value
            ///     The number by which to increment the number of
            ///     files scanned.
            //-----------------------------------------------------------------
            void FileCountInc( file_count_type Value );

            //-----------------------------------------------------------------
            /// \brief Retrieve the number of directories scanned.
            //-----------------------------------------------------------------
            directory_count_type MountPointCount( ) const;

            //-----------------------------------------------------------------
            /// \brief Incriment the number of directories scanned by one.
            //-----------------------------------------------------------------
            void MountPointCountInc( );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            void Start( ) const;

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            timer_delta_type Stop( ) const;

        private:
            typedef LDASTools::AL::GPSTime timer_type;

            mutable timer_type       m_timer;
            mutable timer_delta_type m_delta;

            LDASTools::AL::MutexLock::baton_type m_count_baton;

            directory_count_type   m_directory_count;
            file_count_type        m_file_count;
            mount_point_count_type m_mount_point_count;
        };

        MountPointScanner( const std::string& MountPoint,
                           ScanResults&       Answer,
                           controller_type*   Controller )
            : LDASTools::AL::Task( "Diskcache::MountPointScanner",
                                   controller_type::CANCEL_EXCEPTION ),
              m_mount_point( MountPoint ), m_scan_results( Answer ),
              m_controller( Controller )
        {
            delete_on_completion( true );
            increment( );
        }

        inline ~MountPointScanner( )
        {
            decrement( );
        }

        inline static INT_4U
        Active( )
        {
            LDASTools::AL::MutexLock l(
                m_active_count_baton, __FILE__, __LINE__ );

            return m_active_count;
        }

        inline const std::string&
        Results( ) const
        {
            return m_results;
        }

        //-------------------------------------------------------------------
        /// \brief Check the cancellation state.
        ///
        /// \param[out] Reason
        ///     If the MountPointScanner is requesting that scanning threads
        ///     cancel themselves,
        ///     this string will have a meassage explaining why.
        ///
        /// \return
        ///     If the MountPointScanner is requesting that scanning threads
        ///     cancel themselves, the value true is returned;
        ///     false otherwise.
        //-------------------------------------------------------------------
        static bool CancellationRequest( std::string& Reason );

        static concurrency_type Concurrency( );

        static void Concurrency( concurrency_type Value );

        //-------------------------------------------------------------------
        /// \brief Reports the status of having completed the first scan
        ///
        /// \return
        ///     If the first scan through all entries in the mount point
        ///     list has completed, the value true is returned;
        ///     otherwise false
        //-------------------------------------------------------------------
        static bool FirstScanComplete( );

        //-------------------------------------------------------------------
        /// \brief Sets the status of having completed the first scan
        ///
        /// \param[in] Value
        ///     New value for the status of having completed the first scan.
        //-------------------------------------------------------------------
        static void FirstScanSet( bool Value );

        //-------------------------------------------------------------------
        /// \brief Reports the status of having completed the first scan
        ///
        /// \return
        ///     If the first scan through all entries in the mount point
        ///     list has completed, the value true is returned;
        ///     otherwise false
        //-------------------------------------------------------------------
        static INT_4U ScanIteration( );

        inline static void
        CriticalSection( bool Value )
        {
            if ( Value )
            {
                m_scan_completed.Mutex( ).Lock( __FILE__, __LINE__ );
            }
            else
            {
                m_scan_completed.Mutex( ).Unlock( __FILE__, __LINE__ );
            }
        }

        virtual void OnCompletion( int TaskThreadState );

        //-------------------------------------------------------------------
        /// \brief Rebuild of the cache.
        //-------------------------------------------------------------------
        static void Rebuild( );

        static void Scan( const mount_point_container_type& MountPoints,
                          ScanResults&                      Answer );

        static scanner_sync_rw_type Sync( );

        static scanner_sync_ro_type SyncRO( );

        inline static void
        Wait( )
        {
            m_scan_completed.TimedWait( 10 );
        }

        virtual void operator( )( );

    private:
        //-------------------------------------------------------------------
        /// \brief Protect static variables in a threaded environment
        //-------------------------------------------------------------------
        static LDASTools::AL::MutexLock::baton_type m_variable_baton;

        std::string                                 m_mount_point;
        std::string                                 m_results;
        ScanResults&                                m_scan_results;
        controller_type*                            m_controller;
        static LDASTools::AL::ConditionalVariable   m_scan_completed;
        static LDASTools::AL::MutexLock::baton_type m_active_count_baton;
        static concurrency_type                     m_active_count;
        static concurrency_type                     m_concurrency;
        static LDASTools::AL::MutexLock::baton_type
                      m_first_scan_completed_baton;
        static INT_4U m_first_scan_completed;
        static LDASTools::AL::MutexLock::baton_type
                    m_mount_point_cache_reset_baton;
        static bool m_mount_point_cache_reset;
        //-------------------------------------------------------------------
        /// \brief Variable needed to syncronize access to scanner
        //-------------------------------------------------------------------
        static scanner_sync_type p_scanner_sync;
        //-------------------------------------------------------------------
        /// \brief Syncronize access to scanner
        //-------------------------------------------------------------------
        static scanner_sync_rw_type::baton_type p_scanner_sync_baton;

        inline static void
        decrement( )
        {
            LDASTools::AL::MutexLock l(
                m_active_count_baton, __FILE__, __LINE__ );

            if ( m_active_count )
            {
                --m_active_count;
            }
        }

        inline static void
        increment( )
        {
            LDASTools::AL::MutexLock l(
                m_active_count_baton, __FILE__, __LINE__ );

            ++m_active_count;
        }

        static void on_exit( );

        static void start( const std::string& MountPoint, ScanResults& Answer );

        //-------------------------------------------------------------------
        /// \brief Obtain the state indicating the need to rebuild the cache.
        //-------------------------------------------------------------------
        inline static bool
        mount_point_reset( )
        {
            LDASTools::AL::MutexLock l(
                m_mount_point_cache_reset_baton, __FILE__, __LINE__ );

            return m_mount_point_cache_reset;
        }

        //-------------------------------------------------------------------
        /// \brief Set the state indicating the need to rebuild the cache.
        //-------------------------------------------------------------------
        inline static bool
        mount_point_reset( bool Value )
        {
            LDASTools::AL::MutexLock l(
                m_mount_point_cache_reset_baton, __FILE__, __LINE__ );

            bool retval = m_mount_point_cache_reset;

            m_mount_point_cache_reset = Value;

            return retval;
        }
    };

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline MountPointScanner::ScanResults::directory_count_type
    MountPointScanner::ScanResults::DirectoryCount( ) const
    {
        return m_directory_count;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline void
    MountPointScanner::ScanResults::DirectoryCountInc( )
    {
        LDASTools::AL::MutexLock l( m_count_baton, __FILE__, __LINE__ );

        ++m_directory_count;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline void
    MountPointScanner::ScanResults::DirectoryCountInc(
        directory_count_type Value )
    {
        LDASTools::AL::MutexLock l( m_count_baton, __FILE__, __LINE__ );

        m_directory_count += Value;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline MountPointScanner::ScanResults::file_count_type
    MountPointScanner::ScanResults::FileCount( ) const
    {
        return m_file_count;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline void
    MountPointScanner::ScanResults::FileCountInc( file_count_type Value )
    {
        LDASTools::AL::MutexLock l( m_count_baton, __FILE__, __LINE__ );

        m_file_count += Value;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline MountPointScanner::ScanResults::mount_point_count_type
    MountPointScanner::ScanResults::MountPointCount( ) const
    {
        return m_mount_point_count;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline void
    MountPointScanner::ScanResults::MountPointCountInc( )
    {
        LDASTools::AL::MutexLock l( m_count_baton, __FILE__, __LINE__ );

        ++m_mount_point_count;
    }

    inline void
    MountPointScanner::ScanResults::Start( ) const
    {
        m_timer.Now( );
    }

    inline MountPointScanner::ScanResults::timer_delta_type
    MountPointScanner::ScanResults::Stop( ) const
    {
        m_delta = timer_type::NowGPSTime( ) - m_timer;
        return m_delta;
    }

    inline MountPointScanner::concurrency_type
    MountPointScanner::Concurrency( )
    {
        LDASTools::AL::MutexLock l( m_variable_baton, __FILE__, __LINE__ );

        return m_concurrency;
    }

    inline void
    MountPointScanner::Concurrency( concurrency_type Value )
    {
        LDASTools::AL::MutexLock l( m_variable_baton, __FILE__, __LINE__ );

        m_concurrency = Value;
    }

    //---------------------------------------------------------------------
    /// This method simply returns true once the scanner is in update mode.
    /// If this method returns false, then the scanner is working on its
    /// first pass through the mount point list.
    //---------------------------------------------------------------------
    inline bool
    MountPointScanner::FirstScanComplete( )
    {
        LDASTools::AL::MutexLock l(
            m_first_scan_completed_baton, __FILE__, __LINE__ );

        return ( m_first_scan_completed > 0 );
    }

    //---------------------------------------------------------------------
    /// Some actions need to reset the status concearning the first scan.
    //---------------------------------------------------------------------
    inline void
    MountPointScanner::FirstScanSet( bool Value )
    {
        if ( Value )
        {
            LDASTools::AL::MutexLock l(
                m_first_scan_completed_baton, __FILE__, __LINE__ );

            ++m_first_scan_completed;
        }
        else
        {
            LDASTools::AL::MutexLock l(
                m_first_scan_completed_baton, __FILE__, __LINE__ );

            m_first_scan_completed = 0;
        }
    }

    inline INT_4U
    MountPointScanner::ScanIteration( )
    {
        LDASTools::AL::MutexLock l(
            m_first_scan_completed_baton, __FILE__, __LINE__ );

        return m_first_scan_completed;
    }

    inline MountPointScanner::scanner_sync_rw_type
    MountPointScanner::Sync( )
    {
        return ( scanner_sync_rw_type( p_scanner_sync_baton,
                                       RWLOCK_TIMEOUT,
                                       p_scanner_sync,
                                       __FILE__,
                                       __LINE__ ) );
    }

    inline MountPointScanner::scanner_sync_ro_type
    MountPointScanner::SyncRO( )
    {
        return ( scanner_sync_ro_type( p_scanner_sync_baton,
                                       RWLOCK_TIMEOUT,
                                       p_scanner_sync,
                                       __FILE__,
                                       __LINE__ ) );
    }

    //---------------------------------------------------------------------
    /// Set up the scanner to do a full rebuild of the cache.
    //---------------------------------------------------------------------
    inline void
    MountPointScanner::Rebuild( )
    {
        (void)mount_point_reset( true );
    }
} // namespace diskCache

#endif /* DIKCACHE_API__MOUNT_POINT_SCANNER__HH */
