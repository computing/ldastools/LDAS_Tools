//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include "diskcacheAPI/ServerInterface.hh"

using LDASTools::AL::CommandLineOptions;

namespace diskCache
{
    //=====================================================================
    //
    //=====================================================================
    std::string ServerInterface::DEFAULT_EXTENSION( ".gwf" );
    void
    ServerInterface::FilenamesRDS( filenames_rds_results_type& Results,
                                   const std::string&          IFO,
                                   const std::string&          Desc,
                                   time_type                   StartTime,
                                   time_type                   EndTime,
                                   bool                        Resampling,
                                   const std::string&          Extension )
    {
        static const char* caller = "ServerInterface::FilenameRDS";

        std::list< std::string > args;
        std::ostringstream       start_str;
        std::ostringstream       end_str;

        start_str << StartTime;
        end_str << EndTime;

        args.push_back( "filenames-rds" );
        args.push_back( "--extension" );
        args.push_back( Extension );
        args.push_back( "--ifo" );
        args.push_back( IFO );
        args.push_back( "--type" );
        args.push_back( Desc );
        args.push_back( "--start-time" );
        args.push_back( start_str.str( ) );
        args.push_back( "--end-time" );
        args.push_back( end_str.str( ) );
        if ( Resampling )
        {
            QUEUE_LOG_MESSAGE( "Server Request: requesting resampling",
                               MT_DEBUG,
                               40,
                               caller,
                               "DISKCACHE DAEMON" );
            args.push_back( "--resample" );
        }

        CommandLineOptions cl( args );

        diskCache::MetaCommand::FilenamesRDS diskcache_query( cl, *this );

        diskcache_query( );

        std::insert_iterator< filenames_rds_results_type > insert_it(
            Results, Results.begin( ) );

        std::copy( diskcache_query.Results( ).begin( ),
                   diskcache_query.Results( ).end( ),
                   insert_it );
    }

} // namespace diskCache
