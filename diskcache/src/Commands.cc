//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldastoolsal/ldasexception.hh"

#include "genericAPI/Logging.hh"

#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/QueryParams.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "Commands.hh"
#include "MountPointManagerSingleton.hh"

namespace
{
    void is_first_scan_complete( const char* const File, int Line );
}

namespace diskCache
{
    namespace Commands
    {
        using std::string;

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        const Cache::SDGTx::file_extension_container_type&
        FileExtList( )
        {
            return Cache::SDGTx::FileExtList( );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        getDirCache( Cache::QueryAnswer& Answer,
                     const char*         Ifo,
                     const char*         Type )
        {
            is_first_scan_complete( __FILE__, __LINE__ );

            Timer tracker;

            string data;
            //-----------------------------------------------------------------
            // Set the search criteria
            //-----------------------------------------------------------------
            Cache::QueryParams q;

            q.AddParam( "index", Cache::SDGTx::AsciiId );

            q.AddParam( "site", Ifo );
            q.AddParam( "description", Type );

            try
            {
                Cache::RegistrySingleton::TranslateQuery( q, Answer );
                MountPointManagerSingleton::Find( Answer );
            }
            catch ( const Cache::QueryParams::MissingVariableError& Error )
            {
            }
            tracker.getDelta( "getDirCache()" );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        getFileNames( diskCache::Cache::QueryAnswer& Answer,
                      const char*                    ifo_type_str,
                      const INT_4U                   query_start,
                      const INT_4U                   query_stop,
                      const std::string&             Extension )
        {
            is_first_scan_complete( __FILE__, __LINE__ );

            Timer tracker;

            //--------------------------------------------------------------------
            // Set the search criteria
            //--------------------------------------------------------------------
            Cache::QueryParams q;
            std::ostringstream start_string;
            std::ostringstream stop_string;

            start_string << query_start;
            stop_string << query_stop;

            q.AddParam( "index", Cache::SDGTx::AsciiId );

            q.AddParam( "site-description", ifo_type_str );
            q.AddParam( "start", start_string.str( ) );
            q.AddParam( "stop", stop_string.str( ) );
            q.AddParam( "extension", Extension );

            try
            {
                Cache::RegistrySingleton::TranslateQuery( q, Answer );
                MountPointManagerSingleton::Find( Answer );
            }
            catch ( const Cache::QueryParams::MissingVariableError& Error )
            {
            }

            tracker.getDelta( "getFileNames()" );
        }

        //-------------------------------------------------------------------
        /// This function is used to search global frame data hash for a
        /// specific data.
        //-------------------------------------------------------------------
        void
        getFrameFiles( diskCache::Cache::QueryAnswer& Answer,
                       const char*                    Ifo,
                       const char*                    Type,
                       const INT_4U                   StartTime,
                       const INT_4U                   StopTime,
                       const std::string&             Extension,
                       const bool                     GapsAllowed )
        {
            // Debug timer
            Timer tracker;

            try
            {
                is_first_scan_complete( __FILE__, __LINE__ );
            }
            catch ( const LdasException& Exception )
            {
                /// \todo Need to handle is_first_scan_complete exception
#if 0
	const ErrorInfo& info( Exception[ 0 ] );

	std::ostringstream        msg;

	msg << "{} {" << info.getMessage( ) << "}";
	filenames = msg.str( );
#endif /* 0 */

                tracker.getDelta( "getFrameFiles()" );
                return;
            }
            const std::string  allow_gaps( ( GapsAllowed ) ? "true" : "false" );
            std::ostringstream start_string;
            std::ostringstream stop_string;

            start_string << StartTime;
            stop_string << ( StopTime + 1 ); // Add one to adjust user query

            try
            {
                //-----------------------------------------------------------------
                // Set the search criteria
                //-----------------------------------------------------------------
                Cache::QueryParams q;

                q.AddParam( "index", Cache::SDGTx::AsciiId );

                q.AddParam( "site", Ifo );
                q.AddParam( "description", Type );
                q.AddParam( "extension", Extension );
                q.AddParam( "allow_gaps", allow_gaps );
                q.AddParam( "start", start_string.str( ) );
                q.AddParam( "stop", stop_string.str( ) );

                Cache::RegistrySingleton::TranslateQuery( q, Answer );
                MountPointManagerSingleton::Find( Answer );
            }
            catch ( const Cache::QueryParams::MissingVariableError& Error )
            {
            }

            tracker.getDelta( "getFrameFiles()" );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        getIntervalsList( diskCache::Cache::QueryAnswer& Answer,
                          const char*                    ifo_type_str,
                          const INT_4U                   query_start,
                          const INT_4U                   query_stop,
                          const std::string&             Extension )
        {
            is_first_scan_complete( __FILE__, __LINE__ );

            Timer tracker;

            std::ostringstream start_string;
            std::ostringstream stop_string;

            start_string << query_start;
            stop_string << query_stop;

            Cache::QueryParams q;

            q.AddParam( "index", Cache::SDGTx::AsciiId );

            q.AddParam( "query_variety", "interval" );
            q.AddParam( "site-description", ifo_type_str );
            q.AddParam( "extension", Extension );
            q.AddParam( "start", start_string.str( ) );
            q.AddParam( "stop", stop_string.str( ) );

            try
            {
                Cache::RegistrySingleton::TranslateQuery( q, Answer );
                MountPointManagerSingleton::Find( Answer );
            }
            catch ( const Cache::QueryParams::MissingVariableError& Error )
            {
            }

            tracker.getDelta( "getIntervalsList()" );
        }

        //-------------------------------------------------------------------
        /// This command finds frame files that satisfy createRDS user request.
        /// If "will_resample" flag is set to TRUE, requested time range will be
        /// automatically expanded by frame dt at the beginning and at the end.
        //-------------------------------------------------------------------
        void
        getRDSFrameFiles( diskCache::Cache::QueryAnswer& Answer,
                          const char*                    ifo,
                          const char*                    type,
                          const unsigned int             start_time,
                          const unsigned int             stop_time,
                          const std::string&             Extension,
                          const bool                     will_resample )
        {
            static const char caller[] = "getRDSFrameFiles";

            // Debug timer
            Timer tracker;

            std::string filenames;

            try
            {
                is_first_scan_complete( __FILE__, __LINE__ );

                Cache::QueryParams q;

                std::ostringstream start_string;
                std::ostringstream stop_string;
                std::ostringstream resample_string;

                start_string << start_time;
                stop_string << stop_time;
                resample_string << ( ( will_resample ) ? 1 : 0 );

                q.AddParam( "index", "SDGTx" );

                q.AddParam( "extension", Extension );
                q.AddParam( "site", ifo );
                q.AddParam( "description", type );
                q.AddParam( "start", start_string.str( ) );
                q.AddParam( "stop", stop_string.str( ) );
                q.AddParam( "resample", resample_string.str( ) );

                try
                {
                    Cache::RegistrySingleton::TranslateQuery( q, Answer );

                    std::ostringstream msg;

                    msg << "Search criteria: ifo: " << ifo << " type: " << type
                        << " time range: " << start_time << "-" << stop_time
                        << " extension: " << Extension
                        << " resample: " << resample_string.str( );
                    GenericAPI::queueLogEntry(
                        msg.str( ),
                        GenericAPI::LogEntryGroup_type::MT_DEBUG,
                        0,
                        caller,
                        "SEARCH_MOUNTPT" );

                    MountPointManagerSingleton::Find( Answer );
                }
                catch ( const Cache::QueryParams::MissingVariableError& Error )
                {
                }
            }
            catch ( const LdasException& Exception )
            {
                const ErrorInfo& info( Exception[ 0 ] );

                Answer.AddError( info.getMessage( ) );
            }
            tracker.getDelta( caller );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        updateFileExtList(
            const diskCache::Cache::SDGTx::file_extension_container_type&
                Extensions )
        {
            // Debug timer
            Timer tracker;

            diskCache::Cache::SDGTx::FileExtList( Extensions,
                                                  *( Extensions.begin( ) ) );

            tracker.getDelta( "updateFileExtList()" );

            return;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        updateMountPtList(
            MountPointManagerSingleton::UpdateResults& update_status,
            const MountPointManagerSingleton::mount_point_name_container_type&
                       Paths,
            const bool enable_global_check )
        {
            Timer tracker;

            MountPointManagerSingleton::Update( Paths, update_status );

            string msg( "updateMountPtList( \"" );
            for ( MountPointManagerSingleton::mount_point_name_container_type::
                      const_iterator cur = Paths.begin( ),
                                     last = Paths.end( );
                  cur != last;
                  ++cur )
            {
                msg += *cur;
                msg += " ";
            }
            msg += "\" )";
            tracker.getDelta( msg );

            return;
        }

        //===================================================================
        //===================================================================
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Timer::Timer( )
        {
#ifdef DEBUG_TIME
            gettimeofday( &mTv, NULL );
            mLastSec = mTv.tv_sec;
            mLastUsec = mTv.tv_usec;
#endif
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        Timer::getDelta( const std::string& method )
        {
#ifdef DEBUG_TIME
            gettimeofday( &mTv, NULL );

            REAL_4 delta( mTv.tv_sec - mLastSec +
                          ( mTv.tv_usec - mLastUsec ) * 1e-6 );

            std::cout << "--->DEBUG_TIME: clock time {" << method
                      << "}: " << delta << endl;

            mLastSec = mTv.tv_sec;
            mLastUsec = mTv.tv_usec;
#endif
            return;
        } // method - Timer::getDelta

    } // namespace Commands

} // namespace diskCache

namespace
{
    inline void
    is_first_scan_complete( const char* const File, int Line )
    {
#if WORKING
        if ( diskCache::MountPointScanner::FirstScanComplete( ) == false )
        {
            std::ostringstream msg;

            msg << "The in memory database of the file systems is currently "
                   "unavailable"
                << " due to a request to do a full resync.";

            std::ostringstream info;
            info
                << "There are several reasons this may occur."
                << " 1) When the diskcacheAPI is restarted."
                << " 2) When a request has been made to rebuild the cache."
                << " 3) When the list of mount points has been changed."
                << " In all of these cases, the system `must process each entry"
                << " in the list of mount points once before resuming normal"
                << " system processing.";
            throw LdasException( Library::DISKCACHEAPI,
                                 -1,
                                 msg.str( ),
                                 info.str( ),
                                 File,
                                 Line );
        }
#endif /* WORKING */
    }
} // namespace
