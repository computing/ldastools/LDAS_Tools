//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__IO_HH
#define DISKCACHE_API__IO_HH

#if !defined( SWIG )
#include "genericAPI/Logging.hh"

#include "diskcacheAPI/MountPointManagerSingleton.hh"
#include "diskcacheAPI/DirectoryManagerSingleton.hh"
#endif /* !defined(SWIG) */

namespace diskCache
{
    template < typename StreamT >
    inline StreamT&
    Read( StreamT& Stream )
    {
        static const char method_name[] = "diskCache::Read";

        {
            std::ostringstream msg;

            msg << "Entry";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        diskCache::MountPointManagerSingleton::Read( Stream );
        diskCache::DirectoryManagerSingleton::Read( Stream );
        {
            std::ostringstream msg;

            msg << "Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        return Stream;
    }

    template < typename StreamT >
    inline StreamT&
    Write( StreamT& Stream )
    {
        static const char method_name[] = "diskCache::Write";

        {
            std::ostringstream msg;

            msg << "Entry";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        diskCache::MountPointManagerSingleton::Write( Stream );
        diskCache::DirectoryManagerSingleton::Write( Stream );
        {
            std::ostringstream msg;

            msg << "Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        return Stream;
    }
} // namespace diskCache

#endif /* DISKCACHE_API__IO_HH */
