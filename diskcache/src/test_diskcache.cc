//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* */
//-----------------------------------------------------------------------
// This program tests the diskcache standalone command
//-----------------------------------------------------------------------

#include <cstring>

#if HAVE_VALGRIND_VALGRIND_H
#include "valgrind/valgrind.h"
#endif /* HAVE_VALGRIND_VALGRIND_H */

extern "C" {
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
} /* extern "C" */

#include <cstdlib>

#include <stdexcept>

#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/unittest.h"

#include "diskcacheAPI/Cache/RegistrySingleton.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"

#include "diskcacheAPI.hh"
#include "Commands.hh"
#include "IO.hh"
#include "MountPointManagerSingleton.hh"
#include "MountPointScanner.hh"

//-----------------------------------------------------------------------
// Forward declaration of test functions
//-----------------------------------------------------------------------

static void test_2942_multiple_extensions( );
static void test_2942_multiple_extensions_single_out( );
static void test_2942_multiple_extensions_single_out_non_gwf( );
static void test_1758_overlapping( );

#if USED_WORKING
static void check_file_count( const std::string& Leader,
                              const std::string& IFO_Type,
                              const INT_4U       Start,
                              const INT_4U       Stop,
                              const std::string& Extension,
                              const INT_4U       ExpectedFileCount );
#endif /* USED_WORKING */

//-----------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------
using diskCache::Cache::SDGTx;

LDASTools::Testing::UnitTest Test;
std::string                  ABS_BUILDDIR( "." );

const char*
valgrind( )
{
#if HAVE_VALGRIND_VALGRIND_H
    static const char* VALGRIND =
        PROGRAM_VALGRIND " --verbose"
                         " --demangle=yes"
                         " --track-origins=yes"
                         " --num-callers=40"
                         " --trace-children=yes"
                         " --log-file=test_diskcache.valgrind.log"
                         " ";

    if ( RUNNING_ON_VALGRIND && ( strlen( PROGRAM_VALGRIND ) > 0 ) )
    {
        return VALGRIND;
    }
#endif /* HAVE_VALGRIND_VALGRIND_H */
    return "";
}

//-----------------------------------------------------------------------
// Main testing
//-----------------------------------------------------------------------
int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

#if 0
  //---------------------------------------------------------------------
  // Variables for logging
  //---------------------------------------------------------------------
  static char cwd_buffer[ 2048 ];
  getcwd( cwd_buffer, sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) );
  std::string	cwd( cwd_buffer );
  GenericAPI::Log::stream_file_type	fs( new GenericAPI::Log::StreamFile );
  LDASTools::AL::Log::stream_type	s;

  s = fs;
  GenericAPI::LogFormatter( )->Stream( s );
  GenericAPI::LogFormatter( )->EntriesMax( 500 );
  GenericAPI::setLogTag( "test_diskcache" );
  GenericAPI::setLogOutputDirectory( cwd );
#endif /* 0 */

    {
        const char* abs_builddir = ::getenv( "abs_builddir" );
        if ( abs_builddir )
        {
            ABS_BUILDDIR = abs_builddir;
        }
    }

    diskCache::Initialize( );

    test_2942_multiple_extensions( );
    test_2942_multiple_extensions_single_out( );
    test_2942_multiple_extensions_single_out_non_gwf( );
    test_1758_overlapping( );

    diskCache::Teardown( );
    Test.Exit( );
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
static void
test_2942_multiple_extensions( )
{
#if USE_WORKING
    static const char* TEST_NAME = "2942-MultipleExtensions";
    static const char* TEST_DIR_ROOT = "test-ticket-2942";
    static const char* TEST_DIR_MIXED = "mixed";
    static const char* SDGT_BASE = "Z-Test_2942_MultipleExtensions";
    static const int   BASE_TIME_EXTENSION = 60000;
    static const int   DT = 32;
    static const int   DT_MAX = DT * 4;
    static const int   BASE_TIME_MIXED = 70000;

    std::ostringstream ascii_filename;
    std::ostringstream binary_filename;

    try
    {
        typedef std::list< const char* > extensions_container_type;

        char cwd[ 4096 ];

        if ( ::getcwd( cwd, sizeof( cwd ) ) == (const char*)NULL )
        {
            throw std::runtime_error(
                "Unable to get the current working directory" );
        }

        extensions_container_type extensions;

        extensions.push_back( "gwf" );
        extensions.push_back( "xml" );
        extensions.push_back( "sft" );
        //-------------------------------------------------------------------
        // Setup files for scanning
        //-------------------------------------------------------------------
        ::mkdir( TEST_DIR_ROOT, 0700 );

        extensions.push_back( TEST_DIR_MIXED );
        for ( extensions_container_type::const_iterator
                  cur = extensions.begin( ),
                  last = extensions.end( );
              cur != last;
              ++cur )
        {
            std::ostringstream dir;

            //-----------------------------------------------------------------
            // Create sub-directory
            //-----------------------------------------------------------------
            dir << TEST_DIR_ROOT << "/" << *cur;
            ::mkdir( dir.str( ).c_str( ), 0700 );
        }
        extensions.pop_back( );

        for ( int x = 0; x < DT_MAX; x += DT )
        {
            for ( extensions_container_type::const_iterator
                      cur = extensions.begin( ),
                      last = extensions.end( );
                  cur != last;
                  ++cur )
            {
                //---------------------------------------------------------------
                // Populate extension specific directory
                //---------------------------------------------------------------
                int                fd;
                std::ostringstream dir;
                std::ostringstream filename;

                dir << TEST_DIR_ROOT << "/" << *cur;
                filename << dir.str( ) << "/" << SDGT_BASE << "-"
                         << ( BASE_TIME_EXTENSION + x ) << "-" << DT << "."
                         << *cur;
                fd = ::creat( filename.str( ).c_str( ), 0700 );
                close( fd );
                //---------------------------------------------------------------
                // Populate mixed directory
                //---------------------------------------------------------------
                filename.str( "" );
                filename << TEST_DIR_ROOT << "/" << TEST_DIR_MIXED << "/"
                         << SDGT_BASE << "-" << ( BASE_TIME_MIXED + x ) << "-"
                         << DT << "." << *cur;
                fd = ::creat( filename.str( ).c_str( ), 0700 );
                close( fd );
            }
        }
        //-------------------------------------------------------------------
        // Scan the direcrory
        //-------------------------------------------------------------------
        ascii_filename << TEST_NAME << ".txt";
        binary_filename << TEST_NAME << ".cache";
        std::ostringstream cmd;

        cmd << valgrind( ) << ABS_BUILDDIR << "/diskcache"
            << " --log diskcache-" << TEST_NAME << " scan"
            << " --mount-points " << cwd << "/" << TEST_DIR_ROOT
            << " --extensions ";
        for ( extensions_container_type::const_iterator
                  first = extensions.begin( ),
                  cur = extensions.begin( ),
                  last = extensions.end( );
              cur != last;
              ++cur )
        {
            if ( cur != first )
            {
                cmd << ",";
            }
            cmd << "." << *cur;
        }
        cmd << " --output-ascii " << ascii_filename.str( )
            << " --version-ascii 0x0101"
            << " --output-binary " << binary_filename.str( )
            << " --version-binary 0x0101";
        int cmdstatus = system( cmd.str( ).c_str( ) );
        if ( cmdstatus != 0 )
        {
            throw( std::runtime_error( cmd.str( ).c_str( ) ) );
        }
        //-------------------------------------------------------------------
        // Verify the results
        //-------------------------------------------------------------------
        {
            //-----------------------------------------------------------------
            // Verify the binary file
            //-----------------------------------------------------------------
            LDASTools::AL::ifstream ifs( binary_filename.str( ).c_str( ) );

            if ( ifs.is_open( ) )
            {
                diskCache::Streams::IBinary stream( ifs );

                diskCache::Read( stream );
                ifs.close( );
            }

            //-----------------------------------------------------------------
            // Query for the files
            //-----------------------------------------------------------------
            std::ostringstream leader;

            leader.str( "" );
            leader << TEST_NAME << ": Binary";
            for ( extensions_container_type::const_iterator
                      cur = extensions.begin( ),
                      last = extensions.end( );
                  cur != last;
                  ++cur )
            {
                std::ostringstream ext;

                ext << "." << *cur;
                check_file_count( leader.str( ),
                                  SDGT_BASE,
                                  BASE_TIME_EXTENSION,
                                  BASE_TIME_MIXED + DT_MAX,
                                  ext.str( ),
                                  ( DT_MAX / DT ) * 2 );
            }
        }
    }
    catch ( const std::exception& Exception )
    {
        Test.Check( false ) << TEST_NAME << ": " << Exception.what( );
    }
    //---------------------------------------------------------------------
    // Cleanup
    //---------------------------------------------------------------------
#endif /* USE_WORKING */
}

static void
test_2942_multiple_extensions_single_out( )
{
#if USE_WORKING
    static const char* TEST_NAME = "2942-MultipleExtensionsSingleOut";
    static const char* TEST_DIR_ROOT = "test-ticket-2942";
    static const char* TEST_DIR_MIXED = "mixed";
    static const char* SDGT_BASE = "Z-Test_2942_MultipleExtensions";
    static const int   BASE_TIME_EXTENSION = 60000;
    static const int   DT = 32;
    static const int   DT_MAX = DT * 4;
    static const int   BASE_TIME_MIXED = 70000;

    std::ostringstream ascii_filename;
    std::ostringstream binary_filename;

    try
    {
        typedef std::pair< const char*, int > extension_type;
        typedef std::list< std::pair< const char*, int > >
            extensions_container_type;

        char cwd[ 4096 ];

        if ( ::getcwd( cwd, sizeof( cwd ) ) == (const char*)NULL )
        {
            throw std::runtime_error(
                "Unable to get the current working directory" );
        }

        extensions_container_type extensions;

        extensions.push_back( extension_type( "gwf", ( DT_MAX / DT ) * 2 ) );
        extensions.push_back( extension_type( "xml", 0 ) );
        extensions.push_back( extension_type( "sft", 0 ) );
        //-------------------------------------------------------------------
        // Setup files for scanning
        //-------------------------------------------------------------------
        ::mkdir( TEST_DIR_ROOT, 0700 );

        extensions.push_back( extension_type( TEST_DIR_MIXED, 0 ) );
        for ( extensions_container_type::const_iterator
                  cur = extensions.begin( ),
                  last = extensions.end( );
              cur != last;
              ++cur )
        {
            std::ostringstream dir;

            //-----------------------------------------------------------------
            // Create sub-directory
            //-----------------------------------------------------------------
            dir << TEST_DIR_ROOT << "/" << cur->first;
            ::mkdir( dir.str( ).c_str( ), 0700 );
        }
        extensions.pop_back( );

        for ( int x = 0; x < DT_MAX; x += DT )
        {
            for ( extensions_container_type::const_iterator
                      cur = extensions.begin( ),
                      last = extensions.end( );
                  cur != last;
                  ++cur )
            {
                //---------------------------------------------------------------
                // Populate extension specific directory
                //---------------------------------------------------------------
                int                fd;
                std::ostringstream dir;
                std::ostringstream filename;

                dir << TEST_DIR_ROOT << "/" << cur->first;
                filename << dir.str( ) << "/" << SDGT_BASE << "-"
                         << ( BASE_TIME_EXTENSION + x ) << "-" << DT << "."
                         << cur->first;
                fd = ::creat( filename.str( ).c_str( ), 0700 );
                close( fd );
                //---------------------------------------------------------------
                // Populate mixed directory
                //---------------------------------------------------------------
                filename.str( "" );
                filename << TEST_DIR_ROOT << "/" << TEST_DIR_MIXED << "/"
                         << SDGT_BASE << "-" << ( BASE_TIME_MIXED + x ) << "-"
                         << DT << "." << cur->first;
                fd = ::creat( filename.str( ).c_str( ), 0700 );
                close( fd );
            }
        }
        //-------------------------------------------------------------------
        // Scan the direcrory
        //-------------------------------------------------------------------
        ascii_filename << TEST_NAME << ".txt";
        binary_filename << TEST_NAME << ".cache";
        std::ostringstream cmd;

        cmd << valgrind( ) << ABS_BUILDDIR << "/diskcache"
            << " --log diskcache-" << TEST_NAME << " scan"
            << " --mount-points " << cwd << "/" << TEST_DIR_ROOT
            << " --extensions ";
        for ( extensions_container_type::const_iterator
                  first = extensions.begin( ),
                  cur = extensions.begin( ),
                  last = extensions.end( );
              cur != last;
              ++cur )
        {
            if ( cur != first )
            {
                cmd << ",";
            }
            cmd << "." << cur->first;
        }
        cmd << " --output-ascii " << ascii_filename.str( )
            << " --version-ascii 0x0100"
            << " --output-binary " << binary_filename.str( )
            << " --version-binary 0x0100";
        int cmdstatus = system( cmd.str( ).c_str( ) );
        if ( cmdstatus != 0 )
        {
            throw( std::runtime_error( cmd.str( ).c_str( ) ) );
        }
        //-------------------------------------------------------------------
        // Verify the results
        //-------------------------------------------------------------------
        {
            //-----------------------------------------------------------------
            // Verify the binary file
            //-----------------------------------------------------------------
            LDASTools::AL::ifstream     ifs( binary_filename.str( ).c_str( ) );
            diskCache::Streams::IBinary stream( ifs );

            diskCache::Read( stream );
            ifs.close( );

            //-----------------------------------------------------------------
            // Query for the files
            //-----------------------------------------------------------------
            std::ostringstream leader;

            leader.str( "" );
            leader << TEST_NAME << ": Binary";
            for ( extensions_container_type::const_iterator
                      cur = extensions.begin( ),
                      last = extensions.end( );
                  cur != last;
                  ++cur )
            {
                std::ostringstream ext;

                ext << "." << cur->first;
                check_file_count( leader.str( ),
                                  SDGT_BASE,
                                  BASE_TIME_EXTENSION,
                                  BASE_TIME_MIXED + DT_MAX,
                                  ext.str( ),
                                  cur->second );
            }
        }
    }
    catch ( const std::exception& Exception )
    {
        Test.Check( false ) << TEST_NAME << ": " << Exception.what( );
    }
    //---------------------------------------------------------------------
    // Cleanup
    //---------------------------------------------------------------------
#endif /* USE_WORKING */
}

static void
test_2942_multiple_extensions_single_out_non_gwf( )
{
#if USE_WORKING
    static const char* TEST_NAME = "2942-MultipleExtensionsSingleOutNonGWF";
    static const char* TEST_DIR_ROOT = "test-ticket-2942";
    static const char* TEST_DIR_MIXED = "mixed";
    static const char* SDGT_BASE = "Z-Test_2942_MultipleExtensions";
    static const int   BASE_TIME_EXTENSION = 60000;
    static const int   DT = 32;
    static const int   DT_MAX = DT * 4;
    static const int   BASE_TIME_MIXED = 70000;

    std::ostringstream ascii_filename;
    std::ostringstream binary_filename;

    try
    {
        typedef std::pair< const char*, int > extension_type;
        typedef std::list< std::pair< const char*, int > >
            extensions_container_type;

        char cwd[ 4096 ];

        if ( ::getcwd( cwd, sizeof( cwd ) ) == (const char*)NULL )
        {
            throw std::runtime_error(
                "Unable to get the current working directory" );
        }

        extensions_container_type extensions;

        extensions.push_back( extension_type( "xml", ( DT_MAX / DT ) * 2 ) );
        extensions.push_back( extension_type( "gwf", 0 ) );
        extensions.push_back( extension_type( "sft", 0 ) );
        //-------------------------------------------------------------------
        // Setup files for scanning
        //-------------------------------------------------------------------
        ::mkdir( TEST_DIR_ROOT, 0700 );

        extensions.push_back( extension_type( TEST_DIR_MIXED, 0 ) );
        for ( extensions_container_type::const_iterator
                  cur = extensions.begin( ),
                  last = extensions.end( );
              cur != last;
              ++cur )
        {
            std::ostringstream dir;

            //-----------------------------------------------------------------
            // Create sub-directory
            //-----------------------------------------------------------------
            dir << TEST_DIR_ROOT << "/" << cur->first;
            ::mkdir( dir.str( ).c_str( ), 0700 );
        }
        extensions.pop_back( );

        for ( int x = 0; x < DT_MAX; x += DT )
        {
            for ( extensions_container_type::const_iterator
                      cur = extensions.begin( ),
                      last = extensions.end( );
                  cur != last;
                  ++cur )
            {
                //---------------------------------------------------------------
                // Populate extension specific directory
                //---------------------------------------------------------------
                int                fd;
                std::ostringstream dir;
                std::ostringstream filename;

                dir << TEST_DIR_ROOT << "/" << cur->first;
                filename << dir.str( ) << "/" << SDGT_BASE << "-"
                         << ( BASE_TIME_EXTENSION + x ) << "-" << DT << "."
                         << cur->first;
                fd = ::creat( filename.str( ).c_str( ), 0700 );
                close( fd );
                //---------------------------------------------------------------
                // Populate mixed directory
                //---------------------------------------------------------------
                filename.str( "" );
                filename << TEST_DIR_ROOT << "/" << TEST_DIR_MIXED << "/"
                         << SDGT_BASE << "-" << ( BASE_TIME_MIXED + x ) << "-"
                         << DT << "." << cur->first;
                fd = ::creat( filename.str( ).c_str( ), 0700 );
                close( fd );
            }
        }
        //-------------------------------------------------------------------
        // Scan the direcrory
        //-------------------------------------------------------------------
        SDGTx::file_extension_container_type el;
        ascii_filename << TEST_NAME << ".txt";
        binary_filename << TEST_NAME << ".cache";
        std::ostringstream cmd;

        cmd << valgrind( ) << ABS_BUILDDIR << "/diskcache"
            << " --log diskcache-" << TEST_NAME << " scan"
            << " --mount-points " << cwd << "/" << TEST_DIR_ROOT
            << " --extensions ";
        for ( extensions_container_type::const_iterator
                  first = extensions.begin( ),
                  cur = extensions.begin( ),
                  last = extensions.end( );
              cur != last;
              ++cur )
        {
            std::ostringstream ext;
            if ( cur != first )
            {
                cmd << ",";
            }
            cmd << "." << cur->first;
            ext << "." << cur->first;
            el.push_back( ext.str( ) );
        }
        cmd << " --output-ascii " << ascii_filename.str( )
            << " --version-ascii 0x0100"
            << " --output-binary " << binary_filename.str( )
            << " --version-binary 0x0100";
        int cmdstatus = system( cmd.str( ).c_str( ) );
        if ( cmdstatus != 0 )
        {
            throw( std::runtime_error( cmd.str( ).c_str( ) ) );
        }
        //-------------------------------------------------------------------
        // Verify the results
        //-------------------------------------------------------------------
        {
            //-----------------------------------------------------------------
            // Setup the system
            //-----------------------------------------------------------------
            SDGTx::FileExtList( el, el.front( ) );
            //-----------------------------------------------------------------
            // Verify the binary file
            //-----------------------------------------------------------------
            LDASTools::AL::ifstream     ifs( binary_filename.str( ).c_str( ) );
            diskCache::Streams::IBinary stream( ifs );

            diskCache::Read( stream );
            ifs.close( );

            //-----------------------------------------------------------------
            // Query for the files
            //-----------------------------------------------------------------
            std::ostringstream leader;

            leader.str( "" );
            leader << TEST_NAME << ": Binary";
            for ( extensions_container_type::const_iterator
                      cur = extensions.begin( ),
                      last = extensions.end( );
                  cur != last;
                  ++cur )
            {
                std::ostringstream ext;

                ext << "." << cur->first;
                check_file_count( leader.str( ),
                                  SDGT_BASE,
                                  BASE_TIME_EXTENSION,
                                  BASE_TIME_MIXED + DT_MAX,
                                  ext.str( ),
                                  cur->second );
            }
        }
    }
    catch ( const std::exception& Exception )
    {
        Test.Check( false ) << TEST_NAME << ": " << Exception.what( );
    }
    //---------------------------------------------------------------------
    // Cleanup
    //---------------------------------------------------------------------
#endif /* USE_WORKING */
}

static void
test_1758_overlapping( )
{
#if 0
#define TICKET "1758"

  static const char* TEST_NAME = TICKET "-Overlapping";
  static const char* TEST_DIR_ROOT = "test-ticket-" TICKET;
  static const char* TEST_DIR_MIXED = "overlapping";
  static const char* SDGT_BASE = "Z-Test_" TICKET "_Overlapping";
  static const int BASE_TIME_EXTENSION = 60000;
  static const int DT = 32;
  static const int DT_MAX = DT * 4;
  static const int BASE_TIME_MIXED = 70000;

#undef TICKET

  std::ostringstream	ascii_filename;
  std::ostringstream	binary_filename;
  
  try
  {
    typedef std::list<const char*> extensions_container_type;

    char cwd[4096];

    if ( ::getcwd( cwd, sizeof(cwd) ) == (const char*)NULL )
    {
      throw std::runtime_error( "Unable to get the current working directory" );
    }

    extensions_container_type	extensions;

    extensions.push_back( "gwf" );
    extensions.push_back( "xml" );
    extensions.push_back( "sft" );
    //-------------------------------------------------------------------
    // Setup files for scanning
    //-------------------------------------------------------------------
    ::mkdir( TEST_DIR_ROOT, 0700 );

    extensions.push_back( TEST_DIR_MIXED );
    for( extensions_container_type::const_iterator
	   cur = extensions.begin( ),
	   last = extensions.end( );
	 cur != last;
	 ++cur )
    {
      std::ostringstream	dir;

      //-----------------------------------------------------------------
      // Create sub-directory
      //-----------------------------------------------------------------
      dir << TEST_DIR_ROOT << "/" << *cur;
      ::mkdir( dir.str( ).c_str( ), 0700 );
    }
    extensions.pop_back( );

    for ( int x = 0; x < DT_MAX; x += DT )
    {
      for( extensions_container_type::const_iterator
	     cur = extensions.begin( ),
	     last = extensions.end( );
	   cur != last;
	   ++cur )
      {
	//---------------------------------------------------------------
	// Populate extension specific directory
	//---------------------------------------------------------------
	int			fd;
	std::ostringstream	dir;
	std::ostringstream	filename;

	dir << TEST_DIR_ROOT << "/" << *cur;
	filename << dir.str( ) << "/"
		 << SDGT_BASE << "-" << ( BASE_TIME_EXTENSION + x )
		 << "-" << DT << "." << *cur
	  ; 
	fd = ::creat( filename.str( ).c_str( ), 0700 );
	close( fd );
	//---------------------------------------------------------------
	// Populate mixed directory
	//---------------------------------------------------------------
	filename.str( "" );
	filename << TEST_DIR_ROOT << "/" << TEST_DIR_MIXED << "/"
		 << SDGT_BASE << "-" << ( BASE_TIME_MIXED + x )
		 << "-" << DT << "." << *cur
	  ; 
	fd = ::creat( filename.str( ).c_str( ), 0700 );
	close( fd );
      }
    }
    //-------------------------------------------------------------------
    // Create Overlapping file
    //-------------------------------------------------------------------
    {
      int			start = ( BASE_TIME_MIXED + ( DT / 2 ) );
      int			fd;
      std::ostringstream	dir;
      std::ostringstream	filename;

      filename.str( "" );
      filename << TEST_DIR_ROOT << "/" << TEST_DIR_MIXED << "/"
					  << SDGT_BASE << "-" << ( start )
	       << "-" << DT << ".gwf"
	; 
      fd = ::creat( filename.str( ).c_str( ), 0700 );
      close( fd );
    }
    
    //-------------------------------------------------------------------
    // Scan the direcrory
    //-------------------------------------------------------------------
    ascii_filename << TEST_NAME << ".txt";
    binary_filename << TEST_NAME << ".cache";
    std::ostringstream	cmd;

    cmd << valgrind( )
	<< ABS_BUILDDIR << "/diskcache"
	<< " --log diskcache-" << TEST_NAME
	<< " scan"
	<< " --mount-points " << cwd << "/" << TEST_DIR_ROOT
	<< " --extensions "
      ;
    for( extensions_container_type::const_iterator
	   first = extensions.begin( ),
	   cur = extensions.begin( ),
	   last = extensions.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	cmd << ",";
      }
      cmd << "." << *cur;
    }
    cmd << " --output-ascii " << ascii_filename.str( )
	<< " --version-ascii 0x0101"
	<< " --output-binary " << binary_filename.str( )
	<< " --version-binary 0x0101"
      ;
    int cmdstatus = system( cmd.str( ).c_str( ) );
    if ( cmdstatus != 0 )
    {
      throw( std::runtime_error( cmd.str( ).c_str( ) ) );
    }
    //-------------------------------------------------------------------
    // Verify the results
    //-------------------------------------------------------------------
    {
      //-----------------------------------------------------------------
      // Verify the binary file
      //-----------------------------------------------------------------
      LDASTools::AL::ifstream	ifs( binary_filename.str( ).c_str( ) );

      if ( ifs.is_open( ) )
      {
	diskCache::Streams::IBinary	stream( ifs );

	diskCache::Read( stream );
	ifs.close( );
      }

      //-----------------------------------------------------------------
      // Query for the files
      //-----------------------------------------------------------------
      std::ostringstream	leader;

      leader.str( "" );
      leader << TEST_NAME << ": Binary";
      for( extensions_container_type::const_iterator
	     cur = extensions.begin( ),
	     last = extensions.end( );
	   cur != last;
	   ++cur )
      {
	std::ostringstream	ext;

	ext << "." << *cur;
	check_file_count( leader.str( ) ,
			  SDGT_BASE,
			  BASE_TIME_EXTENSION,
			  BASE_TIME_MIXED + DT_MAX,
			  ext.str( ),
			  (DT_MAX / DT ) * 2 );
      }
    }
  }
  catch( const std::exception& Exception )
  {
    Test.Check( false )
      << TEST_NAME
      << ": " << Exception.what( )
      ;
  }
  //---------------------------------------------------------------------
  // Cleanup
  //---------------------------------------------------------------------
#endif /* 0 */
}

#if USED_WORKING
inline static void
check_file_count( const std::string& Leader,
                  const std::string& IFO_Type,
                  const INT_4U       Start,
                  const INT_4U       Stop,
                  const std::string& Extension,
                  const INT_4U       ExpectedFileCount )
{
    typedef diskCache::Cache::QueryAnswer::filename_container_type
        file_container_type;

    diskCache::Cache::QueryAnswer answer;
    file_container_type           files;

    diskCache::Commands::getFileNames(
        answer, IFO_Type.c_str( ), Start, Stop, Extension );
    answer.Complete( );
    answer.Swap( files );
    Test.Check( files.size( ) == ExpectedFileCount )
        << Leader << ": cache file count for " << Extension << " files"
        << " [ " << files.size( ) << " =?= " << ExpectedFileCount << "  ]"
        << std::endl;
}
#endif /* USED_WORKING */
