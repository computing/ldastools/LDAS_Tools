//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__CACHE__EXCLUDED_DIRECTORIES_HH
#define DISKCACHE__CACHE__EXCLUDED_DIRECTORIES_HH

#include <list>
#include <string>

#include "ldastoolsal/regex.hh"
#include "ldastoolsal/regexmatch.hh"

namespace diskCache
{
    namespace Cache
    {
        class ExcludedDirectories
        {
        public:
            typedef std::list< std::string > directory_container_type;
            typedef directory_container_type::value_type value_type;
            typedef void ( *callback_type )( );

            ExcludedDirectories( );

            bool IsExcluded( const value_type& Directory ) const;

            void Update( const directory_container_type& Directories );

            void OnUpdate( callback_type Callback );

        private:
            typedef std::list< callback_type > callback_container_type;

            Regex                   pattern;
            callback_container_type callbacks;
        };

        inline bool
        ExcludedDirectories::IsExcluded( const value_type& Directory ) const
        {
            RegexMatch compare( 1 );

            //-----------------------------------------------------------------
            // If the pattern is found, then it should be excluded.
            //-----------------------------------------------------------------
            return compare.match( pattern, Directory.c_str( ) );
        }

        inline void
        ExcludedDirectories::OnUpdate( callback_type Callback )
        {
            callbacks.push_back( Callback );
        }
    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE__CACHE__EXCLUDED_DIRECTORIES_HH */
