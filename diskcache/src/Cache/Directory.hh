//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__DIRECTORY_HH
#define DISKCACHE_API__CACHE__DIRECTORY_HH

#include <cassert>

#include <list>
#include <map>
#include <set>
#include <stdexcept>
#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/types.hh"
#include "ldastoolsal/unordered_map.hh"

#include "diskcacheAPI/Streams/StreamsInterface.hh"

#include "diskcacheAPI/Cache/Devices.hh"
#include "diskcacheAPI/Cache/RegistrySingleton.hh"
#include "diskcacheAPI/Cache/SearchInterface.hh"

#define NEW_DIRECTORY_INTERFACE 1

namespace diskCache
{
    extern int RWLOCK_TIMEOUT;

    namespace Streams
    {
        class OASCII;
    }

    namespace Cache
    {
        class DirectoryManager;
        struct DirectoryScanData;
        class QueryAnswer;
        class StatInfo;

        //-------------------------------------------------------------------
        /// \brief Directory caching scheme
        ///
        /// This caching scheme is for the caching of directory information.
        /// It is not used directly by end users to search for files,
        /// but instead is used as the driver for detecting changes to
        /// the file system and causing search caches to be updated.
        //-------------------------------------------------------------------
        class Directory
        {
        public:
            enum
            {
                //---------------------------------------------------------------
                /// \brief Directory needs to be rescanned
                ///
                /// This state is useful for flagging a single directory for
                /// rescanning.
                /// Some examples of when a directory would be flagged in this
                /// manor would be the:
                /// <ul>
                ///   <li>one second bug</li>
                ///   <li>Symbolic links appear in the directory.</li>
                /// </ul>
                //---------------------------------------------------------------
                MODIFIED_RESET,
                //---------------------------------------------------------------
                /// \brief Directory has been newly created
                ///
                /// When a directory is first seen, it is given this state.
                /// This allows for some optimization as it does not need to
                /// Calculate any differences.
                //---------------------------------------------------------------
                MODIFIED_NEW_DIRECTORY,
                //---------------------------------------------------------------
                /// \brief A timeout occured during reading
                ///
                /// This error indicates that a directory or subdirectory
                /// received a timeout error condition during a scan.
                /// The mount point was flagged as being offline
                /// and flagged with this state.
                /// Once the mount point becomes available,
                /// the entire mount point is rescanned.
                //---------------------------------------------------------------
                MODIFIED_OFFLINE
            };

            typedef enum
            {
                UNKNOWN_TYPE,
                FILE_TYPE,
                DIRECTORY_TYPE
            } file_type;

            typedef boost::shared_ptr< Directory > dirref_type;
            typedef INT_4U                         size_type;
            typedef INT_4U                         timestamp_type;
            typedef std::set< std::string >        children_type;
            typedef std::set< std::string >        ignored_type;

            typedef RegistrySingleton::id_type               search_id_type;
            typedef boost::shared_ptr< Streams::Streamable > search_data_type;
            typedef LDASTools::AL::unordered_map< search_id_type,
                                                  search_data_type >
                index_container_type;

            enum directory_state
            {
                DIRECTORY_NEW,
                DIRECTORY_UPDATED,
                DIRECTORY_REMOVED,
                DIRECTORY_OFFLINE
            };

            class ScanResults
            {
            public:
                typedef std::list< std::string >   filename_container_type;
                typedef INT_4U                     directory_count_type;
                typedef INT_4U                     file_count_type;
                typedef Directory::directory_state directory_state;

                struct count_type
                {
                    INT_4U s_added;
                    INT_4U s_removed;
                };

                struct directory_type
                {
                    filename_container_type s_added;
                    filename_container_type s_removed;
                };

                typedef std::map< std::string, count_type > file_container_type;
                struct info_type
                {
                    directory_state     s_state;
                    directory_type      s_subdirs;
                    file_container_type s_engines;
                };

                typedef std::map< std::string, info_type >
                    directory_container_type;

                ScanResults( );

                //---------------------------------------------------------------
                /// \brief Retrieve the collection of added directories
                ///
                /// \param[in] DirectoryName
                ///     The parent directory
                ///
                //---------------------------------------------------------------
                const filename_container_type&
                Added( const std::string& DirectoryName ) const;

                //---------------------------------------------------------------
                /// \brief Add the directories that were newly discovered
                ///
                /// \param[in] DirectoryName
                ///     The parent directory
                ///
                /// \param[in,out] Subdirectories
                ///     The collection of directories that have been newly
                ///     added.
                //---------------------------------------------------------------
                void Added( const std::string&       DirectoryName,
                            filename_container_type& Subdirectories );

                void Added( const std::string& DirectoryName,
                            const std::string& SearchEngine,
                            INT_4U             FileCount );

                //---------------------------------------------------------------
                /// \brief Retrieve the number of directories scanned.
                //---------------------------------------------------------------
                directory_count_type DirectoryCount( ) const;

                //---------------------------------------------------------------
                /// \brief Retrieve the number of directories scanned.
                //---------------------------------------------------------------
                void DirectoryCountInc( );

                //---------------------------------------------------------------
                /// \brief Retrieve the number of Files scanned.
                //---------------------------------------------------------------
                file_count_type FileCount( ) const;

                //---------------------------------------------------------------
                /// \brief Increment the number of Files scanned.
                ///
                /// \param[in] Value
                ///     The amount by which to incriment the file count.
                //---------------------------------------------------------------
                void FileCountInc( file_count_type Value );

                //---------------------------------------------------------------
                /// \brief Create a log message
                //---------------------------------------------------------------
                void Log( const std::string& Caller,
                          const std::string& JobInfo ) const;

                //---------------------------------------------------------------
                /// \brief Record the directory state at the time of scanning
                ///
                /// \param[in] DirectoryName
                ///     The name of the directory that was scanned.
                /// \param[in] State
                ///     The state of the directory at the time of being scanned.
                //---------------------------------------------------------------
                void State( const std::string& DirectoryName,
                            directory_state    State );

                //---------------------------------------------------------------
                /// \brief Retrieve the collection of removed directories
                ///
                /// \param[in] DirectoryName
                ///     The parent directory
                ///
                //---------------------------------------------------------------
                const filename_container_type&
                Removed( const std::string& DirectoryName ) const;

                //---------------------------------------------------------------
                /// \brief Add the directories that were removed
                ///
                /// \param[in] DirectoryName
                ///     The parent directory
                ///
                /// \param[in,out] Subdirectories
                ///     The collection of directories that have been newly
                ///     added.
                //---------------------------------------------------------------
                void Removed( const std::string&       DirectoryName,
                              filename_container_type& Subdirectories );

                void Removed( const std::string& DirectoryName,
                              const std::string& SearchEngine,
                              INT_4U             FileCount );

                const directory_container_type& Results( ) const;

                //---------------------------------------------------------------
                /// \brief Scan through the results
                //---------------------------------------------------------------
                template < typename Op >
                inline void
                operator( )( Op Operation ) const
                {
                    for ( directory_container_type::const_iterator
                              dir_cur = m_results.begin( ),
                              dir_last = m_results.end( );
                          dir_cur != dir_last;
                          ++dir_cur )
                    {
                        file_count_type files_added( 0 );
                        file_count_type files_deleted( 0 );

                        for ( file_container_type::const_iterator
                                  engine_cur =
                                      dir_cur->second.s_engines.begin( ),
                                  engine_last =
                                      dir_cur->second.s_engines.end( );
                              engine_cur != engine_last;
                              ++engine_cur )
                        {
                            files_added += engine_cur->second.s_added;
                            files_deleted += engine_cur->second.s_removed;
                        }
                        Operation( dir_cur->first,
                                   dir_cur->second.s_state,
                                   files_added,
                                   files_deleted );
                    }
                }

            private:
                directory_count_type m_scanned_directories;
                file_count_type      m_scanned_files;

                directory_container_type m_results;
            };

            typedef std::list< std::string > excluded_directories_type;
            typedef LDASTools::AL::ReadWriteLockVariable<
                LDASTools::AL::ReadWriteLock::WRITE,
                excluded_directories_type >
                excluded_directories_rw_type;

            typedef LDASTools::AL::ReadWriteLockVariable<
                LDASTools::AL::ReadWriteLock::READ,
                excluded_directories_type >
                excluded_directories_ro_type;

            //-----------------------------------------------------------------
            /// \brief Exception thrown when a file could not be added to a
            /// cache
            ///
            /// This should be used by the indexing scheme as the base for
            /// exceptions indicating that a file could not be added to
            /// the cache.
            //-----------------------------------------------------------------
            class FileCacheError : public std::runtime_error
            {
            public:
                FileCacheError( const std::string& Filename,
                                const std::string& Reason );

                FileCacheError( const std::string& Reason );

            private:
                static std::string format( const std::string& Filename,
                                           const std::string& Reason );
            };

            //-----------------------------------------------------------------
            /// \brief The top most entry is not a directory
            ///
            /// The top most entry must always be a directory.
            //-----------------------------------------------------------------
            class StartIsNotADirectoryError : public std::runtime_error
            {
            public:
                StartIsNotADirectoryError( const std::string& Path,
                                           const char*        MethodName );

            private:
                static std::string format( const std::string& Path,
                                           const char*        MethodName );
            };

            //-----------------------------------------------------------------
            /// \brief Report symbolic links as errors
            ///
            /// The current implementation of the diskcacheAPI does
            /// not support the use of symbolic links as they may result in
            /// cyclical graphs and such.
            //-----------------------------------------------------------------
            class SymbolicLinkError : public std::runtime_error
            {
            public:
                SymbolicLinkError( const std::string& Path,
                                   const char*        MethodName );

            private:
                static std::string format( const std::string& Path,
                                           const char*        MethodName );
            };

            //-----------------------------------------------------------------
            /// \brief Default Constructor
            //-----------------------------------------------------------------
            Directory( );

            //-----------------------------------------------------------------
            /// \brief Constructor with a directory path
            ///
            /// \param[in] Path
            ///     The fully qualified path.
            /// \param[in] Root
            ///     The root directory
            //-----------------------------------------------------------------
            Directory( const std::string& Path, const std::string& Root );

            //-----------------------------------------------------------------
            /// \brief Constructor with a directory path and parent
            ///
            /// \param[in] Path
            ///     The fully qualified path.
            ///
            /// \param[in] Parent
            ///     The parent of this node
            //-----------------------------------------------------------------
            Directory( const std::string& Path, dirref_type Parent );

            //-----------------------------------------------------------------
            /// \brief Destructor
            //-----------------------------------------------------------------
            virtual ~Directory( );

            //-----------------------------------------------------------------
            //
            //-----------------------------------------------------------------
            void
            AddSubdirectory( dirref_type Dir )
            {
            }

            //-----------------------------------------------------------------
            /// \brief Iterate over the collection of subdirectories
            ///
            /// \param[out] Operation
            ///     This is the action that should be taken for each
            ///     subdirectory.
            //-----------------------------------------------------------------
            template < typename Op >
            void Children( Op& Operation ) const;

            //-----------------------------------------------------------------
            /// \brief Reverse iterate over the collection of subdirectories
            ///
            /// \param[out] Operation
            ///     This is the action that should be taken for each
            ///     subdirectory.
            //-----------------------------------------------------------------
            template < typename Op >
            void ChildrenReverse( Op& Operation ) const;

            //-----------------------------------------------------------------
            /// \brief Allow for customization
            ///
            /// \param[in] Variable
            ///     Name of the option to modify
            /// \param[in] Value
            ///     The ascii string representation of the value for the option.
            ///
            /// \return
            ///     Upon successful setting of the option, true is returned;
            ///     false otherwise.
            //-----------------------------------------------------------------
            virtual bool Configure( const std::string& Variable,
                                    const std::string& Value );

            //-----------------------------------------------------------------
            // \brief Query the number of directories being managed
            //-----------------------------------------------------------------
            size_type DirectoryCount( ) const;

            //-----------------------------------------------------------------
            /// \brief Obtain patterns of directories to exclude
            //-----------------------------------------------------------------
            static const excluded_directories_ro_type ExcludedDirectories( );

            //-----------------------------------------------------------------
            /// \brief Modify the list of directories to be excluded
            ///
            /// \param[in,out] Dirs
            ///     The list of new directories to be excluded.
            ///     The list passed by this variable will be swapped
            ///     with the current list of directories to be excluded.
            //-----------------------------------------------------------------
            static void ExcludedDirectories( excluded_directories_type& Dirs );

            //-----------------------------------------------------------------
            /// \brief Find a collection of files.
            ///
            /// \param[in] Answer
            ///     Local storage for query and answer.
            //-----------------------------------------------------------------
            void Find( QueryAnswer& Answer ) const;

            //-----------------------------------------------------------------
            // \brief Query the number of files being managed
            //-----------------------------------------------------------------
            size_type FileCount( ) const;

            //-----------------------------------------------------------------
            /// \brief Return the fully qualified name of the directory.
            //-----------------------------------------------------------------
            const std::string& Fullname( ) const;

            //-----------------------------------------------------------------
            /// \brief Return true if this object represents a root directory
            //-----------------------------------------------------------------
            bool IsRoot( ) const;

            //-----------------------------------------------------------------
            /// \brief Return true if the directory is offline
            //-----------------------------------------------------------------
            bool IsOffline( ) const;

            //-----------------------------------------------------------------
            /// \brief Return the relative name of the directory.
            ///
            /// \return
            ///     The relative name of the directory.
            //-----------------------------------------------------------------
            const std::string& Name( ) const;

            //-----------------------------------------------------------------
            /// \brief Read the common information from a stream
            //-----------------------------------------------------------------
            template < typename StreamT >
            StreamT& Read( StreamT& Stream );

            //-----------------------------------------------------------------
            /// \brief Retrieve the directory where this tree is rooted.
            ///
            /// \return
            ///     The name of the earliest node in the tree.
            //-----------------------------------------------------------------
            const std::string& Root( ) const;

            //-----------------------------------------------------------------
            /// \brief Scan current directory directory.
            ///
            /// \return
            ///     If differences are found while scanning, then a new node
            ///     is returned which reflects the current status of the
            ///     directory.
            ///     All other cases result a NULL node being returned.
            //-----------------------------------------------------------------
            dirref_type Scan( DirectoryManager&  DirectoryCollection,
                              ScanResults&       Results,
                              const std::string& Caller,
                              const std::string& JobInfo ) const;

            //-----------------------------------------------------------------
            /// \brief Report the last time the directory had been modified
            //-----------------------------------------------------------------
            timestamp_type TimeModified( ) const;

            //-----------------------------------------------------------------
            /// \brief Write the information out to a stream
            //-----------------------------------------------------------------
            template < typename StreamT >
            StreamT& Write( StreamT& Stream ) const;

#if WORKING
            //-----------------------------------------------------------------
            /// \brief Print the contents of the tree to the stream
            //-----------------------------------------------------------------
            template < typename StreamT >
            StreamT& WriteRecursive( StreamT& Stream ) const;
#endif /* WORKING */

        protected:
            //-----------------------------------------------------------------
            /// \brief Return the parent associated with this directory.
            //-----------------------------------------------------------------
            boost::shared_ptr< Directory > parent( ) const;

            //-----------------------------------------------------------------
            /// \brief Retrieve the set of children
            //-----------------------------------------------------------------
            const children_type& children( ) const;

            search_data_type searchData( search_id_type Key );

        private:
            friend class functor_reset;

            template < typename StreamT >
            class functor_dump
            {
            public:
                functor_dump( StreamT& Stream );

                void operator( )( Directory::dirref_type Node ) const;
                void operator( )( const Directory* const Node ) const;

            private:
                StreamT& m_stream;
            };

            class functor_reset
            {
            public:
                void operator( )( Directory::dirref_type Node );
                void operator( )( Directory* Node );
            };

            static LDASTools::AL::MutexLock::baton_type SDGTX_ID_baton;
            static RegistrySingleton::id_type           SDGTx_ID;
            static RegistrySingleton::ascii_key_type    SDGTx_ID_KEY;

            static excluded_directories_rw_type::baton_type
                                             p_excluded_directories_baton;
            static excluded_directories_type p_excluded_directories;

            index_container_type m_indexes;
            //-----------------------------------------------------------------
            /// \brief Device on which the directory is located.
            //-----------------------------------------------------------------
            mutable Devices::element_type device;
            //-----------------------------------------------------------------
            /// \brief Collection of subdirectories.
            ///
            /// This collection of subdirectories stores the fully qualified
            /// name of the directory.
            ///
            /// \todo Optimization -
            /// The collection of subdirectory names should only contain
            /// the name relative to the current directory.
            //-----------------------------------------------------------------
            children_type  m_subdirs;
            ignored_type   m_subdirs_ignored;
            std::string    m_name;
            timestamp_type m_last_time_modified;
            std::string    m_root;
            bool           is_root;

            //-----------------------------------------------------------------
            /// \brief Prevent being modified by multiple threads
            ///
            /// This variable is used to syncronize access to the scan method
            /// so multiple scanners can be used on the
            //-----------------------------------------------------------------
            mutable LDASTools::AL::MutexLock::baton_type m_scan_baton;
            directory_state                              m_state;

            //-----------------------------------------------------------------
            /// \brief Copy Constructor
            //-----------------------------------------------------------------
            Directory( const Directory& Source );

            //-----------------------------------------------------------------
            /// \brief Get a modifiable version of the excluded dirs
            //-----------------------------------------------------------------
            static excluded_directories_rw_type excluded_directories_rw( );

            //-----------------------------------------------------------------
            /// \brief Get the id for the SDGTx indexing class
            //-----------------------------------------------------------------
            static RegistrySingleton::id_type get_sdgtx_id( );

            //-----------------------------------------------------------------
            /// \brief Check for direct relationship
            //-----------------------------------------------------------------
            bool is_parent_of( dirref_type Child ) const;

            //-----------------------------------------------------------------
            // \brief Determine if the directory has been updated.
            //-----------------------------------------------------------------
            static bool is_updated( timestamp_type Old, timestamp_type New );

            dirref_type update( ScanResults&            Results,
                                const DirectoryManager& DirectoryCollection,
                                const StatInfo&         Stat ) const;

            void read_directory( DirectoryScanData& ScanData,
                                 const StatInfo&    Stat );

            void reset( boost::shared_ptr< Directory > Dir );
        };

        struct DirectoryScanData
        {
            //-----------------------------------------------------------------
            /// \brief Storage for search specific data while scanning
            ///
            /// This class serves as the base for private data needed by
            /// search algorithms during the scan of a directory.
            /// Search algorithms should derive from this base and add
            /// to extend it with additional information and/or methods.
            //-----------------------------------------------------------------
            struct scan_data
            {
                typedef Directory::search_data_type search_data_type;
                typedef INT_4U                      size_type;

                //---------------------------------------------------------------
                /// \brief Destructor
                ///
                /// Since this is the base, the destructor is virtual.
                //---------------------------------------------------------------
                virtual ~scan_data( );

                //---------------------------------------------------------------
                /// \brief Virtual constructor
                ///
                /// Create a new instance of the parent.
                //---------------------------------------------------------------
                virtual scan_data* Clone( ) const = 0;

                //---------------------------------------------------------------
                // \brief Retrieve the number of files being managed
                //---------------------------------------------------------------
                virtual size_type Count( ) const = 0;

                //---------------------------------------------------------------
                /// \brief Retrieve the collection of directories
                //---------------------------------------------------------------
                const DirectoryManager& DirManager( ) const;

                //---------------------------------------------------------------
                /// \brief Retrieve the collection of directories
                //---------------------------------------------------------------
                const std::string& Root( ) const;

                //---------------------------------------------------------------
                /// \brief Retrieve the search data
                //---------------------------------------------------------------
                virtual search_data_type SearchData( ) = 0;
            };

            typedef boost::shared_ptr< scan_data >  scan_data_type;
            typedef Directory::index_container_type searches_data_type;
            typedef LDASTools::AL::unordered_map< RegistrySingleton::id_type,
                                                  scan_data_type >
                searches_scan_data_type;

            std::string               s_directory_root;
            std::string               s_filename;
            searches_scan_data_type   s_searches_scan_data;
            const searches_data_type& s_old_searches_data;

            searches_data_type& s_updated_searches_data;

            Directory::ScanResults& s_results;

            //-----------------------------------------------------------------
            /// \brief Container for the updated search engine information
            ///
            /// \param[in] OldData
            ///
            /// \param[out] UpdatedData
            ///
            /// \param[out] Results
            ///
            /// \param[in] DirectoryCollection
            ///
            /// \param[in] Root
            ///     Directory from where to start.
            ///
            /// \note
            ///     This should only be referenced by
            ///     diskCache::Registry::OnDirectoryClose.
            //-----------------------------------------------------------------
            DirectoryScanData( const searches_data_type& OldData,
                               searches_data_type&       UpdatedData,
                               Directory::ScanResults&   Results,
                               const DirectoryManager&   DirectoryCollection,
                               const std::string&        Root );

            const DirectoryManager& DirManager( ) const;

            const std::string& Root( ) const;

        private:
            const DirectoryManager& m_directory_manager;
            std::string             m_root;
        };

        //===================================================================
        // Class - Directory::ScanResults
        //===================================================================
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline const Directory::ScanResults::filename_container_type&
        Directory::ScanResults::Added( const std::string& DirectoryName ) const
        {
            directory_container_type::const_iterator dir =
                m_results.find( DirectoryName );

            if ( dir == m_results.end( ) )
            {
                throw std::range_error( "no directory" );
            }
            return dir->second.s_subdirs.s_added;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::Added( const std::string&       DirectoryName,
                                       filename_container_type& Subdirectories )
        {
            m_results[ DirectoryName ].s_subdirs.s_added.swap( Subdirectories );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::Added( const std::string& DirectoryName,
                                       const std::string& SearchEngine,
                                       INT_4U             FileCount )
        {
            m_results[ DirectoryName ].s_engines[ SearchEngine ].s_added =
                FileCount;
        }

        //-------------------------------------------------------------------
        /// Return the number of directorys that have been scanned.
        //-------------------------------------------------------------------
        inline Directory::ScanResults::directory_count_type
        Directory::ScanResults::DirectoryCount( ) const
        {
            return m_scanned_directories;
        }

        //-------------------------------------------------------------------
        /// Increment the number of directorys that have been scanned.
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::DirectoryCountInc( )
        {
            ++m_scanned_directories;
        }

        //-------------------------------------------------------------------
        /// Return the number of files that have been scanned.
        //-------------------------------------------------------------------
        inline Directory::ScanResults::file_count_type
        Directory::ScanResults::FileCount( ) const
        {
            return m_scanned_files;
        }

        //-------------------------------------------------------------------
        /// Increment the number of files that have been scanned.
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::FileCountInc( file_count_type Value )
        {
            m_scanned_files += Value;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::State( const std::string& DirectoryName,
                                       directory_state    State )
        {
            m_results[ DirectoryName ].s_state = State;
        }
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline const Directory::ScanResults::filename_container_type&
        Directory::ScanResults::Removed(
            const std::string& DirectoryName ) const
        {
            directory_container_type::const_iterator dir =
                m_results.find( DirectoryName );

            if ( dir == m_results.end( ) )
            {
                throw std::range_error( "no directory" );
            }
            return dir->second.s_subdirs.s_removed;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::Removed(
            const std::string&        DirectoryName,
            std::list< std::string >& Subdirectories )
        {
            m_results[ DirectoryName ].s_subdirs.s_removed.swap(
                Subdirectories );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline void
        Directory::ScanResults::Removed( const std::string& DirectoryName,
                                         const std::string& SearchEngine,
                                         INT_4U             FileCount )
        {
            m_results[ DirectoryName ].s_engines[ SearchEngine ].s_removed =
                FileCount;
        }

        inline const Directory::ScanResults::directory_container_type&
        Directory::ScanResults::Results( ) const
        {
            return m_results;
        }

        //===================================================================
        // Class - Directory
        //===================================================================

        template < typename Op >
        inline void
        Directory::Children( Op& Operation ) const
        {
            for ( children_type::const_iterator cur = m_subdirs.begin( ),
                                                last = m_subdirs.end( );
                  cur != last;
                  ++cur )
            {
                Operation( *cur );
            }
        }
        template < typename Op >
        inline void
        Directory::ChildrenReverse( Op& Operation ) const
        {
            for ( children_type::const_reverse_iterator
                      cur = m_subdirs.rbegin( ),
                      last = m_subdirs.rend( );
                  cur != last;
                  ++cur )
            {
                Operation( *cur );
            }
        }

        inline Directory::size_type
        Directory::DirectoryCount( ) const
        {
            return m_subdirs.size( );
        }

        inline const Directory::excluded_directories_ro_type
        Directory::ExcludedDirectories( )
        {
            return ( excluded_directories_ro_type( p_excluded_directories_baton,
                                                   diskCache::RWLOCK_TIMEOUT,
                                                   p_excluded_directories,
                                                   __FILE__,
                                                   __LINE__ ) );
        }

        inline void
        Directory::ExcludedDirectories( excluded_directories_type& Dirs )
        {
            excluded_directories_rw_type excluded_dirs(
                excluded_directories_rw( ) );

            excluded_dirs.Var( ).swap( Dirs );
        }

        inline Directory::size_type
        Directory::FileCount( ) const
        {
            size_type retval( 0 );

            for ( index_container_type::const_iterator cur = m_indexes.begin( ),
                                                       last = m_indexes.end( );
                  cur != last;
                  ++cur )
            {
                if ( cur->second )
                {
                    retval += cur->second->Count( );
                }
            }
            return retval;
        }

        //-------------------------------------------------------------------
        /// Retieve the fully pathed name for the requested directory.
        /// This is achieved by walking up the geneology until the
        /// root node.
        //-------------------------------------------------------------------
        inline const std::string&
        Directory::Fullname( ) const
        {
            return m_name;
        }

        //-------------------------------------------------------------------
        //
        //-------------------------------------------------------------------
        inline bool
        Directory::IsOffline( ) const
        {
            if ( device )
            {
                return device->IsOffline( );
            }
            return Devices::IsOffline( Fullname( ) );
        }

        //-------------------------------------------------------------------
        //
        //-------------------------------------------------------------------
        inline bool
        Directory::IsRoot( ) const
        {
            return is_root;
        }

        //-------------------------------------------------------------------
        /// The relative name is simple the name associated with this
        /// directory information as referenced by its parent.
        /// A root directory name must be fully qualified.
        //-------------------------------------------------------------------
        inline const std::string&
        Directory::Name( ) const
        {
            return m_name;
        }

        //-------------------------------------------------------------------
        /// The root name is the name of the node that appears highest in
        /// the tree.
        /// Taking any node as a start, the tree is assended until
        /// the one with no parent is reached.
        /// It is the name of this node which is returned.
        //-------------------------------------------------------------------
        inline const std::string&
        Directory::Root( ) const
        {
            return m_root;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline Directory::timestamp_type
        Directory::TimeModified( ) const
        {
            return ( m_last_time_modified );
        }

#if WORKING
        template < typename StreamT >
        StreamT&
        Directory::WriteRecursive( StreamT& Stream ) const
        {
            functor_dump< StreamT > s( Stream );

            Walk( s );
            return Stream;
        }
#endif /* WORKING */

        inline Directory::search_data_type
        Directory::searchData( search_id_type Key )
        {
            search_data_type retval;

            index_container_type::const_iterator pos( m_indexes.find( Key ) );

            if ( pos != m_indexes.end( ) )
            {
                retval = pos->second;
            }
            return retval;
        }

        inline Directory::excluded_directories_rw_type
        Directory::excluded_directories_rw( )
        {
            return ( excluded_directories_rw_type( p_excluded_directories_baton,
                                                   diskCache::RWLOCK_TIMEOUT,
                                                   p_excluded_directories,
                                                   __FILE__,
                                                   __LINE__ ) );
        }
        //-------------------------------------------------------------------
        /// Retrieve the numeric identifier for the SDGTx indexing class
        //-------------------------------------------------------------------
        inline RegistrySingleton::id_type
        Directory::get_sdgtx_id( )
        {
            if ( SDGTx_ID == RegistrySingleton::KEY_NULL )
            {
                SDGTx_ID = RegistrySingleton::Id( SDGTx_ID_KEY );
            }
            return SDGTx_ID;
        }

        //-------------------------------------------------------------------
        /// Retrieve a collection of directories that are children
        /// of this directory.
        //-------------------------------------------------------------------
        inline const Directory::children_type&
        Directory::children( ) const
        {
            return m_subdirs;
        }

        inline bool
        Directory::is_updated( timestamp_type Old, timestamp_type New )
        {
            if ( ( Old == 0 ) || ( Old != New ) )
            {
                return true;
            }
            return false;
        }

        //###################################################################
        //
        //#################################################################
        template < typename StreamT >
        Directory::functor_dump< StreamT >::functor_dump( StreamT& Stream )
            : m_stream( Stream )
        {
        }

        template < typename StreamT >
        void
        Directory::functor_dump< StreamT >::
        operator( )( Directory::dirref_type Node ) const
        {
            Node->Write( m_stream );
        }

        template < typename StreamT >
        void
        Directory::functor_dump< StreamT >::
        operator( )( const Directory* const Node ) const
        {
            Node->Write( m_stream );
        }

        //===================================================================
        //===================================================================
        inline DirectoryScanData::DirectoryScanData(
            const searches_data_type& OldData,
            searches_data_type&       UpdatedData,
            Directory::ScanResults&   Results,
            const DirectoryManager&   DirectoryCollection,
            const std::string&        Root )
            : s_old_searches_data( OldData ),
              s_updated_searches_data( UpdatedData ), s_results( Results ),
              m_directory_manager( DirectoryCollection ), m_root( Root )
        {
        }

        inline const DirectoryManager&
        DirectoryScanData::DirManager( ) const
        {
            return m_directory_manager;
        }

        inline const std::string&
        DirectoryScanData::Root( ) const
        {
            return m_root;
        }

    } // namespace Cache

    namespace Streams
    {
        //-------------------------------------------------------------------
        // Handle Binary Streams
        //-------------------------------------------------------------------
        class IBinary;
        class OBinary;

        IBinary& operator>>( IBinary&                       Stream,
                             Cache::Directory::dirref_type& Data );

        OBinary& operator<<( OBinary&                      Stream,
                             Cache::Directory::dirref_type Data );

        //-------------------------------------------------------------------
        // Handle ASCII Streams
        //-------------------------------------------------------------------
        class OASCII;

        OASCII& operator<<( OASCII&                       Stream,
                            Cache::Directory::dirref_type Data );
    } // namespace Streams
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__DIRECTORY_HH */
