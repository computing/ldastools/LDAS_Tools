//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__REGISTRY_HH
#define DISKCACHE_API__CACHE__REGISTRY_HH

#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/Singleton.hh"

#include "diskcacheAPI/Common/Registry.hh"

#include "diskcacheAPI/Streams/StreamsInterface.hh"

#include "diskcacheAPI/Cache/SearchInterface.hh"

namespace diskCache
{
    namespace Cache
    {
        class Directory;
        struct DirectoryScanData;
        class QueryParams;
        class QueryAnswer;

        class Registry : public Common::Registry
        {
        public:
            typedef boost::shared_ptr< SearchInterface > indexing_type;
            typedef void ( *FindFunc )( const Streams::Streamable& Source,
                                        const Directory&           Dir,
                                        QueryAnswer&               Answer );
            typedef bool ( *IsMatchFunc )( DirectoryScanData& Data );
            typedef void ( *OnDirectoryCloseFunc )( DirectoryScanData& Data );
            typedef void ( *TranslateQueryFunc )( const QueryParams& Params,
                                                  QueryAnswer&       Answer );

            class Info : public Common::Registry::Info
            {
            public:
                typedef Registry::indexing_type indexing_type;

                static const FindFunc             FIND_FUNC_NULL;
                static const IsMatchFunc          IS_MATCH_FUNC_NULL;
                static const OnDirectoryCloseFunc ON_DIRECTORY_CLOSE_FUNC_NULL;
                static const TranslateQueryFunc   TRANSLATE_QUERY_FUNC_NULL;

                //---------------------------------------------------------------
                /// \brief Constructor
                ///
                /// \param[in] Key
                ///
                /// \param[in] Find
                ///      Specify the function to use to locate files within
                ///      a directory.
                ///
                /// \param[in] IsMatch
                ///      Function to call to check if a file should be indexed
                ///      by the search engine.
                ///
                /// \param[in] OnDirectoryClose
                ///      Function to call once all entries in the current
                ///      directory have been processed.
                ///      If the search engine has no action,
                ///      then the value ON_DIRECTORY_CLOSE_FUNC_NULL may
                ///      be passed.
                ///
                /// \param[in] TranslateQuery
                ///     Translate the query results (eg: make it easily parsable
                ///     by TCL, Python, etc.).
                ///
                /// \return
                ///     New instance of the object
                //---------------------------------------------------------------
                Info( const std::string&   Key,
                      FindFunc             Find,
                      IsMatchFunc          IsMatch,
                      OnDirectoryCloseFunc OnDirectoryClose,
                      TranslateQueryFunc   TranslateQuery );

                //---------------------------------------------------------------
                //---------------------------------------------------------------
                void Find( const Streams::Streamable& Source,
                           const Directory&           Dir,
                           QueryAnswer&               Answer ) const;

                //---------------------------------------------------------------
                /// \brief Called per file
                ///
                /// \param[in,out] Data
                ///     Search engine specific data.
                ///     This is also a scratch area for search engines
                ///     to record transient information related to processing
                ///     the directory contents.
                //---------------------------------------------------------------
                bool IsMatch( DirectoryScanData& Data ) const;

                //---------------------------------------------------------------
                /// \brief Callback once all data has been read.
                ///
                /// \param[in,out] Data
                ///     Search engine specific data.
                ///     This is also a scratch area for search engines
                ///     to record transient information related to processing
                ///     the directory contents.
                //---------------------------------------------------------------
                void OnDirectoryClose( DirectoryScanData& Data ) const;

                void TranslateQuery( const QueryParams& Params,
                                     QueryAnswer&       Answer ) const;

            protected:
                Info( const Info& Source );

            private:
                FindFunc             m_find_func;
                IsMatchFunc          m_is_match_func;
                OnDirectoryCloseFunc m_on_directory_close_func;
                TranslateQueryFunc   m_translate_query_func;
            };

            Registry( );

            void OnDirectoryClose( DirectoryScanData& Data );

            id_type Register( const Info& Key );

            bool ScanForMatch( DirectoryScanData& Data );

            //-----------------------------------------------------------------
            /// \brief Translate a query
            //-----------------------------------------------------------------
            void TranslateQuery( const Cache::QueryParams& Params,
                                 Cache::QueryAnswer&       Answer ) const;

        private:
        };

        inline bool
        Registry::Info::IsMatch( DirectoryScanData& Data ) const
        {
            if ( m_is_match_func != IS_MATCH_FUNC_NULL )
            {
                return ( *m_is_match_func )( Data );
            }
            return false;
        }

        inline void
        Registry::Info::OnDirectoryClose( DirectoryScanData& Data ) const
        {
            if ( m_on_directory_close_func != ON_DIRECTORY_CLOSE_FUNC_NULL )
            {
                ( *m_on_directory_close_func )( Data );
            }
        }

        inline void
        Registry::Info::TranslateQuery( const QueryParams& Params,
                                        QueryAnswer&       Answer ) const
        {
            if ( m_translate_query_func != TRANSLATE_QUERY_FUNC_NULL )
            {
                ( *m_translate_query_func )( Params, Answer );
            }
        }

        inline Registry::Registry( ) : Common::Registry( )
        {
        }

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__REGISTRY_HH */
