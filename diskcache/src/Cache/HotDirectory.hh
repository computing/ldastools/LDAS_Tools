//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__HOT_DIRECTORY_HH
#define DISKCACHE_API__HOT_DIRECTORY_HH

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/DirectoryManager.hh"

namespace diskCache
{
    namespace Cache
    {
        //-------------------------------------------------------------------
        /// \brief Information about a directory that is being actively watched.
        ///
        /// This maintains a collection of hot directories.
        /// Hot directories are scanned outside of the conventional deep scan
        /// of the MountPoint class so as to reduce discovery latency.
        //-------------------------------------------------------------------
        class HotDirectory
        {
        public:
            typedef DirectoryManager directory_set_type;

            //-----------------------------------------------------------------
            /// \brief Storage type for directory information.
            ///
            /// Storage of the directory information allowing for reference
            /// counting so as to protect the memory being referenced to stay
            /// within scope till all consumers have completed their use of
            /// of the data.
            //-----------------------------------------------------------------
            typedef boost::shared_ptr< Directory > shared_dir_info_type;

            //-----------------------------------------------------------------
            /// \brief Storage type for directory information.
            ///
            /// Having the directory information stored in a shared pointer
            /// container increases thread safety and diminishes the needs
            /// for locks.
            //-----------------------------------------------------------------
            typedef boost::weak_ptr< shared_dir_info_type::element_type >
                weak_dir_info_type;

            typedef Directory::timestamp_type timestamp_type;

            //-----------------------------------------------------------------
            /// \brief Base for unary functions to be used in ForEach call
            //-----------------------------------------------------------------
            class UnaryFunction
            {
            public:
                typedef HotDirectory::weak_dir_info_type element_type;

                virtual void operator( )( const element_type& HotDir ) = 0;
            };

            //-------------------------------------------------------------------
            /// \brief Iterate over each hot directory
            ///
            /// \param[in] Func
            ///     Function to execute.
            //-------------------------------------------------------------------
            static void ForEach( UnaryFunction& Func );

            //-----------------------------------------------------------------
            /// \brief Lower bound of relative age in seconds to be considered.
            ///
            /// \return
            ///     The current lower bound.
            //-----------------------------------------------------------------
            static timestamp_type HotLowerBound( );

            //-----------------------------------------------------------------
            /// \brief Set lower bound of relative age in seconds to be
            /// considered.
            ///
            /// \param[in] Value
            ///     The new lower bound.
            ///     A value of 0 (zero) disables hot directories.
            //-----------------------------------------------------------------
            static void HotLowerBound( timestamp_type Value );

            //-----------------------------------------------------------------
            /// \brief Report if the path is registered as hot
            ///
            /// \param[in] Path
            ///     The path to check.
            //-----------------------------------------------------------------
            static bool IsRegistered( const std::string& Path );

            //-----------------------------------------------------------------
            /// \brief Add a directory to the list of hot directories.
            ///
            /// \param[in] Entry
            ///     The Entry to add to the list of hot directories if and
            ///     only if it meets all hot directory criteria.
            ///
            /// \param[in] Manager
            ///     Directory set.
            ///
            /// \param[in] Log
            ///     Set to true if the directory should log a message if
            ///     Entry is added to the hot list.
            //-----------------------------------------------------------------
            static void RegisterIfHot( weak_dir_info_type        Entry,
                                       const directory_set_type& Manager,
                                       bool                      Log );

            //-----------------------------------------------------------------
            /// \brief Clear the cache
            ///
            /// This returns the cache to an empty state.
            //-----------------------------------------------------------------
            static void Reset( );

            //-----------------------------------------------------------------
            /// \brief Establish the frequency that hot directories are scanned.
            ///
            /// \param[in] Value
            ///     The new scan interval.
            //-----------------------------------------------------------------
            static void ScanInterval( timestamp_type Value );

            //-----------------------------------------------------------------
            /// \brief The frequency that hot directories are scanned.
            ///
            /// \return
            ///     The scan interval.
            //-----------------------------------------------------------------
            static timestamp_type ScanInterval( );

            //-----------------------------------------------------------------
            /// \brief Shutdown daemon which is scanning the list of hot
            /// directories.
            //-----------------------------------------------------------------
            static void StopDaemon( );

            //-----------------------------------------------------------------
            /// \brief Remove a directory from the list of hot directories.
            ///
            /// \param[in] Entry
            ///     The Entry to be removed from the list of hot directories.
            //-----------------------------------------------------------------
            static void Unregister( weak_dir_info_type Entry );

        private:
            //-----------------------------------------------------------------
            /// \brief Storage for lower bound of relative age in seconds.
            ///
            /// A directory that has been modified within this number of
            /// seconds is considered to be hot.
            ///
            /// The default value is 300 seconds ( 5 minutes ).
            //-----------------------------------------------------------------
            static timestamp_type m_lower_bound;

            //-----------------------------------------------------------------
            /// \brief Number of seconds between scans
            ///
            /// This value regulates the number of seconds between scans
            /// of the collection hot directories.
            //-----------------------------------------------------------------
            static timestamp_type m_scan_interval;
        };

        //-------------------------------------------------------------------
        /// Return the time representing the number of seconds to sleep
        /// between scans of the set of hot directories.
        //-------------------------------------------------------------------
        inline HotDirectory::timestamp_type
        HotDirectory::ScanInterval( )
        {
            return m_scan_interval;
        }
    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE_API__HOT_DIRECTORY_HH */
