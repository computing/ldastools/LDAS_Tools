//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <sys/stat.h>

#include <algorithm>
#include <list>
#include <string>
#include <sstream>
#include <utility>

#include "ldastoolsal/Directory.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/System.hh"
#include "ldastoolsal/Timeout.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/Stat.hh"

#include "diskcacheAPI/Common/Logging.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/DirectoryManager.hh"
#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"
#include "diskcacheAPI/Cache/ExcludedPattern.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/RegistrySingleton.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"

#include "Devices.hh"

//-----------------------------------------------------------------------
// Frequently used namespaces.
//-----------------------------------------------------------------------

using GenericAPI::LogEntryGroup_type;
using LDASTools::AL::ErrorLog;
using LDASTools::AL::MutexLock;
using LDASTools::AL::StdErrLog;
using LDASTools::AL::TimeoutException;
using std::string;

using diskCache::Cache::Devices;
using diskCache::Cache::ExcludedDirectoriesSingleton;
using diskCache::Cache::ExcludedPattern;

typedef diskCache::Cache::Directory                    Directory;
typedef diskCache::Cache::Directory::SymbolicLinkError SymbolicLinkError;
typedef diskCache::Cache::Directory::children_type     children_type;
typedef diskCache::Cache::Directory::ignored_type      ignored_type;
typedef diskCache::Cache::Directory::dirref_type       dirref_type;
typedef diskCache::Cache::DirectoryScanData            DirectoryScanData;

typedef std::list< std::string >            filename_container_type;
typedef std::string                         directory_element_type;
typedef std::list< directory_element_type > directory_container_type;

#define VERBOSE_ERROR 0

//-----------------------------------------------------------------------
// Debugging
//-----------------------------------------------------------------------
#if 0
#include "ldastoolsal/IOLock.hh"

#define VERBOSE_DEBUGGING 1
#define AT_BASE( a )                                                           \
    {                                                                          \
        int oldstate;                                                          \
        pthread_setcancelstate( PTHREAD_CANCEL_DISABLE, &oldstate );           \
        MutexLock lock( LDASTools::AL::IOLock::GetKey( std::cerr ) );          \
        pthread_setcancelstate( oldstate, &oldstate );                         \
                                                                               \
        std::cerr << a << __FILE__ << " " << __LINE__ << std::endl             \
                  << std::flush;                                               \
    }

#else
#define AT_BASE( a )
#endif
#define AT( a ) AT_BASE( a << std::endl << "\t" )
#define HERE( ) AT_BASE( "" )

#if VERBOSE_ERROR
#define COND_QUEUE_LOG_MESSAGE( MSG, MODE, LVL, CALLER, TYPE )                 \
    QUEUE_LOG_MESSAGE( MSG, MODE, LVL, CALLER, TYPE )
#define COND_CALLER( A ) static const char* caller = A
#else /* VERBOSE_ERROR */
#define COND_QUEUE_LOG_MESSAGE( MSG, MODE, LVL, CALLER, TYPE )
#define COND_CALLER( A )
#endif /* VERBOSE_ERROR */

//-----------------------------------------------------------------------
// Forward declaration of local functions
//-----------------------------------------------------------------------
namespace
{
    union stat_id
    {
        Devices::id_type id;
        struct
        {
            INT_4U dev_id;
            INT_4U inode_id;
        } sid_id;
    };
    diskCache::Cache::Directory::timestamp_type
    do_stat( const std::string& Path, struct stat& StatStruct, bool IsRoot );

    const string errnoMessage( );

    void read_directory( const std::string&                Path,
                         const std::string&                RootDirectory,
                         directory_container_type&         Children,
                         ignored_type&                     IgnoredDirectories,
                         Directory::index_container_type&  OldData,
                         DirectoryScanData&                ScanData,
                         const diskCache::Cache::StatInfo& Stat );

} // namespace

namespace diskCache
{
    namespace Streams
    {
        template <>
        IBinary&
        IBinary::operator>>( std::set< std::string >& Data )
        {
            //-----------------------------------------------------------------
            // Start with an empty container
            //-----------------------------------------------------------------
            Data.clear( );

            //-----------------------------------------------------------------
            // Read the number of elements
            //-----------------------------------------------------------------
            size_type s;

            *this >> s;

            //-----------------------------------------------------------------
            // Read the elements out to the stream
            //-----------------------------------------------------------------
            std::string d;

            for ( size_type cur = 0, last = s; cur != last; ++cur )
            {
                //---------------------------------------------------------------
                // Extract from the stream
                //---------------------------------------------------------------
                *this >> d;
                //---------------------------------------------------------------
                // Insert into container
                //---------------------------------------------------------------
                Data.insert( d );
            }
            return *this;
        }
    } // namespace Streams

    namespace Cache
    {
        //===================================================================
        //===================================================================
        class StatInfo
        {
        public:
            struct stat               stat_buffer;
            Directory::timestamp_type mod_time;
        };

        //===================================================================
        //===================================================================
        Directory::FileCacheError::FileCacheError( const std::string& Filename,
                                                   const std::string& Reason )
            : std::runtime_error( format( Filename, Reason ) )
        {
        }

        Directory::FileCacheError::FileCacheError( const std::string& Reason )
            : std::runtime_error( Reason )
        {
        }

        std::string
        Directory::FileCacheError::format( const std::string& Path,
                                           const std::string& Reason )
        {
            std::ostringstream msg;

            if ( Reason.size( ) > 0 )
            {
            }
            return msg.str( );
        }

        //===================================================================
        //===================================================================
        Directory::StartIsNotADirectoryError::StartIsNotADirectoryError(
            const std::string& Path, const char* MethodName )
            : std::runtime_error( format( Path, MethodName ) )
        {
        }

        std::string
        Directory::StartIsNotADirectoryError::format( const std::string& Path,
                                                      const char* MethodName )
        {
            std::ostringstream msg;

            if ( MethodName )
            {
                msg << MethodName;
            }
            msg << "Specified path \'" << Path << "\' is not a directory.";

            return msg.str( );
        }

        Directory::SymbolicLinkError::SymbolicLinkError(
            const std::string& Path, const char* MethodName )
            : std::runtime_error( format( Path, MethodName ) )
        {
        }

        std::string
        Directory::SymbolicLinkError::format( const std::string& Path,
                                              const char*        MethodName )
        {
            std::ostringstream msg;

            if ( MethodName )
            {
                msg << MethodName;
            }
            msg << "API does not support symbolic links. Please remove \'"
                << Path << "\'";

            return msg.str( );
        }

        //===================================================================
        //===================================================================
        Directory::ScanResults::ScanResults( )
            : m_scanned_directories( 0 ), m_scanned_files( 0 )
        {
        }

        void
        Directory::ScanResults::Log( const std::string& Caller,
                                     const std::string& JobInfo ) const
        {
            for ( directory_container_type::const_iterator
                      cur = m_results.begin( ),
                      last = m_results.end( );
                  cur != last;
                  ++cur )
            {
                std::string        state( "UNKNOWN" );
                std::ostringstream added;
                std::ostringstream removed;
                std::ostringstream msg;

                switch ( cur->second.s_state )
                {
                case Directory::DIRECTORY_NEW:
                    state = "NEW";
                    break;
                case Directory::DIRECTORY_UPDATED:
                    state = "UPDATED";
                    break;
                case Directory::DIRECTORY_REMOVED:
                    state = "REMOVED";
                    break;
                case Directory::DIRECTORY_OFFLINE:
                    state = "OFFLINE";
                    break;
                }
                for ( ScanResults::file_container_type::const_iterator
                          fi_cur = cur->second.s_engines.begin( ),
                          fi_last = cur->second.s_engines.end( );
                      fi_cur != fi_last;
                      ++fi_cur )
                {
                    if ( fi_cur->second.s_added > 0 )
                    {
                        added << " " << fi_cur->second.s_added << " "
                              << ( ( fi_cur->second.s_added == 1 ) ? "FILE"
                                                                   : "FILES" )
                              << " ADDED"
                              << " for engine " << fi_cur->first;
                    }
                    if ( fi_cur->second.s_removed > 0 )
                    {
                        removed
                            << " " << fi_cur->second.s_removed << " "
                            << ( ( fi_cur->second.s_removed == 1 ) ? "FILE"
                                                                   : "FILES" )
                            << " REMOVED"
                            << " for engine " << fi_cur->first;
                    }
                }
                msg << "{" << cur->first // Directory name
                    << " " << state << " DIRECTORY"
                    << ( ( ( added.str( ).length( ) > 0 ) ||
                           ( removed.str( ).length( ) > 0 ) )
                             ? " WITH"
                             : "" )
                    << added.str( ) << removed.str( ) << "}";
                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_NOTE,
                    0,
                    Caller,
                    JobInfo );
            }
        }

        //###################################################################
        //###################################################################
        MutexLock::baton_type      Directory::SDGTX_ID_baton;
        RegistrySingleton::id_type Directory::SDGTx_ID =
            RegistrySingleton::KEY_NULL;
        RegistrySingleton::ascii_key_type Directory::SDGTx_ID_KEY = "SDGTx";

        Directory::excluded_directories_rw_type::baton_type
                                             Directory::p_excluded_directories_baton;
        Directory::excluded_directories_type Directory::p_excluded_directories;

        typedef boost::weak_ptr< Directory::dirref_type::element_type >
            weakdirref_type;
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------

        Directory::Directory( )
            : m_last_time_modified( 0 ), m_state( DIRECTORY_NEW )

        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Directory::Directory( const std::string& Path, const std::string& Root )
            : m_name( Path ), m_last_time_modified( 0 ), m_root( Root ),
              is_root( ( Path.compare( Root ) == 0 ) ), m_state( DIRECTORY_NEW )
        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Directory::Directory( const std::string& Path, dirref_type Parent )
            : /* m_parent( Parent ), */
              m_name( Path ), m_last_time_modified( 0 ),
              m_root( ( Parent ? Parent->Root( ) : Path ) ),
              is_root( ( Parent ) ), m_state( DIRECTORY_NEW )
        {
        }

        //-------------------------------------------------------------------
        /// Implements the copy constructor for the class
        //-------------------------------------------------------------------
        Directory::Directory( const Directory& Source )
            : m_indexes( Source.m_indexes ), m_subdirs( Source.m_subdirs ),
              m_subdirs_ignored( Source.m_subdirs_ignored ),
              m_name( Source.m_name ),
              m_last_time_modified( Source.m_last_time_modified ),
              m_root( Source.m_root ), is_root( Source.is_root ),
              m_state( Source.m_state )
        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Directory::~Directory( )
        {
        }

        //-------------------------------------------------------------------
        /// Configuration of the directory cache understands the following
        /// variables:
        ///
        /// <ul>
        ///   <li><b>exclude</b> - A colon separated list of patterns
        ///       which describe directories to be ignored.
        ///   </li>
        /// </ul>
        //-------------------------------------------------------------------
        bool
        Directory::Configure( const std::string& Variable,
                              const std::string& Value )
        {
            bool retval = false;

            return retval;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        Directory::Find( QueryAnswer& Answer ) const
        {
            index_container_type::const_iterator pos(
                m_indexes.find( Answer.IndexId( ) ) );

            if ( pos != m_indexes.end( ) )
            {
                RegistrySingleton::info_type info(
                    RegistrySingleton::GetInfo( Answer.IndexId( ) ) );

                if ( info )
                {
                    reinterpret_cast< const Registry::Info* >( info.get( ) )
                        ->Find( *pos->second, *this, Answer );
                }
            }
        }

        //-------------------------------------------------------------------
        //
        //-------------------------------------------------------------------
        Directory::dirref_type
        Directory::Scan( DirectoryManager&  DirectoryCollection,
                         ScanResults&       Results,
                         const std::string& Caller,
                         const std::string& JobInfo ) const
        {
            static const CHAR* const caller(
                "diskCache::Cache::Directory::Scan( ): " );

            QUEUE_LOG_MESSAGE(
                "Starting Directory::Scan", MT_DEBUG, 5, caller, "CXX" );

            dirref_type retval;
            std::string fullname( Fullname( ) );

            try
            {

                //---------------------------------------------------------------
                //
                //---------------------------------------------------------------
                StatInfo stat_info;

                QUEUE_LOG_MESSAGE( "do_stat 1: fullname: " << fullname,
                                   MT_DEBUG,
                                   5,
                                   caller,
                                   "CXX" );
                stat_info.mod_time =
                    do_stat( fullname, stat_info.stat_buffer, IsRoot( ) );
                QUEUE_LOG_MESSAGE(
                    "Directory::Scan: "
                        << Fullname( ) << " modified: "
                        << ( is_updated( m_last_time_modified,
                                         stat_info.mod_time ) == true ),
                    MT_DEBUG,
                    50,
                    caller,
                    "CXX" );
                if ( !device )
                {
                    MutexLock lock( m_scan_baton, __FILE__, __LINE__ );

                    stat_id id;

                    id.sid_id.dev_id = stat_info.stat_buffer.st_dev;
                    id.sid_id.inode_id = 0; // stat_info.stat_buffer.st_ino;

                    device = Devices::Find( id.id );
                    if ( device )
                    {
                        device->Used( true );
                    }
                }
                if ( is_updated( m_last_time_modified, stat_info.mod_time ) ==
                     true )
                {
                    //-------------------------------------------------------------
                    // Lock since it is most likely that the information will be
                    // updated.
                    // This serializes the access from the default directory
                    // scans and other methods such as HotDirectory.
                    //-------------------------------------------------------------
                    MutexLock lock( m_scan_baton, __FILE__, __LINE__ );

                    QUEUE_LOG_MESSAGE( "do_stat 2: fullname: " << fullname,
                                       MT_DEBUG,
                                       5,
                                       caller,
                                       "CXX" );
                    stat_info.mod_time =
                        do_stat( fullname, stat_info.stat_buffer, IsRoot( ) );
                    if ( is_updated( m_last_time_modified,
                                     stat_info.mod_time ) == true )
                    {
                        //---------------------------------------------------------
                        // Verify that the path is a directory
                        //---------------------------------------------------------
                        if ( S_ISDIR( stat_info.stat_buffer.st_mode ) == 0 )
                        {
                            //-----------------------------------------------------
                            // It is not a directory.
                            // Check if it is a symbolic link
                            //-----------------------------------------------------
                            if ( S_ISLNK( stat_info.stat_buffer.st_mode ) )
                            {
                                //-------------------------------------------------
                                // Any entry that is a symbolic link is an error
                                //-------------------------------------------------
                                QUEUE_LOG_MESSAGE( "ERROR: "
                                                       << Fullname( )
                                                       << " is a symbolic link",
                                                   MT_DEBUG,
                                                   10,
                                                   caller,
                                                   "CXX" );
                                throw SymbolicLinkError( m_name, caller );
                            }
                            else
                            {
                                //-------------------------------------------------
                                // It is something other than a directory or a
                                // symbolic link.
                                //-------------------------------------------------
                                if ( m_name.size( ) == m_root.size( ) )
                                {
                                    QUEUE_LOG_MESSAGE(
                                        "ERROR: " << Fullname( )
                                                  << " root is not a directory",
                                        MT_DEBUG,
                                        10,
                                        caller,
                                        "CXX" );
                                    throw StartIsNotADirectoryError( m_name,
                                                                     caller );
                                }
                                return retval;
                            }
                        }
                        //-----------------------------------------------------------
                        // Locate the device on which this directory is mounted
                        //-----------------------------------------------------------
#if 0
              stat_id id;
              id.sid_id.dev_id = stat_info.stat_buffer.st_dev;
              id.sid_id.inode_id = 0; // stat_info.stat_buffer.st_ino;
              Devices::element_type dev = Devices::Find( fullname, id.id );
#else
                        if ( !device )
                        {
                            stat_id id;

                            id.sid_id.dev_id = stat_info.stat_buffer.st_dev;
                            id.sid_id.inode_id =
                                0; // stat_info.stat_buffer.st_ino;

                            device = Devices::Find( id.id );
                        }
                        if ( device )
                        {
                            device->Used( true );
                        }

#endif /* 0 */
                        retval =
                            update( Results, DirectoryCollection, stat_info );

                        if ( retval )
                        {
                            DirectoryCollection.OnUpdate( retval, Results );
                            Results.Log( Caller, JobInfo );
                        }
                    } // if - mutex locked
                } // if
            }
            catch ( const std::runtime_error& Except )
            {
                COND_QUEUE_LOG_MESSAGE( "Exception: " << Except.what( ),
                                        MT_DEBUG,
                                        50,
                                        caller,
                                        "CXX" );
                if ( errno == EINTR )
                {
                    if ( device )
                    {
                        device->Offline( );
                    }
                }
                throw; /* Rethrow the exception */
            }
            QUEUE_LOG_MESSAGE( "Ending", MT_DEBUG, 5, caller, "CXX" );
            return retval;
        } // method Directory::Scan( ScanResults& Results )

        Directory::dirref_type
        Directory::update( ScanResults&            Results,
                           const DirectoryManager& DirectoryCollection,
                           const StatInfo&         StatInfo ) const
        {
            dirref_type retval( new Directory( *this ) );

            DirectoryScanData scan_data( m_indexes,
                                         retval->m_indexes,
                                         Results,
                                         DirectoryCollection,
                                         Root( ) );

            retval->read_directory( scan_data, StatInfo );

            return retval;
        }

        void
        Directory::read_directory( DirectoryScanData& ScanData,
                                   const StatInfo&    StatInfo )
        {
            COND_CALLER( "Directory::read_directory" );
            if ( m_state == DIRECTORY_OFFLINE )
            {
                if ( !device )
                {
                    device = diskCache::Cache::Devices::Find( Fullname( ) );
                }
                if ( ( device ) && ( device->IsOffline( ) == true ) )
                {
                    //-------------------------------------------------------------
                    // The device is still offline so there is no use to do
                    // another stat which would just consume another thread.
                    //-------------------------------------------------------------
                    std::ostringstream msg;

                    msg << "Device marked OFFLINE";

                    errno = EINTR;
                    throw LDASTools::AL::TimeoutException( msg.str( ) );
                }
                m_state = DIRECTORY_NEW;
            }
            //-----------------------------------------------------------------
            // Gather all the names in the directory skipping anything
            //     that matches the ignore pattern
            //-----------------------------------------------------------------
            directory_container_type cur_children;
            ignored_type             cur_ignored;

            COND_QUEUE_LOG_MESSAGE( "Reading directory"
                                        << " Fullname: " << Fullname( )
                                        << " Root: " << Root( ),
                                    MT_DEBUG,
                                    50,
                                    caller,
                                    "CXX" );
            try
            {
                ::read_directory( Fullname( ),
                                  Root( ),
                                  cur_children,
                                  cur_ignored,
                                  m_indexes,
                                  ScanData,
                                  StatInfo );
                if ( !device )
                {
                    device = diskCache::Cache::Devices::Find( Fullname( ) );
                }
                if ( device )
                {
                    device->Used( true );
                }
            }
            catch ( const std::runtime_error& Except )
            {
                if ( errno == EINTR )
                {
                    m_state = DIRECTORY_OFFLINE;
                    if ( device )
                    {
                        device->Offline( );
                    }
                }
                throw; /* Rethrow the exception */
            }

            ScanData.s_results.State( Fullname( ), m_state );
            if ( m_state == DIRECTORY_NEW )
            {
                m_state = DIRECTORY_UPDATED;
            }
            for ( DirectoryScanData::searches_scan_data_type::iterator
                      cur = ScanData.s_searches_scan_data.begin( ),
                      last = ScanData.s_searches_scan_data.end( );
                  cur != last;
                  ++cur )
            {
                m_indexes[ cur->first ] = cur->second->SearchData( );
            }

            //-----------------------------------------------------------------
            // Update with the information gleaned from reading the directory
            //-----------------------------------------------------------------
            {
                //---------------------------------------------------------------
                // Find the new verses the old directories
                //---------------------------------------------------------------
                {
                    filename_container_type additions;
                    filename_container_type deletions;

                    directory_container_type::const_iterator
                        current = cur_children.begin( ),
                        current_end = cur_children.end( );

                    children_type::const_iterator previous =
                                                      children( ).begin( ),
                                                  previous_end =
                                                      children( ).end( );

                    while ( ( previous != previous_end ) &&
                            ( current != current_end ) )
                    {
                        int status = previous->compare( *current );

                        if ( status == 0 )
                        {
                            //---------------------------------------------------------
                            // When the two are the same, then the data just
                            // needs to migrate from the old to the new.
                            //---------------------------------------------------------
                            ++previous;
                            ++current;
                        }
                        else if ( status < 0 )
                        {
                            //---------------------------------------------------------
                            // Deletion
                            //---------------------------------------------------------
                            deletions.push_back( *previous );
                            ++previous;
                        }
                        else
                        {
                            //---------------------------------------------------------
                            // Addition
                            //---------------------------------------------------------
                            additions.push_back( *current );
                            ++current;
                        }
                    }
                    while ( previous != previous_end )
                    {
                        //-----------------------------------------------------------
                        // Anything left in the old tree is flagged for deletion
                        //-----------------------------------------------------------
                        deletions.push_back( *previous );
                        ++previous;
                    }
                    while ( current != current_end )
                    {
                        //-----------------------------------------------------------
                        // Anything left in the new tree is flagged for addition
                        //-----------------------------------------------------------
                        additions.push_back( *current );
                        ++current;
                    }
                    //-------------------------------------------------------------
                    // Remove the directories that no longer exist
                    //-------------------------------------------------------------
                    for ( filename_container_type::const_iterator
                              cur = deletions.begin( ),
                              last = deletions.end( );
                          cur != last;
                          ++cur )
                    {
                        m_subdirs.erase( *cur );
                    }
                    ScanData.s_results.Removed( Fullname( ), deletions );
                    //-------------------------------------------------------------
                    // Add any directories that have been created
                    //-------------------------------------------------------------
                    for ( filename_container_type::const_iterator
                              cur = additions.begin( ),
                              last = additions.end( );
                          cur != last;
                          ++cur )
                    {
                        m_subdirs.insert( *cur );
                    }
                    ScanData.s_results.Added( Fullname( ), additions );
                }
                //---------------------------------------------------------------
                //---------------------------------------------------------------
                //---------------------------------------------------------------
                // Record the last time this directory was modified.
                //---------------------------------------------------------------
                m_last_time_modified = StatInfo.mod_time;
            }
        }

        DirectoryScanData::scan_data::~scan_data( )
        {
        }

    } // namespace Cache

    Streams::IBinary&
    operator>>( Streams::IBinary& Stream, Cache::Directory::dirref_type& Data )
    {
        Data.reset( new Directory( ) );
        // Data.Read( Stream, Data );
        return Stream;
    }
} // namespace diskCache

namespace
{
    inline diskCache::Cache::Directory::timestamp_type
    do_stat( const std::string& Path, struct stat& StatStruct, bool IsRoot )
    {
        static const CHAR* const method_name( __FILE__
                                              "::anonymous::do_stat: " );

        QUEUE_LOG_MESSAGE(
            "do_stat: Path: " << Path, MT_DEBUG, 5, method_name, "CXX" );
        QUEUE_LOG_MESSAGE(
            "Entry: " << Path, MT_DEBUG, 50, method_name, "CXX" );
        int err = 0;
        if ( IsRoot )
        {
            err = GenericAPI::Stat( Path, StatStruct );
        }
        else
        {
            err = GenericAPI::LStat( Path, StatStruct );
        }

        AT( "err: " << err );

        if ( StdErrLog.IsOpen( ) )
        {
            HERE( );
            std::ostringstream msg;

            msg << "doStat: " << Path << " err: " << err << " errno: " << errno;
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
        if ( err != 0 )
        {
            if ( errno == EINTR )
            {
                diskCache::Cache::Devices::element_type dev =
                    diskCache::Cache::Devices::Find( Path );
                if ( dev )
                {
                    QUEUE_LOG_MESSAGE( "Flagging as OFFLINE: " << Path,
                                       MT_NOTE,
                                       0,
                                       method_name,
                                       "CXX" );
                    dev->Offline( );
                }
            }
            //-----------------------------------------------------------------
            // An error has occurred while trying to get information about
            // this directory.
            // 1) Create% a readable error message
            //-----------------------------------------------------------------
            HERE( );
            string exc( method_name );
            exc += "Could not get status for \'";
            exc += Path;
            exc += '\'';
            exc += errnoMessage( );
            exc += "\'";
            QUEUE_LOG_MESSAGE( "Exit: " << Path << " Exception: " << exc,
                               MT_NOTE,
                               0,
                               method_name,
                               "CXX" );
            throw std::runtime_error( exc );
        }
        //-------------------------------------------------------------------
        // Handling of the 1 second bug
        //-------------------------------------------------------------------
        static double BUG_WINDOW = 1.0;

        LDASTools::AL::GPSTime dir(
            StatStruct.st_mtime, 0, LDASTools::AL::GPSTime::UTC );
        LDASTools::AL::GPSTime now;

        now.Now( );
        double diff( now - dir );
        if ( diff < 0 )
        {
            diff *= -1;
        }
        if ( diff <= BUG_WINDOW )
        {
            //-----------------------------------------------------------------
            // The time difference is within the window of opportunity so
            //   need to flag the directory as needing to be rescanned.
            //-----------------------------------------------------------------
            return diskCache::Cache::Directory::MODIFIED_RESET;
        }
        QUEUE_LOG_MESSAGE( "Exit: " << Path, MT_DEBUG, 50, method_name, "CXX" );
        return StatStruct.st_mtime;
    }

    //---------------------------------------------------------------------------
    //
    /// \brief Get errno string message.
    ///
    /// This method interpret error code into the string message.
    ///
    /// \return
    ///     Formatted error message.
    //---------------------------------------------------------------------------

    inline const string
    errnoMessage( )
    {
        std::ostringstream msg;
        msg << " errno=" << errno << " (";

        string errno_msg( strerror( errno ) );
        msg << errno_msg << ')';

        return msg.str( );
    }

    void
    read_directory( const std::string&                Path,
                    const std::string&                RootDirectory,
                    directory_container_type&         Children,
                    ignored_type&                     IgnoredDirectories,
                    Directory::index_container_type&  OldData,
                    DirectoryScanData&                ScanData,
                    const diskCache::Cache::StatInfo& Stat )
    {
        static const CHAR* const method_name( __FILE__
                                              "::anonymous::read_directory: " );

        //-------------------------------------------------------------------
        // Ensure starting with empty lists
        //-------------------------------------------------------------------
        Children.erase( Children.begin( ), Children.end( ) );
        IgnoredDirectories.erase( IgnoredDirectories.begin( ),
                                  IgnoredDirectories.end( ) );
        //-------------------------------------------------------------------
        // Collect all entries for the directory.
        //-------------------------------------------------------------------
        typedef LDASTools::AL::Directory::block_read_type entries_type;
        entries_type                                      entries;

        {
            static const char* close_func = "Close()";
            static const char* next_func = "Next()";
            static const char* open_func = "Open()";
            const char*        active_func;

            LDASTools::AL::Directory current_dir( Path, false );

            try
            {
                //-----------------------------------------------------------------
                // Open
                //-----------------------------------------------------------------
                active_func = open_func;
                current_dir.Open( );
                //-----------------------------------------------------------------
                // Read
                //-----------------------------------------------------------------
                active_func = next_func;
                current_dir.Next( entries );
                //-----------------------------------------------------------------
                // Close
                //-----------------------------------------------------------------
                active_func = close_func;
                current_dir.Close( );

                {
                    std::ostringstream msg;

                    msg << "Completed read of directory: " << Path;
                    GenericAPI::queueLogEntry(
                        msg.str( ),
                        GenericAPI::LogEntryGroup_type::MT_DEBUG,
                        30,
                        method_name,
                        "CXX" );
                }
            }
            catch ( const TimeoutException& Exception )
            {
                try
                {
                    //--------------------------------------------------------------
                    // Close
                    //--------------------------------------------------------------
                    active_func = close_func;
                    current_dir.Close( );
                }
                catch ( ... )
                {
                }

                std::ostringstream msg;

                msg << "TimeoutException for: " << current_dir.Name( ) << " - "
                    << active_func << " " << Exception.what( ) << ")";

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    0,
                    method_name,
                    "CXX_INTERFACE" );
                //-----------------------------------------------------------------
                // Retrow the exception so others can act upon it
                //-----------------------------------------------------------------
                throw;
            }
        }
        //-------------------------------------------------------------------
        // Process the entries
        //-------------------------------------------------------------------
        string            temp_file;
        DirectoryScanData dsd( OldData,
                               OldData,
                               ScanData.s_results,
                               ScanData.DirManager( ),
                               RootDirectory );

        dsd.s_directory_root = RootDirectory;
        for ( entries_type::const_iterator entry_cur = entries.begin( ),
                                           entry_last = entries.end( );
              entry_cur != entry_last;
              ++entry_cur )
        {
            try
            {
                //-----------------------------------------------------------------
                // Within this loop, the file should be identified.
                // Subsequent checks will only be done if the previous check
                //   failed.
                //-----------------------------------------------------------------
                //-----------------------------------------------------------------
                // \todo Check if the file should be ignored
                //-----------------------------------------------------------------
                if ( ( ( entry_cur->size( ) > 0 ) &&
                       ( ( *entry_cur )[ 0 ] == '.' ) ) ||
                     ( ExcludedPattern::IsExcluded( *entry_cur ) ) )
                {
                    continue;
                }

                //-----------------------------------------------------------------
                /// \todo
                /// Check if any registered indexing scheme indexes this type of
                ///   file and if so, what assumption does it make about the
                ///   file type.
                //-----------------------------------------------------------------
                dsd.s_filename = *entry_cur;
                if ( diskCache::Cache::RegistrySingleton::ScanForMatch( dsd ) )
                {
                    continue;
                }
                //-----------------------------------------------------------------
                // Check for leaf optimization
                //
                // If the link count != 2, then this directory is not a leaf.
                // If the link count == 2, then the directory is a leaf
                //    When it is a leaf, then we skip the stat and hence
                //    the check for symlink is also disabled.
                //-----------------------------------------------------------------
                if ( Stat.stat_buffer.st_nlink != 2 )
                {
                    //-------------------------------------------------------------
                    /// \todo
                    /// Check if entry should be excluded from the scan.
                    /// \note
                    ///    This check assumes that patterns that match
                    ///    are not directories
                    //-------------------------------------------------------------
                    //-------------------------------------------------------------
                    // The following checks need to know the file type so a
                    // system
                    //   call is done.
                    //-------------------------------------------------------------
                    temp_file = Path;
                    temp_file += "/";
                    temp_file += *entry_cur;

                    struct stat stat_struct;
                    QUEUE_LOG_MESSAGE( "do_stat:"
                                           << " Path: " << Path
                                           << " entry_cur: " << *entry_cur
                                           << " temp_file: " << temp_file,
                                       MT_DEBUG,
                                       5,
                                       method_name,
                                       "CXX" );
                    do_stat( temp_file, stat_struct, false );

                    if ( S_ISLNK( stat_struct.st_mode ) )
                    {
                        throw SymbolicLinkError( temp_file, NULL );
                    }
                    if ( S_ISDIR( stat_struct.st_mode ) )
                    {
                        //---------------------------------------------------------
                        // Check to see if the directory should be excluded
                        //---------------------------------------------------------
                        if ( ExcludedDirectoriesSingleton::IsExcluded(
                                 *entry_cur ) )
                        {
                            std::ostringstream msg;

                            msg << "Excluding directory: " << temp_file;
                            GenericAPI::queueLogEntry(
                                msg.str( ),
                                GenericAPI::LogEntryGroup_type::MT_NOTE,
                                0,
                                method_name,
                                "CXX_INTERFACE" );
                            IgnoredDirectories.insert( *entry_cur );
                            continue;
                        }
                        //---------------------------------------------------------
                        // Locate the device on which this directory is mounted
                        //---------------------------------------------------------
#if 0
                stat_id id;
                id.sid_id.dev_id = stat_struct.st_dev;
                id.sid_id.inode_id = 0; // stat_struct.st_ino;
                Devices::element_type dev = Devices::Find( temp_file, id.id );
#endif /* 0 */
                        //---------------------------------------------------------
                        /// Add to the list of directories
                        //---------------------------------------------------------
                        Children.push_back(
                            directory_element_type( *entry_cur, false ) );
                        continue;
                    }
                    //-------------------------------------------------------------
                    // Everything else is a file that is unmanaged.
                    //-------------------------------------------------------------
                }
            }
            catch ( const SymbolicLinkError& Except )
            {
                QUEUE_LOG_MESSAGE(
                    Except.what( ), MT_NOTE, 0, method_name, "CXX_INTERFACE" );
                continue;
            }

        } // for
        //-------------------------------------------------------------------
        // Now have all search engines do any necessary cleanup.
        //-------------------------------------------------------------------
        dsd.s_filename = Path;

        diskCache::Cache::RegistrySingleton::OnDirectoryClose( dsd );
        //-------------------------------------------------------------------
        // Put the directories in order for easy post processing
        //-------------------------------------------------------------------
        Children.sort( );
    }
} // namespace
