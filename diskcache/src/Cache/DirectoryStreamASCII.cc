//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <iterator>
#include <list>

#include <boost/shared_ptr.hpp>

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Streams/ASCII.hh"

#include "diskcacheAPI/Cache/SDGTx.hh"

namespace diskCache
{
    namespace Cache
    {
        using Streams::OASCII;

        //-------------------------------------------------------------------
        // Write the memory contents out to the stream.
        //-------------------------------------------------------------------
        template <>
        OASCII&
        Directory::Write( OASCII& Stream ) const
        {
            const Streams::Interface::version_type version( Stream.Version( ) );

            Stream.Directory( Fullname( ) );
            Stream.DirectoryModifyTime( TimeModified( ) );
            //-----------------------------------------------------------------
            // Writing of the search specific data
            //-----------------------------------------------------------------
            index_container_type::const_iterator indexes_cur =
                                                     m_indexes.begin( ),
                                                 indexes_last =
                                                     m_indexes.end( );

            if ( version >= Streams::Interface::VERSION_GENERIC_INDEXES )
            {
                //---------------------------------------------------------------
                /// \todo
                /// Loop over all of the associated indexes and have them
                /// dump their information to the Stream
                /// NOTE: This currently only dumps the SDTx information
                //---------------------------------------------------------------
                const RegistrySingleton::id_type id(
                    RegistrySingleton::Id( SDGTx::AsciiId ) );

                indexes_cur = m_indexes.find( id );
            }
            else
            {
                const RegistrySingleton::id_type id(
                    RegistrySingleton::Id( SDGTx::AsciiId ) );

                indexes_cur = m_indexes.find( id );
                if ( indexes_cur != indexes_last )
                {
                    indexes_last = indexes_cur;
                    ++indexes_last;
                }
            }

            while ( indexes_cur != indexes_last )
            {
                try
                {
                    RegistrySingleton::info_type reg_info(
                        RegistrySingleton::GetInfo( indexes_cur->first ) );

                    OASCII::write_func func(
                        OASCII::Writer( indexes_cur->first ) );

#if 0
	  //-------------------------------------------------------------
	  // Write the label
	  //-------------------------------------------------------------
	  Stream << reg_info->m_key_name;
#endif /* 0 */
                    //-------------------------------------------------------------
                    // Write the output
                    //-------------------------------------------------------------
                    boost::shared_ptr< Streams::Streamable > data(
                        indexes_cur->second );
                    ( *func )( Stream, *data );
                }
                catch ( ... )
                {
                    //-------------------------------------------------------------
                    // Should only have a range error, either on the reg_info
                    // or for func.
                    // Either way, cannot write the information to the stream.
                    // Quietly absorb the condition and continue with the
                    // next entry.
                    //-------------------------------------------------------------
                }
                //---------------------------------------------------------------
                // Advance
                //---------------------------------------------------------------
                ++indexes_cur;
            }
            return Stream;
        } // method template<> Write( )

    } // namespace Cache

} // namespace diskCache
