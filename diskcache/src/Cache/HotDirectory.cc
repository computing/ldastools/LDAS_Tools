//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <sys/time.h>

#include <map>
#include <set>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Task.hh"
#include "ldastoolsal/TaskThread.hh"
#include "ldastoolsal/ThreadPool.hh"

#include "diskcacheAPI/Common/Logging.hh"

#include "diskcacheAPI/Cache/DirectoryManager.hh"
#include "diskcacheAPI/Cache/HotDirectory.hh"

using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::Task;
using LDASTools::AL::TaskThread;
using LDASTools::AL::ThreadPool;

/// \todo
///     The HotDirectory class needs to be integrated into the current scheme.

using diskCache::Cache::HotDirectory;
typedef HotDirectory::timestamp_type     timestamp_type;
typedef HotDirectory::directory_set_type directory_set_type;

namespace
{
    using LDASTools::AL::GPSTime;
#if WORKING
    using diskCache::DirectoryInfo;
#endif /* WORKING */
    using diskCache::Common::LogMessage;
    using GenericAPI::queueLogEntry;

    static const std::string JOB_INFO = "HOT_DIR";

    //---------------------------------------------------------------------
    /// \brief Process the collection of hot directories.
    ///
    /// This maintains the thread that is responsible for scanning
    /// directories that are being updated later than the user
    /// specified lower bound time.
    //---------------------------------------------------------------------
    class daemon : Task
    {
    public:
        //-------------------------------------------------------------------
        /// \brief Storage type for directory information.
        ///
        /// Storage of the directory information allowing for reference
        /// counting so as to protect the memory being referenced to stay
        /// within scope till all consumers have completed their use of
        /// of the data.
        //-------------------------------------------------------------------
        typedef HotDirectory::shared_dir_info_type shared_dir_info_type;

        //-------------------------------------------------------------------
        /// \brief Storage type for directory information.
        ///
        /// Storage of the directory information in a way that allows for
        /// the directory information to be removed from the system.
        //-------------------------------------------------------------------
        typedef HotDirectory::weak_dir_info_type weak_dir_info_type;

        //-------------------------------------------------------------------
        // \brief Low level holding the directory information
        //-------------------------------------------------------------------
        typedef HotDirectory::shared_dir_info_type::element_type dir_info_type;

#if WORKING
        //-------------------------------------------------------------------
        /// \brief Collection of patterns used to exclude directories
        ///
        /// This information needs to be preserved for doing a scan
        /// of a directory.
        /// It should be treated as an opaque object.
        //-------------------------------------------------------------------
        typedef HotDirectory::dir_container_type dir_container_type;
#endif /* WORKING */

        //-------------------------------------------------------------------
        /// \brief Storage class of state information.
        ///
        /// This information is relevant for each directory that is
        /// considered active.
        //-------------------------------------------------------------------
        struct info
        {
            //-----------------------------------------------------------------
            /// \brief Reference to directory information
            ///
            /// This reference is used to request the scanning.
            //-----------------------------------------------------------------
            weak_dir_info_type m_dir_info_ref;

            //-----------------------------------------------------------------
            /// \brief Reference to the collection of directories.
            ///
            /// This is the reference to the set of directories of which
            /// m_dir_info_ref was origionally a part.
            /// It is needed as a parameter later on and as such needs to be
            /// recorded.
            //-----------------------------------------------------------------
            directory_set_type* m_directories;

#if UNUSED
            //-----------------------------------------------------------------
            /// \brief Default Constructor
            //-----------------------------------------------------------------
            info( );
#endif /* UNUSED */

            //-----------------------------------------------------------------
            /// \brief Constructor
            //-----------------------------------------------------------------
            info( weak_dir_info_type        DirInfo,
                  const directory_set_type& DirectorySet );
        };

        //-------------------------------------------------------------------
        /// \brief Default constructor
        //-------------------------------------------------------------------
        daemon( );

        virtual ~daemon( );

        //-------------------------------------------------------------------
        /// \brief Iterate over each hot directory
        ///
        /// \param[in] Func
        ///     Function to execute.
        //-------------------------------------------------------------------
        static void ForEach( HotDirectory::UnaryFunction& Func );

        //-------------------------------------------------------------------
        /// \brief Start the daemon thread
        //-------------------------------------------------------------------
        static void Start( );

        static void Stop( );

#if WORKING
        static void WaitTilIdle( );
#endif /* WORKING */

        //-------------------------------------------------------------------
        /// \brief Update each of entries in the collection of hot directories.
        //-------------------------------------------------------------------
        virtual void operator( )( );

    private:
        class no_next : public std::out_of_range
        {
        public:
            no_next( ) : std::out_of_range( "Hot Directory List exhausted" )
            {
            }
        };

        bool finished;

        //-------------------------------------------------------------------
        /// \brief Get the next hot directory to scan.
        ///
        /// \param[in] DirInfoRef
        /// \param[in] ExcludeDirs
        ///
        /// \return
        ///     The reference that represents
        //-------------------------------------------------------------------
        static weak_dir_info_type next( weak_dir_info_type   DirInfoRef,
                                        directory_set_type*& DirectorySet );
    };

    //---------------------------------------------------------------------
    /// \brief Container type for storing hot directory information.
    ///
    /// This container class stores information about directories
    /// that are considered hot.
    /// The container is indexed by the directory path.
    //---------------------------------------------------------------------
    typedef std::map< std::string, daemon::info > hot_directory_container_type;

    TaskThread* HotDirectoryThread = (TaskThread*)NULL;
    daemon*     HotDirectoryTask = new daemon;

    //---------------------------------------------------------------------
    /// \brief Obtain lock
    ///
    /// The use of a function ensures the proper initialization of the data
    /// without having to depend on the link order initialization.
    //---------------------------------------------------------------------
    inline MutexLock::baton_type
    baton( )
    {
        static MutexLock::baton_type baton;

        return baton;
    }

    //---------------------------------------------------------------------
    /// \brief Obtain list of registered hot directories
    ///
    /// The use of a function ensures the proper initialization of the data
    /// without having to depend on the link order initialization.
    //---------------------------------------------------------------------
    inline hot_directory_container_type&
    hot_list( )
    {
        static hot_directory_container_type retval;

        return retval;
    }

    //---------------------------------------------------------------------
    /// \brief Register Entry in collection of hot directories
    ///
    /// This method registers Entry with the collection of hot
    /// directories only if it is not already a part of the collection.
    ///
    /// \param[in] Entry
    ///     The directory entry to be registered.
    /// \param[in] Log
    ///     Set to true if the directory should log a message if
    ///     Entry is added to the hot list.
    //---------------------------------------------------------------------
    inline void
    cond_register( daemon::weak_dir_info_type              Entry,
                   const HotDirectory::directory_set_type& DirectorySet,
                   bool                                    Log )
    {
        daemon::shared_dir_info_type dir( Entry.lock( ) );
        daemon::shared_dir_info_type hl_dir;

        if ( !dir )
        {
            // Cannot register a directory that has already been released.
            return;
        }

        MutexLock l( baton( ), __FILE__, __LINE__ );

        hot_directory_container_type::iterator pos =
            hot_list( ).find( dir->Fullname( ) );
        if ( pos != hot_list( ).end( ) )
        {
            std::ostringstream msg;

            hl_dir = pos->second.m_dir_info_ref.lock( );

            if ( hl_dir == dir )
            {
                //---------------------------------------------------------------
                // Directory has already been registered.
                //---------------------------------------------------------------
                return;
            }
            //-----------------------------------------------------------------
            // Reset the directory
            //-----------------------------------------------------------------
            pos->second.m_dir_info_ref = Entry;
            msg << "Reset Directory: " << dir->Fullname( )
                << " to list of hot directories.";
            queueLogEntry( msg.str( ),
                           GenericAPI::LogEntryGroup_type::MT_DEBUG,
                           10,
                           "cond_register@HotDirectory.cc",
                           JOB_INFO );
            return;
        }
        //-------------------------------------------------------------------
        // Create new entry.
        //-------------------------------------------------------------------
        hot_directory_container_type::value_type n(
            dir->Fullname( ),
            hot_directory_container_type::mapped_type( Entry, DirectorySet ) );
        hot_list( ).insert( n );

        if ( Log )
        {
            std::ostringstream msg;

            msg << "Added " << dir->Fullname( )
                << " to list of hot directories.";
            queueLogEntry( msg.str( ),
                           GenericAPI::LogEntryGroup_type::MT_NOTE,
                           0,
                           "cond_register@HotDirectory.cc",
                           JOB_INFO );
        }

        //-------------------------------------------------------------------
        // Ensure the scan daemon is running
        //-------------------------------------------------------------------
        daemon::Start( );
    }

    //---------------------------------------------------------------------
    /// \brief Check if Entry is hot
    ///
    /// This method will check if Entry has been modified within the
    /// specified time constraint.
    /// If it has, then it is considered hot.
    ///
    /// \param[in] Entry
    ///     The directory entry to be checked.
    ///
    /// \return
    ///     The value true is returned if the Entry is considered hot,
    ///     false otherwise.
    //---------------------------------------------------------------------
    inline bool
    is_hot( daemon::dir_info_type* Entry )
    {
        struct timeval timev;
        gettimeofday( &timev, NULL );

        const HotDirectory::timestamp_type localtime_secs = timev.tv_sec;

        return ( ( localtime_secs - Entry->TimeModified( ) ) <
                 HotDirectory::HotLowerBound( ) );
    }

    //---------------------------------------------------------------------
    /// \brief Check if Entry is hot
    ///
    /// This method will check if Entry has been modified within the
    /// specified time constraint.
    /// If it has, then it is considered hot.
    ///
    /// \param[in] Entry
    ///     The directory entry to be checked.
    ///
    /// \return
    ///     The value true is returned if the Entry is considered hot,
    ///     false otherwise.
    //---------------------------------------------------------------------
    inline bool
    is_hot( daemon::weak_dir_info_type Entry )
    {
        daemon::shared_dir_info_type dir( Entry.lock( ) );

        if ( !dir )
        {
            return false;
        }
        return is_hot( dir.get( ) );
    }

    //---------------------------------------------------------------------
    /// \brief Remove Entry from collection of hot directories.
    ///
    /// Rmeoves Entry from the collection of hot directories.
    /// This operation needs to be very careful as the requested
    /// Entry for removal may currently be in a scanning mode.
    /// It also needs to be careful as the Entry may be in
    /// its destructor state at the time of this call.
    ///
    /// \param[in] Entry
    ///     The directory entry to be removed from the collection
    ///     of hot directories.
    //---------------------------------------------------------------------
    inline void
    unregister( daemon::weak_dir_info_type Entry )
    {
        daemon::shared_dir_info_type dir( Entry.lock( ) );

        if ( !dir )
        {
            //-----------------------------------------------------------------
            // No info is available.
            //-----------------------------------------------------------------
            return;
        }

        MutexLock l( baton( ), __FILE__, __LINE__ );

        hot_directory_container_type::iterator pos =
            hot_list( ).find( dir->Fullname( ) );

        if ( ( pos != hot_list( ).end( ) ) &&
             ( pos->second.m_dir_info_ref.lock( ).get( ) == dir.get( ) ) )
        {
            std::ostringstream msg;

            msg << "Removed " << pos->first
                << " from the list of hot directories.";
            queueLogEntry( msg.str( ),
                           GenericAPI::LogEntryGroup_type::MT_NOTE,
                           0,
                           "daemon::unregister()@HotDirectory.cc",
                           JOB_INFO );
            hot_list( ).erase( pos );
        }
    }

#if UNUSED
    //---------------------------------------------------------------------
    /// Create a new instance of the object being careful to properly
    /// initialize data members.
    //---------------------------------------------------------------------
    daemon::info::info( )
    {
    }
#endif /* UNUSED */

    //---------------------------------------------------------------------
    /// Construct an instance of the object with all members of the object
    /// specified.
    //---------------------------------------------------------------------
    daemon::info::info( weak_dir_info_type        DirInfoRef,
                        const directory_set_type& DirectorySet )
        : m_dir_info_ref( DirInfoRef ),
          m_directories( const_cast< directory_set_type* >( &DirectorySet ) )
    {
    }

    //---------------------------------------------------------------------
    /// Ensure proper initialization of task.
    //---------------------------------------------------------------------

    daemon::daemon( ) : Task( "HotDir daemon" ), finished( false )
    {
        delete_on_completion( true );
    }

    daemon::~daemon( )
    {
        HotDirectoryTask = (daemon*)NULL;
    }

    void
    daemon::ForEach( HotDirectory::UnaryFunction& Func )
    {
        QUEUE_LOG_MESSAGE( "HotList"
                               << " Size: " << hot_list( ).size( ),
                           MT_DEBUG,
                           5,
                           "HotDirectory.cc::anonymous::daemon::ForEach",
                           "CXX" );
        for ( hot_directory_container_type::iterator cur = hot_list( ).begin( ),
                                                     last = hot_list( ).end( );
              cur != last;
              ++cur )
        {
            Func( cur->second.m_dir_info_ref );
        }
    }

    //---------------------------------------------------------------------
    /// Obtain the next logical directory to scan.
    /// This routine will skip any directory that is only referenced
    /// within this list.
    //---------------------------------------------------------------------
    daemon::weak_dir_info_type
    daemon::next( weak_dir_info_type   DirInfoRef,
                  directory_set_type*& DirectorySet )
    {
        MutexLock l( baton( ), __FILE__, __LINE__ );

        hot_directory_container_type::iterator pos;

        shared_dir_info_type dir( DirInfoRef.lock( ) );

        //-------------------------------------------------------------------
        // Obtain the next logical entry
        //-------------------------------------------------------------------
        if ( dir )
        {
            //-----------------------------------------------------------------
            // Since this is NOT the first time called, get the node just
            // beyond the current postion.
            //-----------------------------------------------------------------
            pos = hot_list( ).upper_bound( dir->Fullname( ) );
        }
        else
        {
            //-----------------------------------------------------------------
            // As this is the first time that it is called, start at the
            // beginning
            //-----------------------------------------------------------------
            pos = hot_list( ).begin( );
        }
        //-------------------------------------------------------------------
        // Loop over the list to skip nodes that should be ignored.
        //-------------------------------------------------------------------
        while ( pos != hot_list( ).end( ) &&
                ( pos->second.m_dir_info_ref.expired( ) == true ) )
        {
            //-----------------------------------------------------------------
            // Skip past any node that is considered unique as they are no
            // longer part of the mount point hash.
            //-----------------------------------------------------------------
            pos = hot_list( ).upper_bound( pos->first );
        }
        //-------------------------------------------------------------------
        // Setup for the return.
        //-------------------------------------------------------------------
        if ( pos != hot_list( ).end( ) )
        {
            //-----------------------------------------------------------------
            // Have a node to scan, so fill in the information for the caller.
            //-----------------------------------------------------------------
            DirInfoRef = pos->second.m_dir_info_ref;
            DirectorySet = pos->second.m_directories;
        }
        else
        {
            //-----------------------------------------------------------------
            // No more entries so notify the caller of this condition.
            //-----------------------------------------------------------------
            throw no_next( );
        }
        return DirInfoRef;
    }

    //---------------------------------------------------------------------
    /// Start the thread for updating entries that are part of the
    /// collection of hot directories.
    //---------------------------------------------------------------------
    void
    daemon::Start( )
    {
        static bool needs_to_be_started = true;

        if ( needs_to_be_started )
        {
            //-----------------------------------------------------------------
            // Get a thread in which to run this task in the background
            //-----------------------------------------------------------------
            TaskThread* HotDirectoryThread = ThreadPool::Acquire( );
            HotDirectoryTask = new daemon;
            //-----------------------------------------------------------------
            /// \todo Need to setup the task thread to be returned to the pool
            ///     once the task is complete.
            //-----------------------------------------------------------------

            //-----------------------------------------------------------------
            // Add the task to the thread and get things running
            //-----------------------------------------------------------------
            HotDirectoryThread->AddTask( HotDirectoryTask );
            needs_to_be_started = false;
        }
    }

    void
    daemon::Stop( )
    {
        if ( ( HotDirectoryThread ) &&
             ( ThreadPool::Active( HotDirectoryThread ) ) )
        {
            HotDirectoryThread->Halt( );
            ThreadPool::Relinquish( HotDirectoryThread );
            HotDirectoryThread = (TaskThread*)NULL;
        }
    }

#if WORKING
    void
    daemon::WaitTilIdle( )
    {
    }
#endif /* WORKING */

    //---------------------------------------------------------------------
    /// Continuously update directories listed in the collection of
    /// hot directories.
    /// After each iteration, pause for specified period of time.
    ///
    /// \todo
    ///     This routine could be made smarter by anticipating when
    ///     the directory will next be updated and suspend till that
    ///     time.
    //---------------------------------------------------------------------
    void
    daemon::operator( )( )
    {
        static const char* caller =
            "anonymous::daemon::operator()@HotDirectory.cc";

        //-------------------------------------------------------------------
        /// Suspend for a period of time
        //-------------------------------------------------------------------
        struct timespec wakeup;
        wakeup.tv_sec = 30;
        wakeup.tv_nsec = 0;
        nanosleep( &wakeup, NULL );

        while ( ( !finished ) && ( MemChecker::IsExiting( ) == false ) )
        {
            //-----------------------------------------------------------------
            // Loop over each element in the hot list
            //-----------------------------------------------------------------
            try
            {
                weak_dir_info_type   cur;
                shared_dir_info_type dir;

                while ( !finished )
                {
                    directory_set_type* manager;
                    LogMessage          errors;

                    cur = next( cur, manager );

                    if ( ( dir = cur.lock( ) ) )
                    {
                        //-----------------------------------------------------------
                        // Since it is now locked, it is safe to scan
                        //-----------------------------------------------------------
                        diskCache::Cache::Directory::ScanResults results;

                        manager->Scan( dir, results );
                        if ( errors.MessageType( ) != LogMessage::LM_DEBUG )
                        {
                            //---------------------------------------------------------
                            // Write the non empty message to the log stream
                            //---------------------------------------------------------
                            GenericAPI::queueLogEntry(
                                errors.Message( ).str( ),
                                ( GenericAPI::LogEntryGroup_type::
                                      message_type )( errors.MessageType( ) ),
                                0,
                                caller,
                                JOB_INFO );
                        }
                    }
                }
            }
            catch ( const no_next& Exception )
            {
                //---------------------------------------------------------------
                // Have completed the loop around list of hot directories
                //---------------------------------------------------------------
            }
            //-----------------------------------------------------------------
            // Cleanup any entries that need to be removed.
            //-----------------------------------------------------------------
            if ( !finished )
            {
                MutexLock l( baton( ), __FILE__, __LINE__ );

                for ( hot_directory_container_type::iterator
                          hl_cur = hot_list( ).begin( ),
                          hl_last = hot_list( ).end( ),
                          dead_entry;
                      hl_cur != hl_last; )
                {
                    daemon::shared_dir_info_type dir(
                        hl_cur->second.m_dir_info_ref.lock( ) );

                    if ( !dir )
                    {
                        //-----------------------------------------------------------
                        // The only reference to the directory information is
                        // within this list of hot directories.
                        //-----------------------------------------------------------
                        dead_entry = hl_cur;
                        ++hl_cur;
                        hot_list( ).erase( dead_entry );
                    }
                    else if ( is_hot( dir.get( ) ) == false )
                    {
                        //-----------------------------------------------------------
                        // The direcotry has not been recently updated and is
                        // no longer considered hot.
                        //-----------------------------------------------------------
                        struct timeval     timev;
                        std::ostringstream msg;

                        gettimeofday( &timev, NULL );

                        const HotDirectory::timestamp_type localtime_secs =
                            timev.tv_sec;

                        msg << "Removed " << hl_cur->first
                            << " from the list of hot directories."
                            << " ( "
                            << ( localtime_secs - dir->TimeModified( ) )
                            << " seconds old "
                            << " [lower bound: "
                            << HotDirectory::HotLowerBound( ) << "]"
                            << " )";
                        queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_NOTE,
                                       0,
                                       "daemon::operator()@HotDirectory.cc",
                                       JOB_INFO );
                        dead_entry = hl_cur;
                        ++hl_cur;
                        hot_list( ).erase( dead_entry );
                    }
                    else
                    {
                        ++hl_cur;
                    }
                }
            }
            //-----------------------------------------------------------------
            /// Suspend for a period of time
            //-----------------------------------------------------------------
            if ( !finished )
            {
                wakeup.tv_sec = HotDirectory::ScanInterval( );
                wakeup.tv_nsec = 0;
                nanosleep( &wakeup, NULL );
            }
        }
    }

} // namespace

namespace diskCache
{
    namespace Cache
    {
        timestamp_type HotDirectory::m_lower_bound = 300;
        timestamp_type HotDirectory::m_scan_interval = 4;

        void
        HotDirectory::ForEach( HotDirectory::UnaryFunction& Func )
        {
            daemon::ForEach( Func );
        }
        //---------------------------------------------------------------------
        /// Return the time representing the lower bounds of what should be
        /// considered hot.
        //---------------------------------------------------------------------
        timestamp_type
        HotDirectory::HotLowerBound( )
        {
            return m_lower_bound;
        }

        //---------------------------------------------------------------------
        /// Establish the time representing the lower bounds of what should be
        /// considered hot.
        //---------------------------------------------------------------------
        void
        HotDirectory::HotLowerBound( timestamp_type Value )
        {
            static const char* caller = "HotDirectory::HotLowerBound";
            QUEUE_LOG_MESSAGE( "Changing hotdirectory lower bound from: "
                                   << m_lower_bound << " to: " << Value,
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );

            m_lower_bound = Value;
        }

        //---------------------------------------------------------------------
        /// This method simply reports if a directory is in the collection
        /// of directories that are scanned at a higher rate as they appear
        /// to be actively filling with new files.
        //---------------------------------------------------------------------
        bool
        HotDirectory::IsRegistered( const std::string& Path )
        {
#if 1
            return ( hot_list( ).find( Path ) != hot_list( ).end( ) );
#else
            bool retval = false;

            hot_directory_container_type::iterator pos =
                hot_list( ).find( Path );

            if ( pos != hot_list( ).end( ) )
            {
                retval = true;
            }
            return retval;
#endif /* 1 */
        }

        //---------------------------------------------------------------------
        /// Checks to see if the directory has been modified within the time
        /// range to be considered hot.
        /// Directories found to be hot are added to the list of hot
        /// directories and are scanned more frequently.
        //---------------------------------------------------------------------
        void
        HotDirectory::RegisterIfHot( weak_dir_info_type        Entry,
                                     const directory_set_type& Manager,
                                     bool                      Log )
        {
            if ( is_hot( Entry ) )
            {
                //-----------------------------------------------------------------
                // This one needs to be registered
                //-----------------------------------------------------------------
                cond_register( Entry, Manager, Log );
            }
        }

        void
        HotDirectory::Reset( )
        {
            MutexLock l( baton( ), __FILE__, __LINE__ );

            hot_list( ).clear( );
        }

        //---------------------------------------------------------------------
        /// Establish the time representing the number of seconds to sleep
        /// between scans of the set of hot directories.
        //---------------------------------------------------------------------
        void
        HotDirectory::ScanInterval( timestamp_type Value )
        {
            static const char* caller = "HotDirectory::ScanInterval";
            QUEUE_LOG_MESSAGE( "Changing hotdirectory scan interval from: "
                                   << m_scan_interval << " to: " << Value,
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );
            m_scan_interval = Value;
        }

        void
        HotDirectory::StopDaemon( )
        {
            daemon::Stop( );
            Reset( );
        }

        //---------------------------------------------------------------------
        /// Remove a directory from the list of hot directories.
        //---------------------------------------------------------------------
        void
        HotDirectory::Unregister( weak_dir_info_type Entry )
        {
            unregister( Entry );
        }
    } // namespace Cache
} // namespace diskCache
