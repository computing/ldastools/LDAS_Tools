//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__QUERY_ANSWER_HH
#define DISKCACHE_API__CACHE__QUERY_ANSWER_HH

#include <list>
#include <map>
#include <stdexcept>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer
        {
        public:
            typedef std::list< std::string > error_container_type;

            class IndexDataBase
            {
            public:
                enum
                {
                    GEN_FILENAMES = 0x0001,
                    GEN_GAPS = 0x0002,
                    GEN_INTERVALS = 0x0004,
                    GEN_MASK = 0xFFFF
                };

                typedef std::list< std::string > filename_container_type;
                typedef std::list< std::string > gap_container_type;
                typedef std::list< std::string > interval_container_type;

                IndexDataBase( INT_4U Mask );

                virtual ~IndexDataBase( );

                virtual void
                Complete( INT_4U Mask = IndexDataBase::GEN_MASK ) = 0;

                const filename_container_type& Filenames( ) const;

                virtual bool IsCompleted( ) const = 0;

                virtual void Resample( ) = 0;

                virtual void Swap( filename_container_type& Filenames );

                virtual void SwapGaps( gap_container_type& Gaps );

                virtual void
                SwapIntervals( interval_container_type& Intervals );

                INT_4U mask;

                filename_container_type m_filenames;
                gap_container_type      m_gaps;
                interval_container_type intervals;
            };

            typedef INT_4U                             id_type;
            typedef boost::shared_ptr< IndexDataBase > index_data_type;
            typedef IndexDataBase::filename_container_type
                                                      filename_container_type;
            typedef IndexDataBase::gap_container_type gap_container_type;
            typedef IndexDataBase::interval_container_type
                interval_container_type;

            QueryAnswer( );

            void AddError( const std::string& Message );

            void Complete( INT_4U = IndexDataBase::GEN_MASK );

            const error_container_type& Errors( ) const;

            const filename_container_type& Filenames( ) const;

            index_data_type IndexData( ) const;

            void IndexData( index_data_type Data );

            void IndexData( IndexDataBase* Data );

            void IndexId( id_type Id );

            id_type IndexId( ) const;

            void IgnoreFilenames( );

            void IgnoreGaps( );

            void IgnoreIntervals( );

            bool IsCompleted( ) const;

            bool IsResampled( ) const;

            void Resample( );

            void Resample( bool Value );

            void Swap( filename_container_type& Filenames );

            void SwapErrors( error_container_type& Errors );

            void SwapGaps( gap_container_type& Gaps );

            void SwapIntervals( interval_container_type& Intervals );

        private:
            index_data_type      m_index_data;
            id_type              m_index_id;
            error_container_type m_errors;
            bool                 m_resample;
            INT_4U               mask;
        }; // class - QueryAnswer

        //===================================================================
        //===================================================================
        inline QueryAnswer::IndexDataBase::IndexDataBase( INT_4U Mask )
            : mask( Mask )
        {
        }

        inline QueryAnswer::IndexDataBase::~IndexDataBase( )
        {
        }

        inline const QueryAnswer::IndexDataBase::filename_container_type&
        QueryAnswer::IndexDataBase::Filenames( ) const
        {
            return m_filenames;
        }

        inline void
        QueryAnswer::IndexDataBase::Swap( filename_container_type& Filenames )
        {
            m_filenames.swap( Filenames );
        }

        inline void
        QueryAnswer::IndexDataBase::SwapGaps( gap_container_type& Gaps )
        {
            m_gaps.swap( Gaps );
        }

        inline void
        QueryAnswer::IndexDataBase::SwapIntervals(
            interval_container_type& Intervals )
        {
            intervals.swap( Intervals );
        }

        //===================================================================
        //===================================================================
        inline QueryAnswer::QueryAnswer( )
            : m_index_id( 0 ), m_resample( false ), mask( 0 )
        {
        }

        inline void
        QueryAnswer::AddError( const std::string& Message )
        {
            m_errors.push_back( Message );
        }

        inline void
        QueryAnswer::Complete( INT_4U Mask )
        {
            if ( m_index_data )
            {
                m_index_data->Complete( Mask );
            }
        }

        inline const QueryAnswer::error_container_type&
        QueryAnswer::Errors( ) const
        {
            return m_errors;
        }

        inline const QueryAnswer::filename_container_type&
        QueryAnswer::Filenames( ) const
        {
            if ( m_index_data )
            {
                return m_index_data->Filenames( );
            }
            throw std::runtime_error(
                "QueryAnswer::Filenames: Invalid m_index_data" );
        }

        inline QueryAnswer::index_data_type
        QueryAnswer::IndexData( ) const
        {
            return m_index_data;
        }

        inline void
        QueryAnswer::IndexData( index_data_type Data )
        {
            m_index_data = Data;
        }

        inline void
        QueryAnswer::IndexData( IndexDataBase* Data )
        {
            m_index_data.reset( Data );
        }

        inline void
        QueryAnswer::IndexId( id_type Id )
        {
            m_index_id = Id;
        }

        inline QueryAnswer::id_type
        QueryAnswer::IndexId( ) const
        {
            return m_index_id;
        }

        inline void
        QueryAnswer::IgnoreFilenames( )
        {
            mask &= ( ~IndexDataBase::GEN_FILENAMES );
        }

        inline void
        QueryAnswer::IgnoreGaps( )
        {
            m_index_data->mask &= ( ~IndexDataBase::GEN_GAPS );
        }

        inline void
        QueryAnswer::IgnoreIntervals( )
        {
            m_index_data->mask &= ( ~IndexDataBase::GEN_INTERVALS );
        }

        inline bool
        QueryAnswer::IsCompleted( ) const
        {
            if ( m_index_data )
            {
                return m_index_data->IsCompleted( );
            }
            return true;
        }

        inline bool
        QueryAnswer::IsResampled( ) const
        {
            return m_resample;
        }

        inline void
        QueryAnswer::Resample( )
        {
            if ( m_index_data )
            {
                m_index_data->Resample( );
                return;
            }
        }

        inline void
        QueryAnswer::Resample( bool Value )
        {
            m_resample = Value;
        }

        inline void
        QueryAnswer::Swap( filename_container_type& Filenames )
        {
            if ( m_index_data )
            {
                m_index_data->Swap( Filenames );
                return;
            }
            throw std::runtime_error(
                "QueryAnswer::Swap: Invalid m_index_data" );
        }

        inline void
        QueryAnswer::SwapErrors( error_container_type& Errors )
        {
            m_errors.swap( Errors );
            return;
        }

        inline void
        QueryAnswer::SwapGaps( gap_container_type& Gaps )
        {
            if ( m_index_data )
            {
                m_index_data->SwapGaps( Gaps );
                return;
            }
            throw std::runtime_error(
                "QueryAnswer::SwapGaps: Invalid m_index_data" );
        }

        inline void
        QueryAnswer::SwapIntervals( interval_container_type& Intervals )
        {
            if ( m_index_data )
            {
                m_index_data->SwapIntervals( Intervals );
                return;
            }
            throw std::runtime_error(
                "QueryAnswer::SwapIntervals: Invalid m_index_data" );
        }

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__QUERY_ANSWER_HH */
