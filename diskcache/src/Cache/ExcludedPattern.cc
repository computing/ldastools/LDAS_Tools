//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <sstream>

#include <boost/algorithm/string/join.hpp>

#include "genericAPI/Logging.hh"

#include "ExcludedPattern.hh"

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< diskCache::Cache::ExcludedPattern > )

namespace diskCache
{
    namespace Cache
    {
        ExcludedPattern::ExcludedPattern( )
        {
            pattern_container_type t;

            t.push_back( ".*[.]tmp" );

            update( t );
        } /* ExcludedDirectories::ExcludedDirectories */

        ExcludedPattern::~ExcludedPattern( )
        {
        }

        const ExcludedPattern::pattern_container_type&
        ExcludedPattern::Get( )
        {
            return Instance( ).get( );
        }

        bool
        ExcludedPattern::IsExcluded( const value_type& Filename )
        {
            return Instance( ).is_excluded( Filename );
        }

        void
        ExcludedPattern::Update( const pattern_container_type& Patterns )
        {
            Instance( ).update( Patterns );
        }

        const ExcludedPattern::pattern_container_type&
        ExcludedPattern::get( ) const
        {
            return readable_pattern;
        }

        bool
        ExcludedPattern::is_excluded( const value_type& Directory ) const
        {
            //-----------------------------------------------------------------
            // If the pattern is found, then it should be excluded.
            //-----------------------------------------------------------------
          return boost::regex_match( Directory, pattern );
        }

        void
        ExcludedPattern::update( const pattern_container_type& Patterns )
        {
            static const char* caller = "ExcludedPattern::update";
            //-----------------------------------------------------------------
            // Generate a new regular expression to quickly identify
            // directories that should be excluded
            //-----------------------------------------------------------------
            std::string        old_pattern( pattern.str( ) );
            std::ostringstream r;

            readable_pattern = Patterns;
            r << "^(" << boost::algorithm::join( readable_pattern, "|") << ")$";

            //-----------------------------------------------------------------
            // Save the new pattern
            //-----------------------------------------------------------------
            pattern = boost::regex( r.str( ), boost::regex::extended );
            QUEUE_LOG_MESSAGE( "Updating file name exclusion pattern from: "
                                   << old_pattern
                                   << " to: " << pattern.str( ),
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );

        } /* ExcludedDirectories::Update */

    } // namespace Cache
} // namespace diskCache
