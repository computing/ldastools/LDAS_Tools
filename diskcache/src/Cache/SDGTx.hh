//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__SDGTX_HH
#define DISKCACHE_API__CACHE__SDGTX_HH

#include <map>
#include <string>

#include <boost/shared_ptr.hpp>

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/RegistrySingleton.hh"
#include "diskcacheAPI/Cache/SearchInterface.hh"

namespace diskCache
{
    namespace Cache
    {
        struct DirectoryScanData;
        class QueryParams;
        class QueryAnswer;

        //-------------------------------------------------------------------
        /// \brief SDGTx caching scheme
        ///
        /// This caching scheme is based on the S-D-G-T.%<ext%> naming
        /// convention that is referenced in the appendix
        /// of the Frame Specification.
        //-------------------------------------------------------------------
        class SDGTx : public SearchInterface
        {
        public:
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            class ExtensionError : public std::runtime_error
            {
            public:
                ExtensionError( );

            private:
                static std::string format( );
            };

            //-----------------------------------------------------------------
            // \brief Tracks the overlap errors that need to be reported
            //-----------------------------------------------------------------
            class OverlapHandler
            {
            public:
                typedef INT_4U time_type;

                //---------------------------------------------------------------
                /// \brief Constructor
                ///
                /// \param[in] SourceDirectory
                ///     The directory being checked for overlap errors
                //---------------------------------------------------------------
                OverlapHandler( const std::string& SourceDirectory );

                //---------------------------------------------------------------
                /// \brief Release resources back to the system
                //---------------------------------------------------------------
                ~OverlapHandler( );

                const std::string& Directory( ) const;

                //---------------------------------------------------------------
                /// \brief Register an overlapping error
                //---------------------------------------------------------------
                void Register( const std::string& DirectoryName,
                       const std::string& Extension,
                       const std::string& IFOType,
                       time_type SourceStart, time_type SourceEnd,
                       time_type SourceDuration,
                       time_type ConflictStart, time_type ConflictEnd,
                       time_type ConflictDuration /* ,
                       const std::string& ConflictDirectoryName */ );

            private:
                typedef std::pair< time_type, time_type > end_type;
                typedef std::map< time_type, end_type >   interval_type;

                typedef std::map< std::string, interval_type > overlapping_type;

                struct range_type
                {
                    time_type        s_end;
                    time_type        s_dt;
                    overlapping_type s_overlaps;
                };

                //---------------------------------------------------------------
                // first - DT
                //---------------------------------------------------------------
                typedef std::map< time_type, range_type >       conflicts_type;
                typedef std::map< std::string, conflicts_type > ifotype_type;
                typedef std::map< std::string, ifotype_type >   ext_type;

                std::string m_directory_name;
                ext_type    m_cache;
            };

            //-----------------------------------------------------------------
            /// \brief This class has the cache of directory information.
            //-----------------------------------------------------------------
            class DirectoryInfo : public Streams::Streamable
            {
            public:
                class OverlappingFileError : public Directory::FileCacheError
                {
                public:
                    OverlappingFileError( const std::string& Filename );

                private:
                    static std::string format( const std::string& Filename );
                };

                typedef DirectoryScanData::scan_data::size_type size_type;
                typedef boost::shared_ptr< Streams::Streamable >
                                    read_return_type;
                typedef std::string extension_type;
                typedef std::string site_type;
                typedef std::string desc_type;
                typedef std::string site_desc_type;
                typedef INT_4U      time_type;
                typedef INT_4U      dt_type;

                DirectoryInfo( );

                void AddFile( const extension_type& Extension,
                              const site_type&      Site,
                              const desc_type&      Description,
                              const time_type       Start,
                              const dt_type         Duration );

                virtual size_type Count( ) const;

                void Find( const Directory& Dir, QueryAnswer& Answer ) const;

                bool FindFile( const extension_type& Extension,
                               const site_type&      Site,
                               const desc_type&      Description,
                               const time_type       Start,
                               const dt_type         Duration ) const;

                bool Overlap( OverlapHandler&       Overlaps,
                              const std::string&    DirectoryName,
                              const extension_type& Extension,
                              const site_type&      Site,
                              const desc_type&      Description,
                              const time_type       Start,
                              const dt_type         Duration ) const;

                template < class ReaderT >
                static read_return_type Read( ReaderT& Stream );

                template < class WriterT >
                void Write( WriterT& Stream ) const;

            private:
                //---------------------------------------------------------------
                /// \brief holder for end time and delta T elements
                //---------------------------------------------------------------
                typedef std::pair< time_type, dt_type > duration_type;
                //---------------------------------------------------------------
                /// \brief holder for start time and duration information
                //---------------------------------------------------------------
                typedef std::map< time_type, duration_type >
                    interval_container_type;
                //---------------------------------------------------------------
                /// \brief holder for site/description and interval data.
                //---------------------------------------------------------------
                typedef std::map< site_desc_type, interval_container_type >
                    site_desc_container_type;
                //---------------------------------------------------------------
                /// \brief holder for extension and site/description data
                //---------------------------------------------------------------
                typedef std::map< extension_type, site_desc_container_type >
                    extension_container_type;

                //---------------------------------------------------------------
                /// \brief Alias for the top level cache container
                //---------------------------------------------------------------
                typedef extension_container_type cache_container_type;

                //---------------------------------------------------------------
                /// \brief Cache of matching files from a single directory
                //---------------------------------------------------------------
                cache_container_type m_cache;

                //---------------------------------------------------------------
                /// \brief Constructor used primarily when reading from a stream
                ///
                /// \param[in,out] Source
                ///     This is the new contents of the cache.
                ///     The old contents are returned as a side effect.
                //---------------------------------------------------------------
                DirectoryInfo( cache_container_type& Source );
            };

            //-----------------------------------------------------------------
            /// \brief Optimized version of an ASCII query
            //-----------------------------------------------------------------
            class QueryParams : public QueryAnswer::IndexDataBase
            {
            public:
                enum
                {
                    CRITERIA_BIT_NONE = 0x0000,
                    CRITERIA_BIT_EXTENSION = 0x0001,
                    CRITERIA_BIT_SITE = 0x0002,
                    CRITERIA_BIT_DESCRIPTION = 0x0004,
                    CRITERIA_BIT_START = 0x0008,
                    CRITERIA_BIT_STOP = 0x0010,
                    CRITERIA_BIT_GAPS = 0x0020,
                    CRITERIA_BIT_RESAMPLE = 0x0040
                };

                typedef INT_4U time_type;

                //---------------------------------------------------------------
                /// \brief Default constructor
                //---------------------------------------------------------------
                QueryParams(
                    INT_4U Mask = QueryAnswer::IndexDataBase::GEN_MASK );

                //---------------------------------------------------------------
                /// \brief Constructor
                ///
                /// \param[in] Params
                ///     The ASCII representation of the query
                //---------------------------------------------------------------
                QueryParams( const Cache::QueryParams& Params );

                virtual void
                Complete( INT_4U Mask = QueryAnswer::IndexDataBase::GEN_MASK );

                void CriteriaDescription( const std::string& Description );

                void CriteriaExtension( const std::string& Extension );

                void CriteriaInterval( time_type Start, time_type Stop );

                void CriteriaSite( const std::string& Site );

                const std::string& Description( ) const;

                const std::string& Extension( ) const;

                void Fill( const std::string& DirectoryName,
                           const std::string& Extension,
                           const std::string& SiteDesc,
                           time_type          Start,
                           time_type          Stop,
                           time_type          Duration );

                virtual bool IsCompleted( ) const;

                virtual void Resample( );

                const std::string& Site( ) const;

                time_type Start( ) const;

                time_type Stop( ) const;

                bool Wildcard( const std::string& Data ) const;

            private:
                friend void    DirectoryInfo::Find( const Directory& Dir,
                                                 QueryAnswer& Answer ) const;
                typedef INT_2U bit_flag_type;
                typedef std::map< time_type, time_type > gap_container_type;

                struct interval_data_type
                {
                    time_type   s_stop;
                    time_type   s_dt;
                    std::string s_directory_name;
                };

                typedef std::map< time_type, interval_data_type >
                    start_container_type;
                typedef std::map< std::string, start_container_type >
                    ext_container_type;
                typedef std::map< std::string, ext_container_type >
                                                 site_desc_container_type;
                typedef site_desc_container_type file_container_type;

                static const char* WILDCARD_PATTERN;

                bit_flag_type m_search_criteria;

                std::string m_extension;

                std::string m_site;

                std::string m_description;

                time_type m_start;

                time_type m_stop;

                gap_container_type m_gaps;

                file_container_type m_files;

                time_type m_min_time;

                time_type m_max_time;

            }; // class QueryParams

            typedef std::string file_extension_type;

            typedef std::list< file_extension_type >
                file_extension_container_type;

            //-----------------------------------------------------------------
            /// \brief Allow for customization
            ///
            /// \param[in] Variable
            ///     Name of the option to modify
            /// \param[in] Value
            ///     The ascii string representation of the value for the option.
            ///
            /// \return
            ///     Upon successful setting of the option, true is returned;
            ///     false otherwise.
            //-----------------------------------------------------------------
            virtual bool Configure( const std::string& Variable,
                                    const std::string& Value );

            //-----------------------------------------------------------------
            /// \brief Register as a search engine
            //-----------------------------------------------------------------
            static RegistrySingleton::id_type RegisterSearchEngine( );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            static void
            FileExtList( const file_extension_container_type& Extensions,
                         const std::string&                   Default );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            static const file_extension_container_type& FileExtList( );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            static void Find( const Streams::Streamable& Source,
                              const Directory&           Dir,
                              QueryAnswer&               Answer );

            //-----------------------------------------------------------------
            /// \brief Function called during processing of a directory
            ///
            /// \param[in] Data
            ///     Place to log information as the directory is being
            ///     processed.
            ///
            /// \return
            ///     The value true is returned if the file being processed
            ///     is cached by the indexing algorithm; false otherwise.
            //-----------------------------------------------------------------
            static bool IsMatch( DirectoryScanData& Data );

            //-----------------------------------------------------------------
            /// \brief Function called after all files have been processed
            ///
            /// \param[in] Data
            ///     Information gathered during teh scanning of the
            ///     directory.
            //-----------------------------------------------------------------
            static void OnDirectoryClose( DirectoryScanData& Data );

            static void TranslateQuery( const Cache::QueryParams& Params,
                                        Cache::QueryAnswer&       Answer );

            //-----------------------------------------------------------------
            /// \brief Query for specific data
            ///
            /// \param[in] Params
            ///     The ASCII representation of the query.
            ///
            /// \param[out] Answer
            ///     The answer set.
            ///
            //-----------------------------------------------------------------
            static void Query( const Cache::QueryParams& Params,
                               Cache::QueryAnswer&       Answer );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            static const char* AsciiId;

            static RegistrySingleton::id_type REGISTRY_ID;

            static file_extension_type s_default_extension;

            static file_extension_container_type s_extensions;

        private:
        };

        inline const std::string&
        SDGTx::OverlapHandler::Directory( ) const
        {
            return m_directory_name;
        }

        //===================================================================
        //===================================================================
        inline void
        SDGTx::QueryParams::CriteriaDescription(
            const std::string& Description )
        {
            m_search_criteria |= CRITERIA_BIT_DESCRIPTION;
            m_description = Description;
        }

        inline void
        SDGTx::QueryParams::CriteriaExtension( const std::string& Extension )
        {
            m_search_criteria |= CRITERIA_BIT_EXTENSION;
            m_extension = Extension;
        }

        inline void
        SDGTx::QueryParams::CriteriaInterval( time_type Start, time_type Stop )
        {
            m_search_criteria |= ( CRITERIA_BIT_START | CRITERIA_BIT_STOP );
            m_start = Start;
            m_stop = Stop;
        }

        inline void
        SDGTx::QueryParams::CriteriaSite( const std::string& Site )
        {
            m_search_criteria |= CRITERIA_BIT_SITE;
            m_site = Site;
        }

        inline const std::string&
        SDGTx::QueryParams::Description( ) const
        {
            return m_description;
        }

        inline const std::string&
        SDGTx::QueryParams::Extension( ) const
        {
            return m_extension;
        }

        inline const std::string&
        SDGTx::QueryParams::Site( ) const
        {
            return m_site;
        }

        inline SDGTx::QueryParams::time_type
        SDGTx::QueryParams::Start( ) const
        {
            return m_start;
        }

        inline SDGTx::QueryParams::time_type
        SDGTx::QueryParams::Stop( ) const
        {
            return m_stop;
        }

        inline bool
        SDGTx::QueryParams::Wildcard( const std::string& Data ) const
        {
            return ( Data.compare( WILDCARD_PATTERN ) == 0 );
        }

        //===================================================================
        //===================================================================

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline const SDGTx::file_extension_container_type&
        SDGTx::FileExtList( )
        {
            return s_extensions;
        }

        //===================================================================
        //===================================================================

        Streams::OInterface&
        SDGTxAsciiWriter( Streams::OASCII&                       Stream,
                          const RegistrySingleton::indexing_type Data );

        Streams::IInterface& SDGTxBinaryReader( Streams::IBinary& Stream );

        Streams::OInterface&
        SDGTxBinaryWriter( Streams::OBinary&                      Stream,
                           const RegistrySingleton::indexing_type Data );
    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__SDGTX_HH */
