//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <algorithm>
#include <memory>
#include <string>
#include <sstream>

#include <fstream>

#include <boost/regex.hpp>

#include "genericAPI/Logging.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/DirectoryManager.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/QueryParams.hh"
#include "diskcacheAPI/Cache/RegistrySingleton.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

using LDASTools::AL::unordered_map;

using GenericAPI::queueLogEntry;

#define VERBOSE_ERROR 1

#define SDGT_PATTERN "^([^\\-]+)\\-([^\\-]+)\\-([[:digit:]]+)\\-([[:digit:]]+)("
#define SDGT_EXT_SUFFIX ")$"

#define SDGT_PATTERN_DEFAULT SDGT_PATTERN "[.]gwf" SDGT_EXT_SUFFIX

static const char* sdgt_pattern =
    "^([^\\-]+)\\-([^\\-]+)\\-([[:digit:]]+)\\-([[:digit:]]+)(";
static const char* sdgt_ext_suffix = ")$";

//-----------------------------------------------------------------------
/// \brief Regular expression used for testing the pattern
///
/// \todo
///     Use of this regular expression needs to be protected by
///     locks.
/// \todo
///     The Regex class needs to be extened to support a reset option
///     so the regular expression may be modified if the list of
///     file extensions is modified.
//-----------------------------------------------------------------------
static boost::regex SDGTx_expression( SDGT_PATTERN_DEFAULT,
                                      boost::regex::extended );

enum
{
    SDGTx_S_OFFSET = 1,
    SDGTx_D_OFFSET,
    SDGTx_G_OFFSET,
    SDGTx_T_OFFSET,
    SDGTx_x_OFFSET,
    // Represents the number of expected matches.
    SDGTx_MATCH_SIZE
};

namespace
{
    using diskCache::Cache::Registry;
    using diskCache::Cache::SDGTx;
    typedef diskCache::Cache::Directory            Directory;
    typedef diskCache::Cache::DirectoryScanData    DirectoryScanData;
    typedef diskCache::Cache::SDGTx::DirectoryInfo DirectoryInfo;

    const char* SEPERATOR( );

    class Info : public Registry::Info
    {
    public:
        Info( const std::string& Name );

        virtual Info* Clone( ) const;

    protected:
        Info( const Info& Source );
    };

    enum gap_compare_type
    {
        GAP_OVERLAP_HEAD = -2,
        GAP_LESS_THAN = -1,
        GAP_MIDDLE = 0,
        GAP_GREATER_THAN = 1,
        GAP_OVERLAP_TAIL = 2,
        GAP_INTERVAL_MIDDLE = 10
    };

    typedef DirectoryInfo::time_type time_type;

    inline gap_compare_type
    gap( const time_type IntervalStart,
         const time_type IntervalEnd,
         const time_type Duration,
         time_type       GapStart,
         time_type       GapEnd )
    {
#if VERBOSE_ERROR
        static const char* caller = "anonymous::gap";
#endif /* VERBOSE_ERROR */

#if VERBOSE_ERROR
        QUEUE_LOG_MESSAGE( "IntervalStart: " << IntervalStart
                                             << " IntervalEnd: " << IntervalEnd
                                             << " Duration: " << Duration
                                             << " GapStart: " << GapStart
                                             << " GapEnd: " << GapEnd,
                           MT_DEBUG,
                           50,
                           caller,
                           "CXX" );
#endif /* VERBOSE_ERROR */
        gap_compare_type retval;

        time_type offset;

        if ( IntervalStart >= GapEnd )
        {
            retval = GAP_LESS_THAN;
        }
        else if ( ( GapStart < IntervalStart ) && ( GapEnd <= IntervalEnd ) &&
                  ( GapEnd > IntervalStart ) )
        {
            // NOTE: GapEnd > IntervalStart
            // Overlapping head
            GapStart = IntervalStart;

            offset = ( IntervalEnd - GapEnd ) % Duration;
            if ( offset )
            {
                GapEnd += Duration - offset;
            }
            retval = GAP_OVERLAP_HEAD;
        }
        else if ( ( GapStart >= IntervalStart ) && ( GapEnd <= IntervalEnd ) )
        {
            // GAP is in the middle
            offset = ( GapStart - IntervalStart ) % Duration;
            GapStart = GapStart - offset;
            offset = ( IntervalEnd - GapEnd ) % Duration;
            if ( offset )
            {
                GapEnd += Duration - offset;
            }
            retval = GAP_MIDDLE;
        }
        else if ( ( GapStart >= IntervalStart ) && ( GapEnd > IntervalEnd ) &&
                  ( GapStart < IntervalEnd ) )
        {
            // Overlapping tail
            offset = ( GapStart - IntervalStart ) % Duration;
            GapStart = GapStart - offset;
            GapEnd = IntervalEnd;
            retval = GAP_OVERLAP_TAIL;
        }
        else if ( GapStart >= IntervalEnd )
        {
            retval = GAP_GREATER_THAN;
        }
        else if ( ( IntervalStart >= GapStart ) && ( IntervalEnd <= GapEnd ) )
        {
            retval = GAP_INTERVAL_MIDDLE;
        }
        else
        {
#if VERBOSE_ERROR
            QUEUE_LOG_MESSAGE(
                "IntervalStart: "
                    << IntervalStart << " IntervalEnd: " << IntervalEnd
                    << " Duration: " << Duration << " GapStart: " << GapStart
                    << " GapEnd: " << GapEnd << " gap: Unhandled pattern",
                MT_DEBUG,
                50,
                caller,
                "CXX" );
#endif /* VERBOSE_ERROR */
            throw std::runtime_error( "gap: Unhandled pattern" );
        }
#if VERBOSE_ERROR
        QUEUE_LOG_MESSAGE( "IntervalStart: " << IntervalStart
                                             << " IntervalEnd: " << IntervalEnd
                                             << " Duration: " << Duration
                                             << " GapStart: " << GapStart
                                             << " GapEnd: " << GapEnd
                                             << " retval: " << retval,
                           MT_DEBUG,
                           50,
                           caller,
                           "CXX" );
#endif /* VERBOSE_ERROR */
        return retval;
    }
    //---------------------------------------------------------------------
    /// \brief
    //---------------------------------------------------------------------
    struct ScanData : public DirectoryScanData::scan_data
    {
        typedef DirectoryInfo verified_file_cache_type;

        typedef DirectoryScanData::scan_data scan_data;

#if 0
    typedef scan_data::size_type 		size_type;
#endif /* 0 */

        typedef DirectoryInfo::extension_type extension_type;
        typedef DirectoryInfo::site_type      site_type;
        typedef DirectoryInfo::desc_type      desc_type;
        typedef DirectoryInfo::site_desc_type site_desc_type;
        typedef DirectoryInfo::time_type      time_type;
        typedef DirectoryInfo::dt_type        dt_type;

        typedef std::map< time_type, time_type >            interval_type;
        typedef unordered_map< dt_type, interval_type >     dt_cache_type;
        typedef unordered_map< desc_type, dt_cache_type >   desc_cache_type;
        typedef unordered_map< site_type, desc_cache_type > site_cache_type;
        typedef unordered_map< extension_type, site_cache_type >
            extension_cache_type;

        //-------------------------------------------------------------------
        /// \brief Constructor
        //-------------------------------------------------------------------
        ScanData( );

        virtual scan_data* Clone( ) const;

        //-------------------------------------------------------------------
        /// \brief Add a file to the cache of files to be validated
        ///
        /// \param[in] Extension
        ///     The filename extension to be added
        /// \param[in] Site
        ///	    The IFO information to be added
        /// \param[in] Description
        ///     The description information to be added
        /// \param[in] Start
        ///     The start time to be added
        /// \param[in] Duration
        ///     The delta time to be added
        ///
        //-------------------------------------------------------------------
        void AddNewFile( const extension_type& Extension,
                         const site_type&      Site,
                         const desc_type&      Description,
                         const time_type       Start,
                         const dt_type         Duration );

        //-------------------------------------------------------------------
        /// \brief Add a file to the cache of known files
        ///
        /// \param[in] Extension
        ///     The filename extension to be added
        /// \param[in] Site
        ///	    The IFO information to be added
        /// \param[in] Description
        ///     The description information to be added
        /// \param[in] Start
        ///     The start time to be added
        /// \param[in] Duration
        ///     The delta time to be added
        ///
        //-------------------------------------------------------------------
        INT_4U AddOldFile( const extension_type& Extension,
                           const site_type&      Site,
                           const desc_type&      Description,
                           const time_type       Start,
                           const dt_type         Duration );

        //-------------------------------------------------------------------
        //
        //-------------------------------------------------------------------
        virtual size_type Count( ) const;

        //-------------------------------------------------------------------
        /// \brief Search if a file exists in the cache
        ///
        /// \param[in] Extension
        ///     The filename extension to be found
        /// \param[in] Site
        ///	    The IFO information to be found
        /// \param[in] Description
        ///     The description information to be found
        /// \param[in] Start
        ///     The start time to be found
        /// \param[in] Duration
        ///     The delta time to be found
        ///
        /// \return
        ///     If an entry is found matching all criteria, then the value
        ///     true is returned; false otherwise.
        //-------------------------------------------------------------------
        bool FindFile( const extension_type& Extension,
                       const site_type&      Site,
                       const desc_type&      Description,
                       const time_type       Start,
                       const dt_type         Duration ) const;

        //-------------------------------------------------------------------
        /// \brief locate a file under the same mount point
        //-------------------------------------------------------------------
        bool FindUnderMountPoint(
            const diskCache::Cache::DirectoryManager& DirManager,
            const std::string&                        Root,
            diskCache::Cache::SDGTx::OverlapHandler&  Overlaps,
            const extension_type&                     Extension,
            const site_type&                          Site,
            const desc_type&                          Description,
            const time_type                           Start,
            const dt_type                             Duration ) const;

        //-------------------------------------------------------------------
        /// \brief Detect overlap
        ///
        /// \param[in] Extension
        ///     The filename extension to be found
        /// \param[in] Site
        ///	    The IFO information to be found
        /// \param[in] Description
        ///     The description information to be found
        /// \param[in] Start
        ///     The start time to be found
        /// \param[in] Duration
        ///     The delta time to be found
        ///
        /// \return
        ///     Return true if the file with the given characteristics
        ///     overlaps existing data.
        //-------------------------------------------------------------------
        bool Overlap( diskCache::Cache::SDGTx::OverlapHandler& Overlaps,
                      const std::string&                       DirectoryName,
                      const extension_type&                    Extension,
                      const site_type&                         Site,
                      const desc_type&                         Description,
                      const time_type                          Start,
                      const dt_type                            Duration ) const;

        //-------------------------------------------------------------------
        /// \brief Return the local information about the directory
        ///
        /// \return
        ///     The local informatioin about the directory
        ///
        //-------------------------------------------------------------------
        virtual search_data_type
        SearchData( )
        {
            return s_search_data;
        }

        extension_cache_type      s_new_cache_data;
        verified_file_cache_type* s_verified_file_cache_data;

    private:
        search_data_type s_search_data;
    };

    //=====================================================================
    //
    //=====================================================================
    //---------------------------------------------------------------------
    // local data
    //---------------------------------------------------------------------
    inline std::string
    Filename( const DirectoryInfo::site_type&      Site,
              const DirectoryInfo::desc_type&      Description,
              const DirectoryInfo::time_type       Start,
              const DirectoryInfo::dt_type         Duration,
              const DirectoryInfo::extension_type& Extension )
    {
        std::ostringstream retval;

        retval << Site << ::SEPERATOR( ) << Description << ::SEPERATOR( )
               << Start << ::SEPERATOR( ) << Duration << Extension;

        return retval.str( );
    }

    inline const char*
    SEPERATOR( )
    {
        static const char* value = "-";

        return value;
    }
} // namespace

namespace diskCache
{
    namespace Cache
    {
        //===================================================================
        //
        //===================================================================

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        SDGTx::OverlapHandler::OverlapHandler(
            const std::string& SourceDirectory )
            : m_directory_name( SourceDirectory )
        {
        }

        //-------------------------------------------------------------------
        /// Within the destructor, the information that was gathered about
        ///   overlapping files is reported.
        //-------------------------------------------------------------------
        SDGTx::OverlapHandler::~OverlapHandler( )
        {
            static const char func_name[] =
                "diskCache::Cache::SDGTx::OverlapHandler::~OverlapHandler";

            for ( ext_type::const_iterator ext_cur = m_cache.begin( ),
                                           ext_last = m_cache.end( );
                  ext_cur != ext_last;
                  ++ext_cur )
            {
                for ( ifotype_type::const_iterator
                          ifotype_cur = ext_cur->second.begin( ),
                          ifotype_last = ext_cur->second.end( );
                      ifotype_cur != ifotype_last;
                      ++ifotype_cur )
                {
                    for ( conflicts_type::const_iterator
                              conflict_cur = ifotype_cur->second.begin( ),
                              conflict_last = ifotype_cur->second.end( );
                          conflict_cur != conflict_last;
                          ++conflict_cur )
                    {
                        for ( overlapping_type::const_iterator
                                  directory_cur =
                                      conflict_cur->second.s_overlaps.begin( ),
                                  directory_last =
                                      conflict_cur->second.s_overlaps.end( );
                              directory_cur != directory_last;
                              ++directory_cur )
                        {
                            for ( interval_type::const_iterator
                                      cur = directory_cur->second.begin( ),
                                      last = directory_cur->second.end( );
                                  cur != last;
                                  ++cur )
                            {
                                std::ostringstream msg;

                                msg << "CONFLICT: Time overlap is detected for "
                                       "frame interval"
                                    << " ([" << cur->first << ","
                                    << cur->second.first
                                    << "):" << cur->second.second
                                    << ") in directory " << m_directory_name
                                    << " and already existing frame interval "
                                    << " ([" << conflict_cur->first << ","
                                    << conflict_cur->second.s_end
                                    << "):" << conflict_cur->second.s_dt << ")"
                                    << " in directory " << directory_cur->first
                                    << " (IFO/Type: " << ifotype_cur->first
                                    << " Extension: " << ext_cur->first
                                    << " SAME MOUNT POINT)." << std::endl;
                                queueLogEntry(
                                    msg.str( ),
                                    GenericAPI::LogEntryGroup_type::MT_EMAIL,
                                    0,
                                    func_name,
                                    "SCAN_MOUNTPT" );
                            }
                        }
                    }
                }
            }
        }

        //-------------------------------------------------------------------
        /// Register the overlapping error.
        //-------------------------------------------------------------------
        void SDGTx::OverlapHandler::
    Register( const std::string& DirectoryName,
	      const std::string& Extension,
	      const std::string& IFOType,
	      time_type SourceStart, time_type SourceEnd,
	      time_type SourceDuration,
	      time_type ConflictStart, time_type ConflictEnd,
	      time_type ConflictDuration /* ,
	      const std::string& ConflictDirectoryName */ )
        {
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            range_type& rt( m_cache[ Extension ][ IFOType ][ ConflictStart ] );

            interval_type& interval( rt.s_overlaps[ DirectoryName ] );
            for ( interval_type::iterator cur = interval.begin( ),
                                          last = interval.end( );
                  cur != last;
                  ++cur )
            {
                if ( cur->second.second == SourceDuration )
                {
                    if ( cur->first == SourceEnd )
                    {
                        interval_type::iterator prev( cur );

                        if ( prev != interval.begin( ) )
                        {
                            --prev;
                            if ( prev->second.first == SourceStart )
                            {
                                //-------------------------------------------------------
                                // Fill in the gap
                                //-------------------------------------------------------
                                prev->second.first = cur->second.first;
                                return;
                            }
                        }

                        time_type end( cur->second.first );
                        interval.erase( cur );
                        interval[ SourceStart ] =
                            end_type( end, SourceDuration );
                        return;
                    }
                    else if ( cur->second.first == SourceStart )
                    {
                        interval_type::iterator next( cur );
                        ++next;
                        if ( ( next != last ) && ( next->first == SourceEnd ) )
                        {
                            // Fill the gap
                            cur->second.first = next->second.first;
                            interval.erase( next );
                        }
                        else
                        {
                            cur->second = end_type( SourceEnd, SourceDuration );
                        }
                        return;
                    }
                }
            }
            interval[ SourceStart ] = end_type( SourceEnd, SourceDuration );
            rt.s_end = ConflictEnd;
            rt.s_dt = ConflictDuration;
        }
        //===================================================================
        // SDGTx::DirectoryInfo::OverlappingFileError
        //===================================================================
        SDGTx::DirectoryInfo::OverlappingFileError::OverlappingFileError(
            const std::string& Filename )
            : FileCacheError( format( Filename ) )
        {
        }

        std::string
        SDGTx::DirectoryInfo::OverlappingFileError::format(
            const std::string& Filename )
        {
            //-----------------------------------------------------------------
            /// \todo
            ///     Need to add the directory name to the error message
            //-----------------------------------------------------------------
            std::ostringstream msg;

            msg << "SDGTx: Overlapping file detected: " << Filename
                << std::endl;

            return msg.str( );
        }

        //===================================================================
        // SDGTx::DirectoryInfo
        //===================================================================
        SDGTx::DirectoryInfo::DirectoryInfo( )
        {
        }

        SDGTx::DirectoryInfo::DirectoryInfo( cache_container_type& Source )
        {
            m_cache.swap( Source );
        }

        void
        SDGTx::DirectoryInfo::AddFile( const extension_type& Extension,
                                       const site_type&      Site,
                                       const desc_type&      Description,
                                       const time_type       Start,
                                       const dt_type         Duration )
        {
            time_type         end_time( Start + Duration );
            const std::string site_desc( Site + ::SEPERATOR( ) + Description +
                                         ::SEPERATOR( ) );

            interval_container_type& intervals(
                m_cache[ Extension ][ site_desc ] );

            for ( interval_container_type::iterator cur = intervals.begin( ),
                                                    last = intervals.end( ),
                                                    prev = intervals.end( );
                  cur != last;
                  ++cur )
            {
                if ( cur->second.second == Duration )
                {
                    if ( ( Start >= cur->first ) &&
                         ( end_time <= cur->second.first ) )
                    {
                        if ( ( Start - cur->first ) % Duration )
                        {
                            //---------------------------------------------------------
                            // The new file cannot be added because it isn't
                            // aligned properly.
                            //---------------------------------------------------------
                            throw OverlappingFileError( Filename( Site,
                                                                  Description,
                                                                  Start,
                                                                  Duration,
                                                                  Extension ) );
                        }
                        else
                        {
                            //---------------------------------------------------------
                            // The entry is already in the list so there is
                            // nothing to do. Logically, this case should never
                            // happen.
                            //---------------------------------------------------------
                            return;
                        }
                    }
                    if ( Start == cur->second.first )
                    {
                        //-----------------------------------------------------------
                        // Check to see if the file fills a gap.
                        // NOTE:
                        //   In this instance prev is being used to reference
                        //   the entry FOLLOWING cur.
                        //-----------------------------------------------------------
                        prev = cur;
                        ++prev;
                        if ( ( prev != intervals.end( ) ) &&
                             ( prev->first == end_time ) &&
                             ( prev->second.second == cur->second.second ) )
                        {
                            //---------------------------------------------------------
                            // Fill in the gap
                            //---------------------------------------------------------
                            cur->second = prev->second;
                            intervals.erase( prev );
                            return;
                        }
                        else
                        {
                            //---------------------------------------------------------
                            // Extend current
                            //---------------------------------------------------------
                            cur->second.first = end_time;
                            return;
                        }
                    }
                    if ( ( end_time == cur->first ) &&
                         ( Duration == cur->second.second ) )
                    {
                        //-----------------------------------------------------------
                        // Check to see if the file actually fills in a gap
                        // between two intervals.
                        //-----------------------------------------------------------
                        if ( ( prev != intervals.end( ) ) &&
                             ( Start == prev->second.first ) &&
                             ( cur->second.second == prev->second.second ) )
                        {
                            //---------------------------------------------------------
                            // The file fills the gap between the previous and
                            // current
                            //---------------------------------------------------------
                            prev->second = cur->second;
                            intervals.erase( cur );
                            return;
                        }
                        else
                        {
                            //---------------------------------------------------------
                            // This is the case where the file extends the front
                            //---------------------------------------------------------
                            end_time = cur->second.first;
                            intervals.erase( cur );
                            intervals[ Start ] =
                                duration_type( end_time, Duration );
                            return;
                        }
                    }
                    if ( end_time < cur->first )
                    {
                        //-----------------------------------------------------------
                        // This is the case where the entry needs to be added
                        //-----------------------------------------------------------
                        break;
                    }
                    //-------------------------------------------------------------
                    // Record the previous interval of the appropriate delta
                    // time.
                    //-------------------------------------------------------------
                    prev = cur;
                } // if - Duration
            } // for
            //-----------------------------------------------------------------
            // There was no suitable place found for the entry.
            // Create a new one.
            //-----------------------------------------------------------------
            intervals[ Start ] = duration_type( end_time, Duration );
        } // method - AddFile

        SDGTx::DirectoryInfo::size_type
        SDGTx::DirectoryInfo::Count( ) const
        {
            size_type retval( 0 );

            for ( extension_container_type::const_iterator
                      ext_cur = m_cache.begin( ),
                      ext_last = m_cache.end( );
                  ext_cur != ext_last;
                  ++ext_cur )
            {
                for ( site_desc_container_type::const_iterator
                          site_desc_cur = ext_cur->second.begin( ),
                          site_desc_last = ext_cur->second.end( );
                      site_desc_cur != site_desc_last;
                      ++site_desc_cur )
                {
                    for ( interval_container_type::const_iterator
                              interval_cur = site_desc_cur->second.begin( ),
                              interval_last = site_desc_cur->second.end( );
                          interval_cur != interval_last;
                          ++interval_cur )
                    {
                        retval += ( ( interval_cur->second.first -
                                      interval_cur->first ) /
                                    interval_cur->second.second );
                    }
                }
            }
            return retval;
        }

        void
        SDGTx::DirectoryInfo::Find( const Directory& Dir,
                                    QueryAnswer&     Answer ) const
        {
#if VERBOSE_ERROR
            static const char* caller = "SDGTx::DirectoryInfo::Find";
#endif /* VERBOSE_ERROR */

            QueryAnswer::index_data_type id( Answer.IndexData( ) );

            QueryParams* info( dynamic_cast< QueryParams* >( id.get( ) ) );

            const std::string& ext( info->Extension( ) );
            const std::string& site( info->Site( ) );
            const std::string& desc( info->Description( ) );

            const bool extw( info->Wildcard( ext ) );
            const bool sitew( info->Wildcard( site ) );
            const bool descw( info->Wildcard( desc ) );

            std::string site_desc;

            if ( sitew || descw )
            {
                throw std::runtime_error(
                    "Wildcards not supported for site/description" );
            }
            else
            {
                site_desc = site + ::SEPERATOR( ) + desc + ::SEPERATOR( );
            }

            if ( info != (QueryParams*)NULL )
            {
                //---------------------------------------------------------------
                // Look for EXTENSION data
                //---------------------------------------------------------------
                cache_container_type::const_iterator ext_cur, ext_last;

                if ( extw )
                {
                    ext_cur = m_cache.begin( );
                    ext_last = m_cache.end( );
                }
                else
                {
                    ext_last = ext_cur = m_cache.find( ext );
                    if ( ext_last != m_cache.end( ) )
                    {
                        ++ext_last;
                    }
                }
                while ( ext_cur != ext_last )
                {
                    //-------------------------------------------------------------
                    // Look for SITE/DESCRIPTION information
                    //-------------------------------------------------------------
                    site_desc_container_type::const_iterator site_desc_cur,
                        site_desc_last;

                    if ( site_desc.size( ) <= 0 )
                    {
                        site_desc_cur = ext_cur->second.begin( );
                        site_desc_last = ext_cur->second.end( );
                    }
                    else
                    {
                        site_desc_last = site_desc_cur =
                            ext_cur->second.find( site_desc );
                        if ( site_desc_last != ext_cur->second.end( ) )
                        {
                            ++site_desc_last;
                        }
                    }
                    while ( site_desc_cur != site_desc_last )
                    {
                        //-----------------------------------------------------------
                        // Look for SITE/DESCRIPTION information
                        //-----------------------------------------------------------
                        if ( site_desc.size( ) > 0 )
                        {
                            if ( site_desc.compare( site_desc_cur->first ) !=
                                 0 )
                            {
                                continue;
                            }
                        }
                        else
                        {
                            //---------------------------------------------------------
                            /// \todo Do a regular expression comparison
                            //---------------------------------------------------------
                        }
                        //-----------------------------------------------------------
                        // Look for START/STOP overlaps
                        //-----------------------------------------------------------
                        interval_container_type::const_iterator
                            interval_cur = site_desc_cur->second.begin( ),
                            interval_last = site_desc_cur->second.end( );
                        QueryParams::gap_container_type::iterator gap_cur(
                            info->m_gaps.begin( ) ),
                            gap_last( info->m_gaps.end( ) );

                        while ( ( interval_cur != interval_last ) &&
                                ( gap_cur != gap_last ) )
                        {
                            time_type duration( interval_cur->second.second ),
                                gs( gap_cur->first ), ge( gap_cur->second );

                            const gap_compare_type gap_class(
                                gap( interval_cur->first,
                                     interval_cur->second.first,
                                     duration,
                                     gs,
                                     ge ) );
                            switch ( gap_class )
                            {
                            case GAP_GREATER_THAN:
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE( "GAP_GREATER_THAN",
                                                   MT_DEBUG,
                                                   50,
                                                   caller,
                                                   "CXX" );
#endif /* VERBOSE_ERROR */
                                ++interval_cur;
                                continue;
                                break;
                            case GAP_LESS_THAN:
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE( "GAP_LESS_THAN",
                                                   MT_DEBUG,
                                                   50,
                                                   caller,
                                                   "CXX" );
#endif /* VERBOSE_ERROR */
                                ++gap_cur;
                                continue;
                                break;
                            case GAP_OVERLAP_HEAD:
                            {
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE( "GAP_OVERLAP_HEAD",
                                                   MT_DEBUG,
                                                   50,
                                                   caller,
                                                   "CXX" );
#endif /* VERBOSE_ERROR */
                                time_type dt = interval_cur->second.second;

                                // Simple case where the end of the gap
                                //   needs to be adjusted since data has
                                //   been found
                                gap_cur->second = interval_cur->first;
                                gs = interval_cur->first;
                                ge += ( ( dt -
                                          ( ge - interval_cur->first ) % dt ) %
                                        dt );
                                ++gap_cur;
                            }
                            break;
                            case GAP_OVERLAP_TAIL:
                            {
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE( "GAP_OVERLAP_TAIL",
                                                   MT_DEBUG,
                                                   50,
                                                   caller,
                                                   "CXX" );
#endif /* VERBOSE_ERROR */
                                // Harder case. Need to delete the current
                                // gap and create a new one.
                                time_type nge( gap_cur->second );
                                time_type dt = interval_cur->second.second;

                                info->m_gaps.erase( gap_cur );
                                info->m_gaps[ interval_cur->second.first ] =
                                    nge;
                                nge = interval_cur->second.first;
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE( "nge: " << nge,
                                                   MT_DEBUG,
                                                   50,
                                                   caller,
                                                   "CXX" );
#endif /* VERBOSE_ERROR */

                                gap_cur = info->m_gaps.find( nge );
                                gs -= ( gs - interval_cur->first ) % dt;
                                ge = nge;
                                ++interval_cur;
                            }
                            break;
                            case GAP_MIDDLE:
                            {
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE(
                                    "GAP_MIDDLE", MT_DEBUG, 50, caller, "CXX" );
#endif /* VERBOSE_ERROR */
                                //-----------------------------------------------------
                                // When the gap is in the middle of an interval,
                                // then the gap is removed from the list of gaps
                                // being processed as it has been completely
                                // filled.
                                //-----------------------------------------------------
                                time_type dt = interval_cur->second.second;

                                QueryParams::gap_container_type::iterator n(
                                    gap_cur );
                                ++n;
                                time_type ngs( ( n == gap_last ) ? 0
                                                                 : n->first );

                                info->m_gaps.erase( gap_cur );
                                if ( ngs == 0 )
                                {
                                    gap_cur = gap_last;
                                }
                                else
                                {
                                    gap_cur = info->m_gaps.find( ngs );
                                }
                                gs -= ( gs - interval_cur->first ) % dt;
                                ge += ( ( dt -
                                          ( ge - interval_cur->first ) % dt ) %
                                        dt );
                            }
                            break;
                            case GAP_INTERVAL_MIDDLE:
                            {
#if VERBOSE_ERROR
                                QUEUE_LOG_MESSAGE( "GAP_INTERVAL_MIDDLE",
                                                   MT_DEBUG,
                                                   50,
                                                   caller,
                                                   "CXX" );
#endif /* VERBOSE_ERROR */
                                time_type nge( gap_cur->second );

                                gap_cur->second = interval_cur->first;
                                info->m_gaps[ interval_cur->second.first ] =
                                    nge;
                                gap_cur = info->m_gaps.find(
                                    interval_cur->second.first );
                                gap_last = info->m_gaps.end( );
                                gs = interval_cur->first;
                                ge = interval_cur->second.first;
                            }
                            break;
                            }
                            //---------------------------------------------------------
                            // Register the found files
                            //---------------------------------------------------------
#if VERBOSE_ERROR
                            QUEUE_LOG_MESSAGE(
                                "Register the found files:"
                                    << " ext_cur->first: " << ext_cur->first
                                    << " site_desc_cur->first: "
                                    << site_desc_cur->first << " gs: " << gs
                                    << " ge: " << ge
                                    << " duration: " << duration,
                                MT_DEBUG,
                                50,
                                caller,
                                "CXX" );
#endif /* VERBOSE_ERROR */
                            info->Fill( Dir.Fullname( ), // DirectoryName
                                        ext_cur->first,
                                        site_desc_cur->first,
                                        gs,
                                        ge,
                                        duration );
                        }
                        ++site_desc_cur;
                    }
                    ++ext_cur;
                }
            }
        }

        bool
        SDGTx::DirectoryInfo::FindFile( const extension_type& Extension,
                                        const site_type&      Site,
                                        const desc_type&      Description,
                                        const time_type       Start,
                                        const dt_type         Duration ) const
        {
            const time_type   end_time( Start + Duration );
            bool              retval( false );
            const std::string site_desc( Site + ::SEPERATOR( ) + Description +
                                         ::SEPERATOR( ) );

            cache_container_type::const_iterator ccp(
                m_cache.find( Extension ) );
            if ( ccp != m_cache.end( ) )
            {
                site_desc_container_type::const_iterator sdcp(
                    ccp->second.find( site_desc ) );
                if ( sdcp != ccp->second.end( ) )
                {
                    for ( interval_container_type::const_iterator
                              cur = sdcp->second.begin( ),
                              last = sdcp->second.end( );
                          cur != last;
                          ++cur )
                    {
                        if ( ( cur->second.second ==
                               Duration ) // Verify that the duration is correct
                             && ( Start >= cur->first ) // Verify the start time
                             && ( end_time <=
                                  cur->second.first ) ) // Verify the end time
                        {
                            if ( ( ( Start - cur->first ) % Duration ) == 0 )
                            {
                                //-------------------------------------------------------
                                // Found an interval which contains the
                                // requested interval
                                //-------------------------------------------------------
                                retval = true;
                                break;
                            }
                        }
                        else if ( cur->first >= end_time )
                        {
                            //---------------------------------------------------------
                            // No reason to look any further as the no other
                            // entries could possibly exist
                            //---------------------------------------------------------
                            break;
                        }
                    }
                }
            }

            return retval;
        }

        bool
        SDGTx::DirectoryInfo::Overlap( OverlapHandler&       Overlaps,
                                       const std::string&    DirectoryName,
                                       const extension_type& Extension,
                                       const site_type&      Site,
                                       const desc_type&      Description,
                                       const time_type       Start,
                                       const dt_type         Duration ) const
        {
            const time_type   end_time( Start + Duration );
            bool              retval( false );
            const std::string site_desc( Site + ::SEPERATOR( ) + Description +
                                         ::SEPERATOR( ) );

            cache_container_type::const_iterator ccp(
                m_cache.find( Extension ) );
            if ( ccp != m_cache.end( ) )
            {
                site_desc_container_type::const_iterator sdcp(
                    ccp->second.find( site_desc ) );
                if ( sdcp != ccp->second.end( ) )
                {
                    for ( interval_container_type::const_iterator
                              cur = sdcp->second.begin( ),
                              last = sdcp->second.end( );
                          cur != last;
                          ++cur )
                    {
                        if ( // overlapping start
                            ( ( Start <= cur->first ) &&
                              ( end_time > cur->first ) )
                            // Overlaping end
                            || ( ( Start < cur->second.first ) &&
                                 ( end_time >= cur->second.first ) )
                            // Already contained within the interval
                            || ( ( Start >= cur->first ) &&
                                 ( end_time <= cur->second.first ) ) )
                        {
                            Overlaps.Register( DirectoryName,
                                               Extension,
                                               site_desc,
                                               Start,
                                               end_time,
                                               Duration,
                                               cur->first,
                                               cur->second.first,
                                               cur->second.second );
                            retval = true;
                            break;
                        }
                    }
                }
            }

            return retval;
        }

        //===================================================================
        // SDGTx::ExtensionError
        //===================================================================
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        SDGTx::ExtensionError::ExtensionError( )
            : std::runtime_error( format( ) )
        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        std::string
        SDGTx::ExtensionError::format( )
        {
            std::string retval;

            retval = "Empty file extension list was specified.";

            return retval;
        }

        //===================================================================
        // SDGTx::QueryParams
        //===================================================================
        //-------------------------------------------------------------------
        /// \note
        /// <ul> This is a list of parameters that this search engine accepts
        ///   <li><b>extension</b>
        ///     This parameter accepts a string to describe the extension
        ///     to be sought.
        ///   </li>
        /// </ul>
        //-------------------------------------------------------------------
        const char* SDGTx::QueryParams::WILDCARD_PATTERN = "*";

        SDGTx::QueryParams::QueryParams( INT_4U Mask )
            : QueryAnswer::IndexDataBase( Mask ),
              m_search_criteria( CRITERIA_BIT_NONE ), m_min_time( ~0 ),
              m_max_time( 0 )
        {
        }

        SDGTx::QueryParams::QueryParams( const Cache::QueryParams& Params )
            : QueryAnswer::IndexDataBase( ~0 ),
              m_search_criteria( CRITERIA_BIT_NONE ), m_min_time( ~0 ),
              m_max_time( 0 )
        {
            //-----------------------------------------------------------------
            // Loop over the parameters and create an optimized version of
            // the query.
            //-----------------------------------------------------------------
            for ( Cache::QueryParams::container_type::const_iterator
                      cur = Params.Params( ).begin( ),
                      last = Params.Params( ).end( );
                  cur != last;
                  ++cur )
            {
                if ( cur->first.compare( "extension" ) == 0 )
                {
                    CriteriaExtension( cur->second );
                }
                else if ( cur->first.compare( "site" ) == 0 )
                {
                    CriteriaSite( cur->second );
                }
                else if ( cur->first.compare( "site-description" ) == 0 )
                {
                    size_t start, end;

                    start = 0;
                    end = cur->second.find_first_of( ::SEPERATOR( ) );
                    if ( end != std::string::npos )
                    {
                        m_site = cur->second.substr( 0, end );
                        m_search_criteria |= CRITERIA_BIT_SITE;
                        start = ++end;
                        end = cur->second.find_first_of( ::SEPERATOR( ), end );
                        if ( start != std::string::npos )
                        {
                            m_description =
                                cur->second.substr( start, ( end - start ) );
                            m_search_criteria |= CRITERIA_BIT_DESCRIPTION;
                        }
                    }
                }
                else if ( cur->first.compare( "description" ) == 0 )
                {
                    CriteriaDescription( cur->second );
                }
                else if ( cur->first.compare( "start" ) == 0 )
                {
                    std::istringstream value( cur->second );

                    m_search_criteria |= CRITERIA_BIT_START;
                    value >> m_start;
                }
                else if ( cur->first.compare( "stop" ) == 0 )
                {
                    std::istringstream value( cur->second );

                    m_search_criteria |= CRITERIA_BIT_STOP;
                    value >> m_stop;
                }
                else
                {
                    //-------------------------------------------------------------
                    // Need to report an error about unknown parameter being
                    // passed to the query
                    //-------------------------------------------------------------
                }
            }
            {
                static const int CRITERIA_START_STOP( CRITERIA_BIT_START |
                                                      CRITERIA_BIT_STOP );

                if ( ( m_search_criteria & CRITERIA_START_STOP ) ==
                     CRITERIA_START_STOP )
                {
                    m_gaps[ m_start ] = m_stop;
                }
            }
        }

        void
        SDGTx::QueryParams::Complete( INT_4U Mask )
        {
            typedef QueryAnswer::IndexDataBase IndexDataBase;

            IndexDataBase::m_gaps.erase( IndexDataBase::m_gaps.begin( ),
                                         IndexDataBase::m_gaps.end( ) );
            IndexDataBase::intervals.erase( IndexDataBase::intervals.begin( ),
                                            IndexDataBase::intervals.end( ) );

            IndexDataBase::m_filenames.erase(
                IndexDataBase::m_filenames.begin( ),
                IndexDataBase::m_filenames.end( ) );

            if ( Mask & IndexDataBase::GEN_FILENAMES )
            {
                for ( site_desc_container_type::const_iterator
                          site_desc_cur = m_files.begin( ),
                          site_desc_last = m_files.end( );
                      site_desc_cur != site_desc_last;
                      ++site_desc_cur )
                {
                    for ( ext_container_type::const_iterator
                              ext_cur = site_desc_cur->second.begin( ),
                              ext_last = site_desc_cur->second.end( );
                          ext_cur != ext_last;
                          ++ext_cur )
                    {
                        for ( start_container_type::const_iterator
                                  start_cur = ext_cur->second.begin( ),
                                  start_last = ext_cur->second.end( );
                              start_cur != start_last;
                              ++start_cur )
                        {
                            //-----------------------------------------------------------
                            // Calculate the filenames
                            //-----------------------------------------------------------
                            for ( time_type
                                      time_cur = start_cur->first,
                                      time_last = start_cur->second.s_stop,
                                      time_inc = start_cur->second.s_dt;
                                  time_cur < time_last;
                                  time_cur += time_inc )
                            {
                                std::ostringstream filename;

                                filename << start_cur->second.s_directory_name
                                         << "/" << site_desc_cur->first
                                         << time_cur << ::SEPERATOR( )
                                         << time_inc << ext_cur->first;
                                QueryAnswer::IndexDataBase::m_filenames
                                    .push_back( filename.str( ) );
                            }
                        }
                    }
                }
            }

            if ( m_gaps.size( ) )
            {
                if ( ( Start( ) < m_gaps.begin( )->first ) &&
                     ( Mask & IndexDataBase::GEN_INTERVALS ) )
                {
                    std::ostringstream i;

                    i << "[ " << Start( ) << ", " << m_gaps.begin( )->first
                      << " )";
                    QueryAnswer::IndexDataBase::intervals.push_back( i.str( ) );
                }

                time_type seg_start = 0;
                for ( gap_container_type::const_iterator cur = m_gaps.begin( ),
                                                         last = m_gaps.end( );
                      cur != last;
                      ++cur )
                {
                    if ( Mask & IndexDataBase::GEN_GAPS )
                    {
                        std::ostringstream g;

                        g << "[ " << cur->first << ", " << cur->second << " )";
                        QueryAnswer::IndexDataBase::m_gaps.push_back(
                            g.str( ) );
                    }
                    if ( Mask & IndexDataBase::GEN_INTERVALS )
                    {
                        if ( seg_start )
                        {
                            std::ostringstream i;

                            i << "[ " << seg_start << ", " << cur->first
                              << " )";
                            QueryAnswer::IndexDataBase::intervals.push_back(
                                i.str( ) );
                        }
                        seg_start = cur->second;
                    }
                }

                if ( ( m_gaps.rbegin( )->second < Stop( ) ) &&
                     ( Mask & IndexDataBase::GEN_INTERVALS ) )

                {
                    std::ostringstream i;

                    i << "[ " << m_gaps.rbegin( )->second << ", " << Stop( )
                      << " )";
                    QueryAnswer::IndexDataBase::intervals.push_back( i.str( ) );
                }
            }
            else
            {
                std::ostringstream i;

                i << "[ " << Start( ) << ", " << Stop( ) << " )";
                intervals.push_back( i.str( ) );
            }
        }

        void
        SDGTx::QueryParams::Fill( const std::string& DirectoryName,
                                  const std::string& Extension,
                                  const std::string& SiteDesc,
                                  time_type          Start,
                                  time_type          Stop,
                                  time_type          Duration )
        {
            //-----------------------------------------------------------------
            // Get reference to data.
            // If the data does not yet exist, create it.
            //-----------------------------------------------------------------
            interval_data_type& id( m_files[ SiteDesc ][ Extension ][ Start ] );
            id.s_stop = Stop;
            id.s_dt = Duration;
            id.s_directory_name = DirectoryName;
            if ( Start < m_min_time )
            {
                m_min_time = Start;
            }
            if ( Stop > m_max_time )
            {
                m_max_time = Stop;
            }
        }

        bool
        SDGTx::QueryParams::IsCompleted( ) const
        {
            return ( m_gaps.size( ) <= 0 );
        }

        void
        SDGTx::QueryParams::Resample( )
        {
            static const char* caller = "SDGTx::QueryParams::Resample";

            if ( m_gaps.size( ) == 0 )
            {
                QUEUE_LOG_MESSAGE( " Gap size zero:"
                                       << " Resample: m_start: " << m_start
                                       << " m_stop: " << m_stop,
                                   MT_DEBUG,
                                   40,
                                   caller,
                                   "SDGTx" );

                //---------------------------------------------------------------
                // Create a gap to get the frame file before
                //---------------------------------------------------------------
                m_gaps[ m_min_time - 1 ] = m_min_time;
                //---------------------------------------------------------------
                // Create a gap to get the frame file after
                //---------------------------------------------------------------
                m_gaps[ m_max_time ] = m_max_time + 1;
            }
            else
            {
                QUEUE_LOG_MESSAGE(
                    " File count: "
                        << QueryAnswer::IndexDataBase::m_filenames.size( )
                        << " Gap size NOT zero: (" << m_gaps.size( ) << ")"
                        << " Resample: m_start: " << m_start
                        << " m_stop: " << m_stop,
                    MT_DEBUG,
                    40,
                    caller,
                    "SDGTx" );
                ;
                time_type start, end;

                //---------------------------------------------------------------
                // Set the gap at the front
                //---------------------------------------------------------------
                if ( m_min_time > m_gaps.begin( )->first )
                {
                    start = end = m_gaps.begin( )->first;
                }
                else if ( m_gaps.begin( )->first == m_min_time )
                {
                    start = m_min_time;
                    end = m_gaps.begin( )->second;
                    m_gaps.erase( start );
                }
                else
                {
                    start = m_min_time;
                    end = m_min_time;
                }
                m_gaps[ --start ] = end;
                //---------------------------------------------------------------
                // Set the gap at the back
                //---------------------------------------------------------------
                if ( m_max_time > m_gaps.rbegin( )->second )
                {
                    start = end = m_max_time;
                    m_gaps[ start ] = ++end;
                }
                else
                {
                    ( m_gaps.rbegin( )->second )++;
                }
            }
        }

        //===================================================================
        // SDGTx
        //===================================================================
        const char*                SDGTx::AsciiId = "SDGTx";
        RegistrySingleton::id_type SDGTx::REGISTRY_ID =
            RegistrySingleton::ID_NULL;
        SDGTx::file_extension_type SDGTx::s_default_extension = ".gwf";
        SDGTx::file_extension_container_type SDGTx::s_extensions;

        bool
        SDGTx::Configure( const std::string& Variable,
                          const std::string& Value )
        {
            bool retval = false;

            if ( Variable.compare( "extension" ) == 0 )
            {
                std::string pattern( sdgt_pattern );

                //---------------------------------------------------------------
                // Loop over list of extensions
                //---------------------------------------------------------------
                size_t cur( 0 );
                size_t last( 0 );

                while ( true )
                {
                    last = Value.find_first_of( ":", cur );

                    std::string ext;
                    if ( last == std::string::npos )
                    {
                        ext = Value.substr( cur );
                    }
                    else
                    {
                        ext = Value.substr( cur, last - cur );
                    }
                    //-------------------------------------------------------------
                    // \todo Replace . (dot) with [.] to protect it in the
                    // regular exp.
                    //-------------------------------------------------------------
                    size_t dot_pos = ext.find_first_of( "." );

                    while ( dot_pos != std::string::npos )
                    {
                        ext.replace( dot_pos, 1, "[.]" );
                        //-----------------------------------------------------------
                        // See if there is another dot in the text.
                        // Two is added to the previous location because of the
                        // extra characters added during the replace.
                        //-----------------------------------------------------------
                        dot_pos = ext.find_first_of( ".", dot_pos + 2 );
                    }
                    //-------------------------------------------------------------
                    // Add the extension pattern to the regular expression
                    //-------------------------------------------------------------
                    if ( cur > 0 )
                    {
                        pattern += "|";
                    }
                    pattern += ext;
                    if ( last == std::string::npos )
                    {
                        break;
                    }
                    cur = last;
                }
                //---------------------------------------------------------------
                // Cap off the regular expression
                //---------------------------------------------------------------
                pattern += sdgt_ext_suffix;
                //---------------------------------------------------------------
                // Refactor the regular expression
                //---------------------------------------------------------------
                SDGTx_expression =
                    boost::regex( pattern, boost::regex::extended );
                retval = true;
            }
            return retval;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        SDGTx::FileExtList( const file_extension_container_type& Extensions,
                            const std::string&                   Default )
        {
            if ( Extensions.size( ) <= 0 )
            {
                throw ExtensionError( );
            }
            s_default_extension = Default;
            s_extensions = Extensions;
            std::string pattern( sdgt_pattern );
            for ( file_extension_container_type::const_iterator
                      first = Extensions.begin( ),
                      cur = Extensions.begin( ),
                      last = Extensions.end( );
                  cur != last;
                  ++cur )
            {
                std::string ext( *cur );
                //-------------------------------------------------------------
                // \todo Replace . (dot) with [.] to protect it in the regular
                // exp.
                //-------------------------------------------------------------
                size_t dot_pos = ext.find_first_of( "." );

                while ( dot_pos != std::string::npos )
                {
                    ext.replace( dot_pos, 1, "[.]" );
                    //-----------------------------------------------------------
                    // See if there is another dot in the text.
                    // Two is added to the previous location because of the
                    // extra characters added during the replace.
                    //-----------------------------------------------------------
                    dot_pos = ext.find_first_of( ".", dot_pos + 2 );
                }
                //-------------------------------------------------------------
                // Add the extension pattern to the regular expression
                //-------------------------------------------------------------
                if ( cur != first )
                {
                    pattern += "|";
                }
                pattern += ext;
            }
            pattern += sdgt_ext_suffix;
            //-----------------------------------------------------------------
            // Refactor the regular expression
            //-----------------------------------------------------------------
            SDGTx_expression = boost::regex( pattern, boost::regex::extended );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        void
        SDGTx::Find( const Streams::Streamable& Source,
                     const Directory&           Dir,
                     QueryAnswer&               Answer )
        {
            try
            {
                const DirectoryInfo& di(
                    dynamic_cast< const DirectoryInfo& >( Source ) );

                di.Find( Dir, Answer );
            }
            catch ( ... )
            {
                /// \todo Need to catch appropriate exceptions
            }
        }

        //-------------------------------------------------------------------
        /// Register the search engine so it can be used throughout the
        /// system.
        //-------------------------------------------------------------------
        RegistrySingleton::id_type
        SDGTx::RegisterSearchEngine( )
        {
            if ( REGISTRY_ID == RegistrySingleton::ID_NULL )
            {
                //---------------------------------------------------------------
                //
                //---------------------------------------------------------------
                ::Info info( AsciiId );

                REGISTRY_ID = RegistrySingleton::Register( info );
            }

            return REGISTRY_ID;
        }

        bool
        SDGTx::IsMatch( DirectoryScanData& Data )
        {
            static const char func_name[] = "SDGTx::IsMatch";

            bool          retval( false );
            boost::smatch filename_pieces;

            try
            {
                retval = boost::regex_search(
                    Data.s_filename, filename_pieces, SDGTx_expression );
            }
            catch ( ... )
            {
                retval = false;
            }
            if ( retval == false )
            {
                return retval;
            }
            //-----------------------------------------------------------------
            // Actually look at the different parts
            //-----------------------------------------------------------------
            if ( Data.s_searches_scan_data.find( REGISTRY_ID ) ==
                 Data.s_searches_scan_data.end( ) )
            {
                Data.s_searches_scan_data[ REGISTRY_ID ].reset(
                    new ScanData( ) );
            }

            DirectoryScanData::scan_data_type sdb =
                Data.s_searches_scan_data[ REGISTRY_ID ];
            ScanData* sd( reinterpret_cast< ScanData* >( sdb.get( ) ) );

            DirectoryInfo::extension_type ext(
                filename_pieces[ SDGTx_x_OFFSET ] );
            std::istringstream       g_str( filename_pieces[ SDGTx_G_OFFSET ] );
            std::istringstream       t_str( filename_pieces[ SDGTx_T_OFFSET ] );
            DirectoryInfo::site_type ifo( filename_pieces[ SDGTx_S_OFFSET ] );
            DirectoryInfo::desc_type description(
                filename_pieces[ SDGTx_D_OFFSET ] );
            DirectoryInfo::time_type start = 0;
            DirectoryInfo::dt_type   dt = 0;

            g_str >> start;
            t_str >> dt;

            bool is_new( true );

            DirectoryScanData::searches_data_type::const_iterator old_pos(
                Data.s_old_searches_data.find( REGISTRY_ID ) );

            try
            {
                if ( old_pos != Data.s_old_searches_data.end( ) )
                {
                    DirectoryInfo* di( reinterpret_cast< DirectoryInfo* >(
                        old_pos->second.get( ) ) );

                    if ( di->FindFile( ext, ifo, description, start, dt ) )
                    {
                        //-----------------------------------------------------------
                        // This file previously existed, so we can trust that it
                        // has been previously verified.
                        //-----------------------------------------------------------
                        sd->AddOldFile( ext, ifo, description, start, dt );
                        is_new = false;
                    }
                }
                if ( is_new )
                {
                    sd->AddNewFile( ext, ifo, description, start, dt );
                }
            }
            catch ( const DirectoryInfo::OverlappingFileError& Exception )
            {
                //---------------------------------------------------------------
                // Report the error in the log file and continue.
                //---------------------------------------------------------------
                queueLogEntry( Exception.what( ),
                               GenericAPI::LogEntryGroup_type::MT_ORANGE,
                               0,
                               func_name,
                               "SCAN_MOUNTPT" );
            }

            return retval;
        } // method - SDGTx::IsMatch

        //-------------------------------------------------------------------
        /// When finished reading from the directory, all files that have
        /// been identified as new need to go under further scrutinization
        /// to ensure they do not violate any of the rules.
        //-------------------------------------------------------------------
        void
        SDGTx::OnDirectoryClose( DirectoryScanData& Data )
        {
            //-----------------------------------------------------------------
            // Make sure that there is something to look at.
            //-----------------------------------------------------------------
            if ( Data.s_searches_scan_data.find( REGISTRY_ID ) ==
                 Data.s_searches_scan_data.end( ) )
            {
                //---------------------------------------------------------------
                // Nothing to do since nothing was found
                /// \todo
                ///     Need to look at the old data to see if files existed
                ///     in the previous version since they will need to be
                ///     removed here.
                //---------------------------------------------------------------
                return;
            }

            //-----------------------------------------------------------------
            // Get references
            //-----------------------------------------------------------------

            INT_4U added( 0 );
            INT_4U removed( 0 );

            DirectoryScanData::scan_data_type sdb =
                Data.s_searches_scan_data[ REGISTRY_ID ];
            ScanData* sd( reinterpret_cast< ScanData* >( sdb.get( ) ) );
            if ( sd == (ScanData*)NULL )
            {
                return;
            }

            OverlapHandler overlaps( Data.s_filename );

            for ( ScanData::extension_cache_type::const_iterator
                      ext_cur = sd->s_new_cache_data.begin( ),
                      ext_last = sd->s_new_cache_data.end( );
                  ext_cur != ext_last;
                  ++ext_cur )
            {
                for ( ScanData::site_cache_type::const_iterator
                          site_cur = ext_cur->second.begin( ),
                          site_last = ext_cur->second.end( );
                      site_cur != site_last;
                      ++site_cur )
                {
                    for ( ScanData::desc_cache_type::const_iterator
                              desc_cur = site_cur->second.begin( ),
                              desc_last = site_cur->second.end( );
                          desc_cur != desc_last;
                          ++desc_cur )
                    {
                        for ( ScanData::dt_cache_type::const_iterator
                                  dt_cur = desc_cur->second.begin( ),
                                  dt_last = desc_cur->second.end( );
                              dt_cur != dt_last;
                              ++dt_cur )
                        {
                            const ScanData::time_type dt( dt_cur->first );
                            for ( ScanData::interval_type::const_iterator
                                      interval_cur = dt_cur->second.begin( ),
                                      interval_last = dt_cur->second.end( );
                                  interval_cur != interval_last;
                                  ++interval_cur )
                            {
                                for ( ScanData::time_type
                                          start = interval_cur->first,
                                          last = interval_cur->second;
                                      start != last;
                                      start += dt )
                                {
                                    //-----------------------------------------------------
                                    // Ensure that the file does not overlap
                                    // data in the local directory.
                                    //-----------------------------------------------------
                                    if ( sd->Overlap( overlaps,
                                                      Data.s_filename,
                                                      ext_cur->first,
                                                      site_cur->first,
                                                      desc_cur->first,
                                                      start,
                                                      dt ) == true )
                                    {
                                        //---------------------------------------------------
                                        /// \todo
                                        /// Need to flag this data as
                                        /// overlapping with other data within
                                        /// this directory
                                        //---------------------------------------------------
                                        continue;
                                    }
                                    //-----------------------------------------------------
                                    /// \todo
                                    /// Verify that file is unique under the
                                    /// mount point
                                    //-----------------------------------------------------
                                    if ( sd->FindUnderMountPoint(
                                             Data.DirManager( ),
                                             Data.Root( ),
                                             overlaps,
                                             ext_cur->first,
                                             site_cur->first,
                                             desc_cur->first,
                                             start,
                                             dt ) == true )
                                    {
                                        continue;
                                    }
                                    //-----------------------------------------------------
                                    // Add to list of verified files since there
                                    // is no conflicts
                                    //-----------------------------------------------------
                                    added += sd->AddOldFile( ext_cur->first,
                                                             site_cur->first,
                                                             desc_cur->first,
                                                             start,
                                                             dt );
                                } // loop - start time - end time
                            } // loop - intervals
                        } // loop - dt_cache_type
                    } // loop - desc_cache_type
                } // loop - site_cache_type
            } // loop - extension_cache_type
            Data.s_results.Added( Data.s_filename, AsciiId, added );
            Data.s_results.Removed( Data.s_filename, AsciiId, removed );
        } // method - SDGTx::OnDirectoryClose

        void
        SDGTx::TranslateQuery( const Cache::QueryParams& Params,
                               Cache::QueryAnswer&       Answer )
        {
            Answer.IndexData( new SDGTx::QueryParams( Params ) );
        }
    } // namespace Cache
} // namespace diskCache

namespace
{
    Info::Info( const std::string& Name )
        : Registry::Info( Name,
                          diskCache::Cache::SDGTx::Find,
                          diskCache::Cache::SDGTx::IsMatch,
                          diskCache::Cache::SDGTx::OnDirectoryClose,
                          diskCache::Cache::SDGTx::TranslateQuery )
    {
    }

    Info::Info( const Info& Source ) : Registry::Info( Source )
    {
    }

    Info*
    Info::Clone( ) const
    {
        return new Info( *this );
    }

    //=====================================================================
    //=====================================================================

    ScanData::ScanData( )
        : s_verified_file_cache_data( (verified_file_cache_type*)NULL ),
          s_search_data( new verified_file_cache_type( ) )
    {
        s_verified_file_cache_data =
            dynamic_cast< verified_file_cache_type* >( s_search_data.get( ) );
    }

    ScanData::scan_data*
    ScanData::Clone( ) const
    {
        std::unique_ptr< ScanData > retval;

        return retval.release( );
    }

    ScanData::size_type
    ScanData::Count( ) const
    {
        return s_verified_file_cache_data->Count( );
    }

    //---------------------------------------------------------------------
    /// Add a file to the cache.
    //---------------------------------------------------------------------
    void
    ScanData::AddNewFile( const extension_type& Extension,
                          const site_type&      Site,
                          const desc_type&      Description,
                          const time_type       Start,
                          const dt_type         Duration )
    {
        const time_type end_time( Start + Duration );
        interval_type&  intervals =
            s_new_cache_data[ Extension ][ Site ][ Description ][ Duration ];

        interval_type::iterator pos( intervals.lower_bound( Start ) );

        if ( intervals.size( ) == 0 )
        {
            intervals[ Start ] = Start + Duration;
            return;
        }
        if ( pos != intervals.begin( ) )
        {
            interval_type::iterator pre( pos );
            --pre;

            if ( Start < pre->second )
            {
                pos = pre;
            }
        }
        if ( pos->second == Start )
        {
            //-----------------------------------------------------------------
            // Check to see if this can be used to fill a gap
            //-----------------------------------------------------------------
            interval_type::iterator next( pos );
            if ( next != intervals.end( ) )
            {
                ++next;
            }

            if ( ( pos != intervals.end( ) ) && ( next != intervals.end( ) ) &&
                 ( next->first == end_time ) )

            {
                //---------------------------------------------------------------
                // Fill the gap
                //---------------------------------------------------------------
                pos->second = next->second;
                intervals.erase( next );
            }
            else
            {
                //---------------------------------------------------------------
                // Extending current interval on the back end
                //---------------------------------------------------------------
                pos->second = end_time;
            }
        }
        else if ( pos->first == end_time )
        {
            //-----------------------------------------------------------------
            // Check to see if this can be used to fill a gap
            //-----------------------------------------------------------------
            interval_type::iterator prev( pos );
            if ( prev != intervals.begin( ) )
            {
                --prev;
            }

            if ( ( prev != pos ) && ( prev->second == Start ) )
            {
                //---------------------------------------------------------------
                // Filling a gap
                //---------------------------------------------------------------
                prev->second = pos->second; // Extend the node.
                intervals.erase( pos ); // Remove the unneeded node
            }
            else
            {
                //---------------------------------------------------------------
                // Extending current interval on the front end
                //---------------------------------------------------------------
                const time_type e(
                    pos->second ); // Preserve the previous end time
                intervals.erase( pos ); // Remove previous entry
                intervals[ Start ] = e; // Create the entry
            }
        }
        else if ( ( ( Start < pos->first ) && ( end_time > pos->first ) ) ||
                  ( ( Start < pos->second ) && ( end_time > pos->second ) ) )
        {
            //-----------------------------------------------------------------
            // The new file cannot be added because it isn't aligned
            // properly.
            //-----------------------------------------------------------------
            throw DirectoryInfo::OverlappingFileError(
                Filename( Site, Description, Start, Duration, Extension ) );
        }
        else
        {
            //-----------------------------------------------------------------
            // Create a completely new interval
            //-----------------------------------------------------------------
            intervals[ Start ] = end_time;
        }
    }

    //---------------------------------------------------------------------
    /// Add a file to the cache.
    //---------------------------------------------------------------------
    inline INT_4U
    ScanData::AddOldFile( const extension_type& Extension,
                          const site_type&      Site,
                          const desc_type&      Description,
                          const time_type       Start,
                          const dt_type         Duration )
    {
        s_verified_file_cache_data->AddFile(
            Extension, Site, Description, Start, Duration );
        return ( 1 );
    }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
    //---------------------------------------------------------------------
    /// \brief Search if a file exists in the cache
    ///
    /// \note
    ///     Used by unit tests
    //---------------------------------------------------------------------

    bool
    ScanData::FindFile( const extension_type& Extension,
                        const site_type&      Site,
                        const desc_type&      Description,
                        const time_type       Start,
                        const dt_type         Duration ) const
    {
        bool retval( false );

        extension_cache_type::const_iterator ecp =
            s_new_cache_data.find( Extension );
        if ( ecp != s_new_cache_data.end( ) )
        {
            site_cache_type::const_iterator scp = ecp->second.find( Site );
            if ( scp != ecp->second.end( ) )
            {
                desc_cache_type::const_iterator dcp =
                    scp->second.find( Description );
                if ( dcp != scp->second.end( ) )
                {
                    dt_cache_type::const_iterator dtcp =
                        dcp->second.find( Duration );
                    if ( dtcp != dcp->second.end( ) )
                    {

                        interval_type::const_iterator icp =
                            dtcp->second.lower_bound( Start );

                        if ( ( icp != dtcp->second.begin( ) ) &&
                             ( ( Start + Duration ) <= icp->first ) )
                        {
                            --icp;
                        }
                        if ( ( icp != dtcp->second.end( ) ) &&
                             ( Start >= icp->first ) &&
                             ( ( Start + Duration ) <= icp->second ) &&
                             ( ( Start - icp->first ) % Duration ) == 0 )
                        {
                            retval = true;
                        }
                    }
                }
            }
        }

        return retval;
    }
#pragma GCC diagnostic pop

    //---------------------------------------------------------------------
    ///
    //---------------------------------------------------------------------
    bool
    ScanData::FindUnderMountPoint(
        const diskCache::Cache::DirectoryManager& DirManager,
        const std::string&                        Root,
        diskCache::Cache::SDGTx::OverlapHandler&  Overlaps,
        const extension_type&                     Extension,
        const site_type&                          Site,
        const desc_type&                          Description,
        const time_type                           Start,
        const dt_type                             Duration ) const
    {
        static const char func_name[] =
            "SDGTx.cc::ScanData::FindUnderMountPoint";
        typedef diskCache::Cache::QueryAnswer::filename_container_type
            filename_container_type;

        bool retval = false;

        //-------------------------------------------------------------------
        // Setup the query to try and find any file under the current
        //   current mount point for which the above data would be
        //   conflicting
        //-------------------------------------------------------------------
        std::unique_ptr< SDGTx::QueryParams > query( new SDGTx::QueryParams );

        query->CriteriaExtension( Extension );
        query->CriteriaSite( Site );
        query->CriteriaDescription( Description );
        query->CriteriaInterval( Start, Start + Duration );

        //-------------------------------------------------------------------
        // Execute the query
        //-------------------------------------------------------------------
        diskCache::Cache::QueryAnswer answer;

        answer.IndexData( query.release( ) );
        DirManager.Find( Root, answer );

        answer.Complete( );

        //-------------------------------------------------------------------
        // \todo Check for files
        //-------------------------------------------------------------------
        filename_container_type conflicts;
        answer.Swap( conflicts );

        //-------------------------------------------------------------------
        // \todo Look for the conflicts
        //-------------------------------------------------------------------
        if ( conflicts.size( ) > 0 )
        {
            std::ostringstream msg;

            msg << "CONFLICT: Time overlap is detected for frame"
                << " ([" << Start << "," << ( Start + Duration )
                << "):" << Duration << ") in directory "
                << Overlaps.Directory( ) << " and already existing file"
                << ( ( conflicts.size( ) == 1 ) ? "" : "s" );

            for ( filename_container_type::const_iterator
                      cur = conflicts.begin( ),
                      last = conflicts.end( );
                  cur != last;
                  ++cur )
            {
                msg << *cur;
            }
            msg << std::endl;

            queueLogEntry( msg.str( ),
                           GenericAPI::LogEntryGroup_type::MT_EMAIL,
                           0,
                           func_name,
                           "SCAN_MOUNTPT" );

            retval = true;
        }
        //-------------------------------------------------------------------
        // All done
        //-------------------------------------------------------------------
        return retval;
    }

    //---------------------------------------------------------------------
    /// Add a file to the cache.
    //---------------------------------------------------------------------
    inline bool
    ScanData::Overlap( diskCache::Cache::SDGTx::OverlapHandler& Overlaps,
                       const std::string&                       DirectoryName,
                       const extension_type&                    Extension,
                       const site_type&                         Site,
                       const desc_type&                         Description,
                       const time_type                          Start,
                       const dt_type                            Duration ) const
    {
        return s_verified_file_cache_data->Overlap( Overlaps,
                                                    DirectoryName,
                                                    Extension,
                                                    Site,
                                                    Description,
                                                    Start,
                                                    Duration );
    }

} // namespace
