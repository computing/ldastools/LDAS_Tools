//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "diskcache_config.h"

#include <stdio.h>

#include <sys/stat.h>
#if HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif /* HAVE_SYS_PARAM_H */
#if HAVE_SYS_UCRED_H
#include <sys/ucred.h>
#endif /* HAVE_SYS_UCRED_H */
#if HAVE_SYS_MNTTAB_H
#include <sys/mnttab.h>
#if !defined( MOUNT_TABLE_FILE )
#define MOUNT_TABLE_FILE MNTTAB
#endif /* ! defined(MOUNT_TABLE_FILE) */
#endif /* HAVE_SYS_MNTTAB_H */
#if HAVE_SYS_MOUNT_H
#include <sys/mount.h>
#endif /* HAVE_SYS_MOUNT_H */

#if HAVE_MNTENT_H
#include <mntent.h>
#if !defined( MOUNT_TABLE_FILE )
#define MOUNT_TABLE_FILE "/etc/mtab"
#endif /* ! defined(MOUNT_TABLE_FILE) */
#endif /* HAVE_MNTENT_H */
#include "time.h"

#include <cstdlib>
#include <set>
#include <string>
#include <vector>

#include "ldastoolsal/System.hh"

#include "genericAPI/Daemon.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/Stat.hh"

#include "Devices.hh"

using GenericAPI::LStat;

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< diskCache::Cache::Devices > )

namespace
{
    typedef union
    {
        diskCache::Cache::Device::id_type full;
        struct stat_id_type
        {
            INT_4U dev_id;
            INT_4U inode_id;
        } stat;
    } stat_id_type;

    //
    // Code from: http://rosettacode.org/wiki/Find_common_directory_path#C.2B.2B
    //
    std::string
    longest_path( const std::vector< std::string >& dirs, char separator = '/' )
    {
        std::vector< std::string >::const_iterator vsi = dirs.begin( );

        int maxCharactersCommon = vsi->length( );

        std::string compareString = *vsi;

        for ( vsi = dirs.begin( ) + 1; vsi != dirs.end( ); vsi++ )
        {
            std::pair< std::string::const_iterator,
                       std::string::const_iterator >
                p = std::mismatch( compareString.begin( ),
                                   compareString.end( ),
                                   vsi->begin( ) );
            if ( ( p.first - compareString.begin( ) ) < maxCharactersCommon )
            {
                maxCharactersCommon = p.first - compareString.begin( );
            }
        }
        std::string::size_type found =
            compareString.rfind( separator, maxCharactersCommon );
        return compareString.substr( 0, found );
    }

    inline std::string
    longest_path( const std::string& D1, const std::string& D2 )
    {
        std::vector< std::string > v;

        v.push_back( D1 );
        v.push_back( D2 );

        return longest_path( v );
    }

#if 0
  bool is_subdir( const std::string& Parent, const std::string& Child )
  {
    bool	retval = true;

    std::vector< std::string >  paths;

    paths.push_back( Parent );
    paths.push_back( Child );

    std::string	common = longest_path( paths );

    retval = ( common.length( ) == Parent.length( ) );

    return retval;
  }
#endif /* 0 */

} // namespace

namespace diskCache
{
    namespace Cache
    {
        struct Devices::pds_type
        {
            typedef std::set< std::string > active_mounts_type;
#if HAVE_GETFSSTAT
            typedef std::vector< struct statfs > statfs_buf_type;
#endif /* HAVE_GETFSSTAT */

            active_mounts_type active_mounts;
#if HAVE_GETFSSTAT
            statfs_buf_type statfs_buf;
#endif /* HAVE_GETFSSTAT */

            void update( active_mounts_type& NewlyMounted );
        };

        void
        Devices::pds_type::update( active_mounts_type& Mounts )
        {
            static const char* caller = "diskCache::Common::Devices::pds_type";
            active_mounts_type active;

#if HAVE_GETFSSTAT
            {
                //---------------------------------------------------------------
                // Get active mount points without having to know which
                //   file contains the details.
                //---------------------------------------------------------------
                int fs_count = ::getfsstat( NULL, 0, MNT_NOWAIT );

                if ( fs_count <= 0 )
                {
                    //-------------------------------------------------------------
                    // Quietly ignore the error and just use what we have
                    //-------------------------------------------------------------
                    return;
                }
                if ( ( size_t )( fs_count ) > statfs_buf.size( ) )
                {
                    statfs_buf.resize( fs_count );
                }
                getfsstat( &statfs_buf[ 0 ],
                           fs_count * sizeof( statfs_buf_type::value_type ),
                           MNT_NOWAIT );

                for ( size_t x = 0, last = (size_t)fs_count; x < last; ++x )
                {
                    //-------------------------------------------------------------
                    // Loop over the currently mounted file systems
                    //-------------------------------------------------------------
                    active.insert( statfs_buf[ x ].f_mntonname );
                }
            }
#elif HAVE_GETMNTENT_R && WORKING
            {
                //---------------------------------------------------------------
                // Get active mount points in a thread safe manor
                //---------------------------------------------------------------
                FILE* mtab = fopen( MOUNT_TABLE_FILE, "r" );

                if ( mtab )
                {
                    struct mntent* m;
                    struct mntent  mbuf;
                    char           sbuf[ 4096 ];

                    while ( (
                        m = getmntent_r( mtab, &mbuf, sbuf, sizeof( sbuf ) ) ) )
                    {
                        if ( mbuf.mnt_dir != NULL )
                        {
                            active.insert( mbuf.mnt_dir );
                        }
                    }
                    endmntent( mtab );
                } // if ( mtab )
            }
#elif HAVE_GETMNTENT
            {
                //---------------------------------------------------------------
                // Get active mount points
                //---------------------------------------------------------------
#if HAVE_SETMNTENT
                FILE* mtab = setmntent( MOUNT_TABLE_FILE, "r" );

                if ( mtab )
                {
                    struct mntent* m;

                    while ( ( m = getmntent( mtab ) ) )
                    {
                        if ( m->mnt_dir != NULL )
                        {
                            active.insert( m->mnt_dir );
                        }
                    }
                    endmntent( mtab );
                } // if ( mtab )
#else /* HAVE_SETMNTENT */
                FILE* mtab = fopen( MOUNT_TABLE_FILE, "r" );

                if ( mtab )
                {
                    struct mnttab m;
                    int           retcode = 0;

                    while ( ( retcode = getmntent( mtab, &m ) ) )
                    {
                        if ( m.mnt_mountp != NULL )
                        {
                            active.insert( m.mnt_mountp );
                        }
                    }
                    if ( retcode > 0 )
                    {
                        QUEUE_LOG_MESSAGE(
                            "getmntent: FAIL: "
                                << LDASTools::System::ErrnoMessage( ),
                            MT_NOTE,
                            0,
                            caller,
                            "CXX" );
                    }
                    fclose( mtab );
                } // if ( mtab )
#endif /* HAVE_SETMNTENT */
            }
#endif /* HAVE_GETFSSTAT */

            //-----------------------------------------------------------------
            // Figure out which devices have been unmounted
            //-----------------------------------------------------------------
            active_mounts_type diff;

            std::set_difference( active_mounts.begin( ),
                                 active_mounts.end( ),
                                 active.begin( ),
                                 active.end( ),
                                 std::inserter( diff, diff.begin( ) ) );

            if ( diff.size( ) > 0 )
            {
                for ( active_mounts_type::const_iterator cur = diff.begin( ),
                                                         last = diff.end( );
                      cur != last;
                      ++cur )
                {
                    QUEUE_LOG_MESSAGE(
                        "Unmounted: " << *cur, MT_DEBUG, 50, caller, "CXX" );
                    ;
                }
            }

            //-----------------------------------------------------------------
            // Figure out which devices have newly mount
            //-----------------------------------------------------------------
            Mounts.clear( );
            std::set_difference( active.begin( ),
                                 active.end( ),
                                 active_mounts.begin( ),
                                 active_mounts.end( ),
                                 std::inserter( Mounts, Mounts.begin( ) ) );

            //-----------------------------------------------------------------
            // Update list of active mount points
            //-----------------------------------------------------------------
            active_mounts.swap( active );
        }

        Devices::Devices( )
            : LDASTools::AL::Task( "DevicesMountManager" ),
              private_data( new private_data_type::element_type( ) )
        {
            delete_on_completion( false );
            Start( );
        }

        Devices::~Devices( )
        {
        }

        void
        Devices::operator( )( )
        {
            while ( true )
            {
                static const struct timespec pause = { 5, 0 };
                update( );
                (void)nanosleep( &pause, (struct timespec*)NULL );
            }
        }

        Devices::element_type
        Devices::Find( id_type Id )
        {
            return Instance( ).find( Id );
        }

        Devices::element_type
        Devices::Find( const std::string& Path )
        {
            return Instance( ).find( Path );
        }

        Devices::element_type
        Devices::Find( const std::string& Path, id_type Id )
        {
            return Instance( ).find( Path, Id );
        }

        void
        Devices::ForEach( UnaryFunction& Func )
        {
            Instance( ).for_each( Func );
        }

        bool
        Devices::IsOffline( const std::string& Path )
        {
            return Instance( ).is_offline( Path );
        }

        Devices::task_type*
        Devices::Task( )
        {
            return this;
        }

        Devices::element_type
        Devices::add_no_lock( Devices::element_type Source )
        {
            devices[ Source->Id( ) ] = Source;
            path_mapping[ Source->Path( ) ] = Source;
            return Source;
        }

        Devices::element_type
        Devices::add_no_lock( const std::string& Path, id_type Id )
        {
            return find_no_lock( Id );
        }

        inline Devices::element_type
        Devices::find( id_type Id ) const
        {
            // static const char* caller = "diskCache::Cache::Devices::find";
            //-----------------------------------------------------------------
            // Check for and return an existing instance if one exists.
            //-----------------------------------------------------------------
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return find_no_lock( Id );
        }

        inline Devices::element_type
        Devices::find( const std::string& Path ) const
        {
            // static const char* caller = "diskCache::Cache::Devices::find";
            //-----------------------------------------------------------------
            // Check for and return an existing instance if one exists.
            //-----------------------------------------------------------------
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return find_no_lock( Path );
        }

        inline Devices::element_type
        Devices::find( const std::string& Path, id_type Id )
        {
            static const char* caller = "diskCache::Cache::Devices::find";
            element_type       retval;

            {
                //---------------------------------------------------------------
                // Check for and return an existing instance if one exists.
                //---------------------------------------------------------------
                LDASTools::AL::ReadWriteLock lock(
                    baton,
                    LDASTools::AL::ReadWriteLock::READ,
                    __FILE__,
                    __LINE__ );

                retval = find_no_lock( Id );
            }
            if ( !retval )
            {
                //---------------------------------------------------------------
                // Does not look like there exists an instance so create a
                // new instance
                //---------------------------------------------------------------
                LDASTools::AL::ReadWriteLock lock(
                    baton,
                    LDASTools::AL::ReadWriteLock::WRITE,
                    __FILE__,
                    __LINE__ );

                retval = find_no_lock( Path, Id );
            }
            else
            {
                QUEUE_LOG_MESSAGE( "Looking for longest: "
                                       << " retval->Path: " << retval->Path( )
                                       << " Path: " << Path,
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CXX" );
                std::string p = longest_path( retval->Path( ), Path );
                if ( p.length( ) < retval->Path( ).length( ) )
                {
                    QUEUE_LOG_MESSAGE( "Resetting path:"
                                           << " p: " << p,
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "CXX" );
                    // add_no_lock( p, retval->Id( ) );
                    QUEUE_LOG_MESSAGE( "New value:" << retval->Path( ),
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "CXX" );
                }
            }
            return retval;
        }

        inline Devices::element_type
        Devices::find_no_lock( const std::string& Path ) const
        {
            const char* caller = "diskCache::Cache::Devices::"
                                 "find_no_lock";

            element_type retval;
            std::string  normalized_path( Path );
#if 0
      char	rp_path[ PATH_MAX ];

      if ( realpath( Path.c_str( ), rp_path ) == rp_path )
      {
        normalized_path.assign( rp_path );
      }
#endif

            path_mapping_type::const_iterator pos =
                path_mapping.upper_bound( normalized_path );

            if ( pos != path_mapping.begin( ) )
            {
                --pos;
                QUEUE_LOG_MESSAGE( "Path lower_bound: " << pos->first
                                                        << " for: " << Path,
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CXX" );
                if ( ( pos != path_mapping.end( ) ) &&
                     ( normalized_path.compare(
                           0, pos->first.length( ), pos->first ) == 0 ) )
                {
                    QUEUE_LOG_MESSAGE(
                        "Returning device info for:" << pos->first,
                        MT_DEBUG,
                        30,
                        caller,
                        "CXX" );
                    retval = pos->second;
                }
            }

            QUEUE_LOG_MESSAGE(
                "Returning: " << retval, MT_DEBUG, 30, caller, "CXX" );
            return retval;
        }

        inline Devices::element_type
        Devices::find_no_lock( id_type Id ) const
        {
            element_type retval;

            container_type::const_iterator pos = devices.find( Id );
            if ( pos != devices.end( ) )
            {
                retval = pos->second;
            }

            return retval;
        }

        inline Devices::element_type
        Devices::find_no_lock( const std::string& Path, id_type Id )
        {
            static const char* caller =
                "diskCache::Cache::Devices::find_no_lock:";
            element_type retval;

            QUEUE_LOG_MESSAGE( "Enttry:"
                                   << " Path: " << Path << " Id: " << Id,
                               MT_DEBUG,
                               30,
                               caller,
                               "CXX" );
            retval = find_no_lock( Id );
            add_no_lock( retval );
            return retval;
        }

        inline void
        Devices::for_each( UnaryFunction& Func ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            for ( container_type::const_iterator cur = devices.begin( ),
                                                 last = devices.end( );
                  cur != last;
                  ++cur )
            {
                Func( cur->second );
            }
        }

        inline bool
        Devices::is_offline( const std::string& Path ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            bool retval = true;

            element_type e = find_no_lock( Path );

            if ( e )
            {
                retval = e->IsOffline( );
            }
            return retval;
        }

        void
        Devices::update( )
        {
            static const char* caller = "diskCache::Cache::Devices::update";
            LDASTools::AL::ReadWriteLock lock(
                baton,
                LDASTools::AL::ReadWriteLock::WRITE,
                __FILE__,
                __LINE__ );

            pds_type::active_mounts_type new_mounts;

            private_data->update( new_mounts );
            {
                stat_id_type id;
                struct stat  statbuf;

                for ( pds_type::active_mounts_type::const_iterator
                          cur = new_mounts.begin( ),
                          last = new_mounts.end( );
                      cur != last;
                      ++cur )
                {
                    int err = LStat( *cur, statbuf );

                    if ( err == 0 )
                    {
                        id.stat.dev_id = statbuf.st_dev;
                        id.stat.inode_id = 0;

                        path_mapping_type::iterator cur_pm =
                            path_mapping.find( *cur );
                        if ( cur_pm == path_mapping.end( ) )
                        {
                            //---------------------------------------------------------
                            // Create new entry
                            //---------------------------------------------------------
                            element_type e =
                                element_type( new element_type::element_type(
                                    *cur, id.full ) );
                            devices[ id.full ] = e;
                            path_mapping[ *cur ] = e;
                            QUEUE_LOG_MESSAGE(
                                "Created: " << *cur << " id: " << id.full,
                                MT_DEBUG,
                                50,
                                caller,
                                "CXX" );
                        }
                        else if ( cur_pm->second->Id( ) != id.full )
                        {
                            //---------------------------------------------------------
                            // Update existing entry
                            //---------------------------------------------------------
                            stat_id_type old_id;

                            old_id.full = cur_pm->second->Id( );
                            devices.erase( cur_pm->second->Id( ) );
                            cur_pm->second->Id( id.full );
                            devices[ id.full ] = cur_pm->second;
                            QUEUE_LOG_MESSAGE( "Modified: "
                                                   << *cur
                                                   << " old_id: " << old_id.full
                                                   << " to id: " << id.full,
                                               MT_DEBUG,
                                               50,
                                               caller,
                                               "CXX" );
                        }
                    }
                    QUEUE_LOG_MESSAGE( "Newly mounted: " << *cur,
                                       MT_DEBUG,
                                       50,
                                       caller,
                                       "CXX" );
                }
            }
        }

    } // namespace Cache
} // namespace diskCache
