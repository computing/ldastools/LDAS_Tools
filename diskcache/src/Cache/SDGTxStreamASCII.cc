//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <iostream>

#include <map>
#include <sstream>

#include "diskcacheAPI/Streams/ASCII.hh"

#include "diskcacheAPI/Cache/SDGTxStreamASCII.hh"

namespace
{
    struct interval_info
    {
        INT_4U      s_count;
        std::string s_pairs;

        inline interval_info( ) : s_count( 0 )
        {
        }
    };

    typedef std::map< INT_4U, interval_info > interval_output_container_type;
} // namespace

namespace diskCache
{
    namespace Cache
    {
        template <>
        void
        SDGTx::DirectoryInfo::Write( Streams::OASCII& Stream ) const
        {
            const Streams::Interface::version_type version( Stream.Version( ) );

            if ( ( version < Streams::Interface::VERSION_ONLINE_STATUS ) &&
                 ( Devices::IsOffline( Stream.Directory( ) ) ) )
            {
                return;
            }

            //-----------------------------------------------------------------
            // Loop over the extensions
            //-----------------------------------------------------------------
            extension_container_type::const_iterator extension_cur =
                                                         m_cache.begin( ),
                                                     extension_last =
                                                         m_cache.end( );

            if ( version < Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
            {
                //--------------------------------------------------------------
                // Only do the default extension
                //--------------------------------------------------------------
                extension_cur = m_cache.find( s_default_extension );
                if ( extension_cur != extension_last )
                {
                    //-------------------------------------------------------------
                    // Have found the default extension in the list.
                    // Reset the end to be the element following.
                    //-------------------------------------------------------------
                    extension_last = extension_cur;
                    ++extension_last;
                }
            }
            while ( extension_cur != extension_last )
            {
                //---------------------------------------------------------------
                // Loop over the site/description elements
                //---------------------------------------------------------------
                const extension_container_type::mapped_type& site_desc_pos(
                    extension_cur->second );

                for ( site_desc_container_type::const_iterator
                          site_desc_cur = site_desc_pos.begin( ),
                          site_desc_last = site_desc_pos.end( );
                      site_desc_cur != site_desc_last;
                      ++site_desc_cur )
                {
                    //-------------------------------------------------------------
                    // Break the site-desc- into seperate elements
                    //-------------------------------------------------------------
                    const size_t sdlen( site_desc_cur->first.length( ) );
                    const size_t splitter( site_desc_cur->first.find( '-' ) );
                    std::string  site(
                        site_desc_cur->first.substr( 0, splitter ) );
                    std::string desc( site_desc_cur->first.substr(
                        splitter + 1, sdlen - splitter - 2 ) );
                    //-------------------------------------------------------------
                    // Loop over interval data for the site/description
                    //-------------------------------------------------------------
                    const site_desc_container_type::mapped_type& interval_pos(
                        site_desc_cur->second );

                    interval_output_container_type interval_container;

                    for ( interval_container_type::const_iterator
                              interval_cur = interval_pos.begin( ),
                              interval_last = interval_pos.end( );
                          interval_cur != interval_last;
                          ++interval_cur )
                    {
                        interval_info& ii(
                            interval_container[ interval_cur->second.second ] );
                        std::ostringstream fmt;

                        if ( ii.s_count )
                        {
                            fmt << " ";
                        }
                        fmt << interval_cur->first << " "
                            << interval_cur->second.first;
                        ii.s_pairs += fmt.str( );
                        ii.s_count += ( ( interval_cur->second.first -
                                          interval_cur->first ) /
                                        interval_cur->second.second );
                    }

                    //-------------------------------------------------------------
                    // Generate the requested output
                    //-------------------------------------------------------------
                    for ( interval_output_container_type::const_iterator
                              ic_cur = interval_container.begin( ),
                              ic_last = interval_container.end( );
                          ic_cur != ic_last;
                          ++ic_cur )
                    {
                        Stream << Stream.Directory( ) << "," << site << ","
                               << desc;
                        if ( version >=
                             Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
                        {
                            Stream << "," << extension_cur->first;
                        }
                        Stream << ",1" // Need to understand the purpose of this
                                       // number
                               << "," << ic_cur->first // dt
                               << " " << Stream.DirectoryModifyTime( );
                        if ( version >=
                             Streams::Interface::VERSION_ONLINE_STATUS )
                        {
                            Stream
                                << ( Devices::IsOffline( Stream.Directory( ) )
                                         ? " OFFLINE"
                                         : " ONLINE" );
                        }
                        Stream << " " << ic_cur->second.s_count << " {"
                               << ic_cur->second.s_pairs << "}";
                        Stream.EndL( );
                    }
                }
                //---------------------------------------------------------------
                // Advance to the next extension
                //---------------------------------------------------------------
                ++extension_cur;
            }
        }

        void
        SDGTxDirectoryInfoToOASCII( Streams::OASCII&           Stream,
                                    const Streams::Streamable& Data )
        {
            try
            {
                const SDGTx::DirectoryInfo& data(
                    dynamic_cast< const SDGTx::DirectoryInfo& >( Data ) );

                data.Write( Stream );
            }
            catch ( const std::bad_cast& Exception )
            {
                //---------------------------------------------------------------
                // Case were an unknown data type was passed to this function
                //---------------------------------------------------------------
            }
        } // function - SDGTxDirectoryInfoToOASCII

    } // namespace Cache
} // namespace diskCache
