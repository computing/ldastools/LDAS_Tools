//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "Device.hh"
#include "Devices.hh"

#include <sys/stat.h>
#include <time.h>

#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/System.hh"
#include "ldastoolsal/SignalHandler.hh"

#include "genericAPI/Logging.hh"

using LDASTools::AL::GPSTime;
using LDASTools::AL::SignalHandler;

// static const SignalHandler::signal_type CANCEL_SIGNAL =
// SignalHandler::SIGNAL_TERMINATE;

namespace
{
    typedef union
    {
        diskCache::Cache::Device::id_type full;
        struct
        {
            INT_4U dev_id;
            INT_4U inode_id;
        } stat_id_type;
    } dev_id_type;
} // namespace

namespace diskCache
{
    namespace Cache
    {
        struct Device::private_type
        {
            dev_id_type id;
            std::string path;

            private_type( )
            {
            }

            private_type( const std::string& Path, Device::id_type Id )
                : path( Path )
            {
                id.full = Id;
            }
        };

        Device::Device( )
            : pdata( new p_type::element_type( ) ), state( ONLINE ),
              used( false )
        {
        }

        Device::Device( const std::string& Path, id_type Id )
            : pdata( new p_type::element_type( Path, Id ) ), state( ONLINE ),
              used( false )
        {
        }

        Device::Device( const Device& Source )
            : pdata( new p_type::element_type( *( Source.pdata.get( ) ) ) ),
              state( Source.state ), used( Source.used )
        {
        }

        Device::~Device( )
        {
            pdata.reset( (p_type::element_type*)NULL );
        }

        Device::id_type
        Device::Id( ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return pdata->id.full;
        }

        void
        Device::Id( Device::id_type Source )
        {
            LDASTools::AL::ReadWriteLock lock(
                baton,
                LDASTools::AL::ReadWriteLock::WRITE,
                __FILE__,
                __LINE__ );

            pdata->id.full = Source;
        }

        void
        Device::Offline( )
        {
            static const char* caller = "diskCache::Cache::Device::"
                                        "Offline";
            {
                LDASTools::AL::ReadWriteLock lock(
                    baton,
                    LDASTools::AL::ReadWriteLock::READ,
                    __FILE__,
                    __LINE__ );

                if ( state == OFFLINE )
                {
                    return;
                }
            }
            QUEUE_LOG_MESSAGE( "Setting device state to OFFLINE: "
                                   << Path( )
                                   << " at: " << GPSTime::NowGPSTime( ),
                               MT_NOTE,
                               0,
                               caller,
                               "FILESYSTEM_STATUS" );
            if ( IsAlive( ) == false )
            {
                {
                    QUEUE_LOG_MESSAGE( "Setting device state to OFFLINE: "
                                           << Path( )
                                           << " at: " << GPSTime::NowGPSTime( )
                                           << " before spawning",
                                       MT_NOTE,
                                       0,
                                       caller,
                                       "FILESYSTEM_STATUS" );
                    {
                        LDASTools::AL::ReadWriteLock lock(
                            baton,
                            LDASTools::AL::ReadWriteLock::WRITE,
                            __FILE__,
                            __LINE__ );

                        if ( state == OFFLINE )
                        {
                            return;
                        }
                        state = OFFLINE;
                    }
                    try
                    {
                        Spawn( );
                        QUEUE_LOG_MESSAGE( "Spawn completed",
                                           MT_NOTE,
                                           0,
                                           caller,
                                           "FILESYSTEM_STATUS" );
                    }
                    catch ( const std::exception& Error )
                    {
                        QUEUE_LOG_MESSAGE(
                            "Spawn failed: "
                                << Error.what( ) << " System: "
                                << LDASTools::System::ErrnoMessage( ),
                            MT_NOTE,
                            0,
                            caller,
                            "FILESYSTEM_STATUS" );
                        throw;
                    }
                }
            }
            else
            {
                QUEUE_LOG_MESSAGE(
                    "IsAlive: " << Path( ) << " at: " << GPSTime::NowGPSTime( ),
                    MT_NOTE,
                    0,
                    caller,
                    "FILESYSTEM_STATUS" );
            }
        }

        const std::string&
        Device::Path( ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return pdata->path;
        }

        void
        Device::action( )
        {
            static const char* caller = "diskCache::Cache::Device::action";
            std::string        dev_path = Path( );

            QUEUE_LOG_MESSAGE( "Action thread started for: "
                                   << dev_path
                                   << " at: " << GPSTime::NowGPSTime( ),
                               MT_NOTE,
                               0,
                               caller,
                               "FILESYSTEM_STATUS" );

            CancellationEnable( false );

            //-----------------------------------------------------------------
            // Setup to handle cancelation requests
            //-----------------------------------------------------------------
            struct stat statbuf;
            dev_id_type stat_id;

            while ( true )
            {
                //---------------------------------------------------------------
                // Do not return until it is known that the file system is
                //   ONLINE
                //---------------------------------------------------------------
                if ( ( stat( dev_path.c_str( ), &statbuf ) != 0 ) ||
                     ( statbuf.st_dev == 0 ) || ( statbuf.st_ino == 0 ) )
                {
                    //-------------------------------------------------------------
                    // Failure of the stat command indicates that the file
                    // system
                    //   is still offline
                    //-------------------------------------------------------------
                    QUEUE_LOG_MESSAGE( "Waiting on: "
                                           << dev_path << " stat failed "
                                           << " at: " << GPSTime::NowGPSTime( ),
                                       MT_DEBUG,
                                       50,
                                       caller,
                                       "FILESYSTEM_STATUS" );
                    goto action_error;
                }
                //---------------------------------------------------------------
                // Stat command completed successfully
                //---------------------------------------------------------------
                QUEUE_LOG_MESSAGE( "Waiting on: "
                                       << dev_path
                                       << " stat completed without error "
                                       << " at: " << GPSTime::NowGPSTime( ),
                                   MT_DEBUG,
                                   50,
                                   caller,
                                   "FILESYSTEM_STATUS" );
                //---------------------------------------------------------------
                // Look at the device id
                //---------------------------------------------------------------
                stat_id.stat_id_type.dev_id = statbuf.st_dev;
                stat_id.stat_id_type.inode_id = 0;
                if ( ( stat_id.full != pdata->id.full ) &&
                     ( Devices::Find( stat_id.full ) ) )
                {
                    //-------------------------------------------------------------
                    // The device id does not belong to the current device
                    //   which means it is a different device.
                    // It does belong to another mounted device which is
                    //   assumed to be its parent device.
                    //-------------------------------------------------------------
                    goto action_error;
                }
                break;
            action_error:
            {
                struct timespec sleep_duration;

                sleep_duration.tv_sec = 5;
                sleep_duration.tv_nsec = 0;
                nanosleep( &sleep_duration, NULL );
            }
            } // while
            {
                LDASTools::AL::ReadWriteLock lock(
                    baton,
                    LDASTools::AL::ReadWriteLock::WRITE,
                    __FILE__,
                    __LINE__ );

                state = ONLINE;
            }
            QUEUE_LOG_MESSAGE( "Back online: " << dev_path << " at: "
                                               << GPSTime::NowGPSTime( ),
                               MT_NOTE,
                               0,
                               caller,
                               "FILESYSTEM_STATUS" );
            CancellationEnable( false );
            QUEUE_LOG_MESSAGE( "action completed for: " << dev_path,
                               MT_NOTE,
                               0,
                               caller,
                               "FILESYSTEM_STATUS" );
            //-----------------------------------------------------------------
            // No one is waiting to collect this thread. Detach to prevent
            // resource leak.
            //-----------------------------------------------------------------
            Detach( );
        }

        void
        Device::path( const std::string& Source )
        {
            LDASTools::AL::ReadWriteLock lock(
                baton,
                LDASTools::AL::ReadWriteLock::WRITE,
                __FILE__,
                __LINE__ );

            pdata->path = Source;
        }
    } // namespace Cache
} // namespace diskCache
