#
# LDASTools diskcache - Tools for querying a collection of files on disk
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools diskcache is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools diskcache is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#------------------------------------------------------------------------

#========================================================================
# Convenience libraries
#========================================================================

set( HDR_DIR ${NAMESPACE}/Cache )
set( cachedir ${CMAKE_INSTALL_INCLUDEDIR}/${HDR_DIR} )
set( cache_HEADERS
	Device.hh
	Devices.hh
	Directory.hh
	DirectoryManager.hh
	ExcludedDirectories.hh
	ExcludedDirectoriesSingleton.hh
	ExcludedPattern.hh
	HotDirectory.hh
	Interface.hh
	QueryAnswer.hh
	QueryParams.hh
	Registry.hh
	RegistrySingleton.hh
	SDGTx.hh
	SDGTxStreamASCII.hh
	SDGTxStreamBinary.hh
	SearchInterface.hh
  CACHE INTERNAL ""
)

##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## liblocal_core.la - These are routines that have no dependencies
##    outside of ldas or the system.
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
set( libcache_la_SOURCES
	Device.cc
	Devices.cc
	Directory.cc
	DirectoryStreamASCII.cc
	DirectoryStreamBinary.cc
	DirectoryManager.cc
	DirectoryManagerStreamASCII.cc
	DirectoryManagerStreamBinary.cc
	ExcludedDirectories.cc
	ExcludedDirectoriesSingleton.cc
	ExcludedPattern.cc
	HotDirectory.cc
	QueryAnswer.cc
	QueryParams.cc
	Registry.cc
	RegistrySingleton.cc
	SDGTx.cc
	SDGTxStreamASCII.cc
	SDGTxStreamBinary.cc
  )

cx_link_headers( ${HDR_DIR} ${cache_HEADERS} )
cx_target_library(
  TARGET cache
  CONVENIENCE
  STATIC
  SOURCES ${libcache_la_SOURCES}
  INCLUDE_DIRECTORIES BEFORE PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_BINARY_DIR}/include
    ${CMAKE_BINARY_DIR}
    ${LDASTOOLS_INCLUDE_DIRS}
  )

install(
  FILES ${cache_HEADERS}
  DESTINATION "${cachedir}" )
