//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__CACHE__DEVICES_HH
#define DISKCACHE__CACHE__DEVICES_HH

#include <map>
#include <memory>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/unordered_map.hh"

#include "genericAPI/Daemon.hh"

#include "Device.hh"

namespace diskCache
{
    namespace Cache
    {
        class Devices : public LDASTools::AL::Singleton< Devices >,
                        protected GenericAPI::Daemon,
                        protected LDASTools::AL::Task
        {
        public:
            typedef Device::id_type             id_type;
            typedef boost::shared_ptr< Device > element_type;
            typedef LDASTools::AL::unordered_map< id_type, element_type >
                                                          container_type;
            typedef std::map< std::string, element_type > path_mapping_type;

            class UnaryFunction
            {
            public:
                typedef Devices::element_type element_type;

                virtual void operator( )( element_type Source ) = 0;
            };

            Devices( );

            ~Devices( );

            static element_type Find( id_type Id );

            static element_type Find( const std::string& Path );

            static element_type Find( const std::string& Path, id_type Id );

            static void ForEach( UnaryFunction& Func );

            static bool IsOffline( const std::string& Path );

        protected:
            virtual task_type* Task( );

            virtual void operator( )( );

        private:
            struct pds_type;
            typedef std::unique_ptr< pds_type >              private_data_type;
            mutable LDASTools::AL::ReadWriteLock::baton_type baton;
            container_type                                   devices;
            path_mapping_type                                path_mapping;
            private_data_type                                private_data;

            element_type add_no_lock( element_type Source );

            element_type add_no_lock( const std::string& Path, id_type Id );

            element_type find( id_type Id ) const;

            element_type find( const std::string& Path ) const;

            element_type find( const std::string& Path, id_type Id );

            element_type find_no_lock( const std::string& Path ) const;

            element_type find_no_lock( id_type Id ) const;

            element_type find_no_lock( const std::string& Path, id_type Id );

            void for_each( UnaryFunction& Func ) const;

            bool is_offline( const std::string& Path ) const;

            void update( );
        };

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE__CACHE__DEVICES_HH */
