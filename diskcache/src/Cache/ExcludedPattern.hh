//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__CACHE__EXCLUDED_PATTERN_HH
#define DISKCACHE__CACHE__EXCLUDED_PATTERN_HH

#include <list>
#include <string>

#include <boost/regex.hpp>

#include "ldastoolsal/Singleton.hh"

#include "genericAPI/Logging.hh"

#include "ExcludedDirectories.hh"

namespace diskCache
{
    namespace Cache
    {
        //-------------------------------------------------------------------
        /// \brief Manages set of filename exclusion patterns
        //-------------------------------------------------------------------
        class ExcludedPattern
            : public LDASTools::AL::Singleton< ExcludedPattern >
        {

        public:
            typedef std::list< std::string >           pattern_container_type;
            typedef pattern_container_type::value_type value_type;

            ExcludedPattern( );

            ~ExcludedPattern( );

            //-----------------------------------------------------------------
            /// \brief Retrieve collection of file name patterns to exclude
            //-----------------------------------------------------------------
            static const pattern_container_type& Get( );

            //-----------------------------------------------------------------
            /// \brief Check if the file should be excluded
            ///
            /// \param[in] Filename
            ///     Name of file to be checked for exclusion
            ///
            /// \return
            ///  If Filename is to be excluded, then return true;
            ///  false otherwise
            //-----------------------------------------------------------------
            static bool IsExcluded( const value_type& Filename );

            //-----------------------------------------------------------------
            /// \brief Change the matching patterns
            ///
            /// \param[in] Patterns
            ///     List of patterns that descibe the files which should be
            ///     excluded (ex: ".*[.]tmp")
            ///
            /// \return
            ///  If Filename is to be excluded, then return true;
            ///  false otherwise
            //-----------------------------------------------------------------
            static void Update( const pattern_container_type& Patterns );

        private:
            pattern_container_type readable_pattern;
            boost::regex           pattern;

            const pattern_container_type& get( ) const;

            bool is_excluded( const value_type& Filename ) const;

            void update( const pattern_container_type& Patterns );
        };

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE__CACHE__EXCLUDED_PATTERN_HH */
