//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <iterator>
#include <list>

#include <boost/shared_ptr.hpp>

#include "diskcacheAPI/Common/Logging.hh"

#include "diskcacheAPI/Cache/Directory.hh"

#include "diskcacheAPI/Streams/Binary.hh"

#include "diskcacheAPI/Cache/SDGTx.hh"

#if 0
#define DEBUG_DIRECTORY 1
#endif /* 0 */

namespace diskCache
{
    namespace Cache
    {
        using Streams::IBinary;
        using Streams::OBinary;

        //-------------------------------------------------------------------
        /// Initialize the memory contents from the stream.
        /// This reads the contents for a single directory.
        //-------------------------------------------------------------------
        template <>
        IBinary&
        Directory::Read( IBinary& Stream )
        {
#if DEBUG_DIRECTORY
            static char const* caller = "diskCache::Cache::Directory::Read";
#endif /* DEBUG_DIRECTORY */
            const Streams::Interface::version_type version( Stream.Version( ) );
            typedef Streams::IBinary::size_type    size_type;

            //-----------------------------------------------------------------
            /// \todo
            /// This can be optimized by reading the root followed by
            ///   reading the path relative to the root.
            /// The current implementation is waistful as the root portion
            /// is in both names.
            //-----------------------------------------------------------------
            typedef std::list< std::string > l_subdir_type;

            l_subdir_type subdirs;
            l_subdir_type subdirs_ignored;

#if DEBUG_DIRECTORY
            Stream >> m_name;
            QUEUE_LOG_MESSAGE( "fullname: " << m_name,

                               MT_DEBUG,
                               DEBUG_DIRECTORY,
                               caller,
                               "CXX" );
            Stream >> m_root;
            QUEUE_LOG_MESSAGE( "m_root: " << m_root,
                               MT_DEBUG,
                               DEBUG_DIRECTORY,
                               caller,
                               "CXX" );
            Stream >> subdirs;
            Stream >> subdirs_ignored;
            Stream >> m_last_time_modified;
            QUEUE_LOG_MESSAGE( "m_last_time_modified: " << m_last_time_modified,
                               MT_DEBUG,
                               DEBUG_DIRECTORY,
                               caller,
                               "CXX" );
#else /* DEBUG_DIRECTORY */
            Stream >> m_name >> m_root >> subdirs >> subdirs_ignored >>
                m_last_time_modified;
#endif /* DEBUG_DIRECTORY */
            //-----------------------------------------------------------------
            // Create the children
            //-----------------------------------------------------------------
            m_subdirs_ignored.clear( );

            if ( subdirs_ignored.size( ) > 0 )
            {
                std::insert_iterator< ignored_type > i(
                    m_subdirs_ignored, m_subdirs_ignored.begin( ) );
                m_subdirs_ignored.clear( );
                std::copy(
                    subdirs_ignored.begin( ), subdirs_ignored.end( ), i );
            }
            //-----------------------------------------------------------------
            // Create the children
            //-----------------------------------------------------------------
            m_subdirs.clear( );
            if ( subdirs.size( ) > 0 )
            {

                std::insert_iterator< children_type > i( m_subdirs,
                                                         m_subdirs.begin( ) );

                std::copy( subdirs.begin( ), subdirs.end( ), i );
            }
            //-----------------------------------------------------------------
            // Read the indexing nodes
            //-----------------------------------------------------------------
            size_type indexes_count( 1 );
            if ( version >= Streams::Interface::VERSION_GENERIC_INDEXES )
            {
                Stream >> indexes_count;
            }
            for ( size_type indexes_cur = 0; indexes_cur != indexes_count;
                  ++indexes_cur )
            {
                std::string index;

                if ( version >= Streams::Interface::VERSION_GENERIC_INDEXES )
                {
                    Stream >> index;
                }
                else
                {
                    index = SDGTx::AsciiId;
                }
                //----------------------------------------------------------------
                // Obtain the numeric value of the index and read the object
                // from the stream
                //----------------------------------------------------------------
                const RegistrySingleton::id_type id(
                    RegistrySingleton::Id( index ) );

                IBinary::read_func func( IBinary::Reader( id ) );

                IBinary::read_return_type v( ( *func )( Stream ) );

                if ( v )
                {
                    //-------------------------------------------------------------
                    // With an object ligitimately read, record it for future
                    // use.
                    //-------------------------------------------------------------
                    m_indexes[ id ] = v;
                }
            }
            return Stream;
        }

        //-------------------------------------------------------------------
        // Write the memory contents out to the stream.
        //-------------------------------------------------------------------
        template <>
        OBinary&
        Directory::Write( OBinary& Stream ) const
        {
#if DEBUG_DIRECTORY
            static char const* caller = "diskCache::Cache::Directory::Write";
#endif /* DEBUG_DIRECTORY */
            const Streams::Interface::version_type version( Stream.Version( ) );
            typedef Streams::OBinary::size_type    size_type;

            //-----------------------------------------------------------------
            /// \todo
            /// This can be optimized by writing the root followed by
            ///   Writing the path relative to the root.
            /// The current implementation is waistful as the root portion
            /// is in both names.
            //-----------------------------------------------------------------

#if DEBUG_DIRECTORY
            Stream << Fullname( );
            QUEUE_LOG_MESSAGE( "fullname: " << Fullname( ),
                               MT_DEBUG,
                               DEBUG_DIRECTORY,
                               caller,
                               "CXX" );
            Stream << Root( );
            QUEUE_LOG_MESSAGE( "Root( ) " << Root( ),
                               MT_DEBUG,
                               DEBUG_DIRECTORY,
                               caller,
                               "CXX" );
            Stream << m_subdirs;
            Stream << m_subdirs_ignored;
            Stream << m_last_time_modified;
            QUEUE_LOG_MESSAGE( "m_last_time_modified: " << m_last_time_modified,
                               MT_DEBUG,
                               DEBUG_DIRECTORY,
                               caller,
                               "CXX" );
#else /* DEBUG_DIRECTORY */
            Stream << Fullname( ) << Root( ) << m_subdirs << m_subdirs_ignored
                   << m_last_time_modified;
#endif /* DEBUG_DIRECTORY */
            //-----------------------------------------------------------------
            // Writing of the search specific data
            //-----------------------------------------------------------------
            index_container_type::const_iterator indexes_cur =
                                                     m_indexes.begin( ),
                                                 indexes_last =
                                                     m_indexes.end( );

            if ( version >= Streams::Interface::VERSION_GENERIC_INDEXES )
            {
                //---------------------------------------------------------------
                /// \todo
                /// Loop over all of the associated indexes and have them
                /// dump their information to the Stream
                //---------------------------------------------------------------
                size_type indexes_count( m_indexes.size( ) );

                Stream << indexes_count;
            }
            else
            {
                const RegistrySingleton::id_type id(
                    RegistrySingleton::Id( SDGTx::AsciiId ) );

                indexes_cur = m_indexes.find( id );
                if ( indexes_cur != indexes_last )
                {
                    indexes_last = indexes_cur;
                    ++indexes_last;
                }
                else
                {
                    OBinary::write_func func( OBinary::Writer( id ) );

                    SDGTx::DirectoryInfo data;

                    ( *func )( Stream, data );
                }
            }

            while ( indexes_cur != indexes_last )
            {
                try
                {
                    RegistrySingleton::info_type reg_info(
                        RegistrySingleton::GetInfo( indexes_cur->first ) );

                    OBinary::write_func func(
                        OBinary::Writer( indexes_cur->first ) );

                    if ( version >=
                         Streams::Interface::VERSION_GENERIC_INDEXES )
                    {
                        //-----------------------------------------------------------
                        // Write the label
                        //-----------------------------------------------------------
                        Stream << reg_info->m_key_name;
                    }
                    //-------------------------------------------------------------
                    // Write the output
                    //-------------------------------------------------------------
                    boost::shared_ptr< Streams::Streamable > data(
                        indexes_cur->second );
                    ( *func )( Stream, *data );
                }
                catch ( ... )
                {
                    //-------------------------------------------------------------
                    // Should only have a range error, either on the reg_info
                    // or for func.
                    // Either way, cannot write the information to the stream.
                    // Quietly absorb the condition and continue with the
                    // next entry.
                    //-------------------------------------------------------------
                }
                //---------------------------------------------------------------
                // Advance
                //---------------------------------------------------------------
                ++indexes_cur;
            }
            return Stream;
        } // method template<> Write( )

    } // namespace Cache

} // namespace diskCache
