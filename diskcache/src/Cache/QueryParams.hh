//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__QUERY_PARAMS_HH
#define DISKCACHE_API__CACHE__QUERY_PARAMS_HH

#include <map>
#include <stdexcept>
#include <string>

namespace diskCache
{
    namespace Cache
    {
        class Registry;
        class QueryAnswer;

        class QueryParams
        {
        public:
            class MissingVariableError : std::runtime_error
            {
            public:
                MissingVariableError( const std::string& Variable );

            private:
                static std::string format( const std::string& Variable );
            };

            typedef std::map< std::string, std::string > container_type;

            void AddParam( const std::string& Variable,
                           const std::string& Value );

            const container_type& Params( ) const;

            const std::string& Value( const std::string& Variable ) const;

            //-----------------------------------------------------------------
            /// \brief Optimize the query.
            //-----------------------------------------------------------------
            void Translate( QueryAnswer&    Answer,
                            const Registry& Indexes ) const;

        private:
            container_type m_symbol_table;
        }; // class - QueryParams

        inline void
        QueryParams::AddParam( const std::string& Variable,
                               const std::string& Value )
        {
            m_symbol_table[ Variable ] = Value;
        } // method - QueryParams::AddParam

        inline const QueryParams::container_type&
        QueryParams::Params( ) const
        {
            return m_symbol_table;
        } // method - QueryParams::Params

        inline const std::string&
        QueryParams::Value( const std::string& Variable ) const
        {
            container_type::const_iterator index_name_pos(
                m_symbol_table.find( Variable ) );

            if ( index_name_pos == m_symbol_table.end( ) )
            {
                throw MissingVariableError( Variable );
            }
            return index_name_pos->second;
        }

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__QUERY_PARAMS_HH */
