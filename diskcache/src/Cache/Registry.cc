//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include "diskcacheAPI/Cache/Registry.hh"
#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/QueryParams.hh"

namespace diskCache
{
    namespace Cache
    {
        const Registry::FindFunc Registry::Info::FIND_FUNC_NULL =
            (Registry::FindFunc)NULL;
        const Registry::IsMatchFunc Registry::Info::IS_MATCH_FUNC_NULL =
            (Registry::IsMatchFunc)NULL;
        const Registry::OnDirectoryCloseFunc
            Registry::Info::ON_DIRECTORY_CLOSE_FUNC_NULL =
                (Registry::OnDirectoryCloseFunc)NULL;
        const Registry::TranslateQueryFunc
            Registry::Info::TRANSLATE_QUERY_FUNC_NULL =
                (Registry::TranslateQueryFunc)NULL;

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Registry::Info::Info( const std::string&   Key,
                              FindFunc             Find,
                              IsMatchFunc          IsMatch,
                              OnDirectoryCloseFunc OnDirectoryClose,
                              TranslateQueryFunc   TranslateQuery )
            : Common::Registry::Info( Key ), m_find_func( Find ),
              m_is_match_func( IsMatch ),
              m_on_directory_close_func( OnDirectoryClose ),
              m_translate_query_func( TranslateQuery )
        {
        }

        //-------------------------------------------------------------------
        // Construct a new instance of the object based on an existing
        // object.
        //-------------------------------------------------------------------
        Registry::Info::Info( const Info& Source )
            : Common::Registry::Info( Source ),
              m_find_func( Source.m_find_func ),
              m_is_match_func( Source.m_is_match_func ),
              m_on_directory_close_func( Source.m_on_directory_close_func ),
              m_translate_query_func( Source.m_translate_query_func )
        {
        }

        void
        Registry::Info::Find( const Streams::Streamable& Source,
                              const Directory&           Dir,
                              QueryAnswer&               Answer ) const
        {
            if ( m_find_func )
            {
                ( *m_find_func )( Source, Dir, Answer );
            }
        }

        //-------------------------------------------------------------------

        /// After all data has been read from the directory, this function
        /// is called to allow all search engines an oppertunity
        //-------------------------------------------------------------------
        void
        Registry::OnDirectoryClose( DirectoryScanData& Data )
        {
            //-----------------------------------------------------------------
            // \todo
            // Loop over all registered search engines to determine if any of
            // them wants to preserve data associated with the file.
            //-----------------------------------------------------------------
            for ( registry_container_type::const_iterator
                      cur = registry( ).begin( ),
                      last = registry( ).end( );
                  cur != last;
                  ++cur )
            {
                if ( cur->second )
                {
                    reinterpret_cast< Info* >( cur->second.get( ) )
                        ->OnDirectoryClose( Data );
                }
            }
            Data.s_updated_searches_data.erase(
                Data.s_updated_searches_data.begin( ),
                Data.s_updated_searches_data.end( ) );
            for ( DirectoryScanData::searches_scan_data_type::const_iterator
                      cur = Data.s_searches_scan_data.begin( ),
                      last = Data.s_searches_scan_data.end( );
                  cur != last;
                  ++cur )
            {
                //---------------------------------------------------------------
                /// \todo
                /// Associate the updated information with the cache
                //---------------------------------------------------------------
                if ( cur->second && cur->second->SearchData( ) )
                {
                    Data.s_updated_searches_data[ cur->first ] =
                        cur->second->SearchData( );
                }
            }
        }

        void
        Registry::TranslateQuery( const Cache::QueryParams& Params,
                                  Cache::QueryAnswer&       Answer ) const
        {
            const std::string key_name( Params.Value( "index" ) );
            const id_type     id( Id( key_name ) );

            try
            {
                const std::string& resample( Params.Value( "resample" ) );
                if ( ( resample.compare( "1" ) == 0 ) ||
                     ( resample.compare( "yes" ) == 0 ) ||
                     ( resample.compare( "true" ) == 0 ) )
                {
                    Answer.Resample( true );
                }
                else
                {
                    Answer.Resample( false );
                }
            }
            catch ( const Cache::QueryParams::MissingVariableError& Error )
            {
                // Ignore if the variable is missing
            }

            info_type info( GetInfo( id ) );

            if ( info )
            {
                reinterpret_cast< Info* >( info.get( ) )
                    ->TranslateQuery( Params, Answer );
                Answer.IndexId( id );
            }
        }

        Registry::id_type
        Registry::Register( const Info& Key )
        {
            id_type retval;

            try
            {
                //---------------------------------------------------------------
                // do the core part of registration.
                //---------------------------------------------------------------
                retval = Common::Registry::Register( Key );
                //---------------------------------------------------------------
                // Do local registration
                //---------------------------------------------------------------
            }
            catch (
                const Common::Registry::AlreadyRegisteredException& Exception )
            {
                //---------------------------------------------------------------
                // This type of object is already know.
                //---------------------------------------------------------------
                retval = Exception.Id( );
            }
            return retval;
        }

        bool
        Registry::ScanForMatch( DirectoryScanData& Data )
        {
            bool retval = false;

            //-----------------------------------------------------------------
            // \todo
            // Loop over all registered search engines to determine if any of
            // them wants to preserve data associated with the file.
            //-----------------------------------------------------------------
            for ( registry_container_type::const_iterator
                      cur = registry( ).begin( ),
                      last = registry( ).end( );
                  cur != last;
                  ++cur )
            {
                if ( cur->second )
                {
                    retval |= reinterpret_cast< Info* >( cur->second.get( ) )
                                  ->IsMatch( Data );
                }
            }
            return retval;
        }
    } // namespace Cache
} // namespace diskCache
