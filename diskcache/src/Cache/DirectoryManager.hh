//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__DIRECTORY_MANAGER_HH
#define DISKCACHE_API__CACHE__DIRECTORY_MANAGER_HH

#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/unordered_map.hh"

#include "genericAPI/Logging.hh"

#include "diskcacheAPI/Cache/Directory.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer;

        class DirectoryManager
        {
        public:
            typedef Directory::ScanResults         ScanResults;
            typedef boost::shared_ptr< Directory > directory_ref_type;

            DirectoryManager( );

            //-----------------------------------------------------------------
            /// \brief Add a new directory to the collection
            //-----------------------------------------------------------------
            bool AddDirectory( directory_ref_type Directory );

            //-----------------------------------------------------------------
            /// \brief Create a new directory to be added to the collection
            ///
            /// \param[in] Name
            ///     The name of the directory.
            /// \param[in] Root
            ///     The root of the above directory.
            //-----------------------------------------------------------------
            void CreateDirectory( const std::string& Name,
                                  const std::string& Root );

            //-----------------------------------------------------------------
            /// \brief Locate files
            //-----------------------------------------------------------------
            void Find( const std::string& Root, QueryAnswer& Answer ) const;

            //-----------------------------------------------------------------
            /// \brief Update information concearning a single directory.
            ///
            /// \param[in] Dir
            ///     The updated directory information.
            /// \param[out] Results
            ///     Storage for logging the results.
            ///
            //-----------------------------------------------------------------
            void OnUpdate( directory_ref_type Dir, ScanResults& Results );

            //-----------------------------------------------------------------
            /// \brief Locate a directory in the collection
            //-----------------------------------------------------------------
            directory_ref_type
            ReferenceDirectory( const std::string& Name ) const;

            //-----------------------------------------------------------------
            /// \brief Remove a directory from the collection
            //-----------------------------------------------------------------
            void RemoveDirectory( const std::string& Name );

            //-----------------------------------------------------------------
            /// \brief Remove a directory from the collection
            //-----------------------------------------------------------------
            void RemoveDirectoryRecursively( const std::string& Name );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            template < typename StreamT >
            StreamT& Read( StreamT& Stream );

            //-----------------------------------------------------------------
            /// \brief Scan the directory for changes
            //-----------------------------------------------------------------
            void Scan( directory_ref_type Dir, ScanResults& Results );

            //-----------------------------------------------------------------
            /// \brief Recursively scan the directory for changes
            //-----------------------------------------------------------------
            void Scan( const std::string& Root, ScanResults& Results );

            template < typename StreamT >
            StreamT& Write( StreamT& Stream );

            //-----------------------------------------------------------------
            /// \brief Walk the tree performing the requested operation
            ///
            /// \param[in] Operation
            ///     A function which is to be performed on each node
            ///     found while walking the tree.
            /// \param[in] Root
            ///     Directory from where to start.
            //-----------------------------------------------------------------
            template < class Op >
            void Walk( Op                 Operation,
                       const std::string& Root,
                       bool               IncludeOffline = false );

            //-----------------------------------------------------------------
            /// \brief Walk the tree performing the requested operation
            ///
            /// \param[in] Operation
            ///     A function which is to be performed on each node
            ///     found while walking the tree.
            /// \param[in] Root
            ///     Directory from where to start.
            //-----------------------------------------------------------------
            template < class Op >
            void Walk( const Op           Operation,
                       const std::string& Root,
                       bool               IncludeOffline = false ) const;

            //-----------------------------------------------------------------
            /// \brief Walk the tree depth first performing the requested
            /// operation
            ///
            /// \param[in] Operation
            ///     A function which is to be performed on each node
            ///     found while walking the tree.
            /// \param[in] Root
            ///     Directory from where to start.
            //-----------------------------------------------------------------
            template < class Op >
            void WalkDepthFirst( Op Operation, const std::string& Root );

        protected:
        private:
            class child_functor : public std::list< std::string >
            {
            public:
                inline void
                Parent( const std::string& DirectoryName )
                {
                    m_parent = DirectoryName;
                }

                inline void
                operator( )( const std::string& Child )
                {
                    std::string c( m_parent );

                    c += "/";
                    c += Child;

                    push_front( c );
                }

            private:
                std::string m_parent;
            };

            //-----------------------------------------------------------------
            /// \brief Collection of directories
            //-----------------------------------------------------------------
            typedef LDASTools::AL::unordered_map< std::string,
                                                  directory_ref_type >
                directory_container_type;

            //-----------------------------------------------------------------
            /// \brief Thread safe read access of the collection of directories
            //-----------------------------------------------------------------
            typedef LDASTools::AL::ReadWriteLockVariable<
                LDASTools::AL::ReadWriteLock::READ,
                directory_container_type >
                directory_container_ro_type;

            //-----------------------------------------------------------------
            /// \brief Thread safe write access of the collection of directories
            //-----------------------------------------------------------------
            typedef LDASTools::AL::ReadWriteLockVariable<
                LDASTools::AL::ReadWriteLock::WRITE,
                directory_container_type >
                directory_container_rw_type;

            //-----------------------------------------------------------------
            /// \brief Lock for the collection of directories
            //-----------------------------------------------------------------
            mutable directory_container_rw_type::baton_type
                p_directory_container_baton;
            //-----------------------------------------------------------------
            /// \brief Container to hold the collection of directories
            //-----------------------------------------------------------------
            mutable directory_container_type p_directory_container;

            //-----------------------------------------------------------------
            /// \brief Read access to the collection of directories
            //-----------------------------------------------------------------
            const directory_container_ro_type directory_container_ro( ) const;

            //-----------------------------------------------------------------
            /// \brief Write access for the collection of directories
            //-----------------------------------------------------------------
            directory_container_rw_type directory_container_rw( );
        };

        inline bool
        DirectoryManager::AddDirectory( directory_ref_type Directory )
        {
            bool retval = true;

            //-----------------------------------------------------------------
            // This routine with either recycle the node and replace with
            // the new information, or create a new one.
            //-----------------------------------------------------------------
            directory_container_rw( ).Var( )[ Directory->Fullname( ) ] =
                Directory;

            return retval;
        }

        inline void
        DirectoryManager::CreateDirectory( const std::string& DirectoryName,
                                           const std::string& Root )
        {
            directory_container_type::mapped_type dir(
                new Directory( DirectoryName, Root ) );

            //-----------------------------------------------------------------
            // This routine with either recycle the node and replace with
            // the new information, or create a new one.
            //-----------------------------------------------------------------
            directory_container_rw( ).Var( )[ DirectoryName ] = dir;
        }

        inline DirectoryManager::directory_ref_type
        DirectoryManager::ReferenceDirectory( const std::string& Name ) const
        {
            directory_container_type::mapped_type retval;
            {
                const directory_container_ro_type dirs(
                    directory_container_ro( ) );

                directory_container_type::const_iterator pos(
                    dirs.Var( ).find( Name ) );
                if ( pos != dirs.Var( ).end( ) )
                {
                    retval = pos->second;
                }
            }
            return retval;
        }

        inline void
        DirectoryManager::RemoveDirectory( const std::string& Name )
        {
            QUEUE_LOG_MESSAGE( "Removing directory: " << Name,
                               MT_NOTE,
                               0,
                               "DirectoryManager::RemoveDirectory",
                               "CXX" );
            directory_container_rw( ).Var( ).erase( Name );
        }

        template < class Op >
        void
        DirectoryManager::Walk( Op                 Operation,
                                const std::string& Root,
                                bool               IncludeOffline )
        {
            //-----------------------------------------------------------------
            // Support for walking the root node.
            //-----------------------------------------------------------------
            directory_container_type::mapped_type dir =
                ReferenceDirectory( Root );

            if ( ( !dir ) ||
                 ( ( IncludeOffline == false ) && ( dir->IsOffline( ) ) ) )
            {
                //---------------------------------------------------------------
                // One of the condition exists:
                //   * The directory does NOT exist
                //   * Requested NOT to follow offline directories
                //---------------------------------------------------------------
                return;
            }
            dir = Operation( dir );

            //-----------------------------------------------------------------
            // Walk the children and their decendants.
            //-----------------------------------------------------------------
            // Start by seeding with root information
            //-----------------------------------------------------------------
            child_functor children;

            children.Parent( dir->Fullname( ) );
            dir->ChildrenReverse( children );
            //-----------------------------------------------------------------
            // Now start the walking portion
            //-----------------------------------------------------------------
            while ( children.size( ) > 0 )
            {
                std::string child = children.front( );

                //---------------------------------------------------------------
                // Remove element from the list so eventually
                // the end will be reached.
                //---------------------------------------------------------------
                children.pop_front( );

                dir = ReferenceDirectory( child );
                if ( ( !dir ) ||
                     ( ( IncludeOffline == false ) && ( dir->IsOffline( ) ) ) )
                {
                    //-------------------------------------------------------------
                    // One of the condition exists:
                    //   * The directory does NOT exist
                    //   * Requested NOT to follow offline directories
                    //-------------------------------------------------------------
                    continue;
                }
                //---------------------------------------------------------------
                // Perform caller's request
                //---------------------------------------------------------------
                dir = Operation( dir );

                //---------------------------------------------------------------
                // Seed with any additional nodes.
                //---------------------------------------------------------------
                children.Parent( dir->Fullname( ) );
                dir->ChildrenReverse( children );
            }
        }

        template < class Op >
        void
        DirectoryManager::Walk( const Op           Operation,
                                const std::string& Root,
                                bool               IncludeOffline ) const
        {
            //-----------------------------------------------------------------
            // Support for walking the root node.
            //-----------------------------------------------------------------
            directory_container_type::mapped_type dir =
                ReferenceDirectory( Root );

            if ( !dir )
            {
                return;
            }
            if ( ( IncludeOffline == false ) && ( dir->IsOffline( ) ) )
            {
                return;
            }

            Operation( dir );
            if ( Operation.Done( ) )
            {
                return;
            }

            //-----------------------------------------------------------------
            // Walk the children and their decendants.
            //-----------------------------------------------------------------
            // Start by seeding with root information
            //-----------------------------------------------------------------
            child_functor children;

            children.Parent( dir->Fullname( ) );
            dir->ChildrenReverse( children );
            //-----------------------------------------------------------------
            // Now start the walking portion
            //-----------------------------------------------------------------
            while ( children.size( ) > 0 )
            {
                std::string child = children.front( );

                //---------------------------------------------------------------
                // Remove element from the list so eventually
                // the end will be reached.
                //---------------------------------------------------------------
                children.pop_front( );

                dir = ReferenceDirectory( child );
                // Perform caller's request
                if ( !dir )
                {
                    continue;
                }
                Operation( dir );
                if ( Operation.Done( ) )
                {
                    return;
                }

                //---------------------------------------------------------------
                // Seed with any additional nodes.
                //---------------------------------------------------------------
                children.Parent( dir->Fullname( ) );
                dir->ChildrenReverse( children );
            }
        }

        template < class Op >
        void
        DirectoryManager::WalkDepthFirst( Op                 Operation,
                                          const std::string& Root )
        {
#if WORKING
#if NEW_DIRECTORY_INTERFACE
            assert( 0 );
#else /* NEW_DIRECTORY_INTERFACE */
            //-----------------------------------------------------------------
            // Walk the children and their decendants.
            //-----------------------------------------------------------------
            // Start by seeding with root information
            //-----------------------------------------------------------------
#if NEW_DIRECTORY_INTERFACE
            std::list< std::string > children;
#else /* NEW_DIRECTORY_INTERFACE */
            std::list< dirref_type > children;
#endif /* NEW_DIRECTORY_INTERFACE */
            std::list< dirref_type > scanned_children;
            for ( children_type::reverse_iterator cur = children.rbegin( ),
                                                  last = children.rend( );
                  cur != last;
                  ++cur )
            {
                children.push_front( cur->second );
            }
            //-----------------------------------------------------------------
            // Now start the walking portion
            //-----------------------------------------------------------------
            while ( children.size( ) > 0 )
            {
                dirref_type child = children.front( );

                //---------------------------------------------------------------
                // Remove element from the list so eventually
                // the end will be reached.
                //---------------------------------------------------------------
                children.pop_front( );

                const children_type& additions( child->children( ) );
                if ( additions.size( ) > 0 )
                {
                    // move to scanned list
                    scanned_children.push_front( child );

                    for ( children_type::reverse_iterator
                              cur = additions.rbegin( ),
                              last = additions.rend( );
                          cur != last;
                          ++cur )
                    {
                        children.push_front( cur->second );
                    }
                }
                else
                {
                    // No children. Start processing
                    child = Operation( child );

                    while ( ( scanned_children.size( ) > 0 ) &&
                            ( ( scanned_children.front( )->is_parent_of(
                                    children.front( ) ) == false ) ) )
                    {
                        child = scanned_children.front( );
                        scanned_children.pop_front( );

                        Operation( child );
                    }
                }
#endif /* WORKING */
        }

        //-----------------------------------------------------------------
        // Support for walking the root node.
        //-----------------------------------------------------------------

        Operation( this );
#endif /* NEW_DIRECTORY_INTERFACE */
    } // namespace Cache

    inline const DirectoryManager::directory_container_ro_type
    DirectoryManager::directory_container_ro( ) const
    {
        return ( directory_container_ro_type( p_directory_container_baton,
                                              diskCache::RWLOCK_TIMEOUT,
                                              p_directory_container,
                                              __FILE__,
                                              __LINE__ ) );
    }

    inline DirectoryManager::directory_container_rw_type
    DirectoryManager::directory_container_rw( )
    {
        return ( directory_container_rw_type( p_directory_container_baton,
                                              diskCache::RWLOCK_TIMEOUT,
                                              p_directory_container,
                                              __FILE__,
                                              __LINE__ ) );
    }

} // namespace diskCache
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__DIRECTORY_MANAGER_HH */
