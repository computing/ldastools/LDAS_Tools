//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <iostream>

#include "diskcacheAPI/Common/Logging.hh"

#include "diskcacheAPI/Cache/SDGTxStreamBinary.hh"

namespace diskCache
{
    namespace Cache
    {
        template <>
        SDGTx::DirectoryInfo::read_return_type
        SDGTx::DirectoryInfo::Read( Streams::IBinary& Stream )
        {
            static char const* caller =
                "diskCache::Cache-SDGTx::DirectoryInfo::Read";
            const Streams::Interface::version_type version( Stream.Version( ) );
            typedef Streams::IBinary::size_type    size_type;
            read_return_type                       retval;

            cache_container_type cache;

            QUEUE_LOG_MESSAGE( "Stream Version: " << std::hex << version
                                                  << std::dec,
                               MT_DEBUG,
                               5,
                               caller,
                               "CXX" );

            //-----------------------------------------------------------------
            // Read the extension information
            //-----------------------------------------------------------------
            size_type extension_count( 1 );

            if ( version >= Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
            {
                Stream >> extension_count;
                QUEUE_LOG_MESSAGE( "extension_count: " << extension_count,
                                   MT_DEBUG,
                                   5,
                                   caller,
                                   "CXX" );
            }
            for ( size_type extension_cur = 0; extension_cur < extension_count;
                  ++extension_cur )
            {
                std::string extension;

                if ( version >=
                     Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
                {
                    Stream >> extension;
                    QUEUE_LOG_MESSAGE( "extension: " << extension,
                                       MT_DEBUG,
                                       5,
                                       caller,
                                       "CXX" );
                }
                else
                {
                    extension = s_default_extension;
                    QUEUE_LOG_MESSAGE(
                        "using s_default_extension: " << s_default_extension,
                        MT_DEBUG,
                        5,
                        caller,
                        "CXX" );
                }
                extension_container_type::mapped_type& extension_pos(
                    cache[ extension ] );

                //---------------------------------------------------------------
                // Read the site-desc information
                //---------------------------------------------------------------
                size_type site_desc_count;

                Stream >> site_desc_count;
                QUEUE_LOG_MESSAGE( "site_desc_count: " << site_desc_count,
                                   MT_DEBUG,
                                   5,
                                   caller,
                                   "CXX" );
                for ( size_type sdc = 0; sdc < site_desc_count; ++sdc )
                {
                    //-------------------------------------------------------------
                    // Read the key
                    //-------------------------------------------------------------
                    std::string site_desc;

                    Stream >> site_desc;
                    QUEUE_LOG_MESSAGE( "site_desc: " << site_desc,
                                       MT_DEBUG,
                                       5,
                                       caller,
                                       "CXX" );
                    site_desc_container_type::mapped_type& site_desc_pos(
                        extension_pos[ site_desc ] );

                    //-------------------------------------------------------------
                    // Read the number of time intervals
                    //
                    // This one uses a 4 byte integer for the size instead of
                    // the default size.
                    //-------------------------------------------------------------
                    INT_4U time_interval_count;

                    Stream >> time_interval_count;
                    QUEUE_LOG_MESSAGE(
                        "time_interval_count: " << time_interval_count,
                        MT_DEBUG,
                        5,
                        caller,
                        "CXX" );
                    for ( size_type tic = 0; tic < time_interval_count; ++tic )
                    {
                        //-----------------------------------------------------------
                        // Read the time interval data
                        //-----------------------------------------------------------
                        time_type     start;
                        duration_type duration;

                        Stream >> start >> duration.first >> duration.second;
                        QUEUE_LOG_MESSAGE(
                            "start: " << start << " end: " << duration.first
                                      << " dt: " << duration.second,
                            MT_DEBUG,
                            5,
                            caller,
                            "CXX" );
                        site_desc_pos[ start ] = duration;
                    }
                    if ( site_desc_pos.size( ) == 0 )
                    {
                        extension_pos.erase( extension_pos.find( site_desc ) );
                    }
                }
            }
            retval.reset( new SDGTx::DirectoryInfo( cache ) );
            return retval;
        }

        template <>
        void
        SDGTx::DirectoryInfo::Write( Streams::OBinary& Stream ) const
        {
            static char const* caller =
                "diskCache::Cache-SDGTx::DirectoryInfo::Write";
            const Streams::Interface::version_type version( Stream.Version( ) );
            typedef Streams::IBinary::size_type    size_type;

            extension_container_type dummy_extension;

            QUEUE_LOG_MESSAGE( "Stream Version: " << std::hex << version
                                                  << std::dec,
                               MT_DEBUG,
                               5,
                               caller,
                               "CXX" );
            //-----------------------------------------------------------------
            // Write the extension information
            //-----------------------------------------------------------------
            if ( version >= Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
            {
                size_type extension_count( m_cache.size( ) );

                QUEUE_LOG_MESSAGE( "extension_count: " << extension_count,
                                   MT_DEBUG,
                                   5,
                                   caller,
                                   "CXX" );
                Stream << extension_count;
                if ( extension_count <= 0 )
                {
                    //-------------------------------------------------------------
                    // Nothing more to do here
                    //-------------------------------------------------------------
                    return;
                }
            }
            //-----------------------------------------------------------------
            // Loop over the extensions
            //-----------------------------------------------------------------
            extension_container_type::const_iterator extension_cur =
                                                         m_cache.begin( ),
                                                     extension_last =
                                                         m_cache.end( );

            if ( version < Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
            {
                //--------------------------------------------------------------
                // Only do the default extension
                //--------------------------------------------------------------
                extension_cur = m_cache.find( s_default_extension );
                if ( extension_cur != extension_last )
                {
                    //-------------------------------------------------------------
                    // Have found the default extension in the list.
                    // Reset the end to be the element following.
                    //-------------------------------------------------------------
                    extension_last = extension_cur;
                    ++extension_last;
                }
                else
                {
                    dummy_extension[ s_default_extension ];
                    extension_cur = dummy_extension.begin( );
                    extension_last = dummy_extension.end( );
                    QUEUE_LOG_MESSAGE( "use_dummy_extension: "
                                           << dummy_extension.size( )
                                           << " s_default_extension: "
                                           << s_default_extension,
                                       MT_DEBUG,
                                       5,
                                       caller,
                                       "CXX" );
                }
            }

            while ( extension_cur != extension_last )
            {
                if ( version >=
                     Streams::Interface::VERSION_MULTIPLE_EXTENSIONS )
                {
                    //-------------------------------------------------------------
                    // Write the extension
                    //-------------------------------------------------------------
                    Stream << extension_cur->first;
                    QUEUE_LOG_MESSAGE( "extension: " << extension_cur->first,
                                       MT_DEBUG,
                                       5,
                                       caller,
                                       "CXX" );
                }
                //---------------------------------------------------------------
                // Loop over the site/description elements
                //---------------------------------------------------------------
                const extension_container_type::mapped_type& site_desc_pos(
                    extension_cur->second );

                size_type site_desc_count( site_desc_pos.size( ) );

                Stream << site_desc_count;
                QUEUE_LOG_MESSAGE( "site_desc_count: " << site_desc_count,
                                   MT_DEBUG,
                                   5,
                                   caller,
                                   "CXX" );

                for ( site_desc_container_type::const_iterator
                          site_desc_cur = site_desc_pos.begin( ),
                          site_desc_last = site_desc_pos.end( );
                      site_desc_cur != site_desc_last;
                      ++site_desc_cur )
                {
                    Stream << site_desc_cur->first;
                    //-------------------------------------------------------------
                    // Loop over interval data for the site/description
                    //-------------------------------------------------------------
                    const site_desc_container_type::mapped_type& interval_pos(
                        site_desc_cur->second );

                    INT_4U interval_count( interval_pos.size( ) );

                    Stream << interval_count; // Number of intervals
                    for ( interval_container_type::const_iterator
                              interval_cur = interval_pos.begin( ),
                              interval_last = interval_pos.end( );
                          interval_cur != interval_last;
                          ++interval_cur )
                    {
                        Stream << interval_cur->first // Start Time
                               << interval_cur->second.first // End Time
                               << interval_cur->second.second // Dt
                            ;
                    }
                }
                //---------------------------------------------------------------
                // Advance to the next extension
                //---------------------------------------------------------------
                ++extension_cur;
            }
        }

        Streams::IBinary::read_return_type
        SDGTxDirectoryInfoFromIBinary( Streams::IBinary& Stream )
        {
            Streams::IBinary::read_return_type retval(
                SDGTx::DirectoryInfo::Read< Streams::IBinary >( Stream ) );

            return retval;
        }

        void
        SDGTxDirectoryInfoToOBinary( Streams::OBinary&          Stream,
                                     const Streams::Streamable& Data )
        {
            try
            {
                const SDGTx::DirectoryInfo& data(
                    dynamic_cast< const SDGTx::DirectoryInfo& >( Data ) );

                data.Write( Stream );
            }
            catch ( const std::bad_cast& Exception )
            {
                //---------------------------------------------------------------
                // Case were an unknown data type was passed to this function
                //---------------------------------------------------------------
            }
        } // function - SDGTxDirectoryInfoToOBinary

    } // namespace Cache
} // namespace diskCache
