# LDASTools diskcache - Tools for querying a collection of files on disk
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools diskcache is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools diskcache is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#------------------------------------------------------------------------
#------------------------------------------------------------------------

set( NAMESPACE diskcacheAPI )

#========================================================================
# Traverse the tree
#========================================================================

add_subdirectory(Common)
add_subdirectory(Streams)
add_subdirectory(Cache)

#------------------------------------------------------------------------
# => 2:0:0 -- 2010.01.25
#   In preperation for 1.18.0 release of ldas
# => 4:0:0 - 2010.03.05 => Modified data type used for exclude list
# => 5:0:0 -- 2013.07.24 ( post 1.19.23 )
#   Removed threaded writeDirCache( const char* )
#   Added threaded writeDirCache( const char*, version_type )
#   Removed threaded writeDirCacheAscii( const char* )
#   Added threaded writeDirCacheAscii( const char*, version_type )
# => 6:0:0 -- 2015.05.08
#   Cache structure changed
# => 7:0:0 -- 2015.07.06
#    Converted to use SignalHandler instead of Unix system signal
# 8:0:0 - 2016.03.03
#    * Extended Status command
#    * Added Device (system mounts) caching
# 8:1:0 - 2016.09.09
#    * Added padding option
# 8:2:0 - 2016.09.15
#    * Added support for additional configuration options
# 8:3:0 - 2016.09.26
#    * Fixed diskcache to recover from devices going offline (#4645)
# 9:0:1 - 2016.10.10
#    * Added back support for systems that use setmntent()
#    * Modified diskcache to support reconfigure command
#    * Reworked signal handling for termination and reconfiguration
#    * Added ability to exclude pattern of files (#4701)
#    * Added variable HOT_DIRECTORY_AGE_SEC (#4646)
#    * Added variable HOT_DIRECTORY_SCAN_INTERVAL_SEC (#4647)
# 10:0:2 - 2017.11.25 (ldas-tools-diskcacheAPI-2.5.6)
#    * Modified internals of AtomicFile class
# 11:0:0 - 2017.11.25 (ldas-tools-diskcacheAPI-2.6.0)
#    * Removed hand rolled smart pointers in favor of boost smart pointers
# 11:1:0 - 2018.10.19 (ldas-tools-diskcacheAPI-2.6.2)
#    * Replaced debugging messages goint to std::cerr with queue_log_message
# 12:0:0 - 2019.08.14 (ldas-tools-diskcacheAPI-2.7.0)
#    * Added rename( char const*, char const* ) to AtomicFile
#    * Added strerror( int ) to AtomicFile
#    * Removed ErrnoMessage( ) from AtomicFile
#    * Removed close( ) from AtomicFile
#    * Removed operator<<( std::ostream&, LDASTools::AL::CommandLineOptions::Option const& ) from Directory
#    * Removed operator<<( std::ostream&, GPSTime const& ) from Directory
# 12:1:0 - 2023.04.19 (ldas-tools-diskcacheAPI-2.7.5)
#    * Added Variable handling to scan subcommand
#    * Resolved race condition when setting options for
#      diskcahce scan subcommand
# 12:2:0 - 2023.05.05 (ldas-tools-diskcacheAPI-2.7.6)
#    * Fixed setting of parameters for diskcache daemon subcommand
# 13:0:1 - 2023.08.05 (ldas-tools-diskcacheAPI-2.7.7)
#    * Added methods to several of the meta commands
# 13:1:1 - 2024.12.16 (ldas-tools-diskcacheAPI-2.8.0)
#------------------------------------------------------------------------
# form: <(c)urrent>:(r)evision>:(a)ge>
#
# 1. Update the version information only immediately before a public
#    release of your software. More frequent updates are unnecessary,
#    and only guarantee that the current interface number gets larger
#    faster.
# 2. If the library source code has changed at all since the last
#    update, then increment revision (c:r:a becomes c:r+1:a).
# 3. If any interfaces [exported functions or data] have been added,
#    removed, or changed since the last update, increment current,
#    and set revision to 0.
# 4. If any interfaces have been added since the last public
#    release, then increment age.
# 5. If any interfaces have been removed since the last public
#    release, then set age to 0.
#------------------------------------------------------------------------

set( libdiskcache_la_CURRENT 13 )
set( libdiskcache_la_REVISION 1 )
set( libdiskcache_la_AGE 1 )

#========================================================================
# Global variables
#========================================================================

#========================================================================
# Example scripts
#========================================================================

set( exampledir  ${CMAKE_INSTALL_DOCDIR} )
set( example_SCRIPTS
	diskcache_poller
  diskcache_server
)

install( PROGRAMS ${example_SCRIPTS}
  DESTINATION ${exampledir}
)

#========================================================================
# Definition of additional variables
#========================================================================

set( HDR_DIR ${NAMESPACE} )
set( hhdir ${CMAKE_INSTALL_INCLUDEDIR}/${HDR_DIR} )
set( hh_HEADERS
	AtomicFile.hh
  Commands.hh
	DirectoryManagerSingleton.hh
	DumpCacheDaemon.hh
	IO.hh
	MetaCommands.hh
	MountPointManager.hh
	MountPointManagerSingleton.hh
	ServerInterface.hh
  Variables.hh
)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# libdiskcache
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

set( libdiskcache_la_SOURCES
	Commands.cc
	counter.cc
	Daemons.cc
	DirectoryManagerSingleton.cc
	diskcacheAPI.cc
	diskcachecmd.cc
	DumpCacheDaemon.cc
	MetaCommands.cc
	MetaCommandDaemon.cc
	MetaCommandDump.cc
	MetaCommandFilenames.cc
	MetaCommandFilenamesRDS.cc
	MetaCommandIntervals.cc
	MetaCommandQuit.cc
	MetaCommandReconfigure.cc
	MetaCommandScan.cc
	MetaCommandStatus.cc
	MountPointManager.cc
	MountPointManagerSingleton.cc
	MountPointManagerStreamASCII.cc
	MountPointManagerStreamBinary.cc
	MountPointScanner.cc
	ScanMountPointsDaemon.cc
	ServerInterface.cc
  Variables.cc
)

set( libdiskcache_la_LIBADD
  cache
  streams
  common
  ${LDASTOOLS_LIBRARIES_FULL_PATH}
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_REGEX_LIBRARY}
)

cx_link_headers( ${HDR_DIR} ${hh_HEADERS} )

cx_list_prefix(common_HEADERS "Common/" )
cx_list_prefix(streams_HEADERS "Streams/" )
cx_list_prefix(cache_HEADERS "Cache/" )
cx_msg_debug_variable(common_HEADERS)

cx_target_library(
  TARGET diskcache
  CURRENT ${libdiskcache_la_CURRENT}
  REVISION ${libdiskcache_la_REVISION}
  AGE ${libdiskcache_la_AGE}
  STATIC
  SOURCES ${libdiskcache_la_SOURCES}
  LIBRARIES ${libdiskcache_la_LIBADD}
  INCLUDE_DIRECTORIES BEFORE PRIVATE
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_BINARY_DIR}/include
  ${CMAKE_BINARY_DIR}
  ${LDASTOOLS_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ABI_MESSAGE "Disk Cache Library"
  ABI_HEADER_DIR ${HDR_DIR}
  ABI_LOCAL_INCLUDE_DIR ${CMAKE_BINARY_DIR}/include/${HDR_DIR}
  ABI_HEADERS
  ${hh_HEADERS}
  ${common_HEADERS}
  ${cache_HEADERS}
)

install(
  FILES ${hh_HEADERS}
  DESTINATION "${hhdir}" )

cx_target_pkgconfig( ldastools-diskcache )

##========================================================================
## Utility programs
##========================================================================
#
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## diskcache
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#bin_PROGRAMS += diskcache
#
set( diskcache_SOURCES
  diskcache.cc
)
set( diskcache_INCLUDES
	${CMAKE_SOURCE_DIR}
	${CMAKE_BINARY_DIR}
	${CMAKE_BINARY_DIR}/include
	${LDASTOOLS_INCLUDE_DIRS}
)

set( diskcache_LDADD
  diskcache
  ${LDASTOOLS_LIBRARIESFULL_PATH}
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_SYSTEM_LIBRARY}
  ${LIB_NSL}
)

cx_target_executable( diskcache_exe
  OUTPUT_NAME diskcache
  SOURCES ${diskcache_SOURCES}
  DEFINES
  FOR_LDAS=1
  SYSCONFDIR="${CMAKE_INSTALL_FULL_SYSCONFDIR}"
  INCLUDE_DIRECTORIES BEFORE PRIVATE
  ${diskcache_INCLUDES}
  LDADD ${diskcache_LDADD}
)

##========================================================================
## Utility programs
##========================================================================
#
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## Configuration sample file
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

install(
  FILES ${CMAKE_CURRENT_SOURCE_DIR}/diskcache.rsc.sample
  DESTINATION ${CMAKE_INSTALL_FULL_SYSCONFDIR}/diskcache
)

##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## RedHat - services
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ( NOT COND_HAVE_SYSTEMD AND RED_HAT_INITD )
  install(
    FILES ${CMAKE_CURRENT_SOURCE_DIR}/diskcached.rh
    DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/initd.d
    RENAME diskcached
  )
endif( )

##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## Debian - services
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ( DEBIAN_INITD )
  #INSTALL_DATA_HOOK+=install-hook-initd-debian
  install(
    FILES ${CMAKE_CURRENT_SOURCE_DIR}/diskcached.debian
    DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/initd.d
    RENAME diskcached
  )
endif( )

##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## OSX - services
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ( OSX_INITD AND NOT COND_HAVE_SYSTEMD )
  install(
    FILES ${CMAKE_CURRENT_SOURCE_DIR}/diskcached.osx
    DESTINATION /Library/LaunchDaemons
    RENAME diskcache.plist
  )
endif( )

##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## Solaris - services via svccfg
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ( SOLARIS_INITD )
  #INSTALL_DATA_HOOK+=install-hook-initd-solaris
  #CLEAN_LOCAL += diskcache-xml-solaris-clean-local
  #
  #diskcache-xml-solaris-clean-local:
  #	rm -f diskcache.xml.solaris
  #
  #diskcache.xml.solaris: Makefile diskcache.xml.solaris.in
  #	$(AM_V_GEN) rm -f $@ $@.tmp; \
  #	srcdir=''; \
  #	  test -f ./$@.in || srcdir=$(srcdir)/; \
  #	  $(edit) $${srcdir}$@.in >$@.tmp \
  #	chmod +x $@.tmp; \
  #	chmod a-w $@.tmp; \
  #	mv $@.tmp $@
  #
  #install-hook-initd-solaris: diskcache.xml.solaris
  #	$(MKDIR_P) $(DESTDIR)/$(datadir)
  #	$(INSTALL_DATA) diskcache.xml.solaris $(DESTDIR)/$(datadir)/diskcache.xml
endif( )

##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
## Generic systemd
##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ( COND_HAVE_SYSTEMD )
  install(
    FILES ${CMAKE_CURRENT_SOURCE_DIR}/diskcache.service
    DESTINATION ${systemdsystemunitdir}
  )
endif( )

#========================================================================
# Testing of functionality
#========================================================================

set( test_diskcache_SOURCES
  test_diskcache.cc
)

set( test_diskcache_INCLUDES
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_BINARY_DIR}
  ${CMAKE_BINARY_DIR}/include
  ${CMAKE_SOURCE_DIR}
  ${LDASTOOLS_INCLUDE_DIRS}
)

set( test_diskcache_LDADD
  diskcache
  ${LDASTOOLS_FULL_PATH}
)

cx_target_executable( test_diskcache_
  NOINST
  SOURCES ${test_diskcache_SOURCES}
  INCLUDE_DIRECTORIES BEFORE PRIVATE
    ${test_diskcache_INCLUDES}
  LDADD ${test_diskcache_LDADD}
)

cx_target_test( test_diskcache
  COMMAND test_diskcache_
)
set_tests_properties(
  test_diskcache
  PROPERTIES
  ENVIRONMENT "ASAN_OPTIONS=$ENV{ASAN_OPTIONS}"
)
