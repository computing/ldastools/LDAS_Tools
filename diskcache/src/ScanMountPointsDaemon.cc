//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <set>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Thread.hh"

#include "Cache/HotDirectory.hh"
#include "diskcachecmd.hh"
#include "MountPointScanner.hh"
#include "ScanMountPointsDaemon.hh"

using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::SignalHandler;
using LDASTools::AL::Thread;

namespace diskCache
{
    MutexLock::baton_type ScanMountPointsDaemon::m_variable_baton;

    //-----------------------------------------------------------------------------
    /// This variable specifies the number of milliseconds to delay between
    /// successive scans of the list of mount points.
    ///
    /// The default value is 500 ( 0.5 sec. ).
    //-----------------------------------------------------------------------------
    ScanMountPointsDaemon::interval_type ScanMountPointsDaemon::m_interval =
        500;

    ScanMountPointsDaemon::ScanMountPointsDaemon( std::ostream* Stream )
        : Task( "DiskCache::ScanMOuntPointDaemon" ), active( true )
    {
        delete_on_completion( true );
    }

    ScanMountPointsDaemon::~ScanMountPointsDaemon( )
    {
    }

    void
    ScanMountPointsDaemon::operator( )( )
    {
        //-------------------------------------------------------------------
        // Scan as long as the application is running.
        //-------------------------------------------------------------------
        while ( ( is_active( ) ) && ( MemChecker::IsExiting( ) == false ) )
        {
            //-----------------------------------------------------------------
            // Scan through the list of mount points.
            //-----------------------------------------------------------------
            if ( MountPointScanner::SyncRequest::PendingRequests( ) == 0 )
            {
                MountPointScanner::ScanResults results;
                ScanMountPointList( results );

                /// \todo
                ///     Need to format the results of scanning and then dump out
                ///     either to the requested stream or use the logging
                ///     mechanism.
            }
            //-----------------------------------------------------------------
            /// Suspend scanning for a period of time
            //-----------------------------------------------------------------
            struct timespec     wakeup;
            const interval_type i( Interval( ) );
            wakeup.tv_sec = i / 1000;
            wakeup.tv_nsec = ( i % 1000 ) * 1000000;
            //-----------------------------------------------------------------
            // Modify the cancellation signal to be a true signal to allow
            // interuptability
            //-----------------------------------------------------------------
            Thread::signal_type       old_aux = SignalHandler::SIGNAL_UNKNOWN;
            const Thread::cancel_type old_type(
                Thread::Self::CancellationType( old_aux ) );
            Thread::Self::CancellationType( Thread::CANCEL_BY_SIGNAL,
                                            SignalHandler::SIGNAL_TERMINATE );
            nanosleep( &wakeup, NULL );
            Thread::Self::CancellationType( old_type, old_aux );
        }
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Cache::HotDirectory::StopDaemon( );
    }
} // namespace diskCache
