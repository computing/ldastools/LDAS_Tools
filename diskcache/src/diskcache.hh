//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__DISK_CACHE_HH
#define DISKCACHE_API__DISK_CACHE_HH

#include <limits>
#include <list>
#include <sstream>
#include <string>

#include "ldastoolsal/types.hh"

#include "diskcacheAPI/Common/Logging.hh"

#define NEW_INTERFACE 1
#define DEPRICATED_INTERFACE 0

namespace diskCache
{
    enum dump_format_type
    {
        DUMP_FORMAT_TCL = 1
    };
    typedef INT_4S mtime_type;

    typedef INT_8U total_file_count_type;

    //---------------------------------------------------------------------
    const total_file_count_type SCANNED_FILES_RESET =
        std::numeric_limits< total_file_count_type >::max( );

    total_file_count_type ScannedFiles( );

    void ScannedFiles( total_file_count_type IncrementValue );

#if NEW_INTERFACE
    void SetFilenameExtensions( const std::list< std::string >& Extensions );
#endif /* NEW_INTERFACE */
} // namespace diskCache

#endif /* DISKCACHE_API__DISK_CACHE_HH */
