//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <sys/select.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <ctype.h>
#include <unistd.h>

#include <algorithm>
#include <list>
#include <map>

#include "ldastoolsal/SignalHandler.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/LDASplatform.hh"
#include "genericAPI/MonitorMemory.hh"
#include "genericAPI/Stat.hh"

#include "diskcacheAPI/Common/Variables.hh"
#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"
#include "diskcacheAPI/Cache/ExcludedPattern.hh"
#include "diskcacheAPI/Cache/HotDirectory.hh"

#include "diskcacheAPI/IO.hh"
#include "diskcacheAPI/MetaCommands.hh"

#include "Commands.hh"
#include "diskcachecmd.hh"
#include "DumpCacheDaemon.hh"
#include "MountPointScanner.hh"
#include "ScanMountPointsDaemon.hh"

using LDASTools::AL::CommandLineOptions;

using diskCache::Commands::updateFileExtList;
using diskCache::Commands::updateMountPtList;

typedef diskCache::MetaCommand::client_type                       client_type;
typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;

#define REQUEST_RELOAD LDASTools::AL::SignalHandler::SIGNAL_HANGUP

namespace
{
    CommandLineOptions
    convert_line_to_options( char* Buffer )
    {
        //-------------------------------------------------------------------
        // Create list of options.
        //-------------------------------------------------------------------
        char* lb_opts[ 64 ];
        int   cur_opt = 0;
        char* lb_pos = Buffer;

        lb_opts[ cur_opt ] = lb_pos;
        while ( *lb_pos )
        {
            if ( ::isspace( *lb_pos ) )
            {
                *lb_pos = '\0';
                ++lb_pos;
                while ( *lb_pos && ::isspace( *lb_pos ) )
                {
                    ++lb_pos;
                }
                if ( *lb_pos )
                {
                    lb_opts[ ++cur_opt ] = lb_pos;
                }
                continue;
            }
            ++lb_pos;
        }
        lb_opts[ ++cur_opt ] = (char*)NULL;

        CommandLineOptions retval( cur_opt, lb_opts );

        return retval;
    }

    void
    order_by_access( std::list< std::string >& Paths )
    {
        typedef std::multimap< time_t, std::string > op_type;
        typedef std::list< std::string >             paths_type;

        op_type ordered_paths;

        struct stat stat_buf;

        for ( paths_type::const_iterator cur = Paths.begin( ),
                                         last = Paths.end( );
              cur != last;
              ++cur )
        {
            if ( GenericAPI::Stat( *cur, stat_buf ) == 0 )
            {
                ordered_paths.insert(
                    op_type::value_type( stat_buf.st_mtime, *cur ) );
            }
        }

        paths_type new_paths;

        for ( op_type::const_reverse_iterator cur = ordered_paths.rbegin( ),
                                              last = ordered_paths.rend( );
              cur != last;
              ++cur )
        {
            new_paths.push_back( cur->second );
        }

        Paths.swap( new_paths );
    }

} // namespace
namespace diskCache
{
    namespace MetaCommand
    {
        Daemon::DaemonConfig_::DaemonConfig_( Daemon& Command )
            : config_base_type( Command.GetVariables( ) ), command( Command )
        {
        }

        void
        Daemon::DaemonConfig_::ParseKeyValue( const std::string& Key,
                                              const std::string& Value )
        {
            switch ( state )
            {
            case NON_BLOCK:
            case BLOCK_DAEMON:
                if ( Key.compare( "SERVER_PORT" ) == 0 )
                {
                    command.set_server_port( Value );
                    return;
                }
                break;
            case BLOCK_EXTENSIONS:
            case BLOCK_EXCLUDED_DIRECTORIES:
            case BLOCK_EXCLUDED_PATTERNS:
            case BLOCK_MOUNT_POINTS:
            case BLOCK_UNKNOWN:
                break;
            }
            config_base_type::ParseKeyValue( Key, Value );
        }

        //===================================================================
        // Daemon
        //===================================================================
        bool       Daemon::daemon_mode;
        OptionSet& Daemon::m_options( Daemon::init_options( ) );

        OptionSet&
        Daemon::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: daemon" );

            retval.Summary( "The daemon sub command is intended to"
                            " provide a continuous scanning mode." );

            retval.Add( Option( OPT_CONCURRENCY,
                                "concurrency",
                                Option::ARG_REQUIRED,
                                "Number of mount points to scan concurrently",
                                "integer" ) );

            retval.Add( Option(
                OPT_CONFIGURATION_FILE,
                "configuration-file",
                Option::ARG_REQUIRED,
                "Name of file containing additional configuration information.",
                "filename" ) );

            retval.Add( Option( OPT_DIRECTORY_TIMEOUT,
                                "directory-timeout",
                                Option::ARG_REQUIRED,
                                "timeout in seconds for directory operations",
                                "integer" ) );

            retval.Add( Option(
                OPT_EXCLUDED_DIRECTORIES,
                "excluded-directories",
                Option::ARG_REQUIRED,
                "Comma seperated list of directories not to be searched",
                "list" ) );

            retval.Add(
                Option( OPT_EXCLUDED_PATTERNS,
                        "excluded-patterns",
                        Option::ARG_REQUIRED,
                        "Comma seperated list of file names not to be searched",
                        "list" ) );

            retval.Add( Option( OPT_EXTENSIONS,
                                "extensions",
                                Option::ARG_REQUIRED,
                                "Comma seperated list of file extensions",
                                "list" ) );

            retval.Add(
                Option( OPT_HOT_DIRECTORY_AGE,
                        "hot-directory-age",
                        Option::ARG_REQUIRED,
                        "Age in seconds for which to consider a directory hot",
                        "integer" ) );

            retval.Add( Option(
                OPT_HOT_DIRECTORY_SCAN_INTERVAL,
                "hot-directory-scan-interval",
                Option::ARG_REQUIRED,
                "Number of seconds between scanning directories in hot list",
                "integer" ) );

            retval.Add( Option( OPT_RWLOCK_INTERVAL,
                                "rwlock-interval-ms",
                                Option::ARG_REQUIRED,
                                "milisecond interval",
                                "integer" ) );

            retval.Add( Option( OPT_RWLOCK_TIMEOUT,
                                "rwlock-timeout-ms",
                                Option::ARG_REQUIRED,
                                "milisecond timeout",
                                "integer" ) );

            retval.Add(
                Option( OPT_SCAN_INTERVAL,
                        "interval",
                        Option::ARG_REQUIRED,
                        "Number of milliseconds to pause between scans.",
                        "integer" ) );

            retval.Add( Option(
                OPT_STAT_TIMEOUT,
                "stat-timeout",
                Option::ARG_REQUIRED,
                "Number of seconds to wait for system stat call to return.",
                "integer" ) );

            retval.Add( Option( OPT_MOUNT_POINTS,
                                "mount-points",
                                Option::ARG_REQUIRED,
                                "Comma seperated list of mount points to scan",
                                "list" ) );

            retval.Add( Option( OPT_OUTPUT_ASCII,
                                "output-ascii",
                                Option::ARG_REQUIRED,
                                "Filename for the ascii output; '-' to direct "
                                "to standard output",
                                "filename" ) );

            retval.Add( Option( OPT_OUTPUT_BINARY,
                                "output-binary",
                                Option::ARG_REQUIRED,
                                "Filename for the binary output",
                                "filename" ) );

            retval.Add(
                Option( OPT_VERSION_ASCII,
                        "version-ascii",
                        Option::ARG_REQUIRED,
                        "Version of the ascii diskcache dump format to output",
                        "version" ) );

            retval.Add(
                Option( OPT_VERSION_BINARY,
                        "version-binary",
                        Option::ARG_REQUIRED,
                        "Version of the binary diskcache dump format to output",
                        "version" ) );
            return retval;
        }

        //-------------------------------------------------------------------
        // Static data values of Daemon
        //-------------------------------------------------------------------

        const char* Daemon::VAR_NAME_EXCLUDED_PATTERNS = "EXCLUDED_PATTERN";

        //-------------------------------------------------------------------
        /// Contruct a new instance of Daemon.
        //-------------------------------------------------------------------
        Daemon::Daemon( CommandLineOptions&                      Args,
                        const ClientServerInterface::ServerInfo& Server,
                        const std::string& DefaultConfigurationFilename,
                        bool               Seedable )
            : m_args( Args ), finished( false ), reset_requested( false ),
              server_info( Server )
        {
            cache_write_delay( "120" );
            set_concurrency( "4" );
            set_scan_interval( "16000" );
            for ( auto pattern : Cache::ExcludedPattern::Get( ) )
            {
                variables.push_excluded_pattern( pattern );
            }

            {
                configuration_filename = DefaultConfigurationFilename;
                std::ifstream stream( configuration_filename.c_str( ) );

                if ( stream.is_open( ) )
                {
                    DaemonConfig_ config( *this );

                    variables.ParseConfigurationFile( stream, config );
                }
            }

            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );

                while ( parsing )
                {
                    switch ( m_args.Parse( m_options, arg_name, arg_value ) )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_CONCURRENCY:
                        diskCache::Common::Variables::Cache(
                            Variables::VAR_NAME_CONCURRENCY, arg_value );
                        break;
                    case OPT_CONFIGURATION_FILE:
                    {
                        configuration_filename = arg_value;
                        std::ifstream stream( configuration_filename.c_str( ) );

                        if ( stream.is_open( ) )
                        {
                            DaemonConfig_ config( *this );

                            variables.ParseConfigurationFile( stream, config );
                        }
                    }
                    break;
                    case OPT_DIRECTORY_TIMEOUT:
                        diskCache::Common::Variables::Cache(
                            Variables::VAR_NAME_DIRECTORY_TIMEOUT, arg_value );
                        break;
                    case OPT_EXCLUDED_DIRECTORIES:
                    {
                        //-----------------------------------------------------------
                        // Generate list of directories
                        //-----------------------------------------------------------
                        Cache::ExcludedDirectoriesSingleton::
                            directory_container_type directories;

                        size_t pos = 0;
                        size_t end = 0;

                        while ( end != std::string::npos )
                        {
                            //-------------------------------------------------------
                            // Extract each directory
                            //-------------------------------------------------------
                            end = arg_value.find_first_of( ",", pos );
                            const size_t dend( ( end == std::string::npos )
                                                   ? end
                                                   : ( end - pos ) );
                            variables.push_excluded_directory(
                                arg_value.substr( pos, dend ) );
                            pos = end + 1;
                        }
                        //---------------------------------------------------------
                        // Update the list of directories to be excluded
                        //---------------------------------------------------------
                        Cache::ExcludedDirectoriesSingleton::Update(
                            directories );
                    }
                    break;
                    case OPT_EXCLUDED_PATTERNS:
                    {
                        //-----------------------------------------------------------
                        // Generate list of directories
                        //-----------------------------------------------------------
                        size_t pos = 0;
                        size_t end = 0;

                        variables.reset_excluded_patterns( );
                        while ( end != std::string::npos )
                        {
                            //-------------------------------------------------------
                            // Extract each directory
                            //-------------------------------------------------------
                            end = arg_value.find_first_of( ",", pos );
                            const size_t dend( ( end == std::string::npos )
                                                   ? end
                                                   : ( end - pos ) );
                            variables.push_excluded_pattern(
                                arg_value.substr( pos, dend ) );
                            pos = end + 1;
                        }
                    }
                    break;
                    case OPT_EXTENSIONS:
                    {
                        //-----------------------------------------------------------
                        // Generate list of extensions
                        //-----------------------------------------------------------
                        size_t pos = 0;
                        size_t end = 0;

                        while ( end != std::string::npos )
                        {
                            end = arg_value.find_first_of( ",", pos );
                            variables.push_extension(
                                arg_value.substr( pos,
                                                  ( ( end == std::string::npos )
                                                        ? end
                                                        : end - pos ) ) );
                            pos = end + 1;
                        }
                    }
                    break;
                    case OPT_HOT_DIRECTORY_AGE:
                        diskCache::Common::Variables::Cache(
                            Variables::VAR_NAME_HOT_DIRECTORY_AGE, arg_value );
                        break;
                    case OPT_HOT_DIRECTORY_SCAN_INTERVAL:
                        diskCache::Common::Variables::Cache(
                            Variables::VAR_NAME_HOT_DIRECTORY_SCAN_INTERVAL,
                            arg_value );
                        break;
                    case OPT_RWLOCK_INTERVAL:
                        diskCache::Common::Variables::Cache(
                            Variables::VAR_NAME_RWLOCK_INTERVAL, arg_value );
                        break;
                    case OPT_RWLOCK_TIMEOUT:
                        diskCache::Common::Variables::Cache(
                            Variables::VAR_NAME_RWLOCK_TIMEOUT, arg_value );
                        break;
                    case OPT_SCAN_INTERVAL:
                        set_scan_interval( arg_value );
                        break;
                    case OPT_MOUNT_POINTS:
                    {
                        //-----------------------------------------------------------
                        // Generate list of mount points
                        //-----------------------------------------------------------
                        size_t pos = 0;
                        size_t end = 0;

                        variables.reset_mount_points( );
                        while ( end != std::string::npos )
                        {
                            end = arg_value.find_first_of( ",", pos );
                            variables.push_mount_point(
                                arg_value.substr( pos,
                                                  ( ( end == std::string::npos )
                                                        ? end
                                                        : end - pos ) ) );
                            pos = end + 1;
                        }
                    }
                    break;
                    case OPT_OUTPUT_ASCII:
                        set_output_ascii( arg_value );
                        break;
                    case OPT_OUTPUT_BINARY:
                        set_output_binary( arg_value );
                        break;
                    case OPT_VERSION_BINARY:
                        set_output_binary_version( arg_value );
                        break;
                    default:
                        break;
                    }
                }
            }
            //-----------------------------------------------------------------
            // Check for seeding
            //-----------------------------------------------------------------
            if ( ( Seedable ) &&
                 ( boost::any_cast< std::string >(
                       Common::Variables::Cache(
                           diskCache::Variables::VAR_NAME_OUTPUT_BINARY ) )
                       .empty( ) == false ) )
            {
                std::list< std::string > paths;
                auto                     output_binary =
                    boost::any_cast< std::string >( Common::Variables::Cache(
                        diskCache::Variables::VAR_NAME_OUTPUT_BINARY ) );

                paths.push_back( output_binary );
                paths.push_back( ( output_binary + ".bak" ) );
                paths.push_back( ( output_binary + "#Resync#" ) );

                ::order_by_access( paths );

                //---------------------------------------------------------------
                // Seed the cache with previous binary
                //---------------------------------------------------------------
                for ( std::list< std::string >::const_iterator
                          cur_path = paths.begin( ),
                          last_path = paths.end( );
                      cur_path != last_path;
                      ++cur_path )
                {
                    try
                    {
                        QUEUE_LOG_MESSAGE(
                            "Trying to seed the cache with: " << *cur_path,
                            MT_NOTE,
                            0,
                            "Daemon::Daemon",
                            "CXX" );

                        LDASTools::AL::ifstream     ifs( cur_path->c_str( ) );
                        diskCache::Streams::IBinary stream( ifs );

                        try
                        {
                            diskCache::Read( stream );
                        }
                        catch ( ... )
                        {
                            ifs.close( );
                            continue;
                        }
                        ifs.close( );
                        //-----------------------------------------------------------
                        // Was able to seed with the given file so terminate
                        //   loop of files to try.
                        //-----------------------------------------------------------
                        QUEUE_LOG_MESSAGE(
                            "Succeeded to seed the cache with: " << *cur_path,
                            MT_NOTE,
                            0,
                            "Daemon::Daemon",
                            "CXX" );
                        break;
                    }
                    catch ( ... )
                    {
                        //-----------------------------------------------------------
                        // Something wrong with reading so advance to next
                        //   file to try.
                        //-----------------------------------------------------------
                    }
                }
            }
            //-----------------------------------------------------------------
            // Set variables once cache is seeded (ticket #2167)
            //-----------------------------------------------------------------
            setup_variables( );
        }

        //-------------------------------------------------------------------
        /// Return resource back to the system
        //-------------------------------------------------------------------
        Daemon::~Daemon( )
        {
            //-----------------------------------------------------------------
            // Remove the daemon from the list of signal handlers
            //-----------------------------------------------------------------
            ResetOnSignal( false );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        const OptionSet&
        Daemon::Options( )
        {
            return m_options;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        int
        Daemon::ServerPort( ) const
        {
            return ( server_info.Port( ) );
        }

        //-------------------------------------------------------------------
        /// \brief Register signal handler
        //-------------------------------------------------------------------
        void
        Daemon::ResetOnSignal( bool Value )
        {
            using LDASTools::AL::SignalHandler;

            static const char* method = "Daemon::ResetOnSignal";

            if ( Value )
            {
                QUEUE_LOG_MESSAGE( "Registering SIGNAL_HANGUP handler",
                                   MT_NOTE,
                                   0,
                                   method,
                                   "CXX" );
                SignalHandler::Register( this, REQUEST_RELOAD );
                SignalCapture( REQUEST_RELOAD );
            }
            else
            {
                QUEUE_LOG_MESSAGE( "Unregistering SIGNAL_HANGUP handler",
                                   MT_NOTE,
                                   0,
                                   method,
                                   "CXX" );
                SignalIgnore( REQUEST_RELOAD );
                SignalHandler::Unregister( this, REQUEST_RELOAD );
            }
        }

        void
        Daemon::SignalCallback( signal_type Signal )
        {
            static const char* method = "Daemon::SignalCallback";

            QUEUE_LOG_MESSAGE( "Calling Daemon::SignalCallback"
                                   << " with signal: " << Signal,
                               MT_NOTE,
                               0,
                               method,
                               "CXX" );
            switch ( Signal )
            {
            case REQUEST_RELOAD:
                //---------------------------------------------------------------
                // Start the process of resetting
                //---------------------------------------------------------------
                reset_requested = true;
                break;
            case LDASTools::AL::SignalHandler::SIGNAL_TERMINATE:
                //---------------------------------------------------------------
                // Start the process of resetting
                //---------------------------------------------------------------
                finished = true;
                break;
            default:
                // Nothing
                break;
            }
        }

        void
        Daemon::do_client_request( )
        {
            static const char* method_name =
                "diskCache::MetaCommand::Daemon::do_client_request";

            if ( reset_requested )
            {
                reset( );
            }
            if ( server->good( ) )
            {
                try
                {
                    fd_set          sset;
                    struct timespec timeout;

                    timeout.tv_sec = 1;
                    timeout.tv_nsec = 0;

                    FD_ZERO( &sset );
                    FD_SET( server->handle( ), &sset );

                    int stat = pselect( server->handle( ) + 1,
                                        &sset,
                                        (fd_set*)NULL,
                                        (fd_set*)NULL,
                                        &timeout,
                                        (const sigset_t*)NULL );

                    if ( stat < 0 )
                    {
                        return;
                    }
                    if ( FD_ISSET( server->handle( ), &sset ) )
                    {
                        QUEUE_LOG_MESSAGE( "Server: waiting for client request",
                                           MT_DEBUG,
                                           30,
                                           method_name,
                                           "CXX" );
                        client = server->accept( );

                        if ( client->good( ) )
                        {
                            Spawn( );
                            Join( );
                        }

                        QUEUE_LOG_MESSAGE( "Server: client request completed",
                                           MT_DEBUG,
                                           30,
                                           method_name,
                                           "CXX" );
                        client.reset(
                            (server_responce_type::element_type*)NULL );
                    }
                }
                catch ( ... )
                {
                }
            }
        }

        bool
        Daemon::process_cmd( CommandLineOptions& Options )
        {
            static const char* caller =
                "diskCAche::MetaCommand::Daemon::process_cmd";

            bool retval = true;

            QUEUE_LOG_MESSAGE( "ENTRY:"
                                   << " Command: " << Options.ProgramName( ),
                               MT_DEBUG,
                               10,
                               caller,
                               "CXX" );

            switch ( CommandTable::Lookup( Options.ProgramName( ) ) )
            {
            case CommandTable::CMD_QUIT:
            {
                //-------------------------------------------------------------
                // Quit the current contents of the cache
                //-------------------------------------------------------------
                MetaCommand::Quit  cmd( Options, server_info );
                std::ostringstream msg;

                cmd.ClientHandle( client );

                if ( finished )
                {
                    msg << "Daemon is already shutting down";
                }
                else
                {
                    msg << "Requested daemon to shut down";
                }
                cmd.msg = msg.str( );
                finished = true;
            }
            break;
            case CommandTable::CMD_DUMP:
            {
                //-------------------------------------------------------------
                // Dump the current contents of the cache
                //-------------------------------------------------------------
                MetaCommand::Dump cmd( Options, server_info );

                cmd.ClientHandle( client );

                cmd( );
            }
            break;
            case CommandTable::CMD_FILENAMES:
            {
                MetaCommand::Filenames cmd( Options, server_info );

                cmd.ClientHandle( client );

                cmd( );
            }
            break;
            case CommandTable::CMD_FILENAMES_RDS:
            {
                MetaCommand::FilenamesRDS cmd( Options, server_info );

                cmd.ClientHandle( client );

                cmd( );
            }
            break;
            case CommandTable::CMD_INTERVALS:
            {
                MetaCommand::Intervals cmd( Options, server_info );

                cmd.ClientHandle( client );

                cmd( );
            }
            break;
            case CommandTable::CMD_RECONFIGURE:
            {
                //-------------------------------------------------------------
                // Request the daemon to re-read configuration file
                //-------------------------------------------------------------
                MetaCommand::Reconfigure cmd( Options, server_info );

                cmd.ClientHandle( client );
                cmd.File( configuration_filename );

                reset( );

                cmd( );
            }
            break;
            case CommandTable::CMD_STATUS:
            {
                //-------------------------------------------------------------
                // Retrieve status information
                //-------------------------------------------------------------
                MetaCommand::Status cmd( Options, server_info );

                cmd.ClientHandle( client );

                cmd( );
            }
            break;
            default:
            {
                QUEUE_LOG_MESSAGE(
                    "Command unsupported: " << Options.ProgramName( ),
                    MT_NOTE,
                    0,
                    caller,
                    "CXX" );
                retval = false;
            }
            break;
            }
            QUEUE_LOG_MESSAGE( "EXIT:", MT_DEBUG, 10, caller, "CXX" );
            return retval;
        }

        bool
        Daemon::read_command( char* Buffer, size_t BufferSize )
        {
            bool retval = false;

            if ( !server )
            {
                std::cin.clear( );
                std::cin.getline( Buffer, BufferSize, '\n' );
                retval = ( ( std::cin.good( ) ) ? true : false );
            }
            return retval;
        }

        inline void
        Daemon::cache_write_delay( const std::string& Value )
        {
            diskCache::Common::Variables::Cache(
                Variables::VAR_NAME_CACHE_WRITE_DELAY, Value );
        }

        inline void
        Daemon::set_concurrency( const std::string& Value )
        {
            diskCache::Common::Variables::Cache(
                Variables::VAR_NAME_CONCURRENCY, Value );
        }

        inline void
        Daemon::set_scan_interval( const std::string& Value )
        {
            diskCache::Common::Variables::Cache(
                Variables::VAR_NAME_SCAN_INTERVAL, Value );
        }

        inline void
        Daemon::set_stat_timeout( const std::string& Value )
        {
            std::istringstream s( Value );

            s >> stat_timeout;
        }

        inline void
        Daemon::set_output_ascii( const std::string& Value )
        {
            diskCache::Common::Variables::Cache(
                Variables::VAR_NAME_OUTPUT_ASCII, Value );
        }

        inline void
        Daemon::set_output_binary( const std::string& Value )
        {
            diskCache::Common::Variables::Cache(
                Variables::VAR_NAME_OUTPUT_BINARY, Value );
        }

        inline void
        Daemon::set_output_binary_version( const std::string& Value )
        {
            diskCache::Common::Variables::Cache(
                Variables::VAR_NAME_OUTPUT_BINARY_VERSION, Value );
        }

        inline void
        Daemon::set_server_port( const std::string& Value )
        {
            std::istringstream s( Value );

            ClientServerInterface::ServerInfo::port_type p;
            s >> p;

            const_cast< ClientServerInterface::ServerInfo& >( server_info )
                .Port( p );
        }

        void
        Daemon::operator( )( )
        {
            static const char* caller = "Daemon::operator()()";
            //-----------------------------------------------------------------
            // Setup to handle termination signal from the main thread
            //   else we risk not being able to properly terminate the program.
            //-----------------------------------------------------------------
            daemon_mode = true;

            QUEUE_LOG_MESSAGE( "Registering SIGNAL_TERMINATE handler",
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );
            LDASTools::AL::SignalHandler::Register(
                this, LDASTools::AL::SignalHandler::SIGNAL_TERMINATE );
            SignalCapture( LDASTools::AL::SignalHandler::SIGNAL_TERMINATE );

            ResetOnSignal( true );
            WaitOnMountPointsAvailable( );
            ScanMountPointListContinuously( );
            DumpCacheDaemonStart( );

            //-----------------------------------------------------------------
            // Check to see if the server port has been requested
            //-----------------------------------------------------------------
            if ( server_info.Port( ) >= 0 )
            {
                server = server_type( new server_type::element_type( ) );
                if ( server )
                {
                    server->open( server_info.Port( ) );
                }
            }

            //-----------------------------------------------------------------
            // Read commands to be executed.
            //-----------------------------------------------------------------
            if ( server )
            {
                while ( ( finished == false ) && ( server->good( ) ) )
                {
                    do_client_request( );
                }
            }
            else
            {
                char line_buffer[ 2048 ];

                std::fill(
                    line_buffer, line_buffer + sizeof( line_buffer ), '\0' );
                while ( finished == false )
                {
                    if ( read_command( line_buffer, sizeof( line_buffer ) ) )
                    {
                        //-----------------------------------------------------------
                        // Switch on the command specified by the user
                        //-----------------------------------------------------------
                        CommandLineOptions line_options(
                            convert_line_to_options( line_buffer ) );

                        process_cmd( line_options );
                    }
                }
            }
        }

        void
        Daemon::action( )
        {
            char line_buffer[ 2048 ];

            INT_4U bytes;

            ( *client ) >> bytes;
            if ( bytes >= sizeof( line_buffer ) )
            {
            }
            else
            {
                client->read( line_buffer, bytes );
                line_buffer[ bytes ] = '\0';

                //---------------------------------------------------------------
                // Create the command line option
                //---------------------------------------------------------------
                CommandLineOptions line_options(
                    convert_line_to_options( line_buffer ) );

                process_cmd( line_options );
            }
        }

        //-------------------------------------------------------------------
        /// Setup the variables according to the requested configuration.
        /// Some of the variables can be reset by modifying the
        /// configuration file and then signaling the daemon process.
        //-------------------------------------------------------------------
        void
        Daemon::setup_variables( int Mask )
        {
            variables.Setup( Mask );
        }

        //-------------------------------------------------------------------
        /// Resetting of the daemon process forces the rereading of
        /// vital configuration information without having to restart
        /// the process.
        //-------------------------------------------------------------------
        void
        Daemon::reset( )
        {
            static const char* method_name =
                "diskCache::MetaCommand::Daemon::reset";
            if ( reset_requested == false )
            {
                reset_requested = true;
                return;
            }

            reset_requested = false;
            //-----------------------------------------------------------------
            // Re-Read the configuration information
            //-----------------------------------------------------------------
            if ( !configuration_filename.empty( ) )
            {
                QUEUE_LOG_MESSAGE(
                    "Rereading configuration file: " << configuration_filename,
                    MT_NOTE,
                    0,
                    method_name,
                    "CXX" );
                std::ifstream stream( configuration_filename.c_str( ) );

                if ( stream.is_open( ) )
                {
                    DaemonConfig_ config( *this );

                    variables.ParseConfigurationFile( stream, config );
                }
            }
            setup_variables( HOT_VARIABLES );
        }
    } // namespace MetaCommand
} // namespace diskCache
