//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <time.h>

#include "ldastoolsal/MemChecker.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/HotDirectory.hh"

#include "DirectoryManagerSingleton.hh"
#include "MountPointScanner.hh"

using LDASTools::AL::ConditionalVariable;
using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::TaskThread;
using LDASTools::AL::ThreadPool;

using diskCache::Cache::HotDirectory;

static bool initialized = false;

namespace diskCache
{
    //=====================================================================
    //=====================================================================
    MountPointScanner::ScanResults::ScanResults( )
        : m_count_baton( ), m_directory_count( 0 ), m_file_count( 0 ),
          m_mount_point_count( 0 )
    {
    }

    //=====================================================================
    //=====================================================================

    MutexLock::baton_type MountPointScanner::SyncRequest::p_request_count_baton;
    MountPointScanner::SyncRequest::request_count_type
        MountPointScanner::SyncRequest::p_request_count = 0;

    MountPointScanner::SyncRequest::SyncRequest( )
    {
        static const char* caller =
            "diskCache::MountPointScanner::SyncRequest::SyncRequest";
        MutexLock l( p_request_count_baton, __FILE__, __LINE__ );

        ++p_request_count;

        std::ostringstream msg;

        msg << "There are now " << p_request_count << " pending request(s)";
        GenericAPI::queueLogEntry( msg.str( ),
                                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                   30,
                                   caller,
                                   "IDLE" );
    }

    MountPointScanner::SyncRequest::~SyncRequest( )
    {
        static const char* caller =
            "diskCache::MountPointScanner::SyncRequest::~SyncRequest";
        MutexLock l( p_request_count_baton, __FILE__, __LINE__ );

        if ( p_request_count > 0 )
        {
            --p_request_count;
        }

        std::ostringstream msg;

        msg << "There are now " << p_request_count << " pending request(s)";
        GenericAPI::queueLogEntry( msg.str( ),
                                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                   30,
                                   caller,
                                   "IDLE" );
    }

    MountPointScanner::SyncRequest::request_count_type
    MountPointScanner::SyncRequest::PendingRequests( )
    {
        static const char* caller =
            "diskCache::MountPointScanner::SyncRequest::PendingRequests";

        MutexLock l( p_request_count_baton, __FILE__, __LINE__ );

        std::ostringstream msg;

        msg << "There are now " << p_request_count << " pending request(s)";
        GenericAPI::queueLogEntry( msg.str( ),
                                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                   50,
                                   caller,
                                   "IDLE" );

        return p_request_count;
    }

    //=====================================================================
    //=====================================================================
    MutexLock::baton_type               MountPointScanner::m_variable_baton;
    ConditionalVariable                 MountPointScanner::m_scan_completed;
    MutexLock::baton_type               MountPointScanner::m_active_count_baton;
    MountPointScanner::concurrency_type MountPointScanner::m_active_count = 0;
    MountPointScanner::concurrency_type MountPointScanner::m_concurrency = 10;
    MutexLock::baton_type MountPointScanner::m_first_scan_completed_baton;
    INT_4U                MountPointScanner::m_first_scan_completed = 0;
    MutexLock::baton_type MountPointScanner::m_mount_point_cache_reset_baton;
    bool                  MountPointScanner::m_mount_point_cache_reset = false;

    MountPointScanner::scanner_sync_type MountPointScanner::p_scanner_sync =
        true;
    MountPointScanner::scanner_sync_rw_type::baton_type
        MountPointScanner::p_scanner_sync_baton;

    //---------------------------------------------------------------------
    /// This method allows for cooperative thread cancellation.
    /// Threads that may block the scanning of the mount points,
    /// should periodicly check if a request to terminate the current
    /// scan has been made.
    /// This allows threads to cancel themselves when it may be easier
    /// to perform any cleanup that needs to occur.
    //---------------------------------------------------------------------
    bool
    MountPointScanner::CancellationRequest( std::string& Reason )
    {
        std::ostringstream msg;
        bool               retval = false;

        if ( retval == false )
        {
            MutexLock l1( m_mount_point_cache_reset_baton, __FILE__, __LINE__ );

            if ( m_mount_point_cache_reset == true )
            {
                msg << "Cancelling because a request has been made to rebuild "
                       "the database.";
                retval = true;
            }
        }
        if ( retval == false )
        {
            const int r = SyncRequest::PendingRequests( );

            if ( r > 0 )
            {
                msg << "Cancellation because " << r << " request"
                    << ( ( r == 1 ) ? " is" : "s are" ) << " pending";
                retval = true;
            }
        }
        if ( retval == false )
        {
            if ( MemChecker::IsExiting( ) == true )
            {
                msg << "Cancelling because the system is shuting down";
            }
        }

        if ( retval )
        {
            Reason = msg.str( );
        }
        return ( retval );
    }

    void
    MountPointScanner::OnCompletion( int TaskThreadState )
    {
#if WORKING
        static const char caller[] = "MountPointScanner::onCompletion";
#endif /* WORKING */

        if ( ( Results( ).empty( ) == false ) &&
             ( MemChecker::IsExiting( ) == false ) )
        {
            /// \todo
            ///     Report scanning results
#if WORKING
            if ( m_stream )
            {
                MutexLock lock( m_scan_completed.Mutex( ), __FILE__, __LINE__ );

                ( *m_stream ) << Results( ) << std::endl;
            }
            else
            {
                GenericAPI::queueLogEntry(
                    Results( ),
                    GenericAPI::LogEntryGroup_type::MT_NOTE,
                    10,
                    caller,
                    "SCAN_MOUNTPT" );
            }
#endif /* WORKING */
        }
        if ( m_controller )
        {
            ThreadPool::Relinquish( m_controller );
            m_controller = (TaskThread*)NULL;
        }
    }

    void
    MountPointScanner::Scan( const mount_point_container_type& MountPoints,
                             ScanResults&                      Answer )
    {
        static const char* caller = "MountPointScanner::Scan";

        //-----------------------------------------------------------
        // Start recording when the start of this action
        //-----------------------------------------------------------
        Answer.Start( );

        //-----------------------------------------------------------
        // Acquire the sync lock
        //-----------------------------------------------------------
        scanner_sync_ro_type slock( SyncRO( ) );

        if ( initialized == false )
        {
            MemChecker::Append(
                on_exit, "diskCache::MountPointScanner::on_exit", 200, true );
            initialized = true;
        }
        mount_point_container_type::const_iterator cur = MountPoints.begin( ),
                                                   last = MountPoints.end( );

        MountPointScanner::CriticalSection( true );
        while ( ( ( cur != last ) || ( Active( ) > 0 ) ) )
        {
            //---------------------------------------------------------
            // Check to see if scanning should be stopped.
            //---------------------------------------------------------
            if ( ( cur != last ) &&
                 ( ( SyncRequest::PendingRequests( ) > 0 ) ||
                   ( mount_point_reset( ) ) ||
                   ( MemChecker::IsExiting( ) == true ) ) )
            {
                QUEUE_LOG_MESSAGE(
                    "Stopping the scan: "
                        << " SyncRequest::PendingRequests( ) > 0: "
                        << ( SyncRequest::PendingRequests( ) > 0 )
                        << " mount_point_reset( ): " << ( mount_point_reset( ) )
                        << " MemChecker::IsExiting( ) == true: "
                        << ( MemChecker::IsExiting( ) == true ),
                    MT_NOTE,
                    0,
                    caller,
                    "CXX" );
                cur = last;
            }
            //---------------------------------------------------------
            // Check to see if there is room to start another scanning
            // thread.
            //---------------------------------------------------------
            while ( ( Active( ) < Concurrency( ) ) && ( cur != last ) )
            {
                {
                    std::ostringstream msg;

                    msg << "Adding " << *cur << " to be scanned";
                    GenericAPI::queueLogEntry(
                        msg.str( ),
                        GenericAPI::LogEntryGroup_type::MT_DEBUG,
                        30,
                        caller,
                        "IDLE" );
                }
                start( *cur, Answer );
                ++cur; // Advance to the mount point to scan
                {
                    std::ostringstream msg;

                    msg << "Of the " << Concurrency( ) << " available scanners,"
                        << " " << Active( ) << " are active";
                    GenericAPI::queueLogEntry(
                        msg.str( ),
                        GenericAPI::LogEntryGroup_type::MT_DEBUG,
                        30,
                        caller,
                        "IDLE" );
                }
            }
            //---------------------------------------------------------
            // Wakeup once a thread has completed.
            //---------------------------------------------------------
            // MountPointScanner::CriticalSection( true );
            if ( Active( ) )
            {
                MountPointScanner::Wait( );
            }
            // MountPointScanner::CriticalSection( false );
        }
        if ( mount_point_reset( ) )
        {
            //---------------------------------------------------------
            // Check to see if the mount point cache is to be rebuilt.
            // This needs to be above lock acquisition since the
            // MountPointHash needs to know we are not going to be
            // updating while it is trying to remove.
            //---------------------------------------------------------
            const mtime_type bound( HotDirectory::HotLowerBound( ) );

            HotDirectory::HotLowerBound( 0 );

            FirstScanSet( false );
            try
            {
                HotDirectory::Reset( );
#if WORKING
                DirectoryHash::Instance( ).removeDirectories( );
                MountPointHash::Reset( );
#endif /* WORKING */
                HotDirectory::HotLowerBound( bound );
            }
            catch ( ... )
            {
                HotDirectory::HotLowerBound( bound );
                throw;
            }
            mount_point_reset( false );
            MountPointManagerSingleton::Reset(
                MountPointManagerSingleton::RESET_CACHE );
        }
        else
        {
            FirstScanSet( true );
            QUEUE_LOG_MESSAGE( "Completed "
                                   << ScanIteration( ) << " scan"
                                   << ( ( ScanIteration( ) == 1 ) ? "" : "s" )
                                   << " through the mount point list",
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );
        }
        MountPointScanner::CriticalSection( false );
        Answer.Stop( );
    }

    void
    MountPointScanner::operator( )( )
    {
        static const char caller[] = "MountPointScanner::operator()";
        try
        {
            DirectoryManagerSingleton::ScanResults r;

            DirectoryManagerSingleton::Scan( m_mount_point, r );
            m_scan_results.FileCountInc( r.FileCount( ) );
            m_scan_results.DirectoryCountInc( r.DirectoryCount( ) );
            m_scan_results.MountPointCountInc( );
        }
        catch ( std::exception& Exception )
        {
            m_results = Exception.what( );
            QUEUE_LOG_MESSAGE( "While scanning: " << m_mount_point
                                                  << " Exception: "
                                                  << m_results,
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );
        }
        catch ( ... )
        {
            m_results = "MountPointScanner received an unknown exception";
            QUEUE_LOG_MESSAGE( "While scanning: " << m_mount_point
                                                  << " Exception: "
                                                  << m_results,
                               MT_NOTE,
                               0,
                               caller,
                               "CXX" );
        }
        CriticalSection( true );
        m_scan_completed.Broadcast( );
        CriticalSection( false );
    }

    void
    MountPointScanner::on_exit( )
    {
        //----------------------------------------------------------------
        // Wait till the scanner daemon has finished
        //----------------------------------------------------------------
        while ( Active( ) > 0 )
        {
            static const struct timespec pause = { 1, 0 };
            (void)nanosleep( &pause, (struct timespec*)NULL );
        }
    }

    inline void
    MountPointScanner::start( const std::string& MountPoint,
                              ScanResults&       Answer )
    {
        if ( MemChecker::IsExiting( ) == true )
        {
            return;
        }

        TaskThread*        thread = ThreadPool::Acquire( );
        MountPointScanner* task =
            new MountPointScanner( MountPoint, Answer, thread );

        try
        {
            thread->AddTask( task );
        }
        catch ( ... )
        {
            throw;
        }
    }

} // namespace diskCache
