//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__DUMP_CACHE_DAEMON_HH
#define DISKCACHE_API__DUMP_CACHE_DAEMON_HH

#include <iosfwd>
#include <string>

#include "ldastoolsal/AtExit.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/Task.hh"
#include "ldastoolsal/TaskThread.hh"
#include "ldastoolsal/ThreadPool.hh"
#include "ldastoolsal/types.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/Symbols.hh"

#include "Streams/StreamsInterface.hh"

namespace diskCache
{
    //=============================================================================
    /// \brief Continuous dump the cache to media
    //=============================================================================
    class DumpCacheDaemon : public LDASTools::AL::Task
    {
    public:
        typedef INT_4U                                      interval_type;
        typedef LDASTools::AL::MutexLockVariable< bool >    io_lock_type;
        typedef diskCache::Streams::Interface::version_type version_type;
        typedef LDASTools::AL::TaskThread                   thread_type;

        //---------------------------------------------------------------------------
        /// \class HASH_FILENAME_ASCII
        ///
        /// \brief This provides thread safe methods for the global variable.
        ///
        /// This is the name used when outputting the hash information as an
        /// ASCII file.
        ///
        /// This class provides the necessary wrapping of the global variable to
        /// allow it to be used in a multi-threaded environment in a thread safe
        /// manor.
        //---------------------------------------------------------------------------
        // \typedef std::string value_type
        //
        // \brief The type of the global variable.
        //
        // This is useful for code portability.
        // Functions and mehtods that need access to this global variable,
        // should use this typdef when reserving space for the Get method.
        //---------------------------------------------------------------------------
        // \fn static void HASH_FILENAME_ASCII::Get( value_type& Value )
        //
        // \brief Retrieve the value of the global variable.
        //
        // This method retrieve the value of the global variable
        // in a thread safe manor.
        //
        // \param[out] Value
        //     Storage reserved by the caller to receive a copy of the global
        //     variable.
        //---------------------------------------------------------------------------
        // \fn static void HASH_FILENAME_ASCII::Set( value_type& Value )
        //
        // \brief Retrieve the value of the global variable.
        //
        // This method sets the value of the global variable
        // in a thread safe manor.
        //
        // \param[in] Value
        //     The new value for the global variable.
        //---------------------------------------------------------------------------

        SYMBOL_CLASS_DECL( HASH_FILENAME_ASCII, std::string );

        //---------------------------------------------------------------------------
        /// \class HASH_FILENAME_BINARY
        ///
        /// \brief This provides thread safe methods for the global variable.
        ///
        /// This is the name used when outputting the hash information as an
        /// binary file.
        ///
        /// This class provides the necessary wrapping of the global variable to
        /// allow it to be used in a multi-threaded environment in a thread safe
        /// manor.
        //---------------------------------------------------------------------------
        // \typedef std::string value_type
        //
        // \brief The type of the global variable.
        //
        // This is useful for code portability.
        // Functions and mehtods that need access to this global variable,
        // should use this typdef when reserving space for the Get method.
        //---------------------------------------------------------------------------
        // \fn static void HASH_FILENAME_BINARY::Get( value_type& Value )
        //
        // \brief Retrieve the value of the global variable.
        //
        // This method retrieve the value of the global variable
        // in a thread safe manor.
        //
        // \param[out] Value
        //     Storage reserved by the caller to receive a copy of the global
        //     variable.
        //---------------------------------------------------------------------------
        // \fn static void HASH_FILENAME_BINARY::Set( value_type& Value )
        //
        // \brief Retrieve the value of the global variable.
        //
        // This method sets the value of the global variable
        // in a thread safe manor.
        //
        // \param[in] Value
        //     The new value for the global variable.
        //---------------------------------------------------------------------------

        SYMBOL_CLASS_DECL( HASH_FILENAME_BINARY, std::string );

        //---------------------------------------------------------------------------
        /// \class INTERVAL
        ///
        /// \brief This provides thread safe methods for the global variable.
        ///
        /// This value governs the fequency with which the ASCII and binary
        /// files are written to disk.
        ///
        /// Its units is milliseconds.
        ///
        /// This class provides the necessary wrapping of the global variable to
        /// allow it to be used in a multi-threaded environment in a thread safe
        /// manor.
        //---------------------------------------------------------------------------
        // \typedef INT_4U value_type
        //
        // \brief The type of the global variable.
        //
        // This is useful for code portability.
        // Functions and mehtods that need access to this global variable,
        // should use this typdef when reserving space for the Get method.
        //---------------------------------------------------------------------------
        // \fn static void INTERVAL::Get( value_type& Value )
        //
        // \brief Retrieve the value of the global variable.
        //
        // This method retrieve the value of the global variable
        // in a thread safe manor.
        //
        // \param[out] Value
        //     Storage reserved by the caller to receive a copy of the global
        //     variable.
        //---------------------------------------------------------------------------
        // \fn static void INTERVAL::Set( value_type& Value )
        //
        // \brief Retrieve the value of the global variable.
        //
        // This method sets the value of the global variable
        // in a thread safe manor.
        //
        // \param[in] Value
        //     The new value for the global variable.
        //---------------------------------------------------------------------------

        SYMBOL_CLASS_DECL_BY_VALUE( INTERVAL, INT_4U );

        DumpCacheDaemon( );

        static version_type ASCIIVersion( );

        static void ASCIIVersion( version_type Value );

        static version_type BinaryVersion( );

        static void BinaryVersion( version_type Value );

        static std::string FilenameAscii( );

        static void FilenameAscii( const std::string& Value );

        static std::string FilenameBinary( );

        static void FilenameBinary( const std::string& Value );

        static io_lock_type& IOLock( );

        static interval_type Interval( );

        static void Interval( interval_type Value );

        static void Start( );

        virtual void operator( )( );

        //---------------------------------------------------------------------------
        /// \brief Extension to use when a cache syncronization request is made.
        //---------------------------------------------------------------------------
        static const char* const RESYNC_EXTENSION;

    private:
        //---------------------------------------------------------------------------
        /// \brief Keep track if the daemon is still active
        //---------------------------------------------------------------------------
        bool active;

        //---------------------------------------------------------------------------
        //
        //---------------------------------------------------------------------------
        static version_type version_ascii;
        //---------------------------------------------------------------------------
        //
        //---------------------------------------------------------------------------
        static version_type version_binary;

        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        static LDASTools::AL::ReadWriteLock::baton_type variable_baton;

        //---------------------------------------------------------------------------
        /// \brief syncronize between the reading and writing of the cache files
        //---------------------------------------------------------------------------
        static LDASTools::AL::MutexLock::baton_type m_io_baton;

        static bool m_io_dummy_var;

        bool is_active( ) const;
    };

    inline DumpCacheDaemon::version_type
    DumpCacheDaemon::ASCIIVersion( )
    {
        LDASTools::AL::ReadWriteLock lock( variable_baton,
                                           LDASTools::AL::ReadWriteLock::READ,
                                           __FILE__,
                                           __LINE__ );

        return version_ascii;
    }

    inline void
    DumpCacheDaemon::ASCIIVersion( version_type Value )
    {
        LDASTools::AL::ReadWriteLock lock( variable_baton,
                                           LDASTools::AL::ReadWriteLock::WRITE,
                                           __FILE__,
                                           __LINE__ );

        version_ascii = Value;
    }

    inline DumpCacheDaemon::version_type
    DumpCacheDaemon::BinaryVersion( )
    {
        LDASTools::AL::ReadWriteLock lock( variable_baton,
                                           LDASTools::AL::ReadWriteLock::READ,
                                           __FILE__,
                                           __LINE__ );

        return version_binary;
    }

    inline void
    DumpCacheDaemon::BinaryVersion( version_type Value )
    {
        LDASTools::AL::ReadWriteLock lock( variable_baton,
                                           LDASTools::AL::ReadWriteLock::WRITE,
                                           __FILE__,
                                           __LINE__ );

        version_binary = Value;
    }

    inline std::string
    DumpCacheDaemon::FilenameAscii( )
    {
        HASH_FILENAME_ASCII::value_type retval;

        HASH_FILENAME_ASCII::Get( retval );

        return retval;
    }

    inline void
    DumpCacheDaemon::FilenameAscii( const std::string& Value )
    {
        HASH_FILENAME_ASCII::Set( Value );
    }

    inline std::string
    DumpCacheDaemon::FilenameBinary( )
    {
        HASH_FILENAME_BINARY::value_type retval;

        HASH_FILENAME_BINARY::Get( retval );

        return retval;
    }

    inline void
    DumpCacheDaemon::FilenameBinary( const std::string& Value )
    {
        HASH_FILENAME_BINARY::Set( Value );
    }

    inline DumpCacheDaemon::io_lock_type&
    DumpCacheDaemon::IOLock( )
    {
        static io_lock_type io_lock(
            m_io_baton, m_io_dummy_var, __FILE__, __LINE__ );
        return io_lock;
    }

    inline DumpCacheDaemon::interval_type
    DumpCacheDaemon::Interval( )
    {
        INTERVAL::value_type retval;

        INTERVAL::Get( retval );

        return retval;
    }

    inline void
    DumpCacheDaemon::Interval( interval_type Value )
    {
        INTERVAL::value_type current;

        INTERVAL::Get( current );

        QUEUE_LOG_MESSAGE( "Changing interval"
                               << " from " << current << " to " << Value,
                           MT_NOTE,
                           0,
                           "DumpCacheDaemon::Interval",
                           "CXX" );
        INTERVAL::Set( Value );
    }

    inline void
    DumpCacheDaemon::Start( )
    {
        using namespace LDASTools::AL;
        //-------------------------------------------------------------------
        /// \todo
        /// Protect from multiple scanner daemons from running.
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        // Get a thread in which to run this task in the background
        //-------------------------------------------------------------------
        thread_type*     thread = ThreadPool::Acquire( );
        DumpCacheDaemon* task = new DumpCacheDaemon( );
        //-------------------------------------------------------------------
        /// \todo Need to setup the task thread to be returned to the pool
        ///     once the task is complete.
        //-------------------------------------------------------------------

        //-------------------------------------------------------------------
        // Add the task to the thread and get things running
        //-------------------------------------------------------------------
        thread->AddTask( task );
    }

    inline bool
    DumpCacheDaemon::is_active( ) const
    {
        LDASTools::AL::ReadWriteLock l( variable_baton,
                                        LDASTools::AL::ReadWriteLock::READ,
                                        __FILE__,
                                        __LINE__ );

        return active;
    }
} // namespace diskCache

#endif /* DISKCACHE_API__DUMP_CACHE_DAEMON_HH */
