//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_THREAD_COUNTER_HH
#define DISKCACHE_THREAD_COUNTER_HH

// General Header Files
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/types.hh"

namespace diskCache
{
    //: Thread counter class.
    //
    // This class is designed to automize thread counter updates.
    //
    class ThreadCounter
    {
    public:
        // Constructor
        ThreadCounter( INT_2S&                                  counter,
                       LDASTools::AL::ReadWriteLock::baton_type lock );

        // Destructor
        ~ThreadCounter( );

    private:
        //: No default constructor
        ThreadCounter( );

        //: No copy constructor
        ThreadCounter( const ThreadCounter& );

        //: No operator=
        const ThreadCounter& operator=( const ThreadCounter& );

        //: Reference to the counter to update.
        INT_2S& mCounter;

        //: Reference to the counter lock.
        LDASTools::AL::ReadWriteLock::baton_type mCounterLock;
    };

} // namespace diskCache

#endif
