//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldastoolsal/MemChecker.hh"
#include "Cache/HotDirectory.hh"

#include "Daemons.hh"
#include "DumpCacheDaemon.hh"
#include "ScanMountPointsDaemon.hh"

using LDASTools::AL::MemChecker;

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< diskCache::Daemon::DumpCache > )
SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< diskCache::Daemon::ScanMountPoints > )

namespace
{
    void
    on_exit_dump_cache( )
    {
        diskCache::Daemon::DumpCache::Terminate( );
    }

    void
    on_exit_scan_mount_points( )
    {
        diskCache::Daemon::ScanMountPoints::Terminate( );
    }
} // namespace

namespace diskCache
{
    namespace Daemon
    {
        //===================================================================
        //===================================================================
        DumpCache::DumpCache( )
        {
            MemChecker::Append(
                on_exit_dump_cache, "diskCache::Daemon::DumpCache", 110, true );
        }

        DumpCache::~DumpCache( )
        {
        }

        void
        DumpCache::Launch( )
        {
            Instance( ).Start( );
        }

        DumpCache::task_type*
        DumpCache::Task( )
        {
            return new DumpCacheDaemon( );
        }

        void
        DumpCache::Terminate( )
        {
            Instance( ).Stop( );
        }

        //===================================================================
        //===================================================================
        ScanMountPoints::ScanMountPoints( )
        {
            MemChecker::Append( on_exit_scan_mount_points,
                                "diskCache::Daemon::DumpCache",
                                120,
                                true );
        }

        ScanMountPoints::~ScanMountPoints( )
        {
        }

        void
        ScanMountPoints::Launch( )
        {
            Instance( ).Start( );
        }

        ScanMountPoints::task_type*
        ScanMountPoints::Task( )
        {
            return new ScanMountPointsDaemon( (std::ostream*)NULL );
        }

        void
        ScanMountPoints::Terminate( )
        {
            Instance( ).Stop( );
            diskCache::Cache::HotDirectory::StopDaemon( );
        }
    } // namespace Daemon
} // namespace diskCache
