//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include "ldastoolsal/BackgroundTaskCounter.hh"

#define DISKCACHE_CMD_NON_THREADED 1

#include "Streams/ASCII.hh"
#include "Streams/Binary.hh"

#include "diskcachecmd.hh"
#include "DumpCacheDaemon.hh"
#include "MountPointScanner.hh"

using LDASTools::AL::BackgroundTaskCounter;
using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::ReadWriteLock;
using LDASTools::AL::SignalHandler;
using LDASTools::AL::Task;
using LDASTools::AL::TaskThread;
using LDASTools::AL::Thread;
using LDASTools::AL::ThreadPool;

namespace
{
    class DumpTask : public Task
    {
    public:
        enum mode_type
        {
            ASCII,
            BINARY
        };

        typedef diskCache::Streams::Interface::version_type version_type;
        DumpTask( mode_type          Mode,
                  const std::string& Filename,
                  version_type       Version,
                  TaskThread*        Controller );

        static INT_4U Active( );

        static void CriticalSection( bool Value );

        virtual void OnCompletion( int TaskThreadState );

        static void Start( mode_type          Mode,
                           const std::string& Filename,
                           version_type       Version );

        static void Wait( );

        virtual void operator( )( );

    private:
        const mode_type              m_mode;
        const std::string            m_filename;
        const version_type           version;
        TaskThread*                  m_controller;
        static BackgroundTaskCounter m_thread_counter;
    };

    std::string
    mk_task_name( const std::string& Filename, DumpTask::version_type Version )
    {
        std::ostringstream retval;

        retval << "DiskCache::Dump: " << Filename << " Version: " << Version;
        return retval.str( );
    }

    BackgroundTaskCounter DumpTask::m_thread_counter;

    DumpTask::DumpTask( mode_type          Mode,
                        const std::string& Filename,
                        version_type       Version,
                        TaskThread*        Controller )
        : Task( mk_task_name( Filename, Version ) ), m_mode( Mode ),
          m_filename( Filename ), version( Version ), m_controller( Controller )
    {
        delete_on_completion( true );
    }

    inline INT_4U
    DumpTask::Active( )
    {
        return m_thread_counter.Active( );
    }

    inline void
    DumpTask::CriticalSection( bool Value )
    {
        m_thread_counter.CriticalSection( Value );
    }

    void
    DumpTask::OnCompletion( int TaskThreadState )
    {
        if ( m_controller )
        {
            //-----------------------------------------------------------------
            // Put the resource back into the pool for future use
            //-----------------------------------------------------------------
            ThreadPool::Relinquish( m_controller );
            m_controller = (TaskThread*)NULL;
        }
    }

    void
    DumpTask::Start( mode_type          Mode,
                     const std::string& Filename,
                     version_type       Version )
    {
        if ( Filename.empty( ) == false )
        {
            TaskThread* thread = ThreadPool::Acquire( );
            DumpTask*   task = new DumpTask( Mode, Filename, Version, thread );

            m_thread_counter.Increment( );
            try
            {
                thread->AddTask( task );
            }
            catch ( ... )
            {
                m_thread_counter.DecrementOnly( );
                throw;
            }
        }
    }

    void
    DumpTask::operator( )( )
    {
        try
        {
            switch ( m_mode )
            {
            case ASCII:
                ::writeDirCacheAscii( m_filename.c_str( ), version );
                break;
            case BINARY:
                ::writeDirCache( m_filename.c_str( ), version );
                break;
            }
        }
        catch ( ... )
        {
            //-----------------------------------------------------------------
            /// \todo
            /// Generate a log entry describing the error that occurred.
            //-----------------------------------------------------------------
        }
        m_thread_counter.Decrement( );
    }

    inline void
    DumpTask::Wait( )
    {
        m_thread_counter.Wait( );
    }

} // namespace

namespace diskCache
{
    //---------------------------------------------------------------------
    /// This is the name of the ascii version of the in memory hash.
    ///
    /// This file only contains entries of searchable data.
    //---------------------------------------------------------------------
    SYMBOL_CLASS_INIT( DumpCacheDaemon::HASH_FILENAME_ASCII,
                       "frame_cache_dump" );
    //---------------------------------------------------------------------
    /// This is the name of the binary version of the in memory hash.
    ///
    /// This file contains an entry for each directory that has been searched.
    /// It will include directories that contain no searchable data as well
    /// as directories that contain searchable data.
    //---------------------------------------------------------------------
    SYMBOL_CLASS_INIT( DumpCacheDaemon::HASH_FILENAME_BINARY, ".frame.cache" );
    //---------------------------------------------------------------------
    /// This variable specifies the number of milliseconds of delay between
    /// writes of the memory cache to the storage media.
    ///
    /// The default value is 120000 ( 2 min. ).
    //---------------------------------------------------------------------
    SYMBOL_CLASS_INIT( DumpCacheDaemon::INTERVAL, 120000 );

    //---------------------------------------------------------------------
    /// The extension that is appended to the filename to generate
    /// dump files that reflect a system that is in the process of
    /// resyncing the in memory cache with information located by
    /// the list of mount points.
    //---------------------------------------------------------------------
    const char* const DumpCacheDaemon::RESYNC_EXTENSION = "#Resync#";

    DumpCacheDaemon::version_type DumpCacheDaemon::version_ascii =
        diskCache::Streams::OASCII::VERSION_DEFAULT;
    DumpCacheDaemon::version_type DumpCacheDaemon::version_binary =
        diskCache::Streams::OBinary::VERSION_DEFAULT;
    ReadWriteLock::baton_type DumpCacheDaemon::variable_baton;

    MutexLock::baton_type DumpCacheDaemon::m_io_baton;
    bool                  DumpCacheDaemon::m_io_dummy_var = false;

    DumpCacheDaemon::DumpCacheDaemon( )
        : Task( "DiskCache::DumpCacheDaemon" ), active( true )
    {
        delete_on_completion( true );
    }

    void
    DumpCacheDaemon::operator( )( ) try
    {
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        static LDASTools::AL::GPSTime last_reported_update;
        static LDASTools::AL::GPSTime last_reported_rebuild_update;
        static LDASTools::AL::GPSTime last_hash_update;

        //-------------------------------------------------------------------
        // Dump while the application is running.
        //-------------------------------------------------------------------
        DumpTask::CriticalSection( true );
        while ( ( is_active( ) ) && ( MemChecker::IsExiting( ) == false ) )
        {
            const bool  first_pass( MountPointScanner::FirstScanComplete( ) ==
                                   false );
            bool        output_files( false );
            std::string ascii_name;
            std::string binary_name;

            //-----------------------------------------------------------------
            // Be cooperative with cancelation process
            //-----------------------------------------------------------------
            Thread::Self::CancellationCheck(
                "diskCache::DumpCacheDaemon::operator()", __FILE__, __LINE__ );

            /// \todo
            ///     Need to keep track of this information within the
            ///     MountPointScanner??
#if WORKING
            last_hash_update = MountPointHash::LastUpdate( );
#endif /* WORKING */
            if ( first_pass )
            {
                if ( ( last_reported_rebuild_update <= last_hash_update ) ||
                     ( last_reported_rebuild_update ==
                       LDASTools::AL::GPSTime( ) ) ||
                     ( last_hash_update == LDASTools::AL::GPSTime( ) ) )
                {
                    last_reported_rebuild_update = last_hash_update;
                    ascii_name = FilenameAscii( );
                    ascii_name += RESYNC_EXTENSION;
                    binary_name = FilenameBinary( );
                    binary_name += RESYNC_EXTENSION;
                    output_files = true;
                }
            }
            else
            {
                if ( ( last_reported_update <= last_hash_update ) ||
                     ( last_reported_update == LDASTools::AL::GPSTime( ) ) ||
                     ( last_hash_update == LDASTools::AL::GPSTime( ) ) )
                {
                    last_reported_update = last_hash_update;
                    ascii_name = FilenameAscii( );
                    binary_name = FilenameBinary( );
                    output_files = true;
                }
            }
            if ( output_files )
            {
                //---------------------------------------------------------------
                // Get exclusive i/o rights
                //---------------------------------------------------------------
                io_lock_type io_lock( IOLock( ), __FILE__, __LINE__ );

                //---------------------------------------------------------------
                // This is a dirty cache which needs to be written to backup
                // media
                //---------------------------------------------------------------
                DumpTask::Start( DumpTask::ASCII, ascii_name, version_ascii );
                DumpTask::Start(
                    DumpTask::BINARY, binary_name, version_binary );
                //---------------------------------------------------------------
                // Wait for the tasks to complete
                //---------------------------------------------------------------
                while ( DumpTask::Active( ) )
                {
                    DumpTask::Wait( );
                }
            }
            //-----------------------------------------------------------------
            // Check if there is a request to halt.
            //-----------------------------------------------------------------
            if ( !( is_active( ) ) || ( MemChecker::IsExiting( ) ) )
            {
                //---------------------------------------------------------------
                // Release critical resources
                //---------------------------------------------------------------
                DumpTask::CriticalSection( false );
                break;
            }
            //-----------------------------------------------------------------
            /// Suspend for a period of time
            //-----------------------------------------------------------------
            DumpTask::CriticalSection( false );
            const interval_type i( Interval( ) );

            struct timespec wakeup;
            wakeup.tv_sec = i / 1000;
            wakeup.tv_nsec = ( i % 1000 ) * 1000000;
            //-----------------------------------------------------------------
            // Establish signal cancelation so as to interupt the system call.
            //-----------------------------------------------------------------
            Thread::Self::CancellationType( Thread::CANCEL_BY_SIGNAL,
                                            SignalHandler::SIGNAL_TERMINATE );
            nanosleep( &wakeup, NULL );
            Thread::Self::CancellationType( Thread::CANCEL_EXCEPTION );
            DumpTask::CriticalSection( true );
        }
        DumpTask::CriticalSection( false );
    }
    catch ( ... )
    {
        DumpTask::CriticalSection( false );
    }

} // namespace diskCache
