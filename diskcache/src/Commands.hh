//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__COMMANDS_HH
#define DISKCACHE_API__COMMANDS_HH

#include <string>

#include "ldastoolsal/ldas_types.h"

#include "diskcacheAPI/Cache/SDGTx.hh"

#include "MountPointManagerSingleton.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer;
    }

    namespace Commands
    {
        //-------------------------------------------------------------------
        /// \brief Debug utility to track down execution time.
        //-------------------------------------------------------------------
        class Timer
        {
        public:
            Timer( );
            void getDelta( const std::string& method );

        private:
#ifdef DEBUG_TIME
            struct timeval mTv;
            INT_4S         mLastSec;
            INT_4S         mLastUsec;
#endif /* DEBUG_TIME */
        };

        //-------------------------------------------------------------------
        //
        //: Obtain the list of file name extensions
        //
        //! usage: set ext_list [ getFileExtList ]
        //
        //! param: none
        //
        //! return: std::string
        //
        const Cache::SDGTx::file_extension_container_type& FileExtList( );

        //-------------------------------------------------------------------
        ///
        /// \brief Get ASCII representation of the current frame hash
        ///
        /// \param[out] Answer
        ///     A collection of files that match the criteria.
        /// \param[in] Ifo
        ///     An ifo to look up. Default is "all".
        /// \param[in] Type
        ///     A type to look up. Default is "all".
        ///
        //-------------------------------------------------------------------
        void getDirCache( diskCache::Cache::QueryAnswer& Answer,
                          const char*                    Ifo = "all",
                          const char*                    Type = "all" );

        //-------------------------------------------------------------------
        /// \brief Get filename based on search criteria
        ///
        /// \param[out] Answer
        ///     A collection of filenames that reflect the search criteria.
        /// \param[in] ifo_type_str
        ///     A space delimited list of IFO-Type strings
        /// \param[in] query_start
        ///     Query start time.
        /// \param[in] query_stop
        ///     Query stop time.
        /// \param[in] Extension
        ///     Query file extension
        //-------------------------------------------------------------------
        void getFileNames( diskCache::Cache::QueryAnswer& Answer,
                           const char*                    ifo_type_str,
                           const unsigned int             query_start,
                           const unsigned int             query_stop,
                           const std::string&             Extension );

        //-------------------------------------------------------------------
        /// \brief Frame query.
        ///
        ///
        /// \param[out] Answer
        ///     A collection of filenames that reflect the search criteria.
        /// \param Ifo
        ///     Data IFO.
        /// \param Type
        ///     Data Type.
        /// \param StartTime
        ///     Data start time.
        /// \param StopTime
        ///     Data stop time.
        /// \param[in] Extension
        ///     Query file extension
        /// \param GapsAllowed
        ///     Flag to indicate if data gaps are allowed.
        ///     Set to true (1) if allowed, to false (0) if not.
        ///
        /// \todo
        ///     Tcl formated string: {frame files} {errors if any}.
        //-------------------------------------------------------------------
        void getFrameFiles( diskCache::Cache::QueryAnswer& Answer,
                            const char*                    Ifo,
                            const char*                    Type,
                            const unsigned int             StartTime,
                            const unsigned int             StopTime,
                            const std::string&             Extension,
                            const bool                     GapsAllowed );

        //-------------------------------------------------------------------
        /// \brief Get TCL formatted lists of intervals for data matching
        ///        the specified and Type within the bounds of the query.
        ///
        /// \param[out] Answer
        ///     List of intervals resulting from the given query.
        /// \param[in] ifo_type_str
        ///     A space delimited list of IFO-Type strings
        /// \param[in] query_start
        ///     Query start time.
        /// \param[in] query_stop
        ///     Query stop time.
        /// \param[in] Extension
        ///     Filename extension
        ///
        /// \return
        ///     A list for each IFO-Type with data intervals:
        ///     IFO-Type1 { i1_start i1_stop ... iN_start iN_stop }
        ///     ... IFO-TypeN { ... }
        //-------------------------------------------------------------------
        void getIntervalsList( diskCache::Cache::QueryAnswer& Answer,
                               const char*                    ifo_type_str,
                               const INT_4U                   query_start,
                               const INT_4U                   query_stop,
                               const std::string&             Extension );

        //-------------------------------------------------------------------
        /// \brief RDS frame query.
        ///
        /// This command finds frame files that satisfy createRDS user request.
        /// If "will_resample" flag is set to TRUE, requested time range will be
        /// automatically expanded by frame dt at the beginning and at the end.
        ///
        /// \param[out] Answer
        ///     A collection of filenames that reflect the search criteria.
        /// \param[in] ifo
        ///     Data IFO.
        /// \param[in] type
        ///     Data Type.
        /// \param[in] start_time
        ///     Data start time.
        /// \param[in] stop_time
        ///     Data stop time.
        /// \param[in] Extension
        ///     Query file extension
        /// \param[in] will_resample
        ///     Flag to indicate if requested data will be
        ///     resampled. Set to TRUE (1) if will be resampled,
        ///     to FALSE (0) if not.
        //-------------------------------------------------------------------
        void getRDSFrameFiles( diskCache::Cache::QueryAnswer& Answer,
                               const char*                    ifo,
                               const char*                    type,
                               const unsigned int             start_time,
                               const unsigned int             stop_time,
                               const std::string&             Extension,
                               const bool                     will_resample );

        //-------------------------------------------------------------------
        //
        //: Update list of file extensions
        //
        //! usage: updateFileExtList [ list ]
        //
        //! param: const CHAR* ext_list - A list of file extensions
        //
        //! return: nothing
        //-------------------------------------------------------------------
        void updateFileExtList(
            const diskCache::Cache::SDGTx::file_extension_container_type&
                Extensions );

        //-------------------------------------------------------------------
        //: Update list of mount point directories (as they appear in
        //: the resource file): initialize list if called at API startup time;
        //:                     otherwise, check if any MOUNT_PT directory
        //:                     has been pruned, remove that directory from
        //:                     global hash.
        //
        //! usage: set dir_list [ updateMountPtList known_mount_pt_dirs
        //! optional_global_check ]
        //
        //! param: const CHAR* dir_list - A list of directories as they appear
        //+       in MOUNT_PT list (API resource file variable).
        //! param: const bool enable_global_check - A flag to indicate if API
        //! should
        //+       check for data conflicts across all MOUNT_PT entries
        //+       (diskcacheAPI@LDAS reports data conflicts only under the same
        //+       MOUNT_PT).
        //+       This flag should be set to TRUE only when called by outside of
        //+       LDAS utility, such as /ldas/bin/cacheCheck.
        //+       Default is FALSE.
        //
        //! return: string - Sorted Tcl list of all removed subdirectories,
        //! followed
        //+        by error messages if any: {dir1 dir2 ...} {error1 error2 ...}
        //
        //-------------------------------------------------------------------
        void updateMountPtList(
            MountPointManagerSingleton::UpdateResults& Status,
            const MountPointManagerSingleton::mount_point_name_container_type&
                       Paths,
            const bool enable_global_check = false );

    } // namespace Commands
} // namespace diskCache

#endif /*  DISKCACHE_API__COMMANDS_HH */
