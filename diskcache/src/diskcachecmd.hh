//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DiskCacheCmdHH
#define DiskCacheCmdHH

// #define DEBUG_TIME

// System Header Files
#include <string>
#include <iostream>

#include "ldastoolsal/types.hh"

#if FOR_TCL
#include "genericAPI/threaddecl.hh"
#include "genericAPI/tid.hh"
#endif /* FOR_TCL */

#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "Streams/StreamsInterface.hh"

#include "DumpCacheDaemon.hh"
#include "MountPointManagerSingleton.hh"
#include "MountPointScanner.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer;
    } // namespace Cache
} // namespace diskCache

using diskCache::DumpCacheDaemon;
using diskCache::MountPointManagerSingleton;
using diskCache::Cache::ExcludedDirectoriesSingleton;

//------------------------------------------------------------------------------
/// \brief Deep scan of a directory.
//
/// This function gets recursive content of the passed to it directory. It
/// should be called only for directories that appear in the MOUNT_PT list.
/// The same function is used to introduce new MOUNT_PT directory or to rescan
/// already existing directory in the global hash for new data.
///
/// \param[in] dirpath
///     A full path to the directory.
///
/// \return
///     A list of all identified and scanned subdirectories
///     that have frame data.
//------------------------------------------------------------------------------
std::string getDirEnt( const std::string dirpath );

//------------------------------------------------------------------------------
/// \brief Get sets of counts for each directory.
///
/// \param[out] Answer
///     Resulting hash numbers based on query parameters.
/// \param[in] Ifo
///     An ifo to look up. Default is "all".
/// \param[in] Type
///     A type to look up. Default is "all".
//
/// \return
///     A list for each mount point with name, number of
///     directories and number of files under that mount point:
///     {mountpoint_name number_of_dirs number_of files }
///
//------------------------------------------------------------------------------
void getHashNumbers( diskCache::Cache::QueryAnswer& Answer,
                     const char*                    Ifo = "all",
                     const char*                    Type = "all" );

//------------------------------------------------------------------------------
//
//: Write content of global frame data hash to binary file.
//
//! param: const char* filename - Name of the file to write frame hash to.
//+       Default is an empty string (C++ will use default file name).
//
//! return: Nothing.
//
void writeDirCache( const char*                                 filename = "",
                    diskCache::Streams::Interface::version_type Version =
                        diskCache::Streams::Interface::VERSION_NONE );
//------------------------------------------------------------------------------
//
//: Write content of global frame data hash to binary file.
//
//! param: const char* filename - Name of the file to write frame hash to.
//+       Default is an empty string (C++ will use default file name).
//
//! return: Nothing.
//
void writeDirCacheAscii( const char* filename = "",
                         diskCache::Streams::Interface::version_type Version =
                             diskCache::Streams::Interface::VERSION_NONE );
//------------------------------------------------------------------------------
//
//: Write content of global frame data hash to binary file.
//
//! param: const char* bfilename - Name of the file to write binary frame hash
//! to.
//+       Default is an empty string (C++ will use default file name).
//
//! param: const char* afilename - Name of the file to write ascii frame hash
//! to.
//+       Default is an empty string (C++ will use default file name).
//
//! return: Nothing.
//
void writeDirCacheFiles( const char* bfilename = "",
                         const char* afilename = "" );

//------------------------------------------------------------------------------
//
//: Read content of global frame data hash from binary file.
//
// Tcl layer can specify different files for read and write operations.
// This function forces all read and write operations to be sequencial.
//
// ATTN: This function destroys existing hash before reading a new one from the
//       given file. Caller must assure there are no running threads that might
//       access global frame data hash at the time "readDirCache" is called.
//
//! param: const char* filename - Name of the file to read frame hash from.
//+       Default is an empty string (C++ will use default file name).
//
//! return: Nothing.
//
void CacheRead( const char* filename = "" );

#if 0
//------------------------------------------------------------------------------
//   
//: Read content of global frame data hash from binary file.   
//
// Tcl layer can specify different files for read and write operations. 
// This function forces all read and write operations to be sequencial.
//   
// ATTN: This function destroys existing hash before reading a new one from the
//       given file. Caller must assure there are no running threads that might
//       access global frame data hash at the time "readDirCache" is called.
//    
//!param: const char* filename - Name of the file to read frame hash from.
//+       Default is an empty string (C++ will use default file name).      
//
//!return: Nothing.
//
void readDirCache( const char* filename = "" );

#endif /* 0 */

//------------------------------------------------------------------------------
//
//: Update list of excluded directories (as they appear in
//: the resource file): check if existing data hash relies on such directories
//:                     already, remove those directories recursively from
//:                     global hash.
//
//! usage: set dir_list [ excludedDirList dirs_to_exclude ]
//
//! param: const CHAR* dir_list - A list of directories to exclude
//+                              (as they appear in API resource file variable).
//
//! return: string - Sorted Tcl list of all removed subdirectories, followed
//+        by error messages if any:
//+        {Directories excluded by pattern 'dir_list': dir1 dir2 ...}
//+        {error1 error2 ...}
//
std::string excludedDirList(
    const ExcludedDirectoriesSingleton::directory_container_type& dir_list );

//------------------------------------------------------------------------------
//
//: Delete global frame data hash.
//
// ATTN: This function destroys existing hash in memory.
//       Caller must assure there are no running threads that might
//       access global frame data hash at the time "deleteDirCache" is called.
//
//! return: Nothing.
//
void deleteDirCache( );

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
void DumpCacheDaemonStart( );

//-----------------------------------------------------------------------
/// \brief stop the currently running Dump Cache Daemon
//-----------------------------------------------------------------------
void DumpCacheDaemonStop( );
//------------------------------------------------------------------------------
/// \brief The number of mount point directories to scan concurrently
//------------------------------------------------------------------------------
void ScanConcurrency( INT_4U Concurrency );

//------------------------------------------------------------------------------
/// \brief Scan the list of mount points.
//------------------------------------------------------------------------------
void ScanMountPointList( diskCache::MountPointScanner::ScanResults& Answer );

//-----------------------------------------------------------------------
/// \brief Continuously scan the list of mount points
//-----------------------------------------------------------------------
void ScanMountPointListContinuously( );

//-----------------------------------------------------------------------
/// \brief Bring the scanner to a close
//-----------------------------------------------------------------------
void ScanMountPointListContinuouslyStop( );

//-----------------------------------------------------------------------
/// \brief Wait till the mount points are available
//-----------------------------------------------------------------------
void WaitOnMountPointsAvailable( );

#endif /* define DiskCacheCmdHH */
