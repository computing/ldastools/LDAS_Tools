//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__STREAMS__ASCII_HH
#define DISKCACHE__STREAMS__ASCII_HH

#if !SWIG
#include <assert.h>

#include <iostream>
#include <set>
#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/reverse.hh"
#include "ldastoolsal/types.hh"

#include "diskcacheAPI/Common/Registry.hh"

#include "diskcacheAPI/Streams/StreamsInterface.hh"
#endif /* ! SWIG */

namespace diskCache
{
    namespace Streams
    {
        //###################################################################
        //###################################################################
        class ASCII
        {
        public:
            typedef INT_8U size_type;
        };

        //###################################################################
        //###################################################################
        class IASCII : public ASCII, public IInterface
        {
        public:
            typedef Common::Registry::id_type       id_type;
            typedef Interface::version_type         version_type;
            typedef std::streamsize                 streamsize;
            typedef boost::shared_ptr< Streamable > read_return_type;

            typedef read_return_type ( *read_func )( IASCII& Stream );

            class SwapError : std::out_of_range
            {
            public:
                SwapError( );
            };

            IASCII( std::istream& Source );
            ~IASCII( );

            bool Readable( ) const;

            static read_func Reader( id_type Id );

            static void Reader( id_type Id, read_func Function );

            template < typename Type >
            IASCII& operator>>( Type& Data );
        };

        //###################################################################
        //###################################################################
        class OASCII : public ASCII, public OInterface
        {
        public:
            typedef INT_4U                    timestamp_type;
            typedef Common::Registry::id_type id_type;
            typedef Interface::version_type   version_type;
            typedef std::streamsize           streamsize;
            typedef void ( *write_func )( OASCII&           Stream,
                                          const Streamable& Data );

            static const version_type VERSION_MAX =
                Interface::VERSION_MULTIPLE_EXTENSIONS;
            static const version_type VERSION_DEFAULT =
                Interface::VERSION_PRE_HEADER;

            OASCII( std::ostream& Sink,
                    version_type  Version = VERSION_DEFAULT );
            ~OASCII( );

            bool Writeable( ) const;

            static write_func Writer( id_type Id );

            static void Writer( id_type Id, write_func Function );

            OASCII& EndL( );

            void Directory( const std::string& Parent );

            const std::string& Directory( ) const;

            void DirectoryModifyTime( timestamp_type Time );

            timestamp_type DirectoryModifyTime( ) const;

            template < typename T, template < typename > class Cont >
            OASCII& operator<<( const Cont< T >& Data );

            template < typename Type >
            OASCII& operator<<( Type Data );

        private:
            friend OASCII& endl( OASCII& Stream );

            typedef LDASTools::AL::unordered_map< id_type, write_func >
                writer_container_type;

            std::ostream& m_stream;

            //-----------------------------------------------------------------
            /// \brief Collection of registered readers
            ///
            /// \todo Need to make this thread safe
            //-----------------------------------------------------------------
            static writer_container_type m_writers;

            std::string    m_directory;
            timestamp_type m_last_time_directory_modified;
        }; // class - OASCII

        inline OASCII&
        OASCII::EndL( )
        {
            m_stream << std::endl;
            return *this;
        }

        inline void
        OASCII::Directory( const std::string& Parent )
        {
            m_directory = Parent;
        }

        inline const std::string&
        OASCII::Directory( ) const
        {
            return m_directory;
        }

        inline void
        OASCII::DirectoryModifyTime( timestamp_type Time )
        {
            m_last_time_directory_modified = Time;
        }

        inline OASCII::timestamp_type
        OASCII::DirectoryModifyTime( ) const
        {
            return m_last_time_directory_modified;
        }

        inline bool
        OASCII::Writeable( ) const
        {
            return m_stream.good( );
        }

        inline OASCII::write_func
        OASCII::Writer( id_type Id )
        {
            writer_container_type::const_iterator pos( m_writers.find( Id ) );

            if ( pos != m_writers.end( ) )
            {
                return pos->second;
            }
            throw std::range_error( "No registered writer for OASCII" );
        }

        inline void
        OASCII::Writer( id_type Id, write_func Function )
        {
            writer_container_type::const_iterator pos( m_writers.find( Id ) );

            if ( pos != m_writers.end( ) )
            {
                return;
            }
            m_writers[ Id ] = Function;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( INT_2U Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( INT_2S Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( INT_4U Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( INT_4S Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( INT_8U Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( INT_8S Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( std::string Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( const char* Data )
        {
            m_stream << Data;
            return *this;
        }

        template <>
        inline OASCII&
        OASCII::operator<<( const std::string& Data )
        {
            m_stream << Data;
            return *this;
        }

        template < typename T, template < typename > class Cont >
        OASCII&
        OASCII::operator<<( const Cont< T >& Data )
        {
            //-----------------------------------------------------------------
            // Record the number of elements
            //-----------------------------------------------------------------
            const size_type s( Data.size( ) );
            *this << s;

            //-----------------------------------------------------------------
            // Write those elements out to the stream
            //-----------------------------------------------------------------
            for ( typename Cont< T >::const_iterator cur = Data.begin( ),
                                                     last = Data.end( );
                  cur != last;
                  ++cur )
            {
                *this << *cur;
            }
            return *this;
        }

    } // namespace Streams

} // namespace diskCache

inline diskCache::Streams::OASCII&
operator<<( diskCache::Streams::OASCII& Stream, const std::string& Data )
{
    return Stream.operator<<( Data );
}
#endif /* DISKCACHE__STREAMS__ASCII_HH */
