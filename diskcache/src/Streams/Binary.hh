//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__STREAMS__BINARY_HH
#define DISKCACHE__STREAMS__BINARY_HH

#if !SWIG
#include <assert.h>

#include <iostream>
#include <list>
#include <set>
#include <stdexcept>
#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/reverse.hh"

#include "diskcacheAPI/Common/Registry.hh"

#include "diskcacheAPI/Streams/StreamsInterface.hh"
#endif /* ! SWIG */

namespace diskCache
{
    namespace Streams
    {
        //###################################################################
        //###################################################################
        class Binary
        {
        public:
            typedef INT_8U size_type;
        };

        //###################################################################
        //###################################################################
        class IBinary : public Binary, public IInterface
        {
        public:
            typedef Common::Registry::id_type       id_type;
            typedef boost::shared_ptr< Streamable > read_return_type;

            typedef read_return_type ( *read_func )( IBinary& Stream );

            class SwapError : std::out_of_range
            {
            public:
                SwapError( );
            };

            typedef Interface::version_type version_type;
            typedef std::streamsize         streamsize;

            IBinary( std::istream& Source );
            ~IBinary( );

            bool Readable( ) const;

            static read_func Reader( id_type Id );

            static void Reader( id_type Id, read_func Function );

            template < typename Type >
            IBinary& operator>>( Type& Data );

        private:
            typedef INT_4U swap_type;

            typedef LDASTools::AL::unordered_map< id_type, read_func >
                reader_container_type;

            bool m_swap;
            //-----------------------------------------------------------------
            /// \brief Collection of registered readers
            ///
            /// \todo Need to make this thread safe
            //-----------------------------------------------------------------
            static reader_container_type m_readers;

            template < typename T >
            IBinary& container_handler( T& Data );

            void check_eof_read( ) const;
        };

        inline IBinary::SwapError::SwapError( )
            : out_of_range(
                  "Unable to determine byte swapping for input stream" )
        {
        }

        inline bool
        IBinary::Readable( ) const
        {
            return ( m_stream.good( ) && !m_stream.eof( ) );
        }

        inline IBinary::read_func
        IBinary::Reader( id_type Id )
        {
            reader_container_type::const_iterator pos( m_readers.find( Id ) );

            if ( pos != m_readers.end( ) )
            {
                return pos->second;
            }
            throw std::range_error( "No registered reader for IBinary" );
        }

        inline void
        IBinary::Reader( id_type Id, read_func Function )
        {
            reader_container_type::const_iterator pos( m_readers.find( Id ) );

            if ( pos != m_readers.end( ) )
            {
                return;
            }
            m_readers[ Id ] = Function;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( std::list< std::string >& Data )
        {
            return container_handler( Data );
        }

        template <>
        inline IBinary&
        IBinary::operator>>( INT_2U& Data )
        {
            m_stream.read( reinterpret_cast< char* >( &Data ), sizeof( Data ) );
            check_eof_read( );
            if ( m_swap )
            {
                reverse< sizeof( Data ) >( &Data, 1 );
            }
            return *this;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( INT_2S& Data )
        {
            m_stream.read( reinterpret_cast< char* >( &Data ), sizeof( Data ) );
            check_eof_read( );
            if ( m_swap )
            {
                reverse< sizeof( Data ) >( &Data, 1 );
            }
            return *this;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( INT_4U& Data )
        {
            m_stream.read( reinterpret_cast< char* >( &Data ), sizeof( Data ) );
            check_eof_read( );
            if ( m_swap )
            {
                reverse< sizeof( Data ) >( &Data, 1 );
            }
            return *this;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( INT_4S& Data )
        {
            m_stream.read( reinterpret_cast< char* >( &Data ), sizeof( Data ) );
            check_eof_read( );
            if ( m_swap )
            {
                reverse< sizeof( Data ) >( &Data, 1 );
            }
            return *this;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( INT_8U& Data )
        {
            m_stream.read( reinterpret_cast< char* >( &Data ), sizeof( Data ) );
            check_eof_read( );
            if ( m_swap )
            {
                reverse< sizeof( Data ) >( &Data, 1 );
            }
            return *this;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( INT_8S& Data )
        {
            m_stream.read( reinterpret_cast< char* >( &Data ), sizeof( Data ) );
            check_eof_read( );
            if ( m_swap )
            {
                reverse< sizeof( Data ) >( &Data, 1 );
            }
            return *this;
        }

        template <>
        inline IBinary&
        IBinary::operator>>( std::string& Data )
        {
            INT_4U s;
            *this >> s;
            if ( s )
            {
                //---------------------------------------------------------------
                /// \todo Protect against buffer overrun
                //---------------------------------------------------------------
                char buffer[ 4096 ];

                assert( sizeof( buffer ) >= s );
                buffer[ s ] = '\0';

                m_stream.read( buffer, s );
                check_eof_read( );
                Data.assign( buffer, s );
            }
            else
            {
                Data = "";
            }
            return *this;
        }

        template < typename T >
        inline IBinary&
        IBinary::container_handler( T& Data )
        {
            //---------------------------------------------------------------
            // Start with an empty container
            //---------------------------------------------------------------
            Data.clear( );

            //---------------------------------------------------------------
            // Read the number of elements
            //---------------------------------------------------------------
            INT_4U s;

            *this >> s;

            //---------------------------------------------------------------
            // Read the elements out to the stream
            //---------------------------------------------------------------
            typename T::value_type d;

            for ( size_type cur = 0, last = s; cur != last; ++cur )
            {
                //-------------------------------------------------------------
                // Extract from the stream
                //-------------------------------------------------------------
                *this >> d;
                //-------------------------------------------------------------
                // Insert into container
                //-------------------------------------------------------------
                Data.push_back( d );
            }
            return *this;
        }

        inline void
        IBinary::check_eof_read( ) const
        {
            if ( ( m_stream.gcount( ) == 0 ) && ( !Readable( ) ) )
            {
                throw std::istream::failure( "eof" );
            }
        }
        //###################################################################
        //###################################################################
        class OBinary : public Binary, public OInterface
        {
        public:
            typedef Common::Registry::id_type id_type;
            typedef Interface::version_type   version_type;
            typedef std::streamsize           streamsize;
            typedef void ( *write_func )( OBinary&          Stream,
                                          const Streamable& Data );

            static const version_type VERSION_MAX =
                Interface::VERSION_MULTIPLE_EXTENSIONS;
            static const version_type VERSION_DEFAULT =
                Interface::VERSION_MULTIPLE_EXTENSIONS;

            OBinary( std::ostream& Sink,
                     version_type  Version = VERSION_DEFAULT );
            ~OBinary( );

            bool Writeable( ) const;

            static write_func Writer( id_type Id );

            static void Writer( id_type Id, write_func Function );

            template < typename Type >
            OBinary& operator<<( const Type& Data );

        private:
            typedef LDASTools::AL::unordered_map< id_type, write_func >
                writer_container_type;

            std::ostream& m_stream;

            //-----------------------------------------------------------------
            /// \brief Collection of registered readers
            ///
            /// \todo Need to make this thread safe
            //-----------------------------------------------------------------
            static writer_container_type m_writers;

            template < typename T >
            OBinary& container_handler( const T& Data );
        }; // class - OBinary

        inline bool
        OBinary::Writeable( ) const
        {
            return m_stream.good( );
        }

        inline OBinary::write_func
        OBinary::Writer( id_type Id )
        {
            writer_container_type::const_iterator pos( m_writers.find( Id ) );

            if ( pos != m_writers.end( ) )
            {
                return pos->second;
            }
            throw std::range_error( "No registered writer for OBinary" );
        }

        inline void
        OBinary::Writer( id_type Id, write_func Function )
        {
            writer_container_type::const_iterator pos( m_writers.find( Id ) );

            if ( pos != m_writers.end( ) )
            {
                return;
            }
            m_writers[ Id ] = Function;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const INT_2U& Data )
        {
            m_stream.write( reinterpret_cast< const char* >( &Data ),
                            sizeof( Data ) );
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const INT_2S& Data )
        {
            m_stream.write( reinterpret_cast< const char* >( &Data ),
                            sizeof( Data ) );
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const INT_4U& Data )
        {
            m_stream.write( reinterpret_cast< const char* >( &Data ),
                            sizeof( Data ) );
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const INT_4S& Data )
        {
            m_stream.write( reinterpret_cast< const char* >( &Data ),
                            sizeof( Data ) );
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const INT_8U& Data )
        {
            m_stream.write( reinterpret_cast< const char* >( &Data ),
                            sizeof( Data ) );
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const INT_8S& Data )
        {
            m_stream.write( reinterpret_cast< const char* >( &Data ),
                            sizeof( Data ) );
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const std::string& Data )
        {
            const INT_4U s( Data.size( ) );
            *this << s;
            if ( s )
            {
                m_stream.write( Data.c_str( ), s );
            }
            return *this;
        }

        template < typename T >
        inline OBinary&
        OBinary::container_handler( const T& Data )
        {
            //-----------------------------------------------------------------
            // Record the number of elements
            //-----------------------------------------------------------------
            const INT_4U s( Data.size( ) );
            *this << s;

            //-----------------------------------------------------------------
            // Write those elements out to the stream
            //-----------------------------------------------------------------
            for ( typename T::const_iterator cur = Data.begin( ),
                                             last = Data.end( );
                  cur != last;
                  ++cur )
            {
                *this << *cur;
            }
            return *this;
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const std::list< std::string >& Data )
        {
            return container_handler( Data );
        }

        template <>
        inline OBinary&
        OBinary::operator<<( const std::set< std::string >& Data )
        {
            return container_handler( Data );
        }

    } // namespace Streams

} // namespace diskCache
#endif /* DISKCACHE__STREAMS__BINARY_HH */
