#! /bin/sh
#
# LDASTools diskcache - Tools for querying a collection of files on disk
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools diskcache is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools diskcache is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#------------------------------------------------------------------------
# This is an example of how to use the standalone diskcache utility
# in daemon mode.
# 
# In this mode, it is not necessary to run this command as cron job
# as the diskcache is left running. Instead, it should be rolled
# into an init script.
#------------------------------------------------------------------------
# User configurable variables
#------------------------------------------------------------------------
DISKCACHE=./diskcache
PORT=224433
OUTPUTDIR="/var/tmp"
ASCII_CACHE_NAME="ldasASCIICache.cache"
BINARY_CACHE_NAME="ldasBinaryCache.cache"

ASCII_CACHE_FULLPATH="${OUTPUTDIR}/${ASCII_CACHE_NAME}"
BINARY_CACHE_FULLPATH="${OUTPUTDIR}/${BINARY_CACHE_NAME}"

CONCURRENCY=4		# Maximum number of threads to use during scan
EXTENSIONS=".gwf"	# File extensions of files to cache

MOUNT_POINT_ROOT_DIR="/devscratch/frames/Samples"
MOUNT_POINTS=""
for m in \
    ${MOUNT_POINT_ROOT_DIR}/S6 \
    ${MOUNT_POINT_ROOT_DIR}/A5 \
    ${MOUNT_POINT_ROOT_DIR}/S5 \
    ${MOUNT_POINT_ROOT_DIR}/A4 \
    ${MOUNT_POINT_ROOT_DIR}/S4
do
  #----------------------------------------
  # Generate the list of mount points by
  # placing the comma in the correct place.
  #----------------------------------------
  case "x${MOUNT_POINTS}" in
  x) MOUNT_POINTS="${m}";;
  *) MOUNT_POINTS="${MOUNT_POINTS},${m}";;
  esac
done

#------------------------------------------------------------------------
# Nothing below this line should require modification
#------------------------------------------------------------------------
INIT_OPTS=""
DAEMON_OPTS=""
if test -f ${BINARY_CACHE_FULLPATH}
then
    #--------------------------------------------------------------------
    # Check to see there is a cache file. If so, use it to bootstrap
    #   the internal cache.
    #--------------------------------------------------------------------
    INIT_OPTS="${INIT_OPTS} --cache-file ${BINARY_CACHE_FULLPATH}"
fi
#------------------------------------------------------------------------
# Check certain options
#------------------------------------------------------------------------
#case "x$ASCII_CACHE_NAME" in
#x) ;;
#*) DAEMON_OPTS="${DAEMON_OPTS} --output-ascii=${ASCII_CACHE_FULLPATH}";;
#esac
#case "x$BINARY_CACHE_NAME" in
#x) ;;
#*) DAEMON_OPTS="${DAEMON_OPTS} --output-binary=${BINARY_CACHE_FULLPATH}";;
#esac
#------------------------------------------------------------------------
# Run the command
#------------------------------------------------------------------------
${DISKCACHE} ${INIT_OPTS} \
    --port ${PORT} \
    daemon ${DAEMON_OPTS}\
      --concurrency=${CONCURRENCY} \
      --extensions=${EXTENSIONS} \
      --mount-points=${MOUNT_POINTS}
