//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <queue>

#include "diskcacheAPI/Streams/Binary.hh"

#include "MountPointManager.hh"

#if 0
#define DEBUG_MOUNT_POINT_MANAGER 1
#endif /* 0 */

namespace diskCache
{
    //---------------------------------------------------------------------
    /// Handles writing of a single collection of mount points to a binary
    /// stream.
    //---------------------------------------------------------------------
    template <>
    Streams::IBinary&
    MountPointManager::Read( Streams::IBinary& Stream )
    {
        mount_point_names_rw_type mpn_lock( mount_point_names_rw( ) );

        Stream >> mpn_lock.Var( ) // read the list of mount points
            ;
        mount_point_name_container_type dictionary_order( mpn_lock.Var( ) );

        dictionary_order.sort( );
        m_mount_points_dictionary_order.swap( dictionary_order );

        //-------------------------------------------------------------------
        // Work is done so return to the caller
        //-------------------------------------------------------------------
        return Stream;
    }

    //---------------------------------------------------------------------
    /// Handles writing of a single collection of mount points to a binary
    /// stream.
    //---------------------------------------------------------------------
    template <>
    Streams::OBinary&
    MountPointManager::Write( Streams::OBinary& Stream )
    {
        const mount_point_names_ro_type mpn_lock( mount_point_names_ro( ) );

        Stream << mpn_lock.Var( ) // write the list of mount points
            ;
        //-----------------------------------------------------------------
        // Work is done so return to the caller
        //-----------------------------------------------------------------
        return Stream;
    }
} // namespace diskCache
