//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <fstream>
#include <iomanip>

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "Commands.hh"
#include "IO.hh"
#include "MetaCommands.hh"

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;

using diskCache::Commands::getIntervalsList;
using diskCache::MetaCommand::transfer_helper;

namespace
{
    class transfer : public transfer_helper
    {
    public:
        //-------------------------------------------------------------------
        /// \brief Default constructor
        //-------------------------------------------------------------------
        transfer( );

        //-------------------------------------------------------------------
        /// \brief read the reponce from the stream
        ///
        /// \param[in] Stream
        ///     The output stream from which to read the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream from which the responce was read.
        //-------------------------------------------------------------------
        std::istream& read( std::istream& Stream );

        //-------------------------------------------------------------------
        /// \brief write the reponce onto the stream
        ///
        /// \param[in] Stream
        ///     The output stream on which to write the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream on which the responce was written.
        //-------------------------------------------------------------------
        std::ostream& write( std::ostream& Stream );

        std::stringstream answer;
    };

} // namespace

namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // Intervals
        //===================================================================
        OptionSet& Intervals::m_options( Intervals::init_options( ) );

        OptionSet&
        Intervals::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: intervals" );

            retval.Summary( "The intervals sub command is intended to"
                            " query the memory cache and return time intervals"
                            " of file data." );

            retval.Add( Option( OPT_EXTENSION,
                                "extension",
                                Option::ARG_REQUIRED,
                                "filename extension to search",
                                "extension" ) );

            retval.Add( Option(
                OPT_IFO_TYPE_LIST,
                "ifo-type-list",
                Option::ARG_REQUIRED,
                "comma seperated list of <ifo>-<type>- entries to search",
                "<ifo>-<type>- list" ) );

            retval.Add( Option( OPT_START_TIME,
                                "start-time",
                                Option::ARG_REQUIRED,
                                "GPS start time of interest",
                                "gps_time" ) );

            retval.Add( Option( OPT_END_TIME,
                                "end-time",
                                Option::ARG_REQUIRED,
                                "GPS end time of interest",
                                "gps_time" ) );

            return retval;
        }

        Intervals::Intervals( CommandLineOptions& Args,
                              const ServerInfo&   Server )
            : ClientServerInterface( Server ), m_args( Args ),
              m_ifo_type_list( "" ), m_time_start( 0 ), m_time_stop( ~0 )
        {
            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );
                int         opt;

                while ( parsing )
                {
                    opt = m_args.Parse( m_options, arg_name, arg_value );

                    switch ( opt )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_EXTENSION:
                    {
                        m_extension = arg_value;
                    }
                    break;
                    case OPT_IFO_TYPE_LIST:
                    {
                        m_ifo_type_list = arg_value;

                        //-----------------------------------------------------------
                        // Replace the commas with spaces
                        //-----------------------------------------------------------
                        size_t pos = 0;

                        while ( ( pos = m_ifo_type_list.find_first_of(
                                      ",", pos ) ) != std::string::npos )
                        {
                            m_ifo_type_list[ pos ] = ' ';
                        }
                    }
                    break;
                    case OPT_START_TIME:
                    {
                        std::istringstream( arg_value ) >> m_time_start;
                    }
                    break;
                    case OPT_END_TIME:
                    {
                        std::istringstream( arg_value ) >> m_time_stop;
                    }
                    break;
                    default:
                        break;
                    }
                }
            }
        }

        const OptionSet&
        Intervals::Options( )
        {
            return m_options;
        }

        void
        Intervals::evalClient( )
        {
            //-----------------------------------------------------------------
            // Construct the request
            //-----------------------------------------------------------------
            std::ostringstream cmd;
            const Option&      extension( m_options[ OPT_EXTENSION ] );
            const Option&      ifo_type( m_options[ OPT_IFO_TYPE_LIST ] );
            const Option&      start( m_options[ OPT_START_TIME ] );
            const Option&      stop( m_options[ OPT_END_TIME ] );

            cmd << CommandTable::Lookup( CommandTable::CMD_INTERVALS ) << " "
                << extension << " "
                << ( m_extension.size( ) ? m_extension : "unspecified" ) << " "
                << ifo_type << " "
                << ( m_ifo_type_list.size( ) ? m_ifo_type_list : "unspecified" )
                << " " << start << " " << m_time_start << " " << stop << " "
                << m_time_stop << std::endl;

            //-----------------------------------------------------------------
            // Submit the request
            //-----------------------------------------------------------------

            ServerRequest( cmd.str( ) );

            //-----------------------------------------------------------------
            // Retrieve the results of the request
            //-----------------------------------------------------------------
            transfer responce;
            responce.read( *( serverRequestHandle( ) ) );

            std::cout << responce.answer.str( );
        }

        void
        Intervals::evalServer( )
        {
            transfer responce;

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            process( responce.answer );

            //-----------------------------------------------------------------
            // Send the results back to the client
            //-----------------------------------------------------------------
            responce.write( *( clientHandle( ) ) );
        }

        void
        Intervals::evalStandalone( )
        {
            //-----------------------------------------------------------------
            // Standalone mode
            //-----------------------------------------------------------------
            process( std::cout );
        }

        void
        Intervals::process( std::ostream& Stream )
        {
            typedef diskCache::Cache::QueryAnswer::interval_container_type
                interval_container_type;
            using diskCache::Cache::QueryAnswer;

            QueryAnswer answer;

            getIntervalsList( answer,
                              m_ifo_type_list.c_str( ),
                              m_time_start,
                              m_time_stop,
                              m_extension );

            interval_container_type intervals;
            answer.Complete(
                INT_4U( QueryAnswer::IndexDataBase::GEN_INTERVALS ) );
            answer.SwapIntervals( intervals );
            for ( interval_container_type::const_iterator
                      cur = intervals.begin( ),
                      last = intervals.end( );
                  cur != last;
                  ++cur )
            {
                Stream << *cur << std::endl;
            }
        }

    } // namespace MetaCommand
} // namespace diskCache

namespace
{
    //=====================================================================
    // transfer
    //=====================================================================
    transfer::transfer( )
    {
    }

    std::istream&
    transfer::read( std::istream& Stream )
    {
        bool available;
        Blob( Stream, available, answer );
        return Stream;
    }

    std::ostream&
    transfer::write( std::ostream& Stream )
    {
        Blob( Stream, true, answer );
        Stream.flush( );
        return Stream;
    }
} // namespace
