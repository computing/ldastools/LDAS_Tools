//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <fstream>
#include <iomanip>

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "Commands.hh"
#include "IO.hh"
#include "MetaCommands.hh"

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;

using diskCache::Commands::getRDSFrameFiles;
using diskCache::MetaCommand::transfer_helper;

namespace
{
    class transfer : public transfer_helper
    {
    public:
        //-------------------------------------------------------------------
        /// \brief Default constructor
        //-------------------------------------------------------------------
        transfer( );

        //-------------------------------------------------------------------
        /// \brief read the reponce from the stream
        ///
        /// \param[in] Stream
        ///     The output stream from which to read the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream from which the responce was read.
        //-------------------------------------------------------------------
        std::istream& read( std::istream& Stream );

        //-------------------------------------------------------------------
        /// \brief write the reponce onto the stream
        ///
        /// \param[in] Stream
        ///     The output stream on which to write the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream on which the responce was written.
        //-------------------------------------------------------------------
        std::ostream& write( std::ostream& Stream );

        std::stringstream answer;
    };

} // namespace

namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // FilenamesRDS
        //===================================================================
        OptionSet& FilenamesRDS::m_options( FilenamesRDS::init_options( ) );

        OptionSet&
        FilenamesRDS::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: filenames-rds" );

            retval.Summary( "The filenames-rds sub command is intended to"
                            " query the memory cache for filenames that can be "
                            "used for createRDS command."
                            " Several search options are available to restrict"
                            " the set of filename returned." );

            retval.Add( Option( OPT_EXTENSION,
                                "extension",
                                Option::ARG_REQUIRED,
                                "filename extension to search",
                                "extension" ) );

            retval.Add(
                Option( OPT_IFO, "ifo", Option::ARG_REQUIRED, "IFO", "ifo" ) );

            retval.Add( Option( OPT_END_TIME,
                                "end-time",
                                Option::ARG_REQUIRED,
                                "GPS end time of interest",
                                "gps_time" ) );

            retval.Add( Option( OPT_RESAMPLE,
                                "resample",
                                Option::ARG_NONE,
                                "Specify that resampling will be done" ) );

            retval.Add( Option( OPT_START_TIME,
                                "start-time",
                                Option::ARG_REQUIRED,
                                "GPS start time of interest",
                                "gps_time" ) );

            retval.Add( Option( OPT_TYPE,
                                "type",
                                Option::ARG_REQUIRED,
                                "Type specifier of the the filename.",
                                "type" ) );

            return retval;
        }

        FilenamesRDS::FilenamesRDS(
            CommandLineOptions&                      Args,
            const ClientServerInterface::ServerInfo& Server )
            : ClientServerInterface( Server ), m_args( Args ), m_ifo( "all" ),
              m_resample( false ), m_time_start( 0 ), m_time_stop( ~0 ),
              m_type( "all" )
        {
            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );
                int         opt;

                while ( parsing )
                {
                    opt = m_args.Parse( m_options, arg_name, arg_value );
                    switch ( opt )
                    {
                    case OPT_EXTENSION:
                    {
                        m_extension = arg_value;
                    }
                    break;
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_IFO:
                        m_ifo = arg_value;
                        break;
                    case OPT_TYPE:
                        m_type = arg_value;
                        break;
                    case OPT_START_TIME:
                    {
                        std::istringstream( arg_value ) >> m_time_start;
                    }
                    break;
                    case OPT_END_TIME:
                    {
                        std::istringstream( arg_value ) >> m_time_stop;
                    }
                    break;
                    case OPT_RESAMPLE:
                        m_resample = true;
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        const OptionSet&
        FilenamesRDS::Options( )
        {
            return m_options;
        }

        void
        FilenamesRDS::evalClient( )
        {
            static const char* caller = "FilenameRDS::evalClient";
            //-----------------------------------------------------------------
            // Construct the request
            //-----------------------------------------------------------------
            std::ostringstream cmd;
            const Option&      extension( m_options[ OPT_EXTENSION ] );
            const Option&      ifo( m_options[ OPT_IFO ] );
            const Option&      type( m_options[ OPT_TYPE ] );
            const Option&      start( m_options[ OPT_START_TIME ] );
            const Option&      stop( m_options[ OPT_END_TIME ] );
            const Option&      resample( m_options[ OPT_RESAMPLE ] );

            cmd << CommandTable::Lookup( CommandTable::CMD_FILENAMES_RDS );
            if ( m_resample )
            {
                cmd << " " << resample;
            }
            cmd << " " << extension << " "
                << ( m_extension.size( ) ? m_extension : "unspecified" ) << " "
                << ifo << " " << ( m_ifo.size( ) ? m_ifo : "unspecified" )
                << " " << type << " "
                << ( m_type.size( ) ? m_type : "unspecified" ) << " " << start
                << " " << m_time_start << " " << stop << " " << m_time_stop
                << std::endl;

            //-----------------------------------------------------------------
            // Submit the request
            //-----------------------------------------------------------------

            QUEUE_LOG_MESSAGE( "Server Request: " << cmd.str( ),
                               MT_DEBUG,
                               40,
                               caller,
                               "DISKCACHE DAEMON" );
            ServerRequest( cmd.str( ) );

            //-----------------------------------------------------------------
            // Retrieve the results of the request
            //-----------------------------------------------------------------
            transfer responce;
            responce.read( *( serverRequestHandle( ) ) );

#if 0
      std::istringstream	s( responce.answer.str( ) );
#endif /* 0 */
            std::string word;

            while ( getline( responce.answer, word ) )
            {
                results.push_back( word );
            }
        }

        void
        FilenamesRDS::evalServer( )
        {
            transfer responce;

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            process( responce.answer );

            //-----------------------------------------------------------------
            // Send the results back to the client
            //-----------------------------------------------------------------
            responce.write( *( clientHandle( ) ) );
        }

        void
        FilenamesRDS::evalStandalone( )
        {
            //-----------------------------------------------------------------
            // Standalone mode
            //-----------------------------------------------------------------
            process( std::cout );
        }

        void
        FilenamesRDS::process( std::ostream& Stream )
        {
            typedef diskCache::Cache::QueryAnswer::filename_container_type
                file_container_type;
            typedef diskCache::Cache::QueryAnswer::gap_container_type
                                          gap_container_type;
            diskCache::Cache::QueryAnswer answer;

            getRDSFrameFiles( answer,
                              m_ifo.c_str( ),
                              m_type.c_str( ),
                              m_time_start,
                              m_time_stop,
                              m_extension,
                              m_resample );

            try
            {
                file_container_type files;

                answer.Swap( files );
                for ( file_container_type::const_iterator cur = files.begin( ),
                                                          last = files.end( );
                      cur != last;
                      ++cur )
                {
                    Stream << *cur << std::endl;
                }

                gap_container_type gaps;
                answer.SwapGaps( gaps );

                if ( gaps.size( ) > 0 )
                {
                    Stream << "Gaps remain at:";
                    for ( gap_container_type::const_iterator
                              cur = gaps.begin( ),
                              last = gaps.end( );
                          cur != last;
                          ++cur )
                    {
                        Stream << " " << *cur;
                    }
                    Stream << std::endl;
                }
            }
            catch ( ... )
            {
            }
        }

    } // namespace MetaCommand
} // namespace diskCache

namespace
{
    //=====================================================================
    // transfer
    //=====================================================================
    transfer::transfer( )
    {
    }

    std::istream&
    transfer::read( std::istream& Stream )
    {
        bool available;
        Blob( Stream, available, answer );
        return Stream;
    }

    std::ostream&
    transfer::write( std::ostream& Stream )
    {
        Blob( Stream, true, answer );
        Stream.flush( );
        return Stream;
    }
} // namespace
