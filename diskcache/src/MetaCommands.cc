//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <fstream>

#include "genericAPI/Logging.hh"

#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "diskcacheAPI/MetaCommands.hh"

#include "Commands.hh"
#include "diskcachecmd.hh"
#include "DumpCacheDaemon.hh"
#include "IO.hh"
#include "MountPointScanner.hh"

//=======================================================================
//=======================================================================

using GenericAPI::queueLogEntry;

using diskCache::Commands::updateFileExtList;
using diskCache::Commands::updateMountPtList;

typedef diskCache::MetaCommand::client_type                       client_type;
typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;

//=======================================================================
// Global variables
//=======================================================================

#if 0
static client_type	client_handle;
#endif /* 0 */

//=======================================================================
//=======================================================================
#if 0
static void send_request( const std::string& Command,
			  const ServerInfo& Server );
#endif /* 0 */

//=======================================================================
/// \brief Maintains a list of commands that are supported.
//=======================================================================
namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // CommandTable
        //===================================================================

        // Instantiate generic to Singleton class methods:
        // see "general" library
        SINGLETON_TS_INST( CommandTable );

        /** \cond ignore_singleton_constructor */
        CommandTable::CommandTable( )
        {
            command_table[ "daemon" ] = CMD_DAEMON;
            command_table[ "dump" ] = CMD_DUMP;
            command_table[ "filenames" ] = CMD_FILENAMES;
            command_table[ "filenames-rds" ] = CMD_FILENAMES_RDS;
            command_table[ "intervals" ] = CMD_INTERVALS;
            command_table[ "mount-point-stats" ] = CMD_MOUNT_POINT_STATS;
            command_table[ "quit" ] = CMD_QUIT;
            command_table[ "reconfigure" ] = CMD_RECONFIGURE;
            command_table[ "scan" ] = CMD_SCAN;
            command_table[ "status" ] = CMD_STATUS;
        }
        /** \endcond */

        CommandTable::~CommandTable( )
        {
            Teardown( );
        }

        void
        CommandTable::Teardown( )
        {
            Instance( ).command_table.erase( Instance( ).command_table.begin( ),
                                             Instance( ).command_table.end( ) );
        }

        ClientServerInterface::~ClientServerInterface( )
        {
            if ( server_request_handle )
            {
                server_request_handle->close( );
            }
        }

        //===================================================================
        // MountPointStats
        //===================================================================
        OptionSet&
            MountPointStats::m_options( MountPointStats::init_options( ) );

        OptionSet&
        MountPointStats::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: mountPointStats" );

            retval.Summary( "The mount-point-stats sub command is intended to "
                            "get some statistical information"
                            " the memory cache grouped by the mount points." );

            retval.Add( Option( OPT_IFO,
                                "ifo",
                                Option::ARG_REQUIRED,
                                "IFO pattern to use for search. (Default: all)",
                                "pattern" ) );

            retval.Add(
                Option( OPT_TYPE,
                        "type",
                        Option::ARG_REQUIRED,
                        "Type pattern to use for search. (Default: all)",
                        "pattern" ) );

            return retval;
        }

        MountPointStats::MountPointStats( CommandLineOptions& Args )
            : m_args( Args ), m_ifo( "all" ), m_type( "all" )
        {
            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );

                while ( parsing )
                {
                    switch ( m_args.Parse( m_options, arg_name, arg_value ) )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_IFO:
                        m_ifo = arg_value;
                        break;
                    case OPT_TYPE:
                        m_type = arg_value;
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        const OptionSet&
        MountPointStats::Options( )
        {
            return m_options;
        }

        void
        MountPointStats::operator( )( )
        {
            diskCache::Cache::QueryAnswer answer;

            getHashNumbers( answer, m_ifo.c_str( ), m_type.c_str( ) );
        }

        //
        ClientServerInterface::ServerInfo::ServerInfo( ) : port( -1 )
        {
        }
    } // namespace MetaCommand
} // namespace diskCache
