//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <iostream>

#include "diskcacheAPI/Common/Registry.hh"

namespace diskCache
{
    namespace Common
    {
        Registry::Info::~Info( )
        {
        } // method - Registry::Registry::~Info

        Registry::Registry( ) : m_max_id( KEY_NULL )
        {
        }

        Registry::id_type
        Registry::Register( const Info& Key )
        {
            id_type retval( KEY_NULL );

            if ( m_name_info.find( Key.m_key_name ) != m_name_info.end( ) )
            {
                throw AlreadyRegisteredException(
                    Key.m_key_name,
                    m_name_info.find( Key.m_key_name )->second );
            }
            //-----------------------------------------------------------------
            /// \todo Lock the registry because changes are going to happen
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            // With things properly locked, make the changes
            //-----------------------------------------------------------------
            ++m_max_id;
            retval = m_max_id;
            m_name_info[ Key.m_key_name ] = retval;
            m_info[ retval ] = info_type( Key.Clone( ) );
            return retval;
        } // method - Registry::Register

    } // namespace Common
} // namespace diskCache
