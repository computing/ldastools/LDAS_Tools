//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <string>

#include "ldastoolsal/unordered_map.hh"

#include "diskcacheAPI/Common/Variables.hh"

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< diskCache::Common::Variables > )

namespace diskCache
{
    namespace Common
    {
        class Variables::pdata_storage_type
        {
        public:
            struct var_info
            {
                std::string cache_value;
                reader_ss   reader_ss_func;
                reader_any  reader_any_func;
                writer_str  writer_str_func;
                bool        cacheable = true;

                var_info( ) : reader_ss_func( NULL ), writer_str_func( NULL )
                {
                }
            };

            typedef LDASTools::AL::unordered_map< std::string, var_info >
                var_container_type;

            var_container_type vars;

            pdata_storage_type( )
            {
            }

            ~pdata_storage_type( )
            {
            }
        };

        void
        Variables::NULL_READER_ANY( boost::any& Value )
        {
        }

        Variables::Variables( ) : pdata( new pdata_type::element_type )
        {
        }

        Variables::~Variables( )
        {
        }

        boost::any
        Variables::Cache( const std::string& Name )
        {
            pdata_storage_type::var_container_type::iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }
            return ( pos->second.cache_value );
        }

        void
        Variables::Cache( const std::string& Name, const std::string& Value )
        {
            pdata_storage_type::var_container_type::iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }
            pos->second.cache_value = Value;
            if ( !pos->second.cacheable )
            {
                Set( Name );
            }
        }

        void
        Variables::Cacheable( const std::string& Name, bool Value )
        {
            pdata_storage_type::var_container_type::iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }
            pos->second.cacheable = Value;
        }

        boost::any
        Variables::Get( const std::string& Name )
        {
            pdata_storage_type::var_container_type::const_iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }
            if ( pos->second.reader_any_func &&
                 ( pos->second.reader_any_func != NULL_READER_ANY ) )
            {
                boost::any value;
                ( *( pos->second.reader_any_func ) )( value );
                return ( value );
            }
            throw std::runtime_error(
                "No valid reader function for variablle" );
        }

        void
        Variables::Get( const std::string& Name, std::ostringstream& Value )
        {
            pdata_storage_type::var_container_type::const_iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }
            if ( pos->second.reader_ss_func )
            {
                ( *( pos->second.reader_ss_func ) )( Value );
            }
        }

        void
        Variables::Init( const std::string& Var,
                         reader_ss          RFunc,
                         writer_str         WFunc,
                         const std::string& Default )
        {
            if ( !Instance( ).pdata )
            {
                Instance( ).pdata.reset( new pdata_type::element_type );
            }

            Instance( ).pdata->vars[ Var ].cache_value = Default;
            Instance( ).pdata->vars[ Var ].reader_ss_func = RFunc;
            Instance( ).pdata->vars[ Var ].reader_any_func = NULL_READER_ANY;
            Instance( ).pdata->vars[ Var ].writer_str_func = WFunc;
        }

        void
        Variables::Init( const std::string& Var,
                         reader_ss          ReadFunc,
                         reader_any         ReaderAnyFunc,
                         writer_str         WriteFunc,
                         const std::string& Default )
        {
            if ( !Instance( ).pdata )
            {
                Instance( ).pdata.reset( new pdata_type::element_type );
            }

            Instance( ).pdata->vars[ Var ].cache_value = Default;
            Instance( ).pdata->vars[ Var ].reader_ss_func = ReadFunc;
            Instance( ).pdata->vars[ Var ].reader_any_func = ReaderAnyFunc;
            Instance( ).pdata->vars[ Var ].writer_str_func = WriteFunc;
        }

        void
        Variables::Set( const std::string& Name )
        {
            pdata_storage_type::var_container_type::const_iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }

            if ( pos->second.writer_str_func )
            {
                ( *( pos->second.writer_str_func ) )( pos->second.cache_value );
            }
        }

        void
        Variables::Set( const std::string& Name, const std::string& Value )
        {
            pdata_storage_type::var_container_type::const_iterator pos =
                Instance( ).pdata->vars.find( Name );
            if ( pos == Instance( ).pdata->vars.end( ) )
            {
                std::ostringstream msg;

                msg << "Unable to locate variable: " << Name;

                throw std::invalid_argument( msg.str( ) );
            }
            if ( pos->second.writer_str_func )
            {
                ( *( pos->second.writer_str_func ) )( Value );
            }
        }

        void
        Variables::SetReader( const std::string& Var, reader_ss Func )
        {
            if ( !Instance( ).pdata )
            {
                Instance( ).pdata.reset( new pdata_type::element_type );
            }

            Instance( ).pdata->vars[ Var ].reader_ss_func = Func;
        }

        void
        Variables::SetWriter( const std::string& Var, writer_str Func )
        {
            if ( !Instance( ).pdata )
            {
                Instance( ).pdata.reset( new pdata_type::element_type );
            }

            Instance( ).pdata->vars[ Var ].writer_str_func = Func;
        }

        void
        Variables::Reset( )
        {
            if ( Instance( ).pdata )
            {
                Instance( ).pdata->vars.clear( );

                Instance( ).pdata.reset( );
            }
        }

    } // namespace Common
} // namespace diskCache
