//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__COMMON__LOGGING_HH
#define DISKCACHE_API__COMMON__LOGGING_HH

#include "genericAPI/Logging.hh"

namespace diskCache
{
    namespace Common
    {
        class LogMessage
        {
        public:
            typedef std::ostringstream ostream;

            enum message_type
            {
                LM_DEBUG = GenericAPI::LogEntryGroup_type::MT_DEBUG,
                LM_NOTE = GenericAPI::LogEntryGroup_type::MT_NOTE,
                LM_MAIL = GenericAPI::LogEntryGroup_type::MT_MAIL
            };

            //---------------------------------------------------------------------------
            /// \brief Default constructor
            //---------------------------------------------------------------------------
            LogMessage( );

            message_type MessageType( ) const;

            void MessageType( message_type Type );

            const ostream& Message( ) const;

            ostream& operator( )( );

            ostream& operator( )( message_type Type );

        private:
            message_type m_message_type;
            ostream      m_message;
        };

        inline LogMessage::message_type
        LogMessage::MessageType( ) const
        {
            return m_message_type;
        }

        inline void
        LogMessage::MessageType( message_type Type )
        {
            switch ( m_message_type )
            {
            case LM_DEBUG:
                if ( Type == LM_NOTE )
                {
                    m_message_type = Type;
                }
                //-----------------------------------------------------------------
                // Fall through for higher values of Type
                //-----------------------------------------------------------------
            case LM_NOTE:
                if ( Type == LM_MAIL )
                {
                    m_message_type = Type;
                }
                //-----------------------------------------------------------------
                // Fall through for higher values of Type
                //-----------------------------------------------------------------
            case LM_MAIL:
                //-----------------------------------------------------------------
                // Nothing to do because the error level has already been
                // set to the highest value
                //-----------------------------------------------------------------
                break;
            }
        }

        inline const LogMessage::ostream&
        LogMessage::Message( ) const
        {
            return m_message;
        }

        inline LogMessage::ostream&
        LogMessage::operator( )( )
        {
            return m_message;
        }

        inline LogMessage::ostream&
        LogMessage::operator( )( message_type Type )
        {
            MessageType( Type );
            return m_message;
        }
    } // namespace Common
} // namespace diskCache

#endif /* DISKCACHE_API__COMMON__LOGGING_HH */
