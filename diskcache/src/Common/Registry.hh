//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//
#ifndef DISKCACHE_API__COMMON__REGISTRY_SINGLETON_HH
#define DISKCACHE_API__COMMON__REGISTRY_SINGLETON_HH

#include <string>
#include <stdexcept>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"
#include "ldastoolsal/unordered_map.hh"

namespace diskCache
{
    namespace Common
    {
        class Registry
        {
        public:
            typedef INT_4U      id_type;
            typedef std::string ascii_key_type;

            static const id_type KEY_NULL = id_type( 0 );

            struct Info
            {
                const ascii_key_type m_key_name;

                Info( const ascii_key_type& Key );

                virtual ~Info( );

                virtual Info* Clone( ) const = 0;

            protected:
                Info( const Info& Source );
            };

            class AlreadyRegisteredException : std::runtime_error
            {
            public:
                typedef Registry::id_type id_type;

                AlreadyRegisteredException( const std::string& Name,
                                            id_type            Id );

                ~AlreadyRegisteredException( ) throw( );

                id_type Id( ) const;

                const std::string& Name( ) const;

            private:
                id_type     m_id;
                std::string m_name;
            }; // class - AlreadyRegisteredException

            typedef boost::shared_ptr< Info > info_type;
            //-----------------------------------------------------------------
            /// \brief Default constructor
            //-----------------------------------------------------------------
            Registry( );

            id_type Register( const Info& Key );

            id_type Id( const std::string& Name ) const;

            const info_type GetInfo( id_type Id ) const;

        protected:
            typedef LDASTools::AL::unordered_map< id_type, info_type >
                registry_container_type;

            Registry( const Registry& Source );

            const registry_container_type& registry( ) const;

        private:
            typedef LDASTools::AL::unordered_map< std::string, id_type >
                registry_name_container_type;

            registry_container_type      m_info;
            registry_name_container_type m_name_info;
            id_type                      m_max_id;
        };

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        // Registry::Info
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        /// This constructor creates a new object with the given key.
        //-------------------------------------------------------------------
        inline Registry::Info::Info( const ascii_key_type& Key )
            : m_key_name( Key )
        {
        }

        inline Registry::Info::Info( const Info& Source )
            : m_key_name( Source.m_key_name )
        {
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        // Registry::AlreadyRegisteredException
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        inline Registry::AlreadyRegisteredException::AlreadyRegisteredException(
            const ascii_key_type& Name, id_type Id )
            : std::runtime_error( "The object has already been registered" ),
              m_id( Id ), m_name( Name )
        {
        }

        inline Registry::AlreadyRegisteredException::
            ~AlreadyRegisteredException( ) throw( )
        {
        }

        inline Registry::AlreadyRegisteredException::id_type
        Registry::AlreadyRegisteredException::Id( ) const
        {
            return m_id;
        }

        inline const std::string&
        Registry::AlreadyRegisteredException::Name( ) const
        {
            return m_name;
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        // Registry
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        inline const Registry::info_type
        Registry::GetInfo( id_type Key ) const
        {
            //-----------------------------------------------------------------
            // Need read lock to prevent registration of new elements while
            //   trying to access currently registered objects
            //-----------------------------------------------------------------
            registry_container_type::const_iterator i( m_info.find( Key ) );

            if ( i == m_info.end( ) )
            {
                std::ostringstream msg;

                msg << "The key: " << Key << " does not exist in the registry";
                throw std::range_error( msg.str( ) );
            }
            return i->second;
        }

        inline Registry::id_type
        Registry::Id( const std::string& Name ) const
        {
            //-----------------------------------------------------------------
            /// \todo This needs to be locked to be thread safe.
            ///      It only needs to be locked with a read lock since it
            ///      only queries the information.
            //-----------------------------------------------------------------
            id_type                                      retval = KEY_NULL;
            registry_name_container_type::const_iterator pos(
                m_name_info.find( Name ) );
            if ( pos != m_name_info.end( ) )
            {
                retval = pos->second;
            }
            return retval;
        }

        inline const Registry::registry_container_type&
        Registry::registry( ) const
        {
            return m_info;
        }

    } // namespace Common
} // namespace diskCache

#endif /* DISKCACHE_API__COMMON__REGISTRY_SINGLETON_HH */
