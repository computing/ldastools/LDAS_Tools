//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>

#include <boost/serialization/strong_typedef.hpp>

#include "genericAPI/LDASplatform.hh"

#include "diskcacheAPI/MetaCommands.hh"

#include "diskcacheAPI.hh"
#include "MountPointScanner.hh"
#include "ScanMountPointsDaemon.hh"

BOOST_STRONG_TYPEDEF( INT_4U, cache_write_delay_secs_type );
BOOST_STRONG_TYPEDEF( INT_4U, concurrency_type );
BOOST_STRONG_TYPEDEF( INT_4U, directory_timeout_type );
BOOST_STRONG_TYPEDEF( std::string, log_type );
BOOST_STRONG_TYPEDEF( std::string, log_archive_directory_type );
BOOST_STRONG_TYPEDEF( INT_4S, log_debug_level_type );
BOOST_STRONG_TYPEDEF( std::string, log_directory_type );
BOOST_STRONG_TYPEDEF( std::string, log_format_type );
BOOST_STRONG_TYPEDEF( INT_4U, log_rotate_entry_count_type );
BOOST_STRONG_TYPEDEF( std::string, output_ascii_type );
BOOST_STRONG_TYPEDEF( INT_4U, output_ascii_version_type );
BOOST_STRONG_TYPEDEF( std::string, output_binary_type );
BOOST_STRONG_TYPEDEF( INT_4U, output_binary_version_type );
BOOST_STRONG_TYPEDEF( INT_4U, scan_interval_type );
BOOST_STRONG_TYPEDEF( INT_4S, server_port_type );

#define BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( TYPE )                           \
    std::ostream& operator<<( std::ostream& Stream, const TYPE& Value )        \
    {                                                                          \
        Stream << Value.t;                                                     \
        return Stream;                                                         \
    }

BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( log_type );
BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( log_archive_directory_type );
BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( log_directory_type );
BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( log_format_type );
BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( output_ascii_type );
BOOST_STRONG_TYPEDEF_OUTPUT_OPERATOR( output_binary_type );

static server_port_type const DEFAULT_SERVER_PORT = server_port_type( -1 );
template < typename T >
std::string
generate_parameter( const std::string& Name, const T& Value )
{
    std::ostringstream buffer;

    buffer << Name << "=" << Value;
    return ( buffer.str( ) );
}

struct expected_type
{
    cache_write_delay_secs_type cache_write_delay_secs;
    concurrency_type            concurrency{
        diskCache::MountPointScanner::Concurrency( )
    };
    directory_timeout_type     directory_timeout{ 20 };
    log_type                   log{ GenericAPI::LDASplatform::AppName( ) };
    log_archive_directory_type log_archive_directory{
        GenericAPI::LoggingInfo::ArchiveDirectory( )
    };
    log_debug_level_type log_debug_level{
        GenericAPI::LoggingInfo::DebugLevel( )
    };
    log_directory_type log_directory{
        GenericAPI::LoggingInfo::LogDirectory( )
    };
    log_format_type            log_format{ GenericAPI::LoggingInfo::Format( ) };
    output_ascii_type          output_ascii{ "frame_cache_dump" };
    output_ascii_version_type  output_ascii_version{ 0x00FF };
    output_binary_type         output_binary{ ".frame.cache" };
    output_binary_version_type output_binary_version{ 0x0101 };
    scan_interval_type         scan_interval{
        diskCache::ScanMountPointsDaemon::Interval( )
    };
};

template < typename ExpectedType, typename SourceType >
void
validator( std::string const& Check,
           std::string const& VariableName,
           SourceType         Value,
           ExpectedType       Expected )
{
    BOOST_CHECK_MESSAGE( ExpectedType( Value ) == Expected,
                         Check << ": " //
                               << VariableName << ": " //
                               << " set to: " << Value //
                               << " instead of: " << Expected //
    );
}

void
validator( const std::string& Check,
           int                server_port,
           server_port_type   ExpectedServerPort )
{
    validator( Check, "SERVER_PORT", server_port, ExpectedServerPort );
}

void
validator( const std::string& Check, const expected_type& ExpectedValues )
{
    validator( Check,
               "CACHE_WRITE_DELAY_SECS",
               diskCache::DumpCacheDaemon::Interval( ) / 1000,
               ExpectedValues.cache_write_delay_secs );
    validator( Check,
               "CONCURRENCY",
               diskCache::MountPointScanner::Concurrency( ),
               ExpectedValues.concurrency );
    validator( Check,
               "DIRECTORY_TIMEOUT",
               LDASTools::AL::Directory::Timeout( ),
               ExpectedValues.directory_timeout );
    validator( Check,
               "SCAN_INTERVAL",
               diskCache::ScanMountPointsDaemon::Interval( ),
               ExpectedValues.scan_interval );
    validator( Check,
               "LOG",
               GenericAPI::LDASplatform::AppName( ),
               ExpectedValues.log );
    validator( Check,
               "LOG_ARCHIVE_DIRECTORY",
               GenericAPI::LoggingInfo::ArchiveDirectory( ),
               ExpectedValues.log_archive_directory );
    validator( Check,
               "LOG_DEBUG_LEVEL",
               GenericAPI::LoggingInfo::DebugLevel( ),
               ExpectedValues.log_debug_level );
    validator( Check,
               "LOG_DIRECTORY",
               GenericAPI::LoggingInfo::LogDirectory( ),
               ExpectedValues.log_directory );
    validator( Check,
               "LOG_FORMAT",
               GenericAPI::LoggingInfo::Format( ),
               ExpectedValues.log_format );
    validator( Check,
               "OUTPUT_ASCII",
               diskCache::DumpCacheDaemon::FilenameAscii( ),
               ExpectedValues.output_ascii );
    validator( Check,
               "OUTPUT_ASCII_VERSION",
               diskCache::DumpCacheDaemon::ASCIIVersion( ),
               ExpectedValues.output_ascii_version );
    validator( Check,
               "OUTPUT_BINARY",
               diskCache::DumpCacheDaemon::FilenameBinary( ),
               ExpectedValues.output_binary );
    validator( Check,
               "OUTPUT_BINARY_VERSION",
               diskCache::DumpCacheDaemon::BinaryVersion( ),
               ExpectedValues.output_binary_version );
}

BOOST_AUTO_TEST_CASE( daemon_default )
{
    using namespace diskCache::MetaCommand;

    diskCache::Initialize( );

    expected_type expected;
    //-------------------------------------------------------------------
    // Daemon class initializes some values
    //-------------------------------------------------------------------
    expected.cache_write_delay_secs = 120;
    expected.concurrency = 4;
    expected.scan_interval = 16000;

    ClientServerInterface::ServerInfo server_info;

    char const* argv[] = { "daemon" };

    CommandLineOptions parameters( sizeof( argv ) / sizeof( argv[ 0 ] ), argv );

    Daemon daemon( parameters, server_info );

    validator( "daemon_default", expected );
    validator( "daemon_default", daemon.ServerPort( ), DEFAULT_SERVER_PORT );
}

BOOST_AUTO_TEST_CASE( daemon_command_line_options )
{
    using namespace diskCache::MetaCommand;

    diskCache::Initialize( );

    expected_type expected;

    expected.cache_write_delay_secs = 120;
    expected.directory_timeout = 60;
    expected.output_ascii = "daemon_clo_cache.txt";
    expected.output_binary = "daemon_clo_cache.dat";

    ClientServerInterface::ServerInfo server_info;

    std::vector< std::string > args;

    args.push_back( "daemon" );
    args.push_back( generate_parameter( "--directory-timeout",
                                        expected.directory_timeout ) );
    args.push_back(
        generate_parameter( "--output-ascii", expected.output_ascii ) );
    args.push_back(
        generate_parameter( "--output-binary", expected.output_binary ) );

    CommandLineOptions parameters( args );

    Daemon daemon( parameters, server_info );

    validator( "daemon_command_line_options", expected );
}

BOOST_AUTO_TEST_CASE( daemon_config_options )
{
    using namespace diskCache::MetaCommand;

    diskCache::Initialize( );

    std::string      config_filename( "daemon_config_options.rsc" );
    server_port_type expected_server_port( 11300 );
    expected_type    expected;

    //-------------------------------------------------------------------
    expected.scan_interval = 16000;
    //-------------------------------------------------------------------
    expected.cache_write_delay_secs = 71;
    expected.concurrency = 8;
    expected.directory_timeout = 70;
    expected.log = "daemon_config_cache";
    expected.log_archive_directory = "daemon_archive.d";
    expected.log_directory = "daemon_logs.d";
    expected.log_debug_level = 1;
    expected.log_format = "text";
    expected.output_ascii = "daemon_config_cache.txt";
    expected.output_ascii_version = 0x0101;
    expected.output_binary = "daemon_config_cache.dat";
    expected.output_binary_version = 0x00FF;

    std::ofstream config_stream( config_filename );
    config_stream //
        << "SERVER_PORT=" << expected_server_port << std::endl
        << "CACHE_WRITE_DELAY_SECS=" << expected.cache_write_delay_secs
        << std::endl //
        << "CONCURRENCY=" << expected.concurrency << std::endl //
        << "DIRECTORY_TIMEOUT=" << expected.directory_timeout << std::endl //
        << "LOG=" << expected.log << std::endl //
        << "LOG_ARCHIVE_DIRECTORY=" << expected.log_archive_directory
        << std::endl //
        << "LOG_DEBUG_LEVEL=" << expected.log_debug_level << std::endl //
        << "LOG_DIRECTORY=" << expected.log_directory << std::endl //
        << "LOG_FORMAT=" << expected.log_format << std::endl //
        << "OUTPUT_ASCII=" << expected.output_ascii << std::endl //
        << "OUTPUT_ASCII_VERSION=0x" << std::hex
        << expected.output_ascii_version << std::dec << std::endl //
        << "OUTPUT_BINARY=" << expected.output_binary << std::endl //
        << "OUTPUT_BINARY_VERSION=0x" << std::hex
        << expected.output_binary_version << std::dec << std::endl //
        ;
    config_stream.close( );

    ClientServerInterface::ServerInfo server_info;

    std::vector< std::string > args;

    args.push_back( "daemon" );
    args.push_back(
        generate_parameter( "--configuration-file", config_filename ) );

    CommandLineOptions parameters( args );

    Daemon daemon( parameters, server_info );

    validator( "daemon_config_options", expected );
    validator( "daemon_default", daemon.ServerPort( ), expected_server_port );
}

BOOST_AUTO_TEST_CASE( scan_command_line_options )
{
    using namespace diskCache::MetaCommand;

    diskCache::Initialize( );

    expected_type expected;

    //-------------------------------------------------------------------
    expected.cache_write_delay_secs = 120;
    expected.concurrency = 1;
    //-------------------------------------------------------------------
    expected.output_ascii = "scan_clo_cache.txt";
    expected.output_binary = "scan_clo_cache.dat";

    ClientServerInterface::ServerInfo server_info;

    std::vector< std::string > args;

    args.push_back( "scan" );
    args.push_back(
        generate_parameter( "--output-ascii", expected.output_ascii ) );
    args.push_back(
        generate_parameter( "--output-binary", expected.output_binary ) );

    CommandLineOptions parameters( args );

    Scan scan( parameters );

    validator( "scan_command_line_options", expected );
}
