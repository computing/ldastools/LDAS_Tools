//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

///=======================================================================
// WARNING: Do not use as an example of how to program against the API.
//          This file is strictly for low level testing.
//=======================================================================

#include <unistd.h>

// General Header Files
#include "ldastoolsal/unittest.h"

//-----------------------------------------------------------------------
// To gain access to all of the functions that need to be tested,
// include the entire source.
//-----------------------------------------------------------------------
#if defined( __clang__ ) && ( __clang_major__ >= 7 )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif /* defined(__clang__) && ( __clang_major__ >= 7 ) */
#ifdef __gcc__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wkeyword-macro"
#endif /* __gcc__ */
#define private public
#ifdef __gcc__
#pragma GCC diagnostic pop
#endif /* __gcc__ */
#if defined( __clang__ ) && ( __clang_major__ >= 7 )
#pragma clang diagnostic pop
#endif /* defined(__clang__) && ( __clang_major__ >= 7 ) */

#include "../src/Cache/SDGTx.cc"
#undef private

#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
LDASTools::Testing::UnitTest Test;

#include "tSDGTxSupport.icc"
#include "tSDGTxDirectoryInfo.icc"
#include "tSDGTxScanData.icc"
#include "tSDGTxDirectory.icc"
#include "tSDGTxFind.icc"

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Get ready for testing
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );
    if ( !LDASTools::AL::MemChecker::IsExiting( ) )
    {

        GenericAPI::LoggingInfo::LogDirectory( "-" );
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        GenericAPI::LDASplatform::AppName( "test_createRDS_cpp" );
        if ( Test.Verbosity( ) > 0 )
        {
            GenericAPI::setLogDebugLevel( Test.Verbosity( ) );
        }

        //---------------------------------------------------------------------
        // Execute a series of tests
        //---------------------------------------------------------------------
        test_directory_info( );
        test_scan_data( );
#if !__APPLE__
        test_directory( );
        test_find( );
#endif /* ! __APPLE */

        //---------------------------------------------------------------------
        // Cleanup and exit
        //---------------------------------------------------------------------
        GenericAPI::SyncLog( );
    }
    Test.Exit( );

    //---------------------------------------------------------------------
    // Should never get this far
    //---------------------------------------------------------------------
    return 1;
}
