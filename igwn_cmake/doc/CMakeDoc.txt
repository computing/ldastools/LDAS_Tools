//-----------------------------------------------------------------------
/**
@mainpage IGWN CMake Software Developement Tools

@section section_abstract Abstract
The main focus of this package is a collection of CMake macros that mimic the
.m4 macros provided by autoconf and automake.

@section quick_start Quick Start

@code{.cmake}
cm_prereq(3.3)
cm_init(
  MyProject
  1.0.0
  https://myproject.org/bugs
  MyProject
  https://myproject.org/
  )
@endcode

 */
//-----------------------------------------------------------------------
