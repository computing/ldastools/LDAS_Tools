# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_PKG_SWIG
#     Check if thread library support
# ---------------------------------------------------------------------
include( CMakeParseArguments )
include( Autotools/ArchiveX/cx_msg_debug_variable )

## /**
## @igwn_group{Archive,}
## @igwn_group_begin
## */
## /**
## @fn cx_pkg_swig( REQUIRED VERSION <Version Number> )
## @brief Check for swig
## @details
## Check if SWIG is available
##
## @igwn_named_param{REQUIRED}
## Option indicating that SWIG must be available
##
## @igwn_named_param{VERSION}
## Minimum version of SWIG needed by 
##
## @code{.cmake}
## cx_pkg_swig( )
## @endcode
##
## @code{.cmake}
## cx_pkg_swig( REQUIRED )
## @endcode
##
## @code{.cmake}
## cx_pkg_swig( VERSION 3.0.7 )
## @endcode
##
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cx_pkg_swig( );
function( cx_pkg_swig )
  set(options
    REQUIRED
    )
  set(oneValueArgs
    VERSION
    )
  set(multiValueArgs FILES )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_REQUIRED )
    set( REQUIRED_OPTION "REQUIRED")
  endif( )
  if ( NOT "${ARG_VERSION}" STREQUAL "" )
    set(VERSION_OPTION ${ARG_VERSION})
  endif()
  find_package(SWIG ${VERSION_OPTION} ${REQUIRED_OPTION})

endfunction()
