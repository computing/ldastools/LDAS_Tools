# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_THREAD
#     Check if thread library support
# ---------------------------------------------------------------------
include( Autotools/ArchiveX/cx_msg_debug_variable )

function( cx_thread )
  set( THREADS_PREFER_PTHREAD_FLAG YES )

  find_package(Threads )
  cx_msg_debug_variable( CMAKE_THREAD_LIBS_INIT )
  cx_msg_debug_variable( CMAKE_USE_SPROC_INIT )
  cx_msg_debug_variable( CMAKE_USE_WIN32_THREADS_INIT )
  cx_msg_debug_variable( CMAKE_USE_PTHREADS_INIT )
  cx_msg_debug_variable( CMAKE_HP_PTHREADS_INIT )
  cx_msg_debug_variable( THREADS_HAVE_PTHREAD_ARG )

  if ( THREAD_HAVE_PTHREAD_ARG )
    cm_subst( THREAD_CFLAGS ${CMAKE_THREAD_LIBS_INIT} )
  endif ( )
  if ( CMAKE_THREAD_LIBS_INIT )
    cm_subst( THREAD_LIBS ${CMAKE_THREAD_LIBS_INIT} )
  endif( )
endfunction()
