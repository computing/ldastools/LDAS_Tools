#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------

include( Autotools/Internal/ci_project_config )

#------------------------------------------------------------------------
# cm_config_headers( header )
#
# NOTE:
#   This is part of the autoheaders interface
#
# TODO:
#   Split header on colon
#------------------------------------------------------------------------
function( cm_config_headers header )
  set( header_template "${header}.in" )
  #----------------------------------------------------------------------
  # Generate the input file
  #----------------------------------------------------------------------
  if ( DEFINED CH_TOP_TEXT )
    ci_project_config( FILENAME ${header_template} PREFIX "${CH_TOP_TEXT}" )
  endif()
  if ( CH_BODY )
    list( SORT CH_BODY )
    foreach( template ${CH_BODY} )
      string( FIND "${template}" "&" POS )
      if ( POS GREATER -1 )
        string( SUBSTRING "${template}" 0 ${POS} var )
        math( EXPR POS "${POS}+1" )
        string( SUBSTRING "${template}" ${POS}  -1 desc )
        string( REPLACE "\\n" "\n" desc "${desc}" )
        string( REGEX REPLACE "^[ \t]" "" desc "${desc}" )
        string( REGEX REPLACE "[\n][ \t]*" "\n  " desc "${desc}" )
      else ( )
        set( var "${template}" )
        string( REGEX REPLACE "^[ \t]" "" var "${var}" )
        string( REGEX REPLACE "[\n][ \t]*" "\n  " var "${var}" )
        set( desc "No description" )
      endif( )
      ci_project_config( WRITE
	"/* ${desc} */"
	"#cmakedefine ${var}    @${var}@"
	" "
	)
    endforeach()
  endif()
  if ( DEFINED CH_BOTTOM_TEXT )
    ci_project_config( WRITE "${CH_BOTTOM_TEXT}" )
  endif()
  #----------------------------------------------------------------------
  # Perform substitution
  #----------------------------------------------------------------------
  configure_file(
    "${header_template}"
    "${header}"
    @ONLY )
endfunction()
