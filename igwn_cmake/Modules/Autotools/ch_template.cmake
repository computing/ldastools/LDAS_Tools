#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( Autotools/Internal/ci_cache )

#------------------------------------------------------------------------
# ch_template( key description)
#
#   Place all text at the bottom of the configuration file
#
# NOTE:
#   This is part of the autoheader look and feel
#------------------------------------------------------------------------
function( ch_template key )
  string( REPLACE ";" "\\n" body "${ARGN}" )
  list( APPEND CH_BODY "${key}&${body}" )
  set( CH_BODY "${CH_BODY}" CACHE INTERNAL "" FORCE )
endfunction()
