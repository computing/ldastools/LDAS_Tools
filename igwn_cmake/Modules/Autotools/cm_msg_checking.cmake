include( CMakeParseArguments )

include( Autotools/cm_msg_notice )
include( Autotools/cm_msg_result )

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cm_msg_checking( MSG )
## @brief Display checking message
## @details
## Display a message informing the user of a feature being checked
## This command should be immediately followed by \ref cm_msg_result
##
## @param MSG
## Message to display
##
## @sa cm_msg_result
##
## @note
## The message displayed will not contain a newline.
##
## @code{.cmake}
## cm_msg_checking( "for something" )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_msg_checking( MSG );
function(CM_MSG_CHECKING MSG)
  set(options)
  set(oneValueArgs CONDITION )
  set(multiValueArgs SOURCE DEFINES)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "Checking ${MSG} ... " NO_NL )
  if ( NOT "" STREQUAL "${ARG_CONDITION}" )
    if ( ${ARG_CONDITION} )
      set( _result "yes" )
    else ( ${ARG_CONDITION} )
      set( _result "no" )
    endif ( ${ARG_CONDITION} )
    cm_msg_result( ${_result} )
  endif ( NOT "" STREQUAL "${ARG_CONDITION}" )
endfunction(CM_MSG_CHECKING)
