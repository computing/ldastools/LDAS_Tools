#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
#------------------------------------------------------------------------
# ci_join( VAR CHAR )
#
#   Join each element of list with CHAR
#
#------------------------------------------------------------------------
function( ci_join VAR CHAR )
  foreach( element ${ARGN} )
    if ( ${VAR} )
      set( ${VAR} "${${VAR}}${element}${CHAR}" )
    else()
      set( ${VAR} "${element}${CHAR}" )
    endif()
    set( ${VAR} "${${VAR}}" PARENT_SCOPE )
  endforeach()
endfunction()
