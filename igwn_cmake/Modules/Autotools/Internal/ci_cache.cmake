#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

macro(ci_cache var )
  set(options OPT_PARENT_SCOPE SCOPE_LOCAL )
  set(oneValueArgs TYPE)
  set(multiValueArgs VALUE)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_VALUE )
    set(ARG_VALUE ${${var}})
  endif ( NOT ARG_VALUE )

  if ( ARG_OPT_PARENT_SCOPE )
    set( ${var} ${ARG_VALUE} PARENT_SCOPE )
  elseif ( ARG_SCOPE_LOCAL )
    set( ${var} ${ARG_VALUE} )
  else ( ARG_OPT_PARENT_SCOPE )
    unset( ${var} CACHE )
    if ( ARG_TYPE )
      set( ${var} ${ARG_VALUE} CACHE ${ARG_TYPE} "" FORCE)
    else( ARG_TYPE )
      set( ${var} ${ARG_VALUE} CACHE INTERNAL "" )
    endif( ARG_TYPE )
  endif ( ARG_OPT_PARENT_SCOPE )

endmacro( )

macro( cm_cache )
  ci_cache( ${ARGN} )
endmacro( )
