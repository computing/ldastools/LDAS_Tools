#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

macro(ci_cache_list action var )
  set(options  )
  set(oneValueArgs)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if( NOT DEFINED ${var} )
    set( type_ INTERNAL )
    set( desc_ "For internal use" )
  else( )
    get_variable_property( type_ ${var} TYPE )
    get_variable_property( desc_ ${var} HELPSTRING )
  endif( )
  list( ${action} ${var} ${ARGN})
  set( ${var} "${${var}}" CACHE ${type_} "${desc_}" FORCE )
endmacro( )
