#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_check_funcs( <file> [ <file> ...] )
#------------------------------------------------------------------------
include( CheckFunctionExists )
include( CMakeParseArguments )

include( Autotools/cm_define )
include( Autotools/ArchiveX/cm_aux_variable_normalize )

function(CM_CHECK_FUNCS )
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FUNCTIONS )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_FUNCTIONS )
    if ( ARG_UNPARSED_ARGUMENTS )
      set( ARG_FUNCTIONS ${ARG_FUNCTIONS} ${ARG_UNPARSED_ARGUMENTS} )
    endif( ARG_UNPARSED_ARGUMENTS )
  else( ARG_FUNCTIONS )
    if ( ARG_UNPARSED_ARGUMENTS )
      set( ARG_FUNCTIONS ${ARG_UNPARSED_ARGUMENTS} )
    else( ARG_UNPARSED_ARGUMENTS )
      set( ARG_FUNCTIONS "" )
    endif( ARG_UNPARSED_ARGUMENTS )
  endif( ARG_FUNCTIONS )

  foreach( f ${ARG_FUNCTIONS} )
    cm_aux_variable_normalize( var "HAVE_${f}" )
    unset( ${var} CACHE )
    check_function_exists( ${f} ${var} )
    cm_define(
      VARIABLE ${var}
      DESCRIPTION "Define to 1 if you have the `${f}' function." )
  endforeach( f )

endfunction(CM_CHECK_FUNCS)
