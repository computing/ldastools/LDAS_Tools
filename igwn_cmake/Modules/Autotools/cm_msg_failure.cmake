#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_error( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include ( Autotools/cm_msg_notice )

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cm_msg_failure( txt )
## @brief Display failure message
## @details
## Display a message describing an issue preventing configuration to complete.
##
## @param txt
## Message to display
##
## @code{.cmake}
## cm_msg_failure( "Something really bad has happened" )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_msg_failure( txt );
function( cm_msg_failure txt )
  set( prefix "FAILURE" )

  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "${prefix}: ${txt}" )
  message( FATAL_ERROR "" )

endfunction( )
