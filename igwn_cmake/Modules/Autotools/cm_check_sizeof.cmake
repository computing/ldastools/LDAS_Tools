#========================================================================
# -*- mode: cmake; -*-
#------------------------------------------------------------------------
#
# cm_check_sizeof( TYPE DEFAULT )
#
#------------------------------------------------------------------------
include(CheckTypeSize)
include(Autotools/cm_define)

function(CM_CHECK_SIZEOF TYPE DEFAULT)
  string( TOUPPER "sizeof_${TYPE}" var )
  string( REPLACE "*" "P" var ${var} )
  string( REGEX REPLACE "[^A-Za-z0-9]" "_" var ${var} )
  check_type_size( ${TYPE} ${var} )
  if ( NOT ${var} )
    set( ${var} ${DEFAULT} )
  endif ( NOT ${var} )
  cm_define(
    VARIABLE ${var}
    DESCRIPTION "The size of `${TYPE}', as computed by sizeof."
    )
  message( STATUS "Checking sizeof ${TYPE} ... ${${var}}" )
endfunction(CM_CHECK_SIZEOF)
