#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# Simulate the AC_TRY_RUN macro
#
# cm_try_run( VARIABLE <variable> RETURN_VALUE <return>
#   SOURCE <source-list>
#   DEFINES <defines> )
#
# <return> - Expected integer return value from running the program.
#            The default is 0.
# <source-list> 
#          - List of source lines to compile. Use "[<sc>]" in place
#            of semicolons
#------------------------------------------------------------------------
include( CMakeParseArguments )

function( CM_TRY_RUN )
  set(options)
  set(oneValueArgs VARIABLE RETURN_VALUE )
  set(multiValueArgs SOURCE DEFINES)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_RETURN_VALUE )
    set( ARG_RETURN_VALUE 0 )
  endif ( NOT ARG_RETURN_VALUE )
  set(fn "${CMAKE_CURRENT_BINARY_DIR}/${ARG_VARIABLE}.cc")
  file(REMOVE ${fn})
  foreach( line ${ARG_SOURCE} )
    string( REPLACE "[<sc>]" ";" line "${line}" )
    file(APPEND ${fn} "${line}\n")
  endforeach( line )
  unset( _run_var CACHE )
  unset( _compile_var CACHE )
  try_run(
    _run_var
    _compile_var
    ${CMAKE_CURRENT_BINARY_DIR}
    ${fn}
    COMPILE_DEFINITIONS ${ARG_DEFINES}
    COMPILE_OUTPUT_VARIABLE _compile_msgs
    RUN_OUTPUT_VARIABLE _run_msgs
    )
  # cx_msg_debug( "${_compile_var} AND ${_run_var}" )
  if ( _compile_var AND "${_run_var}" EQUAL ${ARG_RETURN_VALUE} )
    set( ${ARG_VARIABLE} 1 )
  else ( _compile_var AND "${_run_var}" EQUAL ${ARG_RETURN_VALUE} )
    # cx_msg_debug_variable( _compile_msgs )
    # cx_msg_debug_variable( _run_msgs )
  endif ( _compile_var AND "${_run_var}" EQUAL ${ARG_RETURN_VALUE} )
  unset( _run_var CACHE )
  unset( _compile_var CACHE )
  if(${ARG_VARIABLE})
    set(${ARG_VARIABLE} 1 PARENT_SCOPE)
  else(${ARG_VARIABLE})
    set(${ARG_VARIABLE} 0 PARENT_SCOPE)
  endif(${ARG_VARIABLE})
endfunction( CM_TRY_RUN )
