include( Autotools/Internal/ci_project_config )

function(CM_CONFIG_HEADER filename)
  ci_project_config(
    FILENAME ${filename} )
endfunction(CM_CONFIG_HEADER)
