#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_warn( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include ( Autotools/cm_msg_notice )

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cm_msg_warn( txt )
## @brief Display warning message
##
## @details
## Display a message describing a possible probelm
##
## @param  txt
##    If a list, then a collection of items to be displayed
##    If a string, then a text string to be displayed
##
## @code{.cmake}
## cm_msg_warn( "This was unexpected" )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_msg_warn( txt );
function( cm_msg_warn txt )
  set( prefix "WARNING" )

  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "${prefix}: ${txt}" )

endfunction( )
