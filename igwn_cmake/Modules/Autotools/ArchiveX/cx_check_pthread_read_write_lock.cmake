# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  AX_CHECK_PTHREAD_READ_WRITE_LOCK
#     Check if C++ supports templates as parameters to templates
# ---------------------------------------------------------------------
include( Autotools/cm_check_headers )
include( Autotools/cm_try_compile )
include( Autotools/Archive/cx_thread )

function( cx_check_pthread_read_write_lock )
  cx_thread( )
  if ( CMAKE_USE_PTHREADS_INIT )
    cm_check_headers("pthread.h")
    set(inc "
#if HAVE_PTHREAD_H
#include <pthread.h>
#endif /* HAVE_PTHREAD_H */
")

    set(body "
   pthread_rwlock_t l;
   pthread_rwlock_init( &l, (pthread_rwlockattr_t*)NULL );
")

    cm_try_compile("${inc}" "${body}" HAVE_PTHREAD_RW_LOCK
      DEFINES -DHAVE_PTHREAD_H=${HAVE_PTHREAD_H}
      ${THREAD_CFLAGS}
      LIBRARIES ${THREAD_LIBS}
      )
  endif( )
  cm_define(
    VARIABLE HAVE_PTHREAD_RW_LOCK
    DESCRIPTION "Defined if pthread read/write locks supported" )
  cm_msg_checking( "if pthread library has read/write locks" )
  if ( HAVE_PTHREAD_RW_LOCK )
    cm_msg_result( "yes" )
  else( )
    cm_msg_result( "no" )
  endif( )
endfunction( )
