#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_prog_doxygen
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/Internal/ci_set_policy )

ci_set_policy(CMP0053 NEW)

function(_arg_substitute variable default)
  if ( NOT ARG_${variable} )
    set(ARG_${variable} ${default})
  endif ( NOT ARG_${variable} )
  set( ${variable} ${ARG_${variable}} PARENT_SCOPE)
endfunction(_arg_substitute)

function(_create_doxygen_cfg_template)
  set(options)
  set(oneValueArgs
    AUX_FILES
    DOXYGEN_CFG
    DOXYGEN_CFG_TEMPLATE
    DOXYGEN_EXCLUDE_PATTERNS
    DOXYGEN_EXTRACT_ALL
    DOXYGEN_FILTER_SOURCE_FILES
    DOXYGEN_GENERATE_HTML
    DOXYGEN_GENERATE_LATEX
    DOXYGEN_GENERATE_TAGFILE
    DOXYGEN_WARN_NO_PARAMDOC
    DOXYGEN_WARN_IF_UNDOCUMENTED
    EXTRACT_ANON_NSPACES
    EXTRACT_LOCAL_CLASSES
    EXTRACT_LOCAL_METHODS
    EXTRACT_PRIVATE
    EXTRACT_STATIC
    HIDE_UNDOC_CLASSES
    HIDE_UNDOC_MEMBERS
    NAMESPACE
    RECURSIVE
    SRCDIR_SRC
    SORT_BY_SCOPE_NAME
    WARN_NO_PARAMDOC
    WARN_IF_UNDOCUMENTED
    )
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  _arg_substitute(NAMESPACE "doxygen")
  _arg_substitute(DOXYGEN_CFG "${CMAKE_CURRENT_BINARY_DIR}/${NAMESPACE}.cfg")
  get_filename_component(BASE_DOXYGEN_CFG ${DOXYGEN_CFG} NAME)
  _arg_substitute(DOXYGEN_CFG_TEMPLATE "${CMAKE_CURRENT_BINARY_DIR}/${BASE_DOXYGEN_CFG}.in")

  _arg_substitute(AUX_FILES "")
  _arg_substitute(DOXYGEN_EXTRACT_ALL "YES")
  _arg_substitute(EXTRACT_PRIVATE "NO")
  _arg_substitute(EXTRACT_STATIC "NO")
  _arg_substitute(EXTRACT_LOCAL_CLASSES "NO")
  _arg_substitute(EXTRACT_LOCAL_METHODS "NO")
  _arg_substitute(EXTRACT_ANON_NSPACES "NO")
  _arg_substitute(FILE_PATTERNS "*.tcc 8.icc *.cc *.hh *.c *.h *.txt *.hh.in")
  _arg_substitute(HIDE_UNDOC_MEMBERS "YES")
  _arg_substitute(HIDE_UNDOC_CLASSES "YES")
  _arg_substitute(SORT_BY_SCOPE_NAME "YES")
  
  _arg_substitute(DOXYGEN_EXCLUDE_PATTERNS "")
 
  _arg_substitute(DOXYGEN_GENERATE_HTML "YES")
 
  _arg_substitute(DOXYGEN_GENERATE_LATEX "YES")
  _arg_substitute(DOXYGEN_GENERATE_TAGFILE "${NAMESPACE}.tag")
 
  _arg_substitute(WARN_NO_PARAMDOC "YES")
  _arg_substitute(WARN_IF_UNDOCUMENTED "YES")
 
  _arg_substitute(DOXYGEN_FILTER_SOURCE_FILES "NO")
  _arg_substitute(SRCDIR_SRC "${CMAKE_SOURCE_DIR}/src")
  _arg_substitute(RECURSIVE "YES")
 
  #----------------------------------------------------------------------
  # Create the template file
  #----------------------------------------------------------------------
  file(WRITE ${DOXYGEN_CFG_TEMPLATE}
    "#--------------------------------------------\n"
    "# Project characteristics\n\n"
    "#--------------------------------------------\n"
    "PROJECT_NAME=@PROJECT@\n"
    "PROJECT_NUMBER=@PROJECT_VERSION@\n"
    "#--------------------------------------------\n"
    "# Build related options\n"
    "#--------------------------------------------\n"
    "EXTRACT_ALL=@DOXYGEN_EXTRACT_ALL@\n"
    "EXTRACT_PRIVATE=@EXTRACT_PRIVATE@\n"
    "EXTRACT_STATIC=@EXTRACT_STATIC@\n"
    "EXTRACT_LOCAL_CLASSES=@EXTRACT_LOCAL_CLASSES@\n"
    "EXTRACT_LOCAL_METHODS=@EXTRACT_LOCAL_METHODS@\n"
    "EXTRACT_ANON_NSPACES=@EXTRACT_ANON_NSPACES@\n"
    "HIDE_UNDOC_MEMBERS=@HIDE_UNDOC_MEMBERS@\n"
    "HIDE_UNDOC_CLASSES=@HIDE_UNDOC_CLASSES@\n"
    "SORT_BY_SCOPE_NAME=@SORT_BY_SCOPE_NAME@\n"
    "#--------------------------------------------\n"
    "# Sources to use\n"
    "#--------------------------------------------\n"
    "INPUT=@AUX_FILES@ @SRCDIR_SRC@\n"
    "FILE_PATTERNS=@FILE_PATTERNS@\n"
    "RECURSIVE=@RECURSIVE@\n"
    # ifneq (@DOXYGEN_EXCLUDE_PATTERNS),)
    #   "EXCLUDE_PATTERNS=@DOXYGEN_EXCLUDE_PATTERNS@\n"
    # endif
    "#--------------------------------------------\n"
    "# Quiet\n"
    "#--------------------------------------------\n"
    # ifeq (@AM_V_at),@)
    #	"QUIET=YES\n"
    # else
    #	"QUIET=NO\n"
    # endif
    "#--------------------------------------------\n"
    "# How to handle warnings\n"
    "#--------------------------------------------\n"
    "WARNINGS=YES\n"
    "WARN_NO_PARAMDOC=@WARN_NO_PARAMDOC@\n"
    "WARN_IF_UNDOCUMENTED=@WARN_IF_UNDOCUMENTED@\n"
    "WARN_LOGFILE=doxygen_warnings.log\n"
    "#--------------------------------------------\n"
    "# Filtering of the source (Converting from Perceps to doxygen@\n"
    "#--------------------------------------------\n"
    "FILTER_SOURCE_FILES=@FILTER_SOURCE_FILES@\n"
    # ifneq (@DOXYGEN_FILTER_SOURCE_FILES),NO)
    #  "INPUT_FILTER=@top_srcdir)/config/scripts/doxygen.pl\n"
    # endif
    # ifneq (@DOXYGEN_EXCLUDE),)
    #  "EXCLUDE=@DOXYGEN_EXCLUDE@\n"
    # endif
    # ifneq (@DOXYGEN_EXCLUDE_PATTERNS),)
    #  "EXCLUDE_PATTERNS=@DOXYGEN_EXCLUDE_PATTERNS@\n"
    # endif
    "EXAMPLE_PATH=@DOXYGEN_EXAMPLE_PATH@\n"
     "VERBATIM_HEADERS=NO\n"
    "#--------------------------------------------\n"
    "# Where to output\n"
    "#--------------------------------------------\n"
    "OUTPUT_DIRECTORY=doxygen\n"
    "STRIP_FROM_PATH=@STRIP_FROM_PATH@\n"
    #---------------------------------------------------------------
    # MAN Options
    #---------------------------------------------------------------
    "#--------------------------------------------\n"
    "# Types of output\n"
    "#--------------------------------------------\n"
    "GENERATE_MAN=NO\n"
    "#--------------------------------------------\n"
    "# Source browsing related options\n"
    "#--------------------------------------------\n"
    "INLINE_SOURCES=NO\n"
    "INLINE_INHERITED_MEMB=YES\n"
    "#--------------------------------------------\n"
    "# Preprocessor related options\n"
    "#--------------------------------------------\n"
    "ENABLE_PREPROCESSING=YES\n"
    "MACRO_EXPANSION=YES\n"
    "EXPAND_ONLY_PREDEF=YES\n"
    "EXPAND_AS_DEFINED=\n"
    # @AM_V_at)for v in @DOXYGEN_EXPAND_AS_DEFINED); \
    # do \
    #   echo 'EXPAND_AS_DEFINED+='$$v >> @DOXYGEN_CFG@; \
    # done
    "SEARCH_INCLUDES=YES\n"
    "INCLUDE_PATH=@DOXYGEN_INCLUDE_PATH@\n"
    "PREDEFINED=@DOXYGEN_PREDEFINED@\n"
    #---------------------------------------------------------------
    # External Reference Options
    #---------------------------------------------------------------
    "#--------------------------------------------\n"
    "# External reference options\n"
    "#--------------------------------------------\n"
    "GENERATE_TAGFILE=@DOXYGEN_GENERATE_TAGFILE@\n"
    "TAGFILES=@DOXYGEN_TAGFILES@\n"
    #---------------------------------------------------------------
    # HTML Options
    #---------------------------------------------------------------
    "#--------------------------------------------\n"
    "# HTML Options\n"
    "#--------------------------------------------\n"
    # @file="doxygen.cfg\n"
    "GENERATE_HTML=@DOXYGEN_GENERATE_HTML@\n"
    #---------------------------------------------------------------
    # Latex Options
    #---------------------------------------------------------------
    "#--------------------------------------------\n"
    "# Latex Options\n"
    "#--------------------------------------------\n"
    # ifeq (@DOXYGEN_LATEX),)
    #	"GENERATE_LATEX=NO\n"
    # else
    #	"GENERATE_LATEX=@DOXYGEN_GENERATE_LATEX@\n"
    # endif
    #---------------------------------------------------------------
    # DOT Options
    #---------------------------------------------------------------
    "#--------------------------------------------\n"
    "# DOT Options\n"
    "#--------------------------------------------\n"
    # ifeq (@DOT),)
    #   "HAVE_DOT=NO\n"
    # else
    #	"HAVE_DOT=YES\n"
    # endif
    "DOT_PATH=@DOT_PATH@\n"
    "DOT_IMAGE_FORMAT=png\n"
    "CLASS_GRAPH=YES\n"
    "GRAPHICAL_HIERARCHY=YES\n"
   )
 configure_file(
   ${DOXYGEN_CFG_TEMPLATE}
   ${DOXYGEN_CFG}
   @ONLY
   )
endfunction(_create_doxygen_cfg_template)

function(cx_prog_doxygen)
  set(options USER_DOCS DEVELOPER_DOCS)
  set(oneValueArgs 
    DOC_TYPE
    DOXYGEN_CFG
    )
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  find_package(Doxygen)
  # cx_msg_debug_variable(DOXYGEN_FOUND)
  if (DOXYGEN_FOUND)
    set(htmldocdir )
    if ( NOT ARG_DOXYGEN_CFG )
      set(ARG_DOXYGEN_CFG "${CMAKE_CURRENT_BINARY_DIR}/doxygen.cfg")
    endif ( NOT ARG_DOXYGEN_CFG )
    _create_doxygen_cfg_template(
      DOXYGEN_CFG ${ARG_DOXYGEN_CFG}
      )

    add_custom_target(
      doc ALL
      COMMAND ${DOXYGEN_EXECUTABLE} ${ARG_DOXYGEN_CFG}
      COMMENT "Generating ${ARG_DOC_TYPE} documentation with Doxygen"
      VERBATIM
      )
    install(
      DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doxygen/html"
      DESTINATION "${CMAKE_INSTALL_FULL_DOCDIR}/html"
      )
  endif (DOXYGEN_FOUND)
endfunction(cx_prog_doxygen)
