#========================================================================
# -*- mode: cmake; -*-
#------------------------------------------------------------------------
#
# cm_cxx_exceptions_rethrow( )
#
#     Check if C++ supports exceptions rethrowing
#
#------------------------------------------------------------------------
include( Autotools/Internal/ci_result )
include( Autotools/cm_try_run )

function(cx_cxx_exceptions_rethrow)
  cm_try_run(
    VARIABLE HAVE_RETHROW_WORKING_IN_BAD_EXCEPTION
    SOURCE
      "#include <iostream>"
      "#include <stdexcept>"
      " "
      "//"
      "// Extended bad_exception class with info on exception no handled in the"
      "// throw specifier"
      "//"
      " "
      "class unhandled: public std::bad_exception"
      "{"
      "  public:"
      "    unhandled( const std::string& what )"
      "      : m_what( what )"
      "    {"
      "    }"
      "  "
      "    ~unhandled() throw() {}"
      "  "
      "    virtual const char*"
      "    what() const throw()"
      "    {"
      "      return m_what.c_str()[<sc>]"
      "    }"
      "  "
      "  private:"
      "    std::string m_what[<sc>]"
      "}[<sc>]"
      " "
      "  //"
      "  // New handler for unexpected exceptions"
      "  //"
      " "
      "  void"
      "  handle_unexpected( void )"
      "  {"
      "    try {"
      "      throw[<sc>]"
      "    }"
      "    catch( const std::logic_error& e )"
      "    {"
      "	exit( 0 )[<sc>]"
      "    }"
      "  }"
      " "
      "  //"
      "  // Problem do to logic_error not being in throw specification"
      "  //"
      " "
      "  void"
      "  c3( void ) throw ( std::bad_exception )"
      "  {"
      "    throw std::logic_error(\"c3\")[<sc>]"
      "  }"
      " "
      "  int"
      "  main()"
      "  {"
      "    std::set_unexpected( handle_unexpected )[<sc>]"
      "  "
      "    try {"
      "      c3()[<sc>]"
      "    }"
      "    catch ( const std::logic_error& e )"
      "    {"
      "      exit (1)[<sc>]"
      "    }"
      "    catch ( const std::bad_exception& e )"
      "    {"
      "      exit (0)[<sc>]"
      "    }"
      "    return( 1 )[<sc>]"
      "  }"
      )
  cm_define(
    VARIABLE HAVE_RETHROW_WORKING_IN_BAD_EXCEPTION
    DESCRIPTION "Defined if retrow works in bad_exception" )
  ci_result( HAVE_RETHROW_WORKING_IN_BAD_EXCEPTION result )
  message( STATUS "Checking if compiler supports rethrowing of exceptions ... ${result}" )
endfunction(cx_cxx_exceptions_rethrow)
