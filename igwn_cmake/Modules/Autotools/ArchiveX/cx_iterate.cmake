#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

function( cx_iterate_replace_ )
  if ( ${ARGC} GREATER 2 )
    set( match_string ${ARGV0} )
    set( replace_string ${ARGV1} )
    set( variable ${ARGV2} )
    list(REMOVE_AT ARGN 0 1 2)

    string( REPLACE ${match_string} ${replace_string} ${variable} "${ARGN}" )
    set( ${variable} ${${variable}} PARENT_SCOPE )
  endif( )
endfunction( )

function(cx_iterate)
  set( cmd ${ARGV0})
  list(REMOVE_AT ARGN 0)
  if ( "${cmd}" STREQUAL "REPLACE" )
    cx_iterate_replace_( ${ARGN} )
    set( ${ARGV3} ${${ARGV3}} PARENT_SCOPE )
  endif( )
endfunction( )
