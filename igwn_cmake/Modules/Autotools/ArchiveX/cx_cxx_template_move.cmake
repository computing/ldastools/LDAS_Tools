# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_cxx_template_move
#     Check if C++ supports move template (2011 standard)
#------------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/cm_try_compile )

function(CX_CXX_TEMPLATE_MOVE)
  cm_try_compile(
    "
   #include<algorithm>
   #include<string>
   #include<utility>
   #include<vector>
"
"
   std::vector<std::string> foo;
   foo.push_back( \"air\" );
   foo.push_back( \"water\" );
   foo.push_back( \"fire\" );
   foo.push_back( \"earth\" );
   std::vector<std::string> bar(4);
   foo = std::move(bar);
"
    HAVE_TEMPLATE_MOVE )
  if ( NOT HAVE_TEMPLATE_MOVE )
    set ( HAVE_TEMPLATE_MOVE 0 )
  endif ( NOT HAVE_TEMPLATE_MOVE )
  cm_define(
    VARIABLE HAVE_TEMPLATE_MOVE
    DESCRIPTION "Defined if the move template is available" )
  if ( ${HAVE_TEMPLATE_MOVE} EQUAL 0 )
    set( results "no" )
  else ( ${HAVE_TEMPLATE_MOVE} EQUAL 0 )
    set( results "yes" )
  endif ( ${HAVE_TEMPLATE_MOVE} EQUAL 0 )
  message( STATUS "Checking if the C++ compiler supports move template ... ${results}")
endfunction(CX_CXX_TEMPLATE_MOVE)
