# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_LDAS_CXX_TEMPLATE_ALIASES
#     Check if C++ supports template aliasing (2011 standard)
# ---------------------------------------------------------------------
include( Autotools/cm_define )

function( CX_CXX_TEMPLATE_ALIASES )
  list(FIND "${CMAKE_CXX_COMPILE_FEATURES}" "cxx_alias_templates" FOUND)
  if(NOT ${FOUND} EQUAL -1)
    set(HAVE_CXX_TEMPLATE_ALIASES 1)
    set(msg_result "yes")
  else(NOT ${FOUND} EQUAL -1)
    set(HAVE_CXX_TEMPLATE_ALIASES 0)
    set(msg_result "no")
  endif(NOT ${FOUND} EQUAL -1)
  cm_define(
    VARIABLE HAVE_CXX_TEMPLATE_ALIASES
    DESCRIPTION "Defined if C++ supports template aliasing" )
  message( STATUS "Checking if the C++ compiler supports template aliasing ... ${msg_result}")
endfunction( CX_CXX_TEMPLATE_ALIASES )
