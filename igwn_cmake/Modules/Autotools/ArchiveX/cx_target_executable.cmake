#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( GNUInstallDirs )
include( GNUPkgInstallDirs )

function( cx_target_executable target )
  set(options
    NOINST
    )
  set(oneValueArgs
    INSTALL_DIR
    OUTPUT_NAME
    )
  set(multiValueArgs
    DEFINES
    INCLUDE_DIRECTORIES
    LDADD
    SOURCES
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  add_executable( ${target} ${ARG_SOURCES} )
  if ( ARG_OUTPUT_NAME )
    set_target_properties( ${target}
      PROPERTIES OUTPUT_NAME ${ARG_OUTPUT_NAME}
      )
  endif( )
  if ( ARG_DEFINES)
    set_target_properties( ${target}
      PROPERTIES COMPILE_DEFINITIONS "${ARG_DEFINES}"
      )
  endif( )
  if ( ARG_INCLUDE_DIRECTORIES )
    target_include_directories( ${target}
      ${ARG_INCLUDE_DIRECTORIES} )
  endif( )
  if ( NOT "x${ARG_LDADD}" STREQUAL "x" )
    target_link_libraries( ${target} ${ARG_LDADD} )
  endif ( )
  if ( NOT ARG_NOINST )
    if ( "x${ARG_INSTALL_DIR}" STREQUAL "x" )
      set( ARG_INSTALL_DIR ${CMAKE_INSTALL_BINDIR} )
    endif( )
    install( TARGETS ${target}
      RUNTIME
        DESTINATION ${ARG_INSTALL_DIR} )
  endif( )

endfunction( )
