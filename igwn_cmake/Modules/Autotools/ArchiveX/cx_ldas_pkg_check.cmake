# -*-mode: cmake; indent-tabs-mode: nil; -*-
# ======================================================================
# CM_LDAS_PKG_CHECK
#   Check for LDAS packages
#======================================================================

find_package(PkgConfig)

include( CMakeParseArguments )

function(cx_ldas_pkg_check PREFIX)

  set(options)
  set(oneValueArgs)
  set(multiValueArgs MODULES)
  
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  if ( ARG_MODULES )
    pkg_check_modules( ${PREFIX} REQUIRED ${ARG_MODULES} )
#	PKG_CHECK_MODULES(AS_TR_CPP($1),[$1],[
#			  dnl INCLUDES
#		    	  AS_TR_CPP($1_INCLUDES)="`$PKG_CONFIG --cflags-only-I $1`"
#		    	  cm_subst(AS_TR_CPP($1_INCLUDES))
#		    	  dnl LIBDIRS
#		    	  AS_TR_CPP($1_LIBDIRS)="`$PKG_CONFIG --variable=LIBDIRS $1`"
#		    	  cm_subst(AS_TR_CPP($1_LIBDIRS))
#		    	  dnl PYTHONPATH
#		    	  AS_TR_CPP($1_PYTHONPATH)="`$PKG_CONFIG --variable=PYTHONPATH $1`"
#		    	  cm_subst(AS_TR_CPP($1_PYTHONPATH))
#		    	  dnl SWIGFLAGS
#		    	  AS_TR_CPP($1_SWIGFLAGS)="`$PKG_CONFIG --variable=SWIGFLAGS $1`"
#		    	  cm_subst(AS_TR_CPP($1_SWIGFLAGS))
#  			  ])
  endif ( ARG_MODULES )
endfunction(cx_ldas_pkg_check)
