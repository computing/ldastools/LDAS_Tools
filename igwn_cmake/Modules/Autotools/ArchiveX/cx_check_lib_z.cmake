# ---------------------------------------------------------------------
#  Check for Support of AX_CHECK_LIB_Z
# ---------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/cm_check_lib )

function( CX_CHECK_LIB_Z )
  cm_check_lib( z compress )
  if ( HAVE_LIBZ )
    find_library( LIBZ z )
    check_library_exists( "z" "compress2" "" HAVE_LIBZ_COMPRESS2 )
  endif ( HAVE_LIBZ )
  cm_define(
    VARIABLE HAVE_LIBZ_COMPRESS2
    DESCRIPTION "Defined if libz has compress2 funtion" )
endfunction( CX_CHECK_LIB_Z )
