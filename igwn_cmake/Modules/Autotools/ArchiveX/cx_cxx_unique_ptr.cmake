# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_CXX_TEMPLATE_ALIASES_VIA_USING
#     Check if C++ supports unique_ptr
# ---------------------------------------------------------------------
include( Autotools/cm_try_compile )

function(CX_CXX_UNIQUE_PTR)
  set(inc "
#if HAVE_MEMORY
#include <memory>
using namespace std;
#elif HAVE_TR1_MEMORY
#include <tr1/memory>
using namespace std::tr1;
#endif
")

  set(body "
unique_ptr<int> m;
m.reset( new int );
")

  cm_try_compile("${inc}" "${body}" HAVE_UNIQUE_PTR
    DEFINES -DHAVE_MEMORY=${HAVE_MEMORY}
    -DHAVE_TR1_MEMORY=${HAVE_TR1_MEMORY})
  if(HAVE_UNIQUE_PTR)
    set(result_var "yes")
  else(HAVE_UNIQUE_PTR)
    set(result_var "no")
  endif(HAVE_UNIQUE_PTR)
  cm_define(
    VARIABLE HAVE_UNIQUE_PTR
    DESCRIPTION "Defined if the unique_ptr template is available" )
  message( STATUS "Checking if the C++ compiler supports unique_ptr ... ${result_var}")
endfunction(CX_CXX_UNIQUE_PTR)
