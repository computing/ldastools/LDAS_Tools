#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_target_abi_check( )
#
#------------------------------------------------------------------------
include( Autotools/cm_msg_error )
include( Autotools/ArchiveX/cx_msg_debug_variable )
include( CMakeParseArguments )

function(xx_dump_list FILENAME)
  message(STATUS "FILENAME: ${FILENAME}" )
  string(REPLACE ";" "\n" contents "${ARGN}")
  file(WRITE ${FILENAME} ${contents})
endfunction( )

function(xx_target_abi_check_command_line_variable_option VARIABLE OPTION)
  if ( DEFINED ${VARIABLE} )
    list(LENGTH ARGN num_extra_args)
    if (${num_extra_args} GREATER 0)
      list(GET ARGN 0 VARIABLE_NAME)
    else( )
      set(VARIABLE_NAME ${VARIABLE})
    endif( )
    string(REPLACE ";" "\\;" ${VARIABLE_NAME} "${${VARIABLE}}")
    set(${VARIABLE_NAME} ${OPTION} "\"${${VARIABLE_NAME}}\"" PARENT_SCOPE)
  endif( )
endfunction( )

function(xx_target_abi_check_command_line_variable VARIABLE)
  if ( DEFINED ${VARIABLE} )
    list(LENGTH ARGN num_extra_args)
    if (${num_extra_args} GREATER 0)
      list(GET ARGN 0 VARIABLE_NAME)
    else( )
      set(VARIABLE_NAME ${VARIABLE})
    endif( )
    string(REPLACE ";" "\\;" ${VARIABLE_NAME} "${${VARIABLE}}")
    set(${VARIABLE_NAME} "-D${VARIABLE_NAME}=${${VARIABLE_NAME}}" PARENT_SCOPE)
  endif( )
endfunction( )

function(cx_target_abi_check)
  find_program( ABI_DUMPER_PROGRAM
    NAMES abi-dumper
    )
  find_program( ABI_COMPLIANCE_CHECKER_PROGRAM
    NAMES abi-compliance-checker
    )

  if ( NOT CMAKE_SCRIPT_MODE_FILE
      AND NOT TARGET abi-check )
    add_custom_target( abi-check )
  endif( )

  set(options
    )
  set(oneValueArgs
    HEADER_DIR
    LIBRARY
    LOCAL_INCLUDE_DIR
    MESSAGE
    )
  set(multiValueArgs
    HEADERS
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  get_target_property(library_name ${ARG_LIBRARY} LIBRARY_OUTPUT_NAME)
  if ( True )
    find_package(Python3 COMPONENTS Interpreter)

    xx_target_abi_check_command_line_variable_option(
      ARG_LIBRARY
      "--library-name"
      LIBRARY)
    xx_target_abi_check_command_line_variable_option(
      ARG_HEADER_DIR
      "--header-dir-old"
      HEADER_DIR)
    xx_target_abi_check_command_line_variable_option(
      ARG_LOCAL_INCLUDE_DIR
      "--header-dir-new"
      INCLUDE_NEW_DIR)
    xx_target_abi_check_command_line_variable_option(ARG_MESSAGE "--message" MESSAGE)
    xx_target_abi_check_command_line_variable_option(ARG_HEADERS "--headers" HEADERS)
    #
    # Use script to analyse ABI/API compatibility
    #
    add_custom_target( abi_check_${ARG_LIBRARY}
      COMMAND # ${CMAKE_COMMAND} -E echo
      ${Python3_EXECUTABLE}
      ${LDASTOOLS_CMAKE_SCRIPTS_DIR}/pyAbiApiChecker
      ${DEBUG}
      ${LIBRARY}
      ${HEADER_DIR}
      ${INCLUDE_NEW_DIR}
      ${HEADERS}
      ${MESSAGE}
      )
  else( )
    #
    # Create command line parameters
    #
    xx_target_abi_check_command_line_variable(ARG_HEADER_DIR HEADER_DIR)
    xx_target_abi_check_command_line_variable(ARG_LIBRARY LIBRARY)
    xx_target_abi_check_command_line_variable(ARG_LOCAL_INCLUDE_DIR INCLUDE_NEW_DIR)
    xx_target_abi_check_command_line_variable(ARG_MESSAGE MESSAGE)
    xx_target_abi_check_command_line_variable(ARG_HEADERS HEADERS)
    xx_dump_list( "${CMAKE_CURRENT_BINARY_DIR}/${ARG_LIBRARY}_hdrs.txt" ${ARG_HEADERS})
    if ( CM_MSG_DEBUG_VERBOSE )
      set(DEBUG "-DDEBUG=40")
    endif( )
    set(PY_ABI_API_ANALYSER_SCRIPT "-DPY_ABI_API_ANALYSER_SCRIPT=${LDASTOOLS_CMAKE_SCRIPTS_DIR}/pyAbiApiAnalyser")
    #
    # Use script to analyse ABI/API compatibility
    #
    add_custom_target( abi_check_${ARG_LIBRARY}
      COMMAND ${CMAKE_COMMAND} -E echo
      ${CMAKE_COMMAND}
      -DLOG_FORMAT=CMAKE
      ${DEBUG}
      ${HEADER_DIR}
      ${HEADERS}
      ${INCLUDE_NEW_DIR}
      ${LIBRARY}
      ${MESSAGE}
      ${PY_ABI_API_ANALYSER_SCRIPT}
      -P ${LDASTOOLS_CMAKE_SCRIPTS_DIR}/check_api_abi.cmake
      )
  endif( )
  if (TARGET abi-check)
    add_dependencies( abi-check abi_check_${ARG_LIBRARY} )
  endif( )
endfunction( )
