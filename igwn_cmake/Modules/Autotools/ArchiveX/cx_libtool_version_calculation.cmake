#========================================================================
# CX_LDAS_LIBTOOL_LIBTOOL_VERSION_CALCULATION
#
# library_name = 
# current      = 
# revision     = 
# age          =
#------------------------------------------------------------------------
function( CX_LIBTOOL_VERSION_CALCULATION library_name current revision age )
  math( EXPR ${library_name}_VERSION_MAJOR "${current} - ${age}" )
  set( ${library_name}_VERSION_MAJOR ${${library_name}_VERSION_MAJOR}
    PARENT_SCOPE )
  set( ${library_name}_VERSION_MINOR ${age} PARENT_SCOPE )
  set( ${library_name}_VERSION_MICRO ${revision} PARENT_SCOPE )
  if ( APPLE )
    set( ${library_name}_VERSION
      ${${library_name}_VERSION_MAJOR}
      PARENT_SCOPE )
  else( )
    set( ${library_name}_VERSION
      ${${library_name}_VERSION_MAJOR}.${age}.${revision}
      PARENT_SCOPE )
  endif( )
  set( ${library_name}_SOVERSION
    ${${library_name}_VERSION_MAJOR}
    PARENT_SCOPE )
endfunction( CX_LIBTOOL_VERSION_CALCULATION )
