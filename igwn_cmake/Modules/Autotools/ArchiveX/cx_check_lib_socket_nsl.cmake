#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include(cm_search_libs)

cm_search_libs(
  FUNCTION gethostbyname
  SEARCH_LIBS nsl
  VARIABLE NSL_LIB )
cm_search_libs(
  FUNCTION socket
  SEARCH_LIBS socket
  )
cm_search_libs(
  FUNCTION socket
  SEARCH_LIBS socket
  EXTRA_LIBS ${NSL_LIB}
  )
message( STATUS "Checking for socket and nsl libries ... ${LIBS}" )