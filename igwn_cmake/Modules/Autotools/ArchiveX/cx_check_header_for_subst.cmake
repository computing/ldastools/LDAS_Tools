include( Autotools/cm_check_headers )

function(CX_CHECK_HEADER_FOR_SUBST _FILE _VAR)
  cm_check_headers( ${_FILE} )
  _cm_check_header_variable_name( ${_FILE}  _file_var )
  # cx_msg_debug( "cx_check_header_for_subst: _file_var: ${_file_var} - ${${_file_var}}" )
  if( ${_file_var} )
     set(${_VAR} 1)
  else( ${_file_var} )
     set(${_VAR} 0)
  endif( ${_file_var} )
  set( ${_VAR} ${${_VAR}}
    CACHE INTERNAL "Internal" FORCE )
  # cx_msg_debug( "cx_check_header_for_subst: _VAR: ${_VAR} - ${${_VAR}}" )
endfunction(CX_CHECK_HEADER_FOR_SUBST)
