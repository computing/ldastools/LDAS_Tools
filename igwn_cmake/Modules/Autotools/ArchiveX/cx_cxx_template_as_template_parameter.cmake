# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_LDAS_CXX_TEMPLATE_AS_TEMPLATE_PARAMETER
#     Check if C++ supports templates as parameters to templates
# ---------------------------------------------------------------------
include( Autotools/cm_define )
function( CX_CXX_TEMPLATE_AS_TEMPLATE_PARAMETER )
  list(FIND "${CMAKE_CXX_COMPILE_FEATURES}" "cxx_template_template_parameters" FOUND)
  if(NOT ${FOUND} EQUAL -1)
    set(SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS 1)
    set(msg_result "yes")
  else(NOT ${FOUND} EQUAL -1)
    set(SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS 0)
    set(msg_result "no")
  endif(NOT ${FOUND} EQUAL -1)
  
  cm_define(
    VARIABLE SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS
    DESCRIPTION "Defined if C++ supports templates as template parameters" )
  message( STATUS "Checking if the C++ compiler supports templates as template parameters ... ${msg_result}")
endfunction( CX_CXX_TEMPLATE_AS_TEMPLATE_PARAMETER )
