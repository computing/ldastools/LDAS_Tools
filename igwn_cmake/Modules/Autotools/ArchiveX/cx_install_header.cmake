#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(cx_install_header)
  set(options)
  set(oneValueArgs
    DESTINATION
    WORKINGDIR)
  set(multiValueArgs FILES )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_WORKINGDIR )
    set(ARG_WORKINGDIR ${CMAKE_CURRENT_SOURCE_DIR})
  endif ( NOT ARG_WORKINGDIR )
  
  set(files_ "")
  foreach(f ${ARG_FILES})
    list(APPEND files_ "${ARG_WORKINGDIR}/${f}")
  endforeach(f)
    
  install(
    FILES ${files_}
    DESTINATION ${ARG_DESTINATION} )
endfunction(cx_install_header)