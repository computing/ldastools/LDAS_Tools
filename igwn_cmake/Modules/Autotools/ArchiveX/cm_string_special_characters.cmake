#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_string_special_characters( OPTION STRING VARIABLE)
#------------------------------------------------------------------------
function(CM_STRING_SPECIAL_CHARACTERS OPTION STRING VARIABLE)
  cx_msg_debug_variable( OPTION )
  if ( ${OPTION} STREQUAL "ESCAPE" )
    cx_msg_debug( "Escaping special characters" )
    string( REGEX REPLACE "[;]" "[<cm_semicolon>]" ${VARIABLE} "${STRING}" )
  else ( ${OPTION} STREQUAL "ESCAPE" )
    string( REGEX REPLACE "[[]<cm_semicolon>[]]" ";" ${VARIABLE} "${STRING}" )
  endif ( ${OPTION} STREQUAL "ESCAPE" )
  set( ${VARIABLE} "${${VARIABLE}}" PARENT_SCOPE )
endfunction(CM_STRING_SPECIAL_CHARACTERS )
