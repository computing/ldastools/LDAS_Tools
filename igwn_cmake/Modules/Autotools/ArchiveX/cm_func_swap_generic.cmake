#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_func_swap_generic( )
#------------------------------------------------------------------------

function(CM_FUNC_SWAP_GENERIC _FUNC _VAR _TYPE)
  cm_try_compile(
    HEADER
      "#include <stdlib.h>"
      "#if HAVE_BYTESWAP_H"
      "#include <byteswap.h>"
      "#else /* HAVE_BYTESWAP_H */"
      "#include <sys/types.h>"
      "#if HAVE_SYS_BYTEORDER_H"
      "#include <sys/byteorder.h>"
      "#endif /* HAVE_SYS_BYTEORDER_H */"
      "#if HAVE_MACHINE_BSWAP_H"
      "#include <machine/bswap.h>"
      "#endif /* HAVE_MACHINE_BSWAP_H */"
      "#endif /* HAVE_BYTESWAP_H */"
      "#if HAVE_LIBKERN_OSBYTEORDER_H"
      "#include <libkern/OSByteOrder.h>"
      "#endif /* HAVE_LIBKERN_OSBYTEORDER_H */"
    BODY
      "#if SIZEOF_SHORT == 2"
      "#define int16 short"
      "#endif"
      "#if SIZEOF_INT == 4"
      "#define int32 int"
      "#endif"
      "#if SIZEOF_LONG == 8"
      "#define int64 long"
      "#elif SIZEOF_LONG_LONG == 8"
      "#define int64 long long"
      "#endif"
      " "
      "unsigned ${_TYPE} buf;"
      "(void)${_FUNC}( buf );"
    )
  
endfunction(CM_FUNC_SWAP_GENERIC)
