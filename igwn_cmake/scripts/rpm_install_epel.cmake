#------------------------------------------------------------------------
# Installs epel repository
#------------------------------------------------------------------------
find_program(prog_yum yum)
find_program(prog_rpm rpm)
if ( prog_rpm )
  execute_process(
    COMMAND ${prog_rpm} --eval "%{rhel}"
    OUTPUT_VARIABLE RHEL_MAJOR_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  if( RHEL_MAJOR_VERSION
      AND NOT ( EXISTS "/etc/yum.repos.d/epel.repo"
        OR EXISTS "/etc/yum.repos.d/lscsoft-epel.repo"
        ) )
    find_program(prog_wget NAMES wget)
    if( NOT prog_wget )
      unset(prog_wget CACHE)
      execute_process(COMMAND ${prog_yum} install -y wget)
      find_program(prog_wget NAMES wget)
    endif( )
    execute_process(
      COMMAND ${prog_wget} http://dl.fedoraproject.org/pub/epel/epel-release-latest-${RHEL_MAJOR_VERSION}.noarch.rpm
      )
    execute_process(
      COMMAND ${prog_rpm} -ivh epel-release-latest-${RHEL_MAJOR_VERSION}.noarch.rpm
      )
  endif( )
endif( )
