#========================================================================
# Extensions - Red Hat
#========================================================================

variables:
  RPM_CMAKE_COMMAND_SCRIPT: ${IGWN_CMAKE_PREFIX}/share/${IGWN_CMAKE_PACKAGE_DIR}/cmake/scripts/cmake-command.cmake
  RPM_CMAKE_COMMAND: "cmake3"
  RPM_INSTALL_EPEL_SCRIPT: ${IGWN_CMAKE_PREFIX}/share/${IGWN_CMAKE_PACKAGE_DIR}/cmake/scripts/rpm_install_epel.cmake
  RPM_CMAKE_OPTIONS_BOOST: -DBOOST_INCLUDEDIR=/usr/include/boost169/\;NO_CMAKE_PATH\;NO_CMAKE_ENVIRONMENT_PATH -DBOOST_LIBRARYDIR=/usr/lib64/boost169/ -DBoost_NO_SYSTEM_PATHS:BOOL=ON -DBoost_DEBUG:BOOL=ON
  RPM_LOCAL_REPO_DIR: rpmbuild/RPMS
  RPM_PYTHON2_BUILD_OPTIONS: -DENABLE_SWIG_PYTHON2=YES -DPYTHON2_EXECUTABLE=/usr/bin/python2
  RPM_PYTHON3_BUILD_OPTIONS: -DENABLE_SWIG_PYTHON3=YES -DPYTHON3_EXECUTABLE=/usr/bin/python3
  RPM_LOCAL_PACKAGE_REPO: ${CI_PROJECT_DIR}/${RPM_LOCAL_REPO_DIR}
  RPM_LOCAL_REPO_BINARY_FILE: /etc/yum.repos.d/local-binary.repo
  RPM_LOCAL_REPO_NOARCH_FILE: /etc/yum.repos.d/local-noarch.repo
  RPM_LOCAL_REPO_BINARY_DIR: ${CI_PROJECT_DIR}/rpmbuild/RPMS/x86_64/
  RPM_LOCAL_REPO_BINARY_TEST: test -d ${CI_PROJECT_DIR}/rpmbuild/RPMS/x86_64
  RPM_LOCAL_REPO_NOARCH_DIR: ${CI_PROJECT_DIR}/rpmbuild/RPMS/noarch/
  RPM_LOCAL_REPO_NOARCH_TEST: test -d ${CI_PROJECT_DIR}/rpmbuild/RPMS/noarch
  RPM_BUILD_REQUIRES_SED: sed -e s/^[Bb]uild[Rr]equires:[[:space:]]*// -e s/[[:space:]]*[>][=][[:space:]]*[[:digit:].][[:digit:].]*//g -e s/[[:space:]]*[>][=][[:space:]]*[@][^@]*[@]//p -e s/,//g

#=========================================================
# Images - Red Hat
#---------------------------------------------------------

.image-rh-reference-stable-ext:
  image: igwn/base:el7

.image-rh-reference-testing-ext:
  image:  igwn/base:el7-testing

#---------------------------------------------------------

.image-el7-ext:
  image: ligo/base:el7

.image-sl7-ext:
  image: ligo/base:sl7

#---------------------------------------------------------

.image-centos-latest-ext:
  image: centos:latest

#---------------------------------------------------------

.image-fedora-rawhide-ext:
  image: fedora:rawhide

.image-fedora-latest-ext:
  image: fedora:latest

.image-fedora29-ext:
  image: fedora:29

.image-fedora28-ext:
  image: fedora:28

#==========================
# Anchors (local)
#--------------------------

.rpm-local-repo-artifacts:
  artifacts:
    expire_in: 12h
    paths:
      - ${RPM_LOCAL_REPO_DIR}

.rpm-install-epel: &rpm-install-epel |-
  yum install -y ${IGWN_CMAKE_PACKAGE_NAME} || true
  yum ${RPM_DISABLE_REPO} install -y cmake
  cmake -P ${RPM_INSTALL_EPEL_SCRIPT}
  yum remove -y cmake
  yum remove -y ${IGWN_CMAKE_PACKAGE_NAME} || true

.rpm-local-repo-create: &rpm-local-repo-create |-
  #RPM_LOCAL_REPO_BINARY_FILE: /etc/yum.repos.d/local-binary.repo
  # cat /etc/yum/pluginconf.d/priorities.conf
  # ls -l /etc/yum.repos.d
  # cat /etc/yum.repos.d/staging.repo
  # Binary
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "[local-binary]" > ${RPM_LOCAL_REPO_BINARY_FILE}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "name=Local Binary repo" >> ${RPM_LOCAL_REPO_BINARY_FILE}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "baseurl=file://${RPM_LOCAL_REPO_BINARY_DIR}" >> ${RPM_LOCAL_REPO_BINARY_FILE}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "enabled=1" >> ${RPM_LOCAL_REPO_BINARY_FILE}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "gpgcheck=0" >> ${RPM_LOCAL_REPO_BINARY_FILE}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "protect=1" >> ${RPM_LOCAL_REPO_BINARY_FILE}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo "priority=1" >> ${RPM_LOCAL_REPO_BINARY_FILE}
  # ${RPM_LOCAL_REPO_BINARY_TEST} && cat ${RPM_LOCAL_REPO_BINARY_FILE}
  # ${RPM_LOCAL_REPO_BINARY_TEST} && ls ${RPM_LOCAL_REPO_BINARY_DIR}
  # No Arch
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "[local-noarch]" > ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "name=Local No Arch repo" >> ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "baseurl=file://${RPM_LOCAL_REPO_NOARCH_DIR}"  >> ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "enabled=1" >> ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "gpgcheck=0" >> ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "protect=1"  >> ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo "priority=1"  >> ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && cat ${RPM_LOCAL_REPO_NOARCH_FILE}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && ls ${RPM_LOCAL_REPO_NOARCH_DIR}
  # Update metadata
  yum clean all
  yum repolist enabled

.rpm-local-repo-update: &rpm-local-repo-update |-
  yum install ${RPM_REPOSITORY_DISABLE} -y createrepo
  yum install ${RPM_REPOSITORY_DISABLE} -y yum-plugin-priorities || true
  ${RPM_LOCAL_REPO_BINARY_TEST} && createrepo --verbose ${RPM_LOCAL_REPO_BINARY_DIR}
  ${RPM_LOCAL_REPO_BINARY_TEST} && echo RPM_LOCAL_REPO_BINARY_DIR && ls -l ${RPM_LOCAL_REPO_BINARY_DIR}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && createrepo --verbose ${RPM_LOCAL_REPO_NOARCH_DIR}
  ${RPM_LOCAL_REPO_NOARCH_TEST} && echo ${RPM_LOCAL_REPO_NOARH_DIR} && ls -l ${RPM_LOCAL_REPO_NOARCH_DIR}

.rpm-build-from-src-rpm: &rpm-build-from-src-rpm |-
  rpmbuild --define "_topdir $CI_PROJECT_DIR/rpmbuild" -ts ${PROJECT_PACKAGE_NAME}-*.tar.gz
  export SOURCE_RPM="`rpmbuild --define \"_topdir $CI_PROJECT_DIR/rpmbuild\" -ts ${PROJECT_PACKAGE_NAME}-*.tar.gz | grep Wrote: | sed -e 's,^[^/]*/,/,g'`"
  rpmbuild --define "_topdir $CI_PROJECT_DIR/rpmbuild" --define "_smp_mflags -j${DOCKER_CPUS}" --rebuild ${SOURCE_RPM}

.rpm-cmake-command: &rpm-cmake-command |-
  yum install -y ${IGWN_CMAKE_PACKAGE_NAME} || true
  yum ${RPM_DISABLE_REPO} install -y cmake
  ls -1 /usr/share || true
  export RPM_CMAKE_COMMAND="`cmake -P ${RPM_CMAKE_COMMAND_SCRIPT}`"
  yum ${RPM_DISABLE_REPO} install -y ${RPM_CMAKE_COMMAND}

.rpm-python3: &rpm-python3 |-
  yum install -y cmake python-rpm-macros python3-rpm-macros
  export LDAS_TOOLS_VERSION_DEFINES="`cmake -DTOP_DIR=${CI_PROJECT_DIR} -P ${LDAS_VERSION_INFO_SCRIPT}`"
  yum install -y `cmake \
    -DINCLUDE_PATTERN='^(python[0-9]*|python[0-9]*-devel)$' \
    -DPACKAGER=rpm \
    -DTOP_DIR=${CI_PROJECT_DIR}/${PROJECT_DIR} \
    -DPYTHON2_VERSION=${PY2} \
    -DPYTHON3_VERSION=${PY3} \
    -DPROJECT_NAME=${PROJECT_PACKAGE_NAME} \
    -DPROJECT_VERSION=99.99 \
    -DPROJECT_DESCRIPTION=description \
    -DPACKAGE_URL=url \
    -DCPACK_RPM_PACKAGE_NAME=${PROJECT_PACKAGE_NAME} \
    -DCPACK_RPM_PACKAGE_VERSION=99.99 \
    -DCPACK_RPM_PACKAGE_RELEASE=.1 \
    -DBOOST_MINIMUM_VERSION=99.99 \
    ${LDAS_TOOLS_VERSION_DEFINES} \
    -P ${DEPENDENCY_SCRIPT}` || true
  export PY3="`rpm --eval %{python3_version}`"
  # echo PY3 ${PY3}

.rpm-install-dependencies-egrep: &rpm-install-dependencies-egrep |-
  yum install ${RPM_REPOSITORY_DISABLE} -y `egrep '^[Bb]uild[Rr]equires:' ${PROJECT_DIR}/config/${PROJECT_PACKAGE_NAME}.spec.in \
    | ${RPM_BUILD_REQUIRES_SED}`
  yum -y install yum-utils
  #repoquery --list igwn-cmake
  repoquery --list ldastools-cmake

.rpm-install-dependencies-script: &rpm-install-dependencies |-
  yum install -y ${IGWN_CMAKE_PACKAGE_NAME} || true
  ls -1 $CI_PROJECT_DIR/${RPM_LOCAL_REPO_DIR}/*/*ldas-tools-*.rpm || true
  ls -1 /usr/share || true
  export LDAS_TOOLS_VERSION_DEFINES="`cmake -DTOP_DIR=${CI_PROJECT_DIR} -P ${LDAS_VERSION_INFO_SCRIPT}`"
  yum install -y `cmake \
    -DINSTALL:BOOL=FALSE \
    -DPACKAGER=rpm \
    -DTOP_DIR=${CI_PROJECT_DIR}/${PROJECT_DIR} \
    -DPYTHON2_VERSION=${PY2} \
    -DPYTHON3_VERSION=${PY3} \
    -DPROJECT_NAME=${PROJECT_PACKAGE_NAME} \
    -DPROJECT_VERSION=99.99 \
    -DPROJECT_DESCRIPTION=description \
    -DPACKAGE_URL=url \
    -DCPACK_RPM_PACKAGE_NAME=${PROJECT_PACKAGE_NAME} \
    -DCPACK_RPM_PACKAGE_VERSION=99.99 \
    -DCPACK_RPM_PACKAGE_RELEASE=.1 \
    -DBOOST_MINIMUM_VERSION=99.99 \
    ${LDAS_TOOLS_VERSION_DEFINES} \
    -P ${DEPENDENCY_SCRIPT}`

.cmake-levelN-rpm: &cmake-levelN-rpm
  script:
    #--------------------------------------------------------------------
    - *rpm-local-repo-update
    - *rpm-local-repo-create
    - *rpm-install-epel
    - *rpm-python3
    - *rpm-cmake-command
    - *rpm-install-dependencies
    - if test ${PROJECT_DIR} = '.';
      then
        mkdir build;
        cd build;
      else
        cd ${PROJECT_DIR};
        mkdir ../${PROJECT_DIR}-build;
        cd ../${PROJECT_DIR}-build;
      fi
    - echo "INFO RPM_CMAKE_COMMAND - ${RPM_CMAKE_COMMAND}"
    - ${RPM_CMAKE_COMMAND} -Wno-dev
      ${RPM_PYTHON2_BUILD_OPTIONS}
      ${RPM_PYTHON3_BUILD_OPTIONS}
      ${RPM_CMAKE_OPTIONS_BOOST}
      ${RPM_CMAKE_OPTIONS_OTHER}
      -DCM_MSG_DEBUG_VERBOSE=${CM_MSG_DEBUG_VERBOSE}
      -DCMAKE_INSTALL_PREFIX=/usr
      ../${PROJECT_DIR}
    - ${RPM_CMAKE_COMMAND} --build . --target dist -- ${CMAKE_BUILD_OPTIONS}
    - tar -xOf ${PROJECT_PACKAGE_NAME}-*.tar.gz --wildcards "*.spec"
    - *rpm-build-from-src-rpm
    - pkgs="`ls $CI_PROJECT_DIR/${RPM_LOCAL_REPO_DIR}/*/${PROJECT_PACKAGE_NAME}-*.rpm $CI_PROJECT_DIR/${RPM_LOCAL_REPO_DIR}/*/*-${PROJECT_PACKAGE_NAME}-*.rpm 2>/dev/null`" || true  ; yum --nogpgcheck localinstall -y ${pkgs}
  extends: .rpm-artifacts-pkg-ext

#------------------------------------------------------------------------
#==========================
# Extensions
#--------------------------
.rpm-build-all:
  stage: level1
  script:
    #--------------------------------------------------------------------
    - yum ${RPM_DISABLE_REPO} install -y rpm rpm-build
    - *rpm-install-epel
    - *rpm-python3
    #--------------------------------------------------------------------
    - yum ${RPM_DISABLE_REPO} install -y make
    - *rpm-cmake-command
    #--------------------------------------------------------------------
    - mkdir .build_all_obj
    - cd .build_all_obj
    - ${RPM_CMAKE_COMMAND}
      ${RPM_PYTHON2_BUILD_OPTIONS}
      ${RPM_PYTHON3_BUILD_OPTIONS}
      ${RPM_CMAKE_OPTIONS_BOOST}
      -DTEST_PACKAGER=rpm
      -DMAKE_PARALLEL=${PARALLEL}
      ${CI_PROJECT_DIR}
    - ${RPM_CMAKE_COMMAND} --build . --target rpm -- ${MANUAL_VERBOSE}
  only:
    - manual

.rpm_build_cmake_package:
  extends:
    - .cmake-levelN-rpm

.rpm-documentation:
  script:
    #----------------------------------------------------------------
    - *rpm-local-repo-update
    - *rpm-local-repo-create
    - *rpm-install-epel
    - *rpm-python3
    - *rpm-cmake-command
    - *rpm-install-dependencies
    - yum ${RPM_DISABLE_REPO} install -y boost169-devel graphviz doxygen
    - yum ${RPM_DISABLE_REPO} install -y gtest gtest-devel
    - yum ${RPM_DISABLE_REPO} install -y openssl-devel
    - yum ${RPM_DISABLE_REPO} install -y zlib-devel libzstd-devel
    #----------------------------------------------------------------
    - mkdir docs .build_documentation
    - cd .build_documentation
    - ${RPM_CMAKE_COMMAND}
      -DIGWN_CMAKE_MODULE_DIRS="${IGWN_CMAKE_MODULES_DIRS}"
      -DENABLE_DOCUMENTATION_ONLY=TRUE
      ${PROJECT_DOCUMENTATION_CONFIG_OPTIONS}
      ${RPM_CMAKE_OPTIONS_BOOST}
      ${CI_PROJECT_DIR}
    - ${RPM_CMAKE_COMMAND} --build .
    - ${RPM_CMAKE_COMMAND} --build . --target install-doc
    #----------------------------------------------------------------
    - if test ${PROJECT_HAS_USER_DOCS} = "yes";
      then
        for docdir in test_install/share/doc/*/User;
        do
          pkgname=$(basename $(dirname "${docdir}"));
          mv ${docdir} ../docs/${pkgname};
        done
      fi
    #----------------------------------------------------------------
    - if test ${PROJECT_HAS_ADMIN_DOCS} = "yes";
      then
        for docdir in test_install/share/doc/*/Administrator;
        do
          pkgname=$(basename $(dirname "${docdir}"));
          mkdir -p ../docs/${pkgname} || true;
          mv ${docdir} ../docs/${pkgname}/admin;
        done
      fi
    #----------------------------------------------------------------
    - if test ${PROJECT_HAS_DEVELOPER_DOCS} = "yes";
      then
        for docdir in test_install/share/doc/*/Developer;
        do
          pkgname=$(basename $(dirname "${docdir}"));
          mkdir -p ../docs/${pkgname} || true;
          mv ${docdir} ../docs/${pkgname}/dev;
        done
      fi
  artifacts:
    paths:
    - docs


.rpm-debug-sub:
  script:
    #--------------------------------------------------------------------
    - yum install -y rpm rpm-build
    - *rpm-install-epel
    - *rpm-python3
    #--------------------------------------------------------------------
    - yum install -y make
    - *rpm-cmake-command
    #--------------------------------------------------------------------
    - ${RPM_CMAKE_COMMAND}
      -DEXCLUDE_PATTERN='(ldas-tools|cmake3)'
      -DPACKAGER=rpm
      -DTOP_DIR=${CI_PROJECT_DIR}/${PROJECT_DIR}
      -DPYTHON2_VERSION=27
      -DPYTHON3_VERSION=33
      -DPROJECT_NAME=ldas-tools-framecpp-swig
      -DPROJECT_VERSION=1.1.1
      -DCPACK_RPM_PACKAGE_RELEASE=1
      -DPROJECT_DESCRIPTION="Description"
      -DCPACK_SOURCE_PACKAGE_FILE_EXTENSION=".tar.gz"
      -DPACKAGE_URL="url"
      -DLDAS_TOOLS_AL_VERSION=2.6.100
      -DLDAS_TOOLS_AL_SWIG_VERSION=2.6.100
      -DLDAS_TOOLS_CMAKE_VERSION=2.6.100
      -DLDAS_TOOLS_DISKCACHEAPI_VERSION=2.6.100
      -DLDAS_TOOLS_DISKCACHEAPI_SWIG_VERSION=2.6.100
      -DLDAS_TOOLS_FILTERS_VERSION=2.6.100
      -DLDAS_TOOLS_FRAMEAPI_VERSION=2.6.100
      -DLDAS_TOOLS_FRAMEAPI_SWIG_VERSION=2.6.100
      -DLDAS_TOOLS_FRAMECPP_VERSION=2.6.100
      -DLDAS_TOOLS_FRAMECPP_SWIG_VERSION=2.6.100
      -DLDAS_TOOLS_LDASGEN_VERSION=2.6.100
      -DLDAS_TOOLS_LDASGEN_SWIG_VERSION=2.6.100
      -DLDAS_TOOLS_SUITE_VERSION=2.6.100
      -DLDAS_TOOLS_UTILIITES_VERSION=2.6.100
      -DLDAS_TOOLS_SWIG_VERSION=3.6
      -P ${DEPENDENCY_SCRIPT}
  only:
    - manual

.rpm-sanitizer-addr:
  stage: level1
  script:
    #--------------------------------------------------------------------
    - *rpm-install-epel
    - *rpm-python3
    #--------------------------------------------------------------------
    - yum ${RPM_DISABLE_REPO} install -y make
    - yum ${RPM_DISABLE_REPO} install -y libasan
    - *rpm-cmake-command
    - *rpm-install-dependencies
    #--------------------------------------------------------------------
    - ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer; export ASAN_SYMBOLIZER_PATH
    - ASAN_OPTIONS=symbolize=1:external_symbolizer_path=/usr/bin/llvm-symbolizer:verbosity=2:help=1:alloc_dealloc_mismatch=0; export ASAN_OPTIONS
    - mkdir .build_all_obj
    - cd .build_all_obj
    - ${RPM_CMAKE_COMMAND} -DCMAKE_BUILD_TYPE=Debug -DSANITIZER=address -DMAKE_PARALLEL=${PARALLEL} -DPYTHON2_VERSION=${PY2} -DPYTHON3_VERSION=${PY3} ${CI_PROJECT_DIR}
    - ${RPM_CMAKE_COMMAND} --build . -- ${MANUAL_VERBOSE}
  only:
    - manual

.rpm-artifacts-pkg-ext:
  artifacts:
    expire_in: 12h
    paths:
      - ${RPM_LOCAL_REPO_DIR}

.rpm-deploy-suite:
  script:
    - mkdir ${DEPLOY_DIR}
    - mv ${RPM_LOCAL_REPO_DIR}/* ${DEPLOY_DIR}
  artifacts:
    paths:
      - ${DEPLOY_DIR}

.rpm-localize-repo:
  extends:
    - .rpm-local-repo-artifacts
  script:
    - yum install -y wget
    - eval "mkdir -p ${RPM_LOCALIZE_PACKAGE_REPO}"
    - eval "cd ${RPM_LOCALIZE_PACKAGE_REPO} ; pwd"
    - echo "INFO Retrieving - ${RPM_REMOTE_PACKAGE_REPO}"
    - wget ${RPM_REMOTE_PACKAGE_REPO}
    - cd ${CI_PROJECT_DIR}
    - ls -R ${RPM_LOCAL_PACKAGE_REPO}
    - echo "====================================="
    - rpm --eval '%dump' || true
    - echo "====================================="
