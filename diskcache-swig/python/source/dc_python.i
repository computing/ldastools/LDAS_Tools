/*
 * LDASTools diskcache - Tools for querying a collection of files on disk
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools diskcache is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools diskcache is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

// -*-Mode: C++;-*-
#if defined(SWIGPYTHON)
//-----------------------------------------------------------------------
// Convert a Python list or string to a set of directories
//-----------------------------------------------------------------------
%typemap(in) const directory_container_type&
{
  //---------------------------------------------------------------------
  // Verify the input is a list
  //---------------------------------------------------------------------
  if ( PyList_Check($input) )
  {
    $1 = new directory_container_type;

    for ( int i = 0,
	    nitems = PyList_Size( $input );
	  i < nitems;
	  ++i )
    {
      PyObject *o = PyList_GetItem($input,i);

      if ( PyUnicode_Check( o ) )
      {
	$1->push_back( PyUnicode_AsUTF8( o ) );
      }
      else
      {
	PyErr_SetString( PyExc_TypeError, "list must contain strings" );
	delete $1;
	return NULL;
      }
    }
  }
  else if ( PyUnicode_Check( $input ) )
  {
    $1 = new directory_container_type;

    $1->push_back( PyUnicode_AsUTF8( $input ) );
  }
  else
  {
    PyErr_SetString( PyExc_TypeError, "Not a list nor a string" );
    return NULL;
  }
}
%typemap(freearg) const directory_container_type&
{
  delete $1;
}
#endif /* defined(SWIGPYTHON) */
