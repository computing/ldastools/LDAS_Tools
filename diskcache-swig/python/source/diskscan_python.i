/*
 * LDASTools diskcache - Tools for querying a collection of files on disk
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools diskcache is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools diskcache is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

// -*- Mode: C++; c-basic-offset: 2; -*-
#if defined(SWIGPYTHON)

%include "ldas_python.i"
%include "dc_python.i"

//=======================================================================
// Typemap - Python
//=======================================================================

%typemap(out) string, std::string
{
  $result = PyString_FromString(const_cast< char* >( $1.c_str( ) ) );
}
//-----------------------------------------------------------------------
// Exception handling - Python
//-----------------------------------------------------------------------
%exception
{
    try
    {
        $action;
    }
    catch( const LdasException& e )
    {
      std::string	error;

      for ( size_t x = 0,
	      end = e.getSize( );
	    x != end;
	    ++x )
      {
	if ( x )
	{
	  error += "\n";
	}
	error += e.getError( x ).getMessage( );
      }
      PyErr_SetString( PyExc_Exception,
		       const_cast< char* >( error.c_str( ) ) );
      return NULL;
    }      
    catch ( SwigException& e )
    {
      PyErr_SetString( PyExc_Exception,
		       const_cast< char* >( e.getResult( ).c_str( ) ) );
      return NULL;
    }
    catch ( std::runtime_error& e )
    {
      PyErr_SetString( PyExc_Exception,
		       const_cast< char* >( e.what( ) ) );
      return NULL;
    }
    catch ( std::exception& e )
    {
      PyErr_SetString( PyExc_Exception,
		       const_cast< char* >( e.what( ) ) );
      return NULL;
    }
    catch( ... )
    {
      std::string	error("Unknown exception caught: ");
      std::string	info("Unable to retrieve more information about this exception");

      error += info;
      PyErr_SetString(PyExc_Exception,
		      const_cast< char* >( error.c_str( ) ) );
      return NULL;
    }
}
#endif // defined(SWIGPYTHON)
