/*
 * LDASTools diskcache - Tools for querying a collection of files on disk
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools diskcache is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools diskcache is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef DISCKCACHE__SERVER_INTERFACE_I
#define DISCKCACHE__SERVER_INTERFACE_I

%include "std_string.i"

%{
#include <diskcache_config.h>

#include <stdexcept>

#include "ldastoolsal/ldasexception.hh"

#include "genericAPI/swigexception.hh"

#include "diskcacheAPI/ServerInterface.hh"

  using diskCache::ServerInterface;

 typedef ServerInterface::filenames_rds_results_type filenames_rds_results_type;
 typedef ServerInterface::time_type time_type;
 typedef ServerInterface::port_type port_type;

%}

class ServerInterface
{
 public:
  static std::string DEFAULT_EXTENSION;

  void FilenamesRDS( filenames_rds_results_type& Results,
		     const std::string& IFO,
		     const std::string& Desc,
		     time_type StartTime,
		     time_type EndTime,
		     bool Resample,
		     const std::string& Extension );

  void Server( const std::string& Host, port_type Port );
};

#endif /* DISCKCACHE__SERVER_INTERFACE_I */
