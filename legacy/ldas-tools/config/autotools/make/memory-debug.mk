#------------------------------------------------------------------------
# Support data race analysis without recompiling
#------------------------------------------------------------------------

dataracecheck-recursive:

ifneq ($(RECURSIVE_TARGETS),dataracecheck-recursive)
dataracecheck-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=dataracecheck-recursive dataracecheck-recursive
endif

dataracecheck: dataracecheck-recursive dataracecheck-am

dataracecheck-am: all-am

#------------------------------------------------------------------------
# Support memory corruption/usage analysis without recompiling
#------------------------------------------------------------------------

memcheck-recursive:

ifneq ($(RECURSIVE_TARGETS),memcheck-recursive)
memcheck-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=memcheck-recursive memcheck-recursive
endif

memcheck: memcheck-recursive memcheck-am

memcheck-am: all-am

#------------------------------------------------------------------------
# Support thead safety analysis without recompiling
#------------------------------------------------------------------------

mtsafe-recursive:

ifneq ($(RECURSIVE_TARGETS),mtsafe-recursive)
mtsafe-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=mtsafe-recursive mtsafe-recursive
endif

mtsafe: mtsafe-recursive  mtsafe-am

mtsafe-am: all-am

#------------------------------------------------------------------------
# Support profiling analysis without recompiling
#------------------------------------------------------------------------

profile-recursive:

ifneq ($(RECURSIVE_TARGETS),profile-recursive)
profile-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=profile-recursive profile-recursive
endif

profile: profile-recursive profile-am

profile-am: all-am

#========================================================================
# Include files to support targets
#------------------------------------------------------------------------

ifneq ($(VALGRIND),)
  VALGRIND_LD_LIBRARY_PATH=$(MEMORY_DEBUG_LD_LIBRARY_PATH)
  include $(LDAS_DIRECTORY_MAKE_INCLUDE)/valgrind.mk
endif
ifneq ($(LIBUMEM),)
  include $(LDAS_DIRECTORY_MAKE_INCLUDE)/libumem.mk
endif

