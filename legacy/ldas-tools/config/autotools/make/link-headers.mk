#========================================================================
# Target for linking header files so they have the same look as when
#   installed.
#
# HDR_DIR              - Destiination directory
# HDR_SOURCE_DIR_FILES - Files in the source directory to be linked
# HDR_BUILD_DIR_FILES  - Files in the build directory to be linked
#
#========================================================================
LINK_HEADERS_TIME_STAMPS =
LINK_HEADERS_TIME_STAMPS += hdr-time-parallel.stamp
LINK_HEADERS_TIME_STAMPS += hdr-time-stamp

hdr-time-parallel.stamp: Makefile $(HDR_BUILD_DIR_FILES)
	@rm -f hdr-time-parallel.stamp
	@touch hdr-time-parallel.stamp.tmp
	$(AM_V_GEN)
	$(AM_V_at)-rm -f $(top_builddir)/include/$(HDR_DIR)/* 2>/dev/null
	$(AM_V_at)mkdir -p $(top_builddir)/include/$(HDR_DIR)
	$(AM_V_at)SRCDIR=`( cd $(srcdir) ; pwd )`; export SRCDIR; \
	  for hdr in $(HDR_SOURCE_DIR_FILES); \
	  do \
	    ( cd $(top_builddir)/include/$(HDR_DIR); \
	      ln -s $$SRCDIR/$$hdr . ); \
	  done
	$(AM_V_at)CWD=`pwd`; export CWD; \
	  for hdr in $(HDR_BUILD_DIR_FILES); \
	  do \
	    ( cd $(top_builddir)/include/$(HDR_DIR); rm -f $$hdr ; ln -s $$CWD/$$hdr . ); \
	  done
	$(AM_V_at)touch hdr-time-stamp
	@mv hdr-time-parallel.stamp.tmp hdr-time-parallel.stamp

hdr-time-stamp: Makefile $(HDR_BUILD_DIR_FILES)
	@if test -f $@ ; then :; else \
	  trap 'rm -rf hdr-time-stamp.lock hdr-time-parallel.stamp' 1 2 13 15; \
	  if mkdir hdr-time-stamp.lock 2>/dev/null ; then \
	    $(MAKE) $(AM_MAKEFLAGS) hdr-time-parallel.stamp ; \
	    result=$$?; rm -rf hdr-time-stamp.lock ; exit $$result; \
	  else \
	    while test -d hdr-time-stamp.lock; do sleep 1; done; \
	    test -f hdr-time-parallel.stamp ; \
	  fi; \
	fi

CLEAN_LOCAL+=link-headers-clean
link-headers-clean:
	rm -f hdr-time-parallel.stamp
	rm -f hdr-time-stamp
