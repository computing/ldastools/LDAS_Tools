#------------------------------------------------------------------------
# libraries/ldastoolsla (formerly: libraries/general)
#------------------------------------------------------------------------
ldastoolsdir=$(top_builddir)/libraries/ldastoolsal
generaldir=$(ldastoolsdir)
ldastoolsal_la = $(ldastoolsdir)/src/libldastoolsal.la
ldastoolsal_uninstalled_path=$(ldastoolsdir)/src/.libs
ldastoolsal_uninstalled_python_path=$(ldastools_uninstalled_path):$(ldastoolsdir)/src
#------------------------------------------------------------------------
# libraries/filters
#------------------------------------------------------------------------
filtersdir=$(top_builddir)/libraries/filters
filters_la = $(filtersdir)/src/libfilters.la
#------------------------------------------------------------------------
# libraries/framecpp
#------------------------------------------------------------------------
framecppdir=$(top_builddir)/libraries/framecpp
framecpp_la = $(framecppdir)/src/OOInterface/libframecpp.la
libframecppc = $(framecppdir)/src/CInterface/libframecppc.la
framecpp_uninstalled_path = $(framecppdir)/src/OOInterface/.libs:$(framecppdir)/src/Version8/.libs:$(framecppdir)/src/Version7/.libs:$(framecppdir)/src/Version6/.libs:$(framecppdir)/src/Version4/.libs:$(framecppdir)/src/Version3/.libs:$(framecppdir)/src/Common/.libs:$(ldastoolsal_uninstalled_path)
framecpp_uninstalled_python_path = $(framecpp_uninstalled_path):$(framecppdir)/src/OOInterface:$(framecppdir)/src/Version8:$(framecppdir)/src/Version7:$(framecppdir)/src/Version6:$(framecppdir)/src/Version4:$(framecppdir)/src/Version3:$(framecppdir)/src/Common:$(ldastoolsal_uninstalled_python_path)
#------------------------------------------------------------------------
# libraries/ldasgen (formerly: libraries/genericAPI)
#------------------------------------------------------------------------
ldasgendir=$(top_builddir)/libraries/ldasgen
stat_la=$(ldasgendir)/src/libStat.la
ldasgen_la=$(ldasgendir)/src/libldasgen.la
genericcore_la=$(ldasgen_la)
ldasgen_uninstalled_path=$(ldasgen_dir)/src/.libs:$(ldastoolsal_uninstalled_path)
ldasgen_uninstalled_python_path=$(ldasgen_dir)/src/.libs:$(ldastools_dir)/src:$(ldastoolsal_uninstalled_python_path)
#------------------------------------------------------------------------
# libraries/frameutils
#------------------------------------------------------------------------
frameutilsdir=$(top_builddir)/libraries/frameutils
ldasframe_la=$(frameutilsdir)/src/libldasframe.la
frameutils_la=$(ldasframe_la)
frameutildir=$(frameutilsdir)
frameutils_uninstalled_path=$(frameutilsdir)/src/.libs:$(ldasgendir)/src/.libs:$(filtersdir)/src/.libs:$(framecpp_uninstalled_path):$(ldastoolsal_uninstalled_path)
frameutils_uninstalled_python_path=$(frameutils_uninstalled_path):$(frameutilsdir)/src:$(ldasgendir)/src:$(filtersdir)/src:$(framecpp_uninstalled_python_path):$(ldastoolsal_uninstalled_python_path)
#------------------------------------------------------------------------
# libraries/diskcache
#------------------------------------------------------------------------
diskcachedir=$(top_builddir)/libraries/diskcache
diskcache_la=$(diskcachedir)/src/libdiskcache.la

