#------------------------------------------------------------------------
# This file cannot be built in parallel
#------------------------------------------------------------------------
.NOTPARALLEL:

#------------------------------------------------------------------------
# Key variables that need to be known before including this file:
#========================================================================
#
# doxygenhtmldir - Location where to install the documentation
#
# DOXYGEN_DISABLE_LATEX - If 1, then do not generate latex documentation
#  even if latex is available.
#
# By default, the documentation that is generated is for users.
# To generate the developer's version of the documentation,
# each of the DOXYGEN_EXTRAC_x variables should be set to YES.
#
#------------------------------------------------------------------------
HTML_LOCAL ?=
HTML_INSTALL_LOCAL ?=

NAMESPACE ?= $(PACKAGE)
DOXYGEN_CLEAN = doxygen-clean-html
DOXYGEN_CFG=doxygen.cfg

DOXYGEN_EXTRACT_ALL ?= YES
DOXYGEN_EXTRACT_PRIVATE ?= NO
DOXYGEN_EXTRACT_STATIC ?= NO
DOXYGEN_EXTRACT_LOCAL_CLASSES ?= NO
DOXYGEN_EXTRACT_LOCAL_METHODS ?= NO
DOXYGEN_EXTRACT_ANON_NSPACES ?= NO
DOXYGEN_HIDE_UNDOC_MEMBERS ?= YES
DOXYGEN_HIDE_UNDOC_CLASSES ?= YES
DOXYGEN_SORT_BY_SCOPE_NAME ?= YES
DOXYGEN_EXCLUDE_PATTERNS ?=

DOXYGEN_GENERATE_HTML ?= YES

DOXYGEN_GENERATE_LATEX ?= YES
DOXYGEN_GENERATE_TAGFILE ?= $(NAMESPACE).tag
ifeq ($(DOXYGEN_DISABLE_LATEX),1)
DOXYGEN_LATEX=
else
DOXYGEN_LATEX=$(LATEX)
DOXYGEN_CLEAN += doxygen-clean-latex
endif

DOXYGEN_WARN_NO_PARAMDOC ?= YES
DOXYGEN_WARN_IF_UNDOCUMENTED ?= YES

DOXYGEN_FILTER_SOURCE_FILES ?= NO

doxygenhtmldir ?= $(docdir)/$(NAMESPACE)/html

ifneq ($(DOXYGEN),)
ALL_LOCAL+=doc-doxygen
HTML_LOCAL+=doc-doxygen
HTML_INSTALL_LOCAL+=install-doxygen
INSTALL_DATA_LOCAL+=install-doxygen
UNINSTALL_LOCAL+=uninstall-doxygen
CLEAN_LOCAL+=clean-doxygen
DISTCLEAN_LOCAL+=distclean-doxygen
DOXYGEN_FILE_PATTERNS ?= *.tcc *.icc *.cc *.hh *.c *.h *.txt *.hh.in
DOXYGEN_DIRS ?=

DOXYGEN_SRCDIR_SRC ?=
DOXYGEN_STRIP_FROM_PATH ?=
ifneq ($(DOXYGEN_SKIP_SRCDIR),yes)
ifneq ($(wildcard $(srcdir)/../src$(DOXYGEN_SRCDIR_SUBDIR)/*),)
DOXYGEN_DIRS += $(shell find $(srcdir)/../src$(DOXYGEN_SRCDIR_SUBDIR) -type d \! -name CVS -print)
DOXYGEN_SRCDIR_SRC += $(srcdir)/../src$(DOXYGEN_SRCDIR_SUBDIR)
DOXYGEN_STRIP_FROM_PATH += $(srcdir)/../src/
endif
endif
DOXYGEN_CXX_FILES ?=
DOXYGEN_CXX_FILES += $(foreach dir,$(DOXYGEN_DIRS),$(foreach pattern,$(DOXYGEN_FILE_PATTERNS),$(wildcard $(dir)/$(pattern))))
DOXYGEN_EXAMPLE_PATH ?= $(DOXYGEN_SRCDIR_SRC)
DOXYGEN_RECURSIVE ?= YES

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Crate a list of search paths
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ifndef DOXYGEN_INCLUDE_PATH
DOXYGEN_INCLUDE_PATH =
endif
DOXYGEN_INCLUDE_PATH += \
	$(top_builddir)/include

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Create a list of commonly needed macro expansions
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ifndef DOXYGEN_PREDEFINED
DOXYGEN_PREDEFINED =
endif
DOXYGEN_PREDEFINED += \
	__cplusplus=1 \
	DOCUMENTATION_DOXYGEN=1 \
	LDASTOOLS_DEPRICATED(msg)=/** \depricated msg */
ifndef DOXYGEN_EXPAND_AS_DEFINED
DOXYGEN_EXPAND_AS_DEFINED =
endif
DOXYGEN_EXPAND_AS_DEFINED += \
	CREATE_THREADED0_DECL \
	CREATE_THREADED1_DECL \
	CREATE_THREADED2_DECL \
	CREATE_THREADED3_DECL \
	CREATE_THREADED4_DECL \
	CREATE_THREADED5_DECL \
	CREATE_THREADED6_DECL \
	CREATE_THREADED7_DECL \
	CREATE_THREADED8_DECL \
	CREATE_THREADED9_DECL \
	CREATE_THREADED10_DECL \
	CREATE_THREADED0V_DECL \
	CREATE_THREADED1V_DECL \
	CREATE_THREADED2V_DECL \
	CREATE_THREADED3V_DECL \
	CREATE_THREADED4V_DECL \
	CREATE_THREADED5V_DECL \
	CREATE_THREADED6V_DECL \
	CREATE_THREADED7V_DECL \
	CREATE_THREADED8V_DECL \
	CREATE_THREADED9V_DECL \
	CREATE_THREADED10V_DECL \
	CREATE_THREADED0 \
	CREATE_THREADED1 \
	CREATE_THREADED2 \
	CREATE_THREADED3 \
	CREATE_THREADED4 \
	CREATE_THREADED5 \
	CREATE_THREADED6 \
	CREATE_THREADED7 \
	CREATE_THREADED8 \
	CREATE_THREADED9 \
	CREATE_THREADED10 \
	CREATE_THREADED0V \
	CREATE_THREADED1V \
	CREATE_THREADED2V \
	CREATE_THREADED3V \
	CREATE_THREADED4V \
	CREATE_THREADED5V \
	CREATE_THREADED6V \
	CREATE_THREADED7V \
	CREATE_THREADED8V \
	CREATE_THREADED9V \
	CREATE_THREADED10V \
	SINGLETON_DECL \
	SINGLETON_TS_DECL \
	SINGLETON_INST \
	SINGLETON_TS_INST


#------------------------------------------------------------------------
# Anticipate what the next release will be
#------------------------------------------------------------------------
# DOXYGEN_ENV=NEXT_RELEASE=`$(top_srcdir)/config/scripts/next-release $(PACKAGE_VERSION)`
# DOXYGEN_ENV+=NEXT_TAG_RELEASE=`$(top_srcdir)/config/scripts/next-release $(PACKAGE_VERSION) | tr . _`
# DOXYGEN_ENV+=NEXT_TAG_BASE=`$(top_srcdir)/config/scripts/next-release $(PACKAGE_VERSION) | tr . _ | sed -e 's/_[0-9]*$$/_0/' `
# DOXYGEN_ENV+=NEXT_TAG_BRANCH=`$(top_srcdir)/build/scripts/next-release $(PACKAGE_VERSION) | tr . _ | sed -e 's/_[0-9]*$$/_fixes/' `
#------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------
DOXYGEN_TARGETS = doxygen/html/index.html
ifeq ($(DOXYGEN_GENERATE_LATEX),YES)
DOXYGEN_TARGETS += doxygen/latex/index.tex
endif

doc-doxygen: $(DOXYGEN_TARGETS)

$(DOXYGEN_TARGETS): $(DOXYGEN_CXX_FILES) $(DOXYGEN_CFG) 
	$(AM_V_GEN)
	$(AM_V_at)/usr/bin/env $(DOXYGEN_ENV) $(DOXYGEN) $(DOXYGEN_CFG)
ifeq ($(DOXYGEN_GENERATE_LATEX),YES)
	$(AM_V_at)( if test -d doxygen/latex; then cd doxygen/latex; $(MAKE) < /dev/null; fi )
endif

$(DOXYGEN_CFG): \
	Makefile.am \
	$(LDAS_DIRECTORY_MAKE_INCLUDE)/doxygen.mk

#------------------------------------------------------------------------
# Build rules
#------------------------------------------------------------------------
$(DOXYGEN_CFG):
	$(AM_V_GEN)
	$(AM_V_at)-rm -f $(DOXYGEN_CFG);
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Project characteristics' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'PROJECT_NAME=$(PACKAGE_NAME)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'PROJECT_NUMBER=$(PACKAGE_VERSION)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Build related options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXTRACT_ALL=$(DOXYGEN_EXTRACT_ALL)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXTRACT_PRIVATE=$(DOXYGEN_EXTRACT_PRIVATE)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXTRACT_STATIC=$(DOXYGEN_EXTRACT_STATIC)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXTRACT_LOCAL_CLASSES=$(DOXYGEN_EXTRACT_LOCAL_CLASSES)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXTRACT_LOCAL_METHODS=$(DOXYGEN_EXTRACT_LOCAL_METHODS)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXTRACT_ANON_NSPACES=$(DOXYGEN_EXTRACT_ANON_NSPACES)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'HIDE_UNDOC_MEMBERS=$(DOXYGEN_HIDE_UNDOC_MEMBERS)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'HIDE_UNDOC_CLASSES=$(DOXYGEN_HIDE_UNDOC_CLASSES)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'SORT_BY_SCOPE_NAME=$(DOXYGEN_SORT_BY_SCOPE_NAME)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Sources to use' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'INPUT=$(DOXYGEN_AUX_FILES) $(DOXYGEN_SRCDIR_SRC)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'FILE_PATTERNS=$(DOXYGEN_FILE_PATTERNS)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'RECURSIVE=$(DOXYGEN_RECURSIVE)' >> $(DOXYGEN_CFG)
ifneq ($(DOXYGEN_EXCLUDE_PATTERNS),)
	$(AM_V_at)echo 'EXCLUDE_PATTERNS=$(DOXYGEN_EXCLUDE_PATTERNS)' >> $(DOXYGEN_CFG)
endif
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Quiet' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
ifneq ($(V),0)
	$(AM_V_at)echo 'QUIET=YES' >> $(DOXYGEN_CFG)
else
	$(AM_V_at)echo 'QUIET=NO' >> $(DOXYGEN_CFG)
endif
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# How to handle warnings' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'WARNINGS=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'WARN_NO_PARAMDOC=$(DOXYGEN_WARN_NO_PARAMDOC)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'WARN_IF_UNDOCUMENTED=$(DOXYGEN_WARN_IF_UNDOCUMENTED)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'WARN_LOGFILE=doxygen_warnings.log' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Filtering of the source (Converting from Perceps to doxygen)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'FILTER_SOURCE_FILES=$(DOXYGEN_FILTER_SOURCE_FILES)' >> $(DOXYGEN_CFG)
ifneq ($(DOXYGEN_FILTER_SOURCE_FILES),NO)
	$(AM_V_at)echo 'INPUT_FILTER=$(top_srcdir)/config/scripts/doxygen.pl' >> $(DOXYGEN_CFG)
endif
ifneq ($(DOXYGEN_EXCLUDE),)
	$(AM_V_at)echo 'EXCLUDE=$(DOXYGEN_EXCLUDE)' >> $(DOXYGEN_CFG)
endif
ifneq ($(DOXYGEN_EXCLUDE_PATTERNS),)
	$(AM_V_at)echo 'EXCLUDE_PATTERNS=$(DOXYGEN_EXCLUDE_PATTERNS)' >> $(DOXYGEN_CFG)
endif
	$(AM_V_at)echo 'EXAMPLE_PATH=$(DOXYGEN_EXAMPLE_PATH)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'VERBATIM_HEADERS=NO' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Where to output' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'OUTPUT_DIRECTORY=doxygen' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'STRIP_FROM_PATH=$(DOXYGEN_STRIP_FROM_PATH)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Types of output' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# MAN Options
	@#---------------------------------------------------------------
	$(AM_V_at)echo 'GENERATE_MAN=NO' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Source browsing related options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'INLINE_SOURCES=NO' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'INLINE_INHERITED_MEMB=YES' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# Preprocessor related options
	@#---------------------------------------------------------------
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Preprocessor related options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'ENABLE_PREPROCESSING=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'MACRO_EXPANSION=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXPAND_ONLY_PREDEF=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'EXPAND_AS_DEFINED=' >> $(DOXYGEN_CFG)
	$(AM_V_at)for v in $(DOXYGEN_EXPAND_AS_DEFINED); \
	do \
	  echo 'EXPAND_AS_DEFINED+='$$v >> $(DOXYGEN_CFG); \
	done
	$(AM_V_at)echo 'SEARCH_INCLUDES=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'INCLUDE_PATH=$(DOXYGEN_INCLUDE_PATH)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'PREDEFINED=$(DOXYGEN_PREDEFINED)' >>  $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# External Reference Options
	@#---------------------------------------------------------------
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# External reference options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'GENERATE_TAGFILE=$(DOXYGEN_GENERATE_TAGFILE)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'TAGFILES=$(DOXYGEN_TAGFILES)' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# HTML Options
	@#---------------------------------------------------------------
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# HTML Options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	@file="doxygen.cfg"
	$(AM_V_at)echo 'GENERATE_HTML=$(DOXYGEN_GENERATE_HTML)' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# Latex Options
	@#---------------------------------------------------------------
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# Latex Options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
ifeq ($(DOXYGEN_LATEX),)
	$(AM_V_at)echo 'GENERATE_LATEX=NO' >> $(DOXYGEN_CFG)
else
	$(AM_V_at)echo 'GENERATE_LATEX=$(DOXYGEN_GENERATE_LATEX)' >> $(DOXYGEN_CFG)
endif
	@#---------------------------------------------------------------
	@# DOT Options
	@#---------------------------------------------------------------
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '# DOT Options' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
ifeq ($(DOT),)
	$(AM_V_at)echo 'HAVE_DOT=NO' >> $(DOXYGEN_CFG)
else
	$(AM_V_at)echo 'HAVE_DOT=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'DOT_PATH=$(DOT_PATH)' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'DOT_IMAGE_FORMAT=png' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'CLASS_GRAPH=YES' >> $(DOXYGEN_CFG)
	$(AM_V_at)echo 'GRAPHICAL_HIERARCHY=YES' >> $(DOXYGEN_CFG)
endif
endif
#------------------------------------------------------------------------
# Installation rules
#------------------------------------------------------------------------
install-doxygen: doc-doxygen
	if test -d doxygen/html; \
	then \
	  mkdir -p $(DESTDIR)$(doxygenhtmldir); \
	  for file in doxygen/html/*; \
	  do \
	    $(INSTALL) $$file $(DESTDIR)$(doxygenhtmldir); \
	  done \
	fi

uninstall-doxygen:
	if test -d $(DESTDIR)$(doxygenhtmldir); \
	then \
	    rm -rf $(DESTDIR)$(doxygenhtmldir); \
	fi
doxygen-debug:
	@echo DOXYGEN_AUX_FILES: $(DOXYGEN_AUX_FILES)
	@echo DOXYGEN_TARGETS: $(DOXYGEN_TARGETS)
	@echo DOXYGEN_CXX_FILES: $(DOXYGEN_CXX_FILES)


clean-doxygen: $(DOXYGEN_CLEAN)
	rm -rf doxygen_warnings.log doxygen.cfg *.tag

distclean-doxygen: $(DOXYGEN_CLEAN)
	rm -rf doxygen

doxygen-clean-html:
	rm -rf doxygen/html

doxygen-clean-latex:
	rm -rf doxygen/latex
