dnl ---------------------------------------------------------------------
dnl  Check for existence of libbz2 library
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_LIB_BZ2],
[ have_libbz2='yes'
  AC_CHECK_LIB(bz2,BZ2_bzBuffToBuffCompress,
  [ AC_DEFINE( HAVE_NEW_LIBZ2,1,[Defined if libz2 has new functions] ) ],
  [ AC_CHECK_LIB(bz2, bzBuffToBuffCompress,,
    [ if test -n "$1" && test "$1" = "no"
      then
        AC_MSG_WARN( Library libbz2 not found )
	have_libbz2=''
      else
        AC_MSG_ERROR( Required library libbz2 not found )
      fi
    ])
  ])
  AM_CONDITIONAL(HAVE_LIBBZ2, test x$have_libbz2 != x)
])

dnl ---------------------------------------------------------------------
dnl  Check for existence of frameL library
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_LIB_FRAMEL],[
  AC_LANG_SAVE
  AC_LANG_C
  ldas_saved_cflags=$CFLAGS
  ldas_saved_libs=$LIBS
  AC_ARG_WITH([framel],
  [ --with-framel=DIR   Directory where libframe exists],,
  [ for dir in /opt/lscsoft/libframe $prefix `echo $LD_LIBRARY_PATH | sed -e 's/:/ /g'`
    do
      dir="`echo $dir | sed -e 's/\/lib$//'`";
      if test -f "$dir/include/FrameL.h"
      then
        with_framel="$dir"
	break
      fi
    done ])

  case x$with_framel in
  x)
    ;;
  *)
    LIBS="$LIBS -L$with_framel/lib"
    ;;
  esac
  AC_CHECK_LIB(Frame,FrVectCompress,[
    AC_DEFINE(HAVE_LIBFRAME,1,Define to 1 if you have the `Frame' library (-lFrame))
    case x$with_framel in
    x)
      ;;
    *)
      LIBFRAME="-L$with_framel/lib "
      CPPFLAGSFRAME="-I$with_framel/include"
      ;;
    esac
    LIBFRAME="${LIBFRAME}-lFrame"
    LIBS="${lt_prog_compiler_static} ${LIBFRAME} -lm"
    CFLAGS="$CPPFLAGSFRAME $CFLAGS"
    AC_RUN_IFELSE([AC_LANG_SOURCE([
	#include "FrameL.h"

	extern int FrSlong;

	int
	main( )
	{
	  FrLibShortIni( );
	  /*
	   * Zero exit (true) if FR_LONG_LONG should be defined
	   * Non-zero (false) if FR_LONG_LONG should not be defined
	   */
	  exit( FrSlong == sizeof( long ) );
	}
    ])],[
      AC_DEFINE([DEFINE_FR_LONG_LONG],1,[Define to 1 if FR_LONG_LONG needs to be defined])
    ],[],[])

  ],[],-lm)
  AC_SUBST(LIBFRAME)
  AC_SUBST(CPPFLAGSFRAME)
  AM_CONDITIONAL(HAVE_LIBFRAME, test "x$LIBFRAME" != "x")
  CFLAGS=$ldas_saved_cflags
  LIBS=$ldas_saved_libs
  AC_LANG_RESTORE
])

dnl ---------------------------------------------------------------------
dnl  Check for Support of Xerces library
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_LIB_XERCES],[dnl
  AC_ARG_WITH([xerces],
              [AS_HELP_STRING([--with-xerces],
                              [@<:@default=check@:>@])],
              [],
	      [with_xerces=check])
  dnl -------------------------------------------------------------------
  dnl -------------------------------------------------------------------
  LIBXERCES=
  AS_IF([test "x${with_xerces}" != xno],
        [dnl ------------------------------------------------------------
         dnl  Setup for the C++ version of the library
	 dnl ------------------------------------------------------------
         AC_LANG_PUSH([C++])
	 ldas_xerces_save_cppflags="${CPPFLAGS}"
	 ldas_xerces_save_ldflags="${LDFLAGS}"
	 AS_CASE([${with_xerces}],dnl
	         [check],[],
	         [yes],[],
		 [dnl - default
		  CPPFLAGS="${CPPFLAGS} -I${with_xerces}/include/xml"
		  LDFLAGS="-L${with_xerces}/lib${LIB_64_DIR} ${LDFLAGS}"
                 ])
	 dnl ------------------------------------------------------------
	 dnl  Look for the directory containing the header files
	 dnl ------------------------------------------------------------
     	 AC_CHECK_HEADERS([xercesc/dom/DOMNode.hpp],
			  [AS_CASE([${with_xerces}],
			           [check],[],
			           [yes],[],
				   [XERCES_CPPFLAGS="-I${with_xerces}/include/xml"])
 			   HAVE_XERCESC_DOM_DOMNODE_HPP="1"
                          ]) dnl AC_CHECK_HEADERS
	 dnl ------------------------------------------------------------
	 dnl  Check for the library
	 dnl ------------------------------------------------------------
         AC_CHECK_LIB([xerces-c],[main],
                      [AC_SUBST([LIBXERCES],["-lxerces-c"])
		       AS_CASE([${with_xerces}],
			       [check],[],
			       [yes],[],
				XERCES_LDFLAGS="-L${with_xerces}/lib${LIB_64_DIR}")
		       AC_DEFINE([HAVE_LIBXERCES],[1],
		                 [Define if you have libxerces])
		      ],
		      [if test "x$with_xerces" != xcheck; then
		        AC_MSG_FAILURE(
			  [--with-xerces was given, but test for xerces failed])
		       fi
		      ])
	 dnl ------------------------------------------------------------
	 dnl  Restore to the previous language
	 dnl ------------------------------------------------------------
	 AC_SUBST([XERCES_LDFLAGS])
	 AC_SUBST([XERCES_CPPFLAGS])
	 LDFLAGS="${ldas_xerces_save_ldflags}"
     	 CPPFLAGS="${ldas_xerces_save_cppflags}"
     	 AC_LANG_POP
	]
  )
  dnl -------------------------------------------------------------------
  dnl  Create Makefile variable
  dnl -------------------------------------------------------------------
  AS_IF([test -n "${HAVE_XERCESC_DOM_DOMNODE_HPP}" dnl
              -a -n "${LIBXERCES}"],
	[XERCES_ENABLE_VAL="yes"],
	[XERCES_ENABLE_VAL="no"])
  AM_CONDITIONAL([HAVE_LIBXERCES],
		 [test -n "${HAVE_XERCESC_DOM_DOMNODE_HPP}" dnl
                       -a -n "${LIBXERCES}"])
])

dnl ---------------------------------------------------------------------
dnl  Check for Support of 
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_LIB_Z],
[ have_libz='yes'
  AC_CHECK_LIB( 
	z,
	compress,
	[ AC_DEFINE( HAVE_LIBZ,1,[Defined if have libz] )
	  AC_CHECK_LIB(z, compress2,[AC_DEFINE( HAVE_LIBZ_COMPRESS2,1,[Defined if libz had compress2 function] )]) ],
	[ if test -n "$1" && test "$1" = "no"
	  then
		AC_MSG_WARN( Required zlib library (libz.a) not found ) 
	        have_libz='';
	  else
		AC_MSG_ERROR( Required zlib library (libz.a) not found ) 
	  fi
	])
  AM_CONDITIONAL(HAVE_LIBZ, test x$have_libz != x)
])

dnl ---------------------------------------------------------------------
dnl  Check for Support of md5sum calculations
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_LIB_MD5SUM],
  [ dnl -----------------------------------------------------------------
    dnl  Check for the library first
    dnl -----------------------------------------------------------------
    case `uname` in
    Linux)

      AC_CHECK_LIB(crypto,MD5_Init,
	[ AC_DEFINE(HAVE_MD5_IN_CRYPTO,1,[Defined if have md5 routines in libcrypto])
	  LIBMD5="-lcrypto"
	  AC_SUBST(LIBMD5)
          AC_CHECK_HEADERS([openssl/md5.h])
	],[ AC_CHECK_LIB(md5,MD5Init,
	    [ AC_DEFINE(HAVE_LIBMD5,1,[Defined if have libmd5])
	      LIBMD5="-lmd5"
	      AC_SUBST(LIBMD5)
	      AC_CHECK_HEADERS([md5.h])
	      AC_CHECK_HEADERS([md5global.h])
            ],[AC_MSG_ERROR(Missing md5 support) ])])
      ;;
    *)
      AC_CHECK_HEADERS([CommonCrypto/CommonDigest.h])
      AC_CHECK_LIB(md5,MD5Init,
	[ AC_DEFINE(HAVE_LIBMD5,1,[Defined if have libmd5])
	  LIBMD5="-lmd5"
	  AC_SUBST(LIBMD5)
	  AC_CHECK_HEADERS([md5.h])
	  AC_CHECK_HEADERS([md5global.h])
	],[ AC_CHECK_LIB(crypto,MD5_Init,
	    [ AC_DEFINE(HAVE_MD5_IN_CRYPTO,1,[Defined if have md5 routines in libcrypto])
	      LIBMD5="-lcrypto"
	      AC_SUBST(LIBMD5)
              AC_CHECK_HEADERS([openssl/md5.h])
            ],[AC_MSG_ERROR(Missing md5 support) ])])
      ;;
    esac
  ])

dnl----------------------------------------------------------------------
dnl Check to see if ospace is available -
dnl variable:
dnl	$1 - a value other than "no" causes aborting of the configuration
dnl	     process. A value of "no" indicates that Object Space is
dnl	     an optional library.
dnl----------------------------------------------------------------------

AC_DEFUN([AC_LDAS_CHECK_LIB_OSPACE],
[ AC_CHECK_LIB(socket,connect)
  AC_CHECK_LIB(rt,sched_yield)
  AC_REQUIRE([AC_LDAS_CHECK_LIB_THREAD])
  AC_ARG_WITH([ospace], 
	      [AS_HELP_STRING([--with-ospace[[=DIR]]],
                              [specify ObjectSpace directory])],
	      [],
	      [with_ospace="check"])
  dnl -------------------------------------------------------------------
  dnl  Remember what the system looks like before macro called
  dnl -------------------------------------------------------------------
  HAVE_OSPACE=
  ldas_save_pkg_config_path="$PKG_CONFIG_PATH"
  AS_CASE(["x${with_ospace}"],
	  [xno],[],
	  [xyes],[],
	  [xcheck],[],
	  [ dnl default
	    PKG_CONFIG_PATH="${with_ospace}/lib${LIB_64_DIR}/pkgconfig:${PKG_CONFIG_PATH}"
	  ])
  ldas_save_cppflags="${CPPFLAGS}"
  ldas_save_ldflags="${LDFLAGS}"
  dnl -------------------------------------------------------------------
  dnl  Looking for ObjectSpace
  dnl -------------------------------------------------------------------
  AS_IF([test "x${with_ospace}" != xno],
  	[ dnl -----------------------------------------------------------
	  dnl -----------------------------------------------------------
	  PKG_CHECK_MODULES([OSPACE],[ospace],
	    [ dnl -------------------------------------------------------
	      dnl  Has been found
	      dnl -------------------------------------------------------
	      OSPACE_CXXFLAGS="${OSPACE_CFLAGS}"
	      HAVE_OSPACE="1"
	      OSPACE_LDFLAGS="`${PKG_CONFIG} --libs-only-L ospace`"
	      OSPACE_LIBS="`${PKG_CONFIG} --libs-only-l ospace`"
	      ospace_la="`${PKG_CONFIG} --variable=libdir ospace`/libospace.la"
	    ],
	    [ dnl -------------------------------------------------------
	      dnl Since it could not be found using pkg-config,
	      dnl  do it the old school way
	      dnl -------------------------------------------------------
	      dnl  Setup for the C++ version of the library
	      dnl -------------------------------------------------------
              AS_CASE(["x${with_ospace}"],
	      	[xcheck],[],
	      	[xyes],[],
	      	[ dnl default
		  LDFLAGS="-L${with_ospace}/lib${LIB_64_DIR} ${LDFLAGS}"
		  CPPFLAGS="-I${with_ospace}/include ${CPPFLAGS}"
	      	])
	      AC_LANG_PUSH([C++])
	      AC_CHECK_LIB([ospace],[main],
       	        [AC_SUBST([LIBOSPACE],["-lospace"])
                 AS_CASE(["${with_ospace}"],
	      	   [xcheck],[],
	      	   [xyes],[],
	      	   [ dnl default
		     OSPACE_LDFLAGS="-L${with_ospace}/lib${LIB_64_DIR}"
		     OSPACE_CXXFLAGS="-I${with_ospace}/include"
		     ospace_la="${with_ospace}/lib${LIB_64_DIR}/libospace.la"
	      	   ])
		 HAVE_OSPACE="1"],
		 [],
		 [-lpthread -lnsl -lm])
	      AC_LANG_POP([C++])
	    ]) dnl PKG_CHECK_MODULES
  ])
  dnl -------------------------------------------------------------------
  dnl  Set variables according to discovery
  dnl -------------------------------------------------------------------
  AS_IF([test "x${HAVE_OSPACE}" = "x1"],
        [AC_DEFINE([HAVE_LIBOSPACE],[1],
                   [Define to 1 if you have the `ObjectSpace' library])])
  AC_SUBST([OSPACE_LDFLAGS])
  AC_SUBST([OSPACE_CXXFLAGS])
  AC_SUBST([HAVE_OSPACE])
  AC_SUBST([ospace_la])
  AM_CONDITIONAL([HAVE_OSPACE],[test -n "${HAVE_OSPACE}"])
  dnl -------------------------------------------------------------------
  dnl  Restore the system
  dnl -------------------------------------------------------------------
  PKG_CONFIG_PATH="${ldas_save_pkg_config_path}"
  CPPFLAGS="${ldas_save_cppflags}"
  LDFLAGS="${ldas_save_ldflags}"
])

AC_DEFUN([AC_LDAS_CHECK_LIB_THREAD],[
  AC_ARG_WITH([threads],
	      [  --without-threads       Don't build reentrant code ],
       	      [ case x"$with_threads" in
                "x"|xno) ;;
                *)
		  CFLAGS="${CFLAGS} -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS"
		  CXXFLAGS="${CXXFLAGS} -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS"
		  LDAS_CHECK_PTHREAD_READ_WRITE_LOCK
		  ;;
	        esac
	      ],[
	        CFLAGS="${CFLAGS} -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS"
	        CXXFLAGS="${CXXFLAGS} -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS"
	        LDAS_CHECK_PTHREAD_READ_WRITE_LOCK
	      ])
])
dnl -------------------------------------------------------------------
dnl -- check for nsl networking library
dnl -------------------------------------------------------------------
AC_DEFUN([LDAS_LIB_NSL],[
  AC_CHECK_LIB([nsl],[gethostbyname],[
		LIB_NSL="-lnsl"
		AC_DEFINE([HAVE_LIBNSL],[1],[true if gethostbyname is defined in system library nsl])
		])
  AC_SUBST([LIB_NSL])
])
dnl -------------------------------------------------------------------
dnl -- check for SGI STL library (to use instead of gcc provided one)
dnl -------------------------------------------------------------------
AC_DEFUN([LDAS_LIB_STLPORT],[
  dnl -----------------------------------------------------------------
  dnl --- STLport - Core library
  dnl -----------------------------------------------------------------
  AC_ARG_WITH([stlport],
  [ --with-stlport=DIR   Build with SGI STL library],,
  [ for dir in $prefix `echo $LD_LIBRARY_PATH | sed -e 's/:/ /g'`
    do
      dir="`echo $dir | sed -e 's/\/lib$//'`";
      if test -d "$dir/include/stlport"
      then
        with_stlport="$dir"
	break
      fi
    done ])
  AC_MSG_CHECKING(for stlport installation directory)
  case "x$with_stlport" in
  x|xno)
    AC_MSG_RESULT(none)
    ;;
  *)
    if test -d "$dir"/include/stlport
    then
      with_stlport="$dir"
      AC_MSG_RESULT($with_stlport/include/stlport)
      STLPORT_INCLUDE_DIR="$with_stlport/include/stlport"
      STLPORT_LIB_DIR="$with_stlport/lib"
      if test -e $STLPORT_LIB_DIR/libstlport_gcc.so
      then
        STLPORT_LIB="stlport_gcc"
        STLPORT_DEBUG_LIB="stlport_gcc_stldbg"
      else
        STLPORT_LIB="stlport"
        STLPORT_DEBUG_LIB="stlportg"
      fi
      STLPORT_DEBUG_CXXFLAGS="-D_STLP_DEBUG -D_STLP_DEBUG_ALLOC -D_STLP_DEBUG_UNINITIALIZED"
    else
      AC_MSG_ERROR( invalid stlport directory: $with_stlport )
    fi
    ;;
  esac
  dnl -------------------------------------------------------------------
  dnl -- check for debug STLport library
  dnl -------------------------------------------------------------------
  AC_ARG_ENABLE(stlport-debug,
  [AS_HELP_STRING([--enable-stlport-debug],
	          [Build with debug STL library])],
  [enable_stlport_debug=yes],
  [enable_stlport_debug=no])
  dnl -------------------------------------------------------------------
  dnl -- Establish shell variable to know if using the debug version
  dnl -------------------------------------------------------------------
  case x"${with_stlport}" in
  x|xno)
    dnl -----------------------------------------------------------------
    dnl -- Either the installation directory was invalid or the user
    dnl --   specifically requested not to use STLport
    dnl -----------------------------------------------------------------
    ;;
  *)
    dnl -----------------------------------------------------------------
    dnl -- Want to use STLport
    dnl -----------------------------------------------------------------
    AC_MSG_CHECKING(for stlport debug)
    case x"${enable_stlport_debug}" in
    xyes)
      AC_MSG_RESULT(yes)
      case "${STLPORT_DEBUG_LIB}" in
        stlport_dbg)
        STLDEBUG="_dbg"
        ;;
      stlport_*_stldbg)
        STLDEBUG="_stldbg"
        ;;
      esac
      ;;
    *)
      dnl ---------------------------------------------------------------
      dnl Use non-debug version of stlport
      dnl ---------------------------------------------------------------
      AC_MSG_RESULT(no)
      ;;
    esac # x"${enable_stlport_debug}"
  esac # x"${enable_stlport}"
  dnl '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  dnl Set some compiler specific options
  dnl '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  if test "$ldas_prog_cxx_vendor" = Sun
  then
    if test -z "$STLPORT_LIB"
    then
      CXXFLAGS="$CXXFLAGS -library=stlport4"
    else
      CXXFLAGS="$CXXFLAGS -library=no%Cstd"
    fi
  fi
  AC_SUBST(STLDEBUG)
]) dnl LDAS_LIB_STLPORT

dnl ---------------------------------------------------------------------
dnl   Check for Support of STLport Finish
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_LIB_STLPORT_FINISH],[
  dnl -------------------------------------------------------------------
  dnl -- Establish compiler/linker flags for STLport
  dnl -------------------------------------------------------------------
  case x"${with_stlport}" in
  x|xno)
    dnl -----------------------------------------------------------------
    dnl -- Either the installation directory was invalid or the user
    dnl --   specifically requested not to use STLport
    dnl -----------------------------------------------------------------
    ;;
  *)
    dnl -----------------------------------------------------------------
    dnl -- Want to use STLport
    dnl -----------------------------------------------------------------
    case x"${enable_stlport_debug}" in
    xyes)
      debug_flags="$STLPORT_DEBUG_CXXFLAGS"
      stlport_lib="STLPORT_DEBUG_LIB";
      ;;
    *)
      dnl ---------------------------------------------------------------
      dnl Use non-debug version of stlport
      dnl ---------------------------------------------------------------
      stlport_lib="$STLPORT_LIB";
      ;;
    esac # x"${enable_stlport_debug}"
    case x${STLPORT_CXXFLAGS} in
    x) ;;
    *) AC_MSG_CHECKING(for stlport CXXFLAGS)
       CXXFLAGS="${CXXFLAGS} ${STLPORT_CXXFLAGS}"
       AC_MSG_RESULT(${STLPORT_CXXFLAGS})
       ;;
    esac
    case x${STLPORT_INCLUDE_DIR} in
    x) ;;
    *) AC_MSG_CHECKING(for stlport include flags)
       CXXFLAGS="${CXXFLAGS} -I${STLPORT_INCLUDE_DIR} $debug_flags"
       AC_MSG_RESULT(-I${STLPORT_INCLUDE_DIR} $debug_flags)
       ;;
    esac
    case x${STLPORT_LIB_DIR} in
    x)
       case x${stlport_lib} in
       x)
 	 ;;
       *)
         STLPORT_LDFLAGS="-l${stlport_lib}"
         ;;
       esac
       ;;
    *)
       STLPORT_LDFLAGS="${LDFLAGS} -L${STLPORT_LIB_DIR} -l${stlport_lib}"
       ;;
    esac
    case x$STLPORT_LDFLAGS in
    x) ;;
    *) AC_MSG_CHECKING(for stlport LDFLAGS)
       LDFLAGS="${LDFLAGS} ${STLPORT_LDFLAGS}"
       AC_MSG_RESULT(${STLPORT_LDFLAGS})
       ;;
    esac
    ;;
  esac # x"${enable_stlport}"
])
dnl ---------------------------------------------------------------------
dnl  Check for Support of md5sum calculations
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_LIB_UMEM],
[
  dnl -------------------------------------------------------------------
  dnl  Detect if the OS supplies a version of libumem
  dnl -------------------------------------------------------------------
  AS_IF([test -f /usr/lib${LIB_64_DIR}/libumem.so -o -L /usr/lib${LIB_64_DIR}/libumem.so],
        [LIBUMEM=/usr/lib${LIB_64_DIR}/libumem.so])
  AC_SUBST(LIBUMEM)
  AM_CONDITIONAL([HAVE_LIBUMEM],[test x${LIBUMEM} != x])
])
