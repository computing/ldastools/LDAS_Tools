dnl =====================================================================
dnl  Test for C++ 2011 features
dnl =====================================================================
dnl  LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_CONSTEXPR],[dnl
  AC_MSG_CHECKING([if the C++ compiler supports constexpr])
  AC_LANG_PUSH([C++])
  AC_TRY_RUN([
  constexpr int
  MeaningOfLife( )
  {
    return 42;
  }

  int
  main()
  {
    return ( ( MeaningOfLife( ) == 42 ) ? 0 : 1 );
  }
  ],[ AC_MSG_RESULT(yes)
  ],[ AC_MSG_RESULT(no)
      AC_DEFINE([constexpr],[const],[Defined if C++ does not support constexpr])
  ])
  AC_LANG_POP([C++])
])
dnl ---------------------------------------------------------------------
dnl  LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_TEMPLATE_ALIASES],
[
AC_MSG_CHECKING([if the C++ compiler supports template aliasing])
AC_LANG_PUSH([C++])
AC_TRY_COMPILE([
#include <memory>
namespace {
  template< class T > using AutoArray = std::unique_ptr< T[] >;
  template< typename T, typename U > using DynamicPointerCast = typename std::dynamic_pointer_cast< T, U >;
}
],[
 AutoArray< int >	a;
],[
  AC_DEFINE([HAVE_CXX_TEMPLATE_ALIASES],[1],[Defined if C++ supports template aliasing])
  AC_MSG_RESULT([yes])
  HAVE_CXX_TEMPLATE_ALIASES=1
],[
  AC_MSG_RESULT([no])
  HAVE_CXX_TEMPLATE_ALIASES=0
])
AC_SUBST([HAVE_CXX_TEMPLATE_ALIASES])
AC_LANG_POP([C++])
])
dnl ---------------------------------------------------------------------
dnl  LDAS_CXX_UNIQUE_PTR
dnl     Check if C++ supports move template
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_TEMPLATE_MOVE],[
  AC_LANG_PUSH([C++])
  AC_TRY_COMPILE([
   #include<algorithm>
   #include<string>
   #include<utility>
   #include<vector>
  ],
  [std::vector<std::string> foo;
   foo.push_back( "air" );
   foo.push_back( "water" );
   foo.push_back( "fire" );
   foo.push_back( "earth" );
   std::vector<std::string> bar(4);
   foo = std::move(bar);
  ],[
  AC_DEFINE([HAVE_TEMPLATE_MOVE],[1],dnl
	    [Defined if the move template is available])
  HAVE_TEMPLATE_MOVE=1
  ],[HAVE_TEMPLATE_MOVE=0])
  AC_SUBST([HAVE_TEMPLATE_MOVE])
  AC_LANG_POP([C++])
])
dnl ---------------------------------------------------------------------
dnl  LDAS_CXX_UNIQUE_PTR
dnl     Check if C++ supports unique_ptr template
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_UNIQUE_PTR],[
  AC_LANG_PUSH([C++])
  AC_TRY_COMPILE([
#if HAVE_MEMORY
#include <memory>
using namespace std;
#elif HAVE_TR1_MEMORY
#include <tr1/memory>
using namespace std::tr1;
#endif
  ],
  [unique_ptr<int> m;
   m.reset( new int );
  ],[
  AC_DEFINE([HAVE_UNIQUE_PTR],[1],dnl
	    [Defined if the unique_ptr template is available])
  HAVE_UNIQUE_PTR=1
  ],[HAVE_UNIQUE_PTR=0])
  AC_SUBST([HAVE_UNIQUE_PTR])
  AC_LANG_POP([C++])
])
