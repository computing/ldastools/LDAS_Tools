LDAS_DIRECTORY_MAKE_INCLUDE=[$(top_srcdir)/config/autotools/make]
AC_SUBST([LDAS_DIRECTORY_MAKE_INCLUDE])

AC_DEFUN([AC_LDAS_VAR_APPEND],[
  case "$2" in
  -*) opt="";;
  *)
    case $1 in
    *LDFLAGS) opt="-L" ;;
    *CXXFLAGS|*CFLAGS) opt="-I" ;;
    *) opt="";;
    esac
    ;;
  esac
  case x$3 in
  x) ;;
  xLITERAL) opt="" ;;
  *) opt="$3" ;;
  esac
  opt="${opt}$2"
  st="[[ |	]]";
  echo "[$]$1" |  egrep "^${opt}${st}|${st}${opt}${st}|${st}$opt[$]|^${opt}[$]"
  if test $? -ne 0
  then
    $1="[$]$1 ${opt}"
  fi
])

AC_DEFUN([AC_LDAS_VAR_PREPEND],[
  case "$2" in
  -*) opt="";;
  *)
    case $1 in
    *LDFLAGS) opt="-L" ;;
    *CXXFLAGS|*CFLAGS) opt="-I" ;;
    *) opt="";;
    esac
    ;;
  esac
  case x$3 in
  x) ;;
  xLITERAL) opt="" ;;
  *) opt="$3"
  esac
  opt="${opt}$2"
  st="[[ |	]]";
  echo "[$]$1" |  egrep "^${opt}${st}|${st}${opt}${st}|${st}${opt}\\[$]|^${opt}\\[$]"
  if test $? -ne 0
  then
    $1="[$]$1 ${opt}"
  fi
])

AC_DEFUN([AC_LDAS_VAR_APPEND_COND],[
  if test $1
  then
    AC_LDAS_VAR_APPEND($2,$3)
  fi
])

AC_DEFUN([AC_LDAS_VAR_PREPEND_COND],[
  if test $1
  then
    AC_LDAS_VAR_PREPEND($2,$3)
  fi
])

