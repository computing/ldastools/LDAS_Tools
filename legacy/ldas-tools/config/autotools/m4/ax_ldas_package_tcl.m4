AC_DEFUN([AX_LDAS_PACKAGE_TCL],
[
  AC_ARG_ENABLE([tcl],
    [ --disable-tcl  Disable the use of tcl] )
  case x$enable_tcl in
  xno)
    unset TCLSH
    unset WISH
    unset with_tcl_config
    ;;
  *)
    AC_PATH_PROG( TCLSH, tclsh )
    AC_PATH_PROG( WISH, wish )
    AC_ARG_WITH([tcl-config],
	AC_HELP_STRING([--with-tcl-config=<file>],
		       [Fully pathed location of the TCL configuration file]),
      [],
      [
        dnl ---------------------------------------------------------------
        dnl  Base the educated guess on where the tcl executable was
        dnl    located.
        dnl ---------------------------------------------------------------
        path="`echo $TCLSH | sed -e 's,bin/.*$,lib'\"${LIB_64_DIR}\"',g'`"
        if test ! -f "$path/tclConfig.sh"
        then
          dnl -------------------------------------------------------------
          dnl  Need to see the tclConfig.sh file is in a subdirectory.
          dnl -------------------------------------------------------------
  	path="$path/tcl`echo '$tcl_version' | $TCLSH`"
        fi
        if test -f "$path/tclConfig.sh"
        then
          with_tcl_config=$path/tclConfig.sh
        fi
      ])
    ;;
  esac
  AM_CONDITIONAL(HAVE_TCL,test -f "${with_tcl_config}")
  AC_MSG_CHECKING(for tcl)
  if test -f "${with_tcl_config}"
  then
    AC_MSG_RESULT(yes)
    HAVE_TCL=1
    dnl -----------------------------------------------------------------
    dnl  import the configuration file
    dnl -----------------------------------------------------------------
    . "${with_tcl_config}"
    dnl -----------------------------------------------------------------
    dnl  export those variables that will be needed
    dnl -----------------------------------------------------------------
    AC_SUBST(TCL_INCLUDE_SPEC)
    AC_SUBST(TCL_LIB_SPEC)
    AC_SUBST(TCL_STUB_SPEC)
    AC_SUBST(TCL_VERSION)
    AC_MSG_CHECKING(for tcl version)
    AC_MSG_RESULT($TCL_VERSION)
    dnl -----------------------------------------------------------------
    dnl  Define for building extensions
    dnl -----------------------------------------------------------------
    AC_DEFINE([HAVE_TCL],dnl
	      [1],dnl
              [Defined if the scripting language TCL is supported])
  else
    HAVE_TCL=0
    AC_MSG_RESULT(no)
  fi
  AC_SUBST([HAVE_TCL])
])
