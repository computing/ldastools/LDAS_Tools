dnl =====================================================================

AC_DEFUN([LDAS_PACKAGE_PERL],
[ AC_ARG_ENABLE([perl],
	        AC_HELP_STRING([--enable-perl],
                    	       [use perl (default is CHECK)]),
	        [ case "${enable_perl}" in
	          no)
		    ;;
                  *)
		    ;;
                  esac ],
	        [enable_perl="CHECK";] )
  case x${enable_perl} in
  xno)
    dnl -----------------------------------------------------------------
    dnl  Setup for AM_CONDITIONAL statements since perl was disabled
    dnl -----------------------------------------------------------------
    unset PERL
    ;;
  *)
    AC_PATH_PROG([PERL],perl)
    ;;
  esac
  AM_CONDITIONAL([HAVE_PERL],dnl
                 [test "x${PERL}" != "x"])
])
AC_DEFUN([LDAS_PACKAGE_PYTHON],
[
  AC_ARG_ENABLE([python],
	         AC_HELP_STRING([--enable-python],
                    	        [use python (default is CHECK)]),
	         [ case "${enable_python}" in
	           no)
		     ;;
                   *)
		     ;;
                   esac ],
	         [enable_python="CHECK";] )
  case x${enable_python} in
  xno)
    dnl -----------------------------------------------------------------
    dnl  Setup for AM_CONDITIONAL statements since python was disabled
    dnl -----------------------------------------------------------------
    ldas_have_python_package_ctypes="no"
    ldas_have_python_package_numpy="no"
    PYTHON=":"
    HAVE_PYTHON_H=""
    ;;
  *)
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Look for the basics
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AM_PATH_PYTHON([$1],,[:])
    if test "${PYTHON}" != ":"
    then
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  See if the version of python is recent enough to
      dnl    support python-config
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_PATH_PROG([PYTHON_CONFIG],[python${PYTHON_VERSION}-config python-config])
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Figure out the include directive for python
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for python includes)
      case x$PYTHON_CONFIG in
      x)
	dnl -------------------------------------------------------------
	dnl Get the information the hard way
	dnl -------------------------------------------------------------
        PYTHON_INCLUDES="-I`echo ${PYTHON} | \
	  sed 's,/python$,,g' | \
	  sed 's,/amd64,64,g' | \
	  sed 's,/64,64,g' | \
	  sed 's,/bin\([[^/]]*\),/include\1,g'`/python$PYTHON_VERSION";
	;;
      *)
	dnl -------------------------------------------------------------
	dnl Newer version of python have the python-config script
	dnl -------------------------------------------------------------
    	PYTHON_INCLUDES="`$PYTHON_CONFIG --includes`"
	;;
      esac
      AC_SUBST(PYTHON_INCLUDES)
      AC_MSG_RESULT(${PYTHON_INCLUDES})
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl Checking for Python EXEC_PREFIX
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for python EXEC_PREFIX directory)
      PYTHON_SITE_PACKAGES_DIR="`${PYTHON} -c 'from distutils.sysconfig import EXEC_PREFIX; print(EXEC_PREFIX)'`" 2>/dev/null
      AC_SUBST(PYTHON_SITE_PACKAGES_DIR)
      AC_MSG_RESULT(${PYTHON_SITE_PACKAGES_DIR})
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl Checking for Python module directory
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for python module directory)
      PYTHON_SITE_PACKAGES_DIR="`${PYTHON} -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib(1))'`" 2>/dev/null
      AC_SUBST(PYTHON_SITE_PACKAGES_DIR)
      AC_MSG_RESULT(${PYTHON_SITE_PACKAGES_DIR})
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Checking for Python header files
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Python.h
      dnl ---------------------------------------------------------------
      old_CPPFLAGS="${CPPFLAGS}"
      CPPFLAGS="${PYTHON_INCLUDES} ${CPPFLAGS}"
      AC_CHECK_HEADERS([Python.h],[HAVE_PYTHON_H="yes"])
      CPPFLAGS="${old_CPPFLAGS}"
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Checking for Python packages
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Python Package - ctypes
      dnl ---------------------------------------------------------------
      AC_MSG_CHECKING(for python package ctypes)
      if test -z "`${PYTHON} -c 'import ctypes' 2>&1`"
      then
        ldas_have_python_package_ctypes="yes"
      else
        ldas_have_python_package_ctypes="no"
      fi
      AC_MSG_RESULT($ldas_have_python_package_ctypes)
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Python Package - numpy
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for pythong package numpy)
      if test -z "`${PYTHON} -c 'import numpy' 2>&1`"
      then
        ldas_have_python_package_numpy="yes"
        AC_MSG_RESULT($ldas_have_python_package_numpy)
        ldas_cppflags=${CPPFLAGS}
        PYTHON_NUMPY_INCLUDE="`${PYTHON} -c 'import numpy; print numpy.get_include()' 2>/dev/null | sed -e 's/^[[[].\(.*\).[]]]\$/\\1/g'`"
        if test x${PYTHON_NUMPY_INCLUDE} != x
        then
          PYTHON_NUMPY_INCLUDE="-I${PYTHON_NUMPY_INCLUDE}"
        fi
        CPPFLAGS="${PYTHON_NUMPY_INCLUDE} ${PYTHON_INCLUDES} $CPPFLAGS"
        AC_CHECK_HEADERS(numpy/arrayobject.h,[],[],
  	[#include <Python.h>
        ])
        CPPFLAGS=${ldas_cppflags}
        AC_SUBST(PYTHON_NUMPY_INCLUDE)
      else
        ldas_have_python_package_numpy="no"
        AC_MSG_RESULT($ldas_have_python_package_numpy)
      fi
    fi
    ;;
  esac
  dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  dnl  Checking for Python packages - end
  dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  AM_CONDITIONAL([HAVE_PYTHON_PACKAGE_CTYPES],dnl
                 [test "${ldas_have_python_package_ctypes}" = "yes"])
  AM_CONDITIONAL([HAVE_PYTHON_PACKAGE_NUMPY],dnl
                 [test "${ldas_have_python_package_numpy}" = "yes"])
  AM_CONDITIONAL([HAVE_PYTHON],[test "${PYTHON}" != ":" -a "${HAVE_PYTHON_H}" != ""])
])

dnl =====================================================================

AC_DEFUN([LDAS_PACKAGE_TCL],
[
  AC_ARG_ENABLE([tcl],
    [ --disable-tcl  Disable the use of tcl] )
  case x$enable_tcl in
  xno)
    unset TCLSH
    unset WISH
    unset with_tcl_config
    ;;
  *)
    AC_PATH_PROG( TCLSH, tclsh )
    AC_PATH_PROG( WISH, wish )
    AC_ARG_WITH([tcl-config],
	AC_HELP_STRING([--with-tcl-config=<file>],
		       [Fully pathed location of the TCL configuration file]),
      [],
      [
        dnl ---------------------------------------------------------------
        dnl  Base the educated guess on where the tcl executable was
        dnl    located.
        dnl ---------------------------------------------------------------
        path="`echo $TCLSH | sed -e 's,bin/.*$,lib'\"${LIB_64_DIR}\"',g'`"
        if test ! -f "$path/tclConfig.sh"
        then
          dnl -------------------------------------------------------------
          dnl  Need to see the tclConfig.sh file is in a subdirectory.
          dnl -------------------------------------------------------------
  	path="$path/tcl`echo '$tcl_version' | $TCLSH`"
        fi
        if test -f "$path/tclConfig.sh"
        then
          with_tcl_config=$path/tclConfig.sh
        fi
      ])
    ;;
  esac
  AM_CONDITIONAL(HAVE_TCL,test -f "${with_tcl_config}")
  AC_MSG_CHECKING(for tcl)
  if test -f "${with_tcl_config}"
  then
    AC_MSG_RESULT(yes)
    dnl -----------------------------------------------------------------
    dnl  import the configuration file
    dnl -----------------------------------------------------------------
    . "${with_tcl_config}"
    dnl -----------------------------------------------------------------
    dnl  export those variables that will be needed
    dnl -----------------------------------------------------------------
    AC_SUBST(TCL_INCLUDE_SPEC)
    AC_SUBST(TCL_LIB_SPEC)
    AC_SUBST(TCL_STUB_SPEC)
    AC_SUBST(TCL_VERSION)
    AC_MSG_CHECKING(for tcl version)
    AC_MSG_RESULT($TCL_VERSION)
    dnl -----------------------------------------------------------------
    dnl  Define for building extensions
    dnl -----------------------------------------------------------------
    AC_DEFINE([HAVE_TCL],dnl
	      [1],dnl
              [Defined if the scripting language TCL is supported])
  else
    AC_MSG_RESULT(no)
  fi
])
