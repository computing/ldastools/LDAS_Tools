#! /ldcg/bin/tclsh

proc execdb2cmd { cmd } {
	uplevel {
		set start [ clock seconds ]
		set cmd "exec $::db2 $cmd"
        if  { ! [ regexp -nocase {connect} $cmd ] } {
		    puts $cmd
        }
		catch { eval $cmd } data
		set end [ clock seconds ]
		puts $data
		puts "took [ expr $end-$start ] seconds"
	}
}

set dbconfig { "logsecond 100" "locktimeout 10" "locklist 400" }


set TOPDIR /ldas_outgoing
set LDAS /ldas
set statefile [ file join $::TOPDIR cntlmonAPI cntlmon.state ]
if      { [ file exist $statefile ] } {
        source $statefile
} else {
        set ::LDASLOG /ldas_outgoing/logs
        set ::LDASARC /ldas_outgoing/logs/archive
        set ::RUNCODE [string toupper [exec cat /etc/ldasname]]
        set ::LDASTMP $TOPDIR/tmp
        set ::PUBDIR  $TOPDIR/jobs
        set ::LDAS /ldas
}  
if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
puts "db2=$db2"
#set dsname [ lindex $argv 0 ]

source /ldas/doc/db2/doc/text/dbtables.tcl
set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl
source /ldas_outgoing/metadataAPI/LDASdsnames.ini
source /ldas_outgoing/cntlmonAPI/.cntlmon.state
set backupdir [ file join $::dbdir backup ]
set dblist [ set ::site_dbnames($::LDAS_SYSTEM) ]

foreach dsname $dblist {
    set cmd "connect to $dsname"
    execdb2cmd $cmd
    foreach item $dbconfig {
        set cmd "update database configuration for $dsname using $item"
        execdb2cmd $cmd
    }
    set cmd "get database configuration for $dsname"
    execdb2cmd $cmd
    set cmd terminate
    execdb2cmd $cmd
}
