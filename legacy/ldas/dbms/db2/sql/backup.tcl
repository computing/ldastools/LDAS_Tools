#! /ldcg/bin/tclsh

proc execdb2cmd { cmd } {
	uplevel {
		set start [ clock seconds ]
		set cmd "exec $::db2 $cmd"
        if  { ! [ regexp -nocase {connect} $cmd ] } {
		    puts $cmd
        }
		catch { eval $cmd } data
		set end [ clock seconds ]
		puts $data
		puts "took [ expr $end-$start ] seconds"
	}
}

set statefile [ file join /ldas_outgoing cntlmonAPI cntlmon.state ]
if      { [ file exist $statefile ] } {
        source $statefile
} else {
        set ::LDASLOG /ldas_outgoing/logs
        set ::LDASARC /ldas_outgoing/logs/archive
        set ::RUNCODE [string toupper [exec cat /etc/ldasname]]
        set ::LDASTMP $TOPDIR/tmp
        set ::PUBDIR  $TOPDIR/jobs
        set ::LDAS /ldas
}  

source /ldas_outgoing/cntlmonAPI/cntlmon.state

if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
puts "db2=$db2"
#set dsname [ lindex $argv 0 ]

source /ldas/doc/db2/doc/text/dbtables.tcl
set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl
source /ldas_outgoing/metadataAPI/LDASdsnames.ini
set ::dbdir /ldas_usr/ldas/test

set backupdir [ file join $::dbdir backup ]
if	{ ! [ file exist $backupdir ] } {
	file mkdir $backupdir
}

foreach dsname  [ set ::site_dbnames($::LDAS_SYSTEM) ] {
    set cmd "backup database $dsname to $backupdir"
    execdb2cmd $cmd
    catch { exec ls -lt $backupdir } data
    puts "backup $data"
}
