#! /ldcg/bin/tclsh

proc execdb2cmd { cmd } {
	uplevel {
		set cmd "exec $::db2 $cmd"
		# sendCmd
		catch { eval $cmd } data
		puts $data
	}
}

set auto_path "/ldas/lib $auto_path"
set API metadata
source /ldas_outgoing/LDASapi.rsc
source /ldas_outgoing/cntlmonAPI/cntlmon.state
source /ldas/doc/db2/doc/text/dbtables.tcl

if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
source /ldas/lib/genericAPI/b64.tcl

if	{ $argc } {
	set dblist [ lindex $argv 0 ]
} else {
	source /ldas_outgoing/metadataAPI/LDASdsnames.ini
	set dblist [ set ::site_dbnames($::LDAS_SYSTEM) ]
}

foreach dsname $dblist {
	set cmd "connect to $dsname user ldasdb using [decode64 $::dblock]"
	execdb2cmd $cmd
	foreach table $::Tables {
		set table "'[ string toupper $table ]'"
		lappend temp $table
	}
	set tables [ join $temp , ]

	set cmd "select card,overflow,npages,fpages from syscat.tables where tabname in ($tables)"
	set statscmd $cmd
	puts $cmd
	execdb2cmd $cmd

	;## do runstats on table
	foreach table $::Tables {	
		set cmd "runstats on table ldasdb.$table with distribution and indexes all shrlevel change"
		puts $cmd
		execdb2cmd $cmd
	}

	puts "after runstats"
	set cmd $statscmd
	execdb2cmd $cmd

	set cmd terminate
	execdb2cmd $cmd
}
