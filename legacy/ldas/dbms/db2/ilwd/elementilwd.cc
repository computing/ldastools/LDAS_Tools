#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>


using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const int ARRAYSIZE = 1000000;
int numElements;

namespace element_test {
CHAR_U  elm_CHAR_U[ARRAYSIZE];
CHAR    elm_CHAR_S[ARRAYSIZE];
INT_2U	elm_INT_2U[ARRAYSIZE];
INT_2S	elm_INT_2S[ARRAYSIZE];
INT_4U	elm_INT_4U[ARRAYSIZE];
INT_4S	elm_INT_4S[ARRAYSIZE];
INT_8U  elm_INT_8U[ARRAYSIZE];
INT_8S  elm_INT_8S[ARRAYSIZE];
REAL_4	elm_REAL_4[ARRAYSIZE];
REAL_8	elm_REAL_8[ARRAYSIZE];
string  elm_lstring_2[ARRAYSIZE];
string  elm_lstring_4[ARRAYSIZE];
string  elm_lstring_8[ARRAYSIZE];
string  elm_lstring_16[ARRAYSIZE];

} // end namespace

int thistime = (int)time (0) ;

static void setup_char_s () {
	for (int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
            srand ( i ); 
	        element_test::elm_CHAR_S[i] = (CHAR) ( rand() % 128 );
        } else {
            element_test::elm_CHAR_S[i] = (CHAR) ( rand() % 127 ) * (-1);
        }
	}
}

static void setup_char_u () {

	for (int i=0; i < numElements; i++) {
	    element_test::elm_CHAR_U[i] = (CHAR_U) i % 256;
	}
}

static void setup_int_2s () {

	for (int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
	        element_test::elm_INT_2S[i] = (INT_2S) ( rand() % 128 ) ;
        } else {
            element_test::elm_INT_2S[i] = (INT_2S) ( rand() % 128 ) * (-1);
        }
	}
}

static void setup_int_2u () {

	for (int i=0; i < numElements; i++) {
	    element_test::elm_INT_2U[i] = (INT_2U) rand() ;
	}
}

static void setup_int_4s () {

	for (int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
	        element_test::elm_INT_4S[i] = (INT_4S) ( rand() % 32768 );
        } else {
            element_test::elm_INT_4S[i] = (INT_4S) ( rand() % 32768) * (-1);
        }
	}
}

static void setup_int_4u () {

	for (int i=0; i < numElements; i++) {
	    element_test::elm_INT_4U[i] = (INT_4U) rand() % 32768;
	}
}

static void setup_int_8s () {

	for (int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
	        element_test::elm_INT_8S[i] = (INT_8S) rand();
        } else {
            element_test::elm_INT_8S[i] = (INT_8S) ( rand() * (-1) );
        }
	}    
}

static void setup_int_8u () {

	for (int i=0; i < numElements; i++) {
	    element_test::elm_INT_8U[i] = (INT_8U) rand();
	}
}

static void setup_real_4 () {

	for (unsigned i=0; i < numElements; i++) {
	    element_test::elm_REAL_4[i] = (REAL_4) rand() * 1.1;
	}
}

static void setup_real_8 () {

	for (unsigned int i=0; i < numElements; i++) {
	    element_test::elm_REAL_8[i] = (REAL_8) rand() * 1.1 ;
	}
}

static void setup_lstring_2 () {

// fill in ifo_site array
// primary key
	for ( unsigned i=0; i < numElements; i++) {
	    element_test::elm_lstring_2[i] = string("ab");
	}
}

static void setup_lstring_4 () {

// fill in ifo_site array
// primary key
	for ( unsigned i=0; i < numElements; i++) {
	    element_test::elm_lstring_4[i] = string("abcd");
	}
}
static void setup_lstring_8 () {

// fill in ifo_site array
// primary key
	for ( unsigned i=0; i < numElements; i++) {
	    element_test::elm_lstring_8[i] = string("abcdefgh");
	}
}

static void setup_lstring_16 () {

// fill in ifo_site array
// primary key
	for ( unsigned i=0; i < numElements; i++) {
	    element_test::elm_lstring_16[i] = string("abcdefghijklmnop");
	}
}

// ilwd types
//        ID_CHAR,
//        ID_CHAR_U,
//        ID_INT_2S,
//        ID_INT_2U,
//        ID_INT_4S,
//        ID_INT_4U,
//        ID_INT_8S,
//        ID_INT_8U,
//        ID_REAL_4,
//        ID_REAL_8,
//        ID_ILWD,
//        ID_LSTRING,

// argv[1] = 
main(int argc, char **argv) { 
    char *element_type;
    
    ILwd::Format format;
    ILwd::Compression compression;
    	
    numElements = atoi(argv[1]);
    element_type = argv[2];
    
    if  ( argc > 3 ) {
        if  ( ! strcmp (argv[3], "ascii") ) {
            format = ILwd::ASCII ; 
            compression = ILwd::NO_COMPRESSION ; 
        } else if ( ! strcmp (argv[3], "base64") ){
            format = ILwd::BASE64 ;
            compression = ILwd::GZIP; 
        } else {
            format = ILwd::BINARY ;
            compression = ILwd::GZIP9 ;
        }
    }
    
    if  ( numElements > ARRAYSIZE) {
        cerr << "max size is " << ARRAYSIZE;
        exit (-1);
    }

	LdasContainer ilwd1( "ligo:element_test:file");
    ilwd1.setComment("testing send/recv elements");
    char buffer[500];
    char units[100];
    sprintf (buffer, "element_test:%s", element_type);
	LdasContainer ilwd3( buffer );
    ilwd1.setComment("main container");
    sprintf (buffer, "element_test:%s:%d", element_type, numElements);
    sprintf (units, "%s_units", element_type);
    
    if  ( ! strcmp (element_type , "char_s" ) ) {
    
	        setup_char_s();
            LdasArray< CHAR > char_s_vector ( element_test::elm_CHAR_S, numElements,
            buffer, units );
            char_s_vector.setComment(buffer);            
            ilwd3.push_back( char_s_vector );
            
    } else if ( ! strcmp (element_type , "char_u" )) {
    
	        setup_char_u();
            LdasArray< CHAR_U > char_u_vector ( element_test::elm_CHAR_U, numElements,
            buffer, units );
            char_u_vector.setComment(buffer);   
	        ilwd3.push_back( char_u_vector );  
              
    } else if ( ! strcmp (element_type , "int_2s" )) {
    
	        setup_int_2s();
            LdasArray< INT_2S > int_2s_vector ( element_test::elm_INT_2S, numElements,
 	        buffer, units);
            int_2s_vector.setComment(buffer);  
	        ilwd3.push_back( int_2s_vector );
            
    } else if ( ! strcmp (element_type , "int_2u" )) {
    
	        setup_int_2u();
            LdasArray< INT_2U > int_2u_vector ( element_test::elm_INT_2U, numElements,
 	        buffer, units) ;
            int_2u_vector.setComment(buffer); 
	        ilwd3.push_back( int_2u_vector ); 
               
    } else if ( ! strcmp (element_type , "int_4s" )) {
    
	        setup_int_4s();
            LdasArray< INT_4S > int_4s_vector ( element_test::elm_INT_4S, numElements,
 	        buffer, units) ;
            int_4s_vector.setComment(buffer); 
	        ilwd3.push_back( int_4s_vector );
            
    } else if ( ! strcmp (element_type , "int_4u" )) {
    
	        setup_int_4u();
            LdasArray< INT_4U > int_4u_vector ( element_test::elm_INT_4U, numElements,
 	        buffer, units ) ;
            int_4u_vector.setComment(buffer); 
	        ilwd3.push_back( int_4u_vector );
            
    } else if ( ! strcmp (element_type , "int_8s" )) {
    
	        setup_int_8s();
            LdasArray< INT_8S > int_8s_vector ( element_test::elm_INT_8S, numElements,
 	        buffer, units ) ;
            int_8s_vector.setComment(buffer); 
	        ilwd3.push_back( int_8s_vector );
            
    } else if ( ! strcmp (element_type , "int_8u" )) {
    
	        setup_int_8u();
            LdasArray< INT_8U > int_8u_vector ( element_test::elm_INT_8U, numElements,
 	        buffer, units) ;
            int_8u_vector.setComment(buffer); 
	        ilwd3.push_back( int_8u_vector );
            
    } else if ( ! strcmp (element_type , "real_4" )) {
    
	        setup_real_4();
            LdasArray< REAL_4 > real_4_vector ( element_test::elm_REAL_4, numElements,
 	        buffer, units ) ;
            real_4_vector.setComment(buffer); 
	        ilwd3.push_back( real_4_vector ); 
                   
    } else if ( ! strcmp (element_type , "real_8" )) {
    
	        setup_real_8();
            LdasArray< REAL_8 > real_8_vector ( element_test::elm_REAL_8, numElements,
 	        buffer ) ;
            real_8_vector.setComment(buffer); 
	        ilwd3.push_back( real_8_vector );
            
    } else if ( ! strcmp (element_type , "lstring_2" )) {
	        setup_lstring_2();
            LdasString lstring_vector ( element_test::elm_lstring_2, (size_t) numElements,
 	        string(buffer), string(units) ) ;
            lstring_vector.setComment(buffer); 
	        ilwd3.push_back( lstring_vector );
            
    } else if ( ! strcmp (element_type , "lstring_4" )) {
	        setup_lstring_4();
            LdasString lstring_vector ( element_test::elm_lstring_4, (size_t) numElements,
 	        string(buffer), string(units) ) ;
            lstring_vector.setComment(buffer); 
	        ilwd3.push_back( lstring_vector );
            
    } else if ( ! strcmp (element_type , "lstring_8" )) {
	        setup_lstring_8();
            LdasString lstring_vector ( element_test::elm_lstring_8, (size_t) numElements,
 	        string(buffer), string(units) ) ;
            lstring_vector.setComment(buffer); 
	        ilwd3.push_back( lstring_vector );
            
    } else if ( ! strcmp (element_type , "lstring_16" )) {
	        setup_lstring_16();
            LdasString lstring_vector ( element_test::elm_lstring_16, (size_t) numElements,
 	        string(buffer), string(units) ) ;
            lstring_vector.setComment(buffer); 
	        ilwd3.push_back( lstring_vector );
    }

 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, format, compression );
 	cout << endl;
}
