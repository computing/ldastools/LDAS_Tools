#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3datasource( "gdsgroup:datasource" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;

	static string	channel_id[ARRAYSIZE];
	static string	frame_id[ARRAYSIZE];
	static INT_4S	offset[ARRAYSIZE];
	static INT_4S	time_span[ARRAYSIZE];
	static INT_2S	online_flag[ARRAYSIZE];
	static string	datasource_id[ARRAYSIZE];
} // namespace 

int thistime = (int)time(0);

static void setup_channel_id () {

// fill in channel_id array
// point to specific frames 
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "C%d", i);
	    channel_id[i] = string(buf);
	}
}

static void setup_frame_id () {

// fill in frame_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "LLH_295905800.raw");
	    frame_id[i] = string(buf);
	}
}

static void setup_offset () {

// fill in offset array
	for (int i=0; i < numElements; i++) {
	    offset[i] = i+1 ;
	}
}

static void setup_time_span () {

// fill in time_span array
	for (int i=0; i < numElements; i++) {
	    time_span[i] = i+1 ;
	}
}

static void setup_online_flag () {

// fill in online_flag array
	for (int i=0; i < numElements; i++) {
	    online_flag[i] = i+1 ;
	}
}

static void setup_datasource_id () {

// datasource_id is a uniqueId field, can be filled in by LDAS.
}

void setup_datasource () {

	setup_channel_id();
	ilwd3datasource.push_back(  LdasString(channel_id, (size_t) numElements,
	string("gdsgroup:datasource:channel_id"), string("") ) );

	setup_frame_id();
	ilwd3datasource.push_back(  LdasString(frame_id, (size_t) numElements,
	string("gdsgroup:datasource:frame_id"), string("") ) );

	setup_offset();
	ilwd3datasource.push_back(  LdasArray< INT_4S >(offset, numElements,
	"gdsgroup:datasource:offset" ) );

	setup_time_span();
	ilwd3datasource.push_back(  LdasArray< INT_4S >(time_span, numElements,
	"gdsgroup:datasource:time_span" ) );

	setup_online_flag();
	ilwd3datasource.push_back(  LdasArray< INT_2S >(online_flag, numElements,
	"gdsgroup:datasource:online_flag" ) );

	//datasource_id can be filled in by LDAS

}
