#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace frame {
LdasContainer   frame_id_cont ( "framegroup:frame:frame_id" );
LdasContainer   frameset_time_cont ( "framegroup:frame:frameset_time" );

string	frame_id[ARRAYSIZE];
string	frameset_time[ARRAYSIZE];
} // end namespace

int thistime = (int)time(0);

static void setup_frame_id () {

// fill in frame_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "LLH_%d.raw", thistime + i);
	    frame::frame_id[i] = string(buf);
	}
}

static void setup_frame_id_new () {

// fill in frame_id array
 	CHAR frame_id[100];
    CHAR frame_idattr[200];
    
    frame::frame_id_cont.setComment("frame_id"); 
    
	for (int i=0; i < numElements; i++) {
	    sprintf(frame_id, "LLH_%d.raw", thistime + i);
        sprintf(frame_idattr, "frame:frame_id:%s", frame_id);
        LdasArray< CHAR > frame_id_vector ( frame_id, strlen(frame_id), 
            frame_idattr );
        frame::frame_id_cont.push_back( frame_id_vector );
	}
}

static void setup_frameset_time () {

// fill in frameset_time array
// adjust to frameset_time in frameset table
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "1979-05-18-13.00.16.000000",i % 10);
	    frame::frameset_time[i] = string(buf);
	}
}

static void setup_frameset_time_new () {

// fill in frameset_time array
    CHAR frameset_time[] = "1999-10-24-10.18.46.000096";
    CHAR frameset_timeattr[200];
    
    frame::frameset_time_cont.setComment("frameset time");
    
	for (int i=0; i < numElements; i++) {
        sprintf(frameset_timeattr, "program:frameset:%s", frameset_time);
        LdasArray< CHAR > frameset_time_vector ( frameset_time, strlen(frameset_time), 
           frameset_timeattr );            
        frame::frameset_time_cont.push_back( frameset_time_vector );
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:frame:file" );
	LdasContainer ilwd3( "framegroup:frame:table" );
	setup_frame_id_new();
	//ilwd3.push_back(  LdasString(frame::frame_id, (size_t) numElements,
	//string("framegroup:frame:frame_id"), string("") ) );
    ilwd3.push_back( frame::frame_id_cont );

	setup_frameset_time_new();
	//ilwd3.push_back(  LdasString(frame::frameset_time, (size_t) numElements,
	//string("framegroup:frame:frameset_time"), string("") ) );
    ilwd3.push_back( frame::frameset_time_cont );

 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
