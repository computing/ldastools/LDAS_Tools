#include <string.h>
#include <stdio.h>
#include <time.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace frameset {
LdasContainer   frameset_time_cont ( "framesetgroup:frameset:frameset_time" );
LdasContainer   description_cont ( "framesetgroup:frameset:description" );
LdasContainer	program_id_cont ( "framesetgroup:frameset:program_id" );

string	frameset_time[ARRAYSIZE];
INT_4S	delta_time[ARRAYSIZE];
string	description[ARRAYSIZE];
string	program_id[ARRAYSIZE];
} // end namespace


static void setup_frameset_time () {

// fill in frameset_time array
	char buf[100];
    const time_t thistime = time(0);
    struct tm *ltime = localtime( &thistime );
    
    frameset::frameset_time_cont.setComment("frameset time");
    
	for (int i=0; i < numElements; i++) {
	    //sprintf(buf, "1979-05-18-13.00.16.%06.6d", i );
        sprintf(buf, "%04.4d-%02.2d-%02.2d-%02.2d.%02.2d.%02.2d.%06.6d",
            ltime->tm_year+1900, ltime->tm_mon,
            ltime->tm_mday, ltime->tm_hour,
            ltime->tm_min, ltime->tm_sec, i);
	    frameset::frameset_time[i] = string(buf);
	}
}

static void setup_frameset_time_new () {

// fill in frameset_time array
    CHAR frameset_time[100];
    CHAR frameset_timeattr[200];
    
    const time_t thistime = time(0);
    struct tm *ltime = localtime( &thistime );
    frameset::frameset_time_cont.setComment("frameset time");
    
	for (int i=0; i < numElements; i++) {
        sprintf(frameset_time, "%04.4d-%02.2d-%02.2d-%02.2d.%02.2d.%02.2d.%06.6d",
            ltime->tm_year+1900, ltime->tm_mon,
            ltime->tm_mday, ltime->tm_hour,
            ltime->tm_min, ltime->tm_sec, i);
        sprintf(frameset_timeattr, "program:frameset:%s", frameset_time);
        LdasArray< CHAR > frameset_time_vector ( frameset_time, strlen(frameset_time), 
           frameset_timeattr );            
        frameset::frameset_time_cont.push_back( frameset_time_vector );
	}
}

static void setup_delta_time () {

// fill in delta_time array
	for (int i=0; i < numElements; i++) {
	    frameset::delta_time[i] = i+1 ;
	}
}

static void setup_description () {

// fill in description array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "description_%d", i % 10 );
	    frameset::description[i] = string(buf);
	}
}

static void setup_description_new () {

// fill in description array
 	CHAR description[100];
    CHAR descriptionattr[200];
    
    frameset::description_cont.setComment("description"); 
	for (int i=0; i < numElements; i++) {
	    sprintf(description, "desc_%d", i);
        sprintf(descriptionattr, "frameset:description:%s", description);
        LdasArray< CHAR > description_vector ( description, strlen(description), 
            descriptionattr );
        frameset::description_cont.push_back( description_vector );
	}
}

static void setup_program_id () {

// fill in program_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x'19990927201223201858000000'");
	    frameset::program_id[i] = string(buf);
	}
}

static void setup_program_id_new () {

// fill in program_id array
    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "frameset:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        frameset::program_id_cont.push_back( program_id_vector );        
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:frameset:file" );
	LdasContainer ilwd3( "framesetgroup:frameset:table" );
    
	setup_frameset_time_new();
	//ilwd3.push_back(  LdasString(frameset::frameset_time, (size_t) numElements,
	//string("framesetgroup:frameset:frameset_time"), string("") ) );
    ilwd3.push_back( frameset::frameset_time_cont );
    
	setup_delta_time();
	ilwd3.push_back(  LdasArray< INT_4S >(frameset::delta_time, numElements, 
	"framesetgroup:frameset:delta_time" ) );

	setup_description_new();
	//ilwd3.push_back(  LdasString(frameset::description, (size_t) numElements,
	//string("framesetgroup:frameset:description"), string("") ) );
    ilwd3.push_back( frameset::description_cont );

	setup_program_id_new();
    //ilwd3.push_back(  LdasString(frameset::program_id, (size_t) numElements,
	//string("framesetgroup:frameset:program_id"), string("") ) );
    ilwd3.push_back( frameset::program_id_cont );

 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
