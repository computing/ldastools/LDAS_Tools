#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace src_ind_key {

REAL_8	start_time[ARRAYSIZE];
string	ifo_site[ARRAYSIZE];
string	filter_type[ARRAYSIZE];
REAL_4	signal_to_noise[ARRAYSIZE];
string	src_ind_id[ARRAYSIZE];
string	event_id[ARRAYSIZE];
string	program_id[ARRAYSIZE];
} // end namespace

static void setup_start_time () {

// fill in start_time array
	for (int i=0; i < numElements; i++) {
	    src_ind_key::start_time[i] = i ;
	}
}

static void setup_ifo_site () {

// fill in ifo_site array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "Caltech_40m");
	    src_ind_key::ifo_site[i] = string(buf);
	}
}

static void setup_filter_type () {

// fill in filter_type array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "filter_type_%d", i);
	    src_ind_key::filter_type[i] = string(buf);
	}
}

static void setup_signal_to_noise () {

// fill in signal_to_noise array
	for (int i=0; i < numElements; i++) {
	    src_ind_key::signal_to_noise[i] = i ;
	}
}

static void setup_src_ind_id () {

    // fill in src_ind_id array, same as in src_ind_param
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x'19991004233838428500000000'");
	    src_ind_key::src_ind_id[i] = string(buf);
	}    
}

static void setup_event_id () {

// fill in event_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "event_id_%d", i);
	    src_ind_key::event_id[i] = string(buf);
	}
}

static void setup_program_id () {

// fill in program_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x'19991004234112029738000000'");
	    src_ind_key::program_id[i] = string(buf);
	}
}

main(int argc, char **argv) { 	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:src_ind_key:file" );
	LdasContainer ilwd3( "src_ind_keygroup:src_ind_key" );
	setup_start_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( src_ind_key::start_time, numElements,
 	"src_ind_keygroup:src_ind_key:start_time" ) );

	setup_ifo_site();
	ilwd3.push_back(  LdasString(src_ind_key::ifo_site, (size_t) numElements,
	string("src_ind_keygroup:src_ind_key:ifo_site"), string("") ) );

	setup_filter_type();
	ilwd3.push_back(  LdasString(src_ind_key::filter_type, (size_t) numElements,
	string("src_ind_keygroup:src_ind_key:filter_type"), string("") ) );

	setup_signal_to_noise();
	ilwd3.push_back(  LdasArray< REAL_4 >( src_ind_key::signal_to_noise, numElements,
 	"src_ind_keygroup:src_ind_key:signal_to_noise" ) );

	setup_src_ind_id();
	ilwd3.push_back(  LdasString(src_ind_key::src_ind_id, (size_t) numElements,
	string("src_ind_keygroup:src_ind_key:src_ind_id"), string("") ) );

	//setup_event_id();
	//ilwd3.push_back(  LdasString(src_ind_key::event_id, (size_t) numElements,
	//string("src_ind_keygroup:src_ind_key:event_id"), string("") ) );

	setup_program_id();
	ilwd3.push_back(  LdasString(src_ind_key::program_id, (size_t) numElements,
	string("src_ind_keygroup:src_ind_key:program_id"), string("") ) );


 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
