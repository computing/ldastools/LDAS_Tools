#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

extern	LdasContainer ilwd3datasource;
extern	LdasContainer ilwd3filter;
extern	LdasContainer ilwd3fpfft;
extern	LdasContainer ilwd3ppgeneric;
extern	LdasContainer ilwd3transformeddata;
extern	LdasContainer ilwd3gdstriggers;


extern	void	setup_datasource () ;
extern	void	setup_filter () ;
extern	void	setup_fpfft () ;
extern	void	setup_ppgeneric () ;
extern	void	setup_transformeddata () ;
extern	void	setup_gdstriggers () ;

main(int argc, char **argv) {

	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:gds:file" );
	LdasContainer ilwd3( "gdsgroup:gdstriggers" );
	setup_datasource();
	ilwd1.push_back( ilwd3datasource );

	setup_filter();
	ilwd1.push_back( ilwd3filter );

	setup_fpfft();
	ilwd1.push_back( ilwd3fpfft );

	setup_ppgeneric();
	ilwd1.push_back( ilwd3ppgeneric );

	setup_transformeddata();
	ilwd1.push_back( ilwd3transformeddata );

	setup_gdstriggers();
	ilwd1.push_back( ilwd3gdstriggers );

	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
