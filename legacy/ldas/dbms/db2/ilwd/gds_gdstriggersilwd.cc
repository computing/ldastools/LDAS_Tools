#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3gdstriggers( "gdsgroup:gdstriggers" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;
    
    LdasContainer   name_cont ( "gdsgroup:gdstriggers:name" );
    LdasContainer   node_address_cont ( "gdsgroup:gdstriggers:node_address" );
    LdasContainer   generation_time_cont ( "gdsgroup:gdstriggers:generation_time" );
    LdasContainer   program_id_cont ( "gdsgroup:gdstriggers:program_id" );
    
	static string	name[ARRAYSIZE];
	static INT_4S	sub_type[ARRAYSIZE];
	static INT_4S	priority[ARRAYSIZE];
	static string	node_address[ARRAYSIZE];
	static string	generation_time[ARRAYSIZE];
	static INT_4S	offset[ARRAYSIZE];
	static INT_4S	disposition[ARRAYSIZE];
	static CHAR_U	result[ARRAYSIZE];
	static string	datasource_id[ARRAYSIZE];
	static string	filter_id[ARRAYSIZE];
	static string	transformeddata_id[ARRAYSIZE];
	static string	program_id[ARRAYSIZE];
    
    int thistime = (int)time (0) ;
    
} // namespace 

static void setup_name () {

// fill in name array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "sample_trigger_%d", i);
	    name[i] = string(buf);
	}
}

static void setup_name_new () {

// fill in name array
	CHAR name[100];
    CHAR nameattr[200];
    
    name_cont.setComment("gdstriggers name");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(name, "trigger_%d", i + thistime );
        sprintf(nameattr, "name:%s", name);
        LdasArray< CHAR > name_vector ( name, strlen(name), nameattr);
    // add to name container  
        name_cont.push_back( name_vector );
    }
}

static void setup_sub_type () {

// fill in sub_type array
	for (int i=0; i < numElements; i++) {
	    sub_type[i] = i+1 ;
	}
}

static void setup_priority () {

// fill in priority array
	for (int i=0; i < numElements; i++) {
	    priority[i] = i+1 ;
	}
}

static void setup_node_address () {

// fill in node_address array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "111.111.111.%d", i);
	    node_address[i] = string(buf);
	}
}

static void setup_node_address_new () {

// fill in name array
	CHAR node_address[100];
    CHAR node_addressattr[200];
    
    node_address_cont.setComment("node address");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(node_address, "111.123.456.%d", i );
        sprintf(node_addressattr, "node_address:%s", node_address);
        LdasArray< CHAR > node_address_vector ( node_address, strlen(node_address), node_addressattr);
    // add to name container  
        node_address_cont.push_back( node_address_vector );
    }
}

static void setup_generation_time () {

// fill in generation_time array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "1999-01-01-13.20.05.%06.6d", i);
	    generation_time[i] = string(buf);
	}
}

static void setup_generation_time_new () {

// fill in name array
	CHAR generation_time[100];
    CHAR generation_timeattr[200];
    
    generation_time_cont.setComment("generation time");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(generation_time, "1999-12-01-13.20.05.%06.6d", i );
        sprintf(generation_timeattr, "generation_time:%s", generation_time);
        LdasArray< CHAR > generation_time_vector ( generation_time, strlen(generation_time), 
        generation_timeattr);
    // add to name container  
        generation_time_cont.push_back( generation_time_vector );
    }
}

static void setup_offset () {

// fill in offset array
	for (int i=0; i < numElements; i++) {
	    offset[i] = i+1 ;
	}
}

static void setup_disposition () {

// fill in disposition array
	for (int i=0; i < numElements; i++) {
	    disposition[i] = i+1 ;
	}
}

static void setup_result () {

// fill in result array
	for (int i=0; i < numElements; i++) {
	    result[i] = i+1 ;
	}
}

static void setup_datasource_id () {

// datasource_id is a uniqueId field, can be filled in by LDAS.
}

static void setup_filter_id () {

// filter_id is a uniqueId field, can be filled in by LDAS.
}

static void setup_transformeddata_id () {

// transformeddata_id is a uniqueId field, can be filled in by LDAS.
}

static void setup_program_id () {

// program_id is a uniqueId field, can be filled in by LDAS.
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x'19990927201223201858000000'");
	    program_id[i] = string(buf);
	}
}

static void setup_program_id_new () {

    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "gdstriggers:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        program_id_cont.push_back( program_id_vector );        
	}

}

void setup_gdstriggers () {

	setup_name_new();
	//ilwd3gdstriggers.push_back(  LdasString(name, (size_t) numElements,
	//string("gdsgroup:gdstriggers:name"), string("") ) );
    ilwd3gdstriggers.push_back(  name_cont );

	setup_sub_type();
	ilwd3gdstriggers.push_back(  LdasArray< INT_4S >(sub_type, numElements,
	"gdsgroup:gdstriggers:sub_type" ) );

	setup_priority();
	ilwd3gdstriggers.push_back(  LdasArray< INT_4S >(priority, numElements,
	"gdsgroup:gdstriggers:priority" ) );

	setup_node_address_new();
	//ilwd3gdstriggers.push_back(  LdasString(node_address, (size_t) numElements,
	//string("gdsgroup:gdstriggers:node_address"), string("") ) );
    ilwd3gdstriggers.push_back(  node_address_cont );

	setup_generation_time_new();
	//ilwd3gdstriggers.push_back(  LdasString(generation_time, (size_t) numElements,
	//string("gdsgroup:gdstriggers:generation_time"), string("") ) );
    ilwd3gdstriggers.push_back(  generation_time_cont );

	setup_offset();
	ilwd3gdstriggers.push_back(  LdasArray< INT_4S >(offset, numElements,
	"gdsgroup:gdstriggers:offset" ) );

	setup_disposition();
	ilwd3gdstriggers.push_back(  LdasArray< INT_4S >(disposition, numElements,
	"gdsgroup:gdstriggers:disposition" ) );
    
	setup_program_id_new();
	//ilwd3gdstriggers.push_back(  LdasString( program_id, (size_t) numElements,
	//string("gdsgroup:gdstriggers:program_id"), string("") ) );
    ilwd3gdstriggers.push_back(  program_id_cont );
    
	setup_result();
	// ilwd3gdstriggers.push_back(  LdasArray< CHAR_U >(result, numElements,
	//"gdsgroup:gdstriggers:result" ) );

	//datasource_id can be filled in by LDAS
	//filter_id can be filled in by LDAS
	//transformeddata_id can be filled in by LDAS
	//program_id can be filled in by LDAS

}
