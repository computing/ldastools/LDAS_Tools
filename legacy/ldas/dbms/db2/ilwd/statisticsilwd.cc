#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace statistics {
LdasContainer	channel_id_cont ( "statisticsgroup:statistics:channel_id" );
LdasContainer	frameset_time_cont ( "statisticsgroup:statistics:frameset_time" );

string	channel_id[ARRAYSIZE];
string	frameset_time[ARRAYSIZE];
REAL_8	min_value[ARRAYSIZE];
REAL_8	max_value[ARRAYSIZE];
REAL_8	min_delta[ARRAYSIZE];
REAL_8	max_delta[ARRAYSIZE];
REAL_8	min_deltadelta[ARRAYSIZE];
REAL_8	max_deltadelta[ARRAYSIZE];
REAL_8	sum1[ARRAYSIZE];
REAL_8	sum2[ARRAYSIZE];
REAL_8	sum3[ARRAYSIZE];
REAL_8	sum4[ARRAYSIZE];
} // end namespace

int thistime = (int)time (0) ;

static void setup_channel_id () {

// fill in channel_id array
// unique primary key
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "C%d", thistime + i);
	    statistics::channel_id[i] = string(buf);
	}
}

static void setup_channel_id_new () {

// fill in name array
	CHAR channel_id[100];
    CHAR channel_idattr[200];
    
    statistics::channel_id_cont.setComment("channel id");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(channel_id, "C%d", i + thistime );
        sprintf(channel_idattr, "channel:%s", channel_id);
        LdasArray< CHAR > channel_id_vector ( channel_id, strlen(channel_id), channel_idattr);
    // add to name container  
        statistics::channel_id_cont.push_back( channel_id_vector );
    }
}

static void setup_frameset_time () {

// fill in frameset_time array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "1979-05-18-13.00.16.000000");
	    statistics::frameset_time[i] = string(buf);
	}
}

static void setup_frameset_time_new () {

// fill in frameset_time array
    CHAR frameset_time[] = "1999-10-24-10.18.46.000096";
    CHAR frameset_timeattr[200];
    
    statistics::frameset_time_cont.setComment("frameset time");
    
	for (int i=0; i < numElements; i++) {
        sprintf(frameset_timeattr, "statistics:frameset_time:%s", frameset_time);
        LdasArray< CHAR > frameset_time_vector ( frameset_time, strlen(frameset_time), 
           frameset_timeattr );            
        statistics::frameset_time_cont.push_back( frameset_time_vector );
	}
}

static void setup_min_value () {

// fill in min_value array
	for (int i=0; i < numElements; i++) {
	    statistics::min_value[i] = i+1 ;
	}
}

static void setup_max_value () {

// fill in max_value array
	for (int i=0; i < numElements; i++) {
	    statistics::max_value[i] = i+1 ;
	}
}

static void setup_min_delta () {

// fill in min_delta array
	for (int i=0; i < numElements; i++) {
	    statistics::min_delta[i] = i+1 ;
	}
}

static void setup_max_delta () {

// fill in max_delta array
	for (int i=0; i < numElements; i++) {
	    statistics::max_delta[i] = i+1 ;
	}
}

static void setup_min_deltadelta () {

// fill in min_deltadelta array
	for (int i=0; i < numElements; i++) {
	    statistics::min_deltadelta[i] = i+1 ;
	}
}

static void setup_max_deltadelta () {

// fill in max_deltadelta array
	for (int i=0; i < numElements; i++) {
	    statistics::max_deltadelta[i] = i+1 ;
	}
}

static void setup_sum1 () {

// fill in sum1 array
	for (int i=0; i < numElements; i++) {
	    statistics::sum1[i] = i+1 ;
	}
}

static void setup_sum2 () {

// fill in sum2 array
	for (int i=0; i < numElements; i++) {
	    statistics::sum2[i] = i+1 ;
	}
}

static void setup_sum3 () {

// fill in sum3 array
	for (int i=0; i < numElements; i++) {
	    statistics::sum3[i] = i+1 ;
	}
}

static void setup_sum4 () {

// fill in sum4 array
	for (int i=0; i < numElements; i++) {
	    statistics::sum4[i] = i+1 ;
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:statistics:file" );
	LdasContainer ilwd3( "statisticsgroup:statistics" );
	setup_channel_id_new();
	//ilwd3.push_back(  LdasString(statistics::channel_id, (size_t) numElements,
	//string("statisticsgroup:statistics:channel_id"), string("") ) );
    ilwd3.push_back( statistics::channel_id_cont );

	setup_frameset_time_new();
	//ilwd3.push_back(  LdasString(statistics::frameset_time, (size_t) numElements,
	//string("statisticsgroup:statistics:frameset_time"), string("") ) );
    ilwd3.push_back( statistics::frameset_time_cont );
    
	setup_min_value();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::min_value, numElements,
 	"statisticsgroup:statistics:min_value" ) );

	setup_max_value();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::max_value, numElements,
 	"statisticsgroup:statistics:max_value" ) );

	setup_min_delta();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::min_delta, numElements,
 	"statisticsgroup:statistics:min_delta" ) );

	setup_max_delta();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::max_delta, numElements,
 	"statisticsgroup:statistics:max_delta" ) );

	setup_min_deltadelta();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::min_deltadelta, numElements,
 	"statisticsgroup:statistics:min_deltadelta" ) );

	setup_max_deltadelta();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::max_deltadelta, numElements,
 	"statisticsgroup:statistics:max_deltadelta" ) );

	setup_sum1();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::sum1, numElements,
 	"statisticsgroup:statistics:sum1" ) );

	setup_sum2();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::sum2, numElements,
 	"statisticsgroup:statistics:sum2" ) );

	setup_sum3();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::sum3, numElements,
 	"statisticsgroup:statistics:sum3" ) );

	setup_sum4();
	ilwd3.push_back(  LdasArray< REAL_8 >( statistics::sum4, numElements,
 	"statisticsgroup:statistics:sum4" ) );


 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
