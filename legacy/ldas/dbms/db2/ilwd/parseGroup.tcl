#! /ldcg/bin/tclsh

#set datasource(uniqueId)    { datasource_id }
#set fpgeneric(uniqueId)     { filterparam_id }
#set fpfft(uniqueId)         { filterparam_id }
#set filter(uniqueId)        { filter_id }
#set transformeddata(uniqueId)  { transformeddata_id }
#set program(uniqueId)       { program_id }
#set ppgeneric(uniqueId)     { programparam_id }

#set frameset(uniqueId)      { program_id }
#set burstevent(uniqueId)    { program_id }
#set ringdown(uniqueId)      { program_id event_id binaryinspiral_id }
#set sourceindependent(uniqueId) program_id
#set directedperiodic(uniqueId) { program_id event_id binaryinspiral_id }
#set gdstriggers(uniqueId)   { program_id }

;#   unique Id Cols - column names not provided in ilwd data
;#   list of column names
;#   get value of each column from unique Id
;#   
#set datasource(uniqueIdCols)  { datasource_id }
#set filter(uniqueIdCols)      { filterparam_id filter_id }
#set fpfft(uniqueIdCols)       { filterparam_id }
#set fpgeneric(uniqueIdCols)   { filterparam_id }
#set transformeddata(uniqueIdCols) { transformeddata_id }
#set ppgeneric(uniqueIdCols)   { programparam_id }
#set program(uniqueIdCols)     { program_id }
#set gdstriggers(uniqueIdCols) { datasource_id filter_id  \
                              transformeddata_id } 
                              
proc isUniqueId { dname } {
    set rc1 [ lsearch $::uniqueIds $dname ]
    set rc2 [ lsearch $::uniqueIdCols $dname ]
    return [ expr $rc1 + $rc2 ]
}

proc setUniqueId { line dname } {
    if  { [ regexp {CHAR\(13\) FOR BIT DATA NOT NULL UNIQUE} \
        $line ] } {
        lappend ::uniqueIds $dname 
    } elseif { [ regexp {CHAR\(13\) FOR BIT DATA} $line ] } {
        lappend ::uniqueIdCols $dname 
    }
}

proc out_name {} {
    uplevel #0 {
        switch -- [set ${dname}(dtype)] {
    
        string    { puts $fout "\tilwd3${table}.push_back(  LdasString($dname, (size_t) numElements,"; 
                    puts $fout "\tstring(\"${group}group:$table:$dname\"), string(\"\") ) );\n" }
        REAL_8    { puts $fout "\tilwd3${table}.push_back(  LdasArray< REAL_8 >( $dname, numElements,";
                    puts $fout "\t\"${group}group:$table:$dname\" ) );\n"                    }
        REAL_4    { puts $fout "\tilwd3${table}.push_back(  LdasArray< REAL_4 >( $dname, numElements,";
                    puts $fout "\t\"${group}group:$table:$dname\" ) );\n"                    }              
        INT_4S    { puts $fout "\tilwd3${table}.push_back(  LdasArray< INT_4S >($dname, numElements,"; 
                    puts $fout "\t\"${group}group:$table:$dname\" ) );\n"                    } 
        INT_2S    { puts $fout "\tilwd3${table}.push_back(  LdasArray< INT_2S >($dname, numElements,"; 
                    puts $fout "\t\"${group}group:$table:$dname\" ) );\n"                    }
        CHAR_U    { puts $fout "\t// ilwd3${table}.push_back(  LdasArray< CHAR_U >($dname, numElements,"; 
                    puts $fout "\t//\"${group}group:$table:$dname\" ) );\n"                    }                                
        default   { puts "type = [set ${dname}(dtype)] " }
        }
    }
}


proc set_ilwd_type {} {

    uplevel #0 {

        switch -- $dtype {
        
        BLOB    {  array set $dname { dtype CHAR_U } } 
        TIMESTAMP -
        CHAR    {  array set $dname { dtype string } } 
        DOUBLE  {  array set $dname { dtype REAL_8 } }
        FLOAT   - 
        REAL    {  array set $dname { dtype REAL_4 } } 
        INTEGER {  array set $dname { dtype INT_4S } }
        SMALLINT { array set $dname { dtype INT_2S } }
        default { puts "unknown type = $dtype" }
        
        }
    }
}

proc out_func {} {
    uplevel #0 {
        switch -- [ set ${dname}(dtype) ] {
        
            string { puts $fout  "\tchar buf\[100\];"
                     puts $fout  "\tfor (int i=0; i < numElements; i++) \{"
                     puts $fout  "\t    sprintf(buf, \"${dname}_%d\", i);" 
                     puts $fout  "\t    ${dname}\[i\] = string(buf);\n\t\}"                         
                  }
            default { puts $fout "\tfor (int i=0; i < numElements; i++) \{"
                      puts $fout "\t    ${dname}\[i\] = i + 1;"
                      puts $fout "\t\}"
                  }

        }
    }
}

set tables {} 
set heading1 "#include <string.h>\n\#include <stdio.h>\n\#include <ilwd/ldascontainer.hh>\n\#include <ilwd/ldasarray.hh>\n\#include <ilwd/ldasstring.hh>\n\#include<ilwd/util.hh>" 
set heading2 "using ILwd::LdasContainer;\n\using ILwd::LdasArray;\n\using ILwd::LdasString;"
set heading3 "extern int numElements;"


set group [ lindex $argv 0 ]
puts "argc=$argc, [lindex $argv 1 ]"
if  { $argc == 2 } {
    set filelist [ lindex $argv 1 ]
    set state ONE
} else {
    set filelist { gds.datasource.sql gds.fpfft.new.sql \
    gds.filter.sql gds.ppgeneric.new.sql  \            
    gds.transformeddata.sql gds.triggers.sql }
    set state ALL
}
puts "filelist=$filelist"

foreach fentry $filelist {
 set fd [ open $fentry r ]
 puts "SQL file=$fentry"
 set x [ read $fd ]
 set x [ split $x \n ]
 set dtypes "REAL|DOUBLE|INTEGER|SMALLINT|CHAR|TIMESTAMP|BLOB" 
 
 set namelist {}
 set uniqueIds {}
 set uniqueIdCols {}
 
 foreach line $x {
    regsub -all {[ \t]+} $line " " line
    if  { [ regexp -nocase {^[-]+} $line ] } {
        continue
    }
    if  { [ regexp -nocase $dtypes $line match  ] } {
        if  { [ info exist table ] } {
            set dname [lindex $line 0 ]
            set dtype $match
            lappend namelist $dname
            set_ilwd_type 
            setUniqueId $line $dname
        }
    } elseif { [ regexp -nocase {TABLE[ \t]+[A-Z_]+} $line match table ] } {
        set table [ lindex $match 1 ]
        set table [ string tolower $table ]
    }
}

# output stub
set output "${group}_${table}ilwd.cc"
if  { [ file exists $output ] } {
    catch { file rename $output  "${output}.old" }
}
set fout [ open $output "w" ]

set heading5 "LdasContainer ilwd3${table}( \"${group}group:${table}\" );"
set heading  "$heading1\n\n$heading2\n\n$heading3\n$heading5\n"

puts $fout $heading

# output namespace
puts $fout "namespace \{\n\
\tconst INT_2U ARRAYSIZE = 10000;\n"

foreach dname $namelist {
    puts $fout "\tstatic [set ${dname}(dtype)]\t$dname\[ARRAYSIZE\];"
}

# end namespace
puts $fout "\} // namespace "

puts "uniqueId=$uniqueIds"
puts "uniqueIdCols=$uniqueIdCols"

foreach dname $namelist {
    ;## the function stubs for user to fill in array data
    puts $fout "\nstatic void setup_${dname} () \{\n"
    
    if  { [ isUniqueId $dname ] == -2 } {
        puts $fout "// fill in $dname array"
        out_func
    } else {
        puts $fout "// $dname is a uniqueId field, can be filled in by LDAS." 
    }
    puts $fout "\}"
}

puts $fout "\nvoid setup_${table} () \{\n"

foreach dname $namelist {
    if  { [ isUniqueId $dname ] == -2 } {
        puts $fout "\tsetup_${dname}();"
        out_name 
    } else {
        puts $fout "\t//$dname can be filled in by LDAS"
    }        
}

lappend tables $table

puts $fout "\n\}"

close $fout 
puts "created $output"
}

if  { $state == "ONE" } {
    exit
}
;## write main
set output "${group}_mainilwd.cc" 
set fout [ open $output "w" ]
set heading1 "#include <string.h>\n\#include <stdio.h>\n\#include <ilwd/ldascontainer.hh>\n\#include <ilwd/ldasarray.hh>\n\#include <ilwd/ldasstring.hh>\n\#include<ilwd/util.hh>" 
set heading2 "using ILwd::LdasContainer;\n\using ILwd::LdasArray;\n\using ILwd::LdasString;\n"
set heading3 "const INT_2U ARRAYSIZE = 10000;"
set heading4 "int numElements;"
set heading  "$heading1\n\n$heading2\n\n$heading3\n$heading4\n"

puts $fout $heading

foreach table $tables {
    puts $fout "extern\tLdasContainer ilwd3${table};" 
}
puts $fout "\n"

foreach table $tables {
    puts $fout "extern\tvoid\tsetup_${table} () ;" 
}

puts $fout "\nmain(int argc, char **argv) \{\n"
puts $fout "\tnumElements = atoi(argv\[1\]);"

puts $fout "\tLdasContainer ilwd1( \"ligo:$group:file\" );"
puts $fout "\tLdasContainer ilwd3( \"${group}group:$table\" );"

foreach table $tables {
    puts $fout "\tsetup_${table}();" 
    puts $fout "\tilwd1.push_back( ilwd3${table} );\n" 
}

set ending "\tILwd::writeHeader(cout);\n\
\tilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );\n\
\tcout << endl;\n\}"

puts $fout $ending
close $fout
puts "created $output"
