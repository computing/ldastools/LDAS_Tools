#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3ppgeneric( "gdsgroup:ppgeneric" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;
    LdasContainer   programparam_id_cont ( "gdsgroup:ppgeneric:programparam_id" );
    LdasContainer   name_cont ( "gdsgroup:ppgeneric:name" );
    LdasContainer   type_cont ( "gdsgroup:ppgeneric:type" ); 
    LdasContainer   value_cont ( "gdsgroup:ppgeneric:value" ); 
      
	static string	programparam_id[ARRAYSIZE];
	static string	name[ARRAYSIZE];
	static string	type[ARRAYSIZE];
	static string	value[ARRAYSIZE];
    int thistime = (int)time (0) ;

    
} // namespace 

static void setup_programparam_id () {

	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x'19990927201223201858000000'");
	    programparam_id[i] = string(buf);
	}
}

static void setup_programparam_id_new () {

    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "ppgeneric:program_id:%s", program_id_str);
        LdasArray< CHAR_U > programparam_id_vector ( program_id, 13,
            program_idattr );
        programparam_id_cont.push_back( programparam_id_vector );        
	}

}

static void setup_name () {

// fill in name array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name_%d", i);
	    name[i] = string(buf);
	}
}

static void setup_name_new () {

// fill in name array
	CHAR name[100];
    CHAR nameattr[200];
    
    name_cont.setComment("ppgeneric name");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(name, "PP_%d", i + thistime );
        sprintf(nameattr, "name:%s", name);
        LdasArray< CHAR > name_vector ( name, strlen(name), nameattr);
    // add to name container  
        name_cont.push_back( name_vector );
    }
}

static void setup_type () {

// fill in type array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "double");
	    type[i] = string(buf);
	}
}

static void setup_type_new () {

// fill in name array
	CHAR type[100];
    CHAR typeattr[200];
    
    type_cont.setComment("ppgeneric type");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(type, "type_%d", i );
        sprintf(typeattr, "type:%s", type);
        LdasArray< CHAR > type_vector ( type, strlen(type), typeattr);
    // add to name container  
        type_cont.push_back( type_vector );
    }
}

static void setup_value () {

// fill in array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "%2.2f", (float)i);
	    value[i] = string(buf);
	}
}

static void setup_value_new () {

// fill in array
	CHAR value[100];
    CHAR valueattr[200];
    
    value_cont.setComment("ppgeneric value");
    for ( int i = 0; i < numElements ; i++ ) {
        sprintf(value, "value_%d", i );
        sprintf(valueattr, "value:%s", value);
        LdasArray< CHAR > value_vector ( value, strlen(value), valueattr);
    // add to name container  
        value_cont.push_back( value_vector );
    }
}

void setup_ppgeneric () {

	//programparam_id can be filled in by LDAS
    setup_programparam_id_new();
	//ilwd3ppgeneric.push_back(  LdasString( programparam_id, (size_t) numElements,
	//string("gdsgroup:ppgeneric:programparam_id"), string("") ) );
    ilwd3ppgeneric.push_back(  programparam_id_cont );
    
	setup_name_new();
	//ilwd3ppgeneric.push_back(  LdasString(name, (size_t) numElements,
	//string("gdsgroup:ppgeneric:name"), string("") ) );
    ilwd3ppgeneric.push_back(  name_cont );
    
	setup_type_new();
	//ilwd3ppgeneric.push_back(  LdasString(type, (size_t) numElements,
	//string("gdsgroup:ppgeneric:type"), string("") ) );
    ilwd3ppgeneric.push_back(  type_cont );

	setup_value_new();
	//ilwd3ppgeneric.push_back(  LdasString(value, (size_t) numElements,
	//string("gdsgroup:ppgeneric:value"), string("") ) );
    ilwd3ppgeneric.push_back(  value_cont );
}
