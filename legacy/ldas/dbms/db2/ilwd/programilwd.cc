#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace program {
LdasContainer   name_cont ( "programgroup:program:name" );
LdasContainer   version_cont ( "programgroup:program:version" );
LdasContainer   cvs_repository_cont ( "programgroup:program:cvs_repository" );
LdasContainer	entry_time_cont ( "programgroup:program:entry_time" );
LdasContainer	comment_cont ( "programgroup:program:comment" );
LdasContainer	program_id_cont ( "programgroup:program:program_id" );

string	name[ARRAYSIZE];
string	version[ARRAYSIZE];
string	cvs_repository[ARRAYSIZE];
string	entry_time[ARRAYSIZE];
string	comment[ARRAYSIZE];
string	program_id[ARRAYSIZE];
} // end namespace

int thistime = (int)time (0) ;
int id = 0;

static void setup_name_new () {

// fill in name array
	CHAR name[100];
    CHAR nameattr[200];
    
    program::name_cont.setComment("program name");
    for ( int i = 0; i < numElements ; i++ ) {
        if  ( !id ) 
        {
            sprintf(name, "P_%d", i + thistime );
        } else {
            strcpy (name, "pgmname_test");
        }
        sprintf(nameattr, "name:%s", name);
        LdasArray< CHAR > name_vector ( name, strlen(name), nameattr);
    // add to name container  
        program::name_cont.push_back( name_vector );
    }
}
static void setup_name () {

// fill in name array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "P_%d", i + thistime );
	    program::name[i] = string(buf);
	}
}

static void setup_version_new () {

// fill in version array
	CHAR version[100];
    CHAR versionattr[200];
    
    program::name_cont.setComment("program version");
	for (int i=0; i < numElements; i++) {
	    sprintf(version, "1.0.%d", i);
        sprintf(versionattr, "version:%s", version);
        LdasArray< CHAR > version_vector ( version, strlen(version), 
            versionattr );
        program::version_cont.push_back( version_vector );
	}
}

static void setup_version () {

// fill in version array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "1.0.%d", i);
	    program::version[i] = string(buf);
	}
}

static void setup_cvs_repository () {

// fill in cvs_repository array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "/ldcg/test-0.0.%d", i);
	    program::cvs_repository[i] = string(buf);
	}
}

static void setup_entry_time () {

// fill in entry_time array
	char buf[100];

	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "1979-05-18-13.04.01.%06.6d", i );
	    program::entry_time[i] = string(buf);
	}
}

static void setup_comment () {

// fill in comment array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "%d parameters required", i);
	    program::comment[i] = string(buf);
	}
}

static void setup_program_id () {

// fill in program_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "program_id_%d", i);
	    program::program_id[i] = string(buf);
	}
}

static void setup_cvs_repository_new () {

// fill in cvs_repository array
	CHAR cvs_repository[100];
    CHAR cvs_repositoryattr[200];
    
    program::cvs_repository_cont.setComment("program cvs_repository");
    
	for (int i=0; i < numElements; i++) {
	    sprintf(cvs_repository, "/ldcg/test-0.0.%d", i);
        sprintf(cvs_repositoryattr, "program:cvs_repository:%s", cvs_repository);
        LdasArray< CHAR > cvs_repository_vector ( cvs_repository, strlen(cvs_repository));
        //    cvs_repository );
        program::cvs_repository_cont.push_back( cvs_repository_vector );
	}
}

static void setup_entry_time_new () {

// fill in entry_time array
	CHAR entry_time[100];
    CHAR entry_timeattr[200];
    program::entry_time_cont.setComment("program entry_time");
    
	for (int i=0; i < numElements; i++) {
	    sprintf(entry_time, "1979-05-18-13.04.01.%06.6d", i );
        sprintf(entry_timeattr, "program:entry_time:%s", entry_time );
        LdasArray< CHAR > entry_time_vector ( entry_time, strlen(entry_time),
            entry_time );
        program::entry_time_cont.push_back( entry_time_vector );
	}
}

static void setup_comment_new () {

// fill in comment array
	CHAR comment[100];
    CHAR commentattr[200];

	for (int i=0; i < numElements; i++) {
	    sprintf(comment, "%d parameters required", i );
        sprintf(commentattr, "program:comment:%s", comment);
        LdasArray< CHAR > comment_vector ( comment, strlen(comment));
        //    comment );    
        program::comment_cont.push_back( comment_vector );
	}
}

static void setup_program_id_new () {

// fill in program_id array
    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "program:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        program::program_id_cont.push_back( program_id_vector );        
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:program:file" );
	LdasContainer ilwd3( "programgroup:program:table" );
    if  ( ! strcmp (argv[2], "id" ) ) 
    {
        id = 1 ;
    }
	setup_name_new();
	//ilwd3.push_back(  LdasString(program::name, (size_t) numElements,
	//  string("programgroup:program:name"), string("") ) );
    ilwd3.push_back(  program::name_cont );
    
	setup_version_new()
	//ilwd3.push_back(  LdasString(program::version, (size_t) numElements,
	//  string("programgroup:program:version"), string("") ) );
    ilwd3.push_back(  program::version_cont );
    
	setup_cvs_repository_new();
	//ilwd3.push_back(  LdasString(program::cvs_repository, (size_t) numElements,
	// string("programgroup:program:cvs_repository"), string("") ) );
    ilwd3.push_back(  program::cvs_repository_cont );

	setup_entry_time_new();
	//ilwd3.push_back(  LdasString(program::entry_time, (size_t) numElements,
	//  string("programgroup:program:entry_time"), string("") ) );
	ilwd3.push_back(  program::entry_time_cont );

	setup_comment_new();
	//ilwd3.push_back(  LdasString(program::comment, (size_t) numElements,
	//  string("programgroup:program:comment"), string("") ) );
    ilwd3.push_back(  program::comment_cont );
    
    // program_id is automatically generated by tcl
    if  ( id )
    {
	    setup_program_id_new();
	//ilwd3.push_back(  LdasString(program::program_id, (size_t) numElements,
	//string("programgroup:program:program_id"), string("") ) );
        ilwd3.push_back(  program::program_id_cont );
    }

 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
