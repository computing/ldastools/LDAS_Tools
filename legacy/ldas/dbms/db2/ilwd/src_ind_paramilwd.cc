#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace src_ind_param {

string	src_ind_id[ARRAYSIZE];
string	name[ARRAYSIZE];
string	type[ARRAYSIZE];
string	value[ARRAYSIZE];
} // end namespace

static void setup_src_ind_id () {

// fill in src_ind_id array
// each one is unique

    src_ind_param::src_ind_id[0] = "x'19990928214813306794000000'";
    src_ind_param::src_ind_id[1] = "x'19990928214813526748000000'";
    src_ind_param::src_ind_id[2] = "x'19990928214813528539000000'";
    src_ind_param::src_ind_id[3] = "x'19990928214813530281000000'";
    src_ind_param::src_ind_id[4] = "x'19990928214813532027000000'";
    src_ind_param::src_ind_id[5] = "x'19990928214813533771000000'";
    src_ind_param::src_ind_id[6] = "x'19990928214813535515000000'";
    src_ind_param::src_ind_id[7] = "x'19990928214813537257000000'";
    src_ind_param::src_ind_id[8] = "x'19990928214813539042000000'";
    src_ind_param::src_ind_id[9] = "x'19990928214813540789000000'";

}

static void setup_name () {

// fill in name array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name_%d", i);
	    src_ind_param::name[i] = string(buf);
	}
}

static void setup_type () {

// fill in type array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "REAL_4");
	    src_ind_param::type[i] = string(buf);
	}
}

static void setup_value () {

// fill in value array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "%1.1f", (float)i);
	    src_ind_param::value[i] = string(buf);
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:src_ind_param:file" );
	LdasContainer ilwd3( "src_ind_paramgroup:src_ind_param" );
	//setup_src_ind_id();
	//ilwd3.push_back(  LdasString(src_ind_param::src_ind_id, (size_t) numElements,
	//string("src_ind_paramgroup:src_ind_param:src_ind_id"), string("") ) );

	setup_name();
	ilwd3.push_back(  LdasString(src_ind_param::name, (size_t) numElements,
	string("src_ind_paramgroup:src_ind_param:name"), string("") ) );

	setup_type();
	ilwd3.push_back(  LdasString(src_ind_param::type, (size_t) numElements,
	string("src_ind_paramgroup:src_ind_param:type"), string("") ) );

	setup_value();
	ilwd3.push_back(  LdasString(src_ind_param::value, (size_t) numElements,
	string("src_ind_paramgroup:src_ind_param:value"), string("") ) );


 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
