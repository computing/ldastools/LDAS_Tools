#include <cstring>
#include <algorithm>
#include <iostream>
#include <climits>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3summ_spectrum( "summ_spectrumgroup:summ_spectrum:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer program_cont ( "summ_spectrumgroup:summ_spectrum:program" ); 
	LdasContainer process_id_cont ( "summ_spectrumgroup:summ_spectrum:process_id" ); 
	LdasContainer frameset_group_cont ( "summ_spectrumgroup:summ_spectrum:frameset_group" ); 
	LdasContainer segment_group_cont ( "summ_spectrumgroup:summ_spectrum:segment_group" ); 
	INT_4S	version[ARRAYSIZE];
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	INT_4S	frames_used[ARRAYSIZE];
	REAL_8	start_frequency[ARRAYSIZE];
	REAL_8	delta_frequency[ARRAYSIZE];
	LdasContainer mimetype_cont ( "summ_spectrumgroup:summ_spectrum:mimetype" ); 
	LdasContainer channel_cont ( "summ_spectrumgroup:summ_spectrum:channel" ); 
	LdasContainer spectrum_type_cont ( "summ_spectrumgroup:summ_spectrum:spectrum_type" ); 
	LdasContainer spectrum_cont ( "summ_spectrumgroup:summ_spectrum:spectrum" ); 
	INT_4S	spectrum_length[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"segment.h"
#include	"frameset.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"segment_symref.h"
#include	"frameset_symref.h"
#endif

using namespace local;

static void setup_program () {

// fill in program array
// NOT NULL
	CHAR program[16];
	CHAR programattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(program, (size_t) 16, "program_%d", i + thistime + 1 );
		sprintf(programattr, "summ_spectrum:%s", program);
		LdasArray< CHAR > program_vector ( program, strlen(program),
			"summ_spectrumgroup:summ_spectrum:program" );
		program_vector.setComment( programattr );
		program_cont.push_back( program_vector );
	}
}

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"summ_spectrumgroup:summ_spectrum:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"summ_spectrumgroup:summ_spectrum:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_frameset_group () {

// fill in frameset_group array
	CHAR frameset_group[48];
	CHAR frameset_groupattr[248];

	for (int i=0; i < numElements; i++) {
		strcpy(frameset_group, frameset[ i % frameset_len ].FRAMESET_GROUP);
		sprintf(frameset_groupattr, "summ_spectrum:%s", frameset_group);
		LdasArray< CHAR > frameset_group_vector ( frameset_group, strlen(frameset_group),
			"summ_spectrumgroup:summ_spectrum:frameset_group" );
		frameset_group_vector.setComment( frameset_groupattr );
		frameset_group_cont.push_back( frameset_group_vector );
	}
}

static void setup_segment_group () {

// fill in segment_group array
	CHAR segment_group[64];
	CHAR segment_groupattr[264];

	for (int i=0; i < numElements; i++) {
		strcpy(segment_group, segment[ i % segment_len ].SEGMENT_GROUP);
		sprintf(segment_groupattr, "summ_spectrum:%s", segment_group);
		LdasArray< CHAR > segment_group_vector ( segment_group, strlen(segment_group),
			"summ_spectrumgroup:summ_spectrum:segment_group" );
		segment_group_vector.setComment( segment_groupattr );
		segment_group_cont.push_back( segment_group_vector );
	}
}

static void setup_version () {

// fill in version array
	for (int i=0; i < numElements; i++) {
		local::version[i] = atol( segment[ i % segment_len ].VERSION_local);
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] =  (INT_4S) atol (segment[ i % segment_len ].START_TIME);
	}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] =  (INT_4S) atol(segment[ i % segment_len ].END_TIME);
	}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_frames_used () {

// fill in frames_used array
	for (int i=0; i < numElements; i++) {
		frames_used[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_start_frequency () {

// fill in start_frequency array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_frequency[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_delta_frequency () {

// fill in delta_frequency array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		delta_frequency[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_mimetype () {

// fill in mimetype array
// NOT NULL
	CHAR mimetype[64];
	CHAR mimetypeattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(mimetype, (size_t) 64, "mimetype_%d", i + thistime + 1 );
		sprintf(mimetypeattr, "summ_spectrum:%s", mimetype);
		LdasArray< CHAR > mimetype_vector ( mimetype, strlen(mimetype),
			"summ_spectrumgroup:summ_spectrum:mimetype" );
		mimetype_vector.setComment( mimetypeattr );
		mimetype_cont.push_back( mimetype_vector );
	}
}

static void setup_channel () {

// fill in channel array
// NOT NULL
	CHAR channel[240];
	CHAR channelattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 240, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "summ_spectrum:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"summ_spectrumgroup:summ_spectrum:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_spectrum_type () {

// fill in spectrum_type array
// NOT NULL
	CHAR spectrum_type[128];
	CHAR spectrum_typeattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(spectrum_type, (size_t) 128, "spectrum_type_%d", i + thistime + 1 );
		sprintf(spectrum_typeattr, "summ_spectrum:%s", spectrum_type);
		LdasArray< CHAR > spectrum_type_vector ( spectrum_type, strlen(spectrum_type),
			"summ_spectrumgroup:summ_spectrum:spectrum_type" );
		spectrum_type_vector.setComment( spectrum_typeattr );
		spectrum_type_cont.push_back( spectrum_type_vector );
	}
}

static void setup_spectrum () {

// fill in spectrum array
// NOT NULL
// fill in spectrum array
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}
	spectrum_cont.setComment("spectrum blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {
		sprintf (name, "%s", "spectrumgroup:summ_spectrum:spectrum");
		size_t size( ( i % 100 + 1 )  * BLOBSIZE );
		const CHAR_U fill_char( i%UCHAR_MAX );
		CHAR_U buffer[ size ];
       	fill( buffer, buffer + size, fill_char );
        try
		{
  		   LdasArray< CHAR_U >* char_u_vector = new 
		   	LdasArray< CHAR_U >( buffer, size, name, units );

                   // create variable size blobs
		   spectrum_cont.push_back( char_u_vector, 
           		LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		}
            catch( const std::exception& exc )
		{
		   cout << "Exception: " << exc.what() << endl;
               	   exit( 1 );
	  	}
           	catch( const FormatException& exc )
		{
           	const size_t exc_size( exc.getSize() );
           	cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		}
	}
	spectrum_cont.setWriteFormat(ILwd::BASE64);
	spectrum_cont.setWriteCompression( ILwd::GZIP0);
}

static void setup_spectrum_length () {

// fill in spectrum_length array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		spectrum_length[i] = (INT_4S) ( i + 1 ) * (BLOBSIZE/sizeof(BLOB_TYPE) );
	}
}

void setup_summ_spectrum ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_program();
	ilwd3summ_spectrum.push_back(  program_cont ) ;
	setup_process_id();
	ilwd3summ_spectrum.push_back( process_id_cont ) ;
	setup_frameset_group();
//	ilwd3summ_spectrum.push_back(  frameset_group_cont ) ;
	setup_segment_group();
	ilwd3summ_spectrum.push_back(  segment_group_cont ) ;
	setup_version();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(local::version, numElements,
	"summ_spectrumgroup:summ_spectrum:version" ) );
	setup_start_time();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"summ_spectrumgroup:summ_spectrum:start_time" ) );
	setup_start_time_ns();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"summ_spectrumgroup:summ_spectrum:start_time_ns" ) );
	setup_end_time();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"summ_spectrumgroup:summ_spectrum:end_time" ) );
	setup_end_time_ns();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"summ_spectrumgroup:summ_spectrum:end_time_ns" ) );
	setup_frames_used();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(frames_used, numElements,
	"summ_spectrumgroup:summ_spectrum:frames_used" ) );
	setup_start_frequency();
	ilwd3summ_spectrum.push_back(  LdasArray< REAL_8 >( start_frequency, numElements,
	"summ_spectrumgroup:summ_spectrum:start_frequency" ) );
	setup_delta_frequency();
	ilwd3summ_spectrum.push_back(  LdasArray< REAL_8 >( delta_frequency, numElements,
	"summ_spectrumgroup:summ_spectrum:delta_frequency" ) );
	setup_mimetype();
	ilwd3summ_spectrum.push_back(  mimetype_cont ) ;
	setup_channel();
	ilwd3summ_spectrum.push_back(  channel_cont ) ;
	setup_spectrum_type();
	ilwd3summ_spectrum.push_back(  spectrum_type_cont ) ;
	setup_spectrum();
	ilwd3summ_spectrum.push_back( spectrum_cont ) ;
	setup_spectrum_length();
	ilwd3summ_spectrum.push_back(  LdasArray< INT_4S >(spectrum_length, numElements,
	"summ_spectrumgroup:summ_spectrum:spectrum_length" ) );
}

#ifdef TESTMAIN 
 void setup_summ_spectrum( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_summ_spectrum( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3summ_spectrum );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
