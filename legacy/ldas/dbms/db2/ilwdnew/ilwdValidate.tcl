#! /ldcg/bin/tclsh

## ******************************************************** 
## putValidate.tcl
##
#! /ldcg/bin/tclsh

## Provides data retrieval functions for the LDAS.
##
## Release Date: Not Available
##
##  This script validates the ilwd against the
##  another ilwd, xml which can be 
##  the results of the select query to check the data inserted
## Warnings:
##  if dims 1000 is used, only validates first 100 or it 
## will run out of memory. 
## ******************************************************** 

;#barecode

;## returns an ascii for numeric chars in char_u type 
proc char_u_to_str { blob_data dims type } {
    if  { ! [ regexp {^\\} $blob_data ] } {
        if  { [ regexp {\d\d\d\d-\d\d-\d\d.\d\d:\d\d:\d\d.\d+} $blob_data ] } {
		;## this is for timestamp 
            regsub -all {:} $blob_data {.} blob_data
			regsub -all {\s} $blob_data {-} blob_data
            puts "in here blob_data=$blob_data"
        }
        return $blob_data
    }
    foreach num $blob_data {
    }
    ;## for some reason this caused the binary number 
    ;## to be into num and got formatted correctly.
	binary scan $num ${type}${dims} str 
    return $str
}

proc get_data_from_element { elemp { index -1 } { Colname "" } } {
	set nullmask ""
	set data [ getElement $elemp ascii none ]
	if	{ ! [ string length $Colname ] } {
		set Colname [ getElementAttribute $elemp "name" ]
        if  { [ regexp {([^:]+):Column:XML} $Colname -> Colname ] } {
        } else {
		    set Colname [ lindex [ split $Colname : ] end ]
        }       
	}
	set Colname [ string tolower $Colname ]
	set dims [ getElementAttribute $elemp "dims" ]
	set ilwdtype [ getElementAttribute $elemp "identifier" ]
	set ilwd [ ilwd::preprocess [ getElement $elemp ] ]
	set result [ ilwd::parse $ilwd ]
	set len [ llength $result ]
	set result_data [ lindex $result 2 ]

    ;## convert unique Ids to hex chars
	if	{ [ regexp -nocase {char_u} $ilwdtype ] } {
		;## uniqueId field
		if	{ [ regexp -nocase {_id} $Colname ] && $dims == 13 } {
			set result_data [ char_u_to_str $result_data 26 H ] 
		} else {
			set result_data [ char_u_to_str $result_data $dims A ] 
		}		
	} else {	
		if	{ $dims > 0 } {
			switch $ilwdtype {
				lstring { set result_data [ join $result_data ]
					regsub -all {\,} $result_data {,} result_data 
					set result_data [ split $result_data , ] }
				 char	{ set result_data [ join $result_data ] }
				default { set result_data [ split $result_data ] }
			}
		} 
	}
    ;## if this element is part of a container, just set the data
    if  { $index != -1 } {
        array set ::$::tag,${Colname} [ list $index [ string trim $result_data ] ]
	} else {
    ;## this is an LDAS array so create an array to hold the entries
        set index 0
	    foreach entry $result_data {
		    array set ::$::tag,${Colname} [ list $index [ string trim $entry ] ]
		    incr index 1
	    }
        for { set i 0 } { $i < $dims } { incr i 1 } {
			if	{  [ isNullInElement $elemp $i ] } {
				set nullmask "${nullmask}1"
			} else {
				set nullmask "${nullmask}0"
			}
        }
        if	{ [ regexp {[1]+} $nullmask ] } {
		    array set ::$::tag,${Colname} [ list nullmask $nullmask ]
	    }
    }
    #puts "$Colname,nullmask=$nullmask,entries: [array names ::$::tag,$Colname]"
	return $Colname
}

proc get_data_from_container { contp } {
	set size [ getElementAttribute $contp "size" ]
	set Colname [ getElementAttribute $contp "name" ]
	;## for xml ilwds, name is first, ilwds name is last 
    if  { [ regexp {([^:]+):Column:XML} $Colname -> Colname ] } {
   	} else {
		set Colname [ lindex [ split $Colname : ] end ]
   	}
	set Colname [ string tolower $Colname ]
    ;## identify null entries in container
    set nullmask ""
	for { set i 0 } { $i < $size } { incr i 1 } {
		if	{  [ isNullInElement $contp $i ] } {
			set nullmask "${nullmask}1"
		} else {
			set nullmask "${nullmask}0"
		}
	}
	if	{ [ regexp {[1]+} $nullmask ] } {
		array set ::$::tag,${Colname} [ list nullmask $nullmask ]
	}
	for { set i 0 } { $i < $size } { incr i 1 } {
		set elemp [ refContainerElement $contp $i ]
		get_data_from_element $elemp $i $Colname 
	}
	#puts "container $Colname,nullmask=$nullmask,array names [array names ::${Colname}]"
	return $Colname 	
}

set env(HOST) [ exec uname -n ]

set auto_path "/ldas/lib $auto_path"
puts "auto_path=$auto_path"

set API test
namespace eval $API {}
package require generic
package require ligolw

;## build the input data hash tables
;## nullmask is not accessible as an element attribute.
proc build_hash_table { contp } {

	set numCols [ getElementAttribute $contp "size" ]
	for { set i 0 } { $i < $numCols } { incr i 1 } {
		set elmcontp [ refContainerElement $contp $i ]
		;##puts "i=$i, elmcontp = $elmcontp"

		switch -regexp -- $elmcontp {
	
			{^_[0-9a-f]+_p_LdasElement$} {
           		set name [ get_data_from_element $elmcontp ]
       		}		
			{^_[0-9a-f]+_p_LdasArrayBase$} {
           		set name [ get_data_from_element $elmcontp ]
       		}
			{^_[0-9a-f]+_p_LdasContainer$} {
				set name [ get_data_from_container $elmcontp ]
      		} 
			default { set name ""; puts "unknown type $elmcontp" }	
		}
		#puts "build_hash_table, name=$name"
	}
}

proc normalizeText { text } {
    regsub -all {\{} $text {} text
    regsub -all {\}} $text {} text
    return $text
}

proc compare_entry { colname } {

    set mismatch 0
    set dbvals [ list ]
	;## form a list from dbcols since retrieval may not be in order
	foreach dbcol [ array names ::out,$colname ] {
		set dbval [ format "%s" [ set ::out,${colname}($dbcol) ] ]
		set dbval [ normalizeText $dbval ]
		lappend dbvals $dbval
	}
	set init_len [ llength $dbvals ]
	
	if	{ ! $init_len } {
		puts "no data in output for $colname"
		return 1
	}
	puts "colname=$colname,[ lsort [array names ::in,$colname]]"
	foreach entry [ lsort -increasing [ array names ::in,$colname ] ] {
    	if  { [ regexp {nullmask} $entry ] } {	 
			puts "nullmask=[set ::in,${colname}(nullmask)]"
            continue
        }
		set ilwdval [ set ::in,${colname}($entry) ]
		if	{ [ info exist ::in,${colname}(nullmask) ] } {
			if  { [ regexp {1} [ string range [set ::in,${colname}(nullmask)] $entry $entry ] ] } {
		    	set ilwdval "null"
			} else {
				set ilwdval [ set ::in,${colname}($entry) ]
			}
		}
		if	{ [ info exist ::out,${colname}($entry) ] } {
			set dbval [ format "%s" [ set ::out,${colname}($entry) ] ]
			set ilwdval [ format "%s" $ilwdval ]
            set dbval [ normalizeText $dbval ]
            set ilwdval [ normalizeText $ilwdval ]
            puts "entry=$entry, dbval=$dbval,ilwdval=$ilwdval."
			if	{ [ string compare $dbval $ilwdval ] } {		
				if	{ [ regexp {null} $ilwdval ] && [ regexp {0|NaN|(^$)} $dbval ] } {
				} else {
					;## alternate check if query does not return in order 
					set index [ lsearch -exact $dbvals $ilwdval ]
					if	{ $index > -1 } {
						set dbvals [ lreplace $dbvals $index $index ]
					} else { 
						puts "mismatched in column $colname, $entry:\
			 			expected $ilwdval, got $dbval"
						set mismatch 1
					}
				}
			}
		} else {
            puts "column $colname not in output"
			break
		}	
	}
	if	{ [ llength $dbvals ] < $init_len } {
		puts "$::infile $colname validation by lsearch applied"
	}
	return $mismatch
}

proc processFile {fname} {
	if	{ [ catch {
		switch [ file extension $fname ] {
		.xml  { set lwp [ readLwFile $fname ] 
			foreach { ilwdp pos } [ getLwData $lwp table ] {break}
			set contp $ilwdp
        	#puts "[getElement $contp ascii none]"
			build_hash_table $contp
		  }
		.ilwd  { set filwd [ openILwdFile $fname r ] 
			set contp [ readElement $filwd ]
			closeILwdFile $filwd
        	set contp [ refContainerElement $contp 0 ]
			build_hash_table $contp
		  }
		default { return -code error "$fname not xml or ilwd" }
		}
	} err ] } {
		return -code error "$fname error: $err"
	}
}

set infile [ lindex $argv 0 ]
set ::tag in
puts "infile=$infile"
if	{ [ catch {
	processFile $infile 
	} err ] } {
	puts "$err, exiting"
	exit
}	

set outfile [ lindex $argv 1 ]
set ::tag out
if	{ [ catch {
	processFile $outfile 
	} err ] } {
	puts "$err, exiting"
	exit
}

;## compare results of same columns

set mismatch 0
puts "comparing ..."
puts "in [info vars in,*]"
puts "out [info vars out,*]"
foreach entry [lsort [ info vars in,* ] ] {
	regexp {in,(.+)} $entry -> col
    if  { [ catch {
	    incr mismatch [ compare_entry $col ]
    } err ] } {
        puts "$col mismatch:$err"
    }
}

;## dump out the remainding columns from database
foreach entry [ info vars out,*] {
	regexp {out,(.+)} $entry -> col
	if	{ ! [ info exist in,$col ] } {
		puts "output column $col not in original ilwd."
	}
}

if	{ $mismatch == 0 } {
	puts "$infile has been validated against $outfile"
} else {
	puts "$infile has $mismatch mismatched columns with $outfile"
}
