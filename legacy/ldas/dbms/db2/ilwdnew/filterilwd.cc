#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3filter( "filtergroup:filter:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer program_cont ( "filtergroup:filter:program" ); 
	INT_4S	start_time[ARRAYSIZE];
	LdasContainer process_id_cont ( "filtergroup:filter:process_id" ); 
	LdasContainer filter_name_cont ( "filtergroup:filter:filter_name" ); 
	INT_4S	param_set[ARRAYSIZE];
	LdasContainer comment_cont ( "filtergroup:filter:comment" ); 
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#endif

using namespace local;

static void setup_program () {

// fill in program array
// NOT NULL
	CHAR program[16];
	CHAR programattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(program, (size_t) 16, "program_%d", i + thistime + 1 );
		sprintf(programattr, "filter:%s", program);
		LdasArray< CHAR > program_vector ( program, strlen(program),
			"filtergroup:filter:program" );
		program_vector.setComment( programattr );
		program_cont.push_back( program_vector );
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"filtergroup:filter:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"filtergroup:filter:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filter_name () {

// fill in filter_name array
// NOT NULL
	CHAR filter_name[32];
	CHAR filter_nameattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(filter_name, (size_t) 32, "filter_name_%d", i + thistime + 1 );
		sprintf(filter_nameattr, "filter:%s", filter_name);
		LdasArray< CHAR > filter_name_vector ( filter_name, strlen(filter_name),
			"filtergroup:filter:filter_name" );
		filter_name_vector.setComment( filter_nameattr );
		filter_name_cont.push_back( filter_name_vector );
	}
}

static void setup_filter_id () {

// fill in filter_id array
// NOT NULL
// filter_id is uniqueId, generated by metadataAPI

}

static void setup_param_set () {

// fill in param_set array
	for (int i=0; i < numElements; i++) {
		param_set[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[240];
	CHAR commentattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 240, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "filter:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"filtergroup:filter:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

void setup_filter ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_program();
	ilwd3filter.push_back(  program_cont ) ;
	setup_start_time();
	ilwd3filter.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"filtergroup:filter:start_time" ) );
	setup_process_id();
	ilwd3filter.push_back( process_id_cont ) ;
	setup_filter_name();
	ilwd3filter.push_back(  filter_name_cont ) ;
	setup_filter_id();
	setup_param_set();
	ilwd3filter.push_back(  LdasArray< INT_4S >(param_set, numElements,
	"filtergroup:filter:param_set" ) );
	setup_comment();
	ilwd3filter.push_back(  comment_cont ) ;
}

#ifdef TESTMAIN 
 void setup_filter( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_filter( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3filter );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
