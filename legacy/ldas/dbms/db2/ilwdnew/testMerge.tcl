#! /ldcg/bin/tclsh

## ******************************************************** 
## putValidate.tcl
##
#! /ldcg/bin/tclsh

## Provides data retrieval functions for the LDAS.
##
## Release Date: Not Available
##
##  This script validates the ilwd input against the
##  the results of the select query to check the data inserted
##  
## ******************************************************** 

;#barecode

set env(HOST) [ exec uname -n ]

set auto_path "/usr1/ldas/mlei/genericAPI \
/usr1/ldas/mlei/metadataAPI \
/ldas/ldas-0.0/lib $auto_path"
puts "auto_path=$auto_path"

set API test
package require generic

;## testing merging 2 containers

set filename "process10.ilwd"
set filwd [ openILwdFile $filename "r" ] 
set cont1p [ readElement $filwd ]
closeILwdFile $filwd

set filename "process100.ilwd"
set filwd [ openILwdFile $filename "r" ] 
set cont2p [ readElement $filwd ]
closeILwdFile $filwd

set tblcont2p [ refContainerElement $cont2p 0 ]
mergeContainer $tblcont2p $cont1p
puts "new size= [ getElementAttribute $tblcont2p "size" ]"
puts "[ getElement $tblcont2p ]"

