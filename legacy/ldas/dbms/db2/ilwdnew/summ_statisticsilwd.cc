#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3summ_statistics( "summ_statisticsgroup:summ_statistics:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer program_cont ( "summ_statisticsgroup:summ_statistics:program" ); 
	LdasContainer process_id_cont ( "summ_statisticsgroup:summ_statistics:process_id" ); 
	LdasContainer frameset_group_cont ( "summ_statisticsgroup:summ_statistics:frameset_group" ); 
	LdasContainer segment_group_cont ( "summ_statisticsgroup:summ_statistics:segment_group" ); 
	INT_4S	version[ARRAYSIZE];
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	INT_4S	frames_used[ARRAYSIZE];
	INT_4S	samples[ARRAYSIZE];
	LdasContainer channel_cont ( "summ_statisticsgroup:summ_statistics:channel" ); 
	REAL_8	min_value[ARRAYSIZE];
	REAL_8	max_value[ARRAYSIZE];
	REAL_8	min_delta[ARRAYSIZE];
	REAL_8	max_delta[ARRAYSIZE];
	REAL_8	min_deltadelta[ARRAYSIZE];
	REAL_8	max_deltadelta[ARRAYSIZE];
	REAL_8	mean[ARRAYSIZE];
	REAL_8	variance[ARRAYSIZE];
	REAL_8	rms[ARRAYSIZE];
	REAL_8	skewness[ARRAYSIZE];
	REAL_8	kurtosis[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"segment.h"
#include	"frameset.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"segment_symref.h"
#include	"frameset_symref.h"
#endif

using namespace local;

static void setup_program () {

// fill in program array
// NOT NULL
	CHAR program[16];
	CHAR programattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(program, (size_t) 16, "program_%d", i + thistime + 1 );
		sprintf(programattr, "summ_statistics:%s", program);
		LdasArray< CHAR > program_vector ( program, strlen(program),
			"summ_statisticsgroup:summ_statistics:program" );
		program_vector.setComment( programattr );
		program_cont.push_back( program_vector );
	}
}

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"summ_statisticsgroup:summ_statistics:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"summ_statisticsgroup:summ_statistics:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_frameset_group () {

// fill in frameset_group array
	CHAR frameset_group[48];
	CHAR frameset_groupattr[248];

	for (int i=0; i < numElements; i++) {
		strcpy(frameset_group, frameset[ i % frameset_len ].FRAMESET_GROUP);
		sprintf(frameset_groupattr, "summ_statistics:%s", frameset_group);
		LdasArray< CHAR > frameset_group_vector ( frameset_group, strlen(frameset_group),
			"summ_statisticsgroup:summ_statistics:frameset_group" );
		frameset_group_vector.setComment( frameset_groupattr );
		frameset_group_cont.push_back( frameset_group_vector );
	}
}

static void setup_segment_group () {

// fill in segment_group array
	CHAR segment_group[64];
	CHAR segment_groupattr[264];

	for (int i=0; i < numElements; i++) {
		strcpy(segment_group, segment[ i % segment_len ].SEGMENT_GROUP);
		sprintf(segment_groupattr, "summ_statistics:%s", segment_group);
		LdasArray< CHAR > segment_group_vector ( segment_group, strlen(segment_group),
			"summ_statisticsgroup:summ_statistics:segment_group" );
		segment_group_vector.setComment( segment_groupattr );
		segment_group_cont.push_back( segment_group_vector );
	}
}

static void setup_version () {

// fill in version array
	for (int i=0; i < numElements; i++) {
		local::version[i] = atol( segment[ i % segment_len ].VERSION_local);
	}
}

static void setup_start_time () {

// fill in start_time array
// primary key
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] =  (INT_4S) atol (segment[ i % segment_len ].START_TIME);
	}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// primary key
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] =  (INT_4S) atol(segment[ i % segment_len ].END_TIME);
	}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_frames_used () {

// fill in frames_used array
	for (int i=0; i < numElements; i++) {
		frames_used[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_samples () {

// fill in samples array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		samples[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_channel () {

// fill in channel array
// primary key
// NOT NULL
	CHAR channel[240];
	CHAR channelattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 240, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "summ_statistics:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"summ_statisticsgroup:summ_statistics:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_min_value () {

// fill in min_value array
	for (int i=0; i < numElements; i++) {
		min_value[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_max_value () {

// fill in max_value array
	for (int i=0; i < numElements; i++) {
		max_value[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_min_delta () {

// fill in min_delta array
	for (int i=0; i < numElements; i++) {
		min_delta[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_max_delta () {

// fill in max_delta array
	for (int i=0; i < numElements; i++) {
		max_delta[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_min_deltadelta () {

// fill in min_deltadelta array
	for (int i=0; i < numElements; i++) {
		min_deltadelta[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_max_deltadelta () {

// fill in max_deltadelta array
	for (int i=0; i < numElements; i++) {
		max_deltadelta[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_mean () {

// fill in mean array
	for (int i=0; i < numElements; i++) {
		mean[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_variance () {

// fill in variance array
	for (int i=0; i < numElements; i++) {
		variance[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_rms () {

// fill in rms array
	for (int i=0; i < numElements; i++) {
		rms[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_skewness () {

// fill in skewness array
	for (int i=0; i < numElements; i++) {
		skewness[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_kurtosis () {

// fill in kurtosis array
	for (int i=0; i < numElements; i++) {
		kurtosis[i] = (REAL_8) i + thistime + 1 ;
	}
}

void setup_summ_statistics ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_program();
	ilwd3summ_statistics.push_back(  program_cont ) ;
	setup_process_id();
	ilwd3summ_statistics.push_back( process_id_cont ) ;
	setup_frameset_group();
//	ilwd3summ_statistics.push_back(  frameset_group_cont ) ;
	setup_segment_group();
	ilwd3summ_statistics.push_back(  segment_group_cont ) ;
	setup_version();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(local::version, numElements,
	"summ_statisticsgroup:summ_statistics:version" ) );
	setup_start_time();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"summ_statisticsgroup:summ_statistics:start_time" ) );
	setup_start_time_ns();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"summ_statisticsgroup:summ_statistics:start_time_ns" ) );
	setup_end_time();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"summ_statisticsgroup:summ_statistics:end_time" ) );
	setup_end_time_ns();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"summ_statisticsgroup:summ_statistics:end_time_ns" ) );
	setup_frames_used();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(frames_used, numElements,
	"summ_statisticsgroup:summ_statistics:frames_used" ) );
	setup_samples();
	ilwd3summ_statistics.push_back(  LdasArray< INT_4S >(samples, numElements,
	"summ_statisticsgroup:summ_statistics:samples" ) );
	setup_channel();
	ilwd3summ_statistics.push_back(  channel_cont ) ;
	setup_min_value();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( min_value, numElements,
	"summ_statisticsgroup:summ_statistics:min_value" ) );
	setup_max_value();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( max_value, numElements,
	"summ_statisticsgroup:summ_statistics:max_value" ) );
	setup_min_delta();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( min_delta, numElements,
	"summ_statisticsgroup:summ_statistics:min_delta" ) );
	setup_max_delta();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( max_delta, numElements,
	"summ_statisticsgroup:summ_statistics:max_delta" ) );
	setup_min_deltadelta();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( min_deltadelta, numElements,
	"summ_statisticsgroup:summ_statistics:min_deltadelta" ) );
	setup_max_deltadelta();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( max_deltadelta, numElements,
	"summ_statisticsgroup:summ_statistics:max_deltadelta" ) );
	setup_mean();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( mean, numElements,
	"summ_statisticsgroup:summ_statistics:mean" ) );
	setup_variance();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( variance, numElements,
	"summ_statisticsgroup:summ_statistics:variance" ) );
	setup_rms();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( rms, numElements,
	"summ_statisticsgroup:summ_statistics:rms" ) );
	setup_skewness();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( skewness, numElements,
	"summ_statisticsgroup:summ_statistics:skewness" ) );
	setup_kurtosis();
	ilwd3summ_statistics.push_back(  LdasArray< REAL_8 >( kurtosis, numElements,
	"summ_statisticsgroup:summ_statistics:kurtosis" ) );
}

#ifdef TESTMAIN 
 void setup_summ_statistics( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_summ_statistics( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3summ_statistics );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
