#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3gds_trigger( "gds_triggergroup:gds_trigger:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "gds_triggergroup:gds_trigger:process_id" ); 
	LdasContainer filter_id_cont ( "gds_triggergroup:gds_trigger:filter_id" ); 
	LdasContainer name_cont ( "gds_triggergroup:gds_trigger:name" ); 
	LdasContainer subtype_cont ( "gds_triggergroup:gds_trigger:subtype" ); 
	LdasContainer ifo_cont ( "gds_triggergroup:gds_trigger:ifo" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	REAL_4	duration[ARRAYSIZE];
	INT_4S	priority[ARRAYSIZE];
	INT_4S	disposition[ARRAYSIZE];
	REAL_4	size[ARRAYSIZE];
	REAL_4	significance[ARRAYSIZE];
	REAL_4	frequency[ARRAYSIZE];
	REAL_4	bandwidth[ARRAYSIZE];
	LdasContainer binarydata_cont ( "gds_triggergroup:gds_trigger:binarydata" ); 
	INT_4S	binarydata_length[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"filter.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"filter_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"gds_triggergroup:gds_trigger:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"gds_triggergroup:gds_trigger:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filter_id () {

// fill in filter_id array
	char filter_id_str[30] ;
	CHAR_U filter_id[13];
	CHAR filter_idattr[200];

	int i,j;
// convert filter_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id_str, filter[ i % filter_len	].FILTER_ID);
		for ( int x = 0, j = 0; x < strlen(filter_id_str); x=x+2, j++ ) {
			filter_id[j] = ((filter_id_str[x]-'0' ) << 4) + (filter_id_str[x+1]-'0');
		}
		sprintf(filter_idattr, "filter_id:%s", filter_id_str);
		LdasArray< CHAR_U > filter_id_vector ( filter_id, 13,
		"gds_triggergroup:gds_trigger:filter_id" );
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );
	}
}

static void setup_filter_id_symref () {
// fill in filter_id array
// NOT NULL
	CHAR filter_id[30] ;
	CHAR filter_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id, filter[ i % filter_len ].FILTER_ID );
		sprintf( filter_idattr, "filter_id:%s", filter_id );
		LdasArray< CHAR > filter_id_vector ( filter_id, strlen(filter_id),
		"gds_triggergroup:gds_trigger:filter_id");
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );}
}

static void setup_name () {

// fill in name array
// NOT NULL
	CHAR name[32];
	CHAR nameattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 32, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "gds_trigger:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"gds_triggergroup:gds_trigger:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_subtype () {

// fill in subtype array
// NOT NULL
	CHAR subtype[64];
	CHAR subtypeattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(subtype, (size_t) 64, "subtype_%d", i + thistime + 1 );
		sprintf(subtypeattr, "gds_trigger:%s", subtype);
		LdasArray< CHAR > subtype_vector ( subtype, strlen(subtype),
			"gds_triggergroup:gds_trigger:subtype" );
		subtype_vector.setComment( subtypeattr );
		subtype_cont.push_back( subtype_vector );
	}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "gds_trigger:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"gds_triggergroup:gds_trigger:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_duration () {

// fill in duration array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		duration[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_priority () {

// fill in priority array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		priority[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_disposition () {

// fill in disposition array
	for (int i=0; i < numElements; i++) {
		disposition[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_size () {

// fill in size array
	for (int i=0; i < numElements; i++) {
		size[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_significance () {

// fill in significance array
	for (int i=0; i < numElements; i++) {
		significance[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_frequency () {

// fill in frequency array
	for (int i=0; i < numElements; i++) {
		frequency[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_bandwidth () {

// fill in bandwidth array
	for (int i=0; i < numElements; i++) {
		bandwidth[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_binarydata () {

// fill in binarydata array
	CHAR binarydata[1024];
	CHAR binarydataattr[1224];

	for (int i=0; i < numElements; i++) {
		snprintf(binarydata, (size_t) 1024, "binarydata_%d", i + thistime + 1 );
		sprintf(binarydataattr, "gds_trigger:%s", binarydata);
		LdasArray< CHAR > binarydata_vector ( binarydata, strlen(binarydata),
			"gds_triggergroup:gds_trigger:binarydata" );
		binarydata_vector.setComment( binarydataattr );
		binarydata_cont.push_back( binarydata_vector );
	}
}

static void setup_binarydata_length () {

// fill in binarydata_length array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		binarydata_length[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
// event_id is uniqueId, generated by metadataAPI

}

void setup_gds_trigger ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3gds_trigger.push_back( process_id_cont ) ;
	setup_filter_id();
	ilwd3gds_trigger.push_back( filter_id_cont ) ;
	setup_name();
	ilwd3gds_trigger.push_back(  name_cont ) ;
	setup_subtype();
	ilwd3gds_trigger.push_back(  subtype_cont ) ;
	setup_ifo();
	ilwd3gds_trigger.push_back(  ifo_cont ) ;
	setup_start_time();
	ilwd3gds_trigger.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"gds_triggergroup:gds_trigger:start_time" ) );
	setup_start_time_ns();
	ilwd3gds_trigger.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"gds_triggergroup:gds_trigger:start_time_ns" ) );
	setup_duration();
	ilwd3gds_trigger.push_back(  LdasArray< REAL_4 >( duration, numElements,
	"gds_triggergroup:gds_trigger:duration" ) );
	setup_priority();
	ilwd3gds_trigger.push_back(  LdasArray< INT_4S >(priority, numElements,
	"gds_triggergroup:gds_trigger:priority" ) );
	setup_disposition();
	ilwd3gds_trigger.push_back(  LdasArray< INT_4S >(disposition, numElements,
	"gds_triggergroup:gds_trigger:disposition" ) );
	setup_size();
	ilwd3gds_trigger.push_back(  LdasArray< REAL_4 >( size, numElements,
	"gds_triggergroup:gds_trigger:size" ) );
	setup_significance();
	ilwd3gds_trigger.push_back(  LdasArray< REAL_4 >( significance, numElements,
	"gds_triggergroup:gds_trigger:significance" ) );
	setup_frequency();
	ilwd3gds_trigger.push_back(  LdasArray< REAL_4 >( frequency, numElements,
	"gds_triggergroup:gds_trigger:frequency" ) );
	setup_bandwidth();
	ilwd3gds_trigger.push_back(  LdasArray< REAL_4 >( bandwidth, numElements,
	"gds_triggergroup:gds_trigger:bandwidth" ) );
	setup_binarydata();
	ilwd3gds_trigger.push_back(  binarydata_cont ) ;
	setup_binarydata_length();
	ilwd3gds_trigger.push_back(  LdasArray< INT_4S >(binarydata_length, numElements,
	"gds_triggergroup:gds_trigger:binarydata_length" ) );
	setup_event_id();
}

#ifdef TESTMAIN 
 void setup_gds_trigger( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_gds_trigger( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3gds_trigger );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
