#include <cstring>
#include <algorithm>
#include <iostream>
#include <climits>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3calib_info( "calib_infogroup:calib_info:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	INT_4S	valid_start[ARRAYSIZE];
	INT_4S	valid_end[ARRAYSIZE];
	LdasContainer origin_cont ( "calib_infogroup:calib_info:origin" ); 
	INT_4S	origin_time[ARRAYSIZE];
	LdasContainer insert_by_cont ( "calib_infogroup:calib_info:insert_by" ); 
	LdasContainer channel_cont ( "calib_infogroup:calib_info:channel" ); 
	LdasContainer units_cont ( "calib_infogroup:calib_info:units" ); 
	INT_4S	caltype[ARRAYSIZE];
	LdasContainer caldata_cont ( "calib_infogroup:calib_info:caldata" ); 
	LdasContainer comment_cont ( "calib_infogroup:calib_info:comment" ); 
} // end namespace

using namespace local;

static void setup_valid_start () {

// fill in valid_start array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		valid_start[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_valid_end () {

// fill in valid_end array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		valid_end[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_origin () {

// fill in origin array
	CHAR origin[128];
	CHAR originattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(origin, (size_t) 128, "origin_%d", i + thistime + 1 );
		sprintf(originattr, "calib_info:%s", origin);
		LdasArray< CHAR > origin_vector ( origin, strlen(origin),
			"calib_infogroup:calib_info:origin" );
		origin_vector.setComment( originattr );
		origin_cont.push_back( origin_vector );
	}
}

static void setup_origin_time () {

// fill in origin_time array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		origin_time[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_insert_by () {

// fill in insert_by array
// NOT NULL
	CHAR insert_by[32];
	CHAR insert_byattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(insert_by, (size_t) 32, "insert_by_%d", i + thistime + 1 );
		sprintf(insert_byattr, "calib_info:%s", insert_by);
		LdasArray< CHAR > insert_by_vector ( insert_by, strlen(insert_by),
			"calib_infogroup:calib_info:insert_by" );
		insert_by_vector.setComment( insert_byattr );
		insert_by_cont.push_back( insert_by_vector );
	}
}

static void setup_channel () {

// fill in channel array
// primary key
// NOT NULL
	CHAR channel[64];
	CHAR channelattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 64, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "calib_info:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"calib_infogroup:calib_info:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_units () {

// fill in units array
// primary key
// NOT NULL
	CHAR units[64];
	CHAR unitsattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(units, (size_t) 64, "units_%d", i + thistime + 1 );
		sprintf(unitsattr, "calib_info:%s", units);
		LdasArray< CHAR > units_vector ( units, strlen(units),
			"calib_infogroup:calib_info:units" );
		units_vector.setComment( unitsattr );
		units_cont.push_back( units_vector );
	}
}

static void setup_caltype () {

// fill in caltype array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		caltype[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_caldata () {

// fill in caldata array
// NOT NULL
// fill in caldata array
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}

	caldata_cont.setComment("caldata blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {
		sprintf (name, "%s", "caldatagroup:calib_info:caldata");

		size_t size( ( i % 100 + 1 )  * BLOBSIZE );
       	const CHAR_U fill_char( i%UCHAR_MAX );

        CHAR_U buffer[ size ];
        fill( buffer, buffer + size, fill_char );

        try
		{
  		   LdasArray< CHAR_U >* char_u_vector = new 
		   		LdasArray< CHAR_U >( buffer, size, name, units );

           // create variable size blobs
		   caldata_cont.push_back( char_u_vector, 
           		LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		}
          	catch( const std::exception& exc )
		{
		   	cout << "Exception: " << exc.what() << endl;
               	   exit( 1 );
	  	}
           	catch( const FormatException& exc )
		{
           	const size_t exc_size( exc.getSize() );
           	cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		}
	} 
	caldata_cont.setWriteFormat(ILwd::BASE64);
	caldata_cont.setWriteCompression( ILwd::GZIP0);
}

static void setup_comment () {

// fill in comment array
	CHAR comment[256];
	CHAR commentattr[456];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 256, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "calib_info:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"calib_infogroup:calib_info:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

void setup_calib_info ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_valid_start();
	ilwd3calib_info.push_back(  LdasArray< INT_4S >(valid_start, numElements,
	"calib_infogroup:calib_info:valid_start" ) );
	setup_valid_end();
	ilwd3calib_info.push_back(  LdasArray< INT_4S >(valid_end, numElements,
	"calib_infogroup:calib_info:valid_end" ) );
	setup_origin();
	ilwd3calib_info.push_back(  origin_cont ) ;
	setup_origin_time();
	ilwd3calib_info.push_back(  LdasArray< INT_4S >(origin_time, numElements,
	"calib_infogroup:calib_info:origin_time" ) );
	setup_insert_by();
	ilwd3calib_info.push_back(  insert_by_cont ) ;
	setup_channel();
	ilwd3calib_info.push_back(  channel_cont ) ;
	setup_units();
	ilwd3calib_info.push_back(  units_cont ) ;
	setup_caltype();
	ilwd3calib_info.push_back(  LdasArray< INT_4S >(caltype, numElements,
	"calib_infogroup:calib_info:caltype" ) );
	setup_caldata();
	ilwd3calib_info.push_back( caldata_cont ) ;
	setup_comment();
	ilwd3calib_info.push_back(  comment_cont ) ;
}

#ifdef TESTMAIN 
 void setup_calib_info( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_calib_info( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3calib_info );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
