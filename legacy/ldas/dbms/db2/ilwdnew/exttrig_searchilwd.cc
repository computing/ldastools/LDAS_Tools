#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3exttrig_search( "exttrig_searchgroup:exttrig_search:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "exttrig_searchgroup:exttrig_search:process_id" ); 
	LdasContainer ifos_cont ( "exttrig_searchgroup:exttrig_search:ifos" ); 
	LdasContainer exttrig_type_cont ( "exttrig_searchgroup:exttrig_search:exttrig_type" ); 
	LdasContainer exttrig_id_cont ( "exttrig_searchgroup:exttrig_search:exttrig_id" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	REAL_4	source_ra[ARRAYSIZE];
	REAL_4	source_ra_err[ARRAYSIZE];
	REAL_4	source_dec[ARRAYSIZE];
	REAL_4	source_dec_err[ARRAYSIZE];
	INT_4S	segment_type[ARRAYSIZE];
	REAL_4	duration[ARRAYSIZE];
	REAL_4	ref_delay[ARRAYSIZE];
	REAL_4	lag_1_2[ARRAYSIZE];
	REAL_4	lag_1_3[ARRAYSIZE];
	REAL_4	lag_1_4[ARRAYSIZE];
	REAL_4	amplitude[ARRAYSIZE];
	REAL_4	snr[ARRAYSIZE];
	REAL_4	percent_bad[ARRAYSIZE];
	REAL_4	var_1[ARRAYSIZE];
	REAL_4	var_2[ARRAYSIZE];
	REAL_4	var_3[ARRAYSIZE];
	REAL_4	var_4[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"exttrig_searchgroup:exttrig_search:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"exttrig_searchgroup:exttrig_search:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_ifos () {

// fill in ifos array
	CHAR ifos[18];
	CHAR ifosattr[218];

	for (int i=0; i < numElements; i++) {
		snprintf(ifos, (size_t) 18, "ifos_%d", i + thistime + 1 );
		sprintf(ifosattr, "exttrig_search:%s", ifos);
		LdasArray< CHAR > ifos_vector ( ifos, strlen(ifos),
			"exttrig_searchgroup:exttrig_search:ifos" );
		ifos_vector.setComment( ifosattr );
		ifos_cont.push_back( ifos_vector );
	}
}

static void setup_exttrig_type () {

// fill in exttrig_type array
// NOT NULL
	CHAR exttrig_type[32];
	CHAR exttrig_typeattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(exttrig_type, (size_t) 32, "exttrig_type_%d", i + thistime + 1 );
		sprintf(exttrig_typeattr, "exttrig_search:%s", exttrig_type);
		LdasArray< CHAR > exttrig_type_vector ( exttrig_type, strlen(exttrig_type),
			"exttrig_searchgroup:exttrig_search:exttrig_type" );
		exttrig_type_vector.setComment( exttrig_typeattr );
		exttrig_type_cont.push_back( exttrig_type_vector );
	}
}

static void setup_exttrig_id () {

// fill in exttrig_id array
	CHAR exttrig_id[40];
	CHAR exttrig_idattr[240];

	for (int i=0; i < numElements; i++) {
		snprintf(exttrig_id, (size_t) 40, "exttrig_id_%d", i + thistime + 1 );
		sprintf(exttrig_idattr, "exttrig_search:%s", exttrig_id);
		LdasArray< CHAR > exttrig_id_vector ( exttrig_id, strlen(exttrig_id),
			"exttrig_searchgroup:exttrig_search:exttrig_id" );
		exttrig_id_vector.setComment( exttrig_idattr );
		exttrig_id_cont.push_back( exttrig_id_vector );
	}
}

static void setup_start_time () {

// fill in start_time array
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_source_ra () {

// fill in source_ra array
	for (int i=0; i < numElements; i++) {
		source_ra[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_source_ra_err () {

// fill in source_ra_err array
	for (int i=0; i < numElements; i++) {
		source_ra_err[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_source_dec () {

// fill in source_dec array
	for (int i=0; i < numElements; i++) {
		source_dec[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_source_dec_err () {

// fill in source_dec_err array
	for (int i=0; i < numElements; i++) {
		source_dec_err[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_segment_type () {

// fill in segment_type array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		segment_type[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_duration () {

// fill in duration array
	for (int i=0; i < numElements; i++) {
		duration[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_ref_delay () {

// fill in ref_delay array
	for (int i=0; i < numElements; i++) {
		ref_delay[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_lag_1_2 () {

// fill in lag_1_2 array
	for (int i=0; i < numElements; i++) {
		lag_1_2[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_lag_1_3 () {

// fill in lag_1_3 array
	for (int i=0; i < numElements; i++) {
		lag_1_3[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_lag_1_4 () {

// fill in lag_1_4 array
	for (int i=0; i < numElements; i++) {
		lag_1_4[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_amplitude () {

// fill in amplitude array
	for (int i=0; i < numElements; i++) {
		amplitude[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_snr () {

// fill in snr array
	for (int i=0; i < numElements; i++) {
		snr[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_percent_bad () {

// fill in percent_bad array
	for (int i=0; i < numElements; i++) {
		percent_bad[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_var_1 () {

// fill in var_1 array
	for (int i=0; i < numElements; i++) {
		var_1[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_var_2 () {

// fill in var_2 array
	for (int i=0; i < numElements; i++) {
		var_2[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_var_3 () {

// fill in var_3 array
	for (int i=0; i < numElements; i++) {
		var_3[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_var_4 () {

// fill in var_4 array
	for (int i=0; i < numElements; i++) {
		var_4[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
// event_id is uniqueId, generated by metadataAPI

}

void setup_exttrig_search ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3exttrig_search.push_back( process_id_cont ) ;
	setup_ifos();
	ilwd3exttrig_search.push_back(  ifos_cont ) ;
	setup_exttrig_type();
	ilwd3exttrig_search.push_back(  exttrig_type_cont ) ;
	setup_exttrig_id();
	ilwd3exttrig_search.push_back(  exttrig_id_cont ) ;
	setup_start_time();
	ilwd3exttrig_search.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"exttrig_searchgroup:exttrig_search:start_time" ) );
	setup_start_time_ns();
	ilwd3exttrig_search.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"exttrig_searchgroup:exttrig_search:start_time_ns" ) );
	setup_source_ra();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( source_ra, numElements,
	"exttrig_searchgroup:exttrig_search:source_ra" ) );
	setup_source_ra_err();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( source_ra_err, numElements,
	"exttrig_searchgroup:exttrig_search:source_ra_err" ) );
	setup_source_dec();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( source_dec, numElements,
	"exttrig_searchgroup:exttrig_search:source_dec" ) );
	setup_source_dec_err();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( source_dec_err, numElements,
	"exttrig_searchgroup:exttrig_search:source_dec_err" ) );
	setup_segment_type();
	ilwd3exttrig_search.push_back(  LdasArray< INT_4S >(segment_type, numElements,
	"exttrig_searchgroup:exttrig_search:segment_type" ) );
	setup_duration();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( duration, numElements,
	"exttrig_searchgroup:exttrig_search:duration" ) );
	setup_ref_delay();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( ref_delay, numElements,
	"exttrig_searchgroup:exttrig_search:ref_delay" ) );
	setup_lag_1_2();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( lag_1_2, numElements,
	"exttrig_searchgroup:exttrig_search:lag_1_2" ) );
	setup_lag_1_3();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( lag_1_3, numElements,
	"exttrig_searchgroup:exttrig_search:lag_1_3" ) );
	setup_lag_1_4();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( lag_1_4, numElements,
	"exttrig_searchgroup:exttrig_search:lag_1_4" ) );
	setup_amplitude();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( amplitude, numElements,
	"exttrig_searchgroup:exttrig_search:amplitude" ) );
	setup_snr();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( snr, numElements,
	"exttrig_searchgroup:exttrig_search:snr" ) );
	setup_percent_bad();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( percent_bad, numElements,
	"exttrig_searchgroup:exttrig_search:percent_bad" ) );
	setup_var_1();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( var_1, numElements,
	"exttrig_searchgroup:exttrig_search:var_1" ) );
	setup_var_2();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( var_2, numElements,
	"exttrig_searchgroup:exttrig_search:var_2" ) );
	setup_var_3();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( var_3, numElements,
	"exttrig_searchgroup:exttrig_search:var_3" ) );
	setup_var_4();
	ilwd3exttrig_search.push_back(  LdasArray< REAL_4 >( var_4, numElements,
	"exttrig_searchgroup:exttrig_search:var_4" ) );
	setup_event_id();
}

#ifdef TESTMAIN 
 void setup_exttrig_search( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_exttrig_search( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3exttrig_search );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
