#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3multi_burst( "multi_burstgroup:multi_burst:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "multi_burstgroup:multi_burst:process_id" ); 
	LdasContainer filter_id_cont ( "multi_burstgroup:multi_burst:filter_id" ); 
	LdasContainer ifos_cont ( "multi_burstgroup:multi_burst:ifos" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	REAL_4	duration[ARRAYSIZE];
	REAL_4	central_freq[ARRAYSIZE];
	REAL_4	bandwidth[ARRAYSIZE];
	REAL_4	amplitude[ARRAYSIZE];
	REAL_4	snr[ARRAYSIZE];
	REAL_4	confidence[ARRAYSIZE];
	REAL_4	ligo_axis_ra[ARRAYSIZE];
	REAL_4	ligo_axis_dec[ARRAYSIZE];
	REAL_4	ligo_angle[ARRAYSIZE];
	REAL_4	ligo_angle_sig[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"filter.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"filter_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"multi_burstgroup:multi_burst:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"multi_burstgroup:multi_burst:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filter_id () {

// fill in filter_id array
	char filter_id_str[30] ;
	CHAR_U filter_id[13];
	CHAR filter_idattr[200];

	int i,j;
// convert filter_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id_str, filter[ i % filter_len	].FILTER_ID);
		for ( int x = 0, j = 0; x < strlen(filter_id_str); x=x+2, j++ ) {
			filter_id[j] = ((filter_id_str[x]-'0' ) << 4) + (filter_id_str[x+1]-'0');
		}
		sprintf(filter_idattr, "filter_id:%s", filter_id_str);
		LdasArray< CHAR_U > filter_id_vector ( filter_id, 13,
		"multi_burstgroup:multi_burst:filter_id" );
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );
	}
}

static void setup_filter_id_symref () {
// fill in filter_id array
// NOT NULL
	CHAR filter_id[30] ;
	CHAR filter_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id, filter[ i % filter_len ].FILTER_ID );
		sprintf( filter_idattr, "filter_id:%s", filter_id );
		LdasArray< CHAR > filter_id_vector ( filter_id, strlen(filter_id),
		"multi_burstgroup:multi_burst:filter_id");
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );}
}

static void setup_ifos () {

// fill in ifos array
// NOT NULL
	CHAR ifos[12];
	CHAR ifosattr[212];

	for (int i=0; i < numElements; i++) {
		snprintf(ifos, (size_t) 12, "ifos_%d", i + thistime + 1 );
		sprintf(ifosattr, "multi_burst:%s", ifos);
		LdasArray< CHAR > ifos_vector ( ifos, strlen(ifos),
			"multi_burstgroup:multi_burst:ifos" );
		ifos_vector.setComment( ifosattr );
		ifos_cont.push_back( ifos_vector );
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_duration () {

// fill in duration array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		duration[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_central_freq () {

// fill in central_freq array
	for (int i=0; i < numElements; i++) {
		central_freq[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_bandwidth () {

// fill in bandwidth array
	for (int i=0; i < numElements; i++) {
		bandwidth[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_amplitude () {

// fill in amplitude array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		amplitude[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_snr () {

// fill in snr array
	for (int i=0; i < numElements; i++) {
		snr[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_confidence () {

// fill in confidence array
	for (int i=0; i < numElements; i++) {
		confidence[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_ligo_axis_ra () {

// fill in ligo_axis_ra array
	for (int i=0; i < numElements; i++) {
		ligo_axis_ra[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_ligo_axis_dec () {

// fill in ligo_axis_dec array
	for (int i=0; i < numElements; i++) {
		ligo_axis_dec[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_ligo_angle () {

// fill in ligo_angle array
	for (int i=0; i < numElements; i++) {
		ligo_angle[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_ligo_angle_sig () {

// fill in ligo_angle_sig array
	for (int i=0; i < numElements; i++) {
		ligo_angle_sig[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
// event_id is uniqueId, generated by metadataAPI

}

void setup_multi_burst ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3multi_burst.push_back( process_id_cont ) ;
	setup_filter_id();
	ilwd3multi_burst.push_back( filter_id_cont ) ;
	setup_ifos();
	ilwd3multi_burst.push_back(  ifos_cont ) ;
	setup_start_time();
	ilwd3multi_burst.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"multi_burstgroup:multi_burst:start_time" ) );
	setup_start_time_ns();
	ilwd3multi_burst.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"multi_burstgroup:multi_burst:start_time_ns" ) );
	setup_duration();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( duration, numElements,
	"multi_burstgroup:multi_burst:duration" ) );
	setup_central_freq();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( central_freq, numElements,
	"multi_burstgroup:multi_burst:central_freq" ) );
	setup_bandwidth();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( bandwidth, numElements,
	"multi_burstgroup:multi_burst:bandwidth" ) );
	setup_amplitude();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( amplitude, numElements,
	"multi_burstgroup:multi_burst:amplitude" ) );
	setup_snr();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( snr, numElements,
	"multi_burstgroup:multi_burst:snr" ) );
	setup_confidence();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( confidence, numElements,
	"multi_burstgroup:multi_burst:confidence" ) );
	setup_ligo_axis_ra();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( ligo_axis_ra, numElements,
	"multi_burstgroup:multi_burst:ligo_axis_ra" ) );
	setup_ligo_axis_dec();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( ligo_axis_dec, numElements,
	"multi_burstgroup:multi_burst:ligo_axis_dec" ) );
	setup_ligo_angle();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( ligo_angle, numElements,
	"multi_burstgroup:multi_burst:ligo_angle" ) );
	setup_ligo_angle_sig();
	ilwd3multi_burst.push_back(  LdasArray< REAL_4 >( ligo_angle_sig, numElements,
	"multi_burstgroup:multi_burst:ligo_angle_sig" ) );
	setup_event_id();
}

#ifdef TESTMAIN 
 void setup_multi_burst( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_multi_burst( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3multi_burst );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
