#include <cstring>
#include <algorithm>
#include <iostream>
#include <climits>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3summ_mime( "summ_mimegroup:summ_mime:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer origin_cont ( "summ_mimegroup:summ_mime:origin" ); 
	LdasContainer process_id_cont ( "summ_mimegroup:summ_mime:process_id" ); 
	LdasContainer filename_cont ( "summ_mimegroup:summ_mime:filename" ); 
	LdasContainer submitter_cont ( "summ_mimegroup:summ_mime:submitter" ); 
	LdasContainer frameset_group_cont ( "summ_mimegroup:summ_mime:frameset_group" ); 
	LdasContainer segment_group_cont ( "summ_mimegroup:summ_mime:segment_group" ); 
	INT_4S	version[ARRAYSIZE];
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	LdasContainer channel_cont ( "summ_mimegroup:summ_mime:channel" ); 
	LdasContainer descrip_cont ( "summ_mimegroup:summ_mime:descrip" ); 
	LdasContainer mimedata_cont ( "summ_mimegroup:summ_mime:mimedata" ); 
	INT_4S	mimedata_length[ARRAYSIZE];
	LdasContainer mimetype_cont ( "summ_mimegroup:summ_mime:mimetype" ); 
	LdasContainer comment_cont ( "summ_mimegroup:summ_mime:comment" ); 
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"segment.h"
#include	"frameset.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"segment_symref.h"
#include	"frameset_symref.h"
#endif

using namespace local;

static void setup_origin () {

// fill in origin array
	CHAR origin[64];
	CHAR originattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(origin, (size_t) 64, "origin_%d", i + thistime + 1 );
		sprintf(originattr, "summ_mime:%s", origin);
		LdasArray< CHAR > origin_vector ( origin, strlen(origin),
			"summ_mimegroup:summ_mime:origin" );
		origin_vector.setComment( originattr );
		origin_cont.push_back( origin_vector );
	}
}

static void setup_process_id () {

// fill in process_id array
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"summ_mimegroup:summ_mime:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"summ_mimegroup:summ_mime:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filename () {

// fill in filename array
	CHAR filename[64];
	CHAR filenameattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(filename, (size_t) 64, "filename_%d", i + thistime + 1 );
		sprintf(filenameattr, "summ_mime:%s", filename);
		LdasArray< CHAR > filename_vector ( filename, strlen(filename),
			"summ_mimegroup:summ_mime:filename" );
		filename_vector.setComment( filenameattr );
		filename_cont.push_back( filename_vector );
	}
}

static void setup_submitter () {

// fill in submitter array
	CHAR submitter[48];
	CHAR submitterattr[248];

	for (int i=0; i < numElements; i++) {
		snprintf(submitter, (size_t) 48, "submitter_%d", i + thistime + 1 );
		sprintf(submitterattr, "summ_mime:%s", submitter);
		LdasArray< CHAR > submitter_vector ( submitter, strlen(submitter),
			"summ_mimegroup:summ_mime:submitter" );
		submitter_vector.setComment( submitterattr );
		submitter_cont.push_back( submitter_vector );
	}
}

static void setup_frameset_group () {

// fill in frameset_group array
	CHAR frameset_group[48];
	CHAR frameset_groupattr[248];

	for (int i=0; i < numElements; i++) {
		strcpy(frameset_group, frameset[ i % frameset_len ].FRAMESET_GROUP);
		sprintf(frameset_groupattr, "summ_mime:%s", frameset_group);
		LdasArray< CHAR > frameset_group_vector ( frameset_group, strlen(frameset_group),
			"summ_mimegroup:summ_mime:frameset_group" );
		frameset_group_vector.setComment( frameset_groupattr );
		frameset_group_cont.push_back( frameset_group_vector );
	}
}

static void setup_segment_group () {

// fill in segment_group array
	CHAR segment_group[64];
	CHAR segment_groupattr[264];

	for (int i=0; i < numElements; i++) {
		strcpy(segment_group, segment[ i % segment_len ].SEGMENT_GROUP);
		sprintf(segment_groupattr, "summ_mime:%s", segment_group);
		LdasArray< CHAR > segment_group_vector ( segment_group, strlen(segment_group),
			"summ_mimegroup:summ_mime:segment_group" );
		segment_group_vector.setComment( segment_groupattr );
		segment_group_cont.push_back( segment_group_vector );
	}
}

static void setup_version () {

// fill in version array
	for (int i=0; i < numElements; i++) {
		local::version[i] = atol( segment[ i % segment_len ].VERSION_local);
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] =  (INT_4S) atol (segment[ i % segment_len ].START_TIME);
	}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] =  (INT_4S) atol(segment[ i % segment_len ].END_TIME);
	}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_channel () {

// fill in channel array
	CHAR channel[240];
	CHAR channelattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 240, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "summ_mime:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"summ_mimegroup:summ_mime:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_descrip () {

// fill in descrip array
	CHAR descrip[64];
	CHAR descripattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(descrip, (size_t) 64, "descrip_%d", i + thistime + 1 );
		sprintf(descripattr, "summ_mime:%s", descrip);
		LdasArray< CHAR > descrip_vector ( descrip, strlen(descrip),
			"summ_mimegroup:summ_mime:descrip" );
		descrip_vector.setComment( descripattr );
		descrip_cont.push_back( descrip_vector );
	}
}

static void setup_mimedata () {

// fill in mimedata array
// NOT NULL
// fill in mimedata array
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}
	mimedata_cont.setComment("mimedata blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {
		sprintf (name, "%s", "mimedatagroup:summ_mime:mimedata");
		size_t size( ( i % 100 + 1 )  * BLOBSIZE );
		const CHAR_U fill_char( i%UCHAR_MAX );
		CHAR_U buffer[ size ];
        fill( buffer, buffer + size, fill_char );

		try 
		{
			LdasArray< CHAR_U >* char_u_vector = new 
		   		LdasArray< CHAR_U >( buffer, size, name, units );
			// create variable size blobs
			mimedata_cont.push_back( char_u_vector, 
           		LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		}
			catch( const std::exception& exc )
		{
		  	cout << "Exception: " << exc.what() << endl;
           	exit( 1 );
		}
           	catch( const FormatException& exc )
		{
           	const size_t exc_size( exc.getSize() );
           	cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		}		
	}
	mimedata_cont.setWriteFormat(ILwd::BASE64);
	mimedata_cont.setWriteCompression( ILwd::GZIP0);
}

static void setup_mimedata_length () {

// fill in mimedata_length array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		mimedata_length[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_mimetype () {

// fill in mimetype array
// NOT NULL
	CHAR mimetype[64];
	CHAR mimetypeattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(mimetype, (size_t) 64, "mimetype_%d", i + thistime + 1 );
		sprintf(mimetypeattr, "summ_mime:%s", mimetype);
		LdasArray< CHAR > mimetype_vector ( mimetype, strlen(mimetype),
			"summ_mimegroup:summ_mime:mimetype" );
		mimetype_vector.setComment( mimetypeattr );
		mimetype_cont.push_back( mimetype_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[240];
	CHAR commentattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 240, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "summ_mime:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"summ_mimegroup:summ_mime:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

static void setup_summ_mime_id () {

// fill in summ_mime_id array
// NOT NULL
// summ_mime_id is uniqueId, generated by metadataAPI

}

void setup_summ_mime ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_origin();
	ilwd3summ_mime.push_back(  origin_cont ) ;
	setup_process_id();
	ilwd3summ_mime.push_back( process_id_cont ) ;
	setup_filename();
	ilwd3summ_mime.push_back(  filename_cont ) ;
	setup_submitter();
	ilwd3summ_mime.push_back(  submitter_cont ) ;
	setup_frameset_group();
//	ilwd3summ_mime.push_back(  frameset_group_cont ) ;
	setup_segment_group();
	ilwd3summ_mime.push_back(  segment_group_cont ) ;
	setup_version();
	ilwd3summ_mime.push_back(  LdasArray< INT_4S >(local::version, numElements,
	"summ_mimegroup:summ_mime:version" ) );
	setup_start_time();
	ilwd3summ_mime.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"summ_mimegroup:summ_mime:start_time" ) );
	setup_start_time_ns();
	ilwd3summ_mime.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"summ_mimegroup:summ_mime:start_time_ns" ) );
	setup_end_time();
	ilwd3summ_mime.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"summ_mimegroup:summ_mime:end_time" ) );
	setup_end_time_ns();
	ilwd3summ_mime.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"summ_mimegroup:summ_mime:end_time_ns" ) );
	setup_channel();
	ilwd3summ_mime.push_back(  channel_cont ) ;
	setup_descrip();
	ilwd3summ_mime.push_back(  descrip_cont ) ;
	setup_mimedata();
	ilwd3summ_mime.push_back( mimedata_cont ) ;
	setup_mimedata_length();
	ilwd3summ_mime.push_back(  LdasArray< INT_4S >(mimedata_length, numElements,
	"summ_mimegroup:summ_mime:mimedata_length" ) );
	setup_mimetype();
	ilwd3summ_mime.push_back(  mimetype_cont ) ;
	setup_comment();
	ilwd3summ_mime.push_back(  comment_cont ) ;
	setup_summ_mime_id();
}

#ifdef TESTMAIN 
 void setup_summ_mime( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_summ_mime( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3summ_mime );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
