#include <cstring>
#include <algorithm>
#include <iostream>
#include <climits>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_transdata( "sngl_transdatagroup:sngl_transdata:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_transdatagroup:sngl_transdata:process_id" ); 
	LdasContainer event_table_cont ( "sngl_transdatagroup:sngl_transdata:event_table" ); 
	LdasContainer event_id_cont ( "sngl_transdatagroup:sngl_transdata:event_id" ); 
	LdasContainer ifo_cont ( "sngl_transdatagroup:sngl_transdata:ifo" ); 
	LdasContainer transdata_name_cont ( "sngl_transdatagroup:sngl_transdata:transdata_name" ); 
	INT_4S	dimensions[ARRAYSIZE];
	INT_4S	x_bins[ARRAYSIZE];
	REAL_8	x_start[ARRAYSIZE];
	REAL_8	x_end[ARRAYSIZE];
	LdasContainer x_units_cont ( "sngl_transdatagroup:sngl_transdata:x_units" ); 
	INT_4S	y_bins[ARRAYSIZE];
	REAL_8	y_start[ARRAYSIZE];
	REAL_8	y_end[ARRAYSIZE];
	LdasContainer y_units_cont ( "sngl_transdatagroup:sngl_transdata:y_units" ); 
	LdasContainer data_type_cont ( "sngl_transdatagroup:sngl_transdata:data_type" ); 
	LdasContainer data_units_cont ( "sngl_transdatagroup:sngl_transdata:data_units" ); 
	LdasContainer transdata_cont ( "sngl_transdatagroup:sngl_transdata:transdata" ); 
	INT_4S	transdata_length[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"events.h"
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"events_symref.h"
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_transdatagroup:sngl_transdata:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_transdatagroup:sngl_transdata:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_event_table () {

// fill in event_table array
// NOT NULL
// fill in event_table array
// NOT NULL
	CHAR event_table[18];
	CHAR event_tableattr[218];
	for (int i=0; i < numElements; i++) {
		sprintf(event_table, "%s", events[i].TABLE);
		sprintf(event_tableattr, "event_table:%s", event_table);
		LdasArray< CHAR > event_table_vector ( event_table, strlen(event_table),
		"sngl_transdatagroup:sngl_transdata:event_table" );
	event_table_vector.setComment( event_tableattr );
	event_table_cont.push_back( event_table_vector );}
}

static void setup_event_id () {

// fill in event_id array
// primary key
// NOT NULL
	char event_id_str[30] ;
	CHAR_U event_id[13];
	CHAR event_idattr[200];

	int i,j;
// convert event_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(event_id_str, events[ i % events_len	].EVENT_ID);
		for ( int x = 0, j = 0; x < strlen(event_id_str); x=x+2, j++ ) {
			event_id[j] = ((event_id_str[x]-'0' ) << 4) + (event_id_str[x+1]-'0');
		}
		sprintf(event_idattr, "event_id:%s", event_id_str);
		LdasArray< CHAR_U > event_id_vector ( event_id, 13,
		"sngl_transdatagroup:sngl_transdata:event_id" );
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );
	}
}

static void setup_event_id_symref () {
// fill in event_id array
// NOT NULL
	CHAR event_id[30] ;
	CHAR event_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(event_id, events[ i % events_len ].EVENT_ID );
		sprintf( event_idattr, "event_id:%s", event_id );
		LdasArray< CHAR > event_id_vector ( event_id, strlen(event_id),
		"sngl_transdatagroup:sngl_transdata:event_id");
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_transdata:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_transdatagroup:sngl_transdata:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_transdata_name () {

// fill in transdata_name array
// primary key
// NOT NULL
	CHAR transdata_name[32];
	CHAR transdata_nameattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(transdata_name, (size_t) 32, "transdata_name_%d", i + thistime + 1 );
		sprintf(transdata_nameattr, "sngl_transdata:%s", transdata_name);
		LdasArray< CHAR > transdata_name_vector ( transdata_name, strlen(transdata_name),
			"sngl_transdatagroup:sngl_transdata:transdata_name" );
		transdata_name_vector.setComment( transdata_nameattr );
		transdata_name_cont.push_back( transdata_name_vector );
	}
}

static void setup_dimensions () {

// fill in dimensions array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		dimensions[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_x_bins () {

// fill in x_bins array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		x_bins[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_x_start () {

// fill in x_start array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		x_start[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_x_end () {

// fill in x_end array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		x_end[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_x_units () {

// fill in x_units array
// NOT NULL
	CHAR x_units[16];
	CHAR x_unitsattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(x_units, (size_t) 16, "x_units_%d", i + thistime + 1 );
		sprintf(x_unitsattr, "sngl_transdata:%s", x_units);
		LdasArray< CHAR > x_units_vector ( x_units, strlen(x_units),
			"sngl_transdatagroup:sngl_transdata:x_units" );
		x_units_vector.setComment( x_unitsattr );
		x_units_cont.push_back( x_units_vector );
	}
}

static void setup_y_bins () {

// fill in y_bins array
	for (int i=0; i < numElements; i++) {
		y_bins[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_y_start () {

// fill in y_start array
	for (int i=0; i < numElements; i++) {
		y_start[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_y_end () {

// fill in y_end array
	for (int i=0; i < numElements; i++) {
		y_end[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_y_units () {

// fill in y_units array
	CHAR y_units[16];
	CHAR y_unitsattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(y_units, (size_t) 16, "y_units_%d", i + thistime + 1 );
		sprintf(y_unitsattr, "sngl_transdata:%s", y_units);
		LdasArray< CHAR > y_units_vector ( y_units, strlen(y_units),
			"sngl_transdatagroup:sngl_transdata:y_units" );
		y_units_vector.setComment( y_unitsattr );
		y_units_cont.push_back( y_units_vector );
	}
}

static void setup_data_type () {

// fill in data_type array
// NOT NULL
	CHAR data_type[16];
	CHAR data_typeattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(data_type, (size_t) 16, "data_type_%d", i + thistime + 1 );
		sprintf(data_typeattr, "sngl_transdata:%s", data_type);
		LdasArray< CHAR > data_type_vector ( data_type, strlen(data_type),
			"sngl_transdatagroup:sngl_transdata:data_type" );
		data_type_vector.setComment( data_typeattr );
		data_type_cont.push_back( data_type_vector );
	}
}

static void setup_data_units () {

// fill in data_units array
// NOT NULL
	CHAR data_units[16];
	CHAR data_unitsattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(data_units, (size_t) 16, "data_units_%d", i + thistime + 1 );
		sprintf(data_unitsattr, "sngl_transdata:%s", data_units);
		LdasArray< CHAR > data_units_vector ( data_units, strlen(data_units),
			"sngl_transdatagroup:sngl_transdata:data_units" );
		data_units_vector.setComment( data_unitsattr );
		data_units_cont.push_back( data_units_vector );
	}
}

static void setup_transdata () {

// fill in transdata array
// NOT NULL
// fill in transdata array
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}
	transdata_cont.setComment("transdata blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {
		sprintf (name, "%s", "transdatagroup:sngl_transdata:transdata");
		size_t size( ( i % 100 + 1 )  * BLOBSIZE );
        const CHAR_U fill_char( i%UCHAR_MAX );

        CHAR_U buffer[ size ];
        fill( buffer, buffer + size, fill_char );

		try {
			LdasArray< CHAR_U >* char_u_vector = new 
		   		LdasArray< CHAR_U >( buffer, size, name, units );
			// create variable size blobs
			transdata_cont.push_back( char_u_vector, 
           		LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		} 
			catch( const std::exception& exc )
		{
		  	cout << "Exception: " << exc.what() << endl;
           	exit( 1 );
		}
           	catch( const FormatException& exc )
		{
            const size_t exc_size( exc.getSize() );
            cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		}
	}
	transdata_cont.setWriteFormat(ILwd::BASE64);
	transdata_cont.setWriteCompression( ILwd::GZIP0);
}

static void setup_transdata_length () {

// fill in transdata_length array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		transdata_length[i] = (INT_4S) i + thistime + 1 ;
	}
}

void setup_sngl_transdata ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_transdata.push_back( process_id_cont ) ;
	setup_event_table();
	ilwd3sngl_transdata.push_back(  event_table_cont ) ;
	setup_event_id();
	ilwd3sngl_transdata.push_back( event_id_cont ) ;
	setup_ifo();
	ilwd3sngl_transdata.push_back(  ifo_cont ) ;
	setup_transdata_name();
	ilwd3sngl_transdata.push_back(  transdata_name_cont ) ;
	setup_dimensions();
	ilwd3sngl_transdata.push_back(  LdasArray< INT_4S >(dimensions, numElements,
	"sngl_transdatagroup:sngl_transdata:dimensions" ) );
	setup_x_bins();
	ilwd3sngl_transdata.push_back(  LdasArray< INT_4S >(x_bins, numElements,
	"sngl_transdatagroup:sngl_transdata:x_bins" ) );
	setup_x_start();
	ilwd3sngl_transdata.push_back(  LdasArray< REAL_8 >( x_start, numElements,
	"sngl_transdatagroup:sngl_transdata:x_start" ) );
	setup_x_end();
	ilwd3sngl_transdata.push_back(  LdasArray< REAL_8 >( x_end, numElements,
	"sngl_transdatagroup:sngl_transdata:x_end" ) );
	setup_x_units();
	ilwd3sngl_transdata.push_back(  x_units_cont ) ;
	setup_y_bins();
	ilwd3sngl_transdata.push_back(  LdasArray< INT_4S >(y_bins, numElements,
	"sngl_transdatagroup:sngl_transdata:y_bins" ) );
	setup_y_start();
	ilwd3sngl_transdata.push_back(  LdasArray< REAL_8 >( y_start, numElements,
	"sngl_transdatagroup:sngl_transdata:y_start" ) );
	setup_y_end();
	ilwd3sngl_transdata.push_back(  LdasArray< REAL_8 >( y_end, numElements,
	"sngl_transdatagroup:sngl_transdata:y_end" ) );
	setup_y_units();
	ilwd3sngl_transdata.push_back(  y_units_cont ) ;
	setup_data_type();
	ilwd3sngl_transdata.push_back(  data_type_cont ) ;
	setup_data_units();
	ilwd3sngl_transdata.push_back(  data_units_cont ) ;
	setup_transdata();
	ilwd3sngl_transdata.push_back( transdata_cont ) ;
	setup_transdata_length();
	ilwd3sngl_transdata.push_back(  LdasArray< INT_4S >(transdata_length, numElements,
	"sngl_transdatagroup:sngl_transdata:transdata_length" ) );
}

#ifdef TESTMAIN 
 void setup_sngl_transdata( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_transdata( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_transdata );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
