#include <string.h>

#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <values.h>

#define  FORMAT_UTC      "%a %b %e %H:%M:%S UTC %Y"
#define  FORMAT_LOCAL    "%a %b %e %H:%M:%S %Z %Y"

// create static variables 
namespace {
/*
* MJD of leapseconds (start with 10 prior to the first).
*/
int leap[] = {
41499,
41683,
42048,
42413,
42778,
43144,
43509,
43874,
44239,
44786,
45151,
45516,
46247,
47161,
47892,
48257,
48804,
49169,
49534,
50083,
50630,
51179,
MAXINT
};

} // end namespace 

static  void cp_tm ( struct tm *src, struct tm *dest ) 
{
    dest->tm_sec = src->tm_sec;    
    dest->tm_min = src->tm_min; 
    dest->tm_hour = src->tm_hour;    
    dest->tm_mday = src->tm_mday; 
    dest->tm_mon = src->tm_mon;    
    dest->tm_year = src->tm_year; 
    dest->tm_wday = src->tm_wday; 
    dest->tm_yday = src->tm_yday;   
    dest->tm_isdst = src->tm_isdst;               
}

void get_gpstime(double *gps_seconds, double *gps_nanosecs)
{
    char str[80];
    int UTC;
    int index;
    int tai_utc;
    int gps_utc;
    double unix_sec;
    double gps_sec;
    double tai_sec;
    double utc_sec;
    double fut, mjd, jd;
    struct timeval tp;
    struct tm utc;
    struct tm local;
    struct tm *dummy;

    struct tm *p_local;
    struct tm *p_utc;
    
    #if 1
    /* Current time */
    gettimeofday(&tp, NULL);
    #elif 1
    /* Random time */
    tp.tv_sec = 918702747;
    tp.tv_usec = 0;
    #elif 0
    /* Dec 1998 leap second */
    tp.tv_sec = 915148800;
    tp.tv_usec = 0;
    #elif 0
    /* GPS origin */
    tp.tv_sec = 315964800;
    tp.tv_usec = 0;
    #elif 0
    /* Unix origin */
    tp.tv_sec = 0;
    tp.tv_usec = 0;
    #elif 0
/*
* Example,  gps_sec=583239101.320000  1998-06-30T10:51:28.320000Z
*
* note, to make this example work you need to subtract 315964818 from
* tai_sec to get gps_sec (i.e., 1 sec different than what works to
* get gps_sec to equal 0 at the origin).
*/
    tp.tv_sec = 899203888;
    tp.tv_usec = 320000;
    #endif
    
    p_utc = gmtime(&tp.tv_sec);
    p_local = localtime(&tp.tv_sec);
    
    cp_tm ( p_utc, &utc ) ;
    cp_tm ( p_local, &local );
    
    UTC = (local.tm_sec == (tp.tv_sec%60));

    unix_sec = tp.tv_sec + tp.tv_usec/1e6;

    mjd = unix_sec/86400 + 39126 + 4*365 + 1;
    tai_utc = 10;        /* Number of leapseconds at Unix origin */
    gps_utc = -9;        /* Number of leapseconds at GPS origin  */
    index = 0;
    while (leap[index++] < mjd) {
        ++tai_utc;
        ++gps_utc;
    }

    utc_sec = unix_sec - ((UTC) ? 0 : tai_utc);
    tai_sec = unix_sec + ((UTC) ? tai_utc : 0);
    gps_sec = tai_sec - 315964819;

    fut = (utc_sec)/86400 + 4*365 + 1;
    mjd = fut + 39126;
    jd = 2440587.5 + (utc_sec)/86400;

#ifdef DEBUG
printf("gettimeofday() returns %s.\n", (UTC) ? "UTC" : "TAI(?)");
printf("\n");
printf("asctime(gmtime) = %s", asctime(&utc));
strftime(str, 80, FORMAT_UTC, &utc);
printf("strftime(gmtime) = %s\n", str);
printf("gmtime(): %02d:%02d:%02d\n",
    utc.tm_hour, utc.tm_min, utc.tm_sec);
printf("\n");

printf("asctime(localtime) = %s", asctime(&local));
strftime(str, 80, FORMAT_LOCAL, &local);
printf("strftime(localtime) = %s\n", str);
printf("localtime(): %02d:%02d:%02d\n",
    local.tm_hour, local.tm_min, local.tm_sec);

printf("\n");
printf("FUT = %.14f\n", fut);
printf("MJD = %.14f\n", mjd);
printf("JD  = %.14f\n", jd);

printf("\n");
printf("GPS:  %20.6f  seconds since 00:00:00 UTC,  6 January 1980\n",
    gps_sec);
printf("TAI:  %20.6f  seconds since 00:00:00 UTC,  1 January 1970\n",
    tai_sec);
printf("UNIX: %20.6f  seconds since 00:00:00 UTC,  1 January 1970\n",
    unix_sec);
printf("UTC:  %20.6f  seconds since 00:00:00 UTC,  1 January 1970\n",
    utc_sec);

printf("\n");
printf("TAI-UTC: %d seconds\n", tai_utc);
printf("GPS-UTC: %d seconds\n", gps_utc);

printf("\n");
printf("isdst = %d\n", local.tm_isdst);
printf("tznam[0] = %s\n", tzname[0]);
printf("tzname[1] = %s\n", tzname[1]);
#endif
    
    *gps_seconds = gps_sec;
    *gps_nanosecs = tp.tv_usec * 1000 ;
}

#ifdef DEBUG
int main() 
{
    void get_gpstime(double *, double *);
    double gps_secs, gps_nanosecs;
    
    get_gpstime(&gps_secs, &gps_nanosecs);
    
    printf("GPS:  %20.6f  seconds, %20.6f nanosecs since 00:00:00 UTC,  6 January 1980\n",
    gps_secs, gps_nanosecs );
}
#endif

void    get_timestamp ( char *timestamp ) 
{
    struct timeval tp;
    struct tm local;
    struct tm *p_local;

    gettimeofday( &tp, NULL );
    p_local = localtime(&tp.tv_sec);
    cp_tm ( p_local, &local );
    sprintf ( timestamp, "%4.4d-%2.2d-%2.2d-%2.2d.%2.2d.%2.2d", 
        1900+local.tm_year, local.tm_mon+1, local.tm_mday, 
        local.tm_hour, local.tm_min, local.tm_sec);
}

