#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sim_inst_params( "sim_inst_paramsgroup:sim_inst_params:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer simulation_id_cont ( "sim_inst_paramsgroup:sim_inst_params:simulation_id" ); 
	LdasContainer name_cont ( "sim_inst_paramsgroup:sim_inst_params:name" ); 
	LdasContainer comment_cont ( "sim_inst_paramsgroup:sim_inst_params:comment" ); 
	REAL_8	value[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"sim_inst.h"
#endif

#ifdef  TESTSYMREF
#include	"sim_inst_symref.h"
#endif

using namespace local;

static void setup_simulation_id () {

// fill in simulation_id array
// NOT NULL
	char simulation_id_str[30] ;
	CHAR_U simulation_id[13];
	CHAR simulation_idattr[200];

	int i,j;
// convert simulation_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(simulation_id_str, sim_inst[ i % sim_inst_len	].SIMULATION_ID);
		for ( int x = 0, j = 0; x < strlen(simulation_id_str); x=x+2, j++ ) {
			simulation_id[j] = ((simulation_id_str[x]-'0' ) << 4) + (simulation_id_str[x+1]-'0');
		}
		sprintf(simulation_idattr, "simulation_id:%s", simulation_id_str);
		LdasArray< CHAR_U > simulation_id_vector ( simulation_id, 13,
		"sim_inst_paramsgroup:sim_inst_params:simulation_id" );
		simulation_id_vector.setComment(simulation_idattr);
		simulation_id_cont.push_back( simulation_id_vector );
	}
}

static void setup_simulation_id_symref () {
// fill in simulation_id array
// NOT NULL
	CHAR simulation_id[30] ;
	CHAR simulation_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(simulation_id, sim_inst[ i % sim_inst_len ].SIMULATION_ID );
		sprintf( simulation_idattr, "simulation_id:%s", simulation_id );
		LdasArray< CHAR > simulation_id_vector ( simulation_id, strlen(simulation_id),
		"sim_inst_paramsgroup:sim_inst_params:simulation_id");
		simulation_id_vector.setComment(simulation_idattr);
		simulation_id_cont.push_back( simulation_id_vector );}
}

static void setup_name () {

// fill in name array
// NOT NULL
	CHAR name[24];
	CHAR nameattr[224];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 24, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "sim_inst_params:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"sim_inst_paramsgroup:sim_inst_params:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[64];
	CHAR commentattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 64, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "sim_inst_params:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"sim_inst_paramsgroup:sim_inst_params:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

static void setup_value () {

// fill in value array
	for (int i=0; i < numElements; i++) {
		value[i] = (REAL_8) i + thistime + 1 ;
	}
}

void setup_sim_inst_params ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_simulation_id();
	ilwd3sim_inst_params.push_back( simulation_id_cont ) ;
	setup_name();
	ilwd3sim_inst_params.push_back(  name_cont ) ;
	setup_comment();
	ilwd3sim_inst_params.push_back(  comment_cont ) ;
	setup_value();
	ilwd3sim_inst_params.push_back(  LdasArray< REAL_8 >( value, numElements,
	"sim_inst_paramsgroup:sim_inst_params:value" ) );
}

#ifdef TESTMAIN 
 void setup_sim_inst_params( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sim_inst_params( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sim_inst_params );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
