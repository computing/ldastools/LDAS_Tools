#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_ringdown( "sngl_ringdowngroup:sngl_ringdown:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_ringdowngroup:sngl_ringdown:process_id" ); 
	LdasContainer filter_id_cont ( "sngl_ringdowngroup:sngl_ringdown:filter_id" ); 
	LdasContainer ifo_cont ( "sngl_ringdowngroup:sngl_ringdown:ifo" ); 
	LdasContainer channel_cont ( "sngl_ringdowngroup:sngl_ringdown:channel" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	REAL_4	duration[ARRAYSIZE];
	REAL_4	amplitude[ARRAYSIZE];
	REAL_4	frequency[ARRAYSIZE];
	REAL_4	q[ARRAYSIZE];
	REAL_4	mass[ARRAYSIZE];
	REAL_4	snr[ARRAYSIZE];
	REAL_4	confidence[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"filter.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"filter_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_ringdowngroup:sngl_ringdown:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_ringdowngroup:sngl_ringdown:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filter_id () {

// fill in filter_id array
	char filter_id_str[30] ;
	CHAR_U filter_id[13];
	CHAR filter_idattr[200];

	int i,j;
// convert filter_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id_str, filter[ i % filter_len	].FILTER_ID);
		for ( int x = 0, j = 0; x < strlen(filter_id_str); x=x+2, j++ ) {
			filter_id[j] = ((filter_id_str[x]-'0' ) << 4) + (filter_id_str[x+1]-'0');
		}
		sprintf(filter_idattr, "filter_id:%s", filter_id_str);
		LdasArray< CHAR_U > filter_id_vector ( filter_id, 13,
		"sngl_ringdowngroup:sngl_ringdown:filter_id" );
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );
	}
}

static void setup_filter_id_symref () {
// fill in filter_id array
// NOT NULL
	CHAR filter_id[30] ;
	CHAR filter_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id, filter[ i % filter_len ].FILTER_ID );
		sprintf( filter_idattr, "filter_id:%s", filter_id );
		LdasArray< CHAR > filter_id_vector ( filter_id, strlen(filter_id),
		"sngl_ringdowngroup:sngl_ringdown:filter_id");
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_ringdown:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_ringdowngroup:sngl_ringdown:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_channel () {

// fill in channel array
	CHAR channel[64];
	CHAR channelattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 64, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "sngl_ringdown:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"sngl_ringdowngroup:sngl_ringdown:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_duration () {

// fill in duration array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		duration[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_amplitude () {

// fill in amplitude array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		amplitude[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_frequency () {

// fill in frequency array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		frequency[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_q () {

// fill in q array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		q[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_mass () {

// fill in mass array
	for (int i=0; i < numElements; i++) {
		mass[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_snr () {

// fill in snr array
	for (int i=0; i < numElements; i++) {
		snr[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_confidence () {

// fill in confidence array
	for (int i=0; i < numElements; i++) {
		confidence[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
// event_id is uniqueId, generated by metadataAPI

}

void setup_sngl_ringdown ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_ringdown.push_back( process_id_cont ) ;
	setup_filter_id();
	ilwd3sngl_ringdown.push_back( filter_id_cont ) ;
	setup_ifo();
	ilwd3sngl_ringdown.push_back(  ifo_cont ) ;
	setup_channel();
	ilwd3sngl_ringdown.push_back(  channel_cont ) ;
	setup_start_time();
	ilwd3sngl_ringdown.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"sngl_ringdowngroup:sngl_ringdown:start_time" ) );
	setup_start_time_ns();
	ilwd3sngl_ringdown.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"sngl_ringdowngroup:sngl_ringdown:start_time_ns" ) );
	setup_duration();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( duration, numElements,
	"sngl_ringdowngroup:sngl_ringdown:duration" ) );
	setup_amplitude();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( amplitude, numElements,
	"sngl_ringdowngroup:sngl_ringdown:amplitude" ) );
	setup_frequency();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( frequency, numElements,
	"sngl_ringdowngroup:sngl_ringdown:frequency" ) );
	setup_q();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( q, numElements,
	"sngl_ringdowngroup:sngl_ringdown:q" ) );
	setup_mass();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( mass, numElements,
	"sngl_ringdowngroup:sngl_ringdown:mass" ) );
	setup_snr();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( snr, numElements,
	"sngl_ringdowngroup:sngl_ringdown:snr" ) );
	setup_confidence();
	ilwd3sngl_ringdown.push_back(  LdasArray< REAL_4 >( confidence, numElements,
	"sngl_ringdowngroup:sngl_ringdown:confidence" ) );
	setup_event_id();
}

#ifdef TESTMAIN 
 void setup_sngl_ringdown( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_ringdown( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_ringdown );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
