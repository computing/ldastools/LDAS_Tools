#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_datasource( "sngl_datasourcegroup:sngl_datasource:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_datasourcegroup:sngl_datasource:process_id" ); 
	LdasContainer event_table_cont ( "sngl_datasourcegroup:sngl_datasource:event_table" ); 
	LdasContainer event_id_cont ( "sngl_datasourcegroup:sngl_datasource:event_id" ); 
	LdasContainer ifo_cont ( "sngl_datasourcegroup:sngl_datasource:ifo" ); 
	LdasContainer data_source_cont ( "sngl_datasourcegroup:sngl_datasource:data_source" ); 
	LdasContainer channels_cont ( "sngl_datasourcegroup:sngl_datasource:channels" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"events.h"
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"events_symref.h"
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_datasourcegroup:sngl_datasource:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_datasourcegroup:sngl_datasource:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_event_table () {

// fill in event_table array
// NOT NULL
// fill in event_table array
// NOT NULL
	CHAR event_table[18];
	CHAR event_tableattr[218];
	for (int i=0; i < numElements; i++) {
		sprintf(event_table, "%s", events[i].TABLE);
		sprintf(event_tableattr, "event_table:%s", event_table);
		LdasArray< CHAR > event_table_vector ( event_table, strlen(event_table),
		"sngl_datasourcegroup:sngl_datasource:event_table" );
	event_table_vector.setComment( event_tableattr );
	event_table_cont.push_back( event_table_vector );}
}

static void setup_event_id () {

// fill in event_id array
// primary key
// NOT NULL
	char event_id_str[30] ;
	CHAR_U event_id[13];
	CHAR event_idattr[200];

	int i,j;
// convert event_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(event_id_str, events[ i % events_len	].EVENT_ID);
		for ( int x = 0, j = 0; x < strlen(event_id_str); x=x+2, j++ ) {
			event_id[j] = ((event_id_str[x]-'0' ) << 4) + (event_id_str[x+1]-'0');
		}
		sprintf(event_idattr, "event_id:%s", event_id_str);
		LdasArray< CHAR_U > event_id_vector ( event_id, 13,
		"sngl_datasourcegroup:sngl_datasource:event_id" );
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );
	}
}

static void setup_event_id_symref () {
// fill in event_id array
// NOT NULL
	CHAR event_id[30] ;
	CHAR event_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(event_id, events[ i % events_len ].EVENT_ID );
		sprintf( event_idattr, "event_id:%s", event_id );
		LdasArray< CHAR > event_id_vector ( event_id, strlen(event_id),
		"sngl_datasourcegroup:sngl_datasource:event_id");
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_datasource:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_datasourcegroup:sngl_datasource:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_data_source () {

// fill in data_source array
	CHAR data_source[240];
	CHAR data_sourceattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(data_source, (size_t) 240, "data_source_%d", i + thistime + 1 );
		sprintf(data_sourceattr, "sngl_datasource:%s", data_source);
		LdasArray< CHAR > data_source_vector ( data_source, strlen(data_source),
			"sngl_datasourcegroup:sngl_datasource:data_source" );
		data_source_vector.setComment( data_sourceattr );
		data_source_cont.push_back( data_source_vector );
	}
}

static void setup_channels () {

// fill in channels array
// NOT NULL
	CHAR channels[512];
	CHAR channelsattr[712];

	for (int i=0; i < numElements; i++) {
		snprintf(channels, (size_t) 512, "channels_%d", i + thistime + 1 );
		sprintf(channelsattr, "sngl_datasource:%s", channels);
		LdasArray< CHAR > channels_vector ( channels, strlen(channels),
			"sngl_datasourcegroup:sngl_datasource:channels" );
		channels_vector.setComment( channelsattr );
		channels_cont.push_back( channels_vector );
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] = (INT_4S) ( i + gps_secs );
}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

void setup_sngl_datasource ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_datasource.push_back( process_id_cont ) ;
	setup_event_table();
	ilwd3sngl_datasource.push_back(  event_table_cont ) ;
	setup_event_id();
	ilwd3sngl_datasource.push_back( event_id_cont ) ;
	setup_ifo();
	ilwd3sngl_datasource.push_back(  ifo_cont ) ;
	setup_data_source();
	ilwd3sngl_datasource.push_back(  data_source_cont ) ;
	setup_channels();
	ilwd3sngl_datasource.push_back(  channels_cont ) ;
	setup_start_time();
	ilwd3sngl_datasource.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"sngl_datasourcegroup:sngl_datasource:start_time" ) );
	setup_start_time_ns();
	ilwd3sngl_datasource.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"sngl_datasourcegroup:sngl_datasource:start_time_ns" ) );
	setup_end_time();
	ilwd3sngl_datasource.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"sngl_datasourcegroup:sngl_datasource:end_time" ) );
	setup_end_time_ns();
	ilwd3sngl_datasource.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"sngl_datasourcegroup:sngl_datasource:end_time_ns" ) );
}

#ifdef TESTMAIN 
 void setup_sngl_datasource( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_datasource( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_datasource );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
