#include <string.h>
#include <stdio.h>
#include <cmath>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include <ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern void get_gpstime(double *, double *);

LdasContainer ilwd3( "group:ilwdtypes:table" );

namespace {
    const INT_2U ARRAYSIZE = 10000;
	const INT_2U BLOBSIZE = 5; 
    int thistime = (int)time (0) ;
    double gps_secs;
    double gps_nanosecs;
    
    int numElements;
    LdasContainer type_char_s_cont ( "group:ilwdtypes:type_char_s" ); 
    LdasContainer type_char_u_cont ( "group:ilwdtypes:type_char_u" );  
    LdasContainer type_char_v_cont ( "group:ilwdtypes:type_char_v" ); 
    LdasContainer type_blob_cont   ( "group:ilwdtypes:type_blob" ); 
    LdasContainer type_clob_cont   ( "group:ilwdtypes:type_clob" );
     
    INT_2S	type_int_2s_arr[ARRAYSIZE];
	INT_2U	type_int_2u_arr[ARRAYSIZE];
    INT_4S	type_int_4s_arr[ARRAYSIZE];
	INT_4U	type_int_4u_arr[ARRAYSIZE];	
    INT_8S	type_int_8s_arr[ARRAYSIZE];
	INT_8U	type_int_8u_arr[ARRAYSIZE];
    REAL_4	type_real_4_arr[ARRAYSIZE];
	REAL_8	type_real_8_arr[ARRAYSIZE];	
    string  type_lstring_arr[ARRAYSIZE];
    
#include    "ilwdtypes.h"

} // end namespace

static void setup_char_s () {

    CHAR data_char_s[100];
    CHAR data_char_sattr[200];   
    
	for (int i=0; i < numElements; i++) {
	    sprintf(data_char_s, "P%d", i + thistime );
        sprintf(data_char_sattr, "type_char_s:%s", data_char_s);
        LdasArray< CHAR > type_char_s_vector ( data_char_s, strlen(data_char_s),
            "group:ilwdtypes:type_char_s");
        type_char_s_vector.setComment(data_char_sattr);
        type_char_s_cont.push_back( type_char_s_vector);
				// set some values to null for testing null masks
		if	( ( i % 2 ) != 0 || i == 0 ) {
		    type_char_s_cont.setNull( i, true );			
		}
	}
	//type_char_s_cont.write(0, 4, cerr, ILwd::ASCII, ILwd::NO_COMPRESSION );
}

static void setup_char_v () {

    CHAR data_char_v[100];
    CHAR data_char_vattr[200];   
    
	for (int i=0; i < numElements; i++) {
	    sprintf(data_char_v, "P%d", i + thistime );
        sprintf(data_char_vattr, "type_char_v:%s", data_char_v);
        LdasArray< CHAR > type_char_v_vector ( data_char_v, strlen(data_char_v),
            "group:ilwdtypes:type_char_v");
        type_char_v_vector.setComment(data_char_vattr);
        type_char_v_cont.push_back( type_char_v_vector);
				// set some values to null for testing null masks
		if	( ( i % 2 ) != 0 || i == 0 ) {
		    type_char_v_cont.setNull( i, true );			
		}
	}
	//type_char_s_cont.write(0, 4, cerr, ILwd::ASCII, ILwd::NO_COMPRESSION );
}

// currently db2 does not support unsigned ints
// can be null
static void setup_int_2s () {

// fill in for int_2s
	for (int i=0; i < numElements; i++) {
		if	( i % 2 ) {
	    	type_int_2s_arr[i] = (INT_2S)  ( pow (2, 15 ) -  i );
		} else {
			type_int_2s_arr[i] = - ((INT_2S)  ( pow (2, 15 ) -  i ));
		}
	}
}

// can be null
static void setup_int_2u () {

// fill in for int_2u
	for (int i=0; i < numElements; i++) {
	    //type_int_2u_arr[i] = (INT_2U) ( pow(2,16) - i - 1 );
		type_int_2u_arr[i] = (INT_2U) ( pow(2,15) - i );
	}
}

// can be null
static void setup_int_4s () {

// fill in for int_4s
	for (int i=0; i < numElements; i++) {		
		if	( i % 2 ) {
	    	type_int_4s_arr[i] = (INT_4S) ( pow(2,31) - i - 1  );
		} else {
			type_int_4s_arr[i] = (INT_4S) ( pow(2,31) - i - 1  );
		}
	}
}

// can be null
static void setup_int_4u () {

// fill in for int_4s
	for (int i=0; i < numElements; i++) {
	    //type_int_4u_arr[i] = (INT_4U) ( pow( 2, 32 ) - i - 1  );
		type_int_4u_arr[i] = (INT_4U) ( pow( 2, 31 ) - i );
	}
}

static void setup_int_8s () {
// fill in for int_8s
	for (int i=0; i < numElements; i++) {
		if	( i % 2 ) {
	    	type_int_8s_arr[i] = (INT_8S) ( pow( 2, 63 ) - i - 1 );
		} else {
			type_int_8s_arr[i] = (INT_8S) ( pow( 2, 63 ) - i - 1 );
		}
	}
}

static void setup_int_8u () {
// fill in for int_8u
	for (int i=0; i < numElements; i++) {
	    // type_int_8u_arr[i] = (INT_8U) ( pow( 2, 64 ) - i - 1 );
		type_int_8u_arr[i] = (INT_8U) ( pow( 2, 63 ) - i - 1 );
	}
}

// can be null, unable to use the max values
static void setup_real_4 () {

// fill in for real_4
	for (int i=0; i < numElements; i++) {
	    type_real_4_arr[i] = (REAL_4) ( i + thistime );
	}
}

// can be null
static void setup_real_8 () {

// fill in for real_8
	for (int i=0; i < numElements; i++) {
	    type_real_8_arr[i] = (REAL_8) ( i + thistime );
	}
}

static void setup_char_u () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];
    
	int i,j;
    // convert program string to 13 bit BCD
    for (int i=0; i < numElements; i++) {
        strcpy(process_id_str, process[ i % process_len ].PROCESS_ID);
	    for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
		    process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
	    }
		sprintf(process_idattr, "type_char_u:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13, 
            "group:ilwdtypes:type_char_u" );
        process_id_vector.setComment(process_idattr);
		type_char_u_cont.push_back( process_id_vector );
		if	( ( i % 2 ) != 0 || i == 0 ) {
		    type_char_u_cont.setNull( i, true );			
		}		
	}
	//type_char_u_cont.write(0, 4, cerr, ILwd::ASCII, ILwd::NO_COMPRESSION );
}

static void setup_blob () {

// fill in spectrum array
// NOT NULL
	double blob[ BLOBSIZE*1000] ;
    int i ;
    for ( i = 0; i < 1000*BLOBSIZE ; i++ ) {
        blob[i] = 42949672 + i ;
    }
    
	CHAR_U buffer[1024*1000];
	    
    type_blob_cont.setComment("blob arrays");
    
    char name[100];
    char units[]="blob_units";
    
	// unable to allocate memory on the heap if over 11.8 MB
	// [IBM][CLI Driver][DB2/SUN] SQL0973N  
	// Not enough storage is available in the "QUERY_HEAP" heap 
	// to process the statement.  SQLSTATE=57011
	// very hard to get enough memory to validate it.
	
    for ( i = 0; i < numElements ; i++ ) {
        sprintf (name,  "ilwdtypes:type_blob:blob%d", i);
        size_t size( ( i+1) * BLOBSIZE );
        memcpy(buffer, blob, size);
        LdasArray< CHAR_U > char_u_vector ( buffer, size ,
            name, units );
    // create variable size blobs 
        type_blob_cont.push_back( char_u_vector );
		if	( ( i % 2 ) != 0 || i == 0 ) {
		    type_blob_cont.setNull( i, true );			
		}			
		
    }
    type_blob_cont.setWriteFormat(ILwd::BASE64);
    type_blob_cont.setWriteCompression( ILwd::GZIP0);
}

// A Character Large OBject (CLOB) is a varying-length string measured in bytes 
// that can be up to 2 gigabytes (2 147 483 647 bytes) long. A CLOB is used to store large
// SBCS or mixed (SBCS and MBCS) character-based data such as documents written 
// with a single character set (and, therefore, has an SBCS or mixed code page
// associated with it). Note that a CLOB is considered to be a character string. 

// For testing, read in an html as clob
static int setup_clob ( char *fname ) {

    FILE *fp;
    type_clob_cont.setComment("clob arrays");
    CHAR data_clob[5120];
    CHAR data_clob_attr[200];   
    int i, num;
    char name[100];
    
    if  (( fp = fopen (fname, "r") ) == NULL ) {
        fprintf(stderr,"clob file $fname open failed\n");
        return(0) ;
    }
    num = fread(data_clob, (size_t)5120, (size_t) 1, fp) ;
    fprintf(stderr, "read in %d bytes\n", num);
    
    fclose(fp);
    for ( i = 0; i < numElements ; i++ ) {
        sprintf(name,"ilwdtypes:type_clob:clob%d", i ); 
        LdasArray< CHAR > type_clob_vector ( data_clob , strlen(data_clob),
           name );        
        sprintf(data_clob_attr, "type_clob:%d", i );
        type_clob_vector.setComment(data_clob_attr);
        type_clob_cont.push_back( type_clob_vector);
				// set some values to null for testing null masks
		if	( ( i % 2 ) != 0 || i == 0 ) {
		    type_clob_cont.setNull( i, true );			
		}
    }
    return(1);    
}

// fill in clob array
static void setup_lstring () {

// fill in ifo_site array
// primary key
	for ( unsigned i=0; i < numElements; i++) {
	    type_lstring_arr[i] = string("lstring example");
	}
}

template< class T >
static	void setup_nullmask ( LdasArray< T > *vector ) {

	for (int i=0; i < numElements; i++) {
		if	( ( i % 2 ) == 0 ) {
			vector->setNull(i, true );
		}
	}
}

void   setup_ilwdtypes ( int dims, char *fname ) {

    numElements = dims ;
    
	setup_char_s();
	ilwd3.push_back( type_char_s_cont ) ;
    setup_char_v();
	ilwd3.push_back( type_char_v_cont ) ;
	
	setup_int_2s();
	LdasArray< INT_2S > type_int_2s_vector ( type_int_2s_arr, numElements, 
		"group:ilwdtypes:type_int_2s" );
	setup_nullmask ( &type_int_2s_vector );
	ilwd3.push_back( type_int_2s_vector );
	
	setup_int_2u();
	LdasArray< INT_2U > type_int_2u_vector ( type_int_2u_arr, numElements, 
		"group:ilwdtypes:type_int_2u" );
	setup_nullmask ( &type_int_2u_vector );
	ilwd3.push_back( type_int_2u_vector );
	
	setup_int_4s();
	LdasArray< INT_4S > type_int_4s_vector ( type_int_4s_arr, numElements, 
		"group:ilwdtypes:type_int_4s" );
	setup_nullmask ( &type_int_4s_vector );
	ilwd3.push_back( type_int_4s_vector );
	
	setup_int_4u();
	LdasArray< INT_4U > type_int_4u_vector ( type_int_4u_arr, numElements, 
		"group:ilwdtypes:type_int_4u" );
	setup_nullmask ( &type_int_4u_vector );
	ilwd3.push_back( type_int_4u_vector );	

	setup_int_8s();
	LdasArray< INT_8S > type_int_8s_vector ( type_int_8s_arr, numElements, 
		"group:ilwdtypes:type_int_8s" );
	setup_nullmask ( &type_int_8s_vector );
	ilwd3.push_back( type_int_8s_vector );
	
	setup_int_8u();
	LdasArray< INT_8U > type_int_8u_vector ( type_int_8u_arr, numElements, 
		"group:ilwdtypes:type_int_8u" );
	setup_nullmask ( &type_int_8u_vector );
	ilwd3.push_back( type_int_8u_vector );
	
	setup_real_4();
	LdasArray< REAL_4 > type_real_4_vector ( type_real_4_arr, numElements, 
		"group:ilwdtypes:type_real_4" );
	setup_nullmask ( &type_real_4_vector );
	ilwd3.push_back( type_real_4_vector );
	
	setup_real_8();
	LdasArray< REAL_8 > type_real_8_vector ( type_real_8_arr, numElements, 
		"group:ilwdtypes:type_real_8" );
	setup_nullmask ( &type_real_8_vector );
	ilwd3.push_back( type_real_8_vector );	
	
	setup_char_u();
	ilwd3.push_back( type_char_u_cont );	
	
	setup_blob();
	ilwd3.push_back( type_blob_cont );	
	
    if  ( setup_clob( fname ) ) {
	    ilwd3.push_back( type_clob_cont );	
    } else {
        printf("error creating clob") ;
    }
	
    setup_lstring();
    char buffer[500];
    char units[100];
    sprintf (buffer, "element_test:%s:%d", "lstring", numElements);
    sprintf (units, "%s_units", "lstring" );
    LdasString type_lstring_cont ( type_lstring_arr, (size_t) numElements,
        "group:ilwdtypes:type_lstring" ,
        string(units) ) ;
    type_lstring_cont.setComment(buffer); 
	ilwd3.push_back( type_lstring_cont );   
}


#ifdef TESTMAIN
void setup_ilwdtypes( int dims, char * fname );

int main(int argc, char **argv) {
	LdasContainer ilwd1( "ligo:ldas:file" );
	setup_ilwdtypes( atoi ( argv[1] ), argv[2] );
	ilwd1.push_back( ilwd3 );
	ILwd::writeHeader(cout);
    // write binary
	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
    //ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
	cout << endl;
}
#endif 
