#! /ldcg/bin/tclsh

## ******************************************************** 
## putValidate.tcl
##
#! /ldcg/bin/tclsh

## Provides data retrieval functions for the LDAS.
##
## Release Date: Not Available
##
##  This script validates the ilwd input against the
##  the results of the select query to check the data inserted
##  
## ******************************************************** 

;#barecode

;## expand compressed char_u uniqueId to 26 chars
proc expand_uniqueId { compressed_id } {
	set idstr ""
	foreach num $compressed_id {
		set idstr "$idstr[ format "%02.2x" $num ]"	
	}
	return "x'$idstr'"
}

;## returns an ascii for numeric chars
proc char_to_str { chardata } {
	if	{ [ regexp {^[0-9 ]+$} $chardata ] } {
		set str ""
		foreach num $chardata {
			set str "$str[format "%c" $num]"
		}	
	} else {
		set str [ join $chardata ]
	}
	regsub -all {\n} $str {} str
	return $str
}

;## returns an ascii for numeric chars
proc blob_to_str { blob_data } {
	if	{ [ regexp {^[0-9 ]+$} $blob_data ] } {
		set str ""
		foreach num $blob_data {
			set str "$str[format "%02.2X" $num]"
		}	
	} else {
		set str [ join $blob_data ]
	}
	regsub -all {\n} $str {} str
	return $str
}

proc get_data_from_element { elemp { index 0 } { Colname ""} } {
	set data [ getElement $elemp ]
	if	{ ! [ string compare "" $Colname ] } {
		set Colname [ getElementAttribute $elemp "name" ]
		set Colname [ lindex [ split $Colname : ] end ]
		;##puts "column $Colname"
		if	{ [ lsearch -exact [ set ::$::tag ] ] == -1 } {
			lappend ::$::tag $Colname
			;##puts "[set 	::$::tag ]"
		}
	}
	set dims [ getElementAttribute $elemp "dims" ]
	set ilwdtype [ getElementAttribute $elemp "identifier" ]
	set ilwd [ ilwd::preprocess [ getElement $elemp ] ]
	set result [ ilwd::parse $ilwd ]
	set len [ llength $result ]
	set result_data [ lindex $result 2 ]
	;##puts "[llength $result_data], result=$result_data"
	if	{ ! [ string compare "char_u" $ilwdtype ] } {
		;## uniqueId field
		if	{ [ llength $result_data ] == 13 && 
			  [ regexp -nocase {_id} $Colname ] } {
			set result_data [ list [ expand_uniqueId $result_data ] ]
		} else {
		;## a blob
			set result_data [ list [ blob_to_str $result_data ] ]		
		}
	} else {	
		if	{ $dims > 0 } {
			switch $ilwdtype {
				lstring { set result_data [ join $result_data ]
					regsub -all {\\,} $result_data {,} result_data 
					set result_data [ split $result_data ","] }
				char	{ set result_data [ list [ char_to_str $result_data ] ] }
				default { set result_data [ split $result_data ] }
			}
		} 
	}
	;##puts "[llength $result_data],result_data=$result_data"
	foreach entry $result_data {
		upvar #0 ::${Colname} $Colname
		array set ${Colname} [ list $index [ string trim $entry ] ]
		incr index 1
	}
	return $Colname
}

proc get_data_from_container { contp } {
	set size [ getElementAttribute $contp "size" ]
	for { set i 0 } { $i < $size } { incr i 1 } {
		set elemp [ refContainerElement $contp $i ]
		if	{ $i == 0 } {
			set Colname [ getElementAttribute $contp "name" ]
			set Colname [ lindex [ split $Colname : ] end ]
			;##puts "column $Colname"
			if	{ [ lsearch -exact [ set ::$::tag ] ] == -1 } {
				lappend ::$::tag $Colname
				;##puts "[set 	::$::tag ]"
			}
		}	
		set Colname [ get_data_from_element $elemp $i $Colname ]
		;##upvar #0 $name $name
	}
	return $Colname 	
}

set env(HOST) [ exec uname -n ]

set auto_path "/usr1/ldas/mlei/genericAPI \
/usr1/ldas/mlei/metadataAPI \
/ldas/lib $auto_path"
puts "auto_path=$auto_path"

set API test
package require generic


;## build the input data hash tables
proc build_hash_table { contp } {

	set tblcontp [ refContainerElement $contp 0 ]
	set numCols [ getElementAttribute $tblcontp "size" ]
	puts "numCols=$numCols"
	for { set i 0 } { $i < $numCols } { incr i 1 } {
		set elmcontp [ refContainerElement $tblcontp $i ]
	;##puts "i=$i, elmcontp = $elmcontp"
		switch -regexp -- $elmcontp {

			{^_[0-9a-f]+_LdasElement_p$} {
           		set name [ get_data_from_element $elmcontp ]
       		}		
			{^_[0-9a-f]+_LdasArrayBase_p$} {
           		set name [ get_data_from_element $elmcontp ]
       		}
			{^_[0-9a-f]+_LdasContainer_p$} {
				set name [ get_data_from_container $elmcontp ]
      		} 
			default { puts "unknown type $elmcontp" }	
		}
	}
}

proc compare_entry { colname } {
	set dbcol [ string toupper $colname ]
	upvar #0 ::${colname} $colname
	upvar #0 ::${dbcol} $dbcol
	if	{ ! [ info exist $dbcol ] } {
		return -code error "$dbcol not in database"
	}
	set mismatch 0
	puts "column $colname"
	foreach entry [ lsort -integer [ array names $colname ] ] {
		set ilwdval [ set ${colname}($entry) ]
		set dbval [ set ${dbcol}($entry) ]
		puts "$entry, ilwd=$ilwdval, db=$dbval"
		if	{ [ string compare $dbval $ilwdval ] } {
			puts "mismatched in column $colname, $entry:\
			 expected $ilwdval, got $dbval"
			set mismatch 1
		}	
	}
	return $mismatch
}


set infile [ lindex $argv 0 ]
puts "infile=$infile"
if	{ [ catch {
	set filwd [ openILwdFile $infile "r" ] 
	set contp [ readElement $filwd ]
	closeILwdFile $filwd
	} err ] } {
	puts "$infile error: $err"
	exit
}	
set tag "in"
set in {}
build_hash_table $contp
puts "[llength $in], $tag= $in"

set outfile [ lindex $argv 1 ]
puts "outfile=$outfile"
if	{ [ catch {
	set filwd [ openILwdFile $outfile "r" ] 
	set contp [ readElement $filwd ]
	closeILwdFile $filwd
	} err ] } {
	puts "$outfile error: $err"
	exit
}

set tag "out"
set out {}
build_hash_table $contp
puts "[llength $out], $tag=$out"

;## compare results of same columns

set mismatch 0
foreach col $in {
	incr mismatch [ compare_entry $col ]
}

;## dump out the remainding columns from database
puts "columns in database only"

foreach dbcol $out {
	set ilwdcol [ string tolower $dbcol ]
	if	{ [ lsearch -exact $in $ilwdcol ] == -1 } {
		puts "database column $dbcol"
		foreach entry [ lsort -integer [ array names $dbcol ]] {
			puts "$entry, [ set ${dbcol}($entry) ]"
		}
	}
}

if	{ $mismatch == 0 } {
	puts "ILwd file $infile has been validated"
} else {
	puts "ILwd file $infile has $mismatch mismatched columns with database"
}
