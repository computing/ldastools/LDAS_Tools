#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sim_type( "sim_typegroup:sim_type:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	INT_4S	sim_type[ARRAYSIZE];
	LdasContainer name_cont ( "sim_typegroup:sim_type:name" ); 
	LdasContainer comment_cont ( "sim_typegroup:sim_type:comment" ); 
} // end namespace

using namespace local;

static void setup_sim_type () {

// fill in sim_type array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		sim_type[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_name () {

// fill in name array
// NOT NULL
	CHAR name[24];
	CHAR nameattr[224];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 24, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "sim_type:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"sim_typegroup:sim_type:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[64];
	CHAR commentattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 64, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "sim_type:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"sim_typegroup:sim_type:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

void setup_sim_type ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	// setup_sim_type();
	// ilwd3sim_type.push_back(  LdasArray< INT_4S >(sim_type, numElements,
	// "sim_typegroup:sim_type:sim_type" ) );
	setup_name();
	ilwd3sim_type.push_back(  name_cont ) ;
	setup_comment();
	ilwd3sim_type.push_back(  comment_cont ) ;
}

#ifdef TESTMAIN 
 void setup_sim_type( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sim_type( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sim_type );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
