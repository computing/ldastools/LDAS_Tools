dnl -*- m4 -*-
dnl----------------------------------------------------------------------
dnl These are custom rules for certain programs
dnl----------------------------------------------------------------------
dnl======================================================================
dnl LDAS_PROG_BISON
dnl   Check on how to setup bison
dnl Depends:
dnl======================================================================
AC_DEFUN([LDAS_PROG_BISON],
[ AC_REQUIRE([AC_PROG_YACC])
  case "$YACC" in
  bison\ *) YACC="bison --debug --locations";;
  yacc\ *)
    AC_MSG_ERROR(yacc lacks capabilities required by these parsers)
    ;;
  esac
])

dnl======================================================================
dnl AC_LDAS_PROG_CC
dnl   Check on how to setup cc
dnl Depends:
dnl======================================================================

AC_DEFUN([AC_LDAS_PROG_CC],
[ AC_REQUIRE([AC_PROG_CC])
  if test x${ac_cv_prog_cc_g} = xyes
  then
    dnl------------------------------------------------------------------
    dnl PR3112: Put in -g (debug) option if supported by compiler
    dnl------------------------------------------------------------------
    CFLAGS="-g $CFLAGS"
  fi
  if test "${GCC}" = "yes"
  then
    touch ,t.c
    ac_ldas_default_cc_includes="`${CC} -v ,t.c 2>&1 | \
  	sed -n -e '/\#include/,/End of search list\./p' | \
  	egrep '^ '`"
    rm ,t.c
  fi
  #=======================================================================
  # Determine compiler vendor
  #=======================================================================
  AC_LANG_SAVE
  AC_LANG_C
  LDAS_CC_FLAVOR="generic"
  ldas_prog_cc_vendor=generic
  if test $ldas_prog_cc_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if GNU flavor
    dnl -----------------------------------------------------------------
    if test "${GCC}" = yes
    then
      ldas_prog_cc_vendor="GNU"
      LDAS_CC_FLAVOR=gnu
    fi
  fi
  if test $ldas_prog_cc_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if SunPRO flavor
    dnl -----------------------------------------------------------------
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   #if __SUNPRO_C
                   exit( 0 );
                   #endif /* __SUNPRO_C */
	           exit( 1 );
                 }
               ],[
                ldas_prog_cc_vendor=Sun
        	LDAS_CC_FLAVOR="SunPRO"
               ])
  fi
  AC_LANG_RESTORE
  AM_CONDITIONAL([LDAS_CC_SUNPRO],[test x$LDAS_CC_FLAVOR = xSunPRO])
  dnl -------------------------------------------------------------------
  dnl  setting up of compiler flags
  dnl -------------------------------------------------------------------
  AC_MSG_CHECKING(C vendor)
  AC_MSG_RESULT($ldas_prog_cc_vendor)
  case x$ldas_prog_cc_vendor in
  xGNU) dnl ######## G N U ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cc_optimization_key="-O"
    ldas_prog_cc_optimization_none="-O0"
    ldas_prog_cc_optimization_exrtreme="-O4"
    ldas_prog_cc_optimization_high="-O3"
    ldas_prog_cc_optimization_medium="-O2"
    ldas_prog_cc_optimization_low="-O1"
    ldas_prog_cc_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    case $ldas_processor in
    sparc)
      ldas_prog_cc_64bit="-m64 -mcpu=v9"
      ldas_prog_ld_64bit="-m64 -mcpu=v9"
      ;;
    i386)
      ldas_prog_cc_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    *)
      ldas_prog_cxx_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    esac
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    CFLAGS_FATAL_WARNINGS="-Werror"
    CFLAGS_PEDANTIC="-pedantic -Wno-long-long"
    dnl .................................................................
    dnl   C
    dnl .................................................................
    AC_LANG_PUSH([C])
    ldas_saved_cflags=${CFLAGS}
    for warn in \
	-Wall \
	 -Wextra \
	 -Wno-missing-field-initializers \
	 -Wno-unused-parameter \
	 -Wunused-but-set-variable \
	 -Wc++-compat \
	 -Wunused-private-field
    do
      AC_MSG_CHECKING([if C compiler supports warning flag: ${warn}])
      ldas_saved_cflags=${CFLAGS}
      CFLAGS="${CFLAGS} ${CFLAGS_FATAL_WARNINGS} ${ldas_prog_cc_warning} ${warn}"
      AC_COMPILE_IFELSE(
        [ AC_LANG_PROGRAM([[#include <stdio.h>]
                           [const char hw[] = "Hello, World\n";]],
                          [[fputs (hw, stdout );]]) ],
        [ ldas_prog_cc_warning="${ldas_prog_cc_warning} ${warn}"
	  AC_MSG_RESULT([yes])
	],
        [ AC_MSG_RESULT([no]) ] )
      CFLAGS=${ldas_saved_cflags}
    done
    AC_LANG_POP
    ;;
  xSun) dnl ######## S U N   S T U D I O ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cc_optimization_key="-O"
    ldas_prog_cc_optimization_none="-xO0"
    ldas_prog_cc_optimization_exrtreme="-xO5"
    ldas_prog_cc_optimization_high="-xO4"
    ldas_prog_cc_optimization_medium="-xO3"
    ldas_prog_cc_optimization_low="-xO1"
    ldas_prog_cc_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    ldas_prog_cc_64bit="-xarch=generic64"
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    CFLAGS_FATAL_WARNINGS="-errwarn=%all"
    CFLAGS_PEDANTIC=""
    ;;
  *) dnl ###### D E F A U L T ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cc_optimization_key="-O"
    ldas_prog_cc_optimization_none="-O0"
    ldas_prog_cc_optimization_exrtreme="-O4"
    ldas_prog_cc_optimization_high="-O3"
    ldas_prog_cc_optimization_medium="-O2"
    ldas_prog_cc_optimization_low="-O1"
    ldas_prog_cc_optimization_default=""
    ;;
  esac
  dnl -------------------------------------------------------------------
  dnl Setup flags for 64 bit compilation
  dnl -------------------------------------------------------------------
  AC_REQUIRE([AC_LDAS_ARG_ENABLE_64BIT])
  case x${enable_64bit} in
  xyes)
    case "x${ldas_prog_cc_64bit}" in
    x)
      ;;
    *)
      CFLAGS="$CFLAGS $ldas_prog_cc_64bit"
      ;;
    esac
    case "x${ldas_prog_ld_64bit}" in
    x)
      ;;
    *)
      LDFLAGS="$ldas_prog_ld_64bit $LDFLAGS"
      ;;
    esac
    ;;
  esac
  dnl -------------------------------------------------------------------
  dnl  Checking on how to pass rpath info
  dnl -------------------------------------------------------------------
  saved_LDFLAGS="$LDFLAGS"
  LDFLAGS="$LDFLAGS -Wl,--rpath,/usr/lib"
  AC_TRY_RUN([ #include <stdlib.h>

               int main( );

               int
               main( )
               {
                   exit( 0 );
               }
             ],[
	        RPATH="--rpath"
             ])
  LDFLAGS="$saved_LDFLAGS"
  case "$RPATH" in
  "")
    saved_LDFLAGS="$LDFLAGS"
    LDFLAGS="$LDFLAGS -Wl,-R,/usr/lib"
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   exit( 0 );
                 }
               ],[
	        RPATH="-R"
               ])
    LDFLAGS="$saved_LDFLAGS"
    ;;
  esac
  AC_SUBST([CFLAGS_FATAL_WARNINGS])
  AC_SUBST([CFLAGS_PEDANTIC])
])

dnl======================================================================
dnl AC_LDAS_PROG_CXX
dnl   Check on how to setup c++
dnl Depends:
dnl======================================================================

AC_DEFUN([AC_LDAS_PROG_CXX],
[ AC_REQUIRE([AC_LIBTOOL_CXX])
  AC_REQUIRE([AC_PROG_CXX])
  if test x${ac_cv_prog_cxx_g} = xyes
  then
    dnl------------------------------------------------------------------
    dnl PR3112: Put in -g (debug) option if supported by compiler
    dnl------------------------------------------------------------------
    CXXFLAGS="-g $CXXFLAGS"
  fi
  if test "${GXX}" = "yes"
  then
    touch ,t.cc
    ac_default_cxx_includes="`${CXX} -v ,t.cc 2>&1 | \
  	sed -n -e '/\#include/,/End of search list\./p' | \
  	egrep '^ '`"
    rm ,t.cc
  fi
  #=======================================================================
  # Determine compiler vendor
  #=======================================================================
  AC_LANG_SAVE
  AC_LANG_CPLUSPLUS
  LDAS_CXX_FLAVOR="generic"
  ldas_prog_cxx_vendor=generic
  if test $ldas_prog_cxx_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if GNU flavor
    dnl -----------------------------------------------------------------
    if test "${GXX}" = yes
    then
      ldas_prog_cxx_vendor="GNU"
      LDAS_CXX_FLAVOR=gnu
    fi
  fi
  if test $ldas_prog_cxx_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if SunPRO flavor
    dnl -----------------------------------------------------------------
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   #if __SUNPRO_CC
                   exit( 0 );
                   #endif /* __SUNPRO_CC */
	           exit( 1 );
                 }
               ],[
                ldas_prog_cxx_vendor=Sun
        	LDAS_CXX_FLAVOR="SunPRO"
               ])
  fi
  AC_LANG_RESTORE
  dnl -------------------------------------------------------------------
  dnl  setting up of compiler flags
  dnl -------------------------------------------------------------------
  AC_MSG_CHECKING(C++ vendor)
  AC_MSG_RESULT($ldas_prog_cxx_vendor)
  case x$ldas_prog_cxx_vendor in
  xGNU) dnl ######## G N U ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_optimization_key="-O"
    ldas_prog_cxx_optimization_none="-O0"
    ldas_prog_cxx_optimization_exrtreme="-O4"
    ldas_prog_cxx_optimization_high="-O3"
    ldas_prog_cxx_optimization_medium="-O2"
    ldas_prog_cxx_optimization_low="-O1"
    ldas_prog_cxx_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    case $ldas_processor in
    sparc)
      ldas_prog_cxx_64bit="-m64 -mcpu=v9"
      ldas_prog_ld_64bit="-m64 -mcpu=v9"
      ;;
    i386)
      ldas_prog_cxx_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    *)
      ldas_prog_cxx_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    esac
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    CXXFLAGS_FATAL_WARNINGS="-Werror"
    CXXFLAGS_PEDANTIC="-pedantic -Wno-long-long "
    dnl .................................................................
    dnl   C++
    dnl .................................................................
    AC_LANG_PUSH([C++])
    for warn in \
	-Wall \
	 -Wextra \
	 -Wno-missing-field-initializers \
	 -Wno-unused-parameter \
	 -Wignored-qualifiers \
	 -Wunused-but-set-variable \
	 -Wc++0x-compat \
	 -Wunused-private-field
    do
      AC_MSG_CHECKING([if C++ compiler supports warning flag: ${warn}])
      ldas_saved_cxxflags="${CXXFLAGS}"
      CXXFLAGS="${CXXFLAGS} ${CXXFLAGS_FATAL_WARNINGS} ${ldas_prog_cxx_warning} ${warn}"
      AC_COMPILE_IFELSE(
        [ AC_LANG_PROGRAM([[#include <stdio.h>]
                           [const char hw[] = "Hello, World\n";]],
                          [[fputs (hw, stdout );]]) ],
        [ ldas_prog_cxx_warning="${ldas_prog_cxx_warning} ${warn}"
	  AC_MSG_RESULT([yes])
	],
        [ AC_MSG_RESULT([no]) ] )
      CXXFLAGS="${ldas_saved_cxxflags}"
    done
    dnl -----------------------------------------------------------------
    dnl  Specify the level of C++ conformance.
    dnl     Accept the highest level only.
    dnl -----------------------------------------------------------------
    for flag in \
      -std=c++11 \
      -std=c++0x
    do
      AC_MSG_CHECKING([if C++ compiler supports flag: ${flag}])
      ldas_saved_cxxflags="${CXXFLAGS}"
      CXXFLAGS="${CXXFLAGS} ${flag}"
      AC_COMPILE_IFELSE(
        [ AC_LANG_PROGRAM([[#include <stdio.h>]
                           [const char hw[] = "Hello, World\n";]],
                          [[fputs (hw, stdout );]]) ],
        [ dnl
	  AC_MSG_RESULT([yes])
          break
	],
        [ CXXFLAGS="${ldas_saved_cxxflags}"
	  AC_MSG_RESULT([no]) ] )
    done
    dnl -----------------------------------------------------------------
    dnl  Specify the C++ library to use.
    dnl     Accept the highest level only.
    dnl -----------------------------------------------------------------
    for flag in \
      -stdlib=libc++ \
      -stdlib=libstdc++
    do
      AC_MSG_CHECKING([if C++ compiler supports flag: ${flag}])
      ldas_saved_cxxflags="${CXXFLAGS}"
      CXXFLAGS="${CXXFLAGS} ${flag}"
      AC_COMPILE_IFELSE(
        [ AC_LANG_PROGRAM([[#include <stdio.h>]
                           [const char hw[] = "Hello, World\n";]],
                          [[fputs (hw, stdout );]]) ],
        [ dnl
	  AC_MSG_RESULT([yes])
          break
	],
        [ CXXFLAGS="${ldas_saved_cxxflags}"
	  AC_MSG_RESULT([no]) ] )
    done
    AC_LANG_POP
    ;;
  xSun) dnl ######## S U N   S T U D I O ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_optimization_key="-O"
    ldas_prog_cxx_optimization_none="-xO0"
    ldas_prog_cxx_optimization_exrtreme="-xO4"
    ldas_prog_cxx_optimization_high="-xO3"
    ldas_prog_cxx_optimization_medium="-xO2"
    ldas_prog_cxx_optimization_low="-xO1"
    ldas_prog_cxx_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_64bit="-m64"
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    CXXFLAGS_FATAL_WARNINGS="-errwarn=%all"
    CXXFLAGS_PEDANTIC=""
    dnl -----------------------------------------------------------------
    dnl  Setting up of CXXFLAGS
    dnl -----------------------------------------------------------------
    dnl Use STLport, not default
    CXXFLAGS="$CXXFLAGS -library=stlport4"
    dnl AC_CHECK_LIB([sunmath],[coshf])
    dnl -----------------------------------------------------------------
    dnl  Checking Version of Sun Studio compiler
    dnl -----------------------------------------------------------------
    AC_LANG_SAVE
    AC_LANG_CPLUSPLUS
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   #if __SUNPRO_CC <= 0x0590
                   exit( 0 );
                   #endif /* __SUNPRO_CC */
	           exit( 1 );
                 }
               ],[ldas_inline_kludge="inline"
               ])
    AC_LANG_RESTORE
    ;;
  *) dnl ###### D E F A U L T ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_optimization_key="-O"
    ldas_prog_cxx_optimization_none="-O0"
    ldas_prog_cxx_optimization_exrtreme="-O4"
    ldas_prog_cxx_optimization_high="-O3"
    ldas_prog_cxx_optimization_medium="-O2"
    ldas_prog_cxx_optimization_low="-O1"
    ldas_prog_cxx_optimization_default=""
    ;;
  esac
  AM_CONDITIONAL([LDAS_CXX_SUNPRO],[test x$LDAS_CXX_FLAVOR = xSunPRO])
  AC_SUBST([CXXFLAGS_FATAL_WARNINGS])
  AC_SUBST([CXXFLAGS_PEDANTIC])

  dnl -------------------------------------------------------------------
  dnl Setup flags for 64 bit compilation
  dnl -------------------------------------------------------------------
  AC_REQUIRE([AC_LDAS_ARG_ENABLE_64BIT])
  case x${enable_64bit} in
  xyes)
    CXXFLAGS="$CXXFLAGS $ldas_prog_cxx_64bit"
    ;;
  esac
  AC_DEFINE_UNQUOTED([INLINE_KLUDGE],${ldas_inline_kludge},Set to "inline" if need to inline kludge to prevent multi/undefined variables)
  dnl -------------------------------------------------------------------
  dnl
  dnl -------------------------------------------------------------------
  AC_LANG_PUSH([C++])
  AC_RUN_IFELSE([AC_LANG_SOURCE([
    #include <stdlib.h>
    #include <stdexcept>

    int
    main(int, char**)
    {
      try {
        throw std::runtime_error("Some error");
      }
      catch( ... )
      {
        exit( 0 );
      }
      exit( 1 );
    }
  ])],[],[
    AC_MSG_FAILURE([C++ compiler does not properly catch exceptions])
  ])
  AC_LANG_POP([C++])
  dnl -------------------------------------------------------------------
  dnl  Perform all tests using C++
  dnl -------------------------------------------------------------------
  AC_LANG_CPLUSPLUS
])

dnl======================================================================
dnl LDAS_PROG_DOT
dnl======================================================================
AC_DEFUN([LDAS_PROG_DOT],
[ AC_CACHE_CHECK([whether dot command works],
	         [ldas_cv_shell_dot],
	         [AC_ARG_WITH([dot],
			      [AS_HELP_STRING([--with-dot],
					      [enable support for the dot command])],
			      [],
			      [with_dot=yes])
		  AS_IF([test "x$with_dot" = xyes],
		  	[AC_PATH_PROGS( ldas_cv_shell_dot, dot )],
			[AS_IF([test "x$with_dot" != xno],
			       [ldas_cv_shell_dot="$with_dot"])])
  	          case x$ldas_cv_shell_dot in
                  x) ;;
                  *) ( $ldas_cv_shell_dot ) 2>/dev/null || ldas_cv_shell_dot=""
	             ;;
		  esac])
  DOT="$ldas_cv_shell_dot"
  case x$DOT in
  x) ;;
  *)
    DOT_PATH="`echo $DOT | sed -e 's,/[^/]*$,,'`"
    ;;
  esac
  AC_SUBST(DOT_PATH)
  AM_CONDITIONAL(HAVE_DOT, test x$DOT != x)
])

dnl======================================================================
dnl LDAS_PROG_DOXYGEN
dnl======================================================================
AC_DEFUN([LDAS_PROG_DOXYGEN],
[ AC_REQUIRE([LDAS_PROG_LATEX])
  AC_REQUIRE([LDAS_PROG_DOT])
  AC_ARG_WITH([doxygen],
	[  --with-doxygen=<PATH>  Specify the path for doxygen],
	[],[with_doxygen=doxygen])
  case x"$with_doxygen" in
  x|xno)
    DOXYGEN=""
    ;;
  * )
    AC_MSG_CHECKING(for doxygen)
    AC_PATH_PROGS( DOXYGEN, doxygen )
    case x$DOXYGEN in
    x) AC_MSG_RESULT(no);;
    *) AC_MSG_RESULT($DOXYGEN);;
    esac
  esac
  AM_CONDITIONAL(HAVE_DOXYGEN, test x$DOXYGEN != x)
])

dnl======================================================================
dnl LDAS_PROG_LATEX
dnl======================================================================
AC_DEFUN([LDAS_PROG_LATEX],
[ AC_ARG_ENABLE([latex],
	         AC_HELP_STRING([--enable-latex],
                    	        [use latex (default is CHECK)]),
	         [ case "${enable_latex}" in
	           no)
		     ;;
                   *)
		     ;;
                   esac ],
	         [enable_latex="CHECK";] )

  case $enable_latex in
  no)
    unset LATEX
    ;;
  *)
    AC_PATH_PROGS( LATEX, latex )
    ;;
  esac
  AC_MSG_CHECKING(for latex)
  case x$LATEX in
  x) 
    case $enable_latex in
    CHECK|no)
      AC_MSG_RESULT(no)
      ;;
    *)
      AC_MSG_ERROR(no)
      ;;
    esac
    ;;
  *) AC_MSG_RESULT($LATEX)
    ;;
  esac
  AM_CONDITIONAL(HAVE_LATEX, test x$LATEX != x)
])

dnl======================================================================
dnl LDAS_PROG_LEX
dnl   Check on how to setup lex
dnl Depends:
dnl======================================================================
AC_DEFUN([LDAS_PROG_LEX],
[ AM_PROG_LEX
  case x$LEX in
  x/*)
    ;;
  *)
    AC_PATH_PROGS(LEX,flex lex)
    ;;
  esac
  case x$LEX in
  x/usr/bin*)
    ;;
  *)
    tmp="`echo $LEX | sed -e 's,/bin/.*$,/include,g'`"
    echo $LEX_CPPFLAGS | grep "${tmp}" > /dev/null 2>&1
    case $? in
    0)
      ;;
    *)
      LEX_CPPFLAGS="${LEX_CPPFLAGS} -I${tmp}"
    esac
    ;;
  esac
  $LEX --version > /dev/null 2>&1
  AS_CASE([$?],
          [0],[LDAS_CHECK_HEADER_FLEX_LEXER_H
	       dnl------------------------------------------------------
	       dnl If the version of flex installed on the destination
	       dnl  machine is different from what generated the
	       dnl  sources, then remove the sources so they will be
	       dnl  regenerated.
	       dnl------------------------------------------------------
    	       save_CPPFLAGS=${CPPFLAGS}
	       CPPFLAGS="${LEX_CPPFLAGS} ${CPPFLAGS}"
    	       AC_EGREP_CPP([yy_current_buffer],
    	                    [ #include <FlexLexer.h>
    		            ],
    		            [ AC_DEFINE([HAVE_YY_CURRENT_BUFFER],
                                        [1],
			                [Defined if yy_current_buffer is in yyFlexLexer class])
    		            ],
    		            [ AC_DEFINE([YY_CURRENT_BUFFER_EQUIV],
                                        [(yy_buffer_stack[[yy_buffer_stack_top]])],
                                        [Defined as the equivelent for yy_current_buffer])
    		            ])
	       AC_LANG_PUSH([C++])
	       AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
	                          #include <FlexLexer.h>
				  ],
				  [
				    return 0;
				  ])
		                 ],
				 [_ldas_flex_lexer_h_broken="0"],
				 [_ldas_flex_lexer_h_broken="1"])
	       AC_LANG_POP([C++])
    	       CPPFLAGS=${save_CPPFLAGS}
   	       unset save_CPPFLAGS
	       AS_IF([test x$_ldas_flex_lexer_h_broken != "x1"],
	             [ldas_using_flex="1"])
	       ])
  AM_CONDITIONAL([LDAS_USING_FLEX],[test "x$ldas_using_flex" = "x1"])
])

dnl======================================================================
dnl AC_LDAS_PROG_LIBTOOL
dnl   Check on how to setup libtool
dnl Depends:
dnl======================================================================

AC_DEFUN([AC_LDAS_PROG_LIBTOOL],
[
  dnl -------------------------------------------------------------------
  dnl Need to ensure the correct usage of the C compiler
  dnl -------------------------------------------------------------------
  AC_REQUIRE([AC_LDAS_PROG_CC])
  AC_REQUIRE([AC_LDAS_PROG_CXX])
  dnl -------------------------------------------------------------------
  dnl This hack is needed until libtool 1.5.22 is out. It will properly
  dnl    configure for 64bit Solaris.
  dnl -------------------------------------------------------------------
  ac_save_CC=$CC
  ac_save_CXX=$CXX
  CC="$CC $CFLAGS"
  CXX="$CXX $CXXFLAGS"
  AC_LANG_SAVE
  AC_LANG_C
  AC_PROG_LIBTOOL
  AM_PROG_LIBTOOL
  AC_LANG_RESTORE
  case "$LDAS_CXX_FLAVOR" in
  SunPRO)
	if echo $CXXFLAGS | grep -e -library=stlport4 > /dev/null 2>&1
	then
		dnl -----------------------------------------------------
		dnl libtool currently assumes the default std C++ library.
		dnl   LDAS requires the stlport4 library instead.
		dnl -----------------------------------------------------
		mv libtool libtool.orig
		sed -e 's/-lCstd -lCrun/-library=stlport4/g' libtool.orig > libtool
		chmod +x libtool
	fi
	;;
  esac
  CC=$ac_save_CC
  CXX=$ac_save_CXX
])

dnl======================================================================
dnl AC_LDAS_PROG_PCE2
dnl   Check on how to setup pce2
dnl======================================================================

AC_DEFUN([AC_LDAS_PROG_PCE2],
[ AC_REQUIRE([LDAS_PROG_DOXYGEN])
  dnl if test x$DOXYGEN = x
  dnl then
    dnl =============================================================
    dnl  Use the much older perceps package only if doxygen is
    dnl    not available.
    dnl =============================================================
    if test -x ${srcdir}/${ldas_top}/lib/perceps/pce2
    then
      PERCEPS='[$][(]ldas_top_srcdir[)]/lib/perceps/pce2'
    elif test -x ${prefix}/bin/pce2
    then
      PERCEPS='[$][(]bindir[)]/pce2'
    fi
    if test -d ${srcdir}/${ldas_top}/lib/perceps
    then
      PERCEPS_ENV='[$][(]ldas_top_srcdir[)]/lib/perceps'
    elif test -d "${prefix}/lib/perceps"
    then
      PERCEPS_ENV='[$][(]libdir[)]/perceps'
    else
      PERCEPS_ENV=""
    fi
  dnl fi
  if test -z "${PERCEPS}" \
	|| test -z "${PERCEPS_ENV}"
  then
    dnl =============================================================
    dnl  Reset variables to be empty since there is no perceps.
    dnl =============================================================
    PERCEPS="";
    PERCEPS_FLAGS=""
    PERCEPS_ENV=""
    HAVE_PERCEPS="no"
  else
    HAVE_PERCEPS="yes"
  fi
  AC_SUBST(PERCEPS)
  AC_SUBST(PERCEPS_FLAGS)
  AC_SUBST(PERCEPS_ENV)
  AC_SUBST(HAVE_PERCEPS)
  AM_CONDITIONAL(LDAS_COND_HAVE_PERCEPS, test x$PERCEPS != x)
  PERCEPS_STRIP_GENERATING="sed -e 's/^Generating //' -e 's/^\.\/\///'"
  AC_SUBST(PERCEPS_STRIP_GENERATING)
])

dnl======================================================================
dnl AC_LDAS_PROG_PERCEPS
dnl   Check on how to setup perceps
dnl======================================================================

AC_DEFUN([AC_LDAS_PROG_PERCEPS],
[ AC_PATH_PROG(PERCEPS, perceps)
  PERCEPS_FLAGS="-abhmr"
  if test -d ${srcdir}/${ldas_top}/lib/perceps
  then
    PERCEPS_ENV='[$][(]ldas_top_srcdir[)]/lib/perceps'
  elif test -d ${prefix}/lib/perceps
  then
    PERCEPS_ENV="${prefix}/lib/perceps"
  else
    PERCEPS_ENV=""
  fi
  if test -z "${PERCEPS}" \
	|| test -d "${PERCEPS}" || test ! -x "${PERCEPS}" \
	|| test -z "${PERCEPS_ENV}"
  then
    dnl =============================================================
    dnl  Reset variables to be empty since there is no perceps.
    dnl =============================================================
    PERCEPS="";
    PERCEPS_FLAGS=""
    PERCEPS_ENV=""
    HAVE_PERCEPS="no"
  else
    HAVE_PERCEPS="yes"
    PERCEPS_FLAGS=""
  fi
  AC_SUBST(PERCEPS_FLAGS)
  AC_SUBST(PERCEPS_ENV)
  AC_SUBST(HAVE_PERCEPS)
  AM_CONDITIONAL(HAVE_PERCEPS, test x$PERCEPS != x)
  PERCEPS_STRIP_GENERATING="sed -e 's/^Generating //' -e 's/^\.\/\//'"
  AC_SUBST(PERCEPS_STRIP_GENERATING)
])

dnl======================================================================
dnl LDAS_PROG_PKGBUILDER
dnl======================================================================
AC_DEFUN([LDAS_PROG_PKGBUILDER],
[ AC_MSG_CHECKING(for pkgbuilder)
  AC_PATH_PROGS( PKGBUILDER, pkgbuilder )
  case x$PKGBUILDER in
  x) AC_MSG_RESULT(no);;
  *)
    AC_MSG_RESULT($PKGBUILDER)
    PKGBUILDER_PATH="`echo $PKGBUILDER | sed -e 's,/[^/]*$,,'`"
    ;;
  esac
  AC_SUBST(PKGBUILDER_PATH)
  AM_CONDITIONAL(HAVE_PKGBUILDER, test x$PKGBUILDER != x)
])

dnl======================================================================
dnl LDAS_PROG_SWIG
dnl   Check on how to setup swig
dnl======================================================================

AC_DEFUN([LDAS_PROG_SWIG],
[
  AC_ARG_ENABLE([swig],
	         AC_HELP_STRING([--enable-swig],
                    	        [use swig (default is CHECK)]),
	         [ case "${enable_swig}" in
	           no)
		     ;;
                   *)
		     ;;
                   esac ],
	         [enable_swig="CHECK";] )
  case x${enable_swig} in
  xCHECK)
    AC_PATH_PROG( SWIG, swig )
    case "`$SWIG -swiglib`" in
    *swig_lib) ;;
    *) SWIGFLAGS="$SWIGFLAGS -DSWIGNEW";;
    esac
    ;;
  xno)
    SWIGFLAGS="";
    SWIG="";
    ;;
  esac
  AC_SUBST(SWIGFLAGS)
  AC_SUBST(SWIG)
  AM_CONDITIONAL(HAVE_SWIG, test x$SWIG != x)
])

dnl======================================================================
dnl LDAS_PROG_TIME
dnl======================================================================
AC_DEFUN([LDAS_PROG_TIME],
[ AC_PATH_PROGS( TIME, time, [""] )
])
dnl======================================================================
dnl LDAS_PROG_VALGRIND
dnl======================================================================
AC_DEFUN([LDAS_PROG_VALGRIND],
[ AC_PATH_PROGS( VALGRIND, valgrind )
  AM_CONDITIONAL(HAVE_VALGRIND, test x$VALGRIND != x)
])
