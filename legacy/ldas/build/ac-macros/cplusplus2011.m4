dnl =====================================================================
dnl  Test for C++ 2011 features
dnl =====================================================================
dnl  LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_TEMPLATE_ALIASES],
[
AC_MSG_CHECKING([if the C++ compiler supports template aliasing])
AC_LANG_PUSH([C++])
AC_TRY_COMPILE([
#include <memory>
namespace {
  template< class T > using AutoArray = std::unique_ptr< T[] >;
  template< typename T, typename U > using DynamicPointerCast = typename std::dynamic_pointer_cast< T, U >;
}
],[
 AutoArray< int >	a;
],[
  AC_DEFINE([HAVE_CXX_TEMPLATE_ALIASES],[1],[Defined if C++ supports template aliasing])
  AC_MSG_RESULT([yes])
],[
  AC_MSG_RESULT([no])
])
AC_LANG_POP([C++])
])
