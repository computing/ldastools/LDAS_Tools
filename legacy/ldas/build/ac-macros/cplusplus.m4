dnl  LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_TEMPLATE_ALIASES_VIA_USING],
[
AC_MSG_CHECKING([if the C++ compiler supports template aliasing via using directive])
AC_LANG_PUSH([C++])
AC_TRY_COMPILE([
#include <memory>
namespace MySpace {
#define SharedPtr shared_ptr
  using std::SharedPtr;
}
],[
 MySpace::SharedPtr< int >	a;
],[
  AC_DEFINE([HAVE_CXX_TEMPLATE_ALIASES_VIA_USING],[1],[Defined if C++ supports template aliasing via using directive])
  AC_MSG_RESULT([yes])
],[
  AC_MSG_RESULT([no])
])
AC_LANG_POP([C++])
])
