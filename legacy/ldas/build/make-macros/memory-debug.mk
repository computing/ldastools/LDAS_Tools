ifdef SUBDIRS
memcheck: memcheck-recursive
mtsafe: mtsafe-recursive
else
memcheck: memcheck-am
mtsafe: mtsafe-am
endif
memcheck-am: all-am
mtsafe-am: all-am

ifneq ($(VALGRIND),)
  VALGRIND_LD_LIBRARY_PATH=$(MEMORY_DEBUG_LD_LIBRARY_PATH)
  include $(ldas_top_srcdir)/build/make-macros/valgrind.mk
endif
ifneq ($(LIBUMEM),)
  include $(ldas_top_srcdir)/build/make-macros/libumem.mk
endif

