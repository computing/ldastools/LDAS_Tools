#------------------------------------------------------------------------
# This file cannot be built in parallel
#------------------------------------------------------------------------
.NOTPARALLEL:

#------------------------------------------------------------------------
# Key variables that need to be known before including this file:
#========================================================================
#
# doxygenhtmldir - Location where to install the documentation
#
# DOXYGEN_DISABLE_LATEX - If 1, then do not generate latex documentation
#  even if latex is available.
#
#------------------------------------------------------------------------
NAMESPACE ?= $(PACKAGE)
DOXYGEN_CLEAN = doxygen-clean-html
DOXYGEN_CFG=doxygen.cfg

DOXYGEN_GENERATE_HTML ?= YES

DOXYGEN_GENERATE_LATEX ?= YES
DOXYGEN_GENERATE_TAGFILE ?= $(NAMESPACE).tag
ifeq ($(DOXYGEN_DISABLE_LATEX),1)
DOXYGEN_LATEX=
else
DOXYGEN_LATEX=$(LATEX)
DOXYGEN_CLEAN += doxygen-clean-latex
endif

doxygenhtmldir ?= $(docdir)/$(NAMESPACE)/html

ifneq ($(DOXYGEN),)
ALL_LOCAL+=doc-doxygen
INSTALL_DATA_LOCAL+=install-doxygen
DOXYGEN_FILE_PATTERNS ?= *.tcc *.icc *.cc *.hh *.c *.h *.txt
ifndef DOXYGEN_DIRS
DOXYGEN_DIRS =
endif

DOXYGEN_SRCDIR_SRC ?=
DOXYGEN_STRIP_FROM_PATH ?=
ifneq ($(wildcard $(srcdir)/../src$(DOXYGEN_SRCDIR_SUBDIR)/*),)
DOXYGEN_DIRS += $(shell find $(srcdir)/../src$(DOXYGEN_SRCDIR_SUBDIR) -type d \! -name CVS -print)
DOXYGEN_SRCDIR_SRC += $(srcdir)/../src$(DOXYGEN_SRCDIR_SUBDIR)
DOXYGEN_STRIP_FROM_PATH += $(srcdir)/../src/
endif
ifndef DOXYGEN_CXX_FILES
DOXYGEN_CXX_FILES =
endif
DOXYGEN_CXX_FILES += $(foreach dir,$(DOXYGEN_DIRS),$(foreach pattern,$(DOXYGEN_FILE_PATTERNS),$(wildcard $(dir)/$(pattern))))

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Crate a list of search paths
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ifndef DOXYGEN_INCLUDE_PATH
DOXYGEN_INCLUDE_PATH =
endif
DOXYGEN_INCLUDE_PATH += \
	$(ldas_top_builddir)/include

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Create a list of commonly needed macro expansions
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
ifndef DOXYGEN_EXPAND_AS_DEFINED
DOXYGEN_EXPAND_AS_DEFINED =
endif
DOXYGEN_EXPAND_AS_DEFINED += \
	CREATE_THREADED0_DECL \
	CREATE_THREADED1_DECL \
	CREATE_THREADED2_DECL \
	CREATE_THREADED3_DECL \
	CREATE_THREADED4_DECL \
	CREATE_THREADED5_DECL \
	CREATE_THREADED6_DECL \
	CREATE_THREADED7_DECL \
	CREATE_THREADED8_DECL \
	CREATE_THREADED9_DECL \
	CREATE_THREADED10_DECL \
	CREATE_THREADED0V_DECL \
	CREATE_THREADED1V_DECL \
	CREATE_THREADED2V_DECL \
	CREATE_THREADED3V_DECL \
	CREATE_THREADED4V_DECL \
	CREATE_THREADED5V_DECL \
	CREATE_THREADED6V_DECL \
	CREATE_THREADED7V_DECL \
	CREATE_THREADED8V_DECL \
	CREATE_THREADED9V_DECL \
	CREATE_THREADED10V_DECL \
	CREATE_THREADED0 \
	CREATE_THREADED1 \
	CREATE_THREADED2 \
	CREATE_THREADED3 \
	CREATE_THREADED4 \
	CREATE_THREADED5 \
	CREATE_THREADED6 \
	CREATE_THREADED7 \
	CREATE_THREADED8 \
	CREATE_THREADED9 \
	CREATE_THREADED10 \
	CREATE_THREADED0V \
	CREATE_THREADED1V \
	CREATE_THREADED2V \
	CREATE_THREADED3V \
	CREATE_THREADED4V \
	CREATE_THREADED5V \
	CREATE_THREADED6V \
	CREATE_THREADED7V \
	CREATE_THREADED8V \
	CREATE_THREADED9V \
	CREATE_THREADED10V \
	SINGLETON_DECL \
	SINGLETON_TS_DECL \
	SINGLETON_INST \
	SINGLETON_TS_INST


#------------------------------------------------------------------------
#------------------------------------------------------------------------
DOXYGEN_ENV=NEXT_RELEASE=`$(ldas_top_srcdir)/build/scripts/next-release $(LDAS_PACKAGE_VERSION)`
DOXYGEN_ENV+=NEXT_TAG_RELEASE=`$(ldas_top_srcdir)/build/scripts/next-release $(LDAS_PACKAGE_VERSION) | tr . _`
DOXYGEN_ENV+=NEXT_TAG_BASE=`$(ldas_top_srcdir)/build/scripts/next-release $(LDAS_PACKAGE_VERSION) | tr . _ | sed -e 's/_[0-9]*$$/_0/' `
DOXYGEN_ENV+=NEXT_TAG_BRANCH=`$(ldas_top_srcdir)/build/scripts/next-release $(LDAS_PACKAGE_VERSION) | tr . _ | sed -e 's/_[0-9]*$$/_fixes/' `
#------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------
DOXYGEN_TARGETS = doxygen/html/index.html
ifeq ($(DOXYGEN_GENERATE_LATEX),YES)
DOXYGEN_TARGETS += doxygen/latex/index.tex
endif

doc-doxygen: $(DOXYGEN_TARGETS)

$(DOXYGEN_TARGETS): $(DOXYGEN_CXX_FILES) $(DOXYGEN_CFG) 
	/usr/bin/env $(DOXYGEN_ENV) $(DOXYGEN) $(DOXYGEN_CFG)
ifeq ($(DOXYGEN_GENERATE_LATEX),YES)
	( if test -d doxygen/latex; then cd doxygen/latex; make < /dev/null; fi )
endif

$(DOXYGEN_CFG): \
	Makefile.am $(ldas_top_srcdir)/build/make-macros/doxygen.mk \
	$(ldas_top_srcdir)/build/make-macros/doxygen.mk

#------------------------------------------------------------------------
# Build rules
#------------------------------------------------------------------------
$(DOXYGEN_CFG):
	rm -f $(DOXYGEN_CFG);
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Project characteristics' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'PROJECT_NAME=$(PACKAGE_NAME)' >> $(DOXYGEN_CFG)
	echo 'PROJECT_NUMBER=$(LDAS_PACKAGE_VERSION)' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Build related options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'EXTRACT_ALL=YES' >> $(DOXYGEN_CFG)
	echo 'EXTRACT_PRIVATE=YES' >> $(DOXYGEN_CFG)
	echo 'EXTRACT_STATIC=YES' >> $(DOXYGEN_CFG)
	echo 'EXTRACT_ANON_NSPACES=YES' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Sources to use' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'INPUT=$(DOXYGEN_AUX_FILES) $(DOXYGEN_SRCDIR_SRC)' >> $(DOXYGEN_CFG)
	echo 'FILE_PATTERNS=$(DOXYGEN_FILE_PATTERNS)' >> $(DOXYGEN_CFG)
	echo 'RECURSIVE=YES' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# How to handle warnings' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'WARNINGS=YES' >> $(DOXYGEN_CFG)
	echo 'WARN_IF_UNDOCUMENTED=YES' >> $(DOXYGEN_CFG)
	echo 'WARN_LOGFILE=doxygen_warnings.log' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Filtering of the source (Converting from Perceps to doxygen)' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
ifneq ($(DOXYGEN_FILTER_SOURCE_FILES),NO)
ifeq ($(DOXYGEN_PCE2_FILTER),)
	echo 'FILTER_SOURCE_FILES=YES' >> $(DOXYGEN_CFG)
	echo 'INPUT_FILTER=$(ldas_top_srcdir)/build/scripts/doxygen.pl' >> $(DOXYGEN_CFG)
else
	echo 'FILTER_SOURCE_FILES=YES' >> $(DOXYGEN_CFG)
	echo 'INPUT_FILTER=$(ldas_top_srcdir)/lib/perceps/pce2_to_doxygen.pl' >> $(DOXYGEN_CFG)
endif
endif
ifneq ($(DOXYGEN_EXCLUDE),)
	echo 'EXCLUDE=$(DOXYGEN_EXCLUDE)' >> $(DOXYGEN_CFG)
endif
ifneq ($(DOXYGEN_EXCLUDE_PATTERNS),)
	echo 'EXCLUDE_PATTERNS=$(DOXYGEN_EXCLUDE_PATTERNS)' >> $(DOXYGEN_CFG)
endif
	echo 'VERBATIM_HEADERS=NO' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Where to output' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'OUTPUT_DIRECTORY=doxygen' >> $(DOXYGEN_CFG)
	echo 'STRIP_FROM_PATH=$(DOXYGEN_STRIP_FROM_PATH)' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Types of output' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# MAN Options
	@#---------------------------------------------------------------
	echo 'GENERATE_MAN=NO' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Source browsing related options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'INLINE_SOURCES=NO' >> $(DOXYGEN_CFG)
	echo 'INLINE_INHERITED_MEMB=YES' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# Preprocessor related options
	@#---------------------------------------------------------------
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Preprocessor related options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'ENABLE_PREPROCESSING=YES' >> $(DOXYGEN_CFG)
	echo 'MACRO_EXPANSION=YES' >> $(DOXYGEN_CFG)
	echo 'EXPAND_ONLY_PREDEF=YES' >> $(DOXYGEN_CFG)
	echo 'EXPAND_AS_DEFINED=' >> $(DOXYGEN_CFG)
	for v in $(DOXYGEN_EXPAND_AS_DEFINED); \
	do \
	  echo 'EXPAND_AS_DEFINED+='$$v >> $(DOXYGEN_CFG); \
	done
	echo 'SEARCH_INCLUDES=YES' >> $(DOXYGEN_CFG)
	echo 'INCLUDE_PATH=$(DOXYGEN_INCLUDE_PATH)' >> $(DOXYGEN_CFG)
	echo 'PREDEFINED= __cplusplus=1 DOCUMENTATION_DOXYGEN=1' >>  $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# External Reference Options
	@#---------------------------------------------------------------
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# External reference options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo 'GENERATE_TAGFILE=$(DOXYGEN_GENERATE_TAGFILE)' >> $(DOXYGEN_CFG)
	echo 'TAGFILES=$(DOXYGEN_TAGFILES)' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# HTML Options
	@#---------------------------------------------------------------
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# HTML Options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	@file="doxygen.cfg"
	echo 'GENERATE_HTML=$(DOXYGEN_GENERATE_HTML)' >> $(DOXYGEN_CFG)
	@#---------------------------------------------------------------
	@# Latex Options
	@#---------------------------------------------------------------
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# Latex Options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
ifeq ($(DOXYGEN_LATEX),)
	echo 'GENERATE_LATEX=NO' >> $(DOXYGEN_CFG)
else
	echo 'GENERATE_LATEX=$(DOXYGEN_GENERATE_LATEX)' >> $(DOXYGEN_CFG)
endif
	@#---------------------------------------------------------------
	@# DOT Options
	@#---------------------------------------------------------------
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
	echo '# DOT Options' >> $(DOXYGEN_CFG)
	echo '#--------------------------------------------' >> $(DOXYGEN_CFG)
ifeq ($(DOT),)
	echo 'HAVE_DOT=NO' >> $(DOXYGEN_CFG)
else
	echo 'HAVE_DOT=YES' >> $(DOXYGEN_CFG)
	echo 'DOT_PATH=$(DOT_PATH)' >> $(DOXYGEN_CFG)
	echo 'DOT_IMAGE_FORMAT=png' >> $(DOXYGEN_CFG)
	echo 'CLASS_GRAPH=YES' >> $(DOXYGEN_CFG)
	echo 'GRAPHICAL_HIERARCHY=YES' >> $(DOXYGEN_CFG)
endif
endif
#------------------------------------------------------------------------
# Installation rules
#------------------------------------------------------------------------
install-doxygen: doc-doxygen
	if test -d doxygen/html; \
	then \
	  mkdir -p $(DESTDIR)$(doxygenhtmldir); \
	  for file in doxygen/html/*; \
	  do \
	    $(INSTALL) $$file $(DESTDIR)$(doxygenhtmldir); \
	  done \
	fi

doxygen-debug:
	@echo DOXYGEN_AUX_FILES: $(DOXYGEN_AUX_FILES)
	@echo DOXYGEN_TARGETS: $(DOXYGEN_TARGETS)
	@echo DOXYGEN_CXX_FILES: $(DOXYGEN_CXX_FILES)


clean: $(DOXYGEN_CLEAN)

doxygen-clean-html:
	rm -rf doxygen/html

doxygen-clean-latex:
	rm -rf doxygen/latex
