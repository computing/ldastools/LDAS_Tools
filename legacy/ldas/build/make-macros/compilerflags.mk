#------------------------------------------------------------------------
# Compiler configuration
#------------------------------------------------------------------------
#------------------------------------------------------------------------
# Fatal Warnings
#------------------------------------------------------------------------
ifneq "$(FATAL_WARNINGS)" "no"
ifneq "x$(CFLAGS)" "x"
AM_CFLAGS+=$(CFLAGS_FATAL_WARNINGS)
endif
ifneq "x$(CXXFLAGS)" "x"
AM_CXXFLAGS+=$(CXXFLAGS_FATAL_WARNINGS)
endif
endif

#------------------------------------------------------------------------
# Pedantic
#------------------------------------------------------------------------
ifneq "$(PEDANTIC)" "no"
ifneq "x$(CFLAGS)" "x"
AM_CFLAGS+=$(CFLAGS_PEDANTIC)
endif
ifneq "x$(CXXFLAGS)" "x"
AM_CXXFLAGS+=$(CXXFLAGS_PEDANTIC)
endif
endif

#------------------------------------------------------------------------
# Special variables
#------------------------------------------------------------------------
CFLAGS_NON_FATAL_WARNINGS:=$(patsubst $(CFLAGS_FATAL_WARNINGS),,$(AM_CFLAGS))
CXXFLAGS_NON_FATAL_WARNINGS:=$(patsubst $(CXXFLAGS_FATAL_WARNINGS),,$(AM_CXXFLAGS))
