#------------------------------------------------------------------------
# Definition of variables
#------------------------------------------------------------------------
TEXINPUTS=$(top_builddir)/doc:$(srcdir):$(top_srcdir)/doc:/var/tmp/texfonts:
FLATTEX=$(top_builddir)/utilities/FlatTeX
LATEX2HTML=perl $(srcdir)/../utilities/post_tth.pl

ifneq "$(LATEX)" ""
PSFILES = $(DVIFILES:.dvi=.ps)
ifneq "$(PS2PDF)" ""
PDFFILES = $(DVIFILES:.dvi=.pdf)
endif # HAVE_PS2PDF
ifneq "$(LATEX2HTML)" ""
HTMLFILES = $(DVIFILES:.dvi=.html)
TEXTFILES = $(DVIFILES:.dvi=.txt)
endif # HAVE_LATEX2HTML
endif # HAVE_LATEX

built_texfiles=\
	$(subst  $(top_builddir)/doc/,,$(shell ls $(top_builddir)/doc/lib/*.tex))

srcdir_texfiles=\
	$(addprefix $(srcdir),$(filter-out package_list.tex lib/%,$(shell sed -n -e 's/^\\input{\([^}]*\)}$$/\1/p' $(srcdir)/INSTALL.tex)))
topsrcdir_texfiles=\
	$(addprefix $(top_srcdir)/doc/, $(filter-out $(built_texfiles),$(shell sed -n -e 's/^\\input{\([^}]*\)}$$/\1/p' $(srcdir)/package_list.tex)))
topbuilddir_texfiles=\
	$(addprefix $(top_builddir)/doc/, $(filter $(built_texfiles),$(shell sed -n -e 's/^\\input{\([^}]*\)}$$/\1/p' $(srcdir)/package_list.tex)))

DEPENDS=\
	$(srcdir_texfiles) \
	$(topsrcdir_texfiles) \
	$(topbuilddir_texfiles)

#------------------------------------------------------------------------
# Default rules
# If there's no aux file to begin with, you usually need
# to run LaTeX one additional time
#------------------------------------------------------------------------
%.idx %.aux: %.tex
	env TEXINPUTS=$(TEXINPUTS) $(LATEX) $(*F).tex < /dev/null ; \
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file $(*F).tex"; \
		false; \
	fi

%.ind: %.idx
	env TEXINPUTS=$(TEXINPUTS) makeindex $(*F) < /dev/null ; \
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file $(*F).idx"; \
		false; \
	fi

# Need to run LaTeX twice to make sure the cross-refs are ok
%.dvi: %.tex %.aux %.ind
	env TEXINPUTS=$(TEXINPUTS) $(LATEX) $(*F).tex < /dev/null; \
	env TEXINPUTS=$(TEXINPUTS) $(LATEX) $(*F).tex < /dev/null; \
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file $(*F).tex"; \
		false; \
	fi

%.ps : %.dvi
	env TEXINPUTS=$(TEXINPUTS) \
	dvips $(*F) -o $(*F).ps < /dev/null

%.pdf : %.ps
	$(PS2PDF) $(*F).ps

%.html %.txt: %.tex
	env TEXINPUTS=$(TEXINPUTS) FLATTEX=$(FLATTEX) \
	$(LATEX2HTML) --source=$(srcdir)/$(*F) < /dev/null

clean-local-latex:
	rm -f *.dvi *.aux *.toc *.log *.pdf *.ps *.html *.txt *.ind *.idx
