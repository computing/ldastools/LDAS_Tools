MEMORY_DEBUG_TESTS ?= $(TESTS)
MEMORY_DEBUG_TESTS_ENVIRONMENT ?= $(TESTS_ENVIRONMENT)

memcheck: libumem-memcheck
mtsafe:

libumem-memcheck:
	if test -x /usr/bin/bcheck; \
	then $(MAKE) libumem-memcheck-bcheck; \
	else $(MAKE) libumem-memcheck-no-bcheck; \
	fi

libumem-memcheck-bcheck:
	$(MAKE) \
	  LIBUMEM_PROG=/usr/bin/bcheck \
	  LIBUMEM_TOOL=libumem-memcheck \
          LIBUMEM_ENV="-all" \
	  libumem-doit

libumem-memcheck-no-bcheck:
	$(MAKE) \
	  LIBUMEM_PROG=env \
	  LIBUMEM_TOOL=libumem-memcheck \
          LIBUMEM_ENV="UMEM_DEBUG=default,firewall=1 UMEM_OPTIONS=backend=mmap UMEM_LOGGING=transaction" \
	  libumem-doit

libumem-doit: $(MEMORY_DEBUG_TESTS)
	echo $(MEMORY_DEBUG_LD_LIBRARY_PATH)
	for x in $(MEMORY_DEBUG_TESTS);				\
	do							\
	  if test -x .libs/lt-$$x;				\
	  then							\
	    cmd=".libs/lt-$$x";					\
	  elif test -x .libs/$x;				\
	  then							\
	    cmd=".libs/$$x";					\
	  else							\
	    cmd=".libs/$$x";					\
	  fi;							\
	  echo $(MEMORY_DEBUG_TESTS_ENVIRONMENT)		\
	    env 						\
	    LD_LIBRARY_PATH_64="$(MEMORY_DEBUG_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH_64}" \
	    LD_PRELOAD_64=$(LIBUMEM)				\
	    $(LIBUMEM_PROG) $(LIBUMEM_ENV)                      \
            $$cmd;						\
	  $(MEMORY_DEBUG_TESTS_ENVIRONMENT)			\
	    env 						\
	    LD_LIBRARY_PATH_64="$(MEMORY_DEBUG_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH}" \
	    LD_PRELOAD_64=$(LIBUMEM)				\
	    $(LIBUMEM_PROG) $(LIBUMEM_ENV)                      \
            $$cmd > $$cmd.$(LIBUMEM_TOOL) 2>&1;			\
	done
