#ifndef FLAT_TEX_STYPE_HH
#define FLAT_TEX_STYPE_HH

#include <string>
#include <vector>

class SType
{
public:
  typedef enum {
    UNKNOWN,
    BOOLEAN,
    INTEGER,
    PARAMETER_BLOCKS,
    STRING
  } data_type;

  SType( );

  void
  Assign( const char* Text, std::string::size_type N )
  {
    m_type = STRING;
    m_string.assign( Text, N );
  }

  void AppendBlock( const std::string& Block );
  std::string GetBlock( unsigned int Offset );


  inline void
  BlocksReset( )
  {
    m_blocks.erase( m_blocks.begin( ), m_blocks.end( ) );
  }

  inline void
  Boolean( const std::string& Variable, bool Value )
  {
    m_type = BOOLEAN;
    m_string = Variable;
    m_boolean = Value;
  }

  inline bool
  Boolean( ) const
  {
    return m_boolean;
  }

  inline const std::string&
  String( ) const
  {
    return m_string;
  }

  inline void
  Type( data_type T )
  {
    m_type = T;
  }

  inline data_type
  Type( ) const
  {
    return m_type;
  }

  SType& operator= ( const char* Text );
  SType& operator= ( const std::string& Text );
  SType& operator= ( const int Number );
  SType& operator+= ( const SType& Source );

  inline operator int( ) const
  {
    return m_integer;
  }

  inline operator const std::string& ( ) const
  {
    return m_string;
  }

  inline void
  reset( )
  {
    m_type = UNKNOWN;
  }

private:
  friend std::ostream& operator<<( std::ostream& Stream,
				   const SType& Info );
  friend class FlatTeXParser;

  data_type			m_type;
  std::string			m_string;
  int				m_integer;
  std::vector< std::string >	m_blocks;
  bool				m_boolean;
};

#define YYSTYPE SType

#endif /* FLAT_TEX_STYPE_HH */
