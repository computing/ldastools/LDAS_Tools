package DocMode::LDASPkgBuilderStyle; # Namespace for private functions

$REQUIRED = "Required";
$OPTIONAL = "Optional";

%NeededInfo =
(
);

sub SplitURL( $ );

sub GetNeeded( $ )
{
    my( $rh_rpm ) = "CentOS_5";
    my ( $pkg ) = @_;
    my ( %retval ) =
	( "SunOSCVS" => $REQUIRED,
	  "SunOSTar" => $REQUIRED,
	  "RHLinuxCVS" => $REQUIRED,
	  "RHLinuxTar" => $REQUIRED,
	);
    if ( defined( $NeededInfo{$pkg->{"name"}} ) )
    {
	foreach ( keys %{$NeededInfo{$pkg->{"name"}}} )
	{
	    $retval{$_} = $NeededInfo{$pkg->{"name"}}{$_};
	}
    }
    if ( defined( $pkg->{$rh_rpm} ) )
    {
	my($rpm) = undef;
	foreach ( @{$pkg->{$rh_rpm}} )
	{
	    s,([_\#\$~^%{}]),\\$1,g;       # Protect TeX command caracters
	    if ( !defined( $rpm ) )
	    {
		$rpm = "\\mbox\{$_\}";
	    }
	    else
	    {
		$rpm .= " \\mbox\{$_\}";
	    }
	}
	$rpm = "RPM ($rpm)";
	if ( $#{$pkg->{$rh_rpm}} > 1 )
	{
	    $rpm = "\\parbox\{1.5in\}\{\\raggedright $rpm\}";
	}
	if ( $retval{"RHLinuxCVS"} eq $REQUIRED )
	{
	    $retval{"RHLinuxCVS"} = $rpm;
	}
	if ( $retval{"RHLinuxTar"} eq $REQUIRED )
	{
	    $retval{"RHLinuxTar"} = $rpm;
	}
    }
    return $retval{"SunOSCVS"},
	     $retval{"SunOSTar"},
	     $retval{"RHLinuxCVS"},
	     $retval{"RHLinuxTar"};
}

sub MultiLinks( $$ )
{
    my( $title, $links ) = @_;
    ::DocMode::Output $title . '&';
    my( @links ) = split ' ', $links;
    my( $len ) = $#links;
    my( $doneone ) = 0;
    if ( $len >= 0 )
    {
	::DocMode::Output "\\parbox\{3in}{\\raggedright %";
	foreach ( @links )
	{
	    ::DocMode::Output SplitURL( eval( "return \"$_\";" ) );
	    if ( $len )
	    {
		::DocMode::Output "\\linebreak";
	    }
	    $len -= 1;
	}
	::DocMode::Output "}";
    }
    ::DocMode::Output '\\\\';
}

sub SplitURL( $ )
{
    my( $url ) = @_;

    $url =~ s,([_\#\$~^%{}]),\\$1,g;       # Protect TeX command caracters
    $url =~ s,/([^/]+),/\\mbox\{$1\},g;    # Keep directory/file name together
    $url =~ s,/([^/]),/\\linebreak[0]$1,g; # Break on directory seperator
    return $url;
}

sub TeXProtect( $ )
{
    my($text) = @_;

    $text =~ s/([\#\\&_])/\\$1/g;
    return $text;
}

#------------------------------------------------------------------------
# This is the documentation style 
#------------------------------------------------------------------------
package DocMode; # Make sure we are in the correct namespace

sub DocumentStyle( $ )
{
    my ( $pkg ) = @_;
    my ( $SunOSCVS, $SunOSTar,
	 $RHLinuxCVS, $RHLinuxTar )
	= DocMode::LDASPkgBuilderStyle::GetNeeded( $pkg );

    #--------------------------------------------------------------------
    # Establish some globals in the environment space
    #--------------------------------------------------------------------
    $::ENV{"DEST"} = '{\STOWPREFIX}/' . $pkg->{"symbolic_name"} . '-{\pkgver}';
    $::ENV{"SCRATCH"} = LatexShellVariable( '${BUILD_DIR}' );
    $::ENV{"SOURCE"} = LatexShellVariable( '${PKG_DIR}' );

    #--------------------------------------------------------------------
    # Assign the package name and version
    #--------------------------------------------------------------------

    Output '\renewcommand{\pkgname}{' . $pkg->{"title"}. '}';
    Output '\renewcommand{\pkgver}{' . $pkg->{"version"}. '}';
    Output '';

    #--------------------------------------------------------------------
    # :TODO: Assign the CVS/Tar requirements for Sun and Linux OSs
    #--------------------------------------------------------------------

    Output '\renewcommand{\builddir}{' . $pkg->{"name"} . '-{\pkgver}}';
    Output '\renewcommand{\tarball}{{\builddir}.tar.gz}';
    Output '';
    Output '\renewcommand{\SunOSCVS}{' . $SunOSCVS . '}';
    Output '\renewcommand{\SunOSTar}{' . $SunOSTar . '}';
    Output '\renewcommand{\RHLinuxCVS}{' . $RHLinuxCVS . '}';
    Output '\renewcommand{\RHLinuxTar}{' . $RHLinuxTar . '}';
    Output '';
    Output '\ifInTable';
    Output '\SoftwareTableEntry{'.$pkg->{"name"}." ".$pkg->{"version"}.
	'}{'.$SunOSCVS.'}{'.$SunOSTar.'}{'.$RHLinuxCVS.'}{'.$RHLinuxTar.'}';
    Output '\else% ifInTable';
    Output '\subsection{{\pkgname} {\pkgver}}';

    #--------------------------------------------------------------------
    # Description
    #--------------------------------------------------------------------
    Output '\subsubsection{Description}';
    Output '\index{'.$pkg->{"title"}.'!description}%';
    Output '\index{3rd Party Packages!'. $pkg->{"title"}.'}%';
    Output( DocMode::LDASPkgBuilderStyle::TeXProtect( $pkg->{"description"} ) );

    #--------------------------------------------------------------------
    # Links
    #--------------------------------------------------------------------
    Output '\subsubsection{Links}';
    Output '\vspace{0.3cm}';
    Output '{\centering \begin{tabular}{|c|c|}';
    Output '\hline';
    Output 'Link Description&';
    Output 'Link\\\\';
    Output '\hline';
    Output '\hline';
    DocMode::LDASPkgBuilderStyle::MultiLinks( "Home Page",
					      $pkg->{"home_page"} );
    Output '\hline';
    
    DocMode::LDASPkgBuilderStyle::MultiLinks( "Download",
					      $pkg->{"download"} );
    Output '\hline';
    DocMode::LDASPkgBuilderStyle::MultiLinks( "Mirror",
					      $pkg->{"mirrors"} );
    Output '\hline';
    DocMode::LDASPkgBuilderStyle::MultiLinks( "LDAS Patch",
					      $pkg->{"patch"} );
    Output '\hline';
    Output '\end{tabular}\par}';
    Output '\vspace{0.3cm}';
    Output '';
    #--------------------------------------------------------------------
    # Remind users of the need for this package
    #--------------------------------------------------------------------
    Output '\index{'.$pkg->{"title"}.'!requirements}%';
    Output '\MiniSoftwareTable';
    Output '';
    #--------------------------------------------------------------------
    # :TODO: Build instructions
    #--------------------------------------------------------------------
    Output '\index{'.$pkg->{"title"}.'!build instructions}%';
    Output '\PkgBuilderInstructions{' . $pkg->{"name"} . '}';
    
    #--------------------------------------------------------------------
    # Terminate the table
    #--------------------------------------------------------------------
    Output '\fi% ifInTable';
    
}
