#!/usr/bin/env perl
#------------------------------------------------------------------------
# This script verifies that the correct versions of RPMs have been
#   installed.
#------------------------------------------------------------------------
use Getopt::Long;

#------------------------------------------------------------------------
# Subroutine to split an rpm into the package and version
#------------------------------------------------------------------------
sub rpm_split ($)
{
    ( $_ ) = @_;
    if ( /^(.*)-((\w+(\.(\w+))*)-(\w+(\.(\w+))*))$/ )
    {
	($1, $2);
    }
    else
    {
	$1;
    }
}

#------------------------------------------------------------------------
# Subroutine to read tex file for rpm references
#------------------------------------------------------------------------
sub process_tex_file ($)
{
    my($texfile) = @_;
    my($texbase) = $texfile;
    my($pkgname) = undef;
    my($pkgver) = undef;
    my($rpm) = undef;
    my(%cmds) = undef;

    $texbase =~ s/\.tex$//g;
    $texbase =~ s,^([^/]+/)*,,g;

    open INPUT, "$srcdir/$texfile";
    while( <INPUT> )
    {
	chop;
	if ( /^.*{\\pkgname}{(.*)}$/ )
	{
	    $pkgname = $1;
	}
	elsif ( /^.*{\\pkgver}{(.*)}$/ )
	{
	    $pkgver = $1;
	}
	elsif ( /^.*{\\rpm}{(.*)}$/ )
	{
	    $rpm = $1;
	}
	elsif ( /^\\newcommand{\\(.*)}{(.*)}$/ )
	{
	    $cmds{ $1 } = "$2";
	}
    }
    close INPUT;
    foreach ( keys %cmds )
    {
	if ( $_ =~ /^$/ )
	{
	    next;
	}
	if ( $pkgname =~ /\\$_/ )
	{
	    $pkgname =~ s/\\$_({})?/$cmds{$_}/g;
	}
	if ( $pkgver =~ /\\$_/ )
	{
	    $pkgver =~ s/\\?$_([{][}])?/$cmds{$_}/g;
	}
    }
    $rpm =~ s/\\pkgname/$pkgname/g;
    $rpm =~ s/\\pkgver/$pkgver/g;
    #--------------------------------------------------------------------
    # Check package version of RPM
    #--------------------------------------------------------------------
    foreach ( split(/;\s+/, $rpm) )
    {
	if ( $_ !~ /^$/ )
	{
	    my($pkg, $ver) = rpm_split( $_ );
	    
	    if ( $ver ne $RPMS{$pkg} )
	    {
		print "WARNING: $pkg-$ver should be $pkg-$RPMS{$pkg}\n";
	    }
	}
    }
    #--------------------------------------------------------------------
    # Check package version of source
    #--------------------------------------------------------------------
    if ( defined( $SINSTALL{$texbase} ) )
    {
	if ( $pkgver ne $SINSTALL{$texbase} )
	{
	    print "ERROR: Wrong version in $texfile ($pkgver instead of $SINSTALL{$texbase})\n";
	}
    }
    else
    {
	print "WARNING: Nothing known about: $texbase\n";
    }
}

#------------------------------------------------------------------------
# Options supportd by this command
#  --source=<dir> name of the source directory
#------------------------------------------------------------------------
GetOptions( "source=s" => \$srcdir,
	    "sinstall_group=s" => \$sinstall_group
);

#------------------------------------------------------------------------
# Main loop
#------------------------------------------------------------------------

$state = 0;

open RPMS, "rpm -qa | ";
while( <RPMS> )
{
    chop;
    my($pkg, $ver) = rpm_split( $_ );
    $RPMS{ $pkg } = $ver;
}
close RPMS;

open SINSTALL, "$sinstall_group";
while( <SINSTALL> )
{
    if ( /^\#/ )
    {
	next;
    }
    chop;
    my($pkg, $ver, $os) = split(/\s+/, $_ );
    $SINSTALL{ $pkg } = $ver;
}
close SINSTALL;

foreach ( @ARGV )
{
    open TEX, "$srcdir/$_";
    while( <TEX> )
    {
	if ( /^\\SoftwareTableStart$/ )
	{
	    $state = 1;
	}
	elsif ( ( $state == 1 ) &&
		( /^\\input{(.*)}$/ ) )
	{
	    process_tex_file "$1";
	}
	elsif ( /^\\SoftwareTableEnd$/ )
	{
	    $state = 0;
	    last;
	}
    }
    close TEX;
}

