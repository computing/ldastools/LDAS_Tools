/* -*- mode: c++; c-basic-offset: 2; -*- */
%skeleton "lalr1.cc"
%require "2.1a"
%defines
%output="FlatTeXParser.cc"
%file-prefix="FlatTeXParser"
%define "parser_class_name" "FlatTeXParser"

%{
#include <strings.h>
 
#include <cstdio>
#include <cstdlib>

#include "FlatTeXSType.hh"

   /* #define yyparse(a) FlatTeXParser::yyparse(a) */

#define YYSTYPE SType

namespace yy
{
  class FlatTeXParser;
}

class FlatTeXDriver;

#define YY_STYPE SType
%}

 // The parsing context.
%parse-param { FlatTeXDriver& Driver }
%lex-param   { FlatTeXDriver& Driver }

%{
#ifdef __FLATTEXPARSER_CC

#ifndef YY_DECL
#define YY_DECL \
  static yy::FlatTeXParser::token_type			\
    yylex( yy::FlatTeXParser::semantic_type* YYLval,	\
	   yy::FlatTeXParser::location_type* YYLloc,	\
	   FlatTeXDriver& Driver )
#endif /* YY_DECL */

#include "FlatTeXSType.hh"
#include "FlatTeXDriver.hh"

  //Declare it for the parser's sake
YY_DECL;

#endif /* __FLATTEXPARSER_CC */
%}

%token TKN_END 0

%token TKN_BLOCK
%token TKN_BOOLEAN
%token TKN_BOOLEAN_ASSIGN
%token TKN_CHARACTER
%token TKN_COMMAND
%token TKN_COMMAND_NAME
%token TKN_DEF
%token TKN_ELSE
%token TKN_FI
%token TKN_IF
%token TKN_IF_BLOCK
%token TKN_IF_FILE_EXISTS
%token TKN_IFTHENELSE
%token TKN_INPUT
%token TKN_INTEGER
%token TKN_NEWBOOLEAN
%token TKN_NEWCOMMAND
%token TKN_NEWIF
%token TKN_PARAM_COUNT
%token TKN_SETBOOLEAN
%token TKN_TITLE
%token TKN_RENEWCOMMAND

%start stmts
%%

stmts: /* empty */
	| stmts stmt
	;

stmt: 
	TKN_BOOLEAN_ASSIGN {
          switch ( Driver.Mode( ) )
	  {
	  case FlatTeXDriver::MODE_TTH:
	    break;
	  default:
	    Driver.output( ) << $1;
	    break;
	  }
	  Driver.Boolean( $1.String( ), $1.Boolean( ) );
	}
        | TKN_CHARACTER { Driver.output( $1.String( ) ); }
        | TKN_COMMAND blocks {
	  unsigned int	param_count = 0;
	  std::string	definition;
	  bool	have_command = Driver.GetCommandInfo( $1, param_count, definition );

#if 0
	  std::cerr << std::endl
		    << "DEBUG: Command Lookup: '" << $1
		    << "' param_count: " << param_count
		    << " definition: '" << definition
		    << "'"
		    << std::endl;
#endif /* 0 */
	  if ( have_command )
	  {
	    std::ostringstream	def;
	    if ( param_count > 0 )
	    {
	      for ( std::string::const_iterator
		      cur = definition.begin( ),
		      last = definition.end( );
		    cur != last;
		    ++cur )
	      {
		if ( *cur == '\\' )
		{
		  def << *cur;
		  ++cur;
		  if ( cur == last )
		  {
		    break;
		  }
		  def << *cur;
		} else if ( *cur == '#' ) {
		  std::string num;
		  ++cur;
		  while( isdigit( *cur ) )
		  {
		    num += *cur;
		    ++cur;
		  }
		  --cur;
		  std::istringstream	inum( num );
		  int			param;
		  inum >> param;
		  def << $2.GetBlock( param );
		} else {
		  def << *cur;
		}
	      }
	    }
	    else
	    {
	      def << definition;
	    }
#if 0
	    std::cerr << std::endl
		      << "DEBUG: Evaluating: '" << def.str( )
		      << "'"
		      << std::endl;
#endif /* 0 */
	    Driver.Evaluate( def.str( ) );
	  }
	  else
	  {
	    // Don't have a definition
	    Driver.output( $1 );
	  }
	}
	| conditional TKN_IF_BLOCK else_if {
	  if ( Driver.Boolean( $1 ) )
	  {
	    Driver.Evaluate( $2 );
	  }
	  else
	  {
	    Driver.Evaluate( $3 );
	  }
	}
        | if_file_exists block block block
	{
	  Driver.output( ) << $1
		    <<"{" << $2 << "}"
		    <<"{" << $3 << "}"
		    <<"{" << $4 << "}";
	}
	| TKN_NEWIF TKN_IF {
	  Driver.output( ) << "\\newif" << "\\if" << $2;
	}
        | TKN_TITLE block {
	  std::ostringstream	b;
	  Driver.Evaluate( $2.String( ), &b );

	  switch ( Driver.Mode( ) )
	  {
	  case FlatTeXDriver::MODE_TTH:
	    {
	      std::string	desc( b.str( ) );
	      Driver.output( ) << "\\title{";
	      for ( std::string::const_iterator
		      cur = desc.begin( ),
		      last = desc.end( );
		    cur != last;
		    ++cur )
	      {
		if ( '\\' == *cur  )
		{
		  ++cur;
		  if ( ( '\n' != *cur ) && ( '\\' != *cur ) )
		  {
		    Driver.output( ) << "\\" << *cur;
		  }
		}
		else
		{
		  switch( *cur )
		  {
		  case ':':
		    break;
		  case '\n':
		    Driver.output( ) << " ";
		    break;
		  default:
		    Driver.output( ) << *cur;
		    break;
		  }
		}
	      }
	      Driver.output( ) << "}";
	    }
	    break;
	  default:
	    Driver.output( ) << "\\title{" << b.str( ) << "}";
	    break;
	  }
	}
        | expression {
	  if ( $1.Type( ) == SType::BOOLEAN )
	  {
	    Driver.Expression( $1.Boolean( ) );
	  }
	  else
	  {
	    Driver.Expression( FlatTeXDriver::EXPRESSION_STRING );
	  }
	}
	| stmt_if_then_else
	| stmt_new_boolean
	| stmt_new_command
	| stmt_set_boolean
	;

else_if:
	TKN_ELSE TKN_IF_BLOCK TKN_FI {
	  $$ = $2;
	}
	| TKN_FI {
	  $$ = "";
	}
	;

expression:
	TKN_BOOLEAN block {
	  if ( Driver.HaveBoolean( $2 ) )
	  {
	    $$.Boolean( "__expr__", Driver.Boolean( $2 ) );
	  }
	  else
	  {
	    std::ostringstream	s;

	    s << "\\boolean{"  << $2 << "}";
	    $$ = s.str( );
	  }
	  Driver.output( ) << "\\boolean{" << $2 << "}";
	}
	;

conditional:
	TKN_IF TKN_COMMAND_NAME {
	  $$ = $2;
	}
	| TKN_IF {
	  $$ = $1;
	}
	;

stmt_if_then_else:
	TKN_IFTHENELSE block block block {
	  //:TODO: Need to really evaluate the conditional block

	  int	exp = Driver.EvalCondition( $2 );
	  switch( exp )
	  {
	  case FlatTeXDriver::EXPRESSION_UNKNOWN:
	  case FlatTeXDriver::EXPRESSION_STRING:
	    Driver.output( ) << "\\ifthenelse"
		      << '{' << $2 << '}'
		      << '{' << $3 << '}'
		      << '{' << $4 << '}';
	    break;
	  case FlatTeXDriver::EXPRESSION_TRUE:
	    Driver.Evaluate( $3 );
	    break;
	  case FlatTeXDriver::EXPRESSION_FALSE:
	    Driver.Evaluate( $4 );
	    break;
	  }
	}
	;

stmt_new_boolean:
	TKN_NEWBOOLEAN block {
	  switch ( Driver.Mode( ) )
	  {
	  case FlatTeXDriver::MODE_TTH:
	    break;
	  default:
	    Driver.output( ) << "\\newboolean" << "{" << $2 << "}";
	    break;
	  }
	  Driver.Boolean( $2, false );
	}
	;

stmt_new_command:
	TKN_DEF command_name parameters block {
	  Driver.NewCommand( $2, $3, $4 );
	}
	| TKN_NEWCOMMAND command_name parameters block {
	  Driver.NewCommand( $2, $3, $4 );
	}
	| TKN_RENEWCOMMAND command_name parameters block {
	  Driver.RenewCommand( $2, $3, $4 );
	}
	;

stmt_set_boolean:
	TKN_SETBOOLEAN block block {
	  if ( std::string( $3 ).compare( "true" ) == 0 )
	  {
	    Driver.Boolean( $2, true );
	  }
	  else
	  {
	    Driver.Boolean($2, false );
	  }
	  switch( Driver.Mode( ) )
	  {
	  case FlatTeXDriver::MODE_TTH:
	    break;
	  default:
	    Driver.output( ) << "\\setboolean{" << $2 << "}{" << $3 << "}";
	  }
	}
	;

blocks: /* empty */ {
          $$.Type( SType::PARAMETER_BLOCKS );
	  $$.BlocksReset( );
	}
        | blocks block {
          $$ = $1;
	  $$.AppendBlock( $2 );
	}
	;

block:
	'{' TKN_BLOCK '}' {
	  $$ = $2;
	}
	| '{' '}' {
          $$ = "";
	}
	;

parameters: /* empty */ { $$ = 0; }
	| TKN_PARAM_COUNT {
	  std::istringstream	text( $1.String( ) );
	  int			params;

	  if ( $1.String( )[ 0 ] == '[' ) {
	    char	c;
	    text >> c >> params >> c;
	  }
	  else if ( $1.String( )[ 0 ] == '*' ) {
	    char	c;
	    text >> c >> params;
	  }
	  else
	  {
	    text >> params;
	  }
	  $$ = params;
	}
	;

command_name: TKN_COMMAND_NAME {
          if ( $1.String( )[0] == '{' )
	  {
	    $$ = $1.String( ).substr( 1, $1.String( ).length( ) - 2 );
	  }
	  else
	  {
	    $$ = $1.String( );
	  }
	}
	;
 
if_file_exists: TKN_IF_FILE_EXISTS {  $$ = $1.String( ); }
	;
%%

YY_DECL
{
  return Driver.Lex( YYLval, YYLloc );
}

void yy::FlatTeXParser::
error( const location_type& Location, const std::string& Message )
{
#if 0
  std::cerr << "INFO: file: " << CurrentFilename( )
	    << " line: " << yylineno
	    << " Parsing error: " << Message << std::endl;
#endif /* 0 */
}
