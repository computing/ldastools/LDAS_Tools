#ifndef FLAT_TEX_LEXER_HH
#define FLAT_TEX_LEXER_HH

#include <map>
#include <list>
#include <string>
#include <vector>

#ifndef __FLEX_LEXER_H
#include <FlexLexer.h>
#endif /* __FLEX_LEXER_H */

#include "FlatTeXParser.hh"

class FlatTeXDriver;

//: Handling of lexical input of TeX sources
class FlatTeXLexer
  : public yyFlexLexer
{
public:
  typedef yy::FlatTeXParser::semantic_type              semantic_type;
  typedef yy::FlatTeXParser::location_type              location_type;

  FlatTeXLexer( );
  FlatTeXLexer( const char* Filename );
  FlatTeXLexer( std::istream* In, std::ostream* Out,
		FlatTeXLexer* Lexer = NULL );

  const std::string& CurrentFilename( ) const;

  virtual int yylex( );


  inline std::istream&
  YYIn( )
  {
    return *yyin;
  }

  inline std::ostream&
  YYOut( )
  {
    return *yyout;
  }

  inline void
  YYLloc( location_type* YYLloc )
  {
    m_yylloc = YYLloc;
  }

  inline void
  YYLval( semantic_type* YYLval )
  {
    m_yylval = YYLval;
  }

protected:
  bool getCommandInfo( const std::string& Name,
		       unsigned int& NumOfParams,
		       std::string& Definition	) const;
  void newCommand( const std::string& Name,
		   const int ParameterCount,
		   const std::string& Definition );
  void renewCommand( const std::string& Name,
		     const int ParameterCount,
		     const std::string& Definition );

private:
  friend class FlatTeXDriver;

  typedef struct command {
    inline
    command( )
    {
    }

    inline
    command( const std::string& Name,
	     unsigned int ParameterCount,
	     const std::string& Definition )
      : s_name( Name ),
	s_parameter_count( ParameterCount ),
	s_def( Definition )
    {
    }

    std::string			s_name;
    unsigned int		s_parameter_count;
    std::string			s_def;
  } command_type;

  typedef struct {
    struct yy_buffer_state*	s_buffer_state;
    std::string			s_filename;
    std::istream*		s_stream;
    int				s_lineno;
  } include_data_type;

  typedef std::map<const std::string, command_type>  command_map_type;
  typedef General::unordered_map<const std::string, bool>

  conditional_map_t;

  command_map_type*	m_command_table;
  command_map_type	m_command_table_local;

  //: Name of file currently being parsed
  std::string				m_current_file;
  //: List of included files
  std::list<include_data_type>		m_includes;
  //: Nesting level of parens
  int					m_paren_count;
  //: Number of blocks to associate with a command
  int					m_block_count;
  //: List of directories to search for sources
  std::list<std::string>		m_texinputs;

  //: Remember if the command name has been seen
  bool					m_have_command_name;

  unsigned int				m_start_of_token;

  int					m_previous_token;
  semantic_type*      			m_yylval;
  location_type*      			m_yylloc;

  void init( FlatTeXLexer* Lexer = NULL );

  //: Do initialization appropriate for command definitions
  void command_definition_init( );

  //: Do initialization appropriate for command substitution
  void command_sub_init( unsigned int BlockCount );

  //: Do initialization appropriate for command definitions
  void conditional_init( );

  //: Push on a new stream
  void push_stream( std::istream* InputStream, std::string Filename );
  //: Pop off a completed stream
  void pop_stream( void );

  inline int
  set_token( int Token )
  {
    m_previous_token = Token;
    return Token;
  }

};

inline const std::string& FlatTeXLexer::
CurrentFilename( ) const
{
  return m_current_file;
}

inline bool FlatTeXLexer::
getCommandInfo( const std::string& Name,
		unsigned int& NumOfParams,
		std::string& Definition	) const
{
  FlatTeXLexer::command_map_type::const_iterator
    cmd( m_command_table->find( Name ) );
  if ( cmd == m_command_table->end( ) )
  {
    return false;
  }
  NumOfParams = cmd->second.s_parameter_count;
  Definition = cmd->second.s_def;
  
  return true;
}

#endif /* FLAT_TEX_LEXER_HH */
