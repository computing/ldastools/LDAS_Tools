/* -*- mode: c++ -*- */
%option c++
%option outfile="FlatTeXLexer.cc"
%option yyclass="FlatTeXLexer"
%option noyywrap
%option never-interactive
%option yylineno
%option stack

/* These are the different parsing environments */
%x block
%x block_if
%x command_definition
%x if_name
%x input

COMMAND \\[[:alnum:]]+
COMMENT "%"[^\n]*[\n]?
%{
#include <string.h>

#include <cstdlib>
#include <cstdio>

#include <unistd.h>
#include <fstream>
#include <sstream>

#include "general/unordered_map.hh"

class FlatTeXDriver;

#include "FlatTeXSType.hh"
#include "FlatTeXLexer.hh"
#include "FlatTeXParser.hh"

  using std::cerr;
  using std::endl;

  using std::getenv;
  using std::exit;

  using std::malloc;
  using std::realloc;
  using std::free;

  using General::unordered_map;

  int l_ifcount = 0;

  typedef yy::FlatTeXParser::token token;
  namespace
  {
    int command_decode( const std::string& Command );
  }
%}

%%

%{
  //---------------------------------------------------------------------
  // Rules for handling block definitions
  //---------------------------------------------------------------------
%}
<block>[ \t\n]+ {
  if ( m_paren_count != 0 )
  {
    yymore( );
  }
 }
<block>"{" {
  m_paren_count++;
  if ( m_paren_count == 1 )
  {
    return set_token( *yytext );
  }
  else
  {
    yymore( );
  }
}
<block>"}" {
  m_paren_count--;
  if ( m_paren_count == 0 )
  {
    if ( *yytext == '}' )
    {
      --m_block_count;
      if ( m_block_count == 0 )
      {
	BEGIN(INITIAL);
      }
      return set_token( *yytext );
    }
    else
    {
      m_paren_count++;
      // Make the character available for the next iteration
      unput( yytext[ YYLeng( ) - 1 ] );
      // erase the character that was put back
      m_yylval->Assign( YYText( ), YYLeng( ) - 1 );
      return set_token( token::TKN_BLOCK );
    }
  }
  else
  {
    yymore( );
  }
}
<block>. {
  yymore( );
}
%{
  //---------------------------------------------------------------------
  // Rules for handling if block definitions
  //---------------------------------------------------------------------
%}
<block_if>{COMMENT} {
  m_start_of_token = YYLeng( );
  yymore( );
}
<block_if>{COMMAND} {
  int	cmd = command_decode( &yytext[ m_start_of_token ] );
  bool	ret = false;
  switch( cmd )
  {
  case token::TKN_IF:
    m_paren_count++;
    break;
  case token::TKN_ELSE:
  case token::TKN_FI:
    if ( cmd == token::TKN_FI )
    {
      m_paren_count--;
      if ( m_paren_count == 0 )
      {
	ret = true;
      }
    }
    else
    {
      if ( m_paren_count == 1 )
      {
	ret = true;
      }
    }
    if ( ret )
    {
      // Put back the token
      std::string buffer( &yytext[ m_start_of_token] );
      std::string::const_iterator
	cur = buffer.end( ),
	last = buffer.begin( );
      while( cur != last )
      {
	--cur;
	unput( *cur );
      }
      yytext[ m_start_of_token ] = '\0';
      BEGIN(INITIAL);
      (*m_yylval) = yytext;
      return set_token( token::TKN_IF_BLOCK );
    }
    break;
  default:
    break;
  }
  yymore( );
  m_start_of_token = yyleng;
}
<block_if>[ \t\n] {
  yymore( );
  m_start_of_token = yyleng;
}
<block_if>. {
  yymore( );
  m_start_of_token = yyleng;
}
%{
  //---------------------------------------------------------------------
  // Rules for handling command definitions
  //---------------------------------------------------------------------
%}
<command_definition>{COMMAND} {
  m_have_command_name = true;
  if ( m_paren_count )
  {
    yymore( );
  }
  else
  {
    m_yylval->Assign( YYText( ), YYLeng( ) );
    return set_token( token::TKN_COMMAND_NAME );
  }
}
<command_definition>"[" {
  m_paren_count++;
  yymore( );
}
<command_definition>"]" {
  m_paren_count--;
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return set_token( token::TKN_PARAM_COUNT );
}
<command_definition>#?[0-9]+ {
  if ( m_paren_count )
  {
    yymore( );
  }
  else
  {
    m_yylval->Assign( YYText( ), YYLeng( ) );
    return set_token( token::TKN_PARAM_COUNT );
  }
}
<command_definition>"{" {
  if ( m_have_command_name )
  {
    BEGIN(block);
    unput('{');
  }
  else
  {
    m_paren_count++;
    yymore( );
  }
}
<command_definition>"}" {
  m_paren_count--;
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return set_token( token::TKN_COMMAND_NAME );
}

%{
  //---------------------------------------------------------------------
  // Rules for handling if named with command syntax
  //---------------------------------------------------------------------
%}
<if_name>{COMMAND} {
  BEGIN( block_if );
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return token::TKN_COMMAND_NAME;
}
<if_name>. {
  unput( *yytext );
  BEGIN( block_if );
}

%{
  //---------------------------------------------------------------------
  // Rules for handling the replacement of input directives
  //---------------------------------------------------------------------
%}

<input>[ \t]*	/* eat white spaces */;
<input>\{[^\}]*\} {
  std::string		fullname;
  std::string		filename(&yytext[1], YYLeng( ) - 2);

  for ( std::list<std::string>::const_iterator prefix = m_texinputs.begin();
	prefix != m_texinputs.end();
	prefix++)
  {
    if (prefix->length())
    {
      fullname = *prefix;
      fullname += "/";
    }
    fullname += filename;
    if (access(fullname.c_str(), R_OK) == 0)
    {
      break;
    }
  }
  yyin = new std::ifstream( fullname.c_str() );
  push_stream( yyin, filename );

  yy_switch_to_buffer( yy_create_buffer( yyin, YY_BUF_SIZE) );
  BEGIN(INITIAL);
}

%{
  //---------------------------------------------------------------------
  // Initial state
  //---------------------------------------------------------------------
%}
"\\%" {
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return set_token( token::TKN_CHARACTER );
}
{COMMENT} {
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return set_token( token::TKN_CHARACTER );
}
{COMMAND} {
  m_yylval->Assign( YYText( ), YYLeng( ) );
  int retval = command_decode( m_yylval->String( ) );
  switch ( retval )
  {
  case token::TKN_BOOLEAN:
    {
      m_paren_count = 0;
      m_block_count = 1;
      BEGIN(block);
    }
    break;
  case token::TKN_BOOLEAN_ASSIGN:
    {
      std::string	s( YYText( ), YYLeng( ) );
      unsigned int 	l = 5;
      bool		value = false;
      if ( s.substr( s.length( ) - 4 ) == "true" )
      {
	l = 4;
	value = true;
      }
      m_yylval->Boolean( s.substr( 1, s.length( ) - ( l + 1 ) ), value );
    }
    break;
  case token::TKN_COMMAND:
    {
      FlatTeXLexer::command_map_type::const_iterator
	cmd( m_command_table->find( m_yylval->String( ) ) );

      if ( cmd != m_command_table->end( ) )
      {
	// --------------------------------------------------------------
	// Find out how many blocks are going to be used and set up
	//   for reading them.
	// --------------------------------------------------------------
	if ( cmd->second.s_parameter_count )
	{
	  command_sub_init( cmd->second.s_parameter_count );
	  BEGIN(block);
	}
      }
    }
    break;
  case token::TKN_DEF:
    {
      m_paren_count = 0;
      m_block_count = 1;
      BEGIN(command_definition);
    }
    break;
  case token::TKN_ELSE:
    {
      m_paren_count = 1;
      m_start_of_token = 0;
      BEGIN(block_if);
    }
    break;
  case token::TKN_IF:
    {
      m_paren_count = 1;
      m_start_of_token = 0;
      *m_yylval = m_yylval->String( ).substr( 3 );
      if ( m_previous_token != token::TKN_NEWIF )
      {
	if ( YYLeng( ) == 3 )
	{
	  BEGIN(if_name);
	}
	else
	{
	  BEGIN(block_if);
	}
      }
    }
    break;
  case token::TKN_IF_FILE_EXISTS:
    {
      conditional_init( );
      BEGIN(block);
    }
    break;
  case token::TKN_IFTHENELSE:
    {
      m_paren_count = 0;
      m_block_count = 3;
      BEGIN(block);
    }
    break;
  case token::TKN_INPUT:
    {
      BEGIN(input);
    }
    break;
  case token::TKN_NEWCOMMAND:
    {
      command_definition_init( );
      BEGIN(command_definition);
    }
    break;
  case token::TKN_NEWBOOLEAN:
    {
      m_paren_count = 0;
      m_block_count = 1;
      BEGIN(block);
    }
    break;
  case token::TKN_RENEWCOMMAND:
    {
      command_definition_init( );
      BEGIN(command_definition);
    }
    break;
  case token::TKN_SETBOOLEAN:
    {
      m_paren_count = 0;
      m_block_count = 2;
      BEGIN(block);
    }
    break;
  case token::TKN_TITLE:
    {
      m_paren_count = 0;
      m_block_count = 1;
      BEGIN(block);
    }
    break;
  }
  if ( retval != token::TKN_INPUT )
  {
    return set_token( retval );
  }
}
"\n" {
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return( set_token( token::TKN_CHARACTER ) );
}
. {
  m_yylval->Assign( YYText( ), YYLeng( ) );
  return( set_token( token::TKN_CHARACTER ) );
}

<<EOF>> {
  if ( m_includes.size() )
  {
    pop_stream();
  }
  else
  {
    yyterminate();
  }
}

%%

std::ostream&
operator<<( std::ostream& Stream, const SType& Info )
{
  switch( Info.m_type )
  {
  case SType::UNKNOWN:
    Stream << "<SType unknown>";
    break;
  case SType::BOOLEAN:
    Stream << "\\" << Info.m_string
	   << ( (Info.m_boolean ) ? "true" : "false" );
    break;
  case SType::INTEGER:
    Stream << Info.m_integer;
    break;
  case SType::PARAMETER_BLOCKS:
    Stream << "<SType PARAMETER_BLOCKS>";
    break;
  case SType::STRING:
    Stream << Info.m_string;
    break;
  }
  return Stream;
}

SType::
SType( )
  : m_type( UNKNOWN )
{
}

void SType::
AppendBlock( const std::string& Block )
{
#if 0
  std::cerr << "DEBUG: SType::AppendBlock:"
	    << " size: " << m_blocks.size( )
	    << " Block: " << Block
	    << std::endl;
#endif /* 0 */
  m_type = PARAMETER_BLOCKS;
  m_blocks.push_back( Block );
}

std::string SType::
GetBlock( unsigned int Offset )
{
  if ( ( m_type == PARAMETER_BLOCKS ) &&
       ( Offset <= m_blocks.size( ) ) )
  {
    return m_blocks[ Offset - 1 ];
  }
  return "";
}

SType& SType::
operator=( const char* Text )
{
  m_type = STRING;
  m_string = Text;
  return *this;
}

SType& SType::
operator=( const std::string& Text )
{
  m_type = STRING;
  m_string = Text;
  return *this;
}

SType& SType::
operator=( const int Text )
{
  m_type = INTEGER;
  m_integer = Text;
  return *this;
}

SType& SType::
operator+=( const SType& Source )
{
  if ( m_type == UNKNOWN )
  {
    m_type = Source.m_type;
  }
  if ( m_type == Source.m_type )
  {
    switch ( m_type )
    {
    case INTEGER:
      m_integer = Source.m_integer;
      break;
    case PARAMETER_BLOCKS:
      for ( std::vector< std::string >::const_iterator
	      cur = Source.m_blocks.begin( ),
	      last = Source.m_blocks.end( );
	    cur != last;
	    ++cur )
      {
	m_blocks.push_back( *cur );
      }
      break;
    case STRING:
      m_string += Source.m_string;
      break;
    default:
      break;
    }
  }
  return *this;
}

//
//
//

FlatTeXLexer::
FlatTeXLexer()
  : m_current_file( "__stdin__" )
{
  init( );
}

FlatTeXLexer::
FlatTeXLexer( const char* Filename )
  : m_current_file( Filename )
{
  init();
  yyin = new std::ifstream( Filename );
  if ( !yyin )
  {
    // error("Bad input file");
    exit(1);
  }
  yy_switch_to_buffer( yy_create_buffer( yyin, 16*1024 ) );

}

FlatTeXLexer::
FlatTeXLexer( std::istream* In, std::ostream* Out,
	      FlatTeXLexer* Lexer )
  : m_current_file( "__istream__" )
{
  init( Lexer );
  if ( In != (std::istream*)NULL )
  {
    yyin = In;
  }
  if ( Out != (std::ostream*)NULL )
  {
    yyout = Out;
  }

}

void FlatTeXLexer::
command_definition_init( )
{
  m_have_command_name = false;
  m_paren_count = 0;
  m_block_count = 1;
}

void FlatTeXLexer::
command_sub_init( unsigned int BlockCount )
{
  m_paren_count = 0;
  m_block_count = BlockCount;
}

void FlatTeXLexer::
conditional_init( )
{
  m_paren_count = 0;
  m_block_count = 3;
}

void FlatTeXLexer::
init( FlatTeXLexer* Lexer )
{
  yylineno = 1;
  m_paren_count = 0;
  if ( Lexer )
  {
    m_texinputs = Lexer->m_texinputs;
    m_command_table = Lexer->m_command_table;
  }
  else
  {
    //-------------------------------------------------------------------
    // Create and seed the command table
    //-------------------------------------------------------------------
    m_command_table = &m_command_table_local;

    newCommand( "\\boolean", 1, "#1" );
    newCommand( "\\multirow", 3, "#3" );
    //-------------------------------------------------------------------
    // Calculate where to search for files
    //-------------------------------------------------------------------
    int		offset;
    std::string	texinputs((getenv("TEXINPUTS") == (char*)NULL)
			  ? ""
			  : getenv("TEXINPUTS"));

    //:TRICKY: Assigment inside of comparison
    while((offset = texinputs.find_first_of(':')) >= 0)
    {
      m_texinputs.push_back(texinputs.substr(0, offset));
      texinputs.erase(0, offset+1);
    }
    m_texinputs.push_back(texinputs);
  }
}

void FlatTeXLexer::
newCommand( const std::string& Name,
	    int ParameterCount,
	    const std::string& Definition )
{
#if 0
  std::cerr << "DEBUG: newCommand: '" << Name
	    << "' ParameterCount: " << ParameterCount
	    << " Definition: '" << Definition
	    << "'"
	    << std::endl;
#endif /* 0 */
  (*m_command_table)[ Name ] =
    command_type( Name, ParameterCount, Definition );
}

void FlatTeXLexer::
renewCommand( const std::string& Name,
	    int ParameterCount,
	    const std::string& Definition )
{
#if 0
  std::cerr << "DEBUG: renewCommand: " << Name
	    << " ParameterCount: " << ParameterCount
	    << " Definition: " << Definition
	    << std::endl;
#endif /* 0 */
  (*m_command_table)[ Name ] =
    command_type( Name, ParameterCount, Definition );
}

void FlatTeXLexer::
push_stream( std::istream* InputStream, std::string Filename )
{
  include_data_type	d;
  d.s_buffer_state = YY_CURRENT_BUFFER;
  d.s_filename = m_current_file;
  d.s_lineno = yylineno;
  d.s_stream = InputStream;

  yylineno = 1;
  m_current_file = Filename;

  m_includes.push_back(d);
}

void FlatTeXLexer::
pop_stream( void )
{
  if ( m_includes.size() )
  {
    yy_delete_buffer( YY_CURRENT_BUFFER );
    yy_switch_to_buffer( m_includes.back().s_buffer_state );

    m_current_file = m_includes.back().s_filename;
    yylineno = m_includes.back().s_lineno;
    delete m_includes.back().s_stream;
    m_includes.pop_back();
  }
}

namespace
{
  int
  command_decode( const std::string& Command )
  {
    typedef unordered_map< std::string, int > cmd_map;
    static cmd_map	commands;

    if ( commands.size( ) == 0 )
    {
      commands[ "\\boolean" ]		= token::TKN_BOOLEAN;
      commands[ "\\def" ]		= token::TKN_DEF;
      commands[ "\\else" ]		= token::TKN_ELSE;
      commands[ "\\fi" ]		= token::TKN_FI;
      commands[ "\\IfFileExists" ]	= token::TKN_IF_FILE_EXISTS;
      commands[ "\\ifthenelse" ]	= token::TKN_IFTHENELSE;
      commands[ "\\input" ]		= token::TKN_INPUT;
      commands[ "\\newboolean" ]	= token::TKN_NEWBOOLEAN;
      commands[ "\\newcommand" ]	= token::TKN_NEWCOMMAND;
      commands[ "\\newif" ]		= token::TKN_NEWIF;
      commands[ "\\renewcommand" ]	= token::TKN_RENEWCOMMAND;
      commands[ "\\setboolean" ]	= token::TKN_SETBOOLEAN;
      commands[ "\\title" ]		= token::TKN_TITLE;
    }
    const cmd_map::const_iterator cur = commands.find( Command );
    if ( cur == commands.end( ) )
    {
      unsigned int l = Command.length( );

      if ( ( l >= 3 ) &&
	   ( Command.substr( 0, 3 ) == "\\if" ) )
      {
	return token::TKN_IF;
      }
      else if ( ( l >= 5 ) &&
		( Command.substr( Command.length( ) - 5 ) == "false" ) )
      {
	return token::TKN_BOOLEAN_ASSIGN;
      }
      else if ( ( l >= 4 ) &&
		( Command.substr( Command.length( ) - 4 ) == "true" ) )
      {
	return token::TKN_BOOLEAN_ASSIGN;
      }
      else
      {
	return token::TKN_COMMAND;
      }
    }
    return cur->second;
  }
} // namespace - anonymous
