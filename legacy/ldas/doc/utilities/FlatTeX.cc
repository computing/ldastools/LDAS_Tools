#include <unistd.h>
#include <stdlib.h>

#include <fstream>

#include "FlatTeXSType.hh"
#include "FlatTeXDriver.hh"

//-----------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------
int
main(int ArgC, char** ArgV)
{
  int	exit_status = 0;

  if ( ArgC == 1 )
  {
    try
    {
      FlatTeXDriver	driver;

      driver.Parse( &std::cin, &std::cout );
    }
    catch( const std::exception& e )
    {
      std::cerr << "FATAL: " << e.what( ) << std::endl;
      exit_status = 1;
    }
  }
  else
  {
    FlatTeXDriver	driver;
    std::ifstream	in;
	

    for ( int x = 1; x < ArgC; x++ )
    {
      try
      {
	in.open( ArgV[ x ] );
	driver.Parse( &in, &std::cout );
	in.close( );
      }
      catch( const std::exception& e )
      {
	std::cerr << "FATAL: " << e.what( ) << std::endl;
	exit_status = 1;
      }
    }	
  }
  exit( exit_status );
}
