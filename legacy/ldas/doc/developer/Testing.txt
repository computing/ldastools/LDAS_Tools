// -*- mode: c++ -*-
/*!
\page Testing HowTo validate the LDAS software suite

\section UnitTests Unit Tests

The first step in validating the LDAS software suite is built into the
process by which LDAS is built.
Each night when a new version of LDAS is built, the make
ldas-dist-check option is run.
This results in the validation of each of the tarball distributions
made available by the LDAS team.
The results for the past fourteen builds can be viewed at the following url.

\link
http://www.ldas-dev.ligo.caltech.edu/cgi-bin/create_nightly_test_index.cgi?history=14
\endlink


\section SysTests System Tests

\subsection SysTestsRunning Running Of The System Tests
The system tests are intended to validate the integration of all the
pieces of LDAS.
These tests are done usually once a week to verify that incrimental
changes have not disrupted the system as a whole.

The usual way of running tests is as the ldas user on the
developement system.

\code
runsystest dev SystemTest
\endcode

\subsection SysTestsAnalysis Recording Of The System Tests Results

\code
/ldas/sbin/SystemTestReport
\endcode

The following series of commands allows the results to be saved in
your local working reposity for the system test results.

<li>Set \b LDAS_TEST_REPOSITORY environment variable (CVS directory)
  <ul>
    <li>\b bourne: LDAS_TEST_REPOSITORY=${HOME}/local/src/ldas/ldas_results; export LDAS_TEST_REPOSITORY
    <li>\b csh: setenv LDAS_TEST_REPOSITORY ${HOME}/local/src/ldas/ldas_results
  </ul>
<li>Set \b LDAS_TEST_DATE environment variable (YYmmdd)
  <ul>
    <li>\b bourne: LDAS_TEST_DATE=100205; export LDAS_TEST_DATE
    <li>\b csh: setenv LDAS_TEST_DATE 100205
  </ul>
\code
cd ${LDAS_TEST_REPOSITORY}
ssh ldas@ldas-dev /ldas/sbin/SystemTestReport > reports/results-${LDAS_TEST_DATE}.html
cvs add reports/results-${LDAS_TEST_DATE}.html
${VISUAL} ${LDAS_TEST_REPOSITORY}/system_test_archive.html"
\endcode

\subsection SysTestPublishing Publishing the Results For Others

\code
\endcode
*/
