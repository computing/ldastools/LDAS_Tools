dnl ---------------------------------------------------------------------------
dnl Process this file with autoconf to produce a configure script.
dnl ---------------------------------------------------------------------------

AC_INIT(framecpp,1.0.2,http://www.ligo.caltech.edu,framecpp)
AC_CONFIG_AUX_DIR([.])
AC_CONFIG_MACRO_DIR([../../build/ac-macros])
AC_CONFIG_SRCDIR(src/FrameCPP.hh)

AM_INIT_AUTOMAKE([1.11 foreign tar-ustar silent-rules])
LDAS_SILENT_RULES([yes])

LT_INIT

AC_LDAS_INIT

AC_LDAS_ARG_WITH_OPTIMIZATION

dnl ---------------------------------------------------------------------------
dnl Automake Macros
dnl ---------------------------------------------------------------------------

AM_CONFIG_HEADER(src/config.h)
AH_TOP([
#ifndef FrameCPPConfigH
#define FrameCPPConfigH

#include "framecpp/undef.h"
])
AH_BOTTOM([

#endif /* FrameCPPConfigH */
])

dnl ---------------------------------------------------------------------------
dnl Checks for programs
dnl ---------------------------------------------------------------------------

AC_PROG_AWK
AC_LDAS_PROG_CC
AC_LDAS_PROG_CXX
AC_LDAS_PROG_LIBTOOL
AC_PROG_INSTALL
AC_LDAS_PROG_PCE2
LDAS_PROG_SWIG

dnl ---------------------------------------------------------------------------
dnl Check for libs
dnl ---------------------------------------------------------------------------

AC_LDAS_WITH_CCMALLOC
AC_LDAS_CHECK_LIB_Z
AC_LDAS_CHECK_LIB_BZ2(no)
AC_LDAS_CHECK_LIB_FRAMEL
LDAS_LIB_MD5SUM

dnl ---------------------------------------------------------------------------
dnl Check for Packages
dnl ---------------------------------------------------------------------------

AC_LDAS_CHECK_PACKAGE_GENERAL
LDAS_PACKAGE_PYTHON

dnl ---------------------------------------------------------------------------
dnl Checks for header files.
dnl ---------------------------------------------------------------------------
AC_HEADER_STDC
AC_CHECK_HEADERS([endian.h])

dnl ---------------------------------------------------------------------------
dnl Checks for typedefs, structures, and compiler characteristics
dnl ---------------------------------------------------------------------------
AC_C_BIGENDIAN
AC_TYPE_SIZE_T
AC_CHECK_SIZEOF( short, 0 )
AC_CHECK_SIZEOF( int, 0 )
AC_CHECK_SIZEOF( long, 0 )
AC_CHECK_SIZEOF( long long, 0 )
AC_CHECK_SIZEOF( float, 0 )
AC_CHECK_SIZEOF( double, 0 )

dnl ---------------------------------------------------------------------
dnl -- check for depricated support
dnl ---------------------------------------------------------------------
AC_ARG_ENABLE(depricated-framecpp,
	[ --disable-depricated-framecpp	Allow depricated API calls],
	[],
	[enable_depricated_framecpp=no])

AC_MSG_CHECKING(if depricated frameCPP calls are enabled)
case x${enable_depricated_framecpp} in
xyes)
  AC_MSG_RESULT(yes)
  AC_MSG_WARN(Use of the depricated interface is discouraged)
  AC_DEFINE(ENABLE_DEPRICATED_FRAMECPP,1,[Enable depricated API interface])
  ;;
*)
  AC_MSG_RESULT(no)
  ;;
esac

AC_LDAS_FINISH

AC_OUTPUT( Makefile
	   framecppc.pc
	   framecpp.pc
	   framecpp8.pc
	   framecpp7.pc
	   framecpp6.pc
	   framecpp4.pc
	   framecpp3.pc
	   framecpp_common.pc
	   framecpp.spec
	   framecpp-user-env.csh
	   framecpp-user-env.sh
           src/Makefile
	   src/Common/Makefile
	   src/Version3/Makefile
	   src/Version4/Makefile
	   src/Version6/Makefile
	   src/Version7/Makefile
	   src/Version8/Makefile
	   src/OOInterface/Makefile
	   src/CInterface/Makefile
	   src/CInterface/framecppc.pc
           doc/Makefile
           doc/html/Makefile
           doc/html/framecpp.html
	   test/Makefile )
