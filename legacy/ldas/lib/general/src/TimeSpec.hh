#ifndef GENERAL__TIME_SPEC_HH
#define GENERAL__TIME_SPEC_HH

#include <ldas_tools_config.h>

# include <sys/types.h>
# if TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
# else
#  if HAVE_SYS_TIME_H
#   include <sys/time.h>
#  else
#   include <time.h>
#  endif
# endif


# ifdef ST_MTIM_NSEC
#  define ST_TIME_CMP_NS(a, b, ns) ((a).ns < (b).ns ? -1 : (a).ns > (b).ns)
# else
#  define ST_TIME_CMP_NS(a, b, ns) 0
# endif
# define ST_TIME_CMP(a, b, s, ns) \
    ((a).s < (b).s ? -1 : (a).s > (b).s ? 1 : ST_TIME_CMP_NS(a, b, ns))
# define ATIME_CMP(a, b) ST_TIME_CMP (a, b, st_atime, st_atim.ST_MTIM_NSEC)
# define CTIME_CMP(a, b) ST_TIME_CMP (a, b, st_ctime, st_ctim.ST_MTIM_NSEC)
# define MTIME_CMP(a, b) ST_TIME_CMP (a, b, st_mtime, st_mtim.ST_MTIM_NSEC)

# ifdef ST_MTIM_NSEC
#  define TIMESPEC_NS(timespec) ((timespec).ST_MTIM_NSEC)
# else
#  define TIMESPEC_NS(timespec) 0
# endif

#endif
