#ifndef GENERAL__REF_PTR_HH
#define GENERAL__REF_PTR_HH

#if DEPRICATED
namespace General
{
  template<class X> class RefPtr
  {
  public:
    RefPtr();

    RefPtr( X* Data );

    RefPtr( const RefPtr< X >& Source );

    ~RefPtr( );

    unsigned int Count( ) const;

    bool operator==( const class RefPtr< X >& RighSide ) const;

    bool operator!=( const class RefPtr< X >& RighSide ) const;
    
    RefPtr& operator=( const RefPtr< X >& Source );

    X* operator->() const;

    X& operator*() const;

    const X* get( ) const;

#if ! UNIT_TEST
  private:
#endif	/* ! UNIT_TEST */
    class data_holder;

    data_holder*	m_data;

    void add( );

    void remove( );

  }; // template<class X> class General::RefPtr

  template<class X> inline bool RefPtr<X>::
  operator==( const class RefPtr< X >& RightSide ) const
  {
    return m_data == RightSide.m_data;
  }

  template<class X> inline bool RefPtr<X>::
  operator!=( const class RefPtr< X >& RightSide ) const
  {
    return m_data != RightSide.m_data;
  }
} // namespace General


#include "RefPtr.icc"
#else /* DEPRICATED */
#warn Should use std::shared_ptr instead of General::RefPtr
#endif /* DEPRICATED */


#endif /* GENERAL__REF_PTR_HH */
