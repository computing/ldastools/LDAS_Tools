/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef GENERAL__REF_PTR_ICC
#define GENERAL__REF_PTR_ICC

#include <pthread.h>


//-----------------------------------------------------------------------
/// Forward declaration of class
//-----------------------------------------------------------------------

template<class X> class General::RefPtr<X>::data_holder
{
public:
  data_holder();

  data_holder( X* Data );

  unsigned int count();

  data_holder& operator++();

  bool dec( );

  X* operator->() const;

  X& operator*() const;

#if ! UNIT_TEST
private:
#endif	/* UNIT_TEST */
  X*			m_data;
  unsigned int		m_count;
  pthread_mutex_t	m_lock;

  void lock();

  void unlock();

};

template<class X>
inline unsigned int General::RefPtr<X>::data_holder::
count( )
{
  return m_count;
}

template<class X>
inline void General::RefPtr<X>::data_holder::
lock( )
{
  pthread_mutex_lock( &m_lock );
}

template<class X>
inline void General::RefPtr<X>::data_holder::
unlock( )
{
  pthread_mutex_unlock( &m_lock );
}

template<class X>
inline X* General::RefPtr<X>::data_holder::
operator->() const
{
  return m_data;
}

template<class X>
inline X& General::RefPtr<X>::data_holder::
operator*() const
{
  return *m_data;
}

//-----------------------------------------------------------------------
/// Public
//-----------------------------------------------------------------------

template<class X> General::RefPtr<X>::
RefPtr( )
  : m_data( new data_holder )
{
}

template<class X> General::RefPtr<X>::
RefPtr( X* Data )
  : m_data( new data_holder( Data ) )
{
}

template<class X> General::RefPtr< X >::
RefPtr( const RefPtr< X >& Source )
{
  m_data = Source.m_data;
  add( );
}

template< class X > General::RefPtr< X >::
~RefPtr( )
{
  remove( );
}

template< class X > unsigned int General::RefPtr< X >::
Count( ) const
{
  if( m_data == (data_holder*)NULL )
  {
    return 0;
  }
  return m_data->count( );
}

template<class X> General::RefPtr< X >& General::RefPtr<X>::
operator=( const General::RefPtr< X >& Source )
{
  if ( m_data != Source.m_data )
  {
    remove( );
    m_data = Source.m_data;
    add( );
  }
  return *this;
}

template<class X>
inline X* General::RefPtr<X>::
operator->() const
{
  return m_data->operator->();
}

template<class X>
inline X& General::RefPtr<X>::
operator*() const
{
  return m_data->operator*();
}


template<class X>
inline const X* General::RefPtr<X>::
get( ) const
{
  return m_data->operator->( );
}

  
//-----------------------------------------------------------------------
/// private
//-----------------------------------------------------------------------

template<class X> void General::RefPtr<X>::
add( )
{
  m_data->operator++();
}

template <class X> void General::RefPtr<X>::
remove( )
{
  if ( m_data->dec( ) )
  {
    delete m_data;
    m_data = (data_holder*)NULL;
  }
}

template <class X> General::RefPtr<X>::data_holder::
data_holder()
  : m_data( (X*)0 ),
    m_count( 1 )
{
  pthread_mutex_init( &m_lock, NULL );
}

template<class X> General::RefPtr<X>::data_holder::
data_holder( X* Data )
  : m_data( Data ),
    m_count( 1 )
{
  pthread_mutex_init( &m_lock, NULL );
}

template <class X> typename General::RefPtr<X>::data_holder&
 General::RefPtr<X>::data_holder::operator++()
{
  lock();
  m_count++;
  unlock();
  return *this;
}

template<class X> bool General::RefPtr<X>::data_holder::
dec( )
{
  lock();

  bool retval( false );

  if ( m_count > 0 )
  {
    m_count--;
  }
  if ( m_count == 0 )
  {
    delete m_data;
    m_data = (X*)0;
    retval = true;
  }
  unlock();
  return retval;
}

#endif /* GENERAL__REF_PTR_ICC */
