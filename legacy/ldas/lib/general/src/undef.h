#include "general/undef_ac.h"

#undef PACKAGE
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION
#undef VERSION
#undef HAVE_LIBOSPACE
#undef _FILE_OFFSET_BITS
