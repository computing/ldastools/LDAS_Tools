#if HAVE_CONFIG_H
#include "general/config.h"
#endif /* HAVE_CONFIG_H */

#define UNIT_TEST 1
#include "general/RefPtr.hh"
#undef UNIT_TEST

#include "general/unittest.h"

int main( int ArgC, char** ArgV );

using namespace General;

UnitTest Test;

static void
test( )
{
  // Test Constructors
  RefPtr<int>	a;
  Test.Check( a.m_data->count() == 1 )
    << "Ref Count starts at 1 (" << a.m_data->count() << ")" << std::endl;
  RefPtr<int>	b( new int(5) );
  Test.Check( b.m_data->count() == 1 )
    << "Ref Count starts at 1 (" << b.m_data->count() << ")" << std::endl;
  RefPtr<int>	c( b );
  Test.Check( c.m_data->count() == 2 )
    << "Ref Count starts at 2 (" << c.m_data->count() << ")" << std::endl;
  {
    RefPtr<int>	d( c );
    Test.Check( b.m_data->count() == 3 )
      << "Ref Count starts at 3 (" << c.m_data->count() << ")" << std::endl;
    Test.Check( c.m_data->count() == 3 )
      << "Ref Count starts at 3 (" << c.m_data->count() << ")" << std::endl;
    Test.Check( d.m_data->count() == 3 )
      << "Ref Count starts at 3 (" << c.m_data->count() << ")" << std::endl;
  }
  Test.Check( b.m_data->count() == 2 )
    << "Ref Count starts at 2 (" << c.m_data->count() << ")" << std::endl;
  Test.Check( c.m_data->count() == 2 )
    << "Ref Count starts at 2 (" << c.m_data->count() << ")" << std::endl;

  *b += 10;
  Test.Check( *c == 15 ) << "Dereference check 15 (" << *c << ")" << std::endl;

  RefPtr<int> d;
  d = c;
  Test.Check( d.m_data->count() == 3 )
    << "Ref Count starts at 3 (" << d.m_data->count() << ")" << std::endl;

  d = c;
  Test.Check( d.m_data->count() == 3 )
    << "Ref Count starts at 3 (" << d.m_data->count() << ")" << std::endl;
  d = d;
  Test.Check( d.m_data->count() == 3 )
    << "Ref Count starts at 3 (" << d.m_data->count() << ")" << std::endl;

  RefPtr<int> e;
  d = e;
  Test.Check( d.m_data->count() == 2 &&
	      c.m_data->count() == 2 )
		<< "Ref Count incremented 2 (" << d.m_data->count() << ")" 
		<< " Ref Count down shifted to 2 (" << c.m_data->count() << ")" 
		<< std::endl;
}

int
main( int ArgC, char** ArgV )
{
  Test.Init( ArgC, ArgV );

  test( );

  Test.Exit( );
}
