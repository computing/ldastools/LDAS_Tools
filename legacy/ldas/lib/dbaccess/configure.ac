dnl -*- M4 -*-
dnl ---------------------------------------------------------------------------
dnl Process this file with autoconf to produce a configure script.
dnl ---------------------------------------------------------------------------

AC_INIT(dbaccess,1.0.0,http://www.ligo.caltech.edu,dbaccess)
AC_CONFIG_AUX_DIR([.])
AC_CONFIG_MACRO_DIR([../../build/ac-macros])
AC_CONFIG_SRCDIR(src/dbaccess.cc)

AM_INIT_AUTOMAKE([1.11 foreign tar-ustar silent-rules])
LDAS_SILENT_RULES([yes])

LT_INIT

AC_LDAS_INIT

AC_LDAS_ARG_WITH_OPTIMIZATION

dnl ---------------------------------------------------------------------------
dnl Automake Macros
dnl ---------------------------------------------------------------------------

AM_CONFIG_HEADER(src/config.h)
AH_TOP([
#ifndef DBAccessConfigH
#define DBAccessConfigH

#include "dbaccess/undef.h"

])
AH_BOTTOM([

#endif /* DBAccessConfigH */
])
dnl ---------------------------------------------------------------------------
dnl Checks for programs
dnl ---------------------------------------------------------------------------

AC_PROG_AWK
AC_LDAS_PROG_CXX
AC_LDAS_PROG_LIBTOOL
AC_LANG_CPLUSPLUS
AC_PROG_INSTALL
LDAS_PROG_BISON
LDAS_PROG_LEX(src/db2_lex.cc)
AC_LDAS_PROG_PCE2

LDAS_SYS_64BIT_OS

dnl ---------------------------------------------------------------------------
dnl Checks for header files.
dnl ---------------------------------------------------------------------------

AC_HEADER_STDC
AC_HEADER_DIRENT

dnl ---------------------------------------------------------------------------
dnl Check for additional LDAS packages
dnl ---------------------------------------------------------------------------

AC_LDAS_CHECK_LIB_OSPACE(no)
AC_LDAS_CHECK_PACKAGE_GENERAL
AC_LDAS_CHECK_PACKAGE_ILWD

dnl ---------------------------------------------------------------------------
dnl Look for library features
dnl ---------------------------------------------------------------------------

AC_MSG_CHECKING([for getdomainname function])
AC_TRY_LINK([ #include <unistd.h> ],
	    [ char buffer[256]; getdomainname(buffer, sizeof(buffer));],
	    [ AC_MSG_RESULT(yes); AC_DEFINE(HAVE_FUNC_GETDOMAINNAME,1,[Defined if the system supports the function getdomainname( )])],
	    [ AC_MSG_RESULT(no) ] )

CFLAGS="${CFLAGS} -DPREFIX=\\\"$prefix\\\""
CXXFLAGS="${CXXFLAGS} -DPREFIX=\\\"$prefix\\\""

dnl ---------------------------------------------------------------------------
dnl Checks for typedefs, structures, and compiler characteristics
dnl ---------------------------------------------------------------------------

AC_LDAS_FINISH

AC_OUTPUT( Makefile 
           src/Makefile
	   test/Makefile
	   doc/Makefile
	   doc/html/Makefile
	   doc/html/dbaccess.html
	   doc/html/introduction.html
           )
