#ifdef HAVE_CONFIG_H
#include "config.h"
#endif	/* HAVE_CONFIG_H */


#include "general/unittest.h"
#include "general/Memory.hh"

#include "ilwd/ldascontainer.hh"

#include "dbaccess/Table.hh"
#include "dbaccess/Transaction.hh"
#include "dbaccess/Process.hh"

General::UnitTest	Test;
using namespace std;   

void
simple(void)
{
  try {
    std::unique_ptr< DB::Table >
      p(DB::Table::CreateTable("process"));

    p->AppendColumnEntry("program", "sample process");
    p->AppendColumnEntry("version", "version");
    p->AppendColumnEntry("cvs_repository", "repository");
    p->AppendColumnEntry("cvs_entry_time", 0);
    p->AppendColumnEntry("start_time", 0);
    p->AppendColumnEntry("unix_procid", 1001);
    p->AppendColumnEntry("node", "localhost");
    p->AppendColumnEntry("username", "me");

    p->AppendColumnEntry("program", "sample process");
    p->AppendColumnEntry("version", "version");
    p->AppendColumnEntry("cvs_repository", "repository");
    p->AppendColumnEntry("cvs_entry_time", 0);
    p->AppendColumnEntry("start_time", 0);
    p->AppendColumnEntry("unix_procid", 1002);
    p->AppendColumnEntry("node", "localhost");
    p->AppendColumnEntry("username", "me");


    Test.Message() << "simple";
    std::unique_ptr< ILwd::LdasContainer > ilwd( p->GetILwd() );
    ilwd->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }
  catch (const std::exception& e)
  {
    Test.Check(false) << "Caught excpetion: " << e.what() << endl;
  }
  catch (...)
  {
    Test.Check(false) << "Caught unknown excpetion" << endl;
  }
}

void
ilwd(void)
{
  const char ilwd_str[] =
"<ilwd jobid='88' name='ligo:ldas:file' size='2'>"
"    <ilwd name='processgroup:process:table' size='12'>"
"        <ilwd name='processgroup:process:program'>"
"            <char dims='10' name='processgroup:process:program'>wrapperAPI</char>"
"        </ilwd>"
"        <ilwd name='processgroup:process:process_id'>"
"            <char dims='20' name='processgroup:process:process_id'>process:process_id:0</char>"
"        </ilwd>"
"        <ilwd name='processgroup:process:version'>"
"            <char dims='5' name='processgroup:process:version'>1.0.0</char>"
"        </ilwd>"
"        <ilwd name='processgroup:process:cvs_repository'>"
"            <char dims='35' name='processgroup:process:cvs_repository'>/ldcg_server/common/repository/ldas</char>"
"        </ilwd>"
"        <int_4s name='processgroup:process:cvs_entry_time'>985835043</int_4s>"
"        <ilwd name='processgroup:process:comment'>"
"            <char dims='33' name='processgroup:process:comment'>wrapperAPI is running on 3 nodes.</char>"
"        </ilwd>"
"        <int_4s name='processgroup:process:is_online'>0</int_4s>"
"        <ilwd name='processgroup:process:node'>"
"            <char dims='3' name='processgroup:process:node'>m71</char>"
"        </ilwd>"
"        <ilwd name='processgroup:process:username'>"
"            <char dims='7' name='processgroup:process:username'>mbarnes</char>"
"        </ilwd>"
"        <int_4s name='processgroup:process:unix_procid'>30055</int_4s>"
"        <int_4s name='processgroup:process:start_time'>986000353</int_4s>"
"        <int_4s name='processgroup:process:end_time'>986000360</int_4s>"
"    </ilwd>"
"    <ilwd name='process_paramsgroup:process_params:table' size='5'>"
"        <ilwd name='process_paramsgroup:process_params:program' size='13'>"
"            <char comment='-nodelist param: subset of nodes for actual calculations' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-realTimeRatio param: the desired ratio of the time required to process the data to the time contained within the data segment' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-resultAPI param: hostname and socketport for API which will receive the parallel computation data products' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-filterparams param: list of parameters used to control the parallel filter algorithm' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-mpiAPI param: hostname and socketport for mpiAPI' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-dynlib param: full path of dso library containing the indexed filter algorithms' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-communicateOutput param: specifies the model of the output structure data object used by the filter argorithm' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-np param: total nodes in COMM_WORLD' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-dataAPI param: hostname and socketport for the API to provide input ILWD data' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-jobID param: LDAS job identification associated with this particular instance of the wrapperAPI' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-dataDistributor param: defines the method for distributing input data from the master to the slaves' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-uniqueID param: unique identification in the form seconds.milliseconds associated with this particular instance of the wrapperAPI' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"            <char comment='-doLoadBalance param: to enable or disable load balancing of the parallel job' dims='10' name='process_paramsgroup:process_params:program'>wrapperAPI</char>"
"        </ilwd>"
"        <ilwd name='process_paramsgroup:process_params:process_id' size='13'>"
"            <char comment='-nodelist param: subset of nodes for actual calculations' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-realTimeRatio param: the desired ratio of the time required to process the data to the time contained within the data segment' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-resultAPI param: hostname and socketport for API which will receive the parallel computation data products' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-filterparams param: list of parameters used to control the parallel filter algorithm' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-mpiAPI param: hostname and socketport for mpiAPI' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-dynlib param: full path of dso library containing the indexed filter algorithms' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-communicateOutput param: specifies the model of the output structure data object used by the filter argorithm' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-np param: total nodes in COMM_WORLD' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-dataAPI param: hostname and socketport for the API to provide input ILWD data' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-jobID param: LDAS job identification associated with this particular instance of the wrapperAPI' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-dataDistributor param: defines the method for distributing input data from the master to the slaves' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-uniqueID param: unique identification in the form seconds.milliseconds associated with this particular instance of the wrapperAPI' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"            <char comment='-doLoadBalance param: to enable or disable load balancing of the parallel job' dims='20' name='process_paramsgroup:process_params:process_id'>process:process_id:0</char>"
"        </ilwd>"
"        <ilwd name='process_paramsgroup:process_params:param' size='13'>"
"            <char comment='-nodelist param: subset of nodes for actual calculations' dims='9' name='process_paramsgroup:process_params:param'>-nodelist</char>"
"            <char comment='-realTimeRatio param: the desired ratio of the time required to process the data to the time contained within the data segment' dims='14' name='process_paramsgroup:process_params:param'>-realTimeRatio</char>"
"            <char comment='-resultAPI param: hostname and socketport for API which will receive the parallel computation data products' dims='10' name='process_paramsgroup:process_params:param'>-resultAPI</char>"
"            <char comment='-filterparams param: list of parameters used to control the parallel filter algorithm' dims='13' name='process_paramsgroup:process_params:param'>-filterparams</char>"
"            <char comment='-mpiAPI param: hostname and socketport for mpiAPI' dims='7' name='process_paramsgroup:process_params:param'>-mpiAPI</char>"
"            <char comment='-dynlib param: full path of dso library containing the indexed filter algorithms' dims='7' name='process_paramsgroup:process_params:param'>-dynlib</char>"
"            <char comment='-communicateOutput param: specifies the model of the output structure data object used by the filter argorithm' dims='18' name='process_paramsgroup:process_params:param'>-communicateOutput</char>"
"            <char comment='-np param: total nodes in COMM_WORLD' dims='3' name='process_paramsgroup:process_params:param'>-np</char>"
"            <char comment='-dataAPI param: hostname and socketport for the API to provide input ILWD data' dims='8' name='process_paramsgroup:process_params:param'>-dataAPI</char>"
"            <char comment='-jobID param: LDAS job identification associated with this particular instance of the wrapperAPI' dims='6' name='process_paramsgroup:process_params:param'>-jobID</char>"
"            <char comment='-dataDistributor param: defines the method for distributing input data from the master to the slaves' dims='16' name='process_paramsgroup:process_params:param'>-dataDistributor</char>"
"            <char comment='-uniqueID param: unique identification in the form seconds.milliseconds associated with this particular instance of the wrapperAPI' dims='9' name='process_paramsgroup:process_params:param'>-uniqueID</char>"
"            <char comment='-doLoadBalance param: to enable or disable load balancing of the parallel job' dims='14' name='process_paramsgroup:process_params:param'>-doLoadBalance</char>"
"        </ilwd>"
"        <ilwd name='process_paramsgroup:process_params:type' size='13'>"
"            <char comment='-nodelist param: subset of nodes for actual calculations' dims='4' name='process_paramsgroup:process_params:type'>int4</char>"
"            <char comment='-realTimeRatio param: the desired ratio of the time required to process the data to the time contained within the data segment' dims='5' name='process_paramsgroup:process_params:type'>real4</char>"
"            <char comment='-resultAPI param: hostname and socketport for API which will receive the parallel computation data products' dims='11' name='process_paramsgroup:process_params:type'>string int4</char>"
"            <char comment='-filterparams param: list of parameters used to control the parallel filter algorithm' dims='28' name='process_paramsgroup:process_params:type'>int4 int8 real4 real8 string</char>"
"            <char comment='-mpiAPI param: hostname and socketport for mpiAPI' dims='11' name='process_paramsgroup:process_params:type'>string int4</char>"
"            <char comment='-dynlib param: full path of dso library containing the indexed filter algorithms' dims='6' name='process_paramsgroup:process_params:type'>string</char>"
"            <char comment='-communicateOutput param: specifies the model of the output structure data object used by the filter argorithm' dims='6' name='process_paramsgroup:process_params:type'>string</char>"
"            <char comment='-np param: total nodes in COMM_WORLD' dims='4' name='process_paramsgroup:process_params:type'>int4</char>"
"            <char comment='-dataAPI param: hostname and socketport for the API to provide input ILWD data' dims='11' name='process_paramsgroup:process_params:type'>string int4</char>"
"            <char comment='-jobID param: LDAS job identification associated with this particular instance of the wrapperAPI' dims='4' name='process_paramsgroup:process_params:type'>int4</char>"
"            <char comment='-dataDistributor param: defines the method for distributing input data from the master to the slaves' dims='6' name='process_paramsgroup:process_params:type'>string</char>"
"            <char comment='-uniqueID param: unique identification in the form seconds.milliseconds associated with this particular instance of the wrapperAPI' dims='5' name='process_paramsgroup:process_params:type'>real4</char>"
"            <char comment='-doLoadBalance param: to enable or disable load balancing of the parallel job' dims='6' name='process_paramsgroup:process_params:type'>string</char>"
"        </ilwd>"
"        <ilwd name='process_paramsgroup:process_params:value' size='13'>"
"            <char comment='-nodelist param: subset of nodes for actual calculations' dims='3' name='process_paramsgroup:process_params:value'>1,2</char>"
"            <char comment='-realTimeRatio param: the desired ratio of the time required to process the data to the time contained within the data segment' dims='3' name='process_paramsgroup:process_params:value'>0.9</char>"
"            <char comment='-resultAPI param: hostname and socketport for API which will receive the parallel computation data products' dims='12' name='process_paramsgroup:process_params:value'>marfik 10032</char>"
"            <char comment='-filterparams param: list of parameters used to control the parallel filter algorithm' dims='37' name='process_paramsgroup:process_params:value'>(0.5,2,1,1,0.1,100,100,0,4000,4000,0)</char>"
"            <char comment='-mpiAPI param: hostname and socketport for mpiAPI' dims='12' name='process_paramsgroup:process_params:value'>marfik 13000</char>"
"            <char comment='-dynlib param: full path of dso library containing the indexed filter algorithms' dims='43' name='process_paramsgroup:process_params:value'>/home/mbarnes/TTF-install/lib/TTF/libTTF.so</char>"
"            <char comment='-communicateOutput param: specifies the model of the output structure data object used by the filter argorithm' dims='6' name='procEss_paramsgroup:process_params:value'>ALWAYS</char>"
"            <char comment='-np param: total nodes in COMM_WORLD' name='process_paramsgroup:process_params:value'>3</char>"
"            <char comment='-dataAPI param: hostname and socketport for the API to provide input ILWD data' dims='13' name='process_paramsgroup:process_params:value'>datahost 5678</char>"
"            <char comment='-jobID param: LDAS job identification associated with this particular instance of the wrapperAPI' dims='2' name='process_paramsgroup:process_params:value'>88</char>"
"            <char comment='-dataDistributor param: defines the method for distributing input data from the master to the slaves' dims='7' name='process_paramsgroup:process_params:value'>WRAPPER</char>"
"            <char comment='-uniqueID param: unique identification in the form seconds.milliseconds associated with this particular instance of the wrapperAPI' dims='2' name='process_paramsgroup:process_params:value'>88</char>"
"            <char comment='-doLoadBalance param: to enable or disable load balancing of the parallel job' dims='5' name='process_paramsgroup:process_params:value'>FALSE</char>"
"        </ilwd>"
"    </ilwd>"
"</ilwd>";

  std::istringstream	is( ilwd_str );
  ILwd::Reader		reader(is);
  ILwd::LdasContainer	c;
  std::unique_ptr< ILwd::LdasContainer >	c_ptr;
  
  try {
    c.read(reader);
    DB::Table*		process(DB::Table::CreateTable("process"));
    DB::Table*		process_param(DB::Table::CreateTable("process_params"));
    DB::Transaction	request;

    request.AppendTable(process, true);
    request.AppendTable(process_param, true);
   
    process->AppendILwd(dynamic_cast<const ILwd::LdasContainer&>(*(c[0])));
    process_param->AppendILwd(dynamic_cast<const ILwd::LdasContainer&>(*(c[1])));

    Test.Message() << "Process";
    c_ptr.reset( process->GetILwd( ) );
    c_ptr->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
    
    Test.Message() << "Request";
    c_ptr.reset( request.GetILwd( ) );
    c_ptr->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }
  catch(const LdasException& e)
  {
    Test.Check(false) << "char_test: Caught error on reading"
		      << "(" << e.getError( 0 ).getMessage()
		      << " [" << e.getError( 0 ).getInfo() << "] @ "
		      << e.getError( 0 ).getFile() << " "
		      << e.getError( 0 ).getLine()
		      << ")"
		      << endl << flush;
    return;
  }
  catch (const std::exception& e )
  {
    Test.Check(false) << "Caught exception: " << e.what() << endl;
  }
  catch ( ... )
  {
    Test.Check(false) << "Caught unknown exception" << endl;
  }
}

void
modify(void)
{
  try {
    int		int_buf;

    std::unique_ptr< DB::Table >
      p(DB::Table::CreateTable(DB::Process::TABLE_NAME));

    p->AppendColumnEntry(DB::Process::PROGRAM, "sample process");
    p->AppendColumnEntry(DB::Process::PROGRAM_VERSION, "version");
    p->AppendColumnEntry(DB::Process::REPOSITORY, "repository");
    p->AppendColumnEntry(DB::Process::REPOSITORY_ENTRY_TIME, 0);
    p->AppendColumnEntry(DB::Process::START_TIME, 0);
    p->AppendColumnEntry(DB::Process::OS_PROCESS_ID, 1001);
    p->AppendColumnEntry(DB::Process::NODE, "localhost");
    p->AppendColumnEntry(DB::Process::USERNAME, "me");

    p->AppendColumnEntry(DB::Process::PROGRAM, "sample process");
    p->AppendColumnEntry(DB::Process::PROGRAM_VERSION, "version");
    p->AppendColumnEntry(DB::Process::REPOSITORY, "repository");
    p->AppendColumnEntry(DB::Process::REPOSITORY_ENTRY_TIME, 0);
    p->AppendColumnEntry(DB::Process::START_TIME, 0);
    p->AppendColumnEntry(DB::Process::OS_PROCESS_ID, 1002);
    p->AppendColumnEntry(DB::Process::NODE, "localhost");
    p->AppendColumnEntry(DB::Process::USERNAME, "me");

    p->ModifyEntry( DB::Process::PROGRAM_VERSION, 1, "v14" );
    int_buf = 1102;
    p->ModifyEntry( DB::Process::OS_PROCESS_ID, 1, &int_buf );


    Test.Message() << "modify";
    std::unique_ptr< ILwd::LdasContainer > ilwd( p->GetILwd( ) );
    ilwd->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }
  catch (const std::exception& e)
  {
    Test.Check(false) << "Caught excpetion: " << e.what() << endl;
  }
  catch (...)
  {
    Test.Check(false) << "Caught unknown excpetion" << endl;
  }
}

int
main(int Argc, char** Argv)
{
  Test.Init(Argc, Argv);
     //DB::Table::InitializeTableSet("");

  simple();
  ilwd();
  modify();

  Test.Exit();
}
