#ifdef HAVE_CONFIG_H
#include "dbaccess/config.h"
#endif	/* HAVE_CONFIG_H */

#include <sstream>

#include "general/unittest.h"
#include "general/types.hh"
#include "ilwd/ldascontainer.hh"

#include "dbaccess/Table.hh"

General::UnitTest	Test;

using namespace DB;
using namespace std;   

//-----------------------------------------------------------------------
// Declaration of test fuctions
//-----------------------------------------------------------------------

static void
test( const std::string& Source )
{
  std::string		subroutine( "test" );
  std::istringstream	is( Source );
  ILwd::Reader		reader(is);
  ILwd::LdasContainer	container;
  
  try {
    container.read(reader);
  }
  catch(const LdasException& e)
  {
    for (size_t x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << "test: read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << "test: Caught error on reading" << endl << flush;
    return;
  }

  try
  {
    Table*	table = Table::MakeTableFromILwd( container, true );
    delete table;
    Test.Check( true ) << subroutine
		       << ": " << container.getMetadata<std::string>("test_name")
		       << std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << subroutine
			<< ": " << container.getMetadata<std::string>("test_name")
			<< ": Caught exception: ";
    try
    {
      throw;
    }
    catch( const std::exception& e )
    {
      Test.Message( false ) << e.what();
    }
    catch( ... )
    {
      Test.Message( false ) << "unknown exception";
    }
    Test.Message( false ) << std::endl;
  }
}

//-----------------------------------------------------------------------
// Test the creation of generic table from zero length ILwd.
//-----------------------------------------------------------------------


static std::string zero_length(
"    <ilwd comment='SQL={"
"             SELECT *"
"             FROM SUMM_SPECTRUM"
"             WHERE"
"                channel=&apos;resample(P2:LSC-AS_Q, 1, 8)&apos;"
"                AND spectrum_type=&apos;Welch&apos;"
"                AND start_time=609999999"
"                AND start_time_ns=995117187"
"                AND start_frequency=0.0000000000000000e+00"
"                AND delta_frequency=7.8125000000000000e-03"
"                AND spectrum_length=131073"
"             ORDER BY start_time, spectrum_type"
"             FETCH FIRST 1 ROWS ONLY"
"             }' name='ldasgroup:result_table:table' metadata='test_name=zero_length' size='18'>"
"        <int_4s dims='0' name='CREATOR_DB'></int_4s>"
"        <lstring dims='0' name='PROGRAM'></lstring>"
"        <char_u dims='0' name='PROCESS_ID'></char_u>"
"        <lstring dims='0' name='FRAMESET_GROUP'></lstring>"
"        <lstring dims='0' name='SEGMENT_GROUP'></lstring>"
"        <int_4s dims='0' name='VERSION'></int_4s>"
"        <int_4s dims='0' name='START_TIME'></int_4s>"
"        <int_4s dims='0' name='START_TIME_NS'></int_4s>"
"        <int_4s dims='0' name='END_TIME'></int_4s>"
"        <int_4s dims='0' name='END_TIME_NS'></int_4s>"
"        <int_4s dims='0' name='FRAMES_USED'></int_4s>"
"        <real_8 dims='0' name='START_FREQUENCY'></real_8>"
"        <real_8 dims='0' name='DELTA_FREQUENCY'></real_8>"
"        <lstring dims='0' name='MIMETYPE'></lstring>"
"        <lstring dims='0' name='CHANNEL'></lstring>"
"        <lstring dims='0' name='SPECTRUM_TYPE'></lstring>"
"        <char_u dims='0' name='SPECTRUM'></char_u>"
"        <int_4s dims='0' name='SPECTRUM_LENGTH'></int_4s>"
"    </ilwd>"
);

//-----------------------------------------------------------------------
// Main test program
//-----------------------------------------------------------------------

int
main(int Argc, char** Argv)
{
  Test.Init(Argc, Argv);

  test( zero_length );

  // Test.Exit();
}
