#ifndef DB_FIELD_HH
#define	DB_FIELD_HH

#include <string>

#include "general/bit_vector.hh"

//!ignore_begin:
namespace ILwd {
  class LdasElement;
}
//!ignore_end:

namespace DB {
  class Table;

  //: Column infomation
  //
  // When working with database tables, data is thought of in terms of rows
  // and columns. The Field class represents a column of data.
  class Field
  {
  public:
    //: Type of fields that are supported
    typedef enum {
      FIELD_ARRAY_CHAR,
      FIELD_ARRAY_CHAR_U,
      FIELD_ARRAY_INT_2S,
      FIELD_ARRAY_INT_2U,
      FIELD_ARRAY_INT_4S,
      FIELD_ARRAY_INT_4U,
      FIELD_ARRAY_INT_8S,
      FIELD_ARRAY_INT_8U,
      FIELD_ARRAY_REAL_4,
      FIELD_ARRAY_REAL_8,
      FIELD_ARRAY_COMPLEX_8,
      FIELD_ARRAY_COMPLEX_16,
      FIELD_BLOB,
      FIELD_STRING,
      FIELD_UNIQUE_ID,
      FIELD_NULL,
      /// DB2_ALIASES
      DB2_CHAR = FIELD_STRING,
      DB2_VARCHAR = FIELD_STRING,
      DB2_INTEGER = FIELD_ARRAY_INT_4S,
      DB2_BIGINT = FIELD_ARRAY_INT_8S,
      DB2_FLOAT = FIELD_ARRAY_REAL_4,
      DB2_DOUBLE = FIELD_ARRAY_REAL_8,
      DB2_BLOB = FIELD_BLOB,
      DB2_CLOB = FIELD_BLOB,
      DB2_DBCLOB = FIELD_BLOB
    } field_type;

    //: Type for sizes
    typedef unsigned int size_t;

    //: Constructor
    //
    //!param: std::string Name - Name of column to create
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: DB::Field::field_type Type - The type specifying the column.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    Field(const std::string& Name, bool Required, DB::Field::field_type Type,
	  const DB::Table& Table);
    //: Destructor
    //
    // The destructor is virtual since there are virtual functions.
    virtual ~Field(void);

    //: Create a field based on ILwd info
    static Field* Create( const ILwd::LdasElement& Element,
			  const DB::Table& Table,
			  bool Required = true );

    //: Name of column
    //
    //!return: const std::string& - Name of field
    //
    const std::string& GetName(void) const;

    //: Get ILwd representation
    //
    // This <font color='red'>pure virtual</font> method generates an
    // ilwd representing the data of the field.
    //
    //!return: ILwd::LdasElement* - ilwd representation
    //
    virtual ILwd::LdasElement* GetILwd(bool& NeedToCopy) const = 0;

    //: Type of the field
    //
    //!return: field_type - type of field
    field_type GetType(void) const;

    //: Generate full name of field
    //
    // 
    //!return: std::string - full name of field.
    std::string GenerateFieldName( void ) const;

    //: Determine if an entry is to be considered null
    bool IsNull(size_t Offset) const;

    //: Is the column required
    //!return: bool - true if the column must be specified at the time of
    //+        submission.
    bool IsRequired(void) const;

    //: Virtual copy constructor
    //
    // This method creates a duplicate of the class characteristics
    // without copying the data that may be stored.
    //
    //!return: New field with the same characteristics as this one.
    Field* NewField(void) const;

    //: Virtual constructor
    //
    //!param: std::string Name - Name for the column
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    virtual Field* NewField(const std::string& Name, bool Required,
			    const DB::Table& Table) const = 0;

    //: Mark an entry as being null
    void SetNull(size_t Offset, bool Value);

    //: Transfer null mask
    void SetNullMask(const General::bit_vector& BitMask);

    //: Number of elements in data set.
    //
    // This <font color='red'>pure virtual</font> method returns the number
    // of elements (rows) that are contained in the data set.
    //
    //!return: size_t - Number of elements in the data set.
    virtual size_t Size(void) const = 0;

    static const Field& NullRef;

  private:
    //: Name of column
    //
    // The name of the column.
    std::string		m_name;
    //: Vector to store bits of nulls
    General::bit_vector	m_null_mask;
    //: Required specifier
    //
    // If this value is true, then the column must have values for the
    // the creation of the ILwd to succeed.
    //
    bool		m_required;
    //: Containing table
    //
    // All fields (columns) must belong to a table. This variable stores
    // the reference to the table.
    const Table&	m_table;
    //: Type of column
    //
    // The variable stores the type of the field (column) to reduce the need
    // for dynamic_casts.
    field_type		m_type;

  }; // class - Field

  // Returns the name of the field.
  inline const std::string& Field::
  GetName(void) const
  {
    return m_name;
  }

  // Returns the type of the field.
  inline Field::field_type Field::
  GetType(void) const
  {
    return m_type;
  }

  // A  true value is returned if the column must be specified at the time of
  // submission, false otherwise.
  inline bool Field::
  IsRequired(void) const
  {
    return m_required;
  }

} // namespace - DB
#endif	/* DB_FIELD_HH */
