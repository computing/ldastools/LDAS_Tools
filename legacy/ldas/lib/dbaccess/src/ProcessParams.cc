#include "dbaccess/ProcessParams.hh"

namespace DB
{
  namespace ProcessParams
  {
    const char TABLE_NAME[] = "process_params";

    const char CREATOR_DB[] = "creator_db";

    const char PROGRAM[] = "program";
    const char PROCESS_ID[] = "process_id";

    const char PARAM[] = "param";
    const char TYPE[] = "type";
    const char VALUE[] = "value";
  }
}
