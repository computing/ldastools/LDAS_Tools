#ifndef DB_PROCESS_HH
#define	DB_PROCESS_HH

namespace DB
{
  namespace Process
  {
    extern const char TABLE_NAME[];

    extern const char CREATOR_DB[];

    extern const char PROGRAM[];
    extern const char PROGRAM_VERSION[];
    extern const char REPOSITORY[];
    extern const char REPOSITORY_ENTRY_TIME[];
    extern const char COMMENT[];

    extern const char IS_ONLINE[];
    extern const char NODE[];
    extern const char USERNAME[];
    extern const char OS_PROCESS_ID[];
    extern const char JOB_ID[];
    extern const char DOMAIN_NAME[];
    extern const char START_TIME[];
    extern const char END_TIME[];

    extern const char PROCESS_ID[];
    extern const char PARAM_SET[];
    extern const char IFOS[];
  }
}
#endif	/* DB_PROCESS_HH */
