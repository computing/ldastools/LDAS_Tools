#ifndef DB_FIELD_ARRAY_HH
#define	DB_FIELD_ARRAY_HH

#include "CachePointer.hh"

#include "dbaccess/Field.hh"

namespace DB {
  class Table;

  //: Array Field
  //
  // Most of the data in the database can be represented by this templated
  // class as most data in the database has some structure. The structure
  // here is one of the many well known data types.
  template < class Type >
  class FieldArray: public Field
  {
  public:
    //: Constructor
    //
    //!param: std::string Name - Name of column to create
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldArray(std::string Name, bool Required, const DB::Table& Table);

    //: Append a number of rows at once
    //
    //!param: const Type* Data - Pointer to data to be appended
    //!param: int Rows - Number of elements in Data.
    //
    //!return: none
    void Append(const Type* Data, int Rows);
    
    //: Get ILwd representation
    //
    // This method generates an
    // ilwd representing the data of the field.
    //
    //!return: ILwd::LdasElement* - ilwd representation
    ILwd::LdasElement* GetILwd(bool& NeedToCopy) const;

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const Type* Data - Pointer to new value for field.
    //
    //!return: none
    void ModifyEntry( const int Row, const Type* Data );

    //: Virtual constructor
    //
    //!param: std::string Name - Name for the column
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldArray< Type >* NewField( const std::string& Name,
				  bool Required,
				  const DB::Table& Table ) const;
    //: Number of elements in data set.
    //
    // This method returns the number
    // of elements (rows) that are contained in the data set.
    //
    //!return: Field::size_t - Number of elements in the data set.
    Field::size_t Size(void) const;

    //: Append data
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: const CHAR_U* Data - Data to be appended.
    void push_back(Type Data);

    //: Return the nth element
    Type operator[]( Field::size_t Offset ) const;

  private:
   
    //: No copy constructor
    FieldArray( const FieldArray& );
   
    //: No assignment operator
    FieldArray& operator=( const FieldArray& );
   
    //: Data storage.
    General::CachePointer< Type >	m_data;
  }; // class - FieldArray


  template< class Type >
  inline Type FieldArray< Type >::
  operator[](Field::size_t Offset) const
  {
    return m_data[Offset];
  }
} // namespace - DB
#endif	/* DB_FIELD_ARRAY_HH */


