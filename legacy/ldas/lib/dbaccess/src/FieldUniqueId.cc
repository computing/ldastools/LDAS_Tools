#include "dbaccess/config.h"

#include <cstdio>
#include <ios>
#include <typeinfo>

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"

#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Table.hh"

using namespace std;
   
   
DB::FieldUniqueId::
FieldUniqueId(std::string Name, bool Required, const DB::Table& Table)
  : Field(Name, Required, DB::Field::FIELD_UNIQUE_ID, Table)
{
}

ILwd::LdasContainer* DB::FieldUniqueId::
GetILwd(bool& NeedToCopy) const
{
  NeedToCopy = true;
  return const_cast<ILwd::LdasContainer*>(&m_data);
}

std::string DB::FieldUniqueId::
GetDataAsString(unsigned int Offset)
{
  return std::string(operator[](Offset)->getData(),
		     operator[](Offset)->getDimension(0));
}

std::string DB::FieldUniqueId::
MakeUniqueId(const DB::Table& Table, unsigned int Id) const
{
  std::ostringstream	id;

  id << Table.GetName( )
     << ":" << GetName( )
     << ":" << Id
    ;

  return id.str( );
}

void DB::FieldUniqueId::
ModifyEntry( const int Row, const std::string& Data )
{
  if ( ( Row >= (int)m_data.size() ) || ( Row < 0 ) )
  {
    throw std::range_error("Row to modify is outside array bounds");
  }
  delete m_data[Row];
  m_data[Row] = new ILwd::LdasArray<CHAR>( Data.c_str(), Data.size() );
}

void DB::FieldUniqueId::
ModifyEntry( const int Row, const CHAR* Data )
{
  if ( ( Row >= (int)m_data.size() ) || ( Row < 0 ) )
  {
    throw std::range_error("Row to modify is outside array bounds");
  }
  delete m_data[Row];
  m_data[Row] = new ILwd::LdasArray<CHAR>( Data, strlen(Data) );
}

DB::FieldUniqueId* DB::FieldUniqueId::
NewField( const std::string& Name, bool Required, const DB::Table& Table ) const
{
  return new DB::FieldUniqueId(Name, Required, Table);
}
   
void DB::FieldUniqueId::
push_back( const std::string& Id )
{
  ILwd::LdasArray<CHAR>*
    id( new ILwd::LdasArray<CHAR>( Id.c_str(), Id.length() ) );

  m_data.push_back( id,
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
}

void DB::FieldUniqueId::
push_back(const CHAR* Data, int Length)
{
  m_data.push_back( new ILwd::LdasArray<CHAR>(Data, Length),
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
}

void DB::FieldUniqueId::
push_back(const CHAR_U* Data, int Length)
{
  m_data.push_back( new ILwd::LdasArray<CHAR_U>(Data, Length),
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
}

void DB::FieldUniqueId::
Replace(unsigned int Row, const CHAR* Data, unsigned int Length)
{
  //cerr << "Row offset: " << Row;
  ILwd::LdasContainer::iterator pos( m_data.begin() );
  if( Length >= m_data.size() )
  {
     pos = m_data.end();
  }
  else
  {
     std::advance( pos, Length );
  }
   

  if (pos != m_data.end())
  {
     // Use iterator to the inserted element to erase old entry
     ILwd::LdasContainer::iterator iter( 
        m_data.insert( pos, new ILwd::LdasArray<CHAR>(Data, Length),
                       ILwd::LdasContainer::NO_ALLOCATE,
                       ILwd::LdasContainer::OWN ) );
   
     // Don't need to check for != end(): it's guaranteed
     ++iter;
   
     m_data.erase(iter);
  }
   
  //cerr << " elements: " << m_data.size() << endl;
  return;
}

const ILwd::LdasArray<CHAR>* DB::FieldUniqueId::
operator[](unsigned int Offset) const
{
  return reinterpret_cast<ILwd::LdasArray<CHAR>*>(m_data[Offset]);
}


DB::Field::size_t DB::FieldUniqueId::Size(void) const
{
   return m_data.size();
}   
