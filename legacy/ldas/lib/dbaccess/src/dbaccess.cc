#ifdef HAVE_CONFIG_H
#include "config.h"
#endif	/* HAVE_CONFIG_H */

#ifndef _POSIX_PTHREAD_SEMANTICS
#define	_POSIX_PTHREAD_SEMANTICS
#endif /* _POSIX_PTHREAD_SEMANTICS */

#if ! HAVE_FUNC_GETDOMAINNAME
#include<sys/systeminfo.h>
#define getdomainname(a,b) sysinfo(SI_SRPC_DOMAIN, (a), (b))
#endif

#include <pwd.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include <stdexcept>
#include <typeinfo>
#include <string>

#include "dbaccess.h"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Table.hh"

const DB::Field& DB::
GetFieldType(DB::FieldType_type Type)
{
  static DB::Table		table("__table__");
  static DB::FieldArray<INT_2S>	int_2s("__int_2s__", false, table);
  static DB::FieldArray<INT_4S>	int_4s("__int_4s__", false, table);
  static DB::FieldArray<INT_8S>	int_8s("__int_8s__", false, table);
  static DB::FieldArray<REAL_4>	real_4("__real_4__", false, table);
  static DB::FieldArray<REAL_8>	real_8("__real_8__", false, table);
  static DB::FieldBlob		blob("__blob__", false, table);
  static DB::FieldString	str("__string__", false, table);
  static DB::FieldUniqueId	unique_id("__unique_id__", false, table);

  switch(Type)
  {
  case DB::Array_INT_2S:
    return int_2s;
  case DB::Array_INT_4S:
    return int_4s;
  case DB::Array_INT_8S:
    return int_8s;
  case DB::Array_REAL_4:
    return real_4;
  case DB::Array_REAL_8:
    return real_8;
  case DB::Blob:
    return blob;
  case DB::String:
    return str;
  case DB::UniqueId:
    return unique_id;
  case DB::UNSET:
  default:
    break;
  }
  throw std::bad_cast();
}

bool DB::IsOnline(void)
{
  static bool initialized(false);
  static bool is_online(false);
  if (!initialized)
  {
    std::string domain( DB::GetOSDomain() );

    if ( domain.length() > 0 )
    {
      if ( ( domain == "ligo-wa" ) || ( domain == "ligo-la" ) )
      {
	is_online = true;
      }
      initialized = true;
    }
  }
  return is_online;
}

std::string DB::GetNodeName(void)
{
  static bool initialized(false);
  static std::string node_name;

  if (!initialized)
  {
    char buffer[256];
    if ( gethostname( buffer, sizeof(buffer) ) == 0 )
    {
      node_name = buffer;
    }
    initialized = true;
  }
  return node_name;
}

std::string DB::GetOSDomain(void)
{
    static char	buffer[256] = "";
    static const char* LDAS_DOMAIN_FILE = "/etc/ldasname";

    if ( *buffer == '\0' )
    {
      if ( access( LDAS_DOMAIN_FILE, R_OK ) == 0 )
      {
	int bytes;
	int fd( open( LDAS_DOMAIN_FILE, O_RDONLY ) );

	bytes = read(fd, buffer, sizeof(buffer) );
	close(fd);

	buffer[bytes] = '\0';
	while ( bytes && ( ( buffer[--bytes] == '\n') ||
			   ( buffer[bytes] == '\r' ) ) )
	{
	  buffer[bytes] = '\0';
	}
      }
      if ( *buffer == '\0' )
      {
	getdomainname( buffer, sizeof(buffer) );
      }
    }
    return buffer;
}

int DB::GetOSProcessId(void)
{
  static bool initialized(false);
  static int unix_process_id(0);

  if (!initialized)
  {
    unix_process_id = getpid();
    initialized = true;
  }
  return unix_process_id;
}

std::string DB::GetOSUser(void)
{
  static bool initialized(false);
  static std::string user_name;

  if (!initialized)
  {
    uid_t user_id( getuid() );
    struct passwd user_struct;
    struct passwd* result;
    char	buffer[4096];

    if ( getpwuid_r( user_id, &user_struct, buffer, sizeof( buffer ),
		     &result  ) == 0 )
    {
      user_name = result->pw_name;
      initialized = true;
    }
  }
  return user_name;
}
