#ifndef DB_SUMM_STATISTICS_HH
#define	DB_SUMM_STATISTICS_HH

namespace DB
{
  namespace SummStatistics
  {
    extern const char TABLE_NAME[];

    extern const char CREATOR_DB[];

    extern const char PROGRAM[];
    extern const char PROCESS_ID[];

    extern const char FRAMESET_GROUP[];
    extern const char SEGMENT_GROUP[];
    extern const char SEGMENT_VERSION[];

    extern const char START_TIME_SECONDS[];
    extern const char START_TIME_NANOSECONDS[];
    extern const char END_TIME_SECONDS[];
    extern const char END_TIME_NANOSECONDS[];

    extern const char FRAMES_USED[];

    extern const char SAMPLES[];

    extern const char CHANNEL[];

    extern const char MIN[];
    extern const char MAX[];
    extern const char MIN_DELTA[];
    extern const char MAX_DELTA[];
    extern const char MIN_DELTA_DELTA[];
    extern const char MAX_DELTA_DELTA[];
    extern const char MEAN[];
    extern const char VARIANCE[];
    extern const char RMS[];
    extern const char SKEWNESS[];
    extern const char KURTOSIS[];
  }
}

#endif	/* DB_SUMM_STATISTICS_HH */
