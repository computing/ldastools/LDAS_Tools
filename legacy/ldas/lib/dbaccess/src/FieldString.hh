#ifndef DB_FIELD_STRING_HH
#define	DB_FIELD_STRING_HH

#include "ilwd/ldasstring.hh"
#include "dbaccess/Field.hh"

namespace DB {
  class Table;

  //: String Field
  //
  // This class contains the definition and methods appropriate for
  // working with string data.
  class FieldString: public Field
  {
  public:
    //: Constructor
    //
    //!param: std::string Name - Name of column to create
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldString(const std::string& Name, bool Required, const DB::Table& Table);
    //: Get ILwd representation
    //
    // This method generates an
    // ilwd representing the data of the field.
    //
    //!return: ILwd::LdasElement* - ilwd representation
    ILwd::LdasString* GetILwd(bool& NeedToCopy) const;

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const std::string& Data - Reference to new value for field.
    //
    //!return: none
    void ModifyEntry( const int Row, const std::string& Data );

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const CHAR* Data - Pointer to new value for field.
    //
    //!return: none
    void ModifyEntry( const int Row, const CHAR* Data );

    //: Virtual constructor
    //
    //!param: std::string Name - Name for the column
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldString* NewField( const std::string& Name,
                           bool Required,
			   const DB::Table& Table ) const;
    //: Number of elements in data set.
    //
    // This method returns the number
    // of elements (rows) that are contained in the data set.
    //
    //!return: Field::size_t - Number of elements in the data set.
    Field::size_t Size(void) const;

    //: Append data
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: std::string Data - Data to be appended.
    void push_back(std::string Data);

    //: Append data
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: const CHAR* Data - Data to be appended.
    //!param: unsigned int Length - number of bytes composing Data.
    void push_back(const CHAR* Data, int Length);

    //: Get Nth element
    //
    // Returns the data specified by Offset.
    //
    //!param: unsigned int Offset - Offset of requested data item.
    //!return: std::string - copy of data at offset
    std::string operator[](unsigned int Offset) const;

  private:
    //: Data storage
    ILwd::LdasString	m_data;
  }; // class - FieldString

} // namespace - DB
#endif	/* DB_FIELD_STRING_HH */

