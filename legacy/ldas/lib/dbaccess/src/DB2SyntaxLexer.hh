#ifndef DB__DB2_SYNTAX_LEXER_HH
#define DB__DB2_SYNTAX_LEXER_HH

#include "DB2SyntaxDriver.hh"
#include "DB2SyntaxParser.hh"

#ifndef FLEX_SCANNER
#include <FlexLexer.h>
#endif /* FLEX_SCANNER */

namespace DB
{
  //---------------------------------------------------------------------
  //: DB2 scanner
  //
  // This class is for scanning a subset of DB2 structured SQL.
  // Currently it will only handle CREATE TABLE.
  //---------------------------------------------------------------------
  class DB2SyntaxLexer
    : public yyFlexLexer
  {
  public:
    typedef DB::DB2SyntaxParser::token_type			token_type;
    typedef General::unordered_map<std::string, token_type>	keywords_type;
    typedef DB2SyntaxParser::semantic_type			semantic_type;
    typedef DB2SyntaxParser::location_type			location_type;

    // This function is defined in db2_lex.ll
    virtual int yylex();
    token_type KeywordLookup( const char* Word ) const;
    void SetYYLloc( location_type* YYLloc );
    void SetYYLval( semantic_type* YYLval );

  private:
    // Map of keywords to token values.
    static keywords_type&	m_keywords;
    semantic_type*		m_yylval;
    location_type*		m_yylloc;

  };

  inline void DB2SyntaxLexer::
  SetYYLloc( location_type* YYLloc )
  {
    m_yylloc = YYLloc;
  }

  inline void DB2SyntaxLexer::
  SetYYLval( semantic_type* YYLval )
  {
    m_yylval = YYLval;
  }
}
#endif /* DB__DB2_SYNTAX_LEXER_HH */
