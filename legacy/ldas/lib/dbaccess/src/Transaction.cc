#include "dbaccess/config.h"

#include "ilwd/ldascontainer.hh"

#include "dbaccess/Table.hh"
#include "dbaccess/Transaction.hh"

DB::Transaction::
~Transaction(void)
{
  for (table_list_type::iterator table = m_tables.begin();
       table != m_tables.end();
       table++)
  {
    if (table->s_owns)
    {
      delete table->s_table;
    }
  }
  m_tables.resize(0);
}

ILwd::LdasContainer* DB::Transaction::
GetILwd( ILwd::LdasContainer* Container ) const
{
  if ( ( (ILwd::LdasContainer*)NULL == Container) )
  {
    Container = new ILwd::LdasContainer();
  }
  for (table_list_type::const_iterator table = m_tables.begin();
       table != m_tables.end();
       table++)
  {
    if ((table->s_table)->GetRowCount() > 0)
    {
      //-----------------------------------------------------------------
      // The table has entries. Add it to the ilwd.
      //-----------------------------------------------------------------
      ILwd::LdasElement*	ilwd = (table->s_table)->GetILwd();

      Container->push_back( ilwd,
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
    }
  }
  return Container;
}

DB::Table* DB::Transaction::
GetTable(const std::string& TableName)
{
  for (table_list_type::const_iterator table = m_tables.begin();
       table != m_tables.end();
       table++)
  {
    if ((table->s_table)->GetName() == TableName)
    {
      return table->s_table;
    }
  }
  return (DB::Table*)NULL;
}
