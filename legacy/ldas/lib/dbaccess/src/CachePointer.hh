/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef CACHE_POINTER_HH
#define	CACHE_POINTER_HH

#if 0
#ifdef __GNUG__
#pragma interface
#endif	/* __GNUG__ */
#endif /* 0 */

#include <stdexcept>
#include <vector>   

namespace General {
  template< class Type >
  class CachePointer
  {
  public:
    typedef unsigned int size_t;
    
    CachePointer();
    ~CachePointer();

    const Type* Get() const;
    size_t Length() const;

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const Type* Data - Pointer to new value for field.
    //
    //!return: none
    void ModifyEntry( const int Row, const Type* Data );
    Type operator[]( size_t Offset ) const;

    void push_back(const Type& Data);

  private:
   
    //: No copy constructor
    CachePointer( const CachePointer& );
   
    //: No assignment operator
    CachePointer& operator=( const CachePointer& );
   
    std::vector< Type >	m_ptr;
  };


  template< class Type >
  inline typename CachePointer< Type >::size_t CachePointer< Type >::
  Length() const
  {
    return m_ptr.size();
  }

} // namespace - General

#endif /* CACHE_POINTER_HH */
