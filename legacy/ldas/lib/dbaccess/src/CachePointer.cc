#if 0
#ifdef __GNUG__
#pragma implementation "CachePointer.hh"
#endif	/* __GNUG__ */
#endif /* 0 */

#include "dbaccess/config.h"

#include <stdexcept>
#include <string>

#include "general/types.hh"
#include "CachePointer.hh"
   
using std::vector;   

template<class Type>
General::CachePointer< Type >::
CachePointer()
{}

template<class Type>
General::CachePointer< Type >::
~CachePointer()
{}

template< class Type >
void General::CachePointer< Type >::
push_back(const Type& Data)
{
   m_ptr.push_back( Data );
   return;
}

template< class Type >
void General::CachePointer< Type >::
ModifyEntry( const int Row, const Type* Data )
{
  int size( m_ptr.size() );
   
  if ( ( Row >= size ) || ( Row < 0 ) )
  {
    throw std::range_error( "Row to modify is outside array bounds" );
  }
   
  m_ptr[Row] = *Data;
  return;
}

template< class Type >
Type General::CachePointer< Type >::
operator[](size_t Offset) const
{
  if ( Offset >= m_ptr.size() )
  {
    throw std::range_error("Offset beyond end of array");
  }
   
  return m_ptr[Offset];
}

template< class Type >
const Type* General::CachePointer< Type >::Get() const
{
  if( m_ptr.size() == 0 )
  {
    return 0;
  }
   
  return &( m_ptr[0] );
}
   
   
template class General::CachePointer<CHAR>;
template class General::CachePointer<CHAR_U>;
template class General::CachePointer<INT_2S>;
template class General::CachePointer<INT_2U>;
template class General::CachePointer<INT_4S>;
template class General::CachePointer<INT_4U>;
template class General::CachePointer<INT_8S>;
template class General::CachePointer<INT_8U>;
template class General::CachePointer<REAL_4>;
template class General::CachePointer<REAL_8>;
template class General::CachePointer<COMPLEX_8>;
template class General::CachePointer<COMPLEX_16>;
