#include "dbaccess/SummCSD.hh"

namespace DB
{
  namespace SummCSD
  {
    const char TABLE_NAME[] = "summ_csd";

    const char CREATOR_DB[] = "creator_db";

    const char PROGRAM[] = "program";
    const char PROCESS_ID[] = "process_id";

    const char FRAMESET_GROUP[] = "frameset_group";
    const char SEGMENT_GROUP[] = "segment_group";
    const char SEGMENT_VERSION[] = "version";

    const char START_TIME_SECONDS[] = "start_time";
    const char START_TIME_NANOSECONDS[] = "start_time_ns";
    const char END_TIME_SECONDS[] = "end_time";
    const char END_TIME_NANOSECONDS[] = "end_time_ns";

    const char FRAMES_USED[] = "frames_used";

    const char START_FREQUENCY[] = "start_frequency";
    const char DELTA_FREQUENCY[] = "delta_frequency";
    const char MIMETYPE[] = "mimetype";

    const char CHANNEL_1[] = "channel1";
    const char CHANNEL_2[] = "channel2";

    const char TYPE[] = "spectrum_type";
    const char SPECTRUM[] = "spectrum";
    const char LENGTH[] = "spectrum_length";
  }
}
