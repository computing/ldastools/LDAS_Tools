/* -*- mode: C++; -*- */
#ifndef DBACCESS_H
#define	DBACCESS_H

#include <string>

#define	ADD_FIELD(name, type, required) \
    m_field_info[name] = \
      new DB::Table::FieldDesc(cnt++, name, DB::GetFieldType(type), required)

#define APPEND_FIELD(ColumnName,Value,Type) \
GetField##Type(m_field_info[ColumnName]->GetIndex()).push_back(Value)

namespace DB
{
  class Field;

  typedef enum {
    Array_INT_2S,
    Array_INT_4S,
    Array_INT_8S,
    Array_REAL_4,
    Array_REAL_8,
    Blob,
    String,
    UniqueId,
    UNSET,
    BIGINT = Array_INT_8S,
    CHARACTER = String,
    DOUBLE = Array_REAL_8,
    INTEGER = Array_INT_4S,
    REAL = Array_REAL_4,
    SHORT = Array_INT_2S,
    VARCHAR = String
  } FieldType_type;

  bool IsOnline(void);
  std::string GetNodeName(void);
  std::string GetOSDomain( void );
  int GetOSProcessId(void);
  std::string GetOSUser(void);
  const DB::Field& GetFieldType(FieldType_type Type);
}

#endif	/* DBACCESS_H */
