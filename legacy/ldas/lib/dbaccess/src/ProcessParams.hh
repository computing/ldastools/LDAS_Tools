#ifndef DB_PROCESS_PARAMS_HH
#define	DB_PROCESS_PARAMS_HH

namespace DB
{
  namespace ProcessParams
  {
    extern const char TABLE_NAME[];

    extern const char CREATOR_DB[];

    extern const char PROGRAM[];
    extern const char PROCESS_ID[];

    extern const char PARAM[];
    extern const char TYPE[];
    extern const char VALUE[];
  }
}

#endif	/* DB_PROCESS_PARAMS_HH */
