/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef DB_TRANSACTION_HH
#define	DB_TRANSACTION_HH

namespace DB {
  /// forward declaration of classes
  class Table;

  //: Generic request related to process tables.
  //
  // This class allows for the creation of generic request.
  class Transaction
  {
  public:
    typedef struct {
      DB::Table*	s_table;
      bool		s_owns;
    } table_info_type;
    typedef std::vector<table_info_type> table_list_type;

    /// Use default constructors

    // Destructor
    ~Transaction(void);

    //: Add a table to the requests
    //
    //!param: DB::Table& Table - Table to be added to list of tables
    //!param: bool Owns - true if the transaction is repoonsible for
    //+       releasing the resources associate with Table, false otherwise
    void AppendTable(DB::Table* Table, bool Owns);

    //: Generate ILwd for the request.
    //
    // For any classes that derive from this one, this call should be done
    // first and all data should be appended to the container returned
    // from this call.
    //
    //!param: ILwd::LdasContainer* Container - The outter most container.
    //
    //!return: The container
    ILwd::LdasContainer* GetILwd( ILwd::LdasContainer* Container =
				  (ILwd::LdasContainer*)NULL ) const;

    //: Retrieves a table
    //
    //!param: std::string TableName - name of the table to be retrieved.
    //
    //!return: A pointer to the table information if the table exists. A NULL
    //+        pointer otherwise.
    DB::Table* GetTable(const std::string& TableName);

  private:
    // List of tables.
    table_list_type	m_tables;
  };

  inline void Transaction::
  AppendTable(DB::Table* Table, bool Owns)
  {
    table_info_type	ti = { Table, Owns };
    m_tables.push_back(ti);
  }
}

#endif	/* DB_TRANSACTION_HH */
