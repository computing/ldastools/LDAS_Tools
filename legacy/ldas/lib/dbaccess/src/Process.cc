#include "dbaccess/Process.hh"

namespace DB
{
  namespace Process
  {
    const char TABLE_NAME[] = "process";

    const char CREATOR_DB[] = "creator_db";

    const char PROGRAM[] = "program";
    const char PROGRAM_VERSION[] = "version";
    const char REPOSITORY[] = "cvs_repository";
    const char REPOSITORY_ENTRY_TIME[] = "cvs_entry_time";
    const char COMMENT[] = "comment";

    const char IS_ONLINE[] = "is_online";
    const char NODE[] = "node";
    const char USERNAME[] = "username";
    const char OS_PROCESS_ID[] = "unix_procid";
    const char JOB_ID[] = "jobid";
    const char DOMAIN_NAME[] = "domain";
    const char START_TIME[] = "start_time";
    const char END_TIME[] = "end_time";

    const char PROCESS_ID[] = "process_id";
    const char PARAM_SET[] = "param_set";
    const char IFOS[] = "ifos";
  }
}
