/* -*- mode: c++ -*- */

#ifndef ILWDFCS__SUBCONTAINER_CC
#define ILWDFCS__SUBCONTAINER_CC

#include "ilwdfcs/config.h"

#include "ilwd/ldascontainer.hh"

#include "ilwdfcs/FrHistory.hh"
#include "ilwdfcs/FrTable.hh"
#include "ilwdfcs/FrVect.hh"
#include "ilwdfcs/SubContainer.hh"

namespace ILwdFCS
{
  template < class DataType_ >
  SubContainer< DataType_ >::
  SubContainer( const std::string& Name,
		const std::string& Tag,
		const std::string& Type,
		const std::string& Class )
  {
    getContainer()->appendName( Name );
    getContainer()->appendName( Tag );
    getContainer()->appendName( Type );
    getContainer()->appendName( Class );
  }

  template < class DataType_ >
  SubContainer< DataType_ >::
  ~SubContainer( )
  {
    for ( const_iterator i = m_data.begin(); i != m_data.end(); ++i )
    {
      delete *i;
    }
    m_data.resize( 0 );
  }

  template < >
  void SubContainer< FrVect >::
  Append( FrVect& Data )
  {
    Container::Append( Data );
    m_data.push_back( new FrVect( Data.GetArrayBase( ) ) );
  }

  template < class DataType_ >
  void SubContainer< DataType_ >::
  Append( DataType_& Data )
  {
    Container::Append( Data );
    m_data.push_back( new DataType_( Data.GetContainer( ) ) );
  }

  template class SubContainer< FrHistory >;
  template class SubContainer< FrTable >;
  template class SubContainer< FrVect >;
} // namespace - ILwdFCS

#endif /* ILWDFCS__SUBCONTAINER_CC */
