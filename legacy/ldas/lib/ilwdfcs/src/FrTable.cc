#include "ilwdfcs/config.h"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrTable.hh"
#include "ilwdfcs/FrVect.hh"

#include "helper.hh"

using namespace ILwdFCS;

//-----------------------------------------------------------------------
//               P U B L I C   S E C T I O N
//-----------------------------------------------------------------------

FrTable::
FrTable( )
  : Container()
{
  getContainer()->appendName("");
  getContainer()->appendName("");
  getContainer()->appendName("Table");
}

FrTable::
FrTable( ILwd::LdasElement* Element, ILwdFCS::own_type Ownership )
  : Container( dynamic_cast< ILwd::LdasContainer* >( Element ), Ownership )
{
}

FrTable::
FrTable( const ILwd::LdasElement* Element )
  : Container( dynamic_cast< const ILwd::LdasContainer* >( Element ),
	       ILwdFCS::NO_OWN )
{
}

FrTable::
FrTable( const FrTable& Source )
  : Container( Source )
{
}

void FrTable::
AppendRow( FrVect& Row )
{
  Append( Row );
  m_columns.push_back( &Row );
}

void FrTable::
SetComment( const std::string& Comment )
{
  ILwd::LdasString*	comment =
    FindOrCreateElement<ILwd::LdasString>( *getContainer(), TAG_COMMENT );

  comment->setString( Comment );
}

void FrTable::
SetName( const std::string& Name )
{
  ILwd::LdasString*	name =
    FindOrCreateElement<ILwd::LdasString>( *getContainer(), TAG_NAME );

  name->setString( Name );
}


//-----------------------------------------------------------------------
//            P R O T E C T E D   S E C T I O N
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
//              P R I V A T E   S E C T I O N
//-----------------------------------------------------------------------

const std::string	FrTable::TAG_COMMENT("comment");
const std::string	FrTable::TAG_NAME("name");
const std::string	FrTable::TAG_NUMBER_OF_COLUMNS("nColumns");
const std::string	FrTable::TAG_NUMBER_OF_ROWS("nRows");
