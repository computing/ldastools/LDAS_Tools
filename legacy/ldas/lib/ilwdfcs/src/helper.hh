#ifndef ILWDFCS__HELPER_HH
#define	ILWDFCS__HELPER_HH

#include <string>

#include "general/types.hh"

namespace ILwd
{
  template <class DataType_> class LdasArray;
  class LdasContainer;
}

namespace ILwdFCS
{
  template <class DataType_>
  const DataType_*
  FindElement( const ILwd::LdasContainer& Container,
	       const std::string& Name, ILwd::LdasArrayBase::size_type Offset = 0 );

  template <class DataType_>
  DataType_*
  FindOrCreateElement( ILwd::LdasContainer& Container,
		       const std::string& Name );

  template <class DataType_>
  const ILwd::LdasArray<DataType_>*
  FindArray( const ILwd::LdasContainer& Container,
	     const std::string& Name );

  template <class DataType_>
  ILwd::LdasArray<DataType_>*
  FindOrCreateArray( ILwd::LdasContainer& Container,
		     const std::string& Name,
		     const ILwd::LdasArrayBase::size_type Dims,
		     const double StartX = 0.0,
		     const std::string& Units = "",
		     const double DX = 1.0,
		     const std::string& DataUnits = "" );

  template <class DataType_>
  ILwd::LdasArray<DataType_>*
  FindOrCreateArray( ILwd::LdasContainer& Container,
		     const std::string& Name,
		     const ILwd::LdasArrayBase::size_type NDims,
		     const ILwd::LdasArrayBase::size_type* Dims,
		     const double* StartX = 0,
		     const std::string* Units = 0,
		     const double* DX = 0,
		     const std::string& DataUnits = "" );
}

#endif	/* ILWDFCS__HELPER_HH */
