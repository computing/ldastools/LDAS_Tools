#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrHistory.hh"

#include "helper.hh"

using namespace ILwdFCS;

FrHistory::
FrHistory( const std::string& Comment, const std::string& Name, INT_4U Time )
  : Container()
{
  init_container( Comment, Name, Time );
}

FrHistory::
FrHistory( ILwd::LdasElement* Element, ILwdFCS::own_type Ownership )
  : Container( dynamic_cast< ILwd::LdasContainer* >( Element ), Ownership )
{
}

FrHistory::
FrHistory( const ILwd::LdasElement* Element )
  : Container( dynamic_cast< const ILwd::LdasContainer* >( Element ),
	       ILwdFCS::NO_OWN )
{
}

FrHistory::
FrHistory( const FrHistory& Source )
  : Container( Source )
{
}

FrHistory::
~FrHistory( )
{
}

void FrHistory::
SetComment( const std::string& Comment )
{
  ILwd::LdasString*	comment =
    FindOrCreateElement<ILwd::LdasString>( *getContainer(), TAG_COMMENT );

  comment->setString( Comment );
}

void FrHistory::
SetTime( const INT_4U Time )
{
  ILwd::LdasArray<INT_4U>* time =
    FindOrCreateArray<INT_4U>( *getContainer(),
			       TAG_TIME,
			       1);

  (time->getData())[0] = Time;
}

void FrHistory::
SetName( const std::string& Name )
{
  getContainer()->setName( 0, Name, true );
}

const std::string FrHistory::TAG_COMMENT( "comment" );
const std::string FrHistory::TAG_NAME( "name" );
const std::string FrHistory::TAG_TIME( "time" );

void FrHistory::
init_container( const std::string& Comment,
		const std::string& Name,
		INT_4U Time )
{
  getContainer()->appendName("");
  getContainer()->appendName("");
  getContainer()->appendName("History");
  getContainer()->appendName("Frame");

  SetName( Name );
  SetTime( Time );
  SetComment( Comment );
}


