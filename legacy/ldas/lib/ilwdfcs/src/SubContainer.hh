#ifndef ILWDFCS__SUB_CONTAINER_HH
#define ILWDFCS__SUB_CONTAINER_HH

#include <vector>

#include "ilwdfcs/Container.hh"

namespace ILwdFCS
{
  template <class DataType_>
  class SubContainer : public Container
  {
  public:
    typedef std::vector< DataType_* >	list_type;

    SubContainer( const std::string& Name,
		  const std::string& Tag,
		  const std::string& Type,
		  const std::string& Class );

    inline
    SubContainer( const SubContainer& Source )
      : Container( Source.GetContainer( ), ILwdFCS::NO_OWN )
    {
    }
    
    ~SubContainer();
    
    void Append( DataType_& Data );

    const list_type& Ref( ) const;
    
    typedef typename list_type::iterator iterator;
    typedef typename list_type::const_iterator const_iterator;
    typedef unsigned int size_t;

    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    size_t size() const;
  
  private:
    list_type	m_data;
  };

  template< class DataType_ >
  inline const typename SubContainer< DataType_ >::list_type&
  SubContainer< DataType_ >::
  Ref( ) const
  {
    return m_data;
  }

} // namespace - ILwdFCS

#endif /* ILWDFCS__SUB_CONTAINER_HH */
