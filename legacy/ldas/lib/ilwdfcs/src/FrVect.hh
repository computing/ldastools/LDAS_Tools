#ifndef ILWDFCS__FRVECT_HH
#define	ILWDFCS__FRVECT_HH

#include <string>

#include "general/types.hh"

#include "ilwd/ilwdtoc.hh"

#include "ilwdfcs/Container.hh"
#include "ilwdfcs/ILwdFCS.hh"

namespace ILwd
{
  class LdasContainer;
  class LdasArrayBase;
}

namespace ILwdFCS
{
  class FrVect
  {
  public:
    FrVect( );

    FrVect( ILwd::LdasArrayBase* Vect, own_type Ownershiip );

    FrVect( ILwd::LdasElement* Element, own_type Ownership );

    FrVect( const ILwd::LdasArrayBase* Vect );

    FrVect( const ILwd::LdasElement* Element );

    FrVect( const FrVect& Source );

    ~FrVect( );

    const ILwd::LdasArrayBase* GetArrayBase( ) const;

    REAL_8 GetDx( ILwd::LdasArrayBase::size_type Index ) const;

    INT_8U GetNx( ILwd::LdasArrayBase::size_type Index ) const;

    REAL_8 GetStartX( ILwd::LdasArrayBase::size_type Index ) const;

    std::string GetUnitX( ILwd::LdasArrayBase::size_type Index ) const;

    bool OwnsData( ) const;

    template <class DataType_> void SetData( const DataType_* Data,
					      const ILwd::LdasArrayBase::size_type DataSize,
					      const double StartX = 0.0,
					      const std::string& Units = "",
					      const double DX = 0.0,
					      const std::string& DataUnits = "");
    template <class DataType_> void SetData( const DataType_* Data,
					      const ILwd::LdasArrayBase::size_type NDim,
					      const ILwd::LdasArrayBase::size_type* Dims,
					      const double* StartX = 0,
					      const std::string* Units = 0,
					      const double* DX = 0,
					      const std::string& DataUnits = "");

    void SetName( const std::string& Name );

  protected:
    ILwd::LdasArrayBase* getArrayBase( );

    void setOwnsData( bool Owns );

  private:
    friend class Container;

    ILwdFCS::own_type		m_is_owned;
    bool			m_read_only;
    ILwd::LdasArrayBase*	m_data;
  };

  inline const ILwd::LdasArrayBase* FrVect::
  GetArrayBase( ) const
  {
    return m_data;
  }

  inline bool FrVect::
  OwnsData( ) const
  {
    return ( m_is_owned == OWN );
  }

  inline ILwd::LdasArrayBase* FrVect::
  getArrayBase( )
  {
    return m_data;
  }

  inline void FrVect::
  setOwnsData( bool Owns )
  {
    m_is_owned =  ( Owns ) ? OWN : NO_OWN;
  }
}

#endif	/* ILWDFCS__FRVECT_HH */
