#ifndef ILWDFCS__CONTAINER_HH
#define ILWDFCS__CONTAINER_HH

#include <string>

#include "ilwdfcs/ILwdFCS.hh"

namespace ILwd
{
  class LdasContainer;
}

namespace ILwdFCS
{
  class FrVect;

  class Container
  {
  public:
    Container();

    Container( ILwd::LdasContainer* Container,
	       ILwdFCS::own_type Ownership );

    Container( const ILwd::LdasContainer* Container,
	       ILwdFCS::own_type Ownership );

    virtual ~Container();

    void Append( Container& DataSet );
    void Append( FrVect& DataSet );
    const ILwd::LdasContainer* GetContainer() const;
    bool OwnsData() const;
    ILwd::LdasContainer* Release( );
    const ILwd::LdasContainer* Release( ) const;

  protected:
    Container( const Container& Source );

    ILwd::LdasContainer*	getContainer();
    const ILwd::LdasContainer*	getContainer() const;
    void			setOwnsData( bool OwnsData );

    template<class Data_>
    Data_			getValue( const std::string& Element ) const;
    template<class Data_>
    Data_			getValue( const std::string& Element,
					  const Data_ Default ) const;
    template<class Data_>
    void			setValue( const std::string& Element,
					  const Data_ Value );

  private:
    ILwdFCS::own_type		m_is_owned;
    bool			m_read_only;
    ILwd::LdasContainer*	m_data;
  };

  inline const ILwd::LdasContainer* Container::
  GetContainer() const
  {
    return m_data;
  }

  inline bool Container::
  OwnsData() const
  {
    return ( m_is_owned == ILwdFCS::OWN );
  }

  inline ILwd::LdasContainer* Container::
  Release( )
  {
    m_is_owned = ILwdFCS::NO_OWN;
    return m_data;
  }

  inline ILwd::LdasContainer* Container::
  getContainer()
  {
    return m_data;
  }

  inline const ILwd::LdasContainer* Container::
  getContainer() const
  {
    return m_data;
  }

  inline void Container::
  setOwnsData( bool OwnsData )
  {
    m_is_owned =
      ( OwnsData )
      ? ILwdFCS::OWN
      : ILwdFCS::NO_OWN;
  }
}

#endif	/* ILWDFCS__CONTAINER_HH */
