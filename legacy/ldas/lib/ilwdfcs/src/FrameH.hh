/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef ILWDFCS__FRAMEH_HH
#define	ILWDFCS__FRAMEH_HH

#include <string>
#include <vector>

#include "general/gpstime.hh"

#include "ilwd/ilwdtoc.hh"

#include "Container.hh"

namespace ILwdFCS
{
  class FrDetector;
  class FrProcData;
  class FrHistory;

  class FrameH : public Container
  {
  public:
    enum {
      DATA_QUALITY,
      DELTA_TIME,
      DETECTOR_PROC,
      FRAME_NUMBER,
      GPS_LEAP_SECONDS,
      GPS_TIME,
      GPS_TIME_SECONDS,
      GPS_TIME_NANOSECONDS,
      HISTORY,
      DETECTOR_OR_SIMULATION,
      PROC_DATA,
      RAW_DATA,
      RUN
    };

    static const ILwd::TOC::Keys_type& TOCKeys;

    FrameH();

    FrameH( const FrameH& Source );

    void AppendDetector( FrDetector& Detector, bool Duplicates = false );

    void AppendProcData( FrProcData& ProcData );

    void AppendHistory( FrHistory& History );

    ILwd::LdasContainer* ConvertToILwd( INT_4U JobId,
					const std::string& API,
					const std::string& Name,
					bool RelinquishOwnership );

    FrDetector* GetDetector( const std::string& DetectorName ) const;

    void SetCompression( const std::string& Method, int Level );

    void SetCommentAttribute( const std::string& Comment );

    void SetRun( INT_4S Run );

    void SetFrameNumber( INT_4U FrameNumber );

    void SetDataQuality( INT_4U DataQuality );

    void SetStartTime( const General::GPSTime& StartTime );

    void SetGPSLeapSeconds( INT_2U GPSLeapSeconds );

    void SetDeltaTime( REAL_8 Delta );

  private:
    template <class DataType_>
    class FrameContainer : public Container
    {
    public:
      FrameContainer();

      ~FrameContainer();

      void Append( DataType_& Data );

      typedef typename std::vector<DataType_*>::iterator iterator;
      typedef typename std::vector<DataType_*>::const_iterator const_iterator;
      typedef unsigned int size_t;

      iterator begin();
      const_iterator begin() const;
      iterator end();
      const_iterator end() const;
      size_t size() const;

    private:
      std::vector<DataType_*>	m_data;
    };
    
    FrameContainer<FrDetector>	m_detector_container;
    FrameContainer<FrProcData>	m_proc_data_container;
    FrameContainer<FrHistory>	m_history_container;

    General::GPSTime		m_start;
    REAL_8			m_delta;
  };

  template<class Data_>
  inline typename FrameH::FrameContainer<Data_>::iterator
  FrameH::FrameContainer<Data_>::
  begin()
  {
    return m_data.begin();
  }

  template<class Data_>
  inline typename FrameH::FrameContainer<Data_>::const_iterator
  FrameH::FrameContainer<Data_>::
  begin() const
  {
    return m_data.begin();
  }


  template<class Data_>
  inline typename FrameH::FrameContainer<Data_>::iterator
  FrameH::FrameContainer<Data_>::
  end()
  {
    return m_data.end();
  }

  template<class Data_>
  inline typename FrameH::FrameContainer<Data_>::const_iterator
  FrameH::FrameContainer<Data_>::
  end() const
  {
    return m_data.end();
  }

  template<class Data_>
  inline typename FrameH::FrameContainer<Data_>::size_t
  FrameH::FrameContainer<Data_>::
  size() const
  {
    return m_data.size();
  }
}

#endif // ILWDFCS__FRAMEH_HH
