#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include "ilwdfcs/FrRawData.hh"

namespace {
  const std::string	TAG_SERIAL_DATA( "firstSer" );
  const std::string	TAG_ADC_DATA( ":adcdata:Container(AdcData)" );
  const std::string	TAG_ADC_DATA_2( ":adcData:Container(AdcData):Frame" );
  const std::string	TAG_TABLE_DATA( "firstTable" );
  const std::string	TAG_LOG_MESSAGE( "logMsg" );
  const std::string	TAG_MORE( "more" );

  static const ILwd::TOC::Keys_type& gen_toc_key( );
}

using namespace ILwdFCS;

const ILwd::TOC::Keys_type& FrRawData::TOCKeys = gen_toc_key( );

FrRawData::
FrRawData( const FrRawData& Source )
  : Container( Source )
{
}

namespace {
  static const ILwd::TOC::Keys_type& gen_toc_key( )
  {
    static ILwd::TOC::Keys_type key;

#define	ADD(x) key[ TAG_##x] = FrRawData::x
    ADD( SERIAL_DATA );
    ADD( ADC_DATA );
    ADD( TABLE_DATA );
    ADD( LOG_MESSAGE );
    ADD( MORE );
#undef ADD
    key[ TAG_ADC_DATA_2] = FrRawData::ADC_DATA;

    return key;
  }
}
