/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include <stdlib.h>

#include <stdexcept>
#include <string>   

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/LdasHistory.hh"
#include "ilwd/ldasstring.hh"

// #include "framecpp/FrProcData.hh"

#include "ilwdfcs/FrHistory.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrTable.hh"
#include "ilwdfcs/FrVect.hh"
#include "ilwdfcs/helper.hh"

#if 0
#define AT() std::cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#endif

using namespace ILwdFCS;
using std::string;   

static const INT_2U FR_PROC_DATA_DEFAULT_PROC_DATA_TYPE =
  FrProcData::UNKNOWN_TYPE;
static const INT_2U FR_PROC_DATA_DEFAULT_SUB_TYPE
= FrProcData::UNKNOWN_SUB_TYPE;
static const REAL_8 FR_PROC_DATA_DEFAULT_TRANGE = 0.0;
static const REAL_8 FR_PROC_DATA_DEFAULT_FSHIFT = 0.0;
static const REAL_4 FR_PROC_DATA_DEFAULT_PHASE = 0.0;
static const REAL_8 FR_PROC_DATA_DEFAULT_FRANGE = 0.0;
static const REAL_8 FR_PROC_DATA_DEFAULT_BW = 0.0;


//-------------------------------------------------------------------------

FrProcData::
FrProcData()
  : Container(),
    m_data_container( "", TAG_DATA, "Container(Vect)", "Frame" ),
    m_aux_container( "", TAG_AUX, "Container(Vect)", "Frame" ),
    m_table_container( "", TAG_TABLE, "Container(Vect)", "Frame" ),
    m_history_container( "", TAG_HISTORY, "Container(History)", "Frame" ),
    m_offset( 0.0 ),
    m_delta( 0.0 )
{
  init_container();
}

FrProcData::
FrProcData( const ILwd::LdasContainer* Source )
  : Container( Source, ILwdFCS::NO_OWN ),
    m_data_container( "", TAG_DATA, "Container(Vect)", "Frame" ),
    m_aux_container( "", TAG_AUX, "Container(Vect)", "Frame" ),
    m_table_container( "", TAG_TABLE, "Container(Vect)", "Frame" ),
    m_history_container( "", TAG_HISTORY, "Container(History)", "Frame" ),
    m_offset( 0.0 ),
    m_delta( 0.0 )
{
  AT( );
  init_from_container( );
}

FrProcData::
FrProcData( const FrProcData& Source )
  : Container( Source.GetContainer( ), ILwdFCS::NO_OWN ),
    m_data_container( Source.m_data_container ),
    m_aux_container( Source.m_aux_container ),
    m_table_container( Source.m_table_container ),
    m_history_container( Source.m_history_container ),
    m_start( Source.m_start ),
    m_offset( Source.m_offset ),
    m_delta( Source.m_delta )
{
}

FrProcData::
~FrProcData( )
{
  AT( );
}

void FrProcData::
AppendAuxParam( const std::string& name, const REAL_8 value )
{
  ILwd::LdasContainer* p =
    FindOrCreateElement<ILwd::LdasContainer>(*getContainer(), TAG_AUXPARAMS);

  ILwd::LdasContainer* q = new ILwd::LdasContainer(TAG_AUXPARAM);
  
  q->push_back(new ILwd::LdasString(name, TAG_AUXPARAMNAME),
               ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN);

  q->push_back(new ILwd::LdasArray<REAL_8>(value, TAG_AUXPARAMVALUE),
               ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN);

  p->push_back(q, ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN);
}

void FrProcData::
AppendData( FrVect& Data )
{
  if ( m_data_container.OwnsData() )
  {
    // Add to the container
    Append( m_data_container );
  }
  m_data_container.Append( Data );
}

void FrProcData::
AppendData( ILwd::LdasArrayBase* Data, ILwdFCS::own_type Ownership )
{
  FrVect	d( Data, Ownership );
  AppendData( d );
}

void FrProcData::
AppendAux( FrVect& Aux )
{
  if ( m_aux_container.OwnsData() )
  {
    // Add to the container
    Append( m_aux_container );
  }
  m_aux_container.Append( Aux );
}

void FrProcData::
AppendAux( ILwd::LdasArrayBase* Aux, ILwdFCS::own_type Ownership )
{
  FrVect	a( Aux, Ownership );
  AppendAux( a );
}

void FrProcData::
AppendTable( FrTable& Table )
{
  if ( m_table_container.OwnsData() )
  {
    // Add to the container
    Append( m_table_container );
  }
  m_table_container.Append( Table );
}

void FrProcData::
AppendTable( ILwd::LdasContainer* Table, ILwdFCS::own_type Ownership )
{
  FrTable	t( Table, Ownership );
  AppendTable( t );
}

void FrProcData::
AppendHistory( FrHistory& History )
{
  if ( m_history_container.OwnsData() )
  {
    // Add to the container
    Append( m_history_container );
  }
  m_history_container.Append( History );
}

void FrProcData::
AppendHistory( ILwd::LdasContainer* History, ILwdFCS::own_type Ownership )
{
  FrHistory	h( History, Ownership );
  AppendHistory( h );
}

void FrProcData::
AppendHistory( const ILwd::LdasHistory& history,
	       const General::GPSTime& time )
{
  // Lets be safe
  if (history.size() > 0)
  {
    const ILwd::LdasContainer& h = history.GetHistoryAsILwd();
    const std::string procName = GetName();

    for (ILwd::LdasContainer::const_iterator r = h.begin(); r != h.end(); ++r)
    {
      // Use the FrHistory(comment, name, time) constructor. Note that the
      // "comment" is the "name" field of the LdasHistory element, while the
      // "name" field of the FrHistory must be the same as the name of
      // the FrProc
      FrHistory frHistory((*r)->getNameString(), procName, time.GetSeconds());
      
      AppendHistory(frHistory);
    }
  }

}

GPSTime FrProcData::
GetFrameStartTime( ) const
{
  const string& sec( getContainer( )->getName( 3 ) );
  const string& nan( getContainer( )->getName( 4 ) );
   
  GPSTime	ret( atoi( sec.c_str( ) ),
		     atoi( nan.c_str( ) ) );
  return ret;
}

std::string FrProcData::
GetName( ) const
{
  return getContainer( )->getName( 0 );
}

std::string FrProcData::
GetComment( ) const
{
  const ILwd::LdasString*
    comment( FindElement<ILwd::LdasString>( *getContainer( ),
					    TAG_COMMENT ) );
  if ( comment )
  {
    return comment->getString( );
  }
  return "";
}

INT_2U FrProcData::
GetType( ) const
{
  const ILwd::LdasArray<INT_2U>*
    type( FindArray<INT_2U>( *getContainer( ), TAG_TYPE ) );

  if ( type )
  {
    return (type->getData( ))[0];
  }
  return FR_PROC_DATA_DEFAULT_PROC_DATA_TYPE;
}

INT_2U FrProcData::
GetSubType( ) const
{
  const ILwd::LdasArray<INT_2U>*
    sub_type( FindArray<INT_2U>( *getContainer( ), TAG_SUB_TYPE ) );

  if ( sub_type )
  {
    return ( sub_type->getData( ))[0];
  }
  return FR_PROC_DATA_DEFAULT_SUB_TYPE;
}

REAL_8 FrProcData::
GetTimeOffset( ) const
{
  const ILwd::LdasArray<REAL_8>*
    time_offset( FindArray<REAL_8>( *getContainer( ), TAG_TIME_OFFSET ) );

  if ( time_offset )
  {
    return ( time_offset->getData( ))[0];
  }

  const ILwd::LdasArray<INT_4U>*
    time_offset2( FindArray<INT_4U>( *getContainer( ), TAG_TIME_OFFSET ) );

  if ( time_offset2 )
  {
    return ( (time_offset2->getData( ))[0] +
	     (time_offset2->getData( ))[1] * NANOSECOND );
  }
  return FR_PROC_DATA_DEFAULT_SUB_TYPE;
}

REAL_8 FrProcData::
GetSpan() const
{
    REAL_8 span = 0;

    for ( data_type::const_iterator k = RefData().begin();
	 k != RefData().end();
	 ++k )
    {
      span += (*k)->GetNx( 0 ) * (*k)->GetDx( 0 );
    }

    return span;
}

REAL_8 FrProcData::
GetTRange( ) const
{
  REAL_8 ret = getValue<REAL_8>( TAG_TRANGE, -1.0 );

  if ( ret == -1.0 )
  {
    //-------------------------------------------------------------------
    // Was not found, so try to calculate from the data.
    //-------------------------------------------------------------------

    ret = GetSpan();
  }
  return ret;
}

REAL_8 FrProcData::
GetFShift( ) const
{
  return getValue<REAL_8>( TAG_FSHIFT,
			   FR_PROC_DATA_DEFAULT_FSHIFT );
}

REAL_4 FrProcData::
GetPhase( ) const
{
  return getValue<REAL_4>( TAG_PHASE,
			   FR_PROC_DATA_DEFAULT_PHASE );
}

REAL_8 FrProcData::
GetFRange( ) const
{
  return getValue<REAL_8>( TAG_FRANGE,
			   FR_PROC_DATA_DEFAULT_FRANGE );
}

REAL_8 FrProcData::
GetBW( ) const
{
  return getValue<REAL_8>( TAG_BW,
			   FR_PROC_DATA_DEFAULT_BW );
}

REAL_8 FrProcData::
GetSampleRate( ) const
{
  REAL_8	ret( -1.0 );

  if ( GetType( ) == TIME_SERIES )
  {
    data_type::const_iterator d( RefData( ).begin( ) );
    if ( d != RefData( ).end( ) )
    {
      ret = 1.0 / (*d)->GetDx( 0 );
    }
  }
  return ret;
}

GPSTime FrProcData::
GetStopTime( ) const
{
  return GetStartTime() + m_offset + m_delta;
}

const ILwd::LdasContainer& FrProcData::
RefDataContainer( ) const
{
  const ILwd::LdasContainer*
    data( dynamic_cast< const ILwd::LdasContainer* >
	  ( getContainer( )->find( TAG_DATA, 1 ) ) );

  if ( data )
  {
    return *data;
  }
  throw std::range_error( "No data container in FrProcData" );
}

void FrProcData::
SetFrameStartTime( const GPSTime& FrameStartTime )
{
  std::ostringstream	buffer;

  INT_4U sec( FrameStartTime.GetSeconds() );
  buffer <<  sec;

  getContainer( )->setName( 3, buffer.str( ), true );   
   
  INT_4U nanosec( FrameStartTime.GetNanoseconds() );
  buffer.str( "" ); // Reset stream buffer
  buffer << nanosec;

  getContainer( )->setName( 4, buffer.str( ), true );
}

void FrProcData::
SetName( const std::string& Channel )
{
  getContainer()->setName( 0, Channel, true );
}

void FrProcData::
SetName( const std::string& Channel1, const std::string& Channel2 )
{
  std::string	name( Channel1 );
  name += "-";
  name += Channel2;
  SetName( name );
}

void FrProcData::
SetComment( const std::string& Comment )
{
  ILwd::LdasString*	comment =
    FindOrCreateElement<ILwd::LdasString>( *getContainer(), TAG_COMMENT );

  comment->setString( Comment );
}

void FrProcData::
SetType( const INT_2U type )
{
  ILwd::LdasArray<INT_2U>* p =
    FindOrCreateArray<INT_2U>( *getContainer(),
			       TAG_TYPE,
			       1);

  (p->getData())[0] = type;
}

void FrProcData::
SetSubType( const INT_2U subType )
{
  ILwd::LdasArray<INT_2U>* p =
    FindOrCreateArray<INT_2U>( *getContainer(),
			       TAG_SUB_TYPE,
			       1);

  (p->getData())[0] = subType;
}

void FrProcData::
SetTimeOffset( REAL_8 Offset )
{
  ILwd::LdasArray<REAL_8>* to =
    FindOrCreateArray<REAL_8>( *getContainer(),
			       TAG_TIME_OFFSET,
			       1);

  (to->getData())[0] = Offset;
  m_offset = Offset;
}

void FrProcData::
SetTRange( const REAL_8 tRange )
{
  ILwd::LdasArray<REAL_8>* p =
    FindOrCreateArray<REAL_8>( *getContainer(),
			       TAG_TRANGE,
			       1);

  (p->getData())[0] = tRange;
}

void FrProcData::
SetFShift( const REAL_8 FShift )
{
  ILwd::LdasArray<REAL_8>* freq_shift =
    FindOrCreateArray<REAL_8>( *getContainer(),
			       TAG_FSHIFT,
			       1);

  (freq_shift->getData())[0] = FShift;
}

void FrProcData::
SetPhase( const REAL_4 Phase )
{
  ILwd::LdasArray<REAL_4>*  phase =
    FindOrCreateArray<REAL_4>( *getContainer(),
			       TAG_PHASE,
			       1);

  (phase->getData())[0] = Phase;
}

void FrProcData::
SetFRange( const REAL_8 fRange )
{
  ILwd::LdasArray<REAL_8>* p =
    FindOrCreateArray<REAL_8>( *getContainer(),
			       TAG_FRANGE,
			       1);

  (p->getData())[0] = fRange;
}

void FrProcData::
SetBW( const REAL_8 BW )
{
  ILwd::LdasArray<REAL_8>* p =
    FindOrCreateArray<REAL_8>( *getContainer(),
			       TAG_BW,
			       1);

  (p->getData())[0] = BW;
}

void FrProcData::
SetStartTime( const GPSTime& StartTime )
{
  SetFrameStartTime( StartTime );
  m_start = StartTime;
   
  return;
}

void FrProcData::
SetStopTime( const GPSTime& StopTime )
{
  m_delta = StopTime - ( m_start + m_offset );
}

void FrProcData::
ShiftStartTime( const GPSTime& NewStartTime )
{
  AT();
  REAL_8	shift( m_start - NewStartTime );

  AT();
  SetStartTime( NewStartTime );
  AT();
  SetTimeOffset( m_offset + shift );
  AT();
}

//=======================================================================
//                  P R I V A T E   S E C T I O N
//=======================================================================

const std::string	FrProcData::TAG_CONTAINER( "ProcData" );

const std::string	FrProcData::TAG_NAME( "name" );
const std::string	FrProcData::TAG_COMMENT( "comment" );
const std::string	FrProcData::TAG_TYPE( "type" );
const std::string	FrProcData::TAG_SUB_TYPE( "subType" );
const std::string	FrProcData::TAG_TIME_OFFSET( "timeOffset" );
const std::string	FrProcData::TAG_TRANGE( "tRange" );
const std::string	FrProcData::TAG_FSHIFT( "fShift" );
const std::string	FrProcData::TAG_PHASE( "phase" );
const std::string	FrProcData::TAG_FRANGE( "fRange" );
const std::string	FrProcData::TAG_BW( "BW" );
const std::string	FrProcData::TAG_AUXPARAMS( "auxParams" );
const std::string	FrProcData::TAG_AUXPARAM( "auxParam" );
const std::string	FrProcData::TAG_AUXPARAMNAME( "auxParamName" );
const std::string	FrProcData::TAG_AUXPARAMVALUE( "auxParamValue" );
const std::string	FrProcData::TAG_DATA( "data" );
const std::string	FrProcData::TAG_AUX( "aux" );
const std::string	FrProcData::TAG_TABLE( "table" );
const std::string	FrProcData::TAG_HISTORY( "history" );

void FrProcData::
init_container()
{
  getContainer( )->setName( 2, TAG_CONTAINER, true );
  getContainer()->setName( 5, "Frame", true );
}

void FrProcData::
init_from_container()
{
  AT( );
   
  const ILwd::LdasContainer* c( getContainer() );
   
  if( c->getNameSize() < 3 )
  {
    AT( );   
    throw std::bad_cast();
  }
   
  const string& c_name( c->getName( 2 ) );
   
  if( strcasecmp( c_name.c_str( ), TAG_CONTAINER.c_str() ) != 0 )
  {
    AT( );
    throw std::bad_cast( );
  }
    
  AT( );
   
  const string& sec( c->getName( 3 ) ); 
  const string& nan( c->getName( 4 ) );    
   
  m_start = GPSTime( atoi( sec.c_str() ),
		     atoi( nan.c_str() ) );

  AT( );
  m_offset = getValue<REAL_8>( TAG_TIME_OFFSET, 0.0 );



  AT( );
  const ILwd::LdasContainer*
    data( FindElement<ILwd::LdasContainer>( *getContainer( ), TAG_DATA, 1 ) );
  const ILwd::LdasContainer*
    aux( FindElement<ILwd::LdasContainer>( *getContainer( ), TAG_AUX, 1 ) );
  const ILwd::LdasContainer*
    table( FindElement<ILwd::LdasContainer>( *getContainer( ),
					     TAG_TABLE, 1 ) );
  const ILwd::LdasContainer*
    history( FindElement<ILwd::LdasContainer>( *getContainer( ),
					       TAG_HISTORY, 1 ) );

  AT( );
  if ( data )
  {
    for ( ILwd::LdasContainer::const_iterator c( data->begin( ) );
	  c != data->end( );
	  c++ )
    {
      FrVect v( *c );
      AppendData( v );
    }
  }
  
  AT( );
  if ( aux )
  {
    for ( ILwd::LdasContainer::const_iterator c( aux->begin( ) );
	  c != aux->end( );
	  c++ )
    {
      FrVect v( *c );
      AppendAux( v );
    }
  }
  
  AT( );
  if ( table )
  {
    for ( ILwd::LdasContainer::const_iterator c( table->begin( ) );
	  c != table->end( );
	  c++ )
    {
      FrTable t( *c );
      AppendTable( t );
    }
  }
  
  AT( );
  if ( history )
  {
    for ( ILwd::LdasContainer::const_iterator c( history->begin( ) );
	  c != history->end( );
	  c++ )
    {
      FrHistory h( *c );
      AppendHistory( h );
    }
  }
  
  AT( );

}
