#include "ilwdfcs/config.h"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/helper.hh"

#if 0
#define AT() std::cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#endif

namespace ILwdFCS
{
  template <class DataType_>
  DataType_*
  FindOrCreateElement( ILwd::LdasContainer& Container,
		       const std::string& Name )
  {
    ILwd::LdasElement* e( Container.find( Name ) );
    
    if ( ( e != (ILwd::LdasElement*)NULL ) &&
	 ( static_cast<DataType_*>(e) == (DataType_*)NULL ) )
    {
      // Need to remove because it is of the wrong type
      Container.erase( Container.find( e ) );
      e = (ILwd::LdasElement*)NULL;
    }

    if ( e == (ILwd::LdasElement*)NULL )
    {
      Container.push_back( new DataType_(),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      e = Container.back( );
      e->setNameString( Name );
      Container.rehash( );
    }
	  
    return static_cast<DataType_*>( e );
  }

  template <class DataType_>
  const DataType_*
  FindElement( const ILwd::LdasContainer& Container,
	       const std::string& Name, ILwd::LdasArrayBase::size_type Offset )
  {
    const ILwd::LdasElement* element( Container.find( Name, Offset ) );
    return static_cast<const DataType_*>( element );
  }

#define	INSTANTIATE(class_) \
  template   const class_* \
  FindElement<class_>( const ILwd::LdasContainer& Container, \
		       const std::string& Name, ILwd::LdasArrayBase::size_type Offset ); \
    \
  template   class_* \
  FindOrCreateElement<class_>( ILwd::LdasContainer& Container, \
			       const std::string& Name )

  INSTANTIATE(ILwd::LdasContainer);
  INSTANTIATE(ILwd::LdasString);

  template const ILwd::LdasArrayBase*
  FindElement< ILwd::LdasArrayBase >
  ( const ILwd::LdasContainer& Container,
    const std::string& Name, ILwd::LdasArrayBase::size_type Offset );

#undef INSTANTIATE

  template <class DataType_>
  ILwd::LdasArray<DataType_>*
  FindOrCreateArray( ILwd::LdasContainer& Container,
		     const std::string& Name,
		     const ILwd::LdasArrayBase::size_type Dims,
		     const double StartX,
		     const std::string& Units,
		     const double DX,
		     const std::string& DataUnits )
  {
    AT();
    ILwd::LdasElement*	e( Container.find( Name ) );
  
    AT();
    if ( e != (ILwd::LdasElement*)NULL )
    {
      AT();
      ILwd::LdasArray<DataType_>*
	a( static_cast< ILwd::LdasArray<DataType_>*>( e ) );
      
      AT();
      if ( ( e != (ILwd::LdasElement*)NULL ) &&
	   ( ( a == ( ILwd::LdasArray<DataType_>*)NULL ) ||
	     ( a->getNDim() != 1 ) ||
	     ( a->getDimension( 0 ) != Dims ) ||
	     ( a->getStartX( 0 ) != StartX ) ||
	     ( a->getUnits( 0 ) != Units ) ||
	     ( a->getDx( 0 ) != DX ) ||
	     ( a->getDataValueUnit() != DataUnits) ) )
      {
	// Need to remove because it is of the wrong type
	AT();
	Container.erase( Container.find( e ) );
	AT();
	e = (ILwd::LdasElement*)NULL;
	AT();
      }
    }
    AT();
    if ( e == (ILwd::LdasElement*)NULL )
    {
      AT();
      DataType_*	data;
      if ( Dims )
      {
	data = new DataType_[Dims];
      }
      else
      {
	data = (DataType_*)NULL;
      }
      ILwd::LdasArray<DataType_>* t = 
	new ILwd::LdasArray<DataType_>
	( data, Dims, "", Units,
	  false, true,
	  DX, StartX, DataUnits );
      AT();
      Container.push_back( t,
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      AT();
      e = Container.back( );
      e->setNameString( Name );
      Container.rehash( );
    }
	  
    AT();
    return static_cast<ILwd::LdasArray<DataType_>*>( e );
  }

  template <class DataType_>
  ILwd::LdasArray<DataType_>*
  FindOrCreateArray( ILwd::LdasContainer& Container,
		     const std::string& Name,
		     const ILwd::LdasArrayBase::size_type NDims,
		     const ILwd::LdasArrayBase::size_type* Dims,
		     const double* StartX,
		     const std::string* Units,
		     const double* DX,
		     const std::string& DataUnits )
  {
    AT();
    ILwd::LdasElement*	e( Container.find( Name ) );
  
    AT();
    if ( e != (ILwd::LdasElement*)NULL )
    {
      AT();
      ILwd::LdasArray<DataType_>*
	a( static_cast< ILwd::LdasArray<DataType_>*>( e ) );
      
      AT();
      bool	erase( false );

      if ( ( a == (ILwd::LdasArray<DataType_>*)NULL ) ||
	   ( a->getNDim() != NDims ) ||
	   ( a->getDataValueUnit() != DataUnits) )
      {
	erase = true;
      }
      else
      {
	for ( unsigned int x = 0; x < NDims; x++ )
	{
	  if ( ( a->getDimension( x ) != Dims[x] ) ||
	       ( a->getStartX( x ) != StartX[x] ) ||
	       ( a->getUnits( x ) != Units[x] ) ||
	       ( a->getDx( x ) != DX[x] ) )
	  {
	    erase = true;
	    break;
	  }
	}
      }
      if ( erase )
      {
	// Need to remove because it is of the wrong type
	AT();
	Container.erase( Container.find( e ) );
	AT();
	e = (ILwd::LdasElement*)NULL;
	AT();
      }
    }
    if ( e == (ILwd::LdasElement*)NULL )
    {
      ILwd::LdasArrayBase::size_type	count( 0 );

      if ( NDims > 0 )
      {
	count = Dims[0];
	for ( ILwd::LdasArrayBase::size_type x = 1; x < NDims; x++ )
	{
	  count *= Dims[x];
	}
	AT();
      }
      AT();
      DataType_*	data = new DataType_[count];
      ILwd::LdasArray<DataType_>* t = 
	new ILwd::LdasArray<DataType_>
	( data, NDims, Dims, "", Units,
	  false, true,
	  DX, StartX, DataUnits );
      AT();
      Container.push_back( t,
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      AT();
      
      e = Container.back( );
      e->setNameString( Name );
      Container.rehash( );
    }
	  
    AT();
    return static_cast<ILwd::LdasArray<DataType_>*>( e );
  }

  template <class DataType_>
  const ILwd::LdasArray<DataType_>*
  FindArray( const ILwd::LdasContainer& Container,
	     const std::string& Name )
  {
    const ILwd::LdasElement*	e( Container.find( Name ) );
  
    return static_cast<const ILwd::LdasArray<DataType_>*>( e );
  }

#define	INSTANTIATE(class_) \
  template   const ILwd::LdasArray<class_>* \
  FindArray<class_>( const ILwd::LdasContainer& Container, \
                     const std::string& Name ); \
    \
  template   ILwd::LdasArray<class_>* \
  FindOrCreateArray<class_>( ILwd::LdasContainer& Container, \
			     const std::string& Name, \
			     const ILwd::LdasArrayBase::size_type Dims, \
			     const double StartX, \
			     const std::string& Units, \
			     const double DX, \
			     const std::string& DataUnits ); \
    \
  template   ILwd::LdasArray<class_>* \
  FindOrCreateArray<class_>( ILwd::LdasContainer& Container, \
			     const std::string& Name, \
			     const ILwd::LdasArrayBase::size_type NDims, \
			     const ILwd::LdasArrayBase::size_type* Dims, \
			     const double* StartX, \
			     const std::string* Units, \
			     const double* DX, \
			     const std::string& DataUnits )

  INSTANTIATE(CHAR);
  INSTANTIATE(CHAR_U);
  INSTANTIATE(INT_2S);
  INSTANTIATE(INT_2U);
  INSTANTIATE(INT_4S);
  INSTANTIATE(INT_4U);
  INSTANTIATE(INT_8S);
  INSTANTIATE(INT_8U);
  INSTANTIATE(REAL_4);
  INSTANTIATE(REAL_8);
  INSTANTIATE(COMPLEX_8);
  INSTANTIATE(COMPLEX_16);

#undef INSTANTIATE
}
