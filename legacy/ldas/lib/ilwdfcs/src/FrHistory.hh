#ifndef ILWDFCS__FR_HISTORY_HH
#define	ILWDFCS__FR_HISTORY_HH

#include <string>

#include "general/types.hh"

#include "ilwdfcs/Container.hh"

namespace ILwd
{
  class LdasElement;
}

namespace ILwdFCS
{
  class FrHistory: public Container
  {
  public:
    FrHistory( const std::string& Comment = "",
	       const std::string& Name = "",
	       INT_4U Time = 0UL );

    FrHistory( ILwd::LdasElement* Element, ILwdFCS::own_type Ownership );

    FrHistory( const ILwd::LdasElement* Element );

    FrHistory( const FrHistory& Source );

    ~FrHistory( );

    void SetComment( const std::string& Comment );

    void SetName( const std::string& Name );

    void SetTime( INT_4U Time );

  private:
    static const std::string	TAG_NAME;
    static const std::string	TAG_TIME;
    static const std::string	TAG_COMMENT;

    void init_container( const std::string& Comment,
			 const std::string& Name,
			 INT_4U Time );
  };
}

#endif	/* ILWDFCS__FR_HISTORY_HH */
