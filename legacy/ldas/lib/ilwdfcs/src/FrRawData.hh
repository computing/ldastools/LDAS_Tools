#ifndef ILWDFCS__FRRAWDATA_HH
#define ILWDFCS__FRRAWDATA_HH

#include <string>

#include "ilwd/ilwdtoc.hh"

#include "ilwdfcs/Container.hh"

namespace ILwdFCS
{
  class FrTable;
  class FrVect;

  class FrRawData: public Container
  {
  public:
    enum {
      SERIAL_DATA,
      ADC_DATA,
      TABLE_DATA,
      LOG_MESSAGE,
      MORE
    };

    FrRawData( const FrRawData& Source );

    static const ILwd::TOC::Keys_type& TOCKeys;

  };
}

#endif	/* ILWDFCS__FRRAWDATA_HH */


