#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include <math.h>

#include <cmath>
#include <string>
#include <sstream>
#include <stdexcept>

#include "general/toss.hh"

#include "ilwd/ilwdtoc.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrDetector.hh"
#include "ilwdfcs/FrTable.hh"
#include "ilwdfcs/FrVect.hh"

#include "helper.hh"

#if 0
#define AT() std::cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#endif

using namespace ILwdFCS;

namespace {
  static ILwd::TOC::Keys_type& gen_toc_key( );
  static void get_dms( REAL_8 Value,
		       INT_2S& Sign,
		       INT_2S& Degreess, INT_2S& Minutes, REAL_4& Seconds );
  static const FrDetector::name_map_type& init_detector( );
  static const FrDetector::name_map_type& init_prefix( );
  inline static REAL_8 radians( REAL_8 Degrees )
  {
    return ( Degrees * M_PI ) / 180.;
  }
}

static const std::string	TAG_PREFIX( "prefix" );
static const std::string	TAG_LONGITUDE( "longitude" );
static const std::string	TAG_LONGITUDE_DEGREES( "longitudeD" );
static const std::string	TAG_LONGITUDE_MINUTES( "longitudeM" );
static const std::string	TAG_LONGITUDE_SECONDS( "longitudeS" );
static const std::string	TAG_LATITUDE( "latitude" );
static const std::string	TAG_LATITUDE_DEGREES( "latitudeD" );
static const std::string	TAG_LATITUDE_MINUTES( "latitudeM" );
static const std::string	TAG_LATITUDE_SECONDS( "latitudeS" );
static const std::string	TAG_ELEVATION( "elevation" );
static const std::string	TAG_ARM_X_AZIMUTH( "armXazimuth" );
static const std::string	TAG_ARM_Y_AZIMUTH( "armYazimuth" );
static const std::string	TAG_ARM_X_ALTITUDE( "armXaltitude" );
static const std::string	TAG_ARM_Y_ALTITUDE( "armYaltitude" );
static const std::string	TAG_ARM_X_MIDPOINT( "armXmidpoint" );
static const std::string	TAG_ARM_Y_MIDPOINT( "armYmidpoint" );
static const std::string	TAG_LOCAL_TIME( "localTime" );
static const std::string	TAG_MORE( "more" );
static const std::string	TAG_MORE_TABLE( "moreTable" );
static const std::string	TAG_AUX( "aux" );
static const std::string	TAG_TABLE( "table" );

const REAL_4		FrDetector::DFLT_ARM_X_ALTITUDE( 0.0 );
const REAL_4		FrDetector::DFLT_ARM_Y_ALTITUDE( 0.0 );
const REAL_4		FrDetector::DFLT_ARM_X_MIDPOINT( 0.0 );
const REAL_4		FrDetector::DFLT_ARM_Y_MIDPOINT( 0.0 );
const REAL_4		FrDetector::DFLT_ELEVATION( 0.0 );

const FrDetector::name_map_type& FrDetector::m_detector = init_detector( );
const FrDetector::name_map_type& FrDetector::m_channel_prefix = init_prefix( );
const ILwd::TOC::Keys_type& FrDetector::TOCKeys = gen_toc_key( );

#define	LM_CLASS "FrDetector"


FrDetector::
FrDetector()
  : Container()
{
  init_container();
}

FrDetector::
FrDetector( const std::string& Name,
	    const char* Prefix,
	    const REAL_8 Longitude,
	    const REAL_8 Latitude,
	    const REAL_4 Elevation,
	    const REAL_4 ArmXazimuth,
	    const REAL_4 ArmYazimuth,
	    const REAL_4 ArmXaltitude,
	    const REAL_4 ArmYaltitude,
	    const REAL_4 ArmXmidpoint,
	    const REAL_4 ArmYmidpoint,
	    const INT_4S LocalTime )
{
  AT( );
  SetName( Name );
  AT( );
  SetPrefix( Prefix );
  AT( );
  SetLongitude( Longitude );
  AT( );
  SetLatitude( Latitude );
  AT( );
  SetElevation( Elevation );
  AT( );
  SetArmXazimuth( ArmXazimuth );
  AT( );
  SetArmYazimuth( ArmYazimuth );
  AT( );
  SetArmXaltitude( ArmXaltitude );
  AT( );
  SetArmYaltitude( ArmYaltitude );
  AT( );
  SetArmXmidpoint( ArmXmidpoint );
  AT( );
  SetArmYmidpoint( ArmYmidpoint );
  AT( );
  SetLocalTime( LocalTime );
}

FrDetector::
FrDetector( const FrDetector& Source )
  : Container( Source )
{
}

void FrDetector::
AppendMore( FrVect& Data )
{
  Append( Data );
  m_more.push_back( &Data );
}

void FrDetector::
AppendMoreTable( FrTable& Table )
{
  Append( Table );
  m_more_table.push_back( &Table );
}


const std::string FrDetector::
GetName( ) const
{
  return getContainer()->getName( 0 );
}

const std::string& FrDetector::
MapDetectorToPrefix( const std::string& Detector )
{
  name_map_type::const_iterator	retval( m_detector.find( Detector ) );

  if ( retval == m_detector.end() )
  {
    General::toss<std::range_error>( LM_CLASS, __FILE__, __LINE__,
				       "unknown detector" );
  }
  return (*retval).second;
}

const std::string& FrDetector::
MapPrefixToDetector( const std::string& Prefix )
{
  name_map_type::const_iterator	retval( m_channel_prefix.find( Prefix ) );

  if ( retval == m_channel_prefix.end() )
  {
    General::toss<std::range_error>( LM_CLASS, __FILE__, __LINE__,
				       "unknown prefix" );
  }
  return (*retval).second;
}

void FrDetector::
SetPrefix( const char* Prefix )
{
  ILwd::LdasArray< CHAR >*  v =
    ILwdFCS::FindOrCreateArray<CHAR>( *(this->getContainer() ),
				      TAG_PREFIX, 2 );
  (v->getData())[ 0 ] = Prefix[ 0 ];
  (v->getData())[ 1 ] = Prefix[ 1 ];
}

void FrDetector::
SetLongitude( REAL_8 Value )
{
  setValue<REAL_8>( TAG_LONGITUDE, Value );
}

void FrDetector::
SetLongitudeD( INT_2S Value )
{
  INT_2S sign( 0 );
  INT_2S d( 0 );
  INT_2S m( 0 );
  REAL_4 s( 0.0 );

  REAL_8 l = getValue<REAL_8>( TAG_LONGITUDE, 0.0 );
  get_dms( l, sign, d, m, s );
  setValue<REAL_8>( TAG_LONGITUDE,
		    radians( sign * ( Value + m / 60 + s / 3600 ) ) );
}

void FrDetector::
SetLongitudeM( INT_2S Value )
{
  INT_2S sign( 0 );
  INT_2S d( 0 );
  INT_2S m( 0 );
  REAL_4 s( 0.0 );

  REAL_8 l = getValue<REAL_8>( TAG_LONGITUDE, 0.0 );
  get_dms( l, sign, d, m, s );
  setValue<REAL_8>( TAG_LONGITUDE,
		    radians( sign * ( d + Value / 60 + s / 3600 ) ) );
}

void FrDetector::
SetLongitudeS( REAL_4 Value )
{
  INT_2S sign( 0 );
  INT_2S d( 0 );
  INT_2S m( 0 );
  REAL_4 s( 0.0 );

  REAL_8 l = getValue<REAL_8>( TAG_LONGITUDE, 0.0 );
  get_dms( l, sign, d, m, s );
  setValue<REAL_8>( TAG_LONGITUDE,
		    radians( sign * ( d + m / 60 + Value / 3600 ) ) );
}

void FrDetector::
SetLatitude( REAL_8 Value )
{
  setValue<REAL_8>( TAG_LATITUDE, Value );
}

void FrDetector::
SetLatitudeD( INT_2S Value )
{
  INT_2S sign( 0 );
  INT_2S d( 0 );
  INT_2S m( 0 );
  REAL_4 s( 0.0 );

  REAL_8 l = getValue<REAL_8>( TAG_LATITUDE, 0.0 );
  get_dms( l, sign, d, m, s );
  setValue<REAL_8>( TAG_LATITUDE,
		    radians( sign * ( Value + m / 60 + s / 3600 ) ) );
}

void FrDetector::
SetLatitudeM( INT_2S Value )
{
  INT_2S sign( 0 );
  INT_2S d( 0 );
  INT_2S m( 0 );
  REAL_4 s( 0.0 );

  REAL_8 l = getValue<REAL_8>( TAG_LATITUDE, 0.0 );
  get_dms( l, sign, d, m, s );
  setValue<REAL_8>( TAG_LATITUDE,
		    radians( sign * ( d + Value / 60 + s / 3600 ) ) );
}

void FrDetector::
SetLatitudeS( REAL_4 Value )
{
  INT_2S sign( 0 );
  INT_2S d( 0 );
  INT_2S m( 0 );
  REAL_4 s( 0.0 );

  REAL_8 l = getValue<REAL_8>( TAG_LATITUDE, 0.0 );
  get_dms( l, sign, d, m, s );
  setValue<REAL_8>( TAG_LATITUDE,
		    radians( sign * ( d + m / 60 + Value / 3600 ) ) );
}

void FrDetector::
SetElevation( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ELEVATION, Value );
}

void FrDetector::
SetArmXazimuth( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ARM_X_AZIMUTH, Value );
}

void FrDetector::
SetArmYazimuth( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ARM_Y_AZIMUTH, Value );
}

void FrDetector::
SetArmXaltitude( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ARM_X_ALTITUDE, Value );
}

void FrDetector::
SetArmYaltitude( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ARM_Y_ALTITUDE, Value );
}

void FrDetector::
SetArmXmidpoint( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ARM_X_MIDPOINT, Value );
}

void FrDetector::
SetArmYmidpoint( REAL_4 Value )
{
  setValue<REAL_4>( TAG_ARM_Y_MIDPOINT, Value );
}

void FrDetector::
SetLocalTime( INT_4S Value )
{
  setValue<INT_4S>( TAG_LOCAL_TIME, Value );
}

void FrDetector::
SetName( const std::string& Name )
{
  while( getContainer( )->getNameSize( ) < 4 )
  {
    getContainer( )->appendName( "" );
  }
  AT( );
  getContainer()->setName( 0, Name, true );
  AT( );
  getContainer()->setName( 1, "detectProc", true );
  AT( );
  getContainer()->setName( 2, "Detector", true );
  AT( );
  getContainer()->setName( 3, "Frame", true );
  AT( );
}

//=======================================================================
//                  P R I V A T E   S E C T I O N
//=======================================================================

//=======================================================================
//                  S T A T I C   S E C T I O N
//=======================================================================

static struct {
  const char* name;
  const char* prefix;
  bool primary;
} detector_db[] = {
  { "TAMA_300",		"T1", true },
  { "Virgo_CITF",	"V1", true },
  { "Virgo",		"V2", true },
  { "GEO 600",		"G1", true },
  { "LHO_2k",		"H2", true },
  { "LHO_4k",		"H1", true },
  { "LLO_4k",		"L1", true },
  { "CIT_40",		"P1", true },
  { "ALLEGRO_45",	"A1", true },
  { "ALLEGRO_63",	"A2", true },
  { "AURIGA",		"O1", true },
  { "EXPLORER",		"E1", true },
  { "NIOBE",		"B1", true },
  { "Nautilus",		"N1", true },
  { "ACIGA",		"K1", true },
};

void FrDetector::
init_container()
{
  SetName( "" );

  SetArmXmidpoint( DFLT_ARM_X_MIDPOINT );
  SetArmYmidpoint( DFLT_ARM_Y_MIDPOINT );

  SetElevation( DFLT_ELEVATION );
}

namespace {
  static ILwd::TOC::Keys_type& gen_toc_key( )
  {
    static ILwd::TOC::Keys_type key;

#define	ADD(x) key[TAG_##x] = FrDetector::x

    ADD(LONGITUDE);
    ADD(LONGITUDE_DEGREES);
    ADD(LONGITUDE_MINUTES);
    ADD(LONGITUDE_SECONDS);
    ADD(LATITUDE);
    ADD(LATITUDE_DEGREES);
    ADD(LATITUDE_MINUTES);
    ADD(LATITUDE_SECONDS);
    ADD(ELEVATION);
    ADD(ARM_X_AZIMUTH);
    ADD(ARM_Y_AZIMUTH);
    ADD(ARM_X_ALTITUDE);
    ADD(ARM_Y_ALTITUDE);
    ADD(ARM_X_MIDPOINT);
    ADD(ARM_Y_MIDPOINT);
    ADD(LOCAL_TIME);
    ADD(MORE);
    ADD(MORE_TABLE);
    ADD(AUX);
    ADD(TABLE);

#undef ADD

    return key;
  }

  static void get_dms( REAL_8 Value,
		       INT_2S& Sign,
		       INT_2S& Degrees, INT_2S& Minutes, REAL_4& Seconds )
  {

    if ( Value < 0 )
    {
      Sign = -1;
      Value *= -1;
    }
    else
    {
      Sign = 1;
    }
    Degrees = (int)Value;
    Value = (Value - Degrees ) * 60;
    Minutes = (int)Value;
    Seconds = (Value - Minutes) * 60;
  }

  static const FrDetector::name_map_type& init_detector( )
  {
    static FrDetector::name_map_type	detectors;

    for ( unsigned int x = 0;
	  x < sizeof( detector_db )/sizeof( *detector_db );
	  x++ )
    {
      detectors[ detector_db[x].name ] = detector_db[x].prefix;
    }

    return detectors;
  }

  static const FrDetector::name_map_type&
  init_prefix( )
  {
    static FrDetector::name_map_type	prefix;

    for ( unsigned int x = 0;
	  x < sizeof( detector_db )/sizeof( *detector_db );
	  x++ )
    {
      if ( detector_db[x].primary )
      {
	prefix[ detector_db[x].prefix ] = detector_db[x].name;
      }
    }

    return prefix;
  }
}

