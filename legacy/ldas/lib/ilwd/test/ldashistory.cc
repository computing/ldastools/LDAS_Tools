#include "ilwd/config.h"

#include <unistd.h>
#include <cstring>
#include <cstdarg>

#include <memory>
#include <sstream>
#include <string>

#include "general/unittest.h"

#include "ldascontainer.hh"
#include "LdasHistory.hh"
#include "reader.hh"

using namespace std;   
   
   
General::UnitTest	Test;
   

typedef void (*ExceptionFunction_)(ILwd::LdasContainer* Container);

template<class Exception_>
void
history_exception_test(ExceptionFunction_ Function,
		       const std::string& Input,
		       const std::string* Result = (const std::string*) NULL);

void
history_append_test(const char* Input, ...);

void
history_input_test(const std::string& Input,
		   const std::string* Result = (const std::string*) NULL);

void
check(const std::string& TestName,
      const ILwd::LdasElement& Element,
      const std::string& Result)
{
  const std::string	sub_process("History Exception Test: check: ");
  istringstream		result_stream( Result );
  ILwd::Reader		reader(result_stream);
  ILwd::LdasContainer	result_container;
  
  try {
    result_container.read(reader);
    Test.Check(result_container ==
	       dynamic_cast<const ILwd::LdasContainer&>(Element))
      << TestName
      << endl;
  }
  catch(...)
  {
    Test.Check(false) << TestName << ": Caught error on reading"
		      << endl;
  }
  return;
}

void
non_history_function(ILwd::LdasContainer* Container)
{
  ILwd::LdasHistory*	his((ILwd::LdasHistory*)NULL);
  try {
    his = new ILwd::LdasHistory(Container);
  }
  catch(...)
  {
    delete his;
    throw;
  }
  delete his;
}

ILwd::LdasElement*
read_ilwd(const std::string& Input, const std::string& SubProcess)
{
  istringstream		is( Input );
  ILwd::Reader 		reader(is);

  try {
    return ILwd::LdasElement::createElement(reader);
  }
  catch(const LdasException& e)
  {
    for (size_t x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << SubProcess << ": read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< " " << e[x].getInfo()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << SubProcess << ": Caught error on reading"
		      << endl;
    return (ILwd::LdasElement*)NULL;
  }
  return (ILwd::LdasElement*)NULL;
}

void
history_append_test(const char* Input, ...)
{
  const std::string	sub_process("History Append Test");

  ILwd::LdasHistory			calc;
  std::auto_ptr<ILwd::LdasElement>	element;

  try
  {
    element.reset( read_ilwd(Input, sub_process) );
    
    ILwd::LdasContainer* answer(dynamic_cast<ILwd::LdasContainer*>
				(element.get( ) ));
    va_list	ap;
    va_start(ap, Input );
   
   
    while (1)
    {
      char* description = va_arg(ap, char*);
      if (description)
      {
	std::vector<ILwd::LdasElement*>	e;
	while (1)
	{
	  char* element_string = va_arg(ap, char*);

	  if (element_string)
	  {
	    istringstream     is( element_string );
	    ILwd::Reader      reader(is);
	    e.push_back( ILwd::LdasElement::createElement(reader) );
	  }
	  else
	  {
	    break;
	  }
	}
	switch(e.size())
	{
	case 0:
	  calc.AppendRecord(description, (ILwd::LdasElement*)NULL);
	  break;
	case 1:
	  calc.AppendRecord(description,
			    e[0],
			    (ILwd::LdasElement*)NULL);
	  break;
	case 2:
	  calc.AppendRecord(description,
			    e[0], e[1],
			    (ILwd::LdasElement*)NULL);
	  break;
	case 3:
	  calc.AppendRecord(description,
			    e[0], e[1], e[2],
			    (ILwd::LdasElement*)NULL);
	  break;
	default:
	  Test.Check(false) << sub_process << ": too many arguments: "
			    << Input
			    << endl << flush;
	  return;
	}
	for ( std::vector<ILwd::LdasElement*>::iterator
		cur = e.begin( ),
		last = e.end( );
	      cur != last;
	      ++cur )
	{
	  if ( *cur )
	  {
	    delete *cur;
	  }
	}
      }
      else
      {
	break;
      }
    }
   
    va_end(ap);

    Test.Check((answer) && (calc.GetHistoryAsILwd() == *answer))
      << sub_process << ": ";
    calc.GetHistoryAsILwd().write(Test.Message(false), ILwd::ASCII);
    Test.Message(false) << " =?= ";
    answer->write(Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl << flush;

  }
  catch(...)
  {
     try
     {
        throw;
     }
     catch( const std::exception& e )
     {
        Test.Check(false) << sub_process << " Caught exception: " << e.what()
	                  << endl;
        throw;
     }
     catch(...)
     {
         Test.Check(false) << sub_process << " Caught unknown exception." 
                           << endl;
         throw;
     }
  }
}

template<class Exception_>
void
history_exception_test(ExceptionFunction_ Function,
		       const std::string& Input,
		       const std::string* Result)
{
  const std::string	sub_process("History Exception Test");
  istringstream		is( Input );
  ILwd::Reader 		reader(is);
  ILwd::LdasContainer	st;
  bool			caught_exception(false);
  std::string		msg;
  

  if (Result == (const std::string*)NULL)
  {
    Result = &Input;
  }
  try {
    st.read(reader);
  }
  catch(const LdasException& e)
  {
    for (size_t x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << sub_process << ": read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << sub_process << ": Caught error on reading"
		      << endl;
    return;
  }
  try
  {
    (*Function)(&st);
    caught_exception = false;
    msg = ": No exception thrown";
  }
  catch(const Exception_& e)
  {
    caught_exception = true;
  }
  catch(const std::exception& e)
  {
    msg = ": ";
    msg += e.what();
  }
  catch(...)
  {
    caught_exception = false;
    msg = ": Unknown exception caught";
  }
  const Exception_	exception;

  Test.Check(caught_exception)
    << sub_process
    << exception.what() << msg
    << ": " << Input
    << endl << flush;
}

void
history_input_test(const std::string& Input,
		   const std::string* Result)
{
  const std::string	sub_process("History Input Test");
  istringstream		is( Input );
  ILwd::Reader 		reader(is);
  ILwd::LdasContainer	st;
  

  try
  {
     st.read(reader);
  }
  catch(const LdasException& e)
  {
    for (size_t x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << sub_process << ": read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< " " << e[x].getInfo()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << sub_process << ": Caught error on reading"
		      << endl;
    return;
  }
  //---------------------------------------------------------------------
  // Create the initial history record
  //---------------------------------------------------------------------
  try
  {
    ILwd::LdasHistory	history(&st);
   
    //---------------------------------------------------------------------
    // check the result against the expected value.
    //---------------------------------------------------------------------
    if (Result == (const std::string*)NULL)
    {
       Result = &Input;
    }
    check(sub_process, history.GetHistoryAsILwd(), *Result);   
  }
  catch( const std::exception& exc )
  {
    Test.Check(false) << sub_process << ": Caught error: "
                      << exc.what()
		      << endl;
    return;    
  }
  catch(...)
  {
    Test.Check(false) << sub_process << ": Caught error on reading"
		      << endl;
    return;
  }
   
  return;
}

static const char history_001_str[] =
"<ilwd size='2' name='LDAS_History'>\n"
"  <ilwd name='filterX decimation'>\n"
"    <real_4>1024.000</real_4>\n"
"  </ilwd>\n"
"  <ilwd size='3' name='methodII line_removal'>\n"
"    <real_4 units='HZ'>60.0</real_4>\n"
"    <real_4 units='HZ'>180.0</real_4>\n"
"    <real_4 units='HZ'>360.0</real_4>\n"
"  </ilwd>\n"
"</ilwd>\n";
  
static const char history_ex_001_str[] =
"<ilwd name='Not_History'>\n"
"  <ilwd size='0' name='filter'></ilwd>\n"
"</ilwd>";

static const char history_ex_002_str[] =
"<ilwd size='2' name='Not_History'>\n"
"  <ilwd name='filterX decimation'>\n"
"    <real_4>1024.000</real_4>\n"
"  </ilwd>\n"
"  <ilwd size='3' name='methodII line_removal'>\n"
"    <real_4 units='HZ'>60.0</real_4>\n"
"    <real_4 units='HZ'>180.0</real_4>\n"
"    <real_4 units='HZ'>360.0</real_4>\n"
"  </ilwd>\n"
"</ilwd>";

static const char history_ex_003_str[] =
"<ilwd size='2' name='History'>\n"
"  <real_4>1024.000</real_4>\n"
"  <ilwd size='3' name='methodII line_removal'>\n"
"    <real_4 units='HZ'>60.0</real_4>\n"
"    <real_4 units='HZ'>180.0</real_4>\n"
"    <real_4 units='HZ'>360.0</real_4>\n"
"  </ilwd>\n"
"</ilwd>";
  
int
main(int ArgC, char** ArgV)
{
  //---------------------------------------------------------------------
  // Testing initialization
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Testing of normal operations
  //---------------------------------------------------------------------

  history_input_test(history_001_str);
  history_append_test(history_001_str,
		      "filterX decimation",
		      "<real_4>1024.000</real_4>",
		      (char*)NULL,
		      "methodII line_removal",
		      "<real_4 units='HZ'>60.0</real_4>",
		      "<real_4 units='HZ'>180.0</real_4>",
		      "<real_4 units='HZ'>360.0</real_4>",
		      (char*)NULL,
		      (char*)NULL
		      );

  //---------------------------------------------------------------------
  // Testing of exception handling
  //---------------------------------------------------------------------

  history_exception_test<ILwd::LdasHistory::NonHistoryContainer>
    (non_history_function, history_ex_001_str);
  history_exception_test<ILwd::LdasHistory::NonHistoryContainer>
    (non_history_function, history_ex_002_str);
  history_exception_test<ILwd::LdasHistory::NonHistoryContainer>
    (non_history_function, history_ex_003_str);

  //---------------------------------------------------------------------
  // Testing completion
  //---------------------------------------------------------------------

  Test.Exit();
}
