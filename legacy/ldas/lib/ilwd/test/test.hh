#define TRESULT _r_
#define TSTART() bool TRESULT(true);
#define TEST( a ) TRESULT = TRESULT && ( a )
#define TEST2( b, a ) {bool tmp = ( a ); if ( !tmp ) cout << endl << ( b ) << " failed... "; TRESULT = TRESULT && tmp;}
#define TEND() return TRESULT;
#define TMSG(a) cout << "      " << ((TRESULT) ? "pass" : "fail") << ": " << a << endl
#define TMSGEND(a) TMSG(a); TEND()
#define	TEXIT() exit ((TRESULT) ? 0 : 1)
