#include "ilwd/config.h"

#include <stdio.h>
#include <unistd.h>
#include <stream.h>

#include <general/util.hh>

#include "test.hh"
#include "ldaselement.hh"
#include "ldasformatelement.hh"
#include "ldasarray.hh"

bool
array_char()
{
  TSTART();

  ILwd::LdasArray<CHAR> ca("help", 4, "character array");

  TMSG("CHAR: " << ca);
  TEND();
}

bool
array_int()
{
  TSTART();

  INT_4S data[4] = { -1, -2, -3, -4};
  unsigned int	size(sizeof(data)/sizeof(*data));

  ILwd::LdasArray<INT_4S> a(data, size, "4 byte int array");

  TMSG("INT_4S: " << a);
  TEND();
}

bool
int_array_assign()
{
  TSTART();

  INT_4S data1[] = {-4, -3, -2, -1};

  ILwd::LdasArray<INT_4S>	a1(data1, sizeof(data1)/sizeof(*data1), "a1");
  ILwd::LdasArray<INT_4S>	a2 = a1;

  TMSG("a1: " << a1 << " a2: " << a2);
  TEND();
}

bool
int_array_concat()
{
  TSTART();

  INT_4S data1[] = {-4, -3, -2, -1};
  INT_4S data2[] = {0, 1, 2, 3, 4 };

  ILwd::LdasArray<INT_4S>	a1(data1, sizeof(data1)/sizeof(*data1), "a1");
  ILwd::LdasArray<INT_4S>	a2(data2, sizeof(data2)/sizeof(*data2), "a2");

  size_t	a1_size(a1.getNData());
  size_t	a2_size(a2.getNData());

  a1 += a2;
  TEST(a1.getNData() == (a1_size + a2_size));
  a1_size = a1.getNData();
  TMSG("a1 += a2: " << a1);

  a2 += a2;
  TEST(a2.getNData() == (a2_size + a2_size));
  a2_size = a2.getNData();
  TMSG("a2 += a2: " << a2);
  TEND();
}

bool
int_multi_array_concat()
{
  TSTART();

  size_t       	dim[] = {2, 4};
  size_t	dim3[] = {2, 3};

  INT_4S data1[2][4] = {{11, 12, 13, 14}, {21, 22, 23, 24}};
  INT_4S data2[2][4] = {{31, 32, 33, 34}, {41, 42, 43, 44}};
  INT_4S data3[2][3] = {{31, 32, 33}, {41, 42, 43}};
  INT_4S result[4][4];

  ILwd::LdasArray<INT_4S>	a1(&data1[0][0], 2U, dim, "a1");
  ILwd::LdasArray<INT_4S>	a2(&data2[0][0], 2U, dim, "a2");
  ILwd::LdasArray<INT_4S>	a3(&data3[0][0], 2U, dim3, "a3");

  TMSG("a1: " << a1);
  TMSG("a2: " << a2);

  size_t	a1_size(a1.getNData());
  size_t	a2_size(a2.getNData());

  a1 += a2;
  memcpy(result, a1.getData(), sizeof(result));
  TEST(a1.getNData() == (a1_size + a2_size));
  TEST(result[2][0] == data2[0][0]);
  
  a1_size = a1.getNData();
  TMSG("a1 += a2: " << a1);

  a2 += a2;
  TEST(a2.getNData() == (a2_size + a2_size));
  a2_size = a2.getNData();
  TMSG("a2 += a2: " << a2);


  try {
    a1 += a3;
    TEST(false);
  }
  catch (bad_alloc) {
    TEST(true);
  }
  TMSG("Miss matched multi-dimensional arrays");
  TEND();
}

int
main()
{
  TSTART();

  TEST(array_char());
  TEST(array_int());

  TEST(int_array_assign());

  TEST(int_array_concat());

  TEST(int_multi_array_concat());
  TEXIT();
}

    


    

    
