#include "ilwd/config.h"

#include <setjmp.h>
#include <signal.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
   
#include "general/Memory.hh"

// ILWD Header Files   
#include "ldascontainer.hh"
#include "ldasarray.hh"   
#include "style.hh"
#include "util.hh"
   
// Test Header Files
#include "general/unittest.h"   

// General Header Files   
#include <general/util.hh>   
#include <general/types.hh>   
   
using ILwd::LdasElement;   
using ILwd::LdasContainer;
using ILwd::Reader;
using ILwd::LdasArray;   
   
using namespace std;   
   
General::UnitTest containerTest;
static jmp_buf	alarm_jmp_buf;

extern "C" {
  static void
  sig_alrm( int Signal )
  {
    longjmp( alarm_jmp_buf, 1 );
  }
}

static const char original_container[] =
"<ilwd name='test0:cont0' size='5'>\n"
"    <ilwd name='test1:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test2:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test3:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test4:cont' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test5:cont1' size='0'>\n"
"    </ilwd>\n"
"</ilwd>";
   
   
static const char pop_back_container[] =
"<ilwd name='test0:cont0' size='4'>\n"
"    <ilwd name='test1:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test2:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test3:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test4:cont' size='0'>\n"
"    </ilwd>\n"
"</ilwd>";


static const char found_by_name_element[] =
"<ilwd name='test4:cont' size='0'>\n"
"</ilwd>";  

   
static const char erase_begin_container[] = 
"<ilwd name='test0:cont0' size='3'>\n"
"    <ilwd name='test2:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test3:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test4:cont' size='0'>\n"
"    </ilwd>\n"
"</ilwd>";   
   
   
static const char insert_end_container[] = 
"<ilwd name='test0:cont0' size='4'>\n"
"    <ilwd name='test2:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test3:cont1' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='test4:cont' size='0'>\n"
"    </ilwd>\n"
"    <ilwd name='insert:cont' size='0'>\n"
"    </ilwd>\n"
"</ilwd>";   
   

void writeToFile( const LdasContainer* c, const std::string& name );
   
   
void
truncated_test( const char* Text, const int Offset )
{
  bool	pass = true;

  static const char* filename = "truncated.ilwd";
  if ( signal (SIGALRM, sig_alrm ) == SIG_ERR )
  {
    return;
  }

  std::ofstream os( filename );
  os << Text;
  os.close( );

  std::ifstream is( filename );
  if ( setjmp( alarm_jmp_buf ) == 0 )
  {
    alarm( 5 );			// Give 5 seconds to complete
    ILwd::Reader 	reader(is);
    ILwd::LdasContainer	c;

    try
    {
      c.read( reader );
    }
    catch( const LdasException& se )
    {
      containerTest.Message( );
      for ( size_t x = 0, end = se.getSize( ) ;
	    x < end;
	    x++ )
      {
	containerTest.Message( false )
	  << se[ x ].getLibrary( ) << " "
	  << se[ x ].getCode( ) << " "
	  << se[ x ].getMessage( ) << " "
	  << se[ x ].getInfo( ) << " "
	  << se[ x ].getFile( ) << " "
	  << se[ x ].getLine( ) << std::endl;
      }
    }
  }
  else
  {
    pass = false;
  }

  signal(SIGALRM, SIG_IGN);
  containerTest.Check( pass ) << "Reading of truncated file"
			      << " - " << Offset
			      << std::endl;
  is.close( );
  unlink( filename );
}

void
truncated( )
{
  static const char* ilwds[] = 
    {
      //-----------------------------------------------------------------
      // 0) This one comes from PR#1992
      //-----------------------------------------------------------------
      "<ilwd name='response:spectrum:sequence' size='9'>\n"
      "  <lstring name='complex:domain' size='4'>FREQ</lstring>\n"
      "  <int_4u name='gps_sec:start_time' units='sec'>0</int_4u>\n"
      "  <int_4u name='gps_nan:start_time' units='nanosec'>0</int_4u>\n"
      "  <int_4u name='gps_sec:stop_time' units='sec'>0</int_4u>\n"
      "  <int_4u name='gps_nan:stop_time' units='nanosec'>0</int_4u>\n"
      "  <real_8 name='start_freq' units='hz'>0.0000000000000000e+00</real_8>\n"
      "  <real_8 name='stop_freq' units='hz'>2.0480000000000000e+03</real_8>\n"
      "  <real_8 name='freq:step_size' units='hz'>1.5625000000000000e-02</real_8>\n"
      //-----------------------------------------------------------------
      // 1) This is a modifed version of PR#1992
      //-----------------------------------------------------------------
      ,
      "<ilwd name='response:spectrum:sequence' size='9'>\n"
      "  <lstring name='complex:domain' size='4'>FREQ</lstring>\n"
      "  <int_4u name='gps_sec:start_time' units='sec'>0</int_4u>\n"
      "  <int_4u name='gps_nan:start_time' units='nanosec'>0</int_4u>\n"
      "  <int_4u name='gps_sec:stop_time' units='sec'>0</int_4u>\n"
      "  <int_4u name='gps_nan:stop_time' units='nanosec'>0</int_4u>\n"
      "  <real_8 name='start_freq' units='hz'>0.0000000000000000e+00</real_8>\n"
      "  <real_8 name='stop_freq' units='hz'>2.0480000000000000e+03</real_8>\n"
      "  <real_8 name='freq:step_size'\n"
    }
    ;

  for ( unsigned int x = 0; x < sizeof(ilwds)/sizeof(*ilwds); x++ )
  {
    truncated_test( ilwds[ x ], x );
  }
}

void
bad_dims( )
{
  //---------------------------------------------------------------------
  // PR#1976
  //---------------------------------------------------------------------
  static const char* ilwd = 
    "<ilwd size='1'>"
    "    <real_4 comment='H1_soma' dims='154' name='H1_s2_lines'>10.0 20.0 23.75 6.0 25.7 8.0 35.62 8.0 42.25 4.0 64.0 14.0 71.85 2.0 79.6 3.0 96.5 3.0 110.0 4.0 115.375 2.0 120.0 4.0 134.0 3.0 146.0 6.0 150.0 4.0 168.0 5.0 170.5 2.0 180.0 4.0 186.3 3.0 190.0 3.0 192.0 3.0 209.0 10.0 219.1 2.0 240.0 6.0 300.0 6.0 346.0 8.0 356.5 4.0 360 4.0 420.0 4.0 438.0 10.0 446.0 6.0 450.575 4.0 469.0 3.0 480.0 4.0 540.0 4.0 546.1 3.0 600.0 4.0 660.0 4.0 688.5 6.0 695.0 3.0 720.0 4.0 735.0 20.0 780.0 4.0 840.0 4.0 900.0 4.0 960.0 3.0 973.3 4.0 1020.0 4.0 1080.0 4.0 1092.1 4.0 1140.5 3.0 1200.0 2.0 1260.0 4.0 1320.0 4.0 1360.0 3.0 1380.0 3.0 1418.0 8.0 1426.4 3.0 1442.0 5.0 1478.0 8.0 1500.0 6.0 1560.0 3.0 1582.0 3.0 1620.0 4.0 1680.0 3.0 1740.0 4.0 1740.0 4.0 1774.0 8.0 1800.0 4.0 1860.0 4.0 1980.0 4.0 1995.8 4.0</real_4>"
    "</ilwd>"
  ;
  try {
   // Test container functionality
   std::istringstream s( ilwd );
   std::unique_ptr<Reader> r( new Reader( s ) );   

   std::unique_ptr<LdasContainer> c( new LdasContainer( *r ) );
  }
  catch( const LdasException& exc )
  {
    bool ok = false;
    size_t exc_size( exc.getSize() );
    if ( ( exc_size == 1 ) &&
	 ( exc[ 0 ].getLibrary( ) == Library::ILWD ) &&
	 ( exc[ 0 ].getCode( ) == ILwd::Errors::DIM_ERROR ) )
    {
      // Do Nothing
      ok = true;
    }
    for( size_t i = 0; i < exc_size; ++i )
    {
      containerTest.Check( ok ) << "bad_dims: " 
				<< exc[ i ].getMessage() 
				<< "( " 
				<< exc[ i ].getInfo()
				<< " )"       
				<< endl << flush;
    }
  }
  catch ( const std::exception& Exception )
  {
    std::cerr << "DEBUG: bad_dims: " << Exception.what( ) << std::endl;
  }
  catch( ... )
  {
    std::cerr << "DEBUG: bad_dims: Generic exception" << std::endl;
  }
}

int main( int argc, char** argv )
try   
{
   // Initialize test
   containerTest.Init( argc, argv );
   

   // Test container functionality
   istringstream s( original_container );

   Reader* r( new Reader( s ) );   
   LdasContainer* c( new LdasContainer( *r ) );
   delete r;
   
   
   // Turn hashing on( name position = 1 ) 
   c->setHashing( true, 1 );
   

   //--------------------------------------------------------------------------
   // Find container element by name
   //--------------------------------------------------------------------------   
   const LdasElement* f_elem = c->find( "cont", 1 );
   istringstream found_by_name_stream( found_by_name_element );

   r = new Reader( found_by_name_stream );
   LdasContainer* found_elem( new LdasContainer( *r ) );
   delete r;
   
   // Verify test
   containerTest.Check( ( *f_elem == *found_elem ), 
                        "found element by hash name..." );

   delete found_elem;
   

   //--------------------------------------------------------------------------   
   // Pop back container element
   //--------------------------------------------------------------------------   
   c->pop_back();   
   
   istringstream pop_back_stream( pop_back_container );

   r = new Reader( pop_back_stream );
   LdasContainer* pop_back_elem( new LdasContainer( *r ) );
   delete r;   

   // Verify test success   
   containerTest.Check( ( *c == *pop_back_elem ), 
                        "pop_back..." );

   delete pop_back_elem;
   

   //--------------------------------------------------------------------------   
   // Erase container begin element   
   //--------------------------------------------------------------------------   
   c->erase( c->begin() );   
   
   istringstream erase_begin_stream( erase_begin_container );

   r = new Reader( erase_begin_stream );
   LdasContainer* erase_begin_elem( new LdasContainer( *r ) );
   delete r;   

   // Verify test success   
   containerTest.Check( ( *c == *erase_begin_elem ), 
                        "erase begin element..." );   

   delete erase_begin_elem;
   
   
   //--------------------------------------------------------------------------      
   // Insert new element into container at the very end.
   //--------------------------------------------------------------------------      
   LdasElement* insert_elem( new LdasContainer( "insert:cont" ) );   
   c->insert( c->end(), insert_elem,
	      LdasContainer::NO_ALLOCATE,
	      LdasContainer::OWN );

   istringstream insert_end_stream( insert_end_container );

   r = new Reader( insert_end_stream );
   LdasContainer* insert_end_elem( new LdasContainer( *r ) );
   delete r;   

   // Verify test success   
   containerTest.Check( ( *c == *insert_end_elem ), 
                        "insert element at the end..." );   

   delete insert_end_elem;
   

   //--------------------------------------------------------------------------      
   // Erase all container elements.
   //--------------------------------------------------------------------------         
   c->erase( c->begin(), c->end() );
   
   // Verify test success   
   containerTest.Check( ( c->size() == 0 ), 
                        "erase all container elements..." );   
   
   delete c;
   
   

   
   // Test LdasContainer::operator+=:
   LdasContainer* c1( new LdasContainer( "c1" ) );
   
   const INT_4U buffer[] = { 1, 2, 3, 4, 5 };
   LdasArray< INT_4U >* temp_a( new LdasArray< INT_4U >( 
      buffer, 5, "temp" ) );
   // c1 doesn't own temp_a
   c1->push_back( temp_a, LdasContainer::NO_ALLOCATE, LdasContainer::NO_OWN );
   
   LdasContainer* c2( new LdasContainer( "c2" ) );
   
   ( *c2 ) += ( *c1 );
   
   delete c1;
   delete c2;
   delete temp_a;
   
   // Verify test success   
   containerTest.Check( true, "operator+=()..." );         
   
   truncated( );

   
   // Test container ownership:
   // parent
   //      | | |
   //      | | |> NO_OWN child_1
   //      | |
   //      | |> OWN parent_1
   //      |               | |
   //      |               | |>OWN child_2
   //      |               |
   //      |               |>NO_OWN child_1
   //      |> OWN child_2
   //
   
   // Construct the tree described above:
   unique_ptr< LdasContainer > parent( new LdasContainer( "parent" ) );
   
   // Container doesn't own child #1:
   unique_ptr< LdasContainer > child_1( new LdasContainer( "child1" ) );   
   parent->push_back( child_1.get(), LdasContainer::NO_ALLOCATE, LdasContainer::NO_OWN );
   
   
   // Create parent_1
   unique_ptr< LdasContainer > parent_1( new LdasContainer( "parent1" ) );
   
   // Have access to the parent1
   LdasContainer* parent1_ptr( parent_1.get() );
   parent->push_back( parent_1.release(), LdasContainer::NO_ALLOCATE, LdasContainer::OWN );   
   
   
   unique_ptr< LdasContainer > child_2( new LdasContainer( "child2" ) );
   LdasContainer* child2_ptr( child_2.get() );
   
   // parent1 owns child_2
   parent1_ptr->push_back( child_2.release(), LdasContainer::NO_ALLOCATE, LdasContainer::OWN );
   
   // parent1 has shallow pointer to child_1
   parent1_ptr->push_back( child_1.get(), LdasContainer::NO_ALLOCATE, LdasContainer::NO_OWN );   
   
   
   // parent owns child_2
   parent->push_back( child2_ptr, LdasContainer::NO_ALLOCATE, LdasContainer::OWN );   
   
   
   // Delete parent1: causing deletion of children it owns: child2
   delete parent1_ptr;
   
   parent.release();
   
   child_1.release();

   containerTest.Check( true, "Container ownership..." );            
      
   
   //--------------------------------------------------------------------
   // Misc tests
   //--------------------------------------------------------------------
   bad_dims( );
   
   //--------------------------------------------------------------------
   // Wrap things up
   //--------------------------------------------------------------------
   containerTest.Exit();
}
catch( LdasException& exc )
{
   size_t exc_size( exc.getSize() );
   for( size_t i = 0; i < exc_size; ++i )
   {
     containerTest.Check( false ) << "writeToFile: " 
                                  << exc[ i ].getMessage() 
                                  << "( " 
                                  << exc[ i ].getInfo()
                                  << " )"       
                                  << endl << flush;
   }

   

   containerTest.Exit();
}
catch( std::bad_alloc& )
{
   containerTest.Check( false ) << "memory allocation failed"
                                << endl << flush;
   containerTest.Exit();   
}
catch( ... )
{
   containerTest.Check( false ) << "unknown exception"
                                << endl << flush;
   containerTest.Exit();   
}

   
void writeToFile( const LdasContainer* c, const std::string& name )
{
   static const ILwd::Format format( ILwd::USER_FORMAT );
   static const ILwd::Compression compression( ILwd::USER_COMPRESSION );
   
   ofstream out( name.c_str() );
   
   if( !out )
   {
      containerTest.Check( false ) << "Error opening file for "
                                   << name << endl << flush;
      return;
   }

   
   ILwd::writeHeader( out );

   try
   {
      c->write( 0, 4, out, format, compression );   
   }
   catch( LdasException& exc )
   {
      size_t exc_size( exc.getSize() );
      for( size_t i = 0; i < exc_size; ++i )
      {
         containerTest.Check( true ) << "writeToFile exception: " 
                                  << exc[ i ].getMessage() 
                                  << "( " 
                                  << exc[ i ].getInfo()
                                  << " )"    
                                  << endl << flush;
      }
   }
   
   out.close();   
   return;
}
   
