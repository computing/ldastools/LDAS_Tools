/* -*- C++ -*- */
#include "ilwd/config.h"

#include <stdlib.h>
#include <unistd.h>
#include <sstream>

#include "general/util.hh"
#include "general/unittest.h"

#include "ldaselement.hh"
#include "ldasformatelement.hh"
#include "ldasarray.hh"

using namespace std;
   
   
General::UnitTest	Test;

void
int_array_concat()
{
  INT_4S data1[] = {-4, -3, -2, -1};
  INT_4S data2[] = {0, 1, 2, 3, 4 };

  ILwd::LdasArray<INT_4S>	a1(data1, sizeof(data1)/sizeof(*data1), "a1");
  ILwd::LdasArray<INT_4S>	a2(data2, sizeof(data2)/sizeof(*data2), "a2");

  ILwd::LdasArray<INT_4S>::size_type	a1_size(a1.getNData());
  ILwd::LdasArray<INT_4S>::size_type	a2_size(a2.getNData());

  a1 += a2;
  Test.Check(a1.getNData() == (a1_size + a2_size))
    << "a1 += a2: " << a1 << endl;
  a1_size = a1.getNData();

  a2 += a2;
  Test.Check(a2.getNData() == (a2_size + a2_size))
    << "a2 += a2: " << a2 << endl;
  a2_size = a2.getNData();
}

void
int_multi_array_concat()
{
  bool		exception;
  const INT_4U  numDims( 2 );
  ILwd::LdasArrayBase::size_type       	dim[] = { numDims, 4};
  ILwd::LdasArrayBase::size_type	dim3[] = { numDims, 3};

  const INT_4S data1[2][4] = {{11, 12, 13, 14}, {21, 22, 23, 24}};
  const INT_4S data2[2][4] = {{31, 32, 33, 34}, {41, 42, 43, 44}};
  const INT_4S data3[2][3] = {{31, 32, 33}, {41, 42, 43}};
  INT_4S result[4][4];

  ILwd::LdasArray<INT_4S>	a1(&data1[0][0], numDims, dim, "a1");
  ILwd::LdasArray<INT_4S>	a2(&data2[0][0], numDims, dim, "a2");
  ILwd::LdasArray<INT_4S>	a3(&data3[0][0], numDims, dim3, "a3");

  Test.Message() << "a1: " << a1 << endl;
  Test.Message() << " a2: " << a2 << endl;
  Test.Message() << " a3: " << a3 << endl;   

  ILwd::LdasArrayBase::size_type	a1_size(a1.getNData());
  ILwd::LdasArrayBase::size_type	a2_size(a2.getNData());

  a1 += a2;
  memcpy(result, a1.getData(), sizeof(result));
  Test.Check(a1.getNData() == (a1_size + a2_size));
  Test.Check(result[2][0] == data2[0][0]);
  
  a1_size = a1.getNData();
  Test.Message() <<  "a1 += a2: " << a1 << endl;

  a2 += a2;
  Test.Check(a2.getNData() == (a2_size + a2_size))
    << "a2 += a2: " << a2 << endl;
  a2_size = a2.getNData();

  exception = false;
  try {
    a1 += a3;
  }
  catch ( FormatException ) {
    exception = true;
  }
  Test.Check( exception ) << "Miss matched multi-dimensional arrays" << endl;
}

int
main(int ArgC, char** ArgV)
{
  // Individual tests to be executed
  int_array_concat();
  int_multi_array_concat();

  // Summarize the results
  Test.Exit();
}
