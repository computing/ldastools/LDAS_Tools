#ifndef PIHH
#define PIHH

//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif


#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

// System Includes
#include <iosfwd>

// Local Includes
#include "base.hh"
#include "reader.hh"
#include "errors.hh"


/// \cond DOXYGEN_IGNORE
namespace ILwd
{
    class PI;
}
/// \endcond

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::PI& base );
void os_read( os_bstream& stream, ILwd::PI& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: Processing Instruction
//
// This class represents an XML processing instruction.
//
class ILwd::PI : public ILwd::Base
{
public:

    /* Constructors / Destructor */
    PI();
    PI( Reader& r );
    PI( const PI& o );
    virtual ~PI();

    /* Operator Overloads */
    const PI& operator=( const PI& o );
    bool operator==( const PI& o ) const;
    bool operator!=( const PI& o ) const;

    /* Accessors */
    ClassType getClassId() const;

    /* I/O */
    virtual void read( Reader& r );
    virtual void write( std::ostream& stream ) const;

private:

    std::string mPITarget;
    std::string mData;

#ifdef HAVE_LIBOSPACE
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const PI& - A reference to the object to write to  +
    //!param:    the stream.
    //         
    friend void ::os_write( os_bstream&, const PI& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: PI& - A reference to the object to read from the stream.
    //   
    friend void ::os_read( os_bstream&, PI& );
#endif // HAVE_LIBOSPACE
};


// ObjectSpace
#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::PI )
OS_STREAM_OPERATORS( ILwd::PI )

#endif // HAVE_LIBOSPACE

#endif // PIHH
