#ifndef StartTagHH
#define StartTagHH


//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

// System Includes
#include <string>
#include <vector>
#include <utility>
#include <exception>

// LDAS includes
#include <general/formatexception.hh>
#include <general/util.hh>
#include <general/regexmatch.hh>

// Local includes
#include "attributes.hh"
#include "base.hh"
#include "reader.hh"



/// \cond DOXYGEN_IGNORE
namespace ILwd {
    class StartTag;
}
/// \endcond

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::StartTag& base );
void os_read( os_bstream& stream, ILwd::StartTag& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: Implements an XML start-tag.
//
class ILwd::StartTag : public ILwd::Base
{
public:

    typedef std::map< std::string, AttributeValue, noCaseLess >::
    iterator att_iterator;
    typedef std::map< std::string, AttributeValue, noCaseLess >::
    const_iterator const_att_iterator;

    // Constructors / Destructor
    explicit StartTag( const std::string& name = "" );
    StartTag( const std::string& name, const Attributes& attributes );

    StartTag( const StartTag& tag );
    explicit StartTag( Reader& r );

    virtual ~StartTag();

    // Operator Overloads
    StartTag& operator=( const StartTag& tag );
    bool operator==( const StartTag& s ) const;
    bool operator!=( const StartTag& s ) const;
    
    // Accessors
    const std::string& getIdentifier() const;
    const Attributes& getAttributes() const;
    ClassType getClassId() const;

    // Stream
    void write( std::ostream& stream ) const;
    void read( Reader& r );
     
private:
   
    static const Regex &regex;
    std::string mName;
    Attributes mAttributes;
    
#ifdef HAVE_LIBOSPACE
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const StartTag& - A reference to the object to write to  +
    //!param:    the stream.
    //         
    friend void ::os_write( os_bstream&, const StartTag& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: StartTag& - A reference to the object to read from the stream.
    //   
    friend void ::os_read( os_bstream&, StartTag& );
#endif // HAVE_LIBOSPACE
};


//-----------------------------------------------------------------------------
//
//: Returns a reference to the attributes for this tag.
//
//!return: const Attributes& - A reference to the attributes.
//
inline const ILwd::Attributes& ILwd::StartTag::getAttributes() const
{
    return mAttributes;
}
 

std::ostream& operator<<( std::ostream& stream, const ILwd::StartTag& start );   

   
// ObjectSpace
#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::StartTag )
OS_STREAM_OPERATORS( ILwd::StartTag )

#endif // HAVE_LIBOSPACE

#endif // StartTagHH
