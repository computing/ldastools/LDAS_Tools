#include "ilwd/config.h"

#include "general/AtExit.hh"
#include "general/regexmatch.hh"

#include "attributes.hh"
#include "xml.hh"

using ILwd::Attributes;
using ILwd::AttributeValue;
using ILwd::Reader;

using namespace std;  

static const Regex& init_regex( const std::string& Exp );
   
const Regex &Attributes::regex = init_regex( std::string( "^" ) + XML::Attribute );
   
   
//-----------------------------------------------------------------------
// Default Constructor.  Creates an empty collection of attributes.
//-----------------------------------------------------------------------
ILwd::Attributes::
Attributes()
{
}
   
   
//-----------------------------------------------------------------------------
// Creates a list of attributes base upon a string.  The string must be
// formatted like an attribute list in an XML start-tag.
//-----------------------------------------------------------------------------
ILwd::Attributes::
Attributes( const char* a )
  : std::map< std::string, AttributeValue, ILwd::noCaseLess >()
{
  read( a );
}

ILwd::Attributes::
Attributes( const Attributes& att ) 
  : std::map< std::string, AttributeValue, ILwd::noCaseLess >( att )
{
}


ILwd::Attributes::
~Attributes()
{
}
   
ILwd::Attributes& ILwd::Attributes::
operator=( const Attributes& att )
{
  // Just copy the data.
  std::map< std::string, AttributeValue, ILwd::noCaseLess >::operator=( att );
  return *this;
}

//-----------------------------------------------------------------------
/// This compares this object to another PI for equality.
//-----------------------------------------------------------------------
bool ILwd::Attributes::
operator==( const Attributes& att ) const
{
  if ( att.size() != size() )
  {
    return false;
  }

  const_iterator iter1 = begin();
  const_iterator iter2 = att.begin();
    
  while( iter1 != end() )
  {
    if ( strcasecmp( iter1->first.c_str(), iter2->first.c_str() ) != 0 ||
	 iter1->second != iter2->second )
    {
      return false;
    }

    ++iter1;
    ++iter2;
  }

  return true;
}

bool ILwd::Attributes::
operator!=( const Attributes& att ) const
{
  return !operator==( att );
}
   

//-----------------------------------------------------------------------
// If the attribute already exists, then it is replaced by this attribute.
//-----------------------------------------------------------------------
void ILwd::Attributes::
addAttribute( const std::string& name, 
	      const AttributeValue& v ) 
{
  std::pair< std::string, AttributeValue > p( name, v );
  std::pair< iterator, bool > r = insert( p );
   
  // Attribute already exists, replace it's value
  if ( !r.second )
  {
    erase( r.first );
    insert( p );
  }
}

   
//-----------------------------------------------------------------------
/// Returns a pointer to an AttributeValue given the attribute name.
//-----------------------------------------------------------------------
AttributeValue* ILwd::Attributes::
getAttribute( const std::string& name )
{
  iterator iter = find( name );
  if ( iter == end() )
  {
    return 0;
  }
  else
  {
    return &((*iter).second);
  }
}
   

//-----------------------------------------------------------------------
/// Reads this object from a string.  During the read operation if an
/// attribute appears twice then ana exception is thrown.
//-----------------------------------------------------------------------
void ILwd::Attributes::
read( const char* a )
{

  // Erase all of the attributes presently stored
  erase( begin(), end() );
    
  // Datatype for an attribute, just the name and its value.
  std::pair< std::string, AttributeValue > attribute;

  // Initialize local variables
  RegexMatch rm( XML::MATCHES_ATTRIBUTE );  // The matched regex
  const char* end = a + strlen( a ); // Where the attribute string ends
  const char* index = a;             // The beginning of the current
  // attribute.

  // Match through the whole string.
  while( index < end )
  {
    // Try to match
    if ( !rm.match( regex, index ) )
    {
      string msg( "The attribute is not valid ILWD: " );
      msg += index;
            
      throw ILWD_FORMATEXCEPTION_INFO( Errors::INVALID_ATTRIBUTE, msg );
    }

    // Get the attribute name
    attribute.first = rm.getSubString( XML::M_ATTRIBUTE_NAME );

    // Get the attribute value
    if ( rm.getSubStart( XML::M_ATTRIBUTE_VALUE_QUOTE ) != 0 )
    {
      attribute.second.readString(
				  rm.getSubString( XML::M_ATTRIBUTE_VALUE_QUOTE ) );
    }
    else
    {
      attribute.second.readString(
				  rm.getSubString( XML::M_ATTRIBUTE_VALUE_APOSTROPHE ) );
    }
        
    // Now add the attribute into the collection
    std::pair< iterator, bool > inserted = insert( attribute );

    // Check if it was inserted
    if ( !inserted.second )
    {
      // If it was not inserted it is because an attribute with this
      // name already exists in the map.
      string msg( "An attribute appeared more than once: " );
      msg += attribute.first; 

      throw ILWD_FORMATEXCEPTION_INFO( Errors::DUPLICATE_ATTRIBUTE, msg );
    }

    // Set the starting location for the next regex match to the end of
    // this match.
    index = rm.getSubEnd( XML::M_ALL );
  }
   
  return;
}


//-----------------------------------------------------------------------
/// This writes the attributes as an XML list of attributes (like those
/// appearing in a start-tag).  An apostrophe is used as the attribute
/// value delimiter.
//-----------------------------------------------------------------------
void ILwd::Attributes::
write( std::ostream& stream ) const
{
  // Iterate through all of the attributes
  for ( const_iterator iter = begin();
	iter != end(); iter++ )
  {
    if ( iter != begin() )
    {
      stream << " ";
    }
        
    // just using a quote as a delimiter now.  must change.
    stream << iter->first << "='" << iter->second << '\'';
  }
}


//-----------------------------------------------------------------------
/// Write the attributes to a stream.  This just calls the write method.
//-----------------------------------------------------------------------
ostream&
operator<<( ostream& stream, const Attributes& att )
{
  att.write( stream );
  return stream;
}


static Regex* regex = (Regex*)NULL;
  
static void cleanup( )
{
  delete regex;
  regex = (Regex*)NULL;
}


static const Regex& init_regex( const std::string& Exp )
{
  regex = new Regex( Exp );
  General::AtExit::Append( cleanup,
			   "ilwd::regex_cleanup",
			   0);

  return *regex;
}

// ObjectSpace
#ifdef HAVE_LIBOSPACE

#include <ospace/source.h>
#include <ospace/stream.h>
#include <ospace/uss/std/map.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_0( (ILwd::Attributes*), Stream::ILwd::ATTRIBUTES )
  OS_STREAMABLE_0( (std::map< std::string, AttributeValue, ILwd::noCaseLess >*),
		   Stream::ILwd::AVMAP )

  void os_read( os_bstream& stream, Attributes& o )
{
  os_read( stream,
	   (std::map< std::string, AttributeValue, ILwd::noCaseLess >&)(o) );
}


void os_write( os_bstream& stream, const Attributes& o ) 
{
  os_write(
	   stream, (std::map< std::string, AttributeValue, ILwd::noCaseLess >&)(o) );
}

#endif // HAVE_LIBOSPACE
