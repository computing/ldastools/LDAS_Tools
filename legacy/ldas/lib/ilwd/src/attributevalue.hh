#ifndef AttributeValueHH
#define AttributeValueHH


#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE


// System includes
#include <exception>
#include <float.h>

#include <sstream>

// LDAS Includes
#include <general/formatexception.hh>
#include <general/types.hh>

// Local Includes
#include "reader.hh"
#include "errors.hh"

namespace ILwd
{
  class AttributeValue;
}


#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::AttributeValue& o );
void os_read( os_bstream& stream, ILwd::AttributeValue& o );
#endif /* HAVE_LIBOSPACE */

//-----------------------------------------------------------------------------
//
//: An attribute value.
//
// This class represents an attribute value.  This class stores the string
// representation of the value while also providing methods which will convert
// the string into other types (such as a real, int, or double).
//
// <p>This class
// also provides a mechanism for reading and writing the attribute value while
// performing substitutions on the string to make the string XML compliant
// (that is, replace '&lt;' with '&amp;amp;', '&lt;' with '&amp;lt;',
// '&gt;' with '&amp;gt;', '"' with '&amp;quot;', and ''' with '&amp;apos;' ).
// Reading is performed via <code>readString</code> (XML references are
// converted back intp characters) and writing is performed via
// <code>write</code> (illegal characters are converted into XML references).
//
// <p>The <code>setString</code> and <code>getString</code> methods do not
// perform any conversions.
//
//!todo: Maybe <code>write</code> should be renamed to <code>writeString</code>
//!todo: or <code>readString</code> should be renamed to <code>read</code> to
//!todo: maintain consistency in the method names.
//
class ILwd::AttributeValue
{
public:

  /* Constructors */
  AttributeValue();
  AttributeValue( const std::string& s );
  template< class T > AttributeValue( const T& v );
 
  /* Copy constructor */
  AttributeValue( const AttributeValue& av );
   
  /* Operator Overloads */
  const AttributeValue& operator=( const AttributeValue& av );
  bool operator==( const AttributeValue& av ) const;
  bool operator!=( const AttributeValue& av ) const;
    
  /* Accessors */
  const std::string& getString() const;
  template< class T > T getValue() const;

  /* Mutators */
  void setString( const std::string& v );
  void readString( const std::string& v );
  template< class T > void setValue( const T& v );
 
  /* I/O */
  void write( std::ostream& out ) const;
    
private:

  void replaceReferences();
    
  std::string mValue;

#ifdef HAVE_LIBOSPACE
  //: Write to the stream.
  //
  //!param: os_bstream& - A reference to the stream to write to.
  //!param: const AttributeValue& - A reference to the object to write to  +
  //!param:    the stream.
  //
  friend void ::os_write( os_bstream&, const AttributeValue& );
   
  //: Read from the stream.
  //
  //!param: os_bstream& - A reference to the stream to read from.
  //!param: const AttributeValue& - A reference to the object to read      + 
  //!param:    from the stream.
  //   
  friend void ::os_read( os_bstream&, AttributeValue& );
#endif // HAVE_LIBOSPACE    
};

    
//-----------------------------------------------------------------------------
//
//: Constructs the object with a given value.
//
//!param: const T& v - A reference to the template value representing an      +
//!param:    attribute value.   
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: FormatException - Illegal format was used.
//
template< class T >
ILwd::AttributeValue::AttributeValue( const T& v )
  try
    : mValue( "" )
{
  setValue< T >( v ); // May Throw
}
ADD_ILWD_FORMATEXCEPTION_INFO(
			      Errors::INVALID_ATTVALUE, "The attribute value is not valid ILWD." )
   
   
//-----------------------------------------------------------------------------
//
//: Returns a string representation of the value for the atttribute.
//
//!return: const std::string& -
//
  inline const std::string& ILwd::AttributeValue::getString() const
{
  return mValue;
}


//-----------------------------------------------------------------------------
//
//: Returns the value of the attribute.
//
// <p>The value is determined by reading
// the string representation of the value exactly the same way that the object
// would be read from a stream.
//
// <p>NOTE:  signed integers will be converted into unsigned integers if this
// is called with an unsigned integer template parameter.  That is, if the
// value is "-1234" and the user calls getAttributeValue<unsigned int>, the
// function will return 4294966062 (assuming 4 byte int's).  Similiarly, if
// a floating point number is 'read' with getAttributeValue<int>, the integer
// portion will be read ( 2.34 will be read as 2, -7.5 -> -7 ).  Octal and
// Hexadecimal is not recognized.  In general, the method will attempt to
// convert the value up to the first invalid character ( so "123rd st" -> 123
// if read as an int ).
//
// <p>NOTE:  All values are determined by reading them in via an extraction
// operator.  So anything which applies there applies here.
//
//!param: const AttributeValue& v - The attribute to get the value of.
//
//!return: T - The converted value.
//
//!exc: FormatException - +
//!exc:   BAD_CONVERSION: The value of the attribute was unable to be +
//!exc:       converted into the requested type.
//
template< class T >
T ILwd::AttributeValue::getValue() const
{
  // std::istringstream s( mValue.c_str(), mValue.size() );
  std::istringstream s( mValue );

  T temp;

  if ( ! ( s >> temp ) )
  {
    std::ostringstream msg;
    msg << "I was unable to convert ";
    write( msg );
    msg << " to the requested type.";
    const std::string tmp( msg.str() );
    std::cerr << tmp << std::endl;

    throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_CONVERSION, tmp.c_str() );
  }

  return temp;
}


//-----------------------------------------------------------------------------
//
//: Sets the value of the attribute to the given string.
//
//!param: const std::string& v
//
//!exc: std::bad_alloc - Memory allocation failed.
//
inline void ILwd::AttributeValue::setString( const std::string& v )
{ 
  mValue = v;
  return;
} 

   
//-----------------------------------------------------------------------------
//
//: Sets the value of the attribute.
//
//!param: const T& v
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: FormatException
//
template< class T >
void ILwd::AttributeValue::setValue( const T& v )
{
  std::ostringstream s;
  s << v;
  setString( s.str() ); // May throw
   
  return;
}


namespace ILwd
{
  // Template specializations   
  template<> void AttributeValue::setValue< REAL_4 >( const REAL_4& v );   
  template<> void AttributeValue::setValue< REAL_8 >( const REAL_8& v );
} // namespace - ILwd


namespace std
{
  //-----------------------------------------------------------------------------
  //
  //: Insertion operator.
  //
  //!param: ostream& stream
  //!param: const AttributeValue& e
  //
  //!return: ostream&
  //
  //!exc: exception - An unknown exception was thrown.
  //
  std::ostream& operator<<( std::ostream& stream,
	                    const ILwd::AttributeValue& e );
}


/*-------------------*/
/* ObjectSpace Block */
/*-------------------*/

#ifdef HAVE_LIBOSPACE

OS_CLASS( ILwd::AttributeValue )
  OS_STREAM_OPERATORS( ILwd::AttributeValue )

#endif // HAVE_LIBOSPACE

#endif // AttributeValueHH
