/* -*- mode: c++; c-basic-offset: 4; -*- */

#ifndef LdasElementHH
#define LdasElementHH


/// \author David Farnham

#include "ilwd/config.h"

// System Includes
#include <exception>
#include <list>
#include <string>
#include <iosfwd>   

// LDAS Includes
#include "general/bit_vector.hh"
#include "general/formatexception.hh"
#include "general/unordered_map.hh"
#include "general/regex.hh"
#include "general/refcount.hh"

// Local Includes
#include "element.hh"
#include "style.hh"
#include "reader.hh"

// ObjectSpace Includes
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

namespace General
{
    class GPSTime;
}


namespace ILwd {

    class LdasContainer;

    //-------------------------------------------------------------------
    // \brief The base class for all ILWD LDAS objects.
    //-------------------------------------------------------------------
    class LdasElement
	: public Element, public RefCount
    {
    public:
	static const char* const ATTR_STR_COMMENT;
	static const char* const ATTR_STR_JOBID;
	static const char* const ATTR_STR_METADATA;
	static const char* const ATTR_STR_NAME;
	static const char* const ATTR_STR_NULLMASK;

	/* Constructor / Detsructor */
	//---------------------------------------------------------------
	/// \brief Default constructor.
	///
	/// \param[in] name
	///     The name for the element.  Default: An empty string.
	/// \param[in] comment
	///     A comment.  Default: An empty string.
	//---------------------------------------------------------------
	explicit LdasElement( const std::string& name = "",
			      const std::string& comment = "" );

	//---------------------------------------------------------------
	/// \brief Copy Constructor.
	///
	/// \param e
	///     The element to copy from.
	//---------------------------------------------------------------
	LdasElement( const LdasElement& e );

	//---------------------------------------------------------------
	/// \brief Destructor.
	//---------------------------------------------------------------
	virtual ~LdasElement();

	//---------------------------------------------------------------
	/// \brief Assignment Operator.
	///
	/// \param[in] e
	///     The object to copy from.
	///
	/// \return
	///     A reference to this object.
	//---------------------------------------------------------------
	const LdasElement& operator=( const LdasElement& e );
	//---------------------------------------------------------------
	/// \brief Equal comparison operator.
	///
	/// \param[in] e
	///     The object to compare with.
	///
	/// \return
	///     True if the objects are equal, false otherwise.
	//---------------------------------------------------------------
	bool operator==( const LdasElement& e ) const;

	//---------------------------------------------------------------
	/// \brief Not equal comparison operator.
	///
	/// \param[in] e
	///     The object to compare with.
	///
	/// \return
	///     True if the containers are not equal.
	//---------------------------------------------------------------
	bool operator!=( const LdasElement& e ) const;

	//---------------------------------------------------------------
	/// \brief Add information that this object belong to another object.
	//---------------------------------------------------------------
	void addContainerRef( ILwd::LdasContainer* Container );
   
	//---------------------------------------------------------------
	/// \brief Remove reference to the parent ILWD format container.   
	///
	/// \param[in] Parent
	///     A pointer to the parent container to remove.
	//---------------------------------------------------------------
	void removeContainerRef( ILwd::LdasContainer* const Parent );

   
	/* Accessors */
	//---------------------------------------------------------------
	/// \brief Gets object identifier.
	///
	/// This is pure virtual method to introduce a function interface 
	/// into the base class, all derived classes will overload this 
	/// method.
	//---------------------------------------------------------------
	virtual const std::string& getIdentifier() const = 0;

	//---------------------------------------------------------------
	/// \brief Returns the jobid.
	///
	/// \return
	///     unsigned int - The Job Id.
	//---------------------------------------------------------------
	unsigned int getJobId(void) const;
	//---------------------------------------------------------------
	/// \brief Return one of the element's name fields.
	///
	/// \param i
	///     The name field to return.
	///
	/// \return
	///     The sub name of the object.
	//---------------------------------------------------------------
	std::string getName( size_t i ) const;
	//---------------------------------------------------------------
	/// \brief Get the name as an entire string.
	///
	/// \return
	///     The name of the ilwd object.
	//---------------------------------------------------------------
	const std::string getNameString() const;
	//---------------------------------------------------------------
	/// \brief Returns the number of name fields defined in the element.
	///
	/// \return
	///     The number of name fields.
	//---------------------------------------------------------------
	size_t getNameSize() const;

	//---------------------------------------------------------------
	/// \brief Returns the comment.
	///
	/// \return
	///     The comment.
	//---------------------------------------------------------------
	const std::string& getComment() const;

	//---------------------------------------------------------------
	/// \brief Get this element ID.
	///
	/// This is a pure virtual method that will be overloaded by all 
	/// derived classes.
	//---------------------------------------------------------------
	virtual ElementId getElementId() const = 0;

	//: Clears all metadata
	void clearMetadata( );

	//---------------------------------------------------------------
	/// \brief Gets a named metadata element
	///
	/// \param[in] Variable
	///     Name specifier of metadata
	/// \return
	///     Value assoicated with Variable
	///
	/// \exception std::range_error()
	///     Thrown if the variable does not exist
	//---------------------------------------------------------------
	template< class Type_ >
	Type_  getMetadata( const std::string& Variable ) const;

	//---------------------------------------------------------------
	/// \brief Create copy of the object. 
	///
	/// This is pure virtual method that will be overloaded by all 
	/// derived classes.
	//---------------------------------------------------------------
	virtual LdasElement* createCopy() const = 0;
    
	/* Mutators */
	//---------------------------------------------------------------
	/// \brief Sets the Job Id for the element.
	///
	/// \param[in] JobId
	///     The new value for the job id.
	//---------------------------------------------------------------
	void setJobId( unsigned int JobId );
	//---------------------------------------------------------------
	/// \brief Set one of the name fields.
	///
	/// \param[in] pos
	///     The location of the name to set..
	/// \param[in] name
	///     The new name.
	/// \param[in] FixedPosition
	///     True if the contents are to be over written,
	///     false otherwise.
	//---------------------------------------------------------------
	void setName( size_t pos,
		      const std::string& name,
		      bool FixedPosition = false );
	//---------------------------------------------------------------
	/// \brief Insert a name field somewhere in the name.
	///
	/// \param pos
	///     The location at which the name should be inserted.
	/// \param s
	///     The name to insert.
	//---------------------------------------------------------------
	void insertName( size_t pos, const std::string& s );
	//---------------------------------------------------------------
	/// \brief Delete one of the name fields.
	///
	/// \param[in] pos
	///     The location of the name to delete.  If this position
	///     does not refer to an actual name field in the element then
	///     nothing happens.
	//---------------------------------------------------------------
	void deleteName( size_t pos );
	//---------------------------------------------------------------
	/// \brief Append a name field to the element name.
	///
	/// \param s
	///     The name to append.
	//---------------------------------------------------------------
	void appendName( const std::string& s );
	//---------------------------------------------------------------
	/// \brief Resets the name of the object
	///
	/// \param[in] nameString
	///     The new name to be associated with the object.
	//---------------------------------------------------------------
	void setNameString( const std::string& nameString );

	//---------------------------------------------------------------
	/// \brief Associates a value to a named metadata
	///
	/// \param[in] Variable
	///     Metadata specifier
	/// \param[in] Value
	///     Value for metadata
	//---------------------------------------------------------------
	template< class Type_ >
	void setMetadata( const std::string& Variable, const Type_& Value );

	//---------------------------------------------------------------
	/// \brief Removes metadata from list of metadata
	///
	/// \param[in] Variable
	///     Metadata specifier
	//---------------------------------------------------------------
	void unsetMetadata( const std::string& Variable );

	//---------------------------------------------------------------
	/// \brief Sets a comment for the element.
	///
	/// \param[in] Value
	///     The new value for the comment.
	//---------------------------------------------------------------
	void setComment( const std::string& Value );

	/* I/O */
	//---------------------------------------------------------------
	/// \brief Writes an object to the stream with specified format
	///        and compression.
	///
	/// This is pure virtual method to introduce a function interface 
	/// into the base class, all derived classes will overload this 
	/// method.  
	//---------------------------------------------------------------
	virtual void write( std::ostream& stream, Format f,
			    Compression c = USER_COMPRESSION ) const = 0;
	//---------------------------------------------------------------
	/// \brief Writes an object to the stream.
	///
	/// \param stream
	///     A reference to the stream to write to. 
	///
	/// \exception std::exception 
	//---------------------------------------------------------------
	virtual void write( std::ostream& stream ) const;

	//---------------------------------------------------------------
	/// \brief Create an LdasElement from a Reader.
	///
	/// \param[in] r
	///     The reader to read from.
	///
	/// \return
	///     A pointer to the newly instantiated element.
	///
	/// \exception std::bad_alloc
	///     Memory allocation failed.
	/// \exception StreamException
	///     An error occurred while reading from the stream.
	/// \exception FormatException
	///     The format for the LdasElement was invalid.
	//---------------------------------------------------------------
	static LdasElement* createElement( Reader& r );
	//---------------------------------------------------------------
	/// \brief Create an LdasElement from a stream.
	///
	/// \param[in] stream
	///     The stream to read from.
	/// \param[in] st
	///     The start-tag for the element.
	///
	/// \return
	///     The element read.
	///
	/// \exception std::bad_alloc
	///     Memory allocation failed.
	/// \exception StreamException
	///     An error occurred while reading from the stream.
	/// \exception FormatException
	///     The format for the LdasElement was invalid.
	//---------------------------------------------------------------
	static LdasElement* createElement( Reader& stream, const StartTag& st );

	/* Attribute manipulation */
	//---------------------------------------------------------------
	/// \brief Write attributes.
	///
	/// \param[out] att
	///     The attributes object to fill.
	//---------------------------------------------------------------
	void writeAttributes( Attributes& att ) const;
	//---------------------------------------------------------------
	/// \brief Read & set the attributes for this object.
	///
	/// \param[in] att
	///     Attributes from which to derive local characteristics
	///
	/// \return
	///     The format and compression parameters which were read
	///     from the attributes object.
	//---------------------------------------------------------------
	void readAttributes( const Attributes& att );

	/* Support of NULL mask */
	//---------------------------------------------------------------
	/// \brief Get the bitmask.
	///
	/// \return
	///     Current value of bitmask
	//---------------------------------------------------------------
	const General::bit_vector& getNullMask( );
	//---------------------------------------------------------------
	/// \brief Test if a field is or is not null.
	///
	/// \param[in] Offset
	///     Position to test
	///
	/// \return
	///     True if the field is null, false otherwise.
	//---------------------------------------------------------------
	bool isNull(size_t Offset) const;
	//---------------------------------------------------------------
	/// \brief Set or unset the bit of the bitmask.
	///
	/// \param[in] Offset
	///     Bit to set or unset
	/// \param[in] Value
	///     True to set the bit, false otherwise.
	//---------------------------------------------------------------
	void setNull(size_t Offset, bool Value);
	//---------------------------------------------------------------
	/// \brief Set or unset the bit of the bitmask.
	///
	/// \param Mask64
	///     String representing the bit mask.
	//---------------------------------------------------------------
	void setNull(std::string Mask64);
	//---------------------------------------------------------------
	/// \brief Set the bitmask.
	///
	/// \param[in] BitMask
	///     new value for bitmask
	//---------------------------------------------------------------
	void setNull(const General::bit_vector& BitMask);
    
    private:
	typedef General::unordered_map<std::string, std::string> metadata_type;

	/* Support for metadata attribute */
	//---------------------------------------------------------------
	/// \brief Converts a string into metadata_type
	///
	/// \param[in] Metadata
	///     The string representation of the metadata.
	//---------------------------------------------------------------
	void convert_string_to_metadata( const std::string& Metadata );
	//---------------------------------------------------------------
	/// \brief Converts metadata_type into a string
	///
	/// \return
	///     The string representation of the metadata.
	//---------------------------------------------------------------
	std::string convert_metadata_to_string( ) const;

	/* Support of single dimension nullmask */
	//---------------------------------------------------------------
	/// \brief Converts a string to a null mask
	///
	/// \param[in] Mask
	///     The string representing the null mask.
	//---------------------------------------------------------------
	void convert_string_to_nullmask(const std::string& Mask);
	//---------------------------------------------------------------
	/// \brief Generate string representation of the null mask
	///
	/// \return
	///      The string representation of the null mask information.
	//---------------------------------------------------------------
	std::string convert_nullmask_to_string(void) const;

	General::bit_vector	m_null_mask;
    
	/* Job Id information */
	static const unsigned int DFLT_JOBID;
	unsigned int m_jobid;

	std::vector< std::string > mNameVector;
	std::string mComment;

	metadata_type	m_metadata;

	std::list< ILwd::LdasContainer* > m_container_list;

#ifdef HAVE_LIBOSPACE
	//: Write to the stream.
	//
	//!param: os_bstream& - A reference to the stream to write to.
	//!param: const LdasElement& - A reference to the object to write to  +
	//!param:    the stream.
	//  
	friend void ospaceWrite( os_bstream&, const LdasElement& );
   
	//: Read from the stream.
	//
	//!param: os_bstream& - A reference to the stream to read from.
	//!param: const LdasElement& - A reference to the object to read      + 
	//!param:    from the stream.
	//   
	friend void ospaceRead( os_bstream&, LdasElement& );
#endif // HAVE_LIBOSPACE
    };



    /* Inline Methods */


    inline const std::string& LdasElement::
    getComment() const
    {
	return mComment;
    }


    inline unsigned int LdasElement::
    getJobId(void) const
    {
	return m_jobid;
    }

    inline void LdasElement::
    setComment( const std::string& Value )
    {
	mComment = Value;
	return;
    }
 
    inline void LdasElement::
    setJobId( const unsigned int JobId )
    {
	m_jobid = JobId;
	return;
    }
 
    inline const General::bit_vector&  LdasElement::
    getNullMask( )
    {
	return m_null_mask;
    }

    inline void LdasElement::
    setNull(const General::bit_vector& BitMask)
    {
	m_null_mask = BitMask;
	return;
    }

    //-------------------------------------------------------------------
    /// This compares this object to another LdasContainer for inequality.
    //-------------------------------------------------------------------
    inline bool ILwd::LdasElement::
    operator!=( const LdasElement& Element ) const
    {
	return !operator==( Element );
    }

} // namespace


namespace ILwd
{
    template<>
    std::string LdasElement::
    getMetadata<std::string>( const std::string& Variable ) const;

    template<>
    General::GPSTime LdasElement::
    getMetadata<General::GPSTime>( const std::string& Variable ) const;


    template<>
    void LdasElement::
    setMetadata<std::string>( const std::string& Variable,
			      const std::string& Value );
    template<>
    void LdasElement::
    setMetadata<General::GPSTime>( const std::string& Variable,
				   const General::GPSTime& Value );
} // namespace - ILwd

#include "ldaselement.icc"


//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------

#ifdef HAVE_LIBOSPACE

void os_write( os_bstream&, const ILwd::LdasElement& );
void os_read( os_bstream&, ILwd::LdasElement& );

OS_POLY_CLASS( ILwd::LdasElement )
    OS_STREAM_OPERATORS( ILwd::LdasElement )

#endif // HAVE_LIBOSPACE

#endif // LdasElementHH
