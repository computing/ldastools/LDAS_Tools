#include "ilwd/config.h"

#include <stdlib.h>
#include <stdarg.h>

#include <vector>

#include "ldaselement.hh"
#include "ldascontainer.hh"
#include "LdasHistory.hh"

///-----------------------------------------------------------------------
/// LdasHistoryRecord - public
///-----------------------------------------------------------------------
//!param: const std::string& Description - Text describing the action that
//+             was performed
ILwd::LdasHistoryRecord::
LdasHistoryRecord(const std::string& Description)
  : m_owned(false),
    m_record((ILwd::LdasContainer*)NULL)
{
  if ((m_record = new ILwd::LdasContainer(Description)))
  {
    m_owned = true;
  }
}

//!param: const std::string& Description - Text describing the action that
//+             was performed
//!param: const std::vector<const ILwd::LdasContainer*>& Args - Vector of
//+             arguments sent to the action that was performed.
ILwd::LdasHistoryRecord::
LdasHistoryRecord(const std::string& Description,
		  const std::vector<const ILwd::LdasElement*>& Args)
  : m_owned(false),
    m_record((const ILwd::LdasContainer*)NULL)
{
  ILwd::LdasContainer*	writable;

  if ((writable = new ILwd::LdasContainer(Description)))
  {
    for (std::vector<const ILwd::LdasElement*>::const_iterator
	   a = Args.begin();
 	 a != Args.end();
	 a++)
    {
      writable->push_back(*a);
    }
    m_owned = true;
    m_record = writable;
  }
}

ILwd::LdasHistoryRecord::
LdasHistoryRecord(const ILwd::LdasContainer* Record)
  : m_owned(false),
    m_record(Record)
{
}

ILwd::LdasHistoryRecord::
LdasHistoryRecord(const ILwd::LdasHistoryRecord& Record)
  : m_owned(true)
{
  if ( ! Record.m_record )
  {
    throw std::range_error( "LdasHistoryRecord references a NULL pointer" );
  }
  m_record = new ILwd::LdasContainer( *Record.m_record );
}

ILwd::LdasHistoryRecord::
~LdasHistoryRecord( )
{
  if ( m_owned )
  {
    delete m_record;
    m_record = (ILwd::LdasContainer*)NULL;
  }
}

bool ILwd::LdasHistoryRecord::
IsHistoryRecord(const ILwd::LdasElement* Element)
{
  const ILwd::LdasContainer* history_record =
    dynamic_cast<const ILwd::LdasContainer*>(Element);
  //---------------------------------------------------------------------
  // Test outer most container
  //---------------------------------------------------------------------
  if (!history_record)
  {
    return false;
  }
  return true;
}
   
   
// Retrieve the pointer to the history record.
//!param: bool RelinquishOwnership - true if ownership of the object
//+       should be given up; false otherwise
ILwd::LdasContainer* ILwd::LdasHistoryRecord::
GetHistoryRecordAsIlwd(bool RelinquishOwnership)
{
  m_owned = !(RelinquishOwnership);
  return const_cast<ILwd::LdasContainer*>(m_record);
}
   

//-----------------------------------------------------------------------
// LdasHistory - public
//-----------------------------------------------------------------------

// Create a new LdasHistory class suitable for starting a history record.
ILwd::LdasHistory::
LdasHistory()
try
: m_history((ILwd::LdasContainer*)NULL),
  m_own_history(false)
{
  m_history = new ILwd::LdasContainer(TAG);
  m_own_history = true;
}
catch(...)
{
  delete m_history;
  m_history = (LdasContainer*)NULL;
  m_own_history = false;
  throw;
}

// Create a new LdasHistory class suitable for reading and/or extending
//   history data originating from an ilwd.
ILwd::LdasHistory::
LdasHistory(ILwd::LdasContainer* History)
try
: m_history((ILwd::LdasContainer*)NULL),
  m_own_history(false)
{
  if (!History)
  {
    throw std::range_error( "LdasHistory cannot be initialized with a NULL pointer" );
  }
  m_history = History;
  init();
}
catch(...)
{
  m_history = (ILwd::LdasContainer*)NULL;
  throw;
}

// Create a new LdasHistory class suitable for reading and/or extending
//   history data originating from an ilwd.
ILwd::LdasHistory::
LdasHistory(const ILwd::LdasContainer* History)
try
: m_own_history(true)
{
  if ( History && IsHistoryContainer( History ) )
  {
    m_history = new ILwd::LdasContainer( *(History) );
    init();
  }
  else
  {
    m_history = (ILwd::LdasContainer*)NULL;
    m_own_history = false;
    // Don't think it's appropriate to call init() when m_history is null
  }
}
catch(...)
{
  m_history = (ILwd::LdasContainer*)NULL;
  throw;
}

ILwd::LdasHistory::
LdasHistory(const ILwd::LdasHistory& History)
try
: m_own_history(true)
{
  m_history = new ILwd::LdasContainer( TAG );
  for ( history_record_list_type::const_iterator i = History.m_history_records.begin();
	i != History.m_history_records.end();
	i++ )
  {
    this->AppendRecord( *i );
  }
}
catch(...)
{
  if (m_history)
  {
    delete m_history;
  }
  m_history = (ILwd::LdasContainer*)NULL;
  throw;
}

//-----------------------------------------------------------------------
// Release resource back to the system.
//-----------------------------------------------------------------------
ILwd::LdasHistory::
~LdasHistory(void)
{
  if (m_own_history && m_history)
  {
    delete m_history;
    m_history = (ILwd::LdasContainer*)NULL;
  }
}

   
// This exception is thrown when a non-history container was passed
//   where a history container was expected.
ILwd::LdasHistory::NonHistoryContainer::
NonHistoryContainer()
  : std::runtime_error("Container is not a History container")
{}


// Retrieve a read only version of the ilwd container which contains
//   the history information.
//!return: const ILwd::LdasContainer& - A constant referance to the
//+        LdasContainer use for storing the history information.
const ILwd::LdasContainer& ILwd::LdasHistory::
GetHistoryAsILwd() const
{
  if (m_history)
  {
    return *m_history;
  }
  throw std::bad_cast();
}   
   
   
//-----------------------------------------------------------------------
// Add a group of history records
//-----------------------------------------------------------------------
void ILwd::LdasHistory::
Append( const ILwd::LdasHistory& History )
{
  if ( this == &History )
  {
    throw std::logic_error( "LdasHistory: Self append" );
  }
  for ( history_record_list_type::const_iterator h =
	  History.m_history_records.begin();
	h != History.m_history_records.end();
	h++ )
  {
    AppendRecord( *h );
  }
}

//-----------------------------------------------------------------------
// Add a history record to the current list of records.
//-----------------------------------------------------------------------

void ILwd::LdasHistory::
AppendRecord(const std::string& Description,
	     const ILwd::LdasElement* Element, ... )
{
  va_list				ap;
  std::vector<const ILwd::LdasElement*>	args;

  va_start( ap, Element );
  while( Element )
  {
    args.push_back( Element );
    Element = va_arg(ap, const ILwd::LdasElement*);
  }
  va_end( ap );

  // Create History Record
  m_history_records.push_back(ILwd::LdasHistoryRecord(Description, args));
  m_history->push_back( m_history_records.back().GetHistoryRecordAsIlwd(true),
			LdasContainer::NO_ALLOCATE,
			LdasContainer::OWN );
}

void ILwd::LdasHistory::
AppendRecord( const ILwd::LdasHistoryRecord& Record )
{
  m_history_records.push_back( Record );
  m_history->push_back( m_history_records.back().GetHistoryRecordAsIlwd(true),
		        LdasContainer::NO_ALLOCATE,
			LdasContainer::OWN );
}

bool ILwd::LdasHistory::
IsHistoryContainer(const ILwd::LdasElement* Element)
{
  const ILwd::LdasContainer* history =
    dynamic_cast<const ILwd::LdasContainer*>(Element);
  //---------------------------------------------------------------------
  // See if this container is a "History" container
  //---------------------------------------------------------------------
  if ((!history) || (history->getNameString() != TAG))
  {
    return false;
  }
  //---------------------------------------------------------------------
  // Test each element
  //---------------------------------------------------------------------
  for (ILwd::LdasContainer::const_iterator r = history->begin();
       r != history->end();
       r++)
  {
    if (!ILwd::LdasHistoryRecord::IsHistoryRecord(*r))
    {
      return false;
    }
  }
  return true;
}

//: Assignment opperator
ILwd::LdasHistory& ILwd::LdasHistory::
operator=(const ILwd::LdasHistory& History)
{
  if (this != &History)
  {
    if (m_own_history && (m_history != 0))
    {
      delete m_history;
    }

    if (History.m_history != 0)
    {
      m_history = new ILwd::LdasContainer(*(History.m_history));
      m_own_history = true;
    }
    else
    {
      m_history = 0;
      m_own_history = false;
    }
  }

  return *this;
}

//-----------------------------------------------------------------------
// LdasHistory - private
//-----------------------------------------------------------------------

const std::string ILwd::LdasHistory::TAG("LDAS_History");

void ILwd::LdasHistory::
init(void)
{
  //---------------------------------------------------------------------
  // See if this container is a "History" container
  //---------------------------------------------------------------------
  if (!IsHistoryContainer(m_history))
  {
    throw NonHistoryContainer();
  }
  //---------------------------------------------------------------------
  // Put in each record of history information
  //---------------------------------------------------------------------
  for (ILwd::LdasContainer::const_iterator r = m_history->begin();
       r != m_history->end();
       r++)
  {
    const ILwd::LdasContainer*	c;
    if ((c = dynamic_cast<const ILwd::LdasContainer*>(*r)))
    {
      m_history_records.push_back(ILwd::LdasHistoryRecord(c));
    }
  }
}
