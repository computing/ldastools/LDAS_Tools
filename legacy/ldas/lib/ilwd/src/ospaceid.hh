#ifndef ILwdOSpaceIdHH
#define ILwdOSpaceIdHH

#include <general/ospaceid.hh>

namespace Stream
{
    namespace ILwd
    {
        enum
        {
            BASE            = OS_ILWD,
            ATTRIBUTES,
            ATTRIBUTEVALUE,
            AVMAP,
            CDATA,
            CHARDATA,
            COMMENT,
            ELEMENT,
            ENDTAG,
            GENERICELEMENT,
            GEVECT,
            LDASCONTAINER,
            LDASCONTAINERVECT,
            LDASELEMENT,
            LDASFORMATELEMENT,
            LDASELEMENTNAMEVECTOR,
            LDASSTRING,
            LDASEXTERNAL,
            LE_INT_8S,
            LE_INT_8U,
            LE_INT_4S,
            LE_INT_4U,
            LE_INT_2S,
            LE_INT_2U,
            LE_CHAR,
            LE_CHAR_U,
            LE_REAL_4,
            LE_REAL_8,
            LE_COMPLEX_8,
            LE_COMPLEX_16,
            LDASARRAY,
            LDASARRAYBASE,
            PI,
            STARTTAG,
            STYLE,
            ILWD_END
        };
    }
}


#endif // ILwdOSpaceIdHH
