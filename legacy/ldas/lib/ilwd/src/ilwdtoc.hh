#ifndef ILWD__TOC_HH
#define ILWD__TOC_HH

#include "general/unordered_map.hh"

#include "ilwd/ldasarray.hh"

namespace ILwd
{
  class LdasElement;
  class LdasContainer;

  //---------------------------------------------------------------------
  /// \brief Class to provide Table of Contents access to LdasContainers
  ///
  /// This is a helper class to extend the functionality of the LdasContainer
  /// class. By providing a Table of Contents, random access can be
  /// performed on the LdasContainer with minimal overhead.
  //---------------------------------------------------------------------
  class TOC
  {
  public:
    //-------------------------------------------------------------------
    /// \brief Type definition for the key
    //-------------------------------------------------------------------
    typedef unsigned int key_type;

    //-------------------------------------------------------------------
    /// \brief Type definition for Key pairs
    //-------------------------------------------------------------------
    typedef General::unordered_map<std::string,key_type> Keys_type;

    //-------------------------------------------------------------------
    /// \brief Type definition for reverse lookup Key pairs
    //-------------------------------------------------------------------
    typedef General::unordered_map<key_type,std::string> RKeys_type;

    //-------------------------------------------------------------------
    /// \brief Constructor
    ///
    /// \param Source
    ///     Reference to the LdasContainer for which to generate TOC
    /// \param Keys
    ///     List of keys. The string representation of the keys is
    ///     searched for in the Source. The numeric representation of
    ///     the keys is used in the access methods.
    //-------------------------------------------------------------------
    TOC( const ILwd::LdasContainer& Source, const Keys_type& Keys );

    //-------------------------------------------------------------------
    /// \brief Constructor
    ///
    /// \param Source
    ///     Reference to the LdasContainer for which to generate TOC
    /// \param Keys
    ///     List of keys. The string representation of the keys is
    ///     searched for in the Source. The numeric representation of
    ///     the keys is used in the access methods.
    //-------------------------------------------------------------------
    TOC( const ILwd::LdasElement& Source, const Keys_type& Keys );

    //-------------------------------------------------------------------
    /// \brief Copy Constructor
    //-------------------------------------------------------------------
    TOC( const TOC& Source );

    //-------------------------------------------------------------------
    /// \brief Destructor
    //-------------------------------------------------------------------
    virtual ~TOC( );

    static RKeys_type GenReverseLookupMap( const Keys_type& Keys );

    const ILwd::LdasContainer& GetContainer( ) const;

    template<class Type_>
    Type_
    GetData( key_type Key ) const;

    template<class Type_>
    Type_
    GetData( key_type Key, const Type_& Default ) const;

    template<class Type_>
    Type_&
    GetILwd( key_type Key );

    template<class Type_>
    const Type_&
    GetILwd( key_type Key ) const;

    template<class Type_>
    void
    SetData( key_type Key, const Type_& Value ) const;

    const std::string& ReverseLookup( key_type Key ) const;

  private:
    struct private_data;

    //-------------------------------------------------------------------
    /// \brief All the private data for the class
    //-------------------------------------------------------------------
    private_data&	m_private;
  };
}

#endif /* ILWD__TOC_HH */
