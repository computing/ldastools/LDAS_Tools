#include "ilwd/config.h"

#include <cassert>
#include <cstdio>
#include <cstring>

#include "reader.hh"
#include "errors.hh"


using ILwd::Reader;
using namespace std;

   
//-----------------------------------------------------------------------------
//
//: Creates a Reader object from an istream.
//
//!param: istream& in - The istream to read from.
//
Reader::Reader( std::istream& in )
   : mStream( &in ),
     mBuffer( new std::stringstream() )
{
}


//-----------------------------------------------------------------------------
//
//: Copy constructor.
//
//!param: const Reader& r -
//
Reader::Reader( const Reader& r )
   : mStream( r.mStream ), mBuffer( new std::stringstream() )
{
   mBuffer->str( r.mBuffer->str() );
}


//-----------------------------------------------------------------------------
//
//: Destructor.
//
Reader::~Reader()
{
   delete mBuffer;
   mBuffer = 0;
}


//********************/
/* Operator overloads */
//********************/


//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const Reader& r - The object to assign from.
//
//!return: const Reader& - A reference to this object.
//
//!except: bad_alloc - Memory allocation failed.
//
const Reader& Reader::operator=( const Reader& r )
{
   if ( this != &r )
   {
      mStream = r.mStream;
   
      delete mBuffer;
      mBuffer = new std::stringstream();
      mBuffer->str( r.mBuffer->str() );
   }
   
   return *this;
}
   
   
//******************/
/* XML Read Methods */
//******************/
   
//-----------------------------------------------------------------------
/// \brief Reads an Eq (XML 1.0) from the stream.  
///
/// \return
///     True if an equal symbol was read.
///     False if an equal symbol was not found or if the stream is
///     not in a good state ( is.good() == false ).  All white-space
///     up to the non-equal symbol (or until the stream is no longer
///     good) will be extracted.  
//-----------------------------------------------------------------------
bool Reader::readEq()
{
    skipWhiteSpace();
    if ( !readChar( '=' ) )
    {
        return false;
    }
    skipWhiteSpace();
    
    return true;
}


//-----------------------------------------------------------------------------
//
//: readName
//
// Reads a Name (XML 1.0) from the stream.  This stops at the first
// non-namechar.
//
//!return: std::string -
//
//!except: FormatException -
//!except: StreamException -
//
std::string Reader::readName()
try
{
    // Check the first character.  This could throw an exception.
    int p = peek();     // May throw StreamException    
    char c;             // temporary storage for a character
    string name;        // String to hold the name

    // Check for a valid initial character
    if ( ( p != '_' ) && ( p != ':' ) && ( !isLetter( p ) ) )
    {
        // invalid, throw an exception
        string msg( "Reader::readName(): '" );
        msg += char(p);
        
        throw ILWD_FORMATEXCEPTION_INFO( Errors::INVALID_CHAR, msg );
    }

    // Read the character and add it to the name (we know the stream is good,
    // since it got past the peek).
    c = get();
    name += c;

    // Check the next character & stream state, could throw an exception.
    p = peek();
    
    // loop while namechar's are being read from the stream.  Stop on an
    // EOF or other non-namechar
    while ( ( p != EOF ) && ( isNameChar( p ) ) )
    {
        c = get();
        name += c;
        p = peek(); // could throw
    }

    return name;
}
ADD_ILWD_FORMATEXCEPTION_INFO(
    Errors::INVALID_NAME, "The name was not valid ILWD." )


//**********************/
/* Non-XML read methods */
//**********************/

//-----------------------------------------------------------------------------
//
//: Read the specified character from the stream.
//
// This method checks to see if
// the specified character is the next one on the stream.  If it is, it is
// extracted and the method returns true, otherwise it is left on the stream
// and the method returns false.
//
//!param: char* c - The character to be read.
//
//!return:    bool - True if the character was extracted.
//!return:           False if the character was not extracted or the stream
//!return:           is not 'good' (or if the character was an EOF).
//
bool Reader::readChar( char c )
try
{
    // This may throw an exception if the stream is not good
    int p = peek();
    
    if ( p == EOF || p != c ) 
    {
        return false;
    }
    else
    {
        get();
        return true;
    }
}
catch( const StreamException& )
{
    // just return false on error
    return false;
}


//-----------------------------------------------------------------------------
//
//: Read a character if it is in the provided character list.
//
// If the next
// character on the stream is not in the list then it is not extracted.
//
//!param: char* c - A c-string containing a list of characters which can be
//!param:         read.  If this is null then nothing is extracted.
//!param: char* r - A pointer to the character which was extracted (if 'r'
//!param:         is not null).  If a character was not extracted (or EOF is
//!param:         found) then this is unchanged.
//
//!return:    bool - True if the next character was in the list and extracted.
//!return:           False if the next character was not in the list (and
//!return:           therefore not extracted), if the stream state is not
//!return:           'good', or if the container pointer was null.
//
bool Reader::readChar( char* c, char* r )
try
{
    // Make sure the list of characters is non-null
    if ( c == 0 )
    {
        return false;
    }
    
    // This will throw an exception if the stream is not good
    int p = peek();

    // Check the stream & character
    if ( p == EOF )
    {
        get();
        return false;
    }
    
    bool found = false;  // flag indicating if we have found the character
    int index = 0;       // index into the std::string

    // See if the character is in the list
    while ( !found && ( c[index] != '\0' ) )
    {
        found = ( p == c[index] );
        index++;
    }
    
    if ( found )
    {
        if ( r != 0 )
        {
            *r = get();
        }
        
        return true;
    }

    return false;
}
catch( const StreamException& )
{
    return false;
}


//-----------------------------------------------------------------------------
//
//: readCharNot
//
// Read a character if it is not in the provided list.  If the next character
// on the stream is in the list then it is not extracted.
//
//!param: char* c - A c-std::string containing a list of characters which can't
//!param:           be read.  If this is a null-pointer then the method always
//!param:                returns false.
//!param: char* r - A pointer to the location to store the extracted
//!param:           character (if 'r' is not null).  If a character is not
//!param:           extracted, then this is unchanged.
//
//!return:    bool - True if the next character was not in the list and was
//!return:           therefore extracted.
//!return:           False if the next character was not in the list, the
//!return:           stream is not 'good', the character was an EOF (the EOF
//!return:           is extracted), or the character list pointer was null.
//
bool Reader::readCharNot( char* c, char* r )
try
{
    if ( c == 0 )
    {
        return false;
    }

    // This will throw an exception if the stream is not good.
    int p = peek();

    // Always extract the EOF
    if ( p == EOF )
    {
        get();
        return false;
    }
    
    bool found = false;  // flag indicating if we have found the character
    int index = 0;       // index into the std::string

    // check to see if this character is in the list
    while ( ( !found ) && ( c[index] != '\0' ) )
    {
        found = ( p == c[index] );
        index++;
    }
    
    if ( !found )
    {
        if ( r != 0 )
        {
            *r = get();
        }
        
        return true;
    }
    else
    {
        return false;
    }
}
catch( const StreamException& )
{
    // Just return false on exceptions
    return false;
}



//-----------------------------------------------------------------------------
//
//: Reads the specified std::string from the stream.
//
// This attempts to read the given
// string from the stream, returning whether or not the string was found.
//
//!param: const char* s - A pointer to the dtring to read.  If this is null,
//!param:     then nothing happens and the method returns true.
//
//!return: bool - True if the string was read from the stream, or if 's' was
//!return:        a null pointer.
//!return:      - False if the string was not read or if the stream is not
//!return:        good.  Any extracted characters are pushed back into the
//!return:        reader.
//
bool Reader::readString( const char* s )
{
    // Make sure 's' is not null.
    if ( s == 0 )
    {
        return true;
    }
    
    size_t l = strlen( s ); // string size
    int c;                  // temporary storage for 'get' result
    size_t n = 0;           // The number of characters successfully read
    bool found( true );     // Flag indicating if the string is read
    
    // loop through all of the characters in the name
    for ( size_t i=0; ( i < l ) && ( found ); i++ )
    {
        try
        {
            c = peek(); // could throw
            if ( (c == EOF) || (c != s[i]) )
            {
                found = false;
            }
            else
            {
                c = get();
                ++n;
            }
        }
        catch( StreamException& )
        {
            // just ignore the exception and return false
            found = false;
        }
    }

    if ( !found )
    {
        // We need to restore the proper stream state.
        pushback( s, n );
        return false;
    }
    
    return true;
}


//-----------------------------------------------------------------------------
//
//: skipWhiteSpace
//
// kips whitespace.  Whitespace consists of one or more space characters,
// carriage returns, line feeds, or tabs (XML 1.0).  The method eats the
// whitespace until a non-whitespace character is encountered or eof is
// reached.  EOF is not eaten.
//
//!return: std::string - The eaten white space.
//
std::string Reader::skipWhiteSpace()
{
    ostringstream ss; // temporary to store eaten whitespace

    try
    {
        // This may throw an exception
        int c = peek();
        while( ( c != EOF ) &&
               ( ( c == 0x20 ) || ( c == 0x09 ) ||
                 ( c == 0x0D ) || ( c == 0x0A ) )
               )
        {
            // This may throw an exception
            c = get();
            ss << static_cast< char >( c );
            c = peek();
        }
    }
    catch( std::bad_alloc& )
    {
        // re-throw a bad-alloc
        throw;
    }
    catch( ... )
    {
        // don't do anything, just leave
    };


    return ss.str();
}


//-----------------------------------------------------------------------------
//
//: Get the next character from the stream.
//
// *** Does getting an EOF set eof to true?
//
//!return: int - the code for the next character (could be EOF).
//
//!except: StreamException -
//
int Reader::get()
{
    int t;

    // First check to see if there is anything in the pushback buffer
    if ( !mBuffer->good() )
    {
        // No, so just get something from the istream.
        t = mStream->get();
        // Check the state of the stream to make sure we got something good.
        checkAndThrow();
	return t;
    }
    // this should never fail, since we already checked to see if there was
    // something to get
    t = mBuffer->get();
    if ( !mBuffer->good() ) t = get();
    return t;
}


//-----------------------------------------------------------------------------
//
//: Check what the next character is on the stream.
//
// The character is not extracted.
//
//!return: int - the code of the next character (could be EOF).
//
//!except: StreamException -
//
int Reader::peek()
{
    int t;
    // First check to see if there is anything in the pushback buffer
    if ( !mBuffer->good() || ( mBuffer->peek() == EOF ) )
    {
        // No, so just peek at the istream
        t = mStream->peek();
        // Make sure no failures occurred
        checkAndThrow();
        return t;
    }
    t = mBuffer->peek();
    if ( !mBuffer->good() )
    {
      t = peek();
    }
    // this should never fail
    return t;
}


//-----------------------------------------------------------------------------
//
//: Read into a buffer.
//
//!param: const void* buffer - The address of the buffer to read into.  If
//!param:   this is a null pointer then the bytes are discarded (same as
//!param:   calling skip).
//!param: size_t s - The number of bytes to read.
//
//!except: StreamException -
//
void Reader::read( void* buffer, size_t s ) 
{
    if ( buffer == 0 )
    {
        skip( s );
        return;
    }
    
    size_t pbsize( mBuffer->str().size() );
    
    if ( s > pbsize )
    {
        if ( pbsize > 0 )
        {
            mBuffer->read( (char*)buffer, pbsize );
        }

#if 1
        // What happens if this fails?
        mStream->read( reinterpret_cast< char* >( buffer )+ pbsize,
                       s - pbsize );
        checkAndThrow();
#else
	char*		bpos = reinterpret_cast<char*>(buffer);
	size_t		left = s - pbsize;
	const size_t	inc(32 * 1024);

	while (left > 0)
	{
	  size_t	read_size = (left > inc) ? inc : left;
	  // What happens if this fails?
	  mStream->read( bpos, read_size);
	  checkAndThrow();
	  left -= read_size;
	  bpos += read_size;
	}
#endif
    }
    else
    {
        mBuffer->read( (char*)buffer, s );
    }
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Skips characters.
//
//!param: size_t s - The number of characters to skip.
//
//!except: StreamException -
//
void Reader::skip( size_t s )
{
    size_t pbsize( mBuffer->str().size() );

    if ( s > pbsize )
    {
        if ( pbsize > 0 )
        {
            mBuffer->seekg( pbsize );
        }

        mStream->seekg( s - pbsize );
        checkAndThrow();
    }
    else
    {
        mBuffer->seekg( s );
    }
   
    return;
}


//***************/
/* Stream Status */
//***************/


//-----------------------------------------------------------------------------
//
//: Return whether or not the Reader is in a good state.
//
//!return: bool - true if the reader is good, false otherwise.
//
bool Reader::good() const
{
    // Check to see if we are currently reading from the pushback buffer
    if ( !mBuffer->good() )
    {
        // No, just check the istream
        return mStream->good();
    }

    // Yes, the pushback buffer should ALWAYS be good
    assert( mBuffer->good() );
    return mBuffer->good();
}


//-----------------------------------------------------------------------------
//
//: Return whether or not the Reader is in the 'fail' state.
//
//!return: bool - true if the reader is in the 'fail' state, false otherwise.
//
bool Reader::fail() const
{
    // Check to see if we are currently reading from the pushback buffer
    if ( !mBuffer->good() )
    {
        // No: just check the istream
        return mStream->fail();
    }

    // Yes: the pushback buffer should ALWAYS be good
    assert( !mBuffer->fail() );
    return mBuffer->fail();
}


//-----------------------------------------------------------------------------
//
//: Return whether or not the Reader is in a bad state.
//
//!return: bool - true if the reader is bad, false otherwise.
//
bool Reader::bad() const
{
    // Check to see if we are currently reading from the pushback buffer
    if ( !mBuffer->good() )
    {
        // No: just check the istream
        return mStream->bad();
    }

    // Yes: the pushback buffer NEVER be bad.
    assert( !mBuffer->bad() );
    return mBuffer->bad();
}


//-----------------------------------------------------------------------------
//
//: Return whether or not the Reader is in the 'eof' state.
//
//!return: bool - true if the reader is in the 'eof' state, false otherwise.
//
bool Reader::eof() const
{
    // Check to see if we are currently reading from the pushback buffer
    if ( !mBuffer->good() )
    {
        // No: just check the istream
        return mStream->eof();
    }

    // Yes: the pushback buffer should NEVER be at eof
    assert( !mBuffer->eof() );
    return mBuffer->eof();
}


//*******************/
/* XML check methods */
//*******************/


//-----------------------------------------------------------------------------
//
//: isLetter
//
// returns whether the character is a Letter (XML 1.0).
//
//!param: char c -
//
//!return: bool
//
bool Reader::isLetter( char c )
{
    return ( ( ( c >= 0x41 ) && ( c <= 0x5A ) )
             || ( ( c >= 0x61 ) && ( c <= 0x7A ) ) );
}


//-----------------------------------------------------------------------------
//
//: isDigit
//
// returns whether the character is a Digit (XML 1.0).
//
//!param: char c -
//
//!return: bool
//
bool Reader::isDigit( char c )
{
    return ( ( c >= 0x30 ) && ( c <= 0x39 ) );
}


//-----------------------------------------------------------------------------
//
//: isNameChar
//
// returns whether the character is a NameChar (XML 1.0).
//
//!param: char c -
//
//!return: bool
//
bool Reader::isNameChar( char c )
{
    return ( isLetter( c )
             || isDigit( c )
             || ( c == '.' )
             || ( c == '-' )
             || ( c == '_' )
             || ( c == ':' ) );
}


//***********/
/* Push back */
//***********/

    
//-----------------------------------------------------------------------------
//
//: Pushes a single character back into the reader.
//
//!param: char c - The character to push
//
void Reader::pushback( char c )
{
    string new_str_buf( 1, c );
    new_str_buf += mBuffer->str();
   
    mBuffer->str( new_str_buf );
    return;
}


//-----------------------------------------------------------------------
/// \brief Pushes a string back into the reader.
///
/// \param s
///     A null-terminated ascii string.
//-----------------------------------------------------------------------
void Reader::pushback( const char* s )
{
    string new_str_buf( s );
    new_str_buf += mBuffer->str();
   
    mBuffer->str( new_str_buf );
    return;   
   
}


//-----------------------------------------------------------------------------
//
//: Pushes bytes back into the reader.
//
// The bytes will be pushed in such that
// when they are read out again, the first byte pointed to by the 's' parameter
// will be the first one read.
//
//!param: const char* s - A pointer to the buffer.
//!param: size_t n - The number of bytes to push.
//
void Reader::pushback( const void* s, size_t n )
{
    ostringstream new_stream;
    new_stream.write( static_cast< const char* >( s ), n );
    new_stream << mBuffer->str();
    
    // Reset internal buffer of mBuffer:
    mBuffer->str( new_stream.str() );
    return;
}


//************************/
/* Private Helper Methods */
//************************/


//-----------------------------------------------------------------------------
//
//: Checks the reader state and throws an exception if it isn't good.
//
//!except: StreamException -
//
void Reader::checkAndThrow() const 
{
    if ( !good() )
    {
        throw ILWD_STREAMEXCEPTION( *mStream );
    }
   
    return;
}       


