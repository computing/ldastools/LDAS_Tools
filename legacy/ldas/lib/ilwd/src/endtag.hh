#ifndef EndTagHH
#define EndTagHH


//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

/* System Includes */
#include <string>
#include <iosfwd>
#include <exception>

/* LDAS Includes */
#include <general/formatexception.hh>
#include <general/util.hh>
#include <general/regexmatch.hh>

/* Local Includes */
#include "base.hh"
#include "reader.hh"


/// \cond DOXYGEN_IGNORE
namespace ILwd
{
    class EndTag;
}

namespace std
{
  std::ostream& operator<<( std::ostream& stream,
			    const ILwd::EndTag& end );
}
/// \endcond

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::EndTag& base );
void os_read( os_bstream& stream, ILwd::EndTag& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: Represents an XML end-tag.
//
class ILwd::EndTag : public ILwd::Base
{
public:

    /* Constructors */
    explicit EndTag( const std::string& name = "unkown" );
    explicit EndTag( Reader& is );

    /* Destructor */
    virtual ~EndTag();

    /* Operator overloads */
    const EndTag& operator=( const EndTag& e );
    bool operator==( const EndTag& e ) const;
    bool operator!=( const EndTag& e ) const;
    
    /* Accessor */
    const std::string& getIdentifier() const;
    ClassType getClassId() const;

    /* I/O */
    virtual void write( std::ostream& stream ) const;
    virtual void read( Reader& stream );
    
private:
    static const Regex &regex;
    std::string mName;

#ifdef HAVE_LIBOSPACE
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const EndTag& - A reference to the object to write to  +
    //!param:    the stream.
    //   
    friend void ::os_write( os_bstream&, const EndTag& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: const EndTag& - A reference to the object to read      + 
    //!param:    from the stream.
    //   
    friend void ::os_read( os_bstream&, EndTag& );
#endif // HAVE_LIBOSPACE
};



//-----------------------------------------------------------------------------
// Inline Methods
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Inequality operator.
//
//!param: const EndTag& e - The object to compare to.
//
//!return: bool - true if the objects are not equal.  False otherwise.
//
inline bool ILwd::EndTag::operator!=( const EndTag& e ) const
{
    return !operator==( e );
}


//-----------------------------------------------------------------------------
//
//: Get the identifier contained in the end-tag.
//
//!return: const std::string& - The identifier.
//
inline const std::string& ILwd::EndTag::getIdentifier() const
{
    return mName;
}


// ObjectSpace
#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::EndTag )
OS_STREAM_OPERATORS( ILwd::EndTag )

#endif // HAVE_LIBOSPACE

#endif // EndTagHH
