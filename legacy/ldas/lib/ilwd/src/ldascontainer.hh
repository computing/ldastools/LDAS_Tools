/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef LdasContainerHH
#define LdasContainerHH


//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif


// ObjectSpace Includes
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

// System Includes
#include <vector>
#include "general/unordered_map.hh"

// LDAS Includes
#include <general/util.hh>

// Local Includes
#include "ldaselement.hh"
#include "ldasarraybase.hh"


/// \cond DOXYGEN_IGNORE
namespace ILwd {
    class LdasContainer;
}
/// \endcond

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::LdasContainer& base );
void os_read( os_bstream& stream, ILwd::LdasContainer& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: The ILWD Container class.
//
//!todo: Change to containment.
//!todo: Combine the ownership information and the data into one STL vector.
//!todo: Re-write an iterator so that it appears as though the iterators work
//!todo: the same as before.
//


class ILwd::LdasContainer
    : public ILwd::LdasElement,
      private std::vector< ILwd::LdasElement* >
{
private:
	static const std::string mIdent;

public:
    /* enum's */
    enum allocate_type {
	NO_ALLOCATE = 0,
	ALLOCATE
    };	

    enum own_type {
	NO_OWN = 0,
	OWN
    };

    /* Typedef's */
    typedef std::vector< LdasElement* >::value_type value_type;
    typedef std::vector< LdasElement* >::pointer pointer;
    typedef std::vector< LdasElement* >::reference reference;
    typedef std::vector< LdasElement* >::const_reference const_reference;
    typedef std::vector< LdasElement* >::size_type size_type;
    typedef std::vector< LdasElement* >::difference_type difference_type;
    typedef std::vector< LdasElement* >::iterator iterator;
    typedef std::vector< LdasElement* >::const_iterator const_iterator;
    typedef std::vector< LdasElement* >::reverse_iterator reverse_iterator;
    typedef std::vector< LdasElement* >::const_reverse_iterator
        const_reverse_iterator;
    
    /* Constructors / Destructor */
    explicit LdasContainer( const std::string& name = "" );
    LdasContainer( const LdasContainer& c );
    LdasContainer( Reader& r );
    LdasContainer( Reader& r, const Attributes& att );
    ~LdasContainer();

    /* Operator Overloads */
    const LdasContainer& operator=( const LdasContainer& c );
   
    //
    //: Concatenation operator.
    //
    // This appends another container object to this.  A deep copy 
    // is performed of all contained elements.
    //
    //!param: const LdasContainer& c - The container to append from.
    //
    //!return: const LdasContainer& - A reference to this container.
    //
    //!exc: std::bad_alloc - Memory allocation failed.
    //   
    const LdasContainer& operator+=( const LdasContainer& c);

    bool operator==( const LdasContainer& c ) const;
    bool operator!=( const LdasContainer& c ) const;

    /* Accessors */
    bool isOwned( const_iterator iter ) const;
    bool isOwned( size_t index ) const;
    static const std::string& getIdentifierStatic();
    const std::string& getIdentifier() const;
    ElementId getElementId() const;
    Format getWriteFormat() const;
    Compression getWriteCompression() const;
    bool isHashing() const;
    size_t getHashPosition() const;
    
    /// \cond DOXYGEN_IGNORE
    using std::vector< LdasElement* >::size;
    using std::vector< LdasElement* >::front;
    using std::vector< LdasElement* >::back;
    using std::vector< LdasElement* >::empty;
    /// \endcond
    reference operator[](size_type n) { return std::vector< LdasElement* >::operator[]( n ); }
    const_reference operator[](size_type n) const { return std::vector< LdasElement* >::operator[]( n ); }
    // const std::vector< LdasElement* >::operator[];

    /* Iterators */
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    reverse_iterator rend();
    const_reverse_iterator rend() const;

    /* Mutators */
    iterator insert( iterator pos, const LdasElement& b );
    iterator insert( iterator pos, const LdasElement* b );
    iterator insert( iterator pos, LdasElement* b, allocate_type allocate,
                     own_type owns );

    iterator erase( iterator pos );
    iterator erase( iterator first, iterator last );

    void push_back( const LdasElement& b );
    void push_back( const LdasElement* b );
    void push_back( LdasElement* b, allocate_type allocate, own_type owns );

    void pop_back();
    void setWriteFormat( Format f );
    void setWriteCompression( Compression c );
    void setHashing( bool hashing );
    void setHashing( bool hashing, size_t pos );
    void setHashPosition( size_t pos );
    
    /* Copy */
    LdasContainer* createCopy() const;

    /* Search */
    void rehash() const;
    const LdasElement* find( const std::string& name, size_t pos = 0 ) const;
    LdasElement* find( const std::string& name, size_t pos = 0 );
    iterator find( const LdasElement* b );
    
    /* I/O */
    void read( Reader& r );
    virtual void write( std::ostream& stream, Format f,
			Compression c = USER_COMPRESSION ) const;
    virtual void write( std::ostream& stream ) const;
    void write( size_t indent, size_t delta, std::ostream& stream,
                Format f = USER_FORMAT, Compression c = USER_COMPRESSION ) const;
    void read( Reader& r, const Attributes& att );

   
protected:
   
    //: Unset ownership of child element: for use by ~LdasElement
    friend LdasElement::~LdasElement();
    void unsetOwnership( const_iterator iter );   
   
private:


   
    /* typedef's */
    typedef General::
    unordered_multimap< std::string,
			LdasElement*,
			General::ic_hash< std::string >,
			CaseInsensitiveCmp > NameHash;
    typedef NameHash::iterator namehash_iterator;
    typedef std::vector< own_type >::iterator ownership_iterator;
    typedef std::vector< own_type >::const_iterator const_ownership_iterator;
    
    /* methods */
    NameHash::iterator findHash( const std::string& name ) const;

    /* data */
    bool mIsHashing;    
    mutable NameHash mNameHash;
    Format mFormat;
    Compression mCompression;
    size_t mHashPosition;
    std::vector< own_type > mOwns;
    
#ifdef HAVE_LIBOSPACE
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const LdasContainer& - A reference to the object to +
    //!param:    write to the stream.
    //      
    friend void ::os_write( os_bstream&, const LdasContainer& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: LdasContainer& - A reference to the object to + 
    //!param:    read from the stream.
    //    
    friend void ::os_read( os_bstream&, LdasContainer& );
#endif // HAVE_LIBOSPACE
};



//-----------------------------------------------------------------------------
//: Gets the mIsHashing setting.
//   
//!return: bool - mIsHashing value.
//   
inline bool ILwd::LdasContainer::isHashing() const
{
    return mIsHashing;
}

   
//-----------------------------------------------------------------------------
//: Gets the element's hash position.
//   
//!return: size_t - Hash position.
//   
inline size_t ILwd::LdasContainer::getHashPosition() const
{
    return mHashPosition;
}




//-----------------------------------------------------------------------------
//: Gets the write format of the object.
//
//!return: Format - Write format.
//   
inline ILwd::Format ILwd::LdasContainer::getWriteFormat() const
{
    return mFormat;
}

   
//-----------------------------------------------------------------------------
//: Gets the element's write compression.
//
//!return: Compression - Write compression.
//   
inline ILwd::Compression ILwd::LdasContainer::getWriteCompression() const
{
    return mCompression;
}


//-----------------------------------------------------------------------------
//: Sets the write format of the object.
//   
//!param: Format f - The write format to set mFormat to.
//   
inline void ILwd::LdasContainer::setWriteFormat( Format f )
{
    mFormat = f;
    return;
}


//-----------------------------------------------------------------------------
//: Sets the write compression of the object.
//   
//!param: Compression c - The write compression to set mCompression to.
//   
inline void ILwd::LdasContainer::setWriteCompression( Compression c )
{
    mCompression = c;
    return;   
}


//-----------------------------------------------------------------------------
//: Gets the ownership of specified container element.
//  
//!param: const_iterator iter - Iterator to specify container element.
//
//!return: bool - True if container owns the element, false otherwise.
//   
inline bool ILwd::LdasContainer::isOwned( const_iterator iter ) const
{
    return isOwned( iter - begin() );
}

inline void ILwd::LdasContainer::
write( std::ostream& stream ) const
{
    return ILwd::LdasElement::write( stream );
}

#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::LdasContainer )
OS_STREAM_OPERATORS( ILwd::LdasContainer )

#endif // HAVE_LIBOSPACE


#endif // LdasContainerHH
