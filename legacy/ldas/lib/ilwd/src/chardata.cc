#include "ilwd/config.h"

#include <sstream>
   
#include "chardata.hh"


using namespace std;   
using ILwd::CharData;


//-----------------------------------------------------------------------------
//
//: Input Constructor.
//
// This method reads character data from a stream.
// It stops upon an EOF, '<' (not extracted), or "]]>" (extracted).
// Currently it also stops upon encountering an ampersand.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException
//
//!todo: There is copying here, maybe we can get rid of this.
//
CharData::CharData( Reader& r )
{
    ostringstream s;
    bool run = true;
    int p;
    char c;
    
    while( run )
    {
        p = r.peek(); // May throw StreamException

        if ( p == '&' )
        {
            // not defined yet, stop
            run = false;
        }
        else if ( p == '<' )
        {
            // stop
            run = false;
        }
        else if ( p == ']' )
        {
            p = r.get();
            p = r.peek(); // May throw StreamException

            if ( p == ']' )
            {
                p = r.get();
                p = r.peek(); // May throw StreamException

                if ( p == '>' )
                {
                    p = r.get();
                    run = false;
                }
                else
                {
                    s << "]]";
                }
            }
            else
            {
                s << ']';
            }
        }

        if ( run )
        {
            c = r.get();
            s << c;
        }
    }

    mData = s.str();
    return;
}

   
CharData::~CharData()
{}

  
//-----------------------------------------------------------------------------
//   
//: Reads class id. 
//
// This method gets class id. 
//
//!return ClassType - ID.
ILwd::ClassType CharData::getClassId() const
{
   return ID_CHARDATA;
}   
   

//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const CharData& c - The object to assign from.
//
//!return: const CharData& - A reference to this object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
const CharData& CharData::operator=( const CharData& c )
{
    if ( this != &c )
    {
        Base::operator=( c );
        mData = c.mData;
    }

    return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another for equality.
//
//!param: const CharData& c - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool CharData::operator==( const CharData& c ) const
{
    return ( Base::operator==( c ) && mData == c.mData );
}


// ObjectSpace
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::CharData*), Stream::ILwd::CHARDATA, (ILwd::Base*) )

void os_write( os_bstream& stream, const CharData& o )
{
    os_write( stream, (ILwd::Base&)o );
    stream << o.mData;
}

void os_read( os_bstream& stream, CharData& o )
{
    os_read( stream, (ILwd::Base&)o );
    stream >> o.mData;
}

#endif // HAVE_LIBOSPACE
