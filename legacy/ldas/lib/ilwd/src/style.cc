// System Header Files
#include "ilwd/config.h"

#include <stdio.h>
#include <ctype.h>
#include <math.h>

#include <climits>
#include <iomanip>
#include <sstream>

#include "general/util.hh"
#include "general/autoarray.hh"

// Local Header Files   
#include "ilwdconst.h"
#include "style.hh"
#include "style.icc"
#include "errors.hh"

using ILwd::Style;
   
// Local constants representing 
// numeric limits:   
const INT_8S MAX_CHAR( CHAR_MAX );   
const INT_8S MIN_CHAR( CHAR_MIN );      
const INT_8U MAX_CHAR_U( UCHAR_MAX );      

#if SIZEOF_SHORT == 2   
const INT_8S MAX_INT_2S( SHRT_MAX );   
const INT_8S MIN_INT_2S( SHRT_MIN );   
const INT_8U MAX_INT_2U( USHRT_MAX );   
#elif SIZEOF_INT == 2
const INT_8S MAX_INT_2S( INT_MAX );   
const INT_8S MIN_INT_2S( INT_MIN );   
const INT_8U MAX_INT_2U( UINT_MAX );   
#endif     

#if SIZEOF_INT == 4   
const INT_8S MAX_INT_4S( INT_MAX );
const INT_8S MIN_INT_4S( INT_MIN );   
const INT_8U MAX_INT_4U( UINT_MAX );
#elif SIZEOF_LONG == 4
const INT_8S MAX_INT_4S( LONG_MAX );
const INT_8S MIN_INT_4S( LONG_MIN );   
const INT_8U MAX_INT_4U( ULONG_MAX );
#endif   
   
const REAL_8 MAX_POS_REAL_4( FLT_MAX );   
const REAL_8 MIN_POS_REAL_4( FLT_MIN );      
const REAL_8 MIN_NEG_REAL_4( -FLT_MAX );      
const REAL_8 MAX_NEG_REAL_4( -FLT_MIN );         
   
const long double MAX_POS_REAL_8( DBL_MAX );      
const long double MIN_POS_REAL_8( DBL_MIN );  
const long double MIN_NEG_REAL_8( -DBL_MAX );      
const long double MAX_NEG_REAL_8( -DBL_MIN );     

namespace
{
  template< class T > const char* type_to_string( );

#define INSTANTIATE(T) \
  template<> inline const char* type_to_string<T>( ) { return #T; }

  INSTANTIATE(INT_2U)
  INSTANTIATE(INT_2S)
  INSTANTIATE(INT_4U)
  INSTANTIATE(INT_4S)
  INSTANTIATE(INT_8U)
  INSTANTIATE(INT_8S)
  INSTANTIATE(REAL_4)
  INSTANTIATE(REAL_8)
  INSTANTIATE(COMPLEX_8)
  INSTANTIATE(COMPLEX_16)

#undef INSTANTIATE

  template< class T >
  inline void
  validate_data_dim( const bool BadDataCondition,
		     const bool DataEndCondition,
		     size_t Position,
		     size_t MaxPosition )
  {
    if ( BadDataCondition ) {
      if ( DataEndCondition )
      {
	std::ostringstream msg;

	msg << "Expected " << MaxPosition
	    << " elements but only read " << Position << " elements";
	throw ILWD_FORMATEXCEPTION_INFO( ILwd::Errors::DIM_ERROR, msg.str( ) );
      }
      // bad data of some kind... not a float number.
      std::ostringstream msg;
      
      msg << "Could not convert element " << Position
	  << " to " << type_to_string<T>( );
      throw ILWD_FORMATEXCEPTION_INFO( ILwd::Errors::BAD_DATA_TYPE, msg.str( ) );
    }
  }
} // namespace - anonymous

   
namespace ILwd
{
  //--------------------------------------------------------------------------
  /// Creates a style with the given format and compression.
  //--------------------------------------------------------------------------
  Style::
  Style( Format f, Compression c )
    : mFormat( f ), mCompression( c )
  {
  }


  //--------------------------------------------------------------------------
  /// Creates a Style object by copying the existing object.
  //--------------------------------------------------------------------------
  Style::
  Style( const Style& s )
    : mFormat( s.mFormat ), mCompression( s.mCompression )
  {}


  //-------------------------------------------------------------------------
  /// This assigns another style object to this.
  //-------------------------------------------------------------------------
  const Style& Style::
  operator=( const Style& s ) 
  {
    if ( this != &s )
    {
      mFormat = s.mFormat;
      mCompression = s.mCompression;
    }

    return *this;
  }


  //--------------------------------------------------------------------------
  /// This compares this object to another style for equality.
  //--------------------------------------------------------------------------
  bool Style::
  operator==( const Style& s ) const
  {
    return ( mFormat == s.mFormat && mCompression == s.mCompression );
  }

  //--------------------------------------------------------------------------
  /// This compares this object to another Style for inequality.
  //--------------------------------------------------------------------------
  bool Style::operator!=( const Style& s ) const
  {
    return ( mFormat != s.mFormat || mCompression != s.mCompression );
  }


  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  char* Style::
  binaryToBase64( const char* source, size_t sourceSize ) const
  {
    char* newData = new char[
			     Base64::calculateEncodedSize( sourceSize ) + 1 ];
    Base64::encode( newData, source, sourceSize );
    return newData;
  }


  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  char* Style::
  base64ToBinary( const char* source, size_t* destSize ) const
  {
    *destSize = Base64::calculateDecodedSize( source );
    char* newData( new char[ *destSize ] );
    Base64::decode( newData, source );
    return newData;
  }

   
  template<>
  CHAR* Style::
  asciiToData< CHAR >( const char* source,
		       size_t destSize,
		       const char** end_ptr ) const
  {
    CHAR* dest = new CHAR[ destSize ];

    try {
      size_t i;
      for (i = 0; i < destSize; ++i)
      {
	if (*source == BACK_SLASH_CHAR)
	{
	  ++source;
	  if (*source == '\0')
	  {
	    dest[i++] = BACK_SLASH_CHAR;
	    if (i == destSize) break;
	    dest[i] = *source;
	    continue;
	  }
	  if (*source == BACK_SLASH_CHAR)
	  {
	    dest[i] = *source;
	    source++;
	  }
	  else
	  {
	    int	octal(0);
	    for (size_t o = 0; o < 3; ++o)
	    {
	      octal *= 8;
	      int	inc(*source - '0');
	      if ((inc >= 0) && (inc <= 7))
	      {
		octal += inc;
	      }
	      else
	      {
		// too big or small a number for char type...
		throw ILWD_FORMATEXCEPTION( Errors::OUT_OF_RANGE );
	      }
	      ++source;
	    }
	    dest[i] = octal;
	  }
	}
	else
	{
	  dest[i] = *source;
	  ++source;
	}
      }
      if ((i != destSize) || (*source != '\0'))
      {
	throw ILWD_FORMATEXCEPTION( Errors::INVALID_CHAR );
      }
    }
    catch(...)
    {
      delete[] dest;
      dest = 0;
      throw;
    }
    *end_ptr = source;
    return dest;
  }

   
  template<>
  CHAR_U* Style::
  asciiToData< CHAR_U >( const char* source,
			 size_t destSize,
			 const char** end_ptr ) const
  {
    CHAR_U* dest = new CHAR_U[ destSize ];

    try {
      size_t i;
      for (i = 0; (i < destSize) && (*source != '\0'); i++)
      {
	if (*source == BACK_SLASH_CHAR)
	{
	  source++;
	  if (*source == '\0') break;
	  if (*source == BACK_SLASH_CHAR)
	  {
	    dest[i] = *source;
	    source++;
	  }
	  else
	  {
	    int	octal(0);
	    for (size_t o = 0; o < 3; o++)
	    {
	      octal *= 8;
	      int	inc(*source - '0');
	      if ((inc >= 0) && (inc <= 7))
	      {
		octal += inc;
	      }
	      else
	      {
		// too big or small a number for char type...
		throw ILWD_FORMATEXCEPTION( Errors::OUT_OF_RANGE );
	      }
	      source++;
	    }
	    dest[i] = octal;
	  }
	}
	else
	{
	  dest[i] = *source;
	  source++;
	}
      }
      if ((i != destSize) || (*source != '\0'))
      {
	throw ILWD_FORMATEXCEPTION( Errors::INVALID_CHAR );
      }
    }
    catch(...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    *end_ptr = source;
    return dest;
  }

   
  template<>
  INT_2S* Style::asciiToData< INT_2S >(
				       const char* source, size_t destSize, const char** end_ptr ) const 
  {
    istringstream ss( source );

    INT_2S* dest = new INT_2S[ destSize ];
    INT_8S tmp;  
    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	// give it a try.
	ss >> tmp;
	validate_data_dim< INT_2S >( !ss, ss.eof( ), i, destSize );
	if( MIN_INT_2S > tmp || tmp > MAX_INT_2S ) {
	  // too big or small a number for short type...
	  throw ILWD_FORMATEXCEPTION( Errors::OUT_OF_RANGE );
	}
	dest[ i ] = static_cast< INT_2S >( tmp );
	if ( i != ( destSize-1 ) ) {
	  int c = ss.peek();
	  if( isspace( c ) || c == ',' ) {
	    ss.get();
	  }
	}
      }
    }
    catch(...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    set_end_ptr( ss, source, end_ptr );
    return dest;
  }

   
  template<>
  INT_2U* Style::asciiToData< INT_2U >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    istringstream ss( source );

    INT_2U* dest = new INT_2U[ destSize ];
    INT_8U tmp;  

    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	if( ss.peek() == '-' ) {
	  // negative number to unsigned
	  throw ILWD_FORMATEXCEPTION( Errors::UNSIGNED_BELOW_ZERO );
	}
	// give it a try.
	ss >> tmp;
	validate_data_dim< INT_2U >( !ss, ss.eof( ), i, destSize );
	if( tmp > MAX_INT_2U ) {
	  // too large a number for unsigned short type...
	  throw ILWD_FORMATEXCEPTION( Errors::OUT_OF_RANGE );
	}
	dest[ i ] = static_cast< INT_2U >( tmp );
	if ( i != ( destSize-1 ) ) {
	  int c = ss.peek();
	  if( isspace( c ) || c == ',' ) {
	    ss.get();
	  }
	}
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    set_end_ptr( ss, source, end_ptr );
    return dest;
  }
 

  template<>
  INT_4S* Style::asciiToData< INT_4S >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    istringstream ss( source );

    INT_4S* dest = new INT_4S[ destSize ];
    INT_8S tmp;  

    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	// give it a try.
	ss >> tmp;
	validate_data_dim< INT_4S >( !ss, ss.eof( ), i, destSize );
	if( MIN_INT_4S > tmp || tmp > MAX_INT_4S ) {
	  // too big or small a number for long type...
	  throw ILWD_FORMATEXCEPTION( Errors::OUT_OF_RANGE );
	}
	dest[ i ] = static_cast< INT_4S >( tmp );
	if ( i != ( destSize-1 ) ) {
	  int c = ss.peek();
	  if( isspace( c ) || c == ',' ) {
	    ss.get();
	  }
	}
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    set_end_ptr( ss, source, end_ptr );
    return dest;
  }

   
  template<>
  INT_4U* Style::asciiToData< INT_4U >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    istringstream ss( source );

    INT_4U* dest = new INT_4U[ destSize ];
    INT_8U tmp;  

    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	if( ss.peek() == '-' ) {
	  // negative number to unsigned
	  throw ILWD_FORMATEXCEPTION( Errors::UNSIGNED_BELOW_ZERO );
	}
	// give it a try.
	ss >> tmp;
	validate_data_dim< INT_4U >( !ss, ss.eof( ), i, destSize );
	if( tmp > MAX_INT_4U ) {
	  // too large a number for unsigned long type...
	  throw ILWD_FORMATEXCEPTION( Errors::OUT_OF_RANGE );
	}
	dest[ i ] = static_cast< INT_4U >( tmp );
	if ( i != ( destSize-1 ) ) {
	  int c = ss.peek();
	  if( isspace( c ) || c == ',' ) {
	    ss.get();
	  }
	}
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    set_end_ptr( ss, source, end_ptr );
    return dest;
  }

   
  template<>
  INT_8S* Style::asciiToData< INT_8S >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    istringstream ss( source );

    INT_8S* dest = new INT_8S[ destSize ];
    INT_8S tmp;  

    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	// give it a try.
	ss >> tmp;
	validate_data_dim< INT_8S >( !ss, ss.eof( ), i, destSize );
	dest[ i ] = static_cast< INT_8S >( tmp );
	if ( i != ( destSize-1 ) ) {
	  int c = ss.peek();
	  if( isspace( c ) || c == ',' ) {
	    ss.get();
	  }
	}
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    set_end_ptr( ss, source, end_ptr );
    return dest;
  }

   
  template<>
  INT_8U* Style::asciiToData< INT_8U >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    istringstream ss( source );

    INT_8U* dest = new INT_8U[ destSize ];
    INT_8U tmp;  

    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	if( ss.peek() == '-' ) {
	  // negative number to unsigned
	  throw ILWD_FORMATEXCEPTION( Errors::UNSIGNED_BELOW_ZERO );
	}
	// give it a try.
	ss >> tmp;
	validate_data_dim< INT_8U >( !ss, ss.eof( ), i, destSize );
	dest[ i ] = static_cast< INT_8U >( tmp );
	if ( i != ( destSize-1 ) ) {
	  int c = ss.peek();
	  if( isspace( c ) || c == ',' ) {
	    ss.get();
	  }
	}
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    set_end_ptr( ss, source, end_ptr );
    return dest;
  }

  template<>
  REAL_4* Style::asciiToData< REAL_4 >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    REAL_4* dest = new REAL_4[ destSize ];
    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	char *endptr;
	REAL_8 tmp = strtod( source, &endptr );

	validate_data_dim< REAL_4 >( source == endptr, *source == '\0',
				     i, destSize );

	if ( tmp == HUGE_VAL
	     || tmp == -HUGE_VAL
	     || ( tmp != 0.0 && 
		  ( tmp < MIN_NEG_REAL_4 || tmp > MAX_POS_REAL_4 || 
		    ( tmp > MAX_NEG_REAL_4 && tmp < MIN_POS_REAL_4 ) ) ) ) {
	  // too big or small a number for float type...

	  General::AutoArray< CHAR > val( new CHAR[ endptr - source + 1 ] );    
	  char buffer[ 1024 ];

	  strncpy( val.get(), source, endptr - source );
	  val[ endptr - source ] = 0;
	  sprintf(buffer, "%s - [%g - %g][%g - %g]",
		  val.get(), MIN_NEG_REAL_4, MAX_NEG_REAL_4,
		  MIN_POS_REAL_4, MAX_POS_REAL_4);
	  throw ILWD_FORMATEXCEPTION_INFO( Errors::OUT_OF_RANGE, buffer );
	}

	if( isspace( *endptr ) || *endptr == ',' ) {
	  endptr++;
	}
	source = endptr;
	dest[ i ] = static_cast< REAL_4 >( tmp );
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    *end_ptr = source;
    return dest;
  }


  template<>
  REAL_8* Style::asciiToData< REAL_8 >(
				       const char* source, size_t destSize, const char** end_ptr ) const
  {
    REAL_8* dest = new REAL_8[ destSize ];
    try { 
      for ( size_t i = 0; i < destSize; i++ ) {
	char *endptr;
	REAL_8 tmp = strtod( source, &endptr );

	validate_data_dim< REAL_8 >( source == endptr, *source == '\0',
				     i, destSize );
	if ( tmp == HUGE_VAL 
	     || tmp == -HUGE_VAL
	     || ( tmp != 0.0 && 
		  ( tmp < MIN_NEG_REAL_8 || tmp > MAX_POS_REAL_8 || 
		    ( tmp > MAX_NEG_REAL_8 && tmp < MIN_POS_REAL_8 ) ) ) ) {
	  // too big or small a number for float type...
	  General::AutoArray< CHAR > val( new CHAR[ endptr - source + 1 ] );
	  char buffer[ 1024 ];

	  strncpy( val.get(), source, endptr - source );
	  val[ endptr - source ] = 0;
	  sprintf(buffer, "%s - [%Lg - %Lg][%Lg - %Lg]",
		  val.get(), MIN_NEG_REAL_8, MAX_NEG_REAL_8,
		  MIN_POS_REAL_8, MAX_POS_REAL_8);
	  throw ILWD_FORMATEXCEPTION_INFO( Errors::OUT_OF_RANGE, buffer );
	}

	if( isspace( *endptr ) || *endptr == ',' ) {
	  endptr++;
	}
	source = endptr;
	dest[i] = tmp;
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    *end_ptr = source;
    return dest;
  }

  template<>
  COMPLEX_8* Style::asciiToData< COMPLEX_8 >(
					     const char* source, size_t destSize, const char** end_ptr ) const
  {
    COMPLEX_8* dest = new COMPLEX_8[ destSize ];
    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	char *endptr;
	REAL_8 tmp1 = strtod( source, &endptr );

	validate_data_dim< COMPLEX_8 >( source == endptr, *source == '\0',
					i, destSize );
	if ( tmp1 == HUGE_VAL
	     || tmp1 == -HUGE_VAL
	     || ( tmp1 != 0.0 && 
		  ( tmp1 < MIN_NEG_REAL_4 || tmp1 > MAX_POS_REAL_4 || 
		    ( tmp1 > MAX_NEG_REAL_4 && tmp1 < MIN_POS_REAL_4 ) ) ) ) {
	  // too big or small a number for float type...
	  General::AutoArray< CHAR > val( new CHAR[ endptr - source + 1 ] );
	  char buffer[ 1024 ];

	  strncpy( val.get(), source, endptr - source );
	  val[ endptr - source ] = 0;
	  sprintf(buffer, "%s - [%g - %g][%g - %g]",
		  val.get(), MIN_NEG_REAL_4, MAX_NEG_REAL_4,
		  MIN_POS_REAL_4, MAX_POS_REAL_4);
	  throw ILWD_FORMATEXCEPTION_INFO( Errors::OUT_OF_RANGE, buffer );
	}

	if( isspace( *endptr ) || *endptr == ',' ) {
	  endptr++;
	}
	source = endptr;

	REAL_8 tmp2 = strtod( source, &endptr );
  
	validate_data_dim< COMPLEX_8 >( source == endptr, *source == '\0',
					i, destSize );
	if (tmp2 == HUGE_VAL
	    || tmp2 == -HUGE_VAL
	    || ( tmp2 != 0.0 && 
		 ( tmp2 < MIN_NEG_REAL_4 || tmp2 > MAX_POS_REAL_4 || 
		   ( tmp2 > MAX_NEG_REAL_4 && tmp2 < MIN_POS_REAL_4 ) ) ) ) {
	  // too big or small a number for float type...
	  General::AutoArray< CHAR > val( new CHAR[ endptr - source + 1 ] );
	  char buffer[ 1024 ];

	  strncpy( val.get(), source, endptr - source );
	  val[ endptr - source ] = 0;
	  sprintf(buffer, "%s - [%g - %g][%g - %g]",
		  val.get(), MIN_NEG_REAL_4, MAX_NEG_REAL_4,
		  MIN_POS_REAL_4, MAX_POS_REAL_4);
	  throw ILWD_FORMATEXCEPTION_INFO( Errors::OUT_OF_RANGE, buffer );
	}

	if( isspace( *endptr ) || *endptr == ',' ) {
	  endptr++; 
	}
	source = endptr;

	dest[ i ] = COMPLEX_8( static_cast< REAL_4 >( tmp1 ),
			       static_cast< REAL_4 >( tmp2 ) );
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    *end_ptr = source;
    return dest;
  }

  template<>
  COMPLEX_16* Style::asciiToData< COMPLEX_16 >(
					       const char* source, size_t destSize, const char** end_ptr ) const
  {
    COMPLEX_16* dest = new COMPLEX_16[ destSize ];
    try {
      for ( size_t i = 0; i < destSize; i++ ) {
	char *endptr;
	REAL_8 tmp1 = strtod( source, &endptr );

	validate_data_dim< COMPLEX_16 >( source == endptr, *source == '\0',
					 i, destSize );
	if ( tmp1 == HUGE_VAL 
	     || tmp1 == -HUGE_VAL
	     || ( tmp1 != 0.0 && 
		  ( tmp1 < MIN_NEG_REAL_8 || tmp1 > MAX_POS_REAL_8 || 
		    ( tmp1 > MAX_NEG_REAL_8 && tmp1 < MIN_POS_REAL_8 ) ) ) ) {
	  // too big or small a number for float type...
	  General::AutoArray< CHAR > val( new CHAR[ endptr - source + 1 ] );
	  char buffer[ 1024 ];

	  strncpy( val.get(), source, endptr - source );
	  val[ endptr - source ] = 0;
	  sprintf(buffer, "%s - [%Lg - %Lg][%Lg - %Lg]", 
		  val.get(), MIN_NEG_REAL_8, MAX_NEG_REAL_8,
		  MIN_POS_REAL_8, MAX_POS_REAL_8);
	  throw ILWD_FORMATEXCEPTION_INFO( Errors::OUT_OF_RANGE, buffer );
	}

	if( isspace( *endptr ) || *endptr == ',' ) {
	  endptr++;
	}
	source = endptr;

	REAL_8 tmp2 = strtod( source, &endptr );
 
	validate_data_dim< COMPLEX_16 >( source == endptr, *source == '\0',
					 i, destSize );

	if ( tmp2 == HUGE_VAL 
	     || tmp2 == -HUGE_VAL
	     || ( tmp2 != 0.0 && 
		  ( tmp2 < MIN_NEG_REAL_8 || tmp2 > MAX_POS_REAL_8 || 
		    ( tmp2 > MAX_NEG_REAL_8 && tmp2 < MIN_POS_REAL_8 ) ) ) ) {
	  // too big or small a number for float type...
	  General::AutoArray< CHAR > val( new CHAR[ endptr - source + 1 ] );
	  char buffer[ 1024 ];

	  strncpy( val.get(), source, endptr - source );
	  val[ endptr - source ] = 0;
	  sprintf(buffer, "%s - [%Lg - %Lg][%Lg - %Lg]",
		  val.get(), MIN_NEG_REAL_8, MAX_NEG_REAL_8,
		  MIN_POS_REAL_8, MAX_POS_REAL_8);
	  throw ILWD_FORMATEXCEPTION_INFO( Errors::OUT_OF_RANGE, buffer );
	}
  
	if( isspace( *endptr ) || *endptr == ',' ) {
	  endptr++; 
	}
	source = endptr;

	dest[ i ] = COMPLEX_16( tmp1, tmp2 );
      }
    }
    catch (...)
    {
      delete[] dest;
      dest = 0;   
      throw;
    }
    *end_ptr = source;
    return dest;
  }

  //--------------------------------------------------------------------------
  /// The data is returned as a null-terminated ASCII string.
  ///
  /// \note This method is used for integer types only
  //--------------------------------------------------------------------------
  template< class T >
  const std::string Style::
  dataToAscii( const T* source, size_t sourceSize ) const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }

    ostringstream oss;
    // Write the data

    if ( sourceSize > 0 )
    {
      // No space before the first number
      oss << source[0];
      for ( size_t i = 1; i < sourceSize; i++ )
      {
	oss << " " << source[i];
      }
    }

    return oss.str();
  }

  //---------------------------------------------------------------------
  /// \cond EXCLUDE
  /* Template instantiations for integer types (note that the keyword template
     has no <> after it, template<> is only used for declarations, not 
     template instantiations)
  */

  template
  const std::string Style::dataToAscii( const INT_2S* source,
					size_t sourceSize ) const;
  template
  const std::string Style::dataToAscii( const INT_2U* source,
					size_t sourceSize ) const;
  template
  const std::string Style::dataToAscii( const INT_4S* source,
					size_t sourceSize ) const;
  template
  const std::string Style::dataToAscii( const INT_4U* source,
					size_t sourceSize ) const;
  template
  const std::string Style::dataToAscii( const INT_8S* source,
					size_t sourceSize ) const;
  template
  const std::string Style::dataToAscii( const INT_8U* source,
					size_t sourceSize ) const;

  /// \endcond
  //---------------------------------------------------------------------
  /* Template specializations for other types */

  template<>
  const std::string Style::dataToAscii( const CHAR* source, size_t sourceSize )
    const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }

    ostringstream oss;

    for (size_t i = 0; i < sourceSize; ++i)
    {
      if (!isascii(source[i]) || !isprint(source[i]))
      {
	unsigned char	c = source[i];
	oss << BACK_SLASH_CHAR;
	oss << (char)('0' + (c / 64));
	c %= 64;
	oss << (char)('0' + (c / 8));
	c %= 8;
	oss << (char)('0' + c );
      }
      else
      {
	switch(source[i])
	{
	case BACK_SLASH_CHAR:
	  oss << BACK_SLASH_CHAR << source[ i ];
	  break;
	case '>':
	case '<':
	  {
	    unsigned char	c = source[i];
	    oss << BACK_SLASH_CHAR;
	    oss << (char)('0' + (c / 64));
	    c %= 64;
	    oss << (char)('0' + (c / 8));
	    c %= 8;
	    oss << (char)('0' + c );
	  }
	  break;
	default:
	  oss << source[ i ] ;
	  break;
	}
      }
    }

    return oss.str();       ;
  }

   
  template<>
  const std::string Style::dataToAscii( const CHAR_U* source, size_t sourceSize ) const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }
    
    ostringstream oss;

    // Write the data
    for ( size_t i = 0; i < sourceSize; ++i )
    {
      unsigned char c(source[i]);
      oss << BACK_SLASH_CHAR;
      oss << (char)('0' + (c / 64));
      c %= 64;
      oss << (char)('0' + (c / 8));
      c %= 8;
      oss << (char)('0' + c );
    }

    return oss.str();
  }

   
  //---------------------------------------------------------------------
  /// Specific instances for floating point, real and complex
  /// 
  /// The format for each real number
  /// is -9.99...999e+999 where the number of 9's in the mantissa is
  /// given by the precision. So each number is at most length
  /// 'precision' + 7 (the sign, the decimal point, e, plus sign and exponent)
  ///
  /// The total size needed for the stream is the size needed by each number
  /// (add one for the inter-number space) times the length of the array,
  /// plus another 1 for the null char.
  /// The buffer does not need to be deleted here because a pointer to it
  /// will be returned by oss.str() and deleted later
  //---------------------------------------------------------------------
  template<>
  const std::string Style::
  dataToAscii( const REAL_4* source, size_t sourceSize ) const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }

    static const std::streamsize prec = REAL_4_DIGITS + 1;

    ostringstream oss;
    oss.precision(prec);
    oss.setf(std::ios::scientific, std::ios::floatfield);
    
    if ( sourceSize > 0 )
    {
      // Write the data
      // No space before the first number
      oss << source[0];
      for ( size_t i = 1; i < sourceSize; ++i )
      {
	oss << " " << source[i];
      }
    }

    return oss.str();
  }


  template<>
  const std::string Style::dataToAscii( const REAL_8* source, size_t sourceSize ) const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }

    static const std::streamsize prec = REAL_8_DIGITS + 1;

    ostringstream oss;
    oss.precision(prec);
    oss.setf(std::ios::scientific, std::ios::floatfield);
    
    if ( sourceSize > 0 )
    {
      // Write the data
      // No space before the first number
      oss << source[0];
      for ( size_t i = 1; i < sourceSize; ++i )
      {
	oss << " " << source[i];
      }
    }

    return oss.str();
  }


  template<>
  const std::string Style::dataToAscii( const COMPLEX_8* source, size_t sourceSize ) const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }

    static const std::streamsize prec = REAL_4_DIGITS + 1;

    ostringstream oss;
    oss.precision(prec);
    oss.setf(std::ios::scientific, std::ios::floatfield);
    
    // Write the data
    // No space before the first number
    oss << source[0].real() << " " << source[0].imag();
    for ( size_t i = 1; i < sourceSize; ++i )
    {
      oss << " " << source[i].real() << " " << source[i].imag();
    }

    return oss.str();
  }
   

  template<>
  const std::string Style::dataToAscii( const COMPLEX_16* source, size_t sourceSize ) const
  {
    // Check the source
    if ( source == 0 )
    {
      return 0;
    }

    static const std::streamsize prec = REAL_8_DIGITS + 1;

    ostringstream oss;
    oss.precision(prec);
    oss.setf(std::ios::scientific, std::ios::floatfield);
    
    if ( sourceSize > 0 )
    {
      // Write the data
      // No space before the first number
      oss << source[0].real() << " " << source[0].imag();
      for ( size_t i = 1; i < sourceSize; ++i )
      {
	oss << " " << source[i].real() << " " << source[i].imag();
      }
    }

    return oss.str();
  }

        
  //--------------------------------------------------------------------------
  /// This method writes the attributes for this object to an
  /// Attributes object.
  //--------------------------------------------------------------------------
  void Style::
  writeAttributes( Attributes& att ) const
  {
    // Format
    switch ( mFormat )
    {
    case ASCII:
      break;

    case BASE64:
      att.addAttribute( "format", "base64" );
      break;
            
    case BINARY:
      att.addAttribute( "format", "binary" );
      break;
    default:
      break;
    }

    if ( mFormat == BASE64 || mFormat == BINARY )
    {
      switch( mCompression )
      {
      case NO_COMPRESSION:
      case GZIP0:
	break;

      case GZIP1:
	att.addAttribute( "compression", "gzip1" );
	break;
                
      case GZIP2:
	att.addAttribute( "compression", "gzip2" );
	break;
                
      case GZIP3:
	att.addAttribute( "compression", "gzip3" );
	break;
                
      case GZIP4:
	att.addAttribute( "compression", "gzip4" );
	break;
                
      case GZIP5:
	att.addAttribute( "compression", "gzip5" );
	break;
                
      case GZIP6:
	att.addAttribute( "compression", "gzip6" );
	break;

      case GZIP:
	att.addAttribute( "compression", "gzip" );
	break;
                
      case GZIP7:
	att.addAttribute( "compression", "gzip7" );
	break;
                
      case GZIP8:
	att.addAttribute( "compression", "gzip8" );
	break;
                
      case GZIP9:
	att.addAttribute( "compression", "gzip9" );
	break;
      default:
	break;
      }
    }
   
    return;
  }


  //--------------------------------------------------------------------------
  /// This reads the attributes (from an Attributes object) for this
  /// object and sets them.
  //--------------------------------------------------------------------------
  void Style::
  readAttributes( const Attributes& att ) 
  {
    mFormat = ASCII;
    mCompression = NO_COMPRESSION;    

    Attributes::const_iterator iter( att.find( "compression" ) );
    if ( iter != att.end() )
    {
      const string& iter_str( iter->second.getString() );
      const char* s( iter_str.c_str() );
        
      if ( strcasecmp( s, "none" ) == 0 ||
	   strcasecmp( s, "gzip0" ) == 0 )
      {
	mCompression = NO_COMPRESSION;
      }
      else if ( strcasecmp( s, "gzip" ) == 0 ||
		strcasecmp( s, "gzip6" ) == 0 )
      {
	mCompression = GZIP6;
      }
      else if ( strcasecmp( s, "gzip1" ) == 0 )
      {
	mCompression = GZIP1;
      }
      else if ( strcasecmp( s, "gzip2" ) == 0 )
      {
	mCompression = GZIP1;
      }
      else if ( strcasecmp( s, "gzip3" ) == 0 )
      {
	mCompression = GZIP3;
      }
      else if ( strcasecmp( s, "gzip4" ) == 0 )
      {
	mCompression = GZIP4;
      }
      else if ( strcasecmp( s, "gzip5" ) == 0 )
      {
	mCompression = GZIP5;
      }
      else if ( strcasecmp( s, "gzip7" ) == 0 )
      {
	mCompression = GZIP7;
      }
      else if ( strcasecmp( s, "gzip8" ) == 0 )
      {
	mCompression = GZIP8;
      }
      else if ( strcasecmp( s, "gzip9" ) == 0 )
      {
	mCompression = GZIP9;
      }
      else
      {
	string msg( "\"" );
	msg += s;
	msg += "\" is not a valid compression type.  Valid "
	  "types are none, gzip, gzip0, gzip1, ... gzip9 .";
	throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_ATTRIBUTE, msg );
      }        
    }

    iter = att.find( "format" );
    if ( iter != att.end() )
    {
      const string& iter_str( iter->second.getString() );
      const char* s( iter_str.c_str() );
        
      if ( strcasecmp( s, "ascii" ) == 0 )
      {
	mFormat = ASCII;
      }
      else if ( strcasecmp( s, "base64" ) == 0 )
      {
	mFormat = BASE64;
      }
      else if ( strcasecmp( s, "binary" ) == 0 )
      {
	mFormat = BINARY;
      }
      else
      {
	string msg;
	msg += '\"';
	msg += s;
	msg += "\" is not a valid format type.  Valid "
	  "types are ascii, base64, and binary .";
	throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_ATTRIBUTE, msg );
      }        
    }
   
    return;
  }


  //---------------------------------------------------------------------
  /// \cond EXCLUDE
#define	INSTANTIATE(instance) \
template instance* ILwd::Style:: \
convertFromFormat< instance >(const char* source, \
			      size_t size, \
			      size_t destSize) const; \
template char* ILwd::Style:: \
convertToFormat< instance >(const instance* source, \
			    size_t size, \
			    size_t* destSize) const; \
template char* ILwd::Style:: \
dataToBase64< instance >(const instance* source, size_t sourceSize ) const; \
template instance* ILwd::Style:: \
base64ToData< instance >(const char* source, size_t destSize ) const; \
template char* ILwd::Style:: \
compress< instance >(const instance* source, size_t sourceSize, Compression& compression, \
	             size_t* destSize ) const; \
template instance* ILwd::Style:: \
uncompress< instance >(const char* source, size_t sourceSize, \
		       size_t destSize, \
		       Compression compression ) const

  INSTANTIATE( CHAR );
  INSTANTIATE( CHAR_U );
  INSTANTIATE( INT_2S );
  INSTANTIATE( INT_2U );
  INSTANTIATE( INT_4S );
  INSTANTIATE( INT_4U );
  INSTANTIATE( INT_8S );
  INSTANTIATE( INT_8U );
  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
  /// \endcond
  //---------------------------------------------------------------------

} // namespace - ILwd

  // ObjectSpace
#if HAVE_LIBOSPACE
#include <ospace/source.h>
#include "ospaceid.hh"


void os_write( os_bstream& stream, const ILwd::Style& o )
{
  stream << int( o.getWriteFormat() )
	 << int( o.getWriteCompression() );
   
  return;
}

void os_read( os_bstream& stream, ILwd::Style& o )
{
  int format;
  int compression;
    
  stream >> format >> compression;

  o.setWriteFormat( ILwd::Format( format ) );
  o.setWriteCompression( ILwd::Compression( compression ) );
   
  return;
}

OS_STREAMABLE_0( (ILwd::Style*), Stream::ILwd::STYLE )

#endif // HAVE_LIBOSPACE

