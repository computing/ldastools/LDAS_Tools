#ifndef ILwdUtilHH
#define ILwdUtilHH

#include <iosfwd>


namespace ILwd {


void writeHeader( std::ostream& os );
bool readHeader( std::istream& is );


} // namespace


#endif // ILwdUtilHH
