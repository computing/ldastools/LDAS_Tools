/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ilwd/config.h"

#include <cctype>
#include <cstdio>
#include <cstdlib>

#include <sstream>
#include <algorithm>   

#include "ilwdconst.h"
#include "ldasstring.hh"
#include "endtag.hh"

#ifdef HAVE_LIBOSPACE
#include <ospace/io/device.h>
#include <ospace/stream/protocol.h>
#endif

#include <general/autoarray.hh>

using ILwd::LdasString;
using namespace std;   
using namespace General;
   

#define	FAST_SOCKET

const std::string ILwd::LdasString::mIdent = std::string("lstring");

static std::string protect(const std::string& source);

static void ensure_printable( const std::string& source );

   
//-----------------------------------------------------------------------------
//
//: Constructor.
//
// This constructs a ILWD string with the specified contents, name, comment and
// units.
// The LdasString element will contain a single string.
//
//!param: const std::string& s - The contents of the string.  Default:              +
//!param:     An empty string.
//!param: const std::string& name - The name of the element.                        +
//!param:     An empty string.
//!param: const std::string& comment - A comment.  Default: An empty string.
//!param: const std::string& units - An units associated with data.                 +
//!param:     Default: An empty string.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
LdasString::LdasString(
    const std::string& s, const std::string& name, 
    const std::string& comment, const std::string& units )
    : LdasFormatElement( name, comment ), std::vector< std::string >(), 
      mNUnits( 0 ), mUnits( 0 )
{
    try
    {
       if( units.empty() == false )
       {
          // Allocate memory
          mNUnits = 1;
          mUnits = new std::string[ mNUnits ];
          mUnits[ 0 ] = units;
       }
   
       ensure_printable( s );
       push_back( s );
    }
    catch(...)
    {
       delete[] mUnits;
       mUnits = 0;
       throw;
    }   
}


//-----------------------------------------------------------------------------
//
//: Copy constructor.
//
//!param: const LdasString& ls - The object to copy.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
LdasString::LdasString( const LdasString& ls )
   : LdasFormatElement( ls ), std::vector< std::string >( ls ),
     mNUnits( ls.mNUnits ), mUnits( 0 ) 
{
   if( mNUnits > 0 )
   {
      // Allocate memory for units
      mUnits = new std::string[ mNUnits ];

      try
      {
         // Assign units
         for ( size_type i = 0; i < mNUnits; i++ )
         {
            mUnits[ i ] = ls.mUnits[ i ];
         }
      }
      catch(...)
      {
         delete[] mUnits;
         mUnits = 0;
         throw;
      }   
   }
}

   
//-----------------------------------------------------------------------------
//
//: Input constructor.
//
// Constructs this object from an ILWD reader.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
ILwd::LdasString::LdasString( Reader& r )
    : LdasFormatElement(), std::vector< std::string >(),
      mNUnits( 0 ), mUnits( 0 )
{
    read( r );
}
   

//-----------------------------------------------------------------------------
//
//: Constructor.
//
// This constructs a ILWD string with the specified C-style contents, name, 
// comment and units.
// The LdasString element will contain n count of strings.
//
//!param: const char* s - A pointer to the array of strings.
//!param: size_type n - Number of entries in the data content array.   
//!param: const std::string& name - The name of the element. Default is an empty    +
//!param:    string.
//!param: const std::string& comment - A comment. Default: An empty string.
//!param: const size_type numUnits - Number of entries in the units array.   
//!param: const std::string* units - An array of units associated with data.        +
//!param:    Default is an empty string.
//
//!exc: std::bad_alloc - Memory allocation failed.
//   
LdasString::
LdasString( const char* s[], size_type n, const std::string& name,
	    const std::string& comment, const size_type numUnits,
	    const std::string* units )
    : LdasFormatElement( name, comment ),
      std::vector< std::string >( ),
      mNUnits( numUnits ), mUnits( 0 )
{
   try
   {
      if( mNUnits > 0 )
      {
         mUnits = new std::string[ mNUnits ];
         if( units != 0 )            
         {
            for( size_type i = 0; i < mNUnits; ++i )
            {
               mUnits[ i ] = units[ i ];
            }
         }
      }
      for ( size_type i = 0; i < n; ++i )
      {
   	 ensure_printable( s[ i ] );
         push_back( s[ i ] );
      }
   }
   catch(...)
   {
      delete[] mUnits;
      mUnits = 0;   
      throw;
   }   
}


//-----------------------------------------------------------------------------
//
//: Constructor.
//
// This constructs a ILWD std::string with the specified contents, name, comment and
// units.
// The LdasString element will contain n count of strings.
//
//!param: const std::string s - The array of strings.
//!param: size_type n - Number of entries in the data content array.   
//!param: const std::string& name - The name of the element. Default is an empty    +
//!param:    string.
//!param: const std::string& comment - A comment. Default: An empty string.
//!param: const size_type numUnits - Number of entries in the units array.   
//!param: const std::string* units - An array of units associated with data.        +
//!param:    Default is an empty string.
//
//!exc: std::bad_alloc - Memory allocation failed.
//      
LdasString::
LdasString( const std::string s[], size_type n, const std::string& name,
	    const std::string& comment, const size_type numUnits, 
	    const std::string* units ) 
    : LdasFormatElement( name, comment ), std::vector< std::string >(),
      mNUnits( numUnits ), mUnits( 0 )
{
   try
   {
      if( mNUnits > 0 )
      {
         mUnits = new std::string[ mNUnits ];
         if( units != 0 )            
         {
            for( size_type i = 0; i < mNUnits; ++i )
            {
               mUnits[ i ] = units[ i ];
            }
         }
      }
      for ( size_type i = 0; i < n; ++i )
      {
  	 ensure_printable( s[ i ] );
         push_back( s[ i ] );
      }
   }
   catch(...)
   {
      delete[] mUnits;
      mUnits = 0;   
      throw;
   }
}


//-----------------------------------------------------------------------------
//
//: Constructor.
//
// This constructs a ILWD string with the specified vector of contents, name, 
// comment and units.
//
//!param: const std::vector< std::string >& s - The vector of strings representing data  +
//!param:    content.
//!param: const std::string& name - The name of the element. Default is an empty    +
//!param:    string.
//!param: const std::string& comment - A comment. Default: An empty string.
//!param: const size_type numUnits - Number of entries in the units array.   
//!param: const std::string* units - An array of units associated with data.        +
//!param:    Default is an empty string.
//
//!exc: std::bad_alloc - Memory allocation failed.
//      
LdasString::LdasString( const std::vector< std::string >& s, const std::string& name,
                        const std::string& comment, const size_type numUnits,
                        const std::string* units )
    : LdasFormatElement( name, comment ), std::vector< std::string >( s ),
      mNUnits( numUnits ), mUnits( 0 )
{
    for ( std::vector< std::string >::const_iterator si = s.begin();
	  si != s.end(); ++si )
    {
	ensure_printable( *si );
    }
   
    if( mNUnits > 0 )
    {
       mUnits = new std::string[ mNUnits ];
   
       try
       {
          if( units != 0 )            
          {
             for( size_type i = 0; i < mNUnits; i++ )
             {
                mUnits[ i ] = units[ i ];
             }
          }
       }
       catch(...)
       {
          delete[] mUnits;
          mUnits = 0;   
          throw;
       }
    }
}


//-----------------------------------------------------------------------------
//
//: Constructs this element from a stream, given the attributes.
//
// This constructor is used when the start-tag has already been read.  For
// example, if we do not know what type of element is coming next then the
// start-tag is read from the stream, the type of object identified, and
// the Reader&/Attributes& constructor is called.
//
//!todo: - Have the Reader class ID objects and pushback the start-tag.  Then
//!todo: we wouldn't need this method.
//
//!param: Reader& r - The reader to read from.
//!param: const Attributes& att - The attributes for this object.
//
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//!exc: bad_alloc - Memory allocation failed.
//
LdasString::LdasString( Reader& r, const Attributes& att )
        : LdasFormatElement(), std::vector< std::string >(),
          mNUnits( 0 ), mUnits( 0 )
{
    read( r, att );
}

   
//-----------------------------------------------------------------------------
//
//: Destructor.
//
LdasString::~LdasString()
{
   delete[] mUnits;
   mUnits = 0;   
}


//-----------------------------------------------------------------------------
//: Gets the element ID of this type of object.
//
// The element ID is used to determine what type an object is without
// having to resort to RTTI or comparing identifiers.
//
//!return: ElementId - The element id (an enumeration).
//
ILwd::ElementId LdasString::getElementId() const
{
    return ID_LSTRING;
}

   
//-----------------------------------------------------------------------------
//
//: Get the identifier.
//
// Returns the identifier for this element.
//
//!return: const std::string& - The identifier.
//
const std::string& ILwd::LdasString::getIdentifier() const
{
    return getIdentifierStatic();
}
   
   
//-----------------------------------------------------------------------------
//
//: Get the units for a dimension.
//
//!param: size_type n - The dimension whose units is requested.
//
//!return: const std::string - the units for the dimension.
//
//
std::string ILwd::LdasString::getUnits( size_type n ) const
{
    if ( n >= mNUnits )
    {
        return "";
    }

    return mUnits[ n ];
}

//-----------------------------------------------------------------------------
//
//: Create a copy of this element.
//
// This method allocates memory and creates a copy of this string object.  This
// is a virtual method which allows one to copy an LdasElement without knowing
// what its exact type is.
//
//!return: LdasString* - A pointer to a newly allocated copy of this object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
ILwd::LdasString* ILwd::LdasString::createCopy() const
{
    return new LdasString( *this );
}


//-----------------------------------------------------------------------------
//
//: Not equal comparison operator.
//
// This compares this object to another LdasContainer for inequality.
//
//!param: const LdasElement& ls - The object to compare with.
//
//!return: bool - true if the containers are not equal.
//
bool ILwd::LdasString::operator!=( const LdasString& ls )
    const
{
    return !operator==( ls );
}
   
   
//-----------------------------------------------------------------------------
//   
//: Gets the size of the string.
//
// The size is used in calculating the length of transmission.
//
//!return: size_type - The number of uncompressed bytes.
//
LdasString::size_type LdasString::getSize(void) const
{
  size_type	dims( size() );
  size_type	sz(0);

  for ( const_iterator iter = begin(); iter != end(); ++iter )
  {
    sz += protect(*iter).size();
  }
   
  if (dims>0) sz += 2*(dims-1);
  return sz;
}

   
//-----------------------------------------------------------------------------
//
//: Assignment Operator.
//
//!param: const LdasString& ls - The object to assign from.
//
//!return: const LdasString& - A reference to this object.
//
//!exc: std::bad_alloc: Memory allocation failed.
//
const LdasString& LdasString::operator=( const LdasString& ls )
{
    if ( this != &ls )
    {
        LdasFormatElement::operator=( ls );
        std::vector< std::string >::operator=( ls );

        delete[] mUnits;
        mUnits = 0;
        mNUnits = ls.mNUnits;
        if( mNUnits > 0 )
        {
           mUnits = new std::string[ mNUnits ];
           for( size_type i = 0; i < mNUnits; i++ )
           {
              mUnits[ i ] = ls.mUnits[ i ];
           }
        }
    }

    return *this;
}

//-----------------------------------------------------------------------------
//
//: Concatination Operator.
//
//!param: const LdasString& ls - The object to concat from.
//
//!return: const LdasString& - A reference to this object.
//
//!exc: std::bad_alloc: Memory allocation failed.
//
const LdasString& LdasString::operator+=( const LdasString& ls )
{
  //=====================================================================
  // Sanity checks
  //=====================================================================

  if (mNUnits != ls.mNUnits)
  {
     ostringstream msg;
     msg << "LdasString::operator+=(): inconsistent dimension for unit attribute: "
         << "destination dim = " << mNUnits 
         << " source dim = " << ls.mNUnits;
     
     throw std::runtime_error( msg.str() );
  }
   
  for( size_type i = mNUnits; i-- != 0; )
  {
     if ( mUnits[ i ] != ls.mUnits[ i ] )
     {
        ostringstream msg;
        msg << "LdasString::operator+=(): inconsistent unit attribute "
            << "for dimension index = " << i 
            << ": destination unit = " << mUnits[ i ]
            << " source unit = " << ls.mNUnits;
        
        throw std::runtime_error( msg.str() );   
     }
  }
   
   
  //=====================================================================
  // The real concatination begins
  //=====================================================================
  size_type	ls_size(ls.size());

  mUnits->reserve(size() + ls_size);	// Increase the number of spaces
  for (size_type i = 0; i < ls_size; i++)
  {
     push_back(ls[i]);
  }
    
  return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another array for equality.
//
//!param: const LdasElement& ls - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool LdasString::operator==( const LdasString& ls ) const
{
    if ( (!LdasFormatElement::operator==( ls )) || (size() != ls.size()) )
    {
        return false;
    }

    const_iterator iter1 = begin();
    const_iterator iter2 = ls.begin();
    
    while( iter1 != end() && *iter1 == *iter2 )
    {
        ++iter1;
        ++iter2;
    }
    
    //return ( iter1 == end() );
    bool r = ( iter1 == end() );
    
    for( size_type i = mNUnits; ( i-- != 0 ) && ( r == true ); )
    {
       r = ( mUnits[ i ] == ls.mUnits[ i ] );
    }
    
    return r;
}
   
//-----------------------------------------------------------------------------
//
//: Get the string.
//
// This returns the string which this object represents.
//
//!return const string& - The string.
//
std::string LdasString::getString() const
{
    std::string res;

    
    for ( const_iterator iter = begin(); iter != end(); ++iter )
    {
        if ( iter != begin() )
        {
            res += "\\,";
        }
        res += protect(*iter);
    }
    
    return res;
}


//-----------------------------------------------------------------------------
//
//: Set the string.
//
// This sets the string which this LdasString object is supposed to represent.
//
//!param: const std::string& s - The string.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
void LdasString::setString( const std::string& s )
{
    erase( begin(), end() );
    split(s.c_str());
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Reads this element from a stream.
//
// This method is called when the
// user knows for certain that the next object is this type.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
void LdasString::read( Reader& r )
{
    StartTag st( r );
    if ( st.getIdentifier() != getIdentifier() )
    {
        std::string msg( "The start-tag for this object was not found, expecting <" );
        msg += getIdentifier();
        msg += ">, found <";
        msg += st.getIdentifier();
        msg += ">.";
   
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_START_TAG, msg );
    }

    read( r, st.getAttributes() );
    return;
}

   
//-----------------------------------------------------------------------------
//
//: Reads this element from a stream.
//
//!param: Reader& r - The reader to read from.
//!param: const Attributes& att - The attributes for the array.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the array was invalid.
//
void LdasString::read( Reader& r, const Attributes& att )
{
    erase(begin(), end());
    LdasFormatElement::readAttributes( att );
    
    size_type sz;    
    Attributes::const_iterator iter = att.find( "size" );
    if ( iter != att.end() )
    {
        sz = iter->second.getValue< size_type >();
    }
    else
    {
        sz = 0;
    }
    
    size_type dims;    
    iter = att.find( "dims" );
    if ( iter != att.end() )
    {
        dims = iter->second.getValue< size_type >();
    }
    else
    {
        dims = 1;
    }

    delete[] mUnits;
    mUnits = 0;
    mNUnits = 0;

    iter = att.find( "units" );
    if ( iter != att.end() )
    {
        std::string tmp = iter->second.getString();
        if ( tmp.size() != 0 )
        {
            // Get units dimention:
            std::string::size_type s( 0 );
            while( ( ( s = tmp.find_first_of( ",", s ) ) != std::string::npos ) )
            {
               mNUnits++; 
               s++;
            }
            mNUnits++;            
            mUnits = new std::string[ mNUnits ];

            s = 0;
            std::string::size_type e( 0 );
            size_type i( 0 );
            while( ( s != std::string::npos ) && ( i < mNUnits ) )
            {
                e = tmp.find( ',', s );
                if ( e != std::string::npos )
                {
                    mUnits[ i++ ] = tmp.substr( s, e );

                    if ( e + 1 != tmp.size() )
                    {
                        s = e + 1;
                    }
                }
                else
                {
                    mUnits[ i++ ] = tmp.substr( s );
                    s = std::string::npos;
                }
            }
        }
    }

    if ( ( getWriteFormat() == BINARY &&
           getWriteCompression() == NO_COMPRESSION ) ||
         ( getWriteFormat() == ASCII ) )
    {
        char* s = new char [ sz + 1 ];    
        r.read( s, sz );
        s[ sz ] = 0;

	split(s);

	delete[] s;
        s = 0;
    }
    else
    {
        char* buffer( 0 );
        size_type bytes( 0 );
        
        if ( getWriteFormat() == BINARY )
        {
            Attributes::const_iterator iter = att.find( "bytes" );
            if ( iter == att.end() )
            {
                throw ILWD_FORMATEXCEPTION_INFO(
                    Errors::MISSING_ATTRIBUTE,
                    "Bytes must be specified with format='binary' and "
                    "compression." );
            }
            else
            {
                try
                {
                    bytes = iter->second.getValue< size_type >();
                }
                ADD_ILWD_FORMATEXCEPTION_INFO( Errors::BAD_ATTRIBUTE,
                                    "Couldn't parse 'bytes' attribute." );
            }
            
            buffer = new char[ bytes ];
            r.read( buffer, bytes );
        }
        else
        {
            std::stringbuf oss;
            // Read all characters up to the '<' into stringbuf
            r.getStream().get( oss, '<' );
   
            // How many characters were just read
	    bytes = r.getStream().gcount();
   
            buffer = new char[ bytes+1 ];
   
            if( bytes )
            {
                const string oss_string( oss.str() );
                copy( oss_string.begin(), oss_string.end(), buffer );
            }
	    buffer[ bytes ] = '\0';
	    if ( bytes == 0 )
	    {
		// Make sure that it is not registered as an error if no
		// bytes were read.
		r.getStream().clear();
	    }
        }
        
        char* tmp = convertFromFormat< char >( buffer, bytes, sz );
	tmp[ sz -1 ] = '\0';
        
        delete[] buffer;
        buffer = 0;
        
	split(tmp);
        
        delete[] tmp;
        tmp = 0;
    }

  
    if ((dims == 1) && (getSize() == 0))
    {
      //-----------------------------------------------------------------
      // This is a very special case. The Dims attribute says that there
      //   should be one element, but the element is the empty string.
      //   Push an empty string here.
      //-----------------------------------------------------------------
      push_back("");
    }
   
    if( dims != size() ) 
    {
	std::ostringstream	msg;
   
	msg << "Inconsistent lstring size: attribute=" 
	    << dims
	    << " vs. vector<string>.size="
	    << size();   
   
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_ATTRIBUTE, msg.str( ) );
    }
   
    // :TODO: Check to see if the dims value agrees with our size.
    // :TODO:   If it doesn't, throw an exception about wrong size.

    EndTag et( r );

    if ( et.getIdentifier() != getIdentifier() )
    {
        std::ostringstream msg;
        msg << "The wrong end-tag for this object was found.  Expecting "
            << EndTag( getIdentifier() ) << ", found " << et << '.';
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_END_TAG, msg.str() );
    }

    for ( std::vector< std::string >::const_iterator si = begin();
	  si != end();
	  si++ )
    {
	ensure_printable( *si );
    }
   
   
    return;
}

   
//-----------------------------------------------------------------------------
//
//: Writes the object to a stream.
//
// Writes the array to a stream with the given format and compression.
//
//!param: ostream& stream - The stream to write to.
//
//!exc: exception - An unknown error occurred.
//
void LdasString::writeData( std::ostream& stream ) const
{
    Attributes att;

    size_type dims( size() );
    if ( dims != 1 )
    {
        att.addAttribute( "dims", dims );
    }

    size_type sz( getSize() );
    if ( sz != 0 )
    {
        att.addAttribute( "size", sz );
    }

    // Units
    bool units( false );
    for( size_type i = 0; i < mNUnits; ++i )     
    {
       if( mUnits[ i ].size() != 0 )
       {
          units = true;
	  break;
	}
    }
   
    if( units )
    {
       // No data delimiter for very first field       
       string units_string( mUnits[ 0 ] );

       for( size_type i = 1; i < mNUnits; ++i )
       {
          units_string += ',';
          units_string += mUnits[ i ];
       }

       att.addAttribute( "units", units_string );
    }

    
    if ( ( getWriteFormat() == BINARY &&
           getWriteCompression() == NO_COMPRESSION ) ||
         ( getWriteFormat() == ASCII ) )
    {
	LdasFormatElement::writeAttributes( att );
        stream << StartTag( getIdentifier(), att );
        const_iterator iter( begin() ), end_iter( end() );
        
        if( iter != end_iter )
        {
           // No data delimiter for very first field.
           stream << protect( *iter );
           ++iter;
        }  
   
        while( iter != end_iter )
        {
           stream << "\\," << protect( *iter );
           ++iter;
        }
    }
    else
    {
        char* buffer( 0 );
        size_t bytes( 0 );

        if ( size() == 1 )
        {
            buffer = convertToFormat< char >(
                (*this)[ 0 ].c_str( ), (*this)[ 0 ].length( ), &bytes );
        }
        else
        {
            string data_string;
   
            const_iterator iter( begin() ), end_iter( end() );
            if( iter != end_iter )
            {
               // No data delimiter before first element
               data_string += *iter;
               ++iter;
            }
   
            while( iter != end_iter )
            {
               data_string += "\\,";
               data_string += *iter;
               ++iter; 
            }

            buffer = convertToFormat< char >( data_string.c_str(), 
                                              data_string.size(), &bytes );
        }
        
        if ( getWriteFormat( ) == BINARY )
        {
            att.addAttribute( "bytes", bytes );
        }
	else
	{
	    --bytes;
	}

	LdasFormatElement::writeAttributes( att );
        stream << StartTag( getIdentifier(), att );
        stream.write( buffer, bytes );
   
        delete[] buffer;
        buffer = 0;
    }

    stream << EndTag( getIdentifier() );
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Splits the string.
//
// This method splits the string which this LdasString object is representing.
//
//!param: const char* String - The string.
//
//!exc: std::bad_alloc - Memory allocation failed.
//   
void ILwd::LdasString::split( const char* String )
{
  const char* index = String;
  const char* pos = index;
  std::string field("");

  while(*pos)
  {
    if (*pos == BACK_SLASH_CHAR)
    {
      if ((pos[1] == '\0') || (pos[1] == ','))
      {
	push_back( field );
	if (pos[1] == '\0')
	{
	  break;
	}
	pos += 2;
	index = pos;
	field = "";
	if (*pos == '\0') push_back("");
	continue;
      }
      else if ( pos[1] == BACK_SLASH_CHAR )
      {
	// Literally take this backslash
	pos++;
      }
    }
    field += *pos;
    pos++;
    if (*pos == '\0')
    {
      push_back( field );
      break;
    }
  }
   
  return;
}

// ObjectSpace
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::LdasString*), Stream::ILwd::LDASSTRING,
                 (ILwd::LdasFormatElement*) )

void os_write( os_bstream& stream, const LdasString& o )
{
  char		key;
  os_device*	dev = stream.adapter().device();

  dev->get_flush_lock(&key, 8 * 1024);
  try {
    os_write( stream, dynamic_cast< const ILwd::LdasFormatElement& >( o ) );

    stream << o.mNUnits;
    for ( LdasString::size_type i = 0; i < o.mNUnits; ++i )
    {
      stream << o.mUnits[ i ];
    }

#ifdef FAST_SOCKET
    // How many elements
    os_int32_type	size = o.size();
    stream.write(size);
    
    // How large is each element
    os_int32_type*	esize = (os_int32_type*)calloc(size + 1, sizeof(os_int32_type));
    int		offset(0);
    os_int32_type	max(0);

    for ( LdasString::const_iterator iter = o.begin();
	  iter != o.end(); iter++, offset++ )
    {
      register os_int32_type	len = (*iter).length();
      
      if (len > max) max = len;
      esize[offset] = len;
    }
    esize[size] = max;
    stream.write_chunk(os_protocol::int32, esize, size+1);
  
    // Send each element
    offset = 0;
    for ( LdasString::const_iterator iter = o.begin();
	  iter != o.end(); iter++, offset++)
    {
      if (esize[offset])
      {
	dev->write_exactly((*iter).c_str(), esize[offset]);
      }
    }
    free(esize);
#else
    stream << o.size();
    for ( LdasString::const_iterator iter = o.begin();
	  iter != o.end(); ++iter )
    {
      stream << *iter;
    }
#endif
  }
  catch (...)
  {
    dev->rel_flush_lock(&key);	// Release the lock
    throw;			// Re-throw the exception
  }
  dev->rel_flush_lock(&key);
   
  return;
}

void os_read( os_bstream& stream, LdasString& o )
{
  char		key;
  os_device*	dev = stream.adapter().device();
  os_read( stream, dynamic_cast< ILwd::LdasFormatElement& >( o ) );
    
  delete[] o.mUnits;
  o.mUnits = 0;
  o.mNUnits = 0;
  
  dev->get_flush_lock(&key, 8 * 1024);

  try {
    stream >> o.mNUnits;
    if( o.mNUnits > 0 )
    {
      o.mUnits = new std::string[ o.mNUnits ];
      for ( LdasString::size_type i = 0 ; i < o.mNUnits; i++ )
      {
	stream >> o.mUnits[ i ];
      }
    }
 
#ifdef FAST_SOCKET
    // How many elements
    os_int32_type	size;
    stream.read(size);

    // How large is each element
    os_int32_type*	esize = (os_int32_type*)calloc(size + 1, sizeof(os_int32_type));

    stream.read_chunk(os_protocol::int32, esize, size+1);
  
    // Read each element
    o.erase( o.begin(), o.end() );

    AutoArray< char > buf( new char[ esize[size]+1 ] );

    for (os_int32_type x = 0; x < size; x++)
    {
      register int	len(esize[x]);
      
      if (len)
      {
         dev->read_exactly(buf.get(), len);
      }
      buf[len] = '\0';
      o.push_back(std::string(buf.get()));
    }

    free(esize);
#else
    o.erase( o.begin(), o.end() );
    
    size_type n;
    stream >> n;
    std::string s;
    for ( ; n != 0; --n )
    {
      stream >> s;
      o.push_back( s );
    }
#endif
  }
  catch (...)
  {
    dev->rel_flush_lock(&key);	// Release the lock
    throw;				// Re-throw the exception
  }
  dev->rel_flush_lock(&key);
   
  return;
}

#endif // HAVE_LIBOSPACE

std::string
protect(const std::string& source)
{
    std::string	retval;

    for (std::string::const_iterator c = source.begin(), end_c = source.end();
         c != end_c; ++c)
    {
	if ( *c == '\\' &&
             ( source.size() == 1 || !isdigit(*(c+1)) ) )
	{
	    retval += '\\';
	}
	retval += *c;
    }
    return retval;
}

void
ensure_printable( const std::string& Source )
{
    for ( std::string::const_iterator	c = Source.begin();
	  c != Source.end();
	  c++ )
    {
	if ( ! isprint( *c ) && ! isspace( *c ) )
	{
	    throw ILWD_FORMATEXCEPTION( ILwd::Errors::OUT_OF_RANGE );
	}
    }
   
    return;
}
