#ifndef LdasStringHH
#define LdasStringHH

//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif


// ObjectSpace Includes
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE


// Local Includes
#include "ldasformatelement.hh"


/// \cond DOXYGEN_IGNORE
namespace ILwd
{
    class LdasString;
}
/// \endcond

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::LdasString& base );
void os_read( os_bstream& stream, ILwd::LdasString& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: An ILWD string object.
//
// This class represents the ILWD string class.  It is designed to store an
// array of variable length strings.
//
// <p>The ILWD identifier for the string is 'lstring'.  The lstring element has
// the following attributes:
//
// <p>name - A colon-delimited list of names for the element. See the 
//           LdasElement class for more information. Default: An empty string.
// <p>comment - A comment. Default: An empty string.           
// <p>dims - The number of string objects stored in the class. This must be a
//           non-negative integer. If dims=0 then size must be equal to 0.
//           Default: 1.          
// <p>size - The total number of characters between the start and end-tags.
//           This includes new-lines, tabs, delimiter sequences, etc. This is
//           a non-negative integer. There is no default.
// <p>format - The format the string is stored in. This is one of ASCII, Base64,
//             or Binary. Default: ASCII.
// <p>compression - The compression used on the data. Default: None.
// <p> units - Units associated with data. If all data has the same units 
//             associated with it, then only one units field is assigned to
//             the data. Default: An empty string.
//
// <p>Internally, storage is provided by the STL vector class ( vector<string> )
// from which this class is derived.
//
// Constructors are provided for an array of C or C++ strings, a single C++
// string, or a vector of strings.  Furthermore, copy & input constructors are 
// also provided.
//
// Reading from a stream follows these steps:
//
//   1) The StartTag is constructed from the stream.  The identifier is checked
//      to make sure it is an 'lstring'.
//   2) The attributes are parsed.
//   3) The entire contents of the lstring are read into a temporary buffer.
//      The 'size' attribute is used to perform a block read.
//   4) The format is converted to ASCII if necessary.
//   5) A regular expression is used to match individual strings delimited by
//      '\,'.  The matched strings are pushed into the vector.  Currently,
//      the dims attribute is ignored.
//   6) The end-tag is read.
//
class ILwd::LdasString
  : public ILwd::LdasFormatElement,
    public std::vector< std::string >
{
private:
    static const std::string mIdent;

public:
    typedef INT_4U size_type;

    /* Constructors / Destructor */
    explicit LdasString( const std::string& s = "", const std::string& name = "",
                         const std::string& comment = "", const std::string& units = "" );

    LdasString( const char* s[], size_type n, const std::string& name = "",
                const std::string& comment = "", const size_type numUnits = 0,
                const std::string* units = 0 );

    LdasString( const std::string s[], size_type n, const std::string& name = "",
                const std::string& comment = "", const size_type numUnits = 0,
                const std::string* units = 0 );

    explicit LdasString( const std::vector< std::string >& s, const std::string& name = "",
                         const std::string& comment = "", const size_type numUnits = 0,
                         const std::string* units = 0 );

    LdasString( const LdasString& ls );
   
    LdasString( Reader& r );
    LdasString( Reader& r, const Attributes& att );
    virtual ~LdasString();

    /* Operator Overloads */
    const LdasString& operator=( const LdasString& ls );
   
    //
    //: Concatination Operator.
    //
    //!param: const LdasString& ls - The object to concatenate from.
    //
    //!return: const LdasString& - A reference to this object.
    //
    //!exc: std::bad_alloc - Memory allocation failed.
    //   
    const LdasString& operator+=( const LdasString& ls );
  
    bool operator==( const LdasString& Element ) const;
    bool operator!=( const LdasString& Element ) const;

    /* Accessors */
    static const std::string& getIdentifierStatic();
    const std::string& getIdentifier() const;
    size_type getSize( void ) const;
    std::string getString() const;
    ElementId getElementId() const;
    size_type getNUnits() const;
    std::string getUnits( size_type n ) const;
    
    /* Mutators */
    void setString( const std::string& s );
    
    /* Copy */
    LdasString* createCopy() const;

    /* I/O */
    virtual void read( Reader& r );
    virtual void read( Reader& r, const Attributes& att );

private:

    virtual void writeData( std::ostream& stream ) const;
    void split( const char* String );

    size_type mNUnits;
    std::string* mUnits;

#if HAVE_LIBOSPACE
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const LdasString& - A reference to the object to write to  +
    //!param:    the stream.
    //      
    friend void ::os_write( os_bstream&, const LdasString& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: LdasString& - A reference to the object to read from the stream.
    //      
    friend void ::os_read( os_bstream&, LdasString& );
#endif // HAVE_LIBOSPACE
};



//-----------------------------------------------------------------------------
// Inline Methods
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//
//: Get the identifier.
//
// This returns the identifier for this element.  This is the static version.
//
//!return: const std::string& - The identifier.
//
inline const std::string& ILwd::LdasString::getIdentifierStatic()
{
    return mIdent; // "lstring";
}


//-----------------------------------------------------------------------------
//
//: Get the dimension for units attribute.
//
//!return: size_type& - The number of unit fields in Unit attribute.
//
inline ILwd::LdasString::size_type ILwd::LdasString::
getNUnits() const
{
    return mNUnits;
}


//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------


#ifdef HAVE_LIBOSPACE

void os_write( os_bstream&, const ILwd::LdasString& );
void os_read( os_bstream&, ILwd::LdasString& );

OS_POLY_CLASS( ILwd::LdasString )
OS_STREAM_OPERATORS( ILwd::LdasString )

#endif // HAVE_LIBOSPACE


#endif // LdasStringHH
