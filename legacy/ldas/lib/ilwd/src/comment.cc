#include "ilwd/config.h"

#include <sstream>
   
#include "comment.hh"


using ILwd::Comment;
using namespace std;

  
//-----------------------------------------------------------------------
/// \brief ( Default ) Constructor.
///
/// \param[in] c
///     The comment.
///
/// \exception std::bad_alloc
///     Memory allcoation failed
/// \exception FormatException
///     Invalid format specified.
//-----------------------------------------------------------------------
Comment::Comment( const std::string& c )
  try
    : mComment( c )
{
  if ( !isComment( mComment ) )
  {
    throw ILWD_FORMATEXCEPTION_INFO(
				    Errors::INVALID_CHAR,
				    "An invalid character appeared in the comment." );
  }
}
ADD_ILWD_FORMATEXCEPTION_INFO(
			      Errors::INVALID_COMMENT, "The comment is not valid ILWD." )
   
   
//-----------------------------------------------------------------------
/// \brief Copy constructor.
///
/// \param[in] c
///     The object to copy from.
///
/// \exception std::bad_alloc
///     Memory allocation failed.
//-----------------------------------------------------------------------
  Comment::Comment( const Comment& c )
    : Base( c ),
      mComment( c.mComment )
{}


//-----------------------------------------------------------------------
/// \brief Input constructor.
///
/// Reads an XML comment from a reader.
///
/// \param[out] r
///     The reader to read from.
///
/// \exception std::bad_alloc
///     Memory allocation failed.
/// \exception StreamException
///     An error reading from the stream occurred.
/// \exception FormatException
///     The format for the PI was invalid.
//-----------------------------------------------------------------------
Comment::Comment( Reader& r )
{
  read( r );
}


//-----------------------------------------------------------------------
/// \brief Destructor.
//-----------------------------------------------------------------------
Comment::~Comment()
{}
   
   
//-----------------------------------------------------------------------
/// \brief Assignment operator.
///
/// \param[in] c
///     The object to assign from.
///
/// \return
///     A reference to this object.
///
/// \exception std::bad_alloc
///     Memory allocation failed.
//
//-----------------------------------------------------------------------
const Comment& Comment::operator=( const Comment& c )
{
  if ( this != &c )
  {
    Base::operator=( c );
    mComment = c.mComment;
  }
    
  return *this;
}
   
   
//-----------------------------------------------------------------------
/// \brief Equal comparison operator.
///
/// This compares this object to another for equality.
///
/// \param[in] c
///     The object to compare with.
///
/// \return
///     true if the objects are equal.
//-----------------------------------------------------------------------
bool Comment::operator==( const Comment& c ) const
{
  return ( Base::operator==( c ) && mComment == c.mComment );
}
   
   
//-----------------------------------------------------------------------
/// \brief Sets the comment.
///
/// \param[in] c
///     The comment.
///
/// \exception std::bad_alloc
///     Memory allocation failed.
/// \exception FormatException
//-----------------------------------------------------------------------
void Comment::setComment( const std::string& c )
{
  if ( isComment( c ) )
  {
    mComment = c;
  }
  else
  {
    throw ILWD_FORMATEXCEPTION_INFO(
				    Errors::INVALID_CHAR,
				    "An invalid character appeared in the comment." );
  }
   
  return;
}
   
   
//-----------------------------------------------------------------------
/// \brief Reads class id. 
///
/// This method gets class id. 
///
/// \return
///     ID.
//-----------------------------------------------------------------------
ILwd::ClassType Comment::getClassId() const
{
  return ID_COMMENT;
}   
   

//-----------------------------------------------------------------------
/// \brief Writes a comment.
///
/// \param[out] stream
///     The stream to write to.
///
/// \exception std::exception
///     An unknown exception occurred.
//-----------------------------------------------------------------------
void Comment::write( std::ostream& stream ) const
{
  stream << "<!--" << mComment << "-->";
  return;
}


//-----------------------------------------------------------------------
/// \brief Input constructor.
///
/// Reads an ILWD from a reader.
///
/// \param[out] r
///     The reader to read from.
///
/// \exception StreamException
///     An error reading from the stream occurred.
/// \exception FormatException
///     The format for the PI was invalid.
//-----------------------------------------------------------------------
void Comment::read( Reader& r )
  try
  {
    ostringstream s;
    
    if ( !r.readString( "<!--" ) )
    {
      throw ILWD_FORMATEXCEPTION_INFO(
				      Errors::MISSING_DELIMITER, "The comment start delimiter, '<!--' was "
				      "not found." );
    }

    int c;
    bool end = false;

    do
    {
      c = r.get(); // May throw StreamException
        
      switch( c )
      {
      case '-':
	c = r.get(); // May throw StreamException
	switch( c )
	{
	case '-':
	  if ( !r.readChar( '>' ) )
	  {
	    mComment = s.str();

	    throw ILWD_FORMATEXCEPTION_INFO(
					    Errors::INVALID_CHAR, "An invalid character "
					    "appeared in the comment." );
	  }
	  else
	  {
	    end = true;
	  }
	  break;
                        
	default:
                        
	  s << '-' << c;
	}
	break;
                
      default:
                
	s << char(c);
      }
    }
    while( !end );

    mComment = s.str();

    return;
  }
ADD_ILWD_FORMATEXCEPTION_INFO(
			      Errors::INVALID_COMMENT, "The comment is not valid ILWD." )


//-----------------------------------------------------------------------
/// \brief Returns whether or not the given string is a valid comment.
///
/// \param[in] s
///     The string to check.
///
/// \return
///     True if the string is a valid comment, false otherwise.
//-----------------------------------------------------------------------
  bool Comment::isComment( const std::string& s )
{
  if ( ( s.find_first_of( "--" ) == std::string::npos )
       && ( s[ s.size()-1 ] != '-' ) )
  {
    return true;
  }
  else
  {
    return false;
  }
}


// ObjectSpace
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::Comment*), Stream::ILwd::COMMENT, (ILwd::Base*) )

  void os_write( os_bstream& stream, const Comment& o )
{
  os_write( stream, (ILwd::Base&)o );
  stream << o.mComment;
}

void os_read( os_bstream& stream, Comment& o )
{
  os_read( stream, (ILwd::Base&)o );
  stream >> o.mComment;
}

#endif // HAVE_LIBOSPACE
