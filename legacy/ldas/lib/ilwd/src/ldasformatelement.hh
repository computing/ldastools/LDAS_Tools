/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef IlwdLdasFormatElementHH
#define IlwdLdasFormatElementHH

//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif


// ObjectSpace Includes
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE


// Local Includes
#include "ldaselement.hh"

/// \cond DOXYGEN_IGNORE
namespace ILwd
{
    class LdasFormatElement;
}
/// \endcond

   
//-----------------------------------------------------------------------------
//   
//: Represents LDAS formatted element.
//   
class ILwd::LdasFormatElement
    : public ILwd::LdasElement,
      public ILwd::Style
{
public:

    /* Constructors / Destructor */
    explicit LdasFormatElement(
        const std::string& name = "", const std::string& comment = "" );
    LdasFormatElement( const LdasFormatElement& e );
    virtual ~LdasFormatElement();

    /* Operator Overloads */
    const LdasFormatElement& operator=( const LdasFormatElement& e );

    bool operator==( const LdasFormatElement& e ) const;
    bool operator!=( const LdasFormatElement& e ) const;

    /* I/O */
    virtual void write( std::ostream& stream, Format f,
                        Compression c = USER_COMPRESSION ) const;
    virtual void write( std::ostream& stream ) const;

    /* Attribute manipulation */
    void writeAttributes( Attributes& att ) const;
    void readAttributes( const Attributes& att );

private:

    virtual void writeData( std::ostream& stream ) const = 0;

#if HAVE_LIBOSPACE && 0
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const LdasFormatElement& - A reference to the object to write to  +
    //!param:    the stream.
    //   
    friend void os_write( os_bstream&, const LdasFormatElement& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: LdasFormatElement& - A reference to the object to read      + 
    //!param:    from the stream.
    //   
    friend void os_read( os_bstream&, LdasFormatElement& );
#endif // HAVE_LIBOSPACE
};


//-----------------------------------------------------------------------------
//
//: Not equal comparison operator.
//
// This compares this object to another LdasContainer for inequality.
//
//!param: const LdasElement& e - The object to compare with.
//
//!return: bool - true if the containers are not equal.
//
inline bool ILwd::LdasFormatElement::
operator!=( const LdasFormatElement& e ) const
{
    return !operator==( e );
}

inline void ILwd::LdasFormatElement::
write( std::ostream& stream ) const
{
    return ILwd::LdasElement::write( stream );
}

#if HAVE_LIBOSPACE
void os_write( os_bstream&, const ILwd::LdasFormatElement& );
void os_read( os_bstream&, ILwd::LdasFormatElement& );
#endif /* HAVE_LIBOSPACE */

#endif // IlwdLdasFormatElementHH
