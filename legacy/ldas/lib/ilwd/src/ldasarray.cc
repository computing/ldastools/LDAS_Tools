#include "ilwd/config.h"

#include <string>

#include "general/types.hh"
#include "general/util.hh"

#include "ilwd/ldasarray.hh"

// checkByteOrder specializations   
template<> void
ILwd::LdasArray< COMPLEX_8 >::checkByteOrder( const ByteOrder& byteOrder ) 
{
   using namespace std;   

   if ( getWriteFormat() != ASCII )
   {
      if ( byteOrder != HostOrder )
      {
         size_type num( getNData() ); 
         REAL_4* img( new REAL_4[ num ] );
         REAL_4* real( 0 );
         try
         {
            real = new REAL_4[ num ];   
         }
         catch( bad_alloc& )
         {
            delete[] img;
            img = 0;
            throw;
         }
   
         for( unsigned int i = 0; i < num; ++i )
         {
             img[ i ] = mData[ i ].imag();
             real[ i ]= mData[ i ].real();
         }   
        
         reverse< sizeof( REAL_4 ) >( img, num );
         reverse< sizeof( REAL_4 ) >( real, num );   
    
         for( unsigned int i = 0; i < num; ++i )
         {
            mData[ i ] = COMPLEX_8( real[ i ], img[ i ] );
         }

         delete[] img;
         img = 0;
   
         delete[] real;
         real = 0;
      }
   }
   
   return;
}

   
template<> void
ILwd::LdasArray< COMPLEX_16 >::checkByteOrder( const ByteOrder& byteOrder )
{
   using namespace std;   

   if ( getWriteFormat() != ASCII )
   {
      if ( byteOrder != HostOrder )
      {
         size_type num( getNData() ); 
         REAL_8* img( new REAL_8[ num ] );
         REAL_8* real( 0 );
         try
         {
            real = new REAL_8[ num ];   
         }
         catch( bad_alloc& )
         {
            delete[] img;
            img = 0;
            throw;
         }
   
         for( unsigned int i = 0; i < num; ++i )
         {
             img[ i ] = mData[ i ].imag();
             real[ i ]= mData[ i ].real();
         }   
        
         reverse< sizeof( REAL_8 ) >( img, num );
         reverse< sizeof( REAL_8 ) >( real, num );   
    
         for( unsigned int i = 0; i < num; ++i )
         {
            mData[ i ] = COMPLEX_16( real[ i ], img[ i ] );
         }

         delete[] img;
         img = 0;
   
         delete[] real;
         real = 0;
      }
   }
   
   return;
}
   
#include "ldasarray.icc"

using namespace std;   

//-----------------------------------------------------------------------
// Instantiate the classes so we don't have to do so much inlining.
//-----------------------------------------------------------------------

// mIdentifier specializations
namespace ILwd
{
  template<> const string LdasArray< CHAR >::mIdentifier = "char";
  template<> const string LdasArray< CHAR_U >::mIdentifier = "char_u";
  template<> const string LdasArray< INT_2S >::mIdentifier = "int_2s";
  template<> const string LdasArray< INT_2U >::mIdentifier = "int_2u";
  template<> const string LdasArray< INT_4S >::mIdentifier = "int_4s";
  template<> const string LdasArray< INT_4U >::mIdentifier = "int_4u";
  template<> const string LdasArray< INT_8S >::mIdentifier = "int_8s";
  template<> const string LdasArray< INT_8U >::mIdentifier = "int_8u";
  template<> const string LdasArray< REAL_4 >::mIdentifier = "real_4";
  template<> const string LdasArray< REAL_8 >::mIdentifier = "real_8";
  template<> const string LdasArray< COMPLEX_8 >::mIdentifier = "complex_8";   
  template<> const string LdasArray< COMPLEX_16 >::mIdentifier = "complex_16";   
} /* namespace - ILwd */

// getElementId specializations
template<> ILwd::ElementId
ILwd::LdasArray< CHAR >::getElementId() const
{
    return ID_CHAR;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< CHAR_U >::getElementId() const
{
    return ID_CHAR_U;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< INT_2S >::getElementId() const
{
    return ID_INT_2S;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< INT_2U >::getElementId() const 
{
    return ID_INT_2U;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< INT_4S >::getElementId() const
{
    return ID_INT_4S;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< INT_4U >::getElementId() const 
{
    return ID_INT_4U;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< INT_8S >::getElementId() const 
{
    return ID_INT_8S;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< INT_8U >::getElementId() const
{
    return ID_INT_8U;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< REAL_4 >::getElementId() const
{
    return ID_REAL_4;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< REAL_8 >::getElementId() const 
{
    return ID_REAL_8;
}
   
template<> ILwd::ElementId
ILwd::LdasArray< COMPLEX_8 >::getElementId() const 
{
    return ID_COMPLEX_8;
}   
   
template<> ILwd::ElementId
ILwd::LdasArray< COMPLEX_16 >::getElementId() const 
{
    return ID_COMPLEX_16;
}

// ObjectSpace
#ifdef HAVE_LIBOSPACE

/// \cond DOXYGEN_IGNORE

#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::LdasArray< CHAR >*), Stream::ILwd::LE_CHAR,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< CHAR_U >*), Stream::ILwd::LE_CHAR_U,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< INT_2S >*), Stream::ILwd::LE_INT_2S,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< INT_2U >*), Stream::ILwd::LE_INT_2U,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< INT_4S >*), Stream::ILwd::LE_INT_4S,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< INT_4U >*), Stream::ILwd::LE_INT_4U,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< INT_8S >*), Stream::ILwd::LE_INT_8S,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< INT_8U >*), Stream::ILwd::LE_INT_8U,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< REAL_4 >*), Stream::ILwd::LE_REAL_4,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< REAL_8 >*), Stream::ILwd::LE_REAL_8,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< COMPLEX_8 >*), Stream::ILwd::LE_COMPLEX_8,
                 (ILwd::LdasArrayBase*) )
OS_STREAMABLE_1( (ILwd::LdasArray< COMPLEX_16 >*), Stream::ILwd::LE_COMPLEX_16,
                 (ILwd::LdasArrayBase*) )

   
using namespace ILwd;
   
// template specializations
template<>
void ospaceWrite< COMPLEX_8 >( os_bstream& stream,
			       const LdasArray< COMPLEX_8 >& o )
{
  ospaceWriteHeader( stream, o );
  ILwd::LdasArray< COMPLEX_8>::size_type size( 2 * o.getNData() );
  REAL_4* temp = new REAL_4[ size ];
  ILwd::LdasArray< COMPLEX_8>::size_type index( 0 );
  for( ILwd::LdasArray< COMPLEX_8>::size_type i = 0; i < size; ++i )
  {
    temp[ i ] = o.mData[ index ].real();
    ++i;
    temp[ i ] = o.mData[ index ].imag();   
    ++index;
  }
   
  stream.write_chunk(os_protocol::float32, temp, size);
  delete[] temp;
  temp = 0;
  
  return;
}

   
template<>
void ospaceRead< COMPLEX_8 >( os_bstream& stream, 
			      LdasArray< COMPLEX_8 >& o )
{
  ospaceReadHeader( stream, o );

  // if the data is not compressed, we should convert the byte order.
  ILwd::LdasArray< COMPLEX_8>::size_type ndata( o.getNData() );
  o.mData = new COMPLEX_8[ ndata ];
    
  ILwd::LdasArray< COMPLEX_8>::size_type size( 2 * ndata );
  REAL_4* temp = new REAL_4[ size ];
   
  stream.read_chunk( os_protocol::float32, temp, size );
   
  unsigned int index( 0 );
  for( ILwd::LdasArray< COMPLEX_8>::size_type i = 0; i < size; )
  {
    o.mData[ index ] = COMPLEX_8( temp[ i ], temp[ i + 1 ] );
    ++index;
    i += 2;
  }
     
  delete[] temp;
  temp = 0;

  if ( ndata <= 0 )
  {
    // Cleanup since there is no real data
    delete[] o.mData;
    o.mData = (COMPLEX_8*)NULL;
  }
   
  return;
}   

   
template<>   
void ospaceWrite< COMPLEX_16 >( os_bstream& stream, 
				const LdasArray< COMPLEX_16 >& o )
{
  ospaceWriteHeader( stream, o );
  ILwd::LdasArray< COMPLEX_16 >::size_type size( 2 * o.getNData() );
  REAL_8* temp = new REAL_8[ size ];
  ILwd::LdasArray< COMPLEX_16 >::size_type index( 0 );
  for( ILwd::LdasArray< COMPLEX_16 >::size_type i = 0; i < size; ++i )
  {
    temp[ i ] = o.mData[ index ].real();
    ++i;
    temp[ i ] = o.mData[ index ].imag();   
    ++index;
  }
   
  stream.write_chunk( os_protocol::float64, temp, size );
  delete[] temp;
  temp = 0;
   
  return;
}


template<>
void ospaceRead< COMPLEX_16 >( os_bstream& stream, 
			       LdasArray< COMPLEX_16 >& o )
{
  ospaceReadHeader( stream, o );
  // if the data is not compressed, we should convert the byte order.
  ILwd::LdasArray< COMPLEX_16 >::size_type ndata = o.getNData();   
  o.mData = new COMPLEX_16[ ndata ];
    
  ILwd::LdasArray< COMPLEX_16 >::size_type size( 2 * ndata );
  REAL_8* temp = new REAL_8[ size ];
   
  stream.read_chunk( os_protocol::float64, temp, size );
   
  unsigned int index( 0 );
  for( ILwd::LdasArray< COMPLEX_16 >::size_type i = 0; i < size; )
  {
    o.mData[ index ] = COMPLEX_16( temp[ i ], temp[ i + 1 ] );
    ++index;
    i += 2;
  }
     
  delete[] temp;
  temp = 0;

  if ( ndata <= 0 )
  {
    // Cleanup since there is no real data
    delete[] o.mData;
    o.mData = (COMPLEX_16*)NULL;
  }
}   
/// \endcond

#endif // HAVE_LIBOSPACE



#define	INSTANTIATE(instance) \
template class ILwd::LdasArray< instance >; \
template \
std::ostream& std::operator << < instance >( std::ostream& stream, \
					     ILwd::LdasArray< instance >& element )
// template
// Reader& operator >> < instance >(Reader& r, ILwd::LdasArray< instance >& element )

/// \cond DOXYGEN_IGNORE
INSTANTIATE( CHAR );
INSTANTIATE( CHAR_U );
INSTANTIATE( INT_2S );
INSTANTIATE( INT_2U );
INSTANTIATE( INT_4S );
INSTANTIATE( INT_4U );
INSTANTIATE( INT_8S );
INSTANTIATE( INT_8U );
INSTANTIATE( REAL_4 );
INSTANTIATE( REAL_8 );
INSTANTIATE( COMPLEX_8 );
INSTANTIATE( COMPLEX_16 );
/// \endcond

#undef INSTANTIATE

