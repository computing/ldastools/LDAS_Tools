#ifndef ILwdBaseHH
#define ILwdBaseHH


#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

// System includes
#include <iosfwd>
#include <exception>

// LDAS includes
#include <general/formatexception.hh>
#include <general/streamexception.hh>
#include "reader.hh"


namespace ILwd {
    

//-----------------------------------------------------------------------------
//: An enumeration of the different IILwd objects.
//
// This enum is used to identify the various types of XML objects which
// this library recognizes.  These are the "fundamental" types such as
// comments and PI's.
//-----------------------------------------------------------------------------
enum ClassType
{
    ID_STARTTAG,
    ID_ENDTAG,
    ID_COMMENT,
    ID_PI,
    ID_CDATA,
    ID_CHARDATA
};


//-----------------------------------------------------------------------------
//
//: This class represents the base class for the fundamental XML objects.
//
// This class is abstract,
//
class Base
{
public:

    virtual ~Base();
   
    //: <font color=red>PURE VIRTUAL</font> Returns the class id of 
    //: this object (used to identify the type).
    //
    // Returns the class id of the object.
    // 
    //!return: ClassType - The class id. 
    virtual ClassType getClassId() const = 0; 

    //: <font color=red>PURE VIRTUAL</font> I/O
    //
    // Writes object to the stream.
    //
    //!param: ostream& stream - Output stream to write to.
    // 
    //!return: Nothing.
    //
    virtual void write( std::ostream& stream ) const = 0;
    static Base* createObject( Reader& r );

protected:

    const Base& operator=( const Base& b );
    bool operator==( const Base& b ) const;
    bool operator!=( const Base& b ) const;
};



//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const Base& b - The object to assign from.
//
//!return: const Base& - A reference to this object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
inline const Base& Base::operator=( const Base& b )
{
    return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another PI for equality.
//
//!param: const Base& o - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
inline bool Base::operator==( const Base& o ) const
{
    return true;
}


//-----------------------------------------------------------------------------
//
//: Inequality operator.
//
//!param: const Base& o - The object to compare to.
//
//!return: bool - true if the objects are not equal.  False otherwise.
//
inline bool Base::operator!=( const Base& o ) const
{
    return false;
}


} // namespace ILwd


// ObjectSpace block
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>

void os_write( os_bstream& stream, const ILwd::Base& base );
void os_read( os_bstream& stream, ILwd::Base& base );

OS_POLY_CLASS( ILwd::Base )
OS_STREAM_OPERATORS( ILwd::Base )

#endif // HAVE_LIBOSPACE

#endif // ILwdBaseHH
