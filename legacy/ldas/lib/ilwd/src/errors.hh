#ifndef ILwdErrorsHH
#define ILwdErrorsHH

#include <string>

namespace ILwd
{
    namespace Errors
    {
        // FormatException error codes
        enum Error
        {
            INVALID_DELIMITER,
            INVALID_NAME,
            INVALID_ATTVALUE,
            UNKNOWN_REFERENCE,
            INVALID_ATTRIBUTE,
            INVALID_START_TAG,
            INVALID_END_TAG,
            INVALID_CHAR,
            INVALID_IDENTIFIER,
            INVALID_CDATA,
            INVALID_COMMENT,
            INVALID_PI,
            BAD_ATTRIBUTE,
            BAD_START_TAG,
            BAD_END_TAG,
            BAD_CONVERSION,
            MISSING_ATTRIBUTE,
            MISSING_DELIMITER,
            MISSING_EQUAL,
            DUPLICATE_ATTRIBUTE,
            UNKNOWN_IDENTIFIER,
            UNKNOWN_OBJECT,
            INVALID_FORMAT,
	    OUT_OF_RANGE,
	    UNSIGNED_BELOW_ZERO,
	    BAD_DATA_TYPE,
	    EXTRA_DATA,
	    DIM_ERROR,
	    CONVERSION
        };
    }

    const char* getErrorString( int err );
}


// FormatException Macros
// gcc-3.2.1 and gcc-3.2.2 have problems with "__FILE__" and "__LINE__":
#if defined __GNUC__ && defined __sun__ && 3 ==__GNUC__ && 2 ==__GNUC_MINOR__


#define ILWD_FORMATEXCEPTION( code ) \
    FormatException( Library::ILWD, code, ILwd::getErrorString( code ), "" )

#define ILWD_FORMATEXCEPTION_INFO( code, info ) \
    FormatException( Library::ILWD, code, ILwd::getErrorString( code ), info )

#define ADD_ILWD_FORMATEXCEPTION( code ) \
    catch( FormatException& e ) \
    { \
        e.addError( Library::ILWD, code, ILwd::getErrorString( code ), "" ); \
        throw; \
    }

#define ADD_ILWD_FORMATEXCEPTION_INFO( code, info ) \
    catch( FormatException& e ) \
    { \
        e.addError( Library::ILWD, code, ILwd::getErrorString( code ), info ); \
        throw; \
    }

// StreamException Macros
#define ILWD_STREAMEXCEPTION( stream ) \
    StreamException( stream, Library::ILWD, "" )

#define ILWD_STREAMEXCEPTION_INFO( stream, info ) \
    StreamException( stream, Library::ILWD, info )


#else


#define ILWD_FORMATEXCEPTION( code ) \
    FormatException( Library::ILWD, code, ILwd::getErrorString( code ), "", \
                     __FILE__, __LINE__ )
#define ILWD_FORMATEXCEPTION_INFO( code, info ) \
    FormatException( Library::ILWD, code, ILwd::getErrorString( code ), info, \
                     __FILE__, __LINE__ )
#define ADD_ILWD_FORMATEXCEPTION( code ) \
    catch( FormatException& e ) \
    { \
        e.addError( Library::ILWD, code, ILwd::getErrorString( code ), "", \
                    __FILE__, __LINE__ ); \
        throw; \
    }
#define ADD_ILWD_FORMATEXCEPTION_INFO( code, info ) \
    catch( FormatException& e ) \
    { \
        e.addError( Library::ILWD, code, ILwd::getErrorString( code ), info, \
                    __FILE__, __LINE__ ); \
        throw; \
    }

// StreamException Macros
#define ILWD_STREAMEXCEPTION( stream ) \
    StreamException( stream, Library::ILWD, "", __FILE__, __LINE__ )
#define ILWD_STREAMEXCEPTION_INFO( stream, info ) \
    StreamException( stream, Library::ILWD, info, __FILE__, __LINE__ )


#endif


#endif // ILwdErrorsHH
