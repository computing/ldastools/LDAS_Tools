#include "ilwd/config.h"

#include <sstream>
   
#include "cdata.hh"
#include "errors.hh"

using ILwd::CData;
using namespace std;   


//-----------------------------------------------------------------------------
//
//: Default Constructor.
//
CData::CData()
   : mData()
{}


//-----------------------------------------------------------------------------
//
//: Input constructor.
//
// Reads CData from a reader. 
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the CData was invalid.
//
CData::CData( Reader& r )
{
    read( r );
}


//-----------------------------------------------------------------------------
//
//: Copy constructor
//
//!param: const CData& o - The object to copy.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
CData::CData( const CData& o )
  : Base( o ),
    mData( o.mData )
{
}


//-----------------------------------------------------------------------------
//
//: Destructor
//
CData::~CData()
{}

   
ILwd::ClassType CData::getClassId() const
{
   return ID_CDATA;
}   
   

//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const CData& o - The object to assign from.
//
//!return: const CData& - A reference to this object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
const CData& CData::operator=( const CData& o )
{
    if ( this != &o )
    {
        Base::operator=( o );
        mData = o.mData;
    }

    return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another for equality.
//
//!param: const CData& o - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool CData::operator==( const CData& o ) const
{
    return ( Base::operator==( o ) && mData == o.mData );
}


//-----------------------------------------------------------------------------
//
//: Reads CData.
//
// This reads a CData block.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the CData was invalid.
//
void CData::read( Reader& r )
try
{
    if ( !r.readString( "<![CDATA[" ) )
    {
        throw ILWD_FORMATEXCEPTION_INFO( Errors::MISSING_DELIMITER,
                                   "Opening '<![CDATA[' not found." );
    }

    ostringstream ss;
    bool finish = false;

    try
    {
        while ( !finish )
        {
            int c = r.get(); // May throw StreamException
            
            if ( c == ']' )
            {
                c = r.get(); // May throw StreamException
                if ( c == ']' )
                {
                    c = r.get(); // May throw StreamException
                    if ( c == '>' )
                    {
                        finish = true;
                    }
                    else
                    {
                        ss << "]]";
                    }
                }
                else
                {
                    ss << ']';
                }
            }
            else
            {
                ss << char(c);
            }
        }
    }
    catch( StreamException& )
    {
        mData = ss.str();
        throw;
    }

    mData = ss.str();
    return;
}
ADD_ILWD_FORMATEXCEPTION_INFO(
    Errors::INVALID_CDATA, "The CDATA block contains invalid characters." )


//-----------------------------------------------------------------------------
//
//: Write the CData object to a stream.
//
//!param: ostream& stream - The stream to write to.
//
//!exc: exception - An unknown exception occurred.
//
void CData::write( std::ostream& stream ) const
{
    stream << "<![CDATA[" << mData << "]]>";
    return;
}

            
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::CData*), Stream::ILwd::CDATA, (ILwd::Base*) )

void os_write( os_bstream& stream, const CData& o )
{
    os_write( stream, (ILwd::Base&)o );    
    stream << o.mData;
}

void os_read( os_bstream& stream, CData& o )
{
    os_read( stream, (ILwd::Base&)o );
    stream >> o.mData;
}

#endif // HAVE_LIBOSPACE
