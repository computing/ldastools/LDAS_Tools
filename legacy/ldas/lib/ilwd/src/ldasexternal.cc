/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ilwd/config.h"

#include <cstring>
#include <sstream>
#include <algorithm>   

#include "general/autoarray.hh"
   
#include "ldasexternal.hh"
#include "endtag.hh"

using ILwd::LdasExternal;
using namespace std;   


const char* LdasExternal::ATTR_STR_SIZE("size");
const char* LdasExternal::ATTR_STR_BYTES("bytes");
const char* LdasExternal::ATTR_STR_MIME_TYPE("mime_type");
const std::string LdasExternal::mIdent = std::string("external");

//-----------------------------------------------------------------------------
//
//: Default constructor.
//
// This creates an empty LdasExternal element.
//
LdasExternal::LdasExternal()
   : LdasFormatElement(), mBuffer( 0 ), mSize( 0 )
{}


//-----------------------------------------------------------------------------
//
//: Constructor.
//
// This creates an LdasExternal element containing the data
// pointer to by 'buffer'.
//
//!param: void* buffer - A pointer to the data to store in the external
//!param:     element.
//!param: size_t s - The size of the data buffer in bytes.
//!param: std::string MimeType - The mime type meant to operate on the data.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
LdasExternal::LdasExternal( void* buffer, size_t s,
			    const std::string& MimeType )
   : LdasFormatElement(), mBuffer( 0 ), mSize( s ), mMimeType( MimeType )
{
    unsigned char* b = reinterpret_cast<unsigned char *>( buffer );
   
    mBuffer = new unsigned char[ s+1 ];
    std::copy( b, b + s, mBuffer );
    mBuffer[s] = '\0';
}


//-----------------------------------------------------------------------------
//
//: Constructs this element from a stream, given the attributes.
//
// This constructor is used when the start-tag has already been read.  For
// example, if we do not know what type of element is coming next then the
// start-tag is read from the stream, the type of object identified, and
// the Reader&/Attributes& constructor is called.
//
//!todo: - Have the Reader class ID objects and pushback the start-tag.  Then
//!todo: we wouldn't need this method.
//
//!param: Reader& r - The reader to read from.
//!param: const Attributes& att - The attributes for this object.
//
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
LdasExternal::LdasExternal( Reader& r, const Attributes& att )
   : LdasFormatElement(), mBuffer( 0 ), mSize( 0 ),
     mMimeType( "" )
{
    read( r, att );
}

//-----------------------------------------------------------------------------
//
//: Copy constructor.
//
//!param: const LdasExternal& le - The object to copy.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
LdasExternal::LdasExternal( const LdasExternal& le )
   : LdasFormatElement( le ), mBuffer( 0 ), mSize( le.mSize ),
     mMimeType( le.mMimeType )
{
    mBuffer = new unsigned char[ mSize+1 ];

    std::copy( le.mBuffer, le.mBuffer + mSize, mBuffer );
    mBuffer[mSize] = '\0';
}


//-----------------------------------------------------------------------------
//
//: Input constructor.
//
// Constructs this object from an ILWD reader.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
LdasExternal::LdasExternal( Reader& r )
   : LdasFormatElement(), mBuffer( 0 ), mSize( 0 ),
     mMimeType( "" )
{
    read( r );
}


//-----------------------------------------------------------------------------
//
//: Destructor
//
LdasExternal::~LdasExternal()
{
    delete[] mBuffer;
    mBuffer = 0;
}

   
//-----------------------------------------------------------------------------
//: Gets the element ID of this type of object.
//
// The element ID is used to determine what type an object is without
// having to resort to RTTI or comparing identifiers.
//
//!return: ElementId - The element id (an enumeration).
//
ILwd::ElementId ILwd::LdasExternal::getElementId() const
{
    return ID_EXTERNAL;
}


//-----------------------------------------------------------------------------
//
//: Create a copy of this element.
//
// This method allocates memory and creates a copy of this string object.  This
// is a virtual method which allows one to copy an LdasElement without knowing
// what its exact type is.
//
//!return: LdasExternal* - A pointer to a newly allocated copy of this object.
//
//!except: std::bad_alloc - Memory allocation failed.
//
ILwd::LdasExternal* ILwd::LdasExternal::createCopy() const
{
    return new LdasExternal( *this );
}
   

//-----------------------------------------------------------------------------
//
//: Assignment Operator.
//
//!param: const LdasExternal& le - The object to assign from.
//
//!return: const LdasExternal& - A reference to this object.
//
//!exc: std::bad_alloc: Memory allocation failed.
//
const LdasExternal& LdasExternal::operator=( const LdasExternal& le )
{
    if ( this != &le )
    {
        LdasFormatElement::operator=( le );
        delete[] mBuffer;
   
        mSize = le.mSize;
        mMimeType = le.mMimeType;
   
        mBuffer = new unsigned char[ mSize+1 ];
	std::copy( le.mBuffer, le.mBuffer + mSize + 1, mBuffer );
    }

    return *this;
}


//-----------------------------------------------------------------------------
//
//: Concatenation Operator.
//
//!param: const LdasExternal& le - The object to concat from.
//
//!return: const LdasExternal& - A reference to this object.
//
//!exc: std::bad_alloc: Memory allocation failed.
//
const LdasExternal& LdasExternal::operator+=( const LdasExternal& le )
{
  size_t tSize(mSize + le.mSize);

  unsigned char* tmp = new unsigned char[ tSize+1 ];
  // memcpy(tmp, mBuffer, mSize);
  std::copy( mBuffer, mBuffer + mSize, tmp );
   
  // memcpy(&tmp[mSize], le.mBuffer, le.mSize+1);
  std::copy( le.mBuffer, le.mBuffer + le.mSize + 1, tmp + mSize );
   
  delete[] mBuffer;
  mBuffer = tmp;
  mSize = tSize;
   
  return *this;
}

        
//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another array for equality.
//
//!param: const LdasExternal &le - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool LdasExternal::
operator==( const LdasExternal& le ) const
{
    if ( this == &le ||
         ( LdasFormatElement::operator==( le ) &&
           mSize == le.mSize && mMimeType == le.mMimeType &&
           memcmp( mBuffer, le.mBuffer, mSize )
           )
         )
    {
        return true;
    }
    return false;
}
   
   
//-----------------------------------------------------------------------------
//
//: Not equal comparison operator.
//
// This compares this object to another LdasContainer for inequality.
//
//!param: const LdasExternal& le - The object to compare with.
//
//!return: bool - true if the containers are not equal.
//
bool ILwd::LdasExternal::operator!=( const LdasExternal& le )
    const
{
    return !operator==( le );
}   
   

//-----------------------------------------------------------------------------
//
//: Return the pointer to the buffer.
//
//!return: void* - A pointer to the buffer.
//
void* LdasExternal::getBuffer()
{
    return static_cast<void *>(mBuffer);
}

    
//-----------------------------------------------------------------------------
//
//: Return the pointer to the buffer.
//
//!return: const void* - A pointer to the buffer.
//
const void* LdasExternal::getBuffer() const
{
    return static_cast<void *>(mBuffer);
}

    
//-----------------------------------------------------------------------------
//
//: Return the size of the buffer.
//
//!return: size_t - The size of the buffer.
//
size_t LdasExternal::getSize() const
{
    return mSize;
}


//-----------------------------------------------------------------------------
//
//: Return the mime type.
//
//!return: std::string - The mime type.
//
std::string LdasExternal::getMimeType() const
{
    return mMimeType;
}


//-----------------------------------------------------------------------------
//
//: Reads this element from a stream.
//
// This method is called when the
// user knows for certain that the next object is this type.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasExternal object was
//!exc:     invalid.
//
void LdasExternal::read( Reader& r )
{
    StartTag st( r );
    if ( st.getIdentifier() != getIdentifier() )
    {
        string msg( "The start-tag for this object was not found, expecting <" );
        msg += getIdentifier();
        msg += ">, found <";
        msg += st.getIdentifier();
        msg += ">.";
   
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_START_TAG, msg );
    }

    read( r, st.getAttributes() );
   
    return;
}

        
//-----------------------------------------------------------------------------
//
//: Reads this element from a stream.
//
//!param: Reader& r - The reader to read from.
//!param: const Attributes& att - The attributes for the array.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the LdasExternal object was
//!exc:     invalid.
//
void LdasExternal::read( Reader& r, const Attributes& att )
try
{
    LdasFormatElement::readAttributes( att );
    
    Attributes::const_iterator iter = att.find( ATTR_STR_SIZE );
    if ( iter != att.end() )
    {
        mSize = iter->second.getValue< size_t >();
    }
    else
    {
        mSize = 0;
    }

    iter = att.find( "mime_type" );
    if ( iter != att.end() )
    {
        mMimeType = iter->second.getString();

    }
    else
    {
        mMimeType = "";
    }

    if ( ( getWriteFormat() == BINARY && getWriteCompression() == NO_COMPRESSION ) ||
         ( getWriteFormat() == ILwd::ASCII ) )
    {
        delete[] mBuffer;
        mBuffer = 0;
        mBuffer = new unsigned char[ mSize+1 ];
        r.read( mBuffer, mSize );
    }
    else
    {
        char* buffer( 0 );
        size_t bytes( 0 );
        
        if ( getWriteFormat() == BINARY )
        {
            Attributes::const_iterator iter = att.find( ATTR_STR_BYTES );
            if ( iter == att.end() )
            {
                throw ILWD_FORMATEXCEPTION_INFO(
                    Errors::MISSING_ATTRIBUTE,
                    "Bytes must be specified with format='binary' and "
                    "compression." );
            }
            else
            {
                try
                {
                    bytes = iter->second.getValue< size_t >();
                }
                ADD_ILWD_FORMATEXCEPTION_INFO( Errors::BAD_ATTRIBUTE,
                                    "Couldn't parse 'bytes' attribute." );
            }
            
            buffer = new char[ bytes ];
            r.read( buffer, bytes );
        }
        else
        {
            std::stringbuf oss;
    
            // Read all character up to '<' into stringbuf
            r.getStream().get( oss, '<' );

            // How many characters were just read
            bytes = r.getStream().gcount();
   
            buffer = new char[ bytes+1 ];
           
            const string& oss_str( oss.str() );
	    strncpy( buffer, oss_str.c_str(), bytes * sizeof( char ) );
	    buffer[ bytes ] = '\0';
	    if ( bytes == 0 )
	    {
		// Make sure that it is not registered as an error if no
		// bytes were read.
		r.getStream().clear();
	    }   
        }
        
        unsigned char* tmp = convertFromFormat< unsigned char >( buffer, bytes, mSize );
        
        delete[] buffer;
        buffer = 0;      
   
        delete[] mBuffer;
        mBuffer = tmp;
    }
    
    EndTag et( r );
    if ( et.getIdentifier() != getIdentifier() )
    {
        ostringstream msg;
        msg << "The wrong end-tag for this object was found.  Expecting "
            << EndTag( getIdentifier() ) << ", found " << et << '.';
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_END_TAG, msg.str() );
    }
}
catch( ... )
{
    mSize = 0;
    delete[] mBuffer;
    mBuffer = 0;
    mMimeType = "";
    throw;
}


//-----------------------------------------------------------------------------
//
//: Get the identifier.
//
// This returns the identifier for this element.  This is the static version.
//
//!return: const std::string& - The identifier.
//
const std::string& LdasExternal::getIdentifierStatic()
{
    return mIdent;
}


//-----------------------------------------------------------------------------
//
//: Get the identifier.
//
// Returns the identifier for this element.
//
//!return: const std::string& - The identifier.
//
const std::string& LdasExternal::getIdentifier() const
{
    return getIdentifierStatic();
}


//-----------------------------------------------------------------------------
//
//: Writes the object to a stream.
//
// Writes the array to a stream with the given format and compression.
//
//!param: ostream& stream - The stream to write to.
//
//!exc: exception - An unknown error occurred.
//
//!todo: What does ASCII format mean for this type of object?
//
void LdasExternal::writeData( std::ostream& stream ) const
{
    Attributes att;
    
    if ( mSize != 0 )
    {
        att.addAttribute( ATTR_STR_SIZE, mSize );
    }

    if (mMimeType.length() > 0 )
    {
      att.addAttribute( ATTR_STR_MIME_TYPE, mMimeType );
    }

    if ( ( getWriteFormat() == BINARY &&
           getWriteCompression() == NO_COMPRESSION ) ||
         ( getWriteFormat() == ILwd::ASCII ) )
    {
	LdasFormatElement::writeAttributes( att );
        stream << StartTag( getIdentifier(), att );
        stream.write( (char*)mBuffer, mSize );
    }
    else
    {
        char* buffer( 0 );
        size_t size( 0 );

        buffer = convertToFormat< char >(
            reinterpret_cast< char* >( mBuffer ), mSize, &size );

        if ( getWriteFormat() == BINARY )
        {
            att.addAttribute( ATTR_STR_BYTES, size );
        }
	else
	{
	    --size;
	}

	LdasFormatElement::writeAttributes( att );
        stream << StartTag( getIdentifier(), att );
        stream.write( buffer, size );
   
        delete[] buffer;
        buffer = 0;
    }

    stream << EndTag( getIdentifier() );
   
    return;
}


// ObjectSpace
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::LdasExternal*), Stream::ILwd::LDASEXTERNAL,
                 (ILwd::LdasFormatElement*) )

void os_write( os_bstream& stream, const LdasExternal& o )
{
    os_write( stream, (const ILwd::LdasFormatElement&)o );
    stream << o.mSize << o.mMimeType.length();
    stream.write_chunk( o.mBuffer, o.mSize );
    stream.write_chunk( o.mMimeType.c_str(), o.mMimeType.length() + 1);
   
    return;
}

void os_read( os_bstream& stream, LdasExternal& o )
{
    delete[] o.mBuffer;
    o.mBuffer = 0;
    size_t	mime_type_size;

    os_read( stream, static_cast<ILwd::LdasFormatElement&>(o) );
    stream >> o.mSize;
    stream >> mime_type_size;

    o.mBuffer = new unsigned char[ o.mSize+1 ];
    stream.read_chunk( o.mBuffer, o.mSize );

    General::AutoArray< CHAR > buf( new char[ mime_type_size + 1 ] );
    stream.read_chunk(buf.get(), mime_type_size + 1);
    o.mMimeType = buf.get();

    return;
}



#endif // HAVE_LIBOSPACE
