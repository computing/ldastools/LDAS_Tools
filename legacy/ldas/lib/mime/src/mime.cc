#include "mime/config.h"

#include "mime.hh"

namespace Mime {
  Mime::
  Mime( std::string Type )
    : m_types( 1, Type )
  {
  }

  Mime::
  Mime( std::vector< std::string >& Types )
    : m_types( Types )
  {
  }
} /* namespace Mime */
