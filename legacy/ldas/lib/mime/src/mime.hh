#ifndef MIME_HH
#define	MIME_HH

#include <vector>
#include <string>

namespace Mime
{
  class Mime
  {
  public:
    Mime( std::string Type );
    Mime( std::vector< std::string >& Types );

    const std::vector< std::string >& Types( void ) const;

  private:
    const std::vector<std::string> m_types;
  };

  inline const std::vector< std::string >& Mime::
  Types( void ) const
  {
    return m_types;
  }
}
#endif	/* MIME_HH */
