#include <stdexcept>
#include "general/util.hh"
#include "general/types.hh"

#include "mime/config.h"
#include "mime/spectrum.hh"

using namespace Mime;

inline void do_reverse( Spectrum::size_type Size,
			Spectrum::data_type* Spectrum,
			Spectrum::size_type Length
			)
{
  switch( Size )
  {
  case sizeof( REAL_4 ):
    reverse< sizeof( REAL_4 ) >(Spectrum, Length);
    break;
  case sizeof( REAL_8 ):
    reverse< sizeof( REAL_8 ) >(Spectrum, Length);
    break;  
  default:
    std::runtime_error( "Currently the requested Unpacking is not supported." );
    break;
  }
}

Spectrum::
Spectrum( void )
  : Mime( m_mime_types )
{
  m_header.s_version = 0x00;
}


Spectrum::
Spectrum( const std::string& MimeType,
	  INT_4S StartTimeSeconds,
	  INT_4S StartTimeNanoseconds,
	  INT_4S EndTimeSeconds,
	  INT_4S EndTimeNanoseconds,
	  const CHAR_U* Blob,
	  INT_4S Size,
	  INT_4S Length,
	  REAL_8 Base,
	  REAL_8 Delta)
  : Mime( m_mime_types )
{
  init_spectrum( GetSpectrumId( MimeType ),
		 StartTimeSeconds,
		 StartTimeNanoseconds,
		 EndTimeSeconds,
		 EndTimeNanoseconds,
		 Blob,
		 Size,
		 Length,
		 Base,
		 Delta);
}

Spectrum::
Spectrum( const SpectrumTypeId MimeType,
	  INT_4S StartTimeSeconds,
	  INT_4S StartTimeNanoseconds,
	  INT_4S EndTimeSeconds,
	  INT_4S EndTimeNanoseconds,
	  const CHAR_U* Blob,
	  INT_4S Size,
	  INT_4S Length,
	  REAL_8 Base,
	  REAL_8 Delta)
  : Mime( m_mime_types )
{
  init_spectrum( MimeType,
		 StartTimeSeconds,
		 StartTimeNanoseconds,
		 EndTimeSeconds,
		 EndTimeNanoseconds,
		 Blob,
		 Size,
		 Length,
		 Base,
		 Delta );
}

Spectrum::SpectrumTypeId Spectrum::
GetSpectrumId( const std::string& MimeType )
{
  const std::map<std::string,SpectrumTypeId>::const_iterator p( m_id_map.find(MimeType) );

  if ( p == m_id_map.end() )
  {
    throw std::range_error("Mimetype not appropriate for Spectrum class");
  }
  return p->second;
}

REAL_8 Spectrum::
GetBase( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_base;
  default:
    throw std::runtime_error
      ( "Spectrum::GetBase: version not supported" );
  }
}

INT_4U Spectrum::
GetByteCount( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_length_in_bytes;
  default:
    throw std::runtime_error
      ( "Spectrum::GetByteCount: version not supported" );
  }
}

REAL_8 Spectrum::
GetDelta( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_delta;
  default:
    throw std::runtime_error
      ( "Spectrum::GetDelta: version not supported" );
  }
}

INT_4U Spectrum::
GetLength( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_length_in_elements;
  default:
    throw std::runtime_error
      ( "Spectrum::GetLength: version not supported" );
  }
}

INT_4U Spectrum::
GetStartSecond( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_start_time_seconds;
  default:
    throw std::runtime_error
      ( "Spectrum::GetStartSecond: version not supported" );
  }
}

INT_4U Spectrum::
GetStartNanosecond( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_start_time_nanoseconds;
  default:
    throw std::runtime_error
      ( "Spectrum::GetStartNanosecond: version not supported" );
  }
}

INT_4U Spectrum::
GetStopSecond( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_end_time_seconds;
  default:
    throw std::runtime_error
      ( "Spectrum::GetStopSecond: version not supported" );
  }
}

INT_4U Spectrum::
GetStopNanosecond( ) const
{
  switch( validate_header_version( ) )
  {
  case 0x01:
    return m_header.v1.s_end_time_nanoseconds;
  default:
    throw std::runtime_error
      ( "Spectrum::GetStopNanosecond: version not supported" );
  }
}

std::string Spectrum::
GetSpectrumMimeType( SpectrumTypeId Id )
{
  std::map<std::string,SpectrumTypeId>::const_iterator i;
  for ( i = m_id_map.begin();
	( i != m_id_map.end() ) && (i->second != Id );
	i++)
    ;
  if ( i == m_id_map.end() )
  {
    return "";
  }
  return i->first;
}

void Spectrum::
Unpack( const Spectrum::SpectrumTypeId Type,
	Spectrum::data_type* Spectrum,
	Spectrum::size_type Length,
	Spectrum::size_type ByteCount ) const
{
  Spectrum::size_type	size( length_check( Type, Length, ByteCount ) );

  switch ( Type )
  {
  case REAL_BIGENDIAN:
#ifndef WORDS_BIGENDIAN
    do_reverse( size, Spectrum, Length );
#endif	/* WORDS_BIGENDIAN */
    break;
  case REAL_LITTLEENDIAN:
#ifdef WORDS_BIGENDIAN
    do_reverse( size, Spectrum, Length );
#endif	/* WORDS_BIGENDIAN */
    break;
  case COMPLEX_BIGENDIAN:
#ifndef WORDS_BIGENDIAN
    do_reverse( size, Spectrum, Length * 2 );
#endif	/* WORDS_BIGENDIAN */
    break;
  case COMPLEX_LITTLEENDIAN:
#ifdef WORDS_BIGENDIAN
    do_reverse( size, Spectrum, Length * 2 );
#endif	/* WORDS_BIGENDIAN */
    break;
  default:
    std::runtime_error( "Currently the requested Unpacking is not supported." );
    break;
  }
}


void Spectrum::
init_spectrum( const SpectrumTypeId MimeType,
	       INT_4S StartTimeSeconds,
	       INT_4S StartTimeNanoseconds,
	       INT_4S EndTimeSeconds,
	       INT_4S EndTimeNanoseconds,
	       const CHAR_U* Blob,
	       INT_4S Size,
	       INT_4S Length,
	       REAL_8 Base,
	       REAL_8 Delta)
{
  //---------------------------------------------------------------------
  // Save the values
  //---------------------------------------------------------------------
  m_header.s_version = m_current_version;
  m_header.s_type = (char)MimeType;
#define H_V v1
#define	INIT(LM_VAR,LM_VALUE) \
  m_header.H_V.LM_VAR = LM_VALUE

  INIT(s_start_time_seconds,StartTimeSeconds);
  INIT(s_start_time_nanoseconds,StartTimeNanoseconds);
  INIT(s_end_time_seconds,EndTimeSeconds);
  INIT(s_end_time_nanoseconds,EndTimeNanoseconds);
  INIT(s_length_in_elements,Length);
  INIT(s_length_in_bytes,Size);
  INIT(s_base,Base);
  INIT(s_delta,Delta);

#undef INIT
#undef H_V

  m_data = Blob;

  //---------------------------------------------------------------------
  // Do sanity checks on the length of the Blob
  //---------------------------------------------------------------------

  length_check( GetSpectrumTypeId(), GetLength(), GetByteCount() );

  //---------------------------------------------------------------------
  // Make sure the byte order is correct
  //---------------------------------------------------------------------
  unpack( );
}

//-----------------------------------------------------------------------
//!exc: std::length_error() - Throw when the size of the data spectrum is
//+	not equal to Length or when the the Length parameter is zero.
//!todo: Need to revise this routine once metadata is also stored in the
//+	mime type.
//-----------------------------------------------------------------------
Spectrum::size_type Spectrum::
length_check( const SpectrumTypeId MimeType,
	      Spectrum::size_type Length,
	      Spectrum::size_type ByteCount) const
{
  if ( Length <= 0 )
  {
    throw std::length_error( "Spectrum::Unpack: Length <= 0" );
  }

  Spectrum::size_type	size = 0;

  switch ( MimeType )
  {
  case REAL_BIGENDIAN:
  case REAL_LITTLEENDIAN:
    if ( ByteCount % Length )
    {
      throw std::length_error( "Spectrum::length_check: cannot determine size" );
    }
    size = ByteCount / Length;
    break;
  case COMPLEX_BIGENDIAN:
  case COMPLEX_LITTLEENDIAN:
    // Complex number have 2 elements per unit. Compensate by multiplying
    // the Length by two.
    if ( ByteCount % ( Length * 2 ) )
    {
      throw std::runtime_error( "Spectrum::length_check: cannot determine size" );
    }
    size = ByteCount / ( 2 * Length );
    break;
  }
  return size;
}

void Spectrum::
unpack( )
{
  Unpack( GetSpectrumTypeId(),
	  const_cast<CHAR_U*>( GetSpectrum() ),
	  GetLength(),
	  GetByteCount() );

  switch ( GetSpectrumTypeId() )
  {
  case REAL_BIGENDIAN:
#ifndef WORDS_BIGENDIAN
    m_header.s_type = REAL_LITTLEENDIAN;
#endif	/* WORDS_BIGENDIAN */
    break;
  case REAL_LITTLEENDIAN:
#ifdef WORDS_BIGENDIAN
    m_header.s_type = REAL_BIGENDIAN;
#endif	/* WORDS_BIGENDIAN */
    break;
  case COMPLEX_BIGENDIAN:
#ifndef WORDS_BIGENDIAN
    m_header.s_type = COMPLEX_LITTLEENDIAN;
#endif	/* WORDS_BIGENDIAN */
    break;
  case COMPLEX_LITTLEENDIAN:
#ifdef WORDS_BIGENDIAN
    m_header.s_type = COMPLEX_BIGENDIAN;
#endif	/* WORDS_BIGENDIAN */
    break;
  default:
    std::runtime_error( "Currently the requested Unpacking is not supported." );
    break;
  }
  
}

static std::map<std::string,Spectrum::SpectrumTypeId>
gen_map()
{
  std::map<std::string,Spectrum::SpectrumTypeId> m;

  m["x-ligo/real-spectrum-big"] = Spectrum::REAL_BIGENDIAN;
  m["x-ligo/real-spectrum-little"] = Spectrum::REAL_LITTLEENDIAN;
    
  m["x-ligo/complex-spectrum-big"] = Spectrum::COMPLEX_BIGENDIAN;
  m["x-ligo/complex-spectrum-little"] = Spectrum::COMPLEX_LITTLEENDIAN;

  return m;
}

std::map<std::string,Spectrum::SpectrumTypeId> Spectrum::m_id_map( gen_map() );

static std::vector< std::string > 
gen_types( void )
{
  std::vector< std::string > types;

  if ( types.size() == 0 )
  {
    types.push_back("x-ligo/real-spectrum-big");
    types.push_back("x-ligo/real-spectrum-little");
    
    types.push_back("x-ligo/complex-spectrum-big");
    types.push_back("x-ligo/complex-spectrum-little");
  }

  return types;
}

std::vector< std::string > Spectrum::m_mime_types( gen_types() );
char Spectrum::m_current_version( 0x01 );
