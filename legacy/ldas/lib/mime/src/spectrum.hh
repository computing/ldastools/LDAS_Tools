#ifndef SPECTRUM_HH
#define	SPECTRUM_HH

#include <map>
#include <stdexcept>

#include "mime/mime.hh"

namespace Mime
{
  class Spectrum: public Mime
  {
  public:
    typedef unsigned int size_type;
    typedef unsigned char data_type;
    typedef enum {
      REAL_BIGENDIAN,
      REAL_LITTLEENDIAN,
      COMPLEX_BIGENDIAN,
      COMPLEX_LITTLEENDIAN
    } SpectrumTypeId;

    typedef struct {
      INT_4U		s_start_time_seconds;
      INT_4U		s_start_time_nanoseconds;
      INT_4U		s_end_time_seconds;
      INT_4U		s_end_time_nanoseconds;
      INT_4U		s_length_in_elements;
      INT_4U		s_length_in_bytes;
      REAL_8		s_base;
      REAL_8		s_delta;
    } v1_type;

    typedef struct {
      char	s_checksum[4];	// Checksum of spectrum mime type
      char	s_version;	// Version number of header
      char	s_type;		// SpectrumTypeId reduced to a char
      union {
	v1_type	v1;
      };
    } Header_type;

    Spectrum( void );
    Spectrum( const std::string& MimeType,
	      INT_4S StartTimeSeconds,
	      INT_4S StartTimeNanoseconds,
	      INT_4S EndTimeSeconds,
	      INT_4S EndTimeNanoseconds,
	      const CHAR_U* Blob,
	      INT_4S Size,
	      INT_4S Length,
	      REAL_8 Base,
	      REAL_8 Delta);
    Spectrum( const SpectrumTypeId MimeType,
	      INT_4S StartTimeSeconds,
	      INT_4S StartTimeNanoseconds,
	      INT_4S EndTimeSeconds,
	      INT_4S EndTimeNanoseconds,
	      const CHAR_U* Blob,
	      INT_4S Size,
	      INT_4S Length,
	      REAL_8 Base,
	      REAL_8 Delta);

    //: Retrieve the base from the header
    REAL_8 GetBase( ) const;

    //: Retrieve the byte count from the header
    INT_4U GetByteCount( ) const;

    //: Retrieve the delta from the header
    REAL_8 GetDelta( ) const;

    //: Retrieve the length from the header
    INT_4U GetLength( ) const;

    //: Retrieve the spectrum data
    const CHAR_U* GetSpectrum( ) const;

    //: Retrieve the start second from the header
    INT_4U GetStartSecond( ) const;

    //: Retrieve the start nanosecond from the header
    INT_4U GetStartNanosecond( ) const;

    //: Retrieve the start second from the header
    INT_4U GetStopSecond( ) const;

    //: Retrieve the start nanosecond from the header
    INT_4U GetStopNanosecond( ) const;

    //: Retrieve SpectrumTypeId from the header
    SpectrumTypeId GetSpectrumTypeId( ) const;

    static SpectrumTypeId GetSpectrumId( const std::string& MimeType );
    static std::string GetSpectrumMimeType( SpectrumTypeId Id );

    void Unpack( const std::string& MimeType,
		 Spectrum::data_type* Spectrum,
		 Spectrum::size_type Length,
		 Spectrum::size_type ByteCount) const;
    void Unpack( const SpectrumTypeId MimeType,
		 Spectrum::data_type* Spectrum,
		 Spectrum::size_type Length,
		 Spectrum::size_type ByteCount) const;

  private:
    static std::map<std::string,SpectrumTypeId>	m_id_map;
    static std::vector< std::string >		m_mime_types;
    static char					m_current_version;
    CHAR_U const*				m_data;

    Header_type	m_header;
    
    void init_spectrum( const SpectrumTypeId MimeType,
			INT_4S StartTimeSeconds,
			INT_4S StartTimeNanoseconds,
			INT_4S EndTimeSeconds,
			INT_4S EndTimeNanoseconds,
			const CHAR_U* Blob,
			INT_4S Size,
			INT_4S Length,
			REAL_8 Base,
			REAL_8 Delta);

    //: Ensure the length of the Spectrum data is reasonable.
    Spectrum::size_type length_check( const SpectrumTypeId MimeType,
				   Spectrum::size_type Length,
				   Spectrum::size_type ByteCount) const;

    //: Unpack the data contained within the spectrum
    void unpack( );

    //: ensure the header version is valid
    int validate_header_version( ) const;
  };

  inline const CHAR_U* Spectrum::
  GetSpectrum( ) const
  {
    return m_data;
  }

  inline Spectrum::SpectrumTypeId Spectrum::
  GetSpectrumTypeId( ) const
  {
    return (Spectrum::SpectrumTypeId)(m_header.s_type);
  }

  inline void Spectrum::Unpack( const std::string& MimeType,
				Spectrum::data_type* Spectrum,
				Spectrum::size_type Length,
				Spectrum::size_type ByteCount ) const
  {
    Unpack( GetSpectrumId( MimeType ), Spectrum, Length, ByteCount );
  }

  inline int Spectrum::
  validate_header_version( ) const
  {
    if ( ( m_header.s_version > 0 ) &&
	 ( m_header.s_version <= m_current_version ) )
    {
      return m_header.s_version;
    }
    throw std::range_error( "Spectrum::validate_header_version: invalid header version" );
  }
} /* namespace Mime */

#endif	/* SPECTRUM_HH */
