#!/usr/bin/env perl
$pending_block = "";
sub postprocess()
{
    my($new_pending_block) = "";
    my($stage) = 0;
    foreach ( split '\n', $pending_block )
    {
	if ( $stage == 0 )
	{
	    if ( /^\s*\/\/+\s*$/ )
	    {
		next;
	    }
	    elsif ( /^\s*\/\/+[-]+\s*$/ )
	    {
	    }
	    else
	    {
		$stage = 1;
	    }
	}
	$new_pending_block .= "$_\n";
    }
    $pending_block = $new_pending_block;
}

sub pack($)
{
    my($line) = @_;

    if ( $line =~ /^\s*\/\// )
    {
	$pending_block = $pending_block . "$line\n";
    }
    else
    {
	postprocess;
	print "$pending_block$line\n";
	$pending_block = "";
    }
}

$con = 0;
$ignore = 0;
$translation_mode = 1;

while ( <> )
{
    chop;

    if ( $con )
    {
	s,^(\s*//)!\w+:,$1,;
    }
    $con = 0;
    if ( $_ =~ /\/\/.*\+$/ )
    {
	s,\+$,,;
	$con = 1;
    }
    if ( $ignore )
    {
	if ( $_ =~ /^\s*\/\/\!end_ignore\:\s*$/ )
	{
	    $ignore = 0;
	}
	s,^.*$,,;
    }
    elsif ( ( ! $translation_mode )
	    && ( $_ =~ /^\s*\/\/\!begin_ignore\:\s*$/ ) )
    {
	$ignore = 1;
	s,^.*$,,;
    }
    #--------------------------------------------------------------------
    # Remove lines only intended for visual effects
    #--------------------------------------------------------------------
    if ( ! $translation_mode )
    {
	s,^\s*//-+$^\s*,,m;
    }
    #--------------------------------------------------------------------
    # Traslate macros from pce2 to doxygen
    #--------------------------------------------------------------------
    s,^(\s*)//\+([^+]),$1/// $2,;
    s,^(\s*)//!param:.*\s+(\w+)\s*-,$1/// \\param $2,;
    s,^(\s*)//!param:.*\s+(\w+)\s*$,$1/// \\param $2,;
    s,^(\s*)//![r]eturn:\s*(\w+)(\s*-)?,$1/// \\return $2,;
    s,^(\s*//):,$1 \\brief ,;

    &pack($_);
}
