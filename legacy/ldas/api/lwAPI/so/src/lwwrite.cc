#include "config.h"

// System Header Files
#include <vector>   
#include <stdexcept>   

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMDocument.hpp>   
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/dom/DOMEntity.hpp>

// Local Header Files
#include "lwwrite.hh"
#include "lwdocument.hh"
#include "lwelement.hh"
#include "lwgetchild.hh"
#include "lwutil.hh"
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;
   
   
//-----------------------------------------------------------------------------
//   
//: Writes Stream element to a stream with indentation.
//
//!param: const std::string& buffer - String representing the indentation for the   +
//!param:      current element.
//!param: std::ostream& stream - The stream to write to.
//!param: DOMNode& toWrite - LWDocument node representing Table element.   
//!param: const std::string& value - The Stream value to write to the stream.
//   
//!return: Nothing.
//
//!exc: bad_table_element - No columns were found in the Table.   
//      
void writeTableStream( const std::string& buffer, std::ostream& stream, 
   const DOMNode* const toWrite, const std::string& value )
{
    const DOMNode* tableNode( toWrite->getParentNode() );
    const vector< DOMNode* > childVec = getChildByTag( tableNode,
                                                       LIGO_LW::columnTag );
    
    if ( childVec.empty() )
    {
       throw SWIGEXCEPTION( "bad_table_element" );
    }

    if ( nodeAttribute( toWrite, LIGO_LW::typeAttName ) == 
         LIGO_LW::remoteTypeAttValue )
    {
       stream << buffer << value;
       return;
    }
    
   
    string data( value );
    const string delim_string( nodeAttribute( toWrite, LIGO_LW::delimiterAttName ) );
    if( delim_string.empty() )
    {
       throw runtime_error( "writeTableStream: delimiter is missing for the stream" );
    }
   
    const CHAR delimiter( delim_string[ 0 ] );

    // Number of columns in the table
    INT_4S dim( childVec.size() );
    INT_4S i( 0 );
   
    string::size_type pos( 0 ), prev_pos( 0 );
    string line;
   
    while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
           ( prev_pos != string::npos ) )
    {
      // Do not count escaped delimiter   
      if( pos == 0 || pos == std::string::npos ||
          ( pos != 0 && data[ pos - 1 ] != bslashChar ) )
      {
         if ( i == ( dim - 1 ) )
         {
            i = 0;
            if( pos == std::string::npos )
            {
               line.assign( data, prev_pos, data.size() - prev_pos );
               prev_pos = pos;
            }
            else
            {
               line.assign( data, prev_pos, pos - prev_pos + 1 );
               prev_pos = ++pos;
            }
            stream << buffer << line;
            if ( prev_pos != std::string::npos )
            {
               stream << '\n';
            }
         }
         else
         {
            ++i;
            ++pos;
         }
      }
      else 
      {
         ++pos;
      }
   }
   
   return;
}


//-----------------------------------------------------------------------------
//   
//: Writes the document object to a stream with indentation.
//
//!param: size_t indent - The number of spaces to indent the element.
//!param: size_t delta - The number of spaces to indent children in the        +
//!param:      element.  This is relative to the indentation of the parent     +
//!param:      element.
//!param: ostream& stream - The stream to write to.
//!param: DOMNode& toWrite - LWDocument node to write to the stream.   
//
//!return: Nothing.
//
//!exc: unknown_node_type - Unsupported xml element type.
//!exc: bad_table_element - Bad Table element format is specified. No columns  +
//!exc:    were found in the Table.   
//!exc: malformed_stream_data - Stream data is malformed.   
//!exc: bad_alloc - Memory allocation failed.   
//   
void writeElement( const size_t indent, const size_t delta, 
   ostream& stream, const DOMNode* const toWrite )
{
    static const char* space = " \n\t\v\f\r";
    static const char* quote = "\"";   
    static const CHAR indent_char( ' ' );
   
    const string buffer( indent, indent_char );
   
   
    // Get the name and value out for convenience
    LIGO_LW::XString tmpNodeName = toWrite->getNodeName();
    LIGO_LW::XString tmpNodeValue = toWrite->getNodeValue();

    // To prevent logic_error in string construction ( when passing char* = 0 )
    const string node_name( tmpNodeName.charForm() ? tmpNodeName.charForm() : "" );
    const string node_value( tmpNodeValue.charForm() ? tmpNodeValue.charForm() : "" );

   
    switch( toWrite->getNodeType() )
    {
       case DOMNode::TEXT_NODE: 
       {
          string val( node_value );
   
          const DOMNode* const parentNode( toWrite->getParentNode() ); 


          if( find_if( val.begin(), val.end(), not_space ) != val.end() )
          {
             // Don't need to strip spaces for base64 data:
             std::string parentEncoding( nodeAttribute( parentNode, LIGO_LW::encodingAttName ) );   
   
             LwType parentType( getLwType( parentNode ) );
   
             if( parentType == LW_STREAM_ID &&
                 !contain( parentEncoding, LIGO_LW::base64EncodingAttValue ) )
             {
                // If text node element contains spaces and is not wrapped 
                // into double quotes, then these spaces will get erased.
                string::size_type pos( 0 ),        // Space position
                                  start_elem( 0 ), // Beginning of the element
                                  end_elem( 0 ),   // End of the element
                                  squote( 0 ),     // Opening quote within element
                                  equote( 0 );     // Closing quote within element

                while( ( pos = val.find_first_of( space, pos ) ) != string::npos )
                {
                   // This is a space at the beginning of the data
                   if( pos == 0 )
                   {
                      val.erase( pos, 1 );
                   }
                   // This is escaped space or a space of the same 
                   // element enclosed into double quotes
                   else if( val[ pos - 1 ] == bslashChar )
                   {
                      ++pos;
                   }                    
                   else if( pos > squote && pos < equote ) 
                   {
                      // Skip to the closing quote
                      pos = equote;
                   }
                   else if( val[ pos - 1 ] != bslashChar )    
                   {
                      // Space is within the same element, but not
                      // enclosed into double quotes
                      if( pos < end_elem )
                      {
                          val.erase( pos, 1 );
                          if( pos < squote )
                          {
                             --squote;
                             --equote;
                          }            
                          --end_elem;
                      }
                      else
                      {
                         // Find start position of the element
                         start_elem = pos;
                         do
                         {
                            if( start_elem != pos )
                            {
                               --start_elem;
                            }
                            start_elem = val.rfind( ',', start_elem );
                            if( start_elem == string::npos )
                            {
                               start_elem = 0;
                               break;
                            }
                         }
                         while( ( start_elem != 0 ) && 
                                ( val[ start_elem - 1 ] == bslashChar ) );

                         // Find end position of the element
                         end_elem = pos;
                         do
                         {
                            if( end_elem != pos )
                            {
                               ++end_elem;
                            }
                            end_elem = val.find( ',', end_elem );
                            if( end_elem == string::npos )
                            {
                               end_elem = val.size() - 1;
                               break;
                            }
                         }
                         while( val[ end_elem - 1 ] == bslashChar );
                      
                         // Find opening quote in the element
                         squote = start_elem;
                         squote = val.find( quote, squote );
                         if( squote > end_elem )
                         {
                            squote = std::string::npos;
                         }
                         // Find closing quote in the element
                         equote = end_elem;
                         equote = val.rfind( quote, equote );
                         if( equote < start_elem )
                         {
                            equote = std::string::npos;
                         }                 
                        
                         if( ( squote != string::npos && equote == string::npos ) ||
                             ( squote == string::npos && equote != string::npos ) )                        
                         {
                             throw SWIGEXCEPTION( "malformed_stream_data" );
                         }
                        
                         if( squote == string::npos ||
                             pos < squote ||
                             pos > equote )
                         {
                            // Erase space
                            val.erase( pos, 1 );                            
                            if( squote != string::npos && pos < squote )
                            {
                               --squote;
                               --equote;
                            }            
                            --end_elem;   
                         }
                         else
                         {
                            // Skip to the closing double quote
                            pos = equote;
                         }
                      } // else: pos belongs to the new element
                   } // if val[ pos - 1 ] != bslashChar 
                } // while find space
             } // LW_STREAM_ID
   
             if ( !val.empty() )
             {
                // Escape special characters
                const string xml_chars = "<>&";
                string::size_type pos( 0 );
                string sub_string;
                while( ( pos = val.find_first_of( xml_chars, pos ) ) != string::npos )
                {
                   switch( val[ pos ] )
                   {
                      case( '<' ):
                      {
                         sub_string = "&lt;";
                         break;
                      }
                      case( '>' ):
                      {
                         sub_string = "&gt;";
                         break;
                      }   
                      case( '&' ):
                      {
                         sub_string = "&amp;";
                         break;
                      }   
#ifdef LIGO_LW_FULL_ENTITY_SUPPORT   
                      case( '\'' ):
                      {
                         sub_string = "&apos;";
                         break;
                      }      
                      case( '\"' ):
                      {
                         sub_string = "&quot;";
                         break;
                      }         
#endif   
                      default:
                      {
                         // Should never get here
                         break;
                      }
                   }
   
                   val.erase( pos, 1 );
                   val.insert( pos, sub_string.c_str() );
                   pos += sub_string.size();
                }                
   
                switch( parentType )
                {
                   case(LW_STREAM_ID):      
                   {
                      const DOMNode* const node( parentNode->getParentNode() );
                      switch ( getLwType( node ) )
                      {
                         case(LW_TABLE_ID):
                         { 
                            writeTableStream( buffer, stream, 
                                              parentNode, val );
                            break;
                         }
                         default:
                         {
                            stream << '\n';
   
                            if( !contain( parentEncoding, LIGO_LW::base64EncodingAttValue ) )
                            {
                               stream << buffer;
                            }

                            stream << val;
                            break;
                         }
                      }
                      break;
                   }
                   default:
                   {
                      stream << val; 
                      break;
                   }
                }
             }
          } // Node value has non spaces
          else
          {
             // string Param's could be all spaces
             const string parent_type( nodeAttribute( parentNode, LIGO_LW::typeAttName ) );
   
             if( getLwType( parentNode ) == LW_PARAM_ID &&
                 parent_type == LIGO_LW::lstringTypeAttValue )
             {
                stream << node_value;
             }
          }
          break; 
       }

       case DOMNode::PROCESSING_INSTRUCTION_NODE :
       {
          stream  << "<?"
                  << node_name
                  << ' '
                  << node_value
                  << "?>";
          break;
       }
  
       case DOMNode::DOCUMENT_NODE :
       {
          stream << "<?xml version='1.0' encoding='utf-8' ?>\n";
          stream << "<!DOCTYPE LIGO_LW [\n" << LWDocument::getDTD() << '\n';
          if( ( dynamic_cast< const DOMDocument* const>( toWrite ) )->getDoctype() == 0 )
          {
             stream << "]>\n\n";
          }
          const DOMNode* child = toWrite->getFirstChild();
   
          while( child != 0) 
          {
             writeElement ( indent, delta, stream, child );
             child = child->getNextSibling();
          }
          break;
       }

       case DOMNode::DOCUMENT_TYPE_NODE :
       {
          const DOMNode* child = toWrite->getFirstChild();
          while( child != 0) 
          {
             writeElement ( indent, delta, stream, child );
             child = child->getNextSibling();
          }
          stream << "]>\n\n";
          break;
       }
       case DOMNode::ENTITY_NODE :
       {
          stream << buffer;
          const DOMEntity* const entity( dynamic_cast< const DOMEntity* const >( toWrite ) );
          stream << "<!ENTITY   ";
          stream << node_name;
          stream << "   SYSTEM " << '"';
   
          const XMLCh* tmpEntityID( entity->getSystemId() );
   
          if( tmpEntityID == 0 || 
              XMLString::stringLen( tmpEntityID ) == 0 )
          {
             tmpEntityID = entity->getPublicId();
          }
   
          
          LIGO_LW::XString char_entity( tmpEntityID );
	  if( char_entity.charForm() )
          {
             stream << char_entity.charForm();
          }
   
          stream << "\">\n";
          break;
       }

       case DOMNode::ELEMENT_NODE :
       {
          // Output indentation, the element start tag.
          stream << buffer << '<' << node_name;

          // Output any attributes on this element
          const DOMNamedNodeMap* attributes = toWrite->getAttributes();
          if( attributes )
          {
             const XMLSize_t attrCount = attributes->getLength();

             for( XMLSize_t i = 0; i < attrCount; ++i )
             {
                const DOMNode* attribute = attributes->item( i );
                
   
                // Get the name and value out for convenience
                LIGO_LW::XString tmpAttrName( attribute->getNodeName() );
                const CHAR* attrName( tmpAttrName.charForm() ); 
   
                // Attribute must have a name
                if( attrName )
                {
                   LIGO_LW::XString tmpAttrValue( attribute->getNodeValue() );   
                   const string attrValue( tmpAttrValue.charForm() ? 
                                           tmpAttrValue.charForm() : "" ); 
       
                   stream  << ' ' << attrName << "=\"" << attrValue << '"';
                }
             }
          }

          //  Test for the presence of children which includes both
          //  text content and nested elements.
          DOMNode* child = toWrite->getFirstChild();
          DOMNode* parentNode( toWrite->getParentNode() );
          if( child != 0 )
          {
             // There are children. Close start tag and output children.
             stream << ">"; 
             while( child != 0) 
             {
	       if ( ( ( child->getNodeType() != DOMNode::TEXT_NODE)
		      && ( child->getNodeType() != DOMNode::ENTITY_REFERENCE_NODE) )
		     || ( ( getLwType( parentNode ) == LW_TABLE_ID )
			  && ( getLwType( toWrite ) == LW_STREAM_ID )
			  && ( !nodeValue( toWrite ).empty() ) ) )
		{
        	   stream << '\n'; 
		}

                writeElement ( indent + delta, delta, stream, child );
                child = child->getNextSibling();
             }

             // Done with children. Output the end tag.
             // Returns 0 if strings have the same value
             if ( node_name.compare( LIGO_LW::commentTag ) && 
                  node_name.compare( LIGO_LW::paramTag )   &&
                  node_name.compare( LIGO_LW::timeTag )    &&
                  node_name.compare( LIGO_LW::dimTag ) )
	     {
	        stream << '\n' << buffer;
	     }
             stream << "</" << node_name << ">";
          }
          else
          {
             //  There were no children. Output the short form close of the
             //  element start tag making it an empty element tag.
             stream << "/>";
          }
          break;
       }

       case DOMNode::ENTITY_REFERENCE_NODE:
       {
          stream << '&' << node_name;
          stream << ';';
          break;
       }

       case DOMNode::CDATA_SECTION_NODE: 
       {
          stream << "<![CDATA[" << node_value << "]]>";
          break;
       }

       case DOMNode::COMMENT_NODE:
       {
          stream << "<!--" << node_value << "-->\n";
          break;
       }

       default:
       {
          throw SWIGEXCEPTION( "unknown_node_type" );
          break;
       }
    }

    return;
}
