#ifndef LwWriteHH
#define LwWriteHH

// System Header Files
#include <string>
#include <iostream>
#include <cstddef>

namespace XERCES_CPP_NAMESPACE
{
   class DOMNode;
}


void writeElement( 
   const size_t indent, const size_t delta, std::ostream& stream, 
   const XERCES_CPP_NAMESPACE::DOMNode* const toWrite );

   
#endif
