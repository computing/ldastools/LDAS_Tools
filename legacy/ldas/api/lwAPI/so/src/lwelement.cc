#include "config.h"

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>
   
// General Header Files   
#include <general/util.hh>   
#include <general/types.hh>   

// Local Header Files
#include "lwelement.hh"
#include "util.hh"
#include "lwutil.hh"   
   
using namespace std;   
using namespace XERCES_CPP_NAMESPACE;  

   
typedef General::unordered_map< std::string, LwType > TypeHash;


static TypeHash initTypeHash()
{
   TypeHash h;
   h[ std::string( "ligo_lw" )     ] = LW_LIGO_LW_ID;
   h[ std::string( "igwdframe" )   ] = LW_FRAME_ID;
   h[ std::string( "table" )       ] = LW_TABLE_ID;
   h[ std::string( "column" )      ] = LW_COLUMN_ID;
   h[ std::string( "array" )       ] = LW_ARRAY_ID;
   h[ std::string( "param" )       ] = LW_PARAM_ID;
   h[ std::string( "comment" )     ] = LW_COMMENT_ID;
   h[ std::string( "stream" )      ] = LW_STREAM_ID;
   h[ std::string( "dim" )         ] = LW_DIM_ID;    
   h[ std::string( "adcdata" )     ] = LW_ADCDATA_ID;    
   h[ std::string( "adcinterval" ) ] = LW_ADCINTERVAL_ID;       
   h[ std::string( "detector" )    ] = LW_DETECTOR_ID;  
   h[ std::string( "time" )        ] = LW_TIME_ID;    

   return h;
}


static TypeHash lwTypeHash( initTypeHash() );


//------------------------------------------------------------------------------
//: getLwType: detects LIGO_LW type of the element.
//
//!param: const DOMNode* const node - reference to the lightweight node.
//
//!return: LwType - element type defined by enum type.
// 
//!exc: SwigException 
//   
LwType getLwType( const DOMNode* const node )
{
   LIGO_LW::XString name( node->getNodeName() );

   string tmp = slower( name.charForm() );
   
   
   if ( lwTypeHash.find( tmp.c_str() ) != lwTypeHash.end() )
   {
      return lwTypeHash[ tmp.c_str() ];
   }
   
   return LW_UNDEFINED;
}
   

