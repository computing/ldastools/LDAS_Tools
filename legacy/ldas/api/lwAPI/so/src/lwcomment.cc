#include "config.h"

// System Header Files
#include <new>

// XML Header Files 
#include <xercesc/dom/DOMNode.hpp>

#include "general/Memory.hh"

// ILWD Header Files
#include <ilwd/ldasstring.hh>
using ILwd::LdasString;

// Local Header Files
#include "lwcomment.hh"
#include "lwutil.hh"
#include "query.hh"

using namespace std;   
using namespace XERCES_CPP_NAMESPACE;  
   
static const char* xml_name = "Comment:XML";

   
//-------------------------------------------------------------------------------
//   
//: Converts xml element to the ILWD format string..
//
//!exc: SwigException
//!exc: LdasException   
//!exc: bad_alloc - Memory allocation failed.
//      
unique_ptr< ILwd::LdasElement > comment2ILwd( 
   const DOMNode* const node, std::deque< Query > dq ) 
{
   return unique_ptr< ILwd::LdasElement >( new LdasString( nodeValue( node ), xml_name ) );
}
