#include "config.h"

// Local Header Files   
#include "lwutil.hh"

using namespace std;   
using namespace XERCES_CPP_NAMESPACE;
   

//-----------------------------------------------------------------------------
//   
//: getElementAttribute.
//   
//  Method to retrieve attribute value by name
//
//!param: const DOMNode* const node - A reference to the node.
//!param: const CHAR* const name - Name of the attribute;
// 
//!return: const std::string - Value of the attribute.
// 
//!exc: SwigException   
//
const std::string getElementAttribute( const DOMNode* const node, 
   const CHAR* const name )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "getElementAttribute: NULL DOMNode is passed" );   
   }
   
   const string value( nodeAttribute( node, name ) );
   if( value.empty() )
   {
      return "NULL";
   }
   
   return value;
}


//-----------------------------------------------------------------------------
//
//: Extract element's attribute value by name.
//: Example: for <node Name="elem" Type="nType"/>
//:          nodeAttr( nodeRef, "Type" ) will return "nType"
//
//!param: const DOMNode* const node - A pointer to the element node.
//!param: const CHAR* const name - Name of the attribute.
//
//!return: std::string -  attribute value, or empty string if attribute is empty.
//
//!exc: SwigException -
//!exc:   - not_element_node - 
//
const std::string nodeAttribute( const DOMNode* const node, const CHAR* const name )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "nodeAttribute: NULL DOMNode is passed" );   
   }
   
   if ( node->getNodeType() != DOMNode::ELEMENT_NODE )
   {
      throw SWIGEXCEPTION( "Requesting an attribute for non DOMElement object" );
   }

   const DOMElement* const elem( dynamic_cast< const DOMElement* const >( node ) );
   
   // Transcode name from char* to the xmlch*
   ArrayJanitor< XMLCh > xml_name( XMLString::transcode( name ) );

   if( elem->hasAttribute( xml_name.get() ) == false )
   {
      // Element doesn't have named attribute
      return "";
   }
   
   // Extract attribute by name
   const XMLCh* att_value( elem->getAttribute( xml_name.get() ) );   
   
   if( att_value == 0 || *att_value == 0 ) 
   {
      return "";
   }

   
   ArrayJanitor< CHAR > att( XMLString::transcode( att_value ) );
   return att.get();   
}   


//-----------------------------------------------------------------------------
//
//: Extract node's value: nodeValue
//: Example: for <node name="elem">nodeValue</node>
//:          nodeValue( nodeRef ) will return "nodeValue"
//
//!param: const DOMNode& node - element node( only Comment, Dim, or Param 
//+       nodes can be passed to this function).
//
//!exc: SwigException -
//!exc:   - not_element_node
//!exc:   - text_node_expected
//
//!return: std::string - node value, or an empty string if node doesn't have a value.
//
const std::string nodeValue( const DOMNode* const node )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "nodeValue: NULL DOMNode is passed" );   
   }
   
   if ( node->getNodeType() != DOMNode::ELEMENT_NODE )
   {
     throw SWIGEXCEPTION( "Requesting a value for non DOMElement object" );
   }
  
   const DOMNode* textNode = node->getFirstChild();
   if( textNode == 0 )
   {
      return "";
   }

   
   if ( textNode->getNodeType() != DOMNode::TEXT_NODE )
   {
      throw SWIGEXCEPTION( "Text child node is required to extract value of the node." );
   }

   
   const XMLCh* node_value( textNode->getNodeValue() );
   if( node_value == 0 || *node_value == 0 )
   {
      return "";
   }

   
   ArrayJanitor< CHAR > value( XMLString::transcode( node_value ) );   
   return value.get();
}


//-----------------------------------------------------------------------------
//
//: Get element's name and convert it into the string.
//
//!param: const DOMNode& node - element node;
//
//!exc: SwigException -
//!exc:   - not_element_node
//
//!return: const std::string - Element name.
//
//!exc: SwigException
//   
const std::string nodeName( const DOMNode* const node )
{
  if ( node->getNodeType() != DOMNode::ELEMENT_NODE )
  {
     throw SWIGEXCEPTION( "Requesting name tag for non DOMElement object" );
  }

  const XMLCh* node_name( node->getNodeName() );
  if ( node_name == 0 || *node_name == 0 )
  {
     return "";
  }

  ArrayJanitor< CHAR > name( XMLString::transcode( node_name ) );
  return name.get();
}


//-----------------------------------------------------------------------------
//
//: Gheck if test_string contains token (case-insensitive).
//
//!param: const std::string& test_string - String to test.
//!param: const std::string& token - Token to check for.   
//
//!return: const bool - True if test-string contains token, false otherwise.
//
bool contain( const std::string& test_string, const std::string& token )
{
   if( test_string.empty() )  
   {
      return false;
   }
   
   if( test_string.find( token ) != std::string::npos )
   {
      return true;
   }
   
   return false;
}
   
