#include "config.h"

// System Header Files
#include <new>
#include <sstream>

#include <general/util.hh>
#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
using ILwd::LdasContainer;
using ILwd::LdasElement;   

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>   
   
// Local Header Files
#include "lwframe.hh"
#include "lwadcdata.hh"
#include "lwdetector.hh"
#include "lwligolw.hh"
#include "lwparam.hh"
#include "lwarray.hh"
#include "lwtime.hh"
#include "lwelement.hh"
#include "lwcomment.hh"   
#include "query.hh"
#include "lwutil.hh"
#include "util.hh"

using namespace std;   
using namespace XERCES_CPP_NAMESPACE;  

   
static LwDataHash initLwFrameHash()
{
   LwDataHash h;
   h[ LW_FRAME_ID    ] = lwFrame2ILwd;
   h[ LW_ADCDATA_ID  ] = lwAdcData2ILwd;   
   h[ LW_DETECTOR_ID ] = lwDetector2ILwd;
   h[ LW_LIGO_LW_ID  ] = ligoLw2ILwd;
   h[ LW_PARAM_ID    ] = param2ILwd;
   h[ LW_ARRAY_ID    ] = array2ILwd;
   h[ LW_TIME_ID     ] = time2ILwd;
   h[ LW_COMMENT_ID  ] = comment2ILwd;   
   return h;
}


static LwDataHash frameLwHash( initLwFrameHash() );
static const char* xml_name = ":Frame:XML";


//-----------------------------------------------------------------------
//
//: Convert Frame header element into ILWD format container.
//
//!param: node - LIGO_LW Frame header element to convert.
//!param: dq - Tokenized command.
//
//!return: ILWD container element.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: SwigException
//   
unique_ptr< ILwd::LdasElement > lwFrame2ILwd( 
   const DOMNode* const node, std::deque< Query > dq )
{
   string name = nodeAttribute( node, LIGO_LW::nameAttName );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );

   
   DOMNode* child( node->getFirstChild() );
   LwType type;   
   
   
   while( child != 0) 
   {
      if ( child->getNodeType() == DOMNode::ELEMENT_NODE )
      {
         type = getLwType( child );   
   
         if( frameLwHash.find( type ) != frameLwHash.end() )
         {
            c->push_back( frameLwHash[ type ]( child, dq ).release(),
                          ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
         }
         else if( type != LW_STREAM_ID )
         {
            ostringstream s;
            s << "frame_unsupported_element_type: " 
              << getLwType( child );

            throw SWIGEXCEPTION( s.str() );
         }
      }
      child = child->getNextSibling();
   }
   
   return unique_ptr< LdasElement >( c.release() );
}
