#include "config.h"

// System Header Files
#include <new>
#include <sstream>   

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
using ILwd::LdasContainer;
using ILwd::LdasElement;   

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// Local Header Files
#include "lwdetector.hh"
#include "lwcomment.hh"
#include "lwligolw.hh"
#include "lwparam.hh"
#include "lwarray.hh"   
#include "lwelement.hh"
#include "query.hh"
#include "util.hh"
#include "lwutil.hh"

using namespace std;
using namespace XERCES_CPP_NAMESPACE;  
   

static LwDataHash initDetectorHash()
{
   LwDataHash h;
   
   h[ LW_LIGO_LW_ID ] = ligoLw2ILwd;
   h[ LW_PARAM_ID   ] = param2ILwd; 
   h[ LW_COMMENT_ID ] = comment2ILwd;
   h[ LW_ARRAY_ID   ] = array2ILwd;   
   
   return h;
}

static LwDataHash detectorHash( initDetectorHash() );
static const char* xml_name = ":Detector:XML";


//-----------------------------------------------------------------------
//: Convert LIGO_LW Detector into ILWD format array.
//
//!param: const DOMNode* const node - Table element to convert.
//!param: std::deque< Query > dq - Tokenized command.
//
//!return: ILWD container element.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: detector_unsupported_element_type - Unknown element type is 
//+     detected as Detector child.
//
unique_ptr< LdasElement > lwDetector2ILwd( 
   const DOMNode* const node, std::deque< Query > dq )
{
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );
   
   
   DOMNode* child = node->getFirstChild();
   LwType type;   
   
   while( child != 0) 
   {
      if ( child->getNodeType() == DOMNode::ELEMENT_NODE )
      {   
         type = getLwType( child );
         if( detectorHash.find( type ) != detectorHash.end() )
         {
            c->push_back( detectorHash[ type ]( child, dq ).release(),
                          ILwd::LdasContainer::NO_ALLOCATE,
                          ILwd::LdasContainer::OWN );
         }
         else
         {
            ostringstream s;
            s << "detector_unsupported_element_type: " 
              << type;
   
            throw SWIGEXCEPTION( s.str() );   
         }
      }
      
      child = child->getNextSibling();
   }
   
   return unique_ptr< LdasElement >( c.release() );
}
