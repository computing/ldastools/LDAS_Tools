#include "config.h"

#include <sys/types.h>

// System Header Files
#include <new>

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
using ILwd::LdasContainer;
using ILwd::LdasElement;   

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>
   
// Local Header Files
#include "lwadcdata.hh"
#include "lwcomment.hh"
#include "lwligolw.hh"
#include "lwarray.hh"
#include "lwparam.hh"
#include "lwtime.hh"
#include "lwelement.hh"
#include "query.hh"
#include "util.hh"
#include "lwutil.hh"

using namespace std;
using namespace XERCES_CPP_NAMESPACE;   
   

static LwDataHash initAdcDataHash()
{
   LwDataHash h;
   
   h[ LW_LIGO_LW_ID ] = ligoLw2ILwd;
   h[ LW_ADCDATA_ID ] = lwAdcData2ILwd;
   h[ LW_ARRAY_ID   ] = array2ILwd;
   h[ LW_PARAM_ID   ] = param2ILwd; 
   h[ LW_COMMENT_ID ] = comment2ILwd;
   h[ LW_TIME_ID    ] = time2ILwd;
   
   return h;
}

static LwDataHash adcDataHash( initAdcDataHash() );
static const char* xml_name = ":AdcData:XML";


//-----------------------------------------------------------------------
//: Convert LIGO_LW AdcData into ILWD format array.
//
//!param: const DOMNode* const node - A reference to the LIGO_LW element to convert.
//!param: std::deque< Query > dq - Tokenized command.
//
//!return: LdasElement* - An ILWD format container element.   
//
unique_ptr< LdasElement > lwAdcData2ILwd( 
   const DOMNode* const node, std::deque< Query > dq )
{
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );


   DOMNode* child( node->getFirstChild() );
   LwType type;

   while( child != 0) 
   {
      if( child->getNodeType() == DOMNode::ELEMENT_NODE )
      {    
         type = getLwType( child );
         if( adcDataHash.find( type ) != adcDataHash.end() )
         {
            c->push_back( adcDataHash[ type ]( child, dq ).release(),
                          LdasContainer::NO_ALLOCATE,
                          LdasContainer::OWN );
         }
         else
         {
            ostringstream s;
            s << "adcdata_unsupported_element_type: " 
              << type;
   
            throw SWIGEXCEPTION( s.str() );   
         }
      }
   
      child = child->getNextSibling();
   }

   
   return unique_ptr< LdasElement >( c.release() );
}
