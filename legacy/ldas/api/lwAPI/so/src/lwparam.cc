#include "config.h"

#include "general/Memory.hh"

// Local Header Files
#include "lwparam.hh"

// ILWD Header Files
#include <ilwd/ldasstring.hh>
using ILwd::LdasString;
using ILwd::LdasElement;   
   
   
using namespace std;
using namespace XERCES_CPP_NAMESPACE;
   
   
const string param_xml_name( ":Param:XML" );


//-----------------------------------------------------------------------
//
//: Converts LIGO_LW format parameter element into ILWD format array.
//
//!param: node - LIGO_LW parameter element to convert.
//!param: query - User command to specify the element to extract.
//
//!return: LdasElement* - A pointer to the ILWD format element.
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: bad_query - Query is bad.
//!exc: unsupported_type - Element type is not supported.
//!exc: bad_alloc - Memory allocation failed.   
//
unique_ptr< LdasElement > param2ILwd( const DOMNode* const node,
   std::deque< Query > dq ) 
{
   // Based on the type of the Parameter element, create corresponding 
   // ILWD format element:
   string type = nodeAttribute( node, LIGO_LW::typeAttName ); 
    
   if( ( type == "lstring" ) || ( type == "string" ) || ( type == "boolean" ) )
   {
      string name = nodeAttribute( node, LIGO_LW::nameAttName );      
   
      if( name.find( LIGO_LW::detectorPrefix ) != string::npos )
      {
         return param2ILwd< CHAR >( node );         
      }
   
      if( name.find( param_xml_name ) == string::npos )
      {
         name.append( param_xml_name );
      }

      return unique_ptr< LdasElement >( new LdasString( nodeValue( node ), name.c_str() ) );
   }
   else if ( ( type == "char" ) || ( type == "byte" ) )
   {
      return param2ILwd< CHAR >( node );
   }
   else if ( type == "char_u" )
   {
      return param2ILwd< CHAR_U >( node );
   }
   else if ( ( type == "int_2s" ) || ( type == "short" ) )
   {
      return param2ILwd< INT_2S >( node );
   }
   else if ( type == "int_2u" )
   {
      return param2ILwd< INT_2U >( node );
   }
   else if ( ( type == "int_4s" ) || ( type == "int" ) )
   {
      return param2ILwd< INT_4S >( node );
   }
   else if ( type == "int_4u" )
   {
      return param2ILwd< INT_4U >( node );
   }
   else if ( ( type == "int_8s" ) || ( type == "long" ) )
   {
      return param2ILwd< INT_8S >( node );
   }
   else if ( type == "int_8u" )
   {
      return param2ILwd< INT_8U >( node );
   }
   else if ( ( type == "real_4" ) || ( type == "float" ) )
   {
      return param2ILwd< REAL_4 >( node );
   }
   else if ( ( type == "real_8" ) || ( type == "double" ) )
   {
      return param2ILwd< REAL_8 >( node );
   }
   else if ( type == "complex_8" )
   {
      return param2ILwd< COMPLEX_8 >( node );
   }   
   else if ( type == "complex_16" )
   {
      return param2ILwd< COMPLEX_16 >( node );
   }      
   else
   {
      string msg( "unsupported_type: " );
      msg += type;
      throw SWIGEXCEPTION( msg );
   }
   
   return unique_ptr< LdasElement >( 0 );
}
