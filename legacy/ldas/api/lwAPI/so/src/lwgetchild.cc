#include "config.h"

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>      
#include <xercesc/dom/DOMNodeList.hpp>

// Generic Header Files   
#include <genericAPI/swigexception.hh>   
   
// Local Header Files
#include "lwgetchild.hh"
#include "lwutil.hh"   

using namespace XERCES_CPP_NAMESPACE;   


//-----------------------------------------------------------------------------
//   
//: Get children of the element by tag name.
//
//!param: const DOMNode* node - Element to be searched for the children nodes.
//!param: const CHAR* const name - Name of the child.
// 
//!return: Vector of found child elements (they are "live" elements of the node)
//+       ("shallow pointer" term in xercesc)   
//
const std::vector< DOMNode* > getChildByTag( 
   const DOMNode* const node, const CHAR* const name )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "NULL parent node is passed to \"getChildByTag\"" );
   }
   
   // "live" node list
   const DOMNodeList* nodeList = node->getChildNodes();

   std::vector< DOMNode* > nodeVector;   
   if( nodeList == 0 )
   {
      return nodeVector;
   }
   
   const XMLSize_t len( nodeList->getLength() );
   if( len == 0 )
   {
      return nodeVector;
   }
   
   // Transcode name from char* to the xmlch*
   LIGO_LW::XString xml_name( name );   
   
   for( XMLSize_t i = 0; i < len; ++i )
   {
      if( XMLString::equals( nodeList->item( i )->getNodeName(), 
                             xml_name.unicodeForm() ) )
      {
	 // Update vector
	 nodeVector.push_back( nodeList->item( i ) );
      }
   }

   return nodeVector;
}
   
   
//-----------------------------------------------------------------------------
//   
//: Get children of the element by node type. 
//
//!param: const DOMNode* node - Element to be searched for the children.
//!param: const CHAR* const name - Node type of the child.
// 
//!return: Vector of found child elements (they are "live" elements of the node)
//+       ("shallow pointer" term in xercesc)   
//
const std::vector< DOMNode* > getChildByType( const DOMNode* const node, 
   const DOMNode::NodeType& type )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "NULL parent node is passed to \"getChildByType\"" );
   }
   
   const DOMNodeList* nodeList( node->getChildNodes() );
  
   std::vector< DOMNode* > nodeVector;   
   if( nodeList == 0 || nodeList->getLength() == 0 )
   {
      return nodeVector;
   }
   
   for ( XMLSize_t i = 0, len = nodeList->getLength(); i < len; ++i )
   {
      if ( nodeList->item( i )->getNodeType() == type )
      {
	 // Update vector
	 nodeVector.push_back( nodeList->item( i ) );
      }
   }

   return nodeVector;
}

