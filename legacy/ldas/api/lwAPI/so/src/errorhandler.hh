// XML Header Files
#include <xercesc/sax/HandlerBase.hpp>


//------------------------------------------------------------------------------
//
//: XML parser error reporter.
//
// This class implements error reporter for XML parser.
// <p>The error reporter gets set by parser before parsing any xml documents.
//
class ParserErrorReporter : public XERCES_CPP_NAMESPACE::HandlerBase
{
public:
    /* Constructor */
    ParserErrorReporter();

    /* Destructor */
    virtual ~ParserErrorReporter();

    void warning( const XERCES_CPP_NAMESPACE::SAXParseException& toCatch );
    void error( const XERCES_CPP_NAMESPACE::SAXParseException& toCatch );
    void fatalError( const XERCES_CPP_NAMESPACE::SAXParseException& toCatch );
    void resetErrors();
};
