#ifndef DATA_SOCKET_H
#define DATA_SOCKET_H


int get_data_socket(int *error, FILE* ctrl_in, FILE* ctrl_out);
int user_login( char* user, char* password, int* error, FILE* ctrl_in, FILE* ctrl_out);

#endif
