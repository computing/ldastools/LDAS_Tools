#include "config.h"

// System Header Files
#include <new>

#include "general/Memory.hh"

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// ILWD Header Files
#include <ilwd/ldasstring.hh>
using ILwd::LdasString;
using ILwd::LdasElement;   

// Local Header Files
#include "query.hh"
#include "lwdim.hh"
#include "lwutil.hh"
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE; 
   
   
static const char* xml_name = ":Dim:XML";


//--------------------------------------------------------------------------------
//
//: Convert LIGO_LW Dim element into ILWD format.
//
//!param: const DOMNode* const node - LIGO_LW Dim element.
//!param: std::deque< Query > dq - Tokenized command.
//
//!return: LdasElement* - Pointer to ILWD format element.
//
//!exc: bad_query - Query is bad.
//!exc: bad_alloc - Memory allocation failed.   
//
unique_ptr< LdasElement > dim2ILwd( 
   const DOMNode* const node, deque< Query > dq ) 
{
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   
   string units( nodeAttribute( node, LIGO_LW::unitAttName ) );
   string comment = "";
   
   return unique_ptr< LdasElement >( new LdasString( nodeValue( node ), 
                                                   name, 
                                                   comment,
                                                   units ) );
}
