#include "config.h"

// System Header Files
#include <cstddef>

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// Local Header Files
#include "query.hh"
#include "getattribute.hh"
#include "lwutil.hh"
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;
   
   
//------------------------------------------------------------------------------
//
//: Extract element from the list by specifying the name attribute value.
//
//!param: const std::vector< DOMNode* >& nodeList - List of elements.
//!param: const std::string& nameValue - Value of the Name attribute. 
//!param: std::vector< size_t >& start - Element position vector.
//!param: size_t index - Current index into the position vector.
//
//!return: const DOMNode* const - Found element.
//
//!exc: SwigException
//   
DOMNode* getElement( 
   const std::vector< DOMNode* >& nodeList,
   const std::string& nameValue, std::vector< size_t >& start,
   const size_t index )
{
   for( vector< DOMNode* >::const_iterator iter = nodeList.begin(),
           end_iter = nodeList.end(); iter != end_iter; ++iter )
   {
      if( getElementAttribute( *iter, LIGO_LW::nameAttName ) ==
          nameValue )
      {
         start[ index ] = iter - nodeList.begin();
	 return *iter;
      }
   }

   throw SWIGEXCEPTION( "not_valid_element" );
}


//---------------------------------------------------------------------
//
//: Extract specified element from the list.
//
//!param: const std::vector< DOMNode* >& nodeList - List of elements.
//!param: Query& q - Query to specify an element index or name in the list.
//!param: std::vector< size_t >& start - Element position vector.
//!param: size_t index - Current index into the position vector.   
//
//!return: const DOMNode* const - Found element.
//
//!exc: SwigException
//   
DOMNode* getContained( 
   const std::vector< DOMNode* >& nodeList, 
   Query& q, std::vector< size_t >& start, const size_t index ) 
{
   if ( nodeList.empty() ) 
   {
      return 0;
   }
   
   if ( index == start.size() )
   {
      start.push_back( 0 );
   }
  
   if ( q.getQuery() == "index" )
   {
      size_t i = strtoul( q.getValue().c_str(), 0, 10 );
      if ( i >= nodeList.size() )
      {
         throw SWIGEXCEPTION( "not_valid_element" );
      }
   
      start[ index ] = i;
      return nodeList[ i ];
   }
   else
   if ( q.getQuery() == "name" )
   {
      return getElement( nodeList, q.getValue(), start, index );
   }
   
   throw SWIGEXCEPTION( "bad_query" );
   return 0;
}

                             
