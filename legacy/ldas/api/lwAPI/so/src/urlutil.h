#ifndef URL_UTIL_H
#define URL_UTIL_H


#define ERRNO_NOT_SET                  0

/* normal return */
#define URL_CONTENT_READ               1
#define URL_IS_NOT_HTTP                2
#define URL_IS_NULL_STRING             3
#define TOO_MANY_HEADER_LINES          4
#define URL_ERROR_SOCKET               5
#define NO_DNS_ENTRY                   6
#define BIND_FAILED                    7
#define CONNECT_FAILED                 8
#define WRITE_GET_REQUEST_FAILED       9
#define ERROR_SETSOCKOPT_IP_TOS        10


#define URL_ERROR_CREATE_LOCAL_FILE    20
#define URL_ERROR_WRITE_LOCAL_FILE     21
#define URL_ERROR_WRITE_URL_FILE       22
#define URL_ERROR_OPEN_LOCAL_FILE      23
#define URL_ERROR_MALLOC               24
#define FTP_ERROR_OPEN_FTP_FILE        25
#define FTP_ERROR_LOGIN_FAILED         26
#define FTP_USER_ERROR                 27
#define FTP_PASSWORD_ERROR             28
#define URL_ERROR_PASV_COMMAND         29
#define URL_ERROR_PASV_SCAN            30
#define URL_ERROR_COPY_FILE            31
#define URL_ERROR_PUT_FILE             32
#define URL_ERROR_FILE_MISSING         33
#define FTP_ERROR_INVALID_DIRECTORY    34
#define FTP_ERROR_GET_FAILED           35
#define FTP_ERROR_TYPE_FAILED          36

#define HTTP_CONTENT_READ              80

/* add this to error-code from URL header */
#define HTTP_ERROR                     100


#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

char* openFTP
          (
             const char* fullPath,
             const char* hostName,
             int   portNumber,
             const char* fileName,
             int*  error
          );

char *fgets_fd(char* buf, int nmax, int fd);
int   fgets_fp(char* read_buffer, int nmax, FILE* fp);

char* recv_file( FILE* file_in, int* error);
int put_file(char *ftp_file, FILE* file_out, int* error);


#ifdef __cplusplus
}
#endif

#endif
