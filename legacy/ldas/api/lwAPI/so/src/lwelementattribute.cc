#include "config.h"

// System Header Files
#include <algorithm>
#include <memory>

#include "general/Memory.hh"

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// Local Header Files
#include "lwelementattribute.hh"
#include "lwgetchild.hh"
#include "getattribute.hh"
#include "lwutil.hh"
#include "query.hh"
#include "vect.hh"
#include "lwconsts.hh"

using namespace std;
using namespace XERCES_CPP_NAMESPACE;   
   

//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access LIGO_LW element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access LIGO_LW element attributes.
//
//!returns: Attribute - Initialized structure.
//
ElementAttribute::Attribute& ElementAttribute::initLigoLw()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
      h[ "type" ] = LIGO_LW::typeAttName;
   }

   static ElementSet e;


   if( e.empty() )
   {
      e.insert( "ligo_lw" );
      e.insert( "table" );
      e.insert( "array" );
      e.insert( "igwdframe" );
      e.insert( "adcdata" );
      e.insert( "param" );
      e.insert( "time" );
      e.insert( "stream" );
      e.insert( "adcinterval" );
   }

   static Attribute ligolwAttribute( h, e );
   return ligolwAttribute;    
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Array element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Array element attributes.
//
//!returns: Attribute -
//
ElementAttribute::Attribute& ElementAttribute::initLwArray()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
      h[ "type" ] = LIGO_LW::typeAttName;
      h[ "unit" ] = LIGO_LW::unitAttName;   
   }

   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "dim" );
      e.insert( "stream" );
   }
 
   static Attribute arrayAttribute( h, e );
   return arrayAttribute;     
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Table element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Table element attributes.
//
//!returns: Attribute -
//
ElementAttribute::Attribute& ElementAttribute::initLwTable()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
      h[ "type" ] = LIGO_LW::typeAttName;
   }

   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "column" );
      e.insert( "stream" );
   }

   static Attribute tableAttribute( h, e );
   return tableAttribute;  
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Dim element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Dim element attributes.
//
//!returns: Attribute -
//
ElementAttribute::Attribute& ElementAttribute::initLwDim()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name"  ] = LIGO_LW::nameAttName;
      h[ "unit"  ] = LIGO_LW::unitAttName;
      h[ "start" ] = LIGO_LW::startAttName;   
      h[ "scale" ] = LIGO_LW::scaleAttName;   
   }

   static ElementSet e;

   static Attribute dimAttribute( h, e );
   return dimAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Column element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Column element attributes.
//
//!returns: Attribute -
//
ElementAttribute::Attribute& ElementAttribute::initLwColumn()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
      h[ "type" ] = LIGO_LW::typeAttName;
      h[ "unit" ] = LIGO_LW::unitAttName;
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS  
      h[ "dataunit" ] = LIGO_LW::dataUnitAttName;   
      h[ "start"    ] = LIGO_LW::startAttName;   
      h[ "scale"    ] = LIGO_LW::scaleAttName;   
#endif   
   }

   static ElementSet e;

   static Attribute columnAttribute( h, e );
   return columnAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Stream element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Stream element attributes.
//
//!returns: Attribute -
//
ElementAttribute::Attribute& ElementAttribute::initLwStream()
{
   static AttrHash h;
   if( h.empty() )
   {   
      h[ "name"      ] = LIGO_LW::nameAttName;
      h[ "type"      ] = LIGO_LW::typeAttName;
      h[ "delimiter" ] = "Delimiter";
      h[ "content"   ] = "Content";
      h[ "encoding"  ] = "Encoding";
   }
 
   static ElementSet e;

   static Attribute streamAttribute( h, e );
   return streamAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Param element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Param element attributes.
//
//!returns: Attribute -
//
ElementAttribute::Attribute& ElementAttribute::initLwParam()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name"     ] = LIGO_LW::nameAttName;
      h[ "type"     ] = LIGO_LW::typeAttName;
      h[ "unit"     ] = LIGO_LW::unitAttName;
      h[ "start"    ] = LIGO_LW::startAttName;   
      h[ "scale"    ] = LIGO_LW::scaleAttName;
      h[ "dataunit" ] = LIGO_LW::dataUnitAttName;   
   }
   
   static ElementSet e;

   static Attribute paramAttribute( h, e );
   return paramAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Frame header element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Frame header element attributes.
//
//!returns: Attribute - Initialized structure.
//
ElementAttribute::Attribute& ElementAttribute::initLwFrame()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
   }

   static ElementSet e;
 
   if( e.empty() )
   {
     e.insert( "igwdframe" );
     e.insert( "detector" );
     e.insert( "adcdata" );
     e.insert( "array" );
     e.insert( "time" );
     e.insert( "stream" );
     e.insert( "param" );
     e.insert( "ligo_lw" );
   }

   static Attribute frameAttribute( h, e );
   return frameAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access AdcData element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access AdcData element attributes.
//
//!returns: Attribute - Initialized structure.
//
ElementAttribute::Attribute& ElementAttribute::initLwAdcData()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
   }

   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "param" );
      e.insert( "adcdata" );
      e.insert( "time" );
      e.insert( "ligo_lw" );
   }

   static Attribute adcAttribute( h, e );
   return adcAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Detector element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Detector element attributes.
//
//!returns: Attribute - Initialized structure.
//
ElementAttribute::Attribute& ElementAttribute::initLwDetector()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
   }

   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "param" );
      e.insert( "ligo_lw" );
   }

   static Attribute detectorAttribute( h, e );
   return detectorAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access Time element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access Time element attributes.
//
//!returns: Attribute - Initialized structure.
//
ElementAttribute::Attribute& ElementAttribute::initLwTime()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name" ] = LIGO_LW::nameAttName;
      h[ "type" ] = LIGO_LW::typeAttName;
   }

   static ElementSet e;

   static Attribute timeAttribute( h, e );
   return timeAttribute;
}


//------------------------------------------------------------------------------
//
//: Initialize Attribute structure to access AdcInterval element attributes.
//
// This method initializes unordered_map and set to have appropriate information
// to access AdcInterval element attributes.
//
//!returns: Attribute - Initialized structure.
//
ElementAttribute::Attribute& ElementAttribute::initLwAdcInterval()
{
   static AttrHash h;
   if( h.empty() )
   {
      h[ "name"      ] = LIGO_LW::nameAttName;
      h[ "starttime" ] = "StartTime";   
      h[ "deltat"    ] = "DeltaT";      
   }

   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "adcdata" );
      e.insert( "time" );
   }

   static Attribute adcIntervalAttribute( h, e );
   return adcIntervalAttribute;
}
   

//------------------------------------------------------------------------------
//
//: Initialize LwAttribute unordered_map.
//
// This method initializes hash map used to extract LIGO_LW element 
// attributes. It associates hash maps with a different element types in
// LIGO_LW document.
//
//!returns: LwAttribute - Hash map.
//
ElementAttribute::LwAttribute& ElementAttribute::initAllElements()
{
   static LwAttribute h;
   if( h.empty() )
   {
      static unique_ptr< attrPair > ligolw_pair( new attrPair( "LIGO_LW", initLigoLw() ) );
      h[ "ligo_lw"     ] = ligolw_pair.get();

      static unique_ptr< attrPair > table_pair( new attrPair( "Table", initLwTable() ) );
      h[ "table"       ] = table_pair.get();

      static unique_ptr< attrPair > array_pair( new attrPair( "Array", initLwArray() ) );
      h[ "array"       ] = array_pair.get();

      static unique_ptr< attrPair > dim_pair( new attrPair( "Dim", initLwDim() ) );
      h[ "dim"         ] = dim_pair.get();

      static unique_ptr< attrPair > column_pair( new attrPair( "Column", initLwColumn() ) );
      h[ "column"      ] = column_pair.get();

      static unique_ptr< attrPair > stream_pair( new attrPair( "Stream", initLwStream() ) );
      h[ "stream"      ] = stream_pair.get();

      static unique_ptr< attrPair > param_pair( new attrPair( "Param", initLwParam() ) );
      h[ "param"       ] = param_pair.get();

      static unique_ptr< attrPair > frame_pair( new attrPair( "IGWDFrame", initLwFrame() ) ); 
      h[ "igwdframe"   ] = frame_pair.get();

      static unique_ptr< attrPair > adc_pair( new attrPair( "AdcData", initLwAdcData() ) );
      h[ "adcdata"     ] = adc_pair.get();

      static unique_ptr< attrPair > detector_pair( new attrPair( "Detector", initLwDetector() ) );
      h[ "detector"    ] = detector_pair.get();

      static unique_ptr< attrPair > time_pair( new attrPair( "Time", initLwTime() ) );
      h[ "time"        ] = time_pair.get();

      static unique_ptr< attrPair > adcinterval_pair( new attrPair( "AdcInterval", initLwAdcInterval() ) ); 
      h[ "adcinterval" ] = adcinterval_pair.get();
   }

   return h;
}


ElementAttribute::LwAttribute& ElementAttribute::mAllElements = initAllElements();

   
//------------------------------------------------------------------------------
//
//: ( Default ) Constructor.
//   
ElementAttribute::ElementAttribute()
   : mHash( initLigoLw() )
{}

   
//------------------------------------------------------------------------------
//  
//: Constructor.
//
//!param: Attribute& a - Attribute structure to be initialized with.
//   
ElementAttribute::ElementAttribute( const Attribute& a )
   : mHash( a )
{}
   
   
//------------------------------------------------------------------------------
//
//: Operator() for ElementAttribute.
//
// This operator gets an attribute of the specified LIGO_LW element.
//
//!param: node - LIGO_LW element to be searched for specified attribute.
//!param: dq - Tokenized command.
//!param: start - Vector to record element position in the document tree. 
//!param: index - Current index into position vector.
//
//!return: Attribute value or "NULL" if attribute is not found.
//
//!exc: bad_query - Query is bad.
//!exc: not_valid_element - Element is not valid.
//!exc: ElementAttribute_unknown_error - Unknown error has occured.
//
const std::string ElementAttribute::operator()( DOMNode* const node, 
   std::deque< Query > dq, std::vector< size_t >& start, const size_t index )
{
    if( dq.size() == 0 )
    {
       throw SWIGEXCEPTION( "bad_query" );
    }
   
    if( node == 0 )
    {
       throw SWIGEXCEPTION( "not_valid_element" );
    }

   
    Query q = dq.front();
    dq.pop_front();
    const CHAR* hashIndex( q.getName().c_str() );

    if( !q.isQuery() )
    {
       if( mHash.Attr.size() != 0 )
       {
          if( mHash.Attr.find( hashIndex ) != mHash.Attr.end() )
          {
             return getElementAttribute( node, mHash.Attr[ hashIndex ] );
          }
       }
       if( mHash.Element.size() != 0 )
       {
          if( mHash.Element.count( hashIndex ) == 1 )
          {
             const vector< DOMNode* > nodeVector( getChildByTag( node, 
                                                     mAllElements[ hashIndex ]->first ) );
   
             // Set up new attribute hash and element set
             ElementAttribute e( mAllElements[ hashIndex]->second );
             return e.find( nodeVector, dq, start, index );
          }
          throw SWIGEXCEPTION( "bad_query" );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
    else
    {
       if( mHash.Element.size() != 0 )
       {
          if( mHash.Element.find( hashIndex ) != mHash.Element.end() )
          {
             const vector< DOMNode* > nodeVector( getChildByTag( node,  
                                                     mAllElements[ hashIndex ]->first ) );

             DOMNode* const elem( getContained( nodeVector, q, start, index ) ); 

             ElementAttribute e( mAllElements[ hashIndex ]->second );
             return e( elem, dq, start, index + 1 );
          }
          throw SWIGEXCEPTION( "bad_query" );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
   
   throw SWIGEXCEPTION( "ElementAttribute_unknown_error" );
   return "";
}


//------------------------------------------------------------------------------
//
//: Find method for ElementAttribute.
//
// This method extracts specified attribute of an element from the list of 
// LIGO_LW elements.
//
//!param: nodeList - List of LIGO_LW elements to be searched for specified     +
//!param:    attribute.
//!param: dq - Tokenized command.
//!param: start - Vector to record element position in the document tree.
//!param: index - Current index into position vector.
//
//!return: Attribute value or "NULL" if attribute is not found.
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: ElementAttribute_unknown_error - Unknown error has occured.
//
const std::string ElementAttribute::find( 
   const std::vector< DOMNode* >& nodeList,
   std::deque<Query>& dq,
   std::vector< size_t >& start, const size_t index )
{
   if ( nodeList.empty() )
   {
      throw SWIGEXCEPTION( "not_valid_element" );
   }
   if ( index == start.size() )
   {
      start.push_back( 0 );
   }
   
   string ret;
   INT_4U size( start.size() );

   // Flag to indicate that return value is "NULL"
   bool nullRet( false );
   
   // Flag to indicate that return value is "not_valid_element" 
   bool notValidRet( false );

   
   for( vector< DOMNode* >::const_iterator iter = nodeList.begin(),
        vect_end = nodeList.end(); iter != vect_end; ++iter )
   {
      start[ index ] = iter - nodeList.begin();
      unwind( start, size );
   
      try
      {
         ret = operator()( *iter, dq, start, index );
      }
      catch ( const SwigException& e )
      {
	 if( e.getResult() == "not_valid_element" )
	 {
	    notValidRet = true;
	 }
         else
	 {
	    throw;
	 }
      }

      if( ( ret.empty() == false ) && ( ret != "NULL" ) )
      {
         return ret;
      }
      else
      if( ( nullRet == false ) && ( ret == "NULL" ) )
      {
 	 // Element exists, but it doesn't contain required attribute. 
	 nullRet = true;
      }
   }

   unwind( start, size );
   if ( nullRet == true )
   {   
      // If at least one element was found that doesn't contain required 
      // attribute, can return "NULL" to the user.
      return "NULL";
   }
   else
   if ( ( nullRet == false ) && ( notValidRet == true ) )
   {
      // If all of the elements didn't contain specified element
      throw SWIGEXCEPTION ( "not_valid_element" );
   }
   
   throw SWIGEXCEPTION( "ElementAttribute_unknown_error" );
   return "";
}


