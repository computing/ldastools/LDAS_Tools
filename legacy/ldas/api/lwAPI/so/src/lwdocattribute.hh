#ifndef LwDocAttributeHH
#define LwDocAttributeHH


// System Includes
#include <deque>
#include <string>
#include <vector>
#include <memory>   

#include "general/Memory.hh"

// Forward declarations
namespace ILwd
{
   class LdasElement;
}   
   
class Query;
class LWDocument;


void tokenize( const std::string& s, std::deque< Query >& q );

const std::string getAttribute( 
   const LWDocument* const doc, 
   std::deque< Query >& dq, 
   std::vector< size_t >& start, const size_t index );

std::unique_ptr< ILwd::LdasElement > getData(
   const LWDocument* const doc,
   std::deque< Query >& dq,
   std::vector< size_t >& start, const size_t index );

bool putData(
   const LWDocument* const doc,
   std::deque< Query >& dq, 
   std::vector< size_t >& start, const size_t index,
   const char* const urlAddr, const char* const urlDir );

const std::string getTOC( 
   const LWDocument* const doc,
   std::deque< Query >& dq,
   std::vector< size_t >& start, const size_t index );

   
#endif
