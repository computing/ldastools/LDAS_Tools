/************************************************************************/
/*                              OpenFTP.c                               */
/*       To turn a URL on a remote machine into a file pointer          */
/************************************************************************/
#include "config.h"

#include <setjmp.h>
#include <assert.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/in_systm.h> /* Include this file before ip.h, othewise will get error. */
#include <netinet/ip.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>

#include "ftpcommand.h"
#include "datasocket.h"
#include "urlutil.h"



/****************************************************************/
/* Open control socket for sending commands / receiving replys. */
/****************************************************************/
int getFTPbyParts( const char* machine, int port, int* error )
{
	struct hostent *he;
        struct sockaddr_in sinaddr;
	int    sock;
        int    on;


	he = gethostbyname( machine );

	if(!he)
        {
	   *error = NO_DNS_ENTRY;
	   return -1;
	}


	sock = socket(he->h_addrtype, SOCK_STREAM, 0);
        if (sock < 0)
	{
	  *error = URL_ERROR_SOCKET;
          return -1;
	}

	bzero((caddr_t)&sinaddr, sizeof(sinaddr));

	sinaddr.sin_family = he->h_addrtype;


	if (bind(sock, (struct sockaddr *)&sinaddr, sizeof(sinaddr)) < 0)
        {
	   *error = BIND_FAILED;
	   return -1;
	}

	bcopy(he->h_addr, &sinaddr.sin_addr, he->h_length);


        /* Set up control socket options. */
#if 0
	on = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(int)) < 0)
        {
	   printf("Error during setsockopt(REUSEADDR) for control socket.\n");
           return -1;
        }
#endif /* 0 */

        on = IPTOS_LOWDELAY;
	if (setsockopt(sock, IPPROTO_IP, IP_TOS, (char*)&on, sizeof(int)) < 0)
        {
           *error = ERROR_SETSOCKOPT_IP_TOS;
           return -1;
        }


	sinaddr.sin_port = htons(port);

	if (connect(sock, (struct sockaddr *)&sinaddr, sizeof(sinaddr)) < 0)
        {
	   *error = CONNECT_FAILED;
	   return -1;
	}

	return sock;
}



int openFTPFile( const char* machine, int port, int* error )     
{
   int fd = -1;
   int reply;

   fd = getFTPbyParts( machine, port, error);

   reply = get_reply_from_fd(fd);

   if(reply != 220)
   {
      *error = FTP_ERROR_OPEN_FTP_FILE;
      return -1;
   }
  
   return fd;

}


/**************************************************************/
/* Caller has to deallocate the memory for returned string!!! */
/**************************************************************/
char* openFTP ( const char* url, const char* machine, 
   int port, const char* file, int* error )
{
   int   fd       = -1;
   int   data_fd  = -1 ;
   int   reply    = 0;

   /************************************/
   /* Only anonymous FTP is supported. */
   /************************************/
   char user[]       = "anonymous";
   char password[]   = "postmaster@ldas.ligo-wa.caltech.edu";
   char type[]       = "I";    /* File transfer type: binary */
   char mode[]       = "S";    /* Transfer mode: stream */
   
   int   dirLength;
   char *slash_char, *next_slash_char;
   const char *dir;
   char *new_dir;
   char *ftp_file = NULL;
   FILE* ctrl_in = 0;
   FILE* ctrl_out = 0;
   char* res = 0;
   FILE *data_in = 0;
    
   if((fd = openFTPFile( machine, port, error )) >= 0 )     
   {
      ctrl_in  = fdopen(fd, "r");
      ctrl_out = fdopen(fd, "w");

      /*************************/
      /* Log in anonymous user.*/
      /*************************/
      if (user_login(user, password, error, ctrl_in, ctrl_out))
      {
	 *error = FTP_ERROR_LOGIN_FAILED;
         #ifdef SUN_SHUTDOWN
            shutdown(fd, SHUT_RD);
         #else
            #ifdef LINUX_SHUTDOWN
               shutdown(fd, 0);
            #endif
         #endif
         fclose(ctrl_in);
         fclose(ctrl_out);         
         return NULL;
      }
      else
      {
         data_fd = get_data_socket(error, ctrl_in, ctrl_out);

         if (data_fd >= 0)
         {
            data_in  = fdopen(data_fd, "r");

            /**************************************************/
            /* Set up binary file transfer and create a local */
            /* copy of the file.                              */
            /**************************************************/
            reply = command("TYPE", type, ctrl_in, ctrl_out);
                  
            /******************/
            /* Command is OK. */
            /******************/
            if (reply==200)
            {
                /******************************************/
	        /* Open data connection and get the file. */
                /******************************************/ 
                /* printf("TYPE: %d\n", reply); */

                reply = command("MODE", mode, ctrl_in, ctrl_out);                       
                /**********************************/
                /* Retreive file from the server. */
                /**********************************/
 
                /***************************************************/                     
                /* Break down the file path into directories and   */
                /* do 'cd' on each of them to get to the directory */
                /* where file is stored, then retreive the file.   */
                /***************************************************/                     
                dir = file;
                slash_char      = NULL;
                next_slash_char = NULL;
                       
                while ( (slash_char = (strchr(dir, '/'))) != NULL )
                {
                   /****************************************************/
                   /* Check if next string in the path is a directory. */
                   /****************************************************/
                   if ( (next_slash_char = strchr(slash_char+1, (int)('/') ) ) != NULL )
		   {
		       /* This is a directory. */
                       dirLength = next_slash_char - slash_char - 1;
                       
                       new_dir = (char *)malloc(dirLength+1);
                       
                       strncpy(new_dir, slash_char+1, dirLength);
                       new_dir[dirLength] = '\0';

                       reply = command("CWD", new_dir, ctrl_in, ctrl_out);
                       if (reply != 250)
		       {
                          *error = FTP_ERROR_INVALID_DIRECTORY; 
                          free(new_dir);
                          #ifdef SUN_SHUTDOWN
                             shutdown(data_fd, SHUT_RD);
                             shutdown(fd, SHUT_RD);
                          #else
                             #ifdef LINUX_SHUTDOWN
                                shutdown(data_fd, 0);
                                shutdown(fd, 0);
                             #endif
                          #endif
                          fclose(data_in);       
                          fclose(ctrl_in);
                          fclose(ctrl_out);         
                          return NULL;
                          break;
		       }
                    
                       /* printf("CWD-%s: %d\n", new_dir, reply); */
                       free(new_dir);

                       /* Check next string in the path. */                
                       dir = next_slash_char;
		    }
                    else
                    {
                      if ( (*(slash_char+1) == '\0') || ( isspace( *(slash_char+1) == '\n') ) )
   	              {
                         *error = URL_ERROR_FILE_MISSING;
                          #ifdef SUN_SHUTDOWN
                             shutdown(data_fd, SHUT_RD);
                             shutdown(fd, SHUT_RD);
                          #else
                             #ifdef LINUX_SHUTDOWN
                                shutdown(data_fd, 0);
                                shutdown(fd, 0);
                             #endif
                          #endif
                          fclose(data_in);       
                          fclose(ctrl_in);
                          fclose(ctrl_out);         
                          return NULL;
                          break;
	              }

                      /* This is a file. */
                      ftp_file = slash_char+1;

                      reply = command("RETR", ftp_file, ctrl_in, ctrl_out); 
                      
                      if (reply != 150)                   
                      {
                          #ifdef SUN_SHUTDOWN
                             shutdown(data_fd, SHUT_RD);
                             shutdown(fd, SHUT_RD);
                          #else
                             #ifdef LINUX_SHUTDOWN
                                shutdown(data_fd, 0);
                                shutdown(fd, 0);
                             #endif
                          #endif
                          fclose(data_in);       
                          fclose(ctrl_in);
                          fclose(ctrl_out);         
                          *error = FTP_ERROR_GET_FAILED;
                          if (reply ==  550)
                          {
                            *error = FTP_ERROR_OPEN_FTP_FILE;
                          }
                          return NULL;
                          break;
		      }

                      /* printf("RETR-%s: %d\n", ftp_file, reply); */
               
                      if ( (res = recv_file( data_in, error)) == 0 )
		      {
                         *error = URL_ERROR_COPY_FILE;
                          #ifdef SUN_SHUTDOWN
                             shutdown(data_fd, SHUT_RD);
                             shutdown(fd, SHUT_RD);
                          #else
                             #ifdef LINUX_SHUTDOWN
                                shutdown(data_fd, 0);
                                shutdown(fd, 0);
                             #endif
                          #endif
                          fclose(data_in);       
                          fclose(ctrl_in);
                          fclose(ctrl_out);         
                          return NULL;
                          break;
		      }
                      break;
         	    }
		}
	    }		 
            else
	    {
		
               *error = FTP_ERROR_TYPE_FAILED;
               #ifdef SUN_SHUTDOWN
                  shutdown(data_fd, SHUT_RD);
                  shutdown(fd, SHUT_RD);
               #else
                  #ifdef LINUX_SHUTDOWN
                     shutdown(data_fd, 0);
                     shutdown(fd, 0);
                  #endif
               #endif
               fclose(data_in);       
               fclose(ctrl_in);
               fclose(ctrl_out);         
               return NULL;
	    }
	 } else {
	   /* printf ("error=%d\n", *error); */
            fclose(data_in);       
            fclose(ctrl_in);
            fclose(ctrl_out);         
            return NULL;
	 }
      }

      reply = command("QUIT", NULL, ctrl_in, ctrl_out);
      /* printf("QUIT: %d\n", reply); */

      #ifdef SUN_SHUTDOWN
         shutdown(data_fd, SHUT_RD);
         shutdown(fd, SHUT_RD);
      #else
         #ifdef LINUX_SHUTDOWN
            shutdown(data_fd, 0);
            shutdown(fd, 0);
         #endif
      #endif
      fclose(data_in);       
      fclose(ctrl_in);
      fclose(ctrl_out);         

      return res;
   }

   return NULL;
}
