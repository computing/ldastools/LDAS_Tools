#include "config.h"

// System Header Files
#include <string>
#include <sstream>   

// XML Header Files
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/util/Janitor.hpp>

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>
   
// Local Header Files
#include "errorhandler.hh"
#include "lwutil.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;
   
   
//------------------------------------------------------------------------------
//
//: Constructor.
//
ParserErrorReporter::ParserErrorReporter() : HandlerBase()
{}
  
   
//------------------------------------------------------------------------------
//
//: Destructor.
//
ParserErrorReporter::~ParserErrorReporter()
{}
   
   
//------------------------------------------------------------------------------
//
//: XML warning message handler
//
// This method handles warning messages from IBM XML parser. For now
// it does nothing.
//
//!param: const SAXParseException& toCatch - Error information                 +
//!param:   encapsulated in a SAX parse exception.
//
//!return: Nothing.
//
void ParserErrorReporter::warning( const SAXParseException& toCatch )
{
    //
    // Ignore all warnings.
    //
}


//------------------------------------------------------------------------------
//
//: XML error message handler
//
// This method handles error messages from IBM XML parser.
//
//!param: const SAXParseException& toCatch - Error information                 +
//!param:   encapsulated in a SAX parse exception.
//
//!return: Nothing.
//
//!exc: SwigException - SwigException containing the error information.
//
void ParserErrorReporter::error( const SAXParseException& toCatch )
{
    ostringstream s;
    s << "Error parsing LIGO_LW document on line #" 
      << toCatch.getLineNumber() 
      << " column #" << toCatch.getColumnNumber()
      << " xercesc reported message: " << LIGO_LW::CHAR_STRING( toCatch.getMessage() );
   
    // XML parser generated error messages are of misleading content
    // ( such as system error etc. ), don't append that message.   
    // char* message = XMLString::transcode( toCatch.getMessage() ); 
    // ArrayJanitor<char> janMess( ( char* )message );   
    // ss += message;
    throw SWIGEXCEPTION( s.str() );
}


//------------------------------------------------------------------------------
//
//: XML fatal error handler
//
// This method handles fatal errors from IBM XML parser. 
//
//!param: const SAXParseException& toCatch - Error information                 +
//!param:   encapsulated in a SAX parse exception.
//
//!return: Nothing.
//
//!exc: SwigException - SwigException containing the error information.
//
void ParserErrorReporter::fatalError(const SAXParseException& toCatch)
{
    ostringstream s;
    s << "Error parsing LIGO_LW document on line #" 
      << toCatch.getLineNumber() 
      << " column #" << toCatch.getColumnNumber()
      << " xercesc reported message: " << LIGO_LW::CHAR_STRING( toCatch.getMessage() );   

    // XML parser generated error messages are of misleading content
    // ( such as system error etc. ), don't append that message.   
    // char* message = XMLString::transcode( toCatch.getMessage() ); 
    // ArrayJanitor<char> janMess( ( char* )message );   
    //ss += message;
    throw SWIGEXCEPTION( s.str() );
}


//------------------------------------------------------------------------------
//
//: Reset errors
//
// This method resets errors from IBM XML parser. For now
// it does nothing.
//
//!return: Nothing.
//
void ParserErrorReporter::resetErrors()
{
    // No-op in this case
}
