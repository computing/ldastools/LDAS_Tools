// -*-Mode: C++;-*-
// @configure_input@

//!ignore_begin:

%module LigolwAPI

%feature("compactdefaultargs");

%{
#include <sstream>
#include <typeinfo>
   
#include <genericAPI/swigexception.hh>
#include <general/ldasexception.hh>    
#include <general/util.hh>   
   
#include <xercesc/util/Janitor.hpp>
#include <xercesc/util/XMLString.hpp>   
#include <xercesc/dom/DOMException.hpp>      
#include <xercesc/sax/SAXException.hpp>         

  using std::bad_cast;

%}

%init %{
    Tcl_PkgProvide( interp, "@PACKAGE@", "@LDAS_PACKAGE_VERSION@");
%}

%include "genericAPI/cplusplus.swig"

%exception
{
    Tcl_Obj* tcl_result( Tcl_GetObjResult( interp ) );   
    try
    {
        $function;
    }
    catch( const LdasException& exc )
    {
       SwigException e( exc ); 
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;
    }   
    catch( SwigException& e )
    {
        e.setTcl( interp, tcl_result );
        return TCL_ERROR;
    }    
    catch( const XERCES_CPP_NAMESPACE::XMLException& exc )
    {
       XERCES_CPP_NAMESPACE::ArrayJanitor< char > msg(
          XERCES_CPP_NAMESPACE::XMLString::transcode( exc.getMessage() ) ); 
    
       string error_msg( "XMLExcepton: " );
       error_msg += msg.get();
   
       SwigException e( error_msg );
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;   
    }      
    catch( const XERCES_CPP_NAMESPACE::SAXException& exc )
    {
       XERCES_CPP_NAMESPACE::ArrayJanitor< char > msg(
          XERCES_CPP_NAMESPACE::XMLString::transcode( exc.getMessage() ) ); 
    
       string error_msg( "SAXExcepton: " );
       error_msg += msg.get();
   
       SwigException e( error_msg );
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;   
    }         
    catch( const XERCES_CPP_NAMESPACE::DOMException& exc )
    {
       ostringstream s;
       s << "DOMException: code=" << exc.code; 
       
       if( XERCES_CPP_NAMESPACE::XMLString::stringLen( exc.msg ) )
       {
          XERCES_CPP_NAMESPACE::ArrayJanitor< char > exc_msg( 
             XERCES_CPP_NAMESPACE::XMLString::transcode( exc.msg ) );
       
          s << " message=\"" << exc_msg.get() << '\"'; 
       }

       SwigException e( s.str() );
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;   
    }
    catch( const bad_cast& exc )
    {
       SwigException e( "Malformed data( std::bad_cast )." );
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;   
    }
    catch( const bad_alloc& exc )
    {
       SwigException e( "Memory allocation failed." );
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;   
    } 
    catch( const exception& e )
    {
      string msg("Standard exception: ");
      msg += e.what();
      SwigException swige( msg );
      swige.setTcl( interp, tcl_result );
      return TCL_ERROR;
    }  
    catch(...)
    {
       SwigException e( "Unknown exception." );
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;   
    }   
}

%include "lwcmd.swig"

//!ignore_end:
