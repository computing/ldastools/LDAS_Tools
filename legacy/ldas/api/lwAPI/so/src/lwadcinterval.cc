#include "config.h"

// System Header Files
#include <new>
#include <sstream>   

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
using ILwd::LdasContainer;
using ILwd::LdasElement;   

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// Local Header Files
#include "lwadcdata.hh"
#include "lwcomment.hh"
#include "lwtime.hh"
#include "lwelement.hh"
#include "query.hh"
#include "lwutil.hh"
#include "ilwdutil.hh"   
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;  
   

static MetadataHash initAdcIntervalMetadata()
{
    MetadataHash h;
   
    h.insert( metadataType( "StartTime", "start_time" ) );
    h.insert( metadataType( "DeltaT", "delta_t" ) );   

    return h;   
}

   
static LwDataHash initAdcIntervalHash()
{
   LwDataHash h;
   
   h[ LW_ADCDATA_ID ] = lwAdcData2ILwd;
   h[ LW_COMMENT_ID ] = comment2ILwd;
   h[ LW_TIME_ID    ] = time2ILwd;
   
   return h;
}

   
static LwDataHash adcIntervalHash( initAdcIntervalHash() );
static MetadataHash adcIntervalMetadata( initAdcIntervalMetadata() );   
static const char* xml_name = ":AdcInterval:XML";
   

//-----------------------------------------------------------------------
//: Convert LIGO_LW AdcInterval into ILWD format container
//
//!param: const DOMNode* const node - A reference to the LIGO_LW element to convert.
//!param: std::deque< Query > dq - Tokenized command.
//
//!return: LdasElement* - An ILWD format container element.
//
//!exc: SwigException
//!exc: bad_alloc - Memory allocation failed.
//   
unique_ptr< LdasElement > lwAdcInterval2ILwd( 
   const DOMNode* const node, std::deque< Query > dq )
{
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );
   
   setILwdMetadata( node, adcIntervalMetadata, c.get() );

   
   DOMNode* child = node->getFirstChild();
   LwType type;
   
   while( child != 0 ) 
   {
      if( child->getNodeType() == DOMNode::ELEMENT_NODE )
      {   
         type = getLwType( child );   
         if( adcIntervalHash.find( type ) != adcIntervalHash.end() )
         {
            c->push_back( adcIntervalHash[ type ]( child, dq ).release(),
                          ILwd::LdasContainer::NO_ALLOCATE,
                          ILwd::LdasContainer::OWN );
         }
         else
         {
            ostringstream s;
            s << "adcinterval_unsupported_element_type: " 
              << type;
   
            throw SWIGEXCEPTION( s.str() );   
         }   
      }
      
      child = child->getNextSibling();
   }

   
   return unique_ptr< LdasElement >( c.release() );
}
