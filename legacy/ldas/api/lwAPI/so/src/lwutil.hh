#ifndef LwUtilHH
#define LwUtilHH

// System Header Files
#include <string>
#include <cstring>   
#include <sstream>   
#include <algorithm>   
#include <typeinfo>   

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/Janitor.hpp>   
   
// Generic Header Files   
#include <genericAPI/swigexception.hh>

// General Header Files   
#include <general/util.hh>   
#include <general/types.hh>   

// ---------------------------------------------------------------------------
//   
//: String conversion class   
//   
// This is a simple class that lets us do easy trancoding of CHAR* data 
// to XMLCh data and vice versa.
// The object takes ownership of the array of characters that was created
// with XMLString::transcode() methods (caller is responsible for destruction
// of returned array).   
//   
// ---------------------------------------------------------------------------
namespace LIGO_LW
{
   class XString
   {
   public :
   
       //  Constructors and Destructor
       XString( const CHAR* const toTranscode )
          : fUnicodeForm( 0 ), fCharForm( 0 )
                         
       {
          // Call the private transcoding method
          fUnicodeForm = XERCES_CPP_NAMESPACE::XMLString::transcode( toTranscode );
       }
   
       XString( const XMLCh* const toTranscode )
          : fUnicodeForm( 0 ), fCharForm( 0 )
       {
          // Call the private transcoding method
          fCharForm = XERCES_CPP_NAMESPACE::XMLString::transcode( toTranscode );
       }   
   
       ~XString()
       {
          if( fUnicodeForm )
          {
             XERCES_CPP_NAMESPACE::XMLString::release( &fUnicodeForm );
             fUnicodeForm = 0;
          }
   
          if( fCharForm )
          {
             XERCES_CPP_NAMESPACE::XMLString::release( &fCharForm );
             fCharForm = 0;   
          }
       }
   
   
       // Getter methods
       const XMLCh* unicodeForm() const
       {
          return fUnicodeForm;
       }
   
       const char* charForm() const
       {
          return fCharForm;
       }   
   
   private :

       //: This is the Unicode XMLCh format of the string.
       XMLCh* fUnicodeForm;
   
       //: This is the CHAR format of the string
       char* fCharForm;
   };
   
#define XML_STRING(str) XString(str).unicodeForm()   
#define CHAR_STRING(str) XString(str).charForm()      
}
   

const std::string getElementAttribute( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node, 
   const CHAR* const name );

// Function to extract element's attribute value by name.
const std::string nodeAttribute( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node,
   const CHAR* const attName );

// Function to extract node's value
const std::string nodeValue( const XERCES_CPP_NAMESPACE::DOMNode* const node );

// Function to extract node's name
const std::string nodeName( const XERCES_CPP_NAMESPACE::DOMNode* const node );

// Helper utility to check case-insensitive inclusion of "token" within "test-string"
bool contain( const std::string& test_string, const std::string& token );   

   
template< class T > T* nodeAttribute( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node,
   const CHAR* name, const size_t dims )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "nodeAttribute: NULL DOMNode is passed" );   
   }
   
   if( node->getNodeType() != XERCES_CPP_NAMESPACE::DOMNode::ELEMENT_NODE )
   {
      throw SWIGEXCEPTION( "Requesting an attribute for non DOMElement object" );
   }
  
   const XERCES_CPP_NAMESPACE::DOMElement* const elem( 
      dynamic_cast< const XERCES_CPP_NAMESPACE::DOMElement* const >( node ) );
   if( elem == 0 )
   {
      throw std::bad_cast();
   }
  
    
   // Transcode name from char* to the xmlch*
   LIGO_LW::XString xml_name( name );

   if( elem->hasAttribute( xml_name.unicodeForm() ) == false )
   {
      // Element doesn't have named attribute
      return 0;
   }
  
   // Extract attribute by name
   const XMLCh* att_value( elem->getAttribute( xml_name.unicodeForm() ) );
   
   if( att_value == 0 || *att_value == 0 ) 
   {
      return 0;
   }

   
   LIGO_LW::XString att( att_value );
   
   // Parse values from the string
   XERCES_CPP_NAMESPACE::ArrayJanitor< T > result( new T[ dims ] );
   std::istringstream ss( att.charForm() );  
  
   for ( size_t i = 0; i < dims; ++i )
   {
      ss >> result[ i ];
      if( !ss )
      {
         // Bad data
         std::string msg( "Bad datatype for node attribute: " );         
         msg += name;
         throw SWIGEXCEPTION( msg );
      }   
      if ( i != ( dims - 1 ) )
      {
         int c = ss.peek();
         if( c == ':' )
         {
            ss.get();
         }
      }
   }

   // Check for extra data
   const CHAR* pos( att.charForm() + ss.tellg() );
   const CHAR* end_pos( att.charForm() + strlen( att.charForm() ) ); 

   // Make sure there's no extra data in the input
   if( std::find_if( pos, end_pos, not_space ) != end_pos )
   {
      std::string msg( "Extra data for node attribute: " );
      msg += name;
      throw SWIGEXCEPTION( msg );
   }
   
   return result.release();
}
   
   
#endif
