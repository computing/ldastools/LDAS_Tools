#include "config.h"

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// Local Header Files
#include "lwtoc.hh"
#include "getattribute.hh"
#include "lwgetchild.hh"
#include "query.hh"
#include "vect.hh"
#include "lwutil.hh"
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;   
   

// Hash_map for getLigoLwTOC
ElementTOC::TOCHash ElementTOC::initLwTOC()
{
    TOCHash h;
    h[ "ligo_lw"     ] = "LIGO_LW";
    h[ "table"       ] = "Table";
    h[ "igwdframe"   ] = "IGWDFrame";
    h[ "adcdata"     ] = "AdcData";
    h[ "adcinterval" ] = "AdcInterval";   
    h[ "detector"    ] = "Detector";
    return h;
}


ElementTOC::TOCHash ElementTOC::mTOCHash( initLwTOC() );

   
//------------------------------------------------------------------------------
// 
//: ( Default ) Constructor.
//   
ElementTOC::ElementTOC()
{}   

   
//------------------------------------------------------------------------------------
//
//: Find method for ElementTOC functional.
// 
// This method searches list of LIGO_LW elements for specified 
// element and gets the content of that element.
//
//!param: node - List of LIGO_LW elements.
//!param: dq - Tokenized command.
//!param: start - Vector with element index information.
//!param: index - Current index into 'start' vector.
//
//!return: List of the contents of the LIGO_LW element 
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: bad_query - Query is bad.
//
const std::string ElementTOC::find( 
   const std::vector< DOMNode* >& nodeList,
   std::deque< Query>& dq, 
   std::vector< size_t >& start, const size_t index ) 
{
   if ( nodeList.empty() )
   {
      throw SWIGEXCEPTION( "not_valid_element" );
   }
   
   if ( index == start.size() )
   {
      start.push_back( 0 );
   }

   string ret;
   const INT_4U size( start.size() );
   
   // Flag to indicate that return value is "empty_element"
   bool emptyRet( false );
   
   // Flag to indicate that return value is "not_valid_element" 
   bool notValidRet( false );

   
   for( vector< DOMNode* >::const_iterator iter = nodeList.begin(),
        end_iter = nodeList.end(); iter != end_iter; ++iter )
   {
      start[ index ] = iter - nodeList.begin();
      unwind( start, size );
   
      try
      {
         ret = operator()( *iter, dq, start, index + 1 );
      }
      catch( const SwigException& e )
      {
         // Specified element doesn't exist in current element.
	 if( e.getResult() == "not_valid_element" )
	 {
	    notValidRet = true;
	 }
         else
	 {
	    throw;
	 }
      }

      if( ret.empty() == false )
      {
         return ret;
      }
      else if( ( emptyRet == false ) && ( ret.empty() == true ) )
      {
 	 // Element exists, but it doesn't contain any elements.
	 emptyRet = true;
      }
   }

   unwind( start, size );
   if ( ( emptyRet == false ) && ( notValidRet == true ) )
   {
     // If all of the elements didn't contain specified element
     throw SWIGEXCEPTION ( "not_valid_element" );
   }
   
   return "";   
}


//------------------------------------------------------------------------------------
//
//: Operator() for ElementTOC functional.
//
// This method gets the content of specified LIGO_LW element in the document.
// 
//!param: node - LIGO_LW element.
//!param: dq - Tokenized command.
//!param: start - Vector with element index information.
//!param: index - Current index into 'start' vector.
//
//!return: List of the contents of the LIGO_LW element 
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: bad_query - Query is bad.
//
const std::string ElementTOC::operator()( 
   DOMNode* const node,
   std::deque< Query > dq,
   std::vector< size_t >& start, const size_t index )
{
    if ( node == 0 )
    {
       throw SWIGEXCEPTION( "not_valid_element" );
    }
   
    if ( dq.size() == 0 )
    {
       return getContent( node, dq );
    }

   
    Query q = dq.front();
    dq.pop_front();
    const CHAR* hashIndex( q.getName().c_str() );

    // It's not a query
    if ( !q.isQuery() )
    {
       if( strcmp( hashIndex, "full" ) == 0 )            
       {
	  return getContent( node, dq );
       }
   
       if( mTOCHash.find( hashIndex ) != mTOCHash.end() )
       {
          const vector< DOMNode* > nodeVector( getChildByTag( node, 
                                                        mTOCHash[ hashIndex ] ) );

          return find( nodeVector, dq, start, index );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
    else
    {
       if( mTOCHash.find( hashIndex ) != mTOCHash.end() )
       {
          const vector< DOMNode* > nodeVector( getChildByTag( node, 
                                                        mTOCHash[ hashIndex ] ) );

          DOMNode* const elem( getContained( nodeVector, q, start, index ) ); 

          return operator()( elem, dq, start, index + 1 );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
   
    throw SWIGEXCEPTION( "getTOC_unknown_error" );
}


//------------------------------------------------------------------------------------
//
//: getContent method for ElementTOC functional.
//
// This method gets the content of specified LIGO_LW element in the document.
// 
//!param: node - LIGO_LW element.
//!param: dq - Tokenized command.
//
//!return: List of the contents of the LIGO_LW element 
//
//!exc: bad_query - Query is bad.
//
const std::string ElementTOC::getContent( 
   DOMNode* const node, std::deque< Query> dq ) 
{
   if( dq.size() != 0 )
   {
      throw SWIGEXCEPTION( "bad_query" );
   }
    
   DOMNode* child = node->getFirstChild();
   string ss;
    
   while( child != 0 )
   {
     if ( child->getNodeType() == DOMNode::ELEMENT_NODE )
     {
        ss += nodeName( child );
        ss += " {";
        ss += nodeAttribute( child, LIGO_LW::nameAttName );
        ss += "} ";
     }
    
     child = child->getNextSibling();
   }
 
   return ss;
}
