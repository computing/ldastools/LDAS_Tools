/*****************************************/
/* Functions to manage data connections. */
/*****************************************/
#include "config.h"

#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in_systm.h> /* Include this file before ip.h, othewise will get error. */
#include <netinet/ip.h>
#include <string.h>
#include <strings.h>

#include "ftpcommand.h"
#include "datasocket.h"
#include "urlutil.h"



int get_data_socket(int *error, FILE* ctrl_in, FILE* ctrl_out)
{
  struct sockaddr_in data_addr;
  int    sock = -1;
  int    on;
  int    reply = 0;
  char  *char_p;
  /* IP address and port number. */
  u_long ip1, ip2, ip3, ip4, p1, p2;
  char replyString[256];

  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
  {
     *error = URL_ERROR_SOCKET;
     return -1;
  }

  reply = command_("PASV", NULL, ctrl_in, ctrl_out, replyString);

  if (reply == 227)
  {
    char buf[128];

    /* Get IP address and port numbers. */
    char_p = strchr(replyString, '(');
    
    if (char_p == NULL)
    {
      *error = URL_ERROR_PASV_COMMAND;
      return -1;       
    }

    char_p++;
    
    if ( sscanf(char_p, "%lu,%lu,%lu,%lu,%lu,%lu",
		&ip1, &ip2, &ip3, &ip4, &p1, &p2) != 6)
    {
        /* printf("Passive mode address scan failure.\n"); */
        *error = URL_ERROR_PASV_SCAN;
        return -1;
    }
    sprintf(buf, "%lu.%lu.%lu.%lu", ip1, ip2, ip3, ip4);
    data_addr.sin_family = AF_INET;

    /* data_addr.sin_addr.s_addr = htonl( (ip1 << 24) | (ip2 << 16) | */
    /*                                    (ip3 << 8) | ip4); */
    data_addr.sin_addr.s_addr = inet_addr(buf);

    data_addr.sin_port = htons( (p1 << 8) | p2);
  }
  else {
        /* printf("Error; PASV command reply was %d\n", reply); */
        *error = URL_ERROR_PASV_COMMAND;
	return -1;
  }


  on = IPTOS_THROUGHPUT;
  if (setsockopt(sock, IPPROTO_IP, IP_TOS, (char*)&on, sizeof(int)) < 0)
  {
    /* printf("Error during setsockopt(MAX_THROUGHPUT) for data socket.\n"); */
    *error = ERROR_SETSOCKOPT_IP_TOS;
    return -1;
  }
  
  if (connect(sock, (struct sockaddr *)&data_addr, sizeof(data_addr)) < 0)
  {
    /* printf("Error during connect for data socket (errno=%d).\n", errno); */
    *error = CONNECT_FAILED;
    return -1;
  }
  

  return sock;

}



int user_login( char* user, char* password, int* error, FILE* ctrl_in, FILE* ctrl_out)
{
  int  reply = 0;

  
  /* printf("Sending user name...\n"); */

  reply = command("USER", user, ctrl_in, ctrl_out);

  if (reply == 331)                     
  {
    /*************************/
    /* Send user's password. */
    /*************************/
    /* printf("Sending user password...\n"); */

    reply = command("PASS", password, ctrl_in, ctrl_out);

    if (reply != 230) /* In a case account information (ACCT) is required or other error */
    {
      *error = FTP_USER_ERROR;
      /* printf("Only anonymous user is supported: %d.\n", reply); */
      return -1;
    }

  }
  else 
  {
    *error = FTP_PASSWORD_ERROR;
    return -1;
  }

  return 0;
}
