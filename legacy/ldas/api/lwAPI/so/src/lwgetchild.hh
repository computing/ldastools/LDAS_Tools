#ifndef LwGetChildHH
#define LwGetChildHH

// System Header Files   
#include <vector>
   
// XML Header Files
#include <xercesc/dom/DOMNode.hpp>      

// General Header Files   
#include <general/types.hh>   
   

const std::vector< XERCES_CPP_NAMESPACE::DOMNode* > getChildByTag( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node, 
   const CHAR* const name );
   
const std::vector< XERCES_CPP_NAMESPACE::DOMNode* > getChildByType(
   const XERCES_CPP_NAMESPACE::DOMNode* const node, 
   const XERCES_CPP_NAMESPACE::DOMNode::NodeType& type );


#endif
