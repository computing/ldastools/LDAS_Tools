#include "config.h"

// System Header Files
#include <new>
#include <limits>   

#include "general/Memory.hh"

// ILWD Header Files
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"
#include "ilwd/ldascontainer.hh"

using ILwd::LdasElement;   
using ILwd::LdasArray;
using ILwd::LdasString;
using ILwd::LdasContainer;

#include "general/util.hh"
   
// Local Header Files
#include "lwcolumn.hh"
#include "lwgetchild.hh"
#include "lwstream.hh"
#include "query.hh"
#include "lwutil.hh"
#include "convertstr.hh"
#include "lwconsts.hh"

using namespace std;
using namespace XERCES_CPP_NAMESPACE; 
   
static const char* xml_name = ":Column:XML";


//------------------------------------------------------------------------------
//
//: Parses Units string.
//
//!param: const string& units - Units string.
//!param: size_t& numUnits - Number of units feilds.
//   
//!return: Pointer to the parsed Units array.
//
//!exc: bad_alloc - Memory allocation failed.
//   
static string* parseUnits( const string& units, size_t& numUnits ) 
{
   string* uu( 0 );
   if( units.empty() == false )
   {
      // Get units dimention:
      string::size_type s( 0 );
      while( ( ( s = units.find_first_of( ":", s ) ) != string::npos ) )
      {
         ++numUnits; 
         ++s;
      }
      ++numUnits;            
      uu = new string[ numUnits ];

      s = 0;
      string::size_type e( 0 );
      size_t i( 0 );
      while( ( s != string::npos ) && ( i < numUnits ) )
      {
         e = units.find( ':', s );
         if( e != string::npos )
         {
            uu[ i++ ] = units.substr( s, e );

            if ( e + 1 != units.size() )
            {
               s = e + 1;
            }
         }
         else
         {
            uu[ i++ ] = units.substr( s );
            s = string::npos;
         }
      }
   }
   return uu;
}

   
//------------------------------------------------------------------------------
//
//: Gets number of elements in the string.
//
// This method identifies number of elements in the passed to it string by using
// a delimiter string as data separator. In a case if data delimiter is not found
// it detects if this string represents a binary or a string data, and detects
// its size based on this conclusion.   
//
//!param: const string& val - A reference to the data string.
//!param: const char& delimiter - A reference to the data delimiter character.
//
//!return: unsigned int - Number of data elements.
//
//!exc: malformed_stream_data - Stream data is malformed.   
//   
static unsigned int getNum( const string& val, const char& delimiter )
{
   unsigned int num( 0 );
   string::size_type pos( 0 );
   bool found_delimiter( false );

   while( ( pos = val.find_first_of( delimiter, pos ) ) != string::npos )
   {
      if( pos == 0 )
      {
         // It's not escaped data delimiter
         throw SWIGEXCEPTION( "malformed_stream_data" );
      }
      
      // There are no preceding backslashes or there is an even number 
      // of them
      if( isEscaped( val, pos - 1 ) == false )
      {
         ++num;
         if( !found_delimiter )
         {
            found_delimiter = true;
         }
      }

      ++pos;
   }

   // Data string does not contain any data delimiters
   if( !found_delimiter )
   {
      // Check if it is a binary data: all data should be octal
      // based on the format: \xxx
      if( val[ 0 ] == bslashChar )
      {
         if( val.size() == 1 )
         {
            throw SWIGEXCEPTION( "malformed_stream_data" );
         }
         // If string data begins with backslash then it's 
         // an escape for backslash or data delimiters such as
         // comma or space, otherwise it's octal data with no
         // data delimiters
         if( val[ 1 ] != bslashChar && 
             val[ 1 ] != ',' &&
             val[ 1 ] != delimiter )
         {
            pos = 0;
            while( ( pos = val.find_first_of( bslashChar, pos ) ) !=
                   string::npos )
            {
               ++num;
               ++pos;
            }     

            return num; 
         }
      }

      // This is just a string data. Consider all escapes in the string
      // since they will be deleted later.
      pos = 0;   
      const size_t size( val.size() - 2 );
      unsigned int escapes( 0 );
      while( ( pos = val.find_first_of( bslashChar, pos ) ) != string::npos )
      {
         ++escapes;
         if( pos <= size )
         {
            // Skip over the escaped character
            pos += 2;
         }
         else
         {
            pos = string::npos;
         }
      }
         
      return ( val.size() - escapes );
   }

   return ( num + 1 );
}

   
//------------------------------------------------------------------------------
//
//: Checks if string contains any valid data.
//
// This method is checking if passed to it string contains any valid data or 
// it just represents an empty data element. In a case of a null element the 
// string will be empty or if it represents a very first element of the row or
// the very last element of the stream, it may contain some spaces and newlines.  
//
//!param: const string& s - A reference to the string representing the data    +
//!param:    element.
//
//!return: bonst bool - True if string represents a NULL element, false        +
//!return:    otherwise. 
//   
namespace {
  bool isNull( const string& s )
  {
    if( s.empty() )
    {
      return true;
    }

    string::const_iterator end( s.end() );
    if( find_if( s.begin(), end, not_space ) != end )
    {
      return false;
    }
   
    return true;
  }
}

   
//------------------------------------------------------------------------------
//
//: Extracts part of the string enclosed into double quotes.
//
// This method extracts part of the string that is enclosed into the double
// quotes. This is used in extracting separate elements from the LIGO_LW Table
// Stream. It will be used only in the lstring, char, ilwd:char and ilwd:char_u
// types of column data.
//
//!param: string& elem - A reference to the data string representing one       +
//!param:   of the Stream.
//
//!return: Nothing.
//
//!exc: malformed_stream_data - Stream data is malformed: missing enclosing    +
//!exc:    double quotes for printable data.   
//   
static void extractQuotedData( string& elem )
{
   string::size_type prev_pos( 0 );

   // Make sure that first character of the element is quote
   string::iterator elem_iter( elem.begin() );   
   if( *elem_iter != dquoteChar )
   {
      if( ( prev_pos = elem.find_first_of( dquoteChar, prev_pos ) ) ==
          string::npos )
      {
         // Could not find the opening quote
         throw SWIGEXCEPTION( "malformed_stream_data: missing "
                              "opening double quote" );
      }

      string::iterator elem_end( elem.begin() + prev_pos );      
      if( find_if( elem.begin(), elem_end, not_space ) != elem_end )
      {
         throw SWIGEXCEPTION( "extra_data: before opening "
                              "double quote" );
      } 
      
   }
   // Erase everything before opening double quote including the quote
   elem.erase( 0, ++prev_pos );   
   
   if( elem.size() == 0 )
   {
      throw SWIGEXCEPTION( "malformed_stream_data" );
   }
   
   // Extract data only up to the closing quote( there may be some 
   // newline and spaces at the end of the stream )
   elem_iter = elem.begin() + elem.size() - 1;
   if( *elem_iter != dquoteChar )
   {
      // Find last double quote
      if( ( prev_pos = elem.find_last_of( dquoteChar ) ) == string::npos )
      {
         // Could not find the closing quote
         throw SWIGEXCEPTION( "malformed_stream_data: missing "
                              "closing double quote" );
      }
     
      elem_iter = elem.begin() + prev_pos;
   
      string::iterator end_iter( elem.end() );
      if( find_if( elem_iter + 1, end_iter, not_space ) != end_iter )
      {
         throw SWIGEXCEPTION( "extra_data: after closing "
                              "double quote" );
      }
   }    
   
   // Erase everything after closing double quote including the quote
   elem.erase( elem_iter, elem.end() );   

   
   return;
}


//------------------------------------------------------------------------------
//
//: Erases escapes for the specified character in lstring data. Backslash character is used as
//: an escape character in the LIGO_LW format.
//
// Only comma needs to be escaped within lstring type of data, since comma is
// used as a global data delimiter within the stream.   
//   
//!param: string& sourse - A reference to the data string.
//!param: const char& escChar - A reference to the character which escapes     +
//!param:    should be erased.
//
//!return: Nothing.
//   
static void eraseLstringEscape( string& source, const char& escChar )
{
   string::size_type qpos( 0 );
   while( ( qpos = source.find_first_of( escChar, qpos ) ) != string::npos )
   {
      if( ( qpos != 0 ) && ( source[ qpos - 1 ] == bslashChar ) )
      {
         // If character is preceeded by backslash erase that backslash
         source.erase( qpos - 1, 1 );
      }
      else
      {
         ++qpos;
      }
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Extracts string column from data stream and converts it to                 
//: ILWD lstring element.
//
//!param: const DOMNode* const node - LIGO_LW Column node.
//!param: const string& data - A reference to the data string.
//!param: const char& delimiter - A reference to the data delimiter character.   
//!param: const size_t colIndex - Column index into the table it belongs to.
//!param: const size_t numColumns - Number of columns in the table( decremented by 1 )
//
//!return: LdasString - ILWD lstring element.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: malformed_stream_data - Invalid format is specified for the lstring     +
//!exc:    type of column in the table stream.   
//
static unique_ptr< LdasElement > colString2ILwd( const DOMNode* const node, const string& data, 
   const char& delimiter, const size_t colIndex, 
   const size_t numCols, const unsigned int numElem )
{
   vector< string > value;
   vector< bool > nullmask;

   
   if( !data.empty() )
   {
      string::size_type pos( 0 ), prev_pos( 0 );   
      size_t i( 0 );
      string elem;
      string::const_iterator end( data.begin() ), 
                             data_begin( data.begin() );   
   
      while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
             ( prev_pos != string::npos ) )
      {
         // Do not count escaped stream delimiters
         if( pos == 0 || pos == string::npos ||
             ( pos != 0 && data[ pos - 1 ] != bslashChar ) )
         {
            if ( i == colIndex )
            {
               if( pos == string::npos )
               {
                  end = data.end();
               }
               else
               {
                  end = data_begin + pos;
               }
               elem.assign( data_begin + prev_pos, end );
   
               // Check if data is missing for the current entry of i
               if( isNull( elem ) ) 
               {
                   value.push_back( "" );
                   nullmask.push_back( true );                
               }
               else
               {
                  // Make sure that data is enclosed in double quotes
                  extractQuotedData( elem );    
                  eraseLstringEscape( elem, delimiter );
   
                  value.push_back( elem );
                  nullmask.push_back( false );
               }
               if( value.size() > numElem )
               {
                  throw SWIGEXCEPTION( "malformed_stream_data" );
               }   
            }
   
            if ( i == numCols )
            {
               i = 0;
            } 
            else
            {
               ++i;
            }
   
            if ( pos != string::npos )
            {
               prev_pos = ++pos;
            }
            else
            {
               prev_pos = pos;
            }
         }
         else
         {
            ++pos;
         }
      }
   }
   
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   size_t numUnits( 0 ); 
   const string unitV( nodeAttribute( node, LIGO_LW::unitAttName ) );
   ArrayJanitor< string > units( parseUnits( unitV, numUnits ) );
   string comment = "";
   
   unique_ptr< LdasElement > s( new LdasString( value, name, comment, numUnits, units.get() ) );


   for( size_t i = 0; i < numElem; ++i )
   {
      s->setNull( i, nullmask[ i ] );
   }  

   
   return s;   
}


//------------------------------------------------------------------------------
//
//: Extracts column of specified type from data stream and converts it to the                
//: ILWD array element.   
//
//!param: DOMNode& node - LIGO_LW Column node.
//!param: const string& data - A reference to the data string.
//!param: const char& delimiter - A reference to the data delimiter character.   
//!param: const size_t colIndex - Column index into the table it belongs to.
//!param: const size_t numColumns - Number of columns in the table( decremented by 1 )
//!param: const unsigned int numElem - Number of elements in the column.   
//   
//!return: LdasArray* - A pointer to the created ILWD Array element.
//   
//!exc: malformed_stream_data -  Invalid format is specified for this type of  +
//!exc:    column in the table stream.   
//!exc: bad_alloc - Memory allocation failed.   
//!exc: bad_data_type - Bad data is specified for the type.
//!exc: extra_data - Extra data is specified for the type.   
//   
template< class T >
unique_ptr< LdasElement > colData2ILwd( const DOMNode* const node,
   const string& data, const CHAR& delimiter, 
   const size_t colIndex, const size_t numCols, const INT_4U numElem )
{
   ArrayJanitor< T > buf( 0 );
   vector< bool > nullmask;

   
   if( !data.empty() || numElem )
   {
      buf.reset( new T[ numElem ] );   
      string::size_type pos( 0 ), prev_pos( 0 );
      INT_4U bufIndex( 0 );
      size_t i( 0 );

      string elem;   
      string::const_iterator end( data.begin() ), 
                             data_begin( data.begin() );      
   
      while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
             ( prev_pos != string::npos ) )
      {
         // Do not count escaped stream delimiters   
        if( pos == 0 || pos == string::npos ||
             ( pos != 0 && data[ pos - 1 ] != bslashChar ) ) 
         {
            if ( i == colIndex )
            {
               if( bufIndex >= numElem )
               {
                  throw SWIGEXCEPTION( "malformed_stream_data" );
               }
   
               if( pos == string::npos )
               {
                  end = data.end();
               }
               else
               {
                  end = data_begin + pos;
               }
               elem.assign( data_begin + prev_pos, end );

               // Check if data is missing for the current entry of i   
               if( isNull( elem ) )
               {
                  buf[ bufIndex++ ] = 0;
                  nullmask.push_back( true );
               }
               else
               {
                  ArrayJanitor< T > temp( string2Data< T >( elem, 1, delimiter ) );                  
                  buf[ bufIndex++ ] = temp[ 0 ];
                  nullmask.push_back( false );
               }
            }   
            if ( i == numCols )
            {
               i = 0;
            } 
            else
            {
               ++i;
            }
            if ( pos != string::npos )
            {
               prev_pos = ++pos;
            }
            else
            {
               prev_pos = pos;
            }
         }
         else
         {
            ++pos;
         }
      }
   }
   
   
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   
   const string units( nodeAttribute( node, LIGO_LW::unitAttName ) );

   
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS      
   
   const string unity( nodeAttribute( node, LIGO_LW::dataUnitAttName ) );      

   // All columns are converted to 1-dimensional ILWD format arrays
   const size_t nDim( 1 );
   ArrayJanitor< REAL_8 > dx( nodeAttribute< REAL_8 >( node, LIGO_LW::scaleAttName, nDim ) );
   
   if( dx.get() == 0 )
   {
      dx.reset( new REAL_8[ nDim ] );
      dx[ 0 ] = defaultScaleX;
   }

   
   ArrayJanitor< REAL_8 > startx( nodeAttribute< REAL_8 >( node, LIGO_LW::startAttName, nDim ) );
   
   if( startx.get() == 0 )
   {
      startx.reset( new REAL_8[ nDim ] );
      startx[ 0 ] = defaultStartX;
   }

   
   unique_ptr< LdasElement > a( new LdasArray< T >( buf.get(), numElem, name, units, 
                                                     dx[ 0 ], startx[ 0 ], unity ) );

#else

   unique_ptr< LdasElement > a( new LdasArray< T >( buf.get(), numElem, name, units ) );

#endif   

   
   for( size_t i = 0; i < numElem; ++i )
   {
      a->setNull( i, nullmask[ i ] );
   }     

   
   return a;
}


//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.   
//      
template<>
unique_ptr< LdasElement > colData2ILwd< CHAR >( const DOMNode* const node,
   const string& data, const CHAR& delimiter, 
   const size_t colIndex, const size_t numCols, const INT_4U numElem )
{
   ArrayJanitor< CHAR > buf( 0 );
   vector< bool > nullmask;


   if( !data.empty() || numElem )
   {
      buf.reset( new CHAR[ numElem ] );   
   
      INT_4U bufIndex( 0 );
      size_t i( 0 );

      string elem;
      string::size_type pos( 0 ), prev_pos( 0 ),
                        s_pos( 0 ), s_end_pos( 0 );
      string::const_iterator end( data.begin() ), 
                             data_begin( data.begin() );   
      INT_8S temp;   
   
   
      while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
             ( prev_pos != string::npos ) )
      {
         // Do not count escaped delimiter character   
         if( pos == 0 || pos == string::npos ||
             ( pos != 0 && data[ pos - 1 ] != bslashChar ) )
         {
            if ( i == colIndex )
            {
               if( bufIndex >= numElem )
               {
                  throw SWIGEXCEPTION( "malformed_stream_data" );
               }

               if( pos == string::npos )
               {
                  end = data.end();
               }
               else
               {
                  end = data_begin + pos;
               }
               elem.assign( data_begin + prev_pos, end );
   
               // Check if data is missing for the current entry of i
               if( isNull( elem ) ) 
               {
                   // Insert default value into array
                   buf[ bufIndex++ ] = ' ';
                   nullmask.push_back( true );                
               }
               else
               {
                  // All printable characters will be enclosed into double quotes
                  if( elem.find_first_of( dquoteChar, 0 ) != string::npos )
                  {
                     // Make sure that character is enclosed into double quotes
                     // if it is a printable character
                     extractQuotedData( elem );                      

                     ArrayJanitor< CHAR > d( string2Data< CHAR >( elem, 1, delimiter ) );
                     buf[ bufIndex++ ] = d[ 0 ];   
                  }
                  else
                  {
                     // It's a numeric value
                     istringstream e( elem.c_str() );
                     // Get backslash character for octal representation
                     if( e.peek() != bslashChar )
                     {
                        throw SWIGEXCEPTION( "malformed_stream_data: char");
                     }
   
                     e.get();
                     e >> std::oct >> temp;
                     if( !e )
                     {
                        // Bad data
                        throw SWIGEXCEPTION( "bad_data_type: char "
                                             "octal value" );
                     }

                     if( numeric_limits< CHAR >::max() < temp ||
                         temp < numeric_limits< CHAR >::min() )
                     {
                        // Data is out of range
                        throw SWIGEXCEPTION( "out_of_range: char" );
                     }                      
                     
                     // Check for extra data
                     s_pos = e.tellg();
                     s_end_pos = elem.size() - 1;
                     while( s_pos <= s_end_pos )
                     {
                        if( !isspace( elem[ s_pos ] ) )
                        {
                           throw SWIGEXCEPTION( "extra_data: char" );
                        }
                        ++s_pos;
                     }             
   
                     buf[ bufIndex++ ] = static_cast< CHAR >( temp );
                  }
                  nullmask.push_back( false );      
               }
            }

            if ( i == numCols )
            {
               i = 0;
            } 
            else
            {
               ++i;
            }
            if ( pos != string::npos )
            {
               prev_pos = ++pos;
            }
            else
            {
               prev_pos = pos;
            }
         }
         else
         {
            ++pos;
         }
      }
   }
   
   
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   const string units( nodeAttribute( node, LIGO_LW::unitAttName ) );
   
   
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS         
   const string unity( nodeAttribute( node, LIGO_LW::dataUnitAttName ) );      

   // All columns are converted to 1-dimensional ILWD format arrays
   const size_t nDim( 1 );
   ArrayJanitor< REAL_8 > dx( nodeAttribute< REAL_8 >( node, LIGO_LW::scaleAttName, nDim ) );
   if( dx.get() == 0 )
   {
      dx.reset( new REAL_8[ nDim ] );
      dx[ 0 ] = defaultScaleX;
   }

   ArrayJanitor< REAL_8 > startx( nodeAttribute< REAL_8 >( node, LIGO_LW::startAttName, nDim ) );
   if( startx.get() == 0 )
   {
      startx.reset( new REAL_8[ nDim ] );
      startx[ 0 ] = defaultStartX;
   }
   

   unique_ptr< LdasElement > a( new LdasArray< CHAR >( buf.get(),
                                                           numElem, name,
                                                           units, dx[ 0 ],
                                                           startx[ 0 ], unity ) );

#else
   
   unique_ptr< LdasElement > a( new LdasArray< CHAR >( buf.get(), 
                                                           numElem, name,
                                                           units ) );
   
#endif   

   
   for( size_t i = 0; i < numElem; ++i )
   {
      a->setNull( i, nullmask[ i ] );
   }        
   
   
   return a;   
}


//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.   
//      
template<>
unique_ptr< LdasElement > colData2ILwd< CHAR_U >( const DOMNode* const node,
   const string& data, const CHAR& delimiter, 
   const size_t colIndex, const size_t numCols, const INT_4U numElem )
{
   ArrayJanitor< CHAR_U > buf( 0 );
   vector< bool > nullmask;

   
   if( !data.empty() || numElem )
   {
      buf.reset( new CHAR_U[ numElem ] );   
      size_t i( 0 );
      INT_4U bufIndex( 0 );

      string elem;
      string::size_type pos( 0 ), prev_pos( 0 ),
                        s_pos( 0 ), s_end_pos( 0 );   
      string::const_iterator end( data.begin() ), 
                             data_begin( data.begin() );    
      INT_8U temp;
   
   
      while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
             ( prev_pos != string::npos ) )
      {
         if( pos == 0 || pos == string::npos || 
             ( pos != 0 && data[ pos - 1 ] != bslashChar ) )
         {
            if ( i == colIndex )
            {
               if( bufIndex >= numElem )
               {
                  throw SWIGEXCEPTION( "malformed_stream_data" );
               }
   
               if( pos == string::npos )
               {
                  end = data.end();
               }
               else
               {
                  end = data_begin + pos;
               }
               elem.assign( data_begin + prev_pos, end );
   
               // Check if data is missing for the current entry of i   
               if( isNull( elem ) )
               {
                  buf[ bufIndex++ ] = 0;
                  nullmask.push_back( true );
               }
               else
               {
                  // All printable characters will be enclosed into double quotes
                  if( elem.find_first_of( dquoteChar, 0 ) != string::npos )
                  {
                     // Make sure that character is enclosed into double quotes
                     // if it is a printable character
                     extractQuotedData( elem );                      

                     ArrayJanitor< CHAR_U > d( string2Data< CHAR_U >( elem, 1, delimiter ) );
                     buf[ bufIndex++ ] = d[ 0 ];   
                  }
                  else
                  {
                     // It's a numeric value
                     istringstream e( elem.c_str() );
                     // Get backslash character for octal representation
                     if( e.peek() != bslashChar )
                     {
                        throw SWIGEXCEPTION( "malformed_stream_data: char_u");
                     }   
                     e.get();
                     e >> std::oct >> temp;
   
                     if( !e )
                     {
                        // Bad data
                        throw SWIGEXCEPTION( "bad_data_type: char_u "
                                             "octal value" );
                     }

                     if( temp > numeric_limits< CHAR_U >::max() )
                     {
                        // Data is out of range
                        throw SWIGEXCEPTION( "out_of_range: char_u" );
                     }                      
   
                     // Check for extra data
                     s_pos = e.tellg();
                     s_end_pos = elem.size() - 1;
                     while( s_pos <= s_end_pos )
                     {
                        if( !isspace( elem[ s_pos ] ) )
                        {
			  std::cerr << "CERR DEBUG: here: " << __FILE__ << " " << __LINE__ << std::endl;
                           throw SWIGEXCEPTION( "extra_data: char_u" );
                        }
                        ++s_pos;
                     }                
   
                     buf[ bufIndex++ ] = static_cast< CHAR_U >( temp );
                  }
                  nullmask.push_back( false );         
               }
            }
            if ( i == numCols )
            {
               i = 0;
            } 
            else
            {
               i++;
            }
            if ( pos != string::npos )
            {
               prev_pos = ++pos;
            }
            else
            {
               prev_pos = pos;
            }
         }
         else
         {
            ++pos;
         }
      }
   }
   
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   const string units( nodeAttribute( node, LIGO_LW::unitAttName ) );

   
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS            
   
   const string unity( nodeAttribute( node, LIGO_LW::dataUnitAttName ) );      

   // All columns are converted to 1-dimensional ILWD format arrays
   const size_t nDim( 1 );
   ArrayJanitor< REAL_8 > dx( nodeAttribute< REAL_8 >( node, LIGO_LW::scaleAttName, nDim ) );
   if( dx.get() == 0 )
   {
      dx.reset( new REAL_8[ nDim ] );
      dx[ 0 ] = defaultScaleX;
   }

   ArrayJanitor< REAL_8 > startx( nodeAttribute< REAL_8 >( node, LIGO_LW::startAttName, nDim ) );
   if( startx.get() == 0 )
   {
      startx.reset( new REAL_8[ nDim ] );
      startx[ 0 ] = defaultStartX;
   }
   

   unique_ptr< LdasElement > a( new LdasArray< CHAR_U >( buf.get(), numElem, 
                                                               name, units,
                                                               dx[ 0 ], startx[ 0 ],
                                                               unity ) );

#else
   

   unique_ptr< LdasElement > a( new LdasArray< CHAR_U >( buf.get(), numElem,
                                                               name, units ) );
   
#endif   

   for( size_t i = 0; i < numElem; ++i )
   {
      a->setNull( i, nullmask[ i ] );
   }        
   
   return a;   
}


//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.   
//   
unique_ptr< LdasContainer > char2ILwd( const DOMNode* const node,
   const string& data, const char& delimiter, 
   const size_t colIndex, const size_t numCols ) 
{

   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );

   INT_4U destSize( 0 );

   if( data.empty() )
   {
      CHAR* buffer( 0 );   
      LdasArray< CHAR >* a = new LdasArray< CHAR >( buffer, destSize );
      c->push_back( a,
                    LdasContainer::NO_ALLOCATE,
                    LdasContainer::OWN );      
      c->setNull( 0, false );
      return c;
   }

   
   size_t i( 0 );
   string elem;
   string::size_type pos( 0 ), prev_pos( 0 ); 
   const char dataDelimiter( ' ' );
   
   vector< bool > nullmask;
   string::const_iterator end( data.begin() );
   //data_begin( data.begin() );
   

   while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
          ( prev_pos != string::npos ) )
   {
      if( pos == 0 || pos == string::npos ||
         ( pos != 0 && data[ pos - 1 ] != bslashChar ) )
      {
         if ( i == colIndex )
         {
            if( pos == string::npos )
            {
               end = data.end();
            }
            else
            {  
 	       end = data.begin();
               std::advance( end, pos );
            }

	    string::const_iterator begin( data.begin() );
	    std::advance( begin, prev_pos );

            elem.assign( begin, end );

            // Check if data is missing for the current entry of i
            if( isNull( elem ) ) 
            {
               // Create default array element
               destSize = 0;
               CHAR* buffer( 0 );
               LdasArray< CHAR >* a = new LdasArray< CHAR >( 
                                          buffer, destSize );
               c->push_back( a,
                             LdasContainer::NO_ALLOCATE,
                             LdasContainer::OWN );   
               nullmask.push_back( true );                   
            }
            else
            {
               // Make sure data is enclosed in double quotes
               extractQuotedData( elem );
               destSize = getNum( elem, dataDelimiter );

               ArrayJanitor< CHAR > buffer( string2Data< CHAR >( elem, destSize, dataDelimiter ) );                       
               LdasArray< CHAR >* a = new LdasArray< CHAR >( buffer.get(), destSize );
               c->push_back( a,
                             LdasContainer::NO_ALLOCATE,
                             LdasContainer::OWN );   
               nullmask.push_back( false );   
            }
         }
         if ( i == numCols )
         {
            i = 0;
         } 
         else
         {
            ++i;
         } 
         if ( pos != string::npos )
         {
            prev_pos = ++pos;
         }
         else
         {
            prev_pos = pos;
         }
      }
      else
      {
         ++pos;
      }
   }

   size_t size( c->size() );
   for( size_t i = 0; i < size; ++i )
   {
      c->setNull( i, nullmask[ i ] );
   }     

   return c;
}


//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.   
//      
unique_ptr< LdasContainer > charU2ILwd( const DOMNode* const node,
   const string& data, const char& delimiter,
   const size_t colIndex, const size_t numCols )
{
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );

   INT_4U destSize( 0 );

   
   if( data.empty() )
   {
      CHAR_U* buffer( 0 );   
      LdasArray< CHAR_U >* a( new LdasArray< CHAR_U >( buffer, destSize ) );
      c->push_back( a,
                    LdasContainer::NO_ALLOCATE,
                    LdasContainer::OWN );      

      c->setNull( 0, false );
      return c;
   }

   
   size_t i( 0 );
   string elem;
   string::size_type pos( 0 ), prev_pos( 0 );  
   const char dataDelimiter( ' ' );
   
   vector< bool > nullmask;
   string::const_iterator end( data.begin() ), 
                          data_begin( data.begin() );

   
   while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) ||
          ( prev_pos != string::npos ) )
   {
      if( pos == 0 || pos == string::npos ||
          ( pos != 0 && data[ pos - 1 ] != bslashChar ) )
      {
         if ( i == colIndex )
         {
            if( pos == string::npos )
            {
               end = data.end();
            }
            else
            {
               end = data_begin + pos;
            }
            elem.assign( data_begin + prev_pos, end );

            // Check if data is missing for the current entry of i
            if( isNull( elem ) ) 
            {
               // Create default array element
               destSize = 0;
               const CHAR_U* buffer( 0 );
               LdasArray< CHAR_U >* a( new LdasArray< CHAR_U >(
                                              buffer, destSize ) );
               c->push_back( a,
	                     LdasContainer::NO_ALLOCATE,
                             LdasContainer::OWN );   
               nullmask.push_back( true );
            }
            else
            {   

               // Make sure data is enclosed in double quotes
               extractQuotedData( elem );
               destSize = getNum( elem, dataDelimiter );

               ArrayJanitor< CHAR_U > buffer( string2Data< CHAR_U >( elem, destSize, dataDelimiter ) );      
               LdasArray< CHAR_U >* a = new LdasArray< CHAR_U >( buffer.get(), destSize );
               c->push_back( a,
                             LdasContainer::NO_ALLOCATE,
                             LdasContainer::OWN );   
               nullmask.push_back( false );   
            }   
         }   
         if ( i == numCols )
         {
            i = 0;
         } 
         else
         {
            ++i;
         }
         if ( pos != string::npos )
         {
            prev_pos = ++pos;
         }
         else
         {
            prev_pos = pos;
         }
      }
      else
      {
         ++pos;
      }
   }
   
   size_t size( c->size() );
   for( size_t i = 0; i < size; ++i )
   {
      c->setNull( i, nullmask[ i ] );
   }     
   
   return c;
}


//-----------------------------------------------------------------------
//: Converts LIGO_LW format Column into ILWD format element.
//
//!param: DOMNode& node - LIGO_LW Column element to convert.
//!param: deque< Query > dq - Tokenized command.
//
//!return: ILWD format element( array or lstring )
//
//!exc: stream_is_missing - Data stream element is missing.
//!exc: delimiter_is_missing - Stream delimiter is missing.
//!exc: type_is_missing - Stream type is missing. 
//!exc: file2stream_failed - file2Stream failed.
//!exc: out_of_range - Data type is out of range
//!exc: bad_data_type - Bad data type was specified.   
//!exc: columns_are_missing - Table contains no columns.   
//!exc: extra_data - Extra data was specified for the element.   
//!exc: bad_alloc - Memory allocation failed.   
//
unique_ptr< LdasElement > column2ILwd( const DOMNode* const node, 
   deque< Query > dq ) 
{
   const DOMNode* parentNode( node->getParentNode() );
   
   // Extract "Stream" nodes
   const vector< DOMNode* > streamVector( getChildByTag( parentNode, 
                                                         LIGO_LW::streamTag ) );
   if ( streamVector.empty() )  
   {
      throw SWIGEXCEPTION( "stream_is_missing" );
   }
   

   DOMNode* stream( streamVector.front() );
   const string delimiter_str( nodeAttribute( stream, LIGO_LW::delimiterAttName ) );
   
   if( delimiter_str.empty() )
   {
      throw SWIGEXCEPTION( "stream_delimiter_is_missing" );
   }

   string type( nodeAttribute( stream, LIGO_LW::typeAttName ) );
   if( type.empty() )
   {
      throw SWIGEXCEPTION( "stream_type_is_missing" );
   }

   if( type == LIGO_LW::remoteTypeAttValue )   
   {
      if ( file2Stream( stream ) == false )
      {
         throw SWIGEXCEPTION( "file2stream_failed" );
      }
   }

   
   const vector< DOMNode* > colVector(
      getChildByTag( parentNode, LIGO_LW::columnTag ) );
   
   if( colVector.size() == 0 )
   {
      throw SWIGEXCEPTION( "columns_are_missing" );
   }
   
   size_t index( 0 );
   size_t size( colVector.size() );
   while( ( colVector[ index ] != node ) && ( size-- > 0 ) )
   {
      ++index;
   }

   string::size_type pos( 0 );
   size_t i( 0 );
   const string data( nodeValue( stream ) );
   
   const CHAR delimiter( delimiter_str.data()[ 0 ] );
   
   
   // Calculate total number of elements in the stream
   while( ( ( pos = data.find_first_of( delimiter, pos ) ) != string::npos ) )
   {
      // Do not count escaped delimiter character   
      if( ( pos == 0 ) || 
          ( pos != 0 && ( data[ pos - 1 ] != bslashChar ) ) )
      {
         ++i;
      }
      ++pos;
   }
   
   INT_4U numElem( ( i + 1 )/colVector.size() );

   type = nodeAttribute( node, LIGO_LW::typeAttName ); 
   if ( type == "lstring" )
   {
      return colString2ILwd( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "char" )
   {
      return colData2ILwd< CHAR >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "char_u" )
   {
      return colData2ILwd< CHAR_U >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "ilwd:char" )
   {
      unique_ptr< LdasContainer > c( char2ILwd( node, data, delimiter,
         index, colVector.size() - 1 ) );
   
      if( c->size() != numElem && data.empty() == false )
      {
         throw SWIGEXCEPTION( "bad_ilwd:char_element" );
      }
   
      return unique_ptr< LdasElement >( c.release() );
   }
   else if ( type == "ilwd:char_u" )
   {
      unique_ptr< LdasContainer > c( charU2ILwd( node, data, delimiter,
         index, colVector.size() - 1 ) );
   
      if( c->size() != numElem && data.empty() == false )
      {
         throw SWIGEXCEPTION( "bad_ilwd:char_u_element" );
      }
   
      return unique_ptr< LdasElement >( c.release() );
   }
   else if ( type == "ilwd:null" )
   {
      string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
      if( name.find( xml_name ) == string::npos )
      {
         name.append( xml_name );
      }

      return unique_ptr< LdasElement >( new LdasContainer( name ) );
   }   
   else if ( type == "int_2s" )
   {
      return colData2ILwd< INT_2S >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "int_2u" )
   {
      return colData2ILwd< INT_2S >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "int_4s" )
   {
      return colData2ILwd< INT_4S >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "int_4u" )
   {
      return colData2ILwd< INT_4U >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "int_8s" )
   {
      return colData2ILwd< INT_8S >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "int_8u" )
   {
      return colData2ILwd< INT_8U >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "real_4" )
   {
      return colData2ILwd< REAL_4 >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else if ( type == "real_8" )
   {
      return colData2ILwd< REAL_8 >( node, data, delimiter,
                index, colVector.size() - 1, numElem );
   }
   else
   {
     throw SWIGEXCEPTION( "unsupported_column_type" );
   }

   return unique_ptr< LdasElement >( 0 );
}
