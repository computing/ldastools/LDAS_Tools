#ifndef UtilLwHH
#define UtilLwHH

// System Header Files
#include <strings.h>

#include <set>
#include <string>
#include <cstring>
#include <deque>
#include <memory>   

// General Header Files   

#include "general/unordered_map.hh"
#include "general/types.hh"
#include "general/Memory.hh"

using std::strcmp;
   
// Forward declarations
namespace ILwd
{
   class LdasElement;
}   

namespace XERCES_CPP_NAMESPACE  
{
   class DOMNode;
}

class Query;


//-----------------------------------------------------------------------------
//
//: Case-Sensitive Compare Functional.
//
// This functional is designed to perform a case-sensitive compare of two 
// strings.
//
class EqStr
{
public:
    //
    //: Overloaded operator().
    // 
    //!param: const char* s1 - First string.
    //!param: const char* s2 - Second string.
    //
    //!return: bool - True if strings are equal, false otherwise.
    //
    bool operator()( const CHAR* s1, const CHAR* s2 ) const
    {
       return( strcmp( s1, s2 ) == 0 );
    }
};

   
//-----------------------------------------------------------------------------
//
//: Case-Insensitive Compare Functional.
//
// This functional is designed to perform a case-insensitive compare of two 
// strings.
//
class NoCaseEqStr
{
public:
    //
    //: Overloaded operator().
    // 
    //!param: const char* s1 - First string.
    //!param: const char* s2 - Second string.
    //
    //!return: bool - True if strings are equal, false otherwise.
    //
    size_t operator()( const CHAR* s1, const CHAR* s2 ) const
    {
       return( strcasecmp( s1, s2 ) == 0 );
    }
};
   

// Typedefs for the functionals:
typedef std::set< std::string > ElementSet;

// Typedef for XML-->ILWD conversion function   
typedef std::unique_ptr< ILwd::LdasElement > ( *convFP )( const XERCES_CPP_NAMESPACE::DOMNode* const,
                                        std::deque< Query > );
   
// Typedef for hash of XML data type and corresponding conversion function
typedef General::unordered_map< INT_4U, convFP, 
				General::hash< INT_4U > > LwDataHash;
   
typedef General::unordered_map< std::string, INT_4S > QueryHash;
   
typedef General::unordered_map< std::string, const CHAR* > MetadataHash;

typedef MetadataHash::value_type metadataType;   

   
#endif
