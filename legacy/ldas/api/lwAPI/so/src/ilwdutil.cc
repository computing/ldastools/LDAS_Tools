#include "config.h"

// ILWD Header Files
#include <ilwd/ldaselement.hh>   
   
// XML Header Files
#include <xercesc/dom/DOMNode.hpp>   
   
// Local Header Files
#include "ilwdutil.hh"
#include "lwutil.hh"   
   
using namespace XERCES_CPP_NAMESPACE;
   
   
//-----------------------------------------------------------------------
//: Sets metadata for the ILWD format element.
//
//!param: const DOMNode* const node - A reference to the LIGO_LW element.
//!param: MetadataHash& h - A reference to the metadata hash map.
//!param: const LdasElement* e - A pointer to the ILWD representation of
//+       the <i>node</i> element.  
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
void setILwdMetadata( const DOMNode* const node,
   MetadataHash& h, ILwd::LdasElement* const e ) 
{
   std::string value;
   for( MetadataHash::const_iterator iter = h.begin(),
        end_iter = h.end(); iter != end_iter; ++iter )
   {
      value = nodeAttribute( node, iter->first.c_str( ) );   
      e->setMetadata( iter->second, value );
   }
   
   return;
}
