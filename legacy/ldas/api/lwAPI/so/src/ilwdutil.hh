#ifndef LwILwdUtilHH
#define LwUILwdtilHH

// Local Header Files   
#include "util.hh"   
   
namespace ILwd
{
   class LdasElement;
}
   
namespace XERCES_CPP_NAMESPACE  
{
   class DOMNode;
}
   

//-----------------------------------------------------------------------
//: Sets metadata for the ILWD format element.
//
//!param: const DOMNode* const node - A reference to the LIGO_LW element.
//!param: MetadataHash& h - A reference to the metadata hash map.
//!param: LdasElement* const e - A pointer to the ILWD representation of
//+       the <i>node</i> element.  
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//      
void setILwdMetadata( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node,
   MetadataHash& h, ILwd::LdasElement* const e );
   
   
#endif   

