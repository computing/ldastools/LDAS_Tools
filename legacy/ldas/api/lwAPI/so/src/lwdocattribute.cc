#include "config.h"

#include "general/Memory.hh"

// GenericAPI Includes
#include <genericAPI/swigexception.hh>

// ILwd Header Files   
#include <ilwd/ldaselement.hh>   
   
// General Header Files
#include <general/types.hh>   
   
// XML Header Files
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMElement.hpp>   
   
// Local Header Files
#include "lwdocument.hh"
#include "lwdocattribute.hh"
#include "lwelementattribute.hh"
#include "lwelementdata.hh"
#include "lwputdata.hh"
#include "lwtoc.hh"

// Local Header Files
#include "query.hh"

   
using namespace ILwd;
using namespace std;
using namespace XERCES_CPP_NAMESPACE;   
   
   
void tokenize( const string& s, deque< Query >& q )
{
   const CHAR* end = s.c_str() + s.size();
   const CHAR* index = s.c_str();
   const CHAR* next;
    
   while ( index < end )
   {
      q.push_back( Query( index, &next ) );
      index = next;
   }
   
   return;  
}


//--------------------------------------------------------------------------------
//
//: Extract an attribute of specified item in the document.
//
//!param: const LWDocument* const doc - LIGO_LW Document.
//!param: std::deque< Query >& dq - Tokenized command.
//!param: std::vector< size_t >& start - Vector of item location.  
//!param: size_t index - Current index into position vector..   
//
//!return: Found attribute or "NULL" if attribute is empty.
//
//!exc: SwigException
//   
const std::string getAttribute( 
   const LWDocument* const doc, 
   std::deque< Query >& dq, 
   std::vector< size_t >& start, const size_t index )
{
    // Get the root element of the document
    DOMElement* const root = doc->getRootElement();
    ElementAttribute e;
    return e( root, dq, start, index );
}


//--------------------------------------------------------------------------------
//: Extract data from the document.
// 
//!param: LWDocument& doc - LIGO_LW Document.
//!param: std::deque< Query >& dq - Tokenized command.
//!param: std::vector< size_t >& start - Vector of item location.  
//!param: size_t index - Current index into position vector.
//
//!return: ILwd::LdasElement* - A pointer to ILWD element.
//
//!exc: bad_alloc - Error allocating new memory.
//!exc: SwigException   
//   
std::unique_ptr< ILwd::LdasElement > getData( 
   const LWDocument* const doc,
   std::deque< Query >& dq,
   std::vector< size_t >& start, const size_t index ) 
{
    // Get the root element of the document
    DOMElement* const root = doc->getRootElement();
   
    ElementData d;
    return d( root, dq, start, index );
}
   

//--------------------------------------------------------------------------------
//
//: Put raw data of a specified element in external file.
// 
//!param: LWDocument& doc - LIGO_LW document.
//!param: std::deque< Query >& dq - Tokenized command.
//!param: std::vector< size_t >&start - Vector to record element position
//+       in the document tree.
//!param: size_t index - Current index into position vector.
//!param: const char* urlAddr - URL address.
//!param: const char* urlDir - URL directory.
//
//!return: True if data was placed in external file, false otherwise.
//
//!exc: bad_alloc - Error allocating the memory.   
//!exc: SwigException   
//   
bool putData( 
   const LWDocument* const doc,
   std::deque< Query >& dq, 
   std::vector< size_t >& start, const size_t index,
   const char* const urlAddr, const char* const urlDir ) 
{
    // Get the root element of the document
    DOMElement* const root = doc->getRootElement();
   
    ElementPutData d( urlAddr, urlDir );
    return d( root, dq, start, index ); 
}


//------------------------------------------------------------------------------------
//
//: Get the content of specified LIGO_LW element in the document.
// 
//!param: LWDocument& node - LIGO_LW document.
//!param: std::deque< Query >& dq - Tokenized command.
//!param: std::vector< size_t >& start - Vector with element index
//+       information.
//!param: size_t index - Current index into 'start' vector.
//
//!return: std::string - String containing the content list of the LIGO_LW
//+        element 
//
//!exc: SwigException
//   
const std::string getTOC( 
   const LWDocument* const doc,
   std::deque< Query >& dq,
   std::vector< size_t >& start, const size_t index ) 
{
    // Get the root element of the document
    DOMElement* const root = doc->getRootElement();
   
    ElementTOC e;
    return e( root, dq, start, index );
}
