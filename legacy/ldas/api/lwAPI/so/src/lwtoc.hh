#ifndef LwTOCHH
#define LwTOCHH

// Local Header Files
#include "util.hh"


//------------------------------------------------------------------------------
//
//: ElementTOC functional.
//
// This recursive functional is designed for extracting a content of a specified 
// LIGO_LW element. This functional is legal only for the container type 
// elements, such as LIGO_LW, Table, AdcData, Detector and IGWDFrame.
//
class ElementTOC
{

private:

  const std::string find( 
     const std::vector< XERCES_CPP_NAMESPACE::DOMNode* >& nodeList, 
     std::deque< Query >& dq,
     std::vector< size_t >& start, const size_t index );

  const std::string getContent( 
     XERCES_CPP_NAMESPACE::DOMNode* const node,
     std::deque< Query> dq ); 

  typedef General::unordered_map< std::string, const CHAR* > TOCHash;
   
  static TOCHash initLwTOC();
  static TOCHash mTOCHash;

public:

  /* ( Default ) Constructor. */
   ElementTOC();

  //: Overloaded operator().
  //
  // This operator gets the table of content for specified LIGO_LW element.
  // 
  //!param: const DOMNode* const node - A reference to the LIGO_LW element.
  //!param: std::deque< Query > dq - Tokenized command.
  //!param: std::vector< size_t >start - Vector with element index information.
  //!param: size_t index - Current index into 'start' vector.
  //
  //!return: std::string - Table of content for the LIGO_LW element 
  //
  //!exc: not_valid_element - Specified element is not valid.
  //!exc: bad_query - Query is bad.
  //
  const std::string operator()( 
     XERCES_CPP_NAMESPACE::DOMNode* const node, 
     std::deque< Query > dq, 
     std::vector< size_t >& start, const size_t index );
};


#endif
