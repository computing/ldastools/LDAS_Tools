#ifndef LwAPIConstantsHH
#define LwAPIConstantsHH


// Constants   
const char bslashChar( '\\' );
const char dquoteChar( '\"' );   

// Default values   
const double defaultStartX( 0.0f );   
const double defaultScaleX( 1.0f );     
   

namespace LIGO_LW
{
// XML Tag   
const char* const XMLTag( "XML" );   
   
// Attribute names      
const char* const unitAttName( "Unit" );   
const char* const dataUnitAttName( "DataUnit" );      
const char* const startAttName( "Start" );      
const char* const scaleAttName( "Scale" );         
const char* const typeAttName( "Type" );  
const char* const nameAttName( "Name" );             
const char* const delimiterAttName( "Delimiter" );   
const char* const encodingAttName( "Encoding" );   
   
// Attribute values   
const char* const remoteTypeAttValue( "Remote" );   
const char* const localTypeAttValue( "Local" );      
const char* const timeseriesTypeAttValue( "TimeSeries" );   
const char* const lstringTypeAttValue( "lstring" );   
const char* const gpsTypeAttValue( "GPS" );   
const char* const isoTypeAttValue( "ISO-8601" );   

const char* const littleEndianEncodingAttValue( "LittleEndian" );   
const char* const bigEndianEncodingAttValue( "BigEndian" );      
const char* const base64EncodingAttValue( "base64" );      
const char* const textEncodingAttValue( "Text" );      
const char* const binaryEncodingAttValue( "Binary" );
   
// DTT support: set Type for all AdcData elements ===>
// DTT knows what to do with it.   
const char* const DTTTimeSeries( "LDASTimeSeries" );   
   
const char* const durationNameValue( "Duration" );   
   
// Tag names   
const char* const ligoLwTag( "LIGO_LW" );      
const char* const adcDataTag( "AdcData" );   
const char* const streamTag( "Stream" );   
const char* const columnTag( "Column" );      
const char* const dimTag( "Dim" );   
const char* const paramTag( "Param" );   
const char* const commentTag( "Comment" );   
const char* const timeTag( "Time" );      
const char* const arrayTag( "Array" );      


// ILWD FrDetector elements
const char* const detectorPrefix( "prefix" );   
   
}
   

// Support for Column: Start, Scale and DataUnit was removed( 
// these are not applicable to the Column element,
// in case they'll ever be ---> enable this macro )   
//#define LIGO_LW_COLUMN_EXTRA_ATTS   
   
#endif   
