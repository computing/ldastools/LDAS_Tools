#ifndef LWDocumentHH
#define LWDocumentHH

// System Header Files
#include <iosfwd>
#include <vector>   

#include "general/unordered_map.hh"
#include "general/util.hh"

#include "util.hh"
#include "lwconsts.hh"   

// Forward declarations
namespace ILwd
{
   class LdasArrayBase;
   template < class T > class LdasArray;
   class LdasElement;
   class LdasString;
}
   
namespace XERCES_CPP_NAMESPACE  
{
   class DOMNode;
   class DOMElement;
   class DOMDocument;
}
   

//------------------------------------------------------------------------------
//
//: LIGO_LW Document.
//
// This class represents LIGO_LW format document. It's designed to store all 
// neccessary information for a valid XML document: DTD, document tree, and 
// external references declarations( is not used ). LIGO_LW has its own way of
// manipulating external references in the document.
//
class LWDocument 
{
public:

   /* Constructors */
   LWDocument();
   LWDocument( const XERCES_CPP_NAMESPACE::DOMDocument* doc );

   /* Destructor */
   ~LWDocument();  

   /* Getter methods */
   XERCES_CPP_NAMESPACE::DOMDocument* getDocument() const;
   XERCES_CPP_NAMESPACE::DOMElement* getRootElement() const;
   static const CHAR* getDTD();
   
   /* Mutators */
   static void setExpandReferences( const CHAR* const );
   void setStreamEncoding( const CHAR* format );

   /* Conversion methods */
   void convertToLW( const ILwd::LdasElement* const elem,
      const bool is_root = false ); 
   void convertToTimeSeries( const ILwd::LdasElement* const elem,
      const bool is_root = false );
   
   /* I/O method */
   void write( std::ostream& stream );

private:

   /* Typedefs */
   typedef bool( LWDocument::* LwPMthd )( const ILwd::LdasElement* const, 
                                          const bool, const CHAR* const );
   typedef std::pair< const CHAR*, LwPMthd > vpair;
   
  typedef General::unordered_map< std::string, vpair,
				  General::ic_hash< std::string > >
  LWHash;
   

   // Metadata hash
   typedef General::unordered_map< std::string, MetadataHash*, 
				   General::ic_hash< std::string > >
   LWMetadata;

   
   // Format specific hashes:
   typedef General::unordered_map< std::string, const INT_4U,
				   General::ic_hash< std::string > > 
   StringFormatHash;

   typedef General::unordered_map< INT_4U, const CHAR* > FormatStringHash;
   
   
   static const LWHash initHash();
   static const LWHash initContainerHash();   
   
   static MetadataHash& initAdcIntervalHash();
   static LWMetadata& initMetadataHash();   
   static StringFormatHash initStringFormatHash();   
   static FormatStringHash initFormatStringHash();      

   static const CHAR* const mDTD;
   static bool mExpandReferences;
   
   static LWHash mILwdHash;
   static LWHash mILwdContainerHash;
   
   static LWMetadata& mMetadata;      
   static StringFormatHash mStringFormatHash;   
   static FormatStringHash mFormatStringHash;  

   // Helpers
   void expandReferences();

   /* Methods to create new elements */
   void startElement( const ILwd::LdasElement* const elem,
      const bool is_root = false ); 
   void endElement( const ILwd::LdasElement* const elem,
      const bool is_root = false );
   
   bool makeLigoLw( const ILwd::LdasElement* const elem, 
                    const bool is_root = false,
                    const CHAR* name = LIGO_LW::ligoLwTag );
   bool makeLigoLwType( const ILwd::LdasElement* const elem, 
                        const bool is_root = false,   
                        const CHAR* type = 0 );
   
   void makeColumn( const ILwd::LdasElement* const elem ); 
   void makeParam( const ILwd::LdasString* const elem );
   void makeParam( const ILwd::LdasArrayBase* const elem, 
                   const std::string& name = "" );
   void makeComment( const ILwd::LdasElement* const elem ); 
   void makeStream( const ILwd::LdasElement* const elem ); 
   void makeArray( const ILwd::LdasArrayBase* const elem ); 
   void makeDim( const ILwd::LdasArrayBase* const elem ); 
   void makeTime( const ILwd::LdasElement* const elem, 
                  const std::string& name = "" ); 
   void setMetadata( const CHAR* const name, 
                     const ILwd::LdasElement* const elem );

   /* Accessors */
   template< class T >
   const std::string getData( const ILwd::LdasArray< T >* const elem, 
                        const CHAR& delimiter );

   template< class T >
   const T getData( const ILwd::LdasArray< T >* const elem, 
                    const size_t i );

   template< class T >
   const std::string getArrayData( const ILwd::LdasArray< T >* const elem, 
                                   const CHAR& delimiter );
   
   const std::string getData( const ILwd::LdasString* const elem, 
                              const size_t i, const CHAR& s );
   const std::string getTableData( const ILwd::LdasElement* const elem ); 

   /* Methods to insert data into LIGO_LW elements */
   void insertData( const ILwd::LdasArrayBase* const elem );
   void insertData( const ILwd::LdasArrayBase* const elem, const size_t i,
                    std::ostringstream& s );

   size_t getSize( const ILwd::LdasElement* const elem );
   
   /* Data members */
   XERCES_CPP_NAMESPACE::DOMDocument* mDocument;  
   //: A pointer to the current parent node in the mDocument
   XERCES_CPP_NAMESPACE::DOMNode* mCurrentParent;
   std::vector< XERCES_CPP_NAMESPACE::DOMNode* > mNodeStack;
   
   //: Stream data encoding for writing to the file.
   //: Used only for Array element Stream's.
   INT_4U mStreamFormat;
};


//------------------------------------------------------------------------------
//
//: Get Document node of LIGO_LW document.
//
// The Document node is a parent element for the LIGO_LW document.
// It contains as its children DTD, root element, and external references 
// declarations. This is the node which generates new elements in the 
// LIGO_LW document tree.
//
//!return: DOMDocument* - Document node.
//
inline XERCES_CPP_NAMESPACE::DOMDocument* LWDocument::getDocument() const
{
   return mDocument;
}


#include "lwdocument.icc"

#endif
                                                    
