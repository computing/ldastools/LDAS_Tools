#include "config.h"

// System Header Files
#include <new>
#include <memory>   

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
using ILwd::LdasElement;   
using ILwd::LdasContainer;

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// Local Header Files
#include "lwtable.hh"
#include "lwcomment.hh"
#include "lwcolumn.hh"
#include "lwelement.hh"
#include "query.hh"
#include "util.hh"
#include "lwutil.hh"
#include "lwconsts.hh"   

using namespace std;   
using namespace XERCES_CPP_NAMESPACE;
   

static LwDataHash initTableHash()
{
   LwDataHash h;
   
   h[ LW_COLUMN_ID  ] = column2ILwd;
   //   h[ LW_COMMENT_ID ] = comment2ILwd;
   
   return h;
}

   
static LwDataHash tableHash( initTableHash() );
static const char* xml_name = ":Table:XML";


//-----------------------------------------------------------------------
//: Convert LIGO_LW Table into ILWD format array.
//
//!param: node - Table element to convert.
//!param: dq - Tokenized command.
//
//!return: ILWD container element.
//
//!exc: SwigException
//!exc: bad_alloc - Memory allocation failed.
//   
unique_ptr < LdasElement > table2ILwd( const DOMNode* const node,
   std::deque< Query > dq )
{
   string name = nodeAttribute( node, LIGO_LW::nameAttName );
   
   if( name.find( xml_name ) == string::npos )
   {
      string type( nodeAttribute( node, LIGO_LW::typeAttName ) );
      if( type.empty() == false )
      {
         name += ':';
         name += type;
      }
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );
   
   // Get the Comment element:
   DOMNode* child( node->getFirstChild() );
   
   while( child != 0 ) 
   {
      // We assume that there is only one Comment element
      // per Table.
      if( getLwType( child ) == LW_COMMENT_ID )
      { 
         c->setComment( nodeValue( child ) );
         break;
      }
      child = child->getNextSibling();   
   }
   
   
   child = node->getFirstChild();
   LwType type;
   
   while( child != 0 ) 
   {
      if( child->getNodeType() == DOMNode::ELEMENT_NODE )
      {   
         type = getLwType( child );            
         if( tableHash.find( type ) != tableHash.end() )
         {
            c->push_back( tableHash[ type ]( child, dq ).release(),
                          LdasContainer::NO_ALLOCATE,
                          LdasContainer::OWN );
         }
         else 
         if( type != LW_COMMENT_ID && 
             type != LW_STREAM_ID )
         {
            ostringstream s;
            s << "lwtable_unsupported_element_type: " 
              << type;
   
            throw SWIGEXCEPTION( s.str() );   
         }      
      }
 
      child = child->getNextSibling();
   }
   
   
   return unique_ptr< LdasElement >( c.release() );
}


