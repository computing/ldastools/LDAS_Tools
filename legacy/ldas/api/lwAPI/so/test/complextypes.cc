#include "../src/config.h"

#define ComplexTypesCC

#include <sstream>   
   
// LDAS Header Files
#include <general/objectregistry.hh>
#include <general/unittest.h>   
#include <genericAPI/util.hh>
#include <genericAPI/registry.hh>
#include <genericAPI/swigexception.hh>
   
#include "lwcmd.hh"
#include "lwdocument.hh"
#include "lwutil.hh"   

//#define DETAILED_CONVERSION         
  
#ifdef DETAILED_CONVERSION      
#include "lwdocattribute.hh"   
#include "query.hh"
#endif   

#include <xercesc/util/PlatformUtils.hpp>      
#include <xercesc/sax/SAXException.hpp>   
#include <xercesc/dom/DOMException.hpp>   
#include <xercesc/util/Janitor.hpp>   
#include <xercesc/util/XMLException.hpp>      
   
using ILwd::LdasContainer;   
using ILwd::LdasElement;   
using ILwd::Reader;
   
using namespace std;   
using namespace XERCES_CPP_NAMESPACE;   
   
   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true    
General::UnitTest complexTest;
   
   
static const char datatypes_container[] = 
"<ilwd comment='complex datatypes test' size='10'>\n"
"    <char dims='2' name='TesT' units='TesT'>aa</char>\n"
"    <char_u dims='2' name='TesT' units='TesT'>\142\142</char_u>\n"
"    <int_2s dims='2' name='TesT' units='TesT'>-1 -1</int_2s>\n"
"    <int_2u dims='2' name='TesT' units='TesT'>1 1</int_2u>\n"
"    <int_4s dims='2' name='TesT' units='TesT'>-2 -2</int_4s>\n"
"    <int_2u dims='2' name='TesT' units='TesT'>2 2</int_2u>\n"
"    <real_4 dims='2' name='TesT' units='TesT'>1.0000000e+00 1.0000000e+00</real_4>\n"
"    <real_8 dims='2' name='TesT' units='TesT'>2.0000000000000000e+00 2.0000000000000000e+00</real_8>\n"
"    <complex_8 dims='2' name='TesT' units='TesT'>1.0000000e+00 1.2000000e+00 1.3000000e+00 1.4000000e+00</complex_8>\n"
"    <complex_16 dims='2' name='TesT' units='TesT'>1.0000000000000000e+00 1.2000000000000000e+00 1.3000000000000000e+00 1.3999999999999999e+00</complex_16>\n"
"</ilwd>";   
   
   
int main( int argc, CHAR** argv )
try   
{
   // Initialize test
   complexTest.Init( argc, argv );

   istringstream s( datatypes_container );

   auto_ptr< Reader > r( new Reader( s ) );   
   LdasContainer* c( new LdasContainer( *r ) );
   Registry::elementRegistry.registerObject( c );   
   r.reset( 0 );

   
   try
   {
      XMLPlatformUtils::Initialize();   
   }
   catch( const XMLException& exc )
   {
      LIGO_LW::XString msg( exc.getMessage() );
   
      complexTest.Check( false ) << "error initializing xml: "
                            << msg.charForm() << endl;
      complexTest.Exit();      
   }
   
   
   LWDocument* doc( 0 );      
   try
   {
      doc = ilwd2Lw( c );
   
      // Display the content
      const string doc_string( getLWDocument( doc ) );      
   
#ifdef DETAILED_CONVERSION   
      cout << "Document:" << endl
           << doc_string << endl;
#endif   
   
      string elem( getLwData( doc, "full" ) );         


#ifdef DETAILED_CONVERSION   
    deque< Query > dq;
    tokenize( "full", dq );
   
    vector< size_t > sarray;
    LdasElement* element( getData( doc, dq, sarray, 0 ).release() );
    Registry::elementRegistry.registerObject( element );
   
    if( element->getElementId() != ILwd::ID_ILWD )
    {
       throw runtime_error( "Final ILWD element is not a container." );          
    }
   
    LdasContainer* result_c( dynamic_cast< LdasContainer* >( element ) );
    cout << "Result container size = " << result_c->size() << endl
         << "Container name = " << result_c->getNameString() << endl;
   
    for( LdasContainer::const_iterator iter = ( *result_c ).begin(),  
         end_iter = ( *result_c ).end(); iter != end_iter; ++iter )
    {
       cout << "Element: " << (*iter)->getNameString() << endl;
    }
   
#endif   
   
   }
   catch( const SwigException& e )
   {
      string msg = e.getResult();
      complexTest.Check( false, msg );
   }
   catch( const XMLException& exc )
   {
       ArrayJanitor< CHAR > msg(
          XMLString::transcode( exc.getMessage() ) ); 
    
       string error_msg( "XMLExcepton: " );
       error_msg += msg.get();
   
       complexTest.Check( false, error_msg );   
    }      
    catch( const SAXException& exc )
    {
       ArrayJanitor< char > msg(
          XMLString::transcode( exc.getMessage() ) ); 
    
       string error_msg( "SAXExcepton: " );
       error_msg += msg.get();
   
       complexTest.Check( false, error_msg );   
    }         
    catch( const DOMException& exc )
    {
       ostringstream s;
       s << "DOMException: code=" << exc.code; 
       
       if( XMLString::stringLen( exc.msg ) )
       {
          ArrayJanitor< CHAR > exc_msg( 
             XMLString::transcode( exc.msg ) );
       
          s << " message=\"" << exc_msg.get() << '\"'; 
       }

       complexTest.Check( false, s.str() );   
   }   
   catch( const exception& exc )
   {
       complexTest.Check( false, exc.what() );      
   }

   
   if( doc )
   {
      destructLWDocument( doc ); 
      doc = 0;
   }
   
   // Cleanup xml
   XMLPlatformUtils::Terminate();      
   
   
   Registry::elementRegistry.reset();     
   complexTest.Exit();

   return 0;
}
catch( const bad_alloc& )
{
   complexTest.Check( false ) << "memory allocation failed"
                              << endl << flush;
   complexTest.Exit();   
}
catch( const LdasException& exc )
{
   size_t exc_size( exc.getSize() );
   for( size_t i = 0; i < exc_size; ++i )
   {
     complexTest.Check( false ) << "LdasException: " 
                                << exc[ i ].getMessage() 
                                << "( " 
                                << exc[ i ].getInfo()
                                << " )"       
                                << endl << flush;
   }
   
   complexTest.Exit();   
}
catch( ... )
{
   complexTest.Check( false ) << "unknown exception"
                              << endl << flush;
   complexTest.Exit();   
}

   
