## ********************************************************
## 
## Name: LDASligolw.rsc
##
## This is the ligolw API specific resource file.  It contains
## resource information which is only used by the ligolw API.
##
## If this file is not found there the ligolw API and hence the
## ligolw API will be non-functional.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=1.91;content-type=text%2Fplain
## ********************************************************
;#barecode

## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES

;## desc=max memory MB allowed
set ::MEMFLAG_MEGS 1024

;## desc=valid url patterns
set ::VALID_URLS	"^(http|ftp|file|mailto):"

;## desc=valid LIGO_LW keywords
set ::VALID_KEYWORDS "frame|table|adcdata|frame_group|detector"

;## desc=outer container id
set ::WRAPPED_CONTAINER wrapped

;## desc=max time before dataRecv threads are forced to finish
set  ::THREAD_TIMEOUT_SECS  20

;## desc=time out for job if no data arrives (msecs)
set ::LIGOLW_MAX_JOB_TIME 100000

;## desc=dataRecv thread wakeup bgLoop frequency (secs)
set ::DATARECV_BGLOOP_SECS 60

;## desc=path to lsof executable
set ::PATH_TO_LSOF /usr/sbin/lsof

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default core default cputime default datasize default \
filesize default memorylocked default descriptors default maxproc default ]
