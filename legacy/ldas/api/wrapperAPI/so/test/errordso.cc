#include "LDASConfig.h"

// System Header Files
#include <stdexcept>   
#include <cstring>   
#include <string>
   
// wrapperAPI header Files
//#include <wrapperInterfaceDatatypes.h>

// Local Header Files   
#include "io.hh"   
#include "convertutil.hh"

using IODso::ConvertData;   
using namespace std;   
   
   
//: Flag if routine is called by searchMaster   
bool searchMaster( false );   
   
   
//-------------------------------------------------------------------------------
//   
//: initSearch routine.
//
// This method does nothing. It's just a routine placeholder( gets called by the
// wrapperAPI ).
//   
//!return: INT4 - Error code.
//      
INT4 initSearch( CHAR** initStatus, InitParams* initParams )
{
   if( initParams->nodeClass )
   {
      searchMaster = true;
   }
   
   return 0;
}
   
   
//-------------------------------------------------------------------------------
//   
//: conditionData routine.
//
// This method does nothing. It's just a routine placeholder( gets called by the
// wrapperAPI ).
//   
//!return: INT4 - Error code.
//      
INT4 conditionData( CHAR** conditionStatus, inPut* data,
   SearchParams* searchParams )
{
   return 0;
}
   
   
//-------------------------------------------------------------------------------
//   
//: applySearch routine.
//
// This method converts inPut data structure into the outPut.
//
//!param: CHAR** freeStatus - Error message if any
//!param: SearchOutput* output - Structure holding outPut and its metadata.   
//   
//!return: INT4 - Error code.
//            
INT4 applySearch( CHAR** searchStatus, inPut* input, 
   SearchOutput* output, SearchParams* searchParams )
{
   static UINT4 counter( 1 );
   static const REAL4 delta( 0.25f );

   // Only searchMaster converts inPut into outPut:
   if( searchMaster )
   {
      output->fracRemaining = 1.0f - counter * delta;
      output->notFinished = true;
      output->numOutput = 0;

      if( counter == 1 )
      {
         try
         {
            if( input == 0 )
            {
               throw runtime_error( string( "Null inPut pointer." ) );
            }
   
            if( output == 0 )
            {
               throw runtime_error( string( "Null SearchOutput pointer." ) );   
            }
   
            if( searchParams == 0 )
            {
               throw runtime_error( string( "Null SearchParams pointer." ) );   
            }   
   
            // This will guarantee that applySearch loop exits after 
            // very first call to 'applySearch'.
            // output->fracRemaining = 0.0f;
            // output->notFinished = false;
            output->numOutput = input->numberSequences;
   
            // Do actual conversion
            ConvertData convert( output->numOutput );
            convert( input, &( output->result ) );
         }
         catch( const std::exception& exc )
         {
            const CHAR* error_msg( exc.what() );
            const size_t error_len( strlen( error_msg ) );
   
            // Use malloc since wrapperAPI assumes all dso's are C libraries
            // it will use free to delete allocated memory.
            *searchStatus = ( CHAR* )malloc( sizeof( CHAR ) * ( error_len + 1 ) ); 
            strcpy( *searchStatus, error_msg );
            return 1;
         }
      }
   
      if( counter == 4 )
      {
         // This will guarantee that applySearch loop exits after 
         // 4th call to 'applySearch'.   
         output->fracRemaining = 0.0f;
         output->notFinished = false;         
      }
   
      ++counter;
   }
   else
   {
      // This will guarantee that applySearch loop exits after 
      // very first call to the function:
      output->fracRemaining = 0.0f;
      output->notFinished = false;
   }
   
   return 0;
}
   

//-------------------------------------------------------------------------------
//   
//: freeOutput routine.
//
// This method frees dynamically allocated memory for the outPut data structure.
//
//!param: CHAR** freeStatus - Error message if any
//!param: SearchOutput* output - Structure holding outPut and its metadata.   
//         
//!return: INT4 - Error code.
//   
INT4 freeOutput( CHAR** freeStatus, SearchOutput* output )
{
   ConvertData::cleanup( output->numOutput, output->result );
   
   delete[] output->result;
   output->result = 0;
   output->numOutput = 0;
   
   return 0;
}

   
//-------------------------------------------------------------------------------
//   
//: finalizeSearch routine.
//
// This method generates an exception. 
// It's primarily to test exceptions that appear as part of buffered message
// to mpiAPI.
//   
//!return: INT4 - Error code.
//         
INT4 finalizeSearch( CHAR** finalizeStatus )
{
   const CHAR* error_msg( "Test exception from liberror.so" );
   const size_t error_len( strlen( error_msg ) );
   
   // Use malloc since wrapperAPI assumes all dso's are C libraries
   // it will use free to delete allocated memory.
   *finalizeStatus = ( CHAR* )malloc( sizeof( CHAR ) * ( error_len + 1 ) ); 
   strcpy( *finalizeStatus, error_msg );
   return 1;   
}
   
