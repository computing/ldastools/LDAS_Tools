#include "LDASConfig.h"

// System Header Files
#include <stdexcept>   
#include <string>
   
// Local Header Files
#include "convertutil.hh"
   
using namespace std;
using namespace IODso;   
   

//-------------------------------------------------------------------------------
//   
//: Overloaded call operator.
//
// This method converts C inPut data structure into the C outPut data structure.
//
//!param: const inPut* const input - A pointer to the inPut data structure.
//!param: outPut** output - An address of the array of outPut data structures
//+       to be allocated.  
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error while converting the data.             
//   
void ConvertData::operator()( const inPut* const input, outPut** output )
{
   if( mNum == 0 || 
       input->sequences == 0 )
   {
      throw runtime_error( string( "No sequences available in the inPut." ) );
   }

   *output = new outPut[ mNum ];
   memset( *output, 0, sizeof( outPut ) * mNum );

   try
   {
      for( INT4 i = 0; i < mNum; ++i )
      {
         ( *output )[ i ].optional = new multiDimData[ 1 ];
   
         storeMultiDimData( ( *output )[ i ].optional, 
                            input->sequences[ i ] );
      }
   }
   catch(...)
   {
      cleanup( mNum, *output );
      delete[] *output;
      *output = 0;
   
      throw;
   }

   
   // Convert inPut state information to outPut
   // (attach all state information to the output[0] )
   if( input->states )
   {
      storeStateData( output[ 0 ], input->states );  
   }
   
   return;
}

   
//-------------------------------------------------------------------------------
//   
//: Cleanup dynamically allocated memory.
//
// This method frees all dynamically allocated data for the array of C outPut 
// data structures.
//
//!param: const UINT4 num - Number of elements in the array.   
//!param: outPut* result - An array of dynamically allocated C outPut data     
//+       structures.
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error while converting the data.             
//   
void ConvertData::cleanup( const UINT4 num, outPut* result )
{
   if( result != 0 )
   {
      stateVector* curr_node( 0 ), *next_node( 0 );
      dataBase* db_curr( 0 ), *db_next( 0 );   

      outPut* result_elem( 0 );

   
      for( UINT4 i = 0; i < num; ++i )
      {
         result_elem = &( result[ i ] );
   
   
         // Delete states
         curr_node = result_elem->states; 
      
         while( curr_node != 0 )
         {
            // There will be only one multiDimData structure per each node
            deleteMultiDimData( curr_node->store );         
            delete[] curr_node->store;
            curr_node->store = 0;
   
            next_node = curr_node->next;   
            delete curr_node;

            curr_node = next_node;
         }
    
         result_elem->states = 0;
   
       
         // Delete results: 
         db_curr = result_elem->results;
    
         while( db_curr != 0 )
         {
            delete[] db_curr->rowDimensions;
            db_curr->rowDimensions = 0;
   
            deleteDataArray( &( db_curr->rows ), db_curr->type );
   
            db_next = db_curr->next;
            delete db_curr;
   
            db_curr = db_next;
         }         
        
         result_elem->results = 0;
   
               
         // Delete optional
         if( result_elem->optional != 0 )
         {
            deleteMultiDimData( result_elem->optional );         
            delete[] result_elem->optional;
   
            result_elem->optional = 0; 
         }
      }
   }

   return;   
}
   
   
//-------------------------------------------------------------------------------
//   
//: Stores C multiDimData data structure.
//
// This method stores C multiDimData data structure.
//
//!param: multiDimData* pd - A pointer to the destination data structure.
//!param: multiDimData& ps - A reference to the source data structure.
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error while converting the data.             
//      
void ConvertData::storeMultiDimData( multiDimData* pd,
   const multiDimData& ps )   
{
    UINT4 numData( 1 );
    memset( pd, 0, sizeof( multiDimData ) );
   
    // Copy name and units
    memcpy( pd->name, ps.name, 
            sizeof( CHAR ) * maxMultiDimName );
    memcpy( pd->units, ps.units, 
            sizeof( CHAR ) * maxMultiDimUnits );   
    memcpy( pd->comment, ps.comment, 
            sizeof( CHAR ) * maxMultiDimComment );   

   
    // Copy space, type, numberDimensions
    pd->space = ps.space;
    pd->type  = ps.type;
    pd->numberDimensions = ps.numberDimensions;   
   
   
    // Copy range data
    storeInterval( pd->space, pd->range, ps.range );
   
    if( pd->numberDimensions )
    {
       pd->dimensions = new UINT4[ pd->numberDimensions ]; 
   
       // Copy array of dimensions
       memcpy( pd->dimensions, ps.dimensions, 
               sizeof( UINT4 ) * pd->numberDimensions );

   
       numData = getNDim( pd->numberDimensions,
                          pd->dimensions );
   
       // Copy array of data
       storeDataArray( &( pd->data ), pd->type, numData, ps.data );
    }   

   
    // Handle history 
    dcHistory* d_curr_node( 0 ),
              *d_prev_node( 0 );
    const dcHistory* s_curr_node( ps.history );   
    bool first_node( true );
   
    while( s_curr_node != 0 )
    {
       d_curr_node = new dcHistory;
       memset( d_curr_node, 0, sizeof( dcHistory ) );
   
       // Copy name and units
       memcpy( d_curr_node->name, s_curr_node->name, 
             ( maxHistoryName + maxHistoryUnits ) * sizeof( CHAR ) );
       d_curr_node->type = s_curr_node->type;
       d_curr_node->numberValues = s_curr_node->numberValues;   

       // These methods won't do anything if numberValues=0
       storeDataArray( &( d_curr_node->value ), 
                       d_curr_node->type,
                       d_curr_node->numberValues,
                       s_curr_node->value );
   
       if( first_node )
       {
          pd->history = d_curr_node;
          first_node = false;
       }
       else
       {
          d_curr_node->previous = d_prev_node;
          d_prev_node->next = d_curr_node;
       }
   
   
       d_prev_node = d_curr_node;
       s_curr_node = s_curr_node->next;
    }

    return;   
}

   
void ConvertData::storeStateData( outPut* const output, 
   const stateVector* const src_state )
{
   const stateVector* state( src_state );
   
   stateVector* curr_node( 0 );
   stateVector* prev_node( 0 );
   bool first_node( true );
   
   
   while( state )
   {
      curr_node = new stateVector;
      memset( curr_node, 0, sizeof( stateVector ) );
   
      // Get name of the state
      if( strlen( state->stateName ) )
      {
         memcpy( curr_node->stateName, state->stateName,
                 sizeof( CHAR ) * strlen( state->stateName ) );
      }
   
      if( first_node )
      {
         output->states = curr_node;
         first_node = false;
      }
      else
      {
         curr_node->previous = prev_node;
         prev_node->next = curr_node;
      }

      curr_node->store = new multiDimData[ 1 ];   
      storeMultiDimData( curr_node->store, *( state->store ) );   
   
      prev_node = curr_node;
   
      state = state->next;
   }

   return;   
}
   
   
//-------------------------------------------------------------------------------
//   
//: Stores C interval data structure.
//
// This method stores C interval data structure.
//   
//!param: const domain& d - An enum type specifying the domain.      
//!param: interval& pd - A reference to the destination interval structure
//+       being initialized.
//!param: const interval& ps - A reference to the source interval structure.   
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error while converting the data.             
//         
void ConvertData::storeInterval( const domain& d, interval& pd, const interval& ps )
{
   CHAR*** channelName( 0 );
   UINT4  numberChannels( 0 );
   const CHAR* s_channelName( 0 ); 
   
   switch( d )
   {
      case(noneD):
      {
         memcpy( &( pd.dNone ), &( ps.dNone ), sizeof( rawSequence ) );   
         storeDetector( &( pd.dNone.geometry ), ps.dNone.geometry,
                        pd.dNone.numberDetectors );
   
         numberChannels = pd.dNone.numberChannels;
         // If there're any channel names
         if( numberChannels )
         {
            channelName  = &( pd.dNone.channelName );
            s_channelName = ps.dNone.channelName[ 0 ];
         }
   
         break;
      }   
      case(timeD):
      {
         memcpy( &( pd.dTime ), &( ps.dTime ), sizeof( gpsTimeInterval ) );   
         storeDetector( &( pd.dTime.geometry ), ps.dTime.geometry,
                        pd.dTime.numberDetectors );
   
         numberChannels = pd.dTime.numberChannels;
         // If there're any channel names
         if( numberChannels )
         {
            channelName  = &( pd.dTime.channelName );
            s_channelName = ps.dTime.channelName[ 0 ];
         }
   
         break;
      }
      case(freqD):
      {
         memcpy( &( pd.dFreq ), &( ps.dFreq ), sizeof( frequencyInterval ) );
         storeDetector( &( pd.dFreq.geometry ), ps.dFreq.geometry,
                        pd.dFreq.numberDetectors );
   
         numberChannels = pd.dFreq.numberChannels;
         // If there're any channel names
         if( numberChannels )
         {   
            channelName  = &( pd.dFreq.channelName );   
            s_channelName = ps.dFreq.channelName[ 0 ];   
         }
   
         break;
      }   
      case(bothD):
      {
         memcpy( &( pd.dBoth ), &( ps.dBoth ), sizeof( timeFreqInterval ) );
         storeDetector( &( pd.dBoth.geometry ), ps.dBoth.geometry,
                        pd.dBoth.numberDetectors );
   
         numberChannels = pd.dBoth.numberChannels;   
         // If there're any channel names
         if( numberChannels )
         {      
            channelName  = &( pd.dBoth.channelName );   
            s_channelName = ps.dFreq.channelName[ 0 ];      
         }
         break;
      }   
      case(databaseD):
      {
         memcpy( &( pd.dDatabase ), &( ps.dDatabase ), 
                 sizeof( databaseInterval ) );
         break;
      }      
      default:   
      {
         throw runtime_error( string( "Undefined domain (ConvertData:storeInterval)." ) );
      }      
   }

   // All domains except databaseInterval
   if( numberChannels )
   {
      // Allocate array for pointers to channels
      *channelName = new CHAR*[ numberChannels ];   
      memset( *channelName, 0, sizeof( CHAR* ) * numberChannels );
   
      // Array of channel offsets into the buffer
      UINT4* channel_len = new UINT4[ numberChannels ];
      memset( channel_len, 0, sizeof( UINT4 ) * numberChannels );
      // channel_len[ 0 ] is always '0'
      UINT4 channelSize( strlen( s_channelName ) + 1 ); 
   
      // Get total size of source channels and position of each channel
      // into the buffer
      UINT4* channel( channel_len );
      for( UINT4 index = 1; index < numberChannels; ++index )
      {
         ++channel;   
         *channel = channelSize;
         channelSize += strlen( &( s_channelName[ *channel ] ) ) + 1;
      }
   
      try
      {
         // Allocate the memory for the whole buffer
         ( *channelName )[ 0 ] = new CHAR[ channelSize ];
         memcpy( ( *channelName )[ 0 ], s_channelName,
                 sizeof( CHAR ) * channelSize );
         channel = channel_len;
      
#ifdef DEBUG_CHANNEL_NAME   
         cout << ": Channel( slave )[0]: " << ( *channelName )[ 0 ] << endl;
#endif      
         // Set pointers to channels( 0 channel points to the buffer itself ),
         for( UINT4 index = 1; index < numberChannels; ++index )
         {
            ++channel;
            ( *channelName )[ index ] = ( *channelName )[ 0 ] + ( *channel );
   
#ifdef DEBUG_CHANNEL_NAME   
            cout << ": Channel( slave )[" << index << "]: " << ( *channelName )[ index ] << endl;
#endif   
         }
      }
      catch(...)
      {
         delete[] channel_len;
         channel_len = 0;
   
         delete[] channelName;         
         channelName = 0;
   
         throw;
      }
   
      delete[] channel_len;
      channel_len = 0;
   }
   
   return;
}   

   
//-----------------------------------------------------------------------------
//      
//: Allocates the memory and copies the data for the array of raw data.
//
//!param: dataPointer* data - A pointer to the data array.
//!param: const datatype& type - A reference to the data type of the array data.
//!param: const UINT4 num - Number of elements in the array.
//!param: const dataPointer& source_data - A reference to the source data array.   
//   
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: runtime_error - Error storing array data.
//   
void ConvertData::storeDataArray( dataPointer* data, const datatype& type,
   const UINT4 num, const dataPointer& source_data )
{
   if( num == 0 )
   {
      return;
   }
   
   
   switch( type )
   {
      case(boolean_lu):
      {
         ( *data ).boolean = new BOOLEAN[ num ];
         memcpy( ( *data ).boolean, source_data.boolean, num * sizeof( BOOLEAN ) );   
         break;
      }
      case(char_s):
      {
         ( *data ).chars = new CHAR[ num ];
         memcpy( ( *data ).chars, source_data.chars, num * sizeof( CHAR ) );            
         break;
      }   
      case(char_u):
      {
         ( *data ).charu = new UCHAR[ num ];
         memcpy( ( *data ).charu, source_data.charu, num * sizeof( UCHAR ) );               
         break;
      }   
      case(int_2s):
      {
         ( *data ).int2s = new INT2[ num ];
         memcpy( ( *data ).int2s, source_data.int2s, num * sizeof( INT2 ) );               
         break;
      }   
      case(int_2u):
      {
         ( *data ).int2u = new UINT2[ num ];
         memcpy( ( *data ).int2u, source_data.int2u, num * sizeof( UINT2 ) );                  
         break;
      }   
      case(int_4s):
      {
         ( *data ).int4s = new INT4[ num ];
         memcpy( ( *data ).int4s, source_data.int4s, num * sizeof( INT4 ) );                  
         break;
      }   
      case(int_4u):
      {
         ( *data ).int4u = new UINT4[ num ];
         memcpy( ( *data ).int4u, source_data.int4u, num * sizeof( UINT4 ) );                     
         break;
      }      
      case(int_8s):
      {
         ( *data ).int8s = new INT8[ num ];
         memcpy( ( *data ).int8s, source_data.int8s, num * sizeof( INT8 ) );                     
         break;
      }      
      case(int_8u):
      {
         ( *data ).int8u = new UINT8[ num ];
         memcpy( ( *data ).int8u, source_data.int8u, num * sizeof( UINT8 ) );                     
         break;
      }      
      case(real_4):
      {
         ( *data ).real4 = new REAL4[ num ];
         memcpy( ( *data ).real4, source_data.real4, num * sizeof( REAL4 ) );                     
         break;
      }      
      case(real_8):
      {
         ( *data ).real8 = new REAL8[ num ];
         memcpy( ( *data ).real8, source_data.real8, num * sizeof( REAL8 ) );                        
         break;
      }      
      case(complex_8):
      {
         ( *data ).complex8 = new COMPLEX8[ num ];
         memcpy( ( *data ).complex8, source_data.complex8, num * sizeof( COMPLEX8 ) );   
         break;
      }      
      case(complex_16):
      {
         ( *data ).complex16 = new COMPLEX16[ num ];
         memcpy( ( *data ).complex16, source_data.complex16, num * sizeof( COMPLEX16 ) );      
         break;
      }         
      case(char_s_ptr):
      {
         ( *data ).chars = new CHAR[ num ];
         memcpy( ( *data ).chars, source_data.chars, num * sizeof( CHAR ) );               
         break;
      }   
      case(char_u_ptr):
      {
         ( *data ).charu = new UCHAR[ num ];
         memcpy( ( *data ).charu, source_data.charu, num * sizeof( UCHAR ) );               
         break;
      }      
      default:
      {
         throw runtime_error( string( "Undefined datatype." ) );
         break;
      }
   }   
   
   
   return;   
}
   
   
//-----------------------------------------------------------------------------
//      
//: Allocates the memory and copies the data for the array of C detGeom
//: data structures.
//
//!param: detGeom** pd - A pointer to the destination array of C         
//+       detGeom data structures.
//!param: const detGeom* ps - A source array of C detGeom data structures.
//!param: const UINT4 num - Number of structures in the source and destination 
//+       arrays.
//
//!return: Nothing.
//   
void ConvertData::storeDetector( detGeom** pd, const detGeom* ps,
   const UINT4 num )
{
   if( !num )
   {
      return;
   }
   
   *pd = new detGeom[ num ];
   memcpy( *pd, ps, sizeof( detGeom ) * num );

#ifdef DEBUG_DETECTOR_GEOM   
   for( UINT4 i = 0; i < num; ++i )
   {
      cout << "Slave: detGeom[" << i << "].name=\"" << ( *pd )[ i ].name
           << "\""; 
      cout << " detGeom[" << i << "].longitude="
           << ( *pd )[ i ].longitude << endl;
   }
#endif
   
   return;
}
   
   
//-----------------------------------------------------------------------------
//   
//: Gets the total number of elements.
//   
// This method calculates total number of elements in multidimensional array.
//
//!param: const UINT4 ndim - A number of dimensions in the array.
//!param: const UINT4* dim - An array of dimensions.      
//
//!return: const UINT4 - Total number of array elements.
//   
const UINT4 ConvertData::getNDim( const UINT4 ndim, const UINT4* dim )
{
   if( ndim == 0 )
   {
      return 0;
   }

   
   UINT4 num( 1 );
   const UINT4* dim_end( dim + ndim - 1 );
   const UINT4* dim_elem( dim );

   
   while( dim_elem <= dim_end )
   {
      num *= *dim_elem;
      ++dim_elem;
   }

   
   return num;
}
   

//------------------------------------------------------------------------------
//
//: Cleanup utility for array of multiDimData structures.
//      
// This method frees all dynamically allocated memory in the array of C 
// multiDimData data structures.
//    
//!param: multiDimData* pe - A pointer to the array of "C" multiDimData        
//+       structures.
//!param: const UINT4 num - Number of elements in the array.
//!param: const MPI::Intracomm& comm - A reference to the MPI                  
//+       intracommunicator whithin which datatype is defined. 
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.         
//
//!return: Nothing.
//   
//!exc: Undefined datatype - Undefined datatype was specified.
//   
void ConvertData::deleteMultiDimData( multiDimData* pe )
{
   if( pe == 0 )
   {
      return;
   }

   dcHistory* curr_node( 0 );
   dcHistory* next_node( 0 );

   
   // Delete channel information in interval
   deleteInterval( pe->space, pe->range );        
   
   if( pe->numberDimensions )
   {
      delete[] pe->dimensions;
      pe->dimensions = 0;
   
      deleteDataArray( &( pe->data ), pe->type );   
   }

   // Delete history data
   curr_node = pe->history;
   
   while( curr_node != 0 )
   {
      deleteDataArray( &curr_node->value, curr_node->type );
      next_node = curr_node->next;
   
      delete curr_node;
      curr_node = next_node;        
   }
   
   pe->history = 0;
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Frees dynamically allocated memory for the domain interval
//: data structure.
//
//!param: const domain& d - An enum type specifying the domain.      
//!param: interval& pd - A reference to the interval structure.
//
//!return: Nothing.
//
//!exc: runtime_error( Undefined domain. ) - Undefined domain was specified 
//+     for the C interval union.   
//      
void ConvertData::deleteInterval( const domain& d, interval& pd )
{
   switch( d )
   {
      case(noneD):
      {
         if( pd.dNone.numberChannels )
         {
            delete[] pd.dNone.channelName[ 0 ];
            pd.dNone.channelName[ 0 ] = 0;
   
            delete[] pd.dNone.channelName;
            pd.dNone.channelName = 0;   
         }

         delete[] pd.dNone.geometry;
         pd.dNone.geometry = 0;
   
         break;
      }   
      case(timeD):
      {
         if( pd.dTime.numberChannels )
         {
            delete[] pd.dTime.channelName[ 0 ];
            pd.dTime.channelName[ 0 ] = 0;
   
            delete[] pd.dTime.channelName;
            pd.dTime.channelName = 0;   
         }

         delete[] pd.dTime.geometry;
         pd.dTime.geometry = 0;
   
         break;
      }
      case(freqD):
      {
         if( pd.dFreq.numberChannels )
         {   
            delete[] pd.dFreq.channelName[ 0 ];
            pd.dFreq.channelName[ 0 ] = 0;
   
            delete[] pd.dFreq.channelName;
            pd.dFreq.channelName = 0;   
         }
   
         delete[] pd.dFreq.geometry;
         pd.dFreq.geometry = 0;
   
         break;
      }   
      case(bothD):
      {
         if( pd.dBoth.numberChannels )
         {   
            delete[] pd.dBoth.channelName[ 0 ];
            pd.dBoth.channelName[ 0 ] = 0;
   
            delete[] pd.dBoth.channelName;
            pd.dBoth.channelName = 0;   
         }
   
         delete[] pd.dBoth.geometry;
         pd.dBoth.geometry = 0;
   
         break;
      }   
      case(databaseD):
      {
         break;
      }      
      default:   
      {
         throw runtime_error( string( "Undefined domain (ConvertData:deleteInterval)." ) );
      }      
   }

   return;
}
   

//-----------------------------------------------------------------------------
//   
//: Delete memory for data array.
//
// This method deletes dynamically allocated memory for the data array.
//   
//!param: dataPointer* data - A pointer to the data array.
//!param: const datatype& type - A reference to the data type of the array    
//+       data.   
//
//!exc: runtime_error( Undefined datatype. ) - Illegal datatype was specified.
//   
void ConvertData::deleteDataArray( dataPointer* data, const datatype& type )
{
   if( data == 0 )
   {
      return;
   }

   
   switch( type )
   {
      case(boolean_lu):
      {
         delete[] data->boolean;
         data->boolean = 0;
         break;
      }
      case(char_s):
      {
         delete[] data->chars;
         data->chars = 0;
         break;
      }   
      case(char_u):
      {
         delete[] data->charu;
         data->charu = 0;
         break;
      }   
      case(int_2s):
      {
         delete[] data->int2s;
         data->int2s = 0;
         break;
      }   
      case(int_2u):
      {
         delete[] data->int2u;
         data->int2u = 0;   
         break;
      }   
      case(int_4s):
      {
         delete[] data->int4s;
         data->int4s = 0;   
         break;
      }   
      case(int_4u):
      {
         delete[] data->int4u;
         data->int4u = 0;   
         break;
      }      
      case(int_8s):
      {
         delete[] data->int8s;
         data->int8s = 0;
         break;
      }      
      case(int_8u):
      {
         delete[] data->int8u;
         data->int8u = 0;   
         break;
      }      
      case(real_4):
      {
         delete[] data->real4;
         data->real4 = 0;   
         break;
      }      
      case(real_8):
      {
         delete[] data->real8;
         data->real8 = 0;
         break;
      }      
      case(complex_8):
      {
         delete[] data->complex8;
         data->complex8 = 0;
         break;
      }      
      case(complex_16):
      {
         delete[] data->complex16;
         data->complex16 = 0;
         break;
      }         
      case(char_s_ptr):
      {
         delete[] data->chars;
         data->chars = 0;
         break;
      }   
      case(char_u_ptr):
      {
         delete[] data->charu;
         data->charu = 0;
         break;
      }      
      default:
      {
         throw runtime_error( string( "Undefined datatype." ) );
         break;
      }
   }   
   
   
   return;
}
   
