#include "LDASConfig.h"

// System Header Files
#include <stdexcept>   
#include <algorithm>   
#include <sstream>   
#include <cstring>   
   
// Local Header Files
#include "stateutil.hh"
#include "convertutil.hh"   
   
using namespace std;
using namespace IODso;   
   

//: Static data members initialization   
const UINT4 StateData::mNumOutput( 1 );   
const UINT4 StateData::mNumStates( complex_16 + 1 );      
const UINT4 StateData::mNumData( 11 );   
const UINT4 StateData::mNumDimensions( 1 );   
   
static const BOOLEAN   BOOLEAN_Unit( 1 );
static const CHAR      CHAR_Unit( 2 );   
static const UCHAR     UCHAR_Unit( 3 );   
static const INT2      INT2_Unit( 4 );
static const UINT2     UINT2_Unit( 5 );
static const INT4      INT4_Unit( 6 );   
static const UINT4     UINT4_Unit( 7 );      
static const INT8      INT8_Unit( 8 );      
static const UINT8     UINT8_Unit( 9 );      
static const REAL4     REAL4_Unit( 10.5 );      
static const REAL8     REAL8_Unit( 11.5 );         
static const COMPLEX8  COMPLEX8_Unit = { 12.5, 13.5 };         
static const COMPLEX16 COMPLEX16_Unit = { 14.5, 15.5 };            

   
//------------------------------------------------------------------------------
//
//: Initializes DataTypeHash unordered_map.
//
// This method initializes the datatype unordered_map, where the value is a name
// used for datatype, and the value is an enumerated datatype representing the 
// type.   
//   
//!return: const TypeHash - unordered_map.
//   
//!exc: bad_alloc - Memory allocation failed.
//  
const StateData::TypeHash StateData::initTypeHash()
{
   TypeHash h;
   
   h.insert( hashValueType( "bool_type", boolean_lu ) );
   h.insert( hashValueType( "char_type", char_s ) );
   h.insert( hashValueType( "uchar_type", char_u ) );   
   h.insert( hashValueType( "int2_type", int_2s ) );   
   h.insert( hashValueType( "int2u_type", int_2u ) );   
   h.insert( hashValueType( "int4_type", int_4s ) );      
   h.insert( hashValueType( "int4u_type", int_4u ) );      
   h.insert( hashValueType( "int8_type", int_8s ) );      
   h.insert( hashValueType( "int8u_type", int_8u ) );         
   h.insert( hashValueType( "real4_type", real_4 ) );         
   h.insert( hashValueType( "real8_type", real_8 ) );            
   h.insert( hashValueType( "complex8_type", complex_8 ) );            
   h.insert( hashValueType( "complex16_type", complex_16 ) );               

   
   return h;
}

   
const StateData::TypeHash StateData::mTypes( initTypeHash() );
   
   
//-------------------------------------------------------------------------------
//   
//: Overloaded call operator.
//
// This operator handles stateVector generation or data validation.   
// This method populates stateVector data for outPut with template data if 
// inPut structure doesn't have one. It will compare stateVector of the inPut data
// (if it's present) to the template stateVector.
//
//!param: const inPut* const input - A pointer to the inPut data structure.
//!param: outPut** output - An address of the array of outPut data structures
//+       to be allocated.  
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error while converting the data.             
//   
void StateData::operator()( const inPut* const input, outPut** output )
{
   *output = new outPut[ mNumOutput ];
   memset( *output, 0, sizeof( outPut ) * mNumOutput );
   
   try
   {
      // No stateVector data is available
      if( input->states == 0 )
      {
         createState( *output );
      }
      else // inPut has stateVector
      {
         validateState( input->states, *output );
      }
   }
   catch(...)
   {
      ConvertData::cleanup( mNumOutput, *output );
      throw;
   }
   
   return;
}


//-------------------------------------------------------------------------------
//   
//: Create stateVector data.
//
// This method creates stateVector linked list for the outPut data structure.
//
//!param: const outPut* const dest - A pointer to the outPut data structure.
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error creating the data.                
//!exc: bad_alloc - Memory allocation failed.   
//   
void StateData::createState( outPut* const dest )
{
   stateVector* curr_node( 0 ), *prev_node( 0 );
   size_t s_size( 0 );
   
   // Hash iterator
   TypeHash::const_iterator type_iter( mTypes.begin() );
   
   
   for( UINT4 i = 0; i < mNumStates; ++i, ++type_iter )
   {
      curr_node = new stateVector;
      memset( curr_node, 0, sizeof( stateVector ) );
   
      if( dest->states == 0 )
      {
         dest->states = curr_node;
      }
      else
      {
         curr_node->previous = prev_node;
         prev_node->next = curr_node;
      }

      prev_node = curr_node;
   
   
      // Set the name:
      if( type_iter->first.length( ) > 0 )
      {
	 s_size = min( static_cast< INT4 >( type_iter->first.length( ) + 1 ), 
                       maxStateName - 1 );   
         memcpy( curr_node->stateName, type_iter->first.c_str( ), 
                 sizeof( CHAR ) * s_size );
      }   
   
      
      // Allocate multiDimData 
      createMultiDimData( curr_node, type_iter->second,
			  type_iter->first.c_str( ) );  
   }

   return;
}

   
//-------------------------------------------------------------------------------
//   
//: Create multiDimData data.
//
// This method creates multiDimData structure for the particular state node.
//
//!param: stateVector* const state - A pointer to the outPut's state node.
//!param: const datatype& type - Type of the data to be generated.
//!param: const CHAR* const name - String used for data's name attribute.
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error creating the data.                
//!exc: bad_alloc - Memory allocation failed.   
//      
void StateData::createMultiDimData( stateVector* const state,
   const datatype& type, const CHAR* const name )
{
   static const REAL8 step_size( 0.5f );
   static const UINT4 stop_secs( 5 );
   state->store = new multiDimData;
   
   // Initialize allocated memory to zero
   memset( state->store, 0, sizeof( multiDimData ) );       
  
   // Set metadata:   
   size_t s_size( min( static_cast< INT4 >( strlen( name ) + 1 ), 
                       maxMultiDimName - 1 ) );   
   memcpy( state->store->name, name, sizeof( CHAR ) * s_size );   
   
   state->store->space = timeD;
   state->store->range.dTime.startSec = 0;
   state->store->range.dTime.stopSec = stop_secs;   
   state->store->range.dTime.timeStepSize = step_size;
   state->store->type  = type;   
   state->store->numberDimensions = mNumDimensions;   

   state->store->dimensions = new UINT4[ state->store->numberDimensions ];
   state->store->dimensions[ 0 ] = mNumData;
   
   createArrayData( &( state->store->data ), type );
   
   return;
}
   

//-------------------------------------------------------------------------------
//   
//: Create array data.
//
// This method creates array data.
//
//!param: dataPointer* data - A pointer to the data union.
//!param: const datatype& type - Type of the data to be generated.
//   
//!return: Nothing.
//  
//!exc: runtime_error - Error creating the data.                
//!exc: bad_alloc - Memory allocation failed.   
//         
void StateData::createArrayData( dataPointer* data, const datatype& type )
{
   switch( type )
   {
      case(boolean_lu):
      {
         ( *data ).boolean = new BOOLEAN[ mNumData ];
         BOOLEAN* buf( ( *data ).boolean );
         fill( buf, buf + mNumData, BOOLEAN_Unit );      
   
         break;
      }
      case(char_s):
      {
         ( *data ).chars = new CHAR[ mNumData ];
         CHAR* buf( ( *data ).chars );
         fill( buf, buf + mNumData, CHAR_Unit );      
   
         break;
      }   
      case(char_u):
      {
         ( *data ).charu = new UCHAR[ mNumData ];
         UCHAR* buf( ( *data ).charu );
         fill( buf, buf + mNumData, UCHAR_Unit );      

         break;
      }   
      case(int_2s):
      {
         ( *data ).int2s = new INT2[ mNumData ];
         INT2* buf( ( *data ).int2s );
         fill( buf, buf + mNumData, INT2_Unit );      
   
         break;
      }   
      case(int_2u):
      {
         ( *data ).int2u = new UINT2[ mNumData ];
         UINT2* buf( ( *data ).int2u );
         fill( buf, buf + mNumData, UINT2_Unit );         

         break;
      }   
      case(int_4s):
      {
         ( *data ).int4s = new INT4[ mNumData ];
         INT4* buf( ( *data ).int4s );
         fill( buf, buf + mNumData, INT4_Unit );         
   
         break;
      }   
      case(int_4u):
      {
         ( *data ).int4u = new UINT4[ mNumData ];
         UINT4* buf( ( *data ).int4u );
         fill( buf, buf + mNumData, UINT4_Unit );            

         break;
      }      
      case(int_8s):
      {
         ( *data ).int8s = new INT8[ mNumData ];
         INT8* buf( ( *data ).int8s );
         fill( buf, buf + mNumData, INT8_Unit );            
   
         break;
      }      
      case(int_8u):
      {
         ( *data ).int8u = new UINT8[ mNumData ];
         UINT8* buf( ( *data ).int8u );
         fill( buf, buf + mNumData, UINT8_Unit );               
   
         break;
      }      
      case(real_4):
      {
         ( *data ).real4 = new REAL4[ mNumData ];
         REAL4* buf( ( *data ).real4 );
         fill( buf, buf + mNumData, REAL4_Unit );   
   
         break;
      }      
      case(real_8):
      {
         ( *data ).real8 = new REAL8[ mNumData ];
         REAL8* buf( ( *data ).real8 );
         fill( buf, buf + mNumData, REAL8_Unit );      

         break;
      }      
      case(complex_8):
      {
         ( *data ).complex8 = new COMPLEX8[ mNumData ];
         COMPLEX8* buf( ( *data ).complex8 );
         fill( buf, buf + mNumData, COMPLEX8_Unit );
   
         break;
      }      
      case(complex_16):
      {
         ( *data ).complex16 = new COMPLEX16[ mNumData ];
         COMPLEX16* buf( ( *data ).complex16 );
         fill( buf, buf + mNumData, COMPLEX16_Unit );   
   
         break;
      }         
      default:
      {
         throw runtime_error( "Undefined datatype." );
         break;
      }
   }   
   
   return;
}

   
//-------------------------------------------------------------------------------
//     
//: Second pass of the data - validation of stateVector linked list.
// 
// This method validate the correctness of the passed to the method stateVector
// data. It compares to the "template" data the same shared object is using
// to create the state data.   
//   
//!param: const stateVector* const state - A pointer to the input stateVector
//+       linked list to validate.
//!param: outPut* const output - A pointer to the outPut data structure if any
//+       is planned to generate (not really necessary).   
//
//!return: Nothing.
//   
void StateData::validateState( const stateVector* const state,
   outPut* const output )
{
   const stateVector* curr_node( state );
   size_t counter( 0 );
   
   while( curr_node )
   {
      curr_node = curr_node->next;
      ++counter;
   }
   
   if( counter != mNumStates )
   {
      ostringstream msg;
      msg << "Number of states doesn't match expected number: "
          << counter << " (current) vs. " << mNumStates << " (expected)";
      throw runtime_error( msg.str() );
   }
   
   
   curr_node = state;
   const CHAR* state_name( 0 );
   TypeHash::const_iterator type_iter,
                            end_type_iter( mTypes.end() );   
   
   while( curr_node )
   {
      // Look up state name in the hash map
      state_name = curr_node->stateName;
      type_iter = mTypes.find( state_name );
   
      if( type_iter == end_type_iter )
      {
         string msg( "Undefined state specified: \"" );
         msg += state_name;
         msg += '\"';
         throw runtime_error( msg );
      }
   
      if( curr_node->store == 0 )
      {
         string msg( "State \"" );
         msg += state_name;
         msg += "\" is missing multiDimData.";
         throw runtime_error( msg );
      }
   
      validateArrayData( curr_node->store, state_name );
      
      curr_node = curr_node->next;
   }
   
   return;
}   

   
//-------------------------------------------------------------------------------
//     
//: Validate correctness of the actual data.
//    
//!param: const multiDimData* const store - A pointer to the data structure
//+       storing the data.
//!param: const CHAR* const name - Name of the state   
//
//!return: Nothing.
//   
void StateData::validateArrayData( const multiDimData* const store,
   const CHAR* const name )
{
   if( store->numberDimensions != mNumDimensions )
   {
      ostringstream s;
      s << "Number of data dimensions doesn't match expected number: "
        << store->numberDimensions << " (current) " 
        << mNumDimensions << " (expected).";
      throw runtime_error( s.str() );
   }
   
   if( store->dimensions[ 0 ] != mNumData )
   {
      ostringstream s;
      s << "Number of data elements doesn't match expected number: "
        << store->dimensions[ 0 ] << " (current) " 
        << mNumData << " (expected).";
      throw runtime_error( s.str() );
   }
   
   
   switch( store->type )
   {
      case(boolean_lu):
      {
         BOOLEAN* buf( new BOOLEAN[ mNumData ] );
         fill( buf, buf + mNumData, BOOLEAN_Unit );      

         if( memcmp( buf, store->data.boolean, mNumData * sizeof( BOOLEAN ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;
         break;
      }
      case(char_s):
      {
         CHAR* buf( new CHAR[ mNumData ] );
         fill( buf, buf + mNumData, CHAR_Unit );      
   
         if( memcmp( buf, store->data.chars, mNumData * sizeof( CHAR ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;   
         break;
      }   
      case(char_u):
      {
         UCHAR* buf( new UCHAR[ mNumData ] );
         fill( buf, buf + mNumData, UCHAR_Unit );      
   
         if( memcmp( buf, store->data.charu, mNumData * sizeof( UCHAR ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;   

         break;
      }   
      case(int_2s):
      {
         INT2* buf( new INT2[ mNumData ] );
         fill( buf, buf + mNumData, INT2_Unit );      
   
         if( memcmp( buf, store->data.int2s, mNumData * sizeof( INT2 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;   
   
         break;
      }   
      case(int_2u):
      {
         UINT2* buf( new UINT2[ mNumData ] );
         fill( buf, buf + mNumData, UINT2_Unit );         
   
         if( memcmp( buf, store->data.int2u, mNumData * sizeof( UINT2 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;   

         break;
      }   
      case(int_4s):
      {
         INT4* buf( new INT4[ mNumData ] );
         fill( buf, buf + mNumData, INT4_Unit );         
   
         if( memcmp( buf, store->data.int4s, mNumData * sizeof( INT4 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;      
   
         break;
      }   
      case(int_4u):
      {
         UINT4* buf( new UINT4[ mNumData ] );
         fill( buf, buf + mNumData, UINT4_Unit );            

         if( memcmp( buf, store->data.int4u, mNumData * sizeof( UINT4 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;         
   
         break;
      }      
      case(int_8s):
      {
         INT8* buf( new INT8[ mNumData ] );
         fill( buf, buf + mNumData, INT8_Unit );            
   
         if( memcmp( buf, store->data.int8s, mNumData * sizeof( INT8 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;         
   
         break;
      }      
      case(int_8u):
      {
         UINT8* buf( new UINT8[ mNumData ] );
         fill( buf, buf + mNumData, UINT8_Unit );               
   
         if( memcmp( buf, store->data.int8u, mNumData * sizeof( UINT8 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;            
   
         break;
      }      
      case(real_4):
      {
         REAL4* buf( new REAL4[ mNumData ] );
         fill( buf, buf + mNumData, REAL4_Unit );   
   
         if( memcmp( buf, store->data.real4, mNumData * sizeof( REAL4 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;            
   
         break;
      }      
      case(real_8):
      {
         REAL8* buf( new REAL8[ mNumData ] );
         fill( buf, buf + mNumData, REAL8_Unit );      

         if( memcmp( buf, store->data.real8, mNumData * sizeof( REAL8 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;               
   
         break;
      }      
      case(complex_8):
      {
         COMPLEX8* buf( new COMPLEX8[ mNumData ] );
         fill( buf, buf + mNumData, COMPLEX8_Unit );
   
         if( memcmp( buf, store->data.complex8, mNumData * sizeof( COMPLEX8 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;               
   
         break;
      }      
      case(complex_16):
      {
         COMPLEX16* buf( new COMPLEX16[ mNumData ] );
         fill( buf, buf + mNumData, COMPLEX16_Unit );   
   
         if( memcmp( buf, store->data.complex16, mNumData * sizeof( COMPLEX16 ) ) )
         {
            delete[] buf;
            buf = 0;
           
            string msg( "Unexpected value for array data: " );
            msg += name;
            throw runtime_error( msg );
         }
   
         delete[] buf;
         buf = 0;               
   
         break;
      }         
      default:
      {
         throw runtime_error( "Undefined datatype." );
         break;
      }
   }
   
   return;
}
