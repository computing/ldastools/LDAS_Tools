## ********************************************************
## 
## LDASwrapper.rsc
##
## This is the wrapperAPI specific resource file. It contains
## resource information which is only used by the wrapperAPI.
##
## Comments are preceeded by '#' sign.
##   
## ********************************************************

   
# All these arguments (except for debug flags) if appear on 
# a command line for the wrapperAPI will overwrite the values
# from this resource file.   
   
# host name and port number to talk to mpiAPI on
mpiAPI     ( beowulf, 11000 )
   
# host name and port number to send output data to   
resultAPI  ( metaserver, 10021 )   
   
# value: floating point value
realTimeRatio      1.0
   
# value: TRUE or FALSE  
doLoadBalance      FALSE
   
# process responsible for data distribution   
# value: WRAPPER or SEARCHMASTER   
dataDistributor    WRAPPER
   
# how often to build MPI datatype to represent outPut
# structure.   
# value: ONCE or ALWAYS   
communicateOutput  ALWAYS
   
# value: floating point number    
memoryUsageLimit   1.0  
   
   
## ********************************************************   
## Communication related variables
## ********************************************************
   
# number of connection attempts to the resultAPI( in case of 
# 'connection timed out' exception )
# value: integer number
# default: 1
resultAPI_connection_attempt      3
   
# number of microseconds to delay between connection retries to
# the resultAPI( in case of 'connection timed out' exception )
# value: integer number
# default: 250000 microseconds     
resultAPI_connection_delay        250000
   
# number of seconds to delay between communications to the resultAPI.
# value: integer number
# default: 0 seconds 
# maximum allowed: 300 seconds   
send_ilwd_output_delay            2
   
# number of seconds to delay between communications to the mpiAPI.
# ( to prevent dso's from talking to mpiAPI too frequently )
# value: integer number
# default: 0 seconds 
# maximum allowed: 5 seconds   
mpiAPI_connection_delay           2   
   
# number of seconds to poll for the mpiAPI response message
# ( to allow mpiAPI to handle large beowulf clusters )
# value: integer number
# default: 60 seconds 
# maximum allowed: 600 seconds   
mpiAPI_response_timeout           60

# flag to synchronize mpiApi responses with "continue"
# instructions to the searchMaster
# value: TRUE or FALSE   
# default: FALSE      
synchronize_mpiApi_dsoMaster      FALSE
   
   
## ********************************************************   
## Debug flags: one of TRUE | FALSE
## ********************************************************
   
# default: TRUE   
enable_mpiAPI             TRUE

# When turning this flag on, make sure to set 'run_code'
# that is appropriate for the system (LDAS-DEV,LDAS-TEST,etc.).    
# default: FALSE   
enable_dump_input         FALSE
   
# default: TRUE
enable_resultAPI          TRUE
 
# default: TRUE   
enable_load_dso           TRUE
   
# default: FALSE   
enable_ldas_trace         FALSE
   
# default: FALSE   
enable_xmpi_trace         FALSE   
   
# default: FALSE   
enable_timeout_trace      FALSE
   
# default: FALSE
enable_dump_args          FALSE

# Abriviations used in timing information:   
#    IS = initSearch
#    CD = conditionData
#    AS = applySearch
#    FO = freeOutput
#    FS = finalizeSearch  
# value: TRUE or FALSE[ TRUE ]   
enable_time_info          TRUE
   

## ********************************************************   
## These values are needed only if enable_dump_input_data
## is set to TRUE or enable_resultAPI is set to false.
##   
## ILWD format elements will be written to the 
## $dump_data_directory$/$run_code$_N/$run_code$XXX directory,
## where XXX is a jobID,
## N = jobID / 10000   
## ********************************************************   
   
# default: /ldas_outgoing/jobs   
dump_data_directory     /ldas_outgoing/jobs
   
# job naming convention
# (specific to the system, such as LDAS-WA, LDAS-LA, LDAS-DEV, etc.)   
run_code                LDAS-CVS
       
   
## ********************************************************   
## Debug flags: the value is number of outPut structures
## to simulate   
## ********************************************************   
   
# ATTN: enabling enable_dummy_output will raise SIGSEGV 
#       in finalizeSearch if dso is generating any
#       output.   
# default: 0
enable_dummy_output      0

# default: 0  
enable_dummy_state       0
   
# default: 0
enable_dummy_mdd         0
   
   
## ********************************************************   
## Debug flags: the value is amount of seconds to sleep
## ********************************************************   
   
# default: 0 seconds
enable_sleep_before_dso  0
   
# default: 0 seconds   
enable_sleep_after_dso   0
   
   
   
