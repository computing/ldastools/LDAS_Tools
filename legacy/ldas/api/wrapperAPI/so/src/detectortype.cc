#include "LDASConfig.h"

// System Header Files
#include <string>   
#include <algorithm>
#include <memory>   
   
// ILwd Header Files
#include <ilwd/ldascontainer.hh>   
#include <ilwd/ldasarray.hh>   
using ILwd::LdasArrayBase; 
using ILwd::LdasArray;   
using ILwd::ID_INT_4U;      
using ILwd::ID_REAL_8;
using ILwd::ID_ILWD;
   
// Local Header Files   
#include "detectortype.hh"
#include "ldaserror.hh"
#include "mpilaltypes.hh"   
#include "initvars.hh"   

using namespace std;   
   
using ILwd::LdasContainer;      
using ILwd::LdasElement;   

   
const string DetectorType::mILwdName( "detectProc" );   
const size_t DetectorType::mILwdNamePosition( 1 );      
const CHAR* DetectorType::mOutterContainerName( 
   ":detectProc:Container(Detector):Frame" );   
const CHAR* DetectorType::mContainerName( 
   ":detectProc:Detector:Frame" );      
const CHAR* DetectorType::mPrefixILwdName( "prefix" );   
const CHAR* DetectorType::mLongitudeILwdName( "longitude" );
const CHAR* DetectorType::mLatitudeILwdName( "latitude" );   
const CHAR* DetectorType::mElevationILwdName( "elevation" );   
const CHAR* DetectorType::mArmXazimuthILwdName( "armXazimuth" );   
const CHAR* DetectorType::mArmYazimuthILwdName( "armYazimuth" );      
const CHAR* DetectorType::mArmXaltitudeILwdName( "armXaltitude" );      
const CHAR* DetectorType::mArmYaltitudeILwdName( "armYaltitude" );            
const CHAR* DetectorType::mLocalTimeILwdName( "localTime" );   
   
const INT4 DetectorType::mTypeNum( 10 );   
MPI::Datatype DetectorType::mMpiType( MPI::DATATYPE_NULL );
   
static const INT4 prefix_dim( maxDetectorPrefix - 1 );
   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype that represents array of
// C detGeom structures. When called by master it initializes the structure
// with the data of passed to it ILWD element representing the structure.
// All nodes using detector C union for communication must create this MPI
// datatype.  
// <p>This constructor will be used if structure is part of inPut data 
// structure.   
//   
//!param: const LdasElement* pe - A pointer to the ILWD representation of the
//+       data.
//!param: detGeom** data - A pointer to the array of detector structures.
//!param: const UINT4 num_detectors - A number of array elements.
//!param: const INT4 rank - Rank of the node sending/receiving the data. Default
//+       is -1   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator
//+       within which IntervalType object is defined. Default is           
//+       MPI::COMM_WORLD.   
//
//!exc: Error generating DetectorType. - Error generating MPI datatype to      
//+     represent C detGeom data structure.   
//!exc: bad_alloc - Error allocating the memory.   
//!exc: bad_cast - ILWD input data is malformed.   
//!exc: MPI::Exception - MPI library exception.      
//   
DetectorType::DetectorType( const ILwd::LdasElement* pe, detGeom** data,
   const UINT4 num_detectors, const INT4 rank, const MPI::Intracomm& comm )
   : LdasDatatype( comm, rank ), mNum( num_detectors )
{
   if( mNum )
   {
      allocateData( data );
   
      if( inMaster() )
      {
         // Master parses detector information out
         parseILwd( *pe, *data );
      }
 
      // Build static MPI datatype if datatype will be used the very first time
      createMpiType();
   }
}

   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// Nodes that instantiate this class build MPI derived datatype to represent
// <i>detGeom</i> data structure for communication.
// <p>This constructor will be used if the data structure is part of the outPut. 
//   
//!param: detGeom** data - A pointer to the array of detGeom data structures 
//+       being allocated by master node, and containing valid data on slave 
//+       nodes.         
//!param: const UINT4 num_detectors - A number of array elements.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which DetectorType object is defined. Default is            
//+       MPI::COMM_WORLD.   
//!param: const INT4 rank - Rank of the node sending the data. Default is -1.   
//   
//!exc: Error generating DetectorType. - Error generating MPI datatype to      
//+     represent C interval union.
//!exc: MPI::Exception - MPI library exception.      
//!exc: bad_alloc - Memory allocation failed.   
//   
DetectorType::DetectorType( detGeom** data, const UINT4 num_detectors,
   const MPI::Intracomm& comm, const INT4 rank ) 
   : LdasDatatype( comm, rank ), mNum( num_detectors )
{
   if( mNum )
   {
      if( inMaster() )
      {
         allocateData( data );   
      }
   
      // Build static MPI datatype if datatype will be used the very first time
      createMpiType();
   }
}
   
   
//------------------------------------------------------------------------------
//
//: Destructor
//
DetectorType::~DetectorType()
{}
   

//------------------------------------------------------------------------------
//
//: Converts array of detGeom data structures to the ILWD format element. 
//       
//!param: const detGeom* pe - An array of detGeom data structures.
//!param: const UINT4 num_detectors - Number of array elements.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined.    
//   
//!return: ILwd::LdasElement - A pointer to the ILWD element.
//
//!exc: bad_alloc - Memory allocation failed.
//   
ILwd::LdasElement* DetectorType::toILwd( const detGeom* pe,
   const UINT4 num_detectors, const MPI::Intracomm& comm )
{
   if( num_detectors == 0 )
   {
      return 0;
   }

   auto_ptr< LdasContainer > result( new LdasContainer( mOutterContainerName ) );
   
   auto_ptr< LdasContainer > c;
   const detGeom* det_elem( pe ), 
         *det_elem_end( pe + num_detectors - 1 );
   while( det_elem <= det_elem_end )
   {
      // Convert each structure to separate ILWD container
      string name( det_elem->name );
      name += mContainerName;

      c.reset( new LdasContainer( name ) );
   
      c->push_back( new LdasArray< CHAR >( det_elem->prefix,
                           prefix_dim, mPrefixILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
   
      c->push_back( new LdasArray< REAL8 >( det_elem->longitude,
                           mLongitudeILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
   
      c->push_back( new LdasArray< REAL8 >( det_elem->latitude,
                           mLatitudeILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
   
      c->push_back( new LdasArray< REAL4 >( det_elem->elevation,
                           mElevationILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );   

      c->push_back( new LdasArray< REAL4 >( det_elem->armXazimuth,
                           mArmXazimuthILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );   
   
      c->push_back( new LdasArray< REAL4 >( det_elem->armYazimuth,
                           mArmYazimuthILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );    
   
      c->push_back( new LdasArray< REAL4 >( det_elem->armXaltitude,
                           mArmXaltitudeILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );      

      c->push_back( new LdasArray< REAL4 >( det_elem->armYaltitude,
                           mArmYaltitudeILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );    

      c->push_back( new LdasArray< INT4 >( det_elem->localTime,
                           mLocalTimeILwdName ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );      

      result->push_back( c.release(),
                         ILwd::LdasContainer::NO_ALLOCATE,
                         ILwd::LdasContainer::OWN );
   
      ++det_elem;
   }
   
   return result.release();
}


//------------------------------------------------------------------------------
//
//: Gets ILWD format name attribute for detGeom.
//       
//!return: const std::string& - A reference to the name attribute.
//      
const std::string& DetectorType::getILwdName()
{
   return mILwdName;
}

   
//------------------------------------------------------------------------------
//
//: Gets name field position of the ILWD format attribute.
//       
//!return: const size_t - A field position.
//      
const size_t DetectorType::getILwdNamePosition()
{
   return mILwdNamePosition;
}
   
   
//------------------------------------------------------------------------------
//
//: Frees MPI derived datatype. 
//
// This method frees MPI derived datatype if it was created earlier.
// Called by all nodes of the communicator that were using this datatype.
//  
//!return: Nothing.    
//   
void DetectorType::freeMpiType()
{
   if( mMpiType != MPI::DATATYPE_NULL )
   {
      mMpiType.Free();
      mMpiType = MPI::DATATYPE_NULL;
   }

   return;
}
   

//------------------------------------------------------------------------------
//
//: Gets MPI derived datatype. 
//
// This method accesses MPI derived datatype that represents detGeom C data 
// structure.   
// Called by all nodes of the communicator that're using this datatype.
//  
//!return: const MPI::Datatype& - A reference to the MPI derived datatype.
//   
const MPI::Datatype& DetectorType::getMpiType()
{
   return mMpiType;
}

   
//------------------------------------------------------------------------------
//
//: Creates static MPI derived datatype.
//
// This method creates MPI derived datatype that represents detGeom C data 
// structure. All nodes that use detGeom for communication need to create this
// MPI datatype.   
//
//!return: Nothing.
//
//!exc: Error generating DetectorType. - Error occured while creating MPI derived 
//+     datatype.   
//   
void DetectorType::createMpiType()
{
   if( mNum && mMpiType == MPI::DATATYPE_NULL )
   {
      static const INT4 type_count( 1 );
      detGeom data;
   
      INT4 blocks[ mTypeNum ];
      fill( blocks, blocks + mTypeNum, type_count );
      blocks[ 0 ] = maxDetectorName;
      blocks[ 1 ] = maxDetectorPrefix;

      MPI::Datatype types[ mTypeNum ] = { LDAS_MPI_CHAR,
                                          LDAS_MPI_CHAR,
                                          LDAS_MPI_REAL8,
                                          LDAS_MPI_REAL8,
                                          LDAS_MPI_REAL4,
                                          LDAS_MPI_REAL4,
                                          LDAS_MPI_REAL4,
                                          LDAS_MPI_REAL4,
                                          LDAS_MPI_REAL4,
                                          LDAS_MPI_INT4 };

      MPI::Aint disps[ mTypeNum ] = { MPI::Get_address( data.name ),
                                      MPI::Get_address( data.prefix ),
                                      MPI::Get_address( &data.longitude ),
                                      MPI::Get_address( &data.latitude ),
                                      MPI::Get_address( &data.elevation ),
                                      MPI::Get_address( &data.armXazimuth ),
                                      MPI::Get_address( &data.armYazimuth ),
                                      MPI::Get_address( &data.armXaltitude ),
                                      MPI::Get_address( &data.armYaltitude ),
                                      MPI::Get_address( &data.localTime ) };
      for( INT4 i = mTypeNum - 1; i >= 0; --i )
      {
         disps[ i ] -= disps[ 0 ];
      }
      

      mMpiType = MPI::Datatype::Create_struct( mTypeNum, blocks, 
                                               disps, types );
   
      if( mMpiType == MPI::DATATYPE_NULL )
      {
         node_error( "Error generating DetectorType.",
                     getComm(), isP2P() );
      }
   
      mMpiType.Commit();   
   }
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Gets value of required ILWD array element.
//      
// This method finds ILWD format array of specified type in the passed to it
// ILWD format container and extracts its value. The element is a required data.
//    
//!param: const LdasContainer& re - A reference to the ILWD format element
//+       representing one C detGeom data structure.   
//!param: const CHAR* name - Name key of the element.   
//
//!return: REAL8 - Value of the element.   
//
//!exc: Malformed input format: <b>name</b> is missing. - Required
//+     ILWD format element is missing.      
//!exc: Malformed input format for <b>name</b>. - Specified
//+     ILWD format element is malformed.   
//!exc: bad_cast - Malformed input data.   
///    
template< class T > const T DetectorType::getArrayValue( 
   const ILwd::LdasContainer& e, const CHAR* name ) const
{
   static const size_t name_pos( 0 );
   const LdasElement* elem( findContainerElement( e, name, name_pos ) );   

   try
   {
      const LdasArray< T >* a( dynamic_cast< const LdasArray< T >* >( elem ) );
      if( a == 0 || a->getNData() != 1 )
      {
         throw bad_cast();
      }
   
      return a->getData()[ 0 ];
   }
   catch( const bad_cast& exc )
   {
      string msg( "Malformed input format for " );
      msg += name;
      node_error( msg.c_str(), getComm(), isP2P() );   
   }
   
   return 0;   
}

   
// Template instantiation   
template const REAL8 DetectorType::getArrayValue< REAL8 >( 
   const ILwd::LdasContainer& c, const CHAR* name ) const;
template const REAL4 DetectorType::getArrayValue< REAL4 >( 
   const ILwd::LdasContainer& c, const CHAR* name ) const;
template const INT4 DetectorType::getArrayValue< INT4 >( 
   const ILwd::LdasContainer& c, const CHAR* name ) const;
template const UINT4 DetectorType::getArrayValue< UINT4 >( 
   const ILwd::LdasContainer& c, const CHAR* name ) const;
   

//-----------------------------------------------------------------------------
//   
//: Parse ILwd element.
//   
// This method initializes array of C detGeom data structures.                
// This method is called only by the master node.
//
//!param: ILwd::LdasElement& re - A reference to the ILWD format element
//+       representing array of detGeom data structures.
//!param: detGeom* const data - A pointer to the array of C detGeom data      
//+       structures to be initialized.   
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.   
//!exc: bad_cast - Input ILWD data is malformed.   
//   
void DetectorType::parseILwd( const ILwd::LdasElement& re,
   detGeom* const data ) const
{
   const LdasContainer& pe( dynamic_cast< const LdasContainer& >( re ) );   
   if( pe.size() != mNum )
   {
      // Something went wrong during metadata communication
      ostringstream s;
      s << "Inconsistent number of detectors: ILWD size=" << pe.size() 
        << " vs. metadata size=" << mNum; 
       
      node_error( s.str().c_str(), getComm(), isP2P() );
   }
   
   ilwdConstVector dets( findILwd( pe, mILwdName ) );
   if( dets.size() != mNum )
   {
      // Not all container elements are detectors
      throw bad_cast();
   }
   
   const_iterator iter( dets.begin() ),
                  end_iter( dets.begin() + mNum );
   detGeom* data_elem( data );
   
   static const size_t name_pos( 0 );
   size_t n_size( 0 );
   const LdasArray< CHAR >* prefix_a( 0 );
   
   while( iter != end_iter )
   {
      // Extract all detector elements:
      const string name( ( *iter )->getName( name_pos ) );
      n_size = min( static_cast<const INT4>( name.size() ), maxDetectorName - 1 );
      copy( name.begin(), name.begin() + n_size, data_elem->name );

      const LdasContainer& c( dynamic_cast< const LdasContainer& >( **iter ) );   
   
      // Extract prefix
      const LdasElement* elem( findContainerElement( c, mPrefixILwdName, name_pos ) );      
      prefix_a = dynamic_cast< const LdasArray< CHAR >* >( elem );

      if( prefix_a == 0 || 
          prefix_a->getNDim() != 1 || 
          static_cast< const INT4 >( prefix_a->getNData() ) != prefix_dim )
      {
         throw bad_cast();
      }
   
      const CHAR* prefix_data( prefix_a->getData() );
      copy( prefix_data, prefix_data + prefix_dim, data_elem->prefix );
   
      data_elem->longitude = getArrayValue< REAL8 >( c, mLongitudeILwdName );
      data_elem->latitude = getArrayValue< REAL8 >( c, mLatitudeILwdName );
      data_elem->elevation = getArrayValue< REAL4 >( c, mElevationILwdName );
      data_elem->armXazimuth = getArrayValue< REAL4 >( c, mArmXazimuthILwdName );
      data_elem->armYazimuth = getArrayValue< REAL4 >( c, mArmYazimuthILwdName );
      data_elem->armXaltitude = getArrayValue< REAL4 >( c, mArmXaltitudeILwdName );
      data_elem->armYaltitude = getArrayValue< REAL4 >( c, mArmYaltitudeILwdName );
      data_elem->localTime = getArrayValue< INT4 >( c, mLocalTimeILwdName );

      ++iter;
      ++data_elem;
   }

   return;   
}

   
//------------------------------------------------------------------------------
//
//: Allocates memory for the array of data structures.
//      
// This method has to be called by all nodes in the
// communicator that use the detector information for communication.
//    
//!param: detGeom** data - An address of the array of detGeom structures.
//
//!exc: bad_alloc - Memory allocation failed.   
//    
void DetectorType::allocateData( detGeom** data )
{
   *data = new detGeom[ mNum ];
   memset( *data, 0, sizeof( detGeom ) * mNum );

   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* DetectorType::getDatatypeName() const
{
   return "DetectorType";
}
   
   
   //#define DEBUG_DETECTOR_GEOM      

//------------------------------------------------------------------------------
//
//: Allocates the memory and copies the data for the array of C detGeom
//: data structures.
//
//!param: detGeom** pd - A pointer to the destination array of C         
//+       detGeom data structures.
//!param: const detGeom* ps - A source array of C detGeom data structures.
//!param: const UINT4 num - Number of structures in the source and destination 
//+       arrays.
//!param: const Intracomm& comm - A reference to the intracommunicator within 
//+       which communication occures.
//!param: const bool p2p - Flag to indicate that method is called within p2p 
//+       communication.   
//
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//   
void storeDetector( detGeom** pd, const detGeom* ps,
   const UINT4 num, const MPI::Intracomm& comm, const bool p2p )
{
   if( !num )
   {
      return;
   }
   
   *pd = new detGeom[ num ];
   memcpy( *pd, ps, sizeof( detGeom ) * num );

#ifdef DEBUG_DETECTOR_GEOM   
   for( UINT4 i = 0; i < num; ++i )
   {
      cout << "Slave: detGeom[" << i << "].name=\"" << ( *pd )[ i ].name
           << "\""; 
      cout << " detGeom[" << i << "].longitude="
           << ( *pd )[ i ].longitude << endl;
   }
#endif
   
   return;
}
