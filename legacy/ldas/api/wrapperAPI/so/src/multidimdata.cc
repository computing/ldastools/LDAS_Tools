#include "LDASConfig.h"

// System Header Files
#include <new>   
#include <cstring>   
#include <algorithm>
#include <memory>   

// General Header Files
#include <general/autoarray.hh>

// ILWD Header Files   
#include <ilwd/ldascontainer.hh>   
#include <ilwd/ldasstring.hh>   
#include <ilwd/ldasarray.hh>   

// Local Header Files   
#include "multidimdata.hh"
#include "intervaltype.hh"   
#include "historytype.hh"   
#include "ldaserror.hh"
   
#include "mpidatautil.hh"   
#include "initvars.hh"   

   
using namespace std;   

using ILwd::LdasElement;   
using ILwd::LdasContainer;
using ILwd::LdasString;      
using ILwd::LdasArray;   

   
// Static data members initialization
const string MultiDimDataType::mILwdName( "sequence" );   
const size_t MultiDimDataType::mILwdNamePosition( 2 );      
const UINT4 MultiDimDataType::mTypeNum( 4 );   
const UINT4 MultiDimDataType::mNameUnitsCommentNum(
    maxMultiDimName + maxMultiDimUnits + maxMultiDimComment );   
   
   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype to represent an array of C 
// multiDimData structures used as part of inPut data structure. All nodes in 
// communicator using inPut structure for communication must construct this 
// datatype. When called by master node it will initialize passed to it array 
// of multiDimData structures with the data of passed to it ILWD format element
// representing that array.
//   
//!param: ilwdConstVector& seq_vec - A reference to the vector of ILWD          
//+       elements representing an array of C multiDimData data structures.
//!param: multiDimData** mData - A pointer to the array of multiDimData C     
//+       data structures to initialize by master node, and to be allocated  
//+       by all nodes in the communicator.   
//!param: const MPIStructInfo* dataInfo - An array of metadata structures for   
//+       the elements of <i>mData</i> array.   
//!param: const UINT4 num_elems - Number of elements in <b>mData</b> array.        
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which MultiDimDataType object is defined. Default is        
//+       MPI::COMM_WORLD.   
//   
//!exc: bad_cast - ILWD input data is malformed.
//!exc: bad_alloc - Error allocating memory.
//!exc: Undefined datatype. - Undefined datatype for ILWD element               
//+     representing the data was specified.     
//!exc: Undefined domain. - Undefined domain was specified.                     
//+     One of TIME | FREQ | BOTH.       
//!exc: data is missing. - ILWD format element representing the data is missing.         
//
//!exc: start_time is missing. - Could not find elements with "start_time"     
//+     name field.
//!exc: step_size is missing. - Could not find element with step_size name     
//+     field.   
//!exc: Malformed input format for gpsTimeInterval. - Invalid input format was 
//+     specified for gpsTimeInterval.   
//!exc: start_freq is missing. - Could not find element with start_freq name   
//+     field.   
//!exc: Malformed input format for frequencyInterval. - Invalid input format   
//+     was specified for frequencyInterval.      
//!exc: Malformed input format for timeFreqInterval. - Invalid input format    
//+     was specified for timeFreqInterval.
//!exc: Inconsistent number of samples for timeFreqInterval. - Number of time  
//+     and frequency samples is different.         
//   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype 
//+     to represent C interval union.   
//!exc: Error generating MultiDimDataType. - Error generating MPI derived       
//+     datatype to represent an array of C multiDimData structures.            
//!exc: Error generating HistoryType. - Error generating MPI derived datatype   
//+     to represent history linked list.   
//!exc: Invalid number of history nodes specified. - Negative number of nodes   
//+     was specified.              
//!exc: MPI::Exception - MPI library exception.      
//   
MultiDimDataType::MultiDimDataType( ilwdConstVector& seq_vec,
   multiDimData** mData, const MPIStructInfo* dataInfo, 
   const UINT4 num_elems, const INT4 rank, const MPI::Intracomm& comm ) 
   : LdasDatatype( comm, rank ), LdasData(), mNum( num_elems )
{
   // Allocate memory for the structure
   allocateData( mData, dataInfo );
   
   
   // Master parses ILWD data
   General::AutoArray< ilwdVector > hist_vec( mNum == 0 ? 0 : new ilwdVector[ mNum ] );
   
   
   if( inMaster() )
   {
      parseILwd( seq_vec, hist_vec.get(), *mData );
   }
    
   
   const LdasElement* e( 0 );
   
   vector< MPI::Datatype > rangeType( mNum );
   vector< MPI::Datatype > historyType( mNum );

   UINT4 num( 0 );
   
   for( UINT4 i = 0; i < mNum; ++i )
   {
      if( inMaster() )
      {
         e = seq_vec[ i ];
      }

      num = ( ( ( *mData )[ i ].space == databaseD ) ? ( *mData )[ i ].numberDimensions :
                                                 getNData( ( *mData )[ i ] ) );

      // Build interval structure datatype     
      // No memory leak here, since MPI copy is shallow.   
      rangeType[ i ] = IntervalType( e, ( *mData )[ i ].space, 
                                     ( *mData )[ i ].range,
                                     num, dataInfo[ i ].numberChannels,
                                     dataInfo[ i ].channelSize,
                                     dataInfo[ i ].numberDetectors, rank );
   
      // Build history linked list
      historyType[ i ] = HistoryType( hist_vec[ i ], 
                                      &( *mData )[ i ].history,
                                      dataInfo[ i ].historyInfo,
                                      dataInfo[ i ].numHistory, rank );
   }


   // Build MPI derived datatype
   buildType( rangeType, historyType, *mData );
}

   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype to represent an array of C 
// multiDimData structures used as part of outPut data structure.
// This constructor to be called only by two nodes that will use created datatype 
// for communication: slave node containing the data and master node 
// collecting that data. 
//   
//!param: multiDimData** mData - A pointer to the array of multiDimData C     
//+       data structures to be allocated by master node, and containing     
//+       valid data on slave nodes.
//!param: const MPIStructInfo* dataInfo - An array of metadata structures for   
//+       each element of the <i>mData</i> array.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which MultiDimDataType object is defined. Default is        
//+       MPI::COMM_WORLD.   
//!param: const INT4 rank - Rank of the node sending the data. Default is -1.      
//!param: const UINT4 num - Number of data structures in <i>pe</i> array.      
//   
//!exc: bad_alloc - Error allocating memory.
//!exc: Error generating MultiDimDataType. - Error generating MPI derived       
//+     datatype to represent an array of C multiDimData structures.         
//!exc: Error generating HistoryType. - Error generating MPI derived datatype   
//+     to represent history linked list.
//!exc: Invalid number of history nodes specified. - Negative number of nodes  
//+     was specified.      
//!exc: Error generating IntervalType. - Error generating MPI derived datatype  
//+     to represent C interval union.       
//!exc: Undefined domain. - Undefined domain was specified.                     
//+     One of TIME | FREQ | BOTH.      
//!exc: Undefined datatype. - Undefined datatype for ILWD element               
//+     representing the data was specified.  
//!exc: Invalid number of multiDimData. - Negative number of array elements is  
//+     specified.      
//!exc: MPI::Exception - MPI library exception.      
//   
MultiDimDataType::MultiDimDataType( multiDimData** mData, 
   const MPIStructInfo* dataInfo, const MPI::Intracomm& comm,
   const INT4 rank, const UINT4 num ) 
   : LdasDatatype( comm, rank ), LdasData(), mNum( num )
{
   // There is multiDimData 
   if( mNum > 0 )
   {
      if( inMaster() )
      {
         allocateData( mData, dataInfo );
      }


      vector< MPI::Datatype > rangeType( mNum );
      vector< MPI::Datatype > historyType( mNum );
   
   
      for( UINT4 i = 0; i < mNum; ++i )
      {
         // Build interval datatype     
         // No memory leak here, since MPI copy is shallow.
         rangeType[ i ] = IntervalType( ( *mData )[ i ].space, 
                             ( *mData )[ i ].range, 
                             dataInfo[ i ].numberChannels, 
                             dataInfo[ i ].channelSize,
                             dataInfo[ i ].numberDetectors, comm, rank );
   
         // Build history linked list
         historyType[ i ] = HistoryType( &( *mData )[ i ].history, 
                               dataInfo[ i ], comm, rank );
      }


      buildType( rangeType, historyType, *mData );
   }
   else if( mNum == 0 )
   {
      createNULLType();
   }
   else
   {
      node_error( "Invalid number of multiDimData C structures.", comm );
   }
}   

   
//-----------------------------------------------------------------------------
//
//: Destructor.
//   
MultiDimDataType::~MultiDimDataType()   
{
}

   
//------------------------------------------------------------------------------
//
//: Converts multiDimData data structure to the ILWD format element. 
//    
//!param: const multiDimData& re - A reference to the C multiDimData structure  
//+       to convert into ILWD format element.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined. 
//!return: LdasElement* - A pointer to the created ILWD format element.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined domain. - Undefined domain specified.   
//!exc: Undefined datatype. - Undefined datatype specified.   
//         
LdasElement* MultiDimDataType::toILwd( const multiDimData& re, 
   const MPI::Intracomm& comm )
{
   auto_ptr< LdasContainer > e( new LdasContainer() );
   e->setName( mILwdNamePosition, mILwdName );
   
   e->setName( 0, re.name );
   string struct_comment( re.comment );

   
   // Create domain
   string domain;
   
   switch( re.space )
   {
      case(noneD):
      {
         domain = NoneDomainString;
         break;
      }
      case(timeD):
      {
         domain = TimeDomainString;
         break;
      }
      case(freqD):
      {
         domain = FreqDomainString;
         break;
      }   
      case(bothD):
      {
         domain = BothDomainString;
         break;
      }   
      case(databaseD):
      {
         domain = DatabaseDomainString;
         break;
      }      
      default:
      {
         node_error( "Undefined domain.", comm );
         break;
      }
   }

   
   string s( re.type == boolean_lu ? "boolean" : "real" );
   s += ":domain";
   

   e->push_back( new LdasString( domain, s ),
                 ILwd::LdasContainer::NO_ALLOCATE,
                 ILwd::LdasContainer::OWN );

   string comment;
   list< string > channels;
   ilwdResultVector iv( IntervalType::toILwd( re.space, re.range,
                        comm, channels, comment ) );
   // comment will be not empty only for dataBase domain
   if( !struct_comment.empty() && !comment.empty() )
   {
      struct_comment += ';';
   }
   struct_comment += comment;   
   
   e->setComment( struct_comment );      

   if( !channels.empty() )
   {
      list< string >::const_iterator l_iter( channels.begin() ),
         l_end( channels.end() ), l_begin( channels.begin() );
      while( l_iter != l_end )
      {
         // Start appending channel names after 'primary'
         if( l_iter == l_begin )
         {
            e->setName( mPrimaryPos + 1, *l_iter );
         }
         else
         {
            e->appendName( *l_iter );
         }
         ++l_iter;
      }
   }
   
   result_iterator iter( iv.begin() ),
                   iter_end( iv.end() );
   while( iter != iter_end )
   {
      e->push_back( *iter,
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
      ++iter;
   }

   iv.clear();
   
   // Create history if there is any
   if( re.history != 0 )
   {
      e->push_back( HistoryType::toILwd( re.history, comm ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
   }
   
   
   // Create data ILWD element
   e->push_back( dataToILwd( re.type, re.numberDimensions, 
                             re.dimensions, re.data, comm,
                             "data", re.units ),
                 ILwd::LdasContainer::NO_ALLOCATE,
                 ILwd::LdasContainer::OWN );

   return e.release();   
}
   

//------------------------------------------------------------------------------
//
//: Gets ILWD format name attribute for multiDimData.
//       
//!return: const std::string& - A reference to the name attribute.
//      
const std::string& MultiDimDataType::getILwdName()
{
   return mILwdName;
}

   
//------------------------------------------------------------------------------
//
//: Gets name field position of the ILWD format attribute.
//       
//!return: const size_t - A field position.
//      
const size_t MultiDimDataType::getILwdNamePosition()
{
   return mILwdNamePosition;
}
   
   
//-----------------------------------------------------------------------------
//   
//: Parse ILwd element.
//   
// This method initializes mData array of C multiDimData structures.                
// This method is called only by the master node.
//
//!param: ilwdConstVector& re - A vector of ILWD elements representing        
//+       inPut.sequences data.
//!param: ilwdVector* history_vec - An array of ILWD elements representing    
//+       dcHistory linked lists for each sequence.   
//!param: multiDimData* mData - A pointer to the array of C multiDimData      
//+       structures to be initialized.   
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.   
//!exc: bad_cast - Input ILWD data is malformed.   
//!exc: Undefined datatype. - Undefined datatype for ILWD element              
//+     representing the data was specified.
//!exc: Undefined domain. - Undefined domain was specified.                    
//+     One of TIME | FREQ | BOTH.         
//!exc: data is missing. - ILWD format element representing the data is missing.   
//   
void MultiDimDataType::parseILwd( ilwdConstVector& re, 
   ilwdVector* history_vec, multiDimData* mData ) const
{
   const LdasElement* data_elem( 0 );
   size_t s_size( 0 );
   
   const_iterator iter( re.begin() );
   const_iterator iter_end( iter + mNum );
   
   multiDimData* md_elem( mData );
   ilwdVector*   hv_elem( history_vec );
   
   while( iter != iter_end )
   {
      // Get name of the data
      const string& name = ( *iter )->getNameString();

      // Make sure source string is within C style string limits
      s_size = std::min( static_cast< INT4 >( name.size() ),
                                              maxMultiDimName - 1 );
      std::copy( name.begin(), name.begin() + s_size, 
                 md_elem->name );
      md_elem->name[ s_size ] = '\0';


      // Get comment
      const string& comment = ( *iter )->getComment();

      // Make sure source string is within C style string limits
      s_size = std::min( static_cast< INT4 >( comment.size() ),
                         maxMultiDimComment - 1 );
      std::copy( comment.begin(), comment.begin() + s_size, 
                 md_elem->comment );

   
      // Get history ILWD elements( concatenate history names:
      // history container and history record )
      *hv_elem = getHistory( *iter, true );
   

      // Get data element
      data_elem = findElement( **iter, "data" );
   
   
      // Get data units
      const string& units = getUnits( data_elem );

      // Make sure source string is within C style string limits
      s_size = std::min( static_cast< INT4 >( units.size() ),
                         maxMultiDimUnits - 1 );   

      std::copy( units.begin(), units.begin() + s_size, 
                 md_elem->units );

   
      // Set the data:
      setDataArray( data_elem, &( md_elem->data ), md_elem->type,   
                    getNData( *md_elem ) );
   
   
      ++iter;
      ++md_elem;
      ++hv_elem;
   }

   
   return;
}

   
//------------------------------------------------------------------------------
//   
//: Allocate memory for data.
//
// This method allocates memory for the array of C multiDimData data structures,
// and initializes the metadata of that data.   
//   
//!param: multiDimData** mData - A pointer to the array of multiDimData data   
//+       structures.
//!param: const MPIStructInfo* dataInfo - An array of metadata for the 
//+       array of multiDimData structures.
//   
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.   
//!exc: Undefined datatype - Undefined datatype is specified.   
//   
void MultiDimDataType::allocateData( multiDimData** mData,
   const MPIStructInfo* dataInfo ) const 
{
   *mData = new multiDimData[ mNum ];
   
   // Initialize allocated memory to zero
   memset( *mData, 0, mNum*sizeof( multiDimData ) );       
  
   
   multiDimData* md_elem( *mData );
   const multiDimData* md_elem_end( md_elem + mNum - 1 );
   const MPIStructInfo* data_elem( dataInfo );
   
   // Copy the data
   while( md_elem <= md_elem_end )
   {
      md_elem->space = data_elem->space;
      md_elem->type  = data_elem->type;   
      md_elem->numberDimensions = data_elem->numberDimensions;   

      if( md_elem->numberDimensions )
      {
         md_elem->dimensions = 
              new UINT4[ md_elem->numberDimensions ];
   
	 std::copy( data_elem->dimensions, 
                    data_elem->dimensions + data_elem->numberDimensions,
                    md_elem->dimensions );
   
         allocateDataArray( &( md_elem->data ), md_elem->type, 
                            getNData( *md_elem ), getComm(),
                            isP2P() );            
      }
   
   
      ++md_elem;
      ++data_elem;
   }
   
   
   return;
}

   
//-----------------------------------------------------------------------------
//
//: Build MPI derived datatype.
//
// This method must be called by all nodes in the communicator that will use
// array of C multiDimData structures for the communication.   
// <p>This method generates MPI derived datatype representing the array of    
// C multiDimData structures.
// <p>This method marks passed to it MPI datatype objects for deallocation.
//
//!param: MPI::Datatype rType[] - An array of MPI datatypes where each        
//+       element represents a C interval union within each multiDimData 
//+       structure.
//!param: MPI::Datatype hType[] - An array of MPI datatypes where each        
//+       element represents a C history linked list within each multiDimData        
//+       structure.
//!param: multiDimData* mData - A pointer to the array of C multiDimData data   
//+       structures.   
//
//!exc: Undefined datatype - Undefined datatype is specified.
//!exc: Error generating MultiDimDataType. - Error generating MPI derived     
//+     datatype to represent an array of C multiDimData structures.   
//!exc: bad_alloc - Memory allocation failed.
//!exc: MPI::Exception - MPI library exception.      
//   
void MultiDimDataType::buildType( std::vector< MPI::Datatype >& rType,
   std::vector< MPI::Datatype >& hType, multiDimData* mData )
{
   const INT4 num( mNum * mTypeNum );
   
   vector< INT4 >          blocks( num );
   vector< MPI::Datatype > types( num );
   vector< MPI::Aint >     disps( num );

   INT4 i_num( 0 );
   const bool p2p( isP2P() );
   
   for( UINT4 i = 0; i < mNum; ++i )
   {
      // Init name and units info
      types[ i_num ]  = LDAS_MPI_CHAR;
      blocks[ i_num ] = mNameUnitsCommentNum;
      disps[ i_num ]  = MPI::Get_address( mData[ i ].name );
   
   
      // Init range info
      ++i_num;
      types[ i_num ]  = rType[ i ];
      blocks[ i_num ] = 1;
      disps[ i_num ]  = MPI::Get_address( &mData[ i ].range );   
   
   
      // Init history info
      if( hType[ i ] != MPI::DATATYPE_NULL )
      {
         ++i_num;
         types[ i_num ]  = hType[ i ];
         blocks[ i_num ] = 1;
         disps[ i_num ]  = MPI::Get_address( mData[ i ].history->name );   
      }
   
   
      // Init data info
      if( mData[ i ].numberDimensions )
      {
         ++i_num;
         types[ i_num ]  = dataArrayType( mData[ i ].type, getComm(), p2p ); 
         blocks[ i_num ] = getNData( mData[ i ] );
         disps[ i_num ]  = dataArrayDisplacement( mData[ i ].type, 
                              mData[ i ].data, getComm(), p2p );
      }
   
   
      ++i_num;
   }


   createType( i_num, blocks, disps, types );
   
   
   // Delete each structure specific datatypes
   for( UINT4 i = 0; i < mNum; ++i )
   {
      rType[ i ].Free();
   
      if( hType[ i ] != MPI::DATATYPE_NULL )
      {
         hType[ i ].Free();
      }
   }


   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* MultiDimDataType::getDatatypeName() const
{
   return "MultiDimDataType";
}
   
   
//-----------------------------------------------------------------------------
//   
//: Get number of data elements.
//
// This method returns the number of elements in the passed to it 
// multiDimData structure's data( number of elements in a multidimensional 
// array ).
//   
//!param: const multiDimData& rs - A reference to the structure storing the   
//+       data.   
//   
//!return: const UINT4 - Number of elements.
//   
const UINT4 MultiDimDataType::getNData( const multiDimData& rs ) const 
{
   return getNDim( rs.numberDimensions, rs.dimensions );
}

   
//------------------------------------------------------------------------------
//
//: Allocates the memory and copies the data for the array of C multiDimData
//: data structures.
//
//!param: multiDimData* pd - A pointer to the destination array of C         
//+       multiDimData data structures.
//!param: const multiDimData* ps - A pointer to the source array of C        
//+       multiDimData data structures.
//!param: const UINT4 num - Number of structures in the source and destination 
//+       arrays.
//!param: const Intracomm& comm - A reference to the intracommunicator within 
//+       which communication occures.
//!param: const bool p2p - Flag to indicate that method is called within p2p 
//+       communication.   
//
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined datatype. - Undefined datatype was specified for the data   
//+     array.   
//!exc: Undefined domain. - Undefined domain was specified for the C interval   
//+     union.   
//!exc: NULL multiDimData array for copy. - Null multiDimData array is specified 
//+     for source data.   
//      
void storeMultiDimData( multiDimData* pd, const multiDimData* ps,
   const UINT4 num, const MPI::Intracomm& comm, const bool p2p )
{
    if( ps == 0 )
    {
       node_error( "NULL multiDimData array for copy.", comm, p2p );
    }
    
    UINT4 numData( 1 );
    dcHistory* d_curr_node( 0 ),
              *d_prev_node( 0 );
    const dcHistory* s_curr_node( 0 );   
    bool first_node( true );

    memset( pd, 0, num * sizeof( multiDimData ) );
   
    multiDimData* pd_elem( pd );
    const multiDimData* ps_elem( ps ),
                       *ps_end( ps + num - 1);
   
   
    while( ps_elem <= ps_end )
    { 
       // Copy name and units
       std::copy( ps_elem->name, ps_elem->name + maxMultiDimName,
                  pd_elem->name );

       std::copy( ps_elem->units, ps_elem->units + maxMultiDimUnits,
                  pd_elem->units );

       std::copy( ps_elem->comment, ps_elem->comment + maxMultiDimComment,
                  pd_elem->comment );

   
       // Copy space, type, numberDimensions
       pd_elem->space = ps_elem->space;
       pd_elem->type  = ps_elem->type;
       pd_elem->numberDimensions = ps_elem->numberDimensions;   
   
   
       // Copy range data
       storeInterval( pd_elem->space, pd_elem->range, ps_elem->range,
                      comm, p2p );
   
   
       if( pd_elem->numberDimensions )
       {
          pd_elem->dimensions = new UINT4[ pd_elem->numberDimensions ]; 
   
          // Copy array of dimensions
          std::copy( ps_elem->dimensions, ps_elem->dimensions + ps_elem->numberDimensions,
                     pd_elem->dimensions );

          numData = LdasData::getNDim( pd_elem->numberDimensions,
                                       pd_elem->dimensions );

          // Copy array of data
          LdasData::allocateDataArray( &( pd_elem->data ), 
                       pd_elem->type, numData, comm, p2p );
          LdasData::setDataArray( &( pd_elem->data ), 
                       ps_elem->data, pd_elem->type,
                       numData, comm, p2p );
       }   

   
       // Handle history 
       d_prev_node = 0;
       d_curr_node = 0;
       s_curr_node = ps_elem->history;
       first_node = true;
   
       while( s_curr_node != 0 )
       {
          d_curr_node = new dcHistory;
          memset( d_curr_node, 0, sizeof( dcHistory ) );
   
          // Copy name and units
          std::copy( s_curr_node->name, s_curr_node->name + maxHistoryName,
                     d_curr_node->name );          

          std::copy( s_curr_node->units, s_curr_node->units + maxHistoryUnits,
                     d_curr_node->units );          

          d_curr_node->type = s_curr_node->type;
          d_curr_node->numberValues = s_curr_node->numberValues;   

          // These methods won't do anything if numberValues=0
          LdasData::allocateDataArray( &( d_curr_node->value ), 
                                       d_curr_node->type,
                                       d_curr_node->numberValues,
                                       comm, p2p );      
   
          LdasData::setDataArray( &( d_curr_node->value ), 
                                  s_curr_node->value, 
                                  d_curr_node->type,
                                  d_curr_node->numberValues,
                                  comm, p2p );   
   
          if( first_node )
          {
             pd_elem->history = d_curr_node;
             first_node = false;
          }
          else
          {
             d_curr_node->previous = d_prev_node;
             d_prev_node->next = d_curr_node;
          }
   
   
          d_prev_node = d_curr_node;
          s_curr_node = s_curr_node->next;
       }

       
       ++pd_elem;
       ++ps_elem;
    }   

   
    return;
}
   
   
//------------------------------------------------------------------------------
//
//: Cleanup utility for array of multiDimData structures.
//      
// This method frees all dynamically allocated memory in the array of C 
// multiDimData data structures.
//    
//!param: multiDimData* pe - A pointer to the array of "C" multiDimData        
//+       structures.
//!param: const UINT4 num - Number of elements in the array.
//!param: const MPI::Intracomm& comm - A reference to the MPI                  
//+       intracommunicator whithin which datatype is defined. 
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.         
//
//!return: Nothing.
//   
//!exc: Undefined datatype - Undefined datatype was specified.
//   
void deleteMultiDimData( multiDimData* pe, const UINT4 num,
   const MPI::Intracomm& comm, const bool p2p )
{
   if( pe == 0 )
   {
      return;
   }

   multiDimData* pe_elem( pe );
   const multiDimData* pe_end( pe + num - 1 );
   
   dcHistory* curr_node( 0 );
   dcHistory* next_node( 0 );

   
   while( pe_elem <= pe_end )
   {
      // Delete channel information in interval
      deleteInterval( pe_elem->space, pe_elem->range, comm, p2p );        
   
      if( pe_elem->numberDimensions )
      {
         delete[] pe_elem->dimensions;
         pe_elem->dimensions = 0;
   
         LdasData::deleteDataArray( &( pe_elem->data ), 
                                    pe_elem->type, comm, p2p );   
      }

   
      // Delete history data
      curr_node = pe_elem->history;
   
      while( curr_node != 0 )
      {
         LdasData::deleteDataArray( &curr_node->value, 
                                    curr_node->type, comm, p2p );
         next_node = curr_node->next;
   
         delete curr_node;
   
         curr_node = next_node;        
      }
   
      pe_elem->history = 0;
   
   
      ++pe_elem;
   }

   
   return;
}
