#ifndef WrapperApiMPIArgsHH
#define WrapperApiMPIArgsHH

// System Header Files
#include <utility>
#include <string>   
#include <vector>   

// Local Header Files
#include "LALAtomicDatatypes.h"   
   
   
// Forward declaration   
class Regex;
   
   
//------------------------------------------------------------------------------
//
//: NodeListArg functional.
//
// This functional is designed to parse -nodelist command line argument and 
// mpiAPI response messages that contain node ranks specifications.   
// These arguments can be represented in the mixed format of ranges and/or 
// lists of the nodes. For example: (i-j,k,l,m-n,o,p). 
// 
class NodeListArg
{
   
public:

   //
   //: Default constructor.
   //   
   NodeListArg(){}
   
   std::vector< INT4 > operator()( const CHAR* ) const; 

private:

   //: Regex to match range of ranks. 
   static const Regex re_range;
   
   //: Regex to match list of ranks.
   static const Regex re_list;   
   
};

   
// Typedefs   
//: A pair of string and INT4 to represent (host,port) argument.   
typedef std::pair< std::string, INT4 > HostPort;

   
//------------------------------------------------------------------------------
//
//: HostPortArg functional.
//  
// This functional parses command line arguments that can be represented in the
// following format: (hostname,port).
// 
class HostPortArg
{
   
public:

   //
   //: Default constructor.
   //
   HostPortArg(){}

   HostPort operator()( const CHAR* ) const;

private:
   
   //: Regex to match "(host,port)".
   static const Regex re_host_port;
   
};


//------------------------------------------------------------------------------
//
//: ParameterArg functional.
//  
// This functional parses -filterparams command line argument.
// Parameters can be signed long integers, double precision floats 
// and string values.   
// 
class ParameterArg
{
   
public:
  
   //
   //: Default constructor.
   //
   ParameterArg(){}

   void operator()( const CHAR* pc, std::vector< std::string >& vs )
      const;

private:
   
   //: Regex to match numeric parameter of long type. 
   static const Regex re_long;
   
   //: Regex to match numeric parameter of double type.    
   static const Regex re_double;
   
   //: Regex to match parameter of string type.    
   static const Regex re_string;
   
   //: Regex to match group of parameters, e.t. (a,3,6.78)
   static const Regex re_group;
   
};   
   

#endif
   
