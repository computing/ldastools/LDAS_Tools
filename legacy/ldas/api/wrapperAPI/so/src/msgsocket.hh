#ifndef WrapperApiMsgSocketHH
#define WrapperApiMsgSocketHH

// System Header Files   
#include <netdb.h>   
#include <string>   

// Local Header Files
#include "mpiargs.hh"   

   
//-----------------------------------------------------------------------------
//
//: MsgSocket class.
//
// This class is designed to implement mpiAPI<--->wrapperAPI communication.   
// This is a client implementation.
//   
class MsgSocket   
{
public:
   
   MsgSocket( const HostPort& host_port, const bool error = false,
      const bool mpi = true );
   MsgSocket( const INT4 handle, const bool error = false, 
      const bool mpi = true );
   MsgSocket( const MsgSocket& sh );      
   ~MsgSocket();
   
   void writeMessage( std::string& msg );
   void readMessage( std::string& recv_msg );

   void setIsError( const bool error );
   const bool isConnected() const;
   
   static const std::string parseError( const std::string& from );
   static void setMpiCommunicationDelay( const UINT4 sec );
   static void setMpiAPIResponseTimeout( const UINT4 sec );   
   static const bool canSendMessage( const std::string& msg );
   
private:
   
   // Helper methods
   void connectSocket( const std::string& host, const INT4 port );
   void disconnectSocket();   
   void setNonBlocking() const;
   
   //: Timeout value.    
   static INT4 mTimeOut;
   
   //: Buffer dimension used for message receiving.
   static const INT4 mDim;
   
   //: Time stamp associated with socket writes.
   static REAL8 mLastWriteTime;   
   
   //: Time delay( in seconds ) for communication to the mpiAPI.
   static UINT4 mMpiApiCommDelay;   
   
   //: Buffer to store unsent messages to the mpiAPI.
   static std::string mMpiApiCommBuffer;
   
   //: Handle to the client socket.
   INT4 mHandle;

   //: Flag to indicate if socket is connected.
   bool mConnected;
   
   //: Flag to indicate that socket is used for error submission.
   bool mIsError;
   
   //: Flag to indicate that socket is used for mpiAPI communication
   bool mToMpi;

   //: Capture written message to the socket
   std::string mWrittenMsg;

  //INT4 mMsgBytes;
   
};
   
   
//-----------------------------------------------------------------------------
//
//: Is socket connected?
//
// This method checks if client socket is connected.
//
//!return: const bool - True if socket is connected, false otherwise.
//   
inline const bool MsgSocket::isConnected() const
{
   return mConnected;
}

   
#endif   
