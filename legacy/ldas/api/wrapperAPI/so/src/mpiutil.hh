#ifndef WrapperAPIUtilHH
#define WrapperAPIUtilHH

// System Header Files
#include <vector>   
#include <string>   
   
// Local Header Files   
#include "wrapperInterfaceDatatypes.h"

   
// Global variables declarations( definitions )
#ifdef MpiUtilCC
#define LDAS_MPI_UTIL
#else
#define LDAS_MPI_UTIL extern
#endif   


//-------------------------------------------------------------------------------   
//      
//: Error handler for the dynamically loaded library functions.   
//
// This functional is used for handling errors produced by functions exposed to 
// the wrapperAPI by the dynamically loaded library.
//   
class DLErrorHandler
{
   
public:
   
   DLErrorHandler( const CHAR* func = 0, const INT4 code = 0, 
      CHAR* msg = 0 );

   void operator()( const CHAR* func, const INT4 code, CHAR* msg,
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = false ); 

   /* Accessors */
   std::string getErrorMsg() const;
   std::string getWarningMsg() const;
   const size_t getErrorMsgSize() const;
   const size_t getWarningMsgSize() const;
   
   /* Mutators */
   void resetMessages();
   static void resetMessages( std::string& war_msg, std::string& err_msg ); 
   static const INT8 getMaxMemory();
   
private:

   // Enum type for error code 
   enum ErrorCode
   {
      DLWarning = -1,
      DLNominal,
      DLError
   };

   void checkMemory( const CHAR* func, const MPI::Intracomm& comm, const bool p2p );

   //: Maximum amount of memory used by the process
   static INT8 mMaxMemory;

   //: Initial location of the program break( memory ).
   static void* mInitBreak;
   
   std::string mErrorMsg;
   std::string mWarningMsg; 
   
};
   

//-------------------------------------------------------------------------------   
// 
//: Gets descriptive error message.
//
//!return: string - Error message.
//   
inline std::string DLErrorHandler::getErrorMsg() const 
{
   return mErrorMsg;
}

   
//-------------------------------------------------------------------------------   
// 
//: Gets descriptive warning message.
//
//!return: string - Warning message.
//   
inline std::string DLErrorHandler::getWarningMsg() const 
{
   return mWarningMsg;
}
   
   
//-------------------------------------------------------------------------------   
// 
//: Gets the size of error message.
//      
//!return: const size_t - The error message size.
//   
inline const size_t DLErrorHandler::getErrorMsgSize() const
{
   return mErrorMsg.size();
}
   

//-------------------------------------------------------------------------------   
// 
//: Gets the size of warning message.
//      
//!return: const size_t - The warning message size.
//   
inline const size_t DLErrorHandler::getWarningMsgSize() const
{
   return mWarningMsg.size();
}   
   

bool inRanks( const std::vector< INT4 >& ranks, const INT4 rank );
void createNotLBNodes( const UINT4& num );
void createComm( std::vector< INT4 >& ranks, MPI_Comm* comm ); 

   
//: <font color='green'>This method is to simulate outPut generation
//: ( developement only!!! )</font> 
void buildOutputStruct( outPut** out, const UINT4 num );

//: <font color='green'>This method is to simulate outPut generation
//: ( developement only!!! )</font>    
void buildMDDOutputStruct( outPut** out, const UINT4 num,
   const MPI::Intracomm& comm ); 
   
//: <font color='green'>This method is to simulate outPut generation
//: ( developement only!!! )</font>    
void buildNullOutputStruct( outPut** out, const UINT4 num ); 


#endif   
