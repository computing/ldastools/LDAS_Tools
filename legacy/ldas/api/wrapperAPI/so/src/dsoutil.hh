#ifndef DSOUtilHH
#define DSOUtilHH
   
#include "wrapperInterfaceDatatypes.h"   

   
// Typedefs for dynamically loaded library functions   
typedef INT4 ( *init_fptr )( CHAR**, InitParams* );
typedef INT4 ( *cond_fptr )( CHAR**, inPut*, SearchParams* );
typedef INT4 ( *appl_fptr )( CHAR**, inPut*, SearchOutput*,
                             SearchParams* );   
typedef INT4 ( *free_fptr )( CHAR**, SearchOutput* );
typedef INT4 ( *fnlz_fptr )( CHAR** );         
   
   
#endif   
