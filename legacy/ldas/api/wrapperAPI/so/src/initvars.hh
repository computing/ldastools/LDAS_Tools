#ifndef WrapperApiInitVarsHH
#define WrapperApiInitVarsHH

// Local Header Files
#include "mpiargs.hh"   
   

// Global variables declarations( definitions )
#ifdef InitArgsCC
#define INIT_ARGS
#else
#define INIT_ARGS extern
#endif

   
INIT_ARGS std::vector< INT4 >        nodes;   
INIT_ARGS std::string                dynlib;
INIT_ARGS HostPort                   mpiAPI;
INIT_ARGS HostPort                   dataAPI;
INIT_ARGS HostPort                   resultAPI;
INIT_ARGS std::vector< std::string > filterParams;
INIT_ARGS REAL4                      realTimeRatio;
INIT_ARGS bool                       loadBalance;   
INIT_ARGS bool                       dataDistributor; 
INIT_ARGS bool                       communicateOutput;
INIT_ARGS UINT4                      jobID;
INIT_ARGS REAL4                      memoryUsageLimit;   
INIT_ARGS std::string                userTag;   
   
INIT_ARGS std::vector< INT4 > notNodes;      
INIT_ARGS std::vector< INT4 > masterPlusNodes;        
   
   
// Debug variables
INIT_ARGS bool                       enableMpiApi;
INIT_ARGS bool                       enableDumpInput;
INIT_ARGS std::string                dumpDataDir;
INIT_ARGS bool                       enableResultApi;   
INIT_ARGS UINT4                      numDummyOutput;      
INIT_ARGS UINT4                      numDummyState;         
INIT_ARGS UINT4                      numDummyMdd;       
INIT_ARGS bool                       enableLoadDso;
INIT_ARGS UINT4                      secSleepBeforeDso;
INIT_ARGS UINT4                      secSleepAfterDso;
INIT_ARGS bool                       enableLdasTrace;
INIT_ARGS bool                       enableXmpiTrace;      
INIT_ARGS bool                       enableDumpArgs;
INIT_ARGS bool                       enableTimeoutTrace;   
INIT_ARGS bool                       enableFinalizeTrace;   
INIT_ARGS bool                       LdasDebug;
INIT_ARGS bool                       enableTimeInfo;   

   
// Performance debugging variables   
INIT_ARGS REAL8 commOutputTime,
                commInputTime,
                initSearchTime,
                conditionDataTime,
                applySearchTime,
                freeOutputTime,
                finalizeSearchTime,
                commILwdTime, 
                lbTime,
                initArgsTime,
                nodeInitTime,
                createLdasTypesTime,
                loadDSOTime,
                nodeFinalizeTime;
   
   
// For standalone wrapperAPI only!!!
INIT_ARGS std::string inputFile;
   
   
#endif   
