#ifndef SlaveManagerHH
#define SlaveManagerHH

// Local Header Files
#include "nodemanager.hh"   
   
   
//------------------------------------------------------------------------------
//   
//: Functional for data management by slave node.
//      
class SlaveDataManager : public NodeDataManager   
{
public:

   SlaveDataManager( DLErrorHandler& dl_error, lbNodeInfo& lb_node,
      SearchOutput& search_output );

   
   ~SlaveDataManager(){}

   void operator()( const MPI::Intracomm& comm, const UINT4 num_nodes = 0 );
   
private:

   void communicateHostName( CHAR* const host );         
   void finalizeMessage( const MPI::Intracomm& comm, CHAR* const msg );               
   
   // Only master node has to send final process information to the 
   // resultAPI on job termination/completion.
   void sendFinalInfo( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = false ) { return; }
};
   
   
#endif   
