#ifndef WrapperApiDataBaseHH
#define WrapperApiDataBaseHH

// Local Header Files   
#include "ldasdatatype.hh"   
#include "ldasdata.hh"
#include "mpilaltypes.hh"   
#include "outputtype.hh"

   
//------------------------------------------------------------------------------
//
//: DataBaseType class.
// 
// This class is a wrapper class for the C dataBase linked list. The
// class does not contain linked list data, it only represents it
// MPI-wise for the communication between master and slaves.
//
class DataBaseType : public LdasDatatype, private LdasData
{
   
public:
   
   /* Constructor */
   DataBaseType( dataBase** pe, const UINT4 num, 
      const MPIDataBaseInfo* dbInfo,
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   /* Destructor */
   virtual ~DataBaseType();

   static ILwd::LdasElement* toILwd( const dataBase* pe,
      const MPI::Intracomm& comm ); 
   
private:
  friend OutputType::
  OutputType( outPut** pe,
	      const UINT4 num,
	      const MPI::Intracomm& comm,
	      const INT4 rank );

   //: No default constructor
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>
   //
   DataBaseType( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
      
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //   
   DataBaseType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>      
   //   
   DataBaseType( const DataBaseType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>      
   //   
   DataBaseType& operator=( const DataBaseType& re ); 
   
   /* Helper method: called by slaves only */
   void allocateListData( dataBase** re, 
      const MPIDataBaseInfo* dbInfo ) const;
   
   /* Helper methods: called by all nodes in communicator */
   void buildType( dataBase* re ); 

   virtual const CHAR* getDatatypeName() const;   
   
   //: Name to use for conversion to ILWD.
   static const std::string mILwdName;
   
   //: Number of MPI datatypes per dataBase node.
   static const UINT4 mTypeNum;   
   
   //: Number of characters to store table and column names
   static const UINT4 mNamesNum;   
   
   //: Number of nodes in the dataBase linked list.
   UINT4 mNum;
};

   
#endif // WrapperApiDataBaseHH  
