#ifndef WrapperApiMultiDimDataHH
#define WrapperApiMultiDimDataHH

#include "ldasdatatype.hh"   
#include "ldasdata.hh"
#include "mpilaltypes.hh"   
#include "statevectype.hh"

   
//------------------------------------------------------------------------------
//
//: MultiDimDataType class.
// 
// This class is a wrapper class for the array of C multiDimData structure. This 
// class does not contain any data, it only represents the data structures
// MPI-wise.   
//
class MultiDimDataType : public LdasDatatype, private LdasData
{
   
public:
   
   /* Constructor */
   MultiDimDataType( ilwdConstVector& seq_vec, multiDimData** mData, 
      const MPIStructInfo* dataInfo, const UINT4 num = 1, 
      const INT4 rank = -1, const MPI::Intracomm& comm = MPI::COMM_WORLD ); 
   
   MultiDimDataType( multiDimData** mData, const MPIStructInfo* info,
      const MPI::Intracomm& comm = MPI::COMM_WORLD, const INT4 rank = -1,
      const UINT4 num = 1 );
   
   /* Destructor */
   virtual ~MultiDimDataType();
   
   static ILwd::LdasElement* toILwd( const multiDimData& re, 
      const MPI::Intracomm& comm );
   static const std::string& getILwdName();   
   static const size_t getILwdNamePosition();      

private:
  friend OutputType::
  OutputType( outPut** pe,
	      const UINT4 num,
	      const MPI::Intracomm& comm,
	      const INT4 rank );

  friend StateVectorType::
  StateVectorType( ilwdConstVector& state_vec, stateVector** re,
		   const MPIStructInfo* stateInfo, const UINT4 num,
		   const INT4 rank,
		   const MPI::Intracomm& comm );
   
  friend StateVectorType::
  StateVectorType( stateVector** ps, const UINT4 num,
		   const MPIStructInfo* stateInfo, 
		   const MPI::Intracomm& comm,
		   const INT4 rank ); 

   //: No default constructor 
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   MultiDimDataType( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   MultiDimDataType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD, const INT4 rank = -1 ); 

   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   MultiDimDataType( const MultiDimDataType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   MultiDimDataType& operator=( const MultiDimDataType& re ); 
   
   /* Helper methods: called only by master node */
   void parseILwd( ilwdConstVector& re, ilwdVector* history_vec,
      multiDimData* mData ) const; 

   /* Helper methods: called by all nodes */   
   const UINT4 getNData( const multiDimData& rs ) const;

   void allocateData( multiDimData** mData, 
      const MPIStructInfo* dataInfo ) const;
   
   void buildType( std::vector< MPI::Datatype >& rType,
      std::vector< MPI::Datatype >& hType, multiDimData* mData );
   
   virtual const CHAR* getDatatypeName() const;      

   //: Name attribute to use for conversion to ILWD.
   static const std::string mILwdName;

   //: Name field position to use for conversion to ILWD.
   static const size_t mILwdNamePosition;   
   
   //: Number of MPI datatypes per multiDimData structure.
   static const UINT4 mTypeNum;
   
   //: Number of characters to store name, units and comment.   
   static const UINT4 mNameUnitsCommentNum;
   
   //: Number of elements in the array of multiDimData structures.
   UINT4 mNum;

};

   
// Helper functions declarations   
void storeMultiDimData( multiDimData* pd, const multiDimData* ps,
   const UINT4 num, const MPI::Intracomm& comm, const bool p2p );
   
void deleteMultiDimData( multiDimData* pd, const UINT4 num,
   const MPI::Intracomm& comm, const bool p2p = true );

   
#endif   
