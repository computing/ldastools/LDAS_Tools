#include "LDASConfig.h"

// Local Header Files
#include "nodeinfo.hh"
#include "mpiutil.hh"   
#include "mpilaltypes.hh"   
#include "initvars.hh"   

using namespace std;

   
// Static data initialization( be consistent with NodeError::LDAS_MAX_ERROR )   
const INT4 lbNodeInfo::msgDim( 16384 );   

   
//-------------------------------------------------------------------------------   
//         
//: Constructor.
// 
//!exc: bad_alloc - Memory allocation failed.
//!exc: MPI::Exception - MPI library exception.   
//   
lbNodeInfo::lbNodeInfo() 
   try :
   numResults( 0 ), rank( 0 ), deltaT( 0.0 ), fractionRemaining( 1.0 ),
   notFinished( true ), error( 0 ), warning( 0 ), nodeType( MPI::DATATYPE_NULL )
{
   error   = new CHAR[ msgDim ];
   warning = new CHAR[ msgDim ];   
   
   resetMessages(); 
   
   // Create MPI datatype to represent node information
   createMPIType();
}
catch(...)
{
   cleanup();
   throw;
}

   
//------------------------------------------------------------------------------
//
//: Destructor
//   
lbNodeInfo::~lbNodeInfo()
{
   cleanup();
}
   
   
//------------------------------------------------------------------------------
//
//: Creates MPI datatype to represent lbNodeInfo structure.
//      
//!exc: bad_alloc - Error allocating memory.
//!exc: MPI::Exception - MPI library exception.   
//!exc: Error generating lbNodeInfo MPI datatype. - Could not generate MPI     
//+     datatype to represent lbNodeInfo structure.   
//   
void lbNodeInfo::createMPIType() 
try   
{   
   static const INT4 dim( 7 );
   
   INT4 bs[ dim ] = { 1, 1, 1, 1, 1, msgDim, msgDim };
   
   MPI::Datatype ts[ dim ] = { LDAS_MPI_UINT4,
                               LDAS_MPI_INT4,
                               LDAS_MPI_REAL8,
                               LDAS_MPI_REAL4,
                               LDAS_MPI_BOOLEAN,
                               LDAS_MPI_CHAR, 
                               LDAS_MPI_CHAR };
   
   
   MPI::Aint ds[ dim ] = { MPI::Get_address( &numResults ),
                           MPI::Get_address( &rank ),
                           MPI::Get_address( &deltaT ),
                           MPI::Get_address( &fractionRemaining ),
                           MPI::Get_address( &notFinished ),
                           MPI::Get_address( error ),
                           MPI::Get_address( warning ) };

   
   for( INT4 i = dim - 1; i >= 0; --i )
   {
      ds[ i ] -= ds[ 0 ];
   }
   
   
   nodeType = MPI::Datatype::Create_struct( dim, bs, ds, ts );
   
   
   if( nodeType == MPI::DATATYPE_NULL )
   {
      cleanup();   
      node_error( "Error generating lbNodeInfo MPI datatype.",
                  MPI::COMM_WORLD, false );
   }
   
   nodeType.Commit();
   
   return;
}
catch(...)
{
   cleanup();
   throw;
}

   
//------------------------------------------------------------------------------
//
//: Cleanup utility.
//   
// Releases all dynamically allocated memory.
//   
void lbNodeInfo::cleanup()
{
   delete[] error;
   error = 0;
   
   delete[] warning;
   warning = 0;
}

   
//------------------------------------------------------------------------------
//
//: Sets error and warning messages for the node.
//
// This method sets error and warning messages for the node before sending the
// node information to the master node.
//
//!param: const DLErrorHandler& eh - A reference to the dynamic library error  
//+       handler.   
//   
void lbNodeInfo::setMessages( const DLErrorHandler& eh )
{
   resetMessages();
   
   // Set warning message
   INT4 msg_size( 0 );
   
   INT4 eh_size( eh.getWarningMsgSize() );
   
   
   if( eh_size )
   {
      msg_size = ( eh_size >= msgDim ) ?
                 msgDim - 1 : eh_size;
      const string& eh_msg( eh.getWarningMsg() );

      strncpy( warning, eh_msg.c_str(), msg_size );
   }

   
   // Set error message      
   eh_size = eh.getErrorMsgSize();
   
   if( eh_size ) 
   {
      msg_size = ( eh_size >= msgDim ) ?
                 msgDim - 1 : eh_size;
   
      const string& eh_msg( eh.getErrorMsg() );
      strncpy( error, eh_msg.c_str(), msg_size );
   }
   
   return;
}  

   
//-------------------------------------------------------------------------------   
//         
//: Reset messages.
//   
// This method clears messages to the empty strings.
//   
void lbNodeInfo::resetMessages()
{
   memset( error, 0, sizeof( CHAR ) * msgDim );      
   memset( warning, 0, sizeof( CHAR ) * msgDim );
   
   return;
}
   

