#ifndef WrapperApiStateVectorTypeHH
#define WrapperApiStateVectorTypeHH
   
// Local Header Files   
#include "ldasdatatype.hh"   
#include "mpilaltypes.hh"   
#include "outputtype.hh"
   

//------------------------------------------------------------------------------
//
//: StateVectorType class.
// 
// This class represents MPI datatype corresponding to the "C" stateVector 
// linked list. This class does not contain any data, it only represents the
// data MPI-wise.   
//
class StateVectorType : public LdasDatatype
{
   
public:
   
   /* Constructor */
   StateVectorType( ilwdConstVector& state_vec, stateVector** re,
      const MPIStructInfo* stateInfo, const UINT4 num,
      const INT4 rank = -1,
      const MPI::Intracomm& comm = MPI::COMM_WORLD );
   
   StateVectorType( stateVector** ps, const UINT4 num,
      const MPIStructInfo* stateInfo, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 ); 
   
   /* Destructor */
   virtual ~StateVectorType();
   
   static ILwd::LdasElement* toILwd( const stateVector* pe,
      const MPI::Intracomm& comm );
   static const std::string& getILwdName();
   static const size_t getILwdNamePosition();   
   
private:
  friend OutputType::
  OutputType( outPut** pe,
	      const UINT4 num,
	      const MPI::Intracomm& comm,
	      const INT4 rank );
   //: No default constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //                  
   StateVectorType( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //                  
   StateVectorType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //                  
   StateVectorType( const StateVectorType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //                  
   StateVectorType& operator=( const StateVectorType& re );
   
   /* Helper methods: called only by master node. */
   void parseILwd( ilwdConstVector& re, stateVector* ps ) const; 
   
   /* Helper methods: called by all nodes */
   void allocateListData( stateVector** ps ) const; 
   
   void buildType( std::vector< MPI::Datatype >& sType, stateVector* ps );
   
   virtual const CHAR* getDatatypeName() const;   
  
   //: Name attribute to use for conversion to ILWD.   
   static const std::string mILwdName;
   
   //: Name field position to use for conversion to ILWD.
   static const size_t mILwdNamePosition;      
   
   //: Attribute name to store state name
   static const std::string mILwdStateNameAtt;
   
   //: Number of MPI datatypes per stateVector structure.   
   static const UINT4 mTypeNum;   
   
   //: Number of nodes in the stateVector linked list
   UINT4 mNum;

};
  
   
#endif   
