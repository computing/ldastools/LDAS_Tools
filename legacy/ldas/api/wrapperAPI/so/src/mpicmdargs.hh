#ifndef WrapperApiCmdArgumentsHH
#define WrapperApiCmdArgumentsHH

// System Header Files   
#include <string>   
   
// MPI Header Files
#include <mpi.h>   
   
// Local Header Files
#include "LALAtomicDatatypes.h"   

   
// Load balancing enumeration   
enum LDAS_LB_STATE
{
   LDAS_LB_SUBTRACT = -1,
   LDAS_LB_CONTINUE,
   LDAS_LB_ADD,
   LDAS_LB_KILL
};
   
   
// Global variables   
extern INT4 myNode;
extern INT4 totalNodes;
extern const INT4 masterNode;
   
#ifndef MpiCmdArgumentsCC
#define MPI_CMD_ARGS extern
#else
#define MPI_CMD_ARGS   
#endif

   
MPI_CMD_ARGS std::string error_msg;   
MPI_CMD_ARGS std::string warning_msg;      


#endif   
