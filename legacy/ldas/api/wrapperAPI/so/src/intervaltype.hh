#ifndef WrapperApiIntervalHH
#define WrapperApiIntervalHH

// System Header Files   
#include <list>   

// GenericAPI header files   
#include <genericAPI/ChannelNameLexer.hh>   

using GenericAPI::ChannelNameLexer;   
   
// Local Header Files   
#include "ldasdatatype.hh"
#include "multidimdata.hh"
   
   
//------------------------------------------------------------------------------
//
//: IntervalType class.
// 
// This class represents MPI derived datatype corresponding to the C interval 
// union.
//   
class IntervalType : public LdasDatatype
{
   
public:
   
   /* Constructor */
   IntervalType( const ILwd::LdasElement* pe, const domain& space,
      interval& ri, const UINT4 num_samples, const UINT4 num_channels,
      const UINT4 channel_size, const UINT4 num_detectors, 
      const INT4 rank = -1, const MPI::Intracomm& comm = MPI::COMM_WORLD );
   
   IntervalType( const domain& space, interval& ri, 
      const UINT4 num_channels, const UINT4 channel_size,
      const UINT4 num_detectors, const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   /* Destructor */
   virtual ~IntervalType();

   static ilwdResultVector toILwd( const domain& space, const interval& ri,
      const MPI::Intracomm& comm, std::list< std::string >& channel_name,
      std::string& comment ); 
   
   static const UINT4 getChannelNameInfo( const ILwd::LdasElement* pe,
      UINT4& size );   
   
private:
  friend MultiDimDataType::
  MultiDimDataType( ilwdConstVector&, multiDimData**, 
		    const MPIStructInfo*, const UINT4, 
		    const INT4 rank, const MPI::Intracomm& comm); 
  friend MultiDimDataType::
  MultiDimDataType( multiDimData**, const MPIStructInfo* info,
		    const MPI::Intracomm& comm, const INT4 rank,
		    const UINT4 num );

   // Helpful typedefs
   typedef ChannelNameLexer::data_list_type channelMap;
   typedef ChannelNameLexer::data_list_type::const_iterator const_channel_iterator;

   static void populateChannelParser( ChannelNameLexer& parser, 
      const ILwd::LdasElement* pe );
   
   //: No default constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //            
   IntervalType( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
    
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //            
   IntervalType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //            
   IntervalType( const IntervalType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   IntervalType& operator=( const IntervalType& re ); 
   
   /* Helper methods, called only by master node. */
   void parseChannelName( const ILwd::LdasElement* pe, interval& ri );   
   
   void initNoneInterval( rawSequence& re, const UINT4 num_samples ) const;   
   
   void initTimeInterval( const ILwd::LdasElement& pe, 
      gpsTimeInterval& re, const UINT4 num_samples ) const;
   
   void initFreqInterval( const ILwd::LdasElement& pe,
      frequencyInterval& re, const UINT4 num_samples ) const;
   
   void initBothInterval( const ILwd::LdasElement& pe,
      timeFreqInterval& re, const UINT4 num_samples ) const;
   
   static void initDatabaseInterval( databaseInterval& re,
      const UINT4 num_rows, const std::string& sql );    

   static ilwdResultVector timeToILwd( const gpsTimeInterval& re,
      const MPI::Intracomm& comm );

   static ilwdResultVector freqToILwd( const frequencyInterval& re,
      const MPI::Intracomm& comm );
   
   static ilwdResultVector bothToILwd( const timeFreqInterval& re,
      const MPI::Intracomm& comm );
   
   const REAL8 getREAL8Value( const ILwd::LdasContainer& pe,
      const CHAR* name, const CHAR* domain_name ) const;
   
   /* Methods called by all nodes in communicator */
   void allocateData(  interval& ri );

   // Build MPI datatype
   void buildType( interval& ri );

   virtual const CHAR* getDatatypeName() const;   
   
   //: Number of MPI datatypes per interval structure.
   static const INT4 mTypeNum;
   
   //: Name field to be parsed for channels in addition to the ones 
   //: after the 'primary' position.
   static const INT4 mInitChannelPos;
   
   //: Number of channel names
   const UINT4 mNumberChannels;
   
   //: Size of the buffer to store channel names
   const UINT4 mChannelSize;
   
   //: Number of detector geometry structures
   const UINT4 mNumberDetectors;      
   
   //: Domain of the interval
   domain mDomain;
};


// Helper functions declarations      
void storeInterval( const domain& d, interval& pd, const interval& ps,
   const MPI::Intracomm& comm, const bool p2p );   
   
void deleteInterval( const domain& d, interval& pd, 
   const MPI::Intracomm& comm, const bool p2p = true );   


// Constant values specific to interval datatype
const CHAR* const NoneDomainString( "NONE" );   
const CHAR* const TimeDomainString( "TIME" );   
const CHAR* const FreqDomainString( "FREQ" );      
const CHAR* const BothDomainString( "BOTH" );      
const CHAR* const DatabaseDomainString( "DATABASE" );      
   
   
#endif   
