#ifndef WrapperApiLoadBalanceHH
#define WrapperApiLoadBalanceHH

#include "LDASConfig.h"

// System Header Files
#include <vector>

// MPI Header Files   
#if HAVE_MPI___H
#include <mpi++.h>
#elif HAVE_MPICXX_H
#include <mpicxx.h>
#endif
   
// Local Header Files
#include "mpicmdargs.hh"   
   

//-------------------------------------------------------------------------------   
//   
//: LDAS load balancing functional.
//   
// This functional when instantiated by master node in the COMM_WORLD communicator
// does actual load balancing of the parallel processing job.
// Master makes a decision if load balancing should be done based on the progress
// report received from the search master of the parallel job. If load balancing is 
// disabled or there is no need for it, then master just sends a process report 
// to the mpiAPI. It does all necessary communication to the mpiAPI for the 
// completion of all requests.   
//
class LdasLoadBalance
{
public:
   
   LdasLoadBalance();
   ~LdasLoadBalance();
   
   const INT4 operator()( const REAL4 num, const REAL4 ratio, 
      const REAL4 progress, BOOLEAN& not_finished,
      MPI::Intracomm& lbcomm, MPI::Intracomm& master_lbcomm,
      MPI_Comm* c_lbcomm, const std::string& war_msg = "", 
      const std::string& err_msg = "" );
   
   const bool isLastCall() const;
   const INT4 getNumNodes() const;
   const std::vector< INT4 > getNodes() const; 
   
   static void setSynchronize( const bool flag );
   static const REAL8 getTimeSpent();
   
private:

   enum StateInfo
   {
      IDLE = 0,
      SEND,
      KILL
   };
   
   // Helper methods, called by all nodes
   INT4 getMPIAction() const;   
   void rebuildCommunicators( MPI::Intracomm& lbcomm, 
      MPI::Intracomm& master_lbcomm, MPI_Comm* c_lbcomm );   
   const bool isAsynchronous( const bool& not_finished ) const;
   
   // Helper methods, called by the master node only.
   void packContinueMessage( const BOOLEAN& not_finished );   
   void sendContinueMessage( const MPI::Intracomm& master_lbcomm );      
   
   // Helper methods, called by the slave nodes only
   void receiveContinueMessage( const MPI::Intracomm& master_lbcomm,
      const MPI::Intracomm& lbcomm, BOOLEAN& not_finished );      
   
   //: Flag, if set to true, will synchronize mpiAPI response messages
   //: with "continue" instruction to the searchMaster of dso.
   static bool mSynchronize;
   
   //: This is a total time spent in load balancing functional.
   static REAL8 mTimeSpent;
   
   //: State of the load balancing( set by wrapperMaster ).
   LDAS_LB_STATE mState;
   
   //: State of the mpiAPI response message.
   LDAS_LB_STATE mMPIState;   

   //: Number of nodes to add/subtract.
   UINT4 mNum;   
   
   //: Nodes to add/subtract to/from LB communicator.
   std::vector< INT4 > mNodes;

   //: Size of the buffer used for node communication.
   INT4  mBufferSize;
   
   //: Buffer used for node communication.
   CHAR* mBuffer;

   //: This flag is only for handling mpiAPI "kill" command.
   StateInfo mKillState;
};

   
//------------------------------------------------------------------------------
//
//: isLastCall check.
//
// This method checks if mpiAPI responded with kill command to the previous
// request.   
// 
//!return: const bool - True if mpiAPI issued kill command, false otherwise.
//    
inline const bool LdasLoadBalance::isLastCall() const 
{
   if( mSynchronize )
   {
      return( mMPIState == LDAS_LB_KILL );
   }
   
   return( mKillState == SEND );
}
   
   
//------------------------------------------------------------------------------
//
//: Gets number of nodes to add/subtract to/from the communicator.
//
// This method will return the number of nodes to be added/subtracted
// to/from the communicator if load balancing is enabled.   
//   
//!return: const INT4 - Number of nodes to add/subtract. 
//   
inline const INT4 LdasLoadBalance::getNumNodes() const
{
   return static_cast< INT4 >( mNodes.size() );
}

  
//------------------------------------------------------------------------------
//
//: Gets nodes to add/subtract to/from the communicator.
//   
//!return: const std::vector< INT4 > - A vector of nodes to add/subtract to/from
//+        the communicator.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
inline const std::vector< INT4 > LdasLoadBalance::getNodes() const
{
   return mNodes;
}
   
   
#endif   
