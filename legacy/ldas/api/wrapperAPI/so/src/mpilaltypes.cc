#include "LDASConfig.h"

#define WrapperAPILalTypesCC
#include <new>
#include <exception>
#include <iostream>
#include <algorithm>   
   
#include "mpilaltypes.hh"
#include "ldaserror.hh"   
#include "initvars.hh"   
   
const INT4  listMaxNodes( 100 );
const UINT4 dbMaxDims( 50 ); 
const INT4  multiDimDataMaxDims( 50 );

using namespace std;   
   
//#define TEST_CONTIGUOUS_HELPER_TYPES     
   
   
//------------------------------------------------------------------------------
//
//: Creates specific MPI datatypes.   
//   
// Since MPICH does not have 8-byte integer datatypes this method generates them
// in order to be able to build new datatypes with INT8 and UINT8 types in their
// content. User has to take care of deleting these new datatypes by calling 
// deleteLdasTypes() function.
//   
//!exc: Error generating LDAS_MPI_COMPLEX8 type. - Could not create             
//+     LDAS_MPI_COMPLEX8 type.   
//!exc: Error generating LDAS_MPI_COMPLEX16 type. - Could not create            
//+     LDAS_MPI_COMPLEX16 type.      
//!exc: Error generating LDAS_MPI_INT8 type. - Could not create LDAS_MPI_INT8   
//+     type.
//!exc: Error generating LDAS_MPI_UINT8 type. - Could not create LDAS_MPI_UINT8 
//+     type.   
//!exc: Error generating LDAS_MPI_LIST_INFO datatype. - Could not create        
//+     LDAS_MPI_LIST_INFO datatype.   
//!exc: Error generating LDAS_MPI_STRUCT_INFO datatype. - Could not create      
//+     LDAS_MPI_STRUCT_INFO datatype.      
//!exc: Error generating LDAS_MPI_DB_INFO datatype. - Could not create 
//+     LDAS_MPI_DB_INFO datatype.   
//!exc: bad_alloc - Memory allocation failed.   
//!exc: MPI::Exception - MPI library exception.   
//   
void createLdasTypes()
{
   if( enableLdasTrace )
   {
      std::cout << "Creating specific to LDAS MPI datatypes...";
   }
   
   
   // Create complex datatypes:
   LDAS_MPI_COMPLEX8 = LDAS_MPI_REAL4.Create_contiguous( 2 );
   
   if( LDAS_MPI_COMPLEX8 == MPI::DATATYPE_NULL )
   {
      node_error( "Error generating LDAS_MPI_COMPLEX8 type.",
                  MPI::COMM_WORLD, false );
   }
   
   LDAS_MPI_COMPLEX8.Commit();
   
   
   LDAS_MPI_COMPLEX16 = LDAS_MPI_REAL8.Create_contiguous( 2 );   
   
   if( LDAS_MPI_COMPLEX16 == MPI::DATATYPE_NULL )
   {   
      node_error( "Error generating LDAS_MPI_COMPLEX16 type.",
                  MPI::COMM_WORLD, false );   
   }
   
   LDAS_MPI_COMPLEX16.Commit();      
   
   
   // Create 8 byte integer datatypes:
   if( SIZEOF_LONG == 8 )
   {
      LDAS_MPI_INT8 = MPI::LONG;
      LDAS_MPI_UINT8 = MPI::UNSIGNED_LONG;
   }
   else if( SIZEOF_LONG_LONG == 8 )
   {
      LDAS_MPI_INT8 = LDAS_MPI_INT4.Create_contiguous( 2 );
   
      if( LDAS_MPI_INT8 == MPI::DATATYPE_NULL )
      {
         node_error( "Error generating LDAS_MPI_INT8 type.",
                     MPI::COMM_WORLD, false );   
      }
   
      LDAS_MPI_INT8.Commit();
   
   
      LDAS_MPI_UINT8 = LDAS_MPI_UINT4.Create_contiguous( 2 );
   
      if( LDAS_MPI_UINT8 == MPI::DATATYPE_NULL )
      {
         node_error( "Error generating LDAS_MPI_UINT8 type.",
                     MPI::COMM_WORLD, false );   
      }
   
      LDAS_MPI_UINT8.Commit();
   }

#ifdef TEST_CONTIGUOUS_HELPER_TYPES

   // Create LDAS_MPI_LIST_INFO datatype
   MPIListInfo listInfo;
   
   const UINT4 list_dim( 2 );
   LDAS_MPI_LIST_INFO = LDAS_MPI_INT4.Create_contiguous( list_dim );
   
   if( LDAS_MPI_LIST_INFO == MPI::DATATYPE_NULL )
   {
      node_error( "Error generating LDAS_MPI_LIST_INFO datatype.",
                  MPI::COMM_WORLD, false );
   }
   
   LDAS_MPI_LIST_INFO.Commit();
   

   // Create LDAS_MPI_STRUCT_INFO datatype
   MPIStructInfo structInfo;
   const UINT4 struct_dim( 7 );

   LDAS_MPI_STRUCT_INFO = LDAS_MPI_INT4.Create_contiguous( struct_dim );
   
   if( LDAS_MPI_STRUCT_INFO == MPI::DATATYPE_NULL )
   {
      node_error( "Error generating LDAS_MPI_STRUCT_INFO datatype.",
                  MPI::COMM_WORLD, false );
   }
   
   LDAS_MPI_STRUCT_INFO.Commit();

   
#else   
   
   //cout << " Using mpi struct datatypes for metadata...";      
   
   // Create LDAS_MPI_LIST_INFO datatype
   {
      MPIListInfo listInfo;
   
      const UINT4   dim( 2 );
      INT4          blocks[ dim ] = { 1, 1 };
      MPI::Datatype types[ dim ] = { LDAS_MPI_INT4, LDAS_MPI_UINT4 };
      MPI::Aint     disps[ dim ] = { 0,
                                     MPI::Get_address( &listInfo.numData ) -
                                     MPI::Get_address( &listInfo.type ) };

      LDAS_MPI_LIST_INFO = MPI::Datatype::Create_struct( dim, blocks, 
                              disps, types );
   
      if( LDAS_MPI_LIST_INFO == MPI::DATATYPE_NULL )
      {
         node_error( "Error generating LDAS_MPI_LIST_INFO datatype.",
                     MPI::COMM_WORLD, false );
      }
   
      LDAS_MPI_LIST_INFO.Commit();
   }
   
   
   // Create LDAS_MPI_STRUCT_INFO datatype
   {
      MPIStructInfo structInfo;
   
      const UINT4 dim( 7 );
      INT4  blocks[ dim ];
      fill( blocks, blocks + dim, 1 );

      MPI::Datatype types[ dim ] = { LDAS_MPI_INT4,
                                     LDAS_MPI_INT4,
                                     LDAS_MPI_UINT4,
                                     LDAS_MPI_UINT4, 
                                     LDAS_MPI_UINT4, 
                                     LDAS_MPI_UINT4,
                                     LDAS_MPI_UINT4 };   
   
      MPI::Aint disps[ dim ] = { MPI::Get_address( &structInfo.space ),
                                 MPI::Get_address( &structInfo.type ),
                                 MPI::Get_address( &structInfo.numHistory ),
                                 MPI::Get_address( &structInfo.numberDimensions ),
                                 MPI::Get_address( &structInfo.numberChannels ),
                                 MPI::Get_address( &structInfo.channelSize ),
                                 MPI::Get_address( &structInfo.numberDetectors ) };
   
      for( INT4 i = dim - 1; i >= 0; --i )
      {
         disps[ i ] -= disps[ 0 ];
      }   

      LDAS_MPI_STRUCT_INFO = MPI::Datatype::Create_struct( dim, blocks, 
                                disps, types );
   
      if( LDAS_MPI_STRUCT_INFO == MPI::DATATYPE_NULL )
      {
         node_error( "Error generating LDAS_MPI_STRUCT_INFO datatype.",
                     MPI::COMM_WORLD, false );
      }
   
      LDAS_MPI_STRUCT_INFO.Commit();
   }
   
#endif   
   
   if( enableLdasTrace )
   {
      std::cout << "done." << std::endl;
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: deleteLdasTypes.
//
// This function destroys datatypes created by createLdasTypes() function.
//
//!return: Nothing.
//
//!exc: MPI::Exception - MPI library exception.
//   
void deleteLdasTypes()
{
   // Check if datatypes were created:
   if( SIZEOF_LONG_LONG == 8 )
   {
      if( LDAS_MPI_INT8 != MPI::DATATYPE_NULL )
      {
         LDAS_MPI_INT8.Free();
      }
   
      if( LDAS_MPI_UINT8 != MPI::DATATYPE_NULL )
      {
         LDAS_MPI_UINT8.Free();
      }
   }

   
   if( LDAS_MPI_COMPLEX8 != MPI::DATATYPE_NULL )
   {
      LDAS_MPI_COMPLEX8.Free();
   }
   
   
   if( LDAS_MPI_COMPLEX16 != MPI::DATATYPE_NULL )
   {
      LDAS_MPI_COMPLEX16.Free();
   }   

   
   if( LDAS_MPI_LIST_INFO != MPI::DATATYPE_NULL )
   {
       LDAS_MPI_LIST_INFO.Free();
   }   
   
   
   if( LDAS_MPI_STRUCT_INFO != MPI::DATATYPE_NULL )
   {
       LDAS_MPI_STRUCT_INFO.Free();
   }   
   
   
   return;
}
   
