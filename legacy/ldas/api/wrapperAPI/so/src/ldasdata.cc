#include "LDASConfig.h"

// System Header Files
#include <new>
#include <string>   
#include <string.h> 
   
// ILwd Header Files
#include <ilwd/ldasarray.hh>  
#include <ilwd/ldasstring.hh>
#include <ilwd/ldascontainer.hh>   
   
using ILwd::LdasElement;  
using ILwd::LdasArray; 
using ILwd::LdasString;   
using ILwd::LdasContainer;   
   
// Local Header Files   
#include "ldasdata.hh"
#include "mpilaltypes.hh"   
#include "ldaserror.hh"
#include "initvars.hh"   
   
using namespace std;   
using ILwd::ID_LSTRING;
   
   
//------------------------------------------------------------------------------
//
//: Default constructor.
//   
LdasData::LdasData()
{}
   
   
//-----------------------------------------------------------------------------
//   
//: Allocate memory for data array.
//
// This method allocates memory for the data array based on the datatype and
// number of elements passed to it.
//!ignore_begin:   
// Use 'std' namespace in method arguments( even though we said
// 'using namespace std' ) because perceps won't create proper link 
// for the documentation if we don't use it.
//!ignore_end:         
//   
//!param: dataPointer* data - A pointer to the data array.
//!param: const datatype& type - A reference to the data type of the array data.
//!param: const UINT4 num - Number of elements in the array.
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD.      
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.      
//   
//!exc: bad_alloc - Memory allocation failed.   
//!exc: Undefined datatype.- Illegal datatype was specified.
//   
void LdasData::allocateDataArray( dataPointer* data, 
   const datatype& type, const UINT4 num,
   const MPI::Intracomm& comm, const bool p2p )
{
   if( num == 0 )
   {
      return;
   }
   
   
   switch( type )
   {
      case(boolean_lu):
      {
         ( *data ).boolean = new BOOLEAN[ num ];
         memset( ( *data ).boolean, 0, num * sizeof( BOOLEAN ) );
         break;
      }
      case(char_s):
      {
         ( *data ).chars = new CHAR[ num ];
         memset( ( *data ).chars, 0, num * sizeof( CHAR ) );   
         break;
      }   
      case(char_u):
      {
         ( *data ).charu = new UCHAR[ num ];
         memset( ( *data ).charu, 0, num * sizeof( UCHAR ) );   
         break;
      }   
      case(int_2s):
      {
         ( *data ).int2s = new INT2[ num ];
         memset( ( *data ).int2s, 0, num * sizeof( INT2 ) );   
         break;
      }   
      case(int_2u):
      {
         ( *data ).int2u = new UINT2[ num ];
         memset( ( *data ).int2u, 0, num * sizeof( UINT2 ) );      
         break;
      }   
      case(int_4s):
      {
         ( *data ).int4s = new INT4[ num ];
         memset( ( *data ).int4s, 0, num * sizeof( INT4 ) );      
         break;
      }   
      case(int_4u):
      {
         ( *data ).int4u = new UINT4[ num ];
         memset( ( *data ).int4u, 0, num * sizeof( UINT4 ) );      
         break;
      }      
      case(int_8s):
      {
         ( *data ).int8s = new INT8[ num ];
         memset( ( *data ).int8s, 0, num * sizeof( INT8 ) );      
         break;
      }      
      case(int_8u):
      {
         ( *data ).int8u = new UINT8[ num ];
         memset( ( *data ).int8u, 0, num * sizeof( UINT8 ) );      
         break;
      }      
      case(real_4):
      {
         ( *data ).real4 = new REAL4[ num ];
         memset( ( *data ).real4, 0, num * sizeof( REAL4 ) );      
         break;
      }      
      case(real_8):
      {
         ( *data ).real8 = new REAL8[ num ];
         memset( ( *data ).real8, 0, num * sizeof( REAL8 ) );      
         break;
      }      
      case(complex_8):
      {
         ( *data ).complex8 = new COMPLEX8[ num ];
         memset( ( *data ).complex8, 0, num * sizeof( COMPLEX8 ) );      
         break;
      }      
      case(complex_16):
      {
         ( *data ).complex16 = new COMPLEX16[ num ];
         memset( ( *data ).complex16, 0, num * sizeof( COMPLEX16 ) );      
         break;
      }         
      case(char_s_ptr):
      {
         ( *data ).chars = new CHAR[ num ];
         memset( ( *data ).chars, 0, num * sizeof( CHAR ) );   
         break;
      }   
      case(char_u_ptr):
      {
         ( *data ).charu = new UCHAR[ num ];
         memset( ( *data ).charu, 0, num * sizeof( UCHAR ) );   
         break;
      }      
      default:
      {
         node_error( "Undefined datatype.", comm, p2p );
         break;
      }
   }   
   
   
   return;
}

   
//-----------------------------------------------------------------------------
//   
//: Delete memory for data array.
//
// This method deletes dynamically allocated memory for the data array.
//   
//!param: dataPointer* data - A pointer to the data array.
//!param: const datatype& type - A reference to the data type of the array    
//+       data.   
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD.      
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.         
//
//!exc: Undefined datatype.- Illegal datatype was specified.
//   
void LdasData::deleteDataArray( dataPointer* data, const datatype& type,
   const MPI::Intracomm& comm, const bool p2p )
{
   if( data == 0 )
   {
      return;
   }

   
   switch( type )
   {
      case(boolean_lu):
      {
         delete[] data->boolean;
         data->boolean = 0;
         break;
      }
      case(char_s):
      {
         delete[] data->chars;
         data->chars = 0;
         break;
      }   
      case(char_u):
      {
         delete[] data->charu;
         data->charu = 0;
         break;
      }   
      case(int_2s):
      {
         delete[] data->int2s;
         data->int2s = 0;
         break;
      }   
      case(int_2u):
      {
         delete[] data->int2u;
         data->int2u = 0;   
         break;
      }   
      case(int_4s):
      {
         delete[] data->int4s;
         data->int4s = 0;   
         break;
      }   
      case(int_4u):
      {
         delete[] data->int4u;
         data->int4u = 0;   
         break;
      }      
      case(int_8s):
      {
         delete[] data->int8s;
         data->int8s = 0;
         break;
      }      
      case(int_8u):
      {
         delete[] data->int8u;
         data->int8u = 0;   
         break;
      }      
      case(real_4):
      {
         delete[] data->real4;
         data->real4 = 0;   
         break;
      }      
      case(real_8):
      {
         delete[] data->real8;
         data->real8 = 0;
         break;
      }      
      case(complex_8):
      {
         delete[] data->complex8;
         data->complex8 = 0;
         break;
      }      
      case(complex_16):
      {
         delete[] data->complex16;
         data->complex16 = 0;
         break;
      }         
      case(char_s_ptr):
      {
         delete[] data->chars;
         data->chars = 0;
         break;
      }   
      case(char_u_ptr):
      {
         delete[] data->charu;
         data->charu = 0;
         break;
      }      
      default:
      {
         node_error( "Undefined datatype.", comm, p2p );
         break;
      }
   }   
   
   
   return;
}


//-----------------------------------------------------------------------------
//   
//: Convert data array to the ILWD format element.
//
// This method converts data array to the corresponding ILWD format element.
//
//!ignore_begin:   
// Use 'std' and 'ILwd' namespaces in method arguments( even though we said
// 'using namespace std' ) because perceps won't create proper link 
// for the documentation if we don't use it.
//!ignore_end:      
//   
//!param: const datatype& type - A reference to the data type of the array.
//!param: const UINT4 ndim - A number of dimensions in the array.
//!param: const UINT4* dim - An array of dimensions.   
//!param: const dataPointer& data - A reference to the data array.
//!param: const string& name - A reference to the array name. Default is      
//+       empty string.
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD.   
//!param: const string& units - A reference to the array units. Default is    
//+       empty string.   
//
//!return: LdasElement* - A pointer to the ILWD format element.
//   
//!exc: bad_alloc - Memory allocation failed.   
//!exc: Undefined datatype. - Undefined datatype was specified.   
//!exc: NULL dimensions array when number of dimensions != 0. - Dimensions
//+     array is null while number of dimensions is not 0.  
//!exc: The number of dimensions was zero or dims was null. - Illegal
//+     dimension values has been specified to generate ILWD format
//+     element.   
//   
ILwd::LdasElement* LdasData::dataToILwd( const datatype& type,
   const UINT4 ndim, const UINT4* dim, const dataPointer& data,
   const MPI::Intracomm& comm, const std::string& name, 
   const std::string& unit )
{
   string* units( 0 );
   ILwd::LdasArrayBase::size_type* dims( 0 );
   
   // Check if number of dimensions is not zero
   if( ndim )
   {
      if( dim == 0 )
      {
         node_error( "NULL dimensions array when number of dimensions != 0.",
                     comm );
      }
   
   
      if( type != char_s_ptr && type != char_u_ptr )
      {
         units = new string[ ndim ];
   
         if( unit.empty() == false )
         {
            units[ 0 ] = unit;
         }

         try
         {
            dims = new ILwd::LdasArrayBase::size_type[ ndim ];
         }
         catch(...)
         {
            delete[] units;
            throw;
         }
   
         ILwd::LdasArrayBase::size_type*
	   dims_elem( dims ),
	   *dims_end( dims + ndim - 1 );
         const UINT4*  dim_elem( dim );

   
         while( dims_elem <= dims_end )
         {
            *dims_elem = *dim_elem;
            ++dims_elem;
            ++dim_elem;
         }
      }
   }
   
   
   LdasElement* e( 0 );   

   try
   {
      switch( type )
      {
         case(boolean_lu):
         {
            e = new LdasArray< BOOLEAN >( data.boolean, ndim, dims, name, units );

            break;
         }
         case(char_s):
         {
            const UINT4 num( getNDim( ndim, dim ) );
            bool all_printable( false );      
   
            // If there are any data elements
            if( num )
            {
               all_printable = true;
   
               CHAR* data_elem( data.chars ), 
                    *data_end( data.chars + num - 1 );
   
               while( all_printable && ( data_elem <= data_end ) )
               {
                  // Allow new line and tab 
                  all_printable = all_printable && 
                                  ( isprint( *data_elem ) ||
                                    *data_elem == '\n' ||
                                    *data_elem == '\t' );
   
                  ++data_elem;
               }   
            }
   
   
            if( all_printable )
            {
               // Create ILWD lstring 
               string s( data.chars, num );
               e = new LdasString( s, name, "", unit );
            }
            else
            {
               // Create ILWD array
               e = new LdasArray< CHAR >( data.chars, ndim, dims, name, units );
            }

            break;
         }   
         case(char_u):
         {
            e = new LdasArray< CHAR_U >( data.charu, ndim, dims, name, units );

            break;
         }   
         case(int_2s):
         {
            e = new LdasArray< INT_2S >( data.int2s, ndim, dims, name, units );

            break;
         }   
         case(int_2u):
         {
            e = new LdasArray< INT_2U >( data.int2u, ndim, dims, name, units );

            break;
         }   
         case(int_4s):
         {
            e = new LdasArray< INT_4S >( data.int4s, ndim, dims, name, units );   

            break;
         }   
         case(int_4u):
         {
            e = new LdasArray< INT_4U >( data.int4u, ndim, dims, name, units );   

            break;
         }      
         case(int_8s):
         {
            e = new LdasArray< INT_8S >( data.int8s, ndim, dims, name, units );   

            break;
         }      
         case(int_8u):
         {
            e = new LdasArray< INT_8U >( data.int8u, ndim, dims, name, units );   

            break;
         }      
         case(real_4):
         {
            e = new LdasArray< REAL_4 >( data.real4, ndim, dims, name, units );   

            break;
         }      
         case(real_8):
         {
            e = new LdasArray< REAL_8 >( data.real8, ndim, dims, name, units );      

            break;
         }      
         case(complex_8):
         {
            const UINT4 num( getNDim( ndim, dim ) );
            COMPLEX_8* a( 0 );
   
            // If there are any data elements
            if( num )
            {
               a = new COMPLEX_8[ num ];
   
               COMPLEX_8* a_elem( a ), *a_end( a + num - 1 );   
               COMPLEX8*  n( data.complex8 );
   
               while( a_elem <= a_end )
               {
                  *a_elem = COMPLEX_8( n->re, n->im );
                  ++a_elem;
                  ++n;
               }
            }

            try
            {
               e = new LdasArray< COMPLEX_8 >( a, ndim, dims, name, units );
            }
            catch(...)
            {
               delete[] a;
               throw;
            }
   
            break;
         }      
         case(complex_16):
         {
            const UINT4 num( getNDim( ndim, dim ) );
            COMPLEX_16* a( 0 );
   
            // If there are any data elements
            if( num )
            {
               a = new COMPLEX_16[ num ];
               COMPLEX_16* a_elem( a ), *a_end( a + num - 1 );   
               COMPLEX16*  n( data.complex16 );
   
    
               while( a_elem <= a_end )
               {
                  *a_elem = COMPLEX_16( n->re, n->im );
                  ++a_elem;
                  ++n;
               }
            }
   
            try
            {
               e = new LdasArray< COMPLEX_16 >( a, ndim, dims, name, units );
            }
            catch(...)
            {
               delete[] a;
               throw;
            }
   
            break;
         }   
         case(char_s_ptr):   
         {
            // Create ILWD container to hold all data elements
            e = new LdasContainer( name );
            LdasContainer* c( dynamic_cast< LdasContainer* >( e ) );
     
            const UINT4* dim_elem( dim ),
                       *const dim_end( dim + ndim - 1 );
             
            CHAR* data_ptr( data.chars );
            LdasArray< CHAR >* a( 0 );
   
            while( dim_elem <= dim_end )
            {
               a = new LdasArray< CHAR >( data_ptr, *dim_elem, name, unit );
               c->push_back( a,
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
   
               data_ptr += *dim_elem;
               ++dim_elem;
            }
            break;
         }   
         case(char_u_ptr):
         {
            // Create ILWD container to hold all data elements
            e = new LdasContainer( name );
            LdasContainer* c( dynamic_cast< LdasContainer* >( e ) );
     
            const UINT4* dim_elem( dim ),
                       *const dim_end( dim + ndim - 1 );
             
            UCHAR* data_ptr( data.charu );
            LdasArray< CHAR_U >* a( 0 );
   
            while( dim_elem <= dim_end )
            {
               a = new LdasArray< CHAR_U >( data_ptr, *dim_elem, name, unit );
               c->push_back( a,
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
   
               data_ptr += *dim_elem;
               ++dim_elem;
            }

            break;
         }      
         default:
         {
            node_error( "Undefined datatype.", comm );
            break;
         }        
      }
   }
   catch( const LdasException& exc )
   {
      delete[] units;
      delete[] dims;   
   
      node_error( exc, comm );
   }
   catch( ... )
   {
      delete[] units;
      delete[] dims;   
      throw;
   }

   
   delete[] units;
   delete[] dims;
   
   
   return e;
}
      
   
//-----------------------------------------------------------------------------
//   
//: Set data array.
//
// This method copies data into data array based on the datatype and
// number of elements passed to it. Only master node can call this method!!!
//   
//!ignore_begin:   
// Use 'std' namespace in method arguments( even though we said
// 'using namespace std' ) because perceps won't create proper link 
// for the documentation if we don't use it.
//!ignore_end:         
//   
//!param: const LdasElement* data - A pointer to the data element.
//!param: dataPointer* ptr - A pointer to the data array.
//!param: const datatype& type - A reference to the data type of the array   
//+       data.
//!param: const UINT4 num - Number of elements in the array.
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD.      
// 
//!exc: bad_cast - Malformed ILWD input data.   
//!exc: Undefined datatype.- Illegal datatype was specified.
//    
void LdasData::setDataArray( const ILwd::LdasElement* data,
   dataPointer* ptr, const datatype& type, const UINT4 num ) const
{
   if( num == 0 )
   {
      return;
   }
   
   
   switch( type )
   {
      case(boolean_lu):
      {
         const BOOLEAN* tmp( ( dynamic_cast< const LdasArray< BOOLEAN >* >( 
                                  data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).boolean, tmp, num * sizeof( BOOLEAN ) );

         break;
      }
      case(char_s):
      {
  	 // This temporary string is here only to guarantee 
  	 // that "tmp" is not pointing to the temporary: 
	 string tmp_string;
         const CHAR* tmp( 0 );

         if( data->getElementId() == ID_LSTRING )
         {
            tmp_string = ( dynamic_cast< const LdasString* >( data )->getString() );
            tmp = tmp_string.c_str();
         }
         else 
         {
            tmp = ( dynamic_cast< const LdasArray< CHAR >* >(
                       data ) )->getData();
         }
   
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).chars, tmp, num * sizeof( CHAR ) );   

         break;
      }   
      case(char_u):
      {
         const UCHAR* tmp( ( dynamic_cast< const LdasArray< UCHAR >* >(
                                data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).charu, tmp, num * sizeof( UCHAR ) );

         break;
      }   
      case(int_2s):
      {
         const INT2* tmp( ( dynamic_cast< const LdasArray< INT2 >* >( 
                               data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }   
   
         memcpy( ( *ptr ).int2s, tmp, num * sizeof( INT2 ) );   

         break;
      }   
      case(int_2u):
      {
         const UINT2* tmp( ( dynamic_cast< const LdasArray< UINT2 >* >(
                                data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).int2u, tmp, num * sizeof( UINT2 ) );   

         break;
      }   
      case(int_4s):
      {
         const INT4* tmp( ( dynamic_cast< const LdasArray< INT4 >* >( 
                               data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }   
   
         memcpy( ( *ptr ).int4s, tmp, num * sizeof( INT4 ) );   
   
         break;
      }   
      case(int_4u):
      {
         const UINT4* tmp( ( dynamic_cast< const LdasArray< UINT4 >* >( 
                                data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).int4u, tmp, num * sizeof( UINT4 ) );   
   
         break;
      }      
      case(int_8s):
      {
         const INT8* tmp( ( dynamic_cast< const LdasArray< INT8 >* >( 
                               data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).int8s, tmp, num * sizeof( INT8 ) );   
   
         break;
      }      
      case(int_8u):
      {
         const UINT8* tmp( ( dynamic_cast< const LdasArray< UINT8 >* >( 
                                data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).int8u, tmp, num * sizeof( UINT8 ) );   
   
         break;
      }      
      case(real_4):
      {
         const REAL4* tmp( ( dynamic_cast< const LdasArray< REAL4 >* >( 
                               data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }   
   
         memcpy( ( *ptr ).real4, tmp, num * sizeof( REAL4 ) );   
   
         break;
      }      
      case(real_8):
      {
         const REAL8* tmp( ( dynamic_cast< const LdasArray< REAL8 >* >( 
                                data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }
   
         memcpy( ( *ptr ).real8, tmp, num * sizeof( REAL8 ) );   
   
         break;
      }      
      case(complex_8):
      {
         const COMPLEX_8* tmp( ( dynamic_cast< const LdasArray< COMPLEX_8 >* >( 
                                     data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }   
   
         const COMPLEX_8* tmp_end( tmp + num - 1 );
   
         COMPLEX8* ptr_elem( ( *ptr ).complex8 );

   
         while( tmp <= tmp_end )
         {
            ptr_elem->re = tmp->real();
            ptr_elem->im = tmp->imag();
   
            ++ptr_elem;
            ++tmp;              
         }
   
         break;
      }      
      case(complex_16):
      {
         const COMPLEX_16* tmp( ( dynamic_cast< const LdasArray< COMPLEX_16 >* >( 
                                      data ) )->getData() );
         if( tmp == 0 )
         {
            throw bad_cast();
         }   
   
         const COMPLEX_16* tmp_end( tmp + num - 1 );
   
         COMPLEX16* ptr_elem( ( *ptr ).complex16 );
   
   
         while( tmp <= tmp_end )
         {
            ptr_elem->re = tmp->real();
            ptr_elem->im = tmp->imag();              
      
            ++ptr_elem;
            ++tmp;
         }
   
         break;
      }         
      default:
      {
         node_error( "Undefined datatype.", MPI::COMM_WORLD, 
                     !dataDistributor );

         break;
      }
   }  
}

   
//-----------------------------------------------------------------------------
//   
//: Set data array.
//
// This method copies data into data array based on the datatype and
// number of elements passed to it. All nodes in communicator can call this
// method.   
//   
//!param: dataPointer* ptr - A pointer to the destination data array.
//!param: const dataPointer& sptr - A reference to the source data array.   
//!param: const datatype& type - A reference to the data type of the array    
//+       data.
//!param: const UINT4 num - Number of elements in the array.
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD.      
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.         
//   
//!exc: Undefined datatype.- Illegal datatype was specified.
//    
void LdasData::setDataArray( 
   dataPointer* ptr, const dataPointer& sptr, const datatype& type,
   const UINT4 num, const MPI::Intracomm& comm, const bool p2p )
{
   if( num == 0 )
   {
      return;
   }
   
   
   switch( type )
   {
      case(boolean_lu):
      {
         memcpy( ptr->boolean, sptr.boolean, num * sizeof( BOOLEAN ) );
         break;
      }
      case(char_s):
      {
         memcpy( ptr->chars, sptr.chars, num * sizeof( CHAR ) );   
         break;
      }   
      case(char_u):
      {
         memcpy( ptr->charu, sptr.charu, num * sizeof( UCHAR ) );
         break;
      }   
      case(int_2s):
      {
         memcpy( ptr->int2s, sptr.int2s, num * sizeof( INT2 ) );   
         break;
      }   
      case(int_2u):
      {
         memcpy( ptr->int2u, sptr.int2u, num * sizeof( UINT2 ) );   
         break;
      }   
      case(int_4s):
      {
         memcpy( ptr->int4s, sptr.int4s, num * sizeof( INT4 ) );   
         break;
      }   
      case(int_4u):
      {
         memcpy( ptr->int4u, sptr.int4u, num * sizeof( UINT4 ) );   
         break;
      }      
      case(int_8s):
      {
         memcpy( ptr->int8s, sptr.int8s, num * sizeof( INT8 ) );   
         break;
      }      
      case(int_8u):
      {
         memcpy( ptr->int8u, sptr.int8u, num * sizeof( UINT8 ) );   
         break;
      }      
      case(real_4):
      {
         memcpy( ptr->real4, sptr.real4, num * sizeof( REAL4 ) );   
         break;
      }      
      case(real_8):
      {
         memcpy( ptr->real8, sptr.real8, num * sizeof( REAL8 ) );   
         break;
      }      
      case(complex_8):
      {
         memcpy( ptr->complex8, sptr.complex8, num * sizeof( COMPLEX8 ) );
         break;
      }      
      case(complex_16):
      {
         memcpy( ptr->complex16, sptr.complex16, num * sizeof( COMPLEX16 ) );
         break;
      }         
      case(char_s_ptr):
      {
         memcpy( ptr->chars, sptr.chars, num * sizeof( CHAR ) );   
         break;
      }   
      case(char_u_ptr):
      {
         memcpy( ptr->charu, sptr.charu, num * sizeof( UCHAR ) );
         break;
      }      
      default:
      {
         node_error( "Undefined datatype.", comm, p2p );
         break;
      }
   }  
}
   
   
//-----------------------------------------------------------------------------
//   
//: Get an MPI datatype corresponding to the array type.
//
// This method detects MPI datatype to represent the data array for the 
// communication based on the datatype of the array.
//   
//!param: const datatype& type - A reference to the data type of the array    
//+       data.
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD.      
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.         
//   
//!return: MPI::Datatype - An MPI datatype.
//   
//!exc: Undefined datatype. - Undefined datatype was specified.   
//   
MPI::Datatype LdasData::dataArrayType( const datatype& type,
   const MPI::Intracomm& comm, const bool p2p ) const
{
   switch( type )
   {
      case(boolean_lu):
      {
         return LDAS_MPI_BOOLEAN;
         break;
      }
      case(char_s):
      {
         return LDAS_MPI_CHAR;
         break;
      }   
      case(char_u):
      {
         return LDAS_MPI_UCHAR;
         break;
      }   
      case(int_2s):
      {
         return LDAS_MPI_INT2;
         break;
      }   
      case(int_2u):
      {
         return LDAS_MPI_UINT2;
         break;
      }   
      case(int_4s):
      {
         return LDAS_MPI_INT4;
         break;
      }   
      case(int_4u):
      {
         return LDAS_MPI_UINT4;
         break;
      }      
      case(int_8s):
      {
         return LDAS_MPI_INT8;
         break;
      }      
      case(int_8u):
      {
         return LDAS_MPI_UINT8;
         break;
      }      
      case(real_4):
      {
         return LDAS_MPI_REAL4;
         break;
      }      
      case(real_8):
      {
         return LDAS_MPI_REAL8;
         break;
      }      
      case(complex_8):
      {
         return LDAS_MPI_COMPLEX8;
         break;
      }      
      case(complex_16):
      {
         return LDAS_MPI_COMPLEX16;
         break;
      }         
      case(char_s_ptr):
      {
         return LDAS_MPI_CHAR;
         break;
      }   
      case(char_u_ptr):
      {
         return LDAS_MPI_UCHAR;
         break;
      }      
      default:
      {
         node_error( "Undefined datatype.", comm, p2p );
         break;
      }
   }   

   
   return MPI::DATATYPE_NULL;
}

   
//-----------------------------------------------------------------------------
//   
//: Gets the displacement of the data pointer in the structure.
//
// This method calculates the displacement of the passed to it data pointer.
//   
//!param: const datatype& type - A reference to the data type of the array    
//+       data.   
//!param: const dataPointer& - A reference to the data pointer in the structure.
//!param: const MPI::Intracomm& comm - A reference to the MPI                 
//+       intracommunicator that specifies the communication domain.       
//+       Default is MPI::COMM_WORLD. 
//!param: const bool p2p - A flag to specify that method gets called for      
//+       p2p communication. Default is true.           
//
//!return: MPI::Aint - A data pointer displacement.
//   
//!exc: Undefined datatype. - Undefined datatype was specified.
//!exc: MPI::Exception - MPI library exception.      
//   
MPI::Aint LdasData::dataArrayDisplacement( const datatype& type, 
   const dataPointer& data, const MPI::Intracomm& comm,
   const bool p2p ) const
{
   MPI::Aint disp( 0 );
   
   
   switch( type )
   {
      case(boolean_lu):
      {
         disp = MPI::Get_address( data.boolean );      
         break;
      }
      case(char_s):
      {
         disp = MPI::Get_address( data.chars );      
         break;
      }   
      case(char_u):
      {
         disp = MPI::Get_address( data.charu );      
         break;
      }   
      case(int_2s):
      {
         disp = MPI::Get_address( data.int2s );      
         break;
      }   
      case(int_2u):
      {
         disp = MPI::Get_address( data.int2u );      
         break;
      }   
      case(int_4s):
      {
         disp = MPI::Get_address( data.int4s );      
         break;
      }   
      case(int_4u):
      {
         disp = MPI::Get_address( data.int4u );      
         break;
      }      
      case(int_8s):
      {
         disp = MPI::Get_address( data.int8s );      
         break;
      }      
      case(int_8u):
      {
         disp = MPI::Get_address( data.int8u );      
         break;
      }      
      case(real_4):
      {
         disp = MPI::Get_address( data.real4 );      
         break;
      }      
      case(real_8):
      {
         disp = MPI::Get_address( data.real8 );      
         break;
      }      
      case(complex_8):
      {
         disp = MPI::Get_address( data.complex8 );      
         break;
      }      
      case(complex_16):
      {
         disp = MPI::Get_address( data.complex16 );      
         break;
      }         
      case(char_s_ptr):
      {
         disp = MPI::Get_address( data.chars );      
         break;
      }   
      case(char_u_ptr):
      {
         disp = MPI::Get_address( data.charu );      
         break;
      }      
      default:
      {
         node_error( "Undefined datatype.", comm, p2p );
         break;
      }   
   }
   
   return disp;
}


//-----------------------------------------------------------------------------
//   
//: Gets the total number of elements.
//   
// This method calculates total number of elements in multidimensional array.
//
//!param: const UINT4 ndim - A number of dimensions in the array.
//!param: const UINT4* dim - An array of dimensions.      
//
//!return: const UINT4 - Total number of array elements.
//   
const UINT4 LdasData::getNDim( const UINT4 ndim, const UINT4* dim )
{
   if( ndim == 0 )
   {
      return 0;
   }

   
   UINT4 num( 1 );
   const UINT4* dim_end( dim + ndim - 1 );
   const UINT4* dim_elem( dim );

   
   while( dim_elem <= dim_end )
   {
      num *= *dim_elem;
      ++dim_elem;
   }

   
   return num;
}

   
//-----------------------------------------------------------------------------
//   
//: Gets number of data elements for blob type of data.
//
//!param: const UINT4 ndim - Number of dimensions.
//!param: const UINT4* dim - An array of data dimensions.   
//   
//!return: const UINT4 - Number of data elements.
//   
const UINT4 LdasData::getBlobNDim( const UINT4 ndim, 
   const UINT4* dim )
{
   if( ndim == 0 )
   {
      return 0;
   }
   
   UINT4 num( 0 );
   
   const UINT4* dim_elem( dim ),
              *const dim_end( dim + ndim - 1 );
   
   while( dim_elem <= dim_end )
   {
      num += *dim_elem;
      ++dim_elem;
   }
   
   return num;
}
   
