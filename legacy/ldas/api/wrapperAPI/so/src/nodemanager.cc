#include "LDASConfig.h"

// System Header Files
#include <dlfcn.h>
#include <cstring>  
#include <sstream>   
#include <math.h>   

// Local Header Files   
#include "nodemanager.hh"
#include "mpiutil.hh"   
#include "inputdatatype.hh"   
#include "outputtype.hh"   
#include "ldaserror.hh"   
#include "initvars.hh"      
#include "mpilaltypes.hh"   
#include "nodeinfo.hh"
#include "mpicmdargs.hh"   
#include "detectortype.hh"   
#include "loadbalance.hh"   

using namespace std;   
   
// Static data member initialization
const UINT4 NodeDataManager::mMsgSize( 256 );   
REAL8 NodeDataManager::mSendILwdTime( 0.0f );      
   
   
//------------------------------------------------------------------------------
// 
//: Constructor.
//
//!param: DLErrorHandler& dl_error - A reference to the dso error handler.
//!param: lbNodeInfo& lb_node - A reference to the object containing node       
//+       information.   
//!param: SearchOutput& search_output - A reference to the SearchOutput         
//+       structure containing the result data for the node.   
//   
NodeDataManager::NodeDataManager( DLErrorHandler& dl_error,
   lbNodeInfo& lb_node, SearchOutput& search_output ) :
   mDLError( dl_error ), mLBNode( lb_node ), 
   mSearchOutput( search_output ), mFreeOutput( 0 ), 
   mAnalyzedSeconds( 0 ),
   mOutputType( MPI::DATATYPE_NULL ), mInputSequences( 0 )
{}

   
//------------------------------------------------------------------------------
//
//: Initializing utility.
//
// This method is used by all nodes in the COMM_WORLD communicator:
// slave nodes send hostname to the master, master node appends
// host name information for all slaves to the initialization message for
// the mpiAPI.   
//
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//   
void NodeDataManager::initialize()
{
   // Each node gets its own hostname
   INT4 hostDim;
   CHAR* hostName( new CHAR[ MPI::MAX_PROCESSOR_NAME ] );
   
   MPI::Get_processor_name( hostName, hostDim );
   
   communicateHostName( hostName ); 
   
   delete[] hostName;
   hostName = 0;
   
   return;
}


//------------------------------------------------------------------------------   
//
//: Finalizing routine.
//
// This method serves the purpose of proper finalizing process by all nodes
// in the COMM_WORLD.   
//   
//!param: const MPI::Intracomm& comm - A reference to the load balancing MPI
//+       intracommunicator.      
//   
void NodeDataManager::finalize( const MPI::Intracomm& comm )
{
   ostringstream finalize_msg;
   finalize_msg.setf( ios::fixed, ios::floatfield );   
   finalize_msg.precision( 5 );
   
   // [un]pack = pack/unpack
   // IS = initSearch
   // CD = conditionData
   // AS = applySearch
   // FO = freeOutput
   // FS = finalizeSearch
   finalize_msg << " Rank" << myNode
                << " (inPut: create=" << InputType::getCreateTime()
                << " send=" << commInputTime;
   
   if( myNode == masterNode )
   {
      finalize_msg << " ilwd=" << InputType::getRecvILwdTime();
   }
   
   finalize_msg << "; outPut: create=" << OutputType::getCreateTime()
                << " send=" << commOutputTime;
   if( myNode == masterNode )
   {
      finalize_msg << " ilwd=" << mSendILwdTime;
   }   
   
   finalize_msg << "; metadata: [un]pack=" << LdasDatatype::getPackDelta()
                << " bcast=" << LdasDatatype::getBcastDelta() 
                << "; IS=" << initSearchTime
                << "; CD=" << conditionDataTime
                << "; AS=" << applySearchTime
                << "; FO=" << freeOutputTime
                << "; FS=" << finalizeSearchTime
                << "; LB=" << LdasLoadBalance::getTimeSpent();
   finalize_msg.setf( ios::dec, ios::basefield );   
   finalize_msg << "; max memory=" << DLErrorHandler::getMaxMemory()
                << ")";
   
   const string msg( finalize_msg.str() );

   if( msg.size() >= mMsgSize )
   {
      node_error( "Finalizing message exceeds the size of the buffer( "
                  "implementation change is required )", comm );
   }

   CHAR* msg_buf( new CHAR[ mMsgSize ] );
   strcpy( msg_buf, msg.c_str() );
   
   finalizeMessage( comm, msg_buf );
   
   delete[] msg_buf;
   msg_buf = 0;
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Sets freeOutput function pointer.
//
// This method sets function pointer to the freeOutput function from loaded dso.
//
 //!param: free_fptr free_output - A pointer to the freeOutput function in 
//+       loaded dso.  
//   
//!return: Nothing.
//
void NodeDataManager::setFreeOutput( free_fptr free_output )
{
   mFreeOutput = free_output;
   return;
}

   
//------------------------------------------------------------------------------
//
//: Sets initial number of sequences in input data.
//
// This method sets initial number of sequences in the input data.
//
//!param: const UINT4 num - A number of sequences in the inPut structure
//+       wrapperMaster distributed. 
//   
//!return: Nothing.
//
void NodeDataManager::setNumSequences( const UINT4 num )
{
   mInputSequences = num;
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Sets number of data seconds job is about to analyze.
//
// This is mostly for the purpose of eventmonAPI functionaly to be able to
// filter out jobs that produce more than allowed number of rows per second
// of analyzed data.
//   
//!param: const UINT4 num - A number of sequences in the inPut structure
//+       wrapperMaster distributed. 
//   
//!return: Nothing.
//
void NodeDataManager::setAnalyzedSeconds( const REAL8 dt )
{
   mAnalyzedSeconds = static_cast< UINT8 >( ceil( dt ) );
   return;
}   
   

//------------------------------------------------------------------------------
//
//: Node cleanup utility.
//
// This method is used by all nodes in the COMM_WORLD communicator to 
// release all dynamically allocated memory for a processing job.   
//
//!param: inPut* input - A pointer to the C inPut data structures to free.  
//!param: SearchParams& search_params - A reference to the SearchParams data    
//+       structure.   
//!param: void* dl_handle - A handle to the dynamic library loaded by node.   
//!param: fnlz_fptr finalize_search - A pointer to the finalizeSearch function  
//+       in loaded dso.   
//!param: const bool finalize - A flag to indicate that job will do the proper
//+       cleanup. Default is true.   
//   
//!return: Nothing.
//            
void NodeDataManager::cleanup( inPut** input, SearchParams& search_params,
   void* dl_handle, fnlz_fptr finalize_search, const bool finalize )
{
   // Cleanup
   CHAR* free_status( 0 );   
   INT4  return_code( 0 );
   
   
   // The same outPut memory was reused by multiple applySearch calls
   if( !communicateOutput )
   {
      if( mOutputType != MPI::DATATYPE_NULL )
      {
         OutputType::cleanupMetaData();
   
         mOutputType.Free();
   
         mOutputType = MPI::DATATYPE_NULL;
      }


      if( myNode == masterNode )
      {
         OutputType::cleanup( mSearchOutput.result,
                              mSearchOutput.numOutput ); 
      
         delete[] mSearchOutput.result;
         mSearchOutput.result = 0;         
      }
      else
      {
         // It's a slave node
         if( numDummyOutput )
         {
            OutputType::cleanup( mSearchOutput.result, mSearchOutput.numOutput ); 
      
            delete[] mSearchOutput.result;
            mSearchOutput.result = 0;
         }
         else
         {
            freeOutputTime -= MPI::Wtime();
   
            return_code = mFreeOutput( &free_status, &mSearchOutput );   
   
            freeOutputTime += MPI::Wtime();   
            mDLError( "freeOutput", return_code, free_status,
                       MPI::COMM_WORLD, true );   
   
            if( free_status )
            {   
               free( free_status );
               free_status = 0;
            }
         }
      }   
   }
   
   node_error.pass();
   
   
   // wrapperMaster is not calling finalizeSearch() since
   // it didn't call initSearch().
   if( myNode != masterNode )
   {
      finalizeSearchTime -= MPI::Wtime();
   
      return_code = finalize_search( &free_status );
   
      finalizeSearchTime += MPI::Wtime();   
      mDLError( "finalizeSearch", return_code, free_status,
                MPI::COMM_WORLD, true );

      if( free_status )
      {   
         free( free_status );
         free_status = 0;
      }
   }
   
   node_error.pass();

   
   // Free inPut structure;
   if( *input )
   {
      // Make sure to delete exact number of sequences wrapperMaster
      // has distributed originally to the nodes.
      ( *input )->numberSequences = mInputSequences;
   }
   InputType::cleanup( *input );
   node_error.pass();
   
   delete ( *input );
   *input = 0;


   if( dl_handle )
   {
      dlclose( dl_handle );
   }
   
   
   // Memory cleanup for SearchParams data structure
   if( *( search_params.comm ) != MPI_COMM_NULL )
   {
      MPI_Comm_free( search_params.comm );
   }
   
   delete search_params.comm;
   search_params.comm = 0;
   
   delete search_params.action;
   search_params.action = 0;

   if( !finalize )
   {
      deleteMPIDatatypes();
      sendFinalInfo();   
   }
   
   return;
}

  
//------------------------------------------------------------------------------
// 
//: Destructs basic derived MPI datatypes.
//      
// This method will be used by all nodes in the MPI::COMM_WORLD.
// It destructs all basic derived MPI datatypes that were generated for the    
// job.   
//   
//!return: Nothing.
//   
void NodeDataManager::deleteMPIDatatypes()
{
   // Delete node information datatype
   mLBNode.nodeType.Free();
   
   DetectorType::freeMpiType();
   
   // Delete LDAS specific MPI datatypes
   return deleteLdasTypes();      
}
   

//------------------------------------------------------------------------------
// 
//: Simulates outPut data.
//      
// This method will be used only for the debug purposes by search master of the
// load balancing communicator. 
// It simulates specified type of outPut 'C' data structure as if it was 
// generated by the <b>applySearch</b> function of the dynamically loaded 
// library.  
//   
//!param:  MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+        within which data is sent( master + LBcommunicator ).
//
//!return: Nothing.
//   
void NodeDataManager::simulateData( const MPI::Intracomm& comm )
{
   if( numDummyState && myNode == nodes[ 0 ] )
   {
      mLBNode.deltaT = -1.0f;
      mSearchOutput.numOutput = numDummyState;      
      buildOutputStruct( &( mSearchOutput.result ), 
                         mSearchOutput.numOutput );
  
      // Send simulated outPut to the master   
      operator()( comm );            
   }
   

   // Only searchMaster node will generate the data.
   if( numDummyMdd && myNode == nodes[ 0 ] )
   {
      mLBNode.deltaT = -1.0f;   
      mSearchOutput.numOutput = numDummyMdd;      
      buildMDDOutputStruct( &( mSearchOutput.result ), 
                            mSearchOutput.numOutput, comm );
   
      // Send simulated outPut to the master   
      operator()( comm );            
   }

   
   return;
}
   
   
//------------------------------------------------------------------------------
// 
//: Gets MPI datatype representing array of C outPut data structures.
//      
// This method will be used only when command line argument communicateOutput 
// is set to ONCE. It indicates that once generated MPI derived datatype 
// will be used for the whole job.   
//   
//!return: const MPI::Datatype& - A reference to the MPI derived datatype     
//+        representing an array of C outPut data structures.
//   
const MPI::Datatype& NodeDataManager::getOutputType() const
{
   return mOutputType;
}
   
  
//------------------------------------------------------------------------------
// 
//: Sets MPI datatype representing array of C outPut data structures.
//      
// This method will be used only when command line argument communicateOutput 
// is set to ONCE. It indicates that once generated MPI derived datatype 
// will be used for the whole job.   
//   
//!param: MPI::Datatype& output_type - A reference to the MPI derived datatype
//+       representing an array of C outPut data structures.   
//
//!return: Nothing.
//   
void NodeDataManager::setOutputType( MPI::Datatype& output_type )
{
   // MPI datatype assignment is shallow.
   mOutputType = output_type;
   
   
   return;
}

   
   
   
   
