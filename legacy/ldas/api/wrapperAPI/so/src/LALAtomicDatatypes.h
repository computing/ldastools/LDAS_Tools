#ifndef _LALATOMICDATATYPES_H
#define _LALATOMICDATATYPES_H
   
// Local Header Files
#include "LDASConfig.h"   
#ifndef LDAS_BUILD
#include "LALRCSID.h"   
#endif
   
   
typedef char          CHAR;
typedef unsigned char UCHAR;     
typedef unsigned char BOOLEAN;   

#if SIZEOF_SHORT == 2
typedef short INT2;
typedef unsigned short UINT2;
#elif SIZEOF_INT == 2
typedef int INT2;
typedef unsigned int UINT2;
#else
#   error "Error: No 2 byte integer found"
#endif

#if SIZEOF_INT == 4
typedef int INT4;
typedef unsigned int UINT4;
#elif SIZEOF_LONG == 4
typedef long INT4;
typedef unsigned long UINT4;
#else
#   error "Error: No 4 byte integer found"
#endif

#if SIZEOF_LONG == 8
typedef long INT8;
typedef unsigned long UINT8;
#elif SIZEOF_LONG_LONG == 8
typedef long long INT8;
typedef unsigned long long UINT8;
#else
#   error "Error: No 8 byte integer found"
#endif

#if SIZEOF_FLOAT == 4
typedef float REAL4;
#else
#   error "Error: No 4 byte real found"
#endif

#if SIZEOF_DOUBLE == 8
typedef double REAL8;
#else
#   error "Error: No 8 byte real found"
#endif

   
#ifdef __cplusplus
extern "C"{
#endif   
  
typedef struct
{
   REAL4 re;
   REAL4 im;
}COMPLEX8;
   
   
typedef struct
{
   REAL8 re;
   REAL8 im;
}COMPLEX16;   

   
#ifdef __cplusplus
}
#endif      

   
#endif   // LalTypesHH
