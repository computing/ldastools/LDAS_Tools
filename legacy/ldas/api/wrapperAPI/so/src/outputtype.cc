#include "LDASConfig.h"

// System Header Files
#include <new>
#include <cstring>   
#include <sstream>   
#include <memory>   

// ILWD Header Files
#include <ilwd/ldascontainer.hh>      
#include <ilwd/ldasstring.hh>
   
// Local Header Files
#include "ldaserror.hh"   
#include "outputtype.hh"
#include "mpilaltypes.hh"   
#include "multidimdata.hh"   
#include "statevectype.hh"   
#include "databasetype.hh"   
#include "multidimdata.hh"   
#include "ldasdata.hh"   
#include "ldastag.hh"            
#include "initvars.hh"   
#include "mpiutil.hh"   


using namespace std;      
   
using ILwd::LdasContainer;
using ILwd::LdasElement;
using ILwd::LdasString;      

static const size_t uint4_size( sizeof( UINT4 ) );   
static const size_t int4_size( sizeof( INT4 ) );      

   
//------------------------------------------------------------------------------
//
//: Initializes catagory hash map.
//   
// This method initializes the catagory hash map. It associates a string 
// representation with each value of <i>catagory</i> enum type.
//   
//!return: CatagoryHash - Hash map for catagory.
// 
OutputType::CatagoryHash OutputType::initCatagoryHash() 
{
   OutputType::CatagoryHash h;
   
   h[ binaryInspiral ] = "BinaryInspiral";
   h[ ringDown       ] = "RingDown";
   h[ periodic       ] = "Periodic";
   h[ burst          ] = "Burst";
   h[ stocastic      ] = "Stocastic";
   h[ timeFreq       ] = "TimeFreq";
   h[ instrumental   ] = "Instrumental";
   h[ protoType      ] = "ProtoType";
   h[ experimental   ] = "Experimental";

   return h;
}
   

// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:   
   
// Initialize static data members   
const OutputType::CatagoryHash OutputType::mCatagoryHash( initCatagoryHash() );   
   
bool              OutputType::mInitState( false );
UINT4             OutputType::mInitNum( 0 ); 
UINT4*            OutputType::mNumStates( 0 );   
UINT4*            OutputType::mNumDataBase( 0 );      
UINT4*            OutputType::mNumOptional( 0 );      
MPIStructInfo**   OutputType::mStatesInfo( 0 );      
MPIDataBaseInfo** OutputType::mDataBaseInfo( 0 );         
MPIStructInfo**   OutputType::mOptionalInfo( 0 );         
vector< INT4 >    OutputType::mInitSlaves;   
const UINT4       OutputType::mTypeNum( 6 );      
REAL8             OutputType::mCreateTime( 0.0f );   
      
//!ignore_end:
   
      
//: <font color="green">Only for data testing!!!</font>   
void OutputType::write() const
{
   cout << "Number of sequences: " << mNum << endl;

   if( mOutput == 0 )
   {
      return;
   }
   
   
   for( UINT4 i = 0; i < mNum; ++i )
   {
      cout << "OUTPUT FOR " << i << endl;
   
      cout << "indexNumber = " << mOutput[ i ].indexNumber << endl;
      cout << "search = " << mOutput[ i ].search << endl;   
      cout << "significant = " << mOutput[ i ].significant << endl;   

   
      stateVector* currState( mOutput[ i ].states );
   
      while( currState != 0 )
      {
         cout << i << "_State.name: " << currState->stateName << endl;
         if( currState->store != 0 )
         {
           cout << i << "_State.store[0].name: " << currState->store[ 0 ].name << endl; 
           cout << i << "_State.store[0].range.dTime.startSec: "
                << currState->store[ 0 ].range.dTime.startSec << endl;
   
           if( currState->store[ 0 ].history != 0 )
           {
              cout << i << "_State.store[0].history.name: "
                   << currState->store[ 0 ].history->name << endl;   
           }
         }
         currState = currState->next;
      }      

   
      dataBase* db( mOutput[ i ].results );      
   
   
      while( db != 0 )
      {
         cout << i << "_DB.tableName: " << db->tableName << endl;
         cout << i << "_DB.columnName: " << db->columnName << endl;   
         
         cout << i << "_DB.rows( " << db->numberRows << " ): ";            
   
         for( UINT4 k = 0; k < db->numberRows; ++k )
         {
            cout << db->rows.int2u[ k ] << " ";
         }
   
         cout << endl;
         db = db->next;
      }   

   
      if( mOutput[ i ].optional )
      {
         cout << i << "_optional.name: " 
              << mOutput[ i ].optional[ 0 ].name << endl;      
      }
   }

   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype to represent an array of C
// outPut data structures. This datatype is generated only for point-to-point 
// communication by two nodes: slave node sending the result data and master 
// node collecting that data.   
//   
//!param: outPut** pe - A pointer to the array of C outPut data structures.     
//+       Only slave nodes in the communicator have valid array data. 
//!param: const UINT4 num - Number of elements in the <b>pe</b> array.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which OutputDatatype is defined. Default is MPI::COMM_WORLD.
//!param: const INT4 rank - Rank of the node sending the data if master node,   
//+       and rank of the node to send the data to if slave node. Default is -1.
//
//!exc: Illegal number of outPut structures. - Illegal number of outPut         
//+     structures is specified: <= 0.
//!exc: Empty array of outPut structures when number of elements is not zero. - 
//+     Array of outPut data structures is empty even though number of       
//+     elements in the array is not zero.
//!exc: Index exceeds the size of packing buffer. - Index into the binary       
//+     buffer used for storing the metadata exceeds the size of that buffer.      
//!exc: NULL store in stateVector. - Malformed outPut data structure, such as   
//+     one of the nodes in stateVector linked list has store set to NULL.     
//!exc: Inconsistent outPut data structure signature. - Sceleton for outPut     
//+     data structure differs from original signature.   
//!exc: Undefined datatype. - Undefined datatype was specified.         
//!exc: Undefined domain. - Undefined domain was specified.                     
//+     One of TIME | FREQ | BOTH.         
//!exc: Invalid number of dataBase nodes. - Negative number of dataBase nodes  
//+     was specified.   
//!exc: Error generating DataBaseType. - Error generating a MPI derived        
//+     datatype to represent dataBase linked list.   
//!exc: Invalid number of stateVector nodes. - Negative number specified for   
//+     number of nodes in the linked list.      
//!exc: Error generating StateVectorType. - Error generating MPI derived       
//+     datatype representing the stateVector linked list.   
//!exc: Invalid number of history nodes specified. - Negative number of nodes   
//+     was specified.         
//!exc: Error generating HistoryType. - Error generating MPI derived datatype   
//+     to represent history linked list.   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype  
//+     to represent C interval union.      
//!exc: Invalid number of multiDimData. - Negative number of array elements is  
//+     specified.         
//!exc: Error generating MultiDimDataType. - Error generating MPI derived       
//+     datatype to represent an array of C multiDimData structures.            
//!exc: Memory allocation failed. - Error allocating memory. 
//!exc: Error generating OutputType. - Error generating MPI datatype to         
//+     represent an array of C outPut data structures.
//   
OutputType::OutputType( outPut** pe, const UINT4 num, 
   const MPI::Intracomm& comm, const INT4 rank )
   try:
   LdasDatatype( comm, rank ), mNum( num ),
   mCommWorldRank( initRank( comm, rank ) ), mOutput( 0 )
{
   // Time spent in the OutputType construction
   mCreateTime -= MPI::Wtime();
   
   
   if( mNum <= 0 )
   {
      node_error( "Illegal number of outPut structures.", comm );
   }


   const bool master( inMaster() );
   
   
   if( !master )
   {
      if( *pe == 0 )
      {
         node_error( "Empty array of outPut structures when "
                     "number of elements is not zero.", comm );
      }
   }

   
   if( communicateOutput ||
       ( !communicateOutput && 
         ( !master || 
           ( master && !inRanks( mInitSlaves, mCommWorldRank ) ) ) ) ) 
   {
      // Master node: allocates the memory for mOutput or pe
      if( master )
      {
         if( communicateOutput )
         {
            mOutput = new outPut[ mNum ];
            memset( mOutput, 0, sizeof( outPut ) * mNum );
         }
         else
         if( !mInitState )
         {
            *pe = new outPut[ mNum ];
            memset( *pe, 0, sizeof( outPut ) * mNum );   
         }
      }
   

      // All nodes: allocate memory for static metadata
      if( !communicateOutput && !mInitState )
      {
         allocateMetaData( &mNumStates, &mNumDataBase, 
                           &mNumOptional, &mStatesInfo,
                           &mDataBaseInfo, &mOptionalInfo );
      }
   

      // Current metadata
      UINT4* numStateVector( 0 );
      UINT4* numDataBase( 0 );
      UINT4* numOptional( 0 );
   
      MPIStructInfo**   stateInfo( 0 );
      MPIDataBaseInfo** dbInfo( 0 );
      MPIStructInfo**   optionalInfo( 0 );
   
   
      allocateMetaData( &numStateVector, &numDataBase, 
                        &numOptional, &stateInfo,
                        &dbInfo, &optionalInfo );   
   
   
      // Broadcast data information to the master
      broadcastInfo( *pe, 
                     numStateVector, stateInfo,
                     numDataBase, dbInfo,
                     numOptional, optionalInfo );

   
      // All nodes: build MPI derived datatype
      if( communicateOutput || !mInitState )
      {
         vector< MPI::Datatype > stateTypes( mNum ),
                                 dbTypes( mNum ),
                                 optTypes( mNum );
   
         outPut* out_arr( *pe );

         if( master && communicateOutput )
         {
            out_arr = mOutput;
         }   

   
         outPut* out_elem( out_arr );

   
         for( UINT4 i = 0; i < mNum; ++i )
         {
            // Build stateVector MPI datatype for out_arr[ i ].states 
            stateTypes[ i ] = StateVectorType( &( out_elem->states ),
                                 numStateVector[ i ], stateInfo[ i ],
                                 comm, rank ); 
   
            // Build dataBase MPI datatype for out_arr[ i ].results
            dbTypes[ i ] = DataBaseType( &( out_elem->results ), 
                              numDataBase[ i ], dbInfo[ i ],
                              comm, rank );
   
            // Build multiDimData MPI datatype for out_arr[ i ].optional   
            optTypes[ i ] = MultiDimDataType( &( out_elem->optional ),
                               optionalInfo[ i ], comm, rank,
                               numOptional[ i ] );   
   
            ++out_elem;
         }


         buildType( out_arr, stateTypes, dbTypes, optTypes );
      }
   

      // Cleanup the memory
      deleteMetaData( mNum, 
                      &numStateVector, &numDataBase, 
                      &numOptional, &stateInfo,
                      &dbInfo, &optionalInfo );   
   }
   
   
   mCreateTime += MPI::Wtime();
}
catch( const bad_alloc& exc )
{
   node_error( exc, getComm() );
}
catch( const MPI::Exception& exc )
{
   node_error( exc, getComm() );   
}

   
//------------------------------------------------------------------------------
//
//: Destructor
//
OutputType::~OutputType()
{
   if( mOutput )
   {
      cleanup( mOutput, mNum, getComm() );
      delete[] mOutput;
      mOutput = 0;
   }
}
   
   
//------------------------------------------------------------------------------
//
//: Gets mInitState value.
//
// This method returns the value of mInitState data member. True if MPI       
// datatype representing the array of outPut structures was generated at least
// once, false otherwise.
//   
//!return: const bool - True if MPI derived datatype was generated at least    
//+        once for the node, false otherwise.  
//       
const bool OutputType::getInitState()
{
   return mInitState;
}   

   
//------------------------------------------------------------------------------
//
//: Sets mInitState value.
//
// This method sets the value of mInitState data member.
// <p>This method only to be used when communicateOutput is set to ONCE from the
// command line.
//   
//!param: const bool v - Value to set mInitState to.
//       
void OutputType::setInitState( const bool v )
{
   mInitState = v;
   
   return;
}   
   
  
//------------------------------------------------------------------------------
//
//: Releases dynamically allocated memory for static data members.
//   
//!return: Nothing.
//   
void OutputType::cleanupMetaData() 
{
   if( mInitState )
   {
      deleteMetaData( mInitNum, &mNumStates, &mNumDataBase, 
                      &mNumOptional, &mStatesInfo,
                      &mDataBaseInfo, &mOptionalInfo );
   
      mInitState = false;
   
      mInitSlaves.erase( mInitSlaves.begin(), mInitSlaves.end() );
   }
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: initRank method.
// 
// This method gets the rank of the current process in the
// communicator comm and returns corresponding rank of the 
// same process in the MPI::COMM_WORLD communicator.
// 
//!param: comm MPI::Comm& comm - A reference to the communicator.   
//!param: const INT4 rank - The process rank in the <i>comm</i> communicator.
//
//!return: Nothing.
//   
//!exc: MPI::Exception - MPI library exception.   
//   
const INT4 OutputType::initRank( const MPI::Comm& comm, const INT4 rank ) 
{
   // Check if comm is a handle for the MPI::COMM_WORLD
   // communicator
   if( MPI::Comm::Compare( comm, MPI::COMM_WORLD ) == MPI::IDENT )
   {
      return rank;
   }
   
   
   // Get the group under communicator comm.
   MPI::Group group( comm.Get_group() );
   
   // Get the group under communicator COMM_WORLD.
   MPI::Group world_group( MPI::COMM_WORLD.Get_group() );   

   INT4 new_rank( 0 );
   // Translate the rank.
   MPI::Group::Translate_ranks( group, 1, &rank, 
                                world_group, &new_rank );
   
   group.Free();
   world_group.Free();
   
   return new_rank;
}

   
//------------------------------------------------------------------------------
//
//: Converts i-th element of the mOutput array to the ILWD format element.
//   
// This method gets called only by master node in the communicator.
// It converts specified element of the contained array of C outPut data 
// structures into the ILWD format element.
//   
//!param: const INT4 index - Index into mOutput array of outPut data structures.
//   
//!return: const LdasContainer* - A pointer to the ILWD format element.
//   
//!exc: bad_alloc - Error allocating memory.
//!exc: Index into outPut array is out of range. - Specified index into array  
//+     of outPut structures is out of range.   
//    
const ILwd::LdasContainer* OutputType::toILwd( const UINT4 index ) const 
{
   if( mOutput == 0 )
   {
      // Should throw an exception?!
      return 0;
   }

   if( index >= mNum )
   {
      node_error( "Index into outPut array is out of range.", 
                  getComm() );
   }

   
   return outputToILwd( mOutput[ index ], getComm() );   
}

   
//------------------------------------------------------------------------------
//
//: Converts outPut structure to the ILWD format element.
//   
// This method gets called only by master node in the communicator when 
// communicateOutput = ONCE command line argument is specified.   
//   
//!param: const outPut& result - A reference to the outPut data structure to   
//+       convert into ILWD format element.   
//   
//!return: const LdasContainer* - A pointer to the ILWD format element.
//   
//!exc: bad_alloc - Error allocating memory.
//    
const ILwd::LdasContainer* OutputType::resultToILwd( const outPut& result,
   const MPI::Intracomm& comm )
{
   return outputToILwd( result, comm );   
}
   
   
//------------------------------------------------------------------------------
//
//: Convert outPut data structure to the ILWD format element. 
//    
//!param: const outPut& re - A reference to the outPut data structure to        
//+       convert to the ILWD format element.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined. Default is MPI::COMM_WORLD.      
//    
//!return: LdasContainer* - A pointer to the created ILWD format element.
//
//!exc: Undefined catagory specified. - Undefined catagory value is specified.
//!exc: bad_alloc - Memory allocation failed.   
//   
ILwd::LdasContainer* OutputType::outputToILwd( const outPut& re, 
   const MPI::Intracomm& comm )
{
   ostringstream s;
   s << "template:" << re.indexNumber << ":outPut";
   auto_ptr< LdasContainer > e( new LdasContainer( s.str() ) );   

   // Create catagory element
   CatagoryHash::const_iterator iter( mCatagoryHash.find( re.search ) );
   if( iter == mCatagoryHash.end() )
   {
      node_error( "Undefined catagory specified.", comm );
   }
   
   e->push_back( new LdasString( iter->second, "catagory" ),
                 ILwd::LdasContainer::NO_ALLOCATE,
                 ILwd::LdasContainer::OWN );
   
   
   // Create significant element
   const char* sig_val( re.significant == true ? "TRUE" : "FALSE" );
   e->push_back( new LdasString( sig_val, "significant" ),
                 ILwd::LdasContainer::NO_ALLOCATE,
                 ILwd::LdasContainer::OWN );

   
   // Create stateVector ILWD format element
   if( re.states )
   {
      e->push_back( StateVectorType::toILwd( re.states, comm ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
   }
     
   
   // Create database ILWD format element
   if( re.results )
   {
      e->push_back( DataBaseType::toILwd( re.results, comm ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN);
   }

   
   if( re.optional != 0 )
   {
      e->push_back( MultiDimDataType::toILwd( re.optional[ 0 ], 
                                              comm ),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
   }

   
   return e.release();   
}

   
//------------------------------------------------------------------------------
//
//: Gets total time spent in datatype generation.
//
// <font color='green'>For debugging purposes only!</font>   
//
//!return: const REAL8 - Time value.
//                     
const REAL8 OutputType::getCreateTime()
{
   return mCreateTime;
}
   

//------------------------------------------------------------------------------
//
//: Allocates memory for metadata.
//   
// This method allocates the memory for passed to it structures of metadata.
// Note that arrays should be set to NULL before calling this method, otherwise 
// in a case of a bad_alloc exception, memory cleanup will be done before
// rethrowing that exception( it will be done without checking if memory was 
// actually allocated within the method ).   
//
//!param: UINT4** numStates - A pointer to the array of node count in C        
//+       stateVector linked list.
//!param: UINT4** numDB - A pointer to the array of node count in C dataBase   
//+       linked list.
//!param: UINT4** numOpt - A pointer to the array of element count in the      
//+       array of C multiDimData structures.
//!param: MPIStruct*** states - A pointer to the array of metadata structures  
//+       for C stateVector linked list.
//!param: MPIDataBaseInfo*** db - A pointer to the array of metadata structures    
//+       for C dataBase linked list.
//!param: MPIStructInfo*** opt - A pointer to the array of metadata structures 
//+       for C multiDimData structure.
//
//!exc: bad_alloc - Memory allocation failed.
//    
void OutputType::allocateMetaData( UINT4** numStates, UINT4** numDB,
   UINT4** numOpt, MPIStructInfo*** states, MPIDataBaseInfo*** db, 
   MPIStructInfo*** opt ) 
try
{
   *numStates = new UINT4[ mNum ];
   memset( *numStates, 0, uint4_size * mNum );
   

   *numDB = new UINT4[ mNum ];   
   memset( *numDB, 0, uint4_size * mNum );   
   

   *numOpt = new UINT4[ mNum ];   
   memset( *numOpt, 0, uint4_size * mNum );         

   
   *states = new MPIStructInfo*[ mNum ];
   memset( *states, 0, sizeof( MPIStructInfo* ) * mNum );   
   

   *db = new MPIDataBaseInfo*[ mNum ];
   memset( *db, 0, sizeof( MPIDataBaseInfo* ) * mNum );

   
   *opt = new MPIStructInfo*[ mNum ];
   memset( *opt, 0, sizeof( MPIStructInfo* ) * mNum );
   
   
   return;
}   
catch( bad_alloc& )
{
   delete[] *numStates;
   delete[] *numDB;      
   delete[] *numOpt;   
   delete[] *states;
   delete[] *db;
   throw;      
}


//------------------------------------------------------------------------------
//
//: Deletes memory for metadata.
//      
// This method releases the memory previously allocated to store metadata.
//   
//!param: const UINT4 num - Number of elements in metadata structures.   
//!param: UINT4** numStates - A pointer to the array of node count in C        
//+       stateVector linked list.
//!param: UINT4** numDB - A pointer to the array of node count in C dataBase   
//+       linked list.
//!param: UINT4** numOpt - A pointer to the array of element count in the      
//+       array of C multiDimData structures.
//!param: MPIStruct*** states - A pointer to the array of metadata structures  
//+       for C stateVector linked list.
//!param: MPIDataBaseInfo*** db - A pointer to the array of metadata structures    
//+       for C dataBase linked list.
//!param: MPIStructInfo*** opt - A pointer to the array of metadata structures 
//+       for C multiDimData structure.
//   
//!return: Nothing.
//   
void OutputType::deleteMetaData( const UINT4 num, 
   UINT4** numStates, UINT4** numDB, UINT4** numOpt,
   MPIStructInfo*** states, MPIDataBaseInfo*** db, MPIStructInfo*** opt ) 
{
   MPIStructInfo**   stateInfo( *states );
   MPIDataBaseInfo** dbInfo( *db );
   MPIStructInfo**   optionalInfo( *opt );

   
   MPIStructInfo* struct_elem( 0 ), *struct_end( 0 );
   MPIDataBaseInfo* db_elem( 0 ), *db_end( 0 );
   
   const UINT4* num_state( *numStates ),
              *const num_end( *numStates + num - 1 );
   const UINT4* num_db( *numDB );
   

   while( num_state <= num_end )
   {
      if( *stateInfo && *num_state )
      {
         struct_elem = *stateInfo;
         struct_end  = *stateInfo + ( *num_state ) - 1;
   
   
         while( struct_elem <= struct_end )
         {
            if( struct_elem->numberDimensions )
            {
               delete[] struct_elem->dimensions;
               struct_elem->dimensions = 0;
            }
          
            if( struct_elem->numHistory )
            {
               delete[] struct_elem->historyInfo;
               struct_elem->historyInfo = 0; 
            }
   
   
            ++struct_elem;
         }
   
   
         delete[] ( *stateInfo );
         *stateInfo = 0;
      }
   
   
      if( *dbInfo && *num_db )
      {
         db_elem = *dbInfo;
         db_end  = *dbInfo + ( *num_db ) - 1;
   
         while( db_elem <= db_end )
         {
            delete[] db_elem->dimensions;
            db_elem->dimensions = 0;
            ++db_elem;
         }
   
         delete[] ( *dbInfo );
         *dbInfo = 0;
      }
   
   
      if( *optionalInfo )
      {
         if( ( *optionalInfo )->numberDimensions )
         {
            delete[] ( *optionalInfo )->dimensions;
            ( *optionalInfo )->dimensions = 0;   
         }
   
         if( ( *optionalInfo )->numHistory )
         {
            delete[] ( *optionalInfo )->historyInfo;
            ( *optionalInfo )->historyInfo = 0;
         }
   
   
         delete[] ( *optionalInfo );
         *optionalInfo = 0;
      }   
   
   
      ++stateInfo;
      ++dbInfo;
      ++optionalInfo;
   
      ++num_state;
      ++num_db;
   }
   
   
   delete[] ( *states );
   *states = 0;
   
   delete[] ( *db );
   *db = 0;
   
   delete[] ( *opt );
   *opt = 0;

   delete[] ( *numStates );
   *numStates = 0;
   
   delete[] ( *numDB );
   *numDB = 0;
   
   delete[] ( *numOpt );
   *numOpt = 0;
   
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Copy metadata.
//   
// This method copies the passed to it structures of metadata into the class's 
// static data members.   
//
//!param: const UINT4* numStates - An array of node count in C stateVector     
//+       linked list.
//!param: const UINT4* numDB - An array of node count in C dataBase linked     
//+       list.
//!param: const UINT4* numOpt - An array of element count in the array of C    
//+       multiDimData structures.
//!param: MPIStruct**const states - An array of arrays of metadata structures  
//+       for C stateVector linked lists.
//!param: MPIDataBaseInfo**const db - An array of arrays of metadata structures    
//+       for C dataBase linked lists.
//!param: MPIStructInfo**const opt - An array of arrays of metadata            
//+       structures for C multiDimData structures.
//
//!exc: bad_alloc - Memory allocation failed.
//       
void OutputType::setMetaData( const UINT4* numStates, const UINT4* numDB, 
   const UINT4* numOpt, MPIStructInfo**const stateInfo, 
   MPIDataBaseInfo**const dbInfo, MPIStructInfo**const optInfo )
{
   
   mInitNum = mNum;
   size_t size( uint4_size * mInitNum );          
   
   memcpy( mNumStates, numStates, size );
   memcpy( mNumDataBase, numDB, size );
   memcpy( mNumOptional, numOpt, size );   
   

   size_t struct_size( sizeof( MPIStructInfo ) ),
          list_size( sizeof( MPIListInfo ) ),
          db_size( sizeof( MPIDataBaseInfo ) );

   // int4_size*2 + uint4_size*4;      
   size = ( int4_size << 1 ) + ( uint4_size << 2 );      

   
   MPIStructInfo* struct_elem( 0 ),
                * struct_end( 0 );
   const MPIStructInfo* s_struct_elem( 0 );

   
   const UINT4* num_states( mNumStates ),
              * num_db( mNumDataBase ),
              * num_opt( mNumOptional ),
              *const num_end( mNumStates + mInitNum - 1 );
   
   MPIStructInfo** state_info( mStatesInfo ),
                ** s_state_info( stateInfo ),
                ** opt_info( mOptionalInfo ),
                ** s_opt_info( optInfo);   
   
   MPIDataBaseInfo** db_info( mDataBaseInfo ),
                  ** s_db_info( dbInfo );

   MPIDataBaseInfo* db_elem( 0 ), 
                  * db_end( 0 );
   const MPIDataBaseInfo* s_db_elem( 0 );
   
   while( num_states <= num_end )
   {
      // Copy states data information
      if( *num_states )
      {
         *state_info = new MPIStructInfo[ *num_states ];
         memset( *state_info, 0, struct_size * ( *num_states ) );


         struct_elem = *state_info;
         struct_end  = *state_info + ( *num_states ) - 1;
         s_struct_elem = *s_state_info;

   
         while( struct_elem <= struct_end )       
         {
            memcpy( struct_elem, s_struct_elem, size );
   
            if( struct_elem->numberDimensions )
            {
               struct_elem->dimensions = 
                  new UINT4[ struct_elem->numberDimensions ];
   
               memcpy( struct_elem->dimensions, 
                       s_struct_elem->dimensions, 
                       uint4_size * struct_elem->numberDimensions );
            }
            
            // Copy history data if present
            if( struct_elem->numHistory )
            {
               struct_elem->historyInfo = 
                  new MPIListInfo[ struct_elem->numHistory ];
   
               memcpy( struct_elem->historyInfo, 
                       s_struct_elem->historyInfo, 
                       list_size * struct_elem->numHistory );
            }
   
            ++struct_elem;
            ++s_struct_elem;
         }
      } // states
   

      ++num_states;
      ++state_info;
      ++s_state_info;
   
   
      // Copy results data information
      if( *num_db )
      {
         *db_info = new MPIDataBaseInfo[ *num_db ];      

         memcpy( *db_info, *s_db_info, db_size * ( *num_db ) );         
   
         db_elem = *db_info;
         db_end  = *db_info + *num_db - 1;
         s_db_elem = *s_db_info;
   
         while( db_elem <= db_end )
         {
            if( s_db_elem->dimensions )
            {
               db_elem->dimensions = new UINT4[ db_elem->numData ];
               memcpy( db_elem->dimensions, s_db_elem->dimensions,
                       uint4_size * db_elem->numData );
            }
            
            ++db_elem;
            ++s_db_elem;
         }
      }
   

      ++num_db;
      ++db_info;
      ++s_db_info;   
   
   
      // Copy optional data information
      if( *num_opt )
      {
         *opt_info = new MPIStructInfo[ 1 ];      
         memcpy( *opt_info, *s_opt_info, size );         
   
         struct_elem = *opt_info;

         if( struct_elem->numberDimensions )
         {
            struct_elem->dimensions = 
                  new UINT4[ struct_elem->numberDimensions ];
   
            memcpy( struct_elem->dimensions,
                    ( *s_opt_info )->dimensions, 
                    uint4_size * struct_elem->numberDimensions );
         }

         if( struct_elem->numHistory )
         {
            struct_elem->historyInfo = 
               new MPIListInfo[ struct_elem->numHistory ];

            memcpy( struct_elem->historyInfo,
                    ( *s_opt_info )->historyInfo,
                    list_size * struct_elem->numHistory );
         }
      }
   

      ++num_opt;
      ++opt_info;
      ++s_opt_info;
   }

   
   return;
}


//------------------------------------------------------------------------------
//
//: Compare metadata.
//   
// This method compares passed to it structures of metadata to the class's 
// static metadata.   
//
//!param: const UINT4* numStates - An array of node count in C stateVector     
//+       linked list.
//!param: const UINT4* numDB - An array of node count in C dataBase linked     
//+       list.
//!param: const UINT4* numOpt - An array of element count in the array of C    
//+       multiDimData structures.
//!param: MPIStruct**const stateInfo - An array of arrays of metadata          
//+       structures for C stateVector linked lists.
//!param: MPIDataBaseInfo**const dbInfo - An array of arrays of metadata           
//+       structures for C dataBase linked lists.
//!param: MPIStructInfo**const optInfo - An array of arrays of metadata        
//+       structures for C multiDimData structures.
//   
//!return: const bool - True if data is not equal, false otherwise.
//   
const bool OutputType::compareMetaData( const UINT4* numStates, 
   const UINT4* numDB, const UINT4* numOpt, 
   MPIStructInfo**const stateInfo, MPIDataBaseInfo**const dbInfo,
   MPIStructInfo**const optInfo ) const 
{
   // Allow less than mInitNum outPut structures
   if( mNum > mInitNum )
   {
      return true;
   }

   size_t size( uint4_size * mNum );          
   
   
   if( memcmp( numStates, mNumStates, size ) ||
       memcmp( numDB, mNumDataBase, size ) ||
       memcmp( numOpt, mNumOptional, size ) )
   {
      return true;
   }   

   
   size_t list_size( sizeof( MPIListInfo ) ),
          db_size( int4_size + uint4_size );
   
   // int4_size * 2 + uint4_size * 4
   size = ( int4_size << 1 ) + ( uint4_size << 2 );      
   
   
   const UINT4* num_states( mNumStates ),
              * num_db( mNumDataBase ),
              * num_opt( mNumOptional ),
              *const num_end( mNumStates + mNum - 1 );
   
   
   MPIStructInfo** state_info( mStatesInfo ),
                      ** s_state_info( stateInfo ),
                      ** opt_info( mOptionalInfo ),
                      ** s_opt_info( optInfo );
   
   MPIDataBaseInfo** db_info( mDataBaseInfo ),
                  ** s_db_info( dbInfo );   
   
   const MPIDataBaseInfo* db_elem( 0 ),
                        * db_end( 0 ),
                        * s_db_elem( 0 );
   
   const MPIStructInfo* struct_elem( 0 ),
                      * struct_end( 0 ),
                      * s_struct_elem( 0 );
   
   
   while( num_states <= num_end )
   {
      // Compare states data information
      if( *num_states )
      {
         struct_elem = *state_info;
         struct_end  = *state_info + ( *num_states ) - 1;   
         s_struct_elem = *s_state_info;

   
         while( struct_elem <= struct_end )       
         {
            if( memcmp( struct_elem, s_struct_elem, size ) || 
                ( struct_elem->numberDimensions && 
                  memcmp( struct_elem->dimensions,
                          s_struct_elem->dimensions ,
                          uint4_size * struct_elem->numberDimensions ) ) || 
                ( struct_elem->numHistory && 
                  memcmp( struct_elem->historyInfo,
                          s_struct_elem->historyInfo,
                          list_size * struct_elem->numHistory ) ) )
            {
               return true;
            }
   
   
            ++struct_elem;
            ++s_struct_elem;
         }            
      } // if mNumStates[ i ] != 0
   
       
      ++num_states;
      ++state_info;
      ++s_state_info;
   
   
      // Compare results data information
      if( *num_db )
      {
          db_elem = *db_info;
          db_end  = *db_info + ( *num_db ) - 1;
          s_db_elem = *s_db_info;
   
          while( db_elem <= db_end )
          {
             if( memcmp( db_elem, s_db_elem, db_size ) ||
                 ( db_elem->dimensions && 
                   memcmp( db_elem->dimensions, s_db_elem->dimensions,
                           uint4_size * db_elem->numData ) ) )
             {
                return true;
             }
   
             ++db_elem;
             ++s_db_elem;
          }
       } // if mNumDataBase[ i ] != 0
   
   
      ++num_db;
      ++db_info;
      ++s_db_info;
   
   
      // Copy optional data information
      if( *num_opt && 
           ( memcmp( *opt_info, *s_opt_info, size ) || 
             ( ( *opt_info )->numberDimensions && 
               memcmp( ( *opt_info )->dimensions,
                       ( *s_opt_info )->dimensions,
                        uint4_size * ( *opt_info )->numberDimensions ) ) ||
             ( ( *opt_info )->numHistory && 
               memcmp( ( *opt_info )->historyInfo,
                       ( *s_opt_info )->historyInfo,
                       list_size * ( *opt_info )->numHistory )
             )
           ) 
         )
      {
         return true;
      }
   
   
      ++num_opt;
      ++opt_info;
      ++s_opt_info;
   }

   
   return false;   
}
   
   
//------------------------------------------------------------------------------
//
//: Broadcast data information.
//
// This method to be called by slave and master nodes constructing the datatype
// for sending the data. Slave node sends necessary data information for all
// structures in the <b>pe</b> array to the master in order for the 
// master to allocate appropriate memory for receiving the data.
//   
//!param: const outPut* pe - An array of outPut structures to be represented   
//+       by MPI datatype.
//!param: UINT4* numStateVector - An array of data information for             
//+       outPut.states stateVector linked lists.
//!param: MPIStructInfo** stateInfo - A pointer to the array of metadata       
//+       structures for output.states stateVector linked lists.   
//!param: UINT4* numDataBase - An array of data information for                
//+       outPut.results dataBase linked lists.
//!param: MPIDataBaseInfo** dbInfo - A pointer to the array of metadata structures 
//+       for outPut.results dataBase linked lists.   
//!param: UINT4* numOptional - An array of data information for                
//+       outPut.optional multiDimData structures.      
//!param: MPIStructInfo** optionalInfo - A pointer to the array of metadata    
//+       structures for outPut.optional multiDimData structures.   
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Index exceeds the size of packing buffer. - Index into the binary      
//+     buffer used for storing the metadata exceeds the size of that     
//+     buffer.  
//!exc: NULL store in stateVector. - Malformed outPut data structure, such as  
//+     one of the nodes in stateVector linked list has store set to NULL.   
//!exc: Inconsistent outPut data structure signature. - Sceleton for outPut    
//+     data structure differs from original signature.
//!exc: dataBase: rowDimensions must be specified for char_s_ptr and 
//+     char_u_ptr datatypes. - Dimensions information is missing for char_s_ptr
//+     or char_u_ptr datatypes of dataBase data.   
//!exc: dataBase: multidimensional data is allowed only for char_s_ptr or 
//+     char_u_ptr datatypes. - Dimensions is specified for other than char_s_ptr
//+     or char_u_ptr datatypes of dataBase data.   
//!exc: MPI::Exception - MPI library exception.      
//   
void OutputType::broadcastInfo( const outPut* pe, 
   UINT4* numStateVector, MPIStructInfo** stateInfo,
   UINT4* numDataBase, MPIDataBaseInfo** dbInfo, 
   UINT4* numOptional, MPIStructInfo** optionalInfo )
{
   const MPI::Intracomm& comm( getComm() );
   
   INT8 buf_size( 0 );
   CHAR* buffer( 0 );

   
   if( communicateOutput || 
       ( !communicateOutput && 
         ( ( !inMaster() && !mInitState ) || 
           ( inMaster() && !inRanks( mInitSlaves, mCommWorldRank ) ) 
         )
       )
     )
   {
      // Get the size of arrays containing the count of structures
      buf_size += 3 * LDAS_MPI_UINT4.Pack_size( mNum, comm );
   
      // The size of all history information
      buf_size += ( listMaxNodes + 1 ) * mNum * 
                  LDAS_MPI_LIST_INFO.Pack_size( listMaxNodes, comm );
   
      // The size of results( list ), states->store( struct ) and optional( struct )
      buf_size += mNum * ( LDAS_MPI_LIST_INFO.Pack_size( listMaxNodes, comm ) +
                           LDAS_MPI_STRUCT_INFO.Pack_size( listMaxNodes + 1, 
                                                           comm ) );

      // The size of dataBase->rowEntryDimensions
      buf_size += mNum * listMaxNodes * LDAS_MPI_UINT4.Pack_size( 
                                           dbMaxDims, comm );
   
      // The size of multiDimData->dimensions
      buf_size += mNum * ( listMaxNodes + 1 ) * 
                  LDAS_MPI_UINT4.Pack_size( multiDimDataMaxDims, comm );

   
      buffer = new CHAR[ buf_size ];
   }
   
   
   INT4 buf_position( 0 );
   MPI::Request request;   
   
   UINT4* num_state( numStateVector ),
         *num_db( numDataBase ),
         *num_opt( numOptional );

   MPIStructInfo**   state_info( stateInfo );
   MPIDataBaseInfo** db_info( dbInfo );   
   MPIStructInfo**   opt_info( optionalInfo );
   
   MPIStructInfo* struct_elem( 0 );
   
   
   if( !inMaster() )
   {
      const outPut* out_elem( pe ),
                  * const out_end( pe + mNum - 1 );   
   
      const stateVector* curr_state( 0 );
      const dataBase*    curr_db( 0 );
   
   
      // Get the number of nodes in the linked lists
      while( out_elem <= out_end )
      {
         curr_state = out_elem->states;
   
         while( curr_state != 0 )
         {
            ++( *num_state );
            curr_state = curr_state->next;
         }
   
   
         curr_db = out_elem->results;
   
         while( curr_db != 0 )
         {
            ++( *num_db );
            curr_db = curr_db->next;
         }   
   
   
         if( out_elem->optional != 0 )
         {
            *num_opt = 1;
         }
   
      
         ++out_elem;
         ++num_state;
         ++num_db;
         ++num_opt;
      }

   
      if( communicateOutput || 
          ( !communicateOutput && !mInitState ) )
      {
         mPackDelta -= MPI::Wtime();
   
         // Slave packs the dimension data   
         LDAS_MPI_UINT4.Pack( numStateVector, mNum, buffer, buf_size,
                              buf_position, comm );
   
         LDAS_MPI_UINT4.Pack( numDataBase, mNum, buffer, buf_size,
                              buf_position, comm );   
   
         LDAS_MPI_UINT4.Pack( numOptional, mNum, buffer, buf_size,
                              buf_position, comm );   

   
         mPackDelta += MPI::Wtime();
   
      }
   
   
      // Slave extracts and packs data information     
      MPIDataBaseInfo* db_elem( 0 );
   
      out_elem  = pe;
      num_state = numStateVector;
      num_db    = numDataBase;
      num_opt   = numOptional;

   
      while( out_elem <= out_end )
      {
         // Get stateVector->store data information
         if( *num_state )
         {
            *state_info = new MPIStructInfo[ *num_state ];   
            memset( *state_info, 0, 
                    sizeof( MPIStructInfo ) * ( *num_state ) );   

   
            curr_state = out_elem->states;    
            struct_elem = *state_info;   
   
            while( curr_state != 0 )
            {
               if( curr_state->store == 0 )
               {
                  node_error( "NULL store in stateVector", comm );
               }

   
               setMultiDimMetadata( curr_state->store, struct_elem, comm );
               
               if( communicateOutput || 
                   ( !communicateOutput && !mInitState ) )
               {   
                  mPackDelta -= MPI::Wtime();
   
                  // Pack stateInfo structure
                  LDAS_MPI_STRUCT_INFO.Pack( struct_elem, 1,
                                             buffer, buf_size, buf_position,
                                             comm );

                  if( struct_elem->numberDimensions )
                  {
                     // Pack stateInfo structure.dimensions
                     LDAS_MPI_UINT4.Pack( struct_elem->dimensions,
                                          struct_elem->numberDimensions,
                                          buffer, buf_size, buf_position, comm );   
                  }
   
                  mPackDelta += MPI::Wtime();                  
      
                  if( buf_position > buf_size )
                  {
                     node_error( "Index exceeds the size of packing buffer",
                                 comm );
                  }   
               }
   
   
               if( struct_elem->numHistory )
               {
                  if( communicateOutput || 
                      ( !communicateOutput && !mInitState ) )
                  {   
                     mPackDelta -= MPI::Wtime();   

                     // Pack stateInfo.historyInfo
                     LDAS_MPI_LIST_INFO.Pack( struct_elem->historyInfo,
                                              struct_elem->numHistory,
                                              buffer, buf_size, buf_position,
                                              comm );   

                     mPackDelta += MPI::Wtime();                     
   
   
                     if( buf_position > buf_size )
                     {
                        node_error( "Index exceeds the size of packing buffer",
                                    comm );
                     }   
                  }
               }
   
   
               curr_state = curr_state->next;
               ++struct_elem;
            }     
         } // if numStateVector[ i ] != 0  
   
   
         // Get dataBase data information
         if( *num_db )
         {
            *db_info = new MPIDataBaseInfo[ *num_db ];   
            memset( *db_info, 0, sizeof( MPIDataBaseInfo ) * ( *num_db ) );
   
            curr_db = out_elem->results;    
            db_elem = *db_info;      
   
   
            while( curr_db != 0 )
            {
               db_elem->type = curr_db->type;
               db_elem->numData = curr_db->numberRows;

               if( ( db_elem->type == char_s_ptr || 
                     db_elem->type == char_u_ptr ) &&
                   curr_db->rowDimensions == 0 )
               {
                  node_error( "dataBase: rowDimensions must be specified "
                              "for char_s_ptr and char_u_ptr datatypes." );
               }
   
               if( curr_db->rowDimensions )
               {
                  // Make sure that multidimensional data is allowed only
                  // for char_s and char_u types( blob data ).
                  if( db_elem->type != char_s_ptr && 
                      db_elem->type != char_u_ptr )
                  {
                     node_error( "dataBase: multidimensional data is allowed "
                                 "only for char_s_ptr or char_u_ptr datatypes.", 
                                 comm );
                  }
   
                  db_elem->dimensions = new UINT4[ db_elem->numData ];
                  memcpy( db_elem->dimensions, curr_db->rowDimensions,
                          uint4_size * db_elem->numData );
               }
   
   
               if( communicateOutput || 
                   ( !communicateOutput && !mInitState ) )
               {   
                  mPackDelta -= MPI::Wtime();
   
                  LDAS_MPI_LIST_INFO.Pack( db_elem, 1, buffer, buf_size,
                                           buf_position, comm );
      
                  if( buf_position > buf_size )
                  {
                     node_error( "Index exceeds the size of packing buffer",
                                 comm );
                  }   
   
                  if( db_elem->dimensions )
                  {
                     LDAS_MPI_UINT4.Pack( db_elem->dimensions,
                                          db_elem->numData,
                                          buffer, buf_size,
                                          buf_position, comm );   
   
                     if( buf_position > buf_size )
                     {
                        node_error( "Index exceeds the size of packing buffer",
                                    comm );
                     }      
                  }
   
                  mPackDelta += MPI::Wtime();         
               }   
   
               curr_db = curr_db->next;
               ++db_elem;
            }     
         }
   
   
         // Get optional data information
         if( *num_opt )
         {
            *opt_info = new MPIStructInfo[ 1 ];   
            memset( *opt_info, 0, sizeof( MPIStructInfo ) );

            struct_elem = *opt_info;
   
            setMultiDimMetadata( out_elem->optional, struct_elem, comm );   
   
            if( communicateOutput || 
                ( !communicateOutput && !mInitState ) )
            {   
               mPackDelta -= MPI::Wtime();
   
               LDAS_MPI_STRUCT_INFO.Pack( struct_elem, 1, buffer, buf_size,
                                          buf_position, comm );
   
               mPackDelta += MPI::Wtime();      
   
   
               if( buf_position > buf_size )
               {
                  node_error( "Index exceeds the size of packing buffer",
                              comm );
               }            
   

               if( struct_elem->numberDimensions )
               {
                  mPackDelta -= MPI::Wtime();   
   
                  LDAS_MPI_UINT4.Pack( struct_elem->dimensions, 
                                       struct_elem->numberDimensions,
                                       buffer, buf_size, buf_position, comm );
   
                  mPackDelta += MPI::Wtime();   
    
                  if( buf_position > buf_size )
                  {
                     node_error( "Index exceeds the size of packing buffer",
                                 comm );
                  }            
               }
            }
   
            
            if( struct_elem->numHistory != 0 )
            {
               if( communicateOutput || 
                   ( !communicateOutput && !mInitState ) )
               {   
                  mPackDelta -= MPI::Wtime();   
   
                  LDAS_MPI_LIST_INFO.Pack( struct_elem->historyInfo,
                                           struct_elem->numHistory,
                                           buffer, buf_size, buf_position, 
                                           comm );   
   
                  mPackDelta += MPI::Wtime();      
   
                  if( buf_position > buf_size )
                  {
                     node_error( "Index exceeds the size of packing buffer",
                                 comm );
                  } 
               } // Packing
            } // if optional->numHistory != 0
         } // if numOptional[ i ] != 0
   
   
         ++out_elem;
         ++num_state;
         ++num_db;
         ++num_opt;
   
         ++state_info;
         ++db_info;
         ++opt_info;
      }   


      if( communicateOutput ||
          ( !communicateOutput && !mInitState ) )
      {
         if( buf_position + 16 >= buf_size )
         {
            buf_position = buf_size;
         }
         else
         {
            buf_position += 16;
         }

   
         mBcastDelta -= MPI::Wtime();   
    
         request = comm.Isend( buffer, buf_position, MPI::PACKED, 0, 
                               STRUCT_INFO_TAG );  

         node_error.pass( request, comm );   
         mBcastDelta += MPI::Wtime();

   
         if( !communicateOutput )
         {
            setMetaData( numStateVector, numDataBase, numOptional,
                         stateInfo, dbInfo, optionalInfo );
         }
      }
      else 
      if( compareMetaData( numStateVector, numDataBase, numOptional,
                           stateInfo, dbInfo, optionalInfo ) )
      {
         node_error( "Inconsistent outPut data structure signature.", 
                     comm );   
      }

   }
   else
   {
      mBcastDelta -= MPI::Wtime();
   
      // Master unpacks the dimension data
      request = comm.Irecv( buffer, buf_size, MPI::PACKED, getRank(),
                            STRUCT_INFO_TAG );   

      node_error.pass( request, comm );      
      mBcastDelta += MPI::Wtime();      

   
      mPackDelta -= MPI::Wtime();
   
      LDAS_MPI_UINT4.Unpack( buffer, buf_size, numStateVector, mNum,
                             buf_position, comm );

      LDAS_MPI_UINT4.Unpack( buffer, buf_size, numDataBase, mNum,
                             buf_position, comm );   
   
      LDAS_MPI_UINT4.Unpack( buffer, buf_size, numOptional, mNum,
                             buf_position, comm );   

      mPackDelta += MPI::Wtime();   


      const UINT4* num_end( numStateVector + mNum - 1 );
      MPIStructInfo* struct_end( 0 );

      MPIDataBaseInfo* db_elem( 0 );
      const MPIDataBaseInfo* db_end( 0 );       
   
      // Master unpacks data information     
      //while(  i < mNum; ++i )
      while( num_state <= num_end )
      {
         // Get stateVector data information
         if( *num_state )
         {
            *state_info = new MPIStructInfo[ *num_state ];      
            memset( *state_info, 0, 
                    sizeof( MPIStructInfo ) * ( *num_state ) );

   
            struct_elem = *state_info;
            struct_end = *state_info + ( *num_state ) - 1;
   
   
            while( struct_elem <= struct_end )
            {
               mPackDelta -= MPI::Wtime();   

               LDAS_MPI_STRUCT_INFO.Unpack( buffer, buf_size, 
                                            struct_elem, 1, 
                                            buf_position, comm );   
   
               mPackDelta += MPI::Wtime();      
   
   
               if( buf_position > buf_size )
               {
                  node_error( "Index exceeds the size of packing buffer",
                              comm );
               }   

   
               if( struct_elem->numberDimensions )
               {
                  struct_elem->dimensions = 
                     new UINT4[ struct_elem->numberDimensions ];
   
                  mPackDelta -= MPI::Wtime();
   
                  LDAS_MPI_UINT4.Unpack( buffer, buf_size, 
                                         struct_elem->dimensions,
                                         struct_elem->numberDimensions,
                                         buf_position, comm );
   
                  mPackDelta += MPI::Wtime();
               }
   
   
               if( struct_elem->numHistory )
               {
                  struct_elem->historyInfo = 
                     new MPIListInfo[ struct_elem->numHistory ];
   
                  mPackDelta -= MPI::Wtime();
   
                  LDAS_MPI_LIST_INFO.Unpack( buffer, buf_size, 
                                             struct_elem->historyInfo,
                                             struct_elem->numHistory,
                                             buf_position, comm );
   
                  mPackDelta += MPI::Wtime();
   
                  if( buf_position > buf_size )
                  {
                     node_error( "Index exceeds the size of packing buffer",
                                 comm );
                  }      
               }
         
              
               ++struct_elem;
            }
         }  // if numStateVector[ i ] != 0 
   
   
         // Get dataBase data information
         if( *num_db )
         {
            *db_info = new MPIDataBaseInfo[ *num_db ];      
            memset( *db_info, 0, sizeof( MPIDataBaseInfo ) * ( *num_db ) );

   
            db_elem = *db_info;
            db_end  = *db_info + ( *num_db ) - 1;
   
            while( db_elem <= db_end )
            {
               mPackDelta -= MPI::Wtime();
   
               LDAS_MPI_LIST_INFO.Unpack( buffer, buf_size, db_elem, 
                                          1, buf_position, comm );   

               mPackDelta += MPI::Wtime();      
   
   
               if( buf_position > buf_size )
               {
                  node_error( "Index exceeds the size of packing buffer",
                              comm );
               }      
   
   
               // It's multidimensional( blob ) data, get dimensions
               if( db_elem->type == char_s_ptr || 
                   db_elem->type == char_u_ptr )
               {
                  db_elem->dimensions = new UINT4[ db_elem->numData ];
   
                  mPackDelta -= MPI::Wtime();   
   
                  LDAS_MPI_UINT4.Unpack( buffer, buf_size, 
                                         db_elem->dimensions,
                                         db_elem->numData,
                                         buf_position, comm );   
   
                  mPackDelta += MPI::Wtime();         
   
                  if( buf_position > buf_size )
                  {
                     node_error( "Index exceeds the size of packing buffer",
                                 comm );
                  }         
               }
   
               ++db_elem;
            }
         }
   
   
         // Get optional data information
         if( *num_opt )
         {
            *opt_info = new MPIStructInfo[ 1 ];      
            memset( *opt_info, 0, sizeof( MPIStructInfo ) );

            struct_elem = *opt_info;
   
   
            mPackDelta -= MPI::Wtime();
   
            LDAS_MPI_STRUCT_INFO.Unpack( buffer, buf_size, 
                                         struct_elem, 1,
                                         buf_position, comm );   
   
            mPackDelta += MPI::Wtime();   
   
   
            if( buf_position > buf_size )
            {
               node_error( "Index exceeds the size of packing buffer",
                           comm );
            }      
   
            if( struct_elem->numberDimensions )
            {
               struct_elem->dimensions = 
                  new UINT4[ struct_elem->numberDimensions ];
   
   
               mPackDelta -= MPI::Wtime();
   
               LDAS_MPI_UINT4.Unpack( buffer, buf_size, 
                                      struct_elem->dimensions, 
                                      struct_elem->numberDimensions,
                                      buf_position, comm );   
   
               mPackDelta += MPI::Wtime();               
            }

   
            if( struct_elem->numHistory )
            {
               struct_elem->historyInfo = 
                     new MPIListInfo[ struct_elem->numHistory ];

               mPackDelta -= MPI::Wtime();
   
               LDAS_MPI_LIST_INFO.Unpack( buffer, buf_size, 
                                          struct_elem->historyInfo,
                                          struct_elem->numHistory,
                                          buf_position, comm );
   
               mPackDelta += MPI::Wtime();
            }
   
   
            if( buf_position > buf_size )
            {
               node_error( "Index exceeds the size of packing buffer",
                           comm );
            }      
         } // if numOptional[ i ] != 0     
   
   
         ++num_state;
         ++num_db;
         ++num_opt;
   
         ++state_info;
         ++db_info;
         ++opt_info;   
      }   
   
      
      if( !communicateOutput )
      {
         if( !mInitState )
         {
            setMetaData( numStateVector, numDataBase, numOptional,
                         stateInfo, dbInfo, optionalInfo );
         }
         else 
         if( compareMetaData( numStateVector, numDataBase, numOptional,
                           stateInfo, dbInfo, optionalInfo ) )
         {
            node_error( "Inconsistent outPut data structure signature.",
                        comm );
         }
   
   
         mInitSlaves.push_back( mCommWorldRank );
      }
   
   }

   
   // Memory cleanup
   delete[] buffer;
   
   return;   
}

   
//------------------------------------------------------------------------------
//
//: Sets metadata for the specific multiDimData structure.
//
//!param: const multiDimData* source - A pointer to the multiDimData structure.   
//!param: MPIStructInfo* const meta - A pointer to the metadata    
//+       structure for multiDimData structure.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which OutputDatatype is defined.    
//   
//!return: Nothing.
//   
void  OutputType::setMultiDimMetadata( const multiDimData* source, 
   MPIStructInfo* const meta, const MPI::Intracomm& comm )
{
   meta->space = source->space;
   
   meta->type = source->type;
   
   meta->numberDimensions = source->numberDimensions;   
   
   if( meta->numberDimensions )
   {
      meta->dimensions = new UINT4[ meta->numberDimensions ];
   
      memcpy( meta->dimensions, 
              source->dimensions, 
              uint4_size * meta->numberDimensions );   
   }
   
   // Get channel and detector information
   CHAR** channels( 0 );

   switch( source->space )
   {
      case(noneD):
      {
         meta->numberChannels = source->range.dNone.numberChannels;
         channels = source->range.dNone.channelName;
   
         meta->numberDetectors = source->range.dNone.numberDetectors;
         break;
      }   
      case(timeD):
      {
         meta->numberChannels = source->range.dTime.numberChannels;
         channels = source->range.dTime.channelName;
   
         meta->numberDetectors = source->range.dTime.numberDetectors;
         break;
      }
      case(freqD):
      {
         meta->numberChannels = source->range.dFreq.numberChannels;   
         channels = source->range.dFreq.channelName;
   
         meta->numberDetectors = source->range.dFreq.numberDetectors;   
         break;
      }   
      case(bothD):
      {
         meta->numberChannels = source->range.dBoth.numberChannels;   
         channels = source->range.dBoth.channelName;
   
         meta->numberDetectors = source->range.dBoth.numberDetectors;   
         break;
      }   
      case(databaseD):
      {
         break;
      }      
      default:
      {
         node_error( "Undefined domain.", comm );
         break;
      }      
   }

   
   for( UINT4 index = 0; index < meta->numberChannels; ++index )
   {
      meta->channelSize += strlen( channels[ index ] ) + 1;     
   }

   // Get history information
   const dcHistory* curr_hist( source->history );
   
   while( curr_hist != 0 )
   {
      ++( meta->numHistory );
      curr_hist = curr_hist->next;
   }
   
   if( meta->numHistory )
   {
      meta->historyInfo = new MPIListInfo[ meta->numHistory ];

      curr_hist = source->history;
      MPIListInfo* list_elem( meta->historyInfo );
                
      while( curr_hist != 0 )
      {
         list_elem->type = curr_hist->type;
         list_elem->numData = curr_hist->numberValues;
         curr_hist = curr_hist->next;
         ++list_elem;
      }
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Build MPI derived datatype.
//      
// This method must be called by nodes in the communicator using constructed 
// datatype for communication.   
// It generates MPI derived datatype to represent an array of C outPut data
// structures. Passed to this method MPI derived datatype objects will be marked 
// for deallocation.   
//   
//!param: outPut* pe - An array of C outPut data structures.   
//!param: MPI::Datatype& stateType[] - A pointer to the array of MPI            
//+       datatypes representing C stateVector linked lists(outPut.states).
//!param: MPI::Datatype& dbType[] - A pointer to the array of the MPI           
//+       datatype representing C dataBase linked lists( outPut.results )
//!param: MPI::Datatype& optType[] - A pointer to the array of MPI datatypes    
//+       where each element represents an arrays of C multiDimData        
//+       structures( outPut.optional ).   
//
//!return: Nothing.
//
//!exc: Error generating OutputType. - Error generating MPI datatype to         
//+     represent an array of C outPut data structures.
//!exc: bad_alloc - Error allocating new memory.   
//!exc: MPI::Exception - MPI library exception.      
//   
void OutputType::buildType( outPut* pe, std::vector< MPI::Datatype >& stateType, 
   std::vector< MPI::Datatype >& dbType, std::vector< MPI::Datatype >& optType ) 
{
   const INT4 num( mNum * mTypeNum );
   
   vector< INT4 >          blocks( num );
   vector< MPI::Datatype > types( num );
   vector< MPI::Aint >     disps( num );

   INT4 i_num( 0 );
   outPut* out_elem( pe );

   
   for( UINT4 i = 0; i < mNum; ++i )
   {
      // Init indexNumber info
      types[ i_num ]  = LDAS_MPI_INT8;
      disps[ i_num ]  = MPI::Get_address( &( out_elem->indexNumber ) );
      blocks[ i_num ] = 1;
    
   
      // Init search info
      ++i_num;
      types[ i_num ]  = LDAS_MPI_INT4;
      disps[ i_num ]  = MPI::Get_address( &( out_elem->search ) );
      blocks[ i_num ] = 1;        
   
   
      // Init significant info
      ++i_num;
      types[ i_num ]  = LDAS_MPI_BOOLEAN;
      disps[ i_num ]  = MPI::Get_address( &( out_elem->significant ) );
      blocks[ i_num ] = 1;   
   

      // Init states info
      if( stateType[ i ] != MPI::DATATYPE_NULL )
      {
         ++i_num;
         types[ i_num ]  = stateType[ i ];
         disps[ i_num ]  = MPI::Get_address( out_elem->states->stateName );
         blocks[ i_num ] = 1;      
      }
   
   
      // Init results info
      if( dbType[ i ] != MPI::DATATYPE_NULL )
      {
         ++i_num;
         types[ i_num ]  = dbType[ i ];
         disps[ i_num ]  = MPI::Get_address( out_elem->results->tableName );
         blocks[ i_num ] = 1;      
      }

   
      // Init optional info
      if( optType[ i ] != MPI::DATATYPE_NULL )
      {
         ++i_num;
         types[ i_num ]  = optType[ i ];
         disps[ i_num ]  = MPI::Get_address( out_elem->optional[ 0 ].name );
         blocks[ i_num ] = 1;      
      }
   
   
      ++i_num;
      ++out_elem;
   }

   createType( i_num, blocks, disps, types );

   
   for( UINT4 i = 0; i < mNum; ++i )
   { 
      if( stateType[ i ] != MPI::DATATYPE_NULL )
      {
         stateType[ i ].Free();
      }
   
      if( dbType[ i ] != MPI::DATATYPE_NULL )
      {
         dbType[ i ].Free();
      }
   
      if( optType[ i ] != MPI::DATATYPE_NULL )
      {   
         optType[ i ].Free();
      }
   }
   
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* OutputType::getDatatypeName() const
{
   return "OutputType";
}
   

//------------------------------------------------------------------------------
//
//: Allocates the memory and copies the data for mOutput.
//
//!param: const outPut* pe - An array of C outPut data structures to copy the  
//+       data from.
//!param: const UINT4 num - A number of elements in the array.   
//
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined datatype. - Undefined datatype was specified for the data    
//+     array.      
//      
void OutputType::storeData( outPut* pe, const UINT4 num ) 
{
   if( pe == 0 || num == 0 )
   {
      return;
   }
   
   
   mOutput = new outPut[ num ];
   
   memset( mOutput, 0, sizeof( outPut ) * num );   
   
   mNum = num;
   
   
   stateVector* d_prev_node( 0 ), *d_curr_node( 0 );
   const stateVector* s_curr_node( 0 );   

   dataBase* db_prev( 0 ), *db_curr( 0 );
   const dataBase* sdb_curr( 0 );   
   
   bool first_node( true );
   UINT4 db_num( 0 );
   
   
   for( UINT4 i = 0; i < num; ++i )
   {
      mOutput[ i ].indexNumber = pe[ i ].indexNumber;
      mOutput[ i ].search = pe[ i ].search; 
      mOutput[ i ].significant = pe[ i ].significant;   
      
      // states( stateVector linked list )
      d_prev_node = 0;
      s_curr_node = pe[ i ].states;
      first_node = true;  
    
      
      // Allocate memory for mOutput[ i ].states and
      // copy the data
      while( s_curr_node != 0 )
      {
         d_curr_node = new stateVector;
         memset( d_curr_node, 0, sizeof( stateVector ) );
   
         memcpy( d_curr_node->stateName, s_curr_node->stateName, 
            maxStateName * sizeof( CHAR ) );

         // Allocate memory for store and copy its data
         d_curr_node->store = new multiDimData[ 1 ];
         storeMultiDimData( d_curr_node->store, s_curr_node->store, 
                            1, getComm(), isP2P() );
      
         if( first_node )
         {
            mOutput[ i ].states = d_curr_node;
            first_node = false;
         }
         else
         {
            d_curr_node->previous = d_prev_node;
            d_prev_node->next = d_curr_node;
         }
   
         d_prev_node = d_curr_node;
         s_curr_node = s_curr_node->next;       
      }

   
      // results( dataBase linked list )
      db_prev = 0;
      db_curr = 0;
      sdb_curr = pe[ i ].results;
      first_node = true;

   
      // Allocate memory for mOutput[ i ].results and
      // copy the data
      while( sdb_curr != 0 )
      {
         db_curr = new dataBase;
         memset( db_curr, 0, sizeof( dataBase ) );         

         // Copy table and column name
         memcpy( db_curr->tableName, sdb_curr->tableName, 
                 sizeof( CHAR ) * dbNameLimit * 2 );

         db_curr->type = sdb_curr->type;
         db_curr->numberRows = sdb_curr->numberRows;
   
         if( db_curr->type == char_s_ptr ||
             db_curr->type == char_u_ptr )
         {
            if( sdb_curr->rowDimensions == 0 )
            {
               node_error( "dataBase: rowDimensions must be "
                           "specified for char_s_ptr or char_u_ptr",
                           getComm() );
            }
           
            db_curr->rowDimensions = new UINT4[ db_curr->numberRows ];
            memcpy( db_curr->rowDimensions, sdb_curr->rowDimensions,
                    uint4_size * db_curr->numberRows );
   
            db_num = LdasData::getBlobNDim( sdb_curr->numberRows,
                                            sdb_curr->rowDimensions );
            
         }
         else
         {
            db_num = db_curr->numberRows;
         }
   
         // These methods won't do anything if rows=0
         // Allocate memory for data:
         LdasData::allocateDataArray( &( db_curr->rows ), 
            db_curr->type, db_num, getComm() );   

         // Get the data:
         LdasData::setDataArray( &( db_curr->rows ), sdb_curr->rows,
            db_curr->type, db_num, getComm() );
   
   
         if( first_node )
         {
            mOutput[ i ].results = db_curr;
            first_node = false;
         }
         else
         {
            db_curr->previous = db_prev;
            db_prev->next = db_curr;
         }
   
         db_prev = db_curr;         
         sdb_curr = sdb_curr->next;
      }
   
   
      // optional( multiDimData structure )
      if( pe[ i ].optional != 0 )
      {
         mOutput[ i ].optional = new multiDimData[ 1 ];
   
         storeMultiDimData( mOutput[ i ].optional, 
                            pe[ i ].optional, 1, 
                            getComm(), isP2P() );
      }
   }
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Cleanup utility.
//      
// This method frees all dynamically allocated memory for the array of outPut
// structures.   
//   
//!param: outPut* result - An array of dynamically allocated C outPut data     
//+       structures.
//!param: const UINT4 num - Number of elements in the array.
//
//!return: Nothing.
//   
void OutputType::cleanup( outPut* result, const UINT4 num,
   const MPI::Intracomm& comm ) 
{
   if( result != 0 )
   {
      stateVector* curr_node( 0 ), *next_node( 0 );
      dataBase* db_curr( 0 ), *db_next( 0 );   

      outPut* result_elem( 0 );

   
      for( UINT4 i = 0; i < num; ++i )
      {
         result_elem = &( result[ i ] );
   
   
         // Delete states
         curr_node = result_elem->states; 
      
         while( curr_node != 0 )
         {
            // There will be only one multiDimData structure per each node
            deleteMultiDimData( curr_node->store, 1, comm );         
            delete[] curr_node->store;
            curr_node->store = 0;
   
            next_node = curr_node->next;   
             
            delete curr_node;

            curr_node = next_node;
         }
    
         result_elem->states = 0;
   
       
         // Delete results: 
         db_curr = result_elem->results;
    
         while( db_curr != 0 )
         {
            delete[] db_curr->rowDimensions;
            db_curr->rowDimensions = 0;
   
            LdasData::deleteDataArray( &( db_curr->rows ), 
               db_curr->type, comm );
   
            db_next = db_curr->next;
    
            delete db_curr;
   
            db_curr = db_next;
         }         
        
         result_elem->results = 0;
   
               
         // Delete optional
         if( result_elem->optional != 0 )
         {
            deleteMultiDimData( result_elem->optional, 1, comm );         
            delete[] result_elem->optional;
   
            result_elem->optional = 0; 
         }
      }
   }

   return;
}
