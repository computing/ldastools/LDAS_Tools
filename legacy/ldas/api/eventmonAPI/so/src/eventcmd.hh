#ifndef EventCommandHH
#define EventCommandHH   
   
// System Header Files   
#include <string>   
   
// ILWD Header Files
#include <ilwd/ldascontainer.hh>
   
// Generic Header Files   
#include <genericAPI/threaddecl.hh>
#include <genericAPI/tid.hh>   
   
class LdasException;
class SwigException;   
   
   
//------------------------------------------------------------------------------
//
//: Adds new ILWD format element to the job data bucket.
//   
// This function adds passed to it ILWD format element to the specified by 
// <i>jobID</i> data bucket. If this is a new job, data bucket will be 
// instantiated and first data element will be inserted.
// <p>This method automatically handles data bucket removal on job completion,
// meaning when data bucket receives the very last data element, it will sort
// the data. As a result of sorting it will create three new ILWD format 
// elements representing stateVector, multiDimData and dataBase data( please
// note the order of the elements ).
// Right before returning these three pointers to the Tcl layer event registry
// will remove the entry for this complete job, meaning that it will delete
// all C++ objects representing the job data and unregister these objects from
// Registry::elementRegistry.      
//      
//!param: unsigned int jobID - Data bucket job ID to add data element to.
//!param: LdasContainer* e - A pointer to the ILWD format data element.   
//!param: const bool selfDestruct - A flag to trigger C++ bucket self 
//+       destruction after processing last data product. Default is TRUE.
//+       This flag should be set to FALSE only for "putStandAlone" user 
//+       command.   
//   
//!return: string - Empty string if ILWD format element added to the bucket 
//+        wasn't the last element for the job, string containing three ILWD format
//+        elements pointers representing stateVector, multiDimData and dataBase
//+        data.   
//   
std::string addData( const unsigned int jobID, ILwd::LdasContainer* e, 
   const bool selfDestruct = true );

CREATE_THREADED3_DECL( addData, std::string, 
                       const unsigned int, ILwd::LdasContainer*, const bool );   

   
//------------------------------------------------------------------------------
//
//: Removes job data bucket.
//      
// This function removes entry for a particular job specified by <i>jobID</i>.
// Before removal it will force data bucket contents to be sorted as in
// <i>addJobDataElement</i> function when last data is added to the bucket. 
//
//!param: unsigned int jobID - ID of the job to remove.  
//   
//!return: string - String containing three ILWD format elements pointers
//+        representing stateVector, multiDimData and dataBase data.      
//   
std::string removeJob( const unsigned int jobID );

CREATE_THREADED1_DECL( removeJob, std::string, const unsigned int );      

   
//------------------------------------------------------------------------------
//
//: Destroys job data bucket.
//      
// This function destroys entry for a particular job specified by <i>jobID</i>.
// It doesn't sort the data bucket content, i.e. all data elements associated
// with the job will be destroyed.
//
//!usage: killJob jobID
//   
//!param: unsigned int jobID - ID of the job to remove.  
//   
//!return: Nothing.
//      
void killJob( const unsigned int jobID );

CREATE_THREADED1V_DECL( killJob, const unsigned int );      
   
   
//------------------------------------------------------------------------------
//
//: Sets singleFrame flag for the job.
//   
//   
// This function sets singleFrame flag for the <i>jobID</i> job.
// If this job isn't registered by the eventmonAPI, data bucket will be 
// instantiated and singleFrame flag will be set to the passed <i>flag</i>
// value. If <i>singleFrame</i> is set to true, all multiDimData for the job
// will be represented by a single ILWD format frame, if set to false - there
// will be a frame object per multiDimData.   
//      
//!usage: setSingleFrame jobID value
//   
//!param: unsigned int jobID - Job ID.
//!param: const bool flag - A flag to specify output format for the job's
//+       multiDimData data.
//   
//!return: Nothing.
//      
void setSingleFrame( const unsigned int jobID, const bool flag );

CREATE_THREADED2V_DECL( setSingleFrame,  
                        const unsigned int, const bool );   
   
   
//------------------------------------------------------------------------------
//
//: Sets maximum number of database rows job can generate per second of 
//: analyzed data.
//   
// This is an initialization routine to be called at API start up time.
// It limits number of rows per second of analyzed data job can generate
// for database insertion. If set to -1, number of rows is unlimited.
//
//!param: const int num - Allowed number of rows.
//
//!return: Nothing.
//   
void setMaxDatabaseRows( const int num );   
  
   
//------------------------------------------------------------------------------
//
//: Gets metadata for ILWD format data for a particular job.
//            
// This commands extracts metadata on ILWD format data elements for specified
// job.
// 
//!param: const unsigned int jobID - Job ID.
//
//!return: string - Content of metadata for all data elements for <i>jobID</i>.
//   
std::string listJob( const unsigned int jobID );   

CREATE_THREADED1_DECL( listJob, std::string, const unsigned int );      

   
//------------------------------------------------------------------------------
//
//: Gets metadata for all jobs in the event registry.
//            
// This commands extracts metadata on ILWD format data elements for all
// jobs registered in the eventRegistry.
// 
//!return: string - Content of metadata for all jobs.
//   
std::string listAllJobs();   

CREATE_THREADED0_DECL( listAllJobs, std::string );         
   
   
#endif   
