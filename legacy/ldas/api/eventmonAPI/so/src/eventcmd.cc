// Local Header Files
#include "eventcmd.hh"
#include "eventregistry.hh"
#include "eventdata.hh"   
#include "eventerrors.hh"
#include "dbconverter.hh"   
   
// General Header Files   
#include <general/ldasexception.hh>
#include <general/objectregistry.hh>
   
// GenericAPI Header Files   
#include <genericAPI/swigexception.hh>      
#include <genericAPI/registry.hh>   
#include <genericAPI/thread.hh>
   
using namespace std;   
using ILwd::LdasContainer;   
using namespace eventMonitor;
   

// Global event registry class, all jobs will be registered by 
// this object.   
EventRegistry eventRegistry;   

   
//------------------------------------------------------------------------------
//
//: Adds new ILWD format element to the job data bucket.
//   
string addData( const unsigned int jobID, LdasContainer* e, 
   const bool selfDestruct )
{
   if( !Registry::elementRegistry.isRegistered( e ) )
   {
       throw SWIGEXCEPTION( "invalid_element" );
   }

   
   return eventRegistry.updateEntry( jobID, e, selfDestruct );
}

   
//------------------------------------------------------------------------------
//
//: Removes job entry from eventRegistry.
//         
string removeJob( const unsigned int jobID )
{
   // EventData must sort the data and create 3 ILWD elements
   // before it gets deleted.   
   return eventRegistry.removeEntry( jobID );   
}

   
//------------------------------------------------------------------------------
//
//: Destroys job entry in eventRegistry.
//         
void killJob( const unsigned int jobID )
{
   // EventData must be deleted without sorting the data bucket.
   return eventRegistry.killEntry( jobID );   
}
   
   
//------------------------------------------------------------------------------
//
//: Sets singleFrame flag for the job.
//   
void setSingleFrame( const unsigned int jobID, const bool flag )
{
   return eventRegistry.updateEntry( jobID, flag );
}
   

//------------------------------------------------------------------------------
//
//: Sets maximum number of database rows job can generate per second of 
//: analyzed data.
//   
// This is an initialization routine to be called at API start up time.
// It limits number of rows per second of analyzed data job can generate
// for database insertion. If set to -1, number of rows is unlimited.
//
void setMaxDatabaseRows( const int num )
{   
   DataBaseConverter::setMaxNumberRows( num );
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets metadata for ILWD format data for a particular job.
//            
string listJob( const unsigned int jobID )
{
   return eventRegistry.listEntry( jobID );   
}   

   
//------------------------------------------------------------------------------
//
//: Gets metadata for all jobs in the event registry.
//               
string listAllJobs()
{
   return eventRegistry.listAllEntries();   
}   
   
  
CREATE_THREADED3( addData, string, const unsigned int, 
   LdasContainer*, const bool )
CREATE_THREADED1( removeJob, string, const unsigned int )
CREATE_THREADED1V( killJob, const unsigned int )
CREATE_THREADED2V( setSingleFrame, const unsigned int, 
   const bool )
CREATE_THREADED1( listJob, string, const unsigned int )
CREATE_THREADED0( listAllJobs, string )
