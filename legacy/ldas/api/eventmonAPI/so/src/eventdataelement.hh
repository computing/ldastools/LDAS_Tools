#ifndef EventDataElementHH
#define EventDataElementHH   

// System Header Files
#include <string>   
#include <list>      
#include <memory>   
   
// General Header Files
#include "general/unordered_map.hh"
#include "general/types.hh"

// ILWD Header Files
#include <ilwd/ldascontainer.hh>      
   
   
namespace eventMonitor
{
   // Classes
   class EventException;

//------------------------------------------------------------------------------
//
//: Event data element class.
//    
// This class is designed to encapsulate an information about one ILWD format data 
// element. It implements one data element in the particular job data bucket. 
// EventDataElement object takes over the ownership of the passed to it object 
// unless object is explicitely released by "releaseData" method to the caller.
//       
class EventDataElement 
{
public:   
   
   EventDataElement( std::unique_ptr< ILwd::LdasContainer >& e ); 
   ~EventDataElement();

   enum DataKey{
      PROCESS,
      DB,
      STATE,
      MDD
   };
   
   // Accessors
   const std::list< ILwd::LdasContainer* > getData( 
      const DataKey& key ) const;
   INT_4S getAnalyzedSeconds() const;

   // Helper methods
   bool hasData( const DataKey& key ) const;   
   std::unique_ptr< ILwd::LdasContainer > releaseData();
   
   const ILwd::LdasContainer* getData() const;


private:

   // No default constructor:
   EventDataElement();
   
   // No copy constructor:
   EventDataElement( const EventDataElement& );
  
   // No assignment operator
   const EventDataElement& operator=( const EventDataElement& );   
   
   //: Hash specification for DataKey. 
   class DataKeyHash
   {
   public:
//------------------------------------------------------------------------------
//
//: Hash function for the DataKey.
//         
//!param: const DataKey& e - A reference to the key.
//   
//!return: size_t - An index into the hash.
//    
      size_t operator()( const DataKey& e ) const
      {
         General::hash< INT_4S > h;
         return h( static_cast< INT_4S >( e ) );
      }
   };    
   
   
   // If 'mData' container contains specific data ===>
   // what container elements( which outPut's ) contain it
   typedef General::unordered_map< DataKey, 
				   std::list< ILwd::LdasContainer* >, 
				   DataKeyHash > dataHash;   
   
   // pair of key name and its name field
   typedef std::pair< std::string, size_t > keyPair;
   // unordered_map of key and name associated with the key   
   typedef General::unordered_map< DataKey, keyPair, DataKeyHash > keyHash;
   typedef keyHash::value_type keyValueType;   
   
   typedef General::unordered_map< DataKey, std::string, DataKeyHash > metaHash;   
   typedef metaHash::value_type metaValueType;      

   // Helpers
   void evaluateData();
   bool isProcessInfo();
   
   void evaluateElement( ILwd::LdasContainer* const c,
      const DataKey& key );
   
   void cleanup();

   
   // Static data members
   static const keyHash initKeyHash();
   static const metaHash initMetaHash();   

   //: Metadata attribute of all outPut ILWD objects
   //: (wrapperAPI sets it)
   static const std::string mAnalyzedSecondsMeta;

   static keyHash mKeyHash;   
   static metaHash mMetaHash;
   
   //: unordered_map to hold parsing info for mData
   dataHash mDataHash;
   
   //: A pointer to the ILWD format data element. 
   ILwd::LdasContainer* mData;
};

}
   
#endif   
