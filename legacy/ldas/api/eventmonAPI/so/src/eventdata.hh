#ifndef EventDataHH
#define EventDataHH   

// General Header Files
#include <general/types.hh>   

// System Header Files   
#include <list>
#include <string>   
#include <memory>   
#include <pthread.h>   

#include "general/Memory.hh"

namespace ILwd
{
   class LdasContainer;   
} 
   
namespace eventMonitor
{
   // Classes
   class FrameConverter;
   class DataBaseConverter;
   class EventDataElement;

   
//------------------------------------------------------------------------------
//
//: Event data class.
//    
// This class is designed to keep a track of ILWD format data associated with
// a particular LDAS job. It implements particular job data bucket. 
//       
class EventData 
{
public:

   EventData( const INT_4U& id, std::unique_ptr< ILwd::LdasContainer >& e,
              const bool selfDestruct ); 
   EventData( const INT_4U& id, const bool& single_frame );    
   
   // Destructor
   ~EventData();
   
   const std::string addElement( std::unique_ptr< ILwd::LdasContainer >& e,
                                 const bool selfDestruct ); 
   const std::string getMetaData();
   const std::string getSortedData_Lock();
   void setSingleFrame( const bool& flag );

private:

   // No default constructor
   EventData(); 
   
   // No copy constructor:
   EventData( const EventData& );
   
   // No assignment operator
   const EventData& operator=( const EventData& );
   
   // Typedefs 
   typedef std::list< ILwd::LdasContainer* > dataList;
   typedef dataList::iterator iterator; 
   typedef dataList::const_iterator const_iterator;   

   const std::string getSortedData();
   void sortData( const EventDataElement& elem );
   void updateState( const dataList& states );
   
   bool isMember( const ILwd::LdasContainer* const e ) const;
   void checkWrapperCount();
   
   void cleanup();   
   void eraseProcessList();
   const std::string registerObject( const std::string& msg = "",
      ILwd::LdasContainer* const e = 0 ) const;

   //: Job ID for the data bucket.
   const INT_4U mID;

   //: Number of objects job has generated within wrapperAPI
   INT_4U mWrapperCount;
   
   //: Number of objects API has received from wrapperAPI
   INT_4U mReceivedCount;   
   
   //: Flag to specify if job should generate a single frame
   //: (default is false: multiple frames)
   bool mSingleFrame;   

   //: List of process data elements
   dataList mProcessList;
   
   //: Object lock.
   pthread_mutex_t mLock;
   
   //: Result state data
   std::unique_ptr< ILwd::LdasContainer > mState;
   
   //: Result mdd data
   std::unique_ptr< FrameConverter > mMDD;   
   
   //: Result database data
   std::unique_ptr< DataBaseConverter > mDB;
};
   
}
   
#endif   
