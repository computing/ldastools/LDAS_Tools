// Local Header Files
#include "converter.hh"
#include "eventerrors.hh"   

using namespace std;
using namespace eventMonitor;

   
//------------------------------------------------------------------------------
//
//: Default constructor.
//   
eventMonitor::Converter::Converter()
  : mError()
{}
   
   
//------------------------------------------------------------------------------
//
//: Destructor.
//   
eventMonitor::Converter::~Converter()
{}

   
//------------------------------------------------------------------------------
//
//: Gets error message if any.
//   
// This method returns error message for the convertion.
//
//!return: const string - Error message if any, empty string otherwise.
//   
const string eventMonitor::Converter::getError() const
{
   return mError;
}
   
   
//------------------------------------------------------------------------------
//
//: Did error occured?
//   
// This method checks if error message occured during data convertion.
//
//!return: const bool - True if error message exists, false otherwise.
//   
bool eventMonitor::Converter::hasError() const
{
   return ( !mError.empty() );
}
   

//------------------------------------------------------------------------------
//
//: Sets internal error message.
//   
// This method appends the message of passed to it EventException to the 
// internal error buffer.
//   
//!param: const EventException& exc - A reference to the EventException object
//+       containing the error message.   
//   
//!return: Nothing.
//   
void eventMonitor::Converter::setError( const EventException& exc )
try   
{
   size_t size( exc.getSize() ), i( 0 );

   while( i < size )
   {
      if( mError.size() )
      {
         mError += " ";
      }
      mError += exc.getError( i ).getMessage();   
   
      ++i;
   }
   
   return;
}
catch( range_error& e )
{
   if( mError.size() )
   {
      mError += " ";
   }
   
   mError += "Range error while extracting EventException error messages.";
   return;
}

   
//------------------------------------------------------------------------------
//
//: Sets internal error message.
//   
// This method appends the message of passed to it std::exception to the 
// internal error buffer.
//   
//!param: const exception& exc - A reference to the standard library exception.   
//   
//!return: Nothing.
//      
void eventMonitor::Converter::setError( const std::exception& exc )
{
   if( mError.size() )
   {
      mError += " ";
   }
   
   mError += exc.what();
   return;
}
