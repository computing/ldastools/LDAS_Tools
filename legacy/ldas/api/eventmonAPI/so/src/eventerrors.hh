#ifndef EventMonitorErrorsHH
#define EventMonitorErrorsHH
   
// System Header Files
#include <string>   
   
// General Header Files
#include "general/unordered_map.hh"
#include "general/types.hh"
#include "general/ldasexception.hh"
   
namespace eventMonitor
{
   class EventException;
   
   enum ErrorCode{
      MALFORMED_DATA_STREAM = 1,
      NULL_ILWD_ELEMENT,
      EXISTING_ILWD_ELEMENT,
      MALFORMED_ILWD,
      MISMATCHED_JOBID,
      INSERT_ERROR,
      NOT_VALID_ELEMENT,
      MALFORMED_RESULT_STREAM
   };   
   
   // Or add new value to Library::Lib enum in ldasexception.hh
   enum Library
   {
      EVENTMONAPI = 5
   };
   
   class ErrorCodeHash;
}
   
   
//------------------------------------------------------------------------------
//
//: eventMonitor::ErrorCode hash functional.
//      
class eventMonitor::ErrorCodeHash
{
public:
   
//------------------------------------------------------------------------------
//
//: Hash function for the eventMonitor::ErrorCode.
//         
//!param: const ErrorCode& e - A reference to the EventException error code.
//   
//!return: size_t - An index into the hash.
//            
   size_t operator()( const eventMonitor::ErrorCode& e ) const 
   {
      General::hash< INT_4S > h;
      return h( static_cast< INT_4S >( e ) );
   }
};
   
   
//------------------------------------------------------------------------------
//
//: Event exception class.
//    
// This class is designed to handle eventMonitorAPI exceptions.
//       
class eventMonitor::EventException : public LdasException
{
public:
    
   // Constructors
   EventException( const ErrorCode& code, const std::string& msg = "" );
   EventException( const std::string& msg );      
   
   const std::string what() const;
   
private:
   
   // Typedef
   typedef General::unordered_map< ErrorCode,
				   std::string,
				   ErrorCodeHash > ErrorHash;
   
   static const ErrorHash initErrorHash();
   static const ErrorHash mErrorHash;
};

   
#endif   
