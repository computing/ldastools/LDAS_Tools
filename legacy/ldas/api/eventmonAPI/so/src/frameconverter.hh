#ifndef EventFrameConverterHH
#define EventFrameConverterHH   

// System Header Files
#include <utility>
#include <list>   
   
#include "general/Memory.hh"

// ILWD Header Files
#include <ilwd/elementid.hh>   
#include <ilwd/ldasarraybase.hh>
   
// General Header Files   
#include "general/unordered_map.hh"
#include "general/util.hh"
   
// Local Header Files   
#include "converter.hh"   

// Forward declarations   
namespace ILwd
{
   class LdasElement;
   class LdasContainer;
}
   
   
namespace ILwdFCS    
{
   class FrameH;
   class FrProcData;
   class FrVect;
   class Container;
}


namespace General
{
   class GPSTime;
}
   
   
namespace eventMonitor   
{
   class EventException;
   
   
//------------------------------------------------------------------------------
//
//: Frame converter class.
//    
// This utility class is designed to collect multiDimData data for one particular 
// job and reformat it into frameAPI expected ILWD format element.   
//    
class FrameConverter : public Converter
{
public:   

   // Default constructor
   FrameConverter( const bool& single_frame = false ); 
   
   // Destructor
   ~FrameConverter();
   
   // The only reason for this method is a defficiency of ilwdfcs library:
   // cannot concatenate multiple frames into the single one.
   // As a result, process cannot start creating frames unless mSingleFrame 
   // flag is set for the job by the Tcl layer.
   // Therefore store deep copies of all mdd's until the mSingleFrame is set to
   // TRUE for the job, or Data::getSortedData method is called (the very last 
   // object has arrived into the job bucket, or 'removeJob' tcl command has
   // been issued).
   void storeData( const containerList& mdd );   

   void addData();
   void setSingleFrame( const bool& flag );
   std::unique_ptr< ILwd::LdasContainer > getILWD( const INT_4U& id ); 

private:

   //: No copy constructor
   FrameConverter( const FrameConverter& c );       

   //: No assignment operator
   const FrameConverter& operator=( const FrameConverter& c );          
   
   enum
   {
      NONE,
      TIME,
      FREQ,
      BOTH
   };

   // Need to change this value if maximum number of dimensions for metadata 
   // gets changed!!!   
   // BOTH domain has maximum dimension = 2.   
   enum
   {
      MAX_META_DIM = 2
   };
   
   typedef General::unordered_map< std::string, INT_4S, 
				   General::hash< std::string > > domainHash;
   typedef domainHash::value_type domainValueType;     
   typedef std::list< const ILwd::LdasElement* > ILwdList;

   static const domainHash initDomainHash();   
   static const ILwdList findILwd( const ILwd::LdasContainer* c, 
      const std::string& name, const size_t pos = 1 );
   static const General::GPSTime findTime( const ILwd::LdasContainer* c,
      const std::string& name );
   template< class T > T getElementData( const ILwd::LdasContainer* c,
      const std::string& name, const size_t pos = 0 );   
   static INT_4S getDomain( const ILwd::LdasContainer* c );

   void addShallowData( const containerList& mdd );   
   
   void createHeader();
   void insertProcData( const ILwd::LdasContainer* e );
   void insertHistoryData( const ILwd::LdasContainer* e );
   void setHistory( const ILwd::LdasElement* e );
   static const std::string historyToAscii( const ILwd::LdasElement* e );
   template< class T > static const std::string arrayToAscii( 
      const ILwd::LdasElement* e );
   void insertDetectorData( const ILwd::LdasContainer* e );
   void setDetector( const ILwd::LdasElement* e );   
   template< class T > static T getArrayValue( 
      const ILwd::LdasContainer& e, const std::string& name );   
   template< class T > static const T* getArray( 
      const ILwd::LdasContainer& e, const std::string& name );      
   
   size_t setProcMetadata( const ILwd::LdasContainer* c );   
   void setData( const ILwd::LdasContainer* e );
   void createData( const ILwd::LdasElement* e );
   template< class T > void createArray( const ILwd::LdasElement* e );   
   
   void cleanupOriginalData();
   
   // Frame specific data
   static const std::string mFrameComment;
   static const std::string mAPIName;
   
   // multiDimData specific tags
   static const std::string mStartTimeName;
   static const std::string mStopTimeName;   
   static const std::string mStartFreqName;
   static const std::string mStopFreqName;
   static const std::string mBaseFreqName;   
   static const std::string mSecUnits;
   static const std::string mNanUnits;
   static const std::string mDataName;
   static const std::string mLDASHistoryName;   
   
   // Detector geometry specific attributes
   static const std::string mDetectorContName;
   static const std::string mPrefixName;   
   static const std::string mLongitudeName;
   static const std::string mLatitudeName;
   static const std::string mElevationName;
   static const std::string mArmXazimuthName;
   static const std::string mArmYazimuthName;
   static const std::string mArmXaltitudeName;
   static const std::string mArmYaltitudeName;
   static const std::string mLocalTimeName;
   
   static const domainHash  mDomainHash;

   //: Flag to indicate if result data should be represented by
   //: a single frame.
   bool mSingleFrame;
   
   //: A list of frames.
   std::list< ILwdFCS::FrameH* > mFrameList;
   
   //: A (shallow) pointer to the current FrProcData object.
   ILwdFCS::FrProcData* mCurrentProc;
   
   INT_4S mCurrentDomain;
   
   //: Buffer to hold data for FrVect::dx
   REAL_8 mDx[ MAX_META_DIM ];
   
   //: Buffer to hold data for FrVect::startX
   REAL_8 mStartX[ MAX_META_DIM ];
   
   //: Buffer to hold data for FrVect::unitX   
   std::string mUnitX[ MAX_META_DIM ];
   
   //: Buffer to hold data dimensions.
   ILwd::LdasArrayBase::size_type mDims[ MAX_META_DIM ];
   
   //: String to hold data for unitY
   std::string mUnitY;

   //: Deep copy of original multiDimData data structures (see docs for 
   //: FrameConverter::storeData())
   containerList mOriginalData;
   
   //: Must keep pointers to all ILwdFCS objects that get appended to the
   //: ILwdFCS frames( for later destruction once ILWD representation of frame 
   //: is received ). 
   //: Even though ILwdFCS::Container owns all the objects, it's not destructing
   //: them (?!)
   std::list< ILwdFCS::Container* > mFCSObjects;
   std::list< ILwdFCS::FrVect* > mFCSVectObjects;
};
   
}

   
#endif   
   
