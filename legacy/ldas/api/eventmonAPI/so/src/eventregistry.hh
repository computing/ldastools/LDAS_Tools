#ifndef EventRegistryHH
#define EventRegistryHH   
  
// System Header Files
#include <string>   
#include <pthread.h>   
   
// General Header Files
#include "general/unordered_map.hh"
#include "general/types.hh"

namespace ILwd
{
   class LdasContainer;   
}   
   
namespace eventMonitor
{
   class EventData;   

   
//------------------------------------------------------------------------------
//
//: Event registry class.
//    
// This class is designed to keep a track of jobs and their ILWD format data.  
// Each job will have multiple ILWD format elements associated with it.   
// There should be only one instance of this class per eventMonitorAPI.   
//    
class EventRegistry : 
   private General::unordered_map< INT_4U, EventData* >
{
public:
    
   EventRegistry();
   ~EventRegistry();
   
   /* Modifiers */
   const std::string updateEntry( const INT_4U& key, 
      ILwd::LdasContainer* e, const bool selfDestruct );   
   void updateEntry( const INT_4U& key, const bool& flag );      
   
   const std::string removeEntry( const key_type& key );
   void killEntry( const key_type& key );

   
   /* Helper methods( extracts metadata of ILWD format data ) */
   const std::string listEntry( const INT_4U& key ) const; 
   const std::string listAllEntries() const;
   
private:

   //: No copy constructor
   EventRegistry( const EventRegistry& );  
   
   //: No operator=
   const EventRegistry& operator=( const EventRegistry& );

   void eraseEntries();      
   
   //: Not thread safe method (caller must lock the unordered_map)
   void eraseEntry( iterator& pos );   
   
   /* Typedef's */
   typedef std::pair< General::unordered_map< INT_4U, EventData* >::iterator,
                      bool > insertPair;      
   
   //: Object lock.
   pthread_mutex_t mLock;
};

}

   
#endif   
