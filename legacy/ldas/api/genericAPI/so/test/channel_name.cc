#include "genericAPI/config.h"

#include "general/unittest.h"

#include "ilwd/ldascontainer.hh"

#include "dbaccess/Table.hh"
#include "dbaccess/Process.hh"

#include "ChannelNameLexer.hh"


General::UnitTest	Test;

using namespace GenericAPI;
using namespace std;   

void
simple_dbaccess(void)
{
  try {
    DB::Table*	p(DB::Table::CreateTable("process"));

    p->AppendColumnEntry("program", "sample process");
    p->AppendColumnEntry("version", "version");
    p->AppendColumnEntry("cvs_repository", "repository");
    p->AppendColumnEntry("cvs_entry_time", 0);
    p->AppendColumnEntry("start_time", 0);
    p->AppendColumnEntry("unix_procid", 1001);
    p->AppendColumnEntry("node", "localhost");
    p->AppendColumnEntry("username", "me");

    p->AppendColumnEntry("program", "sample process");
    p->AppendColumnEntry("version", "version");
    p->AppendColumnEntry("cvs_repository", "repository");
    p->AppendColumnEntry("cvs_entry_time", 0);
    p->AppendColumnEntry("start_time", 0);
    p->AppendColumnEntry("unix_procid", 1002);
    p->AppendColumnEntry("node", "localhost");
    p->AppendColumnEntry("username", "me");


    Test.Message() << "simple";
    p->GetILwd()->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }
  catch (const std::exception& e)
  {
    Test.Check(false) << "Caught excpetion: " << e.what() << endl;
  }
  catch (...)
  {
    Test.Check(false) << "Caught unknown excpetion" << endl;
  }
}

void
check( const std::string& Input, const std::string& Key )
{
  ChannelNameLexer	in( Input );
  ChannelNameLexer	key( Key );

  const ChannelNameLexer::data_list_type&	in_channels( in.GetChannelNames() );
  const ChannelNameLexer::data_list_type&	key_channels( key.GetChannelNames() );

  Test.Check(in_channels.size() == key_channels.size() )
    << "Number of channels expected" << std::endl;
  for ( ChannelNameLexer::data_list_type::const_iterator i = in_channels.begin();
	i != in_channels.end();
	i++ )
  {
    ChannelNameLexer::data_list_type::const_iterator k( key_channels.find( i->first ) );

    if ( k == key_channels.end() )
    {
      Test.Check( false ) << "Missing channel name: "
			  << i->first
			  << std::endl;
    }
  }
  Test.Check( true ) << "All channels match" << std::endl;
}

int
main( int ArgC, char** ArgV )
{
  Test.Init( ArgC, ArgV );

  simple_dbaccess();

  check("mix(0, 1, H2\\:AS_RQ)", "H2\\:AS_RQ");
  check("mix(0, H2\\:AS_RQ, H2\\:AS_RQ)", "H2\\:AS_RQ");
  check("mix(0, H2\\:AS_RQ, resample(0, H2\\:AS_RQ, 2.0)", "H2\\:AS_RQ");
  check("mix(0, H2:AS_RQ, H2\\:AS_RQ)", "H2\\:AS_RQ");

  //---------------------------------------------------------------------
  // These are conditions that should produce no channel names
  //---------------------------------------------------------------------
  check("complex()", "");

  Test.Exit();
}
