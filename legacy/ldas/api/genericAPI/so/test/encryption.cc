#include "genericAPI/config.h"

#include <sstream>

#include "general/unittest.h"

#include "genericAPI/genericcmd.hh"

General::UnitTest	Test;

void
check( char* Source, char* Key )
{
    const unsigned int	len = strlen( Source );
    char*			encoded;
    char*			decoded;
    std::ostringstream		leader;

    leader << "[SOURCE: " << Source << " KEY: " << Key << "]: ";
    encoded = encrypt( Source, Key );
    decoded = decrypt( encoded, Key );
    Test.Check( len == strlen( decoded ) )
      << leader.str( )
      << "Decoded length is the same as that of the origional test data"
      << std::endl;
    Test.Message( 10 )
      << leader.str( )
      << "len: " << len << " =?= " << "strlen( decoded ): " << strlen( decoded )
      << std::endl;
    Test.Check( strcmp( Source, decoded ) == 0 )
      << leader.str( )
      << "Decoded data verses origional data"
      << std::endl;
    free( encoded );
    free( decoded );
}

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Prepare the system tests
  //---------------------------------------------------------------------
  Test.Init( ArgC, ArgV );

  //---------------------------------------------------------------------
  // System tests
  //---------------------------------------------------------------------
  char key[ 256 ];
  char src[ 256 ];

  const char* td[] = {
    "TestData", "Hello World",
    "helpme", "Very long Key Here 234",
    "Long data", "s"
  };
  for ( INT_2U x = 0;
	x < ( sizeof( td ) / sizeof( *td ) );
	x += 2 )
  {
    strcpy( src, td[ x ] );
    strcpy( key, td[ x + 1 ] );
    check( src, key );
  }

  //---------------------------------------------------------------------
  // Exit with the status of system tests
  //---------------------------------------------------------------------
  Test.Exit();
}
