#include "genericAPI/config.h"

#include <sys/types.h>
#include <sys/wait.h>

#include <signal.h>
#include <tcl.h>
#include <unistd.h>

#include <cstdlib>
#include <cstring>

#include <string>
#include <sstream>

#include "general/AtExit.hh"
#include "general/autoarray.hh"

int main( int ArgC, char** ArgV );

int Port( 32512 );
#define CORE test_scripts_type::DATA_SOCKET_WITH_OSPACE_OBJECTS
#undef CORE

#ifdef CORE
static void debugger( );
#endif /* CORE */
static int eval( const std::string& Command );
static void test( const std::string& ServerCommand,
		  const std::string& ClientCommand );
static void test2( const std::string& ServerCommand,
		   const std::string& ClientCommand );
static std::string	Program;
static std::string	Title;

struct test_scripts_type {
  enum {
    INFO,
    TCL_SOCKET,
    DATA_SOCKET_SERVER_ONLY,
    DATA_SOCKET,
    DATA_SOCKET_WITH_OSPACE_OBJECTS,
    DATA_SOCKET_BAD_OPTION,
    MAX,
    LAST_TEST_SCRIPT = DATA_SOCKET_WITH_OSPACE_OBJECTS
  };
  enum {
    SERVER,
    CLIENT,
    CS_MAX
  };

  inline static const std::string& GetScript( int x, int y )
  {
    return m_scripts[x][y];
  }

private:
  static std::string	m_scripts[MAX][CS_MAX];
  static bool		m_initialize;

  static bool initialize();

} TestScripts;

int
main( int ArgC, char** ArgV )
{
  Program = ArgV[0];
#if defined( CORE )
    test( TestScripts.GetScript(CORE, test_scripts_type::SERVER ),
	  TestScripts.GetScript(CORE, test_scripts_type::CLIENT ) );
#else
  for ( unsigned int x = 0; x <= test_scripts_type::LAST_TEST_SCRIPT; x++ )
  {
    std::cerr << "Doing test: " << (x+1) << " of " << ( test_scripts_type::LAST_TEST_SCRIPT + 1) << std::endl;
    test( TestScripts.GetScript(x, test_scripts_type::SERVER ),
	  TestScripts.GetScript(x, test_scripts_type::CLIENT ) );
  }
#endif	/* CORE */
}

static int
eval( const std::string& Command )
{
  int			retval;
  std::ostringstream	ss;

  //---------------------------------------------------------------------
  // init command
  //---------------------------------------------------------------------

  ss << "set port "<< Port << std::endl
     << Command;

  //---------------------------------------------------------------------
  // Get the command ready
  //---------------------------------------------------------------------

  General::AutoArray<char> buffer( new char[ ss.str().length() * 2 ] );

  strcpy( buffer.get( ), ss.str().c_str() );

  //---------------------------------------------------------------------
  // Create interpreter
  //---------------------------------------------------------------------

  Tcl_Interp* interp( Tcl_CreateInterp() );

  //---------------------------------------------------------------------
  // Execute the commands requested
  //---------------------------------------------------------------------
  
  retval = Tcl_GlobalEval( interp,
			   (char*)"load ../src/.libs/libgenericAPI.so\n" );
#if defined( CORE )
  if ( Command.length() > 0 )
  {
    debugger();
  }
#endif	/* CORE */
  retval = Tcl_GlobalEval( interp, buffer.get( ) );

  //---------------------------------------------------------------------
  // Delete the Interpreter
  //---------------------------------------------------------------------

  Tcl_DeleteInterp( interp );

  return retval;
}

#ifdef CORE
static void
debugger( )
{
  const char* debug = getenv("DEBUGGER");
  if ( ( debug == (const char*)NULL ) || ( debug == "" ) )
  {
    return;
  }
  int pid( fork() );
  if ( pid < 0 )
  {
    // error
  }
  else if ( pid == 0 )
  {
    std::ostringstream	ppid;

    ppid << getppid();
    
    execlp( "xterm",
	    "xterm",
	    "-title",
	    Title.c_str(),
	    "-e",
	    "gdb", Program.c_str(), ppid.str().c_str(), (char*)NULL );
  }
  else
  {
    // Parent
    sleep( 4 );
  }
}
#endif /* CORE */

static void
test( const std::string& ServerCommand,
      const std::string& ClientCommand )
{
  int	pid( fork() );

  if ( pid < 0 )
  {
    // Fork error
  }
  else if ( pid == 0 )
  {
    // Child
    test2( ServerCommand, ClientCommand );
    exit( 0 );
  }
  else
  {
    // Parent
    int status;

    waitpid( pid, &status, 0 );
  }
}


static void
test2( const std::string& ServerCommand,
       const std::string& ClientCommand )
{
  int	pid( fork() );

  if ( pid < 0 )
  {
    // Fork error
  }
  else if ( pid == 0 )
  {
    // Child
#if defined( CORE )
    // debugger();
    Title = "Client";
#endif /* CORE */
    sleep( 2 );	// Give server time to come up
    switch( eval( ClientCommand ) )
    {
    }
    exit( 0 );
  }
  else
  {
    // Parent
#if defined( CORE )
    //debugger();
    Title = "Server";
#endif /* CORE */
    eval( ServerCommand );
  }
}

std::string test_scripts_type::m_scripts[test_scripts_type::MAX][test_scripts_type::CS_MAX];
bool test_scripts_type::m_initialize = test_scripts_type::initialize();

bool test_scripts_type::
initialize()
{
  m_scripts[ INFO ][ SERVER ] =
    "puts \"tcl_socket server: $port\"\n"
    "exit 0\n"
    ;
  m_scripts[ INFO ][ CLIENT ] =
    "puts \"tcl_socket client: $port\"\n"
    "exit 0\n"
    ;
  //---------------------------------------------------------------------
  // TCL_SOCKET
  //---------------------------------------------------------------------
  m_scripts[ TCL_SOCKET ][ SERVER ] =
    "# ON THE SERVER SIDE: \n"
    "# package require genericAPI\n"
    "\n"
    "# A data socket server is opened:\n"
    "\n"
    "set dataserver [ socket -server config_proc $port ]\n"
    "\n"
    "proc handle { sock } {\n"
    "    global line\n"
    "    if { [eof $sock] || [catch {gets $sock line}]} {\n"
    "        # end of file or abnormal connection drop\n"
    "        close $sock\n"
    "    }\n"
    "}\n"
    "\n"
    "proc config_proc { sock addr port } {\n"
    "    # A fileevent handler is registered by the config_proc,\n"
    "    # and the handler does a readElement on 'fileevent\n"
    "    # readable'.\n"
    "    puts \"Accepted $sock from $addr port $port\"\n"
    "	 fconfigure $sock -buffering line\n"
    "    fileevent $sock readable [list handle $sock]\n"
    "}\n"
    "\n"
    "vwait line\n"
    "puts $line\n"
    "exit 0\n"
    ;
  m_scripts[ TCL_SOCKET ][ CLIENT ] =
    "# ON THE CLIENT SIDE:\n"
    "\n"
    "# package require genericAPI\n"
    "\n"
    "# Initially what is needed is an ilwd in a file.\n"
    "\n"
    "# A data socket client is opened:\n"
    "\n"
    "set sock [ socket localhost $port ]\n"
    "fconfigure $sock -buffering line\n"
    "\n"
    "# and we send the data down the socket:\n"
    "\n"
    "puts $sock \"hello world from tcl_socket\"\n"
    "close $sock\n"
    "exit 0\n"
    ;
  //---------------------------------------------------------------------
  // DATA_SOCKET_SERVER_ONLY
  //---------------------------------------------------------------------
  m_scripts[ DATA_SOCKET_SERVER_ONLY ][ SERVER ] =
    "# ON THE SERVER SIDE: \n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "proc handle { sock } {\n"
    "    puts {handler called for $sock}\n"
    "    global line\n"
    "    set err {eof}\n"
    "    if { [eof $sock] || [catch {gets $sock line} err]} {\n"
    "        # end of file or abnormal connection drop\n"
    "        puts {error condition $sock: $err}\n"
    "        set line $err\n"
    "        close $sock\n"
    "    }\n"
    "    puts {leaving handler: $line}\n"
    "}\n"
    "\n"
    "proc config_proc { sock addr port } {\n"
    "    # A fileevent handler is registered by the config_proc,\n"
    "    # and the handler does a readElement on 'fileevent\n"
    "    # readable'.\n"
    "    puts \"Accepted $sock from $addr port $port\"\n"
    "    fileevent $sock readable [list handle $sock]\n"
    "}\n"
    "\n"
    "# A data socket server is opened:\n"
    "\n"
    "set dataserver [ datasocket -server config_proc $port ]\n"
    "close $dataserver\n"
    "\n"
    "exit 0\n"
    ;
  m_scripts[ DATA_SOCKET_SERVER_ONLY ][ CLIENT ] =
    "exit 0\n"
    ;
  //---------------------------------------------------------------------
  // DATA_SOCKET
  //---------------------------------------------------------------------
  m_scripts[ DATA_SOCKET ][ SERVER ] =
    "# ON THE SERVER SIDE: \n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "proc handle { sock } {\n"
    "    puts {handler called for $sock}\n"
    "    global line\n"
    "    set err {eof}\n"
    "    if { [eof $sock] || [catch {gets $sock line} err]} {\n"
    "        # end of file or abnormal connection drop\n"
    "        puts {error condition $sock: $err}\n"
    "        set line $err\n"
    "        close $sock\n"
    "    }\n"
    "    puts {leaving handler: $line}\n"
    "}\n"
    "\n"
    "proc config_proc { sock addr port } {\n"
    "    # A fileevent handler is registered by the config_proc,\n"
    "    # and the handler does a readElement on 'fileevent\n"
    "    # readable'.\n"
    "    puts \"Accepted $sock from $addr port $port\"\n"
    "    fileevent $sock readable [list handle $sock]\n"
    "}\n"
    "\n"
    "# A data socket server is opened:\n"
    "\n"
    "set dataserver [ datasocket -server config_proc $port ]\n"
    "\n"
#if !defined(CORE)
    "after 10000 set line timeout\n"
#endif /* !defined(CORE) */
    "vwait line\n"
    "puts $line\n"
    "exit 0\n"
    ;
  m_scripts[ DATA_SOCKET ][ CLIENT ] =
    "# ON THE CLIENT SIDE:\n"
    "\n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "# Initially what is needed is an ilwd in a file.\n"
    "\n"
    "# A data socket client is opened:\n"
    "\n"
    "if [catch {datasocket localhost $port} sock ] {\n"
    "  puts stderr \"Error: $sock\"\n"
    "  exit 1\n"
    "}\n"
    "# and we send the data down the socket:\n"
    "\n"
    "puts {above the send}\n"
    "if [catch {puts $sock \"hello world from datasocket\"} err] {\n"
    "  puts stderr {Error: $err}\n"
    "  exit 1\n"
    "}\n"
    "puts {above the close}\n"
    "close $sock\n"
    "exit 0\n"
    ;
  //---------------------------------------------------------------------
  // DATA_SOCKET_WITH_OSPACE_OBJECTS
  //---------------------------------------------------------------------
  m_scripts[ DATA_SOCKET_WITH_OSPACE_OBJECTS ][ SERVER ] =
    "# ON THE SERVER SIDE: \n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "proc handle { sock } {\n"
    "    fconfigure $sock -linger 0]\n"
    "    puts {handler called for $sock}\n"
    "    puts [fconfigure $sock -peername]\n"
    "    puts [fconfigure $sock -sockname]\n"
    "    puts [fconfigure $sock -linger]\n"
    "    puts [fconfigure $sock]\n"
    "    global line\n"
    "    set err {eof}\n"
    "    if { [eof $sock] || [catch {set line [data_gets $sock]} err]} {\n"
    "        # end of file or abnormal connection drop\n"
    "        puts \"error condition $sock: $err\"\n"
    "        set line $err\n"
    "        close $sock\n"
    "    }\n"
    "}\n"
    "\n"
    "proc config_proc { sock addr port } {\n"
    "    # A fileevent handler is registered by the config_proc,\n"
    "    # and the handler does a readElement on 'fileevent\n"
    "    # readable'.\n"
    "    puts \"Accepted $sock from $addr port $port\"\n"
    "    fileevent $sock readable [list handle $sock]\n"
    "}\n"
    "\n"
    "# A data socket server is opened:\n"
    "\n"
    "set dataserver [ datasocket -server config_proc $port ]\n"
    "\n"
#if !defined(CORE) 
    "after 10000 set line timeout\n"
#endif /* !defined(CORE) */
    "vwait line\n"
    "puts [getElement $line]\n"
    "exit 0\n"
    ;
  m_scripts[ DATA_SOCKET_WITH_OSPACE_OBJECTS ][ CLIENT ] =
    "# ON THE CLIENT SIDE:\n"
    "\n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "# Initially what is needed is an ilwd in a file.\n"
    "\n"
    "# A data socket client is opened:\n"
    "\n"
    "if [catch {datasocket localhost $port} sock ] {\n"
    "  puts stderr \"Error: $sock\"\n"
    "  exit 1\n"
    "}\n"
    "# and we send the data down the socket:\n"
    "\n"
    "set e [putElement \"<lstring name='world' size='5'>hello</lstring>\"]\n"
    "puts \"above the send $e\"\n"
    "if [catch {data_puts $sock $e} err] {\n"
    "  puts stderr {Error: $err}\n"
    "  exit 1\n"
    "}\n"
    "puts {above the close}\n"
    "close $sock\n"
    "exit 0\n"
    ;
  //---------------------------------------------------------------------
  // DATA_SOCKET_WITH_OSPACE_OBJECTS
  //---------------------------------------------------------------------
  m_scripts[ DATA_SOCKET_BAD_OPTION ][ SERVER ] =
    "# ON THE SERVER SIDE: \n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "proc handle { sock } {\n"
    "    fconfigure $sock -linger 0]\n"
    "    puts {handler called for $sock}\n"
    "    puts [fconfigure $sock -peername]\n"
    "    puts [fconfigure $sock -sockname]\n"
    "    puts [fconfigure $sock -linger]\n"
    "    puts [fconfigure $sock]\n"
    "    global line\n"
    "    set err {eof}\n"
    "    if { [eof $sock] || [catch {set line [data_gets $sock]} err]} {\n"
    "        # end of file or abnormal connection drop\n"
    "        puts \"error condition $sock: $err\"\n"
    "        set line $err\n"
    "        close $sock\n"
    "    }\n"
    "}\n"
    "\n"
    "proc config_proc { sock addr port } {\n"
    "    # A fileevent handler is registered by the config_proc,\n"
    "    # and the handler does a readElement on 'fileevent\n"
    "    # readable'.\n"
    "    puts \"Accepted $sock from $addr port $port\"\n"
    "    fileevent $sock readable [list handle $sock]\n"
    "}\n"
    "\n"
    "# A data socket server is opened:\n"
    "\n"
    "set dataserver [ datasocket -server config_proc $port ]\n"
    "\n"
#if !defined(CORE) 
    "after 10000 set line timeout\n"
#endif /* !defined(CORE) */
    "vwait line\n"
    "puts [getElement $line]\n"
    "exit 0\n"
    ;
  m_scripts[ DATA_SOCKET_BAD_OPTION ][ CLIENT ] =
    "# ON THE CLIENT SIDE:\n"
    "\n"
    "# load ../src/.libs/libgenericAPI.so\n"
    "\n"
    "# Initially what is needed is an ilwd in a file.\n"
    "\n"
    "# A data socket client is opened:\n"
    "\n"
    "if [catch {datasocket -badoption localhost $port} sock ] {\n"
    "  puts stderr \"Error: $sock\"\n"
    "  exit 1\n"
    "}\n"
    "# and we send the data down the socket:\n"
    "\n"
    "set e [putElement \"<lstring name='world' size='5'>hello</lstring>\"]\n"
    "puts \"above the send $e\"\n"
    "if [catch {data_puts $sock $e} err] {\n"
    "  puts stderr {Error: $err}\n"
    "  exit 1\n"
    "}\n"
    "puts {above the close}\n"
    "close $sock\n"
    "exit 0\n"
    ;
  return true;
}
