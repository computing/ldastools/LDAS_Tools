#ifndef GENERIC_API__UtilTCL_HH
#define GENERIC_API__UtilTCL_HH

#include <string>

std::string makeTclPointer( const void* p, const std::string& classname );

#endif /* GENERIC_API__UtilTCL_HH */
