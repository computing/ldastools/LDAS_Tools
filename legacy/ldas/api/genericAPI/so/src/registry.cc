/* -*- c-basic-offset: 4; -*- */

#include "genericAPI/config.h"

#include "registry.hh"
#include "util.hh"


namespace Registry {

//-----------------------------------------------------------------------------
//
//: Binary Registry
//
BinaryRegistry_type	binaryRegistry;


#if HAVE_LIBOSPACE
//-----------------------------------------------------------------------------
//
//: Active Socket Registry
//
// These are sockets which are created with the createDataSocket command.
//
ObjectRegistry< os_tcp_socket >            activeSocketRegistry;
#endif /* HAVE_LIBOSPACE */


#if HAVE_LIBOSPACE
//-----------------------------------------------------------------------------
//: Passive Socket Registry
//
// These are sockets which are created with the acceptDataSocket command
//
ObjectRegistry< os_tcp_socket >            passiveSocketRegistry;
#endif /* HAVE_LIBOSPACE */


//-----------------------------------------------------------------------------
//
//: Check Binary Pointer
//
// True if the LdasBinary object is valid.
//
//!param: Ldasbinary* binary
//
//!return: bool - True if the object is valid
//
bool isBinaryValid( const LdasBinary* binary ) throw()
{
    return ( binaryRegistry.isRegistered( binary ) );
}


#if HAVE_LDAS_PACKAGE_ILWD
std::string ElementRegistry_type::
getElementList( ) const
{
    General::ReadWriteLock m( m_lock,
			      General::ReadWriteLock::READ );

    std::stringstream ss;

    ss << "{ ";
    for ( const_iterator iter = begin(),
	      last = end( );
	  iter != last;
	  ++iter )
    {
        ss << makeTclPointer( iter->first, "LdasElement" ) << " ";
    }
    ss << "}";
    
    return ss.str();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

//-----------------------------------------------------------------------------

} // namespace Registry

//-----------------------------------------------------------------------------
template class ObjectRegistry< LdasBinary >;
#if HAVE_TID
template class ObjectRegistry< tid >;
#endif /* HAVE_TID */
