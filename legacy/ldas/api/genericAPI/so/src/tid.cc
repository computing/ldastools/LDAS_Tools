/* -*- mode: c++; c-basic-offset: 3; -*- */

#include "genericAPI/config.h"

#if HAVE_TCL

#include <signal.h>

#include <cassert>
#include <csignal>
#include <sstream>
#include <stdexcept>
#include <cerrno>
#include <cstring>   

#include "general/AtExit.hh"
#include "general/DeadLockDetector.hh"
#include "general/gpstime.hh"
#include "general/ldasexception.hh"

#include "util.hh"
#include "tid.hh"
#include "tidEvent.hh"

#undef DUMP_CORE
#define	DUMP_CORE 0

namespace
{
   static Registry::ThreadRegistry_type* thread_registry =
      (Registry::ThreadRegistry_type*)NULL;

   static void
   cleanup_at_exit( )
   {
      if ( thread_registry )
      {
	 delete thread_registry;
	 thread_registry = (Registry::ThreadRegistry_type*)NULL;
      }
   }

   static Registry::ThreadRegistry_type&
   init ( )
   {
      General::AtExit::Append( cleanup_at_exit,
			       "Registry::ThreadRegistry_type::cleanup_at_exit",
			       100 );
      thread_registry = new Registry::ThreadRegistry_type( );
      return *thread_registry;
   }
   
}

bool tid::mDebuggingDefault = false;

void tid::
assertion( const char* const FileName,
	   const int Line,
	   bool ThrowException ) const
{
#if DUMP_CORE
   assert( mState <= JOINED );
   /* assert( ( mParentThread == pthread_self( ) ) ||*/
   /*  ( mThread == pthread_self( ) ) ); */
   assert( mTCLState.get( ) );
   assert( *mTCLState <= UNSET_TCL );
#else
#define LCL_ASSERT( a ) \
  if ( ! ( a ) && \
       ( ! mAssertionExceptionThrown ) ) \
  { \
    if ( ThrowException ) \
    { \
      mAssertionExceptionThrown = true; \
      try \
      { \
	 const_cast< tid* >( this )->Cancel( ); \
      } \
      catch( ... ) \
      { \
      } \
      std::ostringstream	oss; \
      oss << "Assertion Failed: " << #a \
          << " (" << FileName << " " << Line << ")"; \
      throw std::runtime_error( oss.str( ) ); \
    } \
    else \
    { \
      assert( 0 ); \
    } \
  }

   LCL_ASSERT( mState <= JOINED );
   LCL_ASSERT( mTCLState.get( ) );
   LCL_ASSERT( *mTCLState <= UNSET_TCL );
#endif
}

#define ASSERTION( a ) assertion( __FILE__, __LINE__, a );

#define	LM_DEBUG 0
#if LM_DEBUG
#define	AT( a ) std::cerr << "INFO: " << a << " " << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT( a )
#endif

namespace {
   const char*
   status2string( int Status )
   {
      switch( Status )
      {
      case tid::RUNNING:
	 return "RUNNING";
	 
      case tid::FINISHED:
	 return "FINISHED";
	 
      case tid::CANCELED:
	 return "CANCELED";
	 
      case tid::JOINED:
	 return "JOINED";
	 
      case tid::IDLE:
	 return "IDLE";   
	 
      default:
	 break;
      }
      return "";
   }
}

//-----------------------------------------------------------------------------
//: Constructor.
//
// Initializes the thread to an idle state.
//
tid::
tid() throw()
   : mState( IDLE ),
     mTCLState( new tcl_state_type ),
     mExcept( 0 ),
     mTclInterp( 0 ),
     mTraceVarName( 0 ),
     mAssertionExceptionThrown( false ),
     mDebugging( mDebuggingDefault )
{
   pthread_mutex_init( &mDebugLock, (pthread_mutexattr_t*)NULL  );
   pthread_mutex_init( &mStateLock, (pthread_mutexattr_t*)NULL  );

   AppendDebug( "constructor: begin" );
   
   *mTCLState = UNSET_TCL;
   AT( "Allocated: " << this );
   //--------------------------------------------------------------------------
   // Setup so threads can cooperate and terminating
   //--------------------------------------------------------------------------
   CancellationType( CANCEL_EXCEPTION );
   AppendDebug( "constructor: end" );
}


//-----------------------------------------------------------------------------
//
//: Destructor. Cancels the thread and deallocates memory.
//
tid::
~tid()
{
   AppendDebug( "destructor: begin" );
   ASSERTION( true );
   switch( getState( ) )
   {
   case IDLE:
      break;
      
   case RUNNING:
      /* pthread_cancel( mThread ); */
      Join( );
      break;
	 
   case CANCELED:
      try
      {
	 Join( );
      }
      catch( ... )
      {
	 Thread::Join( );
      }
      break;
      
   case FINISHED:
      Join( );
      break;
	 
   case JOINED:
      break;
   }
   if ( General::AtExit::IsExiting( ) == false )
   {
      if ( mTclInterp )
      {
	 AT( "unlinking" );
	 TIDRequestUnlinkVar( ParentThread( ),
			      mTclInterp,
			      mTraceVarName,
			      mTCLState.release( ) );
	 AT( "purging" );
	 TIDPurge( this );
	 AT( "forgetting" );

	 mTclInterp = (Tcl_Interp*)NULL;
      }
      else
      {
	 AT( "purging" )
	    TIDPurge( this );
      }
   } // General::AtExit::IsExiting( )
   if (mExcept)
   {
      delete mExcept;
      mExcept = (SwigException*)NULL;
   }
   
   delete[] mTraceVarName;
   mTraceVarName = 0;
   
   AT( "Deleted: " << this );
}

   
//-----------------------------------------------------------------------------
//
//: Returns the state of the thread.
//
// The thread will always be in one of four states:
// <ol>
//   <li>IDLE - The thread has not yet started.  This is the state which the
//       thread is in immediateley after the object is constructed.</li>
//   <li>RUNNING - The thread is currently running (i.e. the user has ran the
//       'run' method).</li>
//   <li>FINISHED - The thread has completed executing.  The user may obtain
//       the return value (or the exception) by calling the getReturn method
//       in a child class.</li>
//   <li>CANCELED - Thread execution was canceled.</li>
// </ol>
//
//!return: state - The current state of the thread.
//
tid::state tid::
getState() const throw()
{
   ASSERTION( true );
   if ( mState == RUNNING )
   {
      tid* wtid = const_cast<tid*>(this);
      int ec = Kill( 0 );
      if ( mState == RUNNING )
      {
	 switch ( ec )
	 {
	 case 0:
	    // Nothing wrong
	    break;
	 case ESRCH:
	    // Process could not be found, flag as finished
	    wtid->report_zombie( "Thread has vanished when getState called" );
	    wtid->set_state( FINISHED );
	    break;
	 case EINVAL:
	    wtid->report_zombie( "Invalid signal sent when getState called" );
	    wtid->set_state( FINISHED );
	    break;
	 default:
	    {
	       std::ostringstream	msg;
	       msg << "ZOMBIE: " << General::GPSTime::NowGPSTime( )
		   << ": Undocumented error ("
		   << ec
		   << ") when getState called";
	       wtid->report_zombie( msg.str( ) );
	       wtid->set_state( FINISHED );
	    }
	    break;
	 } // switch
      } // if
   }
   ASSERTION( true );
   return mState;
}

//-----------------------------------------------------------------------------
//
//: Begins execution of the thread.
//
// This will begin executing the thread if it is IDLE.
//
//!return: int - 0 if a thread was started, 1 if the thread has already
//!return:       been ran or is running.
//
int tid::
Spawn( )
{
   AppendDebug( "run: begin" );
   ASSERTION( true );
   if ( mState != IDLE )
   {
      AppendDebug( "run: end: mState != IDLE" );
      return 1;
   }

   // _POSIX_THREAD_ATTR_STACKSIZE is defined in unistd.h if supported

   ASSERTION( true );
   int retval = Thread::spawn( ::wrapper );
   ASSERTION( true );
   if ( retval == 0 )
   {
      set_state( RUNNING );
   }
   else
   {
      AppendDebug( "run: Failed to create thread" );
      set_state( CANCELED );
   }
   
   ASSERTION( true );
   std::ostringstream	msg;
   msg << "run: end: retval: " << retval;
   AppendDebug( msg.str( ) );
   return retval;
}


//-----------------------------------------------------------------------------
//
//: Cancels execution of a thread.
//
//!except: SwigException - invalid_state: The thread couldn't be
//!except:     canceled because it is not running.
//
void tid::
Cancel( )
{
   AppendDebug( "cancel: start" );
   
   if ( ! Registry::isThreadValid( this ) )
   {
      AppendDebug( "cancel: end: unregistered thread" );
      return;
   }
   ASSERTION( true );
   if ( getState( ) == RUNNING )
   {
      Thread::Cancel( );
      set_state( CANCELED );
   }
   else
   {
      AppendDebug( "cancel: end: exception" );
      throw SwigException( "invalid_state",
			   ": Can't cancel thread.  It is not running.",
			   __FILE__, __LINE__ );
   }
   ASSERTION( true );
   AppendDebug( "cancel: end" );
}


//-----------------------------------------------------------------------------
//
//: Waits for the thread to finish.
//
//!exc: SwigException - INVALID_STATE: The thread couldn't be
//+     waited on because it was IDLE or CANCELED.
//!exc:    
//
void tid::
Join( )
{
    ASSERTION( true );
    switch( getState() )
    {
        case RUNNING:
        case FINISHED:
        {
	   Thread::Join( );
	   set_state( JOINED );
	   break;
        }    
        case IDLE:
            throw SwigException(
                "invalid_state",
                ": Can't get return value. The thread is idle.",
                __FILE__, __LINE__ );
        case CANCELED:
            throw SwigException(
                "invalid_state",
                ": Can't get return value. The thread was canceled.",
                __FILE__, __LINE__ );
        case JOINED:
           break; 
    }

    ASSERTION( true );
    return;
}

std::string tid::
DebugInfo( ) const
{
    MutexLock	lock( &mDebugLock );

    std::ostringstream msg;
    msg << " Function: " << getFunction( )
	<< " mStartTime: " << mStartTime
	<< " mEndTime: " << mEndTime
	<< " mTraceVarName: " << mTraceVarName
	<< " mState: " << mState
	<< "(" << status2string( mState ) << ")"
	<< " mTCLState: " << *mTCLState
	<< "(" << status2string( *mTCLState ) << ")"
	<< "Log: " << mDebugInfo.str( );
    return msg.str( );
}

bool tid::
IsZombie( const bool CheckThreadValid ) const
{
   ASSERTION( true );
   if ( CheckThreadValid )
   {
      if ( Registry::isThreadValid( this ) == false )
      {
	 return false;
      }
   }
   if (	( mState == RUNNING ) &&
	( IsAlive( ) ) &&
	( mState == RUNNING ) )
   {
      // Force updating of state information
      getState( );
      return true;
   }
   return false;
}

bool tid::
NeedTclSync( ) const
{
   ASSERTION( true );
   return ( ( Registry::isThreadValid( this ) ) &&
	    ( *mTCLState != (tcl_state_type)mState ) );
}

void tid::
TclSync( )
{
   std::ostringstream	msg;

   msg.str( "" );
   msg << "TclSync: begin";
   AppendDebug( msg.str( ) );
   ASSERTION( false );
   if ( ( Registry::isThreadValid( this ) ) &&
	( *mTCLState != (tcl_state_type)mState ) )
   {
      *mTCLState = (tcl_state_type)mState;
      ASSERTION( true );
      if ( mTclInterp )
      {
	 ASSERTION( true );
	 if ( mTraceVarName && *mTraceVarName )
	 {	 
	    AT( "before: mTraceVarName: " << mTraceVarName );
	    msg.str( "" );
	    msg << "TclSync: Requesting sync:"
		<< " ParentThread( ): " << ParentThread( )
		<< " mTclInterp: " << mTclInterp
		<< " mTraceVarName: " << mTraceVarName;
	    AppendDebug( msg.str( ) );
	    TIDRequestUpdateLinkedVar( ParentThread( ),
				       mTclInterp,
				       mTraceVarName );
	    AT( "after: mTraceVarName: " << mTraceVarName );
	 }
	 if ( mCallback.length( ) > 0 )
	 {
	    AT( "before: mCallback: " << mCallback );
	    msg.str( "" );
	    msg << "TclSync: Requesting sync:"
		<< " ParentThread( ): " << ParentThread( )
		<< " mTclInterp: " << mTclInterp
		<< " mCallback: " << mCallback;
	    AppendDebug( msg.str( ) );
	    TIDRequestCallback( ParentThread( ),
				mTclInterp,
				mCallback );
	    AT( "after: mCallback: " << mCallback );
	 }
	 ASSERTION( false );
      }
   }
   msg.str( "" );
   msg << "TclSync: end";
   AppendDebug( msg.str( ) );
}


//-----------------------------------------------------------------------------
//
//: Re-throw an exception thrown by the threaded function.
//
// If the wrapped function throws an excpetion then it will be caught and
// stored as a SwigException inside the tid class.  This method re-throws
// the stored exception.  If an exception was not stored, then this method
// nothing.
//
//!except: SwigException - The stored exception.  This is the SwigException
//!except:     thrown by the wrapped thread. If an exception other than a
//!except:     SwigException was thrown, then this will be a SwigException
//!except:     with a message of unkown_exception.
//
void tid::rethrow() throw( SwigException )
{
  ASSERTION( true );
  if ( mExcept != 0 )
  {
    //-------------------------------------------------------------------
    // Associate the thread id with the exception
    //-------------------------------------------------------------------
    std::string result(makeTclPointer(this, "tid"));
    result += ":";
    result += mExcept->getResult();
    throw( SwigException(result, mExcept->getInfo()) );
  }
}


void tid::
setAlert( Tcl_Interp* interp, const std::string& flag )
{
   std::ostringstream	msg;

   msg.str( "" );
   msg << "setAlert: begin: interp: " << (void*)interp << " flag: " << flag;
   AppendDebug( msg.str( ) );
   //---------------------------------------------------------------------
   // This function must be called from the main TCL thread.
   //---------------------------------------------------------------------
   if ( ! Registry::isThreadValid( this ) )
   {
      AppendDebug( "setAlert: end: invalid thread id" );
      return;
   }
   ASSERTION( true );
   if ( interp )
   {
      //-------------------------------------------------------------------
      // Establish the values
      //-------------------------------------------------------------------
      mTclInterp = interp;
   
      delete[] mTraceVarName;
    
      const size_t flag_size( flag.size() + 1 );
      mTraceVarName = new char[ flag_size ];
      strcpy( mTraceVarName, flag.c_str() );
   
      //-------------------------------------------------------------------
      // Link C++ variable to TCL variable
      //-------------------------------------------------------------------
      ASSERTION( true );
      if ( Tcl_LinkVar( interp, mTraceVarName,
			(char*)( mTCLState.get( ) ),
			TCL_LINK_INT | TCL_LINK_READ_ONLY ) == TCL_ERROR )
      {
	 AppendDebug( "setAlert: end: Tcl_LinkVar failed" );
	 mTclInterp = (Tcl_Interp*)NULL;
   
	 delete[] mTraceVarName;
	 mTraceVarName = 0;

	 throw SwigException( "setAlert",
			      ": Cannot establish alert using specified variable.",
			      __FILE__, __LINE__ );
      }
      //-------------------------------------------------------------------
      // Syncronise between the current value of mTCLState and mState
      //-------------------------------------------------------------------
      ASSERTION( true );
      TIDRequestSyncState( this );
   }
   ASSERTION( true );
   AppendDebug( "setAlert: end" );
}

void tid::
unsetAlert( )
{
   AppendDebug( "unsetAlert: begin" );
   if ( ! Registry::isThreadValid( this ) )
   {
      AppendDebug( "unsetAlert: end: invalid thread id" );
      return;
   }
   ASSERTION( true );
   if ( mTclInterp )
   {
      TIDPurge( this );
      (void)Tcl_UnlinkVar( mTclInterp, mTraceVarName );
   
      delete[] mTraceVarName;
      mTraceVarName = 0;
   
      mTclInterp = (Tcl_Interp*)NULL;
   }
   ASSERTION( true );
   AppendDebug( "unsetAlert: end" );
}

void tid::
setCallback( Tcl_Interp* interp, const std::string& callback )
{
   mTclInterp = interp;
   mCallback = callback;

   TIDRequestCallback( ParentThread( ), mTclInterp, mCallback );
}

void tid::
action( )
{
   AppendDebug( "start: start" );
   try
   {
      ASSERTION( true );
      wrapper();
   }
   catch( LdasException& e )
   {
      if ( General::AtExit::IsExiting( ) == false )
      {
	 mExcept = new SwigException( e );
      }
   }
   catch( SwigException& e )
   {
      if ( General::AtExit::IsExiting( ) == false )
      {
	 mExcept = new SwigException( e );
      }
   }
   catch( std::exception& e )
   {
      if ( General::AtExit::IsExiting( ) == false )
      {
	 mExcept = new SwigException( e.what() );
      }
   }
   catch( ... )
   {
      if ( General::AtExit::IsExiting( ) == false )
      {
	 mExcept = new SwigException( "unknown_exception" );
      }
   }
   if ( General::AtExit::IsExiting( ) == false )
   {
      ASSERTION( true );
      set_state( FINISHED );
      ASSERTION( true );
   }
   AppendDebug( "start: end" );
}

void tid::
report_zombie( const std::string& Message )
{
   std::ostringstream	msg;

   msg << "ZOMBIE: GPS Time:" << General::GPSTime::NowGPSTime( )
       << ": " << Message;
   if ( mDebugging )
   {
      std::cerr << "ZOMBIE: addr: " << (void*)(this)
		<< ": "	<< mDebugInfo.str( )
		<< msg.str( )
		<< std::endl;
   }
   const_cast<tid*>(this)->mExcept = new SwigException( msg.str( ) );
}

void tid::
set_state( state State )
{
   MutexLock	lock( &mStateLock );

   std::ostringstream	msg;

   AppendDebug( "set_state: begin" );
   ASSERTION( true );
   // Make sure the state never goes backwards
   switch( State )
   {
   case IDLE:
      if ( ( mState == RUNNING ) ||
	   ( mState == CANCELED ) ||
	   ( mState == FINISHED ) ||
	   ( mState == JOINED ) )
	 
      {
	 msg << "set_state: end: IDLE: mState: " << mState;
	 AppendDebug( msg.str( ) );
	 return;
      }
      break;
   case RUNNING:
      if ( (mState == RUNNING ) ||
	   ( mState == CANCELED ) ||
	   ( mState == FINISHED ) ||
	   ( mState == JOINED ) )
      {
	 msg << "set_state: end: RUNNING: mState: " << mState;
	 AppendDebug( msg.str( ) );
	 return;
      }
      break;
   case CANCELED:
   case FINISHED:
      if ( ( mState == CANCELED ) ||
	   ( mState == FINISHED ) ||
	   ( mState == JOINED ) )
      {
	 msg << "set_state: end: CANCELED/FINISHED: mState: " << mState;
	 AppendDebug( msg.str( ) );
	 return;
      }
      mEndTime.Now( );
      break;
   case JOINED:
      if ( mState == JOINED )
      {
	 msg << "set_state: end: JOINED: mState: " << mState;
	 AppendDebug( msg.str( ) );
	 return;
      }
      break;
   }
   // Actually change the state
   ASSERTION( true );
   mState = State;
   msg << "set_state: end: " << mState;
   AppendDebug( msg.str( ) );
}

void
wrapper_cleanup( void* Arg )
{
   int	old_cancel_state;

   pthread_setcancelstate( PTHREAD_CANCEL_DISABLE, &old_cancel_state );

   try
   {
      tid* t( reinterpret_cast< tid* >( Arg ) );

      std::cerr << "WARNING: " << General::GPSTime::NowGPSTime( )
		<< " Thread cancelled";
      if ( t )
      {
	 std::cerr << ": " << t->getFunction( )
		   << " mStartTime: " << t->mStartTime
		   << " mState: " << t->mState
		   << " mTCLState: " << (t->mTCLState.get( )
					 ? *(t->mTCLState.get( ) )
					 : tid::UNSET_TCL )
		   << std::endl;
      }
      std::cerr << std::endl;
   }
   catch( ... )
   {
   }

   pthread_setcancelstate( old_cancel_state, &old_cancel_state );
}

void*
wrapper( void* v )
{
  pthread_cleanup_push( wrapper_cleanup, v );	// What to do if cancelled
  pthread_setcanceltype( PTHREAD_CANCEL_DEFERRED, 0 );

  tid* 		t( reinterpret_cast< tid* >( v ) );
  sigset_t	mask;

  // Do not want any signal handling to happen.
  sigemptyset(&mask);
  pthread_sigmask(SIG_SETMASK, &mask, NULL);

  if ( t )
  {
     std::ostringstream desc;
     desc << "[Function: " << t->getFunction( ) << "]";
#if DEAD_LOCK_DETECTOR_ENABLED
     General::DeadLockDetector::ThreadRegister( t->threadId( ), desc.str( ) );
#endif /* DEAD_LOCK_DETECTOR_ENABLED */
     t->mStartTime.Now( );	// Log when the thread got started
     t->action( );
#if DEAD_LOCK_DETECTOR_ENABLED
     General::DeadLockDetector::ThreadUnregister( t->threadId( ) );
#endif /* DEAD_LOCK_DETECTOR_ENABLED */
  }
  pthread_cleanup_pop( 0 );	// Normal completion so cleanup the stack.
  return 0;
}


   
const std::string& tidv0::getFunction() const throw()
{
   return mFcn;
}   
   

void tidv0::wrapper()
{
    (*mFunction)();
}
   
namespace Registry
{
   class format_tid_pointers
   {
   public:
      format_tid_pointers( std::ostream& Stream )
	 : m_stream( Stream ),
	   m_pad( false )
      {
      }

      void operator( )( tid* TId )
      {
	 if ( m_pad )
	 {
	    m_stream << " ";
	 }
	 else
	 {
	    m_pad = true;
	 }
	 m_stream << makeTclPointer( TId, "tid" )
		  << " ("  << TId->getFunction( ) << ") "
		  << threadStatus2String( *TId ) << " ";
      } // method - void operator( )( tid* TId )
      
   private:
      std::ostream&		m_stream;
      bool			m_pad;
   }; // class -  format_tid_pointers

   std::string ThreadRegistry_type::
   getThreadList() const
   {
      std::ostringstream	ss;
      format_tid_pointers	ftid( ss );

      ss << "{";
      for_each( ftid );
      ss << "}";
      
      return ss.str( );
   }

   //--------------------------------------------------------------------
   //
   //: Thread Registry
   //
   ThreadRegistry_type&	threadRegistry = init( );
    
   //--------------------------------------------------------------------
   //
   //: Check Thread Pointer
   //
   // True if the thread is valid.
   //
   //!param: thread (tid*)
   //
   //!return: bool - True if the object is valid
   //
   bool isThreadValid( const tid* thread ) throw()
   {
      return ( threadRegistry.isRegistered( thread ) );
   }
} // namespace - Registry

std::string
threadStatus2String( tid& t )
{
   return( status2string( t.getState( ) ) );
}
    
#endif /* HAVE_TCL */
