/* -*- mode: c++ ; c-basic-offset: 2; -*- */

#ifndef GERNEIC_API__KILL_HH
#define GERNEIC_API__KILL_HH

#include "genericAPI/config.h"

namespace GenericAPI
{
  std::string kill( std::string Sig, int PID = 0);
}

#endif /* GERNEIC_API__KILL_HH */
