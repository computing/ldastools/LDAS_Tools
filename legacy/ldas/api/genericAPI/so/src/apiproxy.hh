#ifndef GenericApiApiProxyHH
#define GenericApiApiProxyHH


// System includes
#include <string>
#include <iosfwd>   

// Local includes
#include "apistore.hh"

#include "general/undef_ac.h"

#if HAVE_LIBOSPACE
#include <ospace/stream.h>
#endif /* HAVE_LIBOSPACE */


namespace Registry {


#if HAVE_LIBOSPACE
//-----------------------------------------------------------------------------
//
//: API Proxy
//
// This class implements a proxy for an API. This proxy serves to store
// information about an API which is needed by the GenericAPI.
//
// <p>The ApiStore class implements the method by which the Generic API learns
// about other modules that have been loaded.  When an instance of this class
// is created, the instance registers itself with the global apiStore object.
// Through this object, the generic API has access to the information stored
// by the proxy class.  For example, when the resetApi command is called, it
// executes the reset method for each registered proxy class.  These reset
// methods call the appropriate api reset command.
//
// <p>Each API creates a single static instantiation of the ApiProxy class.
// This instance is constructed with information about the API.  
//
class ApiProxy
{
public:

    /* Constructor */
    ApiProxy(
        const std::string& name, void (*reset)() = 0,
	void (*save)(os_bstream&) = 0,
        void (*restore)(os_bstream&, std::stringstream&) = 0 );

    /* Accessors */
    const std::string& getName() const throw();
    void reset();
    void save( os_bstream& s );
    void restore( os_bstream& s, std::stringstream& ss );
    
private:

    /* Data Members */
    std::string mName;
    void (*mReset)();
    void (*mSave)( os_bstream& );
    void (*mRestore)( os_bstream&, std::stringstream& );
};


//-----------------------------------------------------------------------------
//
//: Get API Name
//
// Returns the API name.
//
//!return: const std::string&
//
//!except: None
//
inline const std::string& ApiProxy::getName() const throw()
{
    return mName;
}

#endif /* HAVE_LIBOSPACE */

} // namespace Registry


#endif // GenericApiApiProxyHH
