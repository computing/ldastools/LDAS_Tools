#include "genericAPI/config.h"

#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <sstream>
#include <stdexcept>

#include "general/mutexlock.hh"

#include "genericAPI/rlimit.hh"

using std::ostringstream;
using std::range_error;

namespace
{
  typedef General::unordered_map< std::string, rlim_t > limit_string_to_value_type;

  const limit_string_to_value_type& map_limit_string_to_value( );
}

namespace GenericAPI
{
  RLimit::limit_table_type RLimit::m_limit_names_to_id;

  RLimit::
  RLimit( )
  {
    init( );
  }

  void RLimit::
  Limit( const char* ResourceName, const char* ValueString ) const
  {
    static const limit_string_to_value_type& m
      = map_limit_string_to_value( );

    rlim_t	value;

    limit_string_to_value_type::const_iterator cur = m.find( ValueString );
    if ( cur == m.end( ) )
    {
      std::istringstream	s( ValueString );
      s >> value;
    }
    else
    {
      value = cur->second;
    }
    Limit( ResourceName, value );
  }

  void RLimit::
  Limit( const char* ResourceName, value_type Value ) const
  {
    limit_table_type::const_iterator
      cur = m_limit_names_to_id.find( ResourceName );
    //-------------------------------------------------------------------
    // Verify that the resouce name is valid
    //-------------------------------------------------------------------
    if ( cur == m_limit_names_to_id.end( ) )
    {
      ostringstream	msg;

      msg << "Invalid or unsupported resource name: " << ResourceName;

      throw( range_error( msg.str( ) ) );
    }
    //-------------------------------------------------------------------
    // Get current limits
    //-------------------------------------------------------------------
    struct rlimit	cur_limit;

    getrlimit( cur->second, &cur_limit );
    //-------------------------------------------------------------------
    // Prepare to reset in accordence with the request
    //-------------------------------------------------------------------
    if ( ( Value == (value_type)RLIM_INFINITY )
	 || ( Value == (value_type)RLIM_SAVED_MAX )
	 || ( Value == (value_type)RLIM_SAVED_CUR ) )
    {
      cur_limit.rlim_cur = Value;
    }
    if ( (rlim_t)Value > cur_limit.rlim_max )
    {
      cur_limit.rlim_cur = cur_limit.rlim_max;
    }
#if NEEDED
    else if ( Value >= 0 )
    {
      cur_limit.rlim_cur = (rlim_t)Value;
    }
    else
    {
      ostringstream	msg;

      msg << "Value of " << Value
	  << " out of range for resource \"" << ResourceName << "\"";

      throw( range_error( msg.str( ) ) );
    }
#else
    else
    {
      cur_limit.rlim_cur = (rlim_t)Value;
    }
#endif
    //-------------------------------------------------------------------
    // Reset in accordence with the request
    //-------------------------------------------------------------------
    if ( setrlimit( cur->second, &cur_limit ) != 0 )
    {
      ostringstream	msg;
      char		buffer[1024];

      strerror_r( errno, buffer, sizeof( buffer ) - 1);
      msg << "Unable to set \"" << ResourceName << "\""
	  << " to " << Value
	  << " because: " << buffer;
    }
  }

  std::string RLimit::
  LimitString( const char* ResourceName ) const
  {
    value_type	value = Limit( ResourceName );
    std::ostringstream	retval;

    switch( value )
    {
    case (value_type)RLIM_INFINITY:
      retval << "unlimited";
      break;
#if HAVE_RLIM_SAVED_MAX
    case (value_type)RLIM_SAVED_MAX:
      retval << "saved_max";
      break;
#endif /* HAVE_RLIM_SAVED_MAX */
#if HAVE_RLIM_SAVED_CUR
    case (value_type)RLIM_SAVED_CUR:
      retval << "saved_cur";
      break;
#endif /* HAVE_RLIM_SAVED_CUR */
    default:
      {
	retval << value;
      }
    }
    return retval.str( );
  }

  RLimit::value_type RLimit::
  Limit( const char* ResourceName ) const
  {
    limit_table_type::const_iterator
      cur = m_limit_names_to_id.find( ResourceName );
    //-------------------------------------------------------------------
    // Verify that the resouce name is valid
    //-------------------------------------------------------------------
    if ( cur == m_limit_names_to_id.end( ) )
    {
      ostringstream	msg;

      msg << "Invalid or unsupported resource name: " << ResourceName;

      throw( range_error( msg.str( ) ) );
    }
    //-------------------------------------------------------------------
    // Get current limits
    //-------------------------------------------------------------------
    struct rlimit	cur_limit;

    getrlimit( cur->second, &cur_limit );
    //-------------------------------------------------------------------
    // Return to the caller
    //-------------------------------------------------------------------
    return cur_limit.rlim_cur;
  }

  void RLimit::
  init( )
  {
    static bool initialized = false;

    if ( initialized )
    {
      return;
    }

    static MutexLock::lock_type	key = MutexLock::Initialize( );

    MutexLock	lock( key );
    if ( initialized == true )
    {
      return;
    }

    if ( m_limit_names_to_id.size( ) <= 0 )
    {
      //-----------------------------------------------------------------
      // Address space
      //-----------------------------------------------------------------
      m_limit_names_to_id[ "as" ]= RLIMIT_AS;
      m_limit_names_to_id[ "vmemoryuse" ] = RLIMIT_AS;
      //-----------------------------------------------------------------
      // Size of core file
      //-----------------------------------------------------------------
      m_limit_names_to_id[ "core" ] = RLIMIT_CORE;
      //-----------------------------------------------------------------
      // CPU time limit in seconds
      //-----------------------------------------------------------------
      m_limit_names_to_id[ "cpu" ] = RLIMIT_CPU;
      m_limit_names_to_id[ "cputime" ] = RLIMIT_CPU;
      //-----------------------------------------------------------------
      // Maximum size of data segment
      //-----------------------------------------------------------------
      m_limit_names_to_id[ "data" ] = RLIMIT_DATA;
      m_limit_names_to_id[ "datasize" ] = RLIMIT_DATA;
      //-----------------------------------------------------------------
      // Max. size of files to create
      //-----------------------------------------------------------------
      m_limit_names_to_id[ "fsize" ] = RLIMIT_FSIZE;
      m_limit_names_to_id[ "filesize" ] = RLIMIT_FSIZE;
      //-----------------------------------------------------------------
      // Max. # of file locks
      //-----------------------------------------------------------------
#ifdef RLIMIT_LOCKS
      m_limit_names_to_id[ "locks" ] = RLIMIT_LOCKS;
#endif /* RLIMIT_LOCKS */
      //-----------------------------------------------------------------
      // Max # of blocks locked in RAM
      //-----------------------------------------------------------------
#ifdef RLIMIT_MEMLOCK
      m_limit_names_to_id[ "memlock" ] = RLIMIT_MEMLOCK;
      m_limit_names_to_id[ "memorylocked" ] = RLIMIT_MEMLOCK;
      //-----------------------------------------------------------------
      // Max. # of simultanious file desc.
      //-----------------------------------------------------------------
#endif /* RLIMIT_MEMLOCK */
      m_limit_names_to_id[ "nofile" ] = RLIMIT_NOFILE;
      m_limit_names_to_id[ "descriptors" ] = RLIMIT_NOFILE;
      //-----------------------------------------------------------------
      // Max. # of procs. for user.
      //-----------------------------------------------------------------
#ifdef RLIMIT_NPROC
      m_limit_names_to_id[ "nproc" ] = RLIMIT_NPROC;
      m_limit_names_to_id[ "maxproc" ] = RLIMIT_NPROC;
#endif /* RLIMIT_NPROC */
    }

    initialized = true;
  }
}

namespace
{
  const limit_string_to_value_type&
  map_limit_string_to_value( )
  {
    static limit_string_to_value_type	m;

    m[ "infinity" ] = RLIM_INFINITY;
    m[ "unlimited" ] = RLIM_INFINITY;

    return m;
  }
} // namespace - anonymous
