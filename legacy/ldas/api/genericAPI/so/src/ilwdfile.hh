#ifndef GenericApiILwdFileHH
#define GenericApiILwdFileHH


#include "inputfile.hh"
#include "outputfile.hh"


#if HAVE_LDAS_PACKAGE_ILWD
typedef File ILwdFile;


//-----------------------------------------------------------------------------
//
//: ILWD Input File
//
// This is derived from the InputFile class.  This class extends InputFile by
// reading the ILWD PI when it is opened for reading.
//
class InputILwdFile : public InputFile
{
public:

    /* Constructor */
    InputILwdFile( const char* filename, std::ios::openmode m )
        throw( SwigException );
};


//-----------------------------------------------------------------------------
//
//: ILWD Output File
//
// This is derived from the OutputFile class.  This class extends OutputFile by
// writing the ILWD PI when it is opened for writing (but not appending).
//
class OutputILwdFile : public OutputFile
{
public:

    /* Constructor */
    OutputILwdFile( const char* filename, std::ios::openmode m )
        throw( SwigException );
};
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // GenericApiFileHH
