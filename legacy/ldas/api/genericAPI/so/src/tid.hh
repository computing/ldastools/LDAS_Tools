/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef TIDHH
#define TIDHH

#include "genericAPI/config.h"

#if HAVE_TCL
#include <stdlib.h>

#include <memory>
#include <string>
#include <sstream>

#include "general/AtExit.hh"
#include "general/gpstime.hh"
#include "general/mutexlock.hh"
#include "general/objectregistry.hh"
#include "general/Thread.hh"

#include "genericAPI/swigexception.hh"

extern "C" {
    void* wrapper( void* v );
}

//-----------------------------------------------------------------------------
//: The base class for thread argumets.
//-----------------------------------------------------------------------------
template< class Arg_ >
class tid_arg
{
public:
    tid_arg( Arg_ Arg );
    ~tid_arg( );
    Arg_ operator()();

private:
    Arg_	m_arg;
};

//!ignore_begin:
//
// Don't generate documentation for template specialization
template<>
inline tid_arg< const char* >::
tid_arg( const char* Arg )
{
    if ( Arg != (const char*)NULL )
    {
	m_arg = strdup( Arg );
    }
    else
    {
	m_arg = (const char*)NULL;
    }
}

template<>
inline tid_arg< const char* >::
~tid_arg( )
{
    free( (void*)( m_arg ) );
}

template<>
inline tid_arg< char* >::
tid_arg( char* Arg )
{
    if ( Arg != (char*)NULL )
    {
	m_arg = strdup( Arg );
    }
    else
    {
	m_arg = (char*)NULL;
    }
}

template<>
inline tid_arg< char* >::
~tid_arg( )
{
    free( (void*)( m_arg ) );
}
//!ignore_end:

template< class Arg_ >
tid_arg< Arg_ >::
tid_arg( Arg_ Arg )
    : m_arg( Arg )
{
}

template< class Arg_ >
tid_arg< Arg_ >::
~tid_arg( )
{
}

template< class Arg_ >
Arg_ tid_arg< Arg_ >::
operator()()
{
    return m_arg;
}

//-----------------------------------------------------------------------------
//
//: The base class for threads.
//
// The class wraps a standard c-like function and runs it in a separate thread.
//
// "tid" is the parent class for all of the differenc thread classes.  The
// children are templates used to support wrapping functions with varying
// numbers (and types) of parameters and return values.  This parent class
// provides the methods needed to manage the running thread, such as starting,
// canceling and waiting.  The child class is used to initialize the object
// and to obtain the return value.
//
// So, the thread class hierarchy consists of two levels.  The highest level
// has the parent class, tid.  The second level contains all of the children,
// which are templates and differ only in their parameters and return values.
//
// The tid class starts the thread through the "wrapper" pure virtual method.
// This method takes no arguments and returns void.  This wrapper is meant to
// be used to start the wrapped function.  The child class is meant to store
// all of the parameters and the return value, since the parent class can't
// know their types.  The wrapper method must call the proper function, passing
// the parameters and storing the return value.
//
// Program Flow:
//
// <ol>
//   <li>The appropriate child class is constructed.  The parameters are stored
//       in the child class so that they may be passed to the wrapped function
//       later when it is ran.  The parent class sets its state to idle,
//       its pthread_t ID to 0, and its exception pointer to null.</li>
//   <li>The user calls the run method.</li>
//   <li>If the thread isn't already running, call pthread_create, instructing
//       it to begin executing the ::wrapper function.  Pass a pointer to this
//       tid class to the ::wrapper function.</li>
//   <li>In the wrapper function, set the thread cancel type to asynchronous.
//       This ensures that a pthread_cancel takes effect right away. (WHAT
//       ABOUT MEMORY LEAKS FROM THIS?).  Use the passed tid* to call the tid
//       start method.</li>
//   <li>The start method calls the virtual wrapper method (different from the
//       wrapper function, which is outside the class).  If the wrapped method
//       throws an exception it is caught here.  Exceptions are stored in the
//       mExcept variable.  It is the responsibility of the child class to
//       call tid's rethrow method to rethrow any exceptions before returning
//       a value in the child's getReturn method.</li>
//   <li>The virtual wrapper method calls the threaded function and passes all
//       of the stored parameters.</li>
//   <li>When the threaded function returns, the return value is stored in the
//       child class.</li>
//   <li>The start method sets the state to FINISHED.</li>
//   <li>The user calls the child's getReturn method.</li>
//   <li>The getReturn method joins the thread if it is running or finished.
//       (See the todo)</li>
//   <li>getReturn calls rethrow to check for exceptions and then returns the
//       return value.</li>
//
//!todo: Maybe create a field to record whether or not the thread has been
//!todo: joined.  This way, pthread_join wouldn't have to be called repeatedly
//!todo: when getReturn is called to ensure that memory is deallocated.
//
class tid
    : public General::Thread
{
public:

    //: The state of the thread
    enum state
    {
        FINISHED  = 0,		// the thread has completed
        RUNNING   = 1,		// the thread is running
        IDLE      = 2,		// the thread has not yet started
        CANCELED  = 3,		// the thread was canceled
        JOINED    = 4		// the thread was joined
    };

    enum tcl_state_type
    {
	FINISHED_TCL = FINISHED,
	RUNNING_TCL = RUNNING,
	IDL_TCL = IDLE,
	CANCELED_TCL = CANCELED,
	JOINED_TCL = JOINED,
	// Options specific to tcl_state_type
	UNSET_TCL = JOINED + 1
    };
  

    void rethrow() throw( SwigException );
    
    /* Constructor */
    tid() throw();

    /* Destructor */
    virtual ~tid();
    
    /* Accessor */
    state getState() const throw();
    virtual const std::string& getFunction() const throw() = 0;

    thread_type getParentThread( ) const;

    /* Mutators */
    void setAlert( Tcl_Interp* interp, const std::string& flag );
    void setCallback( Tcl_Interp* interp, const std::string& callback );

    /* Mutators */
    void unsetAlert( );
    
    /* Thread Control */
    virtual int Spawn( );

    virtual void Cancel( );
    virtual void Join( );

    /* Interface to support variable tracing */
    bool IsZombie( const bool CheckThreadValid = true ) const;
    bool NeedTclSync( ) const;
    void TclSync( );
    
    void AppendDebug( const std::string& Message );
    std::string DebugInfo( ) const;
    static void DebugDefault( bool Mode );

private:
    friend bool TIDForceQuit( tid* TID );

    //: No copy constructor
    tid( const tid& );
   
    //: No assignment operator
    tid& operator=( const tid& );
   
    friend void* wrapper( void* );
    friend void wrapper_cleanup( void* Arg );
    
    void assertion( const char* const Filename,
		    const int Line,
		    bool ThrowException ) const;

    void report_zombie( const std::string& Message );

    void set_state( state State );

    virtual void action( );

    virtual void wrapper() = 0;

    // Protected via set_state call
    state mState;
    // Last state reported to TCL
    std::auto_ptr< tcl_state_type >	mTCLState;

    SwigException* mExcept;

    Tcl_Interp*				mTclInterp;
    char*       			mTraceVarName;
    std::string       			mCallback;
    mutable bool			mAssertionExceptionThrown;

    static bool				mDebuggingDefault;
    bool				mDebugging;
    mutable pthread_mutex_t		mDebugLock;
    std::ostringstream			mDebugInfo;
    pthread_mutex_t			mStateLock;
    General::GPSTime			mStartTime;
    General::GPSTime			mEndTime;
};

inline void tid::
DebugDefault( bool Mode )
{
    mDebuggingDefault = Mode;
}

inline void tid::
AppendDebug( const std::string& Message )
{
    if ( mDebugging
	 && ! General::AtExit::IsExiting( ) )
    {
	MutexLock	lock( &mDebugLock );

	mDebugInfo << General::GPSTime::NowGPSTime( ) << ": "
		   << Message
		   << std::endl;
    }
}

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

#define TEMPLATE_PARAMS1 class P1
#define TEMPLATE_PARAMS2 TEMPLATE_PARAMS1, class P2
#define TEMPLATE_PARAMS3 TEMPLATE_PARAMS2, class P3
#define TEMPLATE_PARAMS4 TEMPLATE_PARAMS3, class P4
#define TEMPLATE_PARAMS5 TEMPLATE_PARAMS4, class P5
#define TEMPLATE_PARAMS6 TEMPLATE_PARAMS5, class P6
#define TEMPLATE_PARAMS7 TEMPLATE_PARAMS6, class P7
#define TEMPLATE_PARAMS8 TEMPLATE_PARAMS7, class P8
#define TEMPLATE_PARAMS9 TEMPLATE_PARAMS8, class P9
#define TEMPLATE_PARAMS10 TEMPLATE_PARAMS9, class P10

#define TYPE_PARAMS1     P1
#define TYPE_PARAMS2     TYPE_PARAMS1, P2
#define TYPE_PARAMS3     TYPE_PARAMS2, P3
#define TYPE_PARAMS4     TYPE_PARAMS3, P4
#define TYPE_PARAMS5     TYPE_PARAMS4, P5
#define TYPE_PARAMS6     TYPE_PARAMS5, P6
#define TYPE_PARAMS7     TYPE_PARAMS6, P7
#define TYPE_PARAMS8     TYPE_PARAMS7, P8
#define TYPE_PARAMS9     TYPE_PARAMS8, P9
#define TYPE_PARAMS10    TYPE_PARAMS9, P10

#define FCN_PARAMS1      P1 p1
#define FCN_PARAMS2      FCN_PARAMS1, P2 p2
#define FCN_PARAMS3      FCN_PARAMS2, P3 p3
#define FCN_PARAMS4      FCN_PARAMS3, P4 p4
#define FCN_PARAMS5      FCN_PARAMS4, P5 p5
#define FCN_PARAMS6      FCN_PARAMS5, P6 p6
#define FCN_PARAMS7      FCN_PARAMS6, P7 p7
#define FCN_PARAMS8      FCN_PARAMS7, P8 p8
#define FCN_PARAMS9      FCN_PARAMS8, P9 p9
#define FCN_PARAMS10     FCN_PARAMS9, P10 p10

#define DATA_PARAMS1     tid_arg< P1 > mP1
#define DATA_PARAMS2     DATA_PARAMS1; tid_arg< P2 > mP2
#define DATA_PARAMS3     DATA_PARAMS2; tid_arg< P3 > mP3
#define DATA_PARAMS4     DATA_PARAMS3; tid_arg< P4 > mP4
#define DATA_PARAMS5     DATA_PARAMS4; tid_arg< P5 > mP5
#define DATA_PARAMS6     DATA_PARAMS5; tid_arg< P6 > mP6
#define DATA_PARAMS7     DATA_PARAMS6; tid_arg< P7 > mP7
#define DATA_PARAMS8     DATA_PARAMS7; tid_arg< P8 > mP8
#define DATA_PARAMS9     DATA_PARAMS8; tid_arg< P9 > mP9
#define DATA_PARAMS10     DATA_PARAMS9; tid_arg< P10 > mP10

#define DATA_INIT1       mP1( p1 )
#define DATA_INIT2       DATA_INIT1, mP2( p2 )
#define DATA_INIT3       DATA_INIT2, mP3( p3 )
#define DATA_INIT4       DATA_INIT3, mP4( p4 )
#define DATA_INIT5       DATA_INIT4, mP5( p5 )
#define DATA_INIT6       DATA_INIT5, mP6( p6 )
#define DATA_INIT7       DATA_INIT6, mP7( p7 )
#define DATA_INIT8       DATA_INIT7, mP8( p8 )
#define DATA_INIT9       DATA_INIT8, mP9( p9 )
#define DATA_INIT10       DATA_INIT9, mP10( p10 )

#define DATA_MEMBERS1    mP1( )
#define DATA_MEMBERS2    DATA_MEMBERS1, mP2( )
#define DATA_MEMBERS3    DATA_MEMBERS2, mP3( )
#define DATA_MEMBERS4    DATA_MEMBERS3, mP4( )
#define DATA_MEMBERS5    DATA_MEMBERS4, mP5( )
#define DATA_MEMBERS6    DATA_MEMBERS5, mP6( )
#define DATA_MEMBERS7    DATA_MEMBERS6, mP7( )
#define DATA_MEMBERS8    DATA_MEMBERS7, mP8( )
#define DATA_MEMBERS9    DATA_MEMBERS8, mP9( )
#define DATA_MEMBERS10    DATA_MEMBERS9, mP10( )


//-----------------------------------------------------------------------------
#define TID_CLASS_DECL( argn )                                                \
template< class R, TEMPLATE_PARAMS##argn >                                    \
class tid##argn : public tid                                                  \
{                                                                             \
public:                                                                       \
    tid##argn ( R (*f)( TYPE_PARAMS##argn ), const std::string& fcn,          \
                 FCN_PARAMS##argn );                                          \
    R getReturn();                                                            \
    const std::string& getFunction() const throw();                           \
                                                                              \
private:                                                                      \
    void wrapper();                                                           \
    R (*mFunction)( TYPE_PARAMS##argn );                                      \
    std::string mFcn;                                                         \
    R mReturn;                                                                \
    DATA_PARAMS##argn;                                                        \
};                                                                            \
                                                                              \
template< class R, TEMPLATE_PARAMS##argn >                                    \
inline tid##argn< R, TYPE_PARAMS##argn >::tid##argn (                         \
    R (*f)( TYPE_PARAMS##argn ), const std::string& fcn, FCN_PARAMS##argn )   \
        : tid(), mFunction( f ), mFcn( fcn ), mReturn(), DATA_INIT##argn      \
{									      \
}                                                                             \
                                                                              \
template< class R, TEMPLATE_PARAMS##argn >                                    \
R tid##argn< R, TYPE_PARAMS##argn >::getReturn()                              \
{                                                                             \
    Join();                                                                   \
    rethrow();                                                                \
    return mReturn;                                                           \
}                                                                             \
                                                                              \
template< class R, TEMPLATE_PARAMS##argn >                                    \
const std::string& tid##argn< R, TYPE_PARAMS##argn >::getFunction() const     \
   throw()                                                                    \
{                                                                             \
    return mFcn;                                                              \
}                                                                             \
                                                                              \
template< class R, TEMPLATE_PARAMS##argn >                                    \
void tid##argn< R, TYPE_PARAMS##argn >::wrapper()                             \
{                                                                             \
    mReturn = (*mFunction)( DATA_MEMBERS##argn );                             \
    return;                                                                   \
}


//-----------------------------------------------------------------------------
#define TID_CLASSV_DECL( argn )                                               \
template< TEMPLATE_PARAMS##argn >                                             \
class tidv##argn : public tid                                                 \
{                                                                             \
public:                                                                       \
    tidv##argn( void (*f)( TYPE_PARAMS##argn ), const std::string& fcn,       \
                  FCN_PARAMS##argn );                                         \
    void getReturn();                                                         \
    const std::string& getFunction() const throw();                           \
                                                                              \
private:                                                                      \
    void wrapper();                                                           \
    void (*mFunction)( TYPE_PARAMS##argn );                                   \
    std::string mFcn;                                                         \
    DATA_PARAMS##argn;                                                        \
};                                                                            \
                                                                              \
template< TEMPLATE_PARAMS##argn >                                             \
inline tidv##argn< TYPE_PARAMS##argn >::tidv##argn(                           \
    void (*f)( TYPE_PARAMS##argn ), const std::string& fcn, FCN_PARAMS##argn )\
        : tid(), mFunction( f ), mFcn( fcn ), DATA_INIT##argn                 \
{									      \
}                                                                             \
                                                                              \
template< TEMPLATE_PARAMS##argn >                                             \
void tidv##argn< TYPE_PARAMS##argn >::getReturn()                             \
{                                                                             \
    Join();                                                                   \
    rethrow();                                                                \
    return;                                                                   \
}                                                                             \
                                                                              \
template< TEMPLATE_PARAMS##argn >                                             \
const std::string& tidv##argn< TYPE_PARAMS##argn >::getFunction() const       \
   throw()                                                                    \
{                                                                             \
    return mFcn;                                                              \
}                                                                             \
                                                                              \
template< TEMPLATE_PARAMS##argn >                                             \
void tidv##argn< TYPE_PARAMS##argn >::wrapper()                               \
{                                                                             \
    (*mFunction)( DATA_MEMBERS##argn );                                       \
    return;                                                                   \
}

   
//-----------------------------------------------------------------------------
template< class R >
class tid0 : public tid
{
public:
    tid0( R (*f)(), const std::string& fcn );
    R getReturn();
    const std::string& getFunction() const throw ();

private:
    void wrapper();
    R (*mFunction)();
    std::string mFcn;
    R mReturn;
};

template< class R >
inline tid0< R >::tid0( R (*f)(), const std::string& fcn )
        : tid(), mFunction( f ), mFcn( fcn ), mReturn()
{
}

template< class R >
R tid0< R >::getReturn()
{
    Join();
    rethrow();
    return mReturn;
}
   
   
template< class R > 
const std::string& tid0< R >::getFunction() const throw()
{
   return mFcn;
}

template< class R >
void tid0< R >::wrapper()
{
    mReturn = (*mFunction)();
    return;
}


//-----------------------------------------------------------------------------
class tidv0 : public tid
{
public:
    tidv0( void (*f)(), const std::string& fcn );
    void getReturn();
    const std::string& getFunction() const throw();
    
private:
    void wrapper();
    void (*mFunction)();
    std::string mFcn;
};

inline tidv0::tidv0( void (*f)(), const std::string& fcn )
        : tid(), mFunction( f ), mFcn( fcn )
{
}

inline void tidv0::getReturn()
{
    Join();
    rethrow();
    return;
}



TID_CLASSV_DECL( 1 )
TID_CLASSV_DECL( 2 )
TID_CLASSV_DECL( 3 )
TID_CLASSV_DECL( 4 )
TID_CLASSV_DECL( 5 )
TID_CLASSV_DECL( 6 )
TID_CLASSV_DECL( 7 )
TID_CLASSV_DECL( 8 )
TID_CLASSV_DECL( 9 )
TID_CLASSV_DECL( 10 )

TID_CLASS_DECL( 1 )
TID_CLASS_DECL( 2 )
TID_CLASS_DECL( 3 )
TID_CLASS_DECL( 4 )
TID_CLASS_DECL( 5 )
TID_CLASS_DECL( 6 )
TID_CLASS_DECL( 7 )
TID_CLASS_DECL( 8 )
TID_CLASS_DECL( 9 )
TID_CLASS_DECL( 10 )

namespace Registry
{    
    class ThreadRegistry_type
	: public ObjectRegistry< tid >
    {
    public:
	std::string getThreadList( ) const;

#if 0
    protected:
	typedef ObjectRegistry< tid >::const_iterator
	const_iterator;
	typedef ObjectRegistry< tid >::iterator iterator;
#endif /* 0 */
    };

    extern ThreadRegistry_type& threadRegistry;
    bool isThreadValid( const tid* thread ) throw();
}

std::string threadStatus2String( tid& t );

#define HAVE_TID 1

#endif // HAVE_TCL
#endif // TIDHH
