#include "genericAPI/config.h"

#include <iomanip>
#include <iostream>
#include <sstream>

#include "general/types.hh"
#include "general/util.hh"
#include "general/ErrorLog.hh"

#include "genericAPI/UtilTCL.hh"

using General::ErrorLog;

std::string
makeTclPointer( const void* p,
		const std::string& classname )
{
    static const char* func = "makeTclPointer";
    std::ostringstream	dbg_msg;

    if ( General::StdErrLog.IsOpen( ) )
    {
      dbg_msg << func << ": p: " << p
	      << " classname: " << classname;

      General::StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__,
			  dbg_msg.str( ) );
    }

    MEM_PTR	mem( reinterpret_cast< MEM_PTR >( p ) );
#if WORDS_BIGENDIAN != 1
    reverse<sizeof(mem)>(&mem, 1);
#endif
    std::ostringstream ss;
    ss << "_" << std::setfill( '0' )
       << std::hex << std::setw( sizeof( MEM_PTR ) * 2  )
       << mem << "_p_"
       << classname;

    if ( General::StdErrLog.IsOpen( ) )
    {
      dbg_msg.str( func );
      dbg_msg << ": " << ss.str( );

      General::StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__,
			  dbg_msg.str( ) );
    }

    return ss.str();
}

