#include "genericAPI/config.h"

#include "apistore.hh"
#include "apiproxy.hh"


namespace Registry {
    

// The global ApiStore object
ApiStore apiStore;


//: Default constructor
//
// This one is suppled to prevent Compiler warning about not begin able
// to inline the function.
ApiStore::
ApiStore( )
{
}
//-----------------------------------------------------------------------------
//
//: Register a proxy object.
//
// This method registers a proxy object with this
// class.  It first initializes the internal storage (if necessary) and then
// adds the pointer (if it is not NULL).  If the pointer is null, nothing is
// added.
//
//!param: ApiProxy* p - a pointer the proxy class to register.  If this is    +
//+       null then nothing is added to the storage (the internal storage +
//+       will be initialized if it hasn't already).
//
void ApiStore::registerApi( ApiProxy* p ) throw()
{
    if ( p != 0 )
    {
        mApi.push_back( p );
    }
}


//-----------------------------------------------------------------------------
//
//: Return a pointer to a contained proxy.
//
//!param: unsigned int index - An index (zero-offset) referring to a stored   +
//+       proxy.  If this is out of range the method returns a null       +
//+       pointer.
//
//!return: ApiProxy* - A pointer to the contained proxy.  This is null if the
//+        index is out of range.
//
ApiProxy* ApiStore::operator[]( unsigned int index ) throw()
{
    // Make sure the store has been initialized and that the index is in range
    if ( index < mApi.size() )
    {
        return mApi[ index ];
    }

    return 0;
}

        
//-----------------------------------------------------------------------------
//
//: Return a constant pointer to a contained proxy.
//
//!param: unsigned int index - An index (zero-offset) referring to a stored   +
//!param:     proxy.  If this is out of range the method returns a null       +
//!param:     pointer.
//
//!return: const ApiProxy* - A constant pointer to the contained proxy.  This
//!return:     is null if the index is out of range.
//
const ApiProxy* ApiStore::operator[]( unsigned int index ) const throw()
{
    // Make sure the store has been initialized and that the index is in range
    if ( index < mApi.size( ) )
    {
        return mApi[ index ];
    }

    return 0;
}

 
} // namespace Registry
