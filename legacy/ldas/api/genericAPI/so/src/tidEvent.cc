#include "genericAPI/config.h"

#if HAVE_TCL

#include <signal.h>
#include <time.h>
#include <tcl.h>

#include <list>

#include "general/AtExit.hh"
#include "general/unordered_map.hh"
#include "general/mutexlock.hh"
#include "general/autoarray.hh"

#include "genericAPI/tid.hh"

#ifndef CONST84
#define CONST84
#endif

//=======================================================================
//                P R I V A T E   D E C L A R A T I O N S
//=======================================================================

//-----------------------------------------------------------------------
// Request to change state of tid class
//-----------------------------------------------------------------------
namespace {
  class action
  {
  public:
    virtual ~action( )
    {
    }

    virtual void operator()( ) = 0;
  };

  class setVarAction
    : public action
  {
  public:
    setVarAction( Tcl_Interp* Interp, const std::string& Variable )
      : m_interp( Interp ),
	m_variable( Variable )
    {
    }

    virtual void operator()( )
    {
      Tcl_UpdateLinkedVar( m_interp, (CONST84 char *)m_variable.c_str( ) );
    }

  private:
    Tcl_Interp*	m_interp;
    std::string	m_variable;
  };

  class callbackAction
    : public action
  {
  public:
    callbackAction( Tcl_Interp* Interp, const std::string& Callback )
      : m_interp( Interp ),
	m_callback( Callback )
    {
    }

    virtual void operator()( )
    {
      Tcl_Eval( m_interp, (CONST84 char *)m_callback.c_str( ) );
    }

  private:
    Tcl_Interp*	m_interp;
    std::string	m_callback;
  };

  class isZombie
  {
  public:
    isZombie( )
    {
    }

    bool operator()(tid* TID)
    {
      return TID->IsZombie( false );
    }
  };

  class tid_state_change_events
    : private std::list< tid* >
  {
  public:
    tid_state_change_events( pthread_t Thread );
    ~tid_state_change_events( );

    // std::list< tid* >::size;
    using std::list<tid* >::size;

    bool IsSyncing( ) const;
    void Purge( tid* TID );
    void Push( tid* TID );
    bool NeedTclSync( ) const;
    void TclSync( );

  private:
    pthread_t		m_controller;
    bool		m_syncing;
  };

  struct var_type {
    Tcl_Interp*		s_TclInterp;
    std::string		s_name;
    tid::tcl_state_type*	s_storage;

    inline
    var_type( Tcl_Interp*		Interp,
	   const std::string&	Name,
	   tid::tcl_state_type*	Storage )
      : s_TclInterp( Interp ),
	s_name( Name ),
	s_storage( Storage )
    {
    }
  };

  typedef action*  var_data_type;
  typedef std::list< var_data_type > var_list_type;
  typedef std::list< var_type > delete_list_type;

  struct state_info_type {
    pthread_t		m_controller;
    bool		m_syncing;
    pthread_mutex_t	m_queue_lock;
    pthread_mutex_t	m_action_queue_lock;
    pthread_mutex_t	m_delete_lock;

    inline
    state_info_type( pthread_t Thread )
      : m_controller( Thread ),
	m_syncing( false )
    {
      pthread_mutex_init( &m_queue_lock, (pthread_mutexattr_t*)NULL );
      pthread_mutex_init( &m_action_queue_lock, (pthread_mutexattr_t*)NULL );
      pthread_mutex_init( &m_delete_lock, (pthread_mutexattr_t*)NULL );
    }
  };

  struct thread_specific_data_type {
    tid_state_change_events*	s_queue;
    var_list_type*		s_action_queue;
    delete_list_type*		s_delete_queue;
    state_info_type*		s_state_info;
  };

  static Tcl_ThreadDataKey tsd_key;

  static thread_specific_data_type* InitTSD ( pthread_t Thread = pthread_self( ) );
  extern "C" {
    static void ExitProc( ClientData );
  }

  extern "C" {
    static void check( ClientData, int Flags );
    static int handler( Tcl_Event* Event, int Flags );
    static void setup( ClientData, int Flags );
  }

}


//=======================================================================
//                   P U B L I C   F U N C T I O N S
//=======================================================================

bool
TIDForceQuit( tid* TID )
{
  bool	retval = false;
  if ( TID )
  {
    TID->AppendDebug( "TIDForceQuit: begin" );
    //-------------------------------------------------------------------
    // Try force cleanup of zombie processes
    //-------------------------------------------------------------------
    if ( TID->IsZombie( ) )
    {
      retval = true;
    } else {
      //-----------------------------------------------------------------
      // Try canceling the thread and create a zombie
      //-----------------------------------------------------------------
      TID->Cancel( );
      int cnt = 0;
      const int cnt_end = 20000;
      while ( ( cnt < cnt_end) &&
	      ( TID->getState( ) != tid::CANCELED ) &&
	      ( TID->IsZombie( ) == false ) )
      {
	cnt++;
      }

      {
	std::ostringstream	msg;

	msg << "TIDForceQuit: zombie created in " << cnt
	  << " of " << cnt_end << " iterations";
	TID->AppendDebug( msg.str( ) );
      }
      if ( TID->IsZombie( ) == true )
      {
	retval = true;
      }
      else
      {
	TID->mState = tid::RUNNING;
	if ( TID->IsZombie( ) )
	{ 
	  retval = true;
	}
      }
    }
    {
      std::ostringstream msg;

      msg << "TIDForceQuit: end: retval: " << retval;
      TID->AppendDebug( msg.str( ) );
    }
  }
  return retval;
}

void
TIDPurge( tid* TID )
{
  if ( TID )
  {
    TID->AppendDebug( "TIDPurge: begin" );
    if ( General::AtExit::IsExiting( ) )
    {
      //-----------------------------------------------------------------
      // Prevent core dump in lsync (PR#3137)
      //
      // If things are exiting, then the TCL interpreter has already
      //   been destroyed.
      //-----------------------------------------------------------------
      delete TID;
    }
    else
    {
      thread_specific_data_type*
	tsd( InitTSD( TID->ParentThread( ) ) );

      MutexLock	lock( tsd->s_state_info->m_queue_lock );

      if ( tsd && ( tsd->s_queue ) )
      {
	tsd->s_queue->Purge( TID );
      }
      TID->AppendDebug( "TIDPurge: end" );
    }
  }
}

void
TIDRequestSyncState( tid* TID )
{
  if ( TID )
  {
    TID->AppendDebug( "TIDRequestSyncState: begin" );
    thread_specific_data_type*	tsd( InitTSD( TID->ParentThread( ) ) );

    {
      MutexLock	lock( tsd->s_state_info->m_queue_lock );

      if ( tsd->s_queue )
      {
	tsd->s_queue->Push( TID );
      }
    }

    if ( TID->IsParentThread( ) )
    {
      setup( NULL, 0 );
    }
    TID->AppendDebug( "TIDRequestSyncState: end" );
  }
}

void
TIDRequestCallback( const pthread_t& ParentThread,
		    Tcl_Interp* TclInterp,
		    const std::string& Callback )
{
  if ( Callback.length( ) > 0 )
  {
    thread_specific_data_type*	tsd( InitTSD( ParentThread ) );

    if ( tsd && tsd->s_action_queue )
    {
      MutexLock	lock( tsd->s_state_info->m_action_queue_lock );

      tsd->s_action_queue->push_back( new callbackAction( TclInterp,
							  Callback ) );
    }
  }
}

void
TIDRequestUpdateLinkedVar( const pthread_t& ParentThread,
			   Tcl_Interp* TclInterp,
			   const std::string& Variable )
{
  if ( Variable.length( ) > 0 )
  {
    thread_specific_data_type*	tsd( InitTSD( ParentThread ) );

    if ( tsd && tsd->s_action_queue )
    {
      MutexLock	lock( tsd->s_state_info->m_action_queue_lock );

      tsd->s_action_queue->push_back( new setVarAction( TclInterp,
							Variable ) );
    }
  }
}

void
TIDRequestUnlinkVar( const pthread_t& ParentThread,
		     Tcl_Interp* TclInterp,
		     const std::string& Variable,
		     tid::tcl_state_type* Storage )
{
  if ( ( Variable.length( ) > 0 ) && ( Storage ) )
  {
    thread_specific_data_type*	tsd( InitTSD( ParentThread ) );

    if ( tsd && tsd->s_delete_queue )
    {
      MutexLock	lock( tsd->s_state_info->m_delete_lock );

      tsd->s_delete_queue->push_back( var_type( TclInterp, Variable, Storage ) );
    }
  }
}

//=======================================================================
//                     P R I V A T E   C L A S S E S
//=======================================================================

namespace {
  tid_state_change_events::
  tid_state_change_events( pthread_t Thread )
    : m_controller( Thread ),
      m_syncing( false )
  {
  }

  tid_state_change_events::
  ~tid_state_change_events( )
  {
  }

  bool tid_state_change_events::
  IsSyncing( ) const
  {
    return m_syncing;
  }

  void tid_state_change_events::
  Purge( tid* TID )
  {
    if ( TID )
    {
      TID->AppendDebug( "Purge: begin" );
    }
    if ( m_controller == pthread_self( ) )
    {
      // std::cerr << "DEBUG: Purging: " << (void*)TID << std::endl;
      remove( TID );
      // std::cerr << "DEBUG: " << size() << std::endl;
    }
    else
    {
      // std::cerr << "DEBUG: Purging: (Bad Thread): " << (void*)TID << std::endl;
    }
    if ( TID )
    {
      TID->AppendDebug( "Purge: end" );
    }
  }

  void tid_state_change_events::
  Push( tid* TID )
  {
    if ( TID )
    {
      TID->AppendDebug( "Push: begin" );
    }
    if ( ( m_controller == pthread_self( ) ) &&
	 ( Registry::threadRegistry.isRegistered( TID ) ) )
    {
      // std::cerr << "DEBUG: Pushing: " << (void*)TID << std::endl;
      push_back( TID );
      // std::cerr << "DEBUG: " << size() << std::endl;
    }
    else
    {
      // std::cerr << "DEBUG: Pushing: (Bad Thread): " << (void*)TID << std::endl;
    }
    if ( TID )
    {
      TID->AppendDebug( "Push: end" );
    }
  }

  bool tid_state_change_events::
  NeedTclSync( ) const
  {
    bool	retval( false );

    if ( m_controller == pthread_self( ) )
    {
      isZombie	zombie_op;
      Registry::threadRegistry.for_each( zombie_op );
      for ( std::list< tid* >::const_iterator t( begin() );
	    t != end();
	    t++ )
      {
	if ( Registry::threadRegistry.isRegistered( *t ) )
	{
	  // (void)(*t)->IsZombie( );
	  if ( (*t)->NeedTclSync( ) )
	  {
	    retval = true;
	  }
	}
      }
    }
    return retval;
  }

  void tid_state_change_events::
  TclSync( )
  {
    // std::cerr << "DEBUG: Enter: TclSync" << std::endl;
    if ( ( m_controller == pthread_self( ) ) &&
	 ( ! m_syncing ) )
    {
      m_syncing = true;

      std::list< tid* >	sync_list;
    
      for ( std::list< tid* >::iterator t( begin() );
	    t != end(); )
      {
	if ( ! Registry::threadRegistry.isRegistered( *t ) )
	{
	  t = erase( t );
	  continue;
	}
   
	if ( (*t)->NeedTclSync( ) )
	{
	  sync_list.push_back( *t );
	}
   
        ++t;
      }
      if ( sync_list.size( ) )
      {
#if 0
	std::cerr << "DEBUG: SyncList:";
	for ( std::list< tid* >::iterator t( sync_list.begin() );
	      t != sync_list.end( );
	      t++ )
	{
	  std::cerr << " " << (void*)(*t);
	}
	std::cerr << std::endl;
#endif
	for ( std::list< tid* >::iterator t( sync_list.begin() );
	      t != sync_list.end( );
	      t++ )
	{
	  if ( Registry::threadRegistry.isRegistered( *t )  )
	  {
	    (*t)->TclSync();
	  }
	}
      }
      m_syncing = false;
    }
    // std::cerr << "DEBUG: Exit: TclSync" << std::endl;
  }

} // namespace (anonymous)


//=======================================================================
//                   P R I V A T E   F U N C T I O N S
//=======================================================================

//-----------------------------------------------------------------------
// Forward declaration for TCL routines.
//-----------------------------------------------------------------------

#if OLD
EXTERN VOID *TclThreadDataKeyGet _ANSI_ARGS_((Tcl_ThreadDataKey *keyPtr));
#endif /* OLD */

//-----------------------------------------------------------------------
// Initialize tid
//-----------------------------------------------------------------------

namespace {
  static pthread_mutex_t	thread_data_lock = PTHREAD_MUTEX_INITIALIZER;
  typedef General::unordered_map< pthread_t,
				  thread_specific_data_type*
#if NEED_PTHREAD_T_HASH_FUNC
				  , General::hash<const void *>
#endif /* NEED_PTHREAD_T_HASH_FUNC */
  > thread_data_map_type;

  static thread_data_map_type	thread_data;

  bool pending( thread_specific_data_type* TSD );

  static thread_specific_data_type*
  InitTSD ( pthread_t Thread )
  {
    //-------------------------------------------------------------------
    // Protect the hash map from multiple updates
    //-------------------------------------------------------------------
    MutexLock	lock( thread_data_lock );
    //-------------------------------------------------------------------
    // See if thread is already registered
    //-------------------------------------------------------------------
    thread_data_map_type::const_iterator
      d( thread_data.find( Thread ) );

    if ( d != thread_data.end( ) )
    {
      return d->second;
    }

#if OLD
    thread_specific_data_type*	tsdPtr( (thread_specific_data_type*)
					( TclThreadDataKeyGet( &tsd_key ) ) );
    if ( tsdPtr == (thread_specific_data_type*)NULL )
    {
      // std::cerr << "DEBUG: InitTSD: For Thread: " << (void*)Thread
      // << " In Thread: " << (void*)pthread_self( )
      // << std::endl;
      tsdPtr = (thread_specific_data_type*)
	Tcl_GetThreadData( &tsd_key,
			   sizeof( thread_specific_data_type ) );
      tsdPtr->s_queue = new tid_state_change_events( Thread );
      tsdPtr->s_action_queue = new var_list_type;
      tsdPtr->s_delete_queue = new delete_list_type;
      tsdPtr->s_state_info = new state_info_type( Thread );
      Tcl_CreateEventSource( setup, check, NULL );
      Tcl_CreateThreadExitHandler( ExitProc, NULL );
    }
#else /* OLD */
    thread_specific_data_type*
      tsdPtr = (thread_specific_data_type*)
      Tcl_GetThreadData( &tsd_key,
			 sizeof( thread_specific_data_type ) );
    if ( tsdPtr == (thread_specific_data_type*)NULL )
    {
    }
    else if ( tsdPtr->s_queue == (tid_state_change_events*)NULL )
    {
      tsdPtr->s_queue = new tid_state_change_events( Thread );
      tsdPtr->s_action_queue = new var_list_type;
      tsdPtr->s_delete_queue = new delete_list_type;
      tsdPtr->s_state_info = new state_info_type( Thread );
      Tcl_CreateEventSource( setup, check, NULL );
      Tcl_CreateThreadExitHandler( ExitProc, NULL );
    }
#endif /* OLD */
    thread_data[ Thread ] = tsdPtr;
    return tsdPtr;
  }

  static void
  ExitProc( ClientData )
  {
    // std::cerr << "DEBUG: ExitProc" << std::endl;
    thread_specific_data_type*	tsdPtr( InitTSD( ) );
    
    delete tsdPtr->s_queue;
    tsdPtr->s_queue = (tid_state_change_events*)NULL;
    delete tsdPtr->s_action_queue;
    tsdPtr->s_action_queue = (var_list_type*)NULL;
    delete tsdPtr->s_delete_queue;
    tsdPtr->s_delete_queue = (delete_list_type*)NULL;
    delete tsdPtr->s_state_info;
    tsdPtr->s_state_info = (state_info_type*)NULL;
    Tcl_DeleteEventSource( setup, check, NULL );
  }

  static void
  setup( ClientData, int Flags )
  {
    thread_specific_data_type*	tsdPtr( InitTSD( ) );

    if ( ( ! pending( tsdPtr ) ) || ( tsdPtr->s_queue->IsSyncing( ) ) )
    {
      // There is nothing to be done. Wait quietly until another trace
      //   variable is created.
      return;
    }
    else if ( pending( tsdPtr ) )
    {
      Tcl_Time	timeout;

      timeout.sec = 0;
      timeout.usec = 0;
      Tcl_SetMaxBlockTime( &timeout );
    }
    else
    {
      Tcl_Time	timeout;

      timeout.sec = 1;
      timeout.usec = 0;
      Tcl_SetMaxBlockTime( &timeout );
    }
  }

  static void
  check( ClientData, int Flags )
  {
    thread_specific_data_type*	tsdPtr( InitTSD( ) );
   
    if ( tsdPtr->s_queue->IsSyncing( ) )
    {
      return;
    }
    if ( pending( tsdPtr ) )
    {
      Tcl_Event*	event( (Tcl_Event*)(ckalloc( sizeof( Tcl_Event ) )) );

      event->proc = handler;
      Tcl_QueueEvent( event, TCL_QUEUE_TAIL );
    }
  }

  static  int
  handler( Tcl_Event* Event, int Flags )
  {
    thread_specific_data_type*	tsdPtr( InitTSD( ) );
   
    //-------------------------------------------------------------------
    // Process any requests from tid's to change state
    //-------------------------------------------------------------------
    {
      MutexLock	lock( tsdPtr->s_state_info->m_queue_lock );

      tsdPtr->s_queue->TclSync( );
    }
    //-------------------------------------------------------------------
    // Process requests to Update TCL variables
    //-------------------------------------------------------------------
    {

      while( tsdPtr->s_action_queue->size( ) > 0 )
      {
	MutexLock	lock( tsdPtr->s_state_info->m_action_queue_lock );

	std::auto_ptr< action >	a( tsdPtr->s_action_queue->front( ) );
	tsdPtr->s_action_queue->pop_front( );

	lock.Release( );

	(*a)( );	// Perform requested action
      }
    }
    //-------------------------------------------------------------------
    // Process requests to delete TCL variables
    //-------------------------------------------------------------------
    {
      while ( tsdPtr->s_delete_queue->size( ) > 0 )
      {
	MutexLock lock( tsdPtr->s_state_info->m_delete_lock );

	var_type& var( tsdPtr->s_delete_queue->front( ) );
	Tcl_Interp*	interp( var.s_TclInterp );

	General::AutoArray< char > buf( new char[ var.s_name.length( ) + 1 ] );
	tid::tcl_state_type*	state( var.s_storage );

	var.s_storage = (tid::tcl_state_type*)NULL;

	strcpy( buf.get(), var.s_name.c_str( ) );

	tsdPtr->s_delete_queue->pop_front( );

	lock.Release( );

	Tcl_UnlinkVar( interp, buf.get() );
	delete state;
      }
    }
    //-------------------------------------------------------------------
    // Setup for next round of changes
    //-------------------------------------------------------------------
    setup( NULL, 0 );
    return 1;
  }

  bool
  pending( thread_specific_data_type* TSD )
  { 
    if ( TSD )
    {
      MutexLock	lock( TSD->s_state_info->m_queue_lock );

      return ( ( TSD->s_queue->NeedTclSync( ) ) ||
	       ( TSD->s_action_queue->size( ) > 0 ) ||
	       ( TSD->s_delete_queue->size( ) > 0 ) );
    }
    return false;
  }

} // namespace (anonymous)

#endif /* HAVE_TCL */
