#ifndef GENERIC_API__PROCFS_HH
#define GENERIC_API__PROCFS_HH

#include "genericAPI/config.h"

#if HAVE_SYS_PROCFS
#include <sys/procfs>
#endif

namespace GenericAPI
{
  class ProcFS
  {
  public:
    ProcFS( pid_t ProcId );

    // Process Id for this process
    pid_t GetPid( );
#if WORKING
    //:TODO:
    GetState( );
#endif /* WORKING */
    // User owning the process
    const std::string& GetUser( );
    size_t GetVSZ( );
    // Fetch resident set size in Kbytes
    size_t GetRSZ( );
    // % of recent cpu time used by all lwps
    ushort_t GetPCPU( );
    // Size of process image in Kbytes
    size_t GetPMem( );
#if WORKING
    // usr+sys cpu time for this process
    GetETime( );
#endif /* WORKING */
    // Name of execed file
    const std::string& GetFName( );
    // Initial characters of arg list
    const std::string& GetArgs( );

  private:
    // default constructor is private to prevent it from being called
    ProcFS( );
  };
}

#endif /* GENERIC_API__PROCFS_HH */
