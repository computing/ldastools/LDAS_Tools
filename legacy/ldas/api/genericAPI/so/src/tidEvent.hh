#ifndef GENERIC_API__TID_EVENT_HH
#define	GENERIC_API__TID_EVENT_HH

#include "genericAPI/config.h"

#include <string>

#include "genericAPI/tid.hh"

#if HAVE_TCL
bool TIDForceQuit( tid* TID );
void TIDPurge( tid* TID );
void TIDRequestCallback( const pthread_t& ParentThread,
			 Tcl_Interp* TclInterp,
			 const std::string& Callback );
void TIDRequestSyncState( tid* TID );
void TIDRequestUpdateLinkedVar( const pthread_t& ParentThread,
				Tcl_Interp* TclInterp,
				const std::string& Variable );
void TIDRequestUnlinkVar( const pthread_t& ParentThread,
			  Tcl_Interp* TclInterp,
			  const std::string& Variable,
			  tid::tcl_state_type* Storage );
#endif /* HAVE_TCL */

#endif /* GENERIC_API__TID_EVENT_HH */
