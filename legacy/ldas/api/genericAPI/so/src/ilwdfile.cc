#include "genericAPI/config.h"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwdfile.hh"
#include "general/regexmatch.hh"
#include "ilwd/util.hh"

//-----------------------------------------------------------------------------
//
//: Constructor
//
// Open the file and read the ILWD PI header.
//
//!param: const char* filename - The file to open
//!param: std::ios::openmode - The mode in which to open the file.
//
//!exc: permission_denied - Permission to access the file or a
//+     directory component was denied.</li>
//!exc: file_not_found - The file or a directory component was not
//+     found
//!exc: bad_alloc - Insufficient kernel memory to open the file
//!exc: io_error - An unknown I/O error occurred
//!exc: not_ilwd_file - The ILWD PI header was not found
//
InputILwdFile::InputILwdFile( const char* filename, std::ios::openmode m )
    throw( SwigException )
        : InputFile( filename, m )
{
    if( !ILwd::readHeader( getifstream() ) )
    {
        throw SWIGEXCEPTION( "not_ilwd_file" );
    }
}


//-----------------------------------------------------------------------------
//
//: Constructor
//
// Open the file and write the ILWD PI header (unless the file is open for
// appending).
//
//!param: const char* filename - The file to open
//!param: std::ios::openmode - The mode in which to open the file.
//
//!exc: permission_denied - Permission to access the file or a
//+     directory component was denied.
//!exc: file_not_found - The file or a directory component was not
//+     found
//!exc: bad_alloc - Insufficient kernel memory to open the file
//!exc: io_error - An unknown I/O error occurred
//!exc: file_creation_failed - The file could not be created.
//
OutputILwdFile::OutputILwdFile( const char* filename, std::ios::openmode m )
    throw( SwigException )
        : OutputFile( filename, m )
{
    if ( m == std::ios::out )
    {
        getofstream() << "<?ilwd?>\n";
    }
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */



