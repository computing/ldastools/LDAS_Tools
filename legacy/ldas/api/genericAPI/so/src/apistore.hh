#ifndef GenericApiApiStore
#define GenericApiApiStore

// System includes
#include <vector>


namespace Registry {


// Forward declarations
class ApiProxy;

//-----------------------------------------------------------------------------
//
//: API Proxy Store
//
// This class stores information about all of the api's which have been loaded.
// This is accomplished by storing instances of the ApiProxy class.
//
// The Generic API uses a global instance of this object to obtain information
// about other modules. ApiProxy objects automatically register themselves with
// the global ApiStore when they are constructed.
//
// This class has no constructors (other than the ones the compiler creates).
// This is important because we rely upon the linker initializing the
// private data to zero. It is not guaranteed that the global apiStore object
// will be constructed before the ApiProxy objects (which register themselves
// with this object) are constructed.  When a proxy is registered, the storage
// class first checks to see if the storage has been initialized (i.e. 'mApi'
// is non-null).  If it hasn't, memory is allocated.  We don't want the
// constructor to overwrite this storage if a proxy has already been
// registered. So, no constructor exists.
//
class ApiStore
{
public:
    //: Constructor
    ApiStore( );
    // Register a proxy
    void registerApi( ApiProxy* p ) throw();
             
    // Accessors
    ApiProxy* operator[]( unsigned int index ) throw();
    const ApiProxy* operator[]( unsigned int index ) const throw();
    unsigned int size() const throw();
        
private:
        
    std::vector< ApiProxy* > mApi;
};
    

//-----------------------------------------------------------------------------
//
//: The global ApiStore object
//
extern ApiStore apiStore;


//-----------------------------------------------------------------------------
//
//: Get number of ApiProxy objects
//
// Return the number of ApiProxy objects stored.
//
//!return: unsigned int
//
inline unsigned int ApiStore::size() const throw()
{
    return mApi.size();
}


} // namespace Registry


#endif // GenericApiApiStore
