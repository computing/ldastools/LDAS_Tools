#include "genericAPI/config.h"

// Local Header Files
#include "OSpaceException.hh"
#include "socketcmd.hh"
#include "util.hh"
#include "registry.hh"

using GenericAPI::ErrBindFailure;
using GenericAPI::ErrConnectFailure;

static int linger = 5;

//  createDataSocket
/**
  * Creates a tcp socket bound to the designated address.
  *
  * @param       port int - The port to bind to.  If this is zero a unique port
  *                  is chosen. ( Default: 0 )
  * @param       address const char* - The IP address to bind the socket to.
  *                  If this is an empty string then the address returned
  *                  by the ObjectSpace os_ip_address::my_address() method is
  *                  used.
  *                  ( Default: "" )
  *
  * @return      os_tcp_socket* - A pointer to the newly created socket.
  *
  * @exception   std::bad_alloc - Thrown if a memory allocation fails
  * @exception   bind_failure - Unable to bind a socket to the specified
  *                  address & port.
  * @exception   invalid_address - The address is not a valid IP address.
  * @exception   invalid_host - The host to which the address refers can't
  *                  be found.
  * @exception   unexpected_exception
  */
os_tcp_socket* createDataSocket( int port, const char* address )
    throw( SwigException )
{
    os_tcp_socket* tcpSocket = 0;
    
    try
    {
        os_ip_address ipAddress;

        // If no address is given, use my_address
        if ( *address == '\0' )
        {
            ipAddress = os_ip_address::my_address();
        }
        // Otherwise use the designated address
        else
        {
            // throws invalid_address, invalid_host
            ipAddress = os_ip_address( address );
        }

        // Create a socket address with the designated ip address and port
        os_socket_address socketAddress( ipAddress, port );

        try
        {
            // Create the socket bound to the given address
            // throws system_call_failure, bad//  createDataSocket
            tcpSocket = new os_tcp_socket( socketAddress );
	    tcpSocket->linger_information( true, linger );
        }
        catch( os_toolkit_error e )
        {
            if ( ErrBindFailure::osMatch( e.what() ) )
            {
                throw ERRBINDFAILURE( socketAddress );
            }
            else
            {
                throw;
            }
         }

        tcpSocket->auto_close( true );
        Registry::activeSocketRegistry.registerObject( tcpSocket );
    }
    catch( SwigException& e )
    {
        delete tcpSocket;
        tcpSocket = 0;
        throw;
    }
    catch( os_toolkit_error& e )
    {
        delete tcpSocket;
        tcpSocket = 0;   
        throw OSPACEEXCEPTION( e );
    }
    catch( std::bad_alloc& e )
    {
        delete tcpSocket;
        tcpSocket = 0;   
        throw SWIGEXCEPTION( e );
    }
    catch( ... )
    {
        delete tcpSocket;
        tcpSocket = 0;   
        throw SWIGEXCEPTION( "unexpected_exception" );
    }
    
    return tcpSocket;
}


//  createServerSocket
/**
  * Create a server at the specified address.
  *
  * @param       port int - The port to bind to.  If this is zero a unique port
  *                  is chosen. ( Default: 0 )
  * @param       address const char* - The IP address to bind the server to.
  *                  If this is an empty string ("") then use IPADDRESS_ANY,
  *                  i.e. allow connections from any interfaces.
  *                  ( Default: "" )
  *
  * @return      os_tcp_connection_server* - A pointer to the newly created
  *                  serversocket.
  *
  * @exception   std::bad_alloc - Memory allocation failed
  * @exception   bind_failure - Unable to bind a server to the specified
  *                  address & port.
  * @exception   invalid_address - The address is not a valid IP address.
  * @exception   invalid_host - The host to which the address refers can't
  *                  be found.
  * @exception   unexpected_exception
  */
os_tcp_connection_server* createServerSocket( int port, const char* address )
    throw ( SwigException )
{
    os_tcp_connection_server* tcpServer = 0;
    
    try
    {
        os_ip_address ipAddress(INADDR_ANY);
        
        // If no address is given, then
        if ( *address != '\0' )
        {
            // throws invalid_host, invalid_address
            ipAddress = os_ip_address( address );
        }

        // Create a socket address with the designated IP address and port
        os_socket_address socketAddress( ipAddress, port );

        try
        {
            // Create the server and bind it to the address
            // throws system_call_failure, std::bad_alloc
            tcpServer = new os_tcp_connection_server( socketAddress );
	    tcpServer->linger_information( true, linger );
        }
        catch( os_toolkit_error e )
        {
            if ( ErrBindFailure::osMatch( e.what() ) )
            {
                throw ERRBINDFAILURE( socketAddress );
            }
            else
            {
                throw;
            }
        }

        tcpServer->auto_close( true );
        Registry::serverRegistry.registerObject( tcpServer );
    }
    catch( SwigException& e )
    {
        delete tcpServer;
        tcpServer = 0;   
        throw;
    }
    catch( os_network_toolkit_error e )
    {
        delete tcpServer;
        tcpServer = 0;      
        throw OSPACEEXCEPTION( e );
    }
    catch( std::bad_alloc e )
    {
        delete tcpServer;
        tcpServer = 0;      
        throw SWIGEXCEPTION( e );
    }
    catch( ... )
    {
        delete tcpServer;
        tcpServer = 0;      
        throw SWIGEXCEPTION( "unexpected_exception" );
    }
    
    return tcpServer;
}



//  connectDataSocket
/**
  * Connect a tcp socket to a tcp server at the specified
  *     address.
  *
  * @paramParameters: os_tcp_socket* socket - The socket to connect.
  * @param      const char* address - The IP address of the server
  * @param      int port - The port for the server.
  *
  * @return     void
  *
  * @exception      invalid_socket - The socket doesn't exist
  * @exception      connect_failure - The socket was unable to connect
  *                     to the designated address.
  * @exception      invalid_address - The address is not a valid IP address.
  * @exception      invalid_host - The host to which the address refers can't
  *                     be found.
  * @exception      unexpected_exception
  */
void connectDataSocket(
    os_tcp_socket* socket, const char* address, int port )
    throw ( SwigException )
try
{
    // Make sure the socket is OK
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }

    // throws invalid_address, invalid_host
    os_ip_address ipAddress( address );
        
    os_socket_address socketAddress( ipAddress, port );

    try
    {
        // Connect the socket
        // throws system_call_failure
        socket->connect_to( socketAddress );
    }
    catch( os_toolkit_error e )
    {
        if ( ErrConnectFailure::osMatch( e.what() ) )
        {
            throw ERRCONNECTFAILURE( address, port );
        }
        else
        {
            throw;
        }
    }
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_exception" );
}


//  acceptDataSocket
/**
  * Accepts a tcp connection from the designated address.
  *     If a connection is received by a different address the connection is
  *     closed and an exception is thrown.
  *
  * @param      server os_tcp_connection_server* - The server which is going
  *                 to accept the connection.
  * @param      address const char* - The IP address of the connection to
  *                 accept from.  If this is an empty string, "", then the
  *                 connection can be from any address. ( DEFAULT: "" )
  * @param      port int - The port number of the connection to accept from
  *                 ( this is the port number of the connecting socket on the
  *                 remote machine ).  If this is zero, then any port is
  *                 acceptable. ( DEFAULT: 0 )
  *
  * @return     os_tcp_socket* - The socket representing the connection on the
  *                 server's machine.
  *
  * @exception  invalid_server - The server does not exist
  * @exception  invalid_address - The address is not a valid IP address.
  * @exception  invalid_host - The host to which the address refers can't
  *                     be found.
  * @exception  std::bad_alloc - Memory allocation failed
  * @exception  accept_failure - The unix accept command failed.
  * @exception  identify_failure - The source socket's identity was unable
  *                     to be determined.
  * @exception  illegal_connection - A connection was attempted from an
  *                     unauthorized socket.
  */
os_tcp_socket* acceptDataSocket(
    os_tcp_connection_server* server, const char* address, int port )
    throw ( SwigException )
{
    os_tcp_socket* tcpSocket = 0;  // A pointer to the receiving socket
    os_ip_address ipAddress;       // The ip address to accept from
    bool peerOk = true;            // A flag indicating if the connection
                                   //     is allowed

    // Make sure the server pointer is valid
    if ( !Registry::isServerValid( server ) )
    {
        throw ERRINVALIDSERVER();
    }

    try
    {
        if ( *address != '\0' )
        {
            // Check to make sure the address is OK first so that the
            // procedure will return immediately if it isn't.
            // throws invalid_address, invalid_host
            ipAddress = os_ip_address( address );
        }
        
        // Create a socket
        // throws std::bad_alloc
        tcpSocket = new os_tcp_socket( os_tcp_socket::unbound );
 
        // Listen for a connection
        try
        {
            if ( !server->accept( *tcpSocket ) )
            {
                throw ERRACCEPTFAILURE();
            }
        }
        catch( os_toolkit_error e )
        {
            throw ERRACCEPTFAILURE();
        }

        // Identify the connecting socket
        try
        {
            if ( (*address!='\0') || (port!=0) )
            {
                // Get the peer's socket address
                os_socket_address socketAddress = tcpSocket->peer_address();

                // check the address if the user didn't pass ""
                if ( *address != '\0' )
                {
                    if ( !( ipAddress == socketAddress.ip_address() ))
                    {
                        peerOk = false;
                    }
                }

                // check the port if the user didn't pass 0
                if ( port != 0 )
                {
                    if ( port != socketAddress.port() )
                    {
                        peerOk = false;
                    }
                }
                
                if ( !peerOk )
                {
                    tcpSocket->close();
                    delete tcpSocket;
                    tcpSocket = 0;
                    
                    throw ERRILLEGALCONNECTION( socketAddress );
                }

            }
            
            if ( peerOk )
            {
                tcpSocket->auto_close( true );
                Registry::passiveSocketRegistry.registerObject( tcpSocket );
            }
        }
        catch( os_toolkit_error e )
        {
            throw ERRIDENTIFYFAILURE();
        }
    }
    catch( SwigException& e )
    {
        delete tcpSocket;
        tcpSocket = 0;      
        throw;
    }
    catch( os_network_toolkit_error e )
    {
        delete tcpSocket;
        tcpSocket = 0;      
        throw OSPACEEXCEPTION( e );
    }
    catch( std::bad_alloc& e )
    {
        delete tcpSocket;
        tcpSocket = 0;         
        throw SWIGEXCEPTION( e );
    }
    catch(...)
    {
        delete tcpSocket;
        tcpSocket = 0;         
        throw SWIGEXCEPTION( "unexpected_exception" );
    }

    return tcpSocket;
}


//  getSocketIpAddress
/**
  * Returns a socket's IP address.
  *
  * @param      socket (os_socket*) The socket whose IP address is desired.
  *
  * @return     char* - A 'C' string containing the IP address in
  *                 'ddd.ddd.ddd.ddd' format.  This string resides in memory
  *                 belonging to the 'inet_ntoa' function.
  *
  * @exception      invalid_socket - The socket doesn't exist
  * @exception      unbound_socket - The socket isn't bound to an address
  * @exception      system_call_failure - The 'getsockname' system function
  *                     failed.
  * @exception      unexpected_exception
  */
char* getSocketIpAddress( os_tcp_socket* socket )
    throw( SwigException )
try
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !socket->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }
    
    // throws system_call_failure
    return inet_ntoa( socket->socket_address().ip_address() );
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()


//  getSocketPort
/**
  * Returns a socket's port.
  *
  * @param      socket (os_socket*) The socket whose port is desired.
  *
  * @return     int - The port number
  *
  * @exception      invalid_socket - The socket doesn't exist
  * @exception      unbound_socket - The socket isn't bound to an address
  * @exception      system_call_failure - The 'getsockname' system function
  *                     failed.
  * @exception      unexpected_exception
  */
int getSocketPort( os_tcp_socket* socket )
    throw( SwigException )
    try
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !socket->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }
    
    // throws system_call_failure
    return ( socket->socket_address().port() );
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()


//  getSocketPeerIpAddress
/**
  * Returns the IP address of the remote socket which this socket is
  *     connected to.
  *
  * @param      socket (os_tcp_socket*) The socket whose IP address is desired.
  *
  * @return     char* - A 'C' string containing the IP address in
  *                 'ddd.ddd.ddd.ddd' format.  This string resides in memory
  *                 belonging to the 'inet_ntoa' function.
  *
  * @exception      invalid_socket - The socket doesn't exist
  * @exception      unbound_socket - The socket isn't bound to an address
  * @exception      unconnected_socket - The socket is not connected
  * @exception      system_call_failure - The 'getpeername' system function
  *                     failed.
  * @exception      unexpected_exception
  */
char* getSocketPeerIpAddress( os_tcp_socket* socket )
    throw ( SwigException )
    try
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !socket->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }
    if ( !socket->connected() )
    {
        throw ERRUNCONNECTEDSOCKET();
    }
    
    // throws system_call_failure
    return inet_ntoa( socket->peer_address().ip_address() );
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()



//  getSocketPeerPort
/**
  * Returns the port IP address of the remote socket which
  *     this socket is connected to.
  *
  * @param      socket (os_tcp_socket*) The socket whose IP address is desired.
  *
  * @return     int - The remote socket's port.
  *
  * @exception      invalid_socket - The socket doesn't exist
  * @exception      unbound_socket - The socket isn't bound
  * @exception      unconnected_socket - The socket is not connected
  * @exception      system_call_failure - The 'getpeername' system function
  *                     failed.
  * @exception      unexpected_exception
  */
int getSocketPeerPort( os_tcp_socket* socket )
    throw ( SwigException )
try
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !socket->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }
    if ( !socket->connected() )
    {
        throw ERRUNCONNECTEDSOCKET();
    }
    
    // throws system_call_failure
    return ( socket->peer_address().port() );
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()


//  getServerIpAddress
/**
  * Returns a server's IP address.
  *
  * @param      server (os_tcp_connection_server*) The server whose IP address
  *                 is desired.
  *
  * @return     char* - A 'C' string containing the IP address in
  *                 'ddd.ddd.ddd.ddd' format.  This string resides in memory
  *                 belonging to the 'inet_ntoa' function.
  *
  * @exception      invalid_server - The server doesn't exist
  * @exception      unbound_socket - The server socket isn't bound to an
  *                     address
  * @exception      system_call_failure - The 'getsockname' system function
  *                     failed.
  * @exception      unexpected_exception
  */
char* getServerIpAddress( os_tcp_connection_server* server )
    throw ( SwigException )
try
{
    if ( !Registry::isServerValid( server ) )
    {
        throw ERRINVALIDSERVER();
    }
    if ( !server->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }
    
    // throws system_call_failure
    return inet_ntoa( server->socket_address().ip_address() );
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()



//  getServerPort
/**
  * Returns a server's port.
  *
  * @param      server (os_tcp_connection_server*) The server whose port is
  *                 desired.
  *
  * @return     int - The port number
  *
  * @exception      invalid_server - The server doesn't exist
  * @exception      unbound_socket - The server socket isn't bound to an
  *                     address
  * @exception      system_call_failure - The 'getsockname' system function
  *                     failed.
  * @exception      unexpected_exception
  */
int getServerPort( os_tcp_connection_server* server )
    throw ( SwigException )
    try
{
    if ( !Registry::isServerValid( server ) )
    {
        throw ERRINVALIDSERVER();
    }
    if ( !server->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }
    
    // throws system_call_failure
    return ( server->socket_address().port() );
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()


//  closeDataSocket
/**
  * Closes and destructs a socket.
  *
  * @param      socket (os_tcp_socket*) The socket to close
  *
  * @return     void
  *
  * @exception  invalid_socket
  * @exception  unexpected_exception
  */
void closeDataSocket( os_tcp_socket* socket )
    throw ( SwigException )
    try
{
    if ( Registry::activeSocketRegistry.isRegistered( socket ) )
    {
        // what if it is already closed?
        socket->close();
        Registry::activeSocketRegistry.destructObject( socket );
    }
    else if ( Registry::passiveSocketRegistry.isRegistered( socket ) )
    {
        socket->close();
        Registry::passiveSocketRegistry.destructObject( socket );
    }
    else
    {
        throw ERRINVALIDSOCKET();
    }
}
catch( SwigException& e )
{
    throw;
}
catch( os_network_toolkit_error e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()


//  closeServerSocket
/**
  * Closes and destructs a server.
  *
  * @param      server (os_tcp_connection_server* server) The server to close
  *
  * @return     void
  *
  * @exception  invalid_server
  * @exception  unexpected_exception
  */
void closeServerSocket( os_tcp_connection_server* server )
    throw ( SwigException )
    try
{
    if ( !Registry::isServerValid( server ) )
    {
        throw ERRINVALIDSERVER();
    }
    
    server->close();
    Registry::serverRegistry.destructObject( server );
}
catch( SwigException& e )
{
    throw;
}
catch( os_toolkit_error& e )
{
    throw OSPACEEXCEPTION( e );
}    
CATCHSWIGUNEXPECTED()


/**
 * True if the socket is connected.
 *
 * @param socket (os_tcp_socket*)
 *
 * @return bool - True if the socket is connected.
 *
 * @exception invalid_socket
 * @exception unbound_socket
 * @exception unexpected_exception
 */
bool isSocketConnected( os_tcp_socket* socket )
try
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !socket->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }

    return ( socket->connected() );
}
CATCHSWIGUNEXPECTED()

/**
 * True if data is available on the socket.
 *
 * @param socket (os_tcp_socket*)
 * @param int timeout
 *
 * @return bool - True if data is available on the socket
 *
 * @exception invalid_socket
 * @exception unbound_socket
 * @exception unexpected_exception
 */
bool pollDataSocket( os_tcp_socket* socket, int timeout)
  throw ( SwigException )
  try
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !socket->is_open() )
    {
        throw ERRUNBOUNDSOCKET();
    }

    return ( socket->poll( timeout ) );
}
CATCHSWIGUNEXPECTED()

/**
 * Set number of seconds to allow socket to linger before closing
 *
 * @param Linger - Number of seconds to linger
 */
void setLinger( int Linger )
{
  linger = Linger;
}

CREATE_THREADED3( acceptDataSocket, os_tcp_socket*, os_tcp_connection_server*,
                  const char*, int )
