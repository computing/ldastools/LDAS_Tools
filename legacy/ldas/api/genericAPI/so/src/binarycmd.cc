#include "genericAPI/config.h"

#include "general/undef_ac.h"

#include "ospace/rtti/class.h"
#include "ospace/stream/bstream.h"
#include "ospace/socket/socket.h"

#include "binarycmd.hh"
#include "util.hh"
#include "thread.hh"
#include "registry.hh"

/**
 * Sends an LdasBinary object across a socket.
 *
 * @param socket (os_tcp_socket*)
 * @param element (LdasBinary*)
 *
 * @return void
 *
 * @exception invalid_socket
 * @exception invalid_binary
 * @exception unexpected_exception
 */
void sendRawBinary( os_tcp_socket* socket, LdasBinary* element )
    throw( SwigException )
try
{
    if ( !Registry::binaryRegistry.isRegistered( element ) )
    {
        throw SWIGEXCEPTION( "invalid_binary" );
    }
    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !(socket->connected() ) )
    {
        throw SWIGEXCEPTION( "unconnected_socket" );
    }

    os_bstream stream( *socket );
    stream << element;
}
catch( os_network_toolkit_error& e )
{
    throw OSPACEEXCEPTION( e );
}
catch( LdasException& e )
{
    throw SwigException( e );
}
catch( SwigException& e )
{
    throw;
}
CATCHSWIGUNEXPECTED()


/**
 * Receive a binary object through a socket.
 *
 * @param socket (os_tcp_socket*)
 *
 * @return LdasBinary*
 *
 * @exception invalid_socket
 * @exception bad_alloc
 * @exception unexpected_exception
 */
LdasBinary*
recvRawBinary( os_tcp_socket* socket )
    throw( SwigException )
{
    LdasBinary* binary = 0;
    
    try
    {
        if ( !Registry::isSocketValid( socket ) )
        {
            throw ERRINVALIDSOCKET();
        }
        if ( !(socket->connected() ) )
        {
            throw SWIGEXCEPTION( "unconnected_socket" );
        }
        
        os_bstream stream( *socket );
        stream >> binary;
        Registry::binaryRegistry.registerObject( binary );
        return binary;
    }
    catch( SwigException& e )
    {
        delete binary;
        binary = 0;
        throw;
    }
    catch( os_network_toolkit_error& e )
    {
        delete binary;
        binary = 0;   
        throw OSPACEEXCEPTION( e );
    }
    catch( LdasException& e )
    {
        delete binary;
        binary = 0;   
        throw SwigException( e );
    }
    catch( ... )
    {
        delete binary;
        binary = 0;   
        throw SWIGEXCEPTION( "unexpected_exception" );
    }
}


/**
 * Returns raw binary from the C++ layer to the Tcl layer.
 *
 * The SWIG program converts the return value from this function call
 * into a TCL binary string.
 *
 * @param binary (LdasBinary*) A RawBinary object in the C++ layer.
 * @param size (unsigned int&) This parameter will be updated with the number
 *     of bytes used by the binary data.
 *
 * @exception invalid_binary - The pointer was not pointing to a valid raw
 *     binary object.
 */
LdasBinary* getRawBinary(
    LdasBinary* binary, unsigned int& size )
    throw( SwigException )
    try
{
    if ( !Registry::binaryRegistry.isRegistered( binary ) )
    {
        throw SWIGEXCEPTION( "invalid_binary" );
    }

    size = binary->getBytes();
    return binary;
}
catch( SwigException& )
{
    throw;
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_error" );
}


/**
 * Instantiates a raw binary object in the C++ layer.
 *
 * WARNING! If an incorrect value for the 'bytes' parameter is passed, the
 * program may abort with a segmentation fault.
 *
 * @param s (char*) A pointer to a sequence of bytes.  This is typically a
 *     binary string from the TCL layer.
 * @param bytes (unsigned int) The number of bytes encompassing the binary
 *     data.  Warning! If this is incorrect a segmentation fault may occur.
 *
 * @return LdasBinary* A pointer to the binary object.
 */
LdasBinary* putRawBinary(
    char* s, unsigned int bytes )
    throw( SwigException )
{
    LdasBinary* binary = new LdasBinary( s, bytes );
    Registry::binaryRegistry.registerObject( binary );
    return binary;
}


/**
 * Destructs a raw binary object.
 *
 * @param binary (LdasBinary*) A pointer to the raw binary object to destruct.
 *
 * @exception invalid_binary - The pointer does not point to a valid raw
 *     binary object.
 */
void destructBinary( LdasBinary* binary )
    throw( SwigException )
{
    if ( !Registry::binaryRegistry.isRegistered( binary ) )
    {
        throw SWIGEXCEPTION( "invalid_binary" );
    }

    Registry::binaryRegistry.destructObject( binary );
}


CREATE_THREADED2V( sendRawBinary, os_tcp_socket*, LdasBinary* )
CREATE_THREADED1( recvRawBinary, LdasBinary*, os_tcp_socket* )
