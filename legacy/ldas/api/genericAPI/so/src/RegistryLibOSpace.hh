/* -*- c-basic-offset: 4; -*- */

#ifndef RegistryLibOSpaceHH
#define RegistryLibOSpaceHH

#if HAVE_LIBOSPACE

#include <ospace/network.h>

#include <general/objectregistry.hh>

namespace Registry
{    
    class ServerRegistry_type
	: public ObjectRegistry< os_tcp_connection_server >
    {
    };

    extern ServerRegistry_type serverRegistry;
    extern ObjectRegistry< os_tcp_socket > activeSocketRegistry;
    extern ObjectRegistry< os_tcp_socket > passiveSocketRegistry;

    bool isServerValid( const os_tcp_connection_server* server ) throw();
    bool isSocketValid( const os_tcp_socket* socket ) throw();
}

#endif /* HAVE_LIBOSPACE */

#endif /* RegistryLibOSpaceHH */

