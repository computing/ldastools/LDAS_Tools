/* -*- c-basic-offset: 4; -*- */

#ifndef SocketCmdHH
#define SocketCmdHH


// LDAS Includes
#include "swigexception.hh"


// ObjectSpace Includes
#include "general/undef_ac.h"
#include "ospace/rtti/class.h"
#include "ospace/network.h"
#include "ospace/stream.h"

#include "tid.hh"
#include "thread.hh"

extern "C"
{
    os_tcp_socket* createDataSocket(
        int port=0, const char* address="" )
        throw ( SwigException );
    os_tcp_connection_server* createServerSocket(
        int port=0, const char* address="" )
        throw( SwigException );
    void connectDataSocket(
        os_tcp_socket* socket, const char* address, int port )
        throw( SwigException );
    os_tcp_socket* acceptDataSocket(
        os_tcp_connection_server* server, const char* address="", int port=0 )
        throw( SwigException );
    CREATE_THREADED3_DECL( acceptDataSocket, os_tcp_socket*,
                           os_tcp_connection_server*, const char*, int );
    bool isSocketConnected( os_tcp_socket* );

    void closeDataSocket( os_tcp_socket* socket )
        throw( SwigException );
    void closeServerSocket( os_tcp_connection_server* server )
        throw( SwigException );
    char* getSocketIpAddress( os_tcp_socket* socket )
        throw( SwigException );
    int getSocketPort( os_tcp_socket* socketId )
        throw( SwigException );
    char* getSocketPeerIpAddress( os_tcp_socket* socket )
        throw( SwigException );
    int getSocketPeerPort( os_tcp_socket* socket )
        throw( SwigException );
    char* getServerIpAddress( os_tcp_connection_server* server )
        throw( SwigException );
    int getServerPort( os_tcp_connection_server* server )
        throw( SwigException );
    bool pollDataSocket( os_tcp_socket* socket, int timeout)
        throw( SwigException );
    void setLinger( int Linger );
}


#endif // SocketCmdHH
