#include "genericAPI/config.h"

#include "general/undef_ac.h"

#include "genericAPI/RegistryILwd.hh"

#include "util.hh"

namespace Registry
{
#if HAVE_LDAS_PACKAGE_ILWD || 1
  //---------------------------------------------------------------------
  //
  //: Element Registry
  //
  ElementRegistry_type	elementRegistry;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
  //-----------------------------------------------------------------------------
  //
  //: Check an Element Pointer
  //
  // True if the element is valid.
  //
  //!param: LdasElement* element
  //
  //!return: bool - True if the element is valid
  //
  bool isElementValid( const ILwd::LdasElement* element ) throw()
  {
    return ( elementRegistry.isRegistered( element ) ||
             elementRegistry.contentsSearch( element ) );
  }
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
bool ElementRegistry_type::
contentsSearch( const ILwd::LdasElement* Element ) const
{
    General::ReadWriteLock m( m_lock,
			      General::ReadWriteLock::READ );

    bool found = false;
    
    for( const_iterator iter = begin(),
	     last = end( );
	 ( iter != last ) && ( !found );
	 ++iter )
    {
        if ( iter->first->getElementId() == ILwd::ID_ILWD )
        {
            found =
		::contentsSearch( dynamic_cast< ILwd::LdasContainer& >
				  ( *(iter->first) ),
				  Element );
        }
    }

    return found;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */
}

#if HAVE_LDAS_PACKAGE_ILWD
template class ObjectRegistry< ILwd::LdasElement >;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
