/* -*- c-basic-offset: 4; -*- */

#ifndef RegistryILWDHH
#define RegistryILWDHH

#if HAVE_LDAS_PACKAGE_ILWD

#include <general/objectregistry.hh>

#include "ilwd/ldaselement.hh"

namespace Registry
{
    class ElementRegistry_type
	: public ObjectRegistry< ILwd::LdasElement >
    {
    public:

	bool contentsSearch( const ILwd::LdasElement* Element ) const;
	std::string getElementList( ) const;

    };

    extern ElementRegistry_type elementRegistry;
    bool isElementValid( const ILwd::LdasElement* element ) throw();
}

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif /* RegistryILWDHH */
