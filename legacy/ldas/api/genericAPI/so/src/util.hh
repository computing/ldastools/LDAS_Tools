#ifndef UtilHH
#define UtilHH

#include "config.h"

#include <general/objectregistry.hh>

#if HAVE_LDAS_PACKAGE_ILWD
#include <ilwd/ldaselement.hh>
#include <ilwd/ldasstring.hh>
#include <ilwd/ldasexternal.hh>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarraybase.hh>
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/UtilILwd.hh"
#include "genericAPI/UtilTCL.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwdfile.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
extern ObjectRegistry< ILwdFile >		ilwdFileRegistry;
bool isILwdFileValid( const ILwdFile* file );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
// Swig pointer manipulation
std::string createElementPointer( const ILwd::LdasElement* element );

// Enum <-> String conversion
ILwd::Format char2Format( const char* f );
std::string format2String( ILwd::Format f );
ILwd::Compression char2Compression( const char* c );
std::string compression2String( ILwd::Compression c );
std::string arrayOrder2String( ILwd::ArrayOrder a );
std::string byteOrder2String( ILwd::ByteOrder b );


// Element Attribute queries
std::string getLdasArrayBaseAttribute(
    ILwd::LdasArrayBase* array, const char* att );
std::string getLdasContainerAttribute(
    ILwd::LdasContainer* container, const char* att );
std::string getLdasStringAttribute(
    ILwd::LdasString* s, const char* att );
std::string getLdasExternalAttribute(
    ILwd::LdasExternal* ext, const char* att );

#endif /* HAVE_LDAS_PACKAGE_ILWD */


#endif // UtilHH
