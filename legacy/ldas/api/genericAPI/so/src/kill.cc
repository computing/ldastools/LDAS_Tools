/* -*- mode: c++ ; c-basic-offset: 2; -*- */

#include "genericAPI/config.h"

#include <errno.h>
#include <signal.h>

#include <list>
#include <sstream>
#include <stdexcept>

#include "general/AtExit.hh"
#include "general/unordered_map.hh"
#include "general/mutexlock.hh"
#include "general/util.hh"

#include "genericAPI/kill.hh"

#define signal_map (*signal_map_storage)
namespace
{
  typedef General::unordered_map< std::string, int,
				  General::ic_hash< std::string > >
  signal_mapping_type;

  static signal_mapping_type&	signal_mapping_init( );

  static signal_mapping_type&	signal_mapping = signal_mapping_init( );
}

namespace GenericAPI
{
  std::string
  kill( std::string Sig, int PID )
  {
    static bool initialized = false;
    static std::string signal_list;
    if ( initialized == false )
    {
      static MutexLock::lock_type	key = MutexLock::Initialize( );
      MutexLock	lock( key );

      if ( initialized == false )
      {
	if ( signal_mapping.size( ) == 0 )
	{
	  signal_mapping_init( );
	}

	std::list< std::string >	s;

	for ( signal_mapping_type::const_iterator
		first = signal_mapping.begin( ),
		last = signal_mapping.end( ),
		cur = first;
	      cur != last;
	      ++cur )
	{
	  std::ostringstream	t;
	  t << cur->second;
	  if ( cur->first != t.str( ) )
	  {
	    s.push_back( cur->first );
	  }
	}
	s.sort( );
	for ( std::list< std::string >::const_iterator
		first = s.begin( ),
		last = s.end( ),
		cur = first;
	      cur != last;
	      ++cur )
	{
	  if ( cur != first )
	  {
	    signal_list += " ";
	  }
	  signal_list += *cur;
	}
	initialized = true;
      }
    }
    signal_mapping_type::const_iterator	c;

    if ( Sig[ 0 ] == '-' )
    {
      Sig.erase( 0, 1 );
    }
    if ( Sig == "l" )
    {
      return signal_list;
    }
    c = signal_mapping.find( Sig );

    if ( c == signal_mapping.end( ) )
    {
      std::ostringstream	msg;
      msg << "Invalid signal: " << Sig;
      throw std::runtime_error( msg.str( ) );
    }
    int rc = ::kill( PID, c->second );
    if ( rc != 0 )
    {
      std::ostringstream	msg;
      char			error_buf[ 1024 ];

      strerror_r( errno, error_buf, sizeof( error_buf ) );
      msg << "System error when running \"kill -" << Sig << " " << PID
	  << " -- " << error_buf;

      throw std::runtime_error( msg.str( ) );
    }
    return "";
  }
}

namespace
{
  static signal_mapping_type*	signal_map_storage =
    (signal_mapping_type*)NULL;

  static void cleanup_signal_map( )
  {
    delete signal_map_storage;
  }

  inline void
  add_signal( std::string SigName, int SigNumber )
  {
    std::ostringstream	msg;

    msg << SigNumber;
    signal_map[ msg.str( ) ] = SigNumber;
    signal_map[ SigName ] = SigNumber;
    SigName.erase( 0, 3 );
    signal_map[ SigName ] = SigNumber;
  }

  static signal_mapping_type&
  signal_mapping_init( )
  {

    if ( signal_map_storage == (signal_mapping_type*)NULL )
    {
      signal_map_storage = new signal_mapping_type( );
      General::AtExit::Append( cleanup_signal_map,
			       "cleanup_signal_map",
			       0 );
    }

    // Special signal for testing if a PID is alive
    signal_map[ "0" ] = 0;

    // Traditional signals
#define ADD_SIGNAL( SIG ) add_signal( #SIG, SIG )

    ADD_SIGNAL( SIGHUP );
    ADD_SIGNAL( SIGINT );
    ADD_SIGNAL( SIGQUIT );
    ADD_SIGNAL( SIGILL );
    ADD_SIGNAL( SIGTRAP );
    ADD_SIGNAL( SIGABRT );
    ADD_SIGNAL( SIGBUS );
    ADD_SIGNAL( SIGFPE );
    ADD_SIGNAL( SIGKILL );
    ADD_SIGNAL( SIGUSR1 );
    ADD_SIGNAL( SIGSEGV );
    ADD_SIGNAL( SIGUSR2 );
    ADD_SIGNAL( SIGPIPE );
    ADD_SIGNAL( SIGALRM );
    ADD_SIGNAL( SIGTERM );
#ifdef SIGSTKFLT
    ADD_SIGNAL( SIGSTKFLT );
#endif /* SIGSTKFLT */
    ADD_SIGNAL( SIGCHLD );
    ADD_SIGNAL( SIGCONT );
    ADD_SIGNAL( SIGSTOP );
    ADD_SIGNAL( SIGTSTP );
    ADD_SIGNAL( SIGTTIN );
    ADD_SIGNAL( SIGTTOU );
    ADD_SIGNAL( SIGURG );
    ADD_SIGNAL( SIGXCPU );
    ADD_SIGNAL( SIGXFSZ );
    ADD_SIGNAL( SIGVTALRM );
    ADD_SIGNAL( SIGPROF );
    ADD_SIGNAL( SIGWINCH );
#ifdef SIGPOLL
    ADD_SIGNAL( SIGPOLL );
#endif /* SIGPOLL */
#ifdef SIGPWR
    ADD_SIGNAL( SIGPWR );
#endif /* SIGPWR */
    ADD_SIGNAL( SIGSYS );
#ifdef SIGRTMIN
    ADD_SIGNAL( SIGRTMIN );
    ADD_SIGNAL( SIGRTMIN+1 );
    ADD_SIGNAL( SIGRTMIN+2 );
    ADD_SIGNAL( SIGRTMIN+3 );
#endif /* SIGRTMIN */
#ifdef SIGRTMAX
    ADD_SIGNAL( SIGRTMAX-3 );
    ADD_SIGNAL( SIGRTMAX-2 );
    ADD_SIGNAL( SIGRTMAX-1 );
    ADD_SIGNAL( SIGRTMAX );
#endif /* SIGRTMAX */

#undef ADD_SIGNAL

    return signal_map;
  }
}
