// -*- Mode: C++; c-basic-offset: 4; -*-

//! commandtype="Utility"

#if defined(SWIGTCL)
%{
#include "genericAPI/tid.hh"

    extern bool TIDForceQuit( tid* TID );

    namespace {
	//---------------------------------------------------------------
	// Routine to register trace variables.
	//---------------------------------------------------------------
	void setAlert_call( tid* Thread,
			    const char* Variable,
			    Tcl_Interp* Interp )
	{
	    Thread->setAlert( Interp, Variable );
	}

	void forceQuit( tid* Thread )
	{
	    TIDForceQuit( Thread );
	}

	void setTIDCallback_call( tid* Thread,
				  const char* Command,
				  Tcl_Interp* Interp )
	{
	    Thread->setCallback( Interp, Command );
	}

	//---------------------------------------------------------------
	// Routines to help trouble shoot problems
	//---------------------------------------------------------------
	std::string getTIDDebugInfo( tid* Thread )
	{
	    return Thread->DebugInfo( );
	}

	void setTIDDebugging( bool Mode )
	{
	    tid::DebugDefault( Mode );
	}
    } // namespace - anonymous

#define	setAlert( a, b ) setAlert_call( a, b, interp );
#define	setTIDCallback( a, b ) setTIDCallback_call( a, b, interp );
%}

//-----------------------------------------------------------------------
// enumerated types
//-----------------------------------------------------------------------

%inline %{
    enum {
	TID_FINISHED = tid::FINISHED,
	TID_RUNNING = tid::RUNNING,
	TID_IDLE = tid::IDLE,
	TID_CANCELED = tid::CANCELED,
	TID_JOINED = tid::JOINED
    };
%}

//-----------------------------------------------------------------------
// Function library
//-----------------------------------------------------------------------

string getTIDDebugInfo( tid* Thread );
void setAlert( tid* Thread, const char* Variable );
void setTIDCallback( tid* Thread, const char* Command );
void forceQuit( tid* Thread );

//-----------------------------------------------------------------------
//: Controlls debugging of the TID class
//
// This command enables or disables the debugging of the TID class.
// It is recommended to have TID class debugging only be enabled when
// diagnosing a problem with the thread class.
//
//!usage: setTIDDebugging mode
//
//!param: bool mode - a boolean value. True to enable TID class debugging;
//+	              false otherwise.
//

void setTIDDebugging( bool Mode )
{
    tid::DebugDefault( Mode );
}
	
#endif /* defined(SWIGTCL) */
