#ifndef GenericCmdHH
#define GenericCmdHH

#include "genericAPI/config.h"

// LDAS Includes
#include "swigexception.hh"
#include "tid.hh"
#include "thread.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwdfile.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */


void setBase64CharacterPerLineLimit( const int );
char* base64encode(char* in);
char* base64decode(char* in);

// OpenSLL strong encryption
char* decrypt(char* in, char* key);
char* encrypt(char* in, char* key);

// UNIX system calls
int UNIXfork();
int UNIXwaitpid(int);

// Save / Restore
void save( const char* filename );
std::string restore( const char* filename );    

#if HAVE_LDAS_PACKAGE_ILWD
std::string getElementList();
void destroyElements();
#endif /* HAVE_LDAS_PACKAGE_ILWD */
std::string getRegistrySize(void);

// Reset
void resetApi() throw( SwigException );

// Thread Manipulation
void setThreadStackSize( unsigned int ssize );
std::string getThreadStatus( tid* t );
std::string getThreadFunction( const tid* t );   
void cancelThread( tid* t );
std::string getThreadList();


#if HAVE_LDAS_PACKAGE_ILWD
ILwdFile* openILwdFile( const char* filename, const char* mode );
void closeILwdFile( ILwdFile* f );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

void resetGenericApi() throw( SwigException );
void unlimitCoreSize( );


#endif // GenericCmdHH
