#ifndef GENERIC_API__TCL_SYMBOL_TABLE_HH
#define GENERIC_API__TCL_SYMBOL_TABLE_HH

#include <tcl.h>

#include <string>

#include "general/gpstime.hh"

namespace GenericAPI
{
  //---------------------------------------------------------------------
  // \brief Expose TCL variables to C++ in a thread safe manor.
  //---------------------------------------------------------------------
  class TCLSymbolTable
  {
  public:
    typedef General::GPSTime timestamp_type;
    typedef std::string symbol_type;
    typedef std::string string_type;
    typedef double number_type;

    static bool Changed( const symbol_type& SymbolName,
			 const timestamp_type& TimeStamp );

    static bool IsDefined( const symbol_type& SymbolName );

    static void Lookup( const symbol_type& SymbolName,
			string_type& Value,
			timestamp_type* TimeStamp = (timestamp_type*)NULL );

    static void Lookup( const symbol_type& SymbolName,
			number_type& Value,
			timestamp_type* TimeStamp = (timestamp_type*)NULL );

    static void Set( const symbol_type& SymbolName,
		     const string_type& Value );
    static void Set( const symbol_type& SymbolName,
		     const number_type Value );

    static void Unset( const symbol_type& SymbolName );

    static void Watch( Tcl_Interp* Interp,
		       const char* Name1,
		       const char* Name2 );
    //-------------------------------------------------------------------
    // Name of Tcl variable that contains the LDAS archive directory
    //-------------------------------------------------------------------
    static const char* LDAS_ARC;

    //-------------------------------------------------------------------
    // Name of Tcl variable that contains the LDAS API name
    //-------------------------------------------------------------------
    static const char* LDAS_API;

    //-------------------------------------------------------------------
    /// Name of Tcl variable that contains the LDAS log directory.
    //-------------------------------------------------------------------
    static const char* LDAS_LOG_DIR;

  protected:
    static char* changeCallback( ClientData CD,
				 Tcl_Interp* Interp,
				 CONST84 char* Name1,
				 CONST84 char* Name2,
				 int Flags );


  };
}

#endif /* GENERIC_API__TCL_SYMBOL_TABLE_HH */
