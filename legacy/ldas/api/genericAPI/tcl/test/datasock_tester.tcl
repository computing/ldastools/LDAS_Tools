#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

## ********************************************************
## datasock_tester.tcl
##
## Exercise data socket for various ilwd types
##
## Release Date: Not Available
##
##  Test options
##  dims,dims,dims... or all
##  char_s,char_u,... or all
##  with-container or default to no-container
##  server - the one to send data to
##  localhost - myself
## ********************************************************

## ********************************************************
##
## Name: operator
##
## Description:
## Handles requests at the metadata API's operator socket.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket.
##
## Comments:
## This is the only entry point exposed on the operator
## socket.
## See the asstMgr.tcl procedure ${name}::sockhandler
## for the format of the reply and the significance of the
## return values.
## The variable ::metadata::errlvl may be modified by a called
## procedure generating an error condition.

proc operator { cid } {
     set errlvl 0
     set result "done!"
     set cmd [ cmd::result $cid ]
     regexp {^\{(.*)\}$} $cmd -> cmd
     regexp {^\"(.*)\"$} $cmd -> cmd
     puts "cmd=$cmd"
     if {[catch { set result [eval $cmd] } errmsg ]} {
        set result $errmsg
     }
     catch { puts $cid "$errlvl\n$result" }
     catch { close $cid }
}
## ********************************************************

## ********************************************************
##
## Name: emergency
##
## Description:
## Provides the functions required to handle requests of
## the frame API's emergency socket.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket.
##
## Comments:
## This is the only entry point exposed on the emergency
## socket.

proc emergency { { cid "" } } {
    set cmd [ cmd::result $cid ]
    regexp {^\{(.*)\}$} $cmd -> cmd
    regexp {^\"(.*)\"$} $cmd -> cmd
    if {[catch {set result [eval $cmd]} err]} {
        puts "emergency socket eval err: $err"
    }
    catch { close $cid }
    puts "result=$result"
}
## ********************************************************

## ********************************************************
##
## Name: bgerror
##
## Description:
## Any Tcl script which has an event loop must have a
## bgerror procedure defined.  Tk defines a bgerror,
## and you should too!
##
## Usage:
## Called internally by Tcl.  See the bgerror man page.
## Comments:
## Should it do anything else??

proc bgerror { msg } {
    set trace {}
    puts stderr "Error: $msg"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
    }

    return
}
## ********************************************************

proc testExit {} {
    puts "in testExit"
    if {[catch {
            sock::closeDataServer
            closeListenSock operator
            closeListenSock emergency
        } err]} {
        puts "testExit error: $err"
    }
    exit
}


;#barecode

set auto_path "/ldas/lib $auto_path"
puts "auto_path=$auto_path"

set API [ lindex $::argv 0 ]
puts "API=$API"

source LDASapi.rsc
source datasock_test.tcl
package require generic
;##package require datasock_test
;##source datasock_thread.tcl

;## suppress logging
proc addLogEntry { args } { return {} }

set ::localhost [lindex [split [info hostname] "."] 0]
if {[string equal -length 4 "ldas" $::localhost]} {
    set ::localhost "gateway"
}
puts "localhost=$::localhost"

set ::port [ set ${API}(data) ]
puts "port=$::port"
puts "[array get $::API]"

openListenSock operator
openListenSock emergency
set ::serverp {}

;## make ssh happy
catch {close stdin}

vwait enter-mainloop

after 1000
catch {close $::serverp}

;##end

