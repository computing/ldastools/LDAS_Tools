#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

set ::Dims {
    1
    10
    100
    1000
    10000
    100000
    1000000
}

set ::Types {
    char_s
    char_u
    int_2s
    int_2u
    int_4s
    int_4u
    int_8s
    int_8u
    real_4
    real_8
    lstring_2
    lstring_4
    lstring_8
    lstring_16
    complex_8
    complex_16
}

proc main {} {

    set outfile {}
    set overall 0

    ;## Get command line arguments
    set idx [lsearch -exact $::argv "-"]
    if {$idx >= 0} {
        set outfile stdout
        set ::argv [lreplace $::argv $idx $idx]
    }
    set idx [lsearch -exact $::argv "-o"]
    if {$idx >= 0} {
        set overall 1
        set overallfile "overall.stats"
        set ::argv [lreplace $::argv $idx $idx]
        #set overallfile [lindex $::argv [expr {$idx + 1}]]
        #set ::argv [lreplace $::argv $idx [expr {$idx+1}]]
    }
    set ::argc [llength $::argv]

    if {$::argc <= 1} {
        puts stderr "Error: Missing filename for comparison"
        return
    }

    if {[file isdirectory [lindex $::argv end]]} {
        set dir [lindex $::argv end]
    } else {
        set dir {}
        if {$::argc > 2} {
            puts stderr "Error: Last argument must be a directory when comparing multiple files"
            return
        }
    }

    if  { [ catch {
    foreach newfile [lrange $::argv 0 end-1] {
        if {[string length $dir]} {
            set oldfile [file join $dir $newfile]
        } else {
            set oldfile [lindex $::argv end]
        }

        if {![string match stdout $outfile]} {
            set outfile [file rootname $newfile].stats
        }
        set oldfile [ substFile $oldfile ]
        puts "oldfile $oldfile"
        
        puts stderr "Processing $newfile"
        set title [collect $newfile $oldfile]
        calculate
        print $newfile $oldfile $outfile $title

        ;## Accumulate individual data for overall statistics
        if {$overall} {
            foreach type {rate wall cpu} {
                foreach age {new old} {
                    foreach dtype $::Types {
                        eval lappend ::rawG($type,$age,$dtype) $::raw($type,$age,$dtype)
                    }
                    foreach dim $::Dims {
                        eval lappend ::rawG($type,$age,$dim) $::raw($type,$age,$dim)
                    }
                    eval lappend ::rawG($type,$age,all) $::raw($type,$age,all)
                }
            }
        }
    }

    if {$overall} {
        puts stderr "\nCalculating overall statistics..."
        array unset ::raw
        array set ::raw [array get ::rawG]
        calculate
        print "" "" $overallfile "data socket test statistics overall"
    }
    } err ] } {
        return -code error $err
    }
    return
}

proc collect {new old} {

    if  { [ catch {
    set old [ substFile $old ]
    set new [ substFile $new ]
    
    if {[file tail $new] != [file tail $old]} {
        ;## Warning
        puts stderr "Warning: $old and $new do not have the same name."
    }

    array unset ::raw

    ;## Get data from files
    foreach age {new old} {
        set fd [open [set $age] r]
        set data [split [read $fd] "\n"]
        close $fd

        foreach line $data {
            if {[regexp -nocase {Data Socket Element Test (\S+)->(\S+) (without|with) container (non-threaded|threaded) (\d+) samples} $line -> client server withcont thread samples]} {
               set title "data socket test statistics ${client}<->${server}, $withcont container, $thread, $samples samples"
            }
            if { [ regexp {(char_[us]|int_[248us]+|real_[48]|lstring_[12468]+|complex_[168]+)} $line] } {
                scan $line "%s %d %s %s %s %s %s %s %s" dtype dim rate wmean wsd wcov cmean csd ccov
                lappend ::raw(rate,$age,$dtype) $rate
                lappend ::raw(rate,$age,$dim) $rate
                lappend ::raw(rate,$age,all) $rate
                lappend ::raw(wall,$age,$dtype) $wmean
                lappend ::raw(wall,$age,$dim) $wmean
                lappend ::raw(wall,$age,all) $wmean
                lappend ::raw(cpu,$age,$dtype) $cmean
                lappend ::raw(cpu,$age,$dim) $cmean
                lappend ::raw(cpu,$age,all) $cmean
            }
        }
    }
    } err ] } {
        puts stderr $err
        return -code error $err
    }
    return $title
}

proc calculate {args} {
    array unset ::delta
    array unset ::mean

    ;## calculate mean
    foreach type {rate wall cpu} {
        foreach age {new old} {
            foreach dtype $::Types {
                set ::mean($type,$age,$dtype) [mean $::raw($type,$age,$dtype)]
            }

            foreach dim $::Dims {
                set ::mean($type,$age,$dim) [mean $::raw($type,$age,$dim)]
            }

            set ::mean($type,$age,all) [mean $::raw($type,$age,all)]
        }
    }

    ;## calculate differences
    foreach type {rate wall cpu} {
        foreach dtype $::Types {
            set newmean $::mean($type,new,$dtype)
            set oldmean $::mean($type,old,$dtype)

            if {$newmean == 0 || $oldmean == 0} {
                set ::delta($type,$dtype) NA
                continue
            }

            if {[string equal "rate" $type]} {
                set ::delta($type,$dtype) [expr {($newmean - $oldmean) / $oldmean}]
            } else {
                set ::delta($type,$dtype) [expr {($oldmean - $newmean) / $newmean}]
            }
        }

        foreach dim $::Dims {
            set newmean $::mean($type,new,$dim)
            set oldmean $::mean($type,old,$dim)

            if {$newmean == 0 || $oldmean == 0} {
                set ::delta($type,$dim) NA
                continue
            }

            if {[string equal "rate" $type]} {
                set ::delta($type,$dim) [expr {($newmean - $oldmean) / $oldmean}]
            } else {
                set ::delta($type,$dim) [expr {($oldmean - $newmean) / $newmean}]
            }
        }

        set newmean $::mean($type,new,all)
        set oldmean $::mean($type,old,all)

        if {$newmean == 0 || $oldmean == 0} {
            set ::delta($type,all) NA
            continue
        }

        if {[string equal "rate" $type]} {
            set ::delta($type,all) [expr {($newmean - $oldmean) / $oldmean}]
        } else {
            set ::delta($type,all) [expr {($oldmean - $newmean) / $newmean}]
        }
    }

    return
}

proc substFile { fname } {
    
    if  { ! [ file exist $fname ] } {
        if  { [ regexp {opterondata} $fname ] } {
            regsub -all {opterondata} $fname {dataserver} oldfile
            puts "$fname substitued with $oldfile [ file exist $oldfile ]"
            if  { [ file exist $oldfile ] } {
                set fname $oldfile
            } elseif { [ string match "../results.190/dataserver-dev_dataserver-dev*" $oldfile ] } {                
                regsub {dataserver-dev_dataserver-dev} $oldfile {opterondata-dev_dataserver-dev} oldfile
                puts "fname $fname substitued again with $oldfile [ file exist $oldfile ]"
                set fname $oldfile
            }
        } elseif { [ regexp {dataserver} $fname ] } {
            regsub -all {dataserver} $fname {opterondata} oldfile
            puts "$fname substitued with $oldfile [ file exist $oldfile ]"
            if  { [ file exist $oldfile ] } {
                set fname $oldfile
            } elseif { [ string match "../results.190/dataserver-dev_dataserver-dev*" $oldfile ] } {                
                regsub {opterondata-dev_opterondata-dev} $oldfile {opterondata-dev_dataserver-dev} oldfile
                puts "fname $fname substitued again with $oldfile [ file exist $oldfile ]"
                set fname $oldfile
            }
        } else {
            return -code error "Error: $fname not found"
        }
    } 
    return $fname  
}
 
proc print {new old out title} {
    if {[string match stdout $out]} {
        set fout $out
    } else {
        set fout [open $out w]
    }

    puts $fout "$title"
    puts $fout [clock format [clock seconds] -format {%B %d, %Y}]
    if {[string length $new] && [string length $old]} {
        puts $fout "New file: [clock format [file mtime $new] -format {%B %d, %Y}]"
        puts $fout "Old file: [clock format [file mtime $old] -format {%B %d, %Y}]"
    }
    puts $fout ""

    array set text {
        cpu "CPU Time (s)"
        wall "Wall Time (s)"
        rate "Rate (MB/s)"
    }

    puts $fout [format {%-23s%-12s%-14s%-12s} " " "New Avg" "Old Avg" Analysis]
    foreach type {rate wall cpu} {
        if {[string equal NA $::delta($type,all)]} {
            set analysis "N/A"
        } else {
            set analysis [format {%6.2f%% %s} \
                [expr {100 * abs($::delta($type,all))}] \
                [expr {$::delta($type,all) >= 0?"Faster":"Slower"}]]
        }
        puts $fout [format {%-23s%-12.6f%-12.6f%s} \
            "Overall $text($type)" \
            $::mean($type,new,all) \
            $::mean($type,old,all) \
            $analysis]
    }

    foreach type {rate wall cpu} {
        puts $fout ""
        puts $fout [format {%-23s%-12s%-14s%-12s} "$text($type) x Type" "New Avg" "Old Avg" Analysis]
        foreach dtype $::Types {
            if {[string equal NA $::delta($type,$dtype)]} {
                set analysis "N/A"
            } else {
                set analysis [format {%6.2f%% %s} \
                    [expr {100 * abs($::delta($type,$dtype))}] \
                    [expr {$::delta($type,$dtype) >= 0?"Faster":"Slower"}]]
            }
            puts $fout [format {%-23s%-12.6f%-12.6f%s} \
                $dtype \
                $::mean($type,new,$dtype) \
                $::mean($type,old,$dtype) \
                $analysis]
        }
    }

    foreach type {rate wall cpu} {
        puts $fout ""
        puts $fout [format {%-23s%-12s%-14s%-12s} "$text($type) x Dim" "New Avg" "Old Avg" Analysis]
        foreach dim $::Dims {
            if {[string equal NA $::delta($type,$dim)]} {
                set analysis "N/A"
            } else {
                set analysis [format {%6.2f%% %s} \
                    [expr {100 * abs($::delta($type,$dim))}] \
                    [expr {$::delta($type,$dim) >= 0?"Faster":"Slower"}]]
            }
            puts $fout [format {%-23s%-12.6f%-12.6f%s} \
                $dim \
                $::mean($type,new,$dim) \
                $::mean($type,old,$dim) \
                $analysis]
        }
    }

    if {![string match stdout $out]} {
        close $fout
    }

    return
}

proc mean {samples} {
    set mean 0.0
    set N [ llength $samples ]

    if { $N > 0 } {
        set mean [ expr ([ join $samples + ]) / $N. ]
    }

    return $mean
}

main

