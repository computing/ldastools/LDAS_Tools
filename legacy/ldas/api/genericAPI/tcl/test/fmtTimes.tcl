#! /ldcg/bin/tclsh

;## set ups
set bgcolor(1) " bgcolor=\"#ffe6ef\""
set bgcolor(10) " bgcolor=\"#ffe6ab\""
set bgcolor(100) " bgcolor=\"#ffe6cd\""  
set bgcolor(1000) " bgcolor=\"#ffe669\"" 
set bgcolor(10000) " bgcolor=\"#ffe6ef\""
set bgcolor(100000) " bgcolor=\"#ffe6ab\""
set bgcolor(1000000) " bgcolor=\"#ffe6cd\""  

set desc(wallmean)  "Mean Wall Time (seconds)"
set desc(wallsd)    "Wall Time standard deviation (seconds)"
set desc(wallcov)   "Wall Time Coefficient of Variation (%)"
set desc(wallrate) "Wall Time Rate (MBytes/sec)"
set desc(cpumean)  "Mean CPU Time (seconds)"
set desc(cpusd)    "CPU Time standard deviation (seconds)"
set desc(cpucov)   "CPU Time Coefficient of Variation (%)"
set types { char_s char_u int_2s int_2u int_4s int_4u int_8s int_8u real_4 real_8 lstring2 lstring4 lstring8 lstring16}
;## set dimset { 1 10 100 1000 10000 100000 }
;##set dimset { 1 10 100 1000 10000 100000 1000000 }
set dimset {1 }

proc write_table {} {
uplevel {
puts $fhtml "<p>"
puts $fhtml "<table border type=1>"
puts $fhtml "<caption align=center><b>$::desc(${var}) (sample size=100)</b></caption>"
puts $fhtml "<font size=-1>"
puts $fhtml "<tr>\n\
    <th align=left width=100>Type\\dims</th>\n\
    <th width=100 align=right>1</th>\n\
    <th width=100 align=right>10</th>\n\
    <th width=100 align=right>100</th>\n\
    <th width=100 align=right>1000</th>\n\
    <th width=100 align=right>10000</th>\n\
    <th width=100 align=right>100000</th>\n\
    <th width=100 align=right>1000000</th>\n\
</tr>"

foreach type $::types {
	puts $fhtml "<th align=left width=100>$type</th>"   
	foreach dims $::dimset {	        
    	puts $fhtml "<td align=right$::bgcolor($dims)>[ format "%.6f" [set ${type}_${dims}(${var}) ] ]</td>"
	}
    puts $fhtml "</tr>"
    puts $fhtml "\n"
}

puts $fhtml "</table></font>\n</p>"

;## end uplevel 
}
}

proc fmtTimes { fname } {
puts "working on $fname"
set fin [ open $fname r ]

set data [ read $fin ]

close $fin

set data [ split $data \n ]

;## some problem with regexp, cant get cov
foreach line $data {
    if  { [ regexp {Data Socket Element Test (\w+)->(\w+) (without|with)} \
        $line match client server withcont ] } {
        ;##puts "client=$client, server=$server, withcont=$withcont"
    }
    if  { [ regexp {(char_[us]|int_[248us]+|real_[48]|lstring[12468]+)} $line match ] } {
        scan $line "%s %d %s %s %s %s %s %s %s" type dims wallrate wallmean wallsd wallcov cpumean cpusd cpucov        
        ;##puts "type=$type, dims=$dims, rate=$wallrate, wallmean=$wallmean,sd=$wallsd,cpumean=$cpumean,cpusd=$cpusd,cpucov=$cpucov"
        set ${type}_${dims}(wallmean) $wallmean
        set ${type}_${dims}(wallsd) $wallsd
        set ${type}_${dims}(wallcov) $wallcov
		if	{ [ regexp {char} $type ] } {
			;## only for 05-01 run 
			set ${type}_${dims}(wallrate) [ expr $wallrate/2 ]
		} else {
			set ${type}_${dims}(wallrate) $wallrate
		}
        set ${type}_${dims}(cpumean) $cpumean
        set ${type}_${dims}(cpusd) $cpusd
        set ${type}_${dims}(cpucov) $cpucov 
		      
    } 
}

;## output the text file 
set hostnames [ lindex [ split $fname . ] 0 ]
set output "${hostnames}_${withcont}cont.txt"
set fout [ open $output w ]
set hostnames [ split $hostnames _ ]
puts $fout "\t data socket test $client->$server, $withcont container (secs)\n "

set index 0

foreach timetype { wall cpu } {
	switch $timetype {
		wall { set varlist { rate mean sd cov } }
		cpu  { set varlist { mean sd cov } }
	}
	;##puts "varlist=$varlist"	
    foreach var $varlist {
		puts $fout $::desc(${timetype}${var})
        puts $fout "$timetype clock $var\t[join $::dimset \t]\n"
        set var "${timetype}${var}"
        foreach type $::types {
            puts -nonewline $fout "$type\t"
            foreach dimtype $::dimset {
                if  { [ info exist ${type}_${dimtype}(${var}) ] } {
                puts -nonewline $fout "[set ${type}_${dimtype}(${var})]\t"
                } else {
                puts -nonewline $fout "\t"
                puts "no dims=$dimtype for $type, var=$var"
                }
            incr index 1 
            }
        puts $fout ""
        }
        puts $fout "\n"
    }
}
    
close $fout

set host1 [ lindex $hostnames 0 ]
set host2 [ lindex $hostnames 1 ]

if  { [ regexp {controlmon|linuxbox} $host1 ] } {
    set platform(host1) "Linux Box"
} elseif  { [ regexp {dataserver} $host1 ] } {
    set platform(host1) "Sun 450"
} elseif  { [ regexp {sunbox} $host1 ] } {
    set platform(host1) "Sun Ultra-10"
}

if  { [ regexp {controlmon|linuxbox} $host2  ] } {
    set platform(host2) "Linux Box"
} elseif  { [ regexp {dataserver} $host2 ] } {
    set platform(host2) "Sun 450"
} elseif  { [ regexp {sunbox} $host2 ] } {
    set platform(host2) "Sun Ultra-10"
}

if  {  ! [ string compare $host1 $host2 ] } {
    set loopback "Loopback"
} else {
    set loopback ""
}

;## output the html file
set htmlfile "[ join $hostnames _]_${withcont}cont.html"
;##set htmlfile "temp.html"
set fhtml [ open $htmlfile w ]

;## output the heading
set verstr [ exec ls -ld /ldas/lib ]
if	{ ! [ regexp {stow_pkgs/(ldas-[\d.]+)} $verstr match version ] } {
	set version "ldas.0.0.alpha"
} 

set version "ldas.0.0.11"

set domain [ exec domainname ]
set currtime [ clock format [clock seconds] -format "%B %d %Y %H:%M:%S" ]
;## output the heading
set heading "\
<!doctype html public \"-/w3c/dtd html 4.0 transitional/en\">\n\
<html>\n\
<head>\n\
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n\
   <meta name=\"GENERATOR\" content=\"Mozilla/4.5 \[en\] (X11; I; SunOS 5.6 sun4u) \[Netscape\]\">\n\
   <title>LDAS Data Socket Transfer Rates at $domain</title>\n\
</head>\n\
<body>\n\
\n\
<pre><b><font size=-1>LDAS Data Socket Roundtrip Transmission Rates at $domain</font></b></pre>\n\
<font size=-2>\n\
<table cellpadding=1>\n\
<tr>\n\
    <th align=left width=200>Test Period:</th>\n\
    <td width=300>$currtime</td>\n\
</tr>\n\
<tr>\n\
    <th align=left width=200>Input data:</th>\n\
    <td width=300>Ilwd with dimensions 1,10,100,1000,10000,100000<br>$withcont container</td>\n\
    <td>\n\
</tr>\n\
<tr>\n\
    <th align=left width=200>Platforms:</th>\n\
    <td width=300>$platform(host1) <-> $platform(host2) $loopback</td>\n\
    <td>\n\
</tr>\n\
<tr>\n\
    <th align=left width=200>LDAS software:</th>\n\
    <td width=300>$version, Sun Solaris 2.7 for Sun or Linux 6.1 for Intel Pentium</td>\n\
    <td>\n\
</tr>\n\
</table></font>"

puts $fhtml $heading

foreach timetype { wall cpu } {
	switch $timetype { 
		wall { set varlist { rate mean sd cov } }
		cpu  { set varlist { mean sd cov } }
	}
    foreach var $varlist { 
        set var "${timetype}${var}"   
        write_table 
    }
        
}

puts $fhtml "<p>Summary: \n\
</font>\n\
</body>\n\
</html>"

close $fhtml

}

proc typeCompare { a b } {
	regexp {^(.+).(\d\d.\d\d.\d\d)} $a match regexa regex2a
	regexp {^(.+).(\d\d.\d\d.\d\d)} $b match regexb regex2b
	set rc [ string compare $regexa $regexb ]
	if	{ ! $rc } {		
		return [ string compare $regex2a $regex2b ]  
	}
	return $rc
}

set files [ glob *_*.*.??.??.?? ]
set files [ split $files ] 
set files [ lsort -command typeCompare $files ]
set n 1
while { [ llength $files ] > 0 } {
set entry [ lindex $files 0 ]
regexp {^(.+).(\d\d.\d\d.\d\d)} $entry match regex regex2
;##puts "regex=$regex, regex2=$regex2"
	while { [ regexp $regex $files match ] } {
;## use -regexp in case items are lists themselves 
		set i [ lsearch -regexp $files $regex ]
  		set j [ expr {$i + $n - 1} ]
   		set data [ lrange $files $i $j ]
		if	{ ! [ info exist ${regex}list ] } {
			set ${regex}list {}
		}
		lappend ${regex}list $data
    	set a [ lrange $files 0 [ expr $i-1 ] ]
    	set b [ lrange $files [ expr $j+1 ] end ]
   		set files [ concat $a $b ]
     }
	 catch { eval exec cat [ set ${regex}list ] > $regex } err
	 if	{ [ file exist $regex ] } {
	 	fmtTimes $regex
	 }	 
}
