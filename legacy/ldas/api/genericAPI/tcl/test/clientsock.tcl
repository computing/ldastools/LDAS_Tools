#! /ldcg/bin/tclsh

## ******************************************************** 
## clientsock.tcl Version 1.0
##
##  test sending cmds to data socket
##  simulates GDS ilwd every sec
##
##  run this by entering: tclsh clientsock.tcl
## 
## ******************************************************** 

;#barecode

;## sends GDS ilwd object to metadataAPI socket.

  set API metadata
  
  set LDAS [ pwd ]
  source LDASmetadata.rsc                 	
  set auto_path "$env(HOME)/ldas/api/genericAPI/tcl \
  /ldas/lib $auto_path"
  package require generic
  
  proc addLogEntry { args } { return {} }
  
  # lw
  set host deneb
  set port [ set ::${::API}(data) ]
  puts "send to $host:$port"  
   
  set basename "/home/mlei/perform"             			
  ;##set filename "${basename}/frameset10.ilwd"
  set filename "${basename}/frame10.ilwd"
  if  { [ catch { set filwd [ openILwdFile $filename "r" ] } errmsg ] } {
        puts "$filename $errmsg."
  }
    
  if  { [ catch { set datap [ readElement $filwd ] } errmsg ] } {
        puts "readElement $errmsg."
        catch { closeIlwdFile $filwd }
  }      
 
  ;## cannot do socket put with data socket
  
  set limit 1  

  for { set i 0 } { $i < $limit } { incr i 1 } {

    if  { [ catch {
            putDataSocket $API $datap temp
            } err ] } {
            puts "putDataSocket: $err"
            break
        }
    after 500;
  }
  
puts "clientsock exiting"  
;#end
