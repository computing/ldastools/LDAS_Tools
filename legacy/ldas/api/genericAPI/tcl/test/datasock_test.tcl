#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

## ********************************************************
## datasock_test Version 1.0
##
##  test sending ilwd element types to server and
##  receiving it back from server
## ********************************************************

;## we use this one rather than in genericAPI to handle
;## cpu values of 0 seconds.
;## maybe obsolete by use of genericAPI function stats
proc mystats { samples } {
    set mean 0.0
    set S2 0.0
    set cov 0.0

    set N [ llength $samples ]

    if { $N <= 1 } {
        return [ list [ lindex $samples 0 ] 0 100 ]
    }

    ;## calculate the arithmetic mean
    set mean [ expr ([ join $samples + ]) / $N. ]

    ;## calculate the standard deviation
    foreach s $samples {
        set S2 [ expr { $S2+pow(($s-$mean),2) } ]
    }
    set S2 [ expr { $S2/($N-1) } ]
    set S [ expr { sqrt($S2) } ]

    ;## calcualte the % coefficient of variation
    if { $mean != 0 } {
        set cov [ expr { ($S/$mean)*100 } ]
    }

    ;## return the values in a formatted list
    return [ list $mean $S $cov ]
}

;## 100BT ethernet is 100 Mbits/sec
;## or 100/8 = 12 MBytes/sec
;## 1 byte (B) = 8 bits
;## 1 kilobyte (K) = 1,024 bytes
;## 1 megabyte (MB) = 1,024 kilobytes
;## 1 gigabyte (GB) = 1,024 megabytes

proc computeRate { type dims time } {
    if { [ regexp {char} $type ] } {
        set size 1
    } else {
        regexp {(\d+)} $type -> size
    }
    set rateB [ expr {2 * ($dims * $size) / $time} ]
    set rateMB [ expr {$rateB / (1024.0 * 1024.0)} ]

    return $rateMB
}

proc getIlwd { type dims withcont ilwdfmt } {
    set filename "./ilwd/${ilwdfmt}/${type}_${dims}.ilwd"
    puts stderr "filename: $filename"
    set filwd "nofd"
    if {[catch {
        set filwd [ openILwdFile $filename "r" ]
        puts stderr "opened file: $filename"
        set datap [ readElement $filwd ]
        puts stderr "read element: $datap"
    } err]} {
        catch { closeILwdFile $filwd }
        return -code error $err
    }

    catch { closeILwdFile $filwd }
    return $datap
}
## *************************************************

## *************************************************
#proc recvData {thread} {
#    if {$thread} {
#        set failed [catch {
#            set seqpt "acceptDataSocket_t $::serverp"
#            set tid [ acceptDataSocket_t $::serverp ]
#
#            ;## will block until thread returns
#            set seqpt "acceptDataSocket_r $tid"
#            set acceptp [ acceptDataSocket_r $tid ]
#
#            set seqpt "getSocketPeerIpAddress $acceptp"
#            set ::sender [ getSocketPeerIpAddress $acceptp ]
#
#            set seqpt "recvElementObject_t $acceptp"
#            set tid [ recvElementObject_t $acceptp ]
#
#            set seqpt "recvElementObject_r $tid"
#            set elemp [ recvElementObject_r $tid ]
#        } errmsg]
#    } else {;## Non-threaded
#        set failed [catch {
#            set seqpt "acceptDataSocket $::serverp"
#            set acceptp [ acceptDataSocket $::serverp ]
#
#            set seqpt "getSocketPeerIpAddress $acceptp"
#            set ::sender [ getSocketPeerIpAddress $acceptp ]
#
#            set seqpt "recvElementObject $acceptp"
#            set elemp [ recvElementObject $acceptp ]
#        } errmsg]
#    }
#
#    #after 10
#    catch {closeDataSocket $acceptp}
#
#    if {$failed} {
#        return -code error "[ myName ]: $seqpt,$errmsg"
#    }
#
#    puts stderr "RECV [incr ::numRecv]"
#    return $elemp
#}
proc recvData {sid} {
    if {[catch {data_gets $sid} elemp]} {
        return -code error "[myName]: $elemp"
    }

    puts stderr "RECV [incr ::numRecv]"
    #puts stderr "Received $elemp"
    return $elemp
}
## *************************************************

## *************************************************
#proc sendData { clientp datap thread } {
#    if {$thread} {
#        set failed [catch {
#            set seqpt "sendElementObject_t $clientp $datap"
#            set tid [ sendElementObject_t $clientp $datap ]
#
#            ;## will block until thread returns
#            set seqpt "sendElementObject_r $tid"
#            sendElementObject_r $tid
#        } errmsg]
#    } else {;## Non-threaded
#        set failed [catch {
#            set seqpt "sendElementObject $clientp $datap"
#            sendElementObject $clientp $datap
#        } errmsg]
#    }
#
#    if {$failed} {
#        return -code error "[ myName ]: $seqpt,$errmsg"
#    }
#
#    puts stderr "SEND [incr ::numSent]"
#    return
#}
proc sendData {sid datap} {
    #puts "Sending $datap"
    if {[catch {data_puts $sid $datap} errmsg]} {
        set sockerr [fconfigure $sid -error]
        return -code error "[myName]: $errmsg: $sockerr"
    }

    puts stderr "SEND [incr ::numSent]"
    return
}
## *************************************************

proc cmpElementsOld { contp1 contp2 } {
    ;## compare elements
    set result1 [ getElement $contp1 ]
    set result2 [ getElement $contp2 ]

    return [ string compare $result1 $result2 ]
}

;## to avoid out of memory, write element to temp file
;## compare the files, then delete the temp file
proc outFile { objectp } {
    set fname [file join /tmp ${objectp}.ilwd]
    set fout [ openILwdFile $fname "w" ]
    writeElement $fout $objectp ascii none
    closeILwdFile $fout
    return $fname
}

proc cmpElementFiles { fname1 fname2 type dims iter} {
    set exitcode 0
    if {[catch { exec diff $fname1 $fname2 } msg ]} {
        set exitcode -1
        if {[string equal "CHILDSTATUS" [lindex $::errorCode 0]]} {
            set exitcode [lindex $::errorCode 2]
        }
    }

    switch -exact -- $exitcode {
        0 { }
        1 { puts stderr "Error: element data differ: $type, $dims dims, iteration $iter: $msg" }
        default { puts stderr "Error: [myName]: $msg" }
    }

    return
}

proc output {fout type dims cputimes walltimes} {
    foreach timetype { wall cpu } {
        set result [ mystats [ set ${timetype}times ] ]
        foreach {mean stddev cov} $result {break;}
        if { [ string equal $timetype "wall" ] } {
            set rate [ computeRate $type $dims $mean ]
            puts -nonewline $fout [ format "%10s\t%10s\t%s\t%.6f\t%.6f\t%3.2f\t" $type $dims $rate $mean $stddev $cov ]
        } else {
            puts -nonewline $fout [ format "%.6f\t%.6f\t%3.2f\t" $mean $stddev $cov ]
        }
    }
    puts $fout ""
    flush $fout
    return
}

#proc retry_connect {server port} {
#    for { set retry 1 } { $retry < 1000 } { incr retry 1 } {
#        set clientp [ createDataSocket 0 $::localhost ]
#        ;## connect to the server
#        catch { connectDataSocket $clientp $server $port } err
#        if { [ isSocketConnected $clientp ] } {
#            return $clientp
#        } else {
#            puts stderr "retry_connect to $server:$port: try $retry: $err"
#            catch { closeDataSocket $clientp }
#            after 100
#        }
#    }
#    return {}
#}

proc retry_connect {server port} {
    set maxtries 1000
    for {set retry 1} {$retry <= $maxtries} {incr retry} {
        if {[catch {datasocket $server $port} msg]} {
            puts stderr "retry_connect to $server:$port: try $retry of $maxtries: $msg"
            after 100
            continue
        }
        return $msg
    }

    return -code error "Could not connect to $server:$port after $maxtries tries."
}

#proc clientSend {server datap thread} {
#    set clientp [ retry_connect $server $::test_server(data) ]
#    ;##puts stderr "connect status [ isSocketConnected $clientp ]"
#
#    if { ![string length $clientp] } {
#        return
#    }
#
#    __t::start
#    set cpu_begin [ exectime ]
#    sendData $clientp $datap $thread
#    ;## a small delay is needed to prevent bind errors in linux
#    #after 10
#    catch {closeDataSocket $clientp}
#    set elemp [recvData $thread]
#    set cputime [ expr ( [ exectime ] - $cpu_begin ) ]
#    set walltime [ __t::mark ]
#
#    return [list $elemp $cputime $walltime]
#}
proc clientSend {server datap thread} {
    set ::elemp {}
    set sid [retry_connect $server $::test_server(data)]

    __t::start
    set cpu_begin [exectime]
    sendData $sid $datap
    catch {close $sid}

    ;## Wait for the data to come back
    vwait ::elemp

    set cputime [expr {[exectime] - $cpu_begin}]
    set walltime [__t::mark]

    return [list $::elemp $cputime $walltime]
}

;# will be blocked here until it is killed
#proc initServer {thread} {
#    while {1} {
#        if {[catch {
#                set datap [recvData $thread]
#                ;##puts stderr "datap=$datap"
#
#                ;## create a client
#                set clientp [ retry_connect $::sender $::test_client(data) ]
#                ;##puts stderr "clientp=$clientp, sender=$sender, port=$::test_client(data)"
#                ;##puts stderr "connect status [ isSocketConnected $clientp ]"
#
#                ;## send element to the server
#                if { [ string length $clientp ] } {
#                    sendData $clientp $datap $thread
#                    ;## close the client
#                    #after 10
#                    catch {closeDataSocket $clientp}
#                }
#                destructElement $datap
#            } err]} {
#            puts stderr "Error: $err:\n$::errorInfo"
#        }
#    }
#}

proc serverInit {thread} {
    if {![string length $::serverp]} {
        set ::serverp [datasocket -server "serverConfig serverHandler" $::port]
        after 250
    }
}

proc serverConfig {service sid addr port} {
    fileevent $sid readable [list $service $sid $addr $port]
    fconfigure $sid -blocking on
    #fconfigure $sid -linger 0
    fconfigure $sid -buffering none
    fconfigure $sid -translation binary
    fconfigure $sid -encoding binary
    return
}

proc serverHandler {sid addr port} {
    fileevent $sid readable {}

    if {[catch {
            #puts stderr "passed arguments: $sid $addr $port"
            #puts stderr "sockname: [fconfigure $sid -sockname]"
            #puts stderr "peername: [fconfigure $sid -peername]"
            set sender [lindex [fconfigure $sid -peername] 0]

            set datap [recvData $sid]
            catch {close $sid}

            ;## Connect to the sender
            set clientp [retry_connect $addr $::test_client(data)]

            ;## Send the data back
            sendData $clientp $datap
            catch {close $clientp}
            destructElement $datap
        } err]} {
        puts stderr "Error: $err:\n$::errorInfo"
    }
    return
}

proc clientInit { server dimset types thread samples withcont ilwdfmt } {
    if {[string equal $dimset "all"]} {
        set dimset $::default_dimset
    } else {
        set dimset [split $dimset ,]
    }

    if {[string equal $types "all"]} {
        set types $::default_types
    } else {
        set types [split $types ,]
    }

    set ::Error ""
    puts stderr "dimset=$dimset, types=$types, localhost=$::localhost"

    ;## Start a server to receive objects back
    if {![string length $::serverp]} {
        set ::serverp [datasocket -server "clientConfig clientHandler" $::port]
        after 250
    }

    sendElements $server $dimset $types $thread $samples $withcont $ilwdfmt
    puts stderr "Test completed, results in ${::localhost}_$server.times. $::Error\n"

    return "Test completed $::Error"
}

proc clientConfig {service cid addr port} {
    fileevent $cid readable [list $service $cid $addr $port]
    fconfigure $cid -blocking on
    #fconfigure $cid -linger 0
    fconfigure $cid -buffering none
    fconfigure $cid -translation binary
    fconfigure $cid -encoding binary
    return
}

proc clientHandler {cid addr port} {
    fileevent $cid readable {}
    set datap [recvData $cid]
    catch {close $cid}
    set ::elemp $datap
    return
}

proc sendElements {server dimset types thread samples withcont ilwdfmt} {
    ;## open the result file
    set dimslimit 1000000
    ;## create the results directory
    set resultdir "./results-[ clock format [ clock seconds ] -format "%m%d" ]"
    puts stderr "resultdir=$resultdir"

    if { ! [ file exist $resultdir ] } {
        file mkdir $resultdir
    }

    if { $withcont } {
        set conttext "with container"
        set wc "wc"
    } else {
        set conttext "without container"
        set wc "nc"
    }

    if { $thread } {
        set t {_t}
        set threadtext "threaded"
    } else {
        set t {}
        set threadtext "non-threaded"
    }

    __t::start
    set fname "$resultdir/${::localhost}_${server}_${wc}${t}.[clock format [ clock seconds ] -format "%H%M%S" ]"
    set fout [ open $fname a ]

    puts $fout "Data Socket Element Test $::localhost->$server ${conttext} ${threadtext} ${samples} samples\n"

    foreach entry [ concat type dims rate wall_mean wall_stddev wall_cov cpu_mean cpu_stddev cpu_cov] {
        puts -nonewline $fout [ format "%s\t" $entry ]
    }
    puts $fout ""

    lappend timings "open_log_file [__t::mark]"

    set start_time [ clock seconds ]
    ;## now send the elements
    if {[catch {
            foreach dims $dimset {
                puts stderr "sending dims=$dims on $::tcl_platform(os)"

                foreach type $types {
                    set cputimeL {}
                    set walltimeL {}

                    __t::start
                    set contp [ getIlwd $type $dims $withcont $ilwdfmt]
                    lappend timings "getilwd_${type}_${dims} [__t::mark]"

                    if { $withcont } {
                        set datap $contp
                    } else {
                        set datap [ refContainerElement $contp 0 ]
                    }

                    puts stderr "datap=$datap"
                    if { $dims < $dimslimit } {
                        __t::start
                        set fname1 [ outFile $datap ]
                        lappend  timings "fileio1_${type}_${dims} [__t::mark]"

                        set fileio2 0
                        set filecmp 0
                    }
                    ;## create a client
                    for { set ntimes 0 } { $ntimes < $samples } { incr ntimes 1 } {
                        ;## timing starts here
                        ;## send element to the server
                        ;## CPU time in microsecs
                        ;## repeat sending over 10 times to get the variance
                        ;## and standard deviation
                        set retval [clientSend $server $datap $thread]
                        set elemp [lindex $retval 0]
                        lappend cputimeL [lindex $retval 1]
                        lappend walltimeL [lindex $retval 2]
                        ;## out of memory for higher dims when comparing
                        if { $dims < $dimslimit } {
                            __t::start
                            set fname2 [ outFile $elemp ]
                            set fileio2 [expr $fileio2 + [__t::mark]]

                            __t::start
                            cmpElementFiles $fname1 $fname2 $type $dims $ntimes
                            set filecmp [expr $filecmp + [__t::mark]]
                            file delete $fname2
                        }
                        destructElement $elemp
                    }
                    destructElement $contp
                    if { $dims < $dimslimit } {
                        file delete $fname1
                        lappend timings "fileio2total_${type}_${dims} $fileio2"
                        lappend timings "filecmptotal_${type}_${dims} $filecmp"
                    }
                    __t::start
                    output $fout $type $dims $cputimeL $walltimeL
                    lappend timings "output_${type}_${dims} [__t::mark]"
                    lappend timings "totalwall_${type}_${dims} [expr [join $walltimeL "+"]]"
                }
            }
        } err]} {
        puts stderr "error: $err"
        set ::Error "Error: $err"
    }
    ;## compute time taken
    set secs [ expr [ clock seconds ] - $start_time ]
    set mins [ expr $secs / 60 ]
    set hour [ expr $mins / 60 ]
    set mins [ expr $mins % 60 ]
    set secs [ expr $secs % 60 ]
    puts $fout "Test completed in $hour:$mins:$secs. $::Error"

    ;## Report extra timings
    ;##puts $fout ""
    ;##set totaltime 0
    ;##foreach item $timings {
    ;##    puts $fout "$item"
    ;##    set totaltime [expr $totaltime + [lindex $item 1]]
    ;##}
    ;##puts $fout "\nTotal processing time: $totaltime"

    close $fout
}

;## barecode
;##package provide datasock_test
#package require generic

set ::numRecv 0
set ::numSent 0
;## end
## *****************************************

