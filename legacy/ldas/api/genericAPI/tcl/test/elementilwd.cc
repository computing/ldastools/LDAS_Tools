#include <cstdio>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include <ilwd/util.hh>

#ifdef __sun__
#include <sys/types.h>
#endif

using namespace std;

using ILwd::ElementId;
using ILwd::LdasContainer;
using ILwd::LdasElement;
using ILwd::LdasArray;
using ILwd::LdasArrayBase;
using ILwd::LdasString;
using ILwd::Style;

using ILwd::ID_ILWD;
using ILwd::ID_LSTRING;
using ILwd::ID_CHAR;
using ILwd::ID_CHAR_U;
using ILwd::ID_INT_2S;
using ILwd::ID_INT_2U;
using ILwd::ID_INT_4S;
using ILwd::ID_INT_4U;
using ILwd::ID_INT_8S;
using ILwd::ID_INT_8U;
using ILwd::ID_REAL_4;
using ILwd::ID_REAL_8;
using ILwd::ID_COMPLEX_8;
using ILwd::ID_COMPLEX_16;

const int ARRAYSIZE = 1000000;
int numElements;

template <class T>
inline T* ptr( T* Value)
{
  if ( numElements ) {
    return Value;
  } else {
    return (T*)NULL;
  }
}

namespace element_test {
    CHAR_U  elm_CHAR_U[ARRAYSIZE];
    CHAR    elm_CHAR_S[ARRAYSIZE];
    INT_2U  elm_INT_2U[ARRAYSIZE];
    INT_2S  elm_INT_2S[ARRAYSIZE];
    INT_4U  elm_INT_4U[ARRAYSIZE];
    INT_4S  elm_INT_4S[ARRAYSIZE];
    INT_8U  elm_INT_8U[ARRAYSIZE];
    INT_8S  elm_INT_8S[ARRAYSIZE];
    REAL_4  elm_REAL_4[ARRAYSIZE];
    REAL_8  elm_REAL_8[ARRAYSIZE];
    string  elm_lstring_2[ARRAYSIZE];
    string  elm_lstring_4[ARRAYSIZE];
    string  elm_lstring_8[ARRAYSIZE];
    string  elm_lstring_16[ARRAYSIZE];
    COMPLEX_8 elm_COMPLEX_8[ARRAYSIZE];
    COMPLEX_16 elm_COMPLEX_16[ARRAYSIZE];
} // end namespace

static void setup_char_s () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_CHAR_S[i] = (CHAR) ( mrand48() % 127 );
    }
}

static void setup_char_u () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_CHAR_U[i] = (CHAR_U) ( lrand48() % 255 );
    }
}

static void setup_int_2s () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_INT_2S[i] = (INT_2S) ( mrand48() % 32767 ) ;
    }
}

static void setup_int_2u () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_INT_2U[i] = (INT_2U) ( lrand48() % 65535 );
    }
}

static void setup_int_4s () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_INT_4S[i] = (INT_4S) mrand48();
    }
}

static void setup_int_4u () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_INT_4U[i] = (INT_4U) lrand48();
    }
}

static void setup_int_8s () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_INT_8S[i] = (INT_8S) mrand48();
    }
}

static void setup_int_8u () {
    for (int i=0; i < numElements; i++) {
        element_test::elm_INT_8U[i] = (INT_8U) lrand48();
    }
}

static void setup_real_4 () {
    for (unsigned i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
            element_test::elm_REAL_4[i] = (REAL_4) drand48();
        } else {
            element_test::elm_REAL_4[i] = (REAL_4) (drand48() * -1.0);
        }
    }
}

static void setup_real_8 () {
    for (unsigned int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
            element_test::elm_REAL_8[i] = (REAL_8) drand48();
        } else {
            element_test::elm_REAL_8[i] = (REAL_8) (drand48() * -1.0);
        }
    }
}

static void setup_lstring_2 () {
    // fill in ifo_site array
    // primary key
    for ( unsigned i=0; i < numElements; i++) {
        element_test::elm_lstring_2[i] = string("ab");
    }
}

static void setup_lstring_4 () {
    // fill in ifo_site array
    // primary key
    for ( unsigned i=0; i < numElements; i++) {
        element_test::elm_lstring_4[i] = string("abcd");
    }
}

static void setup_lstring_8 () {
    // fill in ifo_site array
    // primary key
    for ( unsigned i=0; i < numElements; i++) {
        element_test::elm_lstring_8[i] = string("abcdefgh");
    }
}

static void setup_lstring_16 () {
    // fill in ifo_site array
    // primary key
    for ( unsigned i=0; i < numElements; i++) {
        element_test::elm_lstring_16[i] = string("abcdefgh\f\t\n\r\v ij");
    }
}

static void setup_complex_8 () {
    for (unsigned int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
            element_test::elm_COMPLEX_8[i] = COMPLEX_8(drand48(), drand48()) ;
        } else {
            element_test::elm_COMPLEX_8[i] = COMPLEX_8((drand48() * -1.0), drand48()) ;
        }
    }
}

static void setup_complex_16 () {
    for (unsigned int i=0; i < numElements; i++) {
        if ( ( i % 2 ) == 0 ) {
            element_test::elm_COMPLEX_16[i] = COMPLEX_16(drand48(), drand48()) ;
        } else {
            element_test::elm_COMPLEX_16[i] = COMPLEX_16((drand48() * -1.0), drand48()) ;
        }
    }
}


// ilwd types
//        ID_CHAR,
//        ID_CHAR_U,
//        ID_INT_2S,
//        ID_INT_2U,
//        ID_INT_4S,
//        ID_INT_4U,
//        ID_INT_8S,
//        ID_INT_8U,
//        ID_REAL_4,
//        ID_REAL_8,
//        ID_ILWD,
//        ID_LSTRING,
//        ID_COMPLEX_8,
//        ID_COMPLEX_16,

// argv[1] =
main(int argc, char **argv) {
    char *element_type;

    ILwd::Format format = ILwd::BINARY;
    ILwd::Compression compression = ILwd::NO_COMPRESSION;

    if ( argc < 3 ) {
        cerr << "Usage: " << argv[0] << " dims type [format]" << endl;
        exit (-1);
    }

    numElements = atoi(argv[1]);
    element_type = argv[2];

    if ( argc > 3 ) {
        if ( ! strcmp (argv[3], "ascii") ) {
            format = ILwd::ASCII ;
            compression = ILwd::NO_COMPRESSION ;
        } else if ( ! strcmp (argv[3], "base64") ){
            format = ILwd::BASE64 ;
            compression = ILwd::GZIP;
        }
    }

    if ( numElements > ARRAYSIZE) {
        cerr << "max size is " << ARRAYSIZE << endl;
        exit (-1);
    }

    //srand48( time(0) );

    LdasContainer ilwd1( "ligo:element_test:file");
    ilwd1.setComment("testing send/recv elements");
    char buffer[500];
    char units[100];
    sprintf (buffer, "element_test:%s", element_type);
    LdasContainer ilwd3( buffer );
    ilwd1.setComment("main container");
    sprintf (buffer, "element_test:%s:%d", element_type, numElements);
    sprintf (units, "%s_units", element_type);

    if ( ! strcmp (element_type , "char_s" ) ) {
        setup_char_s();
        LdasArray< CHAR > char_s_vector ( ptr( element_test::elm_CHAR_S ), numElements, buffer, units );
        char_s_vector.setComment(buffer);
        ilwd3.push_back( char_s_vector );

    } else if ( ! strcmp (element_type , "char_u" )) {
        setup_char_u();
        LdasArray< CHAR_U > char_u_vector ( ptr( element_test::elm_CHAR_U ), numElements, buffer, units );
        char_u_vector.setComment(buffer);
        ilwd3.push_back( char_u_vector );

    } else if ( ! strcmp (element_type , "int_2s" )) {
        setup_int_2s();
        LdasArray< INT_2S > int_2s_vector ( ptr( element_test::elm_INT_2S ), numElements, buffer, units);
        int_2s_vector.setComment(buffer);
        ilwd3.push_back( int_2s_vector );

    } else if ( ! strcmp (element_type , "int_2u" )) {
        setup_int_2u();
        LdasArray< INT_2U > int_2u_vector ( ptr( element_test::elm_INT_2U ), numElements, buffer, units) ;
        int_2u_vector.setComment(buffer);
        ilwd3.push_back( int_2u_vector );

    } else if ( ! strcmp (element_type , "int_4s" )) {
        setup_int_4s();
        LdasArray< INT_4S > int_4s_vector ( ptr( element_test::elm_INT_4S ), numElements, buffer, units) ;
        int_4s_vector.setComment(buffer);
        ilwd3.push_back( int_4s_vector );

    } else if ( ! strcmp (element_type , "int_4u" )) {
        setup_int_4u();
        LdasArray< INT_4U > int_4u_vector ( ptr( element_test::elm_INT_4U ), numElements, buffer, units ) ;
        int_4u_vector.setComment(buffer);
        ilwd3.push_back( int_4u_vector );

    } else if ( ! strcmp (element_type , "int_8s" )) {
        setup_int_8s();
        LdasArray< INT_8S > int_8s_vector ( ptr( element_test::elm_INT_8S ), numElements, buffer, units ) ;
        int_8s_vector.setComment(buffer);
        ilwd3.push_back( int_8s_vector );

    } else if ( ! strcmp (element_type , "int_8u" )) {
        setup_int_8u();
        LdasArray< INT_8U > int_8u_vector ( ptr( element_test::elm_INT_8U ), numElements, buffer, units) ;
        int_8u_vector.setComment(buffer);
        ilwd3.push_back( int_8u_vector );

    } else if ( ! strcmp (element_type , "real_4" )) {
        setup_real_4();
        LdasArray< REAL_4 > real_4_vector ( ptr( element_test::elm_REAL_4 ), numElements, buffer, units ) ;
        real_4_vector.setComment(buffer);
        ilwd3.push_back( real_4_vector );

    } else if ( ! strcmp (element_type , "real_8" )) {
        setup_real_8();
        LdasArray< REAL_8 > real_8_vector ( ptr( element_test::elm_REAL_8 ), numElements, buffer ) ;
        real_8_vector.setComment(buffer);
        ilwd3.push_back( real_8_vector );

    } else if ( ! strcmp (element_type , "lstring_2" )) {
        setup_lstring_2();
        LdasString lstring_vector ( ptr( element_test::elm_lstring_2 ), (size_t) numElements, string(buffer), string(units) ) ;
        lstring_vector.setComment(buffer);
        ilwd3.push_back( lstring_vector );

    } else if ( ! strcmp (element_type , "lstring_4" )) {
        setup_lstring_4();
        LdasString lstring_vector ( ptr( element_test::elm_lstring_4 ), (size_t) numElements, string(buffer), string(units) ) ;
        lstring_vector.setComment(buffer);
        ilwd3.push_back( lstring_vector );

    } else if ( ! strcmp (element_type , "lstring_8" )) {
        setup_lstring_8();
        LdasString lstring_vector ( ptr( element_test::elm_lstring_8 ), (size_t) numElements, string(buffer), string(units) ) ;
        lstring_vector.setComment(buffer);
        ilwd3.push_back( lstring_vector );

    } else if ( ! strcmp (element_type , "lstring_16" )) {
        setup_lstring_16();
        LdasString lstring_vector ( ptr( element_test::elm_lstring_16 ), (size_t) numElements, string(buffer), string(units) ) ;
        lstring_vector.setComment(buffer);
        ilwd3.push_back( lstring_vector );

    } else if ( ! strcmp (element_type , "complex_8" )) {
        setup_complex_8();
        LdasArray< COMPLEX_8 > complex_8_vector ( ptr( element_test::elm_COMPLEX_8 ), numElements, buffer ) ;
        complex_8_vector.setComment(buffer);
        ilwd3.push_back( complex_8_vector );

    } else if ( ! strcmp (element_type , "complex_16" )) {
        setup_complex_16();
        LdasArray< COMPLEX_16 > complex_16_vector ( ptr( element_test::elm_COMPLEX_16 ), numElements, buffer ) ;
        complex_16_vector.setComment(buffer);
        ilwd3.push_back( complex_16_vector );
    }

    ilwd1.push_back( ilwd3 );
    ILwd::writeHeader(cout);
    ilwd1.write( 0, 4, cout, format, compression );
    cout << endl;
}
