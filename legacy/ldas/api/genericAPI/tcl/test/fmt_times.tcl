#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

set ::bgcolor(1) "bgcolor=\"#ffe6ef\""
set ::bgcolor(10) "bgcolor=\"#ffe6ab\""
set ::bgcolor(100) "bgcolor=\"#ffe6cd\""
set ::bgcolor(1000) "bgcolor=\"#ffe669\""
set ::bgcolor(10000) "bgcolor=\"#ffe6ef\""
set ::bgcolor(100000) "bgcolor=\"#ffe6ab\""
set ::bgcolor(1000000) "bgcolor=\"#ffe6cd\""

set ::desc(wallrate) "Wall Time Rate (MBytes/sec)"
set ::desc(wallmean) "Wall Time Mean (seconds)"
set ::desc(wallsd) "Wall Time Standard Seviation (seconds)"
set ::desc(wallcov) "Wall Time Coefficient of Variation (%)"
set ::desc(cpumean) "CPU Time Mean (seconds)"
set ::desc(cpusd) "CPU Time Standard Deviation (seconds)"
set ::desc(cpucov) "CPU Time Coefficient of Variation (%)"

set ::platform(dataserver) "Sun 450"
set ::platform(controlmon) "Linux Box"
set ::platform(metaserver) "Sun Ultra-10"
set ::platform(linuxbox1) "Linux Box"
set ::platform(sunbox1) "Sun Ultra-10"

set ::Types {
    char_s
    char_u
    int_2s 
    int_2u
    int_4s
    int_4u
    int_8s
    int_8u
    real_4
    real_8
    lstring_2
    lstring_4
    lstring_8
    lstring_16
    complex_8
    complex_16
}
set ::Dims { 1 10 100 1000 10000 100000 1000000 }

proc main {} {
    set filelist [lsort -dictionary $::argv]
    set samefileL {}

    for {set idx 0} {$idx <= $::argc} {incr idx} {
        set file [lindex $filelist $idx]
        if {![llength $samefileL]} {
            set samefileL [list "$file [file size $file]"]
            continue
        }
        if {[string equal [file rootname $file] [file rootname [lindex [lindex $samefileL 0] 0]]]} {
            lappend samefileL "$file [file size $file]"
            continue
        }

        set samefileL [lsort -integer -decreasing -index 1 $samefileL]
        set oname [lindex [lindex $samefileL 0] 0]
        set fname [file rootname $oname].log
        file copy -force -- $oname $fname

        if {[llength $samefileL] > 1} {
            set logfd [open $fname a]
            foreach pair [lrange $samefileL 1 end] {
                set fd [open [lindex $pair 0] r]
                puts $logfd [read -nonewline $fd]
                close $fd
            }
            close $logfd
        }

        if {[string length $file]} {
            set samefileL [list "$file [file size $file]"]
        }

        if {![file exists $fname]} {
            puts stderr "${::argv0}: $fname: No such file"
            continue
        }

        ;## clear values
        array unset ::wallrate
        array unset ::wallmean
        array unset ::wallsd
        array unset ::wallcov
        array unset ::cpumean
        array unset ::cpusd
        array unset ::cpucov

        puts stderr "Processing file $fname"
        formatData $fname
    }
}

proc formatData {fname} {
    set fin [ open $fname r ]
    set data [ split [read $fin] \n ]
    close $fin

    foreach line $data {
        if { [ regexp {Data Socket Element Test ([\w\-]+)->([\w\-]+) (without|with) container (non-threaded|threaded) (\d+) samples} $line -> client server withcont thread samples] } {
            puts stderr "client=$client, server=$server, withcont=$withcont"
        }

        if { [ regexp {(char_[us]|int_[248us]+|real_[48]|lstring_[12468]+|complex_[168]+)} $line] } {
            scan $line "%s %d %s %s %s %s %s %s %s" type dims wrate wmean wsd wcov cmean csd ccov
            ;##puts stderr "type=$type, dims=$dims, rate=$wrate, wallmean=$wmean, sd=$wsd, cov=$wcov cpumean=$cmean, cpusd=$csd, cpucov=$ccov"
            set ::wallmean($type,$dims) $wmean
            set ::wallsd($type,$dims) $wsd
            set ::wallcov($type,$dims) $wcov
            set ::wallrate($type,$dims) $wrate
            set ::cpumean($type,$dims) $cmean
            set ::cpusd($type,$dims) $csd
            set ::cpucov($type,$dims) $ccov

            ;## REMOVE ##
            ;## only for 06-30 run
            ;##set ::wallrate($type,$dims)  [ expr {($wrate * 1000000.0) / (1024 * 1024)} ]
        }
    }
    writeData $client $server $withcont $thread $samples
}

proc writeData {client server withcont thread samples} {
    switch -exact -- $withcont {
        with {set cont "wc"}
        without {set cont "nc"}
    }
    switch -exact -- $thread {
        threaded {set t "_t"}
        non-threaded {set t ""}
    }
    set textfile "${client}_${server}_${cont}${t}.txt"
    set htmlfile "${client}_${server}_${cont}${t}.html"
    set ftext [ open $textfile w ]
    set fhtml [ open $htmlfile w ]

    ;## Text header
    puts $ftext "\tdata socket test ${client}<->${server}, $withcont container, $thread, $samples samples\n"

    ;## HTML header
    writeHTMLHeader $fhtml $client $server $withcont $thread $samples

    foreach timetype { wall cpu } {
        switch -exact -- $timetype {
            wall { set varlist { rate mean sd cov } }
            cpu { set varlist { mean sd cov } }
        }
        foreach var $varlist {
            set timetypevar "${timetype}${var}"
            puts $ftext $::desc(${timetypevar})
            puts $ftext "Type \\ Dims\t[join $::Dims \t]\n"
            writeHTMLTableHeader $fhtml $timetypevar

            foreach type $::Types {
                puts -nonewline $ftext "$type\t"
                puts $fhtml "<th align=left width=100>$type</th>"

                foreach dims $::Dims {
                    if { [ info exists ::${timetypevar}($type,$dims) ] } {
                        puts -nonewline $ftext [format "%-10.8f\t" [set ::${timetypevar}($type,$dims)]]
                        puts $fhtml "<td align=right $::bgcolor($dims)>[ format {%.6f} [set ::${timetypevar}($type,$dims) ] ]</td>"
                    } else {
                        puts -nonewline $ftext "\t"
                        puts $fhtml "<td align=right $::bgcolor($dims)> </td>"

                        ;## only print this once
                        if {[string equal "wallrate" $timetypevar]} {
                            puts stderr "missing data for $type type, $dims dims"
                        }
                    }
                }

                puts $ftext ""
                puts $fhtml "</tr>"
                puts $fhtml ""
            }
            puts $ftext "\n"
            puts $fhtml "</table></font>\n</p>"
        }
    }
    puts $fhtml "<p>Summary: \n</font>\n</body>\n</html>"
    close $ftext
    close $fhtml
}

proc writeHTMLHeader {fhtml client server withcont thread samples} {
    if {[info exists ::platform($client)]} {
        set platformC "$::platform($client) ($client)"
    } else {
        set platformC "Unknown ($client)"
    }

    if {[info exists ::platform($server)]} {
        set platformS "$::platform($server) ($server)"
    } else {
        set platformS "Unknown ($server)"
    }

    set loopback ""
    if { [ string equal "$client" "$server" ] } {
        set loopback "Loopback"
    }

    set verstr [ exec ls -ld /ldas/lib ]
    if { ! [ regexp {stow_pkgs/(ldas-[\d.]+)} $verstr -> version ] } {
        set version "ldas.0.0.11"
    }

    set domain [ exec cat /etc/ldasname ]
    set currtime [ clock format [clock seconds] -format "%B %d %Y" ]

    set heading "<!doctype html public \"-/w3c/dtd html 4.0 transitional/en\">
<html>
<head>
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">
   <title>LDAS Data Socket Transfer Rates at $domain</title>
</head>
<body>\n
<pre><b><font size=-1>LDAS Data Socket Roundtrip Transmission Rates at $domain</font></b></pre>
<font size=-2>
<table cellpadding=1>
<tr>
    <th align=left width=200>Test Period:</th>
    <td width=300>$currtime</td>
</tr>
<tr>
    <th align=left width=200>Input data:</th>
    <td width=300>Ilwd $withcont container</td>
    <td>
</tr>
<tr>
    <th align=left width=200>Socket type:</th>
    <td width=300>[string toupper ${thread} 0 0]</td>
    <td>
</tr>
<tr>
    <th align=left width=200>Sample Size:</th>
    <td width=300>$samples</td>
    <td>
</tr>
<tr>
    <th align=left width=200>Platforms:</th>
    <td width=300>$platformC <-> $platformS $loopback</td>
    <td>
</tr>
<tr>
    <th align=left width=200>LDAS software:</th>
    <td width=300>$version, Sun Solaris 2.7 for Sun Sparc and RedHat Linux 6.1 for Intel Pentium</td>
    <td>
</tr>
</table></font>"

    puts $fhtml $heading
}

proc writeHTMLTableHeader {fhtml timetypevar} {
    puts $fhtml "<p>"
    puts $fhtml "<table border type=1>"
    puts $fhtml "<caption align=center><b>$::desc($timetypevar)</b></caption>"
    puts $fhtml "<font size=-1>"
    puts $fhtml "<tr>"
    puts $fhtml "    <th align=left width=100>Type \\ dims</th>"
    puts $fhtml "    <th width=100 align=right>1</th>"
    puts $fhtml "    <th width=100 align=right>10</th>"
    puts $fhtml "    <th width=100 align=right>100</th>"
    puts $fhtml "    <th width=100 align=right>1000</th>"
    puts $fhtml "    <th width=100 align=right>10000</th>"
    puts $fhtml "    <th width=100 align=right>100000</th>"
    puts $fhtml "    <th width=100 align=right>1000000</th>"
    puts $fhtml "</tr>"
}

main

