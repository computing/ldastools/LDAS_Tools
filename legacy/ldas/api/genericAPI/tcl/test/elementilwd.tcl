#!/ldcg/bin/tclsh
#!/bin/sh
# \
exec wish "$0" ${1+"$@"}

set types {
    char_s
    char_u
    int_2s
    int_2u
    int_4s
    int_4u
    int_8s
    int_8u
    real_4
    real_8
    lstring_2
    lstring_4
    lstring_8
    lstring_16
    complex_8
    complex_16
}

set dims {
    1
    10
    100
    1000
    10000
    100000
    1000000
}

set ilwdfmt ""
set basedir ""

;## Processs command-line arguments
for {set idx 0} {$idx < $::argc} {incr idx} {
    set arg [lindex $::argv $idx]
    switch -exact -- $arg {
        -D { set basedir [lindex $::argv [incr idx]] }
        -f { set ilwdfmt [lindex $::argv [incr idx]] }
        -t { set types [lindex $::argv [incr idx]] }
        -d { set dims [lindex $::argv [incr idx]] }

        default { set ilwdfmt [lindex $::argv $idx] }
    }
}

if {![string length $ilwdfmt]} {
    puts stderr "Usage: $::argv0 \[\[-f\] binary | ascii\] \[-D dir\] \[-t types\] \[-d dims\]"
    exit 1
}

if {![string length $basedir]} {
    set basedir $ilwdfmt
}

if {![file exists $basedir]} {
    catch {file mkdir $basedir} err
}

foreach type $types {
    puts -nonewline stdout "$type: "
    flush stdout
    foreach dim $dims {
        puts -nonewline stdout "$dim "
        flush stdout
        if {[catch {
                exec ./elementilwd $dim $type $ilwdfmt >[file join $basedir ${type}_${dim}.ilwd]
            } err]} {
            error $err $::errorInfo
        }
    }
    puts stdout ""
    flush stdout
}

