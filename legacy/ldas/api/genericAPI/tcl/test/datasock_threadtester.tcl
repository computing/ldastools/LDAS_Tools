#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

## ******************************************************** 
## datasock_tester.tcl
##
## Exercise data socket for various ilwd types
##
## Release Date: Not Available
##
##  Test options
##  dims,dims,dims... or all
##  char_s,char_u,... or all
##  with-container or default to no-container       
##  server - the one to send data to
##  localhost - myself   
## ******************************************************** 

;#barecode

set auto_path "/ldas/ldas-0.0/lib $auto_path"
puts "auto_path=$auto_path"

set localhost [ exec uname -n ]
if	{ [ regexp {^ldas} $localhost ] } {
	set localhost "gateway"
}
puts "localhost=$localhost"
set API [ lindex $argv 0 ]
set modestr [ lindex $argv 1 ]

if  { [ regexp {^nothread$} $modestr ] } {
    set mode 0
} else {
    set mode 1
}
puts "mode=$mode"
;## modified stats to handle mean of 0 for cpu time
package require generic 
#source /ldas/ldas-0.0/lib/genericAPI/genericAPI.tcl

;##package require datasock_test
source datasock_threadtest.tcl
source datasock_thread.tcl

source ./LDASapi.rsc 
set port [ set ${API}(data) ]
puts "port=$port"
puts "[array get $::API]"
;## suppress logging
proc addLogEntry { args } { return {} }

set LDAS [ pwd ] 

getArgs 

;## suppress logging
proc addLogEntry { args } { return {} }

## ******************************************************** 
##
## Name: operator 
##
## Description:
## Handles requests at the metadata API's operator socket.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket.
##
## Comments:
## This is the only entry point exposed on the operator
## socket.
## See the asstMgr.tcl procedure ${name}::sockhandler
## for the format of the reply and the significance of the
## return values.
## The variable ::metadata::errlvl may be modified by a called
## procedure generating an error condition.

proc operator { cid } {        
     set errlvl 0
     set result "done!"
     set cmd [ cmd::result $cid ]
     regexp {^\{(.*)\}$} $cmd cmd cmd
     regexp {^\"(.*)\"$} $cmd cmd cmd
     puts "cmd=$cmd"
	 set ::sid $cid
     if { [ catch { 
        set result [ eval $cmd ]
        } errmsg ] } {
        set result $errmsg
     }
     catch { reply $cid "$errlvl\n$result" }
     catch { close $cid }
}

## ******************************************************** 
##
## Name: emergency 
##
## Description:
## Provides the functions required to handle requests of
## the frame API's emergency socket.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket.
##
## Comments:
## This is the only entry point exposed on the emergency
## socket.

proc emergency { { cid "" } } {
    set cmd [ cmd::result $cid ]
    regexp {^\{(.*)\}$} $cmd cmd cmd
    regexp {^\"(.*)\"$} $cmd cmd cmd
    if  { [ catch {
        set result [ eval $cmd ]
        } err ] } {
        puts "emergency socket eval err: $err"
    }
    catch { close $cid }
    puts "result=$result"
}
## ******************************************************** 

## ******************************************************** 
##
## Name: bgerror 
##
## Description:
## Any Tcl script which has an event loop must have a
## bgerror procedure defined.  Tk defines a bgerror,
## and you should too!
##
## Usage:
## Called internally by Tcl.  See the bgerror man page.
## Comments:
## Should it do anything else??

proc bgerror { msg } {
     global errorInfo
     addLogEntry "$msg; errorInfo=$errorInfo"
     puts stderr "${::API}API bgerror: errorInfo=$errorInfo,$msg"
}
## ********************************************************

proc reply { cid msg } {
     puts $cid $msg
     ;##flush $cid
     return {}
}

proc testExit {} {
	puts "in testExit"
	if { [ catch { 
        sock::closeDataServer 
		closeListenSock operator
        closeListenSock emergency
       } err ] } {
        puts "testExit error: $err" 
    }
	exit
}

;## loop to create one accept thread each time
proc handleDataSock { } {

     set tlist [ string trim [ getThreadList ] "{}" ]
     set seqpt ""
     ;## only runs at startup
     if { ! [ llength $tlist ] } {
        set tid [ acceptDataSocket_t $::serverp ]
		array set ::threadtype [ list $tid "A" ]		 
        set tlist [ string trim [ getThreadList ] "{}" ]
     }
     puts stderr "handleDataSock, numThreads=[ expr [ llength $tlist] /2 ]"
     puts stderr "handleDataSock, tlist=$tlist"
	 puts stderr "threadtype=[ array get ::threadtype ]"
	 ;##puts stderr "argms=[ array get ::argms ]"
     foreach { tid status } $tlist {
	 	if	{ [ catch {
        if { [ regexp {FINISHED} $status ] } {
		   regsub {^_[0]+} $tid {_} tid
           if 	{ [ regexp {^A$} $::threadtype($tid) ] } {	
                set seqpt "threadRecv $tid"
          		threadedRecv $tid
                set seqpt "acceptDataSocket_t $::serverp"
           		set ntid [ acceptDataSocket_t $::serverp ]
				array set ::threadtype [ list $ntid "A" ]
		   } else {
		   		if	{ [ info exist ::argms($tid) ] } {
					puts "args for $tid: $::argms($tid)"
                    set seqpt $::argms($tid)
					eval $::argms($tid) 
				}			 	
          }
		} 
		} err ] } {
			puts stderr "handleDataSock: $tid $seqpt,$err"
		}
     }

     ;## 1000 caused a lot of threads in receiver to not complete
     ;## until sender terminates but 600 results in all completed
     after 400 handleDataSock
}    

## ********************************************************
## main line
##
	set ::state 0
	puts "API=$API"
    openListenSock operator
    openListenSock emergency
	set ::serverp [ createServerSocket $port $localhost ]
	handleDataSock
    ;##openDataSock data 
    vwait enter-mainloop
;#end
