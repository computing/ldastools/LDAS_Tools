## ********************************************************
## queue.tcl version 1.0
##
## Provides queue saving, restoring and perusal functions.
##
## Release date: 99.06.01
##
##
## This namespace is private and should not be "imported".
##
## NO COMMENTS SHOULD APPEAR IN THE INPUT!!
##
## ********************************************************

;#barecode

package provide queue 1.0

if { ! [ info exists genericAPI ] } {
   set msg    "queue.tcl is a module of\n"
   append msg "the genericAPI.tcl and provides\n"
   append msg "no independent functionality."
   return -code error $msg
   }

namespace eval queue { }

;#end


## ******************************************************** 
##
## Name: queue::save
##
## Description:
## Write queues to disk file.  Does not wipe queues.
## 
## Parameters:
## qarray - array of queues. Queues are Tcl lists.
##
## Usage:
##        queue::save
## Comments:
## Used for emergency shutdown where resurrection is
## possible or where state logging is wanted.
## saves to a file in the logs subdirectory called
## ${::API}.queue
##

proc queue::save { { qarray QUEUE } } {
     set data {}
     foreach queue [ array names ::$qarray ] {
          append data "$queue\n"
          append data [ list [ set ::${qarray}($queue) ] ]
          append data "\n"
          }
     set fname ${::API}.queue
     set fname [ file join $::LDASLOG $fname ]
     set fid [ open $fname w ]
     puts $fid $data
     close $fid
     cryptFileInPlace $fname $::MGRKEY
     addLogEntry "the QUEUE array has been saved to $fname."
     return {}       
}
## ******************************************************** 

## ******************************************************** 
##
## Name: queue::restore
##
## Description:
## Reads last queue state saved into active queues.
##
## Parameters:
## qarray - array of queues. Queues are Tcl lists.
##
## Usage:
##
## Comments:
## Probably useless except as a debugging tool!!
## Will currently append queues -- needs handling
## for non null queues.
## :TODO:

proc queue::restore { { qarray QUEUE } } {
     set queue {}
     set regpat [ join [ split [ array names ::$qarray ] ] $)|(^ ]
     set regpat "\(\^$regpat\$\)"
     set filename [ file join $::LDASLOG ${::API}.queue ]
     if { [ file exists $filename ] } {
        set data [ decryptFile $filename $::MGRKEY ]
        foreach datum [ split $data "\n" ] {
           set datum [ string trim $datum ]
           if { [ regexp $regpat $datum ] } {
              set queue $datum
              continue
           }
           if { [ llength $queue ] } {
              append ::${qarray}($queue) "$datum\n"
           }
        }
        addLogEntry "$::API QUEUE array restored!"
     }   
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: queue::dump
##
## Description:
## Dumps queue contents into a list
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc queue::dump { { queue "" } { qarray QUEUE } } {
     set data [ list [ set ::${qarray}($queue) ] ]
     ;## do some simple preprocessing
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: queue::html
##
## Description:
## View the queue of your choice!
## Will produce html with colorized text.  As soon as we
## decide what a queue dump should look like!
##
## Parameters:
##
## Usage:
##       set qdata [ queue::html queuename ]
##
## Comments:
## In the works...

proc queue::html { { queue "" } } {
     
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: queue::users
##
## Description:
## Forces an update of the manager queue ::QUEUE(USERS).
## Only new or modified user info will be affected unless
## the flag "reset" is passed.
##
## Parameters:
##
## Usage:
##
## Comments:
## This can be used to get user info in API's other than
## the manager.  This can be disabled by uncommenting the
## commented code.

proc queue::users { { flag update } } {
     #if { ! [ string match manager $::API ] } {
     #   return {}
     #}
     
     if { [ info exists ::ENCRYPTED_USERS_QUEUE ] } {
        if { [ string equal 1 $::ENCRYPTED_USERS_QUEUE ] } {
           set queue users.queue.crypt
           set crypt 1
        } else {
           set queue users.queue
           set crypt 0
        }
     } else {
        set queue users.queue
        set crypt 0
     }
     
     if { [ catch {
        ;## if the users queue does not exist, or a reset
        ;## has been requested, initialise it.
        if { ! [ info exists ::QUEUE(USERS) ] || \
               [ string equal -nocase reset $flag ] } {
           set ::QUEUE(USERS) [ list ]
        }
        
        ;## the distributed unencrypted password file
        set fn [ file join $::LDAS lib managerAPI users.queue ]

        ;## find the file in the current directory or in the
        ;## managerAPI's library directory
        if { [ file readable $queue ] && \
             [ file size $queue ] } {
           set fname $queue
        ;## if the password file is bad, but a .bak exists, use it
        } elseif { [ file readable ${queue}.bak ] && \
                   [ file size ${queue}.bak ] } {
          file copy -force ${queue}.bak $queue
          set fname $queue
        ;## if an unencrypted password file exists and no encrypted
        ;## file or .bak exists, crypt and use the unenecrypted file.
        } elseif { $crypt && \
                   [ file readable users.queue ] && \
                   [ file size users.queue ] } {
           file copy -force users.queue $queue
           cryptFileInPlace $queue $::MGRKEY
           set fname $queue
        ;## last condition is to fall back to the distributed file.
        } elseif { $crypt && \
                   [ file readable $fn ] && \
                   [ file size $fn ] } {
           set crypt 0
           file copy -force $fn users.queue
           set fname [ pwd ]/users.queue
        ;## transitioning from encrypted to non-encrypted
        } elseif { ! $crypt && [ file readable ${queue}.crypt ] } {
           set tmp [ dumpFile ${queue}.crypt ]
           set tmp [ decrypt $tmp $::MGRKEY ]
           set fid [ open $queue w 0600 ]
           puts $fid $tmp
           ::close $fid
           ::unset tmp
           set fname $queue
        ;## generate meaningful error on bad distributed file
        } else {
           ;## open the file ::LDAS/lib/managerAPI/$queue
           set fname [ file join $::LDAS lib managerAPI $queue ]
           if { ! [ file readable $fname ] || ! [ file size $fname ] } {
              return -code error "users file '$fname' not readable!"
           }
        }

        ;## force permissions on file
        if { [ catch {
           file attributes $fname -permissions 0600
        } err ] } {
           set subject "$::LDAS_SYSTEM ${::API}API error!"
           set msg "Subject: ${subject}; Body: $err"
           addLogEntry $msg email
        }
        
        set users [ dumpFile $fname ]
        ;## the user entries are each on a line
        if { $crypt } {
           set users [ decrypt $users $::MGRKEY ]
        }
        set users [ split $users "\n" ]

        foreach user $users {

           ;## discard comments, blank lines, etc
           set user [ string trim $user ]
           if { ! [ string length $user ] } { continue }
           if { ! [ regexp {^-} $user   ] } { continue }
           
           ;## format sanity check
           if { [ llength $user ] != 13 } {
              addLogEntry "invalid $queue entry: $user" 2
           }
           
           ;## OOPS, new LDAS system and the default user has not
           ;## been configured!
           if { [ string equal ldas [ lindex $user 1 ] ] && \
              ! [ string length [ lindex $user 3 ] ] } {
              set msg    "The user file: '$fname' contains an invalid "
              append msg "entry for the default user: 'ldas'.\n This "
              append msg "probably means you did not read the "
              append msg "Run_LDAS.html LDAS setup instructions carefully "
              append msg "and do not have any valid users configured for "
              append msg "your new LDAS system!\n You MUST put the md5sum "
              append msg "of the password for user ldas in the fourth field "
              append msg "of the user ldas entry in the file $fname!\n"
              append msg "encryption has been disabled until next restart."
              puts stderr $msg
              addLogEntry $msg email
              return -code error $msg
           }
           
           ;## if we are updating the users queue
           if { [ string equal -nocase update $flag ] } {
              ;## get the username
              regexp -nocase -- {-name\s+(\S+)} $user -> name
              ;## if the user is already in the queue, skip it
              if { [ regexp -- -name\\s+$name\\s+ $::QUEUE(USERS) ] } {
                 continue
              ;## otherwise add it to the queue   
              } else {
                 lappend ::QUEUE(USERS) $user
              }
           ;## otherwise if we are doing a reset, add the user
           } else {
              if { [ string equal -nocase reset $flag ] } {
                 lappend ::QUEUE(USERS) $user
              }   
           }   
        }
        
        if { ! [ llength $::QUEUE(USERS) ] } {
           return -code error "file '$fname' contains no users!"
        }
     } err ] } {
        catch { ::close $fid }
        return -code error "[ myName ]: $err"
     }
}
## ********************************************************

