## ********************************************************
## log.tcl version 1.1
##
## Provides severity flagged error handling for tcl.
##
## Release date: 99.03.16
##
## Three levels of messaging / error handling are defined:
## 1. Message logging, which writes a message only to the
##    local logfile defined by LOCAL_LOG_FILE in the
##    "current" context, flagging it to level "0".
## 2. Warning logging, which logs locally, flagging to
##    level "1".
## 3. Error logging, which logs locally with flag 2, and
##    logs to the manager througwww.googl; the manager's emergency
##    port.  The manager may then initiate action.
## ;#ol
##
## This namespace is private and should not be "imported".
##
## ********************************************************

;#barecode

package provide log 1.0

package require log_mt

if { ! [ info exists genericAPI ] } {
     set msg    "log.tcl is a module of\n"
     append msg "the genericAPI.tcl and provides\n"
     append msg "no independent functionality."
     return -code error $msg
     }

namespace eval log {
                    set nested 0
                    }

;#end

## ******************************************************** 
##
## Name: log::mgr
##
## Description:
## Connect with the managers' emergency services.
## Usage:
##
## Comments:
## reportError is a function of the managers' emergency
## listener.
##
## Since this calls sock::open, it cannot
## log level 2 errors directly.  Of course, the manager
## is probably unreachable if sock::open is slinging log
## entries.
## This probably needs some refinement.

proc log::mgr { { data "" } } {
     
     set data "$::API:[ uplevel myName ]: $data"
     
     if { [ catch {
        set sid [ sock::open manager emergency ]
        puts  $sid "$::MGRKEY NULL NULL addLogEntry \"$data\""
        flush $sid
        close $sid
        } err ] } {
        set msg    "log::mgr:\n"
        append msg "Error sending log entry to manager.\n"
        append msg $err
        uplevel 2 [ return -code error $msg ]
        }
     
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::open 
##
## Description:
##
## Usage:
##
## Comments:
##

proc log::open { { filename "" } { mode "a+" } } {
     set preamble {}
     set site [ list ]     
     set host $::env(HOST)

     regexp {\-(wa|la|dev|test|cit|mit|uwm)} $::LDAS_SYSTEM -> site
     switch -exact $site {
            wa { set site Hanford }
            la { set site Livingston }
           mit { set site LDAS_MIT } 
           dev { set site Caltech-Dev }
          test { set site Caltech-Test }
           cit { set site Caltech-CIT }
           uwm { set site LDAS_UWM }
     default { set site $::LDAS_SYSTEM }
     }
     
     ;## if a machine returns "" from domainname
     if { ! [ string length $site ] } {
        set site "DOMAIN UNRESOLVED!"
     }
     
     if { ! [ regexp {LDAS([a-z]+)\.log\.html} $filename -> api ] } {
        set api $::API
     }  
     
     set title "The $api API on $host at $site"
     if { [ regexp -nocase -- "$::RUNCODE\\d+" $filename jobid ] } {
        set title "The $jobid Assistant Log File"
     }   
     
     if { ! [ info exists ::${api}_SPECIAL_LOG_HEADER ] } {
        set ::${api}_SPECIAL_LOG_HEADER [ list ]
     }
     
     if { ! [ file exists $filename ] } {
        set preamble "<HTML>
<HEAD>
<TITLE>$title</TITLE>
</HEAD>
<BODY BGCOLOR=\"#DDDDDD\" TEXT=\"#000000\">
<h3>$title</h3>
<b><i>Legend:</i></b><br>
<img src=\"ball_green.gif\" width=\"14\" height=\"12\" alt=\"green ball\" title=\"green ball\">
Normal status or debugging message
<br>
<img src=\"ball_yellow.gif\" width=\"14\" height=\"12\" alt=\"yellow ball\" title=\"yellow ball\">
Notable condition which may be a non-fatal error
<br>
<img src=\"ball_orange.gif\" width=\"14\" height=\"12\" alt=\"orange ball\" title=\"orange ball\">
Error condition not fatal to job
<br>
<img src=\"ball_red.gif\" width=\"14\" height=\"12\" alt=\"red ball\" title=\"red ball\">
Error condition fatal to job
<br>
<img src=\"ball_blue.gif\" width=\"14\" height=\"12\" alt=\"blue ball\" title=\"blue ball\">
Notable condition which is not an error
<br>
<img src=\"ball_purple.gif\" width=\"14\" height=\"12\" alt=\"purple ball\" title=\"purple ball\">
Currently undefined
<br>
<img src=\"mail.gif\" width=\"27\" height=\"16\" alt=\"email\" title=\"email\">
Condition requires email notification of the responsible administrator of this API
<br>
<img src=\"telephone.gif\" width=\"27\" height=\"27\" alt=\"telephone\" title=\"telephone\">
Condition requires phone notification of the responsible administrator of this API
<p>[ set ::${api}_SPECIAL_LOG_HEADER ]
<b><i>Link:</i></b>
<a href=\"APIstatus.html\">API Status Page for $site</a>
<p>"
     regsub -all -- {\n+} $preamble { } preamble
     }
     
     set fid [ ::open $filename $mode ]
     puts $fid $preamble
     file attributes $filename -permissions 0644
     return $fid
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::close 
##
## Description:
##
## Usage:
##
## Comments:
##

proc log::close { filename } {
     set msg {}
     switch -regexp -- $filename {
        {file\d+} {
                    foreach v [ info vars ::log::* ] {
                       foreach { fid ino fnm } [ set $v ] { break }
                       if { [ string match $fid $filename ] } {
                       if { ! [ file exists $fnm ] } {
                          set msg "$fnm no longer exists! "
                             append msg "orphan channel \"$fid\" closed."
                          } else {
                             set msg "$fnm ($fid) closed"
                          }   
                          set tail [ lindex [ log::name $fnm ] 0 ]
                          break
                          }
                       set fid $filename   
                       set msg "unknown orphan channel \"$fid\" closed."
                       }
                    }
           default {          
                    set tail [ lindex [ log::name $filename ] 0 ]
                    if { [ info exists ::log::$tail ] } {
                    foreach {
                          fid inode filename 
                       } [ set ::log::$tail ] { break }
                       if { ! [ file exists $filename ] } {
                       set msg "$filename no longer exists! "
                          append msg "orphan channel \"$fid\" closed."
                       } else {
                          set msg "$filename ($fid) closed"
                       }
                    } else {
                       set msg "file \"$filename\" already closed."
                       }
                    }
                 } ;## end of switch   
     catch { ::close $fid }
     catch { unset ::log::$tail }
     return $msg   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::stat
##
## Description:
## This procedure will handle corrupted or missing log
## files transparently.  It should obviate missing log
## file problems due to erroneous re-opening of API's.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc log::stat { { filename "" } } {
     if { [ catch { 
        foreach { tail filename } \
           [ log::name $filename ] { break }

        if { ! [ info exists ::log::$tail ] } {
           set ::log::$tail [ list {} {} $filename ]
        }   
     
        ;## if the file exists, it will have an inode,
        ;## otherwise we will set the inode to 0.
        set inode 0
     
        catch {
           file stat $filename fstat
           set inode $fstat(ino)
        }   
     
        foreach { fid ino fnm } \
           [ set ::log::$tail ] { break }
     
        ;## inode is 0 or file has been replaced
        if { $inode != $ino } {
           set msg [ log::close $fid ]        
           set fid [ log::open $filename ]
           file stat $filename fstat
           set ::log::$tail [ list $fid $fstat(ino) $filename ]
        
           ;## if we were called by archiveLog, don't repeat msg.
           if { ! [ string match [ uplevel myName ] archiveLog ] } {
              ;## to avoid duplication on shutdown.
              if { ! [ regexp {()} $msg ] } {
                 addLogEntry "$msg. This file replaces it."
              }   
           }   
        }
     } err ] } {
     	if	{ [ file exist $filename ] } {
        	addLogEntry $err red
       	} else {
        	puts "log::stat error $err"
       	}
        return -code error $err
     }
     return $tail
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::lock
##
## Description:
## Prevents a second instance of any given API, assistant,
## or slave interpreter from starting in the filespace of
## an already running API, assistant or slave.
##
## Parameters:
##
## Usage:
##
## Comments:
## If log::lock is called by a shutdown function, it will
## delete the existing locks associated with the current
## API.

proc log::lock { } {
     set globpat \
        [ file join $::env(RUNDIR) ${::API}API .$::API.*.lock ]
     set locks [ glob -nocomplain $globpat ]
     
     ;## if we were called by a shutdown command
     ;## only a fully running API can ever manage
     ;## to issue a shutdown.
     if { [ regexp {sHuTdOwN} [ uplevel myName ] ] } {
        file delete -force -- $locks
        return {}
        }
     
     if { [ llength $locks ] > 1 } {
        puts stderr "multiple $::API API lock files exists!: $locks"
        exit
     }
     
     if { [ llength $locks ] == 1 } {
        if { [ regexp [ pid ] $locks ] } {
           return {}
        } else {   
           puts stderr "$::API API lock file exist!: $locks"
           exit
        }   
     } else {
        set lockname \
           [ file join $::env(RUNDIR) ${::API}API .$::API.[ pid ].lock ]
        file mkdir [ file dirname $lockname ]
        set fid [ ::open $lockname w ]
        
        ;## proc file system may not be sane at startup!
        if { [ catch {
           set psinfo [ list ]
           set psinfo [ ps::ps ${::API}API ]
        } err ] } {
           set psinfo $err 
        }
        
        puts $fid $psinfo
        close $fid
        file attributes $lockname -permissions 0600
        catch { ::exec sync }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::name
##
## Description:
## Forces correct format of a log file name.
## Returns a list of the log file "tail" name and the full
## filename.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc log::name { { filename "" } } {
     
     if { ! [ regexp -nocase {[a-z]+} $filename ] } {
        if { [ catch { set filename $::API } err ] } {
           set caller [ uplevel myName ]
           return -code error "$caller:[ myName ]: $err"
        }
     }

     set tail [ file rootname $filename ]
     set tail [ file rootname $tail ]
     set tail [ file tail $tail ]
     regsub LDAS $tail {} tail
     set filename LDAS${tail}.log.html
     set filename [ file join $::LDASLOG $filename ]
     return [ list $tail $filename ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::fixEmailSubject
##
## Description:
## Rectifies common errors in the formatting of LDAS email
## class error messages by forcing the subject line to
## contain the LDAS system i.d. and the API where the
## message originates.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc log::fixEmailSubject { msg } {
     
     if { [ catch {
        set system [ string toupper $::LDAS_SYSTEM ]
        if { ! [ regexp {^LDAS} $system ] } {
           set system "LDAS $system"
        }
         set bare "Subject: $system ${::API}API "
         foreach [ list subj body ] [ split $msg ";" ] { break }
         set subj [ string trim $subj ]
         set body [ string trim $body ]
         regsub {^Subject:\s+}        $subj {} subj
         regsub {^LDAS\s+}            $subj {} subj
         regsub ^${::LDAS_SYSTEM}\\s+ $subj {} subj
         regsub {^LDAS\s+}            $subj {} subj
         regsub ^${::API}(API)?\\s+   $subj {} subj
         set msg "$bare${subj}; $body"
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $msg
}
## ******************************************************** 

## ******************************************************** 
##
## Name: addLogEntry
##
## Description:
## This is an exposed function of the "log" namespace.
##
## Usage:
##        addLogEntry $msg $errlvl $caller $time $filename
##
## Where only "msg" is required.
## "errlvl" defaults to "0".
## "caller" defaults to the procedure which called
## addLogEntry, found by doing an "uplevel myName".
## "time" will be calculated at the time of the call
## if not supplied.
##
## Comments:
## The "log" namespace throws an exception when:
## 1. The LOCAL_LOG_FILE variable is not set.
## 2. The log file is not open.
## ;#ol

proc addLogEntry { { msg "" } { errlvl 0 } { caller "" } { time "" } { filename "" } } {
     
     set gif [ list ]
     set w   [ list ]
     set h   [ list ]
     set alt [ list ]
     
     ;## ignore all log entries of any color code appearing
     ;## in the resource variable ::IGNORE_THESE_LOG_BALL_COLORS
     if { [ info exists ::IGNORE_THESE_LOG_BALL_COLORS ] } {
        set ignore $::IGNORE_THESE_LOG_BALL_COLORS
        if { [ lsearch $ignore $errlvl ] != -1 } {
           return {}
        }
     }
    
     set jobid IDLE
     ;## bit of nuttiness to protect the manager log
     ;## from jobid redundancy.
     if { [ uplevel info exists jobid ] } {
        if { [ string length [ uplevel set jobid ] ] } {
           set jobid [ uplevel set jobid ]
        }   
     } else {
        if { [ info exists ::jobid ] } {
           if { [ string length $::jobid ] } {
              set jobid $::jobid
           }
        }    
     }   
     
     ;## arg list is empty
     #if {   [ regexp {^$} $msg ] } {
     #   set msg "no message passed for logging"
     #}
     
     ;## calling proc not named
     if { [ string match "" $caller ] } {
        if { [ catch {
           set caller [ uplevel myName ]
        } err ] } {
           set errlvl 1
           set caller $::argv0
        }
     }
     
     ;## timestamp not provided, get current time.
     ;## serious problem if can't get current time!
     if { ! [ regexp {\d{9}} $time ] } {
        if { [ catch {
           set time [ gpsTime ]
        } err ] } {
           set time 000000000
           set tmpmsg $err
           set msg "$tmpmsg (original msg: $caller : $msg)"
           set caller [ myName ]
           set errlvl 2
        }   
     }
     
     ;## PR #2658 - emails with badly formed subjects!
     if { [ regexp -nocase -- {mail} $errlvl ] } {
        set msg [ log::fixEmailSubject $msg ]
     }
     
     ;## remove bad characters
     regsub -all -- {\<} $msg {\&lt;} msg
     regsub -all -- {\>} $msg {\&gt;} msg
     
     ;## special handler allowing breaks to be embedded
     ;## in log entries as the string "!br!"  this is not
     ;## 100% safe, so don't make general use of it!
     regsub -all -- {\!br\!} $msg {<br>} msg

     ;## remove ::MGRKEY and user paswords
     if { [ info exists ::MGRKEY ] } {
        regsub -all -- "(\[\\s\\'\\\{\])$::MGRKEY " $msg {\1::MGRKEY } msg
     }
     
     regsub -all -- {-password\s+[^\s]+} $msg {-password *****} msg
     
     ;## here we create links around all URL's that look sane
     ;## first exclude all bad chars
     set bad_rx \[^\\\s\\\/\\\'\\\"\\\}\\\{\\\)\\\(\\\]\\\[\\\>\\\<\]+
     ;## then make a pattern that looks like a real URL including
     ;## a server AND directory or file spec - tricky!
     set sane_rx (ftp|http):\\\/\\\/$bad_rx\(\\\/$bad_rx\\\/?\)+
     regsub -all -- $sane_rx $msg {<a href='&'>&</a>} msg
    
     #foreach { gif w h alt } [ log::icon $errlvl $time ] { break }
    
     #set gif "<img src=\"$gif\" width=\"$w\" height=\"$h\" alt=\"$alt\" title=\"$alt\">"
     #set time "<b>$time</b>"
     #set caller "<b><i><font color=\"red\">$caller</font></i></b>"
     # set jobinfo [ log::linkJobid $jobid ]
     #set msg "<tt>[ log::uncrlf $msg ]</tt>"
     # set msg [ log::uncrlf $msg ]
     # log::local "$gif $time $jobinfo $caller $msg<br>" $filename
     queueLogEntry $msg $errlvl 0 $caller $jobid $time $filename
}
## ********************************************************

## ******************************************************** 
##
## Name: debugPuts
##
## Description:
## Used to make optional puts's and addLogEntry's based on
## debugging level. setting DEBUG to 0xdeadbeef turns on
## putsing to stderr.
## An optional special handler may be specified as a third
## command line argument.
##
## Parameters:
## msg -   text to be displayed/logged
## level - optional level modifier
## Usage:
##
## Comments:
## Convenience routine for making debugging switchable.
## returns in 20 microseconds if ::DEBUG < 0.
## If ::DEBUG < 0 then the debug level cannot be
## used to cause debugging output.

proc debugPuts { { msg "" } { dbglvl 0 } { handler "" } { logfile "" } } {
     if { $::DEBUG < 0 } { return {} }
    
     ;## get jobid from caller if it exists
     if { [ uplevel info exists jobid ] } {
        set jobid [ uplevel set jobid ]
     }   
     
     ;## get global debug level if none is provided
     if { ! $dbglvl } { set dbglvl $::DEBUG }
     ;## if debugging is on...
     if { $dbglvl } {
        if { [ catch {
           set caller [ uplevel myName ]
        } err ] } {
           set caller $::argv0
        }   
        if { [ regexp {^$} $handler ] } {
           set handler "addLogEntry"
        }
        if { [ string length $msg ] } {
           if { [ string equal $handler "addLogEntry" ] } {
                 addLogEntry $msg 0 $caller {} $logfile
           } else {          
              if { [ catch { uplevel $handler [ list $msg ] } err ] } {
                 addLogEntry "$handler: $err (msg: $msg)" 
              }
           }
           if { $dbglvl == [ expr 0xdeadbeef ] } {
              puts stderr $msg
           }
        }
     }   
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: closeLog
##
## Description:
## Wrapper for log::close to maintain API backwards
## compatibility.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc closeLog { { file "" } } {
     set msg [ log::close $file ]
     debugPuts $msg
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dumpLog
##
## Description:
## Open a log file, read it's contents into a string,
## and return the string for array processing.
## The error condition on failure to open the file
## should never occur, but will be handled transparently
## if it does!
##
## Usage:
##        dumpLog log
##
## Comments:
## returns a string containing the contents of the
## entire log file.

proc dumpLog { { log "" } } {
     set log [ lindex [ log::name $log ] 1 ]
     set data [ dumpFile $log ]
     return $data              
}
## ********************************************************

## ******************************************************** 
##
## Name: archiveLog 
##
## Description:
## Store current log file in local archive, and maintain
## 2 "rolling logs".
##
## Usage:
##        archiveLog [$name]
## Where the logname is optional, defaulting to the local
## log file appropriate for the API.
##
## Comments:
## The archive will be placed in:
## $LDASLOG/archive/LDAS${name}.$time
##

proc archiveLog { { tail "" } } {
     return "Depricated call as this is now done by C++ logging"
     
     foreach { tail logfile } [ log::name $tail ] { break }
    
     if { [ file exists $logfile ] } {
        set time [ gpsTime [ file mtime $logfile ] ]
        set arc [ file join $::LDASARC ${tail}API LDAS${tail}.$time ]
        
        set dir $::LDASARC/${tail}API 
        if { ! [ file isdirectory $dir ] } {
           file mkdir $dir
           file attributes $dir -permissions 0755
           gifBalls $dir
        }

        ;## make sure browsers can browse archived logs!
        set htaccess $dir/.htaccess
        if { ! [ file exists $htaccess ] } {
           set fid [ open $htaccess w 0644 ]
           puts $fid "DefaultType text/html"
           ::close $fid
        }

        set msg [ log::close $logfile ]
        catch { file rename -force $logfile $arc } err0
        catch { archiveIndex $arc }    
        ;## an API logfile gets rotated or archived.
        set base $::LDASLOG/LDAS$tail
        ;## the file must be deleted even if it is a dead link,
        ;## which fails the 'file exists' test.
        file delete -force $base.2.html
        catch { file rename -force $base.1.html $base.2.html }
        ;## the syntax is exactly backwards from UNIX,
        ;## here the name of the target comes last.
        catch { file link -symbolic $base.1.html $arc }   
     }
     
     log::stat $tail
     
     if { [ info exists msg ] } {
        if { ! [ string length $err0 ] } {
           set err0 "archived as $arc"
        }   
        addLogEntry "$msg ($err0)"
     }   
     
     return {}   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::updateArchive
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## just have manager update the archiveIndex
## to prevent race condition and no need for file locking

proc log::updateArchive { data } {

 	if	{ [ catch {        
        set index [ file join $::LDASARC archiveIndex ]
       	set indexfid [ ::open $index a+ 0644 ]
        ::puts $indexfid $data
        ::close $indexfid
        set msg "updated archiveIndex with '$data'"
        addLogEntry $msg purple
    } err ] } {
    	addLogEntry $err red
    } 
    return $msg 
}

## ******************************************************** 
##
## Name: archiveIndex
##
## Description:
## Create an index entry in the log archive index file
## named "archiveIndex" with the name of the log file,
## the name of the first job referenced in it, and the
## name of the last job referenced in it.  The form of
## the entry is simply:
## filename firstjobid lastjobid
##
## Parameters:
##
## Usage:
## Called by archiveLog
##
## Comments:
## Can be called directly with the argument "rebuild"
## to rebuild the index.  Any existing index will be
## removed.
## if filename is current file e.g. .log.html this
## only returns the job limit and start time but does not
## update the archiveIndex so no need to execute
## this in manager
## dont wait for manager response as this would hold up
## api response to manager on startup

proc archiveIndex { filename { runcode "" } } {

    if  { ![ string match manager $::API ] &&
          ! [ string match *bgcntlmon* $::argv0 ] &&
          ! [ regexp {html} [ file extension $filename ] ] } {
        if  { [ catch {
            set msg "archiveIndex $filename"
            getEmergData manager [ list $msg ] 0 
        } err ] }  {
            addLogEntry $error red
            return -code error $error
        }
        return $result
     }
     
     set min -1
     set max -1
     set index [ file join $::LDASARC archiveIndex ]
     
     if { ! [ string length $runcode ] } {
        set runcode $::RUNCODE
     }
     
     ;## recurse and rebuild logs
     if { [ string match rebuild $filename ] } {
        set filename {}
        catch { file delete $index }
        set logs [ glob $::LDASARC/*API/LDAS* ]
        foreach log $logs {
           if { [ string match rebuild $log ] } { continue }
           if { ! [ file exist $log ] } {
           	  addLogEntry "$log does not exist" red
              continue
           }
           if { [ string length $log ] } {
              archiveIndex $log
           }
        }
     return {}
     }
     
     if { [ catch {
        ;## get the index data
        set fid [ open $filename r ]
     } err ] } {
        return -code error $err
     }
     while { [ gets $fid line ] > -1 } {
        if { [ regexp ">${runcode}(\\d+)" $line -> job ] } {
	    if { ( $min == -1 ) || ( $job < $min ) } { set min $job }
           if { ( $max == -1 ) || ( $job > $max ) } { set max $job }
        }
     }   
     
     close $fid
     
     ;## if no jobs were referenced, just return except 
     ;## for cntlmon, set an artificial job number
     ;## NOTE: Because the index file is used for all searches,
     ;##   need to set min max to 0 when no jobs found
     if { $min == -1 || $max == -1 } { 
        if { [ string match cntlmon $::API ] } {
           set min -1
           set max -1
        } else {
           set min 0
           set max 0
        }
     }
         ;## get first timestamp in file
     set st [ log::startTime $filename ]
     ;## handle requests from hotIndex differently
     if { [ regexp {html} [ file extension $filename ] ] } {
        return "$filename $runcode$min $runcode$max $st"
     } else {   
        log::updateArchive "$filename $runcode$min $runcode$max $st"
     } 
}
## ******************************************************** 

## ******************************************************** 
##
## Name: hotIndex
##
## Description:
## Supplement archiveIndex info with info about current
## log files for a comprehensive search.
##
## Parameters:
##
## Usage:
##
## Comments:
## I think this glob pattern excludes the .1 and .2 files,
## which is intentional, since they are archived already.

proc hotIndex { } {
     if { [ catch {
        set retval "\n"
        set logs [ glob $::LDASLOG/LDAS*.log.html ]
        foreach log $logs {
           if { ! [ regexp {\.\d\.} $log ] } {
              append retval [ archiveIndex $log ]
              append retval "\n"
           }   
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: queryLogArchive
##
## Description:
## Returns a nicely formatted HTML document with all entries
## relating to a specified job combined in a single document.
##
## Parameters:
##
## Usage:
##
## Comments:
## 

proc queryLogArchive { jobid { truncate 0 } { runcode "" } } {
     set html    "<html>\n"
     append html "<head>\n"
     append html "<title>Report on $jobid</title>\n"
     append html "</head>\n"
     append html "<body>\n"
     append html "<h3>Report on $jobid</h3>\n"

     regexp {^\D+} $jobid Runcode
     regexp {\d+} $jobid jobid 
     if { ! [ string length $jobid ] } {
        return {}
     }

     if { ! [ string length $runcode ] } {
        if { [ info exists Runcode ] } {
           set runcode $Runcode
        } else {
           set runcode $::RUNCODE
        }
     }
     
     set file      [ file join $::LDASARC archiveIndex ]
     set info      [ dumpFile $file ]
     append info   [ hotIndex ]
     foreach line  [ split $info "\n" ] {
        set fname {}
        set first {}
        set last  {}
        foreach {fname first last} $line { break }
        if { ! [ string length $last ] } { continue }
        regexp {\d+} $first first
        regexp {\d+} $last  last
        if { $first <= $jobid && $last >= $jobid } {
           regexp {LDAS([a-z]+)} $fname -> api
           set tail [ file tail $fname ]
           if { ! [ file exists $fname ] } {
              set fname [ file join $::LDASLOG $tail ]
           }
           if { ! [ file exists $fname ] } {
              set fname [ file join $::LDASARC ${api}API $tail ]
           }
           if { [ file exists $fname ] } {
              set tmp [ open $fname r ]
              while { [ gets $tmp li ] > -1 } {
                 if { [ regexp "${runcode}${jobid}\[^\\d\]" $li ] } {
                    if { $truncate } {
                       set li [ log::truncateLine $li ]
                    }
                    append $api "$li\n"
                 }
              }
              close $tmp
           }   
        }
     } ;## end of foreach on line

     foreach api $::API_LIST {
        append html "<p>\n"
        append html "<h3><font color='brown'>${api}API</font></h3>\n"
        if { [ info exists $api ] } {
           append html [ set $api ]
        } else {
           append html "<b>none</b>\n"
        }
     }
     append html "</body>\n"
     append html "</html>"
     return $html
}
## ******************************************************** 

## ******************************************************** 
##
## Name: setLogPatterns
##
## Description:
## process patterns for NOT and AND condition
## by generating new patterns and mark state
##
## Parameters:
##
## Usage:
##
## Comments:
## done in context of caller

proc setLogPatterns {} {

     uplevel {
        set notMode 0
        set andMode 0
        set origpat $pat
        if { [ regexp {^!!} $pat ] } {
           set notMode 1
           set pat [ string range $pat 2 end ]
           set pat [ join $pat \| ]
        } elseif { [ regexp {^[^&]+&[^&]+} $pat ] } {
           set andMode 1
           regsub -all {&} $pat " " pat
           set patlist [ split [ string trim $pat ] ]
           set firstpat [ lindex $patlist 0 ]
           set otherpats [ lrange $patlist 1 end ]
        }
        set regcmd "regexp $regexpOpt "
        if { ! [ string match "--" $regexpOpt ] } {
           append regcmd " -- "
        }
     } 
}

## ******************************************************** 
##
## Name: logFilterLine
##
## Description:
## apply the pattern to the log line, with handling for
## NOT and AND condition
##
## Parameters:
##
## Usage:
##
## Comments:
## done in context of caller
## *** watch out use of li for the log line

proc logFilterLine {} {
     uplevel {
        if { $andMode } {
           set cmd "$regcmd \{$firstpat\} \{$li\}"
		   if  { [ catch {
           		set found [ eval $cmd ] 
           		if { $found } {
              		foreach subpat $otherpats {
                 		set cmd "$regcmd \{$subpat\} \{$li\}"
                 		set rc  [ eval $cmd ]
                 		set found [ expr $found & $rc ]       
              	    }
				} 
           } err ] } {
		   		addLogEntry "$err for file $fname, cmd '$cmd',line '$li'"
		   }
        } else {
           set found 0
           set cmd "$regcmd \{$pat\} \{$li\}"
		   if { [ catch {
		   	  set rc [ eval $cmd ]
           	  if { $rc } {
              	 if { $notMode } {
                 	set found 0
              	 } else {
                 	set found 1
              	 }			
              } elseif { $notMode } {
                 set found 1
              }
		   } err ] } {
		   		addLogEntry "$err for file $fname, cmd '$cmd',line '$li'"
		   }
        }
        if { $found } {                             
           if { $truncate } {
              set li [ log::truncateLine $li ]
           }
           append $api "$li\n"
           incr count
        }
     }  
}

## ******************************************************** 
##
## Name: queryJobLogs
##
## Description:
## Returns a nicely formatted HTML document with all entries
## relating to a specified job combined in a single document.
## supports pattern and selection of api logs.
##
## Parameters:
##
## Usage:
##
## Comments:
## 

proc queryJobLogs { jobid { pat ".+" } { limit 1000 } { apilist all } { regexpOpt -- } \
{ runcode "" } { truncate 1 } } {

     setLogPatterns

     regexp {^\D+} $jobid Runcode
     regexp {\d+} $jobid jobid 
     if { ! [ string length $jobid ] } {
        return {}
     }

     if { ! [ string length $runcode ] } {
        if { [ info exists Runcode ] } {
           set runcode $Runcode
        } else {
           set runcode $::RUNCODE
        }
     }
     
     if { [ string equal [ join $apilist ] "all" ] } {
        set apilist [ lsort $::API_LIST ]
     }   
     set file      [ file join $::LDASARC archiveIndex ]
        if { [ info exist ::logCache ] } {
        set info [ getLogCacheFnamesByJob $jobid ]
        } else {
        set info      [ dumpFile $file ]
        }
     append info   [ hotIndex ]
     set more ""
     set count 0
     
     foreach line  [ split $info "\n" ] {
        ;## 4x faster than regexp for apis
        set apifound 0
	    foreach api $apilist {
    	    if  { [ string first $api $line ] } {
        		set apifound 1
                break
            }
	    }
        if  { ! $apifound } {
            continue
        }
        
        set fname {}
        set first {}
        set last  {}
        foreach {fname first last} $line { break }
        if { ! [ string length $last ] } { continue }
        regexp {\d+} $first first
        regexp {\d+} $last  last
        if { $first <= $jobid && $last >= $jobid } {
          
           regexp {LDAS([a-z]+)} $fname -> api
           set tail [ file tail $fname ]
           
           if { ! [ file exists $fname ] } {
              set fname [ file join $::LDASLOG $tail ]
           }   
           if { ! [ file exists $fname ] } {
              set fname [ file join $::LDASARC ${api}API $tail ]
           }
           
           ;## catch parse errors in log file
           if { [ file exists $fname ] } {
              set tmp [ open $fname r ]
              set data [ read $tmp ]
              set data [ split $data \n ]
              foreach li $data {
			  	 if	{ [ catch {
                 	if { [ regexp "${runcode}${jobid}\[^\\d\]" $li ] } {
                    	logFilterLine 
                    	if 	{ $count >= $limit } { 
                       		set more "Max $limit lines returned matching $pat"
                       		break 
                    	}
					}                            
                 } err ] } {
				 	addLogEntry "$fname error: $err, line='$li'"
				 }
              }
              close $tmp
           }        
        }
     } ;## end of foreach on line
     
     if { ! [ string length $more ] } {
        set more "$count lines matched $origpat"
     }
     set html    "<html>\n"
     append html "<head>\n"
     append html "<title>Report on Job $jobid</title>\n"
     append html "</head>\n"
     append html "<body>\n"
     append html "<h3>Report on Job $jobid, $more</h3>\n"

     foreach api $apilist {
        append html "<p>\n"
        append html "<h3><font color='brown'>${api}API</font></h3>\n"
        if { [ info exists $api ] } {
           append html [ set $api ]
        } else {
           append html "<b>none</b>\n"
        }
     }
     append html "</body>\n"
     append html "</html>"
     return $html
}
## ******************************************************** 

## ******************************************************** 
##
## Name: grepLogs 
##
## Description:
## Do a grep on logs created after a specified gps time.
##
## Parameters:
## time - start time to grep after, or range of times to
## grep within
## pat - regexp pattern to search on
## limit - maximum total number of lines to return
##
## Usage:
##
## Comments:
## runcode is never used but just to even up the args

proc grepLogs { { time "" } { pat ".+" } { limit 1000 } { apilist {all} } \
{ regexpOpt -- } { truncate 1 } } {

     setLogPatterns
     set count 0
     if { [ regexp {all|\.\+} [ join $apilist ] ] } {
        set apilist $::API_LIST
     }

     if { ! [ regexp {(\d{9})-(\d{9})} $time -> time1 time2 ] } {
        if { ! [ regexp {\d{9}} $time time1 ] } {
           return {}
        }
        set time2 999999999
     }
     
     set endtime [ gpsTime now ]
     if { ! [ string length $pat ] } {
        return {}
     }   
     if  { [ regexp -- {-nocase} $regexpOpt ] } {
        set case -i
     } else {
        set case ""
     }
     set more ""
     if { [ info exist ::logCache ] } {   
        set info [ getLogCacheFnamesByTime $time1 $time2 $apilist $::LDASLOG ]
     } else {
        set file [ file join $::LDASARC archiveIndex ]
        set info [ dumpFile $file ]
        append info [ hotIndex ]
     }
     
     set not ""
     if  { $notMode } {
         set not -v
     } 

     foreach line  [ split $info "\n" ] {    
        foreach {fname first last start} [ split $line ] { break }
        set end 0
        if { ! [ regexp {LDAS([a-z]+).+(\d{9})} $fname -> api end ] } {
           set end $endtime
           regexp {LDAS([a-z]+)\.} $fname -> api
        }
        if  { $start > $time2 } {
            continue
        }
        if { $end >= $time1 } {
           regexp $::env(RUNDIR)\\S+ $fname fname
           if  { $andMode } {
                set cmd "exec grep $case -E {--regexp=$firstpat} $fname"       
           } else {
                set cmd  "exec grep $case $not -E {--regexp=$pat} $fname"   
           }    
           set rc [ catch { eval $cmd } data ]
           if   { $rc } {
                continue
           }
           set data [ split $data \n ]
           foreach li $data {
                regexp {\d{9}} $li timestamp
                if { $timestamp >= $time1 && $timestamp <= $time2 } { 
                    logFilterLine             
                }
           }
        }
        ;## break foreach on files
        if { $count >= $limit } { 
           set more "$limit lines returned matching $pat"
           break 
        }
     }
     if { ! [ string length $more ] } {
        set more "$count lines matched $pat"
     }
     set html    "<html>\n"
     append html "<head>\n"
     append html "<title>Log File Entries matching \{$origpat\}</title>\n"
     append html "</head>\n"
     append html "<body>\n"
     append html "<h3>Log File Entries $time1-$time2,$more</h3>\n"
    
     foreach api $apilist {
        append html "<p>\n"
        append html "<h3><font color='brown'>${api}API</font></h3>\n"
        if { [ info exists $api ] } {
           append html [ set $api ]
        } else {
           append html "<b>none</b>\n"
        }
     }
     append html "</body>\n"
     append html "</html>"
     return $html
}


## ******************************************************** 

## ******************************************************** 
##
## Name: log::startTime
##
## Description:
## Returns timestamp from first entry in a log file.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc log::startTime { { log "" } } {
     if { ! [ file exists $log ] } {
        set log [ lindex [ log::name $log ] 1 ]
     }   
     if { ! [ file exists $log ] } {
        return {}
     }   
     set fid [ ::open $log r ]
     ;## seek past the header
     seek $fid 1000
     ;## read as little as possible to be fast
     set data [ read $fid 2048 ]
     close $fid
     set st ""
     catch { regexp {\d{9}} $data st }
     return $st
}
## ******************************************************** 

## ******************************************************** 
##
## Name: log::truncateLine
##
## Description:
## truncates a log line if over 2048.
##
## Parameters:
##
## Usage:
##
## Comments:
## The resource variable TRUNCATE_LOG_LIMIT can be used to
## change the truncation behaviour on-the-fly.

proc log::truncateLine { line { limit 2048 } } {
     if { [ info exists ::TRUNCATE_LOG_LIMIT ] } {
        set limit $::TRUNCATE_LOG_LIMIT
     }
     if { [ string length $line ] > $limit } {
        if { [ regexp {<tt>(.+)</tt>} $line -> text ] } {
           set text [ string range $text 0 $limit ] 
           append text "(line length > $limit bytes,truncated)"
           regexp {^(.+)<tt>} $line -> data 
           append data "$text </tt><br>"
           set line $data
        }        
     }
     return $line
}
## ********************************************************

## ******************************************************** 
##
## Name: log::stdOutErr
##
## Description:
## Converts errors that normally only ever get reported in
## in the ::API.log file into log entries.
##
## Relies on a file named lookup.patterns in the generic API
## installation directory.
##
## Parameters:
##
## Usage:
## The format of an entry in the lookup.patterns file is:
## 
##      [ list api regex message errorlevel ]
##
## where the regex is probably a unique substring and the
## errorlevel is a log level like "blue" or "email".
##
## Comments:
##

proc log::stdOutErr { args } {
     
     if { [ catch {
        set API [ lindex $args 0 ]
        set filename [ file join $::env(RUNDIR) ${API}API $API.log ]
        set patfile ${::LDAS}/lib/genericAPI/lookup.patterns
        set now [ clock seconds ]
        set subject "$::LDAS_SYSTEM LDAS ${API}API error!"
        set tmp [ list ]
        
        if { [ file exists $patfile ] } {
          
           if { ! [ info exists ::stdout_stderr_fpos($API) ] } {
              set ::stdout_stderr_fpos($API) 0
           }
                      
           set patterns [ dumpFile $patfile ]
           set patterns [ split $patterns "\n" ]
           foreach pattern $patterns {
              foreach [ list api pattern msg errlvl ] $pattern { break }
              if { [ string equal $API $api ] } {
                 lappend tmp [ list $pattern $msg $errlvl ] 
              }
           }
           set patterns $tmp
           
           ;## read the new part of the file written in last minute
           set dsize [ file size $filename ]
           set newbytes [ expr { $dsize - $::stdout_stderr_fpos($API) } ]
           
           ;## if an API was restarted the newbytes could be
           ;## negative, which will cause an error about -NNN
           ;## being invalid and that only -nonewline is valid...
           if { $newbytes < 0 } {
              set ::stdout_stderr_fpos($API) 0
              set newbytes $dsize
           } 

           if { $newbytes } {
              set fid [ ::open $filename r ]
              seek $fid $::stdout_stderr_fpos($API)
              set data [ ::read $fid $newbytes ]
              set ::stdout_stderr_fpos($API) [ tell $fid ]
              ::close $fid
              if { [ string length $data ] > 100000 } {
                 ;## big trouble in this API!!
              }
              set data [ split $data "\n" ]
           } else {
              set data [ list ]
           }
           
           foreach line $data {
              foreach pattern $patterns {
                 foreach [ list pattern msg errlvl ] $pattern { break }
                 if { [ regexp -- $pattern $line ] } {
                    set line "notable line in $filename: ' $line '"
                    if { [ regexp -nocase {(mail|phon|pager)} $errlvl ] } {
                       set msg "Subject: ${subject}; Body: $msg ($line)"
                       addLogEntry $msg $errlvl
                    } else {
                       addLogEntry "$msg ($line)" $errlvl
                    }
                    continue
                 }
              }
           }
        }
     } err ] } {
        if { [ string length $err ] } {
           if { [ info exists ::errorInfo ] } {
              append err " $::errorInfo"
           }
           addLogEntry $err red
        }
        catch { ::close $fid }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: checkLog12InArchive
##
## Description:
## if logs 1 and 2 are in archive, include them in fnames
##
## Parameters:
##
## Usage:
##
## Comments:

proc checkLog12InArchive {} {

    uplevel {
        if  { [ llength $files ] } {
            set log1 [ lindex $files end-3 ]
            set log2 [ lindex $files end-7 ]
            if  { [ file exist $log2 ] } {
                set log2size [ file size $log2 ]
                set filesize [ file size  $logdir/LDAS${api}.2.html ]
                if  { ! $log2size == $filesize } {
                    debugPuts "appending $file"
                    append fnames [ archiveIndex $logdir/LDAS${api}.2.html]
                }
            }
            if  { [ file exist $log1 ] } {
                set log1size [ file size $log1 ]
                set filesize [ file size  $logdir/LDAS${api}.1.html ]
                if  { ! $log1size == $filesize } {
                    debugPuts "appending $file"
                    append fnames [ archiveIndex  $logdir/LDAS${api}.1.html ]
                }
            }
        }
    }
}

## ******************************************************** 
##
## Name: getLogCacheFnamesByTime
##
## Description:
## retrieve file names from cache that satisfies the condition
##
## Parameters:
##
## Usage:
##
## Comments:

proc getLogCacheFnamesByTime { tstart tend apilist logdir } {
     if { [ catch {
        set rc $::RUNCODE
        set fnames [ list ]
		set loglines [ list ]
        foreach api $apilist {
            set $api [ list ]
        }
        if	{ [ info exist ::logCache ] && [ llength $::logCache ] } {
			foreach { timestart timeend jobstart jobend beginIndex endIndex } $::logCacheIndex {
				if	{ $tstart > $timeend || $tend < $timestart } {
					continue
				}
				# debugPuts "tstart=$tstart tend=$tend $timestart, $timeend $beginIndex $endIndex"
				set loglines [ concat $loglines [ lrange $::logCache $beginIndex $endIndex ] ]
			}
			debugPuts  "Examine [ expr [ llength $loglines ]/5 ] archived files out of \
			[ expr [ llength $::logCache ]/5 ]"	

        	foreach { api first last start end } $loglines {
        		set fname [ file join $::LDASARC ${api}API LDAS${api}.$end ]
				append $api "$fname ${::RUNCODE}$first ${::RUNCODE}$last $start\n"
        	}
      	}  
        ;## merge in the current logs (hotIndex)
        if  { ! [ string length $logdir ] } {
            set logdir $::LDASLOG
        }
        foreach api $apilist {
            set files [ set $api ]
            if  { [ llength $files ] } {
                append fnames $files
            }
            set retval {}
            if  { [ string match wrapper $api ] } {
                set log $logdir/LDAS${api}.2.html
                lappend log $logdir/LDAS${api}.1.html
                lappend log $logdir/LDAS${api}.log.html
                foreach file $log {
                    if  { ! [ file exist $file ] } {
                        continue
                    }
                    set fsize [ file size $file ]
                    if  { $fsize > 1000000 } {
                        debugPuts "skipping large file $file, size $fsize"
                    } else {
                        append retval "[ archiveIndex $file ]\n"
                    }
                }
                if  { [ llength $retval ] } {
                    append fnames "$retval\n"
                }        
            } else {
                set log $logdir/LDAS${api}.log.html
                if  { [ file exist $log ] } {
                    append fnames "[ archiveIndex $log ]\n"
                } else {
                    debugPuts "$log does not exist!"
                }
                #set newfiledata [ handleLogRollover $api $log ]
                
                #if  { [ string length $newfiledata ] } {
                #    append fnames "$newfiledata\n"
                #}                           
                #if  { [ file exist $log ] } {
                #    set retval [ archiveIndex $log ]
                #} else {
                    debugPuts "$log does not exist!"
                #}
                #if  { [ llength $retval ] } {
                #    append fnames "$retval\n"
                #}
            }
        } 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ string trim $fnames \n ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: getLogCacheFnamesByJob
##
## Description:
## retrieve file names from cache that satisfies the condition
##
## Parameters:
##
## Usage:
##
## Comments:

proc getLogCacheFnamesByJob { jobid apilist { logdir "" }} {

     if { [ catch {
        set rc $::RUNCODE
        set fnames [ list ]
        foreach api $apilist {
            set $api [ list ]
        }
		regexp {(\d+)} $jobid -> jobnum
		set loglines [ list ]
        
        ;## sometimes no logCache since logs have been purged
        debugPuts "logCache exist [ info exist ::logCache ]"
        if	{ [ info exist ::logCache ] && [ llength $::logCache ] } {
			foreach { timestart timeend jobstart jobend beginIndex endIndex } $::logCacheIndex {
            # debugPuts "$timestart $timeend $jobstart $jobend $beginIndex $endIndex"
				if	{ $jobstart <= $jobnum && $jobend >= $jobnum } {
					set loglines [ concat $loglines [ lrange $::logCache $beginIndex $endIndex ] ]
				}
			}
			debugPuts "Examine [ expr [ llength $loglines ]/5 ] archived files out of \
			[ expr [ llength $::logCache ]/5 ]"
				
        	foreach { api first last start end } $loglines {
              	set fname [ file join $::LDASARC ${api}API LDAS${api}.$end ]
			  	append $api "$fname ${::RUNCODE}$first ${::RUNCODE}$last $start\n"
        	}
        }
        ;## merge in the current logs (hotIndex)
        if  { ! [ string length $logdir ] } {
            set logdir $::LDASLOG
        }
        foreach api $apilist {
            set files [ set $api ]
            if  { [ llength $files ] } {
                append fnames $files
            }
            set retval {}
            if  { [ string match wrapper $api ] } {
                set log $logdir/LDAS${api}.2.html
                lappend log $logdir/LDAS${api}.1.html
                lappend log $logdir/LDAS${api}.log.html
                foreach file $log {
                    if  { ! [ file exist $file ] } {
                        continue
                    }
                    set fsize [ file size $file ]
                    if  { $fsize > 1000000 } {
                        debugPuts "skipping large file $file, size $fsize"
                    } else {
                        #append retval "[ archiveIndex $file ]\n"
                        append retval $file
                    }
                }
                if  { [ llength $retval ] } {
                    append fnames "$retval\n"
                }               
            } else {
                ;## if log is < 1 sec old, wait for roll over to complete 
                ;## and pick up the new archived file
                ;## use archiveIndex to get the job limits and start time
                ;## but does not update archive
                
                set log $logdir/LDAS${api}.log.html
                if  { [ file exist $log ] } {
                    append fnames "[ archiveIndex $log ]\n"
                } else {
                    debugPuts "$log does not exist!"
                }
                  
				#set newfiledata [ handleLogRollover $api $log ]

                #if  { [ string length $newfiledata ] } {
                #    append fnames "$newfiledata\n"
                #}
                
                #if  { [ file exist $log ] } {
                #    set retval [ archiveIndex $log ]
                #    if  { [ llength $retval ] } {
                #        append fnames "$retval\n"
                #    }
                #} else {
                #    debugPuts "$log does not exist!"
                #}
            } 
        }
     } err ] } {
        return -code error "[ myName ]: $api $err"
     }
     return [ string trim $fnames \n ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: buildLogCacheIndex
##
## Description:
## builds an index over logCache to avoid scanning too many files
##
## Parameters:
##
## Usage:
##
## Comments:

proc buildLogCacheIndex {} {

	set count 0
	set ::logCacheIndex [ list ]
    if  { ! [ info exist ::LOGFILTER_CHUNK_SZ ] } {
        set ::LOGFILTER_CHUNK_SZ 1000
    }
    set ::LOGFILTER_CHUNK_CNT [ expr $::LOGFILTER_CHUNK_SZ * 5 ]
    
	if	{ [ info exist ::logCache ] } {
      set len [ llength $::logCache ]
      set endCache [ expr $len - 5 ]
	  set startindex [ llength $::logCacheIndex ]
	  foreach { api first last start end } $::logCache {
		if	{ ! [ info exist timestart ] } {
	   		set timestart $start
	   	} elseif { $start < $timestart } {
		   	set timestart $start
            set saved_timestart $timestart 
		}
        set saved_timestart $timestart 
        
		if	{ ! [ info exist timeend ] } {
	   		set timeend $end
	   	} elseif { $end > $timeend } {
		   	set timeend $end
		}
        set saved_timeend $timeend
        
		if	{ [ info exist jobmax ] } {
		   	if	{ $last > $jobmax } {
				set jobmax $last
			}
		} else {
			set jobmax $last
		}
        set saved_jobmax $jobmax
        
		if	{ [ info exist jobstart ] } {
		   	if	{ $first < $jobstart && $first > -1 } {
				set jobstart $first
			}
		} else {
			set jobstart $first
		}
        set saved_jobstart $jobstart
        
		set done 0
        if	{ $count >= $endCache } {
			set done 1
		} elseif { ! [ expr $count % $::LOGFILTER_CHUNK_CNT ] && $count } {
			set done 1
		} 
        if  { $done } {
		    set endindex [ expr $count - 1 ]
		    lappend ::logCacheIndex $timestart $timeend $jobstart $jobmax $startindex $endindex 
            unset timestart
            unset timeend
            unset jobstart
            unset jobmax
		    set startindex [ expr $endindex + 1 ]
        }
        incr count 5
	 }
    
    ;## tag on the last entries if count has not reached $::LOGFILTER_CHUNK_CNT round number
   
     if	{ [ expr $count % $::LOGFILTER_CHUNK_CNT ] && $count } {
		set endindex [ expr $count - 1 ]
		lappend ::logCacheIndex $saved_timestart $saved_timeend $saved_jobstart $saved_jobmax $startindex $endindex 
	 }
     debugPuts "logCacheIndex $::logCacheIndex"
     debugPuts "logCache last entry [ lrange $::logCache [expr $endindex - 4 ] end ]"
	}
}	

## ******************************************************** 
##
## Name: handleLogRollover
##
## Description:
## if log rollover is in transition wait a little
## to avoid missing information during log filter
## There would be a new archive entry for the
## just rolled over
##
## Parameters:
##
## Usage:
##
## Comments:
## only works for current log
## use by bgcntlmon or standalone log script
## cannot be used within an API due to after delay
## potential rollover
##
## current log file is < 3 secs old
## the log file and .1.html mtime is < 10 secs

proc handleLogRollover { api log } {

    set newfiledata ""
    if  { [ catch {
		file stat $log stats
        regsub {log\.html} $log {1.html} log1
        set curtime [ clock seconds ]
		file stat $log1 stats1
		puts "$log [ array get stats1 ]"
        set delta  [ expr $curtime - $stats(atime) ] 
		set delta_mtime [ expr abs($stats(mtime) - $stats1(mtime)) ] 
        # debugPuts "$log delta $delta delta_mtime $delta_mtime"
        if	{ $delta < 3 && $delta_mtime < 5 } {
			debugPuts "$log may have just rolled at $curtime, created about $stats(atime)"
			catch { eval exec ls -lt [ glob $log $log1 ] } data
            after 2000
            set rc [ catch { exec tail -1 [ file join $::LDASARC archiveIndex ] | grep $api } newfiledata ]
            if  { !$rc } {
			    debugPuts "$data\nadded new archived file\n$newfiledata"
            }
		}
    } err ] } {
		set newfiledata $log
		puts "handleLogRollover $log error: $err"
        # return -code error $err
    }
    return $newfiledata
}
