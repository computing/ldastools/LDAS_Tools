## ******************************************************** 
## sock.tcl
##
## Provides socket handling functions.
##
## Provides functions for opening and closing Tcl and
## ObjectSpace sockets.
##
## Exposes functions described in the generic API baseline
## specification.
##
## This is the module of the generic API that loads the
## genericAPI.so, since it is the functions declared here
## that use it.
## cache is used to hold pointers to ilwd elements ingested
## from dataSocket as FIFO
## ******************************************************** 

;#barecode

package provide sock 1.0

if { ! [ info exists genericAPI ] } {
   set msg    "sock.tcl is a module of\n"
   append msg "the genericAPI.tcl and provides\n"
   append msg "no independent functionality."
   return -code error $msg
   }

if { ! [ regexp user|manager $API ] } {
   package require genericAPI
   }

namespace eval sock { 
                    set cache   {}
                    }

;## set up for wrapping of gets and puts
if { [ lsearch [ info proc *gets ] tcl_gets ] == -1 } {
   rename gets tcl_gets
   rename puts tcl_puts
}

;#end

## ******************************************************** 
##
## Name: gets
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc gets { channel { var "" } } {
     
     if { [ catch {
        set data [ list ]
        if { [ regexp {dsock\d+} $channel ] } {
           if { [ string length $var ] } {
              set data [ uplevel data_gets $channel $var ]
           } else {
              set data [ uplevel data_gets $channel ]
           }
        } else {
           if { [ string length $var ] } {
              set data [ uplevel tcl_gets $channel $var ]
           } else {
              set data [ uplevel tcl_gets $channel ]
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: puts
##
## Description:
## Here we are wrapping the default Tcl channel output
## routine to handle data socket 'dsock' output channels
## by the same interface that is used for 'file' and 'sock'
## channels.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc puts { args } {
     
     if { [ catch {
        if { [ regexp {dsock\d+} [ lindex $args 0 ] ] && \
             [ string length [ lindex $args 1 ]  ] } {
           set name data_puts
           uplevel data_puts $args
        } else {
           set name tcl_puts
           uplevel tcl_puts  $args
        }
     } err ] } {
        if { [ string length $args ] > 1024 } {
           set args [ string range $args 0 1023 ]...
        }
        return -code error "${name}: $err (args: '$args')"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: sock::open
##
## Description:
## Open a Tcl client socket "$sid" to an interpreter on
## a remote machine by api name and service.
##
## Usage:
##       set sid [ sock::open manager operator ]
##
## Comments:
##

proc sock::open { { api "" } { service "" } } {
     if { [ catch {
        if { [ uplevel info exists jobid ] } {
           set jobid [ uplevel set jobid ]
        }
        set target [ validService $api $service ]
        foreach {api host port service sid} $target {break}
        if { [ string equal data $service ] } {
           set sid [ datasocket -myaddr $::env(HOST) $host $port ]
        } else {
        # set sid [ socket -async $host $port ]
		   set sid [ socket $host $port ]
        }
        
        ;## calculate time periods equal to 1%, 10% and 100%
     	;## of the ping timeout panic limit.
        set timeout 5000
     	set short  [ expr { round($timeout / 100.0) } ]
     	set medium [ expr { $short * 9 } ]
     	set long   [ expr { $short * 90 } ]
            
        ;## three stage ping response eval. once after 1% of
        ;## the panic limit, again after 10%, then at the limit.
        after $short
        if { [ catch { fconfigure $sid -peername } ] } {
           after $medium
           if { [ catch { fconfigure $sid -peername } ] } {
              after $long
           }   
        }
        
        set err [ fconfigure $sid -error ]
        if { [ string length $err ] } {
           return -code error $err
        }
        if { [ string equal data $service ] } {
           fconfigure $sid -blocking on
           fconfigure $sid -buffering none
           fconfigure $sid -translation binary
           #fconfigure $sid -linger 10
           if { [ info exists ::DEBUG_DATA_SOCKET ] } {
              if { [ string equal 1 $::DEBUG_DATA_SOCKET ] } {
                 set data [ fconfigure $sid ]
                 set msg "data CLIENT config $sid: ($data)"
                 addLogEntry $msg purple
              }
           }
        } else {   
           fconfigure $sid -blocking off
           fconfigure $sid -buffering line
        }
     } err ] } {
        catch { ::close $sid }
        set msg    "could not connect to $api $service "
        append msg "on port $port on $host.\n"
        lappend msg $err
        return -code error "[ myName ]:\n$msg"
     }
     return $sid
}     
## ********************************************************

## ******************************************************** 
##
## Name: sock::cfg 
##
## Description:
## Configures a listening socket opened by openListenSock.
## A procedure named $service must exist to handle all
## incoming requests.
##
## Usage:
## See openListenSock.
##
## Comments:
## 

proc sock::cfg { service cid addr port } {
     if { [ string equal data $service ] } {
        set service dataRecv
     }
     fileevent $cid readable "$service $cid"
     if { [ string equal dataRecv $service ] } {
        fconfigure $cid -blocking on
        fconfigure $cid -translation binary
        fconfigure $cid -buffering none
        #fconfigure $cid -linger 10
        if { [ info exists ::DEBUG_DATA_SOCKET ] } {
           if { [ string equal 1 $::DEBUG_DATA_SOCKET ] } {
              set data [ fconfigure $cid ]
              set msg "data SERVER HANDOFF config $cid: ($data)"
              addLogEntry $msg purple
           }
        }
     } else {   
        fconfigure $cid -buffersize 50000
        ;## changed buffering to full on 04/24/03 in an attempt
        ;## to stop manager/api communication troubles.
        fconfigure $cid -buffering full
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: openListenSock 
##
## Description:
## Open a Tcl listening socket, registering a procedure
## named $service as the handler through a call to
## sock::cfg.
##
## Usage:
## The handler should look like:
##
##   proc service { cid } {
##        set data [ gets $cid ]
##        puts $cid $data
##        flush $cid
##        close $cid
##   }
##
## Comments:
## Note that when you create a server/client pair for
## testing that they must not exist in the same interpreter.
## Also remember that the listener only works when there
## is an event loop, as created by "vwait enter-mainloop".

proc openListenSock { { service "" } { port "" } { cid "" } } {
     
     ;## poll every 10 seconds to see if service has died
     if { [ string length $cid ] } {
        if { [ catch {
           ;## if this throws, we are definitely gone
           set data [ fconfigure $cid -sockname ]
           set currentport [ lindex [ join $data ] end ]
           ;## if we are ok, just pass on by
           if { $currentport == $port } {
              after 10000 [ list openListenSock $service $port $cid ]
           } else {
              ;## if not, pass on by, but rerun in 0.100 secs
              ;## and restart
              addLogEntry "my $service socket has died!!" mail
              after 100 openListenSock $service
           }
        } err ] } {
           addLogEntry "my $service socket has died!!" mail
           after 100 openListenSock $service
        }
        ;## short circuit return point if we are polling
        return {}
     }

     set target [ validService $::API $service ]
     foreach {api host port service sid} $target {break}

     upvar #0 ::${::API}($service) registry
     
     if { ! [ info exists registry ] } {
        set msg    "$api $service not registered in .rsc file"
        return -code error "[ myName ]: $msg"
     }
     
     catch { set registry [ lreplace $registry 1 1 ] }
     
     if { [ catch {
        if { [ string equal data $service ] } {
           set cid [ datasocket -server "sock::cfg $service" $port ]
           if { [ info exists ::DEBUG_DATA_SOCKET ] } {
              if { [ string equal 1 $::DEBUG_DATA_SOCKET ] } {
                 set data [ fconfigure $cid ]
                 set msg "data SERVER config $cid: ($data)"
                 addLogEntry $msg purple
              }
           }
        } else {
           set cid [ socket -server "sock::cfg $service" $port ]
        }
     } err ] } {
        if { [ catch {
           foreach { name pid owner } [ portInfo $port ] { break }
           set info "name: $name pid: $pid owner: $owner"
           set subject "$::LDAS_SYSTEM $::API API Port Unavailable!!"
           set msg "Could not start sevice '$service' on port $port\n"
           append msg "of host $host.\nThe port is already in use by\n"
           append msg "process '$name' (pid: $pid) owned by: '$owner'.\n"
           append msg "The $::API API will not function properly\n"
           append msg "without the use of this port!"
           addLogEntry "Subject: ${subject}; Body: $msg" email
        } err2 ] } {
           set info $err2
        }
        return -code error "$err : socket $port info: $info"
     }   
     
     lappend registry $cid
     addLogEntry "port $port ($service) opened on $host as $cid" blue
     after 10000 [ list openListenSock $service $port $cid ]
     return $cid
}
## ******************************************************** 

## ******************************************************** 
##
## Name: closeListenSock 
##
## Description:
## Closes a listening Tcl socket, removing the service
## associated with it.
##
## Usage:
##
## Comments:
## 

proc closeListenSock { service } {
     if { [ catch {
        after cancel openListenSock $service
        set target [ validService $service ]
     
        foreach [ list api host port service cid ] $target {break}
     
        after cancel openListenSock $service $port $cid
     
        upvar #0 ::${api}($service) registry
     
        if { ! [ info exists registry ] } {
           set msg    "No service registered at\n"
           append msg "\$\{$host\}\($service\)"
           addLogEntry $msg red
           return {}
        }
     
        set cid [ lindex $registry 1 ]
     
        if { ! [ string length $cid ] } {
           set msg "no cid registered for service '$service'"
           addLogEntry $msg blue
           return {}
        } elseif { ! [ regexp {sock[0-9]+} $cid ] } {
           set msg "'$cid' is not a valid Tcl socket i.d.!"
           addLogEntry $msg red
           return {}
        }
     
        if { [ catch { ::close $cid } err ] } {
           set msg    "error closing $cid,\n$err"
           addLogEntry $msg red
           return {}
        }
     
        catch { set registry [ lreplace $registry 1 1 ] }
        
        addLogEntry "port $port ($cid) ($service) closed on $host" blue 
     
     } err ] } {
        if { [ string length $err ] } {
           addLogEntry $err red
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: openSock 
##
## Description:
## Alias for sock::open, q.v.
##
## Usage:
##
## Comments:
## Please use sock::open, this is here to fulfill the
## requirement of the existence of this command per the
## generic API's baseline specification.

proc openSock { { host "" } { service "" } } {
     if { [ catch {
        uplevel set sid [ sock::open $host $service ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $sid   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dataRecv
##
## Description:
## Reads ilwd objects from an object space data socket.
## As each object is read, the jobid attached to the object
## is extracted, and the SWIG mangled pointer name for the
## object is placed in a list named $jobid_DATABUCKET.
## Objects whose names are placed in the data bucket persist
## until explicitly destructed by a call to destructElement.
## At any time, a call to emptyDataBucket will cause all
## elements in the bucket passed as an argument to be destroyed,
## and the bucket itself to be undefined.
##
## Usage:
## 
##
## Comments:
## This uses the threaded version of accept
## the server listening socket must have been created
## via openDataSock
 
proc dataRecv { sid } {
	
     if { [ catch {
        fileevent $sid readable {}
        set tid [ data_gets_t $sid ]
        if { $::DEBUG > 1 } {
           addLogEntry "thread $tid created for dataRecv $sid"
        }
        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "::dataRecvReaper $tid $sid"

     } err ] } {        
        catch { ::close $sid }
        catch { unset ::$tid }
        addLogEntry $err red
     }        
}

## ******************************************************** 
##
## Name: dataRecvReaper
##
## Description:
## Reads ilwd objects from an object space data socket.
## As each object is read, the jobid attached to the object
## is extracted, and the SWIG mangled pointer name for the
## object is placed in a list named $jobid_DATABUCKET.
## Objects whose names are placed in the data bucket persist
## until explicitly destructed by a call to destructElement.
## At any time, a call to emptyDataBucket will cause all
## elements in the bucket passed as an argument to be destroyed,
## and the bucket itself to be undefined.
##
## Usage:
## 
##
## Comments:
## This uses the threaded version of accept
## the server listening socket must have been created
## via openDataSock
 
proc dataRecvReaper { tid sid args } {  

	 if	{ ! [ info exist ::$tid ] } {
     	addLogEntry ":$tid does not exist" blue
        return
     }
     if { [ catch {
        set state [ set ::$tid ]
        if { [ string equal FINISHED $state ] ||
             [ string equal $state $::TID_FINISHED ]  } {
           
           set elementp [ data_gets_r $tid ]
           set unset_tid 1
           ::unset ::$tid
           unset unset_tid
           
           if { [ regexp {^_[0-9a-f]+_p_} $elementp ] } {
              set jobid [ ilwd::getjob $elementp ]
	       	  after 0 "eval lappend ::${jobid}_DATABUCKET $elementp"
              set msg "$elementp added to ${jobid}_DATABUCKET"
              addLogEntry $msg blue
              ::close $sid
           } elseif { [ string length $elementp ] } {
              set msg "unexpected string received on ${sid}:"
              append msg " '$elementp' is not a pointer!"
              addLogEntry $msg red
           }
           
        } 
     } err ] } {
        catch { ::close $sid }
        if	{ [ info exist unset_tid ] } {        	
        	catch { ::unset ::$tid }
        }
        addLogEntry "[ myName ]: $err"
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: dataSend 
##
## Description:
## send data as an ilwd object out through a data socket
##
## Parameters:
## jobid - job i.d.
## api - an api name; frame, metadata, ligolw, user, etc.
## data - a filename, pointer, or string.
##
## Usage:
## dataSend $api $data
##
## Comments:
## The data argument can be an ilwd or xml text file,
## a binary frame file, an OSpace pointer, or a string.
## Argument list ends with "args" so that it can eat
## the arguments added by the trace in bgDataSend.

proc dataSend { { jobid "" } { api "" } { data "" } args }  {  
     set seqpt {}
     set localp 0
     set targetuser 0

     regexp {^\{(.+)\}$} $data -> data
     set data [ string trim $data ]
     
     if { [ catch {
        if { [ regexp {user} $api ] } {
           ;## we will be using sendElementAscii
           ;## or sendLWDocument
           set targetuser 1
        }
        if { [ file exists $data ] } {
           set type [ fileType $data ]
           if { [ regexp {ilwd} $type ] } {
              set data [ ilwd::readFile $data ]
           } elseif { [ regexp {ligo_lw} $type ] } {
              set seqpt "readLwFile($data):"
              set data [ readLwFile $data ] 
           } elseif { [ regexp {frame} $type ] } {
              set framep [ frame::file2ptr $jobid $data ]
              set data   [ frame::framep2ilwdp $jobid $framep ]
           } else {
              return -code error "file of unknown type: '$data'"
           }
           ;## all pointers are local to the proc
           set localp 1   
           set seqpt {}
        }
        foreach [ list type data ] [ varType $data ] { break }
        ;## what if data is an XML-like string?
        if { [ regexp {ligo_lw} $type ] } {
           if { [ regexp {text} $type ] } {
              set err "Dynamically created LIGO_LW is forbidden!"
              return -code error $err
           }
           if { [ regexp {ilwd} $type ] && [ regexp {text} $type ] } {
              set seqpt "putElement($data):"
              set data [ putElement $data ]
           } else {
              set err "Unknown XML-like string rejected: $data"
              return -code error $err
           }
           set localp 1
           set seqpt {}
        }
        
        set datap [ dataSendPtype $data ]
        ilwd::setjob $datap $jobid
        debugPuts "jobid '[ ilwd::getjob $datap ]' attached to $datap"

        set sid [ sock::open $api data ]
        if { $targetuser } {
           set seqpt "sendLWDocument($sid $datap):"
           sendLWDocument $sid $datap
        } else {
           set seqpt "puts($sid $datap):"
           puts $sid $datap
           ::close $sid
        }
     } err ] } {
        catch { ::close $sid }
        if { $localp } {
           catch { destructElement $datap }
        }
        addLogEntry "$seqpt $err" red
        return -code error "[ myName ]:$seqpt $err"
     } ;## end of enormous catch
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dataSendPtype
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dataSendPtype { data } {
     set seqpt {}
     if { [ catch {
        set localp [ uplevel set localp ]
        set datap {}
        switch -regexp -- $data {
           {^_[0-9a-f]+_p_FrameH$} {
                     set datap $data
                     }
           {^_[0-9a-f]+_p_LdasElement$} {
                     set datap $data
                     }
           {^_[0-9a-f]+_p_LdasArrayBase$} {
                     set err "Bare arrays are forbidden!"
                     return -code error "[ myName ]: $err"
                     }
           {^_[0-9a-f]+_p_LdasContainer$} {
                     set datap $data
                     }
           {^_[0-9a-f]+_p_LWDocument$} {
                     if { [ uplevel set $targetuser ] } {
                        set datap $data
                        } else {
                        set datap [ getLwData $data ]
                        }
                     if { ! $localp } { uplevel unset data }
                     }
           {^_[0-9a-f]+_p_ILwdFile$} {
                     set seqpt "readElement($data):"
                     set datap [ readElement $data ]
                     if { ! $localp } { uplevel unset data }
                     }
           {^_[0-9a-f]+_p_FrameFile$} {
                     set seqpt "readFrame($data):"
                     set datap [ readFrame $data ]
                     if { ! $localp } { uplevel unset data }
                     }
           {^_[0-9a-f]+_p_LdasBinary$} {
                     set datap $data
                     }
             default {
                     }
        } ;## end of switch
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }   
     return $datap
}
## ******************************************************** 

## ******************************************************** 
##
## Name: readDataBucket
##
## Description:
## Returns the list of objects from a data bucket.
## If the data bucket does not exist it will wait for up
## to ::DATABUCKET_TIMEOUT seconds for the data bucket to
## appear before aborting the job.
## After reading, the list of pointer names will be
## reinitialised so that this procedure may be safely called
## again to pick up late arriving data.
##
## Parameters:
## jobid - the current jobid for the caller
##
## Usage:
##
## Comments:
## It MAY be necessary to call this repeatedly if more than
## one large object is being received.
## See the definition of DATABUCKET in the procedure dataRecv
## in this file.

proc readDataBucket { jobid } {
   
   ;## force thew correct bucket name, throw exception
   ;## if symbols are undefined.
   if { [ catch {
      set items [ list ]
      regexp {\d+} $jobid job
      set jobid $::RUNCODE$job
      set databucket ${jobid}_DATABUCKET
      if { [ info exists ::$databucket ] } {
         set items [ set ::$databucket ]
         set ::$databucket [ list ]
      } else {
         return -code error "no databucket found for this job!"
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $items
}
## ******************************************************** 

## ******************************************************** 
##
## Name: processDataBucket
##
## Description:
## Polymorphic data bucket contents handler -- returns
## everything as ilwd pointers.
##
## Parameters:
##
## Usage:
##
## Comments:
## Handles ilwd, filenames and URI's, and raw ilwd text.
## Avoids the terrible overhead of testing for raw text
## if possible.

proc processDataBucket { jobid } {
     set seqpt {}
     set ptrs         [ list ]
     set baditems     [ list ]
     set baddestructs [ list ]
     set tempfiles    [ list ]
     
     if { [ catch {
        set items [ readDataBucket $jobid ]
     	foreach item $items {
           foreach [ list type item ] [ varType $item ] { break }
           ;## if it's a directory it's not valid
           if { [ lsearch $type directory ] != -1 } {
              lappend baditems "$item is a directory"
              continue
           }
           if { [ lsearch $type unknown ] == 0 } {
              if { [ string length $item ] > 256 } {
                 set item "[ string range $item 0 255 ]..."
              }
              lappend baditems "unknown item type: '$item'"
              continue
           }
           
           ;## okay, it's a file or URI
           if { [ regexp {ilwd.+file} $type ] } {
              set ptr [ ilwd::readFile $item $jobid ]
              lappend ptrs $ptr
           ;## no?  maybe it's a pointer
           } elseif { [ regexp {ilwd.+pointer} $type ] } {
              lappend ptrs $item
           ;## maybe raw ilwd text??   
           } elseif { [ regexp {ilwd.+text} $type ] } {
              ;## nasty blocking function!
              set seqpt "putElement($item):"
              set ptr [ putElement $item ]
              lappend ptrs $ptr
           ;## it's an utter enigma -- toss it!
           } elseif { [ regexp {frame} $type ] } {
              if { [ string equal frame $::API ] } {
                 ;## what can the frame API do??
                 ;## frame file...
                 if { [ regexp {file} $type ] } {
                    set framep [ frame::file2ptr $jobid $item ]
                    lappend ptrs [ frame::framep2ilwdp $jobid $framep ]
                 }
                 ;## framecpp pointer...
                 if { [ regexp {pointer} $type ] } {
                    lappend ptrs [ frame::chanp2ilwdp $jobid $item ]
                 }
              } else {
                 lappend baditems "$item '$type' could not be processed"
              }
           } elseif { [ regexp -nocase {(ligo.?lw|xml)} $type ] } {
              if { [ string equal ligolw $::API ] } {
                 ;## what can the lightweight API do??
                 ;## ligo_lw file...
                 if { [ regexp {file} $type ] } {
                    set tid [ ligolw::thread_readLwFile $jobid $item ]
                    lappend ptrs $tid
                 ;## xml text in a variable
                 } elseif { [ regexp {text} $type ] } {
                    set uniq [ key::time ]
                    set tempfile [ file join $::LDASTMP tmp${uniq}.xml ]
                    set tid [ ligolw::thread_readLwFile $jobid $tempfile ]
                    lappend ptrs $tid
                    lappend tempfiles $tempfile
                 ;## binary xml object pointer Lw_p
                 } elseif { [ regexp {pointer} $type ] } {
                    ligolw::thread_lw2ilwd $jobid $item
                 } else {
                    lappend baditems "$item '$type' could not be processed"
                 }
              } else {
                 set err "Only ligolwAPI can accept xml data, not $::API"
                 return -code error $err
              }
           } else {
              if { [ string length $item ] > 256 } {
                 lappend baditems "[ string range $item 0 255 ]..."
              } else {
                 lappend baditems $item
              }   
           }
        }      
        if { [ llength $baditems ] } {
           ;## don't make it fatal.
           set msg "bad items in data bucket: '$baditems'"
           addLogEntry $msg red
           return -code error $msg
        }
        if { [ llength $tempfiles ] } {
           eval after 30000 [ list file delete -force -- $tempfiles ]
        }
     } err ] } {
        foreach ptr $ptrs {
           if { [ catch {
              ;## we probably do not want to destruct HERE.
           } err2 ] } {
              if { [ string length $ptr ] > 256 } {
                 lappend baddestructs "[ string range $ptr 0 255 ]..."
              } else {
                 lappend baddestructs $ptr
              }   
           }
        }
        if { [ llength $baddestructs ] } {
           set err "$err (and cleanup failed on: '$baddestructs')"
        }
        if { [ llength $tempfiles ] } {
           eval after 30000 [ list file delete -force -- $tempfiles ]
        }

        if { [ info exists item ] } {
           set err "$err (item: '$item')"
           if { [ info exists type ] } {
              set err "$err (type: '$type')"
           }
        }
        
        return -code error "[ myName ]:$seqpt $err"
     }
     set ptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: emptyDataBucket
##
## Description:
## Empties a databucket and unsets the name of the
## databucket.
##
## Parameters:
##
## Usage:
##
## Comments:
## Reports names of ilwd objects successfully deleted,
## because under some circumstances successful deletions
## would be undesirable.

proc emptyDataBucket { jobid } {

     regexp {\d+} $jobid jobid
     set jobid ${::RUNCODE}$jobid
     set databucket ${jobid}_DATABUCKET
     if { [ info exists ::$databucket ] } {
        if { [ llength [ set ::$databucket ] ] } {        
           ;## initialise the success/failure message lists.
           set errmsg   [ list ]
           set goneptrs [ list ]

           ;## iterate through the bucket and record the
           ;## results of destroying the elements in it.
           foreach ptr [ set ::$databucket ] {
              if { [ catch {
                 destructElement  $ptr
                 lappend goneptrs $ptr
              } err ] } {
                 lappend errmsg   $err
              }
           }
             
           ;## file report and we're done.
           if { [ llength $goneptrs ] } {
              addLogEntry $goneptrs blue
           }   
           if { [ llength $errmsg ] } {
              addLogEntry $errmsg red
           }
        }
        ;## undefine the databucket name
        unset ::$databucket
     }   
}
## ********************************************************

## ******************************************************** 
##
## Name: sock::diagnostic
##
## Description:
## Try to diagnose machines ability to function within the
## LDAS system. The ping times for this procedure are
## comparable to those of the UNIX ping command.
##
## Parameters:
##
## Usage:
##
## Comments:
## Uses the default ssh port to determine whether a machine
## is alive and able to talk.

proc sock::diagnostic { host { port 22 } } {
     set status 0
     set msg "ok"
     
     ;## parse hostname from foo@hostname:NNN
     ;## cannot recall why we need to do this!!
     if { [ regexp {\@([^\:]+)} $host -> tmp ] } {
        set host $tmp
     }
     
     ;## trial and error showed 2000 to be reasonable and
     ;## conservative.
     if { ! [ info exists ::PING_TIME_PANIC_LIMIT_MS ] } {
        set ::PING_TIME_PANIC_LIMIT_MS 2000 
     }
     
     set Host [ string toupper $host ]
     
     if { [ info exists ::PING_TIME_PANIC_LIMIT_MS_$Host ] } {
        set timeout [ set ::PING_TIME_PANIC_LIMIT_MS_$Host ]
     } else {
        set timeout $::PING_TIME_PANIC_LIMIT_MS
     }
     
     ;## calculate time periods equal to 1%, 10% and 100%
     ;## of the ping timeout panic limit.
     set short  [ expr { round($timeout / 100.0) } ]
     set medium [ expr { $short * 9 } ]
     set long   [ expr { $short * 90 } ]
     
     if { [ catch {
        
        set sid [ socket -async $host $port ]
        
        ;## three stage ping response eval. once after 1% of
        ;## the panic limit, again after 10%, then at the limit.
        after $short
        if { [ catch { fconfigure $sid -peername } ] } {
           after $medium
           if { [ catch { fconfigure $sid -peername } ] } {
              after $long
              if { [ catch { fconfigure $sid -peername } ] } {
                 set status 1
                 set msg "$host is down or has no service"
                 append msg " on port $port"            
              } else {
                 set msg "$host ping more than ${short}ms and "
                 append msg "less than ${timeout}ms"
                 addLogEntry $msg blue
              }
           }   
        }
        
        if { $status == 0 } {
           set err [ fconfigure $sid -error ]
           if { [ string length $err ] } {
              set status 1
              set msg "error reported on ${host}:${port}: $err"
           }
        }
     } err ] } {
        set status 1
        set msg "host $host unreachable"
     }
     catch { ::close $sid }
     return [ list $status $msg ]
}
## ********************************************************

## ******************************************************** 
##
## Name: killedJobBucketReaper
##
## Description:
## Keeps track of jobs registered as killed and cleans out
## asynchronously created data bucket items for one hour
## after the job is killed.
##
## Called using bgLoop at 1 minute intervals.
##
## Very light weight.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc killedJobBucketReaper { args } {
     
     if { [ catch {
        set tmp [ list ]
        if { [ info exists ::__killed_jobs ] && \
             [ llength $::__killed_jobs ] > 0 } {
           set now [ clock seconds ]
           ;## the after 1 should make the foreach loop an
           ;## atomic action in the event loop
           foreach [ list jobid start ] $::__killed_jobs {
              after 1 ::emptyDataBucket $jobid
              if { ($now - $start) < 3600 } {
                 lappend tmp $jobid $start 
              }
           }
           set ::__killed_jobs $tmp
        }
     } err ] } {
        addLogEntry $err red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dataSendThread
##
## Description:
## Threaded data socket transmission procedure. DO NOT
## destrust the ilwd pointer until the thread returns or
## the API will segfault.
##
## Parameters:
##
## Usage:
##
## Comments:
## Jobid is not set in the ilwd in this procedure, so it
## must be done BEFORE calling dataSendThread.

proc dataSendThread { jobid api ilwdp } {
     
     if { [ catch {
        set seqpt {}
        foreach [ list api host port service ] \
           [ validService $api data ] { break }
        
        ;## the ilwdp is not 'owned' by the thread and can be
        ;## prematurely destructed by the Tcl layer, so it
        ;## should be unhooked from any garbage collection
        ;## scheme implemented in the API before calling
        ;## dataSendThread!
        set seqpt "sendDataElement_t($ilwdp $api $host $port):"
        set tid [ sendDataElement_t $ilwdp $port $host ]
        
        ;## clean up before setting up any new trace on a
        ;## tid, because there is a high frequency of tid
        ;## i.d. reuse.

        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "dataSendThreadCallback $jobid $tid $ilwdp"
     } err ] } {
        catch { destructElement $ilwdp }
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dataSendThreadCallback
##
## Description:
## Callback for dataSendThread. Reaps thread and destructs
## the ilwd pointer.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dataSendThreadCallback { jobid tid ilwdp args } {
     
     if { [ catch {
        set seqpt {}
        ;## end of thread can be triggered by setting ::$tid
        ;## to the string 'FINSIHED', or by waiting for the
        ;## thread to complete when the state will be set to
        ;## the integer value $::TID_FINISHED.
        set thread_state [ set ::$tid ]
        if { [ string equal FINISHED $thread_state ] || \
           [ string equal $thread_state $::TID_FINISHED ] } {
           if { [ catch {
              set jobid [ ilwd::getjob $ilwdp ]
           } err ] } {
              addLogEntry $err red
           }
           set seqpt "sendDataElement_r($tid):"
           catch { sendDataElement_r $tid } status
           ::unset ::$tid
           catch { destructElement $ilwdp }
           if { [ string length $status ] } {
              return -code error "$seqpt $status"
           }
           ;## job should return from API NOW
           ;## if trying to maintain job processing
           ;## synchronization (to avoid hitting an
           ;## downstream API before it has received
           ;## all the data).
        }   
     } err ] } {
        catch { ::unset ::$tid }
        catch { destructElement $ilwdp }
        addLogEntry $err red
     }
}
## ******************************************************** 

