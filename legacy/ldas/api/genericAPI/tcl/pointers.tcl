## ********************************************************
##
## Description: pointers.tcl Version 1.0 
## Provides routines for the management of the destruction
## of SWIG generated pointers in Tcl.
##
## Swig generated pointers are named using a hashed value
## of the starting memory address.
## Since modern compilers attempt to reuse the most recently
## freed memory first, the likelihood of the reuse of a base
## address has become very high.
## Experiments done using LDAS have shown that the data
## conditioning API will reuse a base address every 20
## seconds under some circumstances.
## So, if any garbage collection occurs more than 20 seconds
## after a job has begun, there is a possibility of a
## 'premature destruction' of a pointer!
##
## This package solves the problem by keeping pointer
## names associated with the most recent job for which they
## were assigned.
##
## Comments:
##
## ********************************************************

;#barecode

package provide pointers 1.0

namespace eval __p {}

;#end

## ******************************************************** 
##
## Name: destructElement 
##
## Description:
## Wrapper for destructElement
##
## Parameters:
##
## Usage:
##
## Comments:
##

if { ! [ string length [ info commands destructElement_swig ] ] } {
   rename destructElement destructElement_swig
}

proc destructElement { ptr } {
     
     ;## tcl one shot reference lock
     if { [ info exists ::${ptr}_lock ] } {
        set locked 1
     } else {
        set locked 0
     }
     
     if { [ getElementRefCount $ptr ] || $locked } {
        after 1000 "destructElement $ptr"
     } else {
        catch { destructElement_swig $ptr }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: __p::add
##
## Description:
## Register a pointer with the jobid of the caller.
## Will remove registration from older job i.d.'s,
## preventing garbage collection of the current owner's
## pointer by the old owner of the pointer's name.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc __p::add { ptr } {
     
     if { [ catch {
        set jobid [ uplevel set jobid ]
        
        if { [ array exists ::__::pointers ] } {
           foreach job [ array names ::__p::pointers ] {
              set i [ lsearch $::__p::pointers($job) $ptr ]
              ;## if the pointer is already registered
              if { $i != -1 } {
                 ;## unregister it from the old job
                 ::__p::unlock $jobid $ptr
                 set ::__p::pointers($job) \
                    [ lreplace $::__p::pointers($job) $i $i ]
                 set msg    "pointer: '$ptr' reassigned from " 
                 append msg "job i.d. $job"
                 addLogEntry $msg blue
              }
           }
        }
        
        ;## if it is not already registered with this job
        ;## then register it with this job!
        if { [ lsearch $::__p::pointers($jobid) $ptr ] == -1 } {
           lappend ::__p::pointers($jobid) $ptr
        }
        ::__p::lock $ptr
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: __p::remove
##
## Description:
## Destruct a pointer only if it is called with the jobid
## argument set to the latest job i.d. known to have used
## that pointer name, for which it MUST be registered.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc __p::remove { jobid ptr } {
     
     if { [ catch {
        regexp {\d+} $jobid $N
        set destruct -1
        if { [ array exists ::__::pointers ] } {
           foreach job [ array names ::__p::pointers ] {
              set i [ lsearch $::__p::pointers($job) $ptr ]
              ;## if the pointer is registered with some job
              if { $i != -1 } {
                 ;## if it's not the same as the one provided in
                 ;## the arguments
                 if { ! [ string equal $job $jobid ] } {
                    regexp {\d+} $job $Nx
                    set msg    "pointer: $ptr owned by another job: "
                    append msg "'$job' "
                    ;## if it's an earlier job, it's safe to destruct
                    ;## and also remove all older references!
                    if { $N > $Nx } {
                       append msg "since that job is older than this "
                       append msg "job, I am removing the pointer from "
                       append msg "the older job control list."
                       addLogEntry $msg blue
                       ::__p::unlock $job $ptr
                       set ::__p::pointers($job) \
                          [ lreplace $::__p::pointers($job) $i $i ]
                       ;## don't destruct ever if we ever were set to 0
                       if { $destruct } { set destruct 1 }
                    ;## it's a newer job, so clean up all reference
                    ;## in this job and do nothing with the pointer!
                    } elseif { $Nx > $N } {
                       append msg "since that job is newer than this "
                       append msg "job, I will not destruct this "
                       append msg "pointer and will unregister it from "
                       append msg "my job control list."
                       addLogEntry $msg blue
                       ::__p::unlock $jobid $ptr
                       set ::__p::pointers($jobid) \
                          [ lreplace $::__p::pointers($jobid) $i $i ]
                       set destruct 0
                    }
                 ;## it is registered in the current job, so it's
                 ;## ok to destruct as long as no newer job ever was
                 ;## found to claim it (whence destruct would be set to 0).
                 } else {
                    if { $destruct } { set destruct 1 }
                 }
              }
           }
           if { $destruct == -1 } {
              return -code error "pointer: '$ptr' not owned by any job?!"
           }
        }
        
        ;## if we got here then either there were no jobs at all
        ;## registering pointers OR 'destruct is set to 0 or 1.
        if { $destruct == 1 } {
           ::__p::unlock $jobid $ptr
           set j [ lsearch $::__p::pointers($jobid) $ptr ]
           if { $j > -1 } {
              set ::__p::pointers($jobid) \
                 [ lreplace $::__p::pointers($jobid) $j $j ]
           }
           ::destructElement $ptr
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: __p::lock
##
## Description:
## Extra safety measure to prevent the destruction and
## re-assignment of a pointer by a new job while an old
## job is still making use of the pointer.
##
## This can happen with forced garbage collection.  This
## should not happen, but bad code might try to do it!
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc __p::lock { ptr } {
     
     if { [ catch {
        
        set jobid [ uplevel set jobid ]
        
        set lock ::${ptr}_lock
        
        if { [ array exists ::__::locks ] } {
           foreach job [ array names ::__p::locks ] {
              set i [ lsearch $::__p::locks($job) $lock ]
              if { $i != -1 } {
                 set ::__p::locks($job) \
                    [ lreplace $::__p::locks($job) $i $i ]
                 set msg    "lock: '$lock' reassigned from " 
                 append msg "job i.d. $job"
                 addLogEntry $msg blue
              }
           }
        }
        
        set $lock 1
        if { [ lsearch $::__p::locks($jobid) $lock ] == -1 } {
           lappend ::__p::locks($jobid) $lock
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: __p::unlock
##
## Description:
## Function to allow unlocking of the pointer by the job
## that owns it.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc __p::unlock { jobid ptr } {
     
     if { [ catch {
        set remove 1
        regexp {\d+} $jobid N
        if { [ info exists ::${ptr}_lock ] } {
           set i [ lsearch $::__p::locks($jobid) ::${ptr}_lock ]
           foreach job [ array names ::__p::locks ] {
              set j [ lsearch $::__p::locks($job) $ptr ]
              if { $j != -1 } {
                 if { ! [ string equal $job $jobid ] } {
                    regexp {\d+} $job $Nx
                    set msg    "pointer: $ptr lcoked by another job: "
                    append msg "'$job' "
                    if { $N > $Nx } {
                       append msg "since that job is older than this "
                       append msg "job, I am removing the lock from the "
                       append msg "older jobs control list."
                       addLogEntry $msg blue
                       set ::__p::locks($job) \
                          [ lreplace $::__p::locks($job) $j $j ]
                    } elseif { $Nx > $N } {
                       append msg "since that job is newer than this "
                       append msg "job, I will not unlock this "
                       append msg "pointer and will remove it from "
                       append msg "my control list."
                       addLogEntry $msg blue
                       set ::__p::locks($jobid) \
                          [ lreplace $::__p::locks($jobid) $i $i ]
                       set remove 0
                    }
                 }
              }
           }
           if { $i > -1 && $remove } {
              unset ::${ptr}_lock
              set ::__p::locks($jobid) \
                 [ lreplace $::__p::locks($jobid) $i $i ]
              set msg "removed lock from pointer: '$ptr'"
              debugPuts $msg
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: __p::cleanup
##
## Description:
## Remove all record of this job's pointers.  If force is
## set to 1, destruct all pointers registered with this
## job regardless of other ownership.
##
## Parameters:
## force - destruct all registered pointers regardless.
## quiet - do not log actions.
##
## Usage:
##
## Comments:
##

proc __p::cleanup { jobid { force 0 } { quiet 0 } } {
     
     if { [ catch {
        set errs [ list ]
        foreach ptr $::__p::pointers($jobid) {
           if { [ string length $ptr ] } {
              if { [ catch {
                 ::__p::remove $jobid $ptr
              } err ] } {
                 if { ! $quiet } {
                    lappend errs $err
                 }
              }
           }
        }
        if { $force } {
           foreach lock $::__p::locks($jobid) {
              catch { ::unset $lock }
           }
           foreach ptr $::__p::pointers($jobid) {
              catch { ::destructElement $ptr }
              if { ! $quiet } {
                 addLogEntry "destructed '$ptr'" blue
              }
           }
        }
        catch { ::unset ::__p::pointers($jobid) }
        catch { ::unset ::__p::locks($jobid) }
        if { [ llength $errs ] } {
           if { ! $quiet } {
              return -code error $errs
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

