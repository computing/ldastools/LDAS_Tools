## ********************************************************
## gpstime.tcl version 1.0
##
## Provides time conversion and validation routines for
## LDAS.
##
## Release date: 01.06.30
##
##
## This namespace is private and should not be "imported".
##
## ********************************************************

;#barecode
set ::RCS_ID_gpstimetcl { $Id: gpstime.tcl,v 1.23 2006/01/11 00:54:18 emaros Exp $ }
set ::RCS_ID_gpstimetcl [ string trim $::RCS_ID_gpstimetcl "\$" ]

package provide gpstime 1.0

namespace eval gpstime {}
;## Location of local leapseconds table
set ::LOCAL_LEAPSECONDS_FILE \$::PUBDIR/leapseconds
;## next two lines are for independent debugging
;##if { ! [ info exists ::API ] } { set ::API foo }
;##proc myName { args } { return . }
;#end

## ******************************************************** 
##
## Name: duration
##
## Description:
## Convert integer seconds counts into a duration of the
## form days hours minutes seconds.
##
## Parameters:
## secs - integer number of seconds
##
## Usage:
##   example:
##
##      % duration 3661
##      1 hr 1 min 1 sec
##
## Comments:
## This is a modified version of code originally written by
## David Gravereaux in comp.lang.tcl

proc duration { secs } {
     set timeatoms [ list ]
     if { [ catch { 
        foreach div { 86400 3600 60 1 } \
                mod { 0 24 60 60 } \
               name { day hr min sec } {
               
           set secs [ string trimleft $secs 0 ] 
           regsub -all {,} $secs {} secs
           set n [ expr {round($secs) / $div} ]
           if { $mod > 0 } { set n [ expr {$n % $mod} ] }
           if { $n > 1 } {
              lappend timeatoms "$n ${name}s"
           } elseif { $n == 1 } {
             lappend timeatoms "$n $name"
           }
        }
        
        ;## we may be relying on a null string for 0 seconds
        #if { [ llength $timeatoms ] == 0 } {
        #   set timeatoms [ list 0 sec ]
        #}
     
     } err ] } {
        return -code error "duration: $err"
     }
     return [ join $timeatoms ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: leapSecs 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The existence of the leapsecs file is critical to the
## starting of every API.  This proc writes to stderr because
## if it can't run, the API will not initialise.

proc leapSecs { args } {
     
     set data [ list ]
     
     set months { JAN 1 FEB 2 MAR 3 APR 4 MAY 5 JUN 6 \
                  JUL 7 AUG 8 SEP 9 OCT 10 NOV 11 DEC 12 }
     
     if { [ catch {
        set data [ getRawLeapSecondsTable ]
        
        set epochdiff 315964819
        
        foreach { mo ord } $months {
           regsub -all $mo $data $ord data
        }
       
        foreach line $data { 
           foreach { y m d 1 2 3 leap 4 5 6 7 8 9 10 11 } $line {
              set leap [ expr { int($leap) } ]
              set utc [ clock scan $m/$d/$y -gmt 1 ]
              if { $utc >= $epochdiff } {
                 set ::leapdates($leap) $utc
              }
           }
        }
        
     } err ] } {
        catch { close $fid }
        set msg "[ myName ]: CORRUPT LEAPSECONDS TABLE:\n"
        append msg "[ join $data "\n" ]\n"
        append msg "LDAS $::API PANIC!! ABORTING NOW!!"
        puts stderr "[ myName ]: '$err' $msg"
        exit
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: getRawLeapSecondsTable 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:

proc getRawLeapSecondsTable { args } {
     set err  [ list ]
     set data [ list ]
     
     set url $::REMOTE_LEAPSECOND_DATA_URL
     eval set local $::LOCAL_LEAPSECONDS_FILE
     
     if { ! [ file exists $local ] } {
        set distfile [ file join $::LDAS lib genericAPI leapseconds ]
        if { [ file exists $distfile ] } {
           file copy -force $distfile $local
	   setOffsetTableFilename $local
        } else {
           lappend err "file not found: '$local'"
           lappend err "file not found: '$distfile'"
        }
     }
     
     if { [ file exists $local ] } {
        set filetime [ file mtime $local ]
        set fid [ open $local r ]
        set data [ read $fid [ file size $local ] ]
        close $fid
        set data [ split $data "\n" ]
     }
     
     if { [ llength $data ] == 0 } {
        puts stderr "EXITING: $err"
        exit
     }
     
     if { [ llength $data ] < 37 } {
        set err "EXITING: Corrupt lepaseconds file: '$local'"
        puts stderr "EXITING: $err"
        exit
     }
     
     if { [ string equal manager $::API ] } {
        if { [ catch {
           getCurlUrl NULL0 $url $local
           file attributes $local -permissions 0644
        } err ] } {
           set msg    "error encountered while trying to update leap\n"
           append msg "seconds from $url: '$err' (falling back to `\n"
           append msg "$local created at $filetime)."
           puts stderr $msg
        }
     }
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lookupOffset 
##
## Description:
## Finds correct gps leapsecond offset for a given time and
## epoch.
##
## Parameters:
##
## Usage:
##
## Comments:
## used by gpsTime and utcTime - see use of epoch in those
## functions.

proc lookupOffset { time epoch } {
     if { [ catch {
     
        set now [ clock seconds ]
     
        if { [ regexp {^$|now} $time ] } {
           set time $now
        }
        
        ;## canonicalise input to UNIX epoch seconds
        if { [ string equal utc $epoch ] } {
           if { ! [ regexp {^-?\d{8,10}$} $time ] } {
              set time [ clock scan $time -gmt 1 ]
           }   
        }
     
        if { ! [ info exists ::leapdates ] || \
               [ llength [ array names ::leapdates ] ] == 0 } {
           leapSecs
        }
     
        set offsets [ lsort -integer [ array names ::leapdates ] ]
     
        foreach offset $offsets {
           set leapdate $::leapdates($offset)
           if { $leapdate > $now } {
              set offset [ expr { $offset - 1 } ]
              break;
           }
           if { [ string equal gps $epoch ] } {
              set leapdate [ gpsTime $leapdate ]
           }
           if { $time <= $leapdate } {
              break;
           }
        }
        if { ! [ string length $offset ] } {
           return -code error "null offset found in offsets: '$offsets'"
        }
     } err ] } {
        return -code error "lookupOffset: '$err'"
     }
     return [ list $time $offset ]
}        
## ******************************************************** 

## ******************************************************** 
##
## Name: gpsTime
##
## Description:
## Given a GMT or UNIX epoch time, return GPS time.
## If no argument is given, return current GPS time.
##
## Parameters:
## time - UNIX epoch seconds, or a date and time: "01/06/80 01:01:01"
##
## Usage:
##         set gpstime [ gpsTime "04/20/99 01:20:59" ]
##     or  set gpstime [ gpsTime 91232121 ]
##     or  set gpstime [ gpsTime 01/20/01 ]
##     or  set gpstime [ gpsTime 01/20/2001 ]
##     or  set gpstime [ gpsTime now ]
##
## Comments:
## The GPS epoch started at 0 at 01/06/80 00:00:00 GMT.

proc gpsTime { { time "" } } {
     
     if { [ catch {
        ;## the difference between the UNIX epoch and GPS epoch.
        set epochdiff 315964819
        set gpstime 0
        
        foreach [ list time offset ] [ lookupOffset $time utc ] { break }
       
        set gpstime [ expr { $time - $epochdiff + $offset } ]
        
        ;## PR 2621 - bounds check for negative gps time
        if { $gpstime < 0 } {
           set err "requested UTC time: '$time' predates the GPS epoch. "
           append err "note that the GPS epoch began at "
           append err "01/06/80 00:00:00 GMT."
           return -code error $err
        }
        
     } err ] } {
        return -code error "gpsTime: $err"
     }
     return $gpstime
}
## ******************************************************** 

## ******************************************************** 
##
## Name: utcTime
##
## Description:
## Converts GPS epoch time to UNIX epoch time
##
## Parameters:
## time - GPS time in GPS seconds, leap second corrected
##
## Usage:
##        set utctime [ utcTime gpsTime ]
##
## Comments:
## This is what happens when you adopt GPS time as your
## standard!  grumble grumble...
## Yes, it is right -- try:
##  "clock format [ utcTime [ gpsTime ] ] -gmt 0"

proc utcTime { { time "" } } {
     if { [ catch {
        if { ! [ regexp {^-?[0-9]+$} $time ] } {
           return -code error "first argument 'time' must be integer."
        }
     
        ;## the difference between the UNIX epoch and GPS epoch.
        set epochdiff 315964819
        
        foreach [ list time offset ] [ lookupOffset $time gps ] { break }
        
        set utctime [ expr { $time + $epochdiff - $offset } ]
     } err ] } {
        return -code error "utcTime: $err"
     }
     return $utctime
}
## ******************************************************** 

## ******************************************************** 
##
## Name: localTime
##
## Description:
## Convert UNIX epoch seconds to a human representation of
## the time, e.g.: 05/22/99 13:23:43.  Returns local time
## by default, but will return GMT with optional argument
## of 1.
##
## Usage:
##
## Comments:
## Should be able to operate on GPS time also.
##

proc localTime { { time "" } { gmt 0 } } {
     if { [ regexp {^$|now} $time ] } {
        set time [ clock seconds ]
     }
     return [ clock format $time -format "%x %X %Z" -gmt $gmt ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: timeType
##
## Description:
## Unreliable GPS/UTC discriminator.  Actually usable!!
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc timeType { time } {
     
     if { [ catch {
        set type UTC
        set fuzz 50
        set epochdiff 315964819
        set now [ clock seconds ]
        if { $time < ($now - $epochdiff + $fuzz) } {
           set type GPS
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $type   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: gps2utc
##
## Description:
## returns UTC time string of format 11/19/01 13:07:08
##
## time will be GMT if gmt argument is '1', local time
## if gmt argument is 0
##
## Parameters:
##
## Usage:
##
## Comments:

proc gps2utc { gpstime { gmt 1 } } {
     
     if { [ catch {
        set utctime [ utcTime $gpstime ]
		set gmttime [ clock format $utctime -format "%x %X %Z"  -gmt $gmt ] 
     } err ] } {
        return -code error "[ myName ]: error converting to UTC: $err"
     }
	 return $gmttime
}
## ******************************************************** 

## ******************************************************** 
##
## Name: utc2gps
##
## Description:
## converts local or gmt time string e.g. 05/22/99 13:23:43
## to gps. specify time zone as gmt by setting gmt to 1,
## local by setting gmt to 0.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc utc2gps { timestr { gmt 1 } } {
     
     if { [ catch {
     	set time [ clock scan $timestr -base [ clock seconds ] -gmt $gmt ]
		set gmttime [ clock format $time -format "%x %X %Z" -gmt 1 ]		
     } err ] } {
        return -code error "[ myName ]: error converting to GPS: $err"
     }
	 return [ gpsTime $gmttime ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: local2gmt
##
## Description:
## converts a local time string to the corresponding gmt
## time string.
##
## Parameters:
##
## Usage:
## local2gmt "04/20/99 01:20:59" -> 04/20/99 08:20:59 GMT
##
## Comments:
##

proc local2gmt { localtime } {
     
     if { [ catch {
     	set time [ clock scan $localtime -base [ clock seconds ] ]
		set gmttime [ clock format $time -format "%x %X %Z" -gmt 1 ]
     } err ] } {
        return -code error "[ myName ]: error converting to local time: $err"
     }
	 return $gmttime
}
## ******************************************************** 

## ******************************************************** 
##
## Name: gmt2local
##
## Description:
## Converts gmt to local time zone.
##
## Parameters:
##
## Usage:
##
##   gmt2local [ clock format [ utcTime [ gpsTime "04/20/99 01:20:59" ] ] ]
##
## Comments:
##

proc gmt2local { gmttime } {
     
     if { [ catch {
     	set time [ clock scan $gmttime -base [ clock seconds ] -gmt 1 ]
		set localtime [ clock format $time -format "%x %X %Z" ]
     } err ] } {
        return -code error "[ myName ]: error converting to GMT: $err"
     }
	 return $localtime
}
## ******************************************************** 

## ******************************************************** 
##
## Name: gpsOffset
##
## Description:
## Given a gps time, calculate the begin and end gps times
## that result from applying an offset and delta to that
## value.  
##
## Returns a list of the begin time gps seconds, begin time
## gps nanoseconds, end time gps seconds, and end time gps
## nanoseconds.
##
## This function does special rounding to make the
## calculated values usable for accessing LIGO data. 
##
## Parameters:
##
## S - the initial gps time seconds field
## N - the initial gps time nanoseconds field
## O - the offset to apply as an integer or float
## D - the delta to apply as an integer or float
##
## Usage:
##
## foreach { Ss Sn Es En } \
## [ gpsOffset 600000000 0 0.9001111 2.2222 ] \
## { break }
##
## Comments:
## Start times should round UP to the nearest nanosecond
##
## End times should round DOWN to the nearest nanosecond
##

proc gpsOffset { S N O D } {
      if { [ catch {
         ;## round up start time offset
         set Sd [ expr { ceil($O*1000000000.0)/1000000000.0 } ]
         ;## round down end time offset (includes delta)
         set Ed [ expr { floor(($O+$D)*1000000000.0)/1000000000.0 } ]

         ;## get start an end offset seconds and nanoseconds
         set Sos [ expr { int($Sd) } ]
         set Son [ expr { round(($Sd - $Sos) * 1000000000.0) } ]
         set Eos [ expr { int($Ed) } ]
         set Eon [ expr { round(($Ed - $Eos) * 1000000000.0) } ]

         foreach { startS startN } [ addGps $S $N $Sos $Son ] {}
         foreach { endS   endN   } [ addGps $S $N $Eos $Eon ] {}
         
      } err ] } {
         return -code error "gpsOffset: $err"
      }
      return [ list $startS $startN $endS $endN ]
}
## ********************************************************

## ******************************************************** 
##
## Name: addGps
##
## Description:
## Add gps time second/nanosecond pairs
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc addGps { S1 N1 S2 N2 } {

      if { [ catch {
         ;## add seconds and nanoseconds
         set St [ expr { $S1 + $S2 } ]
         set Nt [ expr { $N1 + $N2 } ]
         
         ;## when the offset is negative...
         if { $Nt < 0 } {
            set St [ expr { $St + ($Nt / 1000000000) } ]
            set Nt [ expr { $Nt % 1000000000 } ]
         }
         
         while { $Nt >= 1000000000 } {
            ;## Don't use incr so it works with both float
            ;## and integer!
            set St [ expr { $St + 1 }]
            set Nt [ expr { $Nt - 1000000000 } ]
         }
         
      } err ] } {
         return -code error "addGps: $err"
      }
      return [ list $St $Nt]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: testGpsOffset
##
## Description:
## Some important test cases for gpsOffset
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc testGpsOffset { args } {
     set Gs 600000000
     if { [ catch {
        ;## 600000000 0 600000000 0
        set 1 [ gpsOffset $Gs 0 0 0 ]
        ;## 600000001 1 600000002 1
        set 2 [ gpsOffset $Gs 1 1 1 ]
        ;## 600000001 500000000 600000002 500000000
        set 3 [ gpsOffset $Gs 500000000 1 1 ]
        ;## 600000100 500000000 600000200 500000000
        set 4 [ gpsOffset $Gs 500000000 100 100 ]
        ;## 600000100 600000000 600000200 700000000
        set 5 [ gpsOffset $Gs 500000000 100.100 100.100 ]
        ;## 600000100 500100000 600000200 500100123 
        set 6 [ gpsOffset $Gs 500000000 100.000100 100.0000001234 ]
        puts "$1\n$2\n$3\n$4\n$5\n$6"
     } err ] } {
        return -code error "testGpsOffset: $err"
     }
}
## ******************************************************** 
