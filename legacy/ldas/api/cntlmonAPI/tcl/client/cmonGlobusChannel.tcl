## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon client Tcl Script.
##
## This script has global functions used by packages
## in cmonClient
## for support of using globus toolkit for authenication
##
## requires globus toolkit to be installed on the
## system running cmonClient
##

;#barecode
package provide cmonGlobusChannel 1.0

namespace eval cmonGlobusChannel {

    set globuslibs_required [ list globus_error \
		  globus_object \
		  gt_xio_socket_swig ]
          
    set globuslibs_optional [ list globus_module ]
}

;#end  


## ******************************************************** 
##
## Name: cmonGlobusChannel::init
##
## Description:
## initialize to use the globus tcl channel
##
## Usage:
##
## Comments:

proc cmonGlobusChannel::init {} {
   
    if	{ [ catch {
    
    	if	{ ![ regexp -nocase -- {tclglobus} [ info loaded ] ] } {
        	foreach lib $::cmonGlobusChannel::globuslibs_required {
                set rc [ catch { load [ file join $::TCLGLOBUS_DIR libtcl${lib}.so ] } err ]
                if  { $rc } {
                    error $err
                }
            }
            foreach lib $::cmonGlobusChannel::globuslibs_optional {
                set libpath [ file join $::TCLGLOBUS_DIR libtcl${lib}.so ]
    		    set rc [ catch { load $libpath } err ]
                if	{ $rc } {
                	error "Error loading optional tclglobus module $libpath: $err"
                }
            }
	    }
      	;## need to know user name from proxy
        ;## set the user name here
	;## get username for now via grid-proxy-init
        catch { exec grid-proxy-info -identity } username
        regexp {CN=(.+)\s+\d+$} $username -> username
        regsub -all {\s+} $username "_" username
    	set ::globusUser [ string trim $username _ ]        
        set ::cmonGlobusChannel::passphrase x.509 
        set ::USE_GLOBUS 1
    } err ] } {	
        set ack [ tk_messageBox -type ok -default ok \
		    -message "Error installing globus support: $err; globus connection not possible." ]
        set  ::USE_GLOBUS 0
    	return -code error $err
    }
    
}

## ******************************************************** 
##
## Name: cmonGlobusChannel::open
##
## Description:
## open a globus tcl channel
##
## Usage:
##
## Comments:

proc cmonGlobusChannel::open { host port } {

	if	{ [ catch {
    	set cmd "gt_xio_socket"
    	if	{ [ string equal ldas $::RUNMODE ] } {
        	append cmd " -host"
        }
        if	{ [ info exist ::TCLGLOBUS_SOCKET_TIMEOUT ] } {
        	append cmd " -timeout $::TCLGLOBUS_SOCKET_TIMEOUT"
        }
        if	{ [ info exist ::GSI_AUTH_ENABLED ] } {
        	if	{ [ string equal "-gsi_auth_enabled" $::GSI_AUTH_ENABLED ] } {
            	append cmd " $::GSI_AUTH_ENABLED"
            }
        }
        append cmd " $host $port"
        debugPuts "cmd '$cmd'"
		set sock [ eval $cmd ] 
        ;## dont set buffersize for globus channel
        fconfigure $sock -buffering full -blocking 0
        fileevent $sock readable [ list cmonGlobusChannel::readData $sock ]
        fconfigure $sock -translation binary -encoding binary
        set ::cmonClient::sid $sock 
    } err ] } {
    	debugPuts $err
        return -code error $err
    }
    debugPuts "opened tclglobus channel $sock"
    return $sock
}
       
## ******************************************************** 
##
## Name: cmonGlobusChannel::processResult 
##
## Description:
## parse and process server data
##
## Usage:
##
## Comments:
## increase by 1 if using cmd::size

proc cmonGlobusChannel::processResult { sid server_data } {

	if	{ [ catch {
    			set site [ string trim $::cmonClient::var(site) ]
            	set result [ split $server_data \n ]
            	regexp {^\{(.+)\}$} $server_data match result
                ;## increase by 1 if using 
            	set callback [ lindex $result 0 ]
            	regexp {^\{(.*)} $callback match callback
            	set clientCmd [ lindex $result 1 ]		
	    		set rc [ lindex $result 2 ]
            	set data [ join [ lrange $result 3 end ] \n ]
                debugPuts "callback $callback clientCmd $clientCmd rc $rc data\n$data"
            	;## initial handshake
            	if	{ [ string match nocallback $callback ] } {
					set update 1
					if	{ $rc == 3 } {
                		set disconnect 1
						error $data
					}
                	;## rc=4 for pure update     		
					set cmd [ lindex $result 3 ]
					set reply [ lindex $result 4 ]
                    # debugPuts "reply $reply"
					regexp {^\{(.+)\}$} $cmd -> cmd
	 				set cmd [ subst -nobackslashes -novariables -nocommands $cmd ]
					eval $cmd  
            		if  { $rc == 0 } {
			    		regexp {([^:]+):([^:]+):([^:]+)[:]*(.*)} $clientCmd -> ::cmonClient::client \
						::cmonClient::serverVersion ::cmonClient::serverKey cstatus
                		set ::serverVersionF [ split $::cmonClient::serverVersion . ]            
			    		cmonClient::setConnected
                        set ::cmonClient::sid $sid
			    		if	{ [ string length $reply ] } {
				    		set ack [ MessageDlg .cmonClient[clock seconds] -title Warning \
							-type ok -aspect 500 -message $reply -icon warning -justify left -font $::MSGFONT ]
			    		}
                		;## if user is control, set control password
                		if  { [ string match priviledged $cstatus ] && $::passwdRemain == -1 } {
                        	;
                		}
                        set ::globus_control_status $cstatus
            		} else {
                    	error $reply
                    }
            	} else {
					set data [ subst -nobackslashes -novariables -nocommands $data ]
        			set seqpt "$callback $rc $clientCmd"
					set data [ concat $data ]
					if	{ $::DEBUG } {
                		if  { ! [ file exist $::TMPDIR ] } {
                    		file mkdir $::TMPDIR
                		}
                        if	{ ! [ regexp -nocase -- {NoOp} $data ] } {
							set fd [ ::open $::TMPDIR/dataRecv.$site w ]
							puts $fd "$callback $rc\n$data"
							::close $fd 
                        }
					}
					foreach { procname page } $callback { break }
					;## unset control password if error
                    debugPuts "procname $procname page $page rc $rc clientCmd $clientCmd data $data"
					eval { $procname $page $rc $clientCmd $data }
            
				}
	} err ] } {   
    	debugPuts "$sid error $err"  
        if	{ ! [ regexp -nocase -- {NoOp} $data ] } {
    		set fd [ ::open $::TMPDIR/dataRecv.$site w ]
			::puts $fd "$callback $rc\n$data"
			::close $fd 
        }
        set ack [ tk_messageBox -type ok -default ok \
			-message "Possible data transmission error $err, please examine details in \
				file $::TMPDIR/dataRecv.$site" \
				-icon info ]
    }
}

## ******************************************************** 
##
## Name: cmonGlobusChannel::readData 
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:

proc cmonGlobusChannel::readData { sid } {
  
    if	{ [ catch {
    	if	{ [ eof $sid ] } {
        	set eof 1
            error "end of file detected on globus channel $sid"
        }
    	set buffer [ read  $sid ]
    	set size 0 	
        set recvBytes [ string length $buffer ]
        debugPuts "received $recvBytes bytes\n'$buffer'"
        if	{ [ string length $buffer ] } {
        	while 	{ [ regexp -- {~~(\d+)\n(.+)} $buffer -> size bufdata ] } { 
                set buffer [ string range $bufdata $size end ]
                set bufdata [ string range $bufdata 0 $size ]
            	cmonGlobusChannel::processResult $sid $bufdata
            }
        }
    } err ] } {
    	debugPuts "readData error $err" 
    	catch { ::close $sid } err1
        ;## dont display end of file messages
        if	{ ! [ info exist eof ] } {
    		debugPuts "error [ myName ] $err $err1"              	
    		set ack [ tk_messageBox -type ok -default ok \
			-message "[ myName]: $err" -icon error ] 
        }
        cmonClient::state0 lostconnect  
    }
}

## ******************************************************** 
##
## Name: cmonGlobusChannel::state1
##
## Description:
## enter state1 by opening socket to server via globus XIO 
##
## Usage:
##
## Comments:

proc cmonGlobusChannel::state1 {} {
    
	if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		set ack [ MessageDlg .cmonClientDlg -type ok -aspect 100 -justify left \
        	-message "Please select an LDAS site" -icon info \
    		-title Info -font $::MSGFONT ]
		return
	} 
	if	{ [ string equal $::cmonClient::oldsite $::cmonClient::var(site) ] } {
		if	{ [ llength $::cmonClient::sid ] > 0 } {
			return
		}
	}
	set ::cmonClient::oldsite $::cmonClient::var(site)
	
	if  { [ llength $::cmonClient::sid ] == 0 } {
		if	{ [ catch {	
			set login ""
			set passwd ""
			if	{ ! $::cmonClient::noPass } {
            
            ;## verify proxy           
    			foreach { login passwd } [ validateLogin ] { break }
                              
				if	{ ! [ string equal cancelled $login ] } {
                    ;## file handle construction
					if	{ [ info exist ::siteport($::cmonClient::var(site)) ] } {
						foreach { host port } $::siteport($::cmonClient::var(site)) { break }
					} else {
						set host $::cmonClient::var(site)
					}
                    set sock [ cmonGlobusChannel::open $host $::TCLGLOBUS_PORT ] 
                    set ::cmonClient::sid $sock
                    # puts "sending user and version to $sock $::globusUser $::version"             
 					puts $sock "$::globusUser $::version" 
                    flush $sock
				}
			} 
		} err ] } {
            set ack  [ scrolledMessageDialog $err ok "GSI socket error"]
			cmonClient::setDisconnected 
			return -code error $err
		}
	}
}
