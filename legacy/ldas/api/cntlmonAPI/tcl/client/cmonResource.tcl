## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script supports addition/modification/deletion
## of LDAS and cmonClient resources.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
##
## Resource states in cmonClient: (Tags)
## Newly added:  new resource 
## Modifiable:  already defined in resource files (no tag)
## Marked for deletion: to be deleted
## Updated in memory: change has not been saved to resource file
## Added in memory: addition has not been saved to resource file
## Deleted in memory: deletion has not been saved to resource file
## 
## Resource scope:
## LDAS - common to all of LDAS and defined in LDASapi.rsc
## specific - defined only in API or cmonClient
##
## All resource vars are global to the API or cmonClient
##
## ******************************************************** 

package provide cmonResource 1.0

namespace eval cmonResource {
	set pages [ concat $::rscTargets Client ] 
	set desclen 40
	set curlen 30
	set newlen 30
	set namelen 45
	set rsclist [ concat API $::rscTargets ]
    set coltitle(desc) "Description"
    set coltitle(curvalue) "Current Value ( Modify this column only )"
    set coltitle(origvalue) "Original Value"
    set coltitle(scope) "Variable Scope"
    set changedTag MODIFIED
    set updatedTag UPDATED
    set addedTag ADDED
    set deleteTag DELETED
    set mem_addedTag MEMORY_ADD
    set mem_deleteTag MEMORY_DEL
    set globalTag LDAS
    set columnLen 50
    set linespacing 1
    set colwidth(desc) 400
    set colwidth(curvalue) 500
    set colwidth(origvalue) 500
    set colwidth(scope) 0
    set waddResource .addResource
    set warning "WARNING: Incorrect modification of resources can be fatal to the running process."
    set helptext "Help: Modifiable resources are displayed in brown. Click right mouse button on current value to edit it. \
    To restore original value before applying or saving it, double click left mouse button."
    set notes "Note: Text display in columns is controlled by BLT treeview widget and currently not configurable so it is always centered \
    in the column."
    set tags [ list $::cmonResource::changedTag $::cmonResource::updatedTag $::cmonResource::addedTag ]
    set tagColor($::cmonResource::changedTag) $::magenta
    set tagColor($::cmonResource::updatedTag) $::darkgreen
    set tagColor($::cmonResource::addedTag) $::orange
    set tagColor($::cmonResource::deleteTag) red
    set tagColor($::cmonResource::mem_deleteTag) $::darkgreen
    set tagColor($::cmonResource::mem_addedTag) $::darkgreen
    set varlist [ list global_patterns global_cmds api_patterns api_cmds global_new_descs global_new_cmds api_new_descs api_new_cmds \
            global_del_patterns global_del_descs global_del_cmds api_del_patterns api_del_descs api_del_cmds ]
}

## ******************************************************** 
##
## Name: cmonResource::create
##
## Description:
## creates Main page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook tab
## 
## Comments:
## this proc name is generic for all packages.

proc cmonResource::create { notebook } {

    set frame [ $notebook insert end cmonResource -text "Resources Vars" ]
    
	set ::cmonResource::notebook [ NoteBook $frame.nb -homogeneous 0 \
        	-foreground white -background $::darkbrown -bd 2 \
			-activeforeground white -activebackground blue ]
    set notebook1 $::cmonResource::notebook
    
    foreach page $::cmonResource::pages {
        set ::cmonResource::prevsite($page) none
        $notebook1 insert end $page -text $page \
        -createcmd "cmonResource::createPages $notebook1 $page" 
    }   
    $notebook1 raise [ $notebook1 page 0 ]
	pack $notebook1 -fill both -expand yes -padx 4 -pady 4
    $notebook compute_size
    pack $notebook -fill both -expand yes -padx 4 -pady 4
	$notebook raise [ $notebook page 0 ]
    
}

## ******************************************************** 
##
## Name: cmonResource::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## must do notebook insert into main page cmonLDAS
## 7/03/07 
proc cmonResource::createPages { notebook page } {

        set ::cmonResource::tree_resourcevars($page) [ blt::tree create tree_resourcevars_$page ]
		regsub -all {\-} $page { } pagetext
        set frame [ $notebook getframe $page ] 
		set topf  [ frame $frame.topf ]	
        set ::cmonResource::topfw($page) $topf
        
        if  { ! [ string match Client $page ] } {
		    $notebook itemconfigure $page -raisecmd \
			    [ list cmonCommon::pageRaise cmonResource $page "$page Resources" ]
            cmonCommon::pageUpdate cmonResource $page "$page Resources"
        } else {
            $notebook itemconfigure $page -raisecmd [ list cmonResource::sendRequest $page ]
        }
        specific $topf $page 
        pack $topf -fill x -pady 2
        
		set ::cmonResource::notebook2 [ NoteBook $frame.nb2 -homogeneous 0 \
        	-foreground white -background $::darkbrown -bd 2 \
			-activeforeground white -activebackground blue ]
        
        set ::cmonResource::prevsite(rscTable$page) none
        set notebook2 $::cmonResource::notebook2
        $notebook2 insert end rscTable$page -text "$page Resources" \
        	-createcmd "cmonResource::createResourceTable $notebook2 $page" \
            -raisecmd [ list cmonCommon::pageRaise cmonResource rscTable$page "$page Resources" ]
        cmonCommon::pageUpdate cmonResource rscTable$page  "$page Resources"

		set ::cmonResource::prevsite(cmdStatus$page) none
        $notebook2 insert end cmdStatus$page -text "$page Command Status" \
        	-createcmd "cmonResource::createStatus $notebook2 $page" \
            -raisecmd [ list cmonCommon::pageRaise cmonResource cmdStatus$page  "$page Command Status" ]
        cmonCommon::pageUpdate cmonResource  cmdStatus$page "$page Command Status"
        
        pack $notebook2 -fill both -expand yes -padx 2 -pady 2        
        set ::cmonResource::prevsite($page) $::cmonClient::var(site)
		$notebook2 raise [ $notebook2 page 1 ]
        $notebook2 raise [ $notebook2 page 0 ]
        cmonResource::state0 $page
}
 
## ******************************************************** 
##
## Name: cmonResource::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:   

proc cmonResource::createStatus { notebook page } {  
      
     if	{ ! [ info exist ::cmonResource::statusw($page) ] } {
        set status_frame [ $notebook getframe cmdStatus$page ] 
	;## create a status to display errors and status
		set statusw [ ::createStatus $status_frame ]
		array set ::cmonResource::statusw [ list $page $statusw ]
    }

}

## ******************************************************** 
##
## Name: cmonResource::createResourceTable
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:   

proc cmonResource::createResourceTable { notebook page } {


	if	{ [ info exist ::cmonResource::treeview_resourcevars($page) ] } {
    	return
    }
    
	;## create a scrollable window to hold variable entries
		set resource_frame [ $notebook getframe rscTable$page ]
		set sw [ScrolledWindow $resource_frame.sw -relief sunken -borderwidth 2]
        
    ;## create a treeview widget for column data
        set ::cmonResource::treeview_resourcevars($page) [ blt::treeview $sw.resourcevars$page \
        -tree $::cmonResource::tree_resourcevars($page) -background gray -foreground brown \
        -bg PapayaWhip -font $::LISTFONT -relief raised -flat 1 -icons {} -activeicons {} \
        -linespacing 20 ]
        
        $::cmonResource::treeview_resourcevars($page) sort auto 1
        $::cmonResource::treeview_resourcevars($page) bind all <Double-ButtonPress-1> \
            "+cmonResource::undoChanges $page" 
              
         set i 1
        ;## add columns headers for data values
        #$::cmonResource::treeview_resourcevars($page) style textbox editCurValue -icon {}
        #$::cmonResource::treeview_resourcevars($page) bind editCurValue <ButtonRelease-3> \
             [ list cmonResource::startEdit $page ]
             
        foreach item { desc curvalue origvalue scope } {

            if  { [ string match curvalue $item ] } {
                $::cmonResource::treeview_resourcevars($page) column insert $i $item -justify left \
                -text $::cmonResource::coltitle($item) -edit 0 -pad 5 -relief ridge  \
                -command "cmonResource::sortOnColumn $page $item 0" -activetitlebackground yellow -borderwidth 2 \
                -width $::cmonResource::colwidth($item) 
            } else {
                $::cmonResource::treeview_resourcevars($page) column insert $i $item -justify left \
                -text $::cmonResource::coltitle($item) -edit 0 -pad 10 -relief ridge \
               -command "cmonResource::sortOnColumn $page $item 0" -activetitlebackground yellow -borderwidth 2 \
               -width $::cmonResource::colwidth($item) 
            }
            incr i 1
        }      
        
        ;## add the column title to the resource name column, style does not work for column 0
        set varcol [ lindex [ $::cmonResource::treeview_resourcevars($page) column names ] 0 ]
        $::cmonResource::treeview_resourcevars($page) column configure $varcol -text "Resource name" \
            -command "cmonResource::sortOnColumn $page [ list $varcol ] 0" -activetitlebackground yellow \
            -borderwidth 5
       
        $sw setwidget $::cmonResource::treeview_resourcevars($page)
        
		set ::cmonResource::varFrame($page) $sw
        
		pack $sw -fill both -expand yes		
		
        ;## the style caused the events to freeze
        #$::cmonResource::treeview_resourcevars($page) style textbox editCurValue -icon {}
        #$::cmonResource::treeview_resourcevars($page) column configure curvalue -style editCurValue
        
        #$::cmonResource::treeview_resourcevars($page) bind editCurValue <ButtonRelease-3> \
        #     [ list cmonResource::startEdit $page ]
        ;## best bind tag that works actively 
        # $::cmonResource::treeview_resourcevars($page) bind editCurValue <Leave> [ list cmonResource::setChanged $page ]
        
        #$::cmonResource::tree_resourcevars($page) notify create -create [ list cmonResource::updateNodeColor $page ]
        
        $::cmonResource::tree_resourcevars($page) trace create all curvalue w  [ list cmonResource::updateNodeColor $page ]
        
        $::cmonResource::treeview_resourcevars($page) bind all <ButtonRelease-3> \
        [ list +cmonResource::startEdit $page ]

}

## ******************************************************** 
##
## Name: cmonResource::specific 
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonResource::specific { parent page } {

	if  { [ string match Client $page ] } {
        set pagetext Client
    } else {
        set pagetext "${page}API"
    }

	;## options menu for selecting apis
    
	;## put a trace in variable ::rscTargets each time resource is changed 
	trace variable ::rscTargets w "after 1000 ::cmonResource::traceUpdate $page"
      
	set labf1 [ frame $parent.labf1 -relief sunken -borderwidth 4]
   
    set mb1 [ menubutton $labf1.mb -text "List/Modify Resources" -menu $labf1.mb.menuA \
		-relief raised -borderwidth 2 -font $::LISTFONT ]
    pack $mb1 -padx 1 -pady 1 -fill x -expand 1
    set m1menu [ menu $mb1.menuA -tearoff 1 ]
    set ::cmonResource::functionmenu($page) $m1menu
     
    $m1menu add command -label "Get $page resource" -command "cmonResource::sendRequest $page"
    $m1menu add command -label "View Change Summary" -command "cmonResource::changeSummary view $page"
    $m1menu add command -label "Add new resource" -command "cmonResource::addResourceDialog $page"
    $m1menu add command -label "Apply Changes" -command "cmonResource::changeSummary apply $page"
    $m1menu add command -label "Apply and Save Changes" -command "cmonResource::changeSummary save $page"
            
    set labf2 [ frame $parent.labf2 -relief sunken -borderwidth 4]
    
    set mb2 [ menubutton $labf2.mb -text "Hide columns" -menu $labf2.mb.menuA \
		-relief raised -borderwidth 2 -font $::LISTFONT ]
    pack $mb2 -padx 1 -pady 1 -fill x -expand 1
    
    set m2menu [ menu $mb2.menuA -tearoff 1 ]
    foreach colname [ lsort [ array names ::cmonResource::coltitle ] ] {
    	$m2menu add check -label "Hide $::cmonResource::coltitle($colname)"  \
        	-variable ::cmonResource::showcol($page,$colname) -onvalue 1 -offvalue 0 \
            -command "cmonResource::hideshowCol $page [ list $colname ]" 
    }
        
    ;## warning and help list 
    set labf3 [ frame $parent.labf3 -relief sunken -borderwidth 4]
    set mb3 [ menubutton $labf3.mb -text "Help etc." -menu $labf3.mb.menuA \
		-relief raised -borderwidth 2 -font $::LISTFONT ]
    pack $mb3 -padx 1 -pady 1 -fill x -expand 1
    
    set m3menu [ menu $mb3.menuA -tearoff 0 ]
    
    $m3menu add command -label "Warning" -command "cmonResource::displayMsg warning" -foreground red
    $m3menu add command -label "Help" -command "cmonResource::displayMsg helptext" -foreground brown
    $m3menu add command -label "Notes" -command "cmonResource::displayMsg notes" -foreground brown
    pack  $labf3 -side right -padx 5 -pady 1
    pack  $labf1 $labf2 -side left -fill x -expand 1 -padx 5 -pady 1 
}

## ******************************************************** 
##
## Name: cmonResource::sendRequest
##
## Description:
## gets real time resource values from ldas APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## this command causes the tree to be rebuild instead of updated
## all tags will be lost

proc cmonResource::sendRequest { page } {

    catch { $::cmonResource::tree_resourcevars($page) delete root } err
          
	if	{ [ string match Client $page ] } {
	    set fd [ open $::rscfile r ]
		set data [ read -nonewline $fd ]
		close $fd
		parseRsc $page $data
		dispRsc $page
		set ::cmonResource::state($page) 1
		return
	} 

	if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonResource::statusw($page) "Please select an LDAS site"
		return 
	}
	set rscapi $page

	if	{ [ catch { 
		foreach { login passwd } [ validateLogin 1 ] { break } 	
	} err ] } {
		appendStatus $::cmonResource::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
	if	{ [ catch {
    	set client $::cmonClient::client
		set cmdId "new"
    	set repeat 0
    	set freq 0
		cmonResource::state1 $page 
    	set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId:$login:$passwd\ncntlmon::getAPIresource $rscapi \{global specific\}" 
		sendCmd $cmd     
	} err ] } {
        cmonResource::state0 $page 
		appendStatus $::cmonResource::statusw($page) $err
    }      		
}

## ******************************************************** 
##
## Name: cmonResource::showReply
##
## Description:
## process data from server
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonResource::showReply { page rc clientCmd html} {

	if	{ [ catch {
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
	set size [ string length $html ]
	set status ""
    
    set index [ string first "##" $html ]
    if  { $index > -1 } {
        set status [ string range $html 0 [ expr $index-1 ] ]
        set status [ string trim $status \| ]
        set html [ string range $html $index end ]
    } 

    ;## just getting data, not update
    if  { [ string length $html ] < 5 } {
        set html $status
        set status ""
    }

    set status [ string trim $status \n ]
    
	switch $rc {
	0 {
		if	{ [ string length $html ] } {
			parseRsc $page $html
			dispRsc $page 
            cmonResource::state2 $page      
        }
		if	{ [ string length $status ] } {
			appendStatus $::cmonResource::statusw($page) $status 0 blue
		} 
	  } 
	3 {
		appendStatus $::cmonResource::statusw($page) $html
        cmonResource::state0 $page 
	  }
	}
	} err ] } {
		appendStatus $::cmonResource::statusw($page) $err		
        cmonResource::state0 $page 	
	}
}

## ******************************************************** 
##
## Name: cmonResource::evalArrayValues
##
## Description:
## eval the names and values of an array variable
##
## Parameters:
##
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonResource::evalArrayValues {} {

    uplevel {   
        catch { $interp eval unset $name }
        if  { [ catch {
            set newvalue [ $interp eval $line ]
        } err ] } {
            appendStatus $::cmonResource::statusw($page) "evalArrayValues $name line '$line': $err"
            set newvalue "$value"
        }
        set indices [ $interp eval array names $name ]
    }
} 
    

## ******************************************************** 
##
## Name: cmonResource::parseRsc
##
## Description:
## extract configurable resources from rsc file and real time values
## into blt tree
##
## Parameters:
## page name
## data from server or file data from cmonClient 
##
## Usage:
##  
## Comments:

proc cmonResource::parseRsc { page data } {

	if	{ [ catch {
    	set line ""
    	set interp [ interp create ]
        if	{ ! [ string match Client $page ] } {
       	 	$interp eval $data
        }
		set data [ split $data \n ]
		set state 0
		set desc ""
        set errmsg ""
        set tag $page
        set longlines [ list ]

        ;## dont clear nodes here since it takes 2 rounds to get the nodes
                    
		foreach line $data {
			if	{ [ regexp {Name:[\s\t]*LDASapi.rsc} $line ] } {
				set tag $::cmonResource::globalTag
				set state 0
			} elseif { [ regexp {Name:[\s\t]*LDAS(\w+).rsc} $line -> api ] } {
				set tag $api 
				set state 0
			}
            ;## found a desc line, implies var follows
			if	{ [ regexp {^[;##]+[\s]+desc=([^!]+)} $line -> desc ] } {
				set state 1
				if	{ [ regexp {mod=([^!]+)} $line -> mod ] } {
					set tags mod_$mod
				} else {
                    set tags mod_yes
                }
			}

			if	{ [ regexp {^;## real time values} $line ] } {
				set prevarray ""
				set state 2
                set longlines [ list ]
				continue
			}
            
            if  { [ regexp {^[\s\t\n]*$} $line ] } {
                continue
            }
            set namelist [ list ]
            if  { $state == 3  } {
				lappend longlines $line
				continue
			} 
			if	{ $state == 1 || $state == 2 } { 
                if	{ [ regexp {\\[\t\s\n]*$} $line ] } {
                    lappend longlines $line
                    continue
		        } else {
					;## process the multiple line 
                    if  { [ llength $longlines ] } {
					    lappend longlines $line 
                        regsub -all {\\} [ join $longlines \ ] {} line
					    set longlines [ list ]                    
                    }
				}
				if	{ [ regexp {(^set (\S+))\s+(.+)$} $line -> pattern name value ] } {
                    if  { [ string equal Client $page ] } { 
                        if  { [ info exist $name ] } {                   
                            set value [ set $name ]  
                            if  { [ regexp {([^\(]+)\(([^\)]+)\)} $name -> arrname index ] } {
                                set name $arrname
                                set indices $index                           
                            } else {
                                set indices [ list ]
                            } 
                        } else {
                            set value "ERROR: non existent"
                        }                
                    } else {
                        if  { [ regexp {([^\(]+)\(([^\)]+)\)} $name -> arrname index ] } { 
                            evalArrayValues                                                        
                        } else {
                            set indices [ list ]
                        }                                                            
                    }
                    set namelist [ list $name "$value" ]  
				} elseif { [ regexp {(array set\s+(\S+))\s+} $line -> pattern name ] } {
                    # evalArrayValues 
                    set newvalues [ $interp eval array get $name ]  
                    lappend namelist $name $newvalues 
                    lappend tags array           
                    #foreach { index value } $newvalues {
                    #   lappend namelist ${name}($index) $value
                    #} 
				} 
                
                foreach { name value } $namelist {
                    if	{ [ regexp {^([^\(]+)\([\"]*([^\)\"]+)} $name -> arrname entry ] } {
                    	set displayed "${arrname}($entry)"
                    } else {
                    	set displayed $name
                    }
                    set nodeId [ $::cmonResource::tree_resourcevars($page) find root -exact $displayed ]
                    set numNodes [ llength $nodeId ]
                    if  { $numNodes > 1 } {
                        append errmsg "duplicate node $displayed\n"
                        continue
                    }                                  
                    set value [ wrapText $value ]
                    if  { ! [ llength $nodeId ] } {
                        ;## remove the list braces and leading and trailing spaces
                        ;## this currently does not edit well
                        set nodeId [ $::cmonResource::tree_resourcevars($page) insert root -label $displayed -data \
                        [ list desc [ wrapText $desc ] \
                        curvalue $value \
                        origvalue $value  line [ list $line ] varname $name \
                        indices $indices pattern $pattern scope $tag ] -tags $tags ]
                    } else {                                           
                        $::cmonResource::tree_resourcevars($page) set $nodeId curvalue "$value" origvalue "$value" 
                    }  
                } 
                set longlines [ list ]            
				continue													
		    }
        }
        if  { [ string length $errmsg ] } {
            error [ string trim $errmsg \n ]
        }
        $::cmonResource::tree_resourcevars($page) sort root -ascii -reorder
        updateNodeColor $page
       
        ;## display the resources in error
        ;## will pick up vars containing ERROR in value and marked them for delete
        if  { [ string equal Client $page ] } { 
        	set errnodes [ $::cmonResource::tree_resourcevars($page) find root -key curvalue -glob "*non existent*" \
            -addtag $::cmonResource::mem_deleteTag ]
        	if  { [ llength $errnodes ] } {
            	set msg ""
            	foreach node $errnodes {
                	append msg "[ $::cmonResource::tree_resourcevars($page) label $node ] "
            	}
            	error "These resources are non-existent in memory: $msg"
        	}
        }
	} err ] } {
		appendStatus $::cmonResource::statusw($page) "[myName] last line '$line' $err"
	}
}

## ******************************************************** 
##
## Name: cmonResource::dispRsc
##
## Description:
## extract configurable resources from rsc file
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## also display vars that cannot be changed

proc cmonResource::dispRsc { page } {

	if	{ [ catch {
	    set i 0
	    set color_global blue
	    set color_api #127818
        
        ;## there may not be mod or no mod tags
        catch { $::cmonResource::treeview_resourcevars($page) entry configure mod_no -foreground black }
        catch { $::cmonResource::treeview_resourcevars($page) entry configure mod_yes -foreground brown }
        
        appendStatus $::cmonResource::statusw($page)  \
            "[ expr [ $::cmonResource::tree_resourcevars($page) size root ] - 1 ] resource variables." 0 blue

	} err ] } {
		appendStatus $::cmonResource::statusw($page) "[myName] $err"
	}
}

## ******************************************************** 
##
## Name: cmonResource::applyRequest
##
## Description:
## updates real time values of ldas APIs and optional update of resource file
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## need login/password to update LDAS APIs

proc cmonResource::applyRequest { mode page } {

	set rscapi $page
    
	;## if API send to server
    if  { ![ string match Client $page ] } {
	    if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		    appendStatus $::cmonResource::statusw($page) "Please select an LDAS site"
		    return 
        }
	}
	
	set cmdId "new"
    set repeat 0
    set freq 0

	if	{ [ catch {
		set seqpt  "setAPIresource $page"
        
        set varlist $::cmonResource::varlist
      
        foreach var $varlist {
            set $var [ list ]
        }
		foreach $varlist [ setAPIresource $page $mode ] { break }
           
        set got_data 0
        foreach var $varlist {
            if  { [ llength [ set $var ] ] } {
                set got_data 1
                break
            }
        }
        ;## debug
        ;##foreach var $varlist {
        ;##    puts "$var [ set $var ]"
        ;##}
        
		if	{ !$got_data } {
            error "No changes were made"
		}
        if  { ![ string match Client $page ] } {
            cmonResource::state1 $page 
		    foreach { login passwd } [ validateLogin 1 ] { break } 	
		    if	{ ! [ string equal cancelled $login ] } { 
                cmonResource::state1 $page  
                if  { [ llength $api_cmds ] } {
       		        set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	        $::cmonClient::client:$cmdId:$login:$passwd\ncntlmon::setAPIresource $rscapi \
			        [ list $api_patterns ] [ list $api_cmds ] $mode specific " 
                    # puts "cmd $cmd"
                    sendCmd $cmd  
                }
                if  { [ llength $global_cmds ] } {
                    set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	        $::cmonClient::client:$cmdId:$login:$passwd\ncntlmon::setAPIresource $rscapi \
			        [ list $global_patterns ] [ list $global_cmds ] $mode global " 
                    # puts "cmd $cmd"
                    sendCmd $cmd 
                }
                if  { [ llength $global_new_cmds ] } {
                    set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	        $::cmonClient::client:$cmdId:$login:$passwd\ncntlmon::addAPIResources $rscapi \
			        [ list $glob_new_descs ] [ list $global_new_cmds ] $mode global " 
                    # puts "cmd $cmd"
                    sendCmd $cmd 
                }
                if  { [ llength $api_new_cmds ] } {
                    set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	        $::cmonClient::client:$cmdId:$login:$passwd\ncntlmon::addAPIResources $rscapi \
			        [ list $api_new_descs ] [ list $api_new_cmds ] $mode $rscapi" 
                    # puts "cmd $cmd"
                    sendCmd $cmd 
                }
                if  { [ llength $global_del_cmds ] } {
                    set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	        $::cmonClient::client:$cmdId:$login:$passwd\ncntlmon::deleteAPIResources $rscapi \
			        [ list $glob_new_descs ] [ list $global_new_cmds ] $mode global " 
                    # puts "cmd $cmd"
                    sendCmd $cmd 
                }
                if  { [ llength $api_del_cmds ] } {
                    set cmd "cmonResource::showReply $page\n$repeat\n$freq\n\
        	        $::cmonClient::client:$cmdId:$login:$passwd\ncntlmon::deleteAPIResources $rscapi \
			        [ list $api_del_descs ] [ list $api_del_patterns ] [ list $api_del_cmds ] $mode $rscapi" 
                    # puts "cmd $cmd"
                    sendCmd $cmd 
                }
                ;## clear the list if changes are saved to file           
                if  { [ string match save $mode ] } {
                    $::cmonResource::tree_resourcevars($page) delete root
                }                
            }
        } else {
            ;## cmonClient updates
            if  { [ llength $api_cmds ] } {
                cmonResource::applyResources $page $mode $api_patterns $api_cmds
            }
            if  { [ llength $api_new_cmds ] } {
                cmonResource::addNewResources $page $mode $api_new_descs $api_new_cmds
            }
            if  { [ llength $api_del_cmds ] } {
                cmonResource::deleteResources $page $mode $api_del_descs $api_del_patterns $api_del_cmds
            }
            if  { [ string match save $mode ] } {
                cmonResource::sendRequest $page
            }
        }      
	} err ] } {
        appendStatus $::cmonResource::statusw($page) $err
    } 
    cmonResource::state2 $page   
}

## ******************************************************** 
##
## Name: cmonResource::applyResources
##
## Description:
## apply resource changes to client 
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonResource::applyResources { page mode patterns cmds } {

    if  { [ catch {
        foreach cmd $cmds {
            catch { eval $cmd }
        }
        
        set status "cmonClient resources updated" 
        
	    if	{ [ string equal save $mode ] } {
		    set fd [ open $::rscfile r ]
		    set data [ read -nonewline $fd ]
		    set data [ split $data \n ]
		    close $fd 
		    set i 0
		    set rsctext ""
           
            ;## ensure uniqueness of each pattern
		    foreach line $data {
                set idx 0
			    foreach pat $patterns {	
				    if	{ [ regexp "${pat}(\\s|\\t)" $line ] } {
						if	{ [ regexp {array set (\S+)} $line -> arrname ] } {
							set value [ array get $arrname ]
							set line "$pat \[ list $value \]"
						} else {
							set value [ eval $pat ]
                            set len [ llength $value ]
							if	{ [ llength $value ] > 1 } {
					    		set line "$pat \[ list $value \]"
							} elseif { $len == 1 } {
								set line "$pat $value"
							} else {
                                set line "$pat {}"
                            }
						}
                        break
				    } 
			    }
                append rsctext $line\n
            }
            bak $::rscfile $::RESOURCE_BAK_LEVEL
		    set fd [ open $::rscfile w ]       
            set rsctext [ string trim $rsctext \n ]
		    puts $fd $rsctext
		    close $fd
            append status " and saved in $::env(HOME)/cmonClient.rsc"
            
		    appendStatus $::cmonResource::statusw($page) $status 0 blue
        }
	} err ] } {        
        appendStatus $::cmonResource::statusw($page) "[myName] $err"
    }
}

## ******************************************************** 
##
## Name: cmonResource::addNewResources
##
## Description:
## apply resource changes to client 
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonResource::addNewResources { page mode descs cmds } {

    if  { [ catch {
        set index 0
        foreach cmd $cmds {
            eval $cmd
        }
        set status "New resources applied to $page"
	    if	{ [ string equal save $mode ] } {
            set fd [ open $::rscfile r ]
            set rsctext [ read $fd ]
            close $fd 
            set index 0     
			foreach desc $descs {	
                append rsctext "\n;## desc=$desc\n[ lindex $cmds $index ]\n"
                incr index 1
            }  
            bak $::rscfile $::RESOURCE_BAK_LEVEL
		    set fd [ open $::rscfile w ]     
            set rsctext [ string trim $rsctext \n ]
		    puts $fd $rsctext
		    close $fd
            append status " and added to $::env(HOME)/cmonClient.rsc"            
		    appendStatus $::cmonResource::statusw($page) $status 0 blue
        }
	} err ] } {        
        appendStatus $::cmonResource::statusw($page) "[myName ] $err"
    }
}

## ******************************************************** 
##
## Name: cmonResource::deleteResources
##
## Description:
## apply resource changes to client 
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonResource::deleteResources { page mode descs patterns cmds } {

    if  { [ catch {
        set index 0
        foreach cmd $cmds {
            catch { eval $cmd }
        }
        set status "Deleted resources from $page"
	    if	{ [ string equal save $mode ] } {
 		    set fd [ open $::rscfile r ]
		    set data [ read -nonewline $fd ]
		    set data [ split $data \n ]
		    close $fd 
		    set i 0
		    set rsctext ""
            
            ;## ensure uniqueness of each pattern
		    foreach line $data {
                ;## remove the desc line also
                if  { [ regexp {desc=} $line ] } {
                    set idx 0
                    set found 0
                    foreach desc $descs {
				        if	{ [ regexp $desc $line ] } {
                            puts "found $desc in '$line'"
                            set found 1
                            break
                        }
                        incr idx 1
                    }
                    if  { !$found } {
                        append rsctext $line\n
                    } else {
                        set descs [ lreplace $descs $idx $idx ]
                    }
                    continue
			    }
                set idx 0
                set found 0
			    foreach pat $patterns {	
				    if	{ [ regexp "${pat}(\\s|\\t)" $line ] } {
                        puts "found '$pat' in '$line'"
                        set found 1
                        break
                    }
                    incr idx 1
			    }
                if  { ! $found } {
                    append rsctext $line\n
                } else {
                    set patterns [ lreplace $patterns $idx $idx ]
                }
            }
            bak $::rscfile $::RESOURCE_BAK_LEVEL
            set fd [ open $::rscfile w ]
            puts $fd [ string trim $rsctext \n ]
            close $fd 
            append status " and changes saved in $::env(HOME)/cmonClient.rsc"            
		    appendStatus $::cmonResource::statusw($page) $status 0 blue
        }
	} err ] } {        
        appendStatus $::cmonResource::statusw($page) "[myName ] $err"
    }
}


## ******************************************************** 
##
## Name: cmonResource::setAPIresource
##
## Description:
## assemble cmds to update API resources 
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## use list here since server will split cmds by \n

proc cmonResource::setAPIresource { page mode } {
 
    set varlist $::cmonResource::varlist
    foreach var $varlist {
        set $var [ list ]
    }
    set nodes_changed [ list ]
    set nodes_updated [ list ]
    set nodes_added   [ list ]
    set nodes_deleted [ list ]
    set nodes_mem_added   [ list ]
    set nodes_mem_deleted   [ list ]
    
    set nodes_added_all [ list ]
    set nodes_deleted_all [ list ]

    ;## get all the nodes changed 
    set nodes_changed [ set ::cmonResource::nodes_changed($page) ]
    
    ;## get all the nodes added 
    set nodes_added [ set ::cmonResource::nodes_added($page) ]
    
    ;## get all the nodes added 
    set nodes_deleted [ set ::cmonResource::nodes_deleted($page) ]
    
    ;## get all the changes applied but not saved to file 
    if  { [ string match save $mode ] } {
        set nodes_updated [ set ::cmonResource::nodes_updated($page) ]
        set nodes_mem_added [ set ::cmonResource::nodes_mem_added($page) ]
        set nodes_mem_deleted [ set ::cmonResource::nodes_mem_deleted($page) ]
        set nodelist [ concat $nodes_changed $nodes_updated ]
        set nodes_added_all [ concat $nodes_added $nodes_mem_added ]
        set nodes_deleted_all [ concat $nodes_deleted $nodes_mem_deleted ]
    } else {
        set nodelist $nodes_changed
        set nodes_added_all $nodes_added
        set nodes_deleted_all $nodes_deleted
    }

    foreach nodeId $nodelist { 
        set name [ $::cmonResource::tree_resourcevars($page) label $nodeId ]
        set origvalue [ $::cmonResource::tree_resourcevars($page) get $nodeId origvalue ]
        set curvalue [ $::cmonResource::tree_resourcevars($page) get $nodeId curvalue ]
        set pattern [ $::cmonResource::tree_resourcevars($page) get $nodeId pattern ]
        set nodetags [ $::cmonResource::tree_resourcevars($page) tag names $nodeId ]
        ;## make sure array vars are detected
        regsub -all {\(} $pattern "\\(" pattern
        regsub -all {\)} $pattern "\\)" pattern
        
        set global [ $::cmonResource::tree_resourcevars($page) get $nodeId scope ]
        
        ;## remove extra list put in by blt column
		if	{ [ llength $curvalue ] == 1 } {
        	set curvalue [ lindex $curvalue 0 ]
        }
        $::cmonResource::tree_resourcevars($page) set $nodeId origvalue $curvalue
        
        set curvalue [ unwrapText $curvalue ]
        set cmd "" 
        if	{ [ regexp -nocase -- {array} $nodetags ] } {
        	set cmd "array "
        }
        append cmd "set $name [ list $curvalue ]"

        if  { [ string match $::cmonResource::globalTag $global ] } {
            lappend global_cmds $cmd
            lappend global_patterns $pattern
        } else {
            lappend api_patterns $pattern
            lappend api_cmds $cmd
        }               
    }
    ;## process the new additions
  
    foreach nodeId $nodes_added_all { 
        set name [ $::cmonResource::tree_resourcevars($page) label $nodeId ]
        set curvalue [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $nodeId curvalue ] ]
        set desc [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $nodeId desc ] ]
        set global [ $::cmonResource::tree_resourcevars($page) get $nodeId scope ]
        
        ;## remove extra list put in by blt column
		if	{ [ llength $curvalue ] == 1 } {
        	set curvalue [ lindex $curvalue 0 ]
        }
        set curvalue [ unwrapText $curvalue ]
        $::cmonResource::tree_resourcevars($page) set $nodeId origvalue $curvalue
        
        ;## Fix: arrays not handled here
        if  { [ string match $::cmonResource::globalTag $global ] } {
            lappend global_new_cmds $cmd
            lappend global_new_descs $desc
        } else {
            lappend api_new_descs $desc
            lappend api_new_cmds $cmd
        }               
    }
    
    ;## process the deletions
    foreach nodeId $nodes_deleted_all { 
        set name [ $::cmonResource::tree_resourcevars($page) label $nodeId ]
        set desc [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $nodeId desc ] ]
        
        ;## make sure array vars are detected
        set pattern  [ $::cmonResource::tree_resourcevars($page) get $nodeId pattern ]
        regsub -all {\(} $pattern "\\(" pattern
        regsub -all {\)} $pattern "\\)" pattern
        
        set global [ $::cmonResource::tree_resourcevars($page) get $nodeId scope ]

        set cmd "unset $name"
        if  { [ string match $::cmonResource::globalTag $global ] } {
            lappend global_del_cmds $cmd
            lappend global_del_descs $desc
            lappend global_del_patterns $pattern
        } else {
            lappend api_del_patterns $pattern
            lappend api_del_cmds $cmd
            lappend api_del_descs $desc
        }
               
    }
    
    ;## remove changed tag
    eval $::cmonResource::tree_resourcevars($page) tag forget $::cmonResource::changedTag
    eval $::cmonResource::tree_resourcevars($page) tag forget $::cmonResource::addedTag
    eval $::cmonResource::tree_resourcevars($page) tag forget $::cmonResource::deleteTag

    if  { [ string match apply $mode ] } {
        foreach { tag nodelist } [ list $::cmonResource::updatedTag $nodes_changed \
            $::cmonResource::mem_addedTag $nodes_added $::cmonResource::mem_deleteTag $nodes_deleted ] {
            set nodecmd "$::cmonResource::tree_resourcevars($page) tag add $tag $nodelist"
            catch { eval $nodecmd } err1
        }
    } else {
        foreach { tag nodelist } [ list $::cmonResource::updatedTag $nodes_updated \
            $::cmonResource::mem_addedTag $nodes_mem_added $::cmonResource::mem_deleteTag $nodes_mem_deleted ] {
            set nodecmd "$::cmonResource::tree_resourcevars($page) tag delete $tag $nodelist"
            catch { eval $nodecmd } err1
        } 
    }

    ;## update the color of the nodes
    cmonResource::updateNodeColor $page
    
    foreach var $varlist {
        append result "[ list [ set $var ] ] "
    }
    return $result
    
    #return [ list $global_patterns $global_cmdlist $api_patterns $api_cmdlist \
    #    $global_new_desc $global_new_cmdlist $api_new_desc $api_new_cmdlist \
    #    $global_del_patterns $global_del_cmdlist $api_del_patterns $api_del_cmdlist ]
}

## ******************************************************** 
##
## Name: cmonResource::requestDone
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonResource::requestDone { page rc clientCmd html} {

    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
	switch $rc {
	0 {
		if	{ [ string length $html ] } {
			appendStatus $::cmonResource::statusw($page) $html 0 blue
            cmonResource::state0 $page      
        }
	  }
	3 {
		appendStatus $::cmonResource::statusw($page) $html
        cmonResource::state0 $page 
	  }
	}	
}


##******************************************************** 
##
## Name: cmonResource::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonResource::state0 { page } {

	if	{ [ regexp -nocase -- {cmdStatus|rscTable} $page ] } {
    	return
    }
	catch { updateWidgets $::cmonResource::topfw($page) normal }
    
    set ::cmonResource::nodes_changed($page) [ list ]
    set ::cmonResource::nodes_updated($page) [ list ]
    set ::cmonResource::nodes_added($page)   [ list ]
     	
	if	{ ! [ string match Client* $page ] } {
    	set functionmenu [set ::cmonResource::functionmenu($page) ]
        for { set i 2 } { $i <= 5 } { incr i } {
        	$functionmenu entryconfigure $i -state disabled
       	}
	}
    if  { ! [ string match Client $page ] } {
        catch { $::cmonResource::tree_resourcevars($page) delete root } err
    }
	# $::cmonResource::titf3($page) configure -text "List of resources"
	array set ::cmonResource::state [ list $page 0 ]
}

##******************************************************** 
##
## Name: cmonResource::state1  
##
## Description:
## sending request and pending reply
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonResource::state1 { page } {
	updateWidgets $::cmonResource::topfw($page) disabled
	array set ::cmonResource::state [ list $page 1 ]
}


##******************************************************** 
##
## Name: cmonResource::state2 
##
## Description:
## sending request and pending reply
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  
## the command index starts from 1-5

proc cmonResource::state2 { page } {
	updateWidgets $::cmonResource::topfw($page) normal
	array set ::cmonResource::state [ list $page 2 ]
	# $::cmonResource::titf3($page) configure -text "List of resources at $::cmonClient::var(site)"
    if  { ![ string match Client $page ] && ! [ ckNewServer $::RESOURCE_VERSION ] } {
        $::cmonResource::badd($page) configure -state disabled
    } 
    set functionmenu [set ::cmonResource::functionmenu($page) ]
   	if	{ ! [ string match Client* $page ] } {
    	for { set i 1 } { $i <= 5 } { incr i } {
        	$functionmenu entryconfigure $i -state normal
		}
	}
    if  { ![ string match Client $page ] && ! [ ckNewServer $::RESOURCE_VERSION ] } {
        $functionmenu entryconfigure 3 -state normal
    } 
    
}

#******************************************************** 
##
## Name: cmonResource::traceUpdate
##
## Description:
## update the target options button if resource has changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## 

proc cmonResource::traceUpdate { args } {

		if	{ [ catch {
			set page [ lindex $args 0 ]
			set wtarget $::cmonResource::wtarget($page)
			destroy $wtarget
			set rsclist [ concat API $::rscTargets ]
			set rscmenu [ eval tk_optionMenu $wtarget ::cmonResource::var($page,rscapi) \
        	$rsclist ]
			set ::cmonResource::wtarget($page) $wtarget
			pack $wtarget -side left
		} err ] } { 
			appendStatus $::cmonResource::statusw($page) $err 
		}
}
			
## ******************************************************** 
##
## Name: cmonResource::pageUpdate 
##
## Description:
## update title to reflect the site
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## does nothing

proc cmonResource::pageUpdate { page } {

}

## ******************************************************** 
##
## Name: cmonResource::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## invoke in cmonCommon

proc cmonResource::reset { page } {

    cmonResource::state0 $page 
}

## ******************************************************** 
##
## Name: cmonResource::sortOnColumn
##
## Description:
## sort when user clicks on a column heading 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonResource::sortOnColumn { page col descending } {

    $::cmonResource::treeview_resourcevars($page) sort configure -column $col \
        -decreasing $descending -mode ascii
    $::cmonResource::treeview_resourcevars($page) column configure $col \
        -command "cmonResource::sortOnColumn $page [ list $col ]  [ expr $descending ^ 1  ]"
    
}

## ******************************************************** 
##
## Name: cmonResource::undoChanges
##
## Description:
## sort when user clicks on a column heading 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## keep the wrapped line

proc cmonResource::undoChanges { page } {

    set nodeId [ $::cmonResource::treeview_resourcevars($page) index current ]
    set origvalue [ $::cmonResource::tree_resourcevars($page) get $nodeId origvalue ]
    $::cmonResource::tree_resourcevars($page) set $nodeId curvalue $origvalue
    $::cmonResource::tree_resourcevars($page) tag delete $::cmonResource::changedTag $nodeId
    cmonResource::updateNodeColor $page
    
}

## ******************************************************** 
##
## Name: cmonResource::hideshowCol
##
## Description:
## toggle between showing or hiding a column
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonResource::hideshowCol { page colname } {

    $::cmonResource::treeview_resourcevars($page) column configure $colname -hide $::cmonResource::showcol($page,$colname)
}

## ******************************************************** 
##
## Name: cmonResource::showSpecific
##
## Description:
## filter out some columns
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## not working per documentation
## either show all nodes or hide all nodes

proc cmonResource::showSpecific { page } {

    if  { $::cmonResource::showSpecific($page) } {
        eval $::cmonResource::treeview_resourcevars($page) hide -exact LDAS root
    } else {
        eval $::cmonResource::treeview_resourcevars($page) show -exact LDAS root
    }
    update idletasks
}

## ******************************************************** 
##
## Name: cmonResource::wrapText
##
## Description:
## wrap long text with new lines and remove leading and
## trailing blanks.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## avoids extra spaces from unwrap

proc cmonResource::wrapTextX { text } {

    ;## delistify 
    #catch { regsub {^\{\{} $text \{ text }
    #catch { regsub {\}\}$} $text \} text } 

    if  { [ string length $text ] < $::cmonResource::columnLen } {
        return $text
    } else {
        set newtext ""
        set newlen 0
        foreach word $text {
            incr newlen [ string length "$word " ]
            if  { $newlen > $::cmonResource::columnLen } {
                append newtext "\n$word"
                set newlen 0
            } else {
                append newtext " $word "
            }
        }
        set newtext [ string trim $newtext ]
        set newtext [ string trim $newtext \n ]

        # puts "text len [llength $text ] newtext [ llength $newtext ], unwrap [ llength $unwraptext ]"
        #puts "'$text'\n'$newtext'\n'$unwraptext'"
        
        return $newtext
    }
}   

proc cmonResource::wrapText { text } {

	if  { [ string length $text ] <= $::cmonResource::columnLen } {
        return $text
    } else {
        set newtext ""
        set newlen 0
        ;## dont use foreach - will delistify
        set strlen [ string length $text ]
        
        set newtext ""
       	for { set i 0 } { $i < $strlen } { incr i $::cmonResource::columnLen} {
        	set index 0
        	set limit [ expr $i + $::cmonResource::columnLen ]
            if	{ $limit > $strlen } {
            	set limit end
                append newtext "[ string range $text $i $limit ]"
            } else {
            	set segment [ string range $text $i $limit ]
            	set index [ string last " " $segment ]
            	if	{ $index > -1 } {
                	set fromend [ expr $limit - ( $index + $i ) ]
                    incr limit -$fromend
                    set index -$fromend
                    append newtext "[ string range $text $i $limit ]\n"
            	} else {
                	;## dont wrap yet
                    set index 0
                    append newtext "[ string range $text $i $limit ]"
                }
           	}
            # puts "i=$i limit $limit '[ string range $text $i $limit ]'"
        	#append newtext "[ string range $text $i $limit ]\n"
            incr i $index
            incr i 
        }
        
        set newtext [ string trim $newtext \n ]
		set unwraptext [ unwrapText $newtext ]
        if	{ [ llength $unwraptext ] != [ llength $text ] } {
        	puts "error text len [llength $text ] unwrap [ llength $unwraptext ]\n'$text'\n'$unwraptext'\n'$newtext'"
        }
        
        return $newtext
    }
}

## ******************************************************** 
##
## Name: cmonResource::unwrapText
##
## Description:
## unwrap long text by removing newlines
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonResource::unwrapText { text } {
    regsub -all {\n} $text "" text
    return [ string trim $text ]
}

## ******************************************************** 
##
## Name: cmonResource::deleteRsc
##
## Description:
## undo deletion of this var if it has not been applied yet
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonResource::UndoDeleteRsc { page dlg nodeId } {

    destroy $dlg
    $::cmonResource::treeview_resourcevars($page) tag delete $::cmonResource::deleteTag $nodeId 
    
}

## ******************************************************** 
##
## Name: cmonResource::deleteRsc
##
## Description:
## undo addition of this var
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonResource::deleteRsc { page dlg nodeId } {

    destroy $dlg
    set tags [ $::cmonResource::tree_resourcevars($page) tag names $nodeId ]
    if  { [ lsearch -exact $::cmonResource::addedTag $tags ] > -1  } {
        $::cmonResource::tree_resourcevars($page) delete $nodeId 
        set display 0
    } else {
        $::cmonResource::tree_resourcevars($page) tag add $::cmonResource::deleteTag $nodeId
        $::cmonResource::treeview_resourcevars($page) entry configure $::cmonResource::deleteTag \
            -foreground $::cmonResource::tagColor($::cmonResource::deleteTag)
        set display 1    
    }
    $::cmonResource::tree_resourcevars($page) sort root -ascii -reorder
    if  { $display } {
        $::cmonResource::treeview_resourcevars($page) see $nodeId
    }
}

## ******************************************************** 
##
## Name: cmonResource::doneEdit
##
## Description:
## set user's change into the curvalue of entry
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonResource::doneEdit { page dlg textw nodeId } {

    set changed [ wrapText [ $textw get 1.0 end ] ]
    regsub -all {\n+} $changed "\n" changed 
    $::cmonResource::tree_resourcevars($page) set $nodeId curvalue $changed

    destroy $dlg
    update idletasks
    
    set origvalue [ $::cmonResource::tree_resourcevars($page) get $nodeId origvalue ]
    set tags  [ $::cmonResource::tree_resourcevars($page) tag names $nodeId ]
    
    ;## new resource to be added, dont tag it as changed    
    if  { [ lsearch -exact $::cmonResource::addedTag $tags ] > -1 } {
        $::cmonResource::tree_resourcevars($page) set $nodeId curvalue $changed
    } elseif  { ! [ string equal $changed $origvalue ] } {
        $::cmonResource::treeview_resourcevars($page) tag add $::cmonResource::changedTag $nodeId
        $::cmonResource::tree_resourcevars($page) set $nodeId curvalue $changed
    }
    cmonResource::updateNodeColor $page
    $::cmonResource::treeview_resourcevars($page) see current
}

## ******************************************************** 
##
## Name: cmonResource::startEdit
##
## Description:
## put up a text window for user to edit
## since the original blt editor can only extend sideways
##
## Parameters:
## 
##
## Usage:
##  
##
## Comments:

proc cmonResource::startEdit { page } {

    set nodeId [ $::cmonResource::treeview_resourcevars($page) index current ]
    $::cmonResource::treeview_resourcevars($page) selection clearall
    $::cmonResource::treeview_resourcevars($page) selection set $nodeId
    set name [ $::cmonResource::tree_resourcevars($page) label $nodeId ]
 
    set tags [ $::cmonResource::tree_resourcevars($page) tag names $nodeId ]
    if  { [ lsearch -exact mod_no $tags ] > -1 } {
        set ack [ tk_messageBox -type ok -default ok \
			-message "Resource $name cannot be modified via cmonClient." -icon warning ]
        set w $::cmonResource::treeview_resourcevars($page)
        after 100 "destroy $w.edit"
        return
    }
      
    set curvalue [ $::cmonResource::tree_resourcevars($page) get $nodeId curvalue ]  
    set desc  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $nodeId desc ] ]
    set tags  [ $::cmonResource::tree_resourcevars($page) tag names $nodeId ]

    if  { [ lsearch -exact $::cmonResource::mem_deleteTag $tags ] > -1 } {
        set ack [ tk_messageBox -type ok -default ok \
			-message "Resource $name has been deleted in $page memory. You need to 'APPLY & SAVE CHANGES" -icon warning ]
        set w $::cmonResource::treeview_resourcevars($page)
        after 100 "destroy $w.edit"
        return
    }
    
    ;## done before blt creates insert editor
    set dialogw .cmonResourceCurValue${page}

    if  { ! [ winfo exist $dialogw ] } {	
        set dlg [ Dialog $dialogw -parent . -modal local -separator 1 -title   "Resource: $name" \
        	-side bottom -default 0 -cancel 3 ]	
            
        set dlgframe [$dlg getframe]
        set msgw  [ message $dlgframe.msg1 -text "$name - $desc" -aspect 1000 -justify left -font $::MSGFONT -fg black ]
        pack $msgw -side top -anchor n
        set textw [ text $dlgframe.text -height 5 -bg PapayaWhip -wrap word -tabs "0.5i left" -font $::MSGFONT ]
        pack $textw -side top -fill both -expand 1
        
        $dlg add -name update -text UPDATE -command [ list cmonResource::doneEdit $page $dlg $textw $nodeId ]
        if	{ [ string match Client $page ] } {
        	set rc 1
        } else {
        	set rc [ ckNewServer $::RESOURCE_VERSION ]
        	if  { $rc } {
            	if  { [ lsearch -exact $::cmonResource::deleteTag $tags ] > -1  } {
                	$dlg add -name undodel -text "UNDO DELETE" -command [ list cmonResource::undoDeleteRsc $page $dlg $nodeId ]
            	} else {
               	 	$dlg add -name delete -text "DELETE RESOURCE" -command [ list cmonResource::deleteRsc $page $dlg $nodeId ]
            	}
            }
        } 
        
        $dlg add -name cancel -text CANCEL -command "destroy $dlg"
        $textw insert end $curvalue
        set nlines [ regsub -all {\n+} $curvalue "\n" x ]
        $textw configure -height [ expr $nlines + 2 ]
        $dlg draw 
        ;## popdown the text editor opened by blt
        set w $::cmonResource::treeview_resourcevars($page)
        after 100 "destroy $w.edit"
    } 
}

## ******************************************************** 
##
## Name: cmonResource::addResource
##
## Description:
## add new resource to the tree
##
## Parameters:
## 
##
## Usage:
##  
##
## Comments:

proc cmonResource::addResource { page dlg textw } {
 
    if  { [ catch {
        set name [ string trim [ set ::cmonResource::newResource_${page}(name) ] ]
        if  { [ regexp {^[\s\t]*$} $name ] } {
            error "Resource name cannot be blank"
        }
        if  { [ regexp {\S+[\s\t]+\S+} $name ] } {
            error "Resource name cannot have embedded blanks"
        }
        #if  { [ string length $name ] > 100 } {
        #    error "Please keep resource name down to 100 characters."
        #}   
        set desc [ string trim [ set ::cmonResource::newResource_${page}(desc) ] ]
        if  { [ regexp {^[\s\t]*$} $desc ] } {
            error "Description cannot be blank"
        }
        if  { [ string length $desc ] > 500 } {
            error "Please keep Description down to 500 characters."
        }
        set value [ $textw get 1.0 end ]
        if  { [ regexp {^[\s\t]*$} $value ] } {
            error "Value cannot be blank"
        }
        set scope [ set ::cmonResource::newResource_${page}(scope) ]

        set mod mod_yes
        if  { ![ regexp {^::} $name ] } {
            set name ::$name
        }
        set value [ wrapText $value ]
        set nodeId [ $::cmonResource::tree_resourcevars($page) insert root \
            -label $name -data \
            [ list desc [ wrapText [ set ::cmonResource::newResource_${page}(desc) ] ] \
            curvalue $value \
            origvalue "" line {} scope $scope \
            indices {} pattern "set $name" ] -tags [ list $mod $::cmonResource::addedTag ] ]
              
        $::cmonResource::tree_resourcevars($page) sort root -ascii -reorder
        destroy $dlg
        unset ::cmonResource::newResource_${page}
        $::cmonResource::treeview_resourcevars($page) entry configure $::cmonResource::addedTag \
            -foreground $::cmonResource::tagColor($::cmonResource::addedTag)
        $::cmonResource::treeview_resourcevars($page) see $nodeId
    } err ] } {
        appendStatus $::cmonResource::statusw($page) $err
    }
}

## ******************************************************** 
##
## Name: cmonResource::addResourceDialog
##
## Description:
## dialog to add new resource to the tree
##
## Parameters:
## 
##
## Usage:
##  
##
## Comments:  
   
proc cmonResource::addResourceDialog { page } {

	if	{ [ catch {
        set dlgname .cmonResourceAddNew${page}
		set dlg [ Dialog $dlgname -parent . -modal local \
          	-separator 1 -title   "Add $page resource: " \
        	-side bottom -default 0 -cancel 3 ]
            
    	set dlgframe [$dlg getframe]
		
		set namew  [LabelEntry $dlgframe.name -label "Resource name: " -labelwidth 20 -labelanchor e \
                   -textvariable ::cmonResource::newResource_${page}(name) -editable 1 -width 20 -labeljustify right \
                   -helptext "Name of resource" ]
		set ::cmonResource::newResource_${page}(name) ""

		set descw  [LabelEntry $dlgframe.desc -label "Description: " -labelwidth 20 -labelanchor e \
                   -textvariable ::cmonResource::newResource_${page}(desc) -editable 1 -width 10 -labeljustify right \
                   -helptext "Description of resource" ]
		set ::cmonResource::newResource_${page}(desc) ""
        
        set lbl1 [ label $dlgframe.lbl1 -text "Value: " -width 20 -justify left -anchor e ]
        set valuew [ text $dlgframe.text -height 5 -bg PapayaWhip -wrap word -tabs "0.5i left" ]	
       
        set scope $page 
        #if  { ![ string equal Client $page ] } {
        #    lappend scope LDAS
        #}
        set f1 [ frame $dlgframe.f1 ]
        set lbl [ label $f1.lbl -text "Scope: " -width 20 -justify left -anchor e ]
        set rscmenu [ eval tk_optionMenu $f1.scope ::cmonResource::newResource_${page}(scope) $scope  ]
        pack $lbl $f1.scope -side left 
        
		$namew bind <Return> "focus $descw"
		$descw bind <Return> "focus $valuew"
		
        $dlg add -name ok -text OK -command "cmonResource::addResource $page $dlg $valuew"
        $dlg add -name cancel -text CANCEL -command "destroy $dlg; unset ::cmonResource::newResource_${page}"

		pack $namew $descw $lbl1 $valuew $f1 -side top -fill x -expand 1
		$dlg draw 
	} err ] } {
		set ack [ MessageDlg .cmonClientDlg -type ok -aspect 120 \
			-message $err -icon error ]
		catch { destroy $dlg }
	}
}

## ******************************************************** 
##
## Name: cmonResource::updateNodeColor
##
## Description:
## bind colors to entries with different tags
## to highlight changed, updated and added
##
## Parameters:
## 
##
## Usage:
##  
##
## Comments:  

proc cmonResource::updateNodeColor { page args } {
   
    foreach tag $::cmonResource::tags {
        catch { $::cmonResource::treeview_resourcevars($page) entry configure $tag -foreground $::cmonResource::tagColor($tag) }
    }
    update idletasks
}

## ******************************************************** 
##
## Name: cmonResource::changeSummary
##
## Description:
## display list of nodes changed, updated and added.
##
## Parameters:
## 
##
## Usage:
##  
##
## Comments:

proc cmonResource::changeSummary { mode page } {

    ;## done before blt creates insert editor
    set dialogw .cmonResourceChangeSummary${page}
    
    set ::cmonResource::nodes_changed($page) [ list ]
    set ::cmonResource::nodes_updated($page) [ list ]
    set ::cmonResource::nodes_added($page) [ list ]
    set ::cmonResource::nodes_deleted($page) [ list ]
    set ::cmonResource::nodes_mem_added($page) [ list ]
    set ::cmonResource::nodes_mem_deleted($page) [ list ]
    
    catch { set ::cmonResource::nodes_changed($page) [ $::cmonResource::treeview_resourcevars($page) \
        tag nodes $::cmonResource::changedTag ] }
    catch { set ::cmonResource::nodes_updated($page) [ $::cmonResource::treeview_resourcevars($page) \
        tag nodes $::cmonResource::updatedTag ] }
    catch { set ::cmonResource::nodes_added($page)   [ $::cmonResource::treeview_resourcevars($page) \
        tag nodes $::cmonResource::addedTag ] }
    catch { set ::cmonResource::nodes_deleted($page) [ $::cmonResource::treeview_resourcevars($page) \
        tag nodes $::cmonResource::deleteTag ] }   
    catch { set ::cmonResource::nodes_mem_added($page)   [ $::cmonResource::treeview_resourcevars($page) \
        tag nodes $::cmonResource::mem_addedTag ] }
    catch { set ::cmonResource::nodes_mem_deleted($page) [ $::cmonResource::treeview_resourcevars($page) \
        tag nodes $::cmonResource::mem_deleteTag ] } 
                 
    set nodes_changed [ set ::cmonResource::nodes_changed($page) ]
    set nodes_updated [ set ::cmonResource::nodes_updated($page) ]
    set nodes_added   [ set ::cmonResource::nodes_added($page) ]
    set nodes_deleted   [ set ::cmonResource::nodes_deleted($page) ]
    set nodes_mem_added   [ set ::cmonResource::nodes_mem_added($page) ]
    set nodes_mem_deleted   [ set ::cmonResource::nodes_mem_deleted($page) ]
    
    set color_changed [ set ::cmonResource::tagColor($::cmonResource::changedTag) ]
    set color_updated [ set ::cmonResource::tagColor($::cmonResource::updatedTag) ]
    set color_added   [ set ::cmonResource::tagColor($::cmonResource::addedTag) ]
    set color_deleted [ set ::cmonResource::tagColor($::cmonResource::deleteTag) ]
    set color_mem_added   [ set ::cmonResource::tagColor($::cmonResource::mem_addedTag) ]
    set color_mem_deleted [ set ::cmonResource::tagColor($::cmonResource::mem_deleteTag) ]
    
    set dialogw .cmonResourceChgSumm${page}
    if  { [ string match save $mode ] } {
        set saved " and changes saved in resource file"
    } else {
        set saved ""
    }
    if  { ! [ winfo exist $dialogw ] } {	
        set dlg [ Dialog $dialogw -parent . -modal local \
    		-separator 1 -title   "Summary of Modified resources:" \
        	-side bottom -default 0 -cancel 3 ]	
            set dlgframe [$dlg getframe]
            set textw [ createTextWin $dlgframe 20 both 1 ]
            set text [ list ]
            
            lappend text "Warning: these changes will be lost if you select \
            'Get $page Resource' before doing 'APPLY & SAVE CHANGES'\n\n" [ list red bold ]
            
            lappend text "To be updated in $page memory $saved: " [ list black bold ] 
                      
            foreach node $nodes_changed {
                lappend text "\n\nResource: " [ list black bold ]
                lappend text  "[ $::cmonResource::tree_resourcevars($page) label $node ]\n" [ list $color_changed fixed ]
                set origvalue [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node origvalue ] ]
                set curvalue  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node curvalue ] ]
                lappend text "\tOld value: \t" [ list black bold ] "$origvalue\n\t" [ list brown fixed ] \
                    "New Value:\t" [ list black bold ] "$curvalue" [ list $color_changed fixed ]
            }
            

            lappend text "\n\nTo be added to $page memory $saved: " [ list black bold ]           
            foreach node $nodes_added {
                lappend text "\n\nResource: " [ list black bold ] \
                 "[ $::cmonResource::tree_resourcevars($page) label $node ]\n" [ list $color_added fixed ]
                set curvalue  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node curvalue ] ]
                lappend text "\tValue:\t" [ list black bold ] "$curvalue" [ list $color_added fixed ]
            }
            
            lappend text "\n\nTo be deleted in $page memory $saved: " [ list black bold ]           
            foreach node $nodes_deleted {
                lappend text "\n\nResource: " [ list black bold ] \
                 "[ $::cmonResource::tree_resourcevars($page) label $node ]\n" [ list $color_deleted fixed ]
                set curvalue  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node curvalue ] ]
                lappend text "\tValue:\t" [ list black bold ] "$curvalue" [ list $color_deleted fixed ]
            }
            
            lappend text "\n\nAlready updated in $page memory but not saved in resource file: " [ list black bold ]           
            foreach node $nodes_updated {
                lappend text "\n\nResource: " [ list black bold ] \
                 "[ $::cmonResource::tree_resourcevars($page) label $node ]\n" [ list $color_updated fixed ]
                set curvalue  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node curvalue ] ]
                lappend text "\tMemory value:\t" [ list black bold ] "$curvalue" [ list $color_updated fixed ]
            }    
                    
            lappend text "\n\nAlready added to $page memory but not saved in resource file: " [ list black bold ]           
            foreach node $nodes_mem_added {
                lappend text "\n\nResource: " [ list black bold ] \
                 "[ $::cmonResource::tree_resourcevars($page) label $node ]\n" [ list $color_updated fixed ]
                set curvalue  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node curvalue ] ]
                lappend text "\tMemory value:\t" [ list black bold ] "$curvalue" [ list $color_updated fixed ]
            } 
            
            lappend text "\n\nAlready deleted in $page memory but not saved in resource file: " [ list black bold ]           
            foreach node $nodes_mem_deleted {
                lappend text "\n\nResource: " [ list black bold ] \
                 "[ $::cmonResource::tree_resourcevars($page) label $node ]\n" [ list $color_updated fixed ]
                set curvalue  [ unwrapText [ $::cmonResource::tree_resourcevars($page) get $node curvalue ] ]
                lappend text "\tMemory value:\t" [ list black bold ] "$curvalue" [ list $color_updated fixed ]
            }
                   
            updateTextWin $textw $text 
            if  { [ string match view $mode ] } {
                $dlg add -name ok -text OK -command "destroy $dlg"
            } else {
                $dlg add -name ok -text OK -command "destroy $dlg; cmonResource::applyRequest $mode $page"
                $dlg add -name cancel -text CANCEL -command "destroy $dlg"
            }
            $dlg draw 
    } 
}

## ******************************************************** 
##
## Name: cmonResource::displayMsg
##
## Description:
## put up a text window for user to edit
## since the original blt editor can only extend sideways
##
## Parameters:
## 
##
## Usage:
##  
##
## Comments:

proc cmonResource::displayMsg { type } {

	if	{ [ string match warning $type ] } {
		set ack [ tk_messageBox -type ok -default ok \
        	-message $::cmonResource::warning -icon warning ]
    } else {
    	set ack [ tk_messageBox -type ok -default ok \
        	-message [ set ::cmonResource::$type ] -icon info ] 
    }  
}
