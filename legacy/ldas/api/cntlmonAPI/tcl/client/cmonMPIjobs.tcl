## ******************************************************** 
## cmonMPIjobs Version 1.0
##
## 
## control Monitor API Log Filter Page 
##
## manages mpi Jobs page
##
## 
## ******************************************************** 
;##barecode

package provide cmonMPIjobs 1.0

;## use a fixed font for list to align fields
namespace eval cmonMPIjobs {
    set pages { cmonMPIjobs }
    set fmtstr "%-10s|%-10s|%-10s|%-13.13s|%7.7s|%-22.22s|%15s|"
    set fgcolor #2c348e
    set font $::LISTFONT
    set wActiveNodes .tmpActiveNodes
    set wLogs .tmpCommLogs
    set wadjPri .tmpadjPri
    set wJobChg .tmpdispChg
    set totNodes $::NUMNODES(no-server)
	set jobidlen 10
	set nodelen 15
	set prilen 8
	set sttlen 22
    set pagetext "Job Queue"
	set pattern "$::cmonMPIjobs::wActiveNodes|$::cmonMPIjobs::wLogs|$::cmonMPIjobs::wadjPri|$::cmonMPIjobs::wJobChg"
}
#end

## ******************************************************** 
##
## Name: cmonMPIjobs::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::create { notebook } {
    set page [ lindex $::cmonMPIjobs::pages 0 ] 
    set ::cmonMPIjobs::prevsite($page) none
    $notebook insert end cmonMPIjobs -text MPI \
        -createcmd "cmonMPIjobs::createPages $notebook $page" 
    $notebook raise [ $notebook page 0 ]
}

## ******************************************************** 
##
## Name: cmonMPIjobs::createPages 
##
## Description:
## creates MPI Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::createPages { notebook0 page } {

    if  { [ catch {
        
    	set frame [$notebook0 getframe cmonMPIjobs ]
		;## build notebook to hold all APIs
		set ::cmonMPIjobs::notebook [ NoteBook $frame.nb \
        -foreground white -background $::darkorange -bd 2 \
		-activeforeground white -activebackground blue ]	
		set notebook $::cmonMPIjobs::notebook
        
        $notebook0 itemconfigure cmonMPIjobs -raisecmd "cmonCommon::toppageRaise $notebook"
    
    	set seqpt ""
		cmonNodesStat::create $notebook
        cmonLoadSummary::create $notebook
        
		;## create job page
	    set frame [$notebook insert end cmonMPIjobs:$page -text $::cmonMPIjobs::pagetext \
            -raisecmd "cmonCommon::pageRaise cmonMPIjobs cmonMPIjobs $::cmonMPIjobs::pagetext" ]
		set topf  [ frame $frame.topf ]
	    specific $topf $page
		pack $topf -fill x -pady 2
		
		foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	    set titf2 [TitleFrame $pane2.titf2 -text "Command status" -font $::ITALICFONT ]     
        ;## create a status to display errors and status
	    set	statusw [ ::createStatus  [ $titf2 getframe ] ]
	    array set ::cmonMPIjobs::statusw [ list $page $statusw ]
		pack $titf2 -padx 2 -padx 4 -fill both -expand yes 
		
        set titf3 [TitleFrame $pane3.titf3 -text "MPI Jobs" -font $::ITALICFONT ]
		set ::cmonMPIjobs::titf3($page) $titf3
        jobQueue [ $titf3 getframe ] $page 
        array set ::cmonMPIjobs::state [ list $page 0 ]
		pack $titf3 -pady 2 -padx 4 -fill both -expand yes
		pack $pw1 -side top -fill both -expand 1
		
		$notebook compute_size
		pack $notebook -fill both -expand yes -padx 4 -pady 4
        set ::cmonMPIjobs::prevsite($page) $::cmonClient::var(site) 
        
    } err ] } {
        puts "cmonMPIjobs::create $page $err"
        return -code error $err
    }	
    return $frame
}

## ******************************************************** 
##
## Name: cmonMPIjobs::specific 
##
## Description:
## creates MPI Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::specific { parent page } {

	if  { [ catch {
	set seqpt "start"
	set labfent [LabelFrame $parent.fent -text "List MPI Jobs" -side top -anchor w \
                   -relief sunken -borderwidth 4]
	set subf1 [ $labfent getframe ]	
	set seqpt but1	  
	set but1 [ Button $subf1.login -text "Get MPI Jobs" \
		-helptext "get a list of MPI jobs from server" \
        -command  "::cmonMPIjobs::sendRequest $page getJobInfo" ]
    set ::cmonMPIjobs::bstart($page) $but1
	set seqpt "but2"
	set but2 [ Button $subf1.apply -text "Submit changes"  \
		-helptext "submit request to abort Jobs" \
        -command  "cmonMPIjobs::sendRequest $page setJobChgs" \
        -state disabled ]
    set ::cmonMPIjobs::bapply($page) $but2  
	set seqpt "but3"
    set but3  [ Button $subf1.reset -text "Undo all changes" \
		-helptext "reset all changes" -fg blue \
        -command  "::cmonMPIjobs::undoAll $page" -state disabled ]
	set ::cmonMPIjobs::breset($page) $but3
	
	set but4  [ Button $subf1.select -text "Abort all" \
		-helptext "abort all jobs" -fg red \
        -command  "::cmonMPIjobs::abortAll $page" -state disabled ]
	set ::cmonMPIjobs::bselect($page) $but4
	 
	pack $but1 $but4 $but3 $but2 -side left -padx 2 -pady 2 -anchor w
	pack $labfent -side left -anchor n -fill both -expand yes -padx 8
	} err ] } {
		puts "[ myName ] error $seqpt $err"
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::jobQueue
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::jobQueue { parent page } {

    set pw1   [PanedWindow $parent.pw1 -side top ]
    set pane3 [$pw1 add -minsize 100]
    set pane  [$pw1 add -minsize 100]

    set pw2   [PanedWindow $pane.pw2 -side right ]
    set pane1 [$pw2 add -minsize 70]

    ;## list of jobs on the left pane
    set sw    [ScrolledWindow $pane3.sw]
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }
	set lbhdr [ listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single \
        -font $::LISTFONT ]
    $lbhdr insert end \
        [ format $::cmonMPIjobs::fmtstr ABORT "CHG PRI" UNDO JOBID #NODES "  START TIME  " \
			"COMMMUNICATION" ]
	$lbhdr itemconfigure 0 -fg black
		 
	$sw setwidget $lbhdr

	set sf [ScrollableFrame $sw.f -height 1000 -areaheight 0 ]	
    $sw setwidget $sf	
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $sf ] ]
	
	set subf [$sf getframe]
	set subfh [ frame $subf.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonMPIjobs::varFrame($page) $subfh
	set ::cmonMPIjobs::varFrameParent($page) [ winfo parent $subfh ]
	pack $subfh -fill both -expand 1
	pack $sw -fill both -expand yes	
	
    ;## second pane to hold job details and job modification
    
    set sw2 [ScrolledWindow $pane1.sw -relief sunken -borderwidth  3 ]
    set sf [ScrollableFrame $sw2.f]
    $sw2 setwidget $sf
    set subf [$sf getframe]
    set msg [ message $subf.msg -text "LDAS Job Details" -justify left \
        -aspect 200 ]
    pack $msg -side top -fill x -expand yes
    set ::cmonMPIjobs::details($page) $msg
    
    pack $sw2 -side top -fill both -expand yes
    pack $pw1 $pw2 -fill both -expand yes

}

## ******************************************************** 
##
## Name: cmonMPIjobs::sendRequest 
##
## Description:
## send request 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::sendRequest { page request } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonMPIjobs::statusw($page) "Please select an LDAS site"
		return 
	}
    ;## if password ok,submit request
	set clientdata ""
	set login ""
	set passwd ""
	
    set cmdId "new"
    set repeat 0
    set freq 0      
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    switch $request {
        getJobInfo {}
        setJobChgs { 
            dispChgs $page 
			if	{ ! $::cmonMPIjobs::var(submitOK) } {
				return
			}	
			if	{ [ catch {
    			foreach { login passwd } [ validateLogin 1 ] { break }
				set clientdata ":$login:$passwd"
			} err ] } {
	    		appendStatus $::cmonMPIjobs::statusw($page) $err
       			return
			}
			if	{ [ string equal cancelled $login ] } {
				return
			}    
            set request "$request \{$::cmonMPIjobs::var(jobsAbort)\} \{$::cmonMPIjobs::var(jobsChgPri)\}"
			set msg ""
			if	{ [ llength $::cmonMPIjobs::var(jobsAbort) ] } { 
				append msg "aborting jobs $::cmonMPIjobs::var(jobsAbort);"
			}
			if	{ [ llength $::cmonMPIjobs::var(jobsChgPri) ] } { 
				append msg " adjusting priorities $::cmonMPIjobs::var(jobsChgPri)" 
			}
			appendStatus $::cmonMPIjobs::statusw($page) $msg 0 blue
        }
    }
    set ::cmonMPIjobs::var(jobsAbort)  [ list ]
	set ::cmonMPIjobs::var(jobsChgPri) [ list ]
    if  { [ catch {
        set cmd "cmonMPIjobs::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId$clientdata\ncntlmon::$request" 
        #puts "cmd=$cmd"
        cmonMPIjobs::state1 $page 
		sendCmd $cmd 
	} err ] } {
		 cmonMPIjobs::state0 $page 
         appendStatus $::cmonMPIjobs::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::commSend
##
## Description:
## send request to server to get wrapperAPI communication logs 
## 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::commSend { page jobid button } {
 
    set cmdId "new"
    set repeat 0
    set freq 0      
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    appendStatus $::cmonMPIjobs::statusw($page) "Retrieving mpiAPI logs for job $jobid" 0 blue
	$button configure -state disabled 
	set ::cmonMPIjobs::var($jobid,commbutton) $button
    if  { [ catch {  
        set cmd "cmonMPIjobs::commReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::queryLogsPipe queryJobLogs $jobid MPICommLog  \{.+\} [ list mpi ] --" 
        # puts "cmd=$cmd"
		sendCmd $cmd 
	} err ] } {
        appendStatus $::cmonMPIjobs::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::showReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::showReply { page rc clientCmd html } {
    
	if	{ [ catch {
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
	set cmonClient::client $client
    set cmdId [ lindex $clientCmd 1 ]
    #puts "in showReply, page=$page,rc=$rc, client=$client,cmdId=$cmdId, afterid=$afterid"
	#puts "html=$html"
	set seqpt ""
	switch $rc {
	0 {
        set ::cmonMPIjobs::cmdId($page) $cmdId
        cmonMPIjobs::state2 $page  
		set seqpt "cmonMPIjobs::dispJobs $page \$html"
		set ::cmonMPIjobs::var(numJobs) [ cmonMPIjobs::dispJobs $page $html ]
		if	{ $::cmonMPIjobs::var(numJobs) } {
            cmonMPIjobs::state2 $page 
            appendStatus $::cmonMPIjobs::statusw($page) \
				"$::cmonMPIjobs::var(numJobs) jobs in queue." 1 blue
        } else {
            cmonMPIjobs::state0 $page	
			appendStatus $::cmonMPIjobs::statusw($page) \
				"No jobs in the queue." 1 blue	 
        }               
	  }
    1 {
        appendStatus $::cmonMPIjobs::statusw($page) $html 0 blue
        cmonMPIjobs::state0 $page 
      }
	3 {
        appendStatus $::cmonMPIjobs::statusw($page) $html 1
        cmonMPIjobs::state0 $page        
	  }
	}
	} err ] } {
		appendStatus $::cmonMPIjobs::statusw($page) $err  
	}	
}

## ******************************************************** 
##
## Name: cmonMPIjobs::commReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::commReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
	set cmonClient::client $client
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in commReply, page=$page,rc=$rc, client=$client,cmdId=$cmdId, afterid=$afterid"
    #puts "rc=$rc,html=$html"
    
    set status ""
	regexp {(^[^<\n]*)\n*(<.*>$)*} $html match status html 	

	cmonMPIjobs::state2 $page 
	switch $rc {
	0 { regexp {Report on Job (\d+)} $html -> jobid   
        set win .log-cmonMPIjobs:$page:$jobid
        
        ;## support 1 release before
        if  { ! [ string length $status ] } {
             set site [ string trim $::cmonClient::var(site) ]
             set status file:$::TMPDIR/cmonMPIjobs:$page:${jobid}_${site}_1.html
             set fd [ open $::TMPDIR/cmonMPIjobs:$page:${jobid}_${site}_1.html w ]
             puts $fd $html
             close $fd
        }        
        cmonCommon::dispLogs cmonMPIjobs $page $status $html $win       
        [ set ::cmonMPIjobs::var($jobid,commbutton) ] configure -state normal
	  }    
    1 {
        appendStatus $::cmonMPIjobs::statusw($page) $status 0 blue
      }
    2 { appendStatus $::cmonMPIjobs::statusw($page) $status 0 blue
	  }
	3 {
        appendStatus $::cmonMPIjobs::statusw($page) $status 1
	  }
	}	
}

#******************************************************** 
##
## Name: cmonMPIjobs::mpiQueue 
##
## Description:
## build a queue array like that in mpiAPI for displaying
## job information
##
## Parameters:
## result
##
## Usage:
##  handler for cmd results
## 
## Comments:

proc cmonMPIjobs::mpiQueue { result } {
    set len [ llength $result ]
    set result [ lrange $result 0 [ expr $len - 1 ] ]
	set ::cmonMPIjobs::var(joblist) [ list ]
    foreach { name val } $result {
        if  { [ regexp {mpi.*:emergency:executed|NULL} $name ] } {
            continue
        }
        if  { [ regexp $::MPI_USER $name ] } {
            continue
        }
        array set ::cmonMPIjobs::mpiqueue [ list $name $val ]
        set name [ split $name , ]
        set job [ lindex $name 0 ]
		if  { [ lsearch $::cmonMPIjobs::var(joblist) $job ] == -1 } {
            lappend ::cmonMPIjobs::var(joblist) $job
        } 
    }
    return [ llength $::cmonMPIjobs::var(joblist) ]
}

#******************************************************** 
##
## Name: cmonMPIjobs::dispJobs 
##
## Description:
## display the jobs in mpi queue in listbox
##
## Parameters:
## page
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:

proc cmonMPIjobs::dispJobs { page html } {

    if  { [ catch {
		set parent $::cmonMPIjobs::varFrameParent($page) 
		;## must destroy widget in parseRsc or globalVar array cannot be unset
		catch { destroy $::cmonMPIjobs::varFrame($page) }
	
		set subfh [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
		set ::cmonMPIjobs::varFrame($page) $subfh

		set msg ""
        set numJobs [ mpiQueue $html ]
        $::cmonMPIjobs::details($page) configure -text "MPI Job Details" -fg black
		set i 0 
        if  { $numJobs } {
            foreach job $::cmonMPIjobs::var(joblist) {
				if	{ ! [ info exist ::cmonMPIjobs::mpiqueue($job,nod) ] } {
					set ::cmonMPIjobs::mpiqueue($job,nod) 0
				}
				set subf1 [ frame $subfh.f$i -relief sunken ]
				if	{ $i == 0 } {
					set eventcmd "jobDetails $page $subf1 $i"
				}
                set numnodes [ llength $::cmonMPIjobs::mpiqueue($job,nod) ]
                ;## start time is now a list of { ms gpstime }
				if	{ ! [ info exist ::cmonMPIjobs::mpiqueue($job,pri) ] } {
					set ::cmonMPIjobs::mpiqueue($job,pri) 0
				}
				if	{ ! [ info exist ::cmonMPIjobs::mpiqueue($job,stt) ] } {
					set ::cmonMPIjobs::mpiqueue($job,stt) [ list 0 0 ]
				}
				if	{ ! [ info exist ::cmonMPIjobs::mpiqueue($job,usr) ] } {
					set ::cmonMPIjobs::mpiqueue($job,usr) none
				}
				radiobutton $subf1.del$i -width 6 \
					-text Abort -justify left -variable ::cmonMPIjobs::var($job,action) \
					-value Abort -command "$subf1.jobid$i configure -fg red" \
					-anchor w -font $::LISTFONT	-relief raised -bg PapayaWhip \
					-fg red
					
				radiobutton $subf1.chgpri$i -width 7 \
					-text "Chg Pri" -justify left -variable ::cmonMPIjobs::var($job,action) \
					-command "cmonMPIjobs::adjPri $page $subf1 $i" \
					-anchor w -font $::LISTFONT	-relief raised -bg PapayaWhip \
					-fg purple -value ChgPri
					
				radiobutton $subf1.undo$i -width 7 \
					-text Undo -justify left -variable ::cmonMPIjobs::var($job,action) \
					-command "cmonMPIjobs::undoChgs $subf1 $i" \
					-anchor w -font $::LISTFONT	-relief raised -bg PapayaWhip \
					-fg black -value Undo
					
				button $subf1.jobid$i -width $::cmonMPIjobs::jobidlen \
					-text $job -justify right \
					-command [ list cmonMPIjobs::jobDetails $page $subf1 $i ] \
					-anchor e -font $::LISTFONT	-relief ridge -bg white 
				
                bind $subf1.jobid$i <Button-1> [ list cmonMPIjobs::jobDetails $page $subf1 $i ]
                bind $subf1.jobid$i <Button-3> [ list cmonLogs::autoLogFilter $job ]
                
				set ::cmonMPIjobs::var($job,pri) $::cmonMPIjobs::mpiqueue($job,pri)
				label $subf1.pri$i -width $::cmonMPIjobs::prilen \
					-text $::cmonMPIjobs::var($job,pri) -fg brown -justify right \
					-anchor e -font $::LISTFONT	-relief ridge -bg gray
				
				set stt [ gps2utc [ lindex $::cmonMPIjobs::mpiqueue($job,stt) 1 ] 0 ] 
				label $subf1.stt$i -width $::cmonMPIjobs::sttlen \
					-text $stt  \
					-fg black -justify left \
					-anchor e -font $::LISTFONT	-relief ridge -bg white	
				
				button $subf1.comm$i  -command "cmonMPIjobs::commSend $page $job $subf1.comm$i" \
					-text "Get mpiAPI Logs " -relief raised \
					-fg blue -justify right -bg PapayaWhip \
					-anchor e -font $::LISTFONT
                if  { [ string match search* $job ] } {
                    $subf1.comm$i configure -state disabled
                }
				
				pack $subf1.del$i $subf1.chgpri$i $subf1.undo$i $subf1.jobid$i $subf1.pri$i \
					$subf1.stt$i $subf1.comm$i \
					-side left -fill both -anchor w 
					
				pack $subf1 -side top -fill x -expand 1
				incr i 1										
            }
            pack $subfh -side top -fill both -expand 1
			eval $eventcmd
        } else {
            $::cmonMPIjobs::details($page) configure -text "No jobs found" -fg #5c10cc \
            -font $::cmonMPIjobs::font
        }
    } err ] } {
        appendStatus $::cmonMPIjobs::statusw($page) "dispJobs error: $err" 
        return -code error $err
    }
    return $numJobs 
}

## ******************************************************** 
##
## Name: cmonMPIjobs::jobDetails
##
## Description:
## display details of a job on right pane window
##
## Parameters:
## job listbox widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::jobDetails { page parent index } {

	set jobid [ $parent.jobid$index cget -text ]
	set stt   $::cmonMPIjobs::mpiqueue($jobid,stt)
	set priority  $::cmonMPIjobs::mpiqueue($jobid,pri)
	
	set nodetext ""
	foreach nodeset $::cmonMPIjobs::mpiqueue($jobid,nod) {
		set node [ lindex $nodeset 0 ]
		append nodetext "$node "
	}
    if  { [ info exist ::cmonMPIjobs::mpiqueue($jobid,cmd) ] } {
        set cmd [ set ::cmonMPIjobs::mpiqueue($jobid,cmd) ]
    } else {
        set cmd ""
    }
    set details "Jobid:\t\t$jobid\nPriority:\t$priority\n\
			User:\t\t$::cmonMPIjobs::mpiqueue($jobid,usr)\n\
        	Started:\t$stt\n\
			Active Nodes:\t$nodetext\n\
        	Command:\t$cmd"
   	$::cmonMPIjobs::details($page) configure -text $details -fg #5c10cc \
        	 -font $::cmonMPIjobs::font	  
}

## ******************************************************** 
##
## Name: cmonMPIjobs::abortAll
##
## Description:
## select all jobs to abort
##
## Parameters:
## page
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::abortAll { page } {

	set subfh $::cmonMPIjobs::varFrame($page)
	for { set i 0 } { $i < $::cmonMPIjobs::var(numJobs) } { incr i 1  } {
		set subf1 $subfh.f$i 
		set jobid [ $subf1.jobid$i cget -text ]
		set ::cmonMPIjobs::var($jobid,action) Abort
		$subf1.jobid$i configure -fg red
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::undoAll
##
## Description:
## reset job marked killed
##
## Parameters:
## page
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::undoAll { page } {

	set subfh $::cmonMPIjobs::varFrame($page)
	for { set i 0 } { $i < $::cmonMPIjobs::var(numJobs) } { incr i 1  } {
		set subf1 $subfh.f$i 
		undoChgs $subf1 $i
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::undoChgs
##
## Description:
## reset job marked killed
##
## Parameters:
## page
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::undoChgs { parent index  } {

	set jobid [ $parent.jobid$index cget -text ]
	$parent.jobid$index configure -fg black
	$parent.pri$index configure -text $::cmonMPIjobs::mpiqueue($jobid,pri) -fg brown
	set ::cmonMPIjobs::var($jobid,action) Undo

}

## ******************************************************** 
##
## Name: cmonMPIjobs::adjPri
##
## Description:
## mark an mpi job to be killed
##
## Parameters:
## page
## create   - option to create dialog
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::adjPri { page parent index } {

	if	{ [ catch {
		set job [ $parent.jobid$index cget -text ]
        set oldpriority $::cmonMPIjobs::mpiqueue($job,pri)
        set dlg [Dialog $::cmonMPIjobs::wadjPri -parent . -modal local \
          	-separator 1 -title   "Change Priority for job $job: " \
        	-side bottom -anchor  w -default 0 -cancel 1 ]
        $dlg add -name ok
        $dlg add -name cancel
        set dlgframe [$dlg getframe]
        set ::cmonMPIjobs::priority $oldpriority
		set site $::cmonClient::var(site)

		set lab [ label $fldascmds.lab -text "New Priority (# Nodes): " -width 25 -justify left -anchor w ]
		set spin  [ComboBox $dlgframe.pri -width 3 \
                   -values $nodelist -textvariable ::cmonMPIjobs::var($job,pri) \
                   -helptext "Priority levels: up to total number of nodes"]
        pack $lab $spin -side left -padx 6 -pady 4 -fill x
        $dlg draw 
        destroy $dlg
		$parent.jobid$index configure -fg black
        if  { $oldpriority != $::cmonMPIjobs::var($job,pri) } {
			$parent.pri$index configure -text $::cmonMPIjobs::var($job,pri) -fg purple
        } else {
			$parent.pri$index configure -text $oldpriority -fg brown
            # appendStatus $::cmonMPIjobs::statusw($page) "$job priority is the same."
        }
	} err ] } {
		appendStatus $::cmonMPIjobs::statusw($page) $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::updateChgs
##
## Description:
## set the list of jobs to be killed or to adjust priority
##
## Parameters:
## page
## create   - option to create dialog
##
## Usage:
##  
## 
## Comments:
## sets the list of jobs to be killed and jobs to adjust priority

proc cmonMPIjobs::updateChgs { page } { 

	set subfh $::cmonMPIjobs::varFrame($page)
	set ::cmonMPIjobs::var(jobsAbort) [ list ]
	set ::cmonMPIjobs::var(jobsChgPri) [ list ]
	for { set i 0 } { $i < $::cmonMPIjobs::var(numJobs) } { incr i 1  } {
		set subf1 $subfh.f$i 
		set jobid [ $subf1.jobid$i cget -text ]
		set type $::cmonMPIjobs::var($jobid,action)
		if	{ [ string match Abort $type ] } {
			lappend ::cmonMPIjobs::var(jobs${type}) $jobid
		} else {
			lappend ::cmonMPIjobs::var(jobs${type}) \
				$jobid $::cmonMPIjobs::var($jobid,pri)
		}
	}
}

## ******************************************************** 
##
## Name: cmonMPIjobs::dispChgs
##
## Description:
## display on popup the list of jobs to be killed or
## to adjust priority
##
## Parameters:
## page
## create   - option to create dialog
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMPIjobs::dispChgs { page } {

	if	{ [ catch {
    	updateChgs $page
		set aborts $::cmonMPIjobs::var(jobsAbort)
		set chgpris $::cmonMPIjobs::var(jobsChgPri)
		set numaborts [ llength $aborts ]
		set numchgpris [ llength $chgpris ]
		 
		if	{ $numaborts || $numchgpris } {
    		set dialogw $::cmonMPIjobs::wJobChg
    		if  { ! [ winfo exist $dialogw ] } {
    			set dlg [Dialog $dialogw -parent . -modal local \
   				-separator 1 -title   "Abort Jobs" \
       			-side bottom -anchor  s -default 0 -cancel 1]
				set dlgframe [ $dlg getframe ]
				
				if	{ $numaborts } {
    				;## create a label for jobs to be aborted
					set lbl1 [ label $dlgframe.lbl1 -text "Jobs to be aborted" ]
					pack $lbl1 -side top -expand no -fill x	
    				set lbl2 [ label $dlgframe.lbl2 -text \
					[ join $aborts \n ] -fg red \
        			-justify left -relief sunken -font $::MSGFONT ]
					pack $lbl1 $lbl2 -side top -expand no -fill x
				}
				
				if	{ $numchgpris } {
					;## create a label for jobs priority adjust
					set lbl3 [ label $dlgframe.lbl3 -text "Jobs priorities to be adjusted" ]
					pack $lbl3 -side top -expand no -fill x	
					set text ""
					foreach { jobid newpri } $chgpris {
						append text "$jobid ($::cmonMPIjobs::mpiqueue($jobid,pri) -> $newpri)\n" 
					}
    				set lbl4 [ label $dlgframe.lbl4 -text \
						[ string trim $text \n ] -fg purple \
        			-justify left -relief sunken -font $::MSGFONT ]
    				set ::cmonMPIjobs::lblPri $lbl4
    				pack $lbl3 $lbl4 -side top -expand 0 -fill x	
				}
				$dlg add -name 0 -text OK -command "set ::cmonMPIjobs::var(submitOK) 1; destroy $dlg"
				$dlg add -name cancel -command "set ::cmonMPIjobs::var(submitOK) 0; destroy $dlg" 
				$dlg draw
			}	
		} else {
			set ::cmonMPIjobs::var(submitOK) 0
			error "No changes made"
		}
	} err ] } {
		appendStatus $::cmonMPIjobs::statusw($page) $err
	}
}		
				
## ******************************************************** 
##
## Name: cmonMPIjobs::state0
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::state0 { page } {
    
	catch { destroy $::cmonMPIjobs::varFrame($page) }
	catch { unset ::cmonMPIjobs::var }
    $::cmonMPIjobs::details($page) configure -text "" 
    catch { unset ::cmonMPIjobs::mpiqueue }
	$::cmonMPIjobs::breset($page) configure -state disabled  
	$::cmonMPIjobs::bapply($page) configure -state disabled
	$::cmonMPIjobs::bstart($page) configure -state normal 
	$::cmonMPIjobs::bselect($page) configure -state disabled 
	array set ::cmonMPIjobs::state [ list $page 0 ]
	;## remove any dialogs up

	foreach child [ winfo children . ] {
		if	{ [ regexp $::cmonMPIjobs::pattern $child ] } {
			catch { destroy $child } err
		}
	}

}


## ******************************************************** 
##
## Name: cmonMPIjobs::state1
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::state1 { page } {
   	$::cmonMPIjobs::breset($page) configure -state disabled  
	$::cmonMPIjobs::bapply($page) configure -state disabled
	$::cmonMPIjobs::bstart($page) configure -state normal
	$::cmonMPIjobs::bselect($page) configure -state disabled 
	array set ::cmonMPIjobs::state [ list $page 1 ]
}

## ******************************************************** 
##
## Name: cmonMPIjobs::state2
##
## Description:
## ready to send request to process MPI job changes
## after job list is displayed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::state2 { page } {
	$::cmonMPIjobs::bstart($page) configure -state normal  
   	$::cmonMPIjobs::breset($page) configure -state normal
	$::cmonMPIjobs::bapply($page) configure -state normal
	$::cmonMPIjobs::bselect($page) configure -state normal
  	# cmonMPIjobs::setAction normal 
	array set ::cmonMPIjobs::state [ list $page 2 ]
}


## ******************************************************** 
##
## Name: cmonMPIjobs::state3
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::state3 { page } {
    cmonMPIjobs::state1 $page
	array set ::cmonMPIjobs::state [ list $page 3 ]
}


## ******************************************************** 
##
## Name: cmonMPIjobs::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMPIjobs::reset { page } {
    catch { destroy $::cmonMPIjobs::varFrame($page) }

}
