## ********************************************************
##
## Name: cmonClient.rsc
##
## Description:
## client resource file.
##
## Usage:
##
## Comments:
## Rules to support editing resource via cmonClient
##
## 1. A resource must be global e.g. ::xxx
##
## 2. Use upper case for name if possible.
##
## 3. Divide into NON-VIEWABLE and MODIFIABLE sections in
##   resource file. Non-viewable section precedes modifiable.
##
## 4. A non-viewable resource is defined by any valid tcl statement
## and is not displayed by cmonClient:
## It is marked by:
## 
## ;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT
##
## 5. Modifiable resources:
##  Begin with:
##
## ;## cmonClient MODIFIABLE RESOURCES 
##
## Each modifiable resources has:
## ::Name
## Desc (40 chars) keyword desc=xxxxxxxx
## mod=no if they are viewable but not modifiable via cmonClient
## Value - can be array or simple var
## 
## 6. Multiple line values must be connected
##  by coninuation char \
## 
## 7. An array variable can be defined in two ways
##  a. set arrname(index) <value>
##  b. array set arrname [ list index [ list $value ] list index [ list $value ] ...]
##
## 8. The resource name must be unique in the file:
## i.e. No two lines of the same resource in the file.
##
## 9. Resource with same desc should be right after each other
## 
## Reference http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
## 
## **********************************************************
;#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## currently using ldas distribution
;## APIs for the load page
;## default APIs for sites 
set ::API_LIST_w_cntlmon  { manager  cntlmon datacond diskcache eventmon frame ligolw metadata mpi }
set ::API_LIST_no_cntlmon { manager datacond diskcache eventmon frame ligolw metadata mpi }
set ::API_LIST_n_manager  { cntlmon datacond diskcache eventmon frame ligolw metadata mpi }

set ::API_LIST(no-server) $::API_LIST_w_cntlmon 

;## default APIs are updated from getting info from sites
array set ::API_LIST [ list ldas-dev  $::API_LIST_w_cntlmon ]
array set ::API_LIST [ list ldas-wa   $::API_LIST_w_cntlmon ]
array set ::API_LIST [ list ldas-la   $::API_LIST_w_cntlmon ]
array set ::API_LIST [ list ldas-test $::API_LIST_w_cntlmon ]
array set ::API_LIST [ list ldas-mit  $::API_LIST_w_cntlmon ]
array set ::API_LIST [ list ldas-cit  $::API_LIST_w_cntlmon ]

;## colors
set ::darkgreen #568c3a
set ::darkpurple #ae56b0
set ::darkorange #daa264
set ::darkred	orange
set ::darkbrown #b0581e
set ::magenta #e896e8
set ::textbg #a49ea6
set ::graphgreen #1a641e
set ::paleorange #e76c04
set ::brightgreen #188a00
set ::paleyellow  #dcac0c
set ::darkblue    #024260
set ::brightblue  #0ac8c8
set ::datepurple  #b6558e 
set ::palepink    #f8929e
set ::orange #e76c04

;## text widget color tags
set ::TEXTW_COLORS  [ list red blue green orange brown purple black \
$::darkgreen $::darkpurple $::darkorange $::darkred $::darkbrown \
magenta $::magenta $::textbg $::graphgreen $::brightgreen $::paleyellow \
$::darkblue $::brightgreen $::brightblue $::datepurple $::palepink $::paleorange ]

;## api colors
array set ::Color [ list manager $::paleyellow ]
array set ::Color [ list frame $::darkgreen ]
array set ::Color [ list datacond red ]
array set ::Color [ list eventmon orange ]
array set ::Color [ list metadata blue ]
array set ::Color [ list diskcache brown ]
array set ::Color [ list ligolw magenta ]
array set ::Color [ list mpi $::palepink ]
array set ::Color [ list queue purple ]
array set ::Color [ list total $::darkblue ]
array set ::Color [ list wrapper $::brightblue ]
array set ::Color [ list asstmgr $::brightgreen ]
array set ::Color [ list cntlmon purple ]

;## xterm or rxvt for ssh, set default editor vi
set ::XTERM [ auto_execok rxvt ]
set ::VI [ auto_execok vi ]

;## reminder to select server
set ::help_siteSelect "To get actual site information e.g. databases, beowulf nodes, machine names etc, please select a site to connect to."

;## lock timeout 30 minutes 
set ::LOCKTIMEOUT 30

;## check lock interval 1 min
set ::LOCK_INTERVAL 1

;## should match server
set ::CHALLENGE provPat6encio4

;## beowulf nodes (set by server)
array set ::beowulfNodes { no-server { none } }

;## directory tree top level
set ::ROOTDIR /ldas_outgoing

;## auto_path and GIFDIR set in cmonClient

;## gif images for html
set ::imagelist { ball_red.gif ball_yellow.gif ball_green.gif mail.gif telephone.gif ball_orange.gif ball_blue.gif ball_purple.gif }

;## gif for the intro page
set ::LDASgif $::GIFDIR/LDAS.gif

;## xbm for the intro page
set ::LDASxbm $::GIFDIR/LDAS.xbm

;## ldas dsnames if not connected 
array set ::dbnames { no-server { none } }

;## number of nodes on beowulf
array set ::NUMNODES { no-server {0} }

;## start and end times for last Eng Run E10 
set ::run_E10 [ list 750474013 751028413 ]

;## start and end times for S1
set ::run_S1 [ list 714121213 715590013 ]

;## start and end times for S2
set ::run_S2 [ list 729273613 734367613 ]

;## start and end times for S3
set ::run_S3 [ list 751651213 757699213 ]

;## start and end times for E12
set ::run_E12 [ list 791316013 791877613 ]

;## start and end times for S4
set ::run_S4 [ list 793130413 795679213 ]

;## location of official leapseconds table
set ::REMOTE_LEAPSECOND_DATA_URL ""

;## desc=enable gsi authenication in globus channel or disable (blanks)
set ::GSI_AUTH_ENABLED "-gsi_auth_enabled"

;## cmonClient MODIFIABLE RESOURCES 

;## desc=node login script on gateway ! mod=no
set ::NODELOGIN /ldas/bin/nodeLogin

;## desc=LDAS startup script
set ::RUNLDAS /ldas/bin/runLDAS

;## desc=debug flag to dump server data to file
set ::DEBUG 0

;## desc=LDAS machines default
array set ::LDASmachines { no-server { gateway beowulf } }

;## desc=nice font for lists
set ::LISTFONT -adobe-courier-bold-r-normal--12-120-75-75-m-70-iso8859-1

;## desc=smaller nice font for lists
set ::MEDIUMFONT -adobe-courier-bold-r-normal--11-100-75-75-m-60-iso8859-1

;## desc=smaller nice font for lists
set ::SMALLFONT -adobe-courier-bold-r-normal--10-100-75-75-m-60-iso8859-1

;## desc=italic font 
set ::ITALICFONT -adobe-helvetica-bold-o-normal--12-120-75-75-p-69-iso8859-1

;## desc=message font 
set ::MSGFONT -adobe-helvetica-bold-r-normal--14-100-100-100-p-82-iso8859-1

;## desc=max # of log lines
set ::NUMLOGS 1000

;## desc=tmp directory to hold files from server
set ::TMPDIR [pwd]/cmonClient-tmp

;## desc=ldas site addresses and ports
set ::siteport(ldas-dev)  [ list ldas-dev.ligo.caltech.edu 10000 ]
set ::siteport(ldas-wa)   [ list ldas.ligo-wa.caltech.edu 10000 ]
set ::siteport(ldas-la)   [ list ldas.ligo-la.caltech.edu 10000 ]
set ::siteport(ldas-test) [ list ldas-test.ligo.caltech.edu 10000 ]
set ::siteport(ldas-mit)  [ list ldas.mit.edu 10000 ]
set ::siteport(ldas-cit)  [ list ldas-cit.ligo.caltech.edu 10000 ]

;## desc=ldas read-only editor for remote Tree files
set ::EDITOR "$::XTERM -e $::VI -R "

;## desc=Resource configure targets
set ::rscTargets { manager diskcache frame metadata ligolw mpi datacond eventmon cntlmon } 

;## desc=http timeout 
set ::HTTP_TIMEOUT 30000

;## desc=status file url
set ::STATUS_HTML /ldas_outgoing/logs/APIstatus.html

;## desc=number of times to retry connection
set ::RECONNECT_TIMES 1

;## desc=dummy jobid 
set ::cmonJobId cmonJobId0

;## desc=location of leapseconds file for gpstime
set ::PUBDIR .

;## desc=supported debuggers
set ::DEBUGGERS [ list gdb ddd ]

;## desc=ldas debug script
set ::LDASDEBUG /ldas/bin/ldasDebug

;## desc=Node balance number of nodes per row 
set ::MAXNODESROW 5

;## desc=max number of nodes to add
set ::MAXNODESADD 500 

;## desc=Delays between beeps
set ::ALERTDELAY 5000

;## desc=scale factor for max in graph
set ::YMAXSCALE 1.050
set ::YMINSCALE 0.950

;## desc=wrap length for nodelist
set ::WRAPLENGTH 200

;## desc=cmds should be generated
set ::ldascmds [ list all abortJob activeJobSummary apiStatusSummary cacheGetFilenames cacheGetTimes \
concatFrameData conditionData createRDS dataPipeline dataStandAlone descMetaData getChannels \
getFrameCache getFrameData getFrameElements getMetaData ILwd2Lw Lw2ILwd putMetaData putStandAlone rmJobFiles \
stageData user-specified ]

;## desc=dsos for pipeline
set ::DSOs [ list all block cohere correlation exttrig fct fcthier inspiral knownpulsardemod load power ring \
sick simple slope stochastic test tfcluster trivial waveburst user-specified ]

;## desc=maxjobs for graph
set ::MAXJOBS 5000

;## desc=max number of graphs
set ::MAXGRAPHS 5

;## desc=min for least square
set ::MINFITPOINTS 500

;## desc=job reject reasons
set ::rejectReasons [ list all ambiguous_option blocked_user blocked_dso duplicate_option hourly_rate invalid_command \
 invalid_option queuesize_exceeded submission_rate ]

;## desc=default histogram bin size
set ::HIST_BINSIZE 50

;## desc=graph oval size 1 or 2
set ::POINTSZ 2

;## desc=xterm path
set ::XTERM xterm

;## desc=delay to ping server
set ::PINGDELAY 900000

;## desc=max backwards time for logCache is 6 mos.
set ::MAX_LOGCACHE_START 15638400 

;## desc=print cmd
set ::PRINTCMD lpr

;## desc=format string for time graph Y tick
set ::YTICK_FMTSTR "%10.10s"

;## desc=number of nodes to monitor per page
set ::NODES_MONiTOR_PAGE 40

;## desc=MPI host
set ::MPI_API_HOST beowulf

;## desc=Log pages
set ::NUM_LOG_PAGES 3

;## desc=default database used by DBview
set ::DATABASE_NAME ldas_tst

;## desc=MPI user pattern
set ::MPI_USER search

;## desc=frame types 
set ::FRAME_TYPES { RDS_R_L3 RDS_R_L2 RDS_R_L1 R T M user-specified } 

;## desc=frame sites
set ::FRAME_SITES { H L HL G user-specified }

;## desc=helptext for frame types
array set ::FRAME_TYPES_HLP [ list RDS_R_L3 "RDS Level 3" RDS_R_L2 "RDS Level 2" \
RDS_R_L1 "RDS Level 1" R "Raw" T "second-trend" M "minute-trend" ]

;## desc=helptext for frame sites
array set ::FRAME_SITES_HLP [ list H "LHO" L "LLO" HL "LHO and LLO combined" G "GEO" ]

;## desc=print orientation landscape or portrait
set ::PRINT_ORIENTATION portrait

;## desc=min,max for graph bin size for DBview
set ::BIN_RANGE(cmonDBview) [ list 0.001 5000 ]

;## desc=min,max for graph bin size for Nodes histogram
set ::BIN_RANGE(cmonNodesStat) [ list 1 1000 ]

;## desc=min,max for graph bin size for Times histogram
set ::BIN_RANGE(cmonAPItimes) [ list 0.001 2000 ]

;## desc=max number of nodes displayable in tree
set ::MAX_TREE_NODES 100000

;## desc=max size of status message
set ::MAX_MSG_LEN 100000

;## desc=max number of graph points
#set ::MAX_GRAPH_POINTS 100000
set ::MAX_GRAPH_POINTS 5000

;## desc=version below which does not support add resource
set ::RESOURCE_VERSION 1.5.70

;## desc=max number of bak files for resource changes
set ::RESOURCE_BAK_LEVEL 5

;## desc=max number user commands test
set ::MAXNUMTESTS 50

;## desc=tcl globus lib directory
set ::TCLGLOBUS_DIR /ldcg/lib/64

;## desc=tcl globus port in cntlmonAPI
set ::TCLGLOBUS_PORT 10032

;## desc=timeout (secs) for tclglobus socket
set ::TCLGLOBUS_SOCKET_TIMEOUT 5

;## desc=timeout (secs) for start LDAS key (10 min)
set ::MAX_KEY_SECS 600

;## desc=globus lib directory
set ::GLOBUS_LOCATION /ldcg/gt4-64bit

;## desc=certificates for ldas service cert
set ::X509_CERT_DIR /ldas_outgoing/grid-security/certificates
