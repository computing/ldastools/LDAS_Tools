## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles LDAS log filtering 
##
## cmonLoadSummary Version 1.0
##
## ******************************************************** 

package provide cmonLoadSummary 1.0

namespace eval cmonLoadSummary {
    set notebook ""
    set pages { cmonLoadSummary }
	set tab(cmonLoadSummary) "Load Summary "
	set title(cmonLoadSummary) "Beowulf cluster Load Summary" 
	set xlabel(cmonLoadSummary) "GPS Time"
	set ylabel(cmonLoadSummary) "Job Load ( #Nodes x wrapper runtime ) node-seconds"
	set graph loadsummary
	set wTop .tmpLoadSummary
    set color brown
}

## ******************************************************** 
##
## Name: cmonLoadSummary::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoadSummary::create { notebook } {

    set ::cmonLoadSummary::notebook $notebook
    foreach page $::cmonLoadSummary::pages {
        $notebook insert end cmonLoadSummary -text $::cmonLoadSummary::tab($page)  \
        -createcmd "cmonLoadSummary::createPages $notebook $page $page" \
        -raisecmd "cmonLoadSummary::pageRaise $page"
    }
}

## ******************************************************** 
##
## Name: cmonLoadSummary::pageRaise
##
## Description:
## generic plus backward compatibility check 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## will be removed by next version 

proc cmonLoadSummary::pageRaise { page } {

    cmonCommon::pageRaise cmonLoadSummary $page
    cmonCommon::setLastTime cmonLoadSummary $page
    cmonCommon::updateDBinfo cmonLoadSummary $page


}

## ******************************************************** 
##
## Name: cmonLoadSummary::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoadSummary::createPages { notebook toppage page } {

	if	{ [ catch { 
        set frame  [ $notebook getframe $toppage ]		
		specific $frame $page 
        array set ::cmonLoadSummary::state [ list $page 0 ]	
        set ::cmonLoadSummary::prevsite($page) $::cmonClient::var(site)
	} err ] } {
		puts $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonLoadSummary::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoadSummary::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]

	set labfent [LabelFrame $parent.fent -side top -anchor w \
            -relief sunken -borderwidth 4 -font $::ITALICFONT -text \
			"Specify filters (in regexp) for $::cmonLoadSummary::tab($page)" ]
    set subf [ $labfent getframe ]
    set ::cmonLoadSummary::topfw($page) $subf
	set subf1 [ frame $subf.subf1 -relief sunken -borderwidth 2 ]
	set fuserid [ frame $subf1.fuserid -relief groove -borderwidth 2 ]
	
	set userid_ent   [ LabelEntry $fuserid.euserid \
			-label "By User Id: " -labelwidth 15 -labelanchor w \
    		-textvariable ::cmonLoadSummary::var($page,userid) -editable 1 -width 15 \
        	-helptext "Formats: <user1> <user2> <user3> ..., or leave blank for users"] 
	
    set fdso [ frame $subf1.fdso ]
	set lab [ label $fdso.lab -text "By DSO: " -width 15 -justify left -anchor w ]
    pack $fdso.lab -side left -anchor w 
    cmonCommon::ldasDSOs $fdso.dso $name $page normal
	pack $fdso -side top -anchor w -fill both -expand 1
    trace variable ::DSOs w "cmonCommon::ldasDSOs $fdso.dso $name $page normal"
	
    set but [ button $subf1.view -text "View Selection" \
		-command "cmonLoadSummary::showCmdsDsos $name $page" ]
    pack $but -side top -anchor w -padx 5
	pack $subf1.fuserid.euserid -side top -anchor w -fill x -expand 1
	pack $subf1.fuserid -side top -anchor w -expand 1 -fill both
	set ::cmonLoadSummary::var($page,userid) *

	set subf2 [ frame $subf.subf2 -relief ridge -borderwidth 2 ]    
	set f2 [ frame $subf2.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]
	
	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $f2 $name $page ] { break }
	
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x -expand 1
	set ::cmonLoadSummary::var($page,type) userid
	pack $subf2.ftime -side top -anchor w -expand 1 -fill both
	
	if	{ [ info exist subf1 ] } {
		pack $subf1 $subf2 -side left -fill both -expand 1
	} else {
		pack $subf2 -side left -fill both -expand 1
	}
	
	;## place the cmd status on the bottom
	set labref [LabelFrame $parent.refresh -text "Command Status" -side top -anchor w \
                   -relief sunken -borderwidth 4]
                   
    set subf3  [ $labref getframe ]
	
	set statusw [ ::createStatus  $subf3 ]
	array set ::cmonLoadSummary::statusw [ list $page $statusw ]
	
	set f3 [ frame $subf2.fstart ]
    set  but1 [ Button $f3.start -text "SUBMIT" -width 5 -helptext "submit one time request to server" \
        -command  "::cmonLoadSummary::sendRequest $page" ]

	set ::cmonLoadSummary::bstart($page) $but1  
	
    pack $but1 -side left -padx 100 -pady 5  	
    pack $f3 -side top -expand 1 -fill x	
	pack $labfent -side top -fill x 
    pack $labref -side top -padx 4 -fill both -expand 1
    set ::${name}::var($page,ldascmds) dataPipeline
}

## ******************************************************** 
##
## Name: cmonLoadSummary::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
	
proc cmonLoadSummary::sendRequest { page } {

	set name [ namespace tail [ namespace current ] ]

	if	{ [ catch {
		if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		  	error "Please select an LDAS site"
		}
        ;## user filter 
        set userid [ cmonCommon::setUserIdPattern ::cmonLoadSummary::var($page,userid) ]

        ;## dso filter
        set dsos [ set ::${name}::var($page,dso) ]
        set dsopat ""
	    if	{ ! [ string match all* $dsos ] } {
            if  { [ string length $dsos ] } {
			    foreach dso $dsos {
				    append dsopat ".+${dso}.+\|"
			    }
            }
            set ::${name}::cmdpat($page) $dsos
		} else {
            ;## all dsos
            set cmdpat .+
            set ::${name}::cmdpat($page) all
	    }
        set dsopat [ string trim $dsopat \| ]
		;## need to fill in gap to make filter line up
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
		set servercmd "cntlmon::beowulfLoadSummary $starttime-$endtime $userid [ string trim $dsopat \| ]"
        set client $::cmonClient::client
        set cmdId "new"
		set freq 0
    	set repeat 0
       	set cmd "cmonLoadSummary::showReply $page\n$repeat\n$freq\n\$client:$cmdId\n$servercmd"      
		#puts "cmd=$cmd"
		cmonLoadSummary::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonLoadSummary::state0 $page 
		appendStatus $::cmonLoadSummary::statusw($page) $err 0 red 1
	}
}

## ******************************************************** 
##
## Name: cmonLoadSummary::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonLoadSummary::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"
	# puts "html size [ string length $html ]"
    set ::cmonLoadSummary::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    	
	switch $rc {
	0 {	if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
			cmonLoadSummary::dispGraph $page $html 
			appendStatus $::cmonLoadSummary::statusw($page) updated 0 blue
        } else {
            appendStatus $::cmonLoadSummary::statusw($page) \
                "No data" 
        }
		if	{ ! [ string length $afterid ] } {
                cmonLoadSummary::state0 $page 
            } else {
                cmonLoadSummary::state2 $page 
        }
	  }
	3 {
		appendStatus $::cmonLoadSummary::statusw($page) $html
		;## may need to cancel request	
		cmonLoadSummary::state0 $page 	
	  }
	}
	} err ] } {
		appendStatus $::cmonLoadSummary::statusw($page) $err
		cmonLoadSummary::state0 $page 
	}
}

##*******************************************************
##
## Name: cmonLoadSummary::loadSummaryStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoadSummary::loadSummaryStats { page statsdata parent } {

	set site $::cmonClient::var(site)
	if	{ ! [ info exist ::beowulfNodes($site)  ] } {
    	set ack [ tk_messageBox -type ok -default ok -message \
        	"$site may not have beowulf nodes to support this function." \
            -icon info ]
        return
    }

	if	{ [ catch {
        set ylist [ list ]
        set ymin 0 
        set ymax 5
        set numNodes [ llength $::beowulfNodes($site) ]
        
        foreach { range total data } $statsdata { break }
                
		cmonLoadSummary::clearGraphLabels
			
		set ::${parent}(title) $::cmonLoadSummary::title($page)
		set ::${parent}(y1label) $::cmonLoadSummary::ylabel($page)
		set ::${parent}(submit) "cmonLoadSummary::sendRequest $page"
        
		;## set print info
		set ::${parent}(ptitle) "Beowulf Load Summary @$site LDAS $::cmonClient::serverVersion"
		set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        
		set user $::cmonLoadSummary::var($page,userid)
		if	{ [ regexp {^[\*\s\t]+$} $user ] } {
			set user all
		}
		set cmds $::cmonLoadSummary::var($page,ldascmds)
		if	{ [ regexp {^[\*\s\t]+$} $cmds ] } {
			set cmds all
		}
		set userline "For user(s) $user "
              
        set cmds [ set ::cmonLoadSummary::cmdpat($page) ]
        set cmds [ join $cmds , ]

		lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
        lappend text "Selected Users:\t" [ list black fixed ]
        lappend text "$user" [ list brown bold ]
              
        lappend text "\nFor DSOs:\t" [ list black bold ]
		lappend text "$cmds\n" [ list brown bold ]
        lappend text "$total jobs\t" [ list brown bold ]
        
		;## set print info
		# set ::${parent}(ptitle) "Beowulf Load Summary @$site LDAS $::cmonClient::serverVersion"
		
		set ymin 0
		set ymax 0
        set newdata [ list ]
        set integrated 0.0
        set mean 0.0
        set efficiency 0.0
        set gpsDelta [ expr $gps_end - $gps_start ]
        
        foreach { gpstime nodes runtime } $data {
            set load [ expr $nodes * $runtime ]
            lappend newdata $gpstime $load 
            lappend ylist $load
            set integrated [ expr $load + $integrated ]
        }
        if  { [ llength $ylist ] } {
            set ylist [ lsort -real $ylist ]
            set ymin [ expr floor([ lindex $ylist 0 ]) ]
            set ymax [ expr ceil([ lindex $ylist end ]) ]
        }
        
		if	{ $ymax < 5 } {
			set ymax 5
		}   
        
        if  { $total } {
        ;## compute the stats: mean integrated value and efficiency
            set mean [ format "%.2f" [ expr $integrated / $total ] ]
            set efficiency [ format "%.2f" [ expr ( $integrated / ( $numNodes * $gpsDelta ) ) * 100 ] ] 
            set integrated [ format "%.2f" $integrated ]  
            lappend text "\nAverage per job: $mean, Job Sum: $integrated, Load: $efficiency %" [ list brown bold ]
            set meandata [ list $gps_start $mean $gps_end $mean ]
        }
        updateTextWin [ set ::${parent}(textw) ] $text 
		set bltdata [ list ]
        ;## append first to make blt draw this on top of points
        if  { [ info exist meandata ] } {
            lappend bltdata $meandata
		    lappend bltdata "average_per_job"
		    lappend bltdata none
		    lappend bltdata orange
		    lappend bltdata 3
        }
        
        foreach { newdata msg } [ cmonCommon::reduceDisplayData $newdata $::MAX_GRAPH_POINTS ] { break }
        if  { [ llength $msg ] } {
            appendStatus $::cmonLoadSummary::statusw($page) "Load Summary data: $msg" 0 brown
        }
		lappend bltdata $newdata
		lappend bltdata load_summary
		lappend bltdata circle
		lappend bltdata $::cmonLoadSummary::color
		lappend bltdata 0
	
		blt_graph::showGraph $bltdata $gps_start $gps_end $ymin $ymax $parent			
		raise $parent
		update idletasks
	} err ] } {
		appendStatus $::cmonLoadSummary::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonLoadSummary::dispGraph
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoadSummary::dispGraph { page html } {

	set site [ string trim $::cmonClient::var(site) ]
    
    if	{ ! [ info exist ::beowulfNodes($site)  ] } {
    	set ack [ tk_messageBox -type ok -default ok -message \
        	"$site may not have beowulf nodes to support this function." \
            -icon info ]
        return
    }

	set parent $::cmonLoadSummary::wTop${page}	
    if  { ! [ winfo exist $::cmonLoadSummary::wTop${page} ] } {
        toplevel $parent 
	    set statusw [ createTextWin $parent 6 ]  
        set ::${parent}(textw) $statusw
    }    
	wm deiconify $::cmonLoadSummary::wTop${page}
	wm title $::cmonLoadSummary::wTop${page} "$::cmonLoadSummary::tab($page)@$site"
    cmonLoadSummary::loadSummaryStats $page $html $parent
}


##******************************************************* 
##
## Name: cmonLoadSummary::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonLoadSummary::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonLoadSummary::topfw($page) normal
	$::cmonLoadSummary::bstart($page) configure -state normal -text "SUBMIT" \
		-command "${name}::sendRequest $page"
	array set ::cmonLoadSummary::state [ list $page 0 ]
}

##******************************************************* 
##
## Name: cmonLoadSummary::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonLoadSummary::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonLoadSummary::topfw($page) disabled	
    $::cmonLoadSummary::bstart($page) configure -state disabled 
	array set ::cmonLoadSummary::state [ list $page 1 ]
	
}

##******************************************************* 
##
## Name: cmonLoadSummary::clearGraphLabels
##
## Description:
## set label text to nulls 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonLoadSummary::clearGraphLabels {} {

	uplevel {

        regexp {(\d+)-(\d+)} $range -> gps_start gps_end

        set localstart [ gps2utc $gps_start 0 ]
        set localend   [ gps2utc $gps_end 0 ]
	}
}

## ******************************************************** 
##
## Name: cmonLoadSummary::showCmdsDsos
##
## Description:
## show the selected ldas cmds and DSOs
##
## Parameters:
##
## Comments:

proc cmonLoadSummary::showCmdsDsos { name page { showmsg 1 }} {

    set dsos [ set ::${name}::var($page,dso) ]
    regsub -all {\|} $dsos {, } dsos
	set text "DSOs selected:\n$dsos"
    if  { $showmsg } {
	    set ack [ tk_messageBox -type ok -default ok \
		    -message $text -icon info ]
    } else {
        regsub -all {\n} $text " " text
        return $text
    }
}

