## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script displays the load and cpu usage information:
## on all the Bwatch nodes.
## 
## ******************************************************** 

package provide cmonBeowulfUsers 1.0

namespace eval cmonBeowulfUsers {
    set notebook ""
    set pages { MPI-Queue-Users Verify-Users }
	set title(MPI-Queue-Users) "List of users in MPI queue"
	set title(Verify-Users) "List of users running wrapper jobs"
	set delay { 30 60 120 } 
	set repeat { 0 5 100 10000 } 
    set freq { 30 60 900 3600 } 
	set freqUnit 1000
	set freqUnits secs
	set fmtstr "%-10s  %10s  %10s  %40s  %-40s"
	
	set userlen 10
	set jobidlen 15
	set numNodeslen 10
	set nodelistlen  40
	set objlen 40
	array set servercmd [ list MPI-Queue-Users getUserNodes ]
	array set servercmd [ list Verify-Users getSearchNodes ]
	array set cancelcmd [ list MPI-Queue-Users "cmonBeowulfUsers::cancelRequest \$page"  ]
	array set cancelcmd [ list Verify-Users "cmonBeowulfUsers::sendRequest \$page cancelNsearch" ]
}

;## heading
;## node time #jobs #users load1 load5 load10 
;## cpuidle memtot memfree buffers cached 
;## swaptot swapfree

## ******************************************************** 
##
## Name: cmonBeowulfUsers::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBeowulfUsers::create { notebook } {
    
    foreach page $::cmonBeowulfUsers::pages {    
        regsub -all {\-} $page { } pagetext
        $notebook insert end $page -text $pagetext \
        -createcmd "cmonBeowulfUsers::createPages $notebook $page" \
        -raisecmd [ list cmonCommon::pageRaise cmonBeowulfUsers $page $pagetext  ]
    }
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::create 
##
## Description:
## creates Bwatch page
##
## Parameters:
## notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBeowulfUsers::createPages { notebook page } {

	set frame [ $notebook getframe $page ]

    set topf  [frame $frame.topf]	
	set ::cmonBeowulfUsers::notebook $notebook	
    specific $topf $page
	set ::cmonBeowulfUsers::topfw($page) $topf
	pack $topf -fill x -pady 2
	
	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	regsub -all {\-} $page { } pagetext
    
    ;## create a status to display errors and status
	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonBeowulfUsers::statusw [ list $page $statusw ]
	pack $titf2 -pady 2 -padx 4 -fill both -expand yes
	
	;## create a scrollable window to hold variable entries
	set titf3 [TitleFrame $pane3.titf3 -text $pagetext -font $::ITALICFONT ]
	set tframe [ $titf3 getframe ]
	set sw [ ScrolledWindow $tframe.sw -relief sunken -borderwidth 2 ]
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }	
	set lbhdr [listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single -relief ridge \
        -font $::LISTFONT ]
	$lbhdr insert end [ format $::cmonBeowulfUsers::fmtstr "  User  " "  JobId  " \
				" #nodes " "  nodes used  " "  Shared Object  " ] 
	$sw setwidget $lbhdr
	set ::cmonCores::lbhdr($page) $lbhdr
		
	set sf [ScrollableFrame $sw.f -height 1000 -areaheight 0 ]
		
	$sw setwidget $sf
	;## make the two lists scroll together
			
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $sf ] ]
		
	set subf [$sf getframe]
	set subfh [ frame $subf.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonBeowulfUsers::varFrame($page) $subfh
	set ::cmonBeowulfUsers::varFrameParent($page) [ winfo parent $subfh ]
	pack $sw -fill both -expand yes		
	pack $titf3 -side top -pady 2 -padx 2 -fill both -expand yes
    set ::cmonBeowulfUsers::titf3($page) $titf3 
    
	pack $pw1 -side top -fill both -expand 1
    array set ::cmonBeowulfUsers::state [ list $page 0 ]
    set ::cmonBeowulfUsers::prevsite($page) none
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::specific
##
## Description:
## creates specific action area for the page types
##
## Parameters:
## page
## parent widget  
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBeowulfUsers::specific { parent page } {

	set labfent [LabelFrame $parent.fent -text $::cmonBeowulfUsers::title($page) \
		-side top -anchor w \
        -relief sunken -borderwidth 4 ]
    set subf1 [ $labfent getframe ]
	set cmd $::cmonBeowulfUsers::servercmd($page) 
	Button $subf1.start -text SUBMIT -width 10 \
        -command  "::cmonBeowulfUsers::sendRequest $page $cmd" \
		-helptext "get a list of users and their assigned nodes"
	set ::cmonBeowulfUsers::bstart($page) $subf1.start	
	pack $subf1.start -side left -padx 10 -pady 10
	set labrefresh [LabelFrame $parent.refresh -text "Repeat this request ( set repeat > 0 )" \
        -side top -anchor w -relief sunken -borderwidth 4]
	set subf2 [ $labrefresh getframe ]
	set name [ namespace tail [ namespace current ] ]
	createRepeatFreq $subf2 $name $page
	pack $labfent -side left -padx 4 -fill both -expand 1
	pack $labrefresh -side right -padx 4 -fill both -expand 1	  
 
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::nodeList
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the list of nodes and their usage
## 
## Comments:

proc cmonBeowulfUsers::nodeList { parent page } {

    set sw  [ScrolledWindow $parent.sw]	
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }
	set lbhdr [listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single \
        -font $::LISTFONT ]
	$lbhdr insert end [ format $::cmonBeowulfUsers::fmtstr \
		User JobId #Nodes Nodes "Shared Object" ] 
    set lb [listbox $sw.lb -height 100  \
        -highlightthickness 1 -selectmode single \
        -font $::LISTFONT -selectbackground orange ]
    set ::cmonBeowulfUsers::lbox($page) $lb
	$sw setwidget $lbhdr
    $sw setwidget $lb
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $lb ] ]
    pack $sw -side left -fill both -expand yes
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::sendRequest 
##
## Description:
## send request 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBeowulfUsers::sendRequest { page request } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonBeowulfUsers::statusw($page) "Please select an LDAS site" 
		return 
	}

    set cmdId new
    set repeat $::cmonBeowulfUsers::var($page,repeat)
    set freq [ expr $::cmonBeowulfUsers::var($page,freq)*$::cmonBeowulfUsers::freqUnit ]    
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    
	switch $request {
		getUserNodes { 
			set cmd "cntlmon::getUserNodes" 
			set cmd "cmonBeowulfUsers::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId\n$cmd" 	
			cmonBeowulfUsers::state1 $page 
			}
		getSearchNodes { 
			set cmd "cntlmon::startMonitorPipe Nsearch $repeat $freq" 
			set cmd "cmonBeowulfUsers::showReply $page\n0\n0\n\$client:$cmdId\n$cmd" 	
			cmonBeowulfUsers::state1 $page 
			}
		cancelNsearch { 
			set cmd "cntlmon::cancelMonitorPipe Nsearch" 
			set cmd "cmonBeowulfUsers::showReply $page\n0\n0\n\
        	$client:$cmdId\n$cmd" 
			cmonBeowulfUsers::state3 $page 
			}
	}

    if  { [ catch {
		#puts "cmd $cmd"
		sendCmd $cmd
		appendStatus $::cmonBeowulfUsers::statusw($page) "Working on it, please wait ..." 0 blue 		
	} err ] } {
		 cmonBeowulfUsers::state0 $page 
         appendStatus $::cmonBeowulfUsers::statusw($page) $err 
	}
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::showReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBeowulfUsers::showReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
	set cmonClient::client $client
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    #puts "in showReply, page=$page,rc=$rc, client=$client,cmdId=$cmdId, afterid=$afterid"
	#puts "html=$html"
	# puts "state $::cmonBeowulfUsers::state($page)"
	switch $rc {
	0 { set ::cmonBeowulfUsers::cmdId($page) $cmdId
		;## if cancel request, show status of cancellation
		if	{ $::cmonBeowulfUsers::state($page) == 3 } {
			appendStatus $::cmonBeowulfUsers::statusw($page) $html 0 blue
			cmonBeowulfUsers::state0 $page 
		} else {
			;## getting user info
			set numUsers 0
			if	{ [ regexp {^search|none} $html ] } {
				set numUsers [ cmonBeowulfUsers::dispNode $page $html ]	
				if	{ $numUsers } {
					appendStatus $::cmonBeowulfUsers::statusw($page) "Found $numUsers users." 0 blue	
				} 
			} elseif { [ regexp -nocase {No.+found} $html ] } { 
				catch { destroy $::cmonBeowulfUsers::varFrame($page) }
				appendStatus $::cmonBeowulfUsers::statusw($page) $html 0 blue
			} 
			
			switch $page {
			MPI-Queue-Users { 
					if 	{ ! [ string length $afterid ] } {
						cmonBeowulfUsers::state0 $page
					}	else {
						cmonBeowulfUsers::state2 $page
					}
				}
			Verify-Users { 
					if 	{ [ regexp -nocase {exit} $html ] } {
						cmonBeowulfUsers::state0 $page
					} else {
						cmonBeowulfUsers::state2 $page
					}
				}
			default {} 
			}
		}
	  }
    1 { cmonBeowulfUsers::state0 $page
		appendStatus $::cmonBeowulfUsers::statusw($page) $html 0 blue
      }
	3 { cmonBeowulfUsers::state0 $page
        appendStatus $::cmonBeowulfUsers::statusw($page) $html    
	  }
	}		
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::dispNode
##
## Description:
## insert the newly arrived node data into the listbox
##
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:

proc cmonBeowulfUsers::dispNode { page html } {

	if	{ [ catch {
	;## create a header 
	set parent $::cmonBeowulfUsers::varFrameParent($page) 
	;## must destroy widget in parseRsc or globalVar array cannot be unset
	catch { destroy $::cmonBeowulfUsers::varFrame($page) }
	
	set subfh [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonBeowulfUsers::varFrame($page) $subfh
	set ::cmonBeowulfUsers::varFrameParent($page) [ winfo parent $subfh ]
	
	set i 0
	foreach { user jobid numNodes nodelist obj } $html {

		set subf1 [ frame $subfh.f$i -relief sunken ]
		Label $subf1.user$i -width $::cmonBeowulfUsers::userlen \
			-text $user -justify left \
			-anchor w -font $::LISTFONT	-relief ridge -bg white
			
		Label $subf1.jobid$i -width  $::cmonBeowulfUsers::jobidlen -justify right \
			-text $jobid -fg brown \
			-anchor w -font $::LISTFONT	-relief ridge -bg gray
			
		Label $subf1.numNodes$i -width  $::cmonBeowulfUsers::numNodeslen -justify right \
			-text $numNodes -fg black \
			-anchor w -font $::LISTFONT	-relief ridge -bg white
		
		Label $subf1.nodelist$i -width $::cmonBeowulfUsers::nodelistlen -justify left \
			-text $nodelist -fg brown -wraplength $::WRAPLENGTH \
			-anchor w -font $::LISTFONT	-relief ridge -bg gray 
		
		Label $subf1.obj$i -width $::cmonBeowulfUsers::objlen -justify left \
			-text $obj -fg black -justify left \
			-anchor w -font $::LISTFONT	-relief ridge -bg white		
			
		pack $subf1.user$i $subf1.jobid$i $subf1.numNodes$i $subf1.nodelist$i $subf1.obj$i \
			-side left  -fill both -anchor w
		pack $subf1 -side top -fill x -expand 1
		incr i 1
	}
	pack $subfh -side top -fill both -expand 1
	
	} err ] } {
		puts "[myName],$page,$err"
		appendStatus $::cmonBeowulfUsers::statusw($page) "[myName] $err"
	}
	return $i
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::cancelRequest 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBeowulfUsers::cancelRequest { page } {
	
	set cmdId $::cmonBeowulfUsers::cmdId($page)
    set client $::cmonClient::client
    set repeat 0
    set freq 0
	if	{ [ catch {
		set cmd "cmonBeowulfUsers::showReply $page\n0\n0\n\
        $client:$cmdId\n${client}::cancel $cmdId" 
		#puts "cmd=$cmd"
		cmonBeowulfUsers::state3 $page 
		sendCmd $cmd		 
	} err ] } {
		cmonBeowulfUsers::state0 $page 
		appendStatus $::cmonBeowulfUsers::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::state0
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBeowulfUsers::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonBeowulfUsers::topfw($page) normal
	$::cmonBeowulfUsers::bstart($page) configure -state normal -text \
   		SUBMIT -command "cmonBeowulfUsers::sendRequest $page $::cmonBeowulfUsers::servercmd($page)" \
      	-state normal 
	updateRepeat $name $page normal
	array set ::cmonBeowulfUsers::state [ list $page 0 ]
}


## ******************************************************** 
##
## Name: cmonBeowulfUsers::state1
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBeowulfUsers::state1 { page } {
    set name [ namespace tail [ namespace current ] ]
    updateRepeat $name $page disabled
	$::cmonBeowulfUsers::bstart($page) configure -state disabled
	array set ::cmonBeowulfUsers::state [ list $page 1 ]
}

## ******************************************************** 
##
## Name: cmonBeowulfUsers::state2
##
## Description:
## cancel monitor
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBeowulfUsers::state2 { page } {
	if	{ $::cmonBeowulfUsers::state($page) != 2 } {
		set cmd [ subst $::cmonBeowulfUsers::cancelcmd($page) ]
    	$::cmonBeowulfUsers::bstart($page) configure -text "CANCEL" \
        -state normal -command $cmd
		array set ::cmonBeowulfUsers::state [ list $page 2 ]
	}
}

##******************************************************** 
##
## Name: cmonBeowulfUsers::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBeowulfUsers::state3 { page } {
    $::cmonBeowulfUsers::bstart($page) configure -state disabled
	array set ::cmonBeowulfUsers::state [ list $page 3 ]
}

##******************************************************** 
##
## Name: cmonBeowulfUsers::reset 
##
## Description:
## mark pages to reset
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments

proc cmonBeowulfUsers::reset { page } {

    catch { destroy $::cmonBeowulfUsers::varFrame($page) }    

}
