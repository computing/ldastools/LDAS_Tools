## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script blocking of dso 
##
## cmonBlock Version 1.0
##
## ******************************************************** 

package provide cmonBlock 1.0

namespace eval cmonBlock {
    set pages { cmonBlock }
    set tab(blockDSO) "block DSO"
    set dsolen 30
}

## ******************************************************** 
##
## Name: cmonBlock::create
##
## Description:
## creates Main page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook tab
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBlock::create { notebook } {
    set ::cmonBlock::prevsite(cmonBlock) none
    $notebook insert end cmonBlock -text "Block DSOs" \
        -createcmd "cmonBlock::createPages $notebook" \
        -raisecmd [ list cmonCommon::pageRaise cmonBlock cmonBlock "Dynamic Shared Objects" ]
}

## ******************************************************** 
##
## Name: cmonBlock::createPages 
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBlock::createPages { notebook } {

    set page  [ lindex $::cmonBlock::pages 0 ]
    set frame [ $notebook getframe cmonBlock ]
	set topf  [frame $frame.topf]
    specific $topf $page	
	set ::cmonBlock::topfw($page) $topf
	pack $topf -fill x -pady 1

	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }

	set titf2 [TitleFrame $pane2.titf2 -text "Command Status" -font $::ITALICFONT]
	;## create a status to display errors and status
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonBlock::statusw [ list $page $statusw ]
	pack $titf2 -pady 1 -padx 1 -fill both -expand yes

	;## create a scrollable window to hold variable entries
	set titf3 [TitleFrame $pane3.titf2 -text "Dynamic Shared Objects" -font $::ITALICFONT]
	set ::cmonBlock::titf3($page) $titf3
    cmonCommon::pageUpdate cmonBlock $page $page
    
	set frame3 [ $titf3 getframe ]
	set sw [ ScrolledWindow $frame3.sw -relief sunken -borderwidth 2 ]
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }	
	set lbhdr [listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single -relief ridge \
        -font $::LISTFONT ]
	$lbhdr insert end [ format "%20.20s %20.20s %20.20s %20.20s"  "DSO" "DSO" "DSO" "DSO" ]

	$sw setwidget $lbhdr
	set ::cmonBlock::lbhdr($page) $lbhdr
		
	set sf [ScrollableFrame $sw.f -height 1000 -areaheight 0 ]
		
	$sw setwidget $sf
	;## make the two lists scroll together
			
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $sf ] ]
		
	set subf [$sf getframe]
	set subfh [ frame $subf.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonBlock::varFrame($page) $subfh
	set ::cmonBlock::varFrameParent($page) [ winfo parent $subfh ]
	pack $sw -fill both -expand yes	
	pack $titf3 -pady 1 -padx 1 -fill both -expand yes
	pack $pw1 -side top -fill both -expand 1
    array set ::cmonBlock::state [ list $page 0 ]
    
    ;## build a new dso list with full names
    set ::cmonBlock::DSOs($page) [ list ]
    foreach dso $::DSOs {
        lappend ::cmonBlock::DSOs($page) libldas${dso}.so
    }
    set ::cmonBlock::prevsite($page) $::cmonClient::var(site)
    return $frame
}

## ******************************************************** 
##
## Name: cmonBlock::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBlock::specific { parent page } {

	set labref [LabelFrame $parent.labref -text "" -side top -anchor w \
                   -relief sunken -borderwidth 4 ]
    set subf2  [$labref getframe] 
    set ::cmonBlock::var($page,admincmd) blockDso
    
    set f1 [ frame $subf2.f1 -relief ridge -borderwidth 3 ]
	label $f1.lbl -text "DSO as specified in LDAS command -dynlib option "
	set dso_ent   [LabelEntry $f1.dsoname -label "DSO: " -labelwidth 15 -labelanchor w \
         -textvariable ::cmonBlock::var($page,dso) -editable 1 -width 30 -labeljustify right \
         -helptext "DSO specified by -dynlib option"]
    pack $f1.lbl -side top
    pack $dso_ent -side top -fill x 
    pack $f1 -side top -fill x -expand 1
    set  f2 [ frame $subf2.faction -relief flat ]
	set but2 [ Button $f2.refresh -text "Refresh DSO list" -helptext "get current manger's queue" \
        -command  "cmonBlock::sendRequest $page getBlockedDSOs" ]
    set but3 [ Button $f2.blockdso -text "Block DSO" -bg red -fg white -relief raised \
        -command "cmonBlock::sendRequest $page blockDso" ]
                        
	set ::cmonBlock::bstart2($page) $but2
	set ::cmonBlock::bstart3($page) $but3
    
    pack $but2 $but3 -side left -padx 2 -pady 2 
	pack $f2 -side bottom -fill x -expand 1
	pack $labref -side left -fill both -expand 1
}

## ******************************************************** 
##
## Name: cmonBlock::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBlock::sendRequest { page type } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonBlock::statusw($page) "Please select an LDAS site"
		return
	}
	    
	if	{ [ catch {
    	foreach { login passwd } [ validateLogin 1 ] { break }
	} err ] } {
		appendStatus $::cmonBlock::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
    set repeat 0
    set freq 0
    set client $::cmonClient::client
    
	set name [ namespace tail [ namespace current ] ]
    if  { [ catch {
        if	{ [ string match getBlockedDSOs $type ] } {
			set servercmd "cntlmon::getBlockedDSOs"
		} else {      
            set dso $::cmonBlock::var($page,dso)
            if  { [ regexp {^[\s\t]*$} $dso ] } {
                error "Please specify a DSO"
            }
            set bg [ $::cmonBlock::bstart3($page) cget -bg ]
            if  { [ string match green $bg ] } {
                set admincmd unblockDso
            } else {
                    set admincmd blockDso 
            }
            set dso $::cmonBlock::var($page,dso) 
            set servercmd "cntlmon::useradmin $login $admincmd $dso"
        }
        set client $::cmonClient::client
        set cmdId "new"
		set freq 0
    	set repeat 0
       	set cmd "cmonBlock::showReply $page\n$repeat\n$freq\n\$client:$cmdId:$login:$passwd\n$servercmd"  
		# puts "cmd=$cmd"    
		cmonBlock::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonBlock::state0 $page 
		appendStatus $::cmonBlock::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonBlock::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonBlock::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"
	# puts "html size [ string length $html ]"
    set ::cmonBlock::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    	

	switch $rc {
	0 { set status ""; set data [ list ]
        regexp {^([^\n]+)\n(.+)} $html -> status data 
		if	{ [ string length $status ] } {
			appendStatus $::cmonBlock::statusw($page) $status 0 blue
		}
        cmonBlock::dispDSOs $page $data
      }		
	3 {
		appendStatus $::cmonBlock::statusw($page) $html
	  }
	}
	} err ] } {
		appendStatus $::cmonBlock::statusw($page) $err
	}
    cmonBlock::state0 $page
}

## ******************************************************** 
##
## Name: cmonBlock::setDso
##
## Description:
## set entries field to selected user
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## 

proc cmonBlock::setDSO { page dso blocked } {

    if  { ! [ string match user_specified $dso ] } {
	    set ::cmonBlock::var($page,dso) $dso
    } else {
        set ::cmonBlock::var($page,dso) ""
    }
    if  { $blocked } {
        $::cmonBlock::bstart3($page) configure -text "Unblock DSO" -bg green -fg brown
    } else {
        $::cmonBlock::bstart3($page) configure -text "Block DSO" -bg red -fg white
    }
}

## ******************************************************** 
##
## Name: cmonBlock::dispDSOs
##
## Description:
## display list of DSOs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## also display vars that cannot be changed

proc cmonBlock::dispDSOs { page data } {

	if	{ [ catch {
		set i 0
		;## create a header 
		set parent $::cmonBlock::varFrameParent($page) 
		;## must destroy widget in parseRsc or globalVar array cannot be unset
		catch { destroy $::cmonBlock::varFrame($page) }
	
		set subfh [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
		set ::cmonBlock::varFrame($page) $subfh
		set ::cmonBlock::varFrameParent($page) [ winfo parent $subfh ]
	
		set i 0
		if	{ [ llength $data ] == 1 } {
			set data [ lindex $data 0 ]
		} 
		set dso ""
        set blocked 0
        ;## check if blocked dso is a standard one
        set newdsolist [ list ]
   
        set ::cmonBlock::DSOs($page) [ list ]
        foreach dso $::DSOs {
            lappend ::cmonBlock::DSOs($page) libldas${dso}.so
        }
        set dsolist [ concat $::cmonBlock::DSOs($page) $data ]
        set ::cmonBlock::DSOs($page) [ lsort -unique $dsolist ]
        lappend ::cmonBlock::DSOs($page) "user_specified"
        
		foreach dso $::cmonBlock::DSOs($page) {
			if	{ ! [ expr $i % 4 ] } {
				set subf1 [ frame $subfh.f$i -relief sunken ]
				pack $subf1 -side top -fill x -expand 1
				set sep ""
			} else {
				set sep [ Separator $subf1.sep$i -bg blue -orient vertical -relief ridge ]
				pack $sep -side left  -fill both -anchor w -padx 2 
			}
            if  { [ lsearch -exact $data $dso ] > -1 } {
                set blocked 1
            } else {
                set blocked 0
            }
			set but [ button $subf1.dso$i -text $dso \
				-command  "cmonBlock::setDSO $page $dso $blocked" -justify left \
				-font $::LISTFONT -bg PapayaWhip -width $::cmonBlock::dsolen ]
					
			pack $but -side left  -fill x -anchor w -padx 2 
            if  { $blocked } {
                $but configure -fg red
            } 
			incr i 1
            set dso ""
            set blocked 0
		}
		pack $subfh -side top -fill both -expand 1
		appendStatus $::cmonBlock::statusw($page) "$i DSOs, [ llength $data ] blocked" 0 blue
        
	} err ] } {
		appendStatus $::cmonBlock::statusw($page) "[myName] $err"
	}
}


#******************************************************** 
##
## Name: cmonBlock::reset
##
## Description:
## changes all pages back to initial state
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonBlock::reset { page } {

    catch { destroy $::cmonBlock::varFrame($page) }

}   


##******************************************************* 
##
## Name: cmonBlock::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonBlock::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonBlock::topfw($page) normal
	array set ::cmonBlock::state [ list $page 0 ]
    set ::cmonBlock::var($page,dso) ""
    $::cmonBlock::bstart3($page) configure -text "Block user" -bg red -fg white
}

##******************************************************* 
##
## Name: cmonBlock::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonBlock::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonBlock::topfw($page) disabled	
	array set ::cmonBlock::state [ list $page 1 ]
	
}
