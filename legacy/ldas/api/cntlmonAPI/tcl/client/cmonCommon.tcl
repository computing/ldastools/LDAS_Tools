## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon client Tcl Script.
##
## This script has global functions used by packages
## in cmonClient
## some are imported from LDAS generic 
;#barecode
package provide cmonCommon 1.0

namespace eval cmd {}
;#end  

## ******************************************************** 
##
## Name: traceme
##
## Description:
## quicky trace function
##
## Usage:
##   e.g.trace variable ::cmonClient::pageUpdate w traceme
##
## Comments:

proc traceme {args} {
	set var [ lindex $args 0 ]
	puts "var [ set $var ]"
}

## ******************************************************** 
##
## Name: dumpFile
##
## Description:
## return the contents of a file in a form suitable for
## "more" or "less" etc.
##
## Usage:
##       set data [ dumpFile filename ]
##
## Comments:
## This is an efficient slurper of files.

proc dumpFile { { file "" } } {
     if { ! [ string length $file ] } {
        return {}
        }
     if { ! [ file exists $file ] } {
        return {}
        }
     if { [ catch { set fid [ open $file r ] } err ] } {
        return -code error $err
        }
     set size [ file size $file ]
     set data [ read $fid $size ]
     catch { close $fid }
     return $data
}
## ******************************************************** 

## ******************************************************** 
## sock handling procs (from sock.tcl)
##
## Name: cmd::result
##
## Description:
## Receives data from a socket.  The socket is opened in
## blocking mode, and is read using this function.
## cmd::result puts the socket in non-blocking mode and
## attempts to read as much as possible in line buffered
## mode.  After reading the socket is put back into
## blocking mode.
##
## Usage:
##       set sid [ socket $host $port ]
##       puts $sid "get_me_some_data"
##       flush $sid
##       fileevent $sid readable [ return [ cmd::result $sid ] ]
##
## Comments:
## This procedure provides "connectionful" communication.
## For "connectionless" communication, call cmd::disconnect
## immediately afterwards.

proc cmd::result { { sid "" } { timeout 10 } } {
     set i 0
     set data {}
     ;## line buffered non-blocking configuration forced
     fconfigure $sid -buffering line
     fconfigure $sid -blocking off
     
     while { 1 } {
        after $i;
        if { [ eof $sid ] } { break }
        ;## if no bytes are read
        if { [ gets $sid line ] < 0 } {
           ;## then increment delay
           incr i
           ;## if delay has reached the timeout threshhold
           if { $i > $timeout } {
              ;## then don't try to read any more
              break
           } else {
              ;## otherwise loop and try to read some more
              continue
           }   
        } else {
           ;## if bytes WERE read, reset the delay to 0
           ;## to read remaining data as quickly as
           ;## possible
           set i 0
        }
        
        append data "$line\n"
        
        set line {}
        ;## special handler for persistent connections
        if { [ regexp {^[\n]*~~(\d+)\n$} $data -> size ] } {
           ;## do block data read 
           ;## must set blocking on to read entire msg
           fconfigure $sid -buffering full
           fconfigure $sid -blocking on
           set data [ read $sid $size ]
           break
        }   
     }
     set data [ string trim $data "\n" ]
     ;## reconfigure the socket to blocking mode
     ;## so that fileevent can handle future
     ;## connections
     fconfigure $sid -blocking on
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cmd::size
##
## Description:
## Attach proper size info for handling by cmd::result
## when using a persistent connection.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cmd::size { cmd } {
     set size [ string length $cmd ]
     set cmd "~~$size\n$cmd"
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
## unique key or encryption key generation for passwords
## from genericAPI.tcl
## 
## Name: key::md5
##
## Description:
## Don Libes pure Tcl implementation of the MD5 hashing
## algorithm.
##
## Parameters:
##
## Usage:
##       example: key::md5 foo --> acbd18db4cc2f85cedef654fccc4a4d8
##
## Comments:
## VERY SLOW.  Use only on small strings.  Plainly, the central
## rotator code needs to be redone as an algorithm.
## copied from genericAPI key.tcl

namespace eval key { }

proc key::md5 { text } {

 namespace eval md5 {
    variable i
    variable t
    variable T

    set i 0
    foreach t {
	0xd76aa478 0xe8c7b756 0x242070db 0xc1bdceee
	0xf57c0faf 0x4787c62a 0xa8304613 0xfd469501
	0x698098d8 0x8b44f7af 0xffff5bb1 0x895cd7be
	0x6b901122 0xfd987193 0xa679438e 0x49b40821

	0xf61e2562 0xc040b340 0x265e5a51 0xe9b6c7aa
	0xd62f105d 0x2441453  0xd8a1e681 0xe7d3fbc8
	0x21e1cde6 0xc33707d6 0xf4d50d87 0x455a14ed
	0xa9e3e905 0xfcefa3f8 0x676f02d9 0x8d2a4c8a

	0xfffa3942 0x8771f681 0x6d9d6122 0xfde5380c
	0xa4beea44 0x4bdecfa9 0xf6bb4b60 0xbebfbc70
	0x289b7ec6 0xeaa127fa 0xd4ef3085 0x4881d05
	0xd9d4d039 0xe6db99e5 0x1fa27cf8 0xc4ac5665

	0xf4292244 0x432aff97 0xab9423a7 0xfc93a039
	0x655b59c3 0x8f0ccc92 0xffeff47d 0x85845dd1
	0x6fa87e4f 0xfe2ce6e0 0xa3014314 0x4e0811a1
	0xf7537e82 0xbd3af235 0x2ad7d2bb 0xeb86d391
    } {
	incr i
	set T($i) [expr $t]
    }
 }


 proc md5::md5 {msg} {
    variable T

    set msgLen [string length $msg]

    set padLen [expr {56 - $msgLen%64}]
    if {$msgLen % 64 > 56} {
	incr padLen 64
    }

    if {$padLen == 0} { incr padLen 64 }
    append msg [binary format "a$padLen" \200]

    append msg [binary format "i1i1" [expr {8*$msgLen}] 0]
    
    set msgList [split $msg ""]

    set A [expr 0x67452301]
    set B [expr 0xefcdab89]
    set C [expr 0x98badcfe]
    set D [expr 0x10325476]

    set i 0
    foreach {c1 c2 c3 c4} $msgList {
	binary scan $c1$c2$c3$c4 "i" M($i)
	incr i
    }

    set blockLen [array size M]

    for {set i 0} {$i < $blockLen/16} {incr i} {
	for {set j 0} {$j<16} {incr j} {
	    set X($j) $M([expr {$i*16+$j}])
	}

	set AA $A
	set BB $B
	set CC $C
	set DD $D

	set A [expr {$B + [<<< [expr {$A + [F $B $C $D] + $X(0)  + $T(1) }]  7]}]
	set D [expr {$A + [<<< [expr {$D + [F $A $B $C] + $X(1)  + $T(2) }] 12]}]
	set C [expr {$D + [<<< [expr {$C + [F $D $A $B] + $X(2)  + $T(3) }] 17]}]
	set B [expr {$C + [<<< [expr {$B + [F $C $D $A] + $X(3)  + $T(4) }] 22]}]
	set A [expr {$B + [<<< [expr {$A + [F $B $C $D] + $X(4)  + $T(5) }]  7]}]
	set D [expr {$A + [<<< [expr {$D + [F $A $B $C] + $X(5)  + $T(6) }] 12]}]
	set C [expr {$D + [<<< [expr {$C + [F $D $A $B] + $X(6)  + $T(7) }] 17]}]
	set B [expr {$C + [<<< [expr {$B + [F $C $D $A] + $X(7)  + $T(8) }] 22]}]
	set A [expr {$B + [<<< [expr {$A + [F $B $C $D] + $X(8)  + $T(9) }]  7]}]
	set D [expr {$A + [<<< [expr {$D + [F $A $B $C] + $X(9)  + $T(10)}] 12]}]
	set C [expr {$D + [<<< [expr {$C + [F $D $A $B] + $X(10) + $T(11)}] 17]}]
	set B [expr {$C + [<<< [expr {$B + [F $C $D $A] + $X(11) + $T(12)}] 22]}]
	set A [expr {$B + [<<< [expr {$A + [F $B $C $D] + $X(12) + $T(13)}]  7]}]
	set D [expr {$A + [<<< [expr {$D + [F $A $B $C] + $X(13) + $T(14)}] 12]}]
	set C [expr {$D + [<<< [expr {$C + [F $D $A $B] + $X(14) + $T(15)}] 17]}]
	set B [expr {$C + [<<< [expr {$B + [F $C $D $A] + $X(15) + $T(16)}] 22]}]

	set A [expr {$B + [<<< [expr {$A + [G $B $C $D] + $X(1)  + $T(17)}]  5]}]
	set D [expr {$A + [<<< [expr {$D + [G $A $B $C] + $X(6)  + $T(18)}]  9]}]
	set C [expr {$D + [<<< [expr {$C + [G $D $A $B] + $X(11) + $T(19)}] 14]}]
	set B [expr {$C + [<<< [expr {$B + [G $C $D $A] + $X(0)  + $T(20)}] 20]}]
	set A [expr {$B + [<<< [expr {$A + [G $B $C $D] + $X(5)  + $T(21)}]  5]}]
	set D [expr {$A + [<<< [expr {$D + [G $A $B $C] + $X(10) + $T(22)}]  9]}]
	set C [expr {$D + [<<< [expr {$C + [G $D $A $B] + $X(15) + $T(23)}] 14]}]
	set B [expr {$C + [<<< [expr {$B + [G $C $D $A] + $X(4)  + $T(24)}] 20]}]
	set A [expr {$B + [<<< [expr {$A + [G $B $C $D] + $X(9)  + $T(25)}]  5]}]
	set D [expr {$A + [<<< [expr {$D + [G $A $B $C] + $X(14) + $T(26)}]  9]}]
	set C [expr {$D + [<<< [expr {$C + [G $D $A $B] + $X(3)  + $T(27)}] 14]}]
	set B [expr {$C + [<<< [expr {$B + [G $C $D $A] + $X(8)  + $T(28)}] 20]}]
	set A [expr {$B + [<<< [expr {$A + [G $B $C $D] + $X(13) + $T(29)}]  5]}]
	set D [expr {$A + [<<< [expr {$D + [G $A $B $C] + $X(2)  + $T(30)}]  9]}]
	set C [expr {$D + [<<< [expr {$C + [G $D $A $B] + $X(7)  + $T(31)}] 14]}]
	set B [expr {$C + [<<< [expr {$B + [G $C $D $A] + $X(12) + $T(32)}] 20]}]

	set A [expr {$B + [<<< [expr {$A + [H $B $C $D] + $X(5)  + $T(33)}]  4]}]
	set D [expr {$A + [<<< [expr {$D + [H $A $B $C] + $X(8)  + $T(34)}] 11]}]
	set C [expr {$D + [<<< [expr {$C + [H $D $A $B] + $X(11) + $T(35)}] 16]}]
	set B [expr {$C + [<<< [expr {$B + [H $C $D $A] + $X(14) + $T(36)}] 23]}]
	set A [expr {$B + [<<< [expr {$A + [H $B $C $D] + $X(1)  + $T(37)}]  4]}]
	set D [expr {$A + [<<< [expr {$D + [H $A $B $C] + $X(4)  + $T(38)}] 11]}]
	set C [expr {$D + [<<< [expr {$C + [H $D $A $B] + $X(7)  + $T(39)}] 16]}]
	set B [expr {$C + [<<< [expr {$B + [H $C $D $A] + $X(10) + $T(40)}] 23]}]
	set A [expr {$B + [<<< [expr {$A + [H $B $C $D] + $X(13) + $T(41)}]  4]}]
	set D [expr {$A + [<<< [expr {$D + [H $A $B $C] + $X(0)  + $T(42)}] 11]}]
	set C [expr {$D + [<<< [expr {$C + [H $D $A $B] + $X(3)  + $T(43)}] 16]}]
	set B [expr {$C + [<<< [expr {$B + [H $C $D $A] + $X(6)  + $T(44)}] 23]}]
	set A [expr {$B + [<<< [expr {$A + [H $B $C $D] + $X(9)  + $T(45)}]  4]}]
	set D [expr {$A + [<<< [expr {$D + [H $A $B $C] + $X(12) + $T(46)}] 11]}]
	set C [expr {$D + [<<< [expr {$C + [H $D $A $B] + $X(15) + $T(47)}] 16]}]
	set B [expr {$C + [<<< [expr {$B + [H $C $D $A] + $X(2)  + $T(48)}] 23]}]

	set A [expr {$B + [<<< [expr {$A + [I $B $C $D] + $X(0)  + $T(49)}]  6]}]
	set D [expr {$A + [<<< [expr {$D + [I $A $B $C] + $X(7)  + $T(50)}] 10]}]
	set C [expr {$D + [<<< [expr {$C + [I $D $A $B] + $X(14) + $T(51)}] 15]}]
	set B [expr {$C + [<<< [expr {$B + [I $C $D $A] + $X(5)  + $T(52)}] 21]}]
	set A [expr {$B + [<<< [expr {$A + [I $B $C $D] + $X(12) + $T(53)}]  6]}]
	set D [expr {$A + [<<< [expr {$D + [I $A $B $C] + $X(3)  + $T(54)}] 10]}]
	set C [expr {$D + [<<< [expr {$C + [I $D $A $B] + $X(10) + $T(55)}] 15]}]
	set B [expr {$C + [<<< [expr {$B + [I $C $D $A] + $X(1)  + $T(56)}] 21]}]
	set A [expr {$B + [<<< [expr {$A + [I $B $C $D] + $X(8)  + $T(57)}]  6]}]
	set D [expr {$A + [<<< [expr {$D + [I $A $B $C] + $X(15) + $T(58)}] 10]}]
	set C [expr {$D + [<<< [expr {$C + [I $D $A $B] + $X(6)  + $T(59)}] 15]}]
	set B [expr {$C + [<<< [expr {$B + [I $C $D $A] + $X(13) + $T(60)}] 21]}]
	set A [expr {$B + [<<< [expr {$A + [I $B $C $D] + $X(4)  + $T(61)}]  6]}]
	set D [expr {$A + [<<< [expr {$D + [I $A $B $C] + $X(11) + $T(62)}] 10]}]
	set C [expr {$D + [<<< [expr {$C + [I $D $A $B] + $X(2)  + $T(63)}] 15]}]
	set B [expr {$C + [<<< [expr {$B + [I $C $D $A] + $X(9)  + $T(64)}] 21]}]

	incr A $AA
	incr B $BB
	incr C $CC
	incr D $DD
    }
    return [bytes $A][bytes $B][bytes $C][bytes $D]
 }


 proc md5::<<< {x i} { expr {($x << $i) | ((($x >> 1) & 0x7fffffff) >> (31-$i))} }

 proc md5::F {x y z} {expr {(($x & $y) | ((~$x) & $z))}}
 proc md5::G {x y z} {expr {(($x & $z) | ($y & (~$z)))}}
 proc md5::H {x y z} {expr {$x ^ $y ^ $z}}
 proc md5::I {x y z} {expr {$y ^ ($x | (~$z))}}

 proc md5::byte0 {i} {expr {0xff & $i}}
 proc md5::byte1 {i} {expr {(0xff00 & $i) >> 8}}
 proc md5::byte2 {i} {expr {(0xff0000 & $i) >> 16}}
 proc md5::byte3 {i} {expr {((0xff000000 & $i) >> 24) & 0xff}}

 proc md5::bytes {i} {format %0.2x%0.2x%0.2x%0.2x [byte0 $i] [byte1 $i] [byte2 $i] [byte3 $i]}

 return [ md5::md5 $text ]
}

proc key::time { } {
     set key   [ expr { pow(2,31)+[clock clicks] } ]
     set begin [ expr { [ string length $key ] -8 } ] 
     set end   [ expr { [ string length $key ] -3 } ] 
     set key   [ string range $key $begin $end ]
     set key   [ gpsTime ]$key
     return $key
}

## *******************************************************

## ******************************************************** 
##
## Name: _exec 
##
## Description:
## New wrapper for exec that captures stderr ito a temp
## file and sets a global variable to a list of two elements
## consisting of stdout and stderr.
##
## Parameters:
##
## Usage:
##
##    _exec "curl http://www.ligo.caltech.edu" foo
##
## Use a trace to watch the global variable provided as the
## second argument.
##
## Comments:
##
proc _exec { prog { var "" } { uniq "" } { i 0 } { to 5 } } {
     
     if { ! [ regexp {^file\d+$} $prog ] } {
        ;## we always override the third argument on first call
        set uniq _exec-[ clock seconds ][ clock clicks ]
        set tmpfile /tmp/$uniq
        set tmpvar  ::$uniq
        set $tmpvar [ list ]
        if { [ catch {
           set fid [ open "|/usr/bin/env LD_PRELOAD= $prog 2>$tmpfile" ]
           fconfigure $fid -blocking off
           fconfigure $fid -buffersize 50000
           fconfigure $fid -buffering full
        } err ] } {
           catch { ::unset ::$uniq }
           catch { ::close $fid }
           catch { file delete -force $tmpfile }
           return -code error "_exec: $err"
        }
     } else {
        set fid $prog
        set tmpfile /tmp/$uniq
        set tmpvar  ::$uniq
     }
     
     set ret [ read $fid ]
    
     if { ($i / 10) >= $to } {
        if { ! [ string length $ret ] } {
           set ret timed_out
        }
     }
     
     ;## make sure a tempfile exists no matter what!
     if { ! [ file exists $tmpfile ] } {
        set fid [ open $tmpfile w ]
        ::close $fid
     }
     
     ;## first condition is because we only ever were
     ;## reading 4096 chars ever. bizarre.
     if { [ string length $ret ] && \
        ! [ file size $tmpfile ] && \
        ! [ eof $fid ] } {
        append $tmpvar $ret
        incr i
        if { $to <= 10 } { set to 1 }
        ;## when we re-call we populate 5 arguments
        after 100 [ list _exec $fid $var $uniq $i $to ]

     } elseif { [ string length $ret ] || \
                [ file size $tmpfile ] || \
                [ string length [ set $tmpvar ] ] } {
        set ret [ set $tmpvar ]$ret
        set pids [ pid $fid ]
        
        catch { ::close $fid }
        catch { ::unset $tmpvar }
        foreach pid $pids {
           catch { ::exec kill -KILL $pid }
        }
        set fid [ open $tmpfile r ]
        set err [ read $fid [ file size $tmpfile ] ]
        catch { ::close $fid }
        catch { ::unset $tmpvar }
        file delete -force $tmpfile
        if { [ string length $var ] } {
           set ::$var [ list $ret $err ]
        }   
     } else {
        incr i
        after 100 [ list _exec $fid $var $uniq $i ]
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: sysData 
##
## Description:
## Returns useful information about a running program given
## the program name or the process i.d.
## 
## Parameters:
## api - a program name or process i.d.
##
## Usage:
##       set data [ sysData manager ]
##
##   returns:
##            name pid pcpu pmem vsz rss updt
##     where:
##            name - name of the process as reported by ps
##             pid - process i.d.
##            pcpu - percent of cpu cycles used by process
##            pmem - percent of system memory used by process
##             vsz - total allocated memory for process
##             rss - current memory in use
##           etime - elapsed run time of process
##
## Comments:
## Will return the data for the main thread or the oldest
## running process which matches if the argument is a name.
## quickie modified version of sysData for cmonClient

proc sysData { { api "" } } {
     set done 0
     set datum [ list ]
     if { ! [ string length $api ] } {
        set api $::API
     }
     
     ;## force rational API name
     #regexp {(.+)API} $api -> api
     #set api ${api}API
     
     if { [ catch {
        set tmpvar ps-[ clock clicks ]
        _exec "/bin/ps -Ao fname,pid,pcpu,pmem,vsz,rss,etime,args" $tmpvar
        if { ! [ info exists ::$tmpvar ] } {
           vwait ::$tmpvar
        }
        set data [ lindex [ set ::$tmpvar ] 0 ]
        unset ::$tmpvar
     } err ] } {
        catch { unset ::$tmpvar }
        return -code error "[ myName ]: $err"
     }  
     set data [ lrange [ split $data "\n" ] 1 end-1 ]
     ;## sort in descending percent cpu usage
     set data [ lsort -dictionary -decreasing -index 2 $data ]
     foreach datum $data {
	 	if	{ ! [ regexp "wish.+cmonClient" $datum ] } {
			continue
		}
        ;## lose defunct processes please!
        # if { [ llength $datum ] != 8 } { continue }
        foreach { name pid pcpu pmem vsz rss uptime args } $datum {
			puts stderr "name=$name pid=$pid pcpu=$pcpu pmem=$pmem vsz=$vsz rss=$rss uptime=$uptime args=$args"
			#if	{ ! [ regexp $api $args ] } {
			#	continue
			#}
         	#if { ! [ regexp -nocase {[a-z]} $name ] } {
            #  break
            #}

           ;## all real API names are at least 6 chars long
           # if { [ string length $name ] < 6 } {
           #   break
           #}
           
           ;## if the argument was a pid
           if { [ string equal $api $pid ] } {
              set done 1
			  puts "done=$done"
              break
           }
           ;## if the argument was a program name
           if { [ catch {
              if { [ regexp $name $api ] || [ regexp $api $name ] } {
                 set sortkey [ join [ split $uptime :- ] {} ]
                 set sortkey [ string trimleft $sortkey 0 ]
                 if { ! [ info exists candidate ] } {
                    set candidate [ list $sortkey $datum ]
                 } else {
                    set oldkey [ lindex $candidate 0 ]
                    if { $sortkey > $oldkey } {
                       set candidate [ list $sortkey $datum ]
                    }
                 }    
              }
           } err ] } {
              ;## just continue if the regexp doesn't compile
           }
        }   
        if { $done } { break }
        ;## if no matches are found, don't return the nth element!
        set datum [ list ]
     }
     if { [ info exists candidate ] } {
        set datum [ lindex $candidate 1 ]
     }

     return $datum
}
## ******************************************************** 

## ******************************************************** 
##
## Name: myName
##
## Description:
## Returns the name of the proc that calls it.
##
## Usage:
##       set myname [ myName ]
##
## Comments:
## This is the canonical form for getting a procs name.
## The utility of this is that a proc may be dynamically
## named, and the name managed much more easily this way
## than by trying to stack up and unstack proc names.
##
## Note that level 0 is the context of myName,
## level -1 is the context of the caller.  In general,
## you will not mess with the level.  Though you could
## get the name of the proc that called the caller with
## -2, and so forth.

proc myName { { level "-1" } } { 
     
     if { $level > 0 } {
        return -code error "myName: called with level > 0 ($level)."
     }

     if { [ catch {
        set name [ lindex [ info level $level ] 0 ]
     } err ] } {
        set name $::API
     }
     set name
}

## ******************************************************** 
##
## Name: ifConfig
##
## Description:
## Returns a list of lists consisting of the interface
## id string, the IP address for the interface, and the
## aliases for the IP address from /etc/hosts.
##
## Parameters:
##
## Usage:
##   Example output:
##
##    {lo0 127.0.0.1 localhost}
##    {ge0 10.16.0.5 gateway}
##    {hme0 131.215.115.248 {ldas-dev ldas-dev.ligo.caltech.edu loghost}}
##
## Comments:
##

proc ifConfig { args } {
     
     if { [ catch {
        set interfaces [ list ]
        if { [ file executable /usr/sbin/ifconfig ] } {
           catch { ::exec /usr/sbin/ifconfig -a } data
        } elseif { [ file executable /sbin/ifconfig ] } {
           catch { ::exec /sbin/ifconfig -a } data
        } else {
           return -code error "can't find 'ifconfig' executable!"
        }
        set fid [ open /etc/hosts r ]
        set hostdata [ read $fid [ file size /etc/hosts ] ]
        ::close $fid
        foreach line [ split $hostdata "\n" ] {
           array set hosts \
              [ list [ lindex $line 0 ] [ lrange $line 1 end ] ]
        }   
        foreach line [ split $data "\n" ] {
           regexp {^(lo |[a-z]+\d+)} $line -> if
           set if [ string trim $if ]
           if { [ regexp {^\s+inet\s+(?:addr:)?(\S+)} $line -> ip ] } {
              if { [ info exists hosts($ip) ] } {
                 lappend interfaces [ list $if $ip $hosts($ip) ]
              } else {
                 lappend interfaces [ list $if $ip dhcp ]
              }
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $interfaces
}
## ******************************************************** 

## ********************************************************
## common procs 
## ******************************************************** 
##
## Name: bgerror 
##
## Description:
## Handles uncaught errors
##
## Usage:
##
## Comments:

proc bgerror { msg } {
     puts stderr "bgerror: $msg"
	 puts stderr $::errorInfo 
	 if	{ [ regexp {(cmon[^:]+)::\S+ (\S+)} $msg -> name page ] } {
	 	catch { $name::state0 $page } err
	 }
     set strlist [ split $msg ]
     set index [ lsearch $strlist "*sock*" ]
     if  { $index > -1 } {
         set sid [ lindex $strlist $index ]
         regsub -all  {[\"]} $sid {} sid 
         puts "bgerror sid = $sid."
    }
}

## ******************************************************** 
##
## Name: readData 
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:

proc readData { sid } {

	if	{ [ catch {
	    set seqpt "cmd::result $sid"
	    set result [ cmd::result $sid ]
    } err ] } {
		catch { close $sid }
        cmonClient::state0 lostconnect
        return
	}

    set site [ string trim $::cmonClient::var(site) ]
    if  { [ string length $result ] <= 0  } {
		if { [ eof $sid ] } {
			catch { close $sid }
        	cmonClient::state0 lostconnect
		}
        return
    }
    set disconnect 0
    if  { [ catch { 
		set callback ""
		set seqpt "extracting result"
	    regexp {^\{(.+)\}$} $result match result
	    set result [ split $result \n ]
	    set callback [ lindex $result 0 ]
        regexp {^\{(.*)} $callback match callback
        set clientCmd [ lindex $result 1 ]		
	    set rc [ lindex $result 2 ]
		;## puts "callback=$callback, rc=$rc,$clientCmd."
	    ##puts "received [ llength [ lrange $result 3 end ] ] lines of html"
	    ;##puts "first=[lindex $result 3 ], end=[lindex $result end  ]"	
	    set seqpt "join data"
	    set data [ join [ lrange $result 3 end ] \n ]
		set update 0
		if	{ [ string match nocallback $callback ] } {
			set update 1
			if	{ $rc == 3 } {
                set disconnect 1
				error $data
			} 
            ;## rc=4 for pure update			
			set cmd [ lindex $result 3 ]
			set reply [ lindex $result 4 ]
			regexp {^\{(.+)\}$} $cmd -> cmd
	 		set cmd [ subst -nobackslashes -novariables -nocommands $cmd ]
			eval $cmd 
            ;## if rc == 0, new client
            if  { $rc == 0 } {
			    regexp {([^:]+):([^:]+):([^:]+)[:]*(.*)} $clientCmd -> ::cmonClient::client \
				::cmonClient::serverVersion ::cmonClient::serverKey cstatus
                set ::serverVersionF [ split $::cmonClient::serverVersion . ]
			    cmonClient::setConnected
			    if	{ [ string length $reply ] } {
                	set ack [ showWarning $reply ]                    
			    }
                ;## if user is control, set control password
                if  { [ string match priviledged $cstatus ] && $::passwdRemain == -1 } {
                    set ::cntllogin $::logintext
                    set ::cntlpasswd $::passwdtext
                    setupPasswdWidget
                }
            }
		} elseif { [ string match key* $callback  ] } {
			set update 1
			if	{ $rc == 3 } {
                set disconnect 1
				error $data
			}
			set ::cmonClient::serverKey $clientCmd
			cmonClient::sendUserPasswd $sid 		
		} else {
			set data [ subst -nobackslashes -novariables -nocommands $data ]
        	set seqpt "$callback $rc $clientCmd"
			set data [ concat $data ]
			if	{ $::DEBUG } {
                if  { ! [ file exist $::TMPDIR ] } {
                    file mkdir $::TMPDIR
                }
                if	{ ! [ regexp -nocase -- {NoOp} $data ] } {
					set fd [ open $::TMPDIR/dataRecv.$site w ]
					puts $fd "$callback $rc\n$data"
					close $fd 
               	}
			}
			foreach { procname page } $callback { break }
			;## unset control password if error
			eval { $procname $page $rc $clientCmd $data }
            
            if  { [ checkPasswdError $data ] } {
                resetPasswdWidget
            }
		}
	} err ] } {	
		;## may need to revert to state0
		if	{ [ regexp -nocase {(Invalid|incorrect) password} $err ] || $update } {
            if  { $disconnect } {  
                catch { unset ::logintext }
                catch { unset ::passwdtext }
                resetPasswdWidget
			    cmonClient::state0 readerror $err
            }
		} else {
		    # puts "read error $err, see details in file $::TMPDIR/dataRecv.$site"
			set ack [ tk_messageBox -type ok -default ok \
			-message "Possible data transmission error $err, please examine details in \
				file $::TMPDIR/dataRecv.$site" \
				-icon info ]
		}
		
        if	{ ! [ regexp -nocase -- {NoOp} $data ] } {
			set fd [ open $::TMPDIR/dataRecv.$site w ]
			puts $fd "$callback $rc\n$data"
			close $fd 
        }
	}
}

	
## ******************************************************** 
##
## Name: sendCmd
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
##
## Usage:
##
## Comments:

proc sendCmd { cmd } {

	;## test if socket is still opened
	foreach { host port } $::siteport($::cmonClient::var(site)) { break }
    
    if  { $::cmonClient::state != 1 } {
        return -code "Please connect to an LDAS site first."
    }
    
	;## tclglobus channel and tcl sockets here
    if  { [ catch {
		;## only tcl sockets for now to check if server is still up
        set cmd [ cmd::size $cmd ]
    	if	{ ! $::USE_GLOBUS } {
        	set sid [ socket $host $port ]
        	close $sid  
        } else {
            verifyProxyWithRetry 0
        }     
        debugPuts "$cmd\nwriting [ string length $cmd ] bytes to $::cmonClient::sid"
        puts $::cmonClient::sid $cmd
	    flush $::cmonClient::sid
    } err ] } {
        catch { ::close $::cmonClient::sid } 
        cmonClient::state0 lostconnect
		return -code error "failed to write to $host@$port: $err"
    } 
    #if	{ [ info exist done ] } {
    #	return -code error "Your proxy has expireed. You need to enter the passphrase before you can execute any more requests."
    #}
}

## ******************************************************** 
##
## Name: getSockData
##
## Description:
## read response for a cmd issued to emergency socket of an API
##
## Usage:
##
## Comments:
proc getSockData { sid var } {
	if	{ [ catch {
	    set seqpt "cmd::result $sid"
	    set $var [ cmd::result $sid ]
    } err ] } {
        set $var $err
	}
    catch { close $sid }
}

## ******************************************************** 
##
##
## getEmergData 
## 
## Description 
## connects to ldas mgr at site and gets cntlmonAPI operator port
## number at a site  
## Parameters 
## host - ldas manager host
## port - ldas manager port number 
## 
## Usage 
## set target [ getcntlmonPort ldas-dev.ligo.caltech.edu 10002 ]
## 
## Comments:
## uniqid does not have :: in front 
## get data from an API's emergency socket 
## msg format e.g. set msg "\{puts \$cid \[ validService cntlmon operator \]\}"
 
proc getEmergData { host port msg } {

    set uniqid [ key::time ]
    set ::$uniqid {}
    if 	{ [ catch { 

        set sid [ socketTimeout $host $port 2000 ]
    } err ] } {
        catch { close $sid }
        return -code error "connect error $host@$port: $err"
    }
    if  { [ catch {  
        fconfigure $sid -buffering line
        puts $sid "$::MGRKEY $msg"
        fileevent $sid readable \
            [ list getSockData $sid ::$uniqid ] 
        vwait ::$uniqid
    } err ] } {
        catch { close $sid }
        return -code error "connect error $host@$port: $err"
    }
    set retval [ set ::$uniqid ]
    unset ::$uniqid
    return $retval   
}

## ******************************************************** 
##
## Name: handleExit
##
## Description:
## cleanup when exit is selected or window manager close button
##
## Usage:
##
## Comments:

proc handleExit {} {   
	
   	if	{ ! $::cmonClient::state } {
    	;## just in case it did open globus channel
    	catch { close $::cmonClient::sid }
        exitCleanup
        clientExit
	} else {
		cmonClient::state0 exit
	}
}

## ******************************************************** 
##
## Name: exitCleanup
##
## Description:
## cleanup when exit is selected or window manager close button
##
## Usage:
##
## Comments:

proc exitCleanup {} {

	cmonClient::saveState
    catch { file delete -force $::TMPDIR } err
    catch { close $::logfd }
    catch { file delete -force cmonClient[pid].log }
    foreach pid $::cmonClient::childPids {
	    catch { exec kill -9 $pid }
    } 
}

## ******************************************************** 
##
## Name: clientExit
##
## Description:
## exit point of client
##
## Usage:
##
## Comments:

proc clientExit {} {

    ;## makes exiting faster
    wm withdraw .
    ::exit
}

## ******************************************************** 
##
## Name: socketTimeout 
##
## Description:
## make client socket connection with a timeout
##
## Usage:
##
## Comments:

proc socketTimeout { host port timeout } {
    set ::connected ""
    after $timeout {set ::connected timeout}
    set sid [ socket $host $port ]
    fileevent $sid w {set ::connected ok}
    vwait ::connected
    if  { ! [ string compare $::connected "timeout" ] } {
        return -code error timeout
    } else {
        return $sid
    }
}

## ******************************************************** 
##
## Name: serverOpen
##
## Description:
## open a server port for client to receive email from manager
##
## Usage:
##
## Comments:
## this only works for local port

proc serverOpenX { callback {port 0}} {
         
    if  { [ catch {        
        set lsid [socket -server [ list serverCfg $callback ] -myaddr [info hostname] $port]
        foreach {::LADDR ::LHOST ::LPORT} [fconfigure $lsid -sockname] {break}
        if  { [ regexp {127.0.0.1} $::LADDR ] } {
            if  { [ regexp {eth0\s+([^\s]+)} [ ifConfig ] -> ::LADDR ] } {
                puts $::logfd "eth0 $::LADDR"
                close lsid
                set lsid [socket -server [ list serverCfg $callback ] -myaddr $eth0 $port]
            }
        }                
    } err ] } {
        return -code error $err
    }
    return -code ok lsid
}

proc serverOpen { callback {port 0}} {
         
    if  { [ catch { 
        set data [ ifConfig ]
        set ::LADDR ""
        foreach addr $data {
	        if	{ [ regexp {10\.|127\.|localhost} $addr ] } {
		        continue
	        }
	        if	{ [ regexp {\s+([\d\\.]+)} [ join $addr ] -> ::LADDR ] } {
		        break
	        }
        } 
        if  { ! [ string length $::LADDR ] } {
            set ::LADDR [ info hostname ]
        }
        set lsid [socket -server [ list serverCfg $callback ] -myaddr $::LADDR $port]
        foreach {::LADDR ::LHOST ::LPORT} [fconfigure $lsid -sockname] {break}     
        flush $::logfd    
    } err ] } {
        return -code error $err
    }
    return -code ok lsid
}

## ******************************************************** 
##
## Name: serverCfg
##
## Description:
## configure the server port and callback
##
## Usage:
##
## Comments:

proc serverCfg  { callback sid addr port} {
    if  { [ catch {
        fconfigure $sid -buffering line -blocking off
        fileevent $sid readable [list $callback $sid]
    } err ] } {
        puts stderr "Unable to create server port, exiting"
    }
    return
}

## ******************************************************** 
##
## Name: serverClose
##
## Description:
## close client's server port
##
## Usage:
##
## Comments:

proc serverClose {sid} {
    catch {close $sid}
    return
}

## ********************************************************
## common procs for widgets
##
## ******************************************************** 

## ******************************************************** 
##
## Name: setCombo 
##
## Description:
## set selected item in combo into a variable
##
## Usage:
##
## Comments:

proc setCombo { combo } {
    $combo setvalue @[ $combo getvalue ] 
}

## ******************************************************** 
##
## Name: enableStart 
##
## Description:
## configure the "GO" button to enable state
##
## Usage:
##
## Comments:
## sets default command

proc enableStart { name page { state "normal" } { cmd "sendRequest" } } {
    set bstart [ set ${name}::bstart($page) ]
    $bstart configure -text "SUBMIT" -state $state
    $bstart configure -command "${name}::$cmd $page"
}

## ******************************************************** 
##
## Name: disableStart 
##
## Description:
## configure the "GO" button to disabled state
##
## Usage:
##
## Comments:
## sets default command

proc disableStart { name page { state "normal" } { cmd "cancelRequest" } } {
    set bstart [ set ${name}::bstart($page) ]
    $bstart configure -text "CANCEL" -state $state
    $bstart configure -command "${name}::$cmd $page"
}

## ******************************************************** 
##
## Name: passwdBindReturn 
##
## Description:
## bind return of password dialog
##
## Usage:
##
## Comments:
## this is dependent on widgets used in 1.4.1 and 1.6

proc passwdBindReturn { w } {

	if	{ [ winfo exist $w ] } {
		bind $w <Return> ""
		bind $w.frame.lablog  <Return> "focus $w.frame.labpass.e"
	}

}

## ******************************************************** 
##
## Name: validateLogin 
##
## Description:
## configure the "GO" button to disabled state
##
## Usage:
##
## Comments:
## temporarily validates the login for user
## should be done in server
## control 0 - access user login
## control 1 - priviledged user login

proc validateLogin { { control 0 } } {

	if	{ $::USE_GLOBUS } {    	
    	if	{ [ catch {
            verifyProxyWithRetry 0
        } err ] } {  	
        	return -code error "$err"
        }
        return "$::globusUser x.509"
    } 	
	set result ""
	if	{ $control == 1 } {	
		set helptext "login as priviledged user only"
		set label "Enter priviledged user login Id and password $::cmonClient::var(site)"
		set passwdvar ::cntlpasswd
		set loginvar ::cntllogin
		if	{ [ info exist ::cntllogin ] && 
			  [ info exist ::cntlpasswd ] } {
			return "$::cntllogin [ cmonClient::encryptUserPasswd $::cntlpasswd ]"
		}
		set ::cntlpasswd ""
		set ::cntllogin ""
	} else {
		set helptext "login as non-priviledged user"
		set label "Enter access login Id and password for $::cmonClient::var(site)"
		if 	{ [ info exist ::logintext ] } {
            return "$::logintext [ cmonClient::encryptUserPasswd $::passwdtext ]"
		}
		set passwdvar ::passwdtext 
		set loginvar ::logintext
		set ::passwdtext ""
		set ::logintext ""
	}
	if	{ [ catch {
    	if  { ! [ winfo exist .passwd ] } {
			set ::passwdOk 0 
			after 100 "passwdBindReturn .passwd"
        	PasswdDlg .passwd -type okcancel -modal local \
				-loginhelptext $helptext -title $label \
				-passwdtextvariable $passwdvar \
				-logintextvariable $loginvar \
				-command "set ::passwdOk 1; destroy .passwd" \
				-padx 4 -pady 4 
			set login [ set $loginvar ]
			set passwd [ set $passwdvar ]
			if	{ $::passwdOk } {				
				if	{ ! [ string length $login ] || ! [ string length $passwd ] } {
					catch { unset ::passwdOk }
					error "No login/password provided for $::cmonClient::var(site)"
				}				
				if	{ [ string length $passwd ] } {
					if	{ $control } {
						set ::cntllogin $login
						set ::cntlpasswd $passwd
                        setupPasswdWidget
					} 
					set result "$login [ cmonClient::encryptUserPasswd $passwd ]"
				} 
			} else {
				set result cancelled
                if  { $control } {
                    catch { unset ::cntllogin }
                    catch { unset ::cntlpasswd }
                } else {
                    unset ::logintext
                    unset ::passwdtext
                }
                ;## reset back to no server if not connected
                if  { ! $::cmonClient::state } {
                    set ::cmonClient::var(site) $::cmonClient::NOSERVER
                }
			}
		}
    } err ] } {
		catch { unset $passwdvar }
		catch { unset $loginvar  }	
		return -code error $err
	}
	return -code ok $result
}


## ******************************************************** 
##
## Name: binaryEncrypt
##
## Description:
## simple encryption via rotation and xor
##
## Usage:
##
## Comments:
## news.comp.lang.tcl

proc binaryEncrypt {passphrase cleartext} {
    set r {}
    binary scan $passphrase c* l
    binary scan $cleartext c* d
    set pmax [llength $l]
    set cn 0
    foreach {c} $d {
        set cp [lindex $l $cn]
        append r [format %c [expr {($c & 0xff) + ($cp & 0xff)}]]
        incr cn
        if {$cn >= $pmax} { set cn 0 }
    }
    return $r
}

## ******************************************************** 
##
## Name: binaryDecrypt
##
## Description:
## simple encryption via rotation and xor
##
## Usage:
##
## Comments:
## news.comp.lang.tcl

proc binaryDecrypt {passphrase str} {
    set r {}
    binary scan $passphrase c* l
    binary scan $str c* d
    set pmax [llength $l]
    set cn 0
    foreach {c} $d {
        set cp [lindex $l $cn]
        append r [format %c [expr {($c & 0xff) - ($cp & 0xff)}]]
        incr cn
        if {$cn >= $pmax} { set cn 0 }
    }
    return $r
}


## ******************************************************** 
##
## Name: createPaneWin 
##
## Description:
## creates paned windows for the 3 sections of a page
## action, cmd status and display windows.
##
## Usage:
##
## Comments:

proc createPaneWin { parent { min2 80 } { min3 100 } } {
    set pw1   [PanedWindow $parent.pw1 -side right -weights available ]
    set pane2  [$pw1 add -minsize $min2 -weight 80]
    set pane3  [$pw1 add -minsize $min3 -weight 100]
    return [ list $pw1 $pane2 $pane3 ]
}

## ******************************************************** 
##
## Name: createStatus 
##
## Description:
## creates the command status window to display 
## errors from execution of request.
##
## Usage:
##
## Comments:

proc createStatus { parent } {

	set sw [ScrolledWindow $parent.swstatus -relief raised -borderwidth 0 -auto both]
    set subf [$sw getframe]
	set statusw $subf.status
    text $statusw 
	$sw setwidget $statusw
	bind $statusw <Button-3> "clearStatus %W"
	pack $sw -fill both -expand 1
	;## create red and blue tag for errors and info msgs
	$statusw configure -state disabled 

    foreach color $::TEXTW_COLORS {
        regsub {^::} $color {} colortag
	    $statusw tag configure $colortag -foreground $color
    }
    bind $statusw <Enter> { focus %W }
	return $statusw
}

## ******************************************************** 
##
## Name: createDisplay 
##
## Description:
## creates the display window to display 
## results from execution of request, usually in html.
##
## Usage:
##
## Comments:

proc createDisplay { parent } {

	    set sw [ScrolledWindow $parent.sw -relief sunken -borderwidth 2 -auto both]
        set subf [$sw getframe]
	    set textw $subf.text
	    text $textw -background $::textbg 
	    $sw setwidget $textw
	    pack $sw -fill both -expand yes
	    $textw configure -state disabled
	    $textw tag configure select -background yellow
	    bind $textw <Button-3> "clearDisplay %W"
        bind $textw <Enter> { focus %W }

	    return $textw
}

## ******************************************************** 
##
## Name: updateStatus 
##
## Description:
## updates the command status window with new response 
## options to beep and display text in color
##
## Usage:
##
## Comments:

proc updateStatus { statusw msg  { ringbell 0 } { color red } } {
	
	$statusw configure -state normal 
    $statusw delete 0.0 end 
	set origCursor [ $statusw cget -cursor ]
	$statusw config -cursor watch
	$statusw insert end $msg $color
	$statusw config -cursor $origCursor
	if	{ ! [ string length $msg ] && $ringbell } {
			bell 
	}
	$statusw configure -state disabled
}

## ******************************************************** 
##
## Name: appendStatus 
##
## Description:
## appends new response to the command status window  
## for history
##
## Usage:
##
## Comments:
## max size set to ::NUMLOGS

proc appendStatus { statusw msg { ringbell 0 } { color red } { popup 99 } } {
    set msglen [ string length $msg ]
	if	{ $msglen } {
        if  { $msglen > $::MAX_MSG_LEN } {
            set fname $::TMPDIR/statusmsg.txt
            set fd [ open $fname w ]
            puts $fd $msg
            close $fd 
            set msg "Unable to append message of size $msglen bytes, max is $::MAX_MSG_LEN bytes; saved in file $fname"
        }
		set seconds [ clock seconds ]
		set time [ clock format $seconds -format "%m-%d-%Y %H:%M:%S" ]
		$statusw configure -state normal -wrap word 
		set origCursor [ $statusw cget -cursor ]
		$statusw config -cursor watch
		set numLines [ $statusw index end ]
		regexp {(\d+).(\d+)} $numLines -> numLines 
		if	{ $numLines > $::NUMLOGS } {
			$statusw delete 0.0 [ expr $numLines - $::NUMLOGS ].0 
		}
		$statusw insert end "$::cmonClient::var(site),$time> $msg\n" $color
		$statusw config -cursor $origCursor
		$statusw configure -state disabled
        
        ;## this chokes with large status messages.
        $statusw see end
        
		if	{ [ string length $msg ] && $ringbell } {
			bell 
		}
   
		switch $color {
			red { set icon error }
			yellow { set icon warning 
            		if	{ $popup == 99 } {
            			if	{ [ info exist ::POPUP_SHOW_WARN ] } {
                			set popup $::POPUP_SHOW_WARN
                        }
                	}            
            	}
            blue { set popup 0 } 
            default { set icon info
            		if	{ $popup == 99 } {
            			if	{ [ info exist ::POPUP_SHOW_INFO ] } {
                			set popup $::POPUP_SHOW_INFO
                        }
                	}     
                
                 }       
		}		
		
		if	{ $popup } {
			set win $statusw$seconds
			set image [ Bitmap::get $icon ]
			if	{ ! [ winfo exist $win ] } {
				set dlg [ Dialog $win -parent . -modal none \
          		-separator 1 -title  $icon \
        		-side bottom -anchor  s -default 0 -image $image ]
        		$dlg add -name ok -anchor s -command [ list destroy $dlg ]
				set dlgframe [ $dlg getframe ]
				message $dlgframe.msg -aspect 300 -text $msg \
					-justify left -font $::MSGFONT
				pack $dlgframe.msg -side top 
                catch { grab set $dlg }
				$dlg draw 
				after 15000 [ list destroy $dlg ]
			}
		} 
	}
}

## ******************************************************** 
##
## Name: updateDisplay
##
## Description:
## update display window with new html  
##
## Usage:
##
## Comments:

proc updateDisplay { textw html } {

	set origCursor xterm
	if	{ [ catch {
		    if	{ ! [ string length $html ] } {
			    set html "<html>"
		    }
		    set origCursor [ $textw cget -cursor ]
		    $textw config -cursor watch
            #__t::start
		    set seqpt "html::callback $textw $html"
		    html::callback $textw $html
            #set wallsecs [ __t::mark ]
            #puts "rendering html took $wallsecs seconds"
	} err ] } {	
		puts "updateDisplay error: $err"
	}
	$textw config -cursor $origCursor
}



## ******************************************************** 
##
## Name: createRepeatFreq
##
## Description:
## creates the repeat and Freq combo boxes as a unit
##
## Parameters:
##
proc createRepeatFreq { parent name page } {

	frame $parent.fcombo -relief ridge -borderwidth 3
	set fparent [ frame $parent.fcombo.frepeat ]
    
	set lab [ label $fparent.lab -text "Repeat " \
			-width 8 -justify left -anchor w ]
	
    set combo1 [ComboBox $fparent.crepeat \
                   -width 5 \
                   -textvariable ::${name}::var\($page,repeat\) \
                   -values [set ::${name}::repeat ] \
                   -helptext "Number of times to refresh"   \
				   -editable no \
                   -modifycmd "setCombo $fparent.crepeat" ]
	pack $lab -side left -anchor w

	label $fparent.lbl1 -text times -justify left -anchor w
	pack $combo1 $fparent.lbl1 -side left 
	set units [ set ::${name}::freqUnits ] 
	set fparent [ frame $parent.fcombo.ffreq ]
	set lab [ label $fparent.lab -text "every " \
			-width 8 -justify left -anchor w ]
	set combo2 [ComboBox $fparent.cfreq \
                   -width 5 \
                   -textvariable ::${name}::var\($page,freq\) \
                   -values [ set ::${name}::freq ] \
				   -editable no \
                   -helptext "Repeat every $units"  \
                   -modifycmd "setCombo $fparent.cfreq" ]
	pack $lab -side left -anchor w
	label $fparent.lbl1 -text $units -justify left -anchor w
	pack $combo2 $fparent.lbl1 -side left 
    $combo1 setvalue first
    $combo2 setvalue first
	pack $parent.fcombo.frepeat $parent.fcombo.ffreq -side top -fill x
	pack $parent.fcombo -side top -expand 1 -fill both
	set ::${name}::repeatw($page) $combo1
    set ::${name}::freqw($page) $combo2  
	set ::${name}::var($page,repeat) [ lindex [ set ${name}::repeat ] 0 ]
    set ::${name}::var($page,freq) [ lindex [ set ${name}::freq ] 0 ]  
    	
}
## ********************************************************

## ******************************************************** 
##
## Name: createAPICheckList
##
## Description:
## creates a checkbox for APIs
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc createAPICheckList { parent name page } {

	frame $parent.flab -relief ridge -borderwidth 2
	set lab [ label $parent.flab.lbl -text "Select one or more categories: "  \
		-justify left -font $::LISTFONT -wraplength 300 ]
	set selectmenu [ eval tk_optionMenu $parent.flab.select ::${name}::var($page,select) \
        { "select all" "deselect all" } ]
	$selectmenu entryconfigure 0 -command [ list APIselect $name $page 1 $lab ]
	$selectmenu entryconfigure 1 -command [ list APIselect $name $page 0 $lab ]
	pack $parent.flab.select -side right
	pack $parent.flab.lbl -side left -fill x -expand 1
	pack $parent.flab -side top -fill both -expand 1
	#set sw [ScrolledWindow $parent.sw -relief sunken -borderwidth 2]
	#set sf [ScrollableFrame $sw.fapi -height 10 -areaheight 0 ]
	#$sw setwidget $sf
	#set subfapi [ $sf getframe ]
	
	set subfapi [ frame $parent.fapi -relief ridge -borderwidth 1 ]
	
	set apilist [ set ::${name}::APIs ]
	set numAPIs [ llength $apilist ]
	set apirows [ list ]
	set itemsRow 3 
	for { set i 0 } { $i < $numAPIs } { incr i $itemsRow } {
		if	{ [ catch {
			set apirow$i [ lrange $apilist $i [ expr $i + 2 ] ]
		} err ] } {
			set apirow$i [ lrange $apilist $i end ]
		}
		lappend apirows apirow$i
	}
	foreach apirow $apirows {
		set f [ frame $subfapi.f$apirow ]
		foreach api [ set $apirow ] {
			checkbutton $f.$api -text $api -width 12 -anchor w \
        	-variable ::${name}::var($page,$api) \
        	-font $::LISTFONT -onvalue 1 -offvalue 0 
			set ::${name}::var($page,$api) 0
			pack $f.$api -side left -anchor w
		}
		pack $f -side top -anchor w
	}
	pack $subfapi -fill both -expand 1 
	set ::${name}::var($page,api) datacond
	#pack $sw -anchor w -fill both -expand 1 -padx 2	
}

## ******************************************************** 
##
## Name: APIselect 
##
## Description:
## turn on or off API checkboxes
## update the title to reflect this 
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc APIselect { name page value { titlew "" } } {

	foreach api [ set ::${name}::APIs ] {
		set ::${name}::var($page,$api) $value
	}
	#setAPItitle $page $name $titlew	
}

## ******************************************************** 
## 
## Name: setAPIs 
##
## Description:
## format an API string for selected APIs
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##    
 
proc setAPIs { page name } {
    set apilist {}
	set apiset [ set ::${name}::APIs ]
    foreach api $apiset {
        if  { [ set ::${name}::var($page,$api) ] } {
            lappend apilist $api
        }
    }
	set ::${name}::var($page,apilist) $apilist
    return $apilist
}

## ******************************************************** 
##
## Name: setAPItitle 
##
## Description:
## update title of the display window when an API has been selected
## for its log
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc setAPItitle { page name titlew } {
    
	set apilist [ setAPIs $page $name ] 
	if	{ [ llength $apilist ] } {
    	$titlew configure -text " Selected [join $apilist ", " ] APIs"
	} else {
		$titlew configure -text "Please select one or more APIs"
	}
}

## ******************************************************** 
##
## Name: updateRepeat 
##
## Description:
## disable or enable repeat and frequency combo box 
##
## Usage:
##
## Comments:
## sets default command

proc updateRepeat { name page { state normal } } {
    set repeatw [ set ${name}::repeatw($page) ]
    $repeatw configure -state $state
    set freqw [ set ${name}::freqw($page) ]
    $freqw configure -state $state
}

## ******************************************************** 
##
## Name: updateWidgets
##
## Description:
## update selectable widgets to stop further user action 
##
## Usage:
##
## Comments:
## this proc is recursive if there is children found for a child

proc updateWidgets { parent state } {

   foreach { child } [ winfo children $parent ] {
        if  { [ llength [ winfo children $child ] ] } {
            catch { updateWidgets $child $state } err
        } else {
			set class [ winfo class $child ]
            if  { [ regexp -nocase {button|entry} $class ] } {
                $child configure -state $state
            }
		;## just in case we have options menu
			if  { [ regexp -nocase {menu} $class ] } {
				if	{ [ regexp {(.+).menu$} $child match name ] } {
					catch { $name configure -state $state } err
				}
			}				
        }
   }
}

## ******************************************************** 
##
## Name: clearStatus
##
## Description:
## clear text window lines via popup button
##
## Parameters:
## button-3 in text window invokes the button
## button-1 on button clears
## button-3 again on button removes button

proc clearStatus { w } {
	set b ${w}Clear 
	if	{ [ winfo exist $b ] } { 
		destroy $b 
	} else { 
		button $b -text "Clear" -width 6 -command "updateStatus $w \"\"; destroy $b"
		pack $b 
		place $b -in $w -relx 0.5 -rely 0.5 -anchor center
	}
}

## ******************************************************** 
##
## Name: clearDisplay
##
## Description:
## clear text window html
##
## Parameters:
## button-3 in text window invokes the button
## button-1 on button clears
## button-3 again on button removes button

proc clearDisplay { w } {
	set b ${w}Clear 
	if	{ [ winfo exist $b ] } { 
		destroy $b 
	} else { 
		button $b -text "Clear" -width 6 -command "updateDisplay $w <html>; destroy $b"
		pack $b 
		place $b -in $w -relx 0.5 -rely 0.5 -anchor center 
	}
}

## ******************************************************** 
##
## Name: parseURL
##
## Description:
## Parse URL style strings which might, for example, be used
## as the -returnprotocol option to a user command, and return
## the protocol, the target, and an optional port.
##
## Parameters:
##
## Usage:
## The URL is parsed as follows:
##
## example 1:
## http://foo.bar.edu:8080/baz/bim/buz.html
##
## protocol - http
## targ1 - foo.bar.edu
## port  - 8080
## targ2 - /baz/bim/buz.html 
##
## example 2:
## http://foo.bar.edu/baz/bim/buz.html
##
## protocol - http
## targ1 - foo.bar.edu/baz/bim/buz.html
##
## port and targ2 in this case are null strings.
## so to recover the full URL target when a port is
## given:
##
## ${targ1}:$port$targ2
## 
## will always work.
##
## Comments:
## This function does NOT verify the validity of the
## target, the function which maps to the protocol
## must do that.

proc parseURL { { url "" } } {
     set protocol {}
     set targ1    {}
     set targ2    {}
     set port     {}
     regexp {^\{(.*)\}$} $url -> url
     regexp {^\"(.*)\"$} $url -> url
     set url [ string trim $url ]
     
     set url_rx {^(file|http|mailto|ftp|port):/*([^ :]+):?(([0-9]+)(\/[^ ]+)?)?$}
     
     if { [ regexp $url_rx $url -> protocol targ1 -> port targ2 ] } {
        switch -exact -- $protocol {
           file {
                set targ1 "/$targ1"
                }
           http {
                }
         mailto {
                set port {}
                }
            ftp {
                }
           port {
                   if { ! [ string length $targ1 ] } {
                      return -code error "parseURL: bad URL: '$url'"
                   }
                }
        default {
                return -code error "parseURL: bad URL: '$url'"
                }
        } ;## end of switch    
     } else {
        return -code error "parseURL: bad URL: '$url'"
     }
     
     return [ list $protocol $targ1 $port $targ2 ]
}
## ******************************************************** 
	
## ******************************************************** 
##
## Name: httpGetFile
##
## Description:
## wrapper for http package function http::get to
## transfer files to ldas
##
## Parameters:
## target -  url
## channel - output file fname
## timeout - 
## Usage:
##
## Comments:

proc httpGetFile { target outfile { timeout 0 } } {
     set logmsg [ list ]
     if { ! $timeout } {
        if { ! [ info exists ::HTTP_TIMEOUT ] } {
           return -code error "[ myName ]: ::HTTP_TIMEOUT not defined in LDASapi.rsc"
        }   
        set timeout $::HTTP_TIMEOUT
     }
     if { [ catch {
        set channel [ open $outfile w 0664 ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     if { [ catch {
        set token [ list ]
        set token [ http::geturl $target -channel $channel -timeout $timeout ]
        if { [ string length $token ] } {
           upvar #0 $token state
        }   
     } err ] } {
        if { [ catch {
           set token [ list ]
           after 2000
           set token [ http::geturl $target -channel $channel -timeout $timeout ]
           if { [ string length $token ] } {
              upvar #0 $token state
           }   
        } err2 ] } {
           catch { close $channel }
           if { [ string length $token ] } {
              upvar #0 $token state
           }   
           if { [ info exist state(error) ] } {
              lappend err2 "tcl_http_state: [ parray state ]"
           }   
        }        
        
        catch { http::cleanup $token } 

        return -code error "[ myName ]:(first try error: $err) (second try error: $err2)"
     } ;## end of outer catch
     
     catch { close $channel }
     
     if { [ catch {   
        
        set rfc 1000
        
        foreach { id rfc } $state(http) { break }
        
        switch $rfc {
            100 { set retval "Continue" }
            101 { set retval "Switching Protocols" }
            200 { set retval "OK" }
            201 { set retval "Created" }
            202 { set retval "Accepted" }
            203 { set retval "Non-Authoritative Information" }
            204 { set retval "No Content" }
            205 { set retval "Reset Content" }
            206 { set retval "Partial Content" }
            300 { set retval "Multiple Choices" }
            301 { set retval "Moved Permanently" }
            302 { set retval "Moved Temporarily" }
            303 { set retval "See Other" }
            304 { set retval "Not Modified" }
            305 { set retval "Use Proxy" }
            400 { set retval "Bad Request" }
            401 { set retval "Unauthorized" }
            402 { set retval "Payment Required" }
            403 { set retval "Forbidden" }
            404 { set retval "Not Found" }
            405 { set retval "Method Not Allowed" }
            406 { set retval "Not Acceptable" }
            407 { set retval "Proxy Authentication Required" }
            408 { set retval "Request Timeout" }
            409 { set retval "Conflict" }
            410 { set retval "Gone" }
            411 { set retval "Length Required" }
            412 { set retval "Precondition Failed" }
            413 { set retval " Request Entity Too Large" }
            414 { set retval "Request-URI Too Long" }
            415 { set retval "Unsupported Media Type" }
            500 { set retval "Internal Server Error" }
            501 { set retval "Not Implemented" }
            502 { set retval "Bad Gateway" }
            503 { set retval "Service Unavailable" }
            504 { set retval "Gateway Timeout" }
            505 { set retval "HTTP Version Not Supported" }
            1000 { set retval "Failed To Connect, Timed Out" }
            default {
                    set retval "Unknown Return Value: '$rfc'"
                    set rfc 2000
                    }
        }
        
        set logmsg "HTTP-1.1: $rfc $retval"
        
        http::cleanup $token

        if { $rfc > 202 } {
           return -code error $logmsg
        }
        
     } err ] } {
        
        return -code error "[ myName ]: $err"         
    }

    return $logmsg
}
## ********************************************************

## ******************************************************** 
##
## Name: getUrl 
##
## Description:
## Retrieve html from the given url.
##
## Parameters:
## url - url e.g. http://www.ligo.caltech.edu/~mlei/Run_LDAS.html
##
## Usage:
##  set html [ getUrl $url ]
##
## Comments:
## Returns the url object 
##
proc getUrl { { url "" } } {

     set url [ lindex $url 0 ] 
     
     if { [ catch {
        foreach { protocol t1 port t2 } [ parseURL $url ] { break }
        switch -exact -- $protocol {
           http {
                set tempfile html.tcl.[ expr rand() ]
                set tempfile [ file join $::TMPDIR $tempfile ]  
                set msg [ httpGetFile $url $tempfile ]
                set fid [ open $tempfile r ]
                set text [ read $fid [ file size $tempfile ] ]
                close $fid
                file delete -- $tempfile
                }
        default {
                return -code error "getUrl: $protocol not supported"
                }
        } ;## end of switch
     } err ] } {
        return -code error "getUrl: $err"
     }
     set text
}
## ********************************************************

## ******************************************************** 
##
## Name: createNodeWidget
##
## Description:
## creates the node widget given the set of nodes
##
## Parameters:
## list of nodes
##
## Usage:
##  set nodeW [ createNodeWidget $nodelist ]
##
## Comments:
## create 1st level cascade button for 1-100, 101-109, ...
## create 2nd level cascade button for 1-10, 11-19, ...
## create 3rd level radio button for individual nodes, 10 per cascade

proc createNodeWidget { parent var { nodelist none } { updateW "" } } {
	set numNodes [ llength $nodelist ] 
	catch { destroy $parent.mb }
	set mb [ menubutton $parent.mb -text "Select node" -menu $parent.mb.menuA \
		-relief raised -borderwidth 2 -font $::SMALLFONT ]
	set m1Nodes [ menu $mb.menuA -tearoff 0 ]
	set eachround 100 
	set $var none
	for { set i 0 } { $i < $numNodes } { incr i $eachround } {
		set remNodes [ expr $numNodes - $i ]
		if	{ $remNodes <= $eachround } {
			set eachround $remNodes
		}	
		set end [ expr $i+99 ]
		set text "Nodes $i - $end"
		$m1Nodes add cascade -label $text -menu $text \
			-menu $m1Nodes.sub$i	
		set m2Nodes [ menu $m1Nodes.sub$i -tearoff 0 ]
	
		set eachround2 10
		for { set j 0 } { $j < $eachround } { incr j $eachround2 } {
			set remNodes2 [ expr $eachround - $j ]
			if	{ $remNodes2 <= $eachround2 } {
				set eachround2 $remNodes2
			}
			$m2Nodes add cascade -label "Nodes [ expr $i+$j ] - [ expr $i+$j+9 ]" \
			-menu "Nodes $j - [ expr $j + 9 ]" \
			-menu $m2Nodes.sub$j	
			set m3Nodes [ menu $m2Nodes.sub$j -tearoff 0 ]
			set eachround3 1
			for { set k 0 } { $k < $eachround2 } { incr k $eachround3 } {
				set remNodes3 [ expr $eachround2 - $k ]
				if	{ $remNodes3 <= $eachround3 } {
					set eachround3 $remNodes3
				}	
				set index [ expr $i + $j + $k ]
				set nodeName [ lindex $nodelist $index ]
				$m3Nodes add radio -label $nodeName -variable $var -value $nodeName \
					-command "setNodeName $updateW $var"
			}
		}
	}
	return $mb
}

## ******************************************************** 
##
## Name: dumpWidgets
##
## Description:
## dump widget tree
##
## Usage:
##
## Comments:
## this proc is recursive if there is children found for a child

proc dumpWidgets { parent } {

   foreach { child } [ winfo children $parent ] {
        if  { [ llength [ winfo children $child ] ] } {
            catch { dumpWidgets $child } err
			puts "err $err"
        } 			
   }
}

## ******************************************************** 
##
## Name: scrolledWindow_yscroll
##
## Description:
## dump widget tree
##
## Usage:
##
## Comments:
## this proc is recursive if there is children found for a child

proc scrolledWindow_yscroll { sw } {

   set grid [ winfo children $sw ]   
   foreach { hscroll vscroll } [ winfo children $sw ] { break }
   return "$hscroll $vscroll" 
}

proc setNodeName { widget var } {
	$widget configure -text [ set $var ]
}

proc BindXview { lists args } {
	foreach l $lists {
		eval { $l xview } $args 
	}
}

bind all <Control-c> { destroy %W }

;## default is click-to-type model
;## the following causes a follow mouse type model
;## tk_focusFollowsMouse

## ******************************************************** 
##
## Name: ckNewServer
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:
## 1 for new server, 0 for old 

proc ckNewServer { version } {

	set version [ split $version . ]
	set i 0
	foreach num $version {
		set servernum [ lindex $::serverVersionF $i ]
		if	{ $servernum > $num } {
			return 1
		} elseif { $servernum < $num } {
			return 0
		}
		incr i 1
	}
	;## equal version #
	return 1
}

## ******************************************************** 
##
## Name: resetPasswdWidget
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:

proc resetPasswdWidget {} {
    $::passwdLabel configure -image $::redoImage -bg orange 
    set ::passwdRemain 0   
    if  { [ info exist ::passwdAfterId ] } {
        after cancel $::passwdAfterId
        unset ::passwdAfterId
    }
}

## ******************************************************** 
##
## Name: setupPasswdWidget
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:

proc setupPasswdWidget {} {

    if  { $::passwdRemain > 0 } {
        return
    }
    $::passwdLabel configure -image $::passwdImage -bg green  
    set ::passwdRemain $::LOCKTIMEOUT
    if  { ! [ info exist ::passwdAfterId ] } {
        set ::passwdAfterId [ after $::LOCK_INTERVAL updatePasswdWidget ]
    }
}

## ******************************************************** 
##
## Name: setupGlobusPasswdWidget
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:

proc setupGlobusPasswdWidget { timeleft } {

    if  { $::passwdRemain > 0 } {
        return
    }
    $::passwdLabel configure -image $::passwdImage -bg green
    set ::passwdRemain [ expr round($timeleft/60) ]
    debugPuts "passwdRemain $::passwdRemain"
    $::passwdPrg configure -maximum $::passwdRemain
    if  { ! [ info exist ::passwdAfterId ] } {
        set ::passwdAfterId [ after $::LOCK_INTERVAL updateGlobusPasswdWidget ]
    }
}

## ******************************************************** 
##
## Name: updatePasswdWidget
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:
## only time out control user password

proc updatePasswdWidget {} {

    incr ::passwdRemain -[ expr $::LOCK_INTERVAL/60000 ]
    if  { $::passwdRemain > 0 } {
        set ::passwdAfterId [ after $::LOCK_INTERVAL updatePasswdWidget ]
    } else {
        $::passwdLabel configure -image $::redoImage -bg orange
        foreach var [ list cntllogin cntlpasswd ] {
            if  { [ info exist ::$var ] } {
                unset ::$var
            }
        }
        resetPasswdWidget
        catch { unset ::passwdAfterId }
        set ::passwdRemain 0
    }
}

## ******************************************************** 
##
## Name: updateGlobusPasswdWidget
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:
## only time out control user password

proc updateGlobusPasswdWidget {} {

    incr ::passwdRemain -[ expr $::LOCK_INTERVAL/60000 ]
    if  { $::passwdRemain > 0 } {
    	;## check if Proxy is still valid 
        foreach { rc data } [ getProxyInfo ] { break }
        if	{ ! $rc } {
        	set ::passwdAfterId [ after $::LOCK_INTERVAL updateGlobusPasswdWidget ]
            return
        } 
    } else {
    	set ::passwdRemain 0
	;## proxy has expired or no longer exit 
    	resetPasswdWidget 
    	foreach var [ list cntllogin cntlpasswd ] {
    		if  { [ info exist ::$var ] } {
       	 		unset ::$var
            }
        }
    }
}

## ******************************************************** 
##
## Name: createPasswdWidget
##
## Description:
## determine if the server is old or new for backward compatibility
##
## Parameters:
## version var
##
## Usage:
## 
## Comments:

proc createPasswdWidget { parent } {

    set ::passwdRemain -1
    set frame [frame $parent.f -background white -relief sunken -borderwidth 1  ]
    set ::redoImage [ image create photo -file [ file join $::GIFDIR lock.gif ] ]
    set ::passwdImage   [ image create photo -file [ file join $::BWIDGET::LIBRARY images passwd.gif ] ]
    set ::passwdLabel  [Label $frame.lab1 -image $::redoImage -relief groove -bd 1 -width 25 -height 25 \
        -bg orange -helptext "priviledged user password cached in memory indicator" ]
    set ::passwdPrg   [ProgressBar $frame.prg -width 30 -height 8 -bd 1 -troughcolor orange \
                -relief ridge -variable ::passwdRemain -fg green -bg orange -maximum $::LOCKTIMEOUT ]

    pack $::passwdLabel $::passwdPrg -side top -fill both        
    return $frame
}


## ******************************************************** 
##
## Name: checkPasswdError
##
## Description:
## determine from server msg whether there is a password error
##
## Usage:
##
## Comments:

proc checkPasswdError { msg } {
    if	{ [ regexp -nocase {(Invalid|incorrect)\s+(login|password)} $msg ] ||
          [ string first "not authorized" $msg ] > -1  } {
        set ::passwdRemain 0
        updatePasswdWidget
        return 1
    }
    return 0
}

## ******************************************************** 
##
## Name: createTextWin
##
## Description:
## create a text window for graph info
##
## Usage:
##
## Comments:

proc createTextWin { parent height { fill x } { expand 0 } } {

	set sw [ScrolledWindow $parent.sw -relief raised -borderwidth 0 -auto both]
    set subf [$sw getframe]
	set statusw $subf.status
    text $statusw -height $height -bg PapayaWhip -wrap word -tabs "0.5i left" 
	$sw setwidget $statusw
	pack $sw -fill $fill -expand $expand
	;## create red and blue tag for errors and info msgs
	$statusw configure -state disabled 
    
    foreach color $::TEXTW_COLORS {
        regsub {^::} $color -> colortag
	    $statusw tag configure $colortag -foreground $color
    }
    $statusw tag configure bold     -font { times 12 bold }
    $statusw tag configure italic   -font { times 12 italic }
    $statusw tag configure fixed    -font { helvetica 10 bold }
    $statusw tag configure courier  -font { courier 10 bold }
    $statusw tag configure underline -underline true
    $statusw tag configure super -offset 6 -font { helvetica 8  }
    $statusw tag configure sub   -offset 6 -font { helvetica 8  }
    bind $statusw <Enter> { focus %W }
	return $statusw
}

## ******************************************************** 
##
## Name: updateTextWin
##
## Description:
## add text with tags to text window
##
## Usage:
##
## Comments:

proc updateTextWin { textw data } {
    
    $textw configure -state normal 
    $textw delete 0.0 end 
    foreach { text taglist } $data {
        $textw insert end $text $taglist
    }
	$textw configure -state disabled
}


## ******************************************************** 
##
## Name: bak
##
## Description:
## adapted from genericAPI
## touches the file to reserve the filename
##
## via ssh
## Usage:
##       
##
## Comments:

proc bak { fname { levels 10 } } {
     if { [ catch {
        if { [ file exists $fname ] } {
           set dir [ file dirname $fname ]
           set files [ glob -nocomplain $dir ${fname}.ba* ]
           set i $levels
           while { [ incr i -1 ] } {
              if { [ lsearch $files ${fname}.ba$i ] > -1 } {
                 file rename -force ${fname}.ba$i ${fname}.ba[ incr i ]
                 incr i -1
              }
           }
           if { [ file exists ${fname}.bak ] } {
              file rename -force ${fname}.bak ${fname}.ba2
           }
           file rename -force $fname ${fname}.bak
        } else {
	   		catch { touch $fname }
		}	
     } err ] } {
        return -code error "bak($fname $levels): $err"
     }
}  
   
## ******************************************************** 
##
## Name: ckPriviledged
##
## Description:
## check if user is a priviledged user for globus sockets
##
## Usage:
##   returns 1 -  priviledged user   
##
## Comments:

proc ckPriviledged {} {

	uplevel {
		if	{ [ string equal ldas $::RUNMODE ] } {
			set login ldas
		} elseif  { $::USE_GLOBUS } {
        	if	{ ! [ string equal priviledged $::globus_control_status ] } {
            	error "cmonClient You are not authorized for this function."
            }           	
    	}
    }
}



;## the following procs for getting a proxy are supported only if
;## tclglobus is used 

## ******************************************************** 
##
## Name: initProxy
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc initProxy {} {

	set data ""
	if	{ [ string length $::passphrase ] } {
		set fd [ ::open .info w ]
        puts $fd $::passphrase
        ::close $fd 
        set progpath [ auto_execok grid-proxy-init ]
        if	{ ![ string length $progpath ] } {
            return -code error  "grid-proxy-init not found; unable to verify proxy"
    	}
        set cmd "exec grid-proxy-init -debug -verify -pwstdin < .info"
        catch { eval $cmd } data
        debugPuts "grid-proxy-init data '$data'"
        file delete -force .info
    	if	{ [ regexp -nocase {ERROR:} $data ] } {
        	return -code error $data
        } else {
        	return $data
        }
    } else {
    	return -code error "No passphrase was entered to create a valid proxy."
    }
}

## ******************************************************** 
##
## Name: initProxyDialog
##
## Description:
## dialog to get passphrase to allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc initProxyDialog {} {      

	if	{ [ catch {
    	set ::passphrase ""
        catch { unset ::doinitProxy }
		set dlg [ Dialog .proxyDialog -parent . -modal local \
          	-separator 1 -title   "Enter/re-enter (if incorrect) passphrase for your proxy: " \
        	-side bottom -default 0 -cancel 2 ]
        set dlgframe [$dlg getframe]
        
        $dlg add -name 0 -text ACCEPT -command "set ::doinitProxy 1; destroy $dlg"   
        $dlg add -name cancel -command "destroy $dlg" 
        set passwidget  [ LabelEntry $dlgframe.passw -label "Your pass phrase: " -labelwidth 20 -labelanchor e \
                   -textvariable ::passphrase -editable 1 -width 50 -labeljustify right \
                   -helptext "The passphrase associated with your proxy" -show *]
        pack $passwidget -side top -fill x -expand 1
        $dlg draw $passwidget.e 
        if	{ [ info exist ::doinitProxy ] } {
        	set rc [ initProxy ]
        } 
    } err ] } {
    	return -code error $err
    }
}
 
## ******************************************************** 
##
## Name: getProxyInfo
##
## Description:
## check on user proxy
##
## Usage:
##
## Comments:

proc getProxyInfo {} {

	if	{ [ catch {
    	set rc 0
        set progpath [ auto_execok grid-proxy-init ]
        if	{ ![ string length $progpath ] } {
            error  "grid-proxy-init not found; unable to verify proxy"
    	}
    	catch { exec grid-proxy-info -identity -timeleft } data
        if	{ [ regexp {ERROR.+find a valid proxy} $data ] } {
        	set rc 1
        } 
        if	{ [ regexp {\n-1} $data ] } {
            set data "Your proxy has expired"
            set rc 1
        }
 	} err ] } {
    	return -code error $err
    }
    return [ list $rc $data ]
}
            
        
## ******************************************************** 
##
## Name: verifyProxy
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc verifyProxy { { display 1 } } {

    set msg ""
	if	{ [ string equal ldas $::RUNMODE ] } {
    	set ::globusUser ldas
        set ::passphrase x.509        
    	return
    }
    if	{ [ catch { 		
        foreach { rc data } [ getProxyInfo ] { break }
    	if	{ $rc } {
        	set rc [ initProxyDialog ]
            if	{ [ info exist ::doinitProxy ] } {
            	foreach { rc data } [ getProxyInfo ] { break }
                debugPuts "from getProxyInfo  rc=$rc, data =$data"
                if	{ $rc } {
                	error $data 
                }
            }
        } else {
        	set ::doinitProxy 1
            resetPasswdWidget
        }
        
        ;## if user has not cancel, check the time left on proxy		
        if	{ [ info exist ::doinitProxy ] } {
			if	{ [ regexp  {CN=([^\d]+)[^\n]+\n(\d+)} $data -> username timeleft] } {
				regsub -all {\s+} $username "_" username
        		set ::globusUser [ string trim $username _ ]
        		debugPuts "user $::globusUser, timeleft $timeleft"
			}

    		if	{ ![ string length $timeleft ] } {
				error "grid-proxy-info failed for -timeleft option."
			} elseif { $timeleft > 60 } {                
        		set walltime [ clock format [ expr $timeleft + [ clock seconds ] ] -format "%m-%d-%Y %H:%M:%S" ]
    			set msg "This proxy is valid until $walltime."
                setupGlobusPasswdWidget $timeleft
                if  { $display } {
                    set ack [ tk_messageBox -type ok -icon info -message $msg ]
                }
    		}
        } else {
        	set ::globusUser cancelled
        }
   	} err ] } {  
        if	{ [ regexp -nocase -- {Bad passphrase} $err ] } {
            set message "You have entered a bad passphrase."
        } else {
            set message "Error: $err"
        }
        if  { $display } {
            set ack [ tk_messageBox -icon warning -title "X509 Proxy error" \
        	    -message $message ]
        }
    	catch { unset ::passphrase } 
        set ::globusUser cancelled
        return -code error $message
    }
    if  { $display } {
        set ack [ tk_messageBox -type ok -icon info -message $msg ]
    }
    return $msg
}

## ******************************************************** 
##
## Name: verifyProxyWithRetry
##
## Description:
## verify user proxy and allows up to 3 times for entering
## correct passphrase
## 
## Usage:
##
## Comments:
## avoid using recursion in verifyProxy

proc verifyProxyWithRetry { { display 1 } { retry 3 } } {

    set err Error
    set rc none
    for { set i 0} { $i < $retry } { incr i } {
        if  { [ catch {
            set rc [ verifyProxy 0 ]
            set saved_i $i
            set i $retry
        } err ] } {
        puts stderr "err $err [ regexp -nocase -- {Bad passphrase} $err ]"
            if	{ ! [ regexp -nocase -- {Bad passphrase} $err ] } {
                return -code error $err
            }
        }                
    }
    
    if  { ! [ info exist saved_i ] } {
        set ack [ tk_messageBox -icon warning -title "X509 Proxy error" \
        	    -message $err ]
        return -code error $err
    } else {
        if  { $display } {
            set ack [ tk_messageBox -type ok -icon info -message $rc ] 
        }
    }
}

## ******************************************************** 
##
## Name: parseGlobusError
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc parseGlobusError { error } {

	if	{ [ regexp -nocase -- {(Bad|No) passphrase} $error ] } {
    	return [ list 1 "You did not enter a valid passphrase." ]
    }
    return [ list 0 $error ]
}

## ******************************************************** 
##
## Name: showWarning
##
## Description:
## display warn or info by user preference
##
## Usage:
##
## Comments:
## cancel is 1, ok is 0
proc showWarning { msg type { title Warning } } {

	if	{ [ catch {
    	if	{ $::POPUP_SHOW_WARN } {
    		set ack [ MessageDlg .cmonClient[clock seconds] -title $title \
			-type $type -aspect 500 -message $msg -icon warning  -justify left -font $::MSGFONT ]
        } else {
        	set ack 0
        }
    } err ] } {
    	return -code error $err
    }
    return $ack
}
       
## ******************************************************** 
##
## Name: showInfo
##
## Description:
## display warn or info by user preference
##
## Usage:
##
## Comments:
## ok is 0

proc showInfo { msg { title Info } } {

	if	{ [ catch {
    	if	{ $::POPUP_SHOW_INFO } {
    		set ack [ MessageDlg .cmonClient[clock seconds] -title $title \
			-type ok -aspect 500 -message $msg -icon info  -justify left -font $::MSGFONT ]
        } else {
        	set ack 0
        }
    } err ] } {
    	return -code error $err
    }
    return $ack
}

## ******************************************************** 
##
## Name: touch
##
## Description:
## reserve a file similar to touch shell command but does not 
## need to do exec 
##
## Parameters: 
## file name
##
##
## Usage:
##  
## 
## Comments:

proc touch { filename } {

	if	{ [ catch {
    	set fd [ open $filename w ]
        close $fd
    } err ] } {
    	return -code error "failed to touch $filename: $err"
    }
}
