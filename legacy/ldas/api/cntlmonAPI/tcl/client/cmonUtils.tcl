## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles the startup/shutdown of LDAS APIs
## and control of tape controller at Hanford.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
## ******************************************************** 

package provide cmonUtils 1.0

namespace eval cmonUtils {
	array set helptext { logscan { logscan monitors LDAS logs and \
	sends out email notification when it encounters mail.gif entries } }
	array set helptext { db2utils { db2utils monitors database usage \
	and creates the LDAS database web pages } }
	array set helptext { coreWatch { coreWatch monitors and archives \
	core files } }
	array set helptext { makeFtpDirectory { makeFtpDirectory creates \
	the named subdirectory for anonymous FTP under /ldas_outgoing } }
	array set helptext { diskcache { disk cache caches frame files for \
	servicing frame requests } }
	array set helptext { dbCompare { check differences among databases } }
	set pages [ list ]
    array set helptext { createdb { creates/recreates database \
    old database will be deleted } }
    array set helptext { logCacheUtils { creates log cache from archiveIndex \
    for log filter } }
	set pages { Utilities }
}

## ******************************************************** 
##
## Name: cmonUtils::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonUtils::create { notebook } {

    set page [ lindex $::cmonUtils::pages 0 ]
    set ::cmonUtils::prevsite($page) none
    $notebook insert end cmonUtils -text Utilities \
        -createcmd "cmonUtils::createPages $notebook $page" \
        -raisecmd [ list cmonCommon::pageRaise cmonUtils $page ]
}

## ******************************************************** 
##
## Name: cmonUtils::createPages
##
## Description:
## creates all status pages
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonUtils::createPages { notebook page } {

	set frame [ $notebook getframe cmonUtils ]

	set topf  [frame $frame.topf]

    specific $topf $page	
	set ::cmonUtils::topfw($page) $topf
	pack $topf -fill x -pady 2
	
	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	
	;## create a status to display errors and status
	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonUtils::statusw [ list $page $statusw ]
	pack $titf2 -pady 2 -padx 4 -fill both -expand yes
	
	;## create display
	set titf3 [TitleFrame $pane3.titf3 -text $page -font $::ITALICFONT ]
	set ::cmonUtils::titf3($page) $titf3 
    cmonCommon::pageRaise cmonUtils $page
    
	set textw [ ::createDisplay [ $titf3 getframe ] ]
    array set ::cmonUtils::textw [ list $page $textw ]
	pack $titf3 -pady 2 -padx 4 -fill both -expand yes 
	pack $pw1 -side top -fill both -expand 1

	;## change this when we have true html
	$textw configure -bg PapayaWhip
    array set ::cmonUtils::state [ list $page 0 ]
    set ::cmonUtils::prevsite($page) $::cmonClient::var(site)
    return $frame
}

## ******************************************************** 
##
## Name: cmonUtils::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonUtils::specific { parent page } {

    set labf1 [LabelFrame $parent.labf1 -text "" -side top -anchor w \
                   -relief flat -borderwidth 4]
    set subf  [$labf1 getframe]

	;## select APIs 
	frame $subf.faction1 
	label $subf.faction1.lbl -text "Select an action and hit GO: "
	pack $subf.faction1.lbl -side top -anchor w 
	
	set f1 [ frame $subf.faction1.f1 ]
    set rad1 [ radiobutton $f1.chk1 -text "Restart log monitor" -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value logscan -highlightcolor blue -width 22 -relief ridge ]        
	set rad2 [ radiobutton $f1.chk2 -text "Restart core file monitor" -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value coreWatch -highlightcolor blue  -width 22 -relief ridge ]    
    set rad3 [ radiobutton $f1.chk3 -text "Restart database monitor" -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value db2utils -highlightcolor blue  -width 22 -relief ridge ]
	pack $rad1 $rad2 $rad3 -side left -anchor w -padx 5
	   		
	set f2 	[ frame $subf.faction1.f2 ]
    set rad4 [ radiobutton $f2.chk4 -text "Restart logCache utility" -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value logCacheUtils -highlightcolor blue  -width 22 -relief ridge ]  
	set rad5 [ radiobutton $f2.chk5 -text "Rebuild frame disk cache" -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value "diskcache" -highlightcolor blue  -width 22 -relief ridge ]
    pack $rad4 $rad5 -side left -anchor w -padx 5
    
    set f3 [ frame $subf.faction1.f3 -relief ridge ]
    #set rad6 [ radiobutton $f3.ck6 -text "Set LogCache Start Time: " -variable ::cmonUtils::var($page,cmd) \
	#	-anchor w -value trimLogCache -highlightcolor blue -width 30 ]
	#set ent [ Entry $f3.logtime -textvariable ::cmonUtils::var($page,startLogTime) -editable 1 -width 30 \
    #   	-helptext "start time for log Cache"  ]
	#pack $rad6 $ent -side left -anchor w
	#bind $ent <Button-1> "set ::cmonUtils::var($page,cmd) trimLogCache"
        
	set f4 [ frame $subf.faction1.f4 -relief ridge ]
	set rad4 [ radiobutton $f4.chk4 -text "Compare databases among: " -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value dbCompare -highlightcolor blue -command "cmonUtils::dbCompareDiag $page" -width 30 ]
	Entry $f4.dbs -textvariable ::cmonUtils::var($page,dblist) -editable 0 -width 30 \
       	-helptext "selected databases for compare" 
	set ::cmonUtils::dbEntry($page) $f4.dbs 
	pack $rad4 $f4.dbs -side left -pady 1 
    bind $f4.dbs <Button-1> "set ::cmonUtils::var($page,cmd) dbCompare"

    set f6 [ frame $subf.faction1.f6 -relief ridge ]
	set rad [ radiobutton $f6.chk -text "Create new database: " -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value createdb -highlightcolor blue  -width 30 ]
	Entry $f6.dbs -textvariable ::cmonUtils::var($page,newdb) -editable 1 -width 8 \
       	-helptext "database name" 
    label $f6.lbl -text on -justify center -anchor n 
    Entry $f6.dbpath -textvariable ::cmonUtils::var($page,dbpath) -editable 1 -width 18 \
       	-helptext "database directory"
	set ::cmonUtils::dbEntry($page) $f6.dbs 
	pack $rad $f6.dbs $f6.lbl $f6.dbpath -side left -pady 1
    bind $f6.dbs <Button-1> "set ::cmonUtils::var($page,cmd) createdb"
    bind $f6.dbpath <Button-1> "set ::cmonUtils::var($page,cmd) createdb"

	set fframe [ frame $subf.faction1.fftp -relief ridge ]
	radiobutton $fframe.chk3 -text "Create an FTP subdirectory: " -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value makeFtpDirectory -highlightcolor blue  \
		-width 30
	Entry $fframe.dir -textvariable ::cmonUtils::var($page,dir) -editable 1 -width 30 \
       	-helptext "subdirectory name for FTP directory" 
	pack $fframe.chk3 $fframe.dir -side left -pady 1 
	
	bind $fframe.dir <Return> "cmonUtils::sendRequest $page"
    bind $fframe.dir <Button-1> "set ::cmonUtils::var($page,cmd) makeFtpDirectory"
    
    set fqframe [ frame $subf.faction1.fque -relief ridge ]
    radiobutton $fqframe.chk4 -text "Purge job queue beyond entry  #: " -variable ::cmonUtils::var($page,cmd) \
		-anchor w -value chopQueueTo -highlightcolor blue  \
		-width 30
    set spin  [SpinBox $fqframe.entry -range {0 100 1} -textvariable ::cmonUtils::var($page,queueNo) \
                   -helptext "size of job queue" ]

	pack $fqframe.chk4 $spin -side left -pady 1 
      
	pack $f1 $f2 $f3 $f6 $f4 $fframe $fqframe -side top -anchor w 

    ;## force it to default if blank
    $spin bind <Button-1> "set ::cmonUtils::var($page,cmd) chopQueueTo"
    
	$spin bind <Leave>  "cmonUtils::histSpinNew cmonUtils $page ::cmonUtils::var($page,queueNo) 0 100"
    $spin bind <Return> "cmonUtils::histSpinNew cmonUtils $page ::cmonUtils::var($page,queueNo) 0 100"
    
	set but [ Button $subf.start -text "SUBMIT" -helptext "submit request to server" \
        -command  "::cmonUtils::sendRequest $page " ]
	set ::cmonUtils::bstart($page) $but
	pack $but -side bottom -padx 20 -pady 10
    pack $subf.faction1 -side top -fill x
	pack $labf1 -side left -fill x
}

## ******************************************************** 
##
## Name: cmonUtils::configHelp 
##
## Description:
## configure help text for each radio button
##
## Parameters:
## page name 
##
## Usage:
##  send request to server using standard protocol
## 
## Comments:
## this proc name is generic for all packages.

proc cmonUtils::configHelp { page } {

	set cmd "$::cmonUtils::var($page,cmd)"
	# $::cmonUtils::helpw($page) configure -text $::cmonUtils::helptext($cmd)
}
	
## ******************************************************** 
##
## Name: cmonUtils::sendRequest 
##
## Description:
## send request to server for controlling individual APIs
##
## Parameters:
## page name
##
## Usage:
##  send request to server using standard protocol
## 
## Comments:
## this proc name is generic for all packages.

proc cmonUtils::sendRequest { page args } {

	set cmd "$::cmonUtils::var($page,cmd)"
	#$::cmonUtils::helpw($page) configure -text $::cmonUtils::helptext($cmd)
	
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonUtils::statusw($page) "Please select an LDAS site"
		return 
	}
	set name [ namespace tail [ namespace current ] ]
       
    set client $::cmonClient::client

    set cmdId "new"
    set repeat 0
    set freq 0

	set ftp 0
    switch $cmd {
        makeFtpDirectory {
                if  { [ regexp {^[\s\t]*$} $::cmonUtils::var($page,dir) ] } {
                    appendStatus $::cmonUtils::statusw($page) "Please enter a sub-directory name"
			        return
		        }
                set ftp 1
            }
        createdb { if  { [ regexp {^[\s\t]*$} $::cmonUtils::var($page,newdb) ] } {
                        appendStatus $::cmonUtils::statusw($page) "Please enter a database name"
			            return
		            }
                    if  { [ regexp {^[\s\t]*$} $::cmonUtils::var($page,dbpath) ] } {
                        appendStatus $::cmonUtils::statusw($page) "Please enter a database path"
			            return
		            }
                 }
        trimLogCache { if { [ regexp {^[\s\t]*$} $::cmonUtils::var($page,startLogTime) ] } {
                        appendStatus $::cmonUtils::statusw($page) "Please enter a local time"
			            return
                       }
                       ;## six months is too far back
                       if   { [ catch {
                            set gpstime [ utc2gps $::cmonUtils::var($page,startLogTime) 0  ]
                            if  { [ expr [ gpsTime now ] - $gpstime ] > $::MAX_LOGCACHE_START } {
                                error "This time is too far backwards to set logCache."
                            }
                       } err ] } {
                            appendStatus $::cmonUtils::statusw($page) "Invalid local time."
                            return
                       }
		        }
        chopQueue  {  if  { [ regexp {^[\s\t]*$} $::cmonUtils::var($page,queueNo) ] } {
                            appendStatus $::cmonUtils::statusw($page) "Please enter queue entry # or range, 0 for all"
			            return
                      }
                   }
        default {}
    }

	set login ""
	set passwd ""
	if	{ [ catch {
    	foreach { login passwd } [ validateLogin 1 ] { break }
	} err ] } {
		appendStatus $::cmonUtils::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
    
	if	{ $ftp } {
		set cmd "cmonUtils::showReply $page\n$repeat\n$freq\n\
		    $client:$cmdId:$login:$passwd\ncntlmon::cmd2mgr comment makeFtpDirectory manager $::cmonJobId \
			$::cmonUtils::var($page,dir)" 
	} elseif { [ string match diskcache $cmd ] } {
        cmonUtils::formatCacheUserCmd
	} elseif { [ string match dbCompare $cmd ] } {
		set cmd "cmonUtils::dispReply $page\n$repeat\n$freq\n\
		    $client:$cmdId:$login:$passwd\ncntlmon::dbprogs dbCompare $::cmonUtils::var($page,dblist)"
    } elseif { [ string match createdb $cmd ] } {
        set cmd "cmonUtils::dispReply $page\n$repeat\n$freq\n\
            $client:$cmdId:$login:$passwd\ncntlmon::dbprogs createLDASdb \
            $::cmonUtils::var($page,newdb) $::cmonUtils::var($page,dbpath)"
    } elseif { [ string match trimLogCache $cmd ] } {
        set cmd "cmonUtils::dispReply $page\n$repeat\n$freq\n\
            $client:$cmdId:$login:$passwd\ncntlmon::trimLogCache \{$::cmonUtils::var($page,startLogTime)\}"
    } elseif { [ string match chopQueueTo $cmd ] } {
        set cmd "cmonUtils::dispReply $page\n$repeat\n$freq\n\
            $client:$cmdId:$login:$passwd\ncntlmon::cmd2mgr comment chopQueueTo manager $::cmonUtils::var($page,queueNo)"
	} else { set cmd "cmonUtils::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::bootutils $cmd" 		
	}	
	
	cmonUtils::state1 $page 

	if	{ [ catch {
		sendCmd $cmd   	
	} err ] } {
       	cmonUtils::state0 $page 
		appendStatus $::cmonUtils::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonUtils::showReply 
##
## Description:
## display result of cmd
## cmd 
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## invoked asynchronously when the reply is received from server

proc cmonUtils::showReply { page rc clientCmd html } {  
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
	switch $rc {

	0 {		
		if	{ [ string length $html ] } {
			appendStatus $::cmonUtils::statusw($page) $html 0 blue   
        }
	  }
    2 { appendStatus $::cmonUtils::statusw($page) $html 0 blue }  
	3 {
		appendStatus $::cmonUtils::statusw($page) $html
	  }
	}
    cmonUtils::state0 $page	
}

## ******************************************************** 
##
## Name: cmonUtils::dispReply 
##
## Description:
## display result of cmd
## cmd 
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## invoked asynchronously when the reply is received from server
## saves cmdId in reply for cancellation of repeats.

proc cmonUtils::dispReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
	set ::cmonUtils::cmdId($page) $cmdId 
    set ::cmonUtils::client $client
    set afterid [ lindex $clientCmd 2 ]
    #puts "in dispReply, rc=$rc page=$page, client=$client,cmdId=$cmdId,afterid=$afterid"
    if  { ! [ string compare $afterid "" ] } {
        #appendStatus $::cmonUtils::statusw($page) "No more updates"
        cmonUtils::state0 $page 
    }
    ;## do not anchor </html> to end because there may be
    ;## ssh garbage returned 
	if  { ! [ regexp {(^[^<]*)(<.*/html>)*} $html match status html ] } {
		set html <html>
	} 
	switch $rc {
	0 {
        if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
            if  { [ regexp {^<html} $html ] } {
                updateDisplay $::cmonUtils::textw($page) $html
            } else {
                appendStatus $::cmonUtils::statusw($page) $html 0 blue
            }
        }
		cmonUtils::state0 $page
	  }
	2 { appendStatus $::cmonUtils::statusw($page) $status 0 blue 
        state1 $page }
		
	3 {
		appendStatus $::cmonUtils::statusw($page) $status 1 red
		updateDisplay $::cmonUtils::textw($page) $html
		cmonUtils::state0 $page 	
	  }
	}
}

## ******************************************************** 
##
## Name: cmonUtils::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonUtils::state0 { page }  {

	updateWidgets $::cmonUtils::topfw($page) normal	
	$::cmonUtils::bstart($page) configure -state normal 
	if	{ $::cmonClient::state } {
		$::cmonUtils::titf3($page) configure -text "LDAS utilities at $::cmonClient::var(site)"
	} else {
		$::cmonUtils::titf3($page) configure -text "LDAS utilities"
		updateStatus $::cmonUtils::statusw($page) ""
	}
}

## ******************************************************** 
##
## Name: cmonUtils::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonUtils::state1 { page { disableCmd cancelRequest } } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonUtils::topfw($page) disabled
	$::cmonUtils::bstart($page) configure -state disabled
}

## ******************************************************** 
##
## Name: cmonUtils::dbCompareDiag   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonUtils::dbCompareDiag { page } {
	
	set image [ Bitmap::get info ]
	set dialogw .cmonUtils$page[ clock seconds ]
	if	{ ! [ winfo exist $dialogw ] } {
		set dlg [Dialog $dialogw -parent . -modal local \
   			-separator 1 -title   "Compare databases" \
       		-side bottom -anchor  s -default 0 -image $image -cancel 3]
		set dlgframe [ $dlg getframe ]
		set rc [ cmonCommon::createDBCheckList $dlgframe cmonUtils $page ]
        if  { $rc } {
		    set bstart [$dlg add -name 0 -command "cmonUtils::setDBs $page $dlg" -text OK ]
		    $dlg add -name cancel -command "destroy $dlg" 
		    $dlg draw 
        }
	}
}

## ******************************************************** 
##
## Name: cmonUtils::setDBs   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonUtils::setDBs { page dlg } {
	::setDBs cmonUtils $page 
	if	{ [ llength $::cmonUtils::var($page,dblist) ] < 2 } {
		appendStatus $::cmonUtils::statusw($page) "Must select at least 2 database" 
		return
	}
	destroy $dlg
}

## ******************************************************** 
##
## Name: cmonUtils::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonUtils::reset { page } {

    updateDisplay $::cmonUtils::textw($page) ""

}

# ******************************************************** 
##
## Name: cmonUtils::formatCacheUserCmd 
##
## Description:
## format the user command to update cache directory
##
## Parameters:
## 
##
## Usage:
##  
## Comments:

proc cmonUtils::formatCacheUserCmd {} {

    uplevel {
        if  { [ catch {	
        	if	{ ! $::USE_GLOBUS } {
            	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::cntlpasswd ] ]
            } else {
            	set userpass $passwd
            }
            set userinfo "-name $login -password $userpass  -email 12345:678"
            regsub -all -- {\s+} $userinfo { } userinfo
            set cmd "cacheRebuild -flaga 0 -flagb 1"
            set cmd "cmonUtils::showReply $page\n$repeat\n$freq\n\
            $client:$cmdId:$login:$passwd\ncntlmon::submitLDASjob $page \{$userinfo\} \{$cmd\}"
        } err ] } {
            appendStatus $::cmonUtils::statusw($page) $err
        }
    }
}

##******************************************************* 
##
## Name: cmonUtils::histHelp
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonUtils::histSpinNew { name page var min max } {

	;## force it to integers
    
    set currvalue [ set ${var} ]
	if 	{ [ regexp {(?n)^[\s\t\n]*$} $currvalue ] || \
          ! [ regexp {^[\d]+$} $currvalue ] } { 
		set $var $min
	} elseif { $currvalue < $min } {
        set $var $min
    } elseif { $currvalue > $max } {
        set $var $max
    }
    set ${var} [ expr round($currvalue) ]
    update idletasks
}
