## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles LDAS log filtering 
##
## cmonAPItimes Version 1.0
##
## ******************************************************** 

package provide cmonAPItimes 1.0

namespace eval cmonAPItimes {
    set notebook ""
    set pages { cmonAPItimes }
	set tab(cmonAPItimes) "Time Metrics "
	set title(cmonAPItimes) "Job Times" 
	set xlabel(cmonAPItimes) "GPS Time"
	set ylabel(cmonAPItimes) "Time Spent (secs)"
	set graph apitimes
	set wTop .tmpAPItimes
    set wHist .tmpAPIHist
	set APIs [ concat total queue $::API_LIST_w_cntlmon asstmgr wrapper ]
}


## ******************************************************** 
##
## Name: cmonAPItimes::create
##
## Description:
## creates main page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAPItimes::create { notebook } {
    
    set page cmonAPItimes
    $notebook insert end cmonAPItimes -text $::cmonAPItimes::tab($page) \
        -createcmd "cmonAPItimes::createPages $notebook $page" \
        -raisecmd "cmonCommon::pageRaise cmonAPItimes $page; \
            cmonCommon::setLastTime cmonAPItimes $page"
}


## ******************************************************** 
##
## Name: cmonAPItimes::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAPItimes::createPages { notebook page } {

	if	{ [ catch { 
    	set frame [ $notebook getframe cmonAPItimes ]
		specific $frame $page 	
        array set ::cmonAPItimes::state [ list $page 0 ]
        set ::cmonAPItimes::prevsite($page) $::cmonClient::var(site)
	} err ] } {
		puts $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonAPItimes::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAPItimes::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
	
    set labf1 [LabelFrame $parent.labf1 -text "Select user, commands, APIs, and times" -side top -anchor w \
                   -relief raised -borderwidth 2 ]
    set subf  [$labf1 getframe]
    set ::cmonAPItimes::topfw($page) $subf
    
	set subf1 [ frame $subf.subf1 -relief sunken -borderwidth 2 ]
	;## filter by user
	set fuserid [ frame $subf1.fuserid -relief groove -borderwidth 2 ]
	
	set userid_ent   [ LabelEntry $fuserid.euserid \
		-label "By User Id: " -labelwidth 15 -labelanchor w \
    	-textvariable ::cmonAPItimes::var($page,userid) -editable 1 \
        -helptext "Formats: <user1> <user2> <user3> ..., or leave blank for users"]
	
	pack $subf1.fuserid.euserid -side top -anchor w -fill x -expand 1
	pack $fuserid -side top -anchor w -fill x 
	set ::cmonAPItimes::var($page,userid) *
	cmonCommon::ldascmds $name $page $subf1 

	;## select pass or fail jobs or both
	set f3 [ frame $subf1.jobs -relief groove -borderwidth 2 ]
	set lbl [ label $f3.jobslbl -text "Jobs: " -width 10 -justify left ]
	set but1 [ checkbutton $f3.pass -text "pass" \
        -variable ::cmonAPItimes::var($page,pass) -onvalue 1 -offvalue 0 ]
	set but2 [ checkbutton $f3.fail -text "fail" \
        -variable ::cmonAPItimes::var($page,fail) -onvalue 1 -offvalue 0 ]
	set ::cmonAPItimes::var($page,pass) 1
	set ::cmonAPItimes::var($page,fail) 0
	
	pack $lbl -side left -anchor w 
	pack $but1 $but2 -side left -anchor w -padx 5 -fill x
	pack $f3 -side top -anchor w -expand 1 -fill both
	
	;## select APIs 
	set name [ namespace tail [ namespace current ] ]	
	#set index [ lsearch $::cmonAPItimes::APIs cntlmon ]
	;## remove cntlmon 
	#if	{ $index > 0 } {
	#	set ::cmonAPItimes::APIs [ lreplace $::cmonAPItimes::APIs $index $index ]
	#}
	createAPICheckList $subf1 $name $page
	set ::${name}::var($page,total) 1
	
	set subf2 [ frame $subf.subf2 -relief ridge -borderwidth 2 ]
	set f2 [ frame $subf2.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]

	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $subf2.ftime $name $page ] { break }
	
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x 
	pack $subf2.ftime -side top -anchor w -expand 1 -fill both
	
	pack $subf1 $subf2 -side left -fill both -expand 1
	
	;## place the cmd status on the bottom
	set labref [LabelFrame $parent.refresh -text "Command Status" -side top -anchor w \
                   -relief sunken -borderwidth 4]
                   
    set subf3  [ $labref getframe ]
	
	set statusw [ ::createStatus  $subf3 ]
	array set ::cmonAPItimes::statusw [ list $page $statusw ]
	
	set f3 [ frame $subf2.fstart ]
    set  but1 [ Button $f3.start -text "SUBMIT" -width 6 -helptext "submit one time request to server" \
        -command  "::cmonAPItimes::sendRequest $page" ]
    set  but2 [ Button $f3.down -text "CLOSE GRAPH WINDOWS" -width 25 -helptext "close all graph windows" \
        -command  "cmonAPItimes::closeWindows $name $page" ]
	pack $but1 -side left -padx 10 -pady 2
    pack $but2 -side right -padx 5 -pady 2
	set ::cmonAPItimes::bstart($page) $but1  
    pack $f3 -side top -expand 1 -fill x	
	pack $labf1 -side top -fill x 
    pack $labref -side top -fill both -expand 1 -pady 2

    
}

## ******************************************************** 
##
## Name: cmonAPItimes::createAPILegend 
##
## Description:
## creates the legend
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonAPItimes::createAPILegend { page parent } {

	set apilist $::cmonAPItimes::apilist
	set numAPIs [ llength $apilist ]
	set apirows [ list ]
	set numPerRow 6

	for { set i 0 } { $i < $numAPIs } { incr i $numPerRow } {
		if	{ [ catch {
			set apirow$i [ lrange $apilist $i [ expr $i + $numPerRow - 1 ] ]
		} err ] } {
			set apirow$i [ lrange $apilist $i end ]
		}
		lappend apirows apirow$i
	}
	set flag 0
	if	{ [ winfo exist $parent ] } {
		pack forget $parent
		destroy $parent 
		set flag 1
	}
	set f1 [ frame $parent ]
	foreach apirow $apirows {
		set fapirow [ frame $f1.f$apirow ]
		foreach api [ set $apirow ] {
			label $fapirow.$api -text $api -width 10 -fg $::Color($api) \
        	-font $::LISTFONT -justify left -bg gray
			pack $fapirow.$api -side left -anchor w 
		}
		pack $fapirow -side top -anchor w
	}
	if	{ $flag } {
		pack $f1 
	}	
	return $f1 
}	

## ******************************************************** 
##
## Name: cmonAPItimes::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
## this proc name is generic for all packages.
## sample test times 
## start 		05/04/00 16:38:27 641493520
## end times 	05/04/00 18:13:24 641499217
##	start		957892986	641928192
## end 		957893198	641928404
##	start		957892986	641928192
## end			641928192	641928192
## start		now
## end			now
	
proc cmonAPItimes::sendRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonAPItimes::statusw($page) "Please select an LDAS site"
		return
	}
	set name [ namespace tail [ namespace current ] ]
	set userid [ cmonCommon::setUserIdPattern ::cmonAPItimes::var($page,userid) ]

	if	{ ! $::cmonAPItimes::var($page,pass) && 
		  ! $::cmonAPItimes::var($page,fail) } {
		appendStatus $::cmonAPItimes::statusw($page) "Please select job options: pass, fail."
		return
	}
	
	set cmdpat [ cmonCommon::setCmdDsoPattern cmonAPItimes $page ]

	foreach type { pass fail } {
		if	{ $::cmonAPItimes::var($page,$type) } {
			lappend filter $type
		}
	}
	if	{ [ catch {
		set servercmd "cntlmon::APITimeGraph $userid "
		;## need to fill in gap to make filter line up
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
		set ::cmonAPItimes::xmin($page) $starttime
		set ::cmonAPItimes::xmax($page) $endtime
        append servercmd "$starttime-$endtime "
		append servercmd "$cmdpat "
		append servercmd "[ join $filter | ] "
        
        ;## remove unselected api windows
       
		set apilist [ cmonAPItimes::closeHistograms $page $name ]
		if	{ ! [ string length $apilist ] } {
			appendStatus $::cmonAPItimes::statusw($page) "Please select at least one API"
			return
		}
        ;## apilist in ::${name}::var($page,apilist) but save this for reference
		append servercmd "[ list $apilist ] "
        set client $::cmonClient::client
        set cmdId "new"
		set freq 0
    	set repeat 0
       	set cmd "cmonAPItimes::showReply $page\n$repeat\n$freq\n\$client:$cmdId\n$servercmd"      
		# puts "cmd=$cmd"
		cmonAPItimes::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonAPItimes::state0 $page 
		appendStatus $::cmonAPItimes::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonAPItimes::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonAPItimes::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"
	# puts "html size [ string length $html ]"
    set ::cmonAPItimes::cmdId($page) $cmdId 
    set ::cmonAPItimes::client $client
    	
	switch $rc {
	0 {	if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
            #updateDisplay $::cmonAPItimes::textw($page) $html
			set name [ namespace tail [ namespace current ] ]
			cmonAPItimes::dispGraph $page $html 
			appendStatus $::cmonAPItimes::statusw($page) updated 0 blue			
        } else {
            appendStatus $::cmonAPItimes::statusw($page) \
                "No jobs" 
        }
	  }
	3 {
		appendStatus $::cmonAPItimes::statusw($page) $html
		;## may need to cancel request	
	  }
	}
	} err ] } {
		appendStatus $::cmonAPItimes::statusw($page) $err
	}
	cmonAPItimes::state0 $page
}


##******************************************************* 
##
## Name: cmonAPItimes::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonAPItimes::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonAPItimes::topfw($page) normal
	array set ::cmonAPItimes::state [ list $page 0 ]

}

##******************************************************* 
##
## Name: cmonAPItimes::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonAPItimes::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonAPItimes::topfw($page) disabled	
    $::cmonAPItimes::bstart($page) configure -state disabled 
	array set ::cmonAPItimes::state [ list $page 1 ]
	
}

##*******************************************************
##
## Name: cmonAPItimes::apiTimesStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::apiTimesStats { page statsdata parent } {

	if	{ [ catch {
        set jobtotal [ lindex $statsdata 2 ]
        set range [ lindex $statsdata 1 ]

        regexp {(\d+)-(\d+)} $range -> gps_start gps_end 
        set localstart [ gps2utc $gps_start 0 ]
        set localend   [ gps2utc $gps_end 0 ]
            
	    catch { unset ::cmonAPItimes::passdata($page) }
		catch { unset ::cmonAPItimes::faildata($page) }	
        
		set apilist [ set ::cmonAPItimes::var($page,apilist) ]
		if	{ [ llength $apilist ] > 5 } {
			set categories ""
			append categories [ join [ lrange $apilist 0 4 ] , ]
			append categories "\n[ join [ lrange $apilist 5 end ] , ]"
		} else {
			set categories [ join $apilist , ]
		}
		
		set ::${parent}(title) "$::cmonAPItimes::title($page) for $categories"
		set ::${parent}(y1label) $::cmonAPItimes::ylabel($page)
		set ::${parent}(Enter) "cmonCommon::showJobs cmonAPItimes $page"
		set ::${parent}(Leave) "cmonCommon::removeLabel $parent"
		set ::${parent}(submit) "cmonAPItimes::sendRequest $page"
		set ::${parent}(B1Up)   "cmonCommon::setLogFilter cmonAPItimes $page"
        
        ;## print info for canvas
		set ::${parent}(ptitle) "Time Spent in $categories @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
        
        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
		lappend text "From $localstart ($gps_start) to $localend ($gps_end):\n" [ list brown fixed ]
	
		set user $::cmonAPItimes::var($page,userid)	
		if	{ [ regexp {^[\*\s\t]+$} $user ] } {
			set user all
		}
		set userline "For user(s) $user "
        
        lappend text "Selected Users:\t" [ list black fixed ]
        lappend text "$user" [ list brown bold ]
        
        set cmdtext [ cmonCommon::showCmdsDsos cmonAPItimes $page 0 ]
        lappend text "\n$cmdtext\n" [ list brown bold ]      
        lappend text "Categories selected:\t" [ list brown bold ]
        foreach api $apilist {
            lappend text $api\t [ list $::Color($api) bold ]
        }
        
		;## print info for canvas
		# set ::${parent}(ptitle) "Time Spent in $categories @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"

        set site [ string trim $::cmonClient::var(site) ]
        
        set dataitems "categories range jobtotal ymin ymax numpassed numfailed pfailed passdata faildata"
        
		foreach api $apilist {
			append dataitems " $api ${api}times "
			lappend datalist ${api}times
            set ${api}jobs 0
		}		
		set cmd "list $dataitems"
		foreach [ eval $cmd ] $statsdata { break }
		
        ;## write out each category to individual file for histogram
        
        foreach api $apilist {
            set histwin ${::cmonAPItimes::wHist}${api}
            set fname $::TMPDIR/[ string trimleft $histwin . ].$site    
            set fd [ open $fname w ]
            set apidata [ set ${api}times ]
            puts $fd "$api $range $jobtotal $ymin $ymax $numpassed $numfailed $pfailed ${api}times [ list $apidata ]"
            set ${api}jobs [ expr [ llength $apidata ]/2 ]
            close $fd
		}
        	
		if	{ ! $jobtotal } {
			set numpassed 0
			set numfailed 0
			set pfailed 0.0
			set passdata [ list ]
			set faildata [ list ]
		}
		
		foreach { time job } $passdata {
			set ::cmonAPItimes::pass${page}($time) $job
		}
		
		foreach { time job } $faildata {
			set ::cmonAPItimes::fail${page}($time) $job
		}
        set ymin 0        
		cmonCommon::roundYaxis 
		lappend text "\n$jobtotal jobs" [ list brown bold ]
		lappend text "\t$numpassed passed "  [ list $::brightgreen bold ]
		lappend text "\t$numfailed failed ( ${pfailed}% )\n" [ list red bold ]
		
        ;## append the count of each category
        set numapi 0
        foreach api $apilist {
            lappend text "[ set ${api}jobs ] in $api\t" [ list $::Color($api) bold ]
        }
        set text [ string trim $text \n ]
        updateTextWin [ set ::${parent}(textw) ] $text 
		update idletasks
	
		set bltdata [ list ]
		foreach data $datalist {
			set values [ set $data ]
			regsub {times} $data {} name
            foreach { values msg } [ cmonCommon::reduceDisplayData $values $::MAX_GRAPH_POINTS ] { break }
            if  { [ llength $msg ] } {
                appendStatus $::cmonAPItimes::statusw($page) "$data data: $msg" 0 brown
            }
			lappend bltdata $values
			lappend bltdata ${name}_times
			lappend bltdata circle 
			lappend bltdata $::Color($name)	
			lappend bltdata 0
		}
		
		blt_graph::showGraph $bltdata $::cmonAPItimes::xmin($page) \
				$::cmonAPItimes::xmax($page) $ymin $ymax $parent
				
		raise $parent
		update idletasks	

	} err ] } {
		appendStatus $::cmonAPItimes::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonAPItimes::dispGraph
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::dispGraph { page html } {

	set site [ string trim $::cmonClient::var(site) ]
    set parent $::cmonAPItimes::wTop
    if  { ! [ winfo exist $parent ] } {
        toplevel $parent 
	    set statusw [ createTextWin $parent 5 ]  
        set ::${parent}(textw) $statusw
    }    
    set statusw [ set ::${parent}(textw) ]
    bind $statusw <Destroy> [ list cmonAPItimes::closeWindows $page $parent $site ]	
    wm protocol $parent WM_DELETE_WINDOW [ list cmonAPItimes::closeWindows $page $parent $site ]
    set apilist [ set ::cmonAPItimes::var($page,apilist) ]
    set ::${parent}(replot) [ list cmonAPItimes::selectAPIsforHistogram cmonAPItimes $apilist ]
	wm title $parent "$::cmonAPItimes::tab($page)@$site"   
    wm deiconify $parent
    apiTimesStats $page $html $parent
    cmonAPItimes::updateHistograms $page $apilist 
}

##*******************************************************
##
## Name: cmonAPItimes::apiHistogram
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::apiHistogram { page statsdata parent api { cummPlot 0 } { replot 0 } } {

	if	{ [ catch {
        set dataitems "category range jobtotal ymin ymax numpassed numfailed pfailed apitimes timedata"
        if  { ![ info exist ::${parent}(bucketsz) ] } {
            set binsize 1
        } else {
            set binsize [ set ::${parent}(bucketsz) ]
        }
		set cmd "list $dataitems"
		foreach [ eval $cmd ] $statsdata { break }
        
        if  { ![ string equal $category $api ] } {
            error "expected api $api but got $category"
        }
        regexp {(\d+)-(\d+)} $range -> gps_start gps_end
        set localstart [ gps2utc $gps_start 0 ]
        set localend   [ gps2utc $gps_end 0 ]
		set ::${parent}(ptitle) "$api Time spent by Jobs Histogram @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"	
		
        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
		set user $::cmonAPItimes::var($page,userid)	
		if	{ [ regexp {^[\*\s\t]+$} $user ] } {
			set user all
		}
        set apijobs [ expr [ llength $timedata ]/2 ]
        
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
        lappend text "Selected Users:\t" [ list black fixed ]
        lappend text "$user" [ list brown bold ]
        
		;## print info for canvas
		# set ::${parent}(ptitle) "$api Time spent by Jobs Histogram @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"	
					
		set ::${parent}(title) "$api $::cmonAPItimes::title($page) Histogram"
		set ::${parent}(y1label) "# Jobs"
		set ::${parent}(x1label) "Time spent (secs )"
		set ::${parent}(x1step) $binsize
		set ::${parent}(replot) "cmonAPItimes::replot $page $api"
        set ::${parent}(histargs) "cmonAPItimes $page [ set ::BIN_RANGE(cmonAPItimes) ]"
                
        lappend text "\nSelected category:\t" [ list black fixed ]
        lappend text "$api" [ list $::Color($api) bold ]
      	
        set cmdtext  [ cmonCommon::showCmdsDsos cmonAPItimes $page 0 ]

		lappend text "\n$cmdtext\n" [ list brown bold ]
        
        set ymin 0
        set ymax 5
		if	{ ! $jobtotal } {
			set numpassed 0
			set numfailed 0
		} 
        set histdata [ list ]
        if  { [ llength $timedata ] } {   
            counter::init ${api}times -hist $binsize
            set ylist [ list ]
            foreach { x y } $timedata {
                counter::count ${api}times $y
                lappend ylist $y
            }
            foreach { xtotal xmean xmedian xstddev xcor } [ bltstats $ylist ] { break }
            set ylist [ lsort -unique $ylist ]
            set ymin [ lindex $ylist 0 ]
            set ymax [ lindex $ylist 1 ]
            unset ylist
            set histdata [ counter::getDBHistData ${api}times ]
            counter::init ${api}times
        } 
		lappend text " $jobtotal jobs " [ list brown bold ]
		if	{ $::cmonAPItimes::var($page,pass) } {
			lappend text "\t$numpassed passed " [ list $::brightgreen bold ]
		}
		if	{ $::cmonAPItimes::var($page,fail) } {
			lappend text "\t$numfailed failed ( ${pfailed}% ) " [ list red bold ]
		}
        lappend text "\t$apijobs in $api" [ list $::Color($api) bold ]
        
        if  { $cummPlot } {
            if  { [ llength $timedata ] } {
                set histdata [ cmonCommon::getHistCummData $histdata $binsize ]
            }
            set ::${parent}(y1label) "% Jobs, cummulative"        
        } 
		foreach { xmin ymin xmax ymax } [ cmonCommon::computeXYMinMax $histdata $binsize ] { break }
        
		set bltdata [ list ]
		lappend bltdata $histdata
		lappend bltdata ${api}_histogram
		lappend bltdata $::Color($api)
		lappend bltdata $binsize

		set graph [ blt_graph::barChart $bltdata $xmin $xmax $ymin $ymax $parent ]
        set statstext ""
        if  { [ info exist xmean ] } {
            append statstext "X axis\ttotal=$xtotal, mean=$xmean , median=$xmedian, std-dev=$xstddev\n"           
        }
        if  { [ info exist ::${parent}(ystats) ] } {
            foreach { ytotal ymean ymedian ystddev }  [ set ::${parent}(ystats) ] { break }
            append statstext "Y axis\ttotal jobs=$ytotal, mean=[ expr round($ymean) ], median=[ expr round($ymedian) ], std-dev=[ format %.2f $ystddev ]"
        }
        lappend text "\n$statstext" [ list brown bold ]
        updateTextWin [ set ::${parent}(textw) ] $text 
		raise $parent
		update idletasks
	} err ] } {
		appendStatus $::cmonAPItimes::statusw($page) $err
	}
}


##*******************************************************
##
## Name: cmonAPtimes::createHistWin
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::createHistWin { page api } {

    if  { [ catch {
	    set site [ string trim $::cmonClient::var(site) ]
        set parent $::cmonAPItimes::wHist${api}	
        if  { ! [ winfo exist $parent ] } {
            toplevel $parent 
	        set statusw [ createTextWin $parent 5 ]  
            set ::${parent}(textw) $statusw
            set ::${parent}(cummPlot) 0
        }    
	    wm deiconify $parent
        wm title $parent "$api $::cmonAPItimes::tab($page) Histogram @$site"
    } err ] } {
         return -code error $err
    }
    return $parent 

}

##*******************************************************
##
## Name: cmonAPItimes::replot
##
## Description:
## replot the graph again, based in points or lines
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::replot { page api } {

    set site [ string trim $::cmonClient::var(site) ]
    set parent [ cmonAPItimes::createHistWin $page $api ]
    set origCursor [ $parent cget -cursor ]
    set fname $::TMPDIR/[ string trimleft $parent . ].$site

	if	{ [ file exist $fname ] } {
        $parent config -cursor watch
		set fd [ open $fname r  ]
		set html [ read $fd  ]
		close $fd
		wm deiconify $parent
        apiHistogram $page $html $parent $api [ set ::${parent}(cummPlot) ] 1
	} else {
		appendStatus $::cmonAPItimes::statusw($page) \
		"Unable to replot $api Histogram for $site from $fname; \
            please resubmit your request to server"
	}
    $parent config -cursor $origCursor	
}


##*******************************************************
##
## Name: cmonAPItimes::YHistograms
##
## Description:
## replot the graph again, based in points or lines
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::YHistograms { page apilist } {

    foreach api $apilist {
        cmonAPItimes::replot $page $api
    }
}


##*******************************************************
##
## Name: cmonAPItimes::updateHistograms
##
## Description:
## refresh histograms when time data is refreshed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::updateHistograms { page apilist } {

    foreach api $apilist {
        set parent $::cmonAPItimes::wHist${api}	
        if  { [ winfo exist $parent ] } {
            cmonAPItimes::replot $page $api
        }
    }
}

## ******************************************************** 
## 
## Name: cmonAPItimes::closeWindows
##
## Description:
## create database tables widget
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::closeWindows { name page { site "" } } {

    set win [ set ::${name}::wTop ]
    if  { [ winfo exist $win ] } {
        destroy $win
    }
    if  { ! [ string length $site ] } {
        set site [ string trim $::cmonClient::var(site) ]
    }
    foreach api [ set ::${name}::APIs ] {    
        set win [ set ::${name}::wHist ]${api}
        if  { [ winfo exist $win ] } {
            destroy $win
        }
        set fname $::TMPDIR/[ string trimleft $win . ].$site
        catch { file delete -force $fname }
    }
}

##*******************************************************
##
## Name: cmonAPItimes::closeHistograms
##
## Description:
## refresh histograms when time data is refreshed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::closeHistograms { name page } {

    set prev_apis [ list ]
    if  { [ info exist ::${name}::var($page,apilist) ] } {
        set prev_apis [ set ::${name}::var($page,apilist) ]
    }
    set apilist  [ ::setAPIs $name $page ]
    set site [ string trim $::cmonClient::var(site) ]
    foreach api $prev_apis {
        if  { [ lsearch -exact $apilist $api ] == -1  } {   
            set win [ set ::${name}::wHist ]${api}
            if  { [ winfo exist $win ] } {
                destroy $win
            }
            set fname $::TMPDIR/[ string trimleft $win . ].$site
            catch { file delete -force $fname }
        }
    }
    return $apilist 
}

##*******************************************************
##
## Name: cmonAPItimes::selectAPIsForHistogram
##
## Description:
## allow users to select APIs for histogram display
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::selectAPIsforHistogram { page apilist } {

    set name cmonAPItimes
    if  { [ llength $apilist ] < 4 } {
        after 0 "cmonAPItimes::YHistograms cmonAPItimes [ list $apilist ]"
        return
    }
    set dialogw .cmonAPItimesDialog
    if	{ ! [ winfo exist $dialogw ] } {
		set dlg [Dialog $dialogw -parent . -modal local \
   			-separator 1 -title   "Select APIs for Job Times Histogram" \
       		-side bottom -anchor  s -default 0 -cancel 3]
        set dlgframe [ $dlg getframe ]
        cmonAPItimes::createAPICheckListHist $dlgframe $name $page $apilist 
        set bstart [$dlg add -name 0 -command  \
            "[ list cmonAPItimes::displaySelectedHistograms cmonAPItimes cmonAPItimes $apilist ]; \
            destroy $dlg" -text HISTOGRAMS ]
	    $dlg add -name cancel -command "destroy $dlg" 
		$dlg draw 
    }
}

##*******************************************************
##
## Name: cmonAPItimes::displaySelectedHistograms
##
## Description:
## display selected histograms
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPItimes::displaySelectedHistograms { name page apilist } {

    foreach { selectedAPIs unselectedAPIs } \
        [ cmonAPItimes::setAPIsHist $name $page $apilist ] { break }
        
    ;## remove unneeded windows
    foreach api $unselectedAPIs {
        set win [ set ::${name}::wHist ]${api}
         if  { [ winfo exist $win ] } {
             destroy $win
         }
    }
    cmonAPItimes::YHistograms cmonAPItimes $selectedAPIs
    
}

## ******************************************************** 
##
## Name: cmonAPItimes createAPICheckListHist
##
## Description:
## creates a checkbox for APIs
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc cmonAPItimes::createAPICheckListHist { parent name page apilist } {

	frame $parent.flab -relief ridge -borderwidth 2
	set lab [ label $parent.flab.lbl -text "Select one or more categories: "  \
		-justify left -font $::LISTFONT -wraplength 300 ]
	set selectmenu [ eval tk_optionMenu $parent.flab.select ::${name}::hist($page,select) \
        { "select all" "deselect all" } ]
	$selectmenu entryconfigure 0 -command [ list cmonAPItimes::APIselectHist $name $page 1 $apilist ]
	$selectmenu entryconfigure 1 -command [ list cmonAPItimes::APIselectHist $name $page 0 $apilist ]
	pack $parent.flab.select -side right
	pack $parent.flab.lbl -side left -fill x -expand 1
	pack $parent.flab -side top -fill both -expand 1

	set subfapi [ frame $parent.fapi -relief ridge -borderwidth 1 ]
	
	set numAPIs [ llength $apilist ]
	set apirows [ list ]
	set itemsRow 3 
	for { set i 0 } { $i < $numAPIs } { incr i $itemsRow } {
		if	{ [ catch {
			set apirow$i [ lrange $apilist $i [ expr $i + 2 ] ]
		} err ] } {
			set apirow$i [ lrange $apilist $i end ]
		}
		lappend apirows apirow$i
	}
	foreach apirow $apirows {
		set f [ frame $subfapi.f$apirow ]
		foreach api [ set $apirow ] {
			checkbutton $f.$api -text $api -width 12 -anchor w \
        	-variable ::${name}::hist($page,$api) \
        	-font $::LISTFONT -onvalue 1 -offvalue 0 
			set ::${name}::hist($page,$api) 0
			pack $f.$api -side left -anchor w
		}
		pack $f -side top -anchor w
	}
	pack $subfapi -fill both -expand 1 
}

## ******************************************************** 
##
## Name: APIselect 
##
## Description:
## turn on or off API checkboxes
## update the title to reflect this 
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc cmonAPItimes::APIselectHist { name page value apilist } {

	foreach api $apilist {
		set ::${name}::hist($page,$api) $value
	}
}

## ******************************************************** 
## 
## Name: setAPIs 
##
## Description:
## format an API string for selected APIs
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##    
 
proc cmonAPItimes::setAPIsHist { page name apiset } {
    set apilist {}
    set unselected {}
    foreach api $apiset {
        if  { [ set ::${name}::hist($page,$api) ] } {
            lappend apilist $api
        } else {
            lappend unselected $api
        }
    }
    unset ::${name}::hist
    return "[ list $apilist $unselected ]"
}
