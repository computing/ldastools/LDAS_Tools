## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles the startup/shutdown of LDAS APIs
## and control of tape controller at Hanford.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
## ******************************************************** 

package provide cmonLDAS 1.0
package require cmonResource 1.0
package require cmonUtils 1.0
package require cmonCores 1.0
package require cmonBlock 1.0
package require cmonLDASPurge 1.0

namespace eval cmonLDAS {

    set pages { APIs System }
	array set timeout { APIs { 5 10000 } }
	array set timeout { System { 6 50000 } }
    set apilist [ concat API $::API_LIST_n_manager ]
	set hostlist [ concat host $::LDASmachines(no-server) ]
}

## ******************************************************** 
##
## Name: cmonLDAS::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::create { notebook } {

    set frame  [ $notebook insert end cmonLDAS -text "Control" ]
          
	set ::cmonLDAS::notebook [ NoteBook $frame.nb -homogeneous 0 \
        -foreground white -background $::darkred -bd 2 ]
    set notebook1 $::cmonLDAS::notebook

    foreach page $::cmonLDAS::pages {
        $notebook1 insert end cmonLDAS:$page -text $page \
        -createcmd "cmonLDAS::createPages $notebook1 $page" \
		    -raisecmd "cmonLDAS::pageRaise $page; cmonCommon::updateDBinfo cmonLDAS $page" \
		    -leavecmd "set ::cmonClient::noPass 0; return 1"	
    
    }
    $notebook itemconfigure cmonLDAS -raisecmd \
        "cmonCommon::toppageRaise $notebook1"
    
    cmonResource::create $notebook1
    cmonUtils::create $notebook1
    cmonAdmin::create $notebook1
    cmonBlock::create $notebook1
    cmonCores::create $notebook1
    cmonLDASPurge::create $notebook1
    pack $notebook1 -fill both -expand yes -padx 4 -pady 4
    $notebook1 raise [ $notebook1 page 0 ] 
}

## ******************************************************** 
##
## Name: cmonLDAS::statuswin 
##
## Description:
## creates status window for API and System
##
## Parameters:
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::statuswin {} {

    uplevel {       
        pack $topf -fill x -pady 2
		set ::cmonLDAS::topfw($page) $topf
	
		foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
		
		;## create a status to display errors and status
    
    	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
		set statusw [ ::createStatus  [ $titf2 getframe ] ]
		array set ::cmonLDAS::statusw [ list $page $statusw ]
		pack $titf2 -pady 2 -padx 4 -fill both -expand yes
	
    	set titf3 [TitleFrame $pane3.titf3 -text $page -font $::ITALICFONT ]
		set ::cmonLDAS::titf3($page) $titf3 
		set textw [ ::createDisplay [ $titf3 getframe ] ]
    	array set ::cmonLDAS::textw [ list $page $textw ]
		pack $titf3 -pady 2 -padx 4 -fill both -expand yes
		pack $pw1 -side top -fill both -expand 1   
    }
}

## ******************************************************** 
##
## Name: cmonLDAS::createPages 
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::createPages { notebook page } {
   
        set frame [ $notebook getframe cmonLDAS:${page} ]	
        set topf  [frame $frame.topf]
        if  { [ regexp -nocase {system} $page ] } {
            LDASspecific $topf $page
        } elseif { [ regexp -nocase {api} $page ] } {
            specific $topf $page
        }
        statuswin 
        array set ::cmonLDAS::state [ list $page 0 ]

    $notebook compute_size
    pack $notebook -fill both -expand yes -padx 4 -pady 4
    set ::cmonLDAS::prevsite($page) $::cmonClient::var(site)
    $notebook raise cmonLDAS:${page}
}

## ******************************************************** 
##
## Name: cmonLDAS::specific 
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::specific { parent page } {

    set labf1 [LabelFrame $parent.labf1 -text "LDAS API control options" -side top -anchor w \
                   -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
	
	frame $subf.fapi -relief ridge -borderwidth 2
	label $subf.fapi.labf1 -text "Select LDAS API: "  -relief flat 
    set apimenu [ eval tk_optionMenu $subf.fapi.apilist ::cmonLDAS::var($page,api) \
        $::cmonLDAS::apilist ]
	
	set numAPIs [ llength $::cmonLDAS::apilist ] 
	for { set i 0 } { $i < $numAPIs } { incr i 1 } {
		set api [ lindex $::cmonLDAS::apilist $i ] 
		$apimenu entryconfigure $i -command "cmonLDAS::nonCntlmonOpts $page"
		if	{ [ string match cntlmon $api ] } {
			$apimenu entryconfigure $i -command "cmonLDAS::cntlmonOpts $page"
		} 
	}
	pack $subf.fapi.labf1 $subf.fapi.apilist -side left -anchor w
		
	frame $subf.faction1 -relief ridge -borderwidth 2
	label $subf.faction1.lbl -text "Select an action: "
	pack $subf.faction1.lbl -side top -anchor w 
    radiobutton $subf.faction1.chk1 -text "Restart" -variable ::cmonLDAS::var($page,mgrcmd) \
		-anchor w -value bootstrap 
    radiobutton $subf.faction1.chk2 -text "Shutdown" -variable ::cmonLDAS::var($page,mgrcmd) \
		-anchor w -value shutdown 
    radiobutton $subf.faction1.chk3 -text "Remove" -variable ::cmonLDAS::var($page,mgrcmd) \
		-anchor w -value remove 
	pack $subf.faction1.chk1 $subf.faction1.chk2 $subf.faction1.chk3 -side left -anchor w 
	
    set f [ frame $subf.faction1.fhost ]
    radiobutton $f.rapihost -text "Add API on host: " \
        -variable ::cmonLDAS::var($page,mgrcmd) \
        -anchor w  -value add 
    pack $f.rapihost -side left -fill x -anchor w 
    cmonCommon::createLDASmachinesCombo cmonLDAS $page $f.ldashosts
    trace variable ::LDASmachines w [ list ::cmonLDAS::traceUpdateMachines $page ]
    #set hostmenu [ eval tk_optionMenu $subf.faction1.fhost.ldashosts ::cmonLDAS::var($page,apihostname) \
    #    $::cmonLDAS::hostlist ]

    pack $f.rapihost $f.ldashosts -side left -fill x -anchor w 
    pack $subf.faction1.chk1 $subf.faction1.chk2 $subf.faction1.chk3 $f -padx 2 -side left -anchor w 
	
	set cmonLDAS::var($page,process_sel) 0
	set cmonLDAS::var($page,mgrcmd) bootstrap
	
	frame $subf.freason -relief ridge -borderwidth 2
    set comment_ent [ cmonCommon::createRecordWidget $subf.freason cmonLDAS $page ]
    
	pack $comment_ent -side top -anchor w -fill x
	pack $subf.fapi $subf.faction1 $subf.freason -side top -padx 2 -pady 2 -fill x
    
	set but [ Button $subf.start -text "SUBMIT" -helptext "submit request" \
        -command  "cmonLDAS::sendRequest $page " ]
    pack $but -side top -padx 10 -pady 5 
    set ::cmonLDAS::bstart($page) $but
         
	pack $labf1 -side left -fill both -expand 1

	set ::cmonLDAS::notcntlmonw [ list $subf.faction1.chk2 $subf.faction1.chk3 \
		$subf.faction1.fhost.rapihost \
		$subf.faction1.fhost.ldashosts ]
}

## ******************************************************** 
##
## Name: cmonLDAS::LDASspecific 
##
## Description:
## creates Load page specific portion for LDAS system
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::LDASspecific { parent page } {

    set titf1 [TitleFrame $parent.titf1 -text \
	"Specify options"]
	set psubf [ $titf1 getframe ]
	set ::cmonLDAS::var($page,focus) $psubf

    set labf1 [LabelFrame $psubf.labf1 -text "LDAS System options" -side top -anchor w \
                   -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]

    frame $subf.fhost -relief ridge -borderwidth 2
    radiobutton $subf.fhost.rapihost -text "Start LDAS with key: " \
        -variable ::cmonLDAS::var($page,mgrcmd) \
        -anchor w  -value runLDAS -command "set ::cmonClient::noPass 1"
        
    set  ent [ Entry $subf.fhost.key -textvariable ::cmonLDAS::var($page,key) -width 15 -show * ]
    bind $ent <Button-1> "set ::cmonLDAS::var($page,mgrcmd) runLDAS; set ::cmonClient::noPass 1"
    
    pack $subf.fhost.rapihost $ent -side left -fill x -anchor w

	set ::cmonLDAS::var($page,mgrcmd) sHuTdOwN
	
	set f2 [ frame $subf.freason -relief ridge -borderwidth 2 ]
    set rad2 [ radiobutton $f2.chk1 -text "Stop LDAS" -variable ::cmonLDAS::var($page,mgrcmd) \
		-anchor w -value sHuTdOwN -command "set ::cmonClient::noPass 0" ]
        
    set comment_ent [ cmonCommon::createRecordWidget $f2 cmonLDAS $page ]

	pack $rad2 -side top -anchor w 
    pack $comment_ent -side top -anchor w -fill x

    pack $subf.fhost $f2 -side top -fill x -ipady 2 -anchor w

	set ::cmonLDAS::var($page,process_sel) 0
    
    Button $subf.start -text "SUBMIT" -helptext "submit request to server" \
        -command  [ list cmonLDAS::LDAScontrol $page ]	
	set ::cmonLDAS::bstart($page) 	$subf.start
	pack $subf.start -side top -padx 10 -pady 10  
       
	pack $labf1 -side left -fill both -expand 1
    pack $titf1 -fill both -expand 1
    
    ;## expire the manager key after a defined time
    after [ expr $::MAX_KEY_SECS * 1000 ] [ list cmonLDAS::expireKey $page ]
}

## ******************************************************** 
##
## Name: cmonLDAS::sendRequest 
##
## Description:
## send request to server for controlling individual APIs
##
## Parameters:
## page name
##
## Usage:
##  send request to server using standard protocol
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::sendRequest { page } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDAS::statusw($page) "Please select an LDAS site"
		return 
	}
	set name [ namespace tail [ namespace current ] ]
       
    set client $::cmonClient::client

    set cmdId "new"
    set repeat 0
    set freq 0
    set api ""
    set mgrcmd "$::cmonLDAS::var($page,mgrcmd)API"
    
    set apihost [ set ::cmonLDAS::${page}(hostname) ]
	set api $::cmonLDAS::var($page,api)
	if	{ [ regexp -nocase [ lindex $::cmonLDAS::apilist 0 ]  $api ] } {
		appendStatus $::cmonLDAS::statusw($page) "Please select an API"
	    return
    }
    if  { [ string compare $mgrcmd "addAPI" ] } {
            set apihost ""
    } else {
        set apihost [ set ::cmonLDAS::${page}(hostname) ]
		if	{ [ regexp -nocase [ lindex $::cmonLDAS::hostlist 0 ]  $apihost ] } {
			appendStatus $::cmonLDAS::statusw($page) "Please select a host for API."
			return
        }
    }
	if	{ ! [ string length $::cmonLDAS::var($page,comment) ] } {
	    appendStatus $::cmonLDAS::statusw($page) "Please supply a reason for API action"
		return
    }
        
	if	{ [ catch {
    	foreach { login passwd } [ validateLogin 1 ] { break }		
	} err ] } {
		appendStatus $::cmonLDAS::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
	cmonLDAS::state1 $page 
    set comment ""
    if  { ! [ string match manager $api ] } {
	    if	{ [ catch {
			set comment " by $login for '$::cmonLDAS::var($page,comment)'" 
        	set cmd "cmonLDAS::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId:$login:$passwd\ncntlmon::cmd2mgr \{$comment\} $mgrcmd $api $apihost" 
			# puts "cmd=$cmd"
			sendCmd $cmd  
			if	{ [ regexp {cntlmon} $api ] } {
				set connect 1
			} else {
				set connect 0
			}
			set ::cmonLDAS::pullId($page) [ after 5000 [ list cmonLDAS::pullStatus $page $::cmonClient::var(site) $connect ] ]  
	    } err ] } {
        	cmonLDAS::state0 $page 
			appendStatus $::cmonLDAS::statusw($page) $err
			catch { unset ::cmonLDAS::pullId($page) } 
	    }
    }
}

## ******************************************************** 
##
## Name: cmonLDAS::cancelRequest 
##
## Description:
## send request to server for cancel individual APIs
## cmd 
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## maybe not be needed ??

proc cmonLDAS::cancelRequest { page } {
	set name [ namespace tail [ namespace current ] ]
	set cmdId $::cmonLDAS::cmdId($page)
    set client $::cmonLDAS::client
    disableStart $name $page disabled
    
    set repeat 0
    set freq 0
	if	{ [ catch {
		set cmd "cmonLDAS::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\n${client}::cancel $cmdId" 
		#puts "cmd=$cmd"
        cmonLDAS::state1 $page 
		sendCmd $cmd 
	} err ] } {
        cmonLDAS::state0 $page 
		appendStatus $::cmonLDAS::statusw($page) $err 
        enableStart $name $page  
	}
}

## ******************************************************** 
##
## Name: cmonLDAS::showReply 
##
## Description:
## display result of cmd
## cmd 
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## invoked asynchronously when the reply is received from server

proc cmonLDAS::showReply { page rc clientCmd html } {  
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
	#puts "[myName], $page, $rc, $html"
	switch $rc {
	0 {		
		if	{ [ string length $html ] } {
			appendStatus $::cmonLDAS::statusw($page) $html 0 blue
            cmonLDAS::state0 $page 
  
        }
	  }
	3 {
		appendStatus $::cmonLDAS::statusw($page) $html
        cmonLDAS::state0 $page 
		catch { after cancel $::cmonLDAS::pullId($page) }
	  }
	}	
}

## ******************************************************** 
##
## Name: cmonLDAS::reset
##
## Description:
## go to state 0.
## 
## Parameters:
##
## Usage:
##  
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::reset { page } {

		 switch $page {
		 	System	{ cmonLDAS::state0 $page 
		 		  cmonLDAS::traceUpdate $page
				}
		 	APIs 	{ cmonLDAS::state0 $page 
                cmonLDAS::traceUpdateMachines $page }
			default {}		
		}
}   

## ******************************************************** 
##
## Name: cmonLDAS::LDAScontrol
##
## Description:
## execute request to server for the LDAS-system control page.
## 1. start LDAS
## 2. shutdown LDAS
## 3. start cntlmonAPI 
## 
## The above commands are directed to the manager.
##
## Parameters:
##
## Usage:
##  
## Comments:
## this proc name is generic for all packages.

proc cmonLDAS::LDAScontrol { page } {
	update idletasks
	
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDAS::statusw($page) "Please select an LDAS site"
		return 
	}
		
	if  { [ string match runLDAS $::cmonLDAS::var($page,mgrcmd) ] } {
		if	{ ! [ string length $::cmonLDAS::var($page,key) ] } {
			appendStatus $::cmonLDAS::statusw($page) "Please supply a key to start LDAS"
			return 
		}
		cmonLDAS::startLDAS $page
		return
	}
	;## stop LDAS only
	if	{ ! [ string length $::cmonLDAS::var($page,comment) ] } {
	    appendStatus $::cmonLDAS::statusw($page) "Please supply a reason for API action"
		return
    }
	if	{ [ catch {
    	foreach { login passwd } [ validateLogin 1 ] { break }
	} err ] } {
		appendStatus $::cmonLDAS::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		cmonLDAS::state0 $page
		return
	}
    ;## for shutdown to manager via cntlmonAPI
    set name [ namespace tail [ namespace current ] ]
       
    set client $::cmonClient::client
	 
    set cmdId "new"
    set repeat 0
    set freq 0
    set args ""
	if	{ [ string match *User* $::cmonLDAS::var($page,mgrcmd) ] } {
		set args "$::cmonLDAS::var($page,usrname) [ encode64 $::cmonLDAS::var($page,passwd) ] \
			$::cmonLDAS::var($page,email)"
	}
	if	{ [ catch {
	    set comment "by user $login for '$::cmonLDAS::var($page,comment)'" 
    ;## when cntlmonAPI server is killed, all pages resets 
        set cmd "cmonLDAS::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::cmd2mgr \{$comment\} $::cmonLDAS::var($page,mgrcmd) $page $args" 
		# puts "cmd=$cmd"
        cmonLDAS::state1 $page LDAScontrol
		sendCmd $cmd  
		set ::cmonLDAS::pullId($page) [ after 5000 cmonLDAS::pullStatus $page $::cmonClient::var(site) ]
	} err ] } {
        cmonLDAS::state0 $page
		appendStatus $::cmonLDAS::statusw($page) $err
		catch { after cancel $::cmonLDAS::pullId($page) } 
	}
}

#******************************************************** 
##
## Name: cmonLDAS::startLDAS 
##
## Description:
## restart LDAS
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::startLDAS { page } {

		;## password is asked by ssh, using interactive ssh due to remote client
    	foreach { host port } $::siteport($::cmonClient::var(site)) { break }
        set key $::cmonLDAS::var($page,key) 
		# cannot execssh from an outside network
			
		;## bring up a terminal to allow password to be entered
		catch { exec $::XTERM -geometry 40x6 \
		-title $::user@$host -e ssh -f ldas@$host /usr/bin/env  \
			$::RUNLDAS $::cmonLDAS::var($page,key) & } pid

        if  { [ regexp {\d+} $pid ] || ! [ string length $pid ] } {
			#$::cmonLDAS::bstart($page) configure -text "CANCEL" -command "catch {exec kill -9 $pid}"
            appendStatus $::cmonLDAS::statusw($page) "start LDAS request issued. $pid" 0 blue 			
        } else {
            appendStatus $::cmonLDAS::statusw($page) "start LDAS failed: $pid"
        }
		
        ;## wait about 2 mins and find out by getting the status page 
		;## will reset to state 0 when cntlmonAPI disconnects
		if	{ [ regexp {^\d+} $pid ] } {
			lappend ::cmonLDAS::terms $pid
			after 60000 ::cmonLDAS::killTerms
		}
		$::cmonLDAS::titf3($page) configure -text "LDAS System at $::cmonClient::var(site)"
		state0 $page	
		;## do not reconnect for now	
		set ::cmonLDAS::pullId($page) \
		[ after 10000 [ list cmonLDAS::pullStatus $page $::cmonClient::var(site) ] 1 ]	
		;## this will allow login dialog to come up
		after 2000 "set ::cmonLDAS::var($page,mgrcmd) sHuTdOwN; set ::cmonClient::noPass 0"
}

#******************************************************** 
##
## Name: cmonLDAS::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonLDAS::state0 { page }  {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonLDAS::topfw($page) normal	
    if  { [ regexp API $page ] } {
        set enableCmd sendRequest 
    } else {
        set enableCmd LDAScontrol
    }
	$::cmonLDAS::bstart($page) configure -state normal \
	-command "${name}::${enableCmd} $page"
	;## disable cntlmon functions except bootstrap
	catch {
		if	{ [ string match cntlmon $::cmonLDAS::var($page,api) ] } {
			cmonLDAS::cntlmonOpts $page
		} 
	}
}

#******************************************************** 
##
## Name: cmonLDAS::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonLDAS::state1 { page { disableCmd cancelRequest } } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonLDAS::topfw($page) disabled
	$::cmonLDAS::bstart($page) configure -state disabled
	after 1000
}

#******************************************************** 
##
## Name: cmonLDAS::cntlmonOpts 
##
## Description:
## only enable bootstrap for cntlmonAPI
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::cntlmonOpts { page } {
	set ::cmonLDAS::var($page,mgrcmd) bootstrap
	foreach cmdw $::cmonLDAS::notcntlmonw {
		$cmdw configure -state disabled
	}
} 


#******************************************************** 
##
## Name: cmonLDAS::notCntlmonOpts 
##
## Description:
## enable the disabled functions for non cntlmonAPI 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::nonCntlmonOpts { page } {
	foreach cmdw $::cmonLDAS::notcntlmonw {
		$cmdw configure -state normal
	}
} 


#******************************************************** 
##
## Name: cmonLDAS::killTerms 
##
## Description:
## rm rxvt term used to start LDAS
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::killTerms {} {
	foreach page $::cmonLDAS::pages {
		foreach pid $::cmonLDAS::terms {
			catch { exec kill -9 $pid } err
		}
	}
}

## ******************************************************** 
##
## Name: cmonLDAS::pullStatus  
##
## Description:
## keep retrieving status page to see if the command completed
##
## Parameters:
## 
##
## Usage:
##  
## Comments:
## repeat every minute for 3 minutes
## due to password request auto-reconnections can be pretty hairy

proc cmonLDAS::pullStatus { page site { connect 0 } { retry 0 } { delay 5000 } } {

	if	{ [ catch {
		foreach { host port } $::siteport($site) { break }
		set html [ getUrl http://${host}${::STATUS_HTML} ] 
		updateDisplay $::cmonLDAS::textw($page) $html
		foreach { retry_max delay } $::cmonLDAS::timeout($page) { break }
		incr retry 1
		if	{ $retry < $retry_max } {
			set ::cmonLDAS::pullId($page) \
			[ after $delay [ list cmonLDAS::pullStatus $page $site $connect $retry $delay ] ]
		} else {
			if	{ $connect } {
				appendStatus $::cmonLDAS::statusw($page) "reconnecting to $site" 0 blue
				cmonClient::retryConnect $site $delay 
                if  { $::cmonClient::state == 1 } {
                    if  { [ regexp API $page ] } {
                        state0 $page 
                    } else {
                        state0 $page
                    }
                }
			}
		}
	} err ] } {
		appendStatus $::cmonLDAS::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLDAS::pageRaise   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## top page just page raise the current page


proc cmonLDAS::pageRaise { page } {
	if	{ [ string match runLDAS $::cmonLDAS::var($page,mgrcmd) ] } {
		set nopass 1 
	} else {
		set nopass 0
	}
    cmonCommon::pageRaise cmonLDAS $page $page $nopass
    
}

##******************************************************** 
##
## Name: cmonLDAS::traceUpdateMachines 
##
## Description:
## update database name selection when site is changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::traceUpdateMachines { args } {

		if	{ [ catch {
			set page [ lindex $args 0 ]
            cmonCommon::createLDASmachinesCombo cmonLDAS $page [ set ::cmonLDAS::${page}(machCombo) ]
            update idletasks
		} err ] } {
			catch { appendStatus $::cmonLDAS::statusw($page) $err }
		}
}

## ******************************************************** 
##
## Name: cmonLDAS::pageUpdate   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::pageUpdate { page } {
    updateDisplay $::cmonLDAS::textw($page) ""
    cmonCommon::pageUpdate cmonLDAS $page $page
}

##*******************************************************
##
## Name: cmonStatistics::reset
##
## Description:
## reset data
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::reset { page } {
    
    cmonCommon::updateDBinfo cmonLDAS $page
    

}

# ******************************************************** 
##
## Name: cmonLDAS::expireKey  
##
## Description:
## expire the key entry value if no action
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDAS::expireKey { page } {

    if  { [ catch {
        if  { ! [ info exist ::cmonLDAS::keyLastUpdate($page) ] } {
            set ::cmonLDAS::keyLastUpdate($page) [ clock seconds ]
        }
        set lastupdate [ set ::cmonLDAS::keyLastUpdate($page) ]
        set now [ clock seconds ]
        set delta [ expr $now - $lastupdate ]
        if  { $delta > $::MAX_KEY_SECS } {
            set ::cmonLDAS::var($page,key) ""
            set ::cmonLDAS::keyLastUpdate($page) $now
        }
    } err ] } {
        puts "[info level 0 ] error: $err"
    }
    after [ expr $::MAX_KEY_SECS * 1000 ] [ list cmonLDAS::expireKey $page ]
}
            
