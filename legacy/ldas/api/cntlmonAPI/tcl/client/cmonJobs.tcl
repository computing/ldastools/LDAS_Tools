## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This package handles LDAS job queue control
## 
## cmonJobs version 1.0
##
## ******************************************************** 
;##barecode

package provide cmonJobs 1.0

;## use a fixed font for list to align fields
namespace eval cmonJobs {
    set pages { cmonJobs:Jobs }
    set pagetext "Active Jobs"
    set fmtstr "%-10s|%-13.13s|%-41.41s|%-10.10s|%-15.15s|%10.10s|%10.10s"
    set fgcolor #2c348e
    set font $::LISTFONT
    set jobacts1  [ list "Get LDAS Jobs" "get list of running jobs" "cmonJobs::sendRequest getLDASJobInfo" black ]
	set jobacts2 [ list "Abort all" "Select all jobs to abort" "cmonJobs::abortAll" red \
		"Undo all aborts" "Undo jobs to abort" "cmonJobs::undoAll" blue \
		"Submit Changes" "send request to server" "cmonJobs::sendRequest setLDASJobChgs" black ]
    set numacts1 [ expr [ llength $jobacts1 ] / 3 ]
	set numacts2 [ expr [ llength $jobacts2 ] / 3 ]
    set wJobChg .tmpJobChg
	set jobidlen 10
	set cmdtypelen 41
	set userlen 15
	set apilen  10 
	set totaltimelen 10
	set apitimelen 10
}
#end


## ******************************************************** 
##
## Name: cmonJobs::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::create { notebook } {

    set page [ lindex $::cmonJobs::pages 0 ] 
    set ::cmonJobs::prevsite($page) none
    $notebook insert end cmonJobs -text "Jobs" \
        -createcmd "cmonJobs::createPages $notebook" 
}

## ******************************************************** 
##
## Name: cmonJobs::createPages 
##
## Description:
## creates Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::createPages { notebook0 } {

    if  { [ catch {
    	set frame [ $notebook0 getframe cmonJobs ]
        
		;## build notebook to hold all APIs
		set ::cmonJobs::notebook [ NoteBook $frame.nb \
        -foreground white -background $::darkorange -bd 2 ]	
		set notebook $::cmonJobs::notebook
		            
        $notebook0 itemconfigure cmonJobs -raisecmd \
            "cmonCommon::toppageRaise $notebook"
        
        cmonStatistics::create $notebook cmonJobs
        
		;## create job page
		set page cmonJobs:Jobs
	    set frame [$notebook insert end $page -text $::cmonJobs::pagetext \
            -raisecmd "cmonCommon::pageRaise cmonJobs $page $::cmonJobs::pagetext" ]
            
        set topf  [ frame $frame.topf ]
	    specific $topf $page
		pack $topf -fill x -pady 2
		foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
			
	    set titf2 [TitleFrame $pane2.titf2 -text "Command status" -font $::ITALICFONT ]     
        ;## create a status to display errors and status
	    set statusw [ ::createStatus  [ $titf2 getframe ] ]
	    array set ::cmonJobs::statusw [ list $page $statusw ]
		pack $titf2 -padx 2 -padx 4 -fill both -expand yes 
		
        set titf3 [TitleFrame $pane3.titf3 -text "Active Jobs" -font $::ITALICFONT ]
		set ::cmonJobs::titf3($page) $titf3 
        jobQueue [ $titf3 getframe ] $page 
        array set ::cmonJobs::state [ list $page 0 ]
		pack $titf3 -pady 2 -padx 4 -fill both -expand yes
		pack $pw1 -side top -fill both -expand 1
	    set ::cmonJobs::prevsite($page) $::cmonClient::var(site)
        
		cmonTree::create $notebook cmonJobs
        cmonJobSummary::create $notebook cmonJobs
        
    } err ] } {
        puts "cmonJobs::create $err"
        return -code error $err
    }
    $notebook compute_size
    pack $notebook -fill both -expand yes -padx 4 -pady 4
	$notebook raise [ $notebook page 0 ]
    return $frame
}

## ******************************************************** 
##
## Name: cmonJobs::specific 
##
## Description:
## creates LDAS Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::specific { parent page } {

	set labfent [LabelFrame $parent.fent -text "Select LDAS jobs to abort" -side top -anchor w \
                   -relief sunken -borderwidth 4]
	set subf1 [ $labfent getframe ]	
	
	set bbox1 [ ButtonBox $subf1.finfo1 -spacing 0 -default -1 -homogeneous 1 ]
	
    foreach { action help cmd color } $::cmonJobs::jobacts1 {
        $bbox1 add -highlightthickness 2 -relief raised \
        -borderwidth 3 -width 20 \
            -text $action -helptext $help \
            -command "$cmd $page" -fg $color -state normal
    }
    set ::cmonJobs::bbox1($page) $bbox1
	
	set bbox2 [ ButtonBox $subf1.finfo2 -spacing 0 -default -1 -homogeneous 1 ]
	
    foreach { action help cmd color } $::cmonJobs::jobacts2 {
        $bbox2 add -highlightthickness 2 -relief raised \
        -borderwidth 3 -width 20 \
            -text $action -helptext $help \
            -command "$cmd $page" -fg $color -state disabled
    }
    set ::cmonJobs::bbox2($page) $bbox2
    pack $bbox1 $bbox2 -side left -anchor w 
	# $bbox1 itemconfigure 0 -state normal
	pack $labfent -side left -anchor n -fill both -expand yes -padx 8 -pady 5
}

## ******************************************************** 
##
## Name: cmonJobs::jobQueue
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::jobQueue { parent page } {

    set pw1   [PanedWindow $parent.pw1 -side top ]
    set pane3 [$pw1 add -minsize 100]
    set pane  [$pw1 add -minsize 100]

    set pw2   [PanedWindow $pane.pw2 -side right ]
    set pane1 [$pw2 add -minsize 70]

    ;## list of jobs on the left pane
    set sw    [ScrolledWindow $pane3.sw]
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }
	set lbhdr [listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single \
        -font $::LISTFONT ]
	$lbhdr insert end \
        [ format $::cmonJobs::fmtstr ABORT " JOBID " "  LDAS COMMAND  " " API " \
			" USER " "TOTAL TIME" " API TIME " ] 
	$lbhdr itemconfigure 0 -fg black
	
    $sw setwidget $lbhdr
	set sf [ScrollableFrame $sw.f -height 1000 -areaheight 0 ]
	
    $sw setwidget $sf	
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $sf ] ]
	
	set subf [$sf getframe]
	set subfh [ frame $subf.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonJobs::varFrame($page) $subfh
	set ::cmonJobs::varFrameParent($page) [ winfo parent $subfh ]
	pack $sw -fill both -expand yes	
	
    ;## second pane to hold job details and job modification
    
    set sw2 [ScrolledWindow $pane1.sw -relief sunken -borderwidth  3 ]
    set sf [ScrollableFrame $sw2.f]
    $sw2 setwidget $sf
    set subf [$sf getframe]
    set msg [ message $subf.msg -text "LDAS Job Details" -justify left \
        -aspect 200 ]
    pack $msg -side top -fill x -expand yes
    set ::cmonJobs::details($page) $msg
    
    pack $sw2 -side top -fill both -expand yes
    pack $pw1 $pw2 -fill both -expand yes
}

## ******************************************************** 
##
## Name: cmonJobs::sendRequest 
##
## Description:
## send request 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::sendRequest { request page } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonJobs::statusw($page) "Please select an LDAS site"
		return 
	}

    ;## if password ok,submit request
	
	set login ""
	set passwd ""
	
    set cmdId "new"
    set repeat 0
    set freq 0      
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set done 0
    
    switch $request {
        getLDASJobInfo { set ::cmonJobs::askpass 0 }
        setLDASJobChgs { 
			dispChgs $page
			if	{ ! $::cmonJobs::var(submitOK) } {
                set ::cmonJobs::askpass 0
				return
			}
            if  { [ catch { 
                if  { $::cmonJobs::askpass == 1 } {	
                    foreach { login passwd } [ validateLogin 1 ] { break }
                } else {
                    foreach { login passwd } [ validateLogin 0 ] { break }
                }
			    if	{ [ string equal cancelled $login ] } {
				    set done 1
			    } else {
                	ckPriviledged
                	set request "$request \{$::cmonJobs::var(abort)\}"
			    	appendStatus $::cmonJobs::statusw($page) "aborting jobs $::cmonJobs::var(abort)" 0 blue
                }
            } err ] } {
                error $err
            }
        }
    }
    set ::cmonJobs::var(abort) [ list ]
    if  { $done } {
        return
    }
    if  { [ catch {
        if  { $::cmonJobs::askpass == 1 } {
            set cmd "cmonJobs::showReply $page\n$repeat\n$freq\n\
                $client:$cmdId:$login:$passwd\ncntlmon::$request" 
        } else {
            set cmd "cmonJobs::showReply $page\n$repeat\n$freq\n\
                $client:$cmdId\ncntlmon::$request"
        }
        cmonJobs::state1 $page 
        set ::cmonJobs::askpass 0
		sendCmd $cmd 
	} err ] } {
		 cmonJobs::state0 $page 
         appendStatus $::cmonJobs::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonJobs::showReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::showReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
	set cmonClient::client $client
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    #puts "in showReply, page=$page,rc=$rc, client=$client,cmdId=$cmdId, afterid=$afterid"    
	switch $rc {
	0 {
        set ::cmonJobs::cmdId($page) $cmdId
		set ::cmonJobs::numJobs [ cmonJobs::dispJobs $page $html ]
        if  { $::cmonJobs::numJobs } {
            cmonJobs::state2 $page 
            appendStatus $::cmonJobs::statusw($page) "$::cmonJobs::numJobs jobs in queue." 1 blue
        } else {
            cmonJobs::state0 $page 
			appendStatus $::cmonJobs::statusw($page) "No jobs found." 0 blue
        }               
	  }
    1 {
        cmonJobs::state0 $page
		appendStatus $::cmonJobs::statusw($page) $html 0 "blue"
      }
	3 {
		cmonJobs::state0 $page
        appendStatus $::cmonJobs::statusw($page) $html 1   
	  }
	}	
}

#******************************************************** 
##
## Name: cmonJobs::selectedJob 
##
## Description:
## job selected for abort
##
## Parameters:
## widget - checkbutton
##
## Usage:
##  sets color for selection, de-select, sets log filter
## 
## Comments:

proc cmonJobs::selectedJob { button page jobid } {

	if	{ $::cmonJobs::delete($page,$jobid) } {
		$button configure -fg red
	} else {
		$button configure -fg black
	}
}

#******************************************************** 
##
## Name: cmonJobs::dispJobs 
##
## Description:
## display the jobs in LDAS queue in listbox
##
## Parameters:
## page
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:

proc cmonJobs::dispJobs { page html } {

	if	{ [ catch {
		;## create a header 
		set parent $::cmonJobs::varFrameParent($page) 
		;## must destroy widget in parseRsc or globalVar array cannot be unset
		catch { destroy $::cmonJobs::varFrame($page) }
	
		set subfh [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
		set ::cmonJobs::varFrame($page) $subfh
		# set ::cmonJobs::varFrameParent($page) [ winfo parent $subfh ]
	
		set result [ split $html \n ]
		set numJobs [ llength $result ] 
		set i 0 
		if	{ $numJobs } {
    		foreach job $result {
				if	{ [ llength $job ] == 7 } {
					foreach [ list jobid cmdtype api user totaltime apitime cmd ] $job { break }
				} else {
					foreach [ list jobid cmdtype api totaltime apitime cmd ] $job { break }
					set user unknown
				}
				set subf1 [ frame $subfh.f$i -relief sunken ]
				if	{ $i == 0 } {
					set eventcmd "jobDetails $page $subf1 $i"
				}
				checkbutton $subf1.del$i -width 6 \
					-text Abort -justify left -variable ::cmonJobs::var($jobid,delete) \
					-onvalue 1 -offvalue 0 -command "$subf1.jobid$i configure -fg red" \
					-anchor w -font $::LISTFONT	-relief raised -bg PapayaWhip \
					-fg red
					
				button $subf1.jobid$i -width $::cmonJobs::jobidlen \
					-text $jobid -justify left  \
					-anchor w -font $::LISTFONT	-relief ridge -bg white 
			    
                bind $subf1.jobid$i <Button-1> [ list cmonJobs::jobDetails $page $subf1 $i ]
                bind $subf1.jobid$i <Button-3> [ list cmonLogs::autoLogFilter $jobid ]
                
				button $subf1.cmdtype$i -width  $::cmonJobs::cmdtypelen -justify right \
					-text $cmdtype -fg brown \
					-anchor w -font $::LISTFONT	-relief ridge -bg gray 
		
				# bind $subf1.cmdtype$i <Button-2> [ list cmonJobs::jobtoLog $page $subf1 $i ]
			
				label $subf1.api$i -width $::cmonJobs::apilen -justify left \
					-text $api -fg black \
					-anchor w -font $::LISTFONT	-relief ridge -bg white 
		
				label $subf1.user$i -width $::cmonJobs::userlen \
					-text $user -fg brown -justify left \
					-anchor w -font $::LISTFONT	-relief ridge -bg gray		
				
				label $subf1.totaltime$i -width $::cmonJobs::totaltimelen \
					-text $totaltime -fg black -justify right \
					-anchor e -font $::LISTFONT	-relief ridge -bg white	
					
				label $subf1.apitime$i -width $::cmonJobs::apitimelen \
					-text $apitime -fg brown -justify right \
					-anchor e -font $::LISTFONT	-relief ridge -bg gray	
											
				pack $subf1.del$i $subf1.jobid$i $subf1.cmdtype$i $subf1.api$i $subf1.user$i \
					$subf1.totaltime$i $subf1.apitime$i \
					-side left  -fill both -anchor w
				pack $subf1 -side top -fill x -expand 1
				incr i 1
				set ::cmonJobs::jobqueue($page,$jobid) $cmd
			}
			pack $subfh -side top -fill both -expand 1
			eval $eventcmd
    } else {
        $::cmonJobs::details($page) configure -text "No jobs found" -fg #5c10cc \
            -font $::cmonJobs::font
    }
	} err ] } {
		appendStatus $::cmonJobs::statusw($page) $err
		return -code error $err
	}
    return $numJobs 
}
## ******************************************************** 
##
## Name: cmonJobs::jobDetails
##
## Description:
## display details of a job on right pane window
##
## Parameters:
## job listbox widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::jobDetails { page parent index } {
	set jobid [ $parent.jobid$index cget -text ]
	set cmdtype [ $parent.cmdtype$index cget -text ]
	set api [ $parent.api$index cget -text ]
	set user [ $parent.user$index cget -text ]

    set details "Jobid:\t$jobid\n\
			ldas cmd:\t$cmdtype\n\
			ldas API:\t$api\n\
			ldas user:\t$user\n\
			Command:\t$::cmonJobs::jobqueue($page,$jobid)"
    $::cmonJobs::details($page) configure -text $details -fg #5c10cc \
         -font $::cmonJobs::font	
}

## ******************************************************** 
##
## Name: cmonJobs::abortAll
##
## Description:
## reset job marked killed
##
## Parameters:
## page
##
## Usage:
##  
## 
## Comments:

proc cmonJobs::abortAll { page } {

	set ::cmonJobs::var(abort) [ list ]
	set subfh $::cmonJobs::varFrame($page)
	for { set i 0 } { $i < $::cmonJobs::numJobs } { incr i 1  } {
		set subf1 $subfh.f$i 
		set jobid [ $subf1.jobid$i cget -text ]
		set ::cmonJobs::var($jobid,delete) 1
		$subf1.jobid$i configure -fg red
	}
}

## ******************************************************** 
##
## Name: cmonJobs::undoAll
##
## Description:
## reset job marked killed
##
## Parameters:
## page
##
## Usage:
##  
## 
## Comments:

proc cmonJobs::undoAll { page } {

	set ::cmonJobs::var(abort) [ list ]
	set subfh $::cmonJobs::varFrame($page)
	for { set i 0 } { $i < $::cmonJobs::numJobs } { incr i 1  } {
		set subf1 $subfh.f$i 
		set jobid [ $subf1.jobid$i cget -text ]
		if	{ $::cmonJobs::var($jobid,delete) } {
			set ::cmonJobs::var($jobid,delete) 0
			$subf1.jobid$i configure -fg black
		}
	}
}

## ******************************************************** 
##
## Name: cmonJobs::updateChgs
##
## Description:
## set the list of jobs to be killed or to adjust priority
##
## Parameters:
## page
## create   - option to create dialog
##
## Usage:
##  
## 
## Comments:
## sets the list of jobs to be killed and jobs to adjust priority

proc cmonJobs::updateChgs { page } { 	

	set subfh $::cmonJobs::varFrame($page)
    set cntlloginexist [ info exist ::cntllogin ]

    set ::cmonJobs::askpass 0

	for { set i 0 } { $i < $::cmonJobs::numJobs } { incr i 1  } {
		set subf1 $subfh.f$i 
		set jobid [ $subf1.jobid$i cget -text ]
        set user  [ $subf1.user$i cget -text  ]
		if	{ $::cmonJobs::var($jobid,delete) } {
            lappend ::cmonJobs::var(abort) $jobid
            if  { $cntlloginexist } {
                continue
            }
            if	{ $::USE_GLOBUS } {
            	if	{ [ string match $user $::globusUser ] } {
                	set ::cmonJobs::askpass 1
                }
            } elseif  { ! [ string match $user $::logintext ] } {
                set ::cmonJobs::askpass 1
            }     
        }
	}
}

## ******************************************************** 
##
## Name: cmonJobs::dispChgs
##
## Description:
## display on popup the list of jobs to be killed or
## to adjust priority
##
## Parameters:
## page
## create   - option to create dialog
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobs::dispChgs { page } {

	if	{ [ catch {
    	updateChgs $page
		if	{ [ llength $::cmonJobs::var(abort) ] } {
    		set dialogw $::cmonJobs::wJobChg
    		if  { ! [ winfo exist $dialogw ] } {
    			set dlg [Dialog $dialogw -parent . -modal local \
   				-separator 1 -title   "Abort Jobs" \
       			-side bottom -anchor  s -default 0 -cancel 1]
				set dlgframe [ $dlg getframe ]
    			;## create a label for window
				set lbl1 [ label $dlgframe.lbl1 -text "Jobs to be aborted" ]
				pack $lbl1 -side top -expand no -fill x	
    			set lbl2 [ label $dlgframe.lbl2 -text [ join $::cmonJobs::var(abort) \n ] \
				-fg red -justify left -relief sunken -font $::MSGFONT ]
    			pack $lbl2 -side top -expand 1 -fill x	
				$dlg add -name 0 -text OK -command "set ::cmonJobs::var(submitOK) 1; destroy $dlg"
				$dlg add -name cancel -command "set ::cmonJobs::var(submitOK) 0; destroy $dlg" 
				$dlg draw
			}
		} else {
			set ::cmonJobs::var(submitOK) 0
			error "No changes made"
		}
	} err ] } {
		appendStatus $::cmonJobs::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonJobs::state0
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonJobs::state0 { page } {
 
 	catch { destroy $::cmonJobs::varFrame($page) }
	catch { unset ::cmonJobs::var }
	catch { unset ::cmonJobs::jobqueue }
    $::cmonJobs::details($page) configure -text ""    
    $::cmonJobs::bbox1($page) itemconfigure 0 -state normal
	$::cmonJobs::bbox2($page) itemconfigure 0 -state disabled
	$::cmonJobs::bbox2($page) itemconfigure 1 -state disabled
	$::cmonJobs::bbox2($page) itemconfigure 2 -state disabled
	catch { destroy $::cmonJobs::wJobChg }
}

## ******************************************************** 
##
## Name: cmonJobs::state1
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonJobs::state1 { page } {

	$::cmonJobs::bbox1($page) itemconfigure 0 -state disabled
	$::cmonJobs::bbox2($page) itemconfigure 0 -state disabled
	$::cmonJobs::bbox2($page) itemconfigure 1 -state disabled
	$::cmonJobs::bbox2($page) itemconfigure 2 -state disabled
}

## ******************************************************** 
##
## Name: cmonJobs::state2
##
## Description:
## ready to send request to process LDAS job changes
## after job list is displayed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonJobs::state2 { page } {
	
	$::cmonJobs::bbox1($page) itemconfigure 0 -state normal
	$::cmonJobs::bbox2($page) itemconfigure 0 -state normal
	$::cmonJobs::bbox2($page) itemconfigure 1 -state normal
	$::cmonJobs::bbox2($page) itemconfigure 2 -state normal
}

## ******************************************************** 
##
## Name: cmonJobs::pageRaise   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonJobs::pageRaise { page } {
	set ::cmonClient::noPass 0
    set ::cmonClient::pageReset "cmonJobs::reset $page"
	set ::cmonClient::pageUpdate "cmonJobs::updateTitle3 $page"
	cmonJobs::updateTitle3 $page
    set name cmonJobs
    if  { [ info exist ::resetPageFlag(${name}:$page) ] } {
        if  { $::resetPageFlag(${name}:$page) } {
            ${name}::reset $page
        }
    }
}

## ******************************************************** 
##
## Name: cmonJobs::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonJobs::reset { page } {

    catch { destroy $::cmonJobs::varFrame($page) }
}
