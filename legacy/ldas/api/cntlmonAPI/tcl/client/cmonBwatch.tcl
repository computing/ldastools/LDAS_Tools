## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script displays the load and cpu usage information:
## on all the Bwatch nodes.
## 
## ******************************************************** 

package provide cmonBwatch 1.0

namespace eval cmonBwatch {
    set notebook ""
	set delay { 15 30 60 120 } 
	set repeat { 0 5 100 10000 } 
    set freq { 30 60 900 3600 } 
	set freqUnit 1000
	set freqUnits secs
	set fmtstr "\
%-6s|%8s|%5s|%6s|%7s|%7s|%8s|%10s|%8s|%8s|%8s|%8s|%8s|%8s"
	set pages [ list "Monitor_N_0-[ expr $::NODES_MONiTOR_PAGE - 1 ]" ]
	set prevsite none
    set tabindex 4
}

;## heading
;## node time #jobs #users load1 load5 load10 
;## cpuidle memtot memfree buffers cached 
;## swaptot swapfree

## ******************************************************** 
##
## Name: cmonBwatch::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBwatch::create { notebook } {
    set page [ lindex $::cmonBwatch::pages 0 ] 
    set cmonBwatch::prevsite none
    regsub -all {\_} $page { } pagetext 
    set ::cmonBwatch::notebook $notebook
    $notebook insert end cmonBwatch:$page -text $pagetext \
        -raisecmd "cmonBwatch::pageUpdate"
    cmonBwatch::createAllPages    
}

## ******************************************************** 
##
## Name: cmonBwatch::createPages 
##
## Description:
## creates Bwatch page
##
## Parameters:
## notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBwatch::createPages { notebook page } {

    regsub -all {\_} $page { } pagetext 
    set frame [ $notebook insert end cmonBwatch:$page -text $pagetext \
        -raisecmd [ list cmonBwatch::pageUpdate ] ]
        	
    set topf  [frame $frame.topf]

	set ::cmonBwatch::notebook $notebook

    specific $topf $page
	set ::cmonBwatch::topfw($page) $topf
	pack $topf -fill x -pady 2
	
	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	
    ;## create a status to display errors and status
	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonBwatch::statusw [ list $page $statusw ]
	pack $titf2 -pady 2 -padx 4 -fill both -expand yes

	set titf3 [TitleFrame $pane3.titf3 -text "Node Monitor" -font $::ITALICFONT ]
	;## create an empty list 
	nodeList [ $titf3 getframe ]  $page
    pack $titf3 -pady 2 -padx 4 -fill both -expand yes 
    set ::cmonBwatch::titf3($page) $titf3
    cmonCommon::pageUpdate cmonBwatch $page "Node Monitor"
    
	pack $pw1 -side top -fill both -expand 1
    array set ::cmonBwatch::state [ list $page 0 ]

	set ::cmonBwatch::nodeIndex($page) -1
    set ::cmonBwatch::prevsite $::cmonClient::var(site)
}

## ******************************************************** 
##
## Name: cmonBwatch::specific
##
## Description:
## creates specific action area for the page types
##
## Parameters:
## page
## parent widget  
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBwatch::specific { parent page } {

	set titf1 [TitleFrame $parent.titf1 -text \
	    "Beowulf Nodes Monitor" -font $::ITALICFONT ]
    set subf  [$titf1 getframe]
	set labfent [LabelFrame $subf.fent -text "Hit SUBMIT to start monitor" -side top -anchor w \
                   -relief sunken -borderwidth 4]
    set subf1 [ $labfent getframe ]

	regexp {N_(\d+)-(\d+)} $page -> first last
	set lbl [ label $subf1.monNodes -text "Monitor Nodes $first - $last" ]
    set ::cmonBwatch::nodeLabel($page) $lbl
  
	set but1 [ button $subf1.start -text "SUBMIT" -width 10 \
        -command  "::cmonBwatch::sendRequest $page Bwatch"  ]
	set ::cmonBwatch::bstart($page) $but1
    bind $but1 <Enter> "cmonBwatch::showActualNodes $but1 $page %x %y"
    bind $but1 <Leave> "cmonBwatch::popdownActualNodes"
	pack $subf1.monNodes $subf1.start -side left -padx 5 -pady 5
	pack $subf.fent -side left -padx 4 -fill both -expand 1
	set labrefresh [LabelFrame $subf.refresh -text "Repeat this request ( set repeat > 0 )" \
        -side top -anchor w -relief sunken -borderwidth 4]
	set subf2 [ $labrefresh getframe ]
	set name [ namespace tail [ namespace current ] ]
	createRepeatFreq $subf2 $name $page
	pack $subf.refresh -side top  
	pack $subf.refresh -side right -padx 4 -fill both -expand 1	  
    pack $titf1 -fill both -expand 1
    
}

## ******************************************************** 
##
## Name: cmonBwatch::nodeList
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the list of nodes and their usage
## 
## Comments:

proc cmonBwatch::nodeList { parent page } {

    set sw  [ScrolledWindow $parent.sw]	
	
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }
	set lbhdr [listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single \
        -font $::LISTFONT ]
	$lbhdr insert end [ format $::cmonBwatch::fmtstr Node Time #Jobs #users "1 min L" \
			"5 min L" "10 min L" "%CPU idle" "Mem Tot" "Mem Free" \
			"Buffers" "Cached" "Swap Tot" "Swap Free" ] 
	set ::cmonBwatch::lbhdr($page) $lbhdr
    set lb [listbox $sw.lb -height 100  \
        -highlightthickness 1 -selectmode single \
        -font $::LISTFONT -selectbackground orange ]
    set ::cmonBwatch::lbox($page) $lb
	$sw setwidget $lbhdr
    $sw setwidget $lb
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $lb ] ]
    pack $sw -side left -fill both -expand yes
}

## ******************************************************** 
##
## Name: cmonBwatch::sendRequest 
##
## Description:
## send request 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBwatch::sendRequest { page request } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonBwatch::statusw($page) "Please select an LDAS site" 
		return 
	}

    set cmdId "new"
    set repeat 0
    set freq 0      
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    regexp {N_(\d+)-(\d+)} $page -> first last
	if	{ [ regexp {cancel} $request ] } {
		set cmd "cntlmon::cancelMonitorPipe Bwatch $first-$last" 
		cmonBwatch::state3 $page 
	} else {
		set cmd "cntlmon::startMonitorPipe $request $::cmonBwatch::var($page,repeat) \
			[ expr $::cmonBwatch::var($page,freq)*$::cmonBwatch::freqUnit ] $first-$last" 
		cmonBwatch::state1 $page 
	}

    if  { [ catch {
        set cmd "cmonBwatch::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\n$cmd" 
        #puts "cmd=$cmd"
		sendCmd $cmd 
	} err ] } {
		 cmonBwatch::state0 $page 
         appendStatus $::cmonBwatch::statusw($page) $err 
	}
}

## ******************************************************** 
##
## Name: cmonBwatch::showReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonBwatch::showReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
	set cmonClient::client $client
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]

	switch $rc {
	0 { set ::cmonBwatch::cmdId($page) $cmdId
		if	{ [ regexp {\d+MB} $html ] } { 
			cmonBwatch::dispNode $page $html 
		} else {
			if	{ [ regexp -nocase {error} $html ] } {
				appendStatus $::cmonBwatch::statusw($page) $html 
			} else {
				appendStatus $::cmonBwatch::statusw($page) $html 0 blue
			}
		}
		if	{ [ regexp -nocase {completed} $html ] } {
			set index [ expr $::cmonBwatch::nodeIndex($page) + 1 ]
			catch { $::cmonBwatch::lbox($page) selection clear $index end }
			set ::cmonBwatch::nodeIndex($page) -1
		} 
		if	{ [ regexp -nocase {exit} $html ] } {
			cmonBwatch::state0 $page
		} else { 
			switch $::cmonBwatch::state($page) {
				1 { cmonBwatch::state2 $page }
				3 { cmonBwatch::state0 $page }
			}
		}
	  }
    1 { cmonBwatch::state0 $page
		appendStatus $::cmonBwatch::statusw($page) $html 0 blue
      }
	3 { cmonBwatch::state0 $page
        appendStatus $::cmonBwatch::statusw($page) $html    
	  }
	}	
}

## ******************************************************** 
##
## Name: cmonBwatch::dispNode
##
## Description:
## insert the newly arrived node data into the listbox
##
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:

proc cmonBwatch::dispNode { page html } {

	if	{ [ catch {
		if	{ [ string length $html ] } {
			set html [ split $html \n ]
			regexp {N_(\d+)-(\d+)} $page -> first last
			if	{ $::cmonBwatch::nodeIndex($page) >= $::NODES_MONiTOR_PAGE } {
				set ::cmonBwatch::nodeIndex($page) -1
			} 
			incr ::cmonBwatch::nodeIndex($page) 1
			set index $::cmonBwatch::nodeIndex($page)
			set lbox $::cmonBwatch::lbox($page) 
			foreach line $html {
				foreach { node time numJobs users load1 load5 load10 cpuIdle \
					memTot memFree buffers cached swapTot swapFree } $line { 
					break
				}
				if	{ [ expr $index % 2 ] } {
					set bg white
				} else {
					set bg yellow
				}
				;## deselect everything
				catch { $lbox selection clear 0 end }
				set line [ $lbox get $index $index ]
				if	{ [ string length $line ] } {
					$lbox delete $index $index
				}
				update idletasks
				$lbox insert $index [ format $::cmonBwatch::fmtstr $node $time \
					$numJobs $users $load1 $load5 $load10 $cpuIdle \
					$memTot $memFree $buffers $cached $swapTot $swapFree]
    			$lbox itemconfigure $index -bg $bg
				$lbox selection set $index $index
			}			
		}	
	} err ] } {
		appendStatus $::cmonBwatch::statusw($page) $err 1 red
	}
}

## ******************************************************** 
##
## Name: cmonBwatch::state0
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBwatch::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	#updateWidgets $::cmonBwatch::topfw($page) normal
	$::cmonBwatch::bstart($page) configure -state normal -text \
   		SUBMIT \
      	-command "::cmonBwatch::sendRequest $page Bwatch" \
      	-state normal 
	updateRepeat $name $page normal
	array set ::cmonBwatch::state [ list $page 0 ]	
}


## ******************************************************** 
##
## Name: cmonBwatch::state1
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBwatch::state1 { page } {
    set name [ namespace tail [ namespace current ] ]
	#updateWidgets $::cmonBwatch::topfw($page) disabled
    updateRepeat $name $page disabled
	$::cmonBwatch::bstart($page) configure -state disabled
	array set ::cmonBwatch::state [ list $page 1 ]
}

## ******************************************************** 
##
## Name: cmonBwatch::state2
##
## Description:
## ready to send request to process LDAS job changes
## after job list is displayed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBwatch::state2 { page } {
	if	{ $::cmonBwatch::state($page) != 2 } {
    	$::cmonBwatch::bstart($page) configure -text "CANCEL" \
        -state normal -command "cmonBwatch::sendRequest $page cancelBwatch" 
		array set ::cmonBwatch::state [ list $page 2 ]
	}
}

#******************************************************** 
##
## Name: cmonBwatch::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonBwatch::state3 { page } {
    $::cmonBwatch::bstart($page) configure -state disabled
	array set ::cmonBwatch::state [ list $page 3 ]
}


##******************************************************** 
##
## Name: cmonBwatch::createAllPages
##
## Description:
## add more pages for more nodes
## may have changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## sometimes node update has not arrived from server yet.
## only update if it has.

proc cmonBwatch::createAllPages { args } {

    set site [ string trim $::cmonClient::var(site) ]
    if  { ! [ info exist ::beowulfNodes($site) ] } {
        return
    }
    ;## raise pages only current or if Beowulf is raised
    set activePage [ $::cmonClient::notebook raise ]
  
	set currPages [ lrange [ $::cmonBwatch::notebook pages ] $::cmonBwatch::tabindex end ]
    foreach page [ lrange $currPages 0 end ] {
        catch { $::cmonBwatch::notebook delete $page }
    }
	if	{ $::cmonClient::state } {
		set numNodes [ llength $::beowulfNodes($site) ] 
		for { set i 0 } { $i < $numNodes } { incr i $::NODES_MONiTOR_PAGE } {
            set last [ expr $i + $::NODES_MONiTOR_PAGE - 1 ]
            if  { $last > $numNodes } {
                set last $numNodes
            }
			set page "Monitor_N_$i-$last"	
            cmonBwatch::createPages $::cmonBwatch::notebook $page
		}
		;## if this one has less nodes, remove the extra pages
        ;## update the page and label in page to reflect current number of nodes
        
        set currPages [ lrange [ $::cmonBwatch::notebook pages ] $::cmonBwatch::tabindex end ]
        
        set firstpage [ lindex $currPages 0 ]
        regexp {:(\S+)} $firstpage -> bwatchpage
		if	{ $numNodes < $::NODES_MONiTOR_PAGE } {
            $::cmonBwatch::notebook itemconfigure $firstpage -text "Monitor_N_0-$numNodes"
            $::cmonBwatch::nodeLabel($bwatchpage) configure -text "Monitor Nodes 0 - $numNodes" 
		} else {
            set last [ expr $::NODES_MONiTOR_PAGE - 1 ]
            $::cmonBwatch::notebook itemconfigure $firstpage -text "Monitor_N_0-$last"
            $::cmonBwatch::nodeLabel($bwatchpage) configure -text "Monitor Nodes 0 - $last" 
        }
	} else {        
        set page "Monitor_N_0-[ expr $::NODES_MONiTOR_PAGE-1 ]"
        cmonBwatch::createPages $::cmonBwatch::notebook $page
		set ::cmonBwatch::pages $page
	}

    if  { [ regexp {cmonBwatch} $::cmonClient::pageUpdate ] } {
        $::cmonBwatch::notebook raise [ $::cmonBwatch::notebook page 3 ]
    } else {
        $::cmonClient::notebook raise $activePage
    }
    update idletasks
}

##******************************************************** 
##
## Name: cmonBwatch::showActualNodes
##
## Description:
## show order of node in the list 
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  

proc cmonBwatch::showActualNodes { button page x y } {

    catch {
	set labelname .lblframe
    regexp {N_(\d+)-(\d+)} $page -> first last
    set text [ list ]
    set site [ string trim $::cmonClient::var(site) ]
    foreach node [ lrange $::beowulfNodes($site) $first $last ] {
        lappend text $node
    }
    set text [ join $text ", "]
    set text "Actual node names:\n$text"
    message $labelname -font $::LISTFONT -text $text \
		-justify left -aspect 500 -anchor w -foreground black -relief ridge \
        -bg yellow
    place $labelname -in $button -x $x -y $y 
    } err 

}

##******************************************************** 
##
## Name: cmonBwatch::popdownActualOrder
##
## Description:
## remove label when out of the node
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  

proc cmonBwatch::popdownActualNodes {} {

    catch { destroy .lblframe } err 

}

##******************************************************** 
##
## Name: cmonBwatch::pageUpdate
##
## Description:
## update the options button for the nodes since the site
## may have changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## called by trace also
## still register with cmonClient

proc cmonBwatch::pageUpdate { args } {
	if	{ [ catch {	
        set ::cmonClient::noPass 0
	    set ::cmonClient::pageUpdate "cmonBwatch::pageUpdate"
        ;## update is done by cmonNodes
        
	} err ] } { 
        puts "[myName] $err"
	}
}

