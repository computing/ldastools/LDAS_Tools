## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon client Tcl Script.
##
## This script handles API status and database information
##
## currently database page is disabled
## ******************************************************** 
;##barecode

package provide cmonStatus 1.0

namespace eval cmonStatus {
	set pages { "API-Status" "NTP-Server-Diagnostics" \
		API-State-Summary }
	array set cmd { API-Status apiStatusPage }
	array set cmd { NTP-Server-Diagnostics apiTimes }
	array set cmd { API-State-Summary apiStateSummary }
    set repeat { 0 5 100 10000 } 
    set freq { 60 120 900 3600 } 
    set notebook ""
    set freqUnit 1000  
	set freqUnits secs
	set APIs $::API_LIST(no-server) 
}

#end

## ******************************************************** 
##
## Name: cmonStatus::create 
##
## Description:
## creates all status pages
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonStatus::create { nb } {

    set fnb [$nb insert end cmonStatus -text "Status"]
	$nb itemconfigure cmonStatus \
		-raisecmd [ list cmonCommon::pageRaise cmonStatus \
            [ lindex $::cmonStatus::pages 0 ] "API Status" ]	
	;## build notebook to hold all status types
	set ::cmonStatus::notebook [ NoteBook $fnb.nb \
        -foreground white -background $::darkgreen -bd 2 \
		-activeforeground white -activebackground blue ] 
	set notebook $::cmonStatus::notebook
	foreach page $::cmonStatus::pages {
	
	regsub -all {\-} $page { } pagetext
	set frame [$notebook insert end cmonStatus:${page} -text $pagetext ]
	$notebook itemconfigure cmonStatus:${page} \
		-raisecmd [ list cmonCommon::pageRaise cmonStatus $page Status ]	
 	set topf  [ frame $frame.topf ]
	
	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	
    specific $topf $page	
	set ::cmonStatus::topfw($page) $topf
	pack $topf -fill x -pady 2
	
	;## create a status to display errors and status
	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonStatus::statusw [ list $page $statusw ]
	pack $titf2 -pady 2 -padx 4 -fill both -expand yes
	
	set titf3 [TitleFrame $pane3.titf3 -text $page -font $::ITALICFONT ]
	set ::cmonStatus::titf3($page) $titf3 
	set textw [ ::createDisplay [ $titf3 getframe ] ]
    array set ::cmonStatus::textw [ list $page $textw ]
	pack $titf3 -pady 2 -padx 4 -fill both -expand yes
	
	$notebook compute_size
	pack $notebook -fill both -expand yes -padx 4 -pady 4
    set ::cmonStatus::prevsite($page) $::cmonClient::var(site)
    array set ::cmonStatus::state [ list $page 0 ]
	pack $pw1 -side top -fill both -expand 1
	}
	$notebook raise [ $notebook page 0 ]
    return $frame
}


## ******************************************************** 
##
## Name: cmonStatus::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonStatus::specific { parent page } {

    set labf1 [LabelFrame $parent.labf1 -text "Select one or more APIs and press SUBMIT" -side top -anchor w \
                   -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]

	;## select APIs 
	set name [ namespace tail [ namespace current ] ]	
	createAPICheckList $subf $name $page 
    set labref [LabelFrame $parent.refresh -text "Repeat this request ( set repeat > 0 )" -side top -anchor w \
                   -relief sunken -borderwidth 4]
                   
    set subf2  [ $labref getframe ]
	set name [ namespace tail [ namespace current ] ]
	set ::${name}::var($page,apilist) [ list ]
	createRepeatFreq $subf2 cmonStatus $page
	set but [ Button $subf2.start -text "SUBMIT" -helptext "submit request to server" \
        -command  "::cmonStatus::sendRequest $page " ]
	set ::cmonStatus::bstart($page) $but
	pack $but -side top -padx 20 -pady 20
	pack $labf1 $labref -side left -fill both -expand 1

}


## ******************************************************** 
##
## Name: cmonStatus::sendRequest 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonStatus::sendRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonStatus::statusw($page) "Please select an LDAS site" 
		return 
	}
	set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set cmdId "new"
    set freq [ expr $::cmonStatus::var($page,freq)*$::cmonStatus::freqUnit ]
    set repeat $::cmonStatus::var($page,repeat)
	setAPIs $page $name
	if	{ ! [ llength $::cmonStatus::var($page,apilist) ] } {
		appendStatus $::cmonStatus::statusw($page) "Please select one or more APIs."
		return
	} 
	;## this is a broadcast function 
	if	{ [ catch {
        #set cmd "cmonStatus::showReply $page\n0\n0\n\
        ;##$client:$cmdId\ncntlmon::getFile $page" 
		set cmd "cmonStatus::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::$::cmonStatus::cmd($page) \{$::cmonStatus::var($page,apilist)\}"
		#puts "cmd=$cmd"
        cmonStatus::state1 $page 
		appendStatus $::cmonStatus::statusw($page) "Working on it, please wait ..." 0 blue
		sendCmd $cmd              
	} err ] } {
        cmonStatus::state0 $page 
		appendStatus $::cmonStatus::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonStatus::cancelRequest
##
## Description:
## cancels the previous request which repeats
##
## Parameters:
## parent widget
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonStatus::cancelRequest { page } {
	set name [ namespace tail [ namespace current ] ]
	set cmdId $::cmonStatus::cmdId($page)
    set client $::cmonClient::client

    set repeat 0
    set freq 0
	if	{ [ catch {
		set cmd "cmonStatus::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\n${client}::cancel $cmdId" 
		#puts "cmd=$cmd"
        cmonStatus::state3 $page 
		sendCmd $cmd 
	} err ] } {
        cmonStatus::state0 $page 
		appendStatus $::cmonStatus::statusw($page) $err 
	}
}

## ******************************************************** 
##
## Name: cmonStatus::showReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonStatus::showReply { page rc clientCmd html } {  
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    #puts "showReply, $page, client=$client,cmdId=$cmdId, afterid=$afterid,html [string length $html]"
	set ::cmonStatus::cmdId($page) $cmdId 
    set ::cmonClient::client $client

	switch $rc {
	0 -
	2 {	if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
            if  { [ regexp {^<html} $html ] } {
                updateDisplay $::cmonStatus::textw($page) $html
            } else {
                appendStatus $::cmonStatus::statusw($page) $html
            }
        } else {
            appendStatus $::cmonStatus::statusw($page) \
                "No data"
            updateDisplay $::cmonStatus::textw($page) ""   
        }
		if	{ ! [ string length $afterid ] } {
                cmonStatus::state0 $page 
            } else {
                cmonStatus::state2 $page 
        }
		# for broadcast only
		#cmonStatus::state${rc} $page
	  }
	3 {
		appendStatus $::cmonStatus::statusw($page) $html
		;## may need to cancel request	
		cmonStatus::state0 $page 	
	  }
	}	
}

#******************************************************** 
##
## Name: cmonStatus::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonStatus::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonStatus::topfw($page) normal
	regsub -all {\-} $page { } pagetext
	# updateTitle3 $page "$pagetext"
    updateRepeat $name $page normal
	array set ::cmonStatus::state [ list $page 0 ]
}

#******************************************************** 
##
## Name: cmonStatus::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonStatus::state1 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonStatus::topfw($page) disabled	
    $::cmonStatus::bstart($page) configure -state disabled 
    updateRepeat $name $page disabled
	array set ::cmonStatus::state [ list $page 1 ]
}

#******************************************************** 
##
## Name: cmonStatus::state2  
##
## Description:
## request is repeated, enable cancel only
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatus::state2 { page } {
	updateWidgets $::cmonStatus::topfw($page) disabled	
    $::cmonStatus::bstart($page) configure -text "CANCEL" \
        -state normal -command "cmonStatus::cancelRequest $page"
	array set ::cmonStatus::state [ list $page 2 ]
}

#******************************************************** 
##
## Name: cmonStatus::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatus::state3 { page } {
	updateWidgets $::cmonStatus::topfw($page) disabled
	array set ::cmonStatus::state [ list $page 3 ]
}

## ******************************************************** 
##
## Name: cmonStatus::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatus::reset { page } {

    updateDisplay $::cmonStatus::textw($page) ""

}
