## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This package displays the directory tree 
## on the remote machine
## 
## cmonJobs version 1.0
##
## ******************************************************** 
;##barecode
package provide cmonTree 1.0

namespace eval cmonTree {
    variable count
    variable dblclick
	set pages { Tree }
	set page Tree
    set pagetext "Job Output"
	set wviewFile .tmpTreeFile
	set pids [ list ]
}

#end

## ******************************************************** 
##
## Name: cmonTree::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::create { notebook toppage } {
    set page [ lindex $::cmonTree::pages 0 ]
    $notebook insert end $toppage:$page -text $::cmonTree::pagetext \
        -createcmd "cmonTree::createPages $notebook $toppage:$page $page" \
        -raisecmd [ list cmonCommon::pageRaise cmonTree $page \
            "Remote Directory Tree" ]

}

## ******************************************************** 
##
## Name: cmonTree::createPages
##
## Description:
## creates Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::createPages { notebook toppage page } {

    set frame [ $notebook getframe $toppage ]
	set ::cmonTree::topframe $frame
	set subf  [ frame $frame.f ]
	set titf2 [TitleFrame $subf.titf2 -text "Command status" -font $::ITALICFONT] 
	set subf1  [ $titf2 getframe ]
	set topf  [ frame $subf.topf ]
	specific $topf $::cmonTree::page
	;## create a status to display errors and status
	set statusw [ ::createStatus  $subf1 ]
	array set ::cmonTree::statusw [ list $::cmonTree::page $statusw ]
    $statusw configure -height 7
	pack $topf -side left -padx 2 -padx 4 
	pack $titf2 -side right -padx 2 -pady 1 -fill both -expand 1
	pack $subf -side top -fill x  -padx 5
		
	set pw1	  [PanedWindow $frame.pw1 -side top ]
    set pane  [$pw1 add -minsize 100 -weight 1 ]
    set title [TitleFrame $pane.lf -text "Remote Directory tree" -font $::ITALICFONT ]
	set ::cmonTree::titf3($page) $title
	
    set sw    [ScrolledWindow [$title getframe].sw \
                  -relief sunken -borderwidth 2]
    set tree  [Tree $sw.tree \
                   -relief flat -borderwidth 0 -width 15 -highlightthickness 0\
		   -redraw 0 -dropenabled 1 -dragenabled 1 \
                   -dragevent 3 \
                   -droptypes {
                       TREE_NODE    {copy {} move {} link {}}
                       LISTBOX_ITEM {copy {} move {} link {}}
                   } \
                   -opencmd   "cmonTree::moddir 1 $sw.tree" \
                   -closecmd  "cmonTree::moddir 0 $sw.tree" \
				   -selectbackground yellow ]
    $sw setwidget $tree
	set ::cmonTree::tree $tree
	
    pack $sw    -side top  -expand yes -fill both
    pack $title -fill both -expand yes -padx 2 -pady 2

    set pane [$pw1 add -minsize 100 -weight 2 ]
    set lf   [TitleFrame $pane.lf -text "Directory Content" -font $::ITALICFONT ]
    set sw   [ScrolledWindow [$lf getframe].sw \
                  -scrollbar horizontal -auto none -relief sunken -borderwidth 2]
    set list [ListBox::create $sw.lb \
                  -relief flat -borderwidth 0 \
                  -dragevent 3 \
                  -dropenabled 1 -dragenabled 1 \
                  -width 20 -highlightthickness 0 -multicolumn true \
                  -redraw 0 -dragenabled 1 \
                  -droptypes {
                      TREE_NODE    {copy {} move {} link {}}
                      LISTBOX_ITEM {copy {} move {} link {}}} \
				  -selectbackground yellow ]
    $sw setwidget $list
	set ::cmonTree::list $list
    pack $sw $lf -fill both -expand yes
	pack $pw1 -side top -fill both -expand yes 

    $tree bindText  <ButtonPress-1>        "cmonTree::select tree 1 $tree $list"
    $tree bindText  <Double-ButtonPress-1> "cmonTree::select tree 2 $tree $list"
    $list bindText  <ButtonPress-1>        "cmonTree::select list 1 $tree $list"
    $list bindText  <Double-ButtonPress-1> "cmonTree::select list 2 $tree $list"
    #$list bindImage <Double-ButtonPress-1> "cmonTree::select list 2 $tree $list"

	array set ::cmonTree::state [ list $::cmonTree::page 0 ]
	set ::cmonTree::origCursor [ $frame cget -cursor ]
    set ::cmonTree::prevsite($page) $::cmonClient::var(site)
}

## ******************************************************** 
##
## Name: cmonTree::init 
##
## Description:
## creates initialize the directory treze 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::init { { tree "" } { list "" } args } {
    global   tcl_platform
    variable count
	;## make temp dir to hold remote files
	# tmpdir
	
	#state0 $::cmonTree::page 
	if	{ ! [ string length $tree ] } {
		set tree $::cmonTree::tree
		set list $::cmonTree::list
	}
    set count 0
    if { $tcl_platform(platform) == "unix" } {
        set rootdir [glob "~"]
    } else {
        set rootdir "c:\\"
    }
	set rootdir $::ROOTDIR

	if	{ [ catch {
		cmonTree::jobOutput $::cmonTree::page $rootdir
		catch { $tree delete [ $tree nodes root ] }
		set nodelist [ getdir $tree home $rootdir $::cmonTree::reply ]
		array set ::cmonTree::state [ list $::cmonTree::page 0 ]
		foreach node $nodelist {			
    		$tree opentree $node
			cmonTree::select tree 1 $tree $list $node
		}
		set node [ lindex $nodelist 0 ]
		$tree selection set $node
    	$tree configure -redraw 1
    	$list configure -redraw 1
	} err ] } {
		appendStatus $::cmonTree::statusw($::cmonTree::page) $err
	}
	cmonTree::state0 $::cmonTree::page
}

## ******************************************************** 
##
## Name: cmonTree::specific 
##
## Description:
## creates Refresh button to get dirs 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::specific { parent page } {

	set titf1 [TitleFrame $parent.titf1 -text \
	"View Job Output, e.g. 1234,5678-5690,L100,L20" -font $::ITALICFONT ]
	set subf1  [$titf1 getframe]
	set jobid_ent1   [LabelEntry $subf1.jobids -label "Job Ids: " -labelwidth 7 -labelanchor w \
    	-textvariable ::cmonTree::var($page,jobids) -editable 1 -width 30 \
        -helptext "Formats: numeric jobid e.g. 12345, or with optional runcode, e.g. LDAS-DEV12345" ]

	pack $jobid_ent1 -side top -padx 1 -pady 1 -expand 1 -fill x -anchor w
	message $subf1.help -aspect 400 \
		-text "Examples of Job Ids: 1245,5678,900-950,L20 (last 20 jobs),L100 (last 100 jobs); displays up to 100 jobs" \
		 -justify left -font $::MSGFONT -fg blue
	pack $subf1.help -side top -anchor w 
	set but1 [ Button $subf1.bstart -text SUBMIT -helptext "get job output directoru listings" \
        -command  "cmonTree::init" -padx 10 -pady 2  ]
    set ::cmonTree::bstart($page) $but1
	pack $but1 -side top -padx 10 -pady 10 -anchor s
	pack $titf1 -fill both -expand 1
}

## ******************************************************** 
##
## Name: cmonTree::jobOutput 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::jobOutput { page { path "" } } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonTree::statusw($::cmonTree::page) "Please select an LDAS site"
		return -code error 
	}
	if	{ $::cmonTree::state($page) } {
		appendStatus $::cmonTree::statusw($::cmonTree::page) \
		"Retrieving data, please wait" 0 blue
		return 
	}
	
	set jobids $::cmonTree::var($page,jobids)
	regsub -all {\s+} $jobids {} jobids 
	if	{ ! [ string length $jobids ] } {
		return -code error "Please enter job Id"
	}
	cmonTree::state1 $page 
    set client $::cmonClient::client
    set cmdId "new"
	set freq 0
	set repeat 0
	if	{ [ catch {
        set cmd "cmonTree::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::jobOutput $jobids" 
		# puts "cmd=$cmd"
		sendCmd $cmd                 
	} err ] } {
        cmonTree::state0 $page 
		appendStatus $::cmonTree::statusw($::cmonTree::page) $err
	}
	vwait ::cmonTree::reply
}

## ******************************************************** 
##
## Name: cmonTree::getFileContents
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::getFileContents { page path } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonTree::statusw($::cmonTree::page) "Please select an LDAS site"
		return -code error 
	}
	if	{ $::cmonTree::state($page) } {
		appendStatus $::cmonTree::statusw($::cmonTree::page) \
		"Retrieving file data, please wait" 0 blue
		return 
	}

	cmonTree::state1 $page 
    set client $::cmonClient::client
    set cmdId "new"
	set freq 0
	set repeat 0
	if	{ [ catch {
        set cmd "cmonTree::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::remoteBrowser $path" 
		# puts "cmd=$cmd"
		sendCmd $cmd                 
	} err ] } {
        cmonTree::state0 $page 
		appendStatus $::cmonTree::statusw($::cmonTree::page) $err
	}
	vwait ::cmonTree::reply
}

## ******************************************************** 
##
## Name: cmonTree::showReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonTree::showReply { page rc clientCmd html } {  

	if	{ [ catch {
    	set clientCmd [ split $clientCmd : ]
    	set client [ lindex $clientCmd 0 ]
    	set cmdId [ lindex $clientCmd 1 ]
    	set afterid [ lindex $clientCmd 2 ]
    	#puts "clientCmd=$clientCmd"
    	#puts "showReply, $page, client=$client,cmdId=$cmdId, rc=$rc,afterid=$afterid"
		# puts "html=$html"
		set ::cmonTree::cmdId($page) $cmdId 
    	set ::cmonClient::client $client
		switch $rc {
		0 {
			regexp {([^\|]+)\|([^\|]+)\|(.+)} $html -> type offset data
			set ::cmonTree::offset $offset
			set ::cmonTree::type $type
			set ::cmonTree::reply $data		
	  	}
		3 {
			appendStatus $::cmonTree::statusw($page) $html
			cmonTree::state0 $page	
	  	}
		}
	} err ] } {
		puts $err
		cmonTree::state0 $page	
	}
	
}

## ******************************************************** 
##
## Name: cmonTree::getdir 
##
## Description:
## display new directories on nodes
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:

proc cmonTree::getdir { tree node path lentries } {
    variable count
    #set lentries [glob -nocomplain [file join $path "*"]]
	set dirs [ lindex $lentries 0 ]
	set files [ lindex $lentries 1 ]
	set rootdir [ lindex $dirs 0 ]
	set level1dir [ lindex $dirs 1 ] 
	set level1 [ file tail $level1dir ]
	set image_openfold [ Bitmap::get openfold ]
	set image_folder [Bitmap::get folder ]
	$tree insert end root home -text $rootdir -data $rootdir -open 1 \
        -image $image_openfold
	set parentNode($rootdir) home
	$tree insert end home n:0 -text $level1 -data $level1 -open 1 \
        -image $image_openfold
	set parentNode($level1dir) n:0
	set count 1
	set top n:0
	catch { unset ::lfiles }
	set nodelist [ list ]

	foreach dir [ lrange $dirs  2 end ] {
		set parentdir [ file dirname $dir ]
		if	{ [ info exist parentNode($parentdir) ] } {
			set parent $parentNode($parentdir)
		} else {
			set parent $top
		}
		set tail [ file tail $dir ]
		set nodeName $parent:$count
		if	{ [ string match  $rootdir/$level1/$tail $dir ] } {
			set image $image_openfold
			set nodelist [ linsert $nodelist 0 $nodeName ] 
		} else {
			set image $image_folder
		}
		$tree insert end $parent $nodeName \
                -text      $tail \
                -image     $image \
                -drawcross allways \
                -data      $dir
		set parentNode($dir) $nodeName
       	incr count
	}
	foreach file $files {
		set parent [ file dir $file ]
		lappend ::lfiles($parent) [ file tail $file ]
	}
	foreach parent [ array names parentNode ] {
		set node $parentNode($parent)
		if	{ [ info exist lfiles($parent) ] } {
    		$tree itemconfigure $node -drawcross auto -data $::lfiles($parent)
		}
	}
	return $nodelist
}

## ******************************************************** 
##
## Name: cmonTree::moddir 
##
## Description:
## selected a new directory, display new leaves
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## must transition back to state0 after calling sendRequest

proc cmonTree::moddir { idx tree node } {

	if	{ $::cmonTree::state($::cmonTree::page) } {
		return
	}
    if { $idx && [$tree itemcget $node -drawcross] == "allways" } {
		#puts "moddir,[$tree itemcget $node -data]"
		#sendRequest $::cmonTree::page [$tree itemcget $node -data]
		select_node $tree $::cmonTree::list $node
        # getdir $tree $node [$tree itemcget $node -data] $::cmonTree::reply
        if { [llength [$tree nodes $node]] } {
            $tree itemconfigure $node -image [Bitmap::get openfold]
        } else {
            $tree itemconfigure $node -image [Bitmap::get folder]
        }
		cmonTree::state0 $::cmonTree::page
    } else {
        $tree itemconfigure $node -image [Bitmap::get [lindex {folder openfold} $idx]]
    }	
}

## ******************************************************** 
##
## Name: cmonTree::select 
##
## Description:
## handle mouse selection
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonTree::select { where num tree list node } {

	if	{ $::cmonTree::state($::cmonTree::page) } {
		return
	}
    variable dblclick
    set dblclick 1
	update idletasks
	# puts "where=$where,num=$num,tree=$tree,list=$list,node=$node"
	if	{ [ catch { 
    if { $num == 1 } {
        if { $where == "tree" && [lsearch [$tree selection get] $node] != -1 } {
            unset dblclick
            # after 500 "cmonTree::edit tree $tree $list $node"
        } elseif { $where == "list" && [lsearch [$list selection get] $node] != -1 } {
            unset dblclick
            # after 500 "cmonTree::edit list $tree $list $node"           
        } elseif { $where == "tree" } {
            select_node $tree $list $node
        } else {
            $list selection set $node
        }
    } elseif { $where == "list" && [$tree exists $node] } {
		set parent [$tree parent $node]
		while { $parent != "root" } {
	    $tree itemconfigure $parent -open 1
	    set parent [$tree parent $parent]
		select_node $tree $list $node 
		} 
    } elseif { $num == 2 } {
		if	{ $where == "list" } {
		;## a file, retrieve due to double click		
		set selection [ $list selection get ]
		set selected_node [ $tree selection get ]
		set dir [ $tree itemcget $selected_node -text ]
		set fname [ $list itemcget $selection -text ]
		# puts "num=2,selection $selection,dir $dir,fname $fname"
		;## traverse up to get the full path
		set dirpath $dir 
		set parent [$tree parent $selected_node]
		while { $parent != "root" } {
	    	$tree itemconfigure $parent -open 1
			set dirpath [ linsert $dirpath 0 [ $tree itemcget $parent -text ] ]
	    	set parent [$tree parent $parent]
		}
		set dir [ join $dirpath / ]
		set fname [ file tail $fname ]
		set selectfile $dir/$fname
		cmonTree::getFileContents $::cmonTree::page $selectfile
		cmonTree::viewFile $::cmonTree::page $selectfile $::cmonTree::reply
		cmonTree::state0 $::cmonTree::page
		} elseif { [$tree exists $node] } {
			set parent [$tree parent $node]
			while { $parent != "root" } {
	   	 		$tree itemconfigure $parent -open 1
	    		set parent [$tree parent $parent]
				select_node $tree $list $node 
			}
		}
	} 
	} err ] } {
		appendStatus $::cmonTree::statusw($::cmonTree::page) "select error: $err"
		cmonTree::state0 $::cmonTree::page
	}
}

## ******************************************************** 
##
## Name: cmonTree::select_node 
##
## Description:
## handle selection of a node
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonTree::select_node { tree list node } {

	if	{ $::cmonTree::state($::cmonTree::page) } {
		puts "not state 0"
		return
	}
	catch { $tree itemconfigure $::cmonTree::node_selected -image [Bitmap::get folder] }
    $tree selection set $node
	set ::cmonTree::node_selected $node
    update
    eval $list delete [$list item 0 end]
	set dir [$tree itemcget $node -data]
	;## if this is a directory
    if { [$tree itemcget $node -drawcross] == "allways" } {
		$tree itemconfigure $node -image [Bitmap::get openfold]
    } 

    foreach subnode [$tree nodes $node] {
		# puts "subnote $subnode,text=[$tree itemcget $subnode -text]"
        $list insert end $subnode \
            -text  [$tree itemcget $subnode -text] \
            -image [Bitmap::get folder]
    }
    set num 0
	if	{ [ info exist ::lfiles($dir) ] } {
    	foreach f $::lfiles($dir) {
        	$list insert end f:$num \
            	-text  $f \
            	-image [Bitmap::get file]
        	incr num
		}
    }
	cmonTree::state0 $::cmonTree::page
}

## ******************************************************** 
##
## Name: cmonTree::expand 
##
## Description:
## handle expansion of a node when selected 
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonTree::expand { tree but } {
    if { [set cur [$tree selection get]] != "" } {
        if { $but == 0 } {
            $tree opentree $cur
        } else {
            $tree closetree $cur
        }
    }
}


## ******************************************************** 
##
## Name: cmonTree::viewFile
##
## Description:
## handle display of wrapperAPI logs
##
## Parameters:
##
## Usage:
##  
## 
## Comments:

proc cmonTree::viewFile { page fname html { create 1 } } {

	if	{ [ catch {		
		;## use text editor to show text files 
        set tmpfile $::TMPDIR/[ file tail $fname ]
	    set fd [ open $tmpfile  w ]
		puts $fd $html
		close $fd
        
		if	{ [ string match text $::cmonTree::type ] } {
			set cmd "exec $::EDITOR $tmpfile &"
			catch { eval $cmd } err
			if	{ [ regexp {(\d+)} $err -> pid ] } {
				lappend ::cmonTree::pids $pid
				appendStatus $::cmonTree::statusw($::cmonTree::page) \
				"$tmpfile display: $err" 0 blue
			} else {
				appendStatus $::cmonTree::statusw($::cmonTree::page) \
				"$tmpfile display error: $err"
			}
		} else { 
            set parent ${::cmonTree::wviewFile}[ clock seconds ]
    		if  { ! [ winfo exist $parent ] && $create } {
                toplevel $parent
        		set frame [ frame $parent.f -relief sunken -borderwidth 2 ]
        		set textw [ ::createDisplay $frame ]
        		$textw configure -bg PapayaWhip -font $::LISTFONT
        		pack $frame -fill both -expand yes
                wm title $parent $fname
				set html [ subst -nobackslashes -nocommands -novariables $html ]
				updateDisplay $textw $html 
    		}  
		}
	} err ] } {
		appendStatus $::cmonTree::page "error displaying $fname: $err"
		return
	}
}

#******************************************************** 
##
## Name: cmonTree::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonTree::state0 { page } {
	$::cmonTree::bstart($page) configure -state normal
	array set ::cmonTree::state [ list $page 0 ]
	$::cmonTree::topframe configure -cursor $::cmonTree::origCursor
}


#******************************************************** 
##
## Name: cmonTree::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonTree::state1 { page } {
    #$::cmonTree::bstart($page) configure -state disabled
	array set ::cmonTree::state [ list $page 1 ]
	appendStatus $::cmonTree::statusw($::cmonTree::page) \
	"Sending request, please wait..." 0 blue
	$::cmonTree::topframe configure -cursor watch 
}

#******************************************************** 
##
## Name: cmonTree::resetAll  
##
## Description:
## changes all pages back to initial state
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
##  
proc cmonTree::reset { page args } {

	foreach pid $::cmonTree::pids {
		catch { kill -9 $pid } err
	}
    set tree [ set ::cmonTree::tree ]
    catch { $tree delete [ $tree nodes home ] }
    catch { $tree delete home }        
    $::cmonTree::list delete [ $::cmonTree::list item 0 end ]
}   
