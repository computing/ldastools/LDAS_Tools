## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script display database row values in graphical format
##
## cmonDBview Version 1.0
##
## ******************************************************** 

package provide cmonDBview 1.0

namespace eval cmonDBview {
    set notebook ""
    set pages [ list cmonDBview ]
	set title(cmonDBview) "table row insertions " 
	set xlabel(cmonDBview) "GPS Time"
	set ylabel(cmonDBview) "# Insertions"
	set graph jobstats
    set colors [ list blue magenta ]
    set sqlwin .cmonDBviewsql
    set sqlstub " and "
}

## ******************************************************** 
##
## Name: cmonDBview::create
##
## Description:
## creates tab for this page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook tab
## 
## Comments:
## this proc name is generic for all packages.

proc cmonDBview::create { notebook } {

    set page [ lindex $::cmonDBview::pages 0 ]
    set ::cmonDBview::prevsite($page) none
    $notebook insert end cmonDBview -text "View Database" \
        -createcmd "cmonDBview::createPages $notebook $page" \
        -raisecmd "cmonDBview::pageRaise; \
            cmonCommon::setLastTime cmonDBview $page; \
            cmonCommon::updateDBinfo cmonDBview $page"
}


## ******************************************************** 
##
## Name: cmonDBview::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonDBview::createPages { notebook page } {

	if	{ [ catch { 
        array set ::TABCOLS [ list table column ]
    	set frame [ $notebook getframe cmonDBview ]
	
		specific $frame $page 	
        array set ::cmonDBview::state [ list $page 0 ]
	    $notebook compute_size
        set ::cmonDBview::prevsite($page) $::cmonClient::var(site)
	} err ] } {
		puts $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonDBview::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonDBview::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
	
    set labf1 [LabelFrame $parent.labf1 -text "Fitler by database, table and 2 columns" -side top -anchor w \
                   -relief raised -borderwidth 2 ]
    set subf  [$labf1 getframe]
    set ::cmonDBview::topfw($page) $subf
	set subf1 [ frame $subf.subf1 -relief sunken -borderwidth 2 ]

	set fdbname [ frame $subf1.fdbname -relief groove -borderwidth 2 ]    
    set lab [ Label $fdbname.lbl -text "Database: "  \
		-justify left -helptext "Select test or production database" ]
    pack $lab -side left -anchor w 
    cmonCommon::createDBinfo cmonDBview $page $fdbname
    
	pack $fdbname -side top -anchor w -fill both -expand 1
    
    cmonDBview::dbtables cmonDBview $page $subf1 
    
    #set fcktime [ frame $subf1.cktime -relief groove -borderwidth 2 ]
    #set lab [ label $fcktime.lab -text "Show graphs: " ]
	#set cktime [ checkbutton $fcktime.cktime -text "X Y Line"  \
	#	-variable ::cmonDBview::${page}(cktime) -onvalue 1 -offvalue 0 ]
	#set ::cmonDBview::${page}(cktime) 1
	
	;## histogram select
	#set ckhist [ checkbutton $fcktime.ckhist -text "Y value row count histogram"  \
	#	-variable ::cmonDBview::${page}(ckhist) -onvalue 1 -offvalue 0 ]
    #set ::cmonDBview::${page}(ckhist) 1
	#pack $lab $cktime $ckhist -side left -anchor w -padx 2
    #pack $fcktime -side top -anchor w -fill x 
    
	set subf2 [ frame $subf.subf2 -relief ridge -borderwidth 2 ]
	set f2 [ frame $subf2.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]

	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $subf2.ftime $name $page ] { break }
	
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x 
	pack $subf2.ftime -side top -anchor w -expand 1 -fill both
	
	pack $subf1 $subf2 -side left -fill both -expand 1
	
	;## place the cmd status on the bottom
	set labref [LabelFrame $parent.refresh -text "Command Status" -side top -anchor w \
                   -relief sunken -borderwidth 4]
                   
    set subf3  [ $labref getframe ]
	
	set statusw [ ::createStatus  $subf3 ]
	array set ::cmonDBview::statusw [ list $page $statusw ]
	$statusw configure -bg PapayaWhip
	
	set f3 [ frame $subf2.fstart  ]
    set  but3 [ Button $f3.edit -text "EDIT SQL" -width 10 -helptext "EDIT SQL" \
        -command  "::cmonDBview::editSQL $page" ]  
        
    set  but1 [ Button $f3.start -text "SUBMIT" -width 8 -helptext "submit one time request to server" \
        -command  "::cmonDBview::sendRequest $page" ] 
      
    set ::cmonDBview::bstart($page) $but1
    set  but2 [ Button $f3.cancel -text "ABORT REQUEST:" -width 20 \
        -helptext "select a request from below to abort " \
        -command  "::cmonDBview::cancelRequest $page" -state disabled ]
    set ::cmonDBview::bcancel($page) $but2
    pack $but3 $but1 $but2 -side left -anchor w -padx 1
    set f4 [ frame $subf2.fcancel  ]
    createRequestCombo cmonDBview $page $f4.cancelcombo
    
    pack $f3 $f4 -side top -expand 1 -fill x	
	pack $labf1 -side top -fill x 
    pack $labref -side top -fill both -expand 1 -pady 2
}

## ******************************************************** 
##
## Name: cmonDBview::setdbtable
##
## Description:
## creates a checkbox for APIs
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc cmonDBview::setdbtable { combo name page } {

    $combo setvalue @[ $combo getvalue ] 
    set table [ set ::${name}::${page}(dbtable) ]

    set col1w [ set ::${name}::${page}(XcolCombo) ]
    destroy $col1w
    
    set values $::TABCOLS($table)

    set cmd_ent   [ ComboBox $col1w \
		-text "X-axis Column (time): " -width 25 \
    	-textvariable ::${name}::${page}(x-col) -editable 0 \
		-values insertion_time -state normal \
        -helptext "X-axis column" \
		-modifycmd "setCombo $col1w" ]
	pack $col1w -side left -anchor w -fill x -expand 1
	$col1w setvalue first

    set col2w [ set ::${name}::${page}(YcolCombo) ]
    destroy $col2w
    set cmd_ent   [ ComboBox $col2w \
		-text "Y-axis Column: " -width 25 \
    	-textvariable ::${name}::${page}(y-col) -editable 0 \
		-values $values -state normal \
        -helptext "Numeric columns" \
		-modifycmd "setCombo $col2w" ]

	pack $col2w -side left -anchor w -fill x -expand 1
	$col2w setvalue first

}

## ******************************************************** 
##
## Name: cmonDBview::createTabCombo
##
## Description:
## creates the table combo
##
## Parameters:
##
## Comments:

proc cmonDBview::createTabCombo { name page combo } {
    
    catch { destroy $combo }
    set tables [ lsort -dictionary [ array names ::TABCOLS ] ]
    ;## remove tables from the list if there are real tables
    if  { [ llength $tables ] > 1 } {
        set index [ lsearch -exact $tables table ]
        set tables [ lreplace $tables $index $index ]
        set default [ lsearch -exact $tables process ]
    } else {
        set default [ lsearch -exact $tables table ]
    }
	set cmd_ent   [ ComboBox $combo \
		-text "Table: " -width 15 \
    	-textvariable ::${name}::${page}(dbtable) -editable 0 \
		-values $tables  \
        -modifycmd "cmonDBview::setdbtable $combo $name $page" ]
    set ::${name}::${page}(tabCombo) $combo
    pack $combo -side left -anchor w -fill x -expand 1
    $combo setvalue @$default
}

## ******************************************************** 
##
## Name: cmonDBview::createXcolCombo
##
## Description:
## creates the table combo
##
## Parameters:
##
## Comments:

proc cmonDBview::createXcolCombo { name page combo } {
    
    catch { destroy $combo }
	set cmd_ent   [ ComboBox $combo \
		-text "X-axis column (time): " -width 25 \
    	-textvariable ::${name}::${page}(x-col) -editable 0 \
		-values insertion_time -state normal \
		-modifycmd "setCombo $combo" ]
    set ::${name}::${page}(XcolCombo) $combo
    pack $combo -side left -anchor w -fill x -expand 1
    $combo setvalue first
}

## ******************************************************** 
##
## Name: cmonDBview::createYcolCombo
##
## Description:
## creates the table combo
##
## Parameters:
##
## Comments:

proc cmonDBview::createYcolCombo { name page combo } {
    
    catch { destroy $combo }
    set table  [ set  ::${name}::${page}(dbtable) ]
    set column [ set ::TABCOLS($table) ]
	set cmd_ent   [ ComboBox $combo \
		-text "Y-axis column: " -width 25 \
    	-textvariable ::${name}::${page}(y-col) -editable 0 \
		-values $column -state normal \
		-modifycmd "setCombo $combo" ]
    set ::${name}::${page}(YcolCombo) $combo
    pack $combo -side left -anchor w -fill x -expand 1
    $combo setvalue first
}

## ******************************************************** 
##
## Name: cmonCommon::dbtables
##
## Description:
## creates usercmd selection 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonDBview::dbtables { name page parent } {

	set f1 [ frame $parent.f1 -relief groove -borderwidth 2 ]

	;## select tables
	set fdbtable [ frame $f1.ftable ]
    set lab [ label $fdbtable.lbl -text "Table: " -width 25 -justify left -anchor w ]
    pack $lab  -side left -anchor w 
    cmonDBview::createTabCombo $name $page $fdbtable.cmd
	pack $fdbtable -side top -anchor w -fill both -expand 1
	
	;## select X axis columns
	
	set fcol1 [ frame $f1.fcol1 ]
	set lab [ label $fcol1.lbl -text "X-axis Column (time): " -width 25 -justify left -anchor w ]
    pack $fcol1.lbl -side left -anchor w 
    cmonDBview::createXcolCombo $name $page $fcol1.col     
	pack $fcol1 -side top -anchor w -fill both -expand 1

    ;## Y axis columns
	
	set fcol2 [ frame $f1.fcol2 ]
	set lab [ label $fcol2.lbl -text "Y-axis Column: " -width 25 -justify left -anchor w ]
    pack $lab -side left -anchor w 
    cmonDBview::createYcolCombo $name $page $fcol2.col
	pack $fcol2 -side top -anchor w -fill both -expand 1

	pack $f1 -side top -fill both -expand 1 
	
	;## dont configure listbox to be extended select instead of browse for now
	#set command [ $fldascmds.cmd.a  cget -command ]
	#$fldascmds.cmd.a configure -command "$command; cmonCommon::listSelectMode $fldascmds.cmd"
	#set command [ $fdso.dso.a cget -command ]
	#$fdso.dso.a configure -command "$command; cmonCommon::listSelectMode $fdso.dso"
	
	return $f1
}	

##******************************************************* 
##
## Name: cmonDBview::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonDBview::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
    set state [ $::cmonDBview::bcancel($page) cget -state ]
	updateWidgets $::cmonDBview::topfw($page) normal
    $::cmonDBview::bcancel($page) configure -state $state
	array set ::cmonDBview::state [ list $page 0 ]    
}

##******************************************************* 
##
## Name: cmonDBview::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonDBview::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonDBview::topfw($page) disabled	
    $::cmonDBview::bstart($page) configure -state disabled 
    $::cmonDBview::bcancel($page) configure -state disabled 
	array set ::cmonDBview::state [ list $page 1 ]
	
}

#******************************************************** 
##
## Name: cmonDBview::updateDBinfo
##
## Description:
## update database name selection when site is changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::updateDBinfo { args } {

		if	{ [ catch {
			set page [ lindex $args 0 ]
            set site $::cmonClient::var(site)
            cmonCommon::updateDBinfo cmonDBview $page
            set index [ lsearch $::dbnames($site) ldas_tst ]
            if  { $index < 0 } {
                set index 0
            }
			set ::cmonDBview::dbname($page) [ lindex $::dbnames($site) $index ]
            cmonDBview::createTabCombo cmonDBview $page  [ set ::cmonDBview::${page}(tabCombo) ]
            cmonDBview::createXcolCombo cmonDBview $page [ set ::cmonDBview::${page}(XcolCombo) ]
            cmonDBview::createYcolCombo cmonDBview $page [ set ::cmonDBview::${page}(YcolCombo) ]
            update
		} err ] } {
			catch { appendStatus $::cmonDBview::statusw($page) $err }
		}
}

## ******************************************************** 
##
## Name: cmonDBview::sendRequest
##
## Description:
## sends request to server
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.
## timeopt, histopt and binsize are passed for backwards compatibility

proc cmonDBview::sendRequest { page { timeopt -1} { histopt -1} {binsize 1} {dbname ""} \
{dbtable ""} {timecol ""} {valuecol ""} { sql "" } } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonDBview::statusw($page) "Please select an LDAS site"
		return 
	}
    set login ""
	set passwd ""
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0
    foreach { login passwd } [ validateLogin 1 ] { break }
    if  { [ catch {
       	if	{ ! $::USE_GLOBUS } {
        	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::cntlpasswd ] ]
        } else {
        	set userpass $passwd
            ckPriviledged
        }
    } err ] } {
		appendStatus $::cmonDBview::statusw($page) $err
		return        
	}
    if  { [ string equal cancelled $login ] } {
		return
	}
    if  { [ catch {
    	set userinfo "-name $login -password $userpass -email persistent_socket"
        regsub -all -- {\s+} $userinfo { } userinfo
        if  { $timeopt < 0 } {
            set timecol [ set ::cmonDBview::${page}(x-col) ]
            set valuecol [ set ::cmonDBview::${page}(y-col) ]
            set dbtable [ set ::cmonDBview::${page}(dbtable) ]
            set dbname [ set ::cmonDBview::dbname($page) ]
            set timeopt 1
            set histopt 0
        }
        
        if  { [ string match table $dbtable ] } {
            error "Please select a table"
        }
        if  { [ string match column $timecol] || [ string match column $valuecol ] } {
            error "Please select a column"
        }
        foreach { starttime endtime } [ cmonCommon::dbTimeStamp $page $name ] { break }
        
        ;## current sql or take the one from edit window
        if  { ! [ string length $sql ] } {
            set sql "select $timecol, $valuecol from $dbtable where $timecol >= timestamp('$starttime') \
            and $timecol <= timestamp('$endtime') and $valuecol is not null"
        }
         
        set cmdinfo "getMetaData \
    	-returnprotocol file:/getMeta \
    	-outputformat {ilwd ascii} \
    	-database \{$dbname\} \
    	-sqlquery \{$sql\}"
    
        ;## collapse to a single line
        regsub -all -- {\s+} $cmdinfo { } cmdinfo 
        set cmdinfo [ string trim $cmdinfo ]
    
        set cmd "cmonDBview::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::submitDBqueryJob $page $timeopt $histopt $binsize \
            \{$userinfo\} \{$cmdinfo\}"
        # puts "cmd=$cmd"
        sendCmd $cmd 
        after 10 [ list cmonDBview::submitInfo $page ]

        cmonDBview::state1 $page
    } err ] } {
        cmonDBview::state0 $page 
        appendStatus $::cmonDBview::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonDBview::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonDBview::showReply { page rc clientCmd html } {

	if	{ [ catch {
        set name [ namespace tail [ namespace current ] ]
        set clientCmd [ split $clientCmd : ]
        set client [ lindex $clientCmd 0 ]
        set cmdId [ lindex $clientCmd 1 ]
        set afterid [ lindex $clientCmd 2 ]
        # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"

        set ::cmonDBview::cmdId($page) $cmdId 
        set ::cmonClient::client $client
    	
	    switch $rc {
	    0  { if  { [ string length $html ] } {
                 cmonDBview::dispGraph $page $html ${client}:${cmdId}
                 appendStatus $::cmonDBview::statusw($page) "*** Request completed***" 0 blue
                 cmonDBview::unregisterRequest $page $client $cmdId
              }
           }
	    2  {  if    { [ string length $html ] } {
			        appendStatus $::cmonDBview::statusw($page) $html 0 blue
                    if  { [ regexp {submitting} $html ] } {
                        cmonDBview::registerRequest $page $html $client $cmdId
                    }
              }
           } 
	    3  { if    { [ string length $html ] } {
			       appendStatus $::cmonDBview::statusw($page) $html
             }
             cmonDBview::unregisterRequest $page $client $cmdId
           }
	  }
	} err ] } {
		appendStatus $::cmonDBview::statusw($page) $err
        catch { cmonDBview::unregisterRequest $page $client $cmdId }
	}
    cmonDBview::state0 $page
}

## ******************************************************** 
##
## Name: cmonDBview::dispGraph
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:

proc cmonDBview::dispGraph { page html clientId  } {

    foreach { dbname dbtable startime endtime timeopt histopt binsize col1 col2 } $html { break }
    set sql [ set ::cmonDBview::${page}($clientId,sql) ]

    set histwin .dbviewhist${col2}
    set site [ string trim $::cmonClient::var(site) ]
    
    ;## save for histogram option 
    set fname $::TMPDIR/[ string trimleft $histwin . ].$site
    set fd [ open $fname  w ]
    puts $fd $html
    close $fd
    set parent .dbview${col2}time
    
    if  { ! [ winfo exist $parent ] } {
        toplevel $parent 
	    set statusw [ createTextWin $parent 5 ]  
        set ::${parent}(textw) $statusw
    }    
    set statusw [ set ::${parent}(textw) ]
    set ::${parent}(replot) [ list cmonDBview::replot $page $histwin $sql ]
    
    wm title $parent "DB view $col2 vs $col1 @$site"
    bind $statusw <Destroy> [ list cmonDBview::closeWin $page $parent $histwin $site ]	
    wm protocol $parent WM_DELETE_WINDOW [ list cmonDBview::closeWin $page $parent $histwin $site ]
    dbRowTimeBLT $page $html $parent $sql
    cmonDBview::updateHistogram $page $histwin $sql
    
}

##*******************************************************
##
## Name: cmonDBview::dbRowTimeBLT 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::formatTimePeriod {} {

    uplevel {
        foreach { gps_start localstart } [ formatDBtime $starttime ] { break }
        foreach { gps_end localend } [ formatDBtime $endtime ] { break } 
    }
}

##*******************************************************
##
## Name: cmonDBview::dbRowTimeBLT 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::dbRowTimeBLT { page statsdata parent sql } {

    if  { [ catch {
        foreach { dbname dbtable starttime endtime timeopt histopt binsize \
        timecol valuecol numRows xmin xmax ymin ymax timedata } $statsdata { break }
        
        ;## print info
		set ::${parent}(ptitle) "$dbname database $dbtable table $valuecol vs time @$::cmonClient::var(site)"
        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        
        cmonDBview::formatTimePeriod
        lappend text "Time Period:\t" [ list black fixed ]
		lappend text "From $localstart ($gps_start) to $localend ($gps_end):\n" [ list brown fixed ]
	
        ;## put in sql
        lappend text "SQL:\t" [ list black fixed ]
        lappend text "$sql\n" [ list brown bold ]
        lappend text "Retrieved " [ list black fixed ]
		lappend text "$numRows rows" [ list brown bold ]
        
		set ::${parent}(title) "$dbname database $dbtable table $valuecol vs time"
		set ::${parent}(y1label) $valuecol		
		set ::${parent}(submit) "cmonDBview::sendRequest $page $dbname $dbtable $timecol $valuecol"	
		updateTextWin [ set ::${parent}(textw) ] $text 
        
		set bltdata [ list ]
        foreach { timedata msg } [ cmonCommon::reduceDisplayData $timedata $::MAX_GRAPH_POINTS ] { break }
        if  { [ llength $msg ] } {
            appendStatus $::cmonDBview::statusw($page) "$valuecol data: $msg" 0 brown
        }
		lappend bltdata $timedata ${valuecol}_time circle blue 0
        if  { [ expr $xmax - $xmin ] <= 1 } {
            set xmax [ expr $xmin + 2 ]
        }
        if  { [ expr $ymax - $ymin ] <= 1 } {
            set ymax [ expr $ymin + 2 ]
        }
		blt_graph::showGraph $bltdata $xmin \
			$xmax $ymin $ymax $parent
		raise $parent
		update
    } err ] } {
        return -code error "[myName] $err"
    }
}

##*******************************************************
##
## Name: cmonDBview::dbRowHistBLT 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::dbRowHistBLT { page statsdata parent sql { cummPlot 0 } } {

	if	{ [ catch {
        foreach { dbname dbtable starttime endtime timeopt histopt binsize timecol valuecol \
            numRows xmin xmax ymin ymax timedata } $statsdata { break }
            
        if  { $numRows } { 
            set ylist [ list ]
            foreach { x y } $timedata {
                lappend ylist $y
            }
            foreach { xmean xmedian xstddev xcor } [ bltstats $ylist ] { break }
            unset ylist
        }
        
        if  { ![ info exist ::${parent}(bucketsz) ] } {
            set binsize 1
        } else {
            set binsize [ set ::${parent}(bucketsz) ]
        }
        cmonDBview::formatTimePeriod
        
        ;## print info for parent
		set ::${parent}(ptitle) "$dbname database $dbtable table $valuecol histogram @$::cmonClient::var(site)"
        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
        
        ;## put in sql
        lappend text "SQL:\t" [ list black fixed ]
        lappend text "$sql\n" [ list brown bold ]
        lappend text "Retrieved " [ list black fixed ]
		lappend text "$numRows rows" [ list brown bold ]
			  
		set ::${parent}(title) "Histogram of $dbname database $dbtable table $valuecol:"
		set ::${parent}(y1label) "# rows"	
		set ::${parent}(x1label) "$valuecol values"
		set ::${parent}(x1step) $binsize
		set ::${parent}(replot) [ list cmonDBview::replot $page $parent \"$sql\" ]
		set ::${parent}(submit) "cmonDBview::sendRequest $page $dbname $dbtable $timecol $valuecol"
        set ::${parent}(histargs) "cmonDBview $page [ set ::BIN_RANGE(cmonDBview) ]"
       
        set histdata [ list ]
        if  { [ llength $timedata ] } {   
            counter::init $valuecol -hist $binsize
            set ylist [ list ]
            foreach { x y } $timedata {
                counter::count $valuecol $y
                lappend ylist $y
            }
            foreach { xtotal xmean xmedian xstddev xcor } [ bltstats $ylist ] { break }
            set ylist [ lsort -real $ylist ]
            set ymin [ lindex $ylist 0 ]
            set ymax [ lindex $ylist end ]
            unset ylist
            set histdata [ counter::getDBHistData $valuecol ]
            counter::init $valuecol
        } 
        
        if  { $cummPlot } {
            if  { [ llength $timedata ] } {
                set histdata [ cmonCommon::getHistCummData $histdata $binsize ]
            }
            set ::${parent}(y1label) "% of total rows, cumulative"
        } 

		foreach { xmin ymin xmax ymax } [ cmonCommon::computeXYMinMax $histdata $binsize ] { break }

		set bltdata [ list ]	
		lappend bltdata $histdata
		lappend bltdata ${valuecol}_hist
		lappend bltdata blue
		lappend bltdata $binsize

		set graph [ blt_graph::barChart $bltdata $xmin $xmax $ymin $ymax $parent ]
        set statstext ""
        if  { [ info exist xmean ] } {
            append statstext "X axis total=$xtotal, mean=$xmean , median=$xmedian, std-dev=$xstddev\n"
        }

        if  { [ info exist ::${parent}(ystats) ] } {
            foreach { ytotal ymean ymedian ystddev }  [ set ::${parent}(ystats) ] { break }
            append statstext "Y axis total=$ytotal, mean=[ expr round($ymean) ], median=[ expr round($ymedian) ], std-dev=[ format %.2f $ystddev ]"
        }
        lappend text "\n$statstext" [ list brown bold ]
        
        updateTextWin [ set ::${parent}(textw) ] $text 
		raise $parent
		update idletasks
	} err ] } {
		appendStatus $::cmonDBview::statusw($page) $err
	}
}

              
##*******************************************************
##
## Name: cmonDBview::replot
##
## Description:
## replot the graph again, based in points or lines
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::replot { page parent sql  } {
 
    set site [ string trim $::cmonClient::var(site) ]
    set fname $::TMPDIR/[ string trimleft $parent . ].$site 
    regsub -all {\"} $sql {} sql
	if	{ [ file exist $fname ] } {
		set fd [ open $fname r  ]
		set html [ read $fd  ]
		close $fd
        cmonDBview::createHistWin $page $parent
		dbRowHistBLT $page $html $parent $sql [ set ::${parent}(cummPlot) ]
	} else {
		appendStatus $::cmonDBview::statusw($page) \
		"No previous data to replot for $site; please resubmit your request to server"
	}
}

##*******************************************************
##
## Name: cmonDBview::submitInfo
##
## Description:
## display submit info for the first time
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::submitInfo { page } {
    set win .cmonDBviewSubmitInfo
    set icon info
    set image [ Bitmap::get $icon ]
    set msg "Each request takes one or more iterations of LDAS getMetaData command to gather database data. \
    The response time is dependent on LDAS job load. You can submit more than one request at any time. Only \
    non-null unique values are returned."
    if  { ! [ info exist ::cmonDBview::${page}(showInfo) ] } {
        set ::cmonDBview::${page}(showInfo) 0
    }
	if	{ ! [ winfo exist $win ] } {
        if  { ! [ set ::cmonDBview::${page}(showInfo) ] } {
		    set dlg [ Dialog $win -parent . -modal none \
          	-separator 1 -title  $icon \
        	-side bottom -anchor  s -default 0 -image $image ]
            $dlg add -name ok -anchor s -command [ list destroy $dlg ]
		    set dlgframe [ $dlg getframe ]
		    message $dlgframe.msg -aspect 300 -text $msg \
					-justify left -font $::MSGFONT
            set chkb [ checkbutton $dlgframe.showInfo  -text "Do not display this again." \
            -anchor w -justify left \
        	-variable ::cmonDBview::${page}(showInfo) \
        	-font $::LISTFONT -onvalue 1 -offvalue 0 ]
		    pack $dlgframe.msg $chkb -side top -anchor w 
		    $dlg draw 
		    after 15000 [ list destroy $dlg ]
        }
    }
}

##*******************************************************
##
## Name: cmonDBview::registerRequest
##
## Description:
## display submit info for the first time
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::registerRequest { page html client cmdId } {

    if  { [ catch {
        set rc [ regexp -- {submitting.+\-database \{([^\}]+)\}.+from\s+(\S+).+and\s+(\S+)} $html -> \
          dbname dbtable valuecol ]
        if	{ $rc } {		  
            set ::cmonDBview::${page}(${client}:${cmdId}) [ list $dbname $dbtable $valuecol ]
            cmonDBview::createRequestCombo cmonDBview $page [ set ::cmonDBview::${page}(requestCombo) ]
            ;## save the sql query also
            regexp -- {-sqlquery\s+\{([^\}]+)} $html -> sql 
            set ::cmonDBview::${page}(${client}:${cmdId},sql) $sql
        } else {
            error "regexp failed for '$html'"
        }
    } err ] } {
        puts $err
    }
}

##*******************************************************
##
## Name: cmonDBview::unregisterRequest
##
## Description:
## display submit info for the first time
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::unregisterRequest { page client cmdId } {

    if  { [ catch {
        unset ::cmonDBview::${page}($client:${cmdId})
        unset ::cmonDBview::${page}($client:${cmdId},sql)
        cmonDBview::createRequestCombo cmonDBview $page \
            [ set ::cmonDBview::${page}(requestCombo) ]
    } err ] } {
        puts $err
    }
}

## ******************************************************** 
##
## Name: cmonDBview::createRequestCombo
##
## Description:
## creates the request combo
##
## Parameters:
##
## Comments:

proc cmonDBview::createRequestCombo { name page combo } {
    
    catch { destroy $combo }    
    set cmds [ lsort -dictionary [ array names ::${name}::${page} client* ] ]

    if  { ! [ llength $cmds ] } {
        set requests [ list "Nothing requested" ]
        set state disabled
    }  else {
        set requests [ list ]
        foreach cmd $cmds {
            if  { ! [ regexp {sql} $cmd ] } { 
                foreach { dbname dbtable valuecol } [ set ::${name}::${page}($cmd) ] { break }
                lappend requests "$cmd db=$dbname,table=$dbtable,col=$valuecol"
            }
        }
        set state normal
    }
	set cmd_ent   [ ComboBox $combo \
		-text "" -width 15 \
    	-textvariable ::${name}::${page}(cancelRequest) -editable 0 \
		-values $requests  -state $state \
        -modifycmd "setCombo $combo" ]
    set ::${name}::${page}(requestCombo) $combo
    pack $combo -side bottom -anchor w -fill x -expand 1
    $combo setvalue first
    $::cmonDBview::bcancel($page) configure -state $state
    
}

##*******************************************************
##
## Name: cmonDBview::cancelRequest
##
## Description:
## cancel current request in progress and display data
## gathered so far
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::cancelRequest { page } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonDBview::statusw($page) "Please select an LDAS site"
		return 
	}
    if  { $::cmonDBview::state($page) != 0 } {
        return
    }
	if	{ [ catch {
        foreach { login passwd } [ validateLogin 1 ] { break }
		set repeat 0
		set freq 0
        set canceltext [ set ::cmonDBview::${page}(cancelRequest) ]
        set cmdId new
        regexp {(client\d+):(\d+)\s+} $canceltext -> client cmdId
        ;## pull out client 
        set cmd "cmonDBview::showReply $page\n$repeat\n$freq\n\
        	${client}:$cmdId:$login:$passwd\ncntlmon::cancelDBQuery $page $client $cmdId"
        set msg "Cancelling query '$canceltext'"
        appendStatus $::cmonDBview::statusw($page) $msg 0 blue
		cmonDBview::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonDBview::state0 $page 
		appendStatus $::cmonDBview::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonDBview::reset
##
## Description:
## changes all pages back to initial state
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonDBview::reset { page } {
    
    unset ::TABCOLS  
    set ::TABCOLS(table) column
    set cmds [ array names ::cmonDBview::${page} client* ]
    if { [ llength $cmds ] } {
            foreach cmd $cmds {
                unset ::cmonDBview::${page}($cmd)
            }
            createRequestCombo cmonDBview $page \
                [ set ::cmonDBview::${page}(requestCombo) ]
    }        

    $::cmonDBview::bcancel($page) configure -state disabled    
}   


## ******************************************************** 
##
## Name: cmonDBview::pageRaise   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonDBview::pageRaise {} {

	set ::cmonClient::noPass 0
	set ::cmonClient::pageUpdate "cmonDBview::pageRaise"
    
    set page cmonDBview
    if  { ! [ string equal $::cmonDBview::prevsite($page) $::cmonClient::var(site) ] } {        
        cmonDBview::state0 $page
        set ::cmonDBview::prevsite($page) $::cmonClient::var(site)
        if  { ! $::cmonClient::state } {
            cmonDBview::reset $page
        }
        cmonDBview::updateDBinfo $page
    }
}

## ******************************************************** 
##
## Name: cmonDBview::closeSQLwin   
##
## Description:
## close SQL edit window
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonDBview::closeSQLwin { textw msgw win page } {
     
    set editsql [ $textw get 0.0 end ]
    regsub -all {[\s\t\n]+} $editsql { } editsql
    set sql [ $msgw cget -text ]
    if  { [ string length $editsql ] && ! [ regexp -nocase {^and$} [ string trim $editsql ] ] } {
        set sql "$sql $editsql"
    } 
    destroy $win 

    set timeopt -1
    set histopt -1
    set binsize 1
    set dbname ""
    set dbtable ""
    set timecol "" 
    set valuecol ""
    cmonDBview::sendRequest $page $timeopt $histopt $binsize $dbname \
    $dbtable $timecol $valuecol $sql
}

## ******************************************************** 
##
## Name: cmonDBview::editSQL
##
## Description:
## sends request to server
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:

proc cmonDBview::editSQL { page } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonDBview::statusw($page) "Please select an LDAS site"
		return 
	}
    set login ""
	set passwd ""
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0

    if  { [ catch {
        set timecol [ set ::cmonDBview::${page}(x-col) ]
        set valuecol [ set ::cmonDBview::${page}(y-col) ]
        set dbtable [ set ::cmonDBview::${page}(dbtable) ]
        set dbname [ set ::cmonDBview::dbname($page) ]
        
        if  { [ string match table $dbtable ] } {
            error "Please select a table"
        }
        if  { [ string match column $timecol] || [ string match column $valuecol ] } {
            error "Please select a column"
        }
        foreach { starttime endtime } [ cmonCommon::dbTimeStamp $page $name ] { break }
        
        set sql "select $timecol, $valuecol from $dbtable where $timecol >= timestamp('$starttime') \
            and $timecol <= timestamp('$endtime') and $valuecol is not null"
       
        set win $::cmonDBview::sqlwin
        if  { ! [ winfo exist $win ] } {
            toplevel $win 
            wm title $win "Edit SQL before submitting"
            set flbl [ frame $win.flbl ]
            set lbl1 [ label $flbl.lbl1 -text "Edit SQL if desire and hit SUBMIT; " ]
            set lbl2 [ label $flbl.lbl2 -text "incorrect SQL syntax will result in database errors" -fg red ]
            pack $lbl1 $lbl2 -side left -padx 2
            set msgw [ message $win.fixedtext -aspect 1000 \
		    -text $sql -justify left -font $::MSGFONT -fg black -relief groove ]
            pack $flbl $msgw -side top -fill x
            set textw [ createTextWin $win 10 both yes ]
            $textw configure -bg PapayaWhip -fg black -state normal -height 5 \
                -font $::MSGFONT -width 80 -wrap word -spacing1 5 -spacing2 10 -relief sunken -bd 2
            frame $win.f 
            set but1 [ Button $win.close -text "SUBMIT THIS SQL" -helptext "modify SQL to your desire" \
            -command  "cmonDBview::closeSQLwin $textw $msgw $win $page" -relief raised -bd 2 -width 20 ]
            set but2 [ Button $win.cancel -text "DON'T SUBMIT" -helptext "no modifications" \
            -command  "destroy $win" -relief raised -bd 2 -width 20 ]
            pack $but1 $but2 -side left -expand 1
            pack $win.f -side bottom -anchor center 
        }
        wm deiconify $win
        updateStatus $textw $::cmonDBview::sqlstub 0 brown
        $textw configure -state normal 
    } err ] } {
        appendStatus $::cmonDBview::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonDBview::formatDBtime
##
## Description:
## format database time to readable format
##
## Parameters: 
## dbtime
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::formatDBtime { dbtime } {

    if  { [ catch {
        regexp {^\d{2}(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$} $dbtime \
            -> year mon day hour min sec
    	set localtime [ format "%s/%s/%s %s:%s:%s" $mon $day $year $hour $min $sec ]
    	set gpstime [ utc2gps $localtime 0 ]
    } err ] } {
        return -code error "formatDBview $dbtime $err"
    }
    return [ list $gpstime $localtime ]
}

##*******************************************************
##
## Name: cmonDBview::reset
##
## Description:
## reset data
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::reset { page } {
    
    cmonCommon::updateDBinfo cmonDBview $page
    

}

##*******************************************************
##
## Name: cmonDBview::createHistWin
##
## Description:
## create histogram graph window
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::createHistWin { page parent } {

    if  { [ catch {
	    set site [ string trim $::cmonClient::var(site) ]
        regexp {\.dbviewhist(.+)} $parent -> col2
        if  { ! [ winfo exist $parent ] } {
            toplevel $parent 
	        set statusw [ createTextWin $parent 5 ]  
            set ::${parent}(textw) $statusw
            set ::${parent}(cummPlot) 0
        }    
	    wm deiconify $parent
        wm title $parent "DB view $col2 row count @$site"        
    } err ] } {
         return -code error $err
    }
    return $parent 

}


##*******************************************************
##
## Name: cmonDBview::updateHistogram
##
## Description:
## refresh histograms when time data is refreshed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonDBview::updateHistogram { page histwin sql } {

    if  { [ winfo exist $histwin ] } {
        cmonDBview::replot $page $histwin $sql
    }
    
}

## ******************************************************** 
##
## Name: cmonDBview::closeWin 
##
## Description: 
## report on exact gaps 
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc cmonDBview::closeWin { page parent histwin site } {
   
    set fname $::TMPDIR/[ string trimleft $histwin .].$site
    file delete -force $fname
    destroy $parent
    catch { destroy $histwin }

}
