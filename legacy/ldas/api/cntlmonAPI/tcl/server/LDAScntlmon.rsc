# -*- mode: tcl -*-
## ********************************************************
## Name: LDAScntlmon.rsc
## 
## This is the cntlmon API specific resource file.  It contains
## resource information which is only used by the mpi API.
##
## This file should be located and named as follows:
##
##    /ldas/ldas-0.0/lib/mpiAPI/LDASmpi.rsc
##
## If this file is not found there the mpi API and hence the
## wrapper API will be non-functional.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=1.91;content-type=text%2Fplain
## ********************************************************
;#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## who to obtain login and passwd for client
set passwd_auth "Ed Maros at maros_e@ligo.caltech.edu"

;## Bwatch pipe monitor 
array set ::Bwatch [ list script $::LDAS/bin/nodeUsage ]

;## Verify search user monitor
array set ::Nsearch [ list script $::LDAS/bin/nodeSearch ]

;## flag to allow add User via cmonClient
set ::userAdminOK 0

;## set client key
set ::CLIENTKEY kanbepviTysIanit

;## set client key
set ::CHALLENGE provPat6encio4

;## cmonClient MODIFIABLE RESOURCES
 
;## desc=millisecs between database page
set ::dbDelay 900000

;## desc=millisecs between monitoring log
set ::logDelay 2000

;## desc=millisecs between checking cores
set ::cwDelay 15000

;## desc=DB2 path ! mod=no
set ::DB2PATH /ldcg/IBMdb2

;## desc=status web page ! mod=no
set ::statusHtml /ldas_outgoing/logs/APIstatus.html

;## desc=database web page ! mod=no
set ::databaseHtml /ldas_outgoing/logs/LDASdatabase.html

;## desc=patterns to search for Bwatch ! mod=no
set ::NUSAGEEXIT exiting

;## desc=default number of nodes on beowulf ! mod=no
set ::NUMNODES 9

;## desc=default beowulf node names ! mod=no
set ::beowulfNodes {node0 node1 node2 node3 node4 node5 node6 node7 node8}

;## desc=default database names ! mod=no
set ::dbnames {none }

;## desc=max number of clients at one time
set ::MAX_NUMBER_OF_CLIENTS 20

;## desc=max bytes to retrieve for remote browser 
set ::MAXFILESZ 102400

;## desc=jobs statistics log file
set ::LDASJOBSTATS jobstats.log

;## desc=machine for coreWatch
set ::COREHOST gateway

;## desc=core archive subdirectory
set ::COREARCH savedCores

;## desc=debugger cmd for gdb on suns and linux
array set ::gdb [ list sparc "/ldcg/bin/rxvt -e gdb " intel "/usr/X11R6/bin/rxvt -e gdb " ]

;## desc=debugger cmd for ddd on suns and linux
array set ::ddd [ list intel /ldcg/bin/ddd sparc /ldcg/bin/ddd  ]

;## desc=node pattern
set ::NODEPAT {^node\d+}

;## desc=max number of histogram bins
set ::MAXHISTBINS 10

;## desc=time interval to check api stdout
set ::APISTDOUTTIME 60000

;## desc=CGI host
set ::CGIHOST_IP_ADDRESSES {^(131\.215\.115\.247|127\.0\.0\.)}

;## desc=number of log lines per index
set ::LOG_ARCHIVE_CHUNK_SIZE 5000

;## desc=delay before building log cache
set ::LOG_CACHE_DELAY 900000

;## desc=max stdouterr file bytes to read in
set ::MAX_STDOUTERR 1048576

;## desc=max log files to process in one round
set ::LOGFILTER_CHUNK_SZ 1000

;## desc=log pattern
set ::LOGPAT {^<img\s+src=\"(mail.gif|telephone.gif|ball_red.gif)\"}

;## desc=start time for log Cache
set ::START_LOG_PERIOD 1105144224

;## desc=debug bgcntlmon
set ::DEBUG_bgcntlmon 2

;## desc=define http site name 
set ::SITE_HTTP http://ldas-dev.ligo.caltech.edu

;## desc=max lines for each log page
set ::MAX_LOG_LINES 800

;## desc=max number of log pages
set ::MAX_LOG_PAGES 5

;## desc=periodic cleanup of logs and jobs (millisecs) (1 hrs)
set ::CLEAR_OUTPUT_PERIOD 1800000

;## desc=time to keep old jobs (6 hrs)
set ::JOBS_PURGE_BEFORE 10800

;## desc=time to keep old logs (secs) 5 days
set ::LOGS_PURGE_BEFORE 432000

;## desc=program to test all MPI users and nodes
set ::NODES_USERS_TEST /ldas/bin/nodesUsers.test

;## desc=debug flag for nodesUsers.test
set ::DEBUG_NODESUSERS 1

;## desc=test of all user commands
set ::USER_COMMANDS_TEST user_commands.tcl

;## desc=cancel file for all user commands test
set ::USER_COMMANDS_CANCEL .user_commands.cancel

;## desc=max job timeout in secs 5 min for all user command 
set ::USER_COMMANDS_JOB_TIMEOUT 600

;## desc=createRDS test frame output directory
set ::CREATERDS_FRAME_DIR [ list \
/ldas_outgoing/frames/RDSVerify  \
/scratch/test/frames/RDSVerify ]

;## desc=default test database
set ::TEST_DBNAME ldas_tst

;## desc=clean  up  database  and  RDS  before running  cmonClient  tests
set ::TEST_CLEANUP /ldas/bin/test_cleanup.tcl

;## desc=block sending of emails, e.g. on tandems
set ::BLOCK_SEND_MAIL 0

;## desc=globus port for cntlmon
set ::TCLGLOBUS_PORT 10032

;## desc=control group alias for globus
set ::globus_control_group [ list Mary_O_Lei Michael_Samidi \
Peter_Shawhan  Stuart_Anderson \
Kent_Blackburn  Erik_Espinoza Edward_Maros \
ldas ]

;## desc=option to use globus tcl channel
set ::USE_GLOBUS_CHANNEL 1

;## desc=service for globus tcl channel
set ::SERVICE_NAME ldas

;## desc=tclglobus socket timeout
set ::TCLGLOBUS_SOCKET_TIMEOUT 1

;## desc=max number of emails sent in an hour
set ::MAX_EMAILS_SENT_HOUR 100

;## desc=secs delay before sending a duplicate email
set ::DUPLICATE_EMAIL_DELAY_SECS 120

;## desc=max number of API's email history to retain
set ::MAX_API_EMAIL_HISTORY 100

;## desc=virtual memory limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default datasize \
default  core default maxproc default \
descriptors  default memorylocked default \
filesize  default cputime default ]

;## desc=description for diskcache log patterns
array set ::DISKCACHE_LOG_PATTERNS [ list \
"MOUNT_PT updated" "MOUNT_PT updated if dcmangle is running" \
"does not exist" "mount point does not exist" \
missing "missing mount points" \
"overlap error" "overlap frame time error(s)" \
duplicate "duplicate frame times" \
"errors!; Body:" "cache errors!'" \
"hash updated" "updated (hash updated)" \
"update dt:" "Excessive scan time" \
"Connection timed out" "Thread timed out while accessing cache"]

;## desc=description for log patterns of known errors
array set ::KNOWN_ERRS_LOG_DESC [ list \
"predelete.+aborted after.+seconds in" "Job timed out" \
"dc::endCallbackErr.+dc::endCallbackErr:.+no such symbol" "datacond no such symbol" \
"(frame::killJob.+threads running|frame::reapOrphanThread)" "frame orphaned threads" \
"frame recieved malformed command" "frame received malformed command" \
"Gaps are detected" "diskcache detected frame gaps" \
"duplicate key" "metadata duplicate insertion" \
"unique constraint" "metadata invalid parent key" \
"datacondAPI outputUrls.+giving up" "datacond output url timed out"]

;## desc=memory leak rate limit KB/sec to raise yellow flag
set ::MEM_LEAK_YELLOW_LIMIT 0.3

;## desc=memory leak rate limit KB/sec to raise red flag
set ::MEM_LEAK_RED_LIMIT 2.0

;## desc=days in within gsi certificate will expire
set ::DAYS_TO_CERT_EXPIRE 30

;## desc=max bytes for rmJobFiles response
set ::MAX_REPLY_BYTES 1000000

;## desc=log exec
set ::LOG_EXEC 0
