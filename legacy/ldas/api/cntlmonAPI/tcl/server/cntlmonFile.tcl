## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon server Tcl Script.
##
## functions to login beowulf and return display to client machine
## functions to get file for client
## 
## ******************************************************** 


package provide cntlmonFile 1.0
package require cntlmonJobs 1.0

## ******************************************************** 
##
## Name: cntlmon::getFile
##
## Description:
## retrieve file contents 
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getFile { { type "LDAS-APIs" } } {

	if	{ [ catch { 
        set result "3\n"
        switch  $type {
            LDAS-APIs   { set fname $::statusHtml }
            database { set fname $::databaseHtml }
            default  { set fname "" }
        }
        if  { [ string length $fname ] } {
            set fd [ open $fname r ]
            set data [ read $fd ]
            close $fd
	        set result "0\n$data"
        } else {
            set result "3\nunknown type $type"
        }
	} err ] } {
		set result "3\n$err"
	}
	return $result
}

## ******************************************************** 
##
## Name: cntlmon::getUsersQueue
##
## Description:
## retrieve file contents 
##
## Parameters:
##
## Usage:
##
## Comment:
## use \S for dates to get the {} for empty list

proc cntlmon::getUsersQueue {} {

	if	{ [ catch { 
        set result "3\n"
		set queuefile [ usersQueueFile ]
		if	{ [ regexp crypt $queuefile ] } {
			set data [ decryptFile $queuefile $::MGRKEY ]
		} else {
			set fd [ open $queuefile r ]
			set data [ read $fd ]
			close $fd
		}
		set data [ split $data \n ]
        set blockfile [ file join $::TOPDIR managerAPI blocked.users ] 
        if  { [ file exist $blockfile ] } {
            set blockusers [ dumpFile $blockfile ]
        } else {
            set blockusers {}
        }
		set values [ list ]
		foreach user $data {
			if	{ [ regexp -- {-name (\S+) -password (\S+) -email (\S*) -fullname\s+(.+)\s+-phone\s+(.+) -expires\s+(.+)\s+} $user \
				 -> name passwd email fullname phone expires ] } {                 
                set blocked  [ lsearch $blockusers $name ]                 
				lappend values [ list $name $passwd $email $fullname $phone $expires $blocked ]        
			}
		}
		set values [ lsort -index 0 $values ]
		set values [ list [ join $values ] ]
		set values [ base64::encode64 [ binaryEncrypt $::CHALLENGE $values ] ]
		set result "0\nUpdated.\n$values"
	} err ] } {
		set result "3\n$err"
	}
	return $result

}

## ******************************************************** 
##
## Name: cntlmon::buildUsersQueue
##
## Description:
## build ::QUEUE(USERS) in memory
##
## Parameters:
##
## Usage:
##
## Comment:
## 

proc cntlmon::buildUsersQueue {} {

	if	{ [ catch { 
        set result "3\n"
		set queuefile [ usersQueueFile ]
		if	{ [ regexp crypt $queuefile ] } {
			set data [ decryptFile $queuefile $::MGRKEY ]
		} else {
			set fd [ open $queuefile r ]
			set data [ read $fd ]
			close $fd
		}
		set data [ split $data \n ]
		set values [ list ]
		set ::QUEUE(USERS) [ list ]
		foreach user $data {
			if	{ [ regexp -- {^-name} $user ] } {
				lappend ::QUEUE(USERS) $user
			}
		}
	} err ] } {
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cntlmon::apiStateSummary
##
## Description:
## retrieve file contents 
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::apiStateSummary { args } {

	if	{ [ catch {
		set cmd "\{ puts \$cid \[ mgr::activeJobStatusSummary html \] \}"
       	set result [ getEmergData manager $cmd ]   
		set result "0\n$result"        
    } err ] } {
		set result "3\n$err"
	}
	return $result
}


## ******************************************************** 
##
## Name: cntlmon::pipeHostCmd
##
## Description:
## execute cmd on host via securePipe, accumulate results
## in variable in client and then return the data via
## callback when the cmd has completed.
##
## Parameters:
##
## Usage:
##
## Comment:
## needs callback and unique var to store the variable
## filters for html text 

proc cntlmon::readsshPipe { fid { var "" } { callback ""} { cmdId "" } } {

	if  { ! [ string length [ info proc $callback ] ] } {
        catch { close $fid }
        addLogEntry "proc $callback does not exist, closed $fid" purple
		return
	}
	regexp {^(::[^:]+)} $callback -> client
	set logfilter 0
	set result ""
    if  { [ catch {
		set lines [ read $fid ]
		set lines [ split $lines \n ]
		foreach line $lines {
			if	{ [ regexp {^[\s\t\n]*$} $line ] } {
				continue
			}
			if	{ [ regexp {pid=(\d+)} $line -> pid ] } {
				set ${client}::pid($fid) $pid
				addLogEntry "process $pid for channel $fid started for $client." purple
				continue
			}
            if  { [ regexp {(^\s*<html>.*$)} $line -> line ] } {
                set $var "$line\n"
				continue
            }
            if  { [ regexp {^\s*<html>} [ set $var ] ] } {
				if	{ [ regexp {^<img src=} $line ] } {
                	append $var "\n$line"
					set logfilter 1
				} else {
					append $var $line
					if	{ ! $logfilter } {
						append $var \n
					}
				}
            }
            if  { [ regexp {(.*</html>)} $line -> line ] } {
				set result [ set $var ]
				regsub -all {\n\n} $result \n result
            }
            if  { [ regexp {COMPLETED$} $line ] } {
            	set logmsg "normal completion for $callback"
				set done 1
            }
		}
		;## close fid if program has completed ( broken pipe )
		if 	{ [ eof $fid ] || [ info exist done ] } {
            set result "0\n[ set $var ]"
            close $fid
            if	{ ! [ info exist logmsg ] } {
            	set logmsg "broken pipe for $callback"
            }
            addLogEntry "$logmsg, closed $fid, result '[ string range $result 0 10 ]'" purple
            catch { unset ${client}::pid($fid) }
            unset $var 
        } else {
            set result "2\n$result"
        }  
        if  { [ string length [ info proc $callback ] ] } {
		    $callback $cmdId "$result"   
        } else {
            error "$callback no longer exist"
        }     
    } err ] } {
        addLogEntry "[myName] error: $err" red
		catch { $callback $cmdId "3\n$err" }
		catch { unset $var }
        catch { close $fid }
		catch { unset ${client}::pid($fid) }
    }       
}

## ******************************************************** 
##
## Name: cntlmon::pipeHostCmd
##
## Description:
## execute cmd on host via securePipe, accumulate results
## in variable in client and then return the data via
## callback when the cmd has completed.
##
## Parameters:
##
## Usage:
##
## Comment:
## needs callback and unique var to store the variable

proc cntlmon::pipeHostCmd { host callback cmdId args } {
    if  { [ catch {
        set fid [ securePipe ldas $host ]
        puts $fid "$args"
        flush $fid
        set result ""
        set uniqid [ key::time ]
        set ::$uniqid {}
        ;## wait for pipe to be set up
        after 100
        ;## must use :: for client namespace 
        fileevent $fid readable [ list cntlmon::readsshPipe $fid \
            ::$uniqid $callback $cmdId ]
        set result "Working on it ..."
    } err ] } {
        return -code error "[myName]: $err"
    }    
	return $result	
}		

## ******************************************************** 
##
## Name: ${API}::loadLevel
##
## Description:
## returns process statistics, memory usage for a machine
## via ssh or exec local
## sort field supports sorting 
## Usage:
##       
##
## Comments:
## this must be called within cntlmonClient

proc cntlmon::loadLevel { host { sortfield cpu } { sortorder -decreasing } { userfilter .+ } } {
    if  { [ catch {
        set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]
        if	{ [ regexp {mpi} $::API_LIST ] && [ info exist ::MPI_API_HOST ]} {
        	error "mpiAPI is not active"
        }
        if  { [ regexp $::NODEPAT $host ] } {
            set result [ pipeHostCmd $::MPI_API_HOST "::${client}::reply" $cmdId \
                rsh $host [ file join $::LDAS bin snapSysData ] \
                loadLevel $sortfield $sortorder $userfilter ] 
        } else {
            if  { [ string match $::CNTLMON_API_HOST $host ] } {
                catch { exec /usr/bin/env LD_PRELOAD= [ file join $::LDAS bin snapSysData ] \
                    loadLevel $sortfield $sortorder $userfilter } result
            } else {
                set result [ pipeHostCmd $host "::${client}::reply" $cmdId \
                [ file join $::LDAS bin snapSysData ] loadLevel $sortfield $sortorder $userfilter ] 
            }
        }
        set result "0\n$result"
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: ${API}::dbprogs
##
## Description:
## returns process statistics, memory usage for a machine
## via ssh or exec local
## sort field supports sorting 
## Usage:
##       
##
## Comments:
## this must be called within cntlmonClient

proc cntlmon::dbprogs { prog args } {
    if  { [ catch {
    	if  { ! [ info exist ::METADATA_API_HOST ] } {
    		error "Unable to execute $prog because \
        	::METADATA_API_HOST is undefined" red
        }
        set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]	
        ;## use pipehost to background task	
        set result [ eval "pipeHostCmd $::METADATA_API_HOST ::${client}::reply $cmdId \
            /usr/bin/env LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		    DB2INSTANCE=ldasdb [ file join $::LDAS bin $prog ] $args" ] 
		if	{ [ regexp -nocase {working} $result ] } {
        	set result "2\n$result ($prog $args)"
		} else {
			set result "0\n$result"
		}			
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: ${API}::hostcmd 
##
## Description:
## executes a cmd on local host or via ssh
## via ssh
## Usage:
##       
##
## Comments:
## 

proc cntlmon::hostcmd { host args } {
    set hostdata ""
    if	{  ! [ string compare $host $::CNTLMON_API_HOST ] } {
        set cmd "exec "
        regsub -all {\$} $args \\$ args
        foreach arg $args {
            set cmd "$cmd $arg"
        }
        catch { eval $cmd } hostdata
    } else {
	    catch { execssh $host "$args" } hostdata  
    }
    return $hostdata
}

## ******************************************************** 
##
## Name: cntlmon::getNodeLine
##
## Description:
## reads data from ssh/rsh pipe when there is data to read
##
## Parameters:
##
## Usage:
##
## Comment:
## closes socket for error or when script on the end of
## pipe is finished. Works with Bwatch.

proc cntlmon::getMonitorLine { var fid callback cmdId range { pid "" } } {

    if  { [ catch {
		set lines [ read $fid ]
		set lines [ split $lines \n ]
		foreach line $lines {
        	if  { [ string length $line ] } {
        ;## exit if script is done the number of times specified
            	if  { [ regexp {pid=(\d+)} $line -> pid ] } {
                	regexp {^[::]*(client\d+)} $callback -> client
                	set ::${client}::${var}(pid,$range) $pid
                	addLogEntry "[ set ::${var}(script) ] started for $var nodes $range, pid=[ set ::${client}::${var}(pid,$range) ]"
                	fileevent $fid readable [ list cntlmon::getMonitorLine \
                    	$var $fid $callback $cmdId $range $pid ]                       
				}
            	;## script has exited normally, no need to kill
            	if  { [ regexp -nocase $::NUSAGEEXIT $line ] } {
                	cntlmon::cancelMonitorPipe $var $range $pid $fid $cmdId 0
            	}
            	if  { [ string length [ info proc $callback ] ] } {
                	$callback $cmdId "0\n$line" 
            	} else {
                ;## client has exited, kill to be sure 
                	cntlmon::cancelMonitorPipe $var $range $pid $fid $cmdId 
				}
            }
		}
    } err ] } {
        ;## error in the pipe
        if  { [ string length $err ] } {
            cntlmon::cancelMonitorPipe $var $range $pid $fid $cmdId 
        }
    } 
}

## ******************************************************** 
##
## Name: ${API}::startMonitorPipe
##
## Description:
## retrieve usage of nodes
## via a pipe to ssh 
##
## Usage:
## var identifies a monitor e.g. Bwatch, Nsearch      
## on beowulf nodes
##
## Comments:
## This is done with repeat in the beowulf script
## so it keeps the pipe open until it is canceled
## or if script exits
## pipe fid is keep in client

proc cntlmon::startMonitorPipe { var { repeat 0 } { frequency 0 } { range all } } {
    if  { [ catch {
        set client [ uplevel { namespace tail [ namespace current ] } ]
        if  { ! [ regexp {^::} $client ] } {
            set client ::${client}
        }
        set cmdId  [ uplevel { set cmdId } ]
        if	{ ! [ regexp {mpi} $::API_LIST ] || ! [ info exist ::MPI_API_HOST ]} {
        	error "mpiAPI is not on LDAS API list ( $::API_LIST )"
        }
        set fid [ securePipe ldas $::MPI_API_HOST ]
        set script  [ set ::${var}(script) ]
        puts $fid "$script $::TOPDIR $repeat $frequency $range" 
        flush $fid
        set ${client}::${var}(fid,$range) $fid 
        set result ""
        set times 0
        ;## wait for pipe to be set up
        after 100
        ;## must use :: for client namespace 
        fileevent $fid readable [ list cntlmon::getMonitorLine $var $fid \
            "::${client}::reply" $cmdId $range ]
        set result "0\nMonitoring \
        every [ expr $frequency/1000 ] secs for $repeat times."        
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: ${API}::cancelMonitorPipe
##
## Description:
## retrieve usage of nodes
## via a pipe to ssh 
##
## Usage:
##       
##
## Comments:
## This is done with repeat in the beowulf script
## so it keeps the pipe open until it is canceled
## or if script exits

proc cntlmon::cancelMonitorPipe { var { range all } { pid "" } { fid "" } { cmdId 0 } { kill 1 } } {

    if  { [ catch {
        if  { ! [ string length $pid ] } {
            set procname ""
            set args ""
            set client ""
            catch { foreach { procname cmdId }  [ info level -1 ] { break } }
            if  { [ regexp {^(client\d+)} $procname -> client ] } {
                if  { [ info exist ::${client}::${var}(pid,$range) ] } {
                    set pid [ set ::${client}::${var}(pid,$range) ]
                    set fid [ set ::${client}::${var}(fid,$range) ]
                }
            }
        }
        set result ""
        if  { [ string length $pid ] && $kill } {
            catch { execssh $::MPI_API_HOST kill -9 $pid } err
            catch { close $fid } err
            set result "Monitor $var cancelled."
            addLogEntry $result
            catch { unset ::${client}::${var}(pid,$range) }
            catch { unset ::${client}::${var}(fid,$range) }
        }        
        set result "0\n$result"
    } err ] } {
        set result "3\nError canceling monitor: $err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::apiStatusPage
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cntlmon::apiStatusPage { apilist { repeat 0 } { freq 0 } } {

    #if	{ [ catch {
	#	set client [ uplevel { namespace tail [ namespace current ] } ]
    #    set cmdId  [ uplevel { set cmdId } ]
    #    cntlmon::registerClient $client $cmdId ::cntlmon::apistatus
    #    set aftercall 0
    #} err ] } {
    #    set aftercall 1
    #}
          
     set time [ gpsTime ]
     set t_utc [ utcTime $time ]
     set t_local [ clock format $t_utc -format "%x-%X %Z" ]
     set t_gmt   [ clock format $t_utc -format "%x-%X %Z" -gmt 1 ]
     
     set result ""
     append result "<html><head>"
     append result "<!-- reload this page every 5 minutes -->" 
     append result "<META HTTP-EQUIV=\"REFRESH\" CONTENT=300>"
     append result "<title>LDAS API Status Monitor</title>"
     append result "</head>"
     append result "<BODY BGCOLOR='#DDDDDD' TEXT='#000000'>"
     append result "<h2>LDAS API <font color='blue'>([ join $apilist "," ])</font> Status at $::LDAS_SYSTEM</h2>"
     append result "<h3><font color='red'>$time</font>"
     append result "<font color='green'>$t_local</font>"
     append result "<font color='brown'>$t_gmt</font>"
     append result "</h3>"
    
     foreach api $apilist {
        if { [ catch {
           foreach { properties channels uptime state color } \
              [ cntlmon::apiStatus $api ]  { break }
        } err ] } {
           set color red
           ;## at shutdown, connection may succeed before the api
           ;## has completely expired.
           if { [ regexp {broken pipe} $err ] } {
              set state "$api API is not running: $err"
           } else {
              set state "API host unreachable: $err"
           }   
        }
        
        set gif "<img src=\"ball_$color.gif\">"
        
        if { [ string equal $color red ] } {
           if { ! [ string equal ok $state ] } {
              # set host [ set ::${::LDAS_SYSTEM}($api) ]
              append result "$gif <b><a href=LDAS$api.log.html>$api API</a> [ subst $state ]"
              append result "</b><p>"
           }
        } else {
		   foreach { name host port pid pcpu pmem vsz rss junk } \
           	$properties { break }
           foreach { socks files threads events objects total } \
           	$channels      { break }   
           foreach { s m h d } $uptime { break }
        
           set port ::BASEPORT+[ string range $port end-1 end ]
           append result "$gif <b><a href=LDAS$api.log.html>$api API</a> is running on"
           append result "<i>$host</i> port $port</b>"
           append result "<ul><tt>"
           append result "<li><font color='red'>uptime:"
           append result "<b>$d days + $h:$m:$s</b></font>"
           append result "<li>maximum virtual memory allocated by this API:"
           append result "<font color='red'>$vsz Kb</font>"
           append result "<ul>"
           append result "(current resident set size: $rss Kb)"
           
           ;## if we have a previous vsz value, calculate the
           ;## integrated rate of change of memory usage.
           if { [ info exists ::cntlmon::${api}_vsz ] } {
              set nowmem [ set ::cntlmon::${api}_vsz ]
              if { [ regexp {^[\.\d]+$} $nowmem ] } {
                 if { [ catch { 
                    set dmem [ expr { $vsz - $nowmem } ]
                 } err ] } {
                    # weird bug occurred!!
                    set dmem 0
                 }
                 set dt [ expr { $::LOGINTERVAL / 1000.0 } ]
                 set dmdt [ format "%3.2f" [ expr { $dmem / $dt } ] ]
                 append result "<br>(heap size change rate: $dmdt Kb/sec.)"
              }   
           }
           
           set ::cntlmon::${api}_vsz $vsz
           append result "</ul>"
           ;## character 37 is the percent symbol
           append result "<li>current cpu usage ${pcpu}&#037;"
           append result "<ul>"
           append result "<li>number of open sockets: $socks"
           append result "<li>number of open files: $files"
           append result "<li>number of running threads: $threads"
           append result "<li>number of pending events: $events"
           append result "<li>number of objects registered: $objects"
           append result "<li>total: $total"
           append result "</ul>"
           append result "</tt></ul>"
        }
     }

    append result "</body></html>"
	set result "0\n$result"
    return $result
	
    #if  { $repeat > 0 && $freq > 0 } {
    #    set result "2\n$result"
    #} else {
    #    set result "0\n$result"
    #}

    ;## called by client, update it or called by after, update all
    #if  { ! $aftercall } {
    #    ::${client}::reply $cmdId $result
    #} else {
    #    cntlmon::notifyClients $result ::cntlmon::apistatus 
    #}
    ;## if recurring for a number of times, schedule it
    #if  { $repeat > 0 && $freq > 0 } {
    #    set afterId [after $freq cntlmon::apiTimes [ list $apilist ] [ expr $repeat-1 ] $freq ]
    #    array set ::cntlmon::aptimes [ list afterId $afterId ]
    #}   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::apiStatus 
##
## Description:
## Collect the data about an individual API required to
## prepare a status report.
##
## Parameters:
##
## Usage:
##
## Comments:
## adapted from mgr

proc cntlmon::apiStatus { api } {
     
     if { [ catch {
        set retval [ list ]
        ;## set color and state
        set color green
        set state ok
        set target [ validService $api operator ]
		if	{ ! [ regexp "$api.+operator" $target ] } {
			error $target
		}
        foreach { api host port service sid } $target { break }
        set status [ sock::diagnostic $host ]
        foreach { flag msg } $status { break }
        if { $flag } {
           set color red
           set state $msg
           set retval [ list {} {} {} $state $color ]
           return {}
        }
        
        ;## set props
        if { [ string equal -nocase $host $::MANAGER_API_HOST ] } {
           set props [ sysData $api ]
        } else {
           if { [ catch {
              set sid [ sock::open $api emergency ]
           } err ] } {
              set color red
              set state "$host is up, but $api API is not running"
              set props [ list {} $host $port {} {} {} {} {} 0-0:0:0 ]
              set chans [ list {} {} {} {} {} ]
              set uptime [ list 0 0 0 0 ]
              set retval [ list $props $chans $uptime $state $color ]
			  set error 1
           } else {
              fconfigure $sid -blocking off
              puts $sid "$::MGRKEY \{puts \$cid \[ sysData \]\}"
              set props [ list ]
              set props [ cmd::result $sid ]
              ;## lose the message about the command executed!
              set props [ lrange $props 0 6 ]
              if { ! [ llength $props ] } {
                 set props $api
              }
              ::close $sid
           }
        }
		if	{ [ info exist error ] } {
			return {}
		}
        set props [ linsert $props 1 $host $port ]
        set dummy [ list {} {} {} {} {} {} {} {} 0-0:0:0 ]
        set size [ llength $props ]
        eval lappend props [ lrange $dummy $size end ]
        
        ;## parse uptime
        set uptime [ split [ lindex $props end ] "-:" ]
        set s [ lindex $uptime end   ]
        set m [ lindex $uptime end-1 ]
        set h [ lindex $uptime end-2 ]
        set d [ lindex $uptime end-3 ]
        if { ! [ string length $d ] } { set d 0 }
        if { ! [ string length $h ] } { set h 0 }
        if { ! [ string length $m ] } { set m 0 }
        set uptime [ list $s $m $h $d ]
 
        ;## set chans
        if { [ string equal $api manager ] } {
           set chans [ countChannels ]
        } else {   
           if { [ catch {
              set sid [ sock::open $api emergency ]
           } err ] } {
              set color red
              set state "$host is up, but $api API is not running"
              set props [ list {} $host $port {} {} {} {} {} 0-0:0:0 ]
              set chans [ list {} {} {} {} {} ]
              set uptime [ list 0 0 0 0 ]
              set retval [ list $props $chans $uptime $state $color ]
              return {}
           } else {   
              fconfigure $sid -blocking off
              puts $sid "$::MGRKEY \{puts \$cid \[ countChannels \]\}"
              set chans [ list ]
              set chans [ cmd::result $sid ]
              ::close $sid
           }
        }
        if { ! [ string length $chans ] } {
           set chans [ list 0 0 0 0 0 "(api is too busy to respond)" ]
        } else {
           set dummy [ list {} {} {} {} {} ]
           set size [ llength $chans ]
           eval lappend chans [ lrange $dummy $size end ]
           foreach { socks files threads events objects } $chans {
              break
           }
           set chans     [ llength $socks ]
           lappend chans [ llength $files ]
           lappend chans [ expr [ llength $threads ] / 2 ]
           lappend chans [ llength $events ]
           lappend chans [ llength $objects ]
           lappend chans [ expr [ join [ split $chans ] + ] ] 
        }   
        
     } err ] } {
        catch { ::close $sid }
        if { ! [ string length $err ] } {
           return $retval
        }
        return -code error "[ myName ]: $err"
     }
     return [ list $props $chans $uptime $state $color ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::apiTimes 
##
## Description:
## Collect current time from API and check if any one is
## off by 2 seconds.
##
## Parameters:
##
## Usage:
##
## Comments:
## supports broadcasting to multi-clients
## support of broadcasting temporarily disabled since
## selections may be different 

proc cntlmon::apiTimes { apilist { repeat 0 } { freq 0 } } {

    #if	{ [ catch {
	#	set client [ uplevel { namespace tail [ namespace current ] } ]
    #    set cmdId  [ uplevel { set cmdId } ]
    #    cntlmon::registerClient $client $cmdId ::cntlmon::apitimes
    #    set aftercall 0
    #} err ] } {
    #    set aftercall 1
    #}
    
    set time [ gpsTime ]
    set t_utc [ utcTime $time ]
    set t_local [ clock format $t_utc -format "%x-%X %Z" ]
    set t_gmt   [ clock format $t_utc -format "%x-%X %Z" -gmt 1 ]
     
    set result ""
    append result "<html><head>"
    append result "<!-- reload this page every 5 minutes -->" 
    append result "<META HTTP-EQUIV=\"REFRESH\" CONTENT=300>"
    append result "<title>LDAS API Time Monitor</title>"
    append result "</head>"
    append result "<BODY BGCOLOR='#DDDDDD' TEXT='#000000'>"
    append result "<h3>LDAS API <font color='blue'>([ join $apilist "," ])</font> times at $::LDAS_SYSTEM</h3>"
    append result "<h3><font color='black'>$time</font>&nbsp;&nbsp;"
    append result "<font color='green'>${t_local}&nbsp;</font>"
    append result "<font color='brown'>${t_gmt}&nbsp;</font>"
    append result "</h3>"
    append result "<font color='black' size=-1><i><p>The Network Time Protocol (NTP) is used to
    synchronize the time of network computers; to verify that NTP is functioning, 
    <p>cntlmon API polls each API for time and compares it to cntlmon API's time upon reply;<BR>\
    the API time is shown in red if it differs from cntlmonAPI by more than 2 seconds,
    indicating an possible out-of-sync condition.</font></i><p>"
    
    set heading [ format "%15.15s%15.15s%20.20s%20.20s" \
        API host "API time" "cntlmonAPI time" ]
    append result "<pre><font black size=+1><u>$heading</u></font>\n"
    
    set datefmt "+%m/%d/%Y %H:%M:%S"
    set datecmd /bin/date
    ;## compare api date vs cntlmon's date
    foreach api $apilist {
        set seqpt "get Time for $api"   
        if  { [ catch { 
        	if	{ ! [ regexp $api $::API_LIST ] } {
        		error "$api is not on LDAS API list ( $::API_LIST )"
        	}
            set host [ set ::[ string toupper $api ]_API_HOST ]
            if  { ! [ string equal cntlmon $api ] } {            
                set msg "\{ puts \$cid \[ exec $datecmd \"$datefmt\" \]\}"
		        set curdata [ getEmergData $api $msg ] 
                ;## remove the lines return by emergency socket proc 
                set apidate [ split $curdata \n ] 
				set apidate [ join $apidate ]
                if  { [ string length $apidate ] } { 
                    set apiseconds [ clock scan $apidate -base [clock seconds] ]
                } else {
                    set apiseconds 0
                    set apidate "No response"
                }
                set refdate [ exec $datecmd $datefmt ]  
                set refseconds [ clock scan $refdate -base [clock seconds] ]
                if  { [ expr abs( $refseconds - $apiseconds ) > 2 ] } {
                    append result "<font color='blue'>[ format "%15.15s" $api API ]</font>\
                    <font color='brown'>[ format "%15.15s" $host ]</font>\
                    <font color='red'>[ format "%20.20s" $apidate ]</font>\
                    <font color='green'>[ format "%20.20s" $refdate ]</font>\n"
                } else {
                    append result "<font size=+1 color='blue'>[ format "%15.15s" $api API ]</font>\
                    <font color='brown'>[ format "%15.15s" $host ]</font>\
                    <font color='green'>[ format "%20.20s" $apidate ]</font>\
                    <font color='green'>[ format "%20.20s" $refdate ]</font>\n"
                }
            } 
        } err ] } {
            append result "<font color='blue'>[ format "%15.15s" $api API ]</font></td>\
            <font size=+1 color='brown'>[ format "%15.15s" $host ]</font>\
            <font size=+1 color='red'>$err</font>\n"
        }
    }
    
    append result "</pre>"
    append result "</body></html>"
    
    #if  { $repeat > 0 && $freq > 0 } {
    #    set result "2\n$result"
    #} else {
    #    set result "0\n$result"
    #}
    
    set result "0\n$result"
    return $result
    
    ;## called by client, update it or by after, update all clients
    #if  { !$aftercall } {
    #    ::${client}::reply $cmdId $result
    #} else {
    #    cntlmon::notifyClients $result ::cntlmon::apitimes 
    #}
    ;## if recurring for a number of times, schedule it
    #if  { $repeat > 0 && $freq > 0 } {
    #    set afterId [after $freq cntlmon::apiTimes [ list $apilist ] [ expr $repeat-1 ] $freq ]
    #    array set ::cntlmon::apitimes [ list afterId $afterId ]
    #}
}     

## ******************************************************** 
##
## Name: cntlmon::stopUpdate 
##
## Description:
## cancel updates by client, update scheduling automatically
## stops if no clients 
##
## Parameters:
##
## Usage:
##
## Comments:
## supports broadcasting to multi-clients

proc cntlmon::stopUpdate { client type } {
    
    if  { [ catch {
        if  { [ regexp -nocase {API-(.+)} $type -> str ] } {
            set var ::cntlmon::api[ string tolower $str ]     
            unregisterClient $client $var
        }
    } err ] } {
        return "3\n$err"
    }
    return "0\nupdate cancelled for $var."
}

## ******************************************************** 
##
## Name: cntlmon::coreArchives 
##
## Description:
## cancel updates by client, update scheduling automatically
## stops if no clients 
##
## Parameters:
##
## Usage:
##
## Comments:
## supports broadcasting to multi-clients

proc cntlmon::coreArchives { range { filter .+ } } {

	set coredata [ list ]
    regexp {(\d+)-(\d+)} $range -> starttime endtime
	if	{ [ catch {
		foreach core [ glob -nocomplain $::TOPDIR/$::COREARCH/*.core ] {
			set corename [ file tail $core ]
			set dir [ file dir $core ]
			set corelist [ split $corename . ]
			foreach { api gpstime acore } [ split $corename .] { break }
            if  { $gpstime < $starttime || $gpstime > $endtime } {
                continue
            }
			regexp {([^\d]+)} $api -> apiname
			if	{ [ regexp $filter $api ] } {
				set fd [ open [ file join $dir ${api}.${gpstime}.file ] r ]
				set data [ read $fd ]
				;## remove filename in the front
				regexp {:\s*(.+)} $data -> data 
				close $fd
				
				lappend ${apiname}_data [ list $api $gpstime $core $data ] 
			}
		}
		foreach apidata [ lsort -ascii [ info vars *_data ] ] {
			set apidata [ lsort -integer -decreasing -index 1 [ set $apidata ] ]
			foreach data $apidata { 
			 	lappend coredata $data 
			}
		}
	} err ] } {
		return "3\n$err"
	}
	return "0\n$coredata"
}

## ******************************************************** 
##
## Name: cntlmon::deleteCore 
##
## Description:
## cancel updates by client, update scheduling automatically
## stops if no clients 
##
## Parameters:
##
## Usage:
##
## Comments:
## supports broadcasting to multi-clients

proc cntlmon::deleteCores { corefiles range { filter .+} } {

	if	{ [ catch {
		foreach corefile $corefiles {
			file delete -force $corefile
			regexp {(.+).core$} $corefile -> fstem
			file delete -force ${fstem}.file
		}
		set result [ coreArchives $range $filter ]
	} err ] } {
		return "3\n$corefile delete error: $err"
	}
	return $result
}

## ******************************************************** 
##
## Name: cntlmon::jobLists 
##
## Description:
## return  a list of jobs based on range specification
##
## Usage:
##       
## Comments:
## 4/24/08 included cntlmon at end of job_stats line

proc cntlmon::jobLists { jobstr } {
	
	if	{ [ catch {
		set alljobs [ split $jobstr , ]
		set joblist [ list ]
		cntlmon::job_statsColumns

		foreach job $alljobs {
			;## start-end pattern
			if	{ [ regexp {[^\d]*(\d+)-[^\d]*(\d+)} $job -> startjob endjob ] } {	
				for { set i $startjob } { $i <= $endjob } { incr i 1 } {
					lappend joblist $i 
				}
				continue
			}
			;## Last set of jobs pattern, get jobs from job_stats.log 
			if	{ [ regexp -nocase {L(\d+)} $job -> numjobs ] } {
                set cmd "exec tail -$numjobs [ file join $::TOPDIR logs job_stats.log ]"
                catch { eval $cmd } data
                set data [ split $data \n ]
                foreach line $data {
					foreach [ eval $cmds ] $line { break } 
                    lappend joblist $jobid 
                }
			}
			;## single job pattern
			lappend joblist $job
		}
		if	{ [ llength $joblist ] > 100 } {
			set joblist [ lrange $joblist 0 99 ]
		}
	} err ] } {
		if	{ [ regexp {emergency.+error} $err ] } {
			set msg "No jobs found."
		} else {
			set msg $err
		}
		return -code error $msg
	}
	return $joblist	
}
		
## ******************************************************** 
##
## Name: cntlmon::jobOutput 
##
## Description:
## get job output given jobId
##
## Usage:
##       
## Comments:
## 

proc cntlmon::jobOutput { jobids } {

	if	{ [ catch {
		set dirs [ list $::TOPDIR $::TOPDIR/jobs ]
		set files [ list ]
		set joblist [ jobLists $jobids ]
		foreach jobid $joblist {
			if	{ [ catch {
				set jobdir [ jobDirectory $jobid ]
			} err ] } {
				addLogEntry "$jobid error: $err" 2
				continue
			}
			set jobdir [ jobDirectory $jobid ]
			regexp {^(.+)/} $jobdir -> updir
			if 	{ [ lsearch -exact $dirs $updir ] == -1 } {
				lappend dirs $updir
			}
			if	{ [ file isdirectory $jobdir ] } {
				lappend dirs $jobdir
				set entries [ glob -nocomplain [ file join $jobdir * ] ]
				foreach f $entries {
					if { [file isdirectory $f] } {
						lappend dirs $f
					} else {
						lappend files $f
					}
				}
			}
		}
   		set dirs [ lsort -dictionary $dirs ]
    	set files [ lsort -dictionary $files ]
		set result "0\ndir|0|\{$dirs\} \{$files\}" 
	} err ] } {
        set result "3\n$err"
    }
	return $result
}

## ******************************************************** 
##
## Name: securePipe
##
## Description:
## Opens an ssh tunnel to the remote host.
## When ssh falls back to rsh under these circumstances,
## two bad lines will get returned at the top of the
## result and one bad line at the bottom.  When it doesn't
## fall back, one bad line will be returned at the bottom.
##
## Parameters:
##
## Usage:
## To handle the potential bad lines:
##
##   puts $fid "ps -Ao fname,user"
##   set data [ split [ read $fid ] "\n" ]
##   if { [ string match "ps -Ao fname,user" [ lindex $data 0 ] } {
##      set data [ lrange $data 2 end-1 ] ;## rsh fallback
##   } else {
##      set data [ lrange $data 0 end-1 ] ;## ssh1 normal
##   }
##
##
## If the command is forced into the background, an immediate
## gets will return a background task i.d. and a process i.d.:
##
##   puts $fid "ps -Ao fname,user &"
##   gets $fid -   [ 2 ] 10359
##
## And the next command sent through after the task completes
## will have the status line for the last backgrounded task
## appended to it's output:
##
##   puts $fid date
##   read $fid -
##   Fri Jan 26 13:19:11 PST 2001
##   [2]  + Done  ps -Ao fname,user
##
## along with the usual blank line.
##
## And this works:
##
##   puts $fid "ssh1 -n user@host $command"
##
## So you can use securePipe to tunnel into private networks.
##
## Comments:
## If the command sent down the pipe never returns, the pipe
## will become useless.  It will not block, but will always
## return a blank line.
## I have not figured out how to make this manage
## x-forwarding.
## Using this to connect as another user on the local host
## can fail in unexpected ways.
##
## with RedHat 7.1, need to locate up ssh agent
## by running script ldasagent and set up ::env(SSH_AUTH_SOCK)
## do not use ssh -n

proc securePipe { user host } {
     
     if { [ catch {
        foreach { status msg } [ sock::diagnostic $host ] { break }
        if { $status } {
           return -code error $msg
        }
        set fid [ open "|ssh ${user}@$host" a+ ]
        fconfigure $fid -blocking off
        fconfigure $fid -buffering full
        ;## let ssh settle...
        sleep 2000
        set junk [ read $fid ] 
        ;## now make sure we got authenticated (this will throw
        ;## a "broken pipe" exception if auth failed).
        if { [ catch {
           fconfigure $fid -buffering line
           puts $fid hostname
        } err ] } {
           if { [ regexp {broken pipe} $err ] } {
              set err "ssh/rsh auth failed for ${user}@$host"
           }
           return -code error $err
        }
        sleep 1000
        fconfigure $fid -buffering full
        set junk [ read $fid ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $fid
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::getBlockedDSOs
##
## Description:
## return the list of blocked DSOs
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::getBlockedDSOs {} {

    if  { [ catch {
        set blockedDSOs [ dumpFile [ file join $::TOPDIR managerAPI blocked.dsos ] ]
        set result "0\nUpdated.\n$blockedDSOs"
    } err ] } {
        set result "0\nUpdated."
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::getDSOs
##
## Description:
## return the list of standard DSOs.
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::getDSOs {} {
    
    if  { [ catch {
        set ::DSOs [ list ]
        if	{ [ file exist $::DYNLIB_DIRECTORY ] } {
        	foreach dso [ glob -nocomplain $::DYNLIB_DIRECTORY/*.so ] {
            	regexp {libldas([^\.]+).so} [ file tail $dso ] -> dso
            	lappend ::DSOs $dso
       	 	}
        	set ::DSOs [ lsort -dictionary $::DSOs ]
        }
    } err ] } {
        set ::DSOs [ list ]
        addLogEntry $err 2 
    }
}
    
## ******************************************************** 
##
## Name: cntlmon::getDBtabcols
##
## Description:
## return the list of standard DSOs.
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::getDBtabcols {} {

    set dbcolsfile [ file join $::TOPDIR cntlmonAPI dbtabcols.tcl ]
    if  { [ file exist $dbcolsfile ] } {
        source $dbcolsfile 
    } elseif  { [ regexp {metadata} $::API_LIST ]  } {
        addLogEntry "Cannot locate $dbcolsfile" 2
    }
}


## ******************************************************** 
##
## Name: ${API}::NodesUsersTest
##
## Description:
## run scripts and return results
## Usage:
##       
##
## Comments:
## this must be called within cntlmonClient

proc cntlmon::runTest { type userinfo args } {
    if  { [ catch {
    	
        set allapis [ list manager diskcache frame metadata ligolw datacond mpi eventmon cntlmon ]
        set missing [ list ]
    	foreach api $allapis {
        	if	{ [ lsearch $::API_LIST $api ] == -1 } {
            	lappend missing $api
            } 
        }
        set errors ""
        if	{ [ llength $missing ] } {
        	set errors "Some tests will fail as [ join $missing " " ] API(s) missing, "
        }
        set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]	
        ;## use pipehost to background task	
        
        switch -regexp -- $type {
        	user_commands { set secret .user_commands
             				if 	{ [ string match x.509 [ set ::${client}::serverKey ] ] } {	                                        
                               set ::USER_COMMANDS_TEST user_commands.tcl
                               set runmode gsi	 
                           } else {	 
                               set ::USER_COMMANDS_TEST user_commands.tcl
                               set runmode tcl
                           }	 
            			   set cmd $::USER_COMMANDS_TEST }
        	nodesUsers { if	{ [ string length $errors ] } {
            				error $errors
                         }
            			 set secret .nodesusers
            			 set cmd $::NODES_USERS_TEST
                         set runmode tcl }
    	}
        ;## prepare the user info
        regexp -- {-name (\S+)} $userinfo -> user
        regexp -- {-password\s+(\S+)} $userinfo -> passwd
        set secret $::env(HOME)/$secret
        
        foreach { host pid port } $::logCacheUtils { break }
        
        set fd [ open $secret w ]
        puts $fd "set ::USER $user"
        puts $fd "set ::pw $passwd"
        puts $fd "set ::MGRKEY $::MGRKEY"
        if	{ [ info exist ::MPI_API_HOST ] } {
        	puts $fd "set ::mpi(host) $::MPI_API_HOST"
        }
        if	{ [ string length $::cntlmon::mpiport ] } {
        	puts $fd "set ::mpi(emergency) $::cntlmon::mpiport"
        }
        puts $fd "set ::logCacheUtils \[ list $host %pid $port \]"
        puts $fd "set ::RUNMODE $runmode"
        close $fd 
        file attributes $secret -permissions 0600 
        
        set pipe [ open "|$cmd $args" a+ ]
		set uniqid [ key::time ]
        set ::$uniqid {}
		fileevent $pipe readable [ list cntlmon::readsshPipe $pipe ::$uniqid ::${client}::reply $cmdId ]
        fconfigure $pipe -blocking off
		set result "2\n$errors working on it ..."	
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: ${API}::localCmd
##
## Description:
## run local scripts and return results
## Usage:
##       
##
## Comments:
## this must be called within cntlmonClient

proc cntlmon::localCmd { cmd args } {
    if  { [ catch {
        set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]	
        ;## use pipehost to background task	
    
        set pipe [ open "|[ set $cmd ] $args" a+ ]
		set uniqid [ key::time ]
        set ::$uniqid {}
		fileevent $pipe readable [ list cntlmon::readsshPipe $pipe ::$uniqid ::${client}::reply $cmdId ]
        fconfigure $pipe -blocking off
		set result "2\nworking on it ..."	
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::refreshMountPoints
##
## Description:
## wrapper to get mount points for client
## Usage:
##       
##
## Comments:
## this must be called within cntlmonClient

proc cntlmon::refreshMountPoints {} {

    if  { [ catch {
        cntlmon::getMountPoints
        set result "0\nset ::MOUNT_PT \{$::MOUNT_PT\}"
    } err ] } {
        return -code error "3\n$err"
    }
    return $result 
}

## ******************************************************** 
##
## Name: ${API}::cancelAllCmdsTest
##
## Description:
## drop a cancel file to terminate user_commands test
## Usage:
##       
##
## Comments:

proc cntlmon::cancelAllCmdsTest { {type user_commands} } {

    switch -regexp -- $type {
        user_commands { set prog user_commands.tcl 
        				set cancelfile $::USER_COMMANDS_CANCEL }
        nodesUsers { set prog nodesUsers.test
        			 set cancelfile .nodesUsers.cancel }
    }
    set msg "0\n"
    if	{ [ catch {
    	set pids [ cntlmon::getPid $prog ]
    	if	{ [ string length $pids ] } {    	
    		catch { touch $cancelfile } err
    		addLogEntry "$type test ($pids) cancelled by user $err." blue
    		set msg "2\nTest is being cancelled. Please wait for results."
   		} else {
        	addLogEntry "$prog has terminated" purple
            catch { file delete $cancelfile }
    	}
    } err ] } {
    	addLogEntry $err red
        set msg "3\n$err"
    }
    return $msg
}

## ******************************************************** 
##
## Name: cntlmon::debugger
##
## Description:
## wrapper to get mount points for client
## Usage:
##       
##
## Comments:

proc cntlmon::debugger { debug corefile } {

	if	{ [ catch {
		if	{ [ file exist $corefile ] } {
    		regsub {core} $corefile "file" fname
            set platform intel
            if	{ [ file exist $fname ] } {
            	set rc [ catch { exec grep SPARC $fname } data ]
            	if	{ !$rc } {
                	set platform sparc
                    set cmd "[ set ::${debug}(sparc) ] tclsh $corefile &"
                } else {
                	set cmd "ssh -n -obatchmode=yes $::EVENTMON_API_HOST \
                    [ set ::${debug}(intel) ] tclsh $corefile &"
                }
                set rc [ catch { eval "exec $cmd" } err ]
                if	{ $rc } {
            		error "Error executing '$cmd': $err"
            	}                     	
            } 
        } else {
        	error "$corefile does not exist"
        }
   	} err ] } {
    	return -code error $err
    }
    return "0\n$debug invoked for $corefile."
}
                
## ******************************************************** 
##
## Name: cntlmon::NoOp
##
## Description:
## dummy command that returns ok to client in an attempt
## to flush out globus requests 
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::NoOp  {} {

	return "0\nNoOp"
}
