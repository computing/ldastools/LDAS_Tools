#!/ldcg/bin/tclsh

## ********************************************************
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) script for ldas.
##
## this script exercises all the ldas user commands
## and level 1 and level 2 createRDS
## it requires cmonClient ldas command files
## and it runs on ldas system to be tested
##
## requires test_cleanup.tcl
## Note: cleanup of the database could take a while if the
## number of process rows are large.
##
## THIS WORKS WITH GLOBUS TCL CHANNELS ONLY
## ********************************************************

##******************************************************** 
##
## Name: serverOpen, serverCfg and serverClose
##
## Description:
## opens a listening socket for manager emails,
## handler to reading email replies
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments
## tcl sockets only
proc serverOpen {{port 0}} {
    set lsid [socket -server serverCfg -myaddr [info hostname] $port]
    foreach {::LADDR ::LHOST ::LPORT} [fconfigure $lsid -sockname] {break}
    return lsid
}

proc serverCfg  {sid addr port} {
    
	fconfigure $sid -buffering line -blocking off	
    fileevent $sid readable [list getResult $sid]
	        
    return
}

proc serverClose {sid} {
    catch {close $sid}
    return
}

## DO NOT puts to stderr for ssh pipe runs

set ::auto_path "/ldas/lib . $auto_path"
if	{ ! [ regexp "/ldas/bin" $::env(PATH) ] } {
    set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

set startTime [ clock seconds ]

set cntlmonstate /ldas_outgoing/cntlmonAPI/cntlmon.state
if	{ [ file exist $cntlmonstate ] } {
	source /ldas_outgoing/cntlmonAPI/cntlmon.state
} else {
	catch { exec cat /etc/ldasname } ::RUNCODE
	set ::RUNCODE [ string trim [ string toupper $::RUNCODE ] \n ]
	
}
set API test
source /ldas_outgoing/LDASapi.rsc
;## load genericAPI after tclglobus

source /ldas_outgoing/cntlmonAPI/LDAScntlmon.rsc

;## default timeout for tclglobus socket to 5 secs
if	{ ! [ info exist ::TCLGLOBUS_SOCKET_TIMEOUT ] } {
	set ::TCLGLOBUS_SOCKET_TIMEOUT 5
}
;## get a user
set info $::env(HOME)/.user_commands
source $info

if	{ ![ info exist ::RUNMODE ] } {
	set ::RUNMODE tcl
}
if	{ [ string equal gsi $::RUNMODE ] } {
	source /ldas/lib/cntlmonAPI/LDAScntlmon.ini
	set ::userinfo "-name $::USER -password x.509 -email persistent_socket"
} else {
	;## open a local port to listen for email replies
	set localport 0
	set ::startTime [ clock seconds ]
	;## open a port to listen for returns
	# set server_sid [serverOpen $localport ]
	# set ::userinfo "-name $::USER -password md5protocol -email $::LHOST:$::LPORT"
    set ::userinfo "-name $::USER -password md5protocol -email persistent_socket"
}

;## always do this after loading tclglobus as libcrypto from globus is needed
;## or else results in core dump when using globus socket
package require generic

set ::pw [ key::md5 [ binaryDecrypt $::CHALLENGE [ base64::decode64 $::pw ] ] ]  
file delete -force $info

proc traceme {args} {
	set var [ lindex $args 0 ]
	debugPuts "var [ set $var ], called by [ info level -1 ]"
}

## ******************************************************** 
##
## Name: debugPuts
##
## Description:
## output debug or interim results to a log file
##
## Usage:
##       
## Comments:

proc debugPuts { msg } {
	puts $::fout $msg
	flush $::fout
}


## ******************************************************** 
##
## Name: exitTest
##
## Description:
## record results so far, cleanup and exit
##
## Usage:
##       
## Comments:

proc exitTest {} {

	set details ""
	if	{ [ info exist ::fout ] } {
		flush $::fout
		seek $::fout 0 start
		set details [ read -nonewline $::fout ]
		close $::fout
		;## strip embedded html 
		regsub -all {<html>} $details {html}  details
		regsub -all {</html>} $details {/html}  details
	}
	if	{ ! $::NUMSENT } {
		set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='red'>TEST DID NOT RUN</font>"
	}  elseif	{ ( $::NUMSENT == $::NUMRECV ) && $::NUMSENT} {
		if	{ !$::::NUMFAIL } {
			set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='$::darkgreen'>TEST PASSED</font>"
		} else {
			set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='red'> TEST FAILED</font>"
		}
	} else {
		set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='red'>TEST FAILED</font>"
	}
	
    if  { [ info exist ::flock ] } {
	    file delete -force $::LOCKFILE 
	    catch { file delete -force $::USER_COMMANDS_CANCEL } err
    }

	set endTime [ clock format [ clock seconds ] -format "%x %X %Z" ]
	set startTime [ clock format $::startTime -format "%x %X %Z" ]
    
    ;## will be captured by cntlmon via pipe
    set result "<html><pre>From $startTime - $endTime\n$text\n$details\n</pre></html>"
	puts $result
    flush stdout
    
    ;## also output to file
    catch { close $::fout }
    set fid [ open $::LOGFILE w ]
    puts $fid $result
    close $fid
    
	exit
}

## ******************************************************** 
##
## Name: outputInterimReport
##
## Description:
## display interim results if desired by user
##
## Usage:
##       
## Comments:
;## do not flush stdout or it will hang

proc outputInterimReport {} {
  
    if  { $::INTERIM } {   
        if  { ! [ expr ${::NUMRECV}%$::INTERIM ] && ${::NUMRECV} >= $::INTERIM } {          
            flush $::fout
	        set fd [ open $::LOGFILE r ]
	        set details [ read -nonewline $fd ]
            close $fd
            set endTime [ clock format [ clock seconds ] -format "%x %X %Z" ]
	        set startTime [ clock format $::startTime -format "%x %X %Z" ] 
            set numRemain [ expr $::NUMTESTS - $::NUMSENT ]
            set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, $numRemain more tests to run"
            puts "<html><pre>From $startTime - $endTime\n$text\n$details\n</pre></html>"
        }
    }
}    

## ******************************************************** 
##
## Name: strip
##
## Description:
## strip ldas job reply
##
## Usage:
##       
## Comments:

proc strip {data} {
    regsub -all -- {[\n\s\t]+} $data { } data
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set data [string replace $data $idx end]
    }
    return [string trim $data]
}

## ******************************************************** 
##
## Name: bgerror
##
## Description:
## catch bgerrors
##
## Usage:
##       
## Comments:

proc bgerror {err} {
    set trace {}
    append ::out "\nError: $err"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
		debugPuts "*** Stack Trace ***\n$trace"
    }	
    exitTest
}

## ******************************************************** 
##
## Name: killer
##
## Description:
## remove 
##
## Usage:
##       
## Comments:

;## remove all running program of a pattern
proc killer { pattern } {
	catch { exec ps -A -o "pid,args" | grep $pattern } data
   	foreach line [ split $data \n ]  {
    	if	{ [ regexp {(\d+).+sh} $line -> pid ] } {
            debugPuts "killing $pattern $pid, $line"
            catch { kill -9 $pid }
        }
    }
}

## ******************************************************** 
##
## Name: getJobStatus
##
## Description:
## print out job number if job has been submitted ok
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments
## do not report anything if job was successfully submitted
## tcl sockets only using md5salt

proc getJobStatus { sid usercmd text cmd var } {

	;## will not reach here for md5salt type of jobs
	set jobid unknown 
    if  { [ catch {  
        set reply [ set $var ]
   		;## debugPuts "getJobStatus reply '$reply'"
   		after cancel $::afterId
   
   		if	{ [ regexp {md5salt\s+(\d+)} $reply -> md5salt ] } {
       		set md5digest [ key::md5 ${::pw}${md5salt} ]
        	puts $sid [ list "md5digest" $md5digest ]
            ;## need this to enable next msg to be read
            unset $var
            fileevent $sid readable [ list cmd::receive $sid $var [ list getJobStatus $sid $usercmd $text $cmd ] ] 
		} else { 
        	if  { ! [regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ]} {
        		incr ::NUMFAIL 1
            	debugPuts "\nsubmit error: $cmd\n[string trim $reply]\n*** <font color='red'>$usercmd $text-----FAILED</font>"
            	set ::DONE 1           
        	} else {
        		incr ::NUMSENT 1
            	set ::CURRENT_JOB(usercmd) $usercmd
            	set ::CURRENT_JOB(cmd) $cmd
            	set ::CURRENT_JOB(text) $text
                ;## debugPuts "set ::SUBMITTED $jobid"
            	set ::SUBMITTED $jobid
                fileevent $sid readable [ list getResult $sid $usercmd $text $cmd ]
            }
            if	{ ! [ regexp {persistent_socket} $::userinfo ] } {
            	catch { close $sid }
            }
    		catch { unset $var } 
        }
    } err ] } {
        debugPuts "<font color='red'>ERROR: jobid $jobid $err</font>\n"
        set ::SUBMITTED -1
        set ::DONE 1
        catch { close $sid }
    }
}

## ******************************************************** 
##
## Name: getResult
##
## Description:
## results via GSI persistent sockets 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments
## do not report anything if job was successfully submitted
## results to tcl listening socket or gsi socket

proc getResult {sid args } {

	set reply ""
	if	{ [ catch {  
		if	{ [ regexp {gtxio|sock} $sid ] } {
			set usercmd [ lindex $args 0 ]
			set text [ lindex $args 1 ]
			set cmd [ lindex $args 2 ]
		} else {
			set usercmd $::CURRENT_JOB(usercmd)
    		set cmd $::CURRENT_JOB(cmd)
    		set text $::CURRENT_JOB(text)
		}
		set reply [ read $sid ]
        ;## debugPuts "getResult reply $reply"
		after cancel $::afterId
        
		if	{ [ string length $reply ] } {
        	;## debugPuts "$sid usercmd $usercmd RECV: $reply"
    		if	{ [ info exist ::ABORT_JOBID ] } {
    			set pat "$::ABORT_JOBID.+(killed|aborted)"
    			if	{ [ regexp -nocase $pat $reply ] } {
					set ::abortJobSid $sid
                    debugPuts "\n*** $::ABORT_JOBID <font color='brown'>$usercmd $text aborted by abortJob</font>"
					unset ::ABORT_JOBID
        			set done 1
				}
        	}
			;## tclglobus socket tends to merge both running as and reply into one buffer
			if	{ ! [ info exist done ] } { 
				set jobid nojob			
				if	{ [ regexp -- "Your job is running as.+(${::RUNCODE}\\d+)" $reply -> jobid ] } {
					incr ::NUMSENT 1
					set ::SUBMITTED $jobid
				} 
				if 	{ [ regexp -nocase -- "Subject:\[^\\n\]*\\n(${::RUNCODE}\\d+)" $reply -> jobid ] } {
        			set errmsg TBD
        	;## do not logged the job used to test abortJob
    				if	{ [ regexp -nocase -- {error![\n\s]*([^=]+)} $reply -> errmsg ] } {
    					incr ::NUMFAIL 1
        				debugPuts "\nJob Error: $cmd\n[string trim $errmsg]\n*** $jobid <font color='red'>$usercmd $text-----FAILED</font>"
    				} elseif { [ regexp -nocase -- {aborted|killed} $reply -> errmsg ] } {
						if	{ [ string equal abortJob $usercmd ] } {
							debugPuts "\n*** $jobid <font color='$::darkgreen'>$usercmd $text-----PASSED</font>"
                            if	{ [ info exist ::abortJobSid ] } {
                        		debugPuts "1:unset ::abortJobSid $::abortJobSid, sid $sid" 
                        		unset ::abortJobSid
                        	}
						} else {
							incr ::NUMFAIL 1
        					debugPuts "\n*** $jobid <font color='red'>$usercmd $text aborted-----FAILED</font>"  
						} 
        			} else {
    					debugPuts "\n*** $jobid <font color='$::darkgreen'>$usercmd $text-----PASSED</font>"
                        if	{ [ info exist ::abortJobSid ] } {
                        	debugPuts "2:unset ::abortJobSid $::abortJobSid, sid $sid" 
                        	unset ::abortJobSid
                        }
        			}
        			lappend ::WIPE_LIST $jobid
					incr ::NUMRECV 1
    			} 
				if	{ [ string equal nojob $jobid ] } {
    				incr ::NUMFAIL 1
					error "No jobid: $usercmd\n$reply"
    			} else {
					error done
				}
			}
		}
        if	{ [ eof $sid ] } {
        	error "end of file detected on client globus channel $sid"
        }   
	} err ] } {
		if	{ ![ string match done $err ] && ! [ regexp "end of file" $err ] } {
    		debugPuts "<font color='red'>Error: $err\n$reply</font>"
		}
		catch { close $sid }
		if	{ ! [ info exist ::abortJobSid ] || ! [ string equal $::abortJobSid $sid ] } {
			set ::DONE 1
		}
	}
}

## ******************************************************** 
##
## Name: sendJob
##
## Description:
## send an ldasjob to manager
##
## Parameters:
## usercmd - ldas cmd
## cmd - cmd text from file or inline e.g. dataPipeline
## tag - additional ldas cmd info e.g. inspiral
##
## Usage:
##  
## 
## Comments

proc sendJob { usercmd cmd tag { wait 1 } } {

	if	{ [ file exist $::CANCELFILE ] } {
		debugPuts "<font color='red'>Test cancelled by user</font>"
		exitTest
	}
	catch { after cancel $::afterId }
    
    set retval ""  
    set tmp [ list ] 
    ;## strip comments and new lines 
    foreach line [split $cmd "\n"] {
        set line [string trim $line]
        if {[regexp {^\s*#} $line] } {
            continue
        }
        lappend tmp $line
    }
    set cmd [join $tmp "\n"]
	set cmd [subst -nobackslashes $cmd]
	regsub -all -- {[\s\n\t]+} $cmd { } cmd
    
	if	{ [ string equal $usercmd $tag ] } {
		set text ""
	} else {
		set text $tag
	}
	
    regsub -all -- {[\s\n\t]+} $cmd { } cmd
	
    catch { unset ::DONE }
	
    if 	{ [ catch { 
    	if	{ [ string equal gsi $::RUNMODE ] } { 
        	set sid [ gt_xio_socket -host -timeout $::TCLGLOBUS_SOCKET_TIMEOUT \
            -gsi_auth_enabled $::GLOBUS_MANAGER_API_HOST $::TCLGLOBUS_HOST_PORT ]
            fconfigure $sid -buffering full -blocking 0
        	fileevent $sid readable [ list getResult $sid $usercmd $text $cmd ]
        	fconfigure $sid -translation binary -encoding binary
        } else {
        	set sid [ socket -async $::MANAGER_API_HOST 10001 ] 
          	set timeout 250
     		set short  [ expr { round($timeout / 100.0) } ]
     		set medium [ expr { $short * 9 } ]
     		set long   [ expr { $short * 90 } ]
            
        	;## three stage ping response eval. once after 1% of
        	;## the panic limit, again after 10%, then at the limit.
        	after $short
        	if { [ catch { fconfigure $sid -peername } ] } {
           		after $medium
           		if { [ catch { fconfigure $sid -peername } ] } {
              		after $long
           		}   
        	}
        
        	set err [ fconfigure $sid -error ]
        	if 	{ [ string length $err ] } {
           		return -code error $err
        	}
        	fconfigure $sid -buffering line
            set ::CURRENT_JOB(usercmd) $usercmd
    		set ::CURRENT_JOB(cmd) $cmd
    		set ::CURRENT_JOB(text) $text

			set var [ key::time ]
			catch { unset ::$var } 
			fconfigure $sid -buffering full -blocking 0
            
			# trace variable ::$var w [ list getJobStatus $sid $usercmd $text $cmd ]
    		fileevent $sid readable [ list cmd::receive $sid $var [ list getJobStatus $sid $usercmd $text $cmd ] ] 	
            #fileevent $sid readable [ list getJobStatus $sid $usercmd $text $cmd ] 
        }
    } err ] } {
    	incr ::NUMFAIL 1
        debugPuts "\nsocket error: \n'$cmd'\n$err\n*** <font color='red'>$usercmd $text-----FAILED</font>"
        return
   	}   
    
  
   	puts $sid "ldasJob \{$::userinfo\} \{$cmd\}"  
	flush $sid 
    
    ;## debugPuts "$sid ldasJob \{$::userinfo\} \{$cmd\}" 
    
    catch { unset ::DONE }
    catch { unset ::SUBMITTED }
    
    # trace variable ::SUBMITTED w traceme
    # trace variable ::DONE w traceme
    
    if	{ $wait } {	
    	;## debugPuts "vwait $usercmd $text"
        set ::afterId [ after  $::USER_COMMANDS_JOB_TIMEOUT [ list set ::DONE 1 ] ]
    	vwait ::DONE
        if	{ $::DONE == -1  } {
    		debugPuts "$usercmd $text timed out."
    	}
    }
	outputInterimReport
}

## ******************************************************** 
##
## Name: numTests2Run
##
## Description:
## determine the total number of tests to run
##
## Usage:
##       
## Comments:

proc numTests2Run {} {

    ;## count tests from cmonClient 
    catch { exec ls -lR ${::cmddir} | grep "\\\.cmd" } data
    catch { exec ls -lR ${::cmddir} | grep createRDS_ } data1
    catch { exec ls -lR ${::cmddir} | grep createRDS_H } data2
    catch { exec ls -lR ${::cmddir} | grep createRDS_L } data3
	catch { exec ls -lR ${::cmddir} | grep createRDS_mit } data4
    
    set numRDS 0
    set numRDS_LHO	0
    set numRDS_LLO 	0
    set numRDS_MIT	0
    
    set ::NUMTESTS [ llength [ split $data \n ] ]
    if	{ [ regexp {createRDS_} $data1 ] } {	    		
    	set numRDS [ llength [ split $data1 \n ] ]
    }
    if	{ [ regexp {createRDS_H} $data2 ] } {
    	set numRDS_LHO [ llength [ split $data2 \n ] ]
    }
    if	{ [ regexp {createRDS_L} $data3 ] } {
    	set numRDS_LLO [ llength [ split $data3 \n ] ]
    }
    if	{ [ regexp {createRDS_mit} $data4 ] } {
		set numRDS_MIT [ llength [ split $data4 \n ] ]
    }
    
	switch -regexp -- $::SITE {
			mit  { set ::NUMTESTS [ expr $::NUMTESTS - $numRDS + $numRDS_MIT ] }
			llo  { set ::NUMTESTS [ expr $::NUMTESTS - $numRDS + $numRDS_LLO ] }
			lho  { set ::NUMTESTS [ expr $::NUMTESTS - $numRDS + $numRDS_LHO ] }
			default  {}
	}

    ;## include the ones submitted internally
    incr ::NUMTESTS [ llength $::moretests ]
    ;## debugPuts "::NUMTESTS $::NUMTESTS numRDS $numRDS numRDS_LHO $numRDS_LHO numRDS_LLO $numRDS_LLO other tests [ llength $::moretests ]"
    debugPuts "Total number of tests to run on $::SITE is $::NUMTESTS"

}

## ********************************************************
## MAIN
## ********************************************************

;## initialization

set ::LOCKFILE $::TOPDIR/cntlmonAPI/.user_commands.lock
set ::LOGFILE $::TOPDIR/cntlmonAPI/user_commands.log
set	::CANCELFILE $::TOPDIR/cntlmonAPI/$::USER_COMMANDS_CANCEL
set ::WIPE_LIST [ list ]
set ::SHAREDDIR /ldas/share/ldas
set ::ABORTCMD $::SHAREDDIR/ldascmds/dataStandAlone/stochastic.cmd
set ::darkgreen #568c3a
set ::moretests [ list getChannels cacheGetTimes getFrameCache rmJobFiles abortJob ]
set ::USER_COMMANDS_JOB_TIMEOUT [ expr $::USER_COMMANDS_JOB_TIMEOUT * 1000 ]

switch -regexp -- $::LDAS_SYSTEM {
	ldas-dev 	{ set ::SITE dev }
	ldas-test 	{ set ::SITE test }
	ldas-cit	{ set ::SITE cit }
	ldas-wa		{ set ::SITE lho }
	ldas-la		{ set ::SITE llo }
    ldas-mit    { set ::SITE mit }
	tandem*		-
	default		{ set ::SITE $::env(HOST) }
}

catch { exec uname -n } ::HOST

;## job counters
set ::NUMSENT 0
set ::NUMRECV 0
set ::::NUMFAIL 0
set ::LATERCMDS [ list ]
set ::TIMEOUT_RECV 1000

;## only 1 copy runs at a time
if	{ [ file exist $::LOCKFILE ] } {
	set fd [ open $::LOCKFILE r ]
    set data [ read $fd ]
    close $fd
    regexp {(\d+)} $data -> thatpid
   
	catch { exec ps -Ao fname,pid,args | grep $thatpid } data
    
    foreach line [ split $data \n ] { 
    	if	{ [ regexp -- "(\\S+)\\s+$thatpid\\s+(.+)" $line -> prog rest ] } {
        	if	{ [ regexp {user_commands\.tcl} $rest ] } {
            	puts  "<html><pre><font color='red'>ERROR: Another copy of user command test is currently running \
				( remove lock file $::LOCKFILE to run test if you think this is not true ).\n</font></pre></html>"
                exit
            }
         }
    }
}

;## check for cancellation
if	{ [ file exist $::CANCELFILE ] } {
	puts  "<html><pre><font color='red'>ERROR: Test cancelled by user before any jobs have been submitted. ( Remove
    lock file $::CANCELFILE to run test if you think this is not true ). \n</font></pre></html>"
	exit
}

if  { $argc } {
    set ::INTERIM [ lindex $argv 0 ]
} else {
    set ::INTERIM 0
}

set ::startTime [ clock seconds ]

;## write out a log 
catch { file delete $::LOGFILE }
set ::fout [ open $::LOGFILE a+ ]

set ::flock [ open $::LOCKFILE w ]
puts $::flock [ pid ]
close $::flock	

catch { exec test_cleanup.tcl } err
# debugPuts "cleanup $err"

;## get jobs cmds from cmonClient lib
set ::cmddir [ file join $::SHAREDDIR ldascmds ]
set date [ clock format [ clock seconds ] -format "%m%d" ]
;## debugPuts "listening for reply at $::LHOST:$::LPORT"

;## compute the number of jobs to run
numTests2Run

if  { $::NUMTESTS <= $::INTERIM || !$::INTERIM } {
    set ::INTERIM 0
    debugPuts "No interim results are displayed."
} else {
    debugPuts "Display interim results after every $::INTERIM tests\n"
} 

;## just run level1 RDS first 

foreach subdir [ lsort [ glob -nocomplain $cmddir/* ]] {
	set usercmd [ file tail $subdir ]
	regsub {\.cmd} $usercmd {} usercmd
	
	if	{ [ string equal createRDS [ file tail $subdir ] ] } {
		switch -regexp -- $::SITE {
			mit { set cmds [ glob -nocomplain $subdir/createRDS_mit*.cmd ] }
			llo  { set cmds [ glob -nocomplain $subdir/createRDS_L_*_1.cmd ] 
				   set ::LATERCMDS [ glob -nocomplain $subdir/createRDS_L_*_3.cmd ] }
			lho  { set cmds [ glob -nocomplain $subdir/createRDS_H_*_1.cmd ] 
				   set ::LATERCMDS [ glob -nocomplain $subdir/createRDS_H_*_3.cmd ]}
			default  { set cmds [ glob -nocomplain $subdir/createRDS*_1.cmd ]
                    lappend cmds $subdir/createRDS_mit.cmd
				   set ::LATERCMDS [ glob -nocomplain $subdir/createRDS*_3.cmd ] }
		}
	} else {
    	set cmds [ lsort [ glob -nocomplain $subdir/*.cmd ] ]
	}
    foreach cmd $cmds {
		set tag [ file tail $cmd ]
		regsub {\.cmd} $tag {} tag         
		set fd [ open $cmd r ]
		set cmdtext [ read -nonewline $fd ]
		close $fd
        sendJob $usercmd $cmdtext $tag
    }
} 

;## additional cmds not in cmonClient
;## site specific cmds
if	{ [ string match llo $::SITE ] } {	
	set ifo L
} else {
	set ifo H
}

;## getChannels
switch -regexp -- $::SITE {
	llo -
	lho -
	cit -
	mit  { set cmd "getChannels  -returnprotocol http://daq -interferometer $ifo -frametype {RDS_R_L3} -time 756518892  -metadata 1" }
	dev -
	test { set cmd "getChannels  -returnprotocol http://daq -interferometer $ifo -frametype {RDS_R_L3} -time 733042094  -metadata 1" }
}
	
sendJob getChannels $cmd getChannels

;## cacheGetTimes
set cmd "cacheGetTimes -types {RDS_R_L3 RDS_R_L2 RDS_R_L1 R} -ifos {H L HL G} -start 751651213 -end 757699213"
sendJob cacheGetTimes $cmd cacheGetTimes

;## getFrameCache
set cmd "getFrameCache -returnprotocol http://frame.cache"
sendJob getFrameCache $cmd getFrameCache

;## now run level 2 createRDS
foreach file $::LATERCMDS {
	set tag [ file tail $file ]
	set fd [ open $file r ]
	set cmd [ read -nonewline $fd ]
	close $fd
	sendJob createRDS $cmd $tag
}

;## test rmJobFiles
if  { [ llength $::WIPE_LIST ] } {
	set cmd "rmJobFiles -jobids [ list $::WIPE_LIST ] -extensions {all}"
	sendJob rmJobFiles $cmd rmJobFiles
} 

## ******************************************************** 
##
## Name: issueAbortJob
##
## Description:
## test abortJob by submitting a long job and then abort it
##
## Usage:
##       
## Comments:
## may not work on a tandem without frame data for dataStandAlone

proc issueAbortJob {} {
	
	set fd [ open $::ABORTCMD r ]
	set cmd [ read -nonewline $fd ]
	close $fd
	
	#regsub -all -- {[\n\s\t]+} $cmd { } cmd
	set cmd [subst -nobackslashes $cmd]
	set cmd [ string trimleft $cmd " " ] 

    sendJob createRDS $cmd createRDS 0

	incr ::NUMSENT -1
    ;## set a timeout
    set ::afterId [ after  $::USER_COMMANDS_JOB_TIMEOUT [ list set ::SUBMITTED -1 ] ]

    vwait ::SUBMITTED

    if	{ $::SUBMITTED == -1  } {
    	debugPuts "Job to be aborted was timed out; abortJob was not submitted."
    	return
    }
   	set pat "($::SUBMITTED)\\\s+dataStandAlone"
   	for { set i 0 } { $i < 50000 } { incr i 500 } {
    	catch { exec cat $::TOPDIR/jobs/activejobs.tcl } data
		if	{ [ regexp $pat $data -> ::ABORT_JOBID ] } {
			set cmd "abortJob -stopjob $::ABORT_JOBID"
			debugPuts "waited $i msecs before issuing abortJob"
        	sendJob abortJob $cmd abortJob
			break
		} else {
			after 500
		}
	} 
    if	{ $i >= 50000 } {
   	 	debugPuts "$::SUBMITTED is not an active job"
    }
}

issueAbortJob
exitTest
