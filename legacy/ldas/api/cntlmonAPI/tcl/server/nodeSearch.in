#! /ldcg/bin/tclsh

set ::LDAS /ldas
set auto_path ". /ldas/lib $auto_path"
set API mpi
set TOPDIR [ lindex $argv 0 ]

;## suppress some assumptions
set ::API_LIST [ list ]

set apirsc [ file join $::TOPDIR ${API}API LDAS${API}.rsc ]
if 	{ [ file exists $apirsc ] } {
  	source $apirsc
} 	else {
   	source [ file join /ldas lib ${API}API LDAS${API}.rsc ]
}   
 
## ******************************************************** 
##
## Name: sleep
##
## Description:
## Does a sleep like the UNIX sleep.  Evaluation is
## suspended at the line where sleep is called, but event
## loop processing continues.
##
## Parameters:
## ms - milliseconds to sleep
##
## Usage:
##
## Comments:
##

proc sleep { ms } {
     if { $ms < 1 } { return {} }
     set uniq [ clock clicks ]
     set ::__sleep__tmp__$uniq 0
     after $ms set ::__sleep__tmp__$uniq 1
     vwait ::__sleep__tmp__$uniq
     unset ::__sleep__tmp__$uniq
}
## ******************************************************** 

## ********************************************************
## Name: bgerror 
##
## Description:
## Handles uncaught errors and terminates 
##
## Usage:
##
## Comments:

proc bgerror { msg } {
	 puts $::errorInfo 
	 puts "exiting due to bgerror: $msg"
	 exit
}


## ******************************************************** 
##
## Name: parseUsage
##
## Description:
## Parse usage data from snapSysData usageMap command to
## format a line for a node
##
## Parameters:
## node
## usage -	line returned for node by running snapSysData on it
##
## Usage:
##
## Comments:
##
proc parseUsage  {} {
	
	set result ""
	foreach user [ lsort [ info vars ::search* ] ] {
		set nodelist [ set $user ]
		set numNodes [ llength $nodelist ]
		regsub {::} $user {} user
		append result "$user -- $numNodes [ join $nodelist , ] -- "
        unset ::$user
	}
	if   { ! [ string length $result ] } {
	     set result "No wrapper jobs found"
	}
	puts $result
}


## ******************************************************** 
##
## Name: getUsage
##
## Description:
## rsh from beowulf to node to run snapSysData usageMap command to
## get usage info
##
## Parameters:
## node
## Usage:
##
## Comments:
## Nodes tend to respond in the order sent but I am not counting on it.

proc getUsage { node timediff } {
    catch { exec rsh -n $node /ldas/bin/snapSysData verifySearch } data
	set user ""
	set wrapper ""
	if	{ [ catch { 
		foreach { rnode user wrapper } $data {
			if	{ [ string equal beowulf $rnode ] } {
				set rnode $::node0
			}
			regexp {n[^\d]+(\d+)} $rnode -> num
			lappend ::$user n$num
		}
	} err ] } {
		puts "error on $node: $err"
	}
    if	{ [ lsearch -exact $::repliedNodes $node ] == -1 } {
    	lappend ::repliedNodes $node
    }
    if   { [ llength $::repliedNodes ] >= $::numNodes } {
    	 ;## tells caller I am done with 1 round
	 	 parseUsage 
		 ;## DO NOT write to stderr
         puts "Completed all nodes. $timediff more iterations."
	 	 if  { [ string match 0 $timediff ] } {
	     	puts "exiting ..."
			exit
	 	 }
    }
}

## ******************************************************** 
##
## Name: main
##
## Description:
## executes snapSysData usageMap on each node to generate
## a usage line per node.
## repeat as specified by repeat and frequency command line args.
##
## Parameters:
##
## Comments:
##

;## site specific resources 
set TOPDIR [ lindex $argv 0 ]

set apirsc [ file join $::TOPDIR mpiAPI LDASmpi.rsc ]
if { [ file exists $apirsc ] } {
   source $apirsc
 } else {
   source [ file join /ldas lib mpiAPI LDASmpi.rsc ]
 }   
 
set listOfHosts $::NODENAMES
set first [ lindex $listOfHosts 0 ]
set last [ lindex $listOfHosts end ]
if	{ [ string match $first $last ] } {
	set listOfHosts $first
}
 
set numNodes [ llength $listOfHosts ]

if  { $argc > 1 } {
	set repeat [ lindex $argv 1 ]
	set frequency [ lindex $argv 2 ]
} else {
	set repeat 0
	set frequency 0
}

;## DO NOT write to stderr
puts "pid=[ pid ]"
;## let caller capture this
flush stdout 
set node0 [ lindex $listOfHosts 0 ]
after 1000
for { set ::times 0 } { $::times <= $repeat } { incr ::times 1 } {
     set ::repliedNodes [ list ]
     set timediff [ expr $repeat - $times ]
     foreach node $listOfHosts {
    	after 10 getUsage $node $timediff   
     }
     sleep $frequency
}

;## exit when getUsage detects all completed
vwait forever



