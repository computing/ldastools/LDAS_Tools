## ******************************************************** 
##
## Name: createClient 
##
## Description:
## Namespace model for client handler creation. This
## procedure generates a namespace named with a
## by order from cntlmon server.
##
## The newly created namespace has all the functions
## required by a client handler, and inherits all
## of the functions of the calling namespace, which
## includes the genericAPI.tcl, but does this without having
## to duplicate an instance of the calling namespace,
## as a spawned interpreter would have to do.
##
## Usage:
##
## Comments:
##
## Note that the code inside the procs is not evaluated
## until the generated proc is called.
##
## When the namespace has completed it's task it is
## deleted, and all of it's resources are freed.
##
## during the time that the namespace has real work to do,
## it handles all client request from the socket; 
## socket communications are handled so as to prevent their
## blocking the event loop.
## Note also that the body of this proc does not need to
## be evaluated uplevel because it is in it's own
## namespace.
## similar to asstMgr.tcl in managerAPI

;#barecode
package provide cntlmonClient 1.0
;#end

## vars used
## sid 	- socket used to communicate with a client
## cmdId - counter for the number of cmds submitted by client
## cmd$cmdId - array to hold data for a cmd

proc createClient { name { login "" } } {
     
     if { [ regexp ::${name} [ namespace children ] ] } {
        ;##puts "Namespace \"$name\" already exists." 2
        return {}
        }
      
     ;## private variables
     namespace eval $name {
        set name [ namespace tail [ namespace current ] ]
        set sid         {}
		set cmdCount		0
        set cmditems { callback repeat iterations frequency afterid }
        set tracedvars [ list ]
        set monitors { Bwatch Nsearch }
        }
;#end

## ******************************************************** 
##
## Name: ${name}::evalCmd
##
## Description:
## evals a cmd 
##
## Usage:
##        Called internally by ${name}::seq.
##
## Comments:
## Tcl exception thrown when the command returned is less
## than 4 characters in length.

proc ${name}::evalCmd { cmdlist } {
	 set name [ namespace tail [ namespace current ] ]
     if { [ catch {
        if { [ string length $cmdlist ] > 0  } {
            set cmdlist [ split $cmdlist \n ]
            set callback [ lindex $cmdlist 0 ]
	        set repeat [ lindex $cmdlist 1 ]
	        set frequency [ lindex $cmdlist 2 ]
            set clientCmd [ lindex $cmdlist 3 ]
            set clientCmd [ split $clientCmd : ]
 
            foreach { client cmdId login passwd } $clientCmd { break }
            regsub -all {.} $passwd {*} mockpasswd 
            set mockpasswd [ string range $mockpasswd 0 5 ]

            set cmd [ lindex $cmdlist 4 ]
			set logcmd $cmd
			if	{ [ regexp -nocase {useradmin} $logcmd ] } { 
				set logcmd "request to add/update/delete user"
				set cmd [ join [ lrange $cmdlist 4 end ] \n ]				
			} elseif { [ string match "*submitLDASjob*" $logcmd ] } {
                set logcmd "[ string range $logcmd 0 200 ]..."
            }
            
            if	{ [ string match "*NoOp*" $logcmd ] }  { 
        		if	{ $::DEBUG == 2 } {
            		addLogEntry "received cmd: $logcmd" purple 
                } 
            } else {
            	addLogEntry "received cmd: $logcmd"
            }
            regexp {^\{(.*)\}$} $cmd cmd cmd
            regexp {^\"(.*)\"$} $cmd cmd cmd

     ;## generate a cmdId if new
	        if	{ ! [ string compare $cmdId "new" ] } {
		        set cmdId [ set ${name}::cmdCount]
		        incr ::${name}::cmdCount 1
                ${name}::registerCmd $cmdId $callback $repeat $frequency
	        }
	        if	{ [ llength $cmd ] > 0 } {
	 	        after 0 [ list ${name}::bgCmd $cmdId $cmd $login $passwd ]
                # ${name}::bgCmd $cmdId $cmd $login $passwd
	        }
        }
    } err ] } {
		addLogEntry "evalCmd err: $err" 2
		if	{ [ info exist cmdId ] } {
			catch { reply $cmdId "3\n$err" }
        	unregisterCmd $cmdId
		}
    }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::registerCmd 
##
## Description:
## registers the callback, runtimes and frequency of
## a new cmd or updates it for an old cmd 
##
## Usage:
##       
##
## Comments:

proc ${name}::registerCmd { cmdId callback repeat frequency } {

    set name [ namespace tail [ namespace current ] ]
	array set ::${name}::callback [ list $cmdId $callback ]
	array set ::${name}::repeat [ list $cmdId $repeat ]
	array set ::${name}::iterations [ list $cmdId 0 ]
	array set ::${name}::frequency [ list $cmdId $frequency ]
    if  {  ! [ info exist ::${name}::afterid($cmdId) ] } {
        array set ::${name}::afterid [ list $cmdId "" ]
    }
}

## ******************************************************** 
##
## Name: ${name}::unregisterCmd 
##
## Description:
## unregisters the callback, runtimes and frequency of
## a cmd that is repeating  
##
## Usage:
##       
##
## Comments:

proc ${name}::unregisterCmd { cmdId } {
    set name [ namespace tail [ namespace current ] ]
    foreach item ${name}::cmditems {
        catch { unset ${name}::$item($cmdId) } 
    }
}

## ******************************************************** 
##
## Name: ${name}::bgCmd
##
## Description:
## For commands that needs refresh this sends a periodic
## update to the client
##
## Usage:
##        Called internally by ${name}::evalCmd.
##
## Comments:
## this is recursive 

proc ${name}::bgCmd { cmdId cmd { login "" } { passwd "" } } {	

    if  { [ catch { 
        set name [ namespace tail [ namespace current ] ]
        ;## if command is canceled, do nothing
        if  { [ string length $login ] } {
			# set passwd [ binaryDecrypt $::CHALLENGE [ base64::decode64 $passwd ] ]
            if	{ [ string equal x.509 $passwd ] } {
            	if	{ ! [ string equal priviledged [ set ::${name}::status ] ] } { 
                	error "You are not authorized for this function."
                }
            } else {
            	validateLogin $login $passwd [ set ::${name}::serverKey ] 1 
            }
        }  
        if  { ! [ info exist ::${name}::callback($cmdId) ] } {
            return
        }
	    set result [ eval $cmd ]
        set repeat [ set ${name}::repeat($cmdId) ]
        set frequency [ set ${name}::frequency($cmdId) ]
	    if	{ $repeat > 0 && $frequency > 0 } {
		    incr ::${name}::iterations($cmdId) 1 
		    if	{ [ set ::${name}::iterations($cmdId) ] <= $repeat } {
	 		    set afterid [ after $frequency [ list ${name}::bgCmd $cmdId $cmd ] ]
			    set ::${name}::afterid($cmdId) $afterid 
		    } else {
                set ::${name}::afterid($cmdId) ""
            }
	    } else {
            set ::${name}::afterid($cmdId) ""            
        }
        if	{ [ string match "*NoOp*" $cmd ] }  { 
        	if	{ $::DEBUG == 2 } {
            	addLogEntry "replied to cmd $cmd [ string length $result ] bytes" purple 
            }
        } else {
        	addLogEntry "replied to cmd $cmd [ string length $result ] bytes" purple
        }
        reply $cmdId $result
        if  { [ set ::${name}::iterations($cmdId) ] > $repeat } {
            unregisterCmd $cmdId
        }
    } err ] } {
        ;##puts "bgCmd err: $err"
		if	{ [ string length $err ] } {
			addLogEntry "bgCmd err: $err" 2
		}
        catch { reply $cmdId "3\n$err" }
        catch { unregisterCmd $cmdId   }     
    }
}

## ******************************************************** 
##
## Name: ${name}::setsid 
##
## Description:
## Opens a connection to an API, returning the socket i.d.
## (a string of the form sockNN) of the channel opened.
##
## Usage:
##          set sid [ ${name}::getsid $API ]
##
## Comments:
## Connects to the operator socket of the named API by
## default.

proc ${name}::setsid { cid } {
    set name [ namespace tail [ namespace current ] ]  
    set ${name}::sid $cid
    ;##puts "setsid, sid=[ set ${name}::sid ]"
    ;##puts "old handler=[ fileevent $cid readable ]"
	fileevent $cid readable [ list ${name}::sockhandler $cid ]
    ;##puts "new handler=[ fileevent $cid readable ]"
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::reply 
##
## Description:
## Send result back to client
##
## Usage:
## 	 [ ${name}::reply $cmdId 0 $result 
##
## Comments:
## Connects to the operator socket of the named API by
## default.
## handles globus tcl channels also

proc ${name}::reply {cmdId result} {
    	
    set name [ namespace tail [ namespace current ] ]
    if  { [ info exist ::${name}::handle ] } {
    	${name}::replyGlobus $cmdId $result
      	return
    }
    if  { [ catch {    
        set callback [ set ::${name}::callback($cmdId) ]
        set afterid  [ set ::${name}::afterid($cmdId) ]        
	    ;##puts "in $name reply, callback=$callback,cmdId=$cmdId"
		set results "$callback\n$name:$cmdId:$afterid\n$result"
        set cid [ set ${name}::sid ]
        set msg [ cmd::size $results ]
        regexp {[~]*(\d*)[\n]*([^~]+)} $msg -> size remain
        ;## debugPuts "sent $size bytes [ string range $results 0 20 ]"
		puts $cid $msg
        flush $cid 
        ;## must flush to get msg through
        ;## this is included to flush out the buffer PR#3030
        fconfigure $cid -blocking off
		if { $::DEBUG } { leakLogger }
	} err ] } {
		if	{ [ regexp {read.+callback} $err ] } {
			return
		}
        addLogEntry "[ myName ] $err, deleting $name if still exist." 2
        catch { ${name}::delete } err
		# addLogEntry "deleting $name: $err" 
		if { $::DEBUG } { leakLogger }
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: ${name}::replyGlobus 
##
## Description:
## Send result back to client via globus socket
##
## Usage:
## 	 [ ${name}::reply $cmdId 0 $result 
##
## Comments:

proc ${name}::replyGlobus { cmdId result } {
    set name [ namespace tail [ namespace current ] ]
    if  { [ catch {    
        set callback [ set ::${name}::callback($cmdId) ]
        set afterid  [ set ::${name}::afterid($cmdId) ]        
		set result "$callback\n$name:$cmdId:$afterid\n$result"
        cntlmonGlobus::sendCmd $result [ set ${name}::handle ]
		if { $::DEBUG } { leakLogger }
	} err ] } {
        addLogEntry "[ myName ] $err, deleting $name if still exist." 2
        catch { ${name}::delete } err
		if { $::DEBUG } { leakLogger }
		return -code error $err
	}    
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::sockhandler
##
## Description:
## When the "current" socket becomes readable, handle the
## returned "stuff".  This should never block the event
## loop, since it is called by "fileevent readable" and
## uses the cmd::result procedure from the cmd.tcl module
## to read the socket in non-blocking mode.
##
## Usage:
##        Callback for fileevent.  Do not call directly.
##
## Comments:

proc ${name}::sockhandler { sid } {

    if  { [ catch {
        set name [ namespace tail [ namespace current ] ]
        ;##puts "in sockhandler, sid=$sid,name=$name"
        set cmdlist [ cmd::result $sid ]
        ;##puts "cmdlist=$cmdlist"
    } err ] } {
        ;##puts stderr "sockhandler socket error: $err"
        ${name}::delete 
        return 
    }
    if  { [ eof $sid ] } {
        addLogEntry "eof detected on $sid: $err"
        ${name}::delete 
        return
    }
    if  { [ catch { 
        ${name}::evalCmd $cmdlist
    } err ] } {
        ;##puts "sockhandler evalCmd error: $err"  
		addLogEntry "sockhandler evalCmd error: $err" 2
    } 
    ;##puts "exiting sockhandler" 
}

## ******************************************************** 
##
## Name: ${name}::GlobusChannelHandler
##
## Description:
## When the "current" socket becomes readable, handle the
## returned "stuff".  This should never block the event
## loop, since it is called by "fileevent readable" and
## uses the cmd::result procedure from the cmd.tcl module
## to read the socket in non-blocking mode.
##
## Usage:
##        Callback for fileevent.  Do not call directly.
##
## Comments:
## handler of globus tcl channels

proc ${name}::GlobusChannelHandler { sid } {

	set name [ namespace tail [ namespace current ] ]
    if  { [ catch {
    	if  { [ eof $sid ] } {	
        	set eof 1
        	error "eof detected on $sid"
       	}
        set buffer [ ::read $sid ]
        if	{ [ string match "*cntlmon::NoOp*" $buffer ] }  { 
        	if	{ $::DEBUG == 2 } {
            	addLogEntry "read [ string length $buffer ] bytes globus channel '$buffer'" purple 
            }
        } else {
        	addLogEntry "read [ string length $buffer ] bytes globus channel '$buffer'" purple 
        }
               
        if	{ [ string length $buffer ] } {
            while 	{ [ regexp {~~(\d+)\n(.+)} $buffer -> size bufdata ] } { 
            	if	{ ! [ regexp {^~~\d+\n} $bufdata ] } {
            		set buffer [ string range $bufdata $size end ]
                	set bufdata [ string range $bufdata 0 $size ]
                	${name}::evalCmd $bufdata
            	} else {
            		set buffer $bufdata
            	}
        	}
        }                
    } err ] } {
    	if	{ ! [ info exist eof ] } {
        	addLogEntry $err 2 
       	}
        ${name}::delete 
        return 
    }
}

## ******************************************************** 
##
## Name: ${name}::readCallback
##
## Description:
## When the "current" socket becomes readable, handle the
## returned "stuff".  Invoked by tclglobus.
##
## Usage:
##        Callback for fileevent.  Do not call directly.
##
## Comments:

proc ${name}::readCallback { user_parms handle result buffer data_desc } {
	set name [ namespace current ]
	addLogEntry "$name readCallback read callback $user_parms handle $handle result  \
		$result buffer $buffer data_desc $data_desc" purple	
    
    if	{ [ regexp -nocase {(end of file|Operation was canceled)} $result ] } {
    	addLogEntry $result 2
    	${name}::delete 
        return
    }
    if	{ [ catch {     	
        if	{ [ string length $buffer ] } {
            set begin 0
        	while { [ regexp {~~(\d+)([^~]+)} $buffer -> size bufdata ] } {    
                set begin 1           
        		if	{ ! [ info exist ::${name}::server_data_size ] } {
        			set ::${name}::server_data_size $size
                   	set ::${name}::server_data $bufdata
                }
                if	{ [ string length [ set ::${name}::server_data ] ] > [ set ::${name}::server_data_size ] } {
                	#puts "done reading, eval 1 server data\n'[ set ::${name}::server_data]'"
                	${name}::evalCmd [ string trim [ set ::${name}::server_data ] \n ]
                    set ::${name}::server_data ""
                    unset ::${name}::server_data_size
                }
                set buffer [ string range $buffer $size end ]
            }
            if	{ !$begin } {
            	append ::${name}::server_data $buffer
        		#puts "append server data [ string length [ set ::${name}::server_data ] ]"
                if	{ [ string length [ set ::${name}::server_data ] ] > [ set ::${name}::server_data_size ] } {
                	#puts "done reading, eval 2 server_data\n'[ set ::${name}::server_data]'"
                	${name}::evalCmd [ string trim [ set ::${name}::server_data ] \n ]
                    set ::${name}::server_data ""
                    unset ::${name}::server_data_size
                }
           } 
        } 
        ;## always register a read callback
        lassign [ globus_xio_register_read $handle $::GLOBUS_BUFFER_SIZE \
    	 		$::RawGlobus::waitforbytes NULL \
    			[ list ${name}::readCallback NULL ] ] result
    } err ] } {
    	addLogEntry $err 2
    }
}

## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::log
##
## Description:
## Make a log entry in the seperate log file for this
## assistant manager. The log file is named consistently
## with the key of the command from which the asst gets
## it's name.
##
## Usage:
##
## Comments:
## This should probably be considered a debugging aid.
## the LDASmgr.log should probably receive these entries.

proc ${name}::log { { msg "" } } {
     set name [ namespace tail [ namespace current ] ]
     if { [ catch {
     puts $msg 0 $name [ gpsTime ] $name
     } err ] } {
     catch { puts $err 2 }
     return -code error "${name}::log: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::cancel 
##
## Description:
## cancel continuous update
## 
##
## Usage:
##
## Comments:
## this cmd is called when client wants to stop the update

proc ${name}::cancel { cmdId } {

    if  { [ catch { 
        set name [ namespace tail [ namespace current ] ]
        set repeat [ set ::${name}::repeat($cmdId) ]
        set iterations [ set ::${name}::iterations($cmdId) ]
        set left [ expr $repeat - $iterations ]
        set ::${name}::repeat($cmdId) 0
        set ::${name}::iterations($cmdId) 0
        if  { [ info exist ::${name}::afterid($cmdId) ] } {
	        set afterid [ set ::${name}::afterid($cmdId) ]
            addLogEntry "cancelled after cmd $afterid for cmd $cmdId"
	        after cancel $afterid           
        }
        addLogEntry "cmd $cmdId repeats cancelled, \
        iterated $iterations times, $left remained." 
        set msg "0\nAuto update cancelled."
    } err ] } {
        set msg "Cancel auto update failed: $err"
        addLogEntry $msg 2 
        set msg "3\n$msg."
    } 
    return $msg  
}


## ******************************************************** 
##
## Name: ${name}::delete 
##
## Description:
## Self destruct assistant! Deletes the namespace, closes
## socket, removes after cmd 
##
## Usage:
##
## Comments:
## this cmd is called when client disconnects 


proc ${name}::delete { } {

     set name [ namespace tail [ namespace current ] ]
     if {  [ catch { 
        set sid [ set ${name}::sid ]
        catch { ::close $sid } err
        addLogEntry "closed socket $sid $err" purple
        
        ;## close globus sockets if present
        if	{ [ info exist ::${name}::handle ] } {
        	RawGlobus::close [ set ::${name}::handle ] $name
            unset ::${name}::handle
        }
        
        ;## maybe obsolete if ssh is used instead of xauth
        ;##catch { cntlmon::rmDisplayTimeout $name [ set ::${name}::display ] }
        ;## cancel any after cmds
        foreach cmdId [ array names ::${name}::afterid ] {
            after cancel [ set ::${name}::afterid($cmdId) ] 
        }
        ;## cancel any tail traces  
        foreach var [ set ::${name}::tracedvars ] {
            if  { [ info exist $var ] } {
                catch { cntlmon::cancelQuery $var }
            }
        }
        ;## cancel any pipe monitors
        foreach var [ set ::${name}::monitors ] {
            if  { [ array exists ::${name}::$var ] } {
                catch { cntlmon::cancelMonitorPipe $var }
            }
        }
		;## close off local channels and processes at the other end
		foreach fd [ array names ::${name}::pid ] {
			if	{ [ info exist ::${name}::pid($fd) ] } {
				catch { close $fd }
				catch { kill -9 [ set ::${name}::pid($fd) ] } err
                addLogEntry "kill -9  [ set ::${name}::pid($fd) ] $err" purple
			}
		}
        ;## remove log files
        catch { eval file delete -force [ glob $::CLIENTLOGDIR/${name}*.html ] }  
        ;## remove bgcntlmon log files
        catch { eval file delete -force [ glob $::TOPDIR/cntlmonAPI/${name}*.log ] } 
        
        namespace delete [ namespace current ]     
     } err ] } {
        addLogEntry "client handler \"$name\" delete error: $err" 2
        if { [ regexp $name [ namespace current ] ] } {
           namespace delete [ namespace current ]
        }   
     }     
     ;##puts "client handler \"$name\" destroyed."
     addLogEntry "client handler \"$name\" destroyed." 
     
     incr ::cntlmon::activejobs -1
     return {}
}
## ******************************************************** 

## ******************************************************** 
;#barecode
     ::cntlmon::throttle
     incr ::cntlmon::activejobs 1
     ;## log successful assistant creation.
     addLogEntry "Namespace \"$name\" created for user $login" 0
     ;##puts "[ Namespace_List $name ]"
     return {}
} ;## end of createClient


