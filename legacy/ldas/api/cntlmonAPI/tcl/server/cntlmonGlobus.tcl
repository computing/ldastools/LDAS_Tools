## ******************************************************** 
##
## Name: cntlmonGlobus 
##
## Description:
## handler for use of globus sockets 
## for network 
##
## Usage:
##
## Comments:
##

;#barecode

package provide cntlmonGlobus 1.0

;## 
;## globus libraries loaded in LDAScntlmon.ini
;## namespace eval cntlmonGlobus 

namespace eval cntlmonGlobus {} {
	set server_cs $::CNTLMON_API_HOST:$::TCLGLOBUS_PORT
    set register 1
    set manager_cs $::GLOBUS_MANAGER_API_HOST:$::TCLGLOBUS_HOST_PORT
}

;#end

## ********************************************************
## common procs 
## ******************************************************** 
##
## Name: bgerror 
##
## Description:
## Handles uncaught errors
##
## Usage:
##
## Comments:

proc bgerror { msg } {
     puts stderr "bgerror: $msg"
	 puts stderr $::errorInfo 
	 if	{ [ regexp {(cmon[^:]+)::\S+ (\S+)} $msg -> name page ] } {
	 	catch { $name::state0 $page } err
	 }
     set strlist [ split $msg ]
     set index [ lsearch $strlist "*sock*" ]
     if  { $index > -1 } {
         set sid [ lindex $strlist $index ]
         regsub -all  {[\"]} $sid {} sid 
         puts "bgerror sid = $sid."
    }
}

## ******************************************************** 
##
## Name: cntlmonGlobus::readCallback
##
## Description:
## handler for data read off globus socket from client
##
## Usage:
## 
##
## Comments:

proc cntlmonGlobus::readCallback { user_parms handle result buffer data_desc } {

debugPuts "read callback $user_parms $handle $result buffer $buffer data_desc $data_desc" 

	set seqpt ""
    if	{ [ regexp -nocase -- {end of file} $result ] } {
        addLogEntry "terminate read callback due to $result" blue
       	return
    }
	if	{ [ catch {
    	if	{ [ regexp {~~(\d+)([^~]+)} $buffer -> size bufdata ] } {
    		foreach { user version } $bufdata { break } 
        	set clientStatus [ cntlmon::validateControl $user 0 ]
        	set reply [  validateVersion $version ]
        	addLogEntry "user $user version $version status $clientStatus reply $reply" purple
     		set data [ cntlmon::updateClient ]
     		incr cntlmon::clientCount 1
     		set num $cntlmon::clientCount
        	;## temporary hardcoded myself
            
            lassign [ globus_xio_handle_cntl \
              $handle \
              $::RawGlobus::tcpdriver \
              $::GLOBUS_XIO_TCP_GET_REMOTE_CONTACT \
             ] result ipaddr
	 		createClient client$::cntlmon::clientCount $user
            
			set ::client${num}::serverKey x.509
			set ::client${num}::ipaddr $ipaddr
        	set ::client${num}::handle $handle
            set ::client${num}::status $clientStatus
            set ::client${num}::server_data ""
        	# acknowledge login ok 
        	set key x.509
        	set msg "nocallback\nclient${num}:$::LDAS_VERSION:$key:$clientStatus\n0\n$data\n$reply"

        	cntlmonGlobus::sendCmd $msg $handle    
            ;## register another callback only if client is still around
            if	{ [ string length [ info commands ::client${num}::readCallback ] ] } {
        		;## now handle over the readPoll to the client namespace
  				lassign [ globus_xio_register_read $handle $::GLOBUS_BUFFER_SIZE \
    	 			$::RawGlobus::waitforbytes NULL \
    				[ list client${num}::readCallback $user_parms ] ] result
                debugPuts "register read callback for client${num} result $result"
            }  
        } else {
        	error "invalid data $buffer"
        }

   } err ] } {
   		addLogEntry $err 2
        catch { cntlmonGlobus::sendCmd "nocallback\n0\n3\n$err\n" $handle }
   }
}

## ******************************************************** 
##
## Name: cntlmonGlobus::sendCmd
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
##
## Usage:
##
## Comments:
## later we can use remote file transfer for getting job
## files over cmonTree::getFileContents 

proc cntlmonGlobus::sendCmd { cmd handle } {

    ;## write cmd to client
    if  { [ catch {
    	if	{ [ string length $cmd ] } {
           	set cmd [ cmd::size $cmd ]
            RawGlobus::sendCmd $cmd $handle 
        } 
    } err ] } {
        addLogEntry $err 2
    }  
}


## ******************************************************** 
##
## Name: cntlmonGlobus::ldasJob
##
## Description:
## get mgr msg upon job submission e.g. job number
##
## Parameters:
##
## Usage:
##
## Comment:
proc cntlmonGlobus::ldasJob { ldasjob client cmdId cmdinfo } {

	if	{ [ catch {
    	set key [ key::time ]
		RawGlobusClient::init $key host
        set ::${key}(callback) [ list cntlmonGlobus::getJobStatus $client $cmdId $cmdinfo ]
        
    	RawGlobusClient::connect $::cntlmonGlobus::manager_cs $key   
        
        ;## sendCmd will do register read back from socket
    	RawGlobusClient::sendCmd $ldasjob $key
               
        #lassign [ globus_xio_register_read $::RawGlobusClient::networkhandle($key) $::GLOBUS_BUFFER_SIZE \
    	# $::RawGlobusClient::waitforbytes NULL \
    	#[ list cntlmonGlobus::ldasJobCallback $key ] ] result
        #debugPuts "register read callback result $result" 
        
    } err ] } {
        catch { unset ::${key} }
    	return -code error $err
    }
}

## ******************************************************** 
##
## Name: cntlmonGlobus::getJobStatus
##
## Description:
## get mgr msg upon job submission e.g. job number
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmonGlobus::getJobStatus { client cmdId cmdinfo args } {

    if  { [ catch {
 	    set reply [ lindex $args 0 ]
        if	{ [ regexp {Subject} $reply ] } {
        	set msg "0\n$reply"
            ::${client}::reply $cmdId $msg
        } elseif  { [regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ]} {
            set msg "2\n$client:$cmdId: submitted '$cmdinfo' request as job $jobid"
            ::${client}::reply $cmdId $msg
        } else {
            error "Error '$cmdinfo' for $client:$cmdId: '$reply'"
        }
    } err ] } {
        addLogEntry $err 2
        catch { ::${client}::reply $cmdId "3\n$err" }
    }
}

## ******************************************************** 
##
## Name: cntlmonGlobus::DBQueryJob
##
## Description:
## get mgr msg upon job submission e.g. job number
##
## Parameters:
##
## Usage:
##
## Comment:
proc cntlmonGlobus::DBQueryJob { ldasjob client cmdId cmdinfo } {

	if	{ [ catch {
    	set key [ key::time ]
		RawGlobusClient::init $key host
        
        set ::${key}(callback) [ list cntlmonGlobus::getDBQueryResult $client $cmdId $cmdinfo ]
    	RawGlobusClient::connect $::cntlmonGlobus::manager_cs $key   
    	RawGlobusClient::sendCmd $ldasjob $key
                    
        #lassign [ globus_xio_register_read $::RawGlobusClient::networkhandle($key) $::GLOBUS_BUFFER_SIZE \
    	# $::RawGlobusClient::waitforbytes NULL \
    	#[ list cntlmonGlobus::DBQueryCallback $key ] ] result
        #debugPuts "register read callback result $result" 
         
    } err ] } {
        catch { unset ::${key} }
    	return -code error $err
    }
}


## ******************************************************** 
##
## Name: cntlmon::getDBqueryResult
##
## Description:
## process results from getMetaData into data sets
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmonGlobus::getDBQueryResult { client cmdId cmdinfo args } {

    set seqpt ""
    if  { [ catch {
        set reply [ lindex $args 0 ]
        set jobid none        
        set pat "(${::RUNCODE}\\d+).+\nYour results:\n(\[^\\n\]+)\n.+($::PUBDIR/\[^\\n\]+)"        
        if  {[regexp -nocase -- $pat $reply -> jobid fname jobdir ]} {
        debugPuts "jobid $jobid fname $fname jobdir $jobdir"
		    set fname [ file join $jobdir $fname ]
                
                # regexp {(client\d+):(\d+)} $client -> client cmdId
                
                foreach { dbname dbtable starttime endtime timeopt histopt binsize } \
                    [ set ::${client}::${cmdId}(DBoption) ] { break }
                if  { [ info exist ::${client}::${cmdId}(cancel) ] } {   
                    regexp {(\S+)\s+is not null} [ set ::${client}::${cmdId}(cmdInfo) ] -> valuecol          
                    error "Request: 'db=$dbname, table=$dbtable, col=$valuecol from $starttime-$endtime' aborted by user."
                }
                ;## get data from fname
                set seqpt "cntlmon::getMetaDataFile $fname $jobid"
                cntlmon::getMetaDataFile $fname $jobid
                set seqpt "cntlmon::formDataset $jobid $client $cmdId"
                set rc  [ cntlmon::formDataset $jobid $client $cmdId $endtime ]
                if  { ! $rc } {
                ;## all done 
                    set seqpt done
                    set results "0\n"              
                    append results "$dbname $dbtable $starttime $endtime $timeopt $histopt $binsize \
                    [ set ::${client}::${cmdId}(limits) ]  \
                    [ list [ set ::${client}::${cmdId}(timedata) ] ]"
                    ::${client}::reply $cmdId $results    
                    unset ::${client}::${cmdId}
                } else { 
                    set cmdInfo [ set ::${client}::${cmdId}(cmdInfo) ]
                    set userinfo [ set ::${client}::${cmdId}(userInfo) ]
                    foreach { col lasttime } [ set ::${client}::${cmdId}(lastset) ] { break }
                    regsub {>= timestamp\('\d+'\)} $cmdInfo ">= timestamp\('$lasttime'\)" cmdInfo
                    addLogEntry "submitting again $cmdInfo"
                    cntlmon::submitDBqueryJob DBview $timeopt $histopt $binsize $userinfo $cmdInfo $client $cmdId
                }
	    } elseif  { [regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ]} {
        	set msg "2\n$client:$cmdId: submitted '$cmdinfo' request as job $jobid"
            ::${client}::reply $cmdId $msg
        } else {
        	error "Error '$cmdinfo' for $client:$cmdId: '$reply'"
        }
    } err ] } {
        addLogEntry  "$seqpt $err" orange
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${client}::${cmdId} }
    }
    
}
