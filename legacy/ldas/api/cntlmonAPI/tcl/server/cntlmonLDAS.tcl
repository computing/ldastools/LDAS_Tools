## -*- indent-tabs-mode: nil -*-
## ex: set tabstop=8 expandtab:
## ********************************************************
##
## This is the Laser Interferometer Gravitational
## Observatory (LIGO) controlmon server Tcl Script.
##
## This script handles commands made to ldas manager
##
## ********************************************************

;## does ps on machine and formats the return for gui
;## will have to ssh for remote machine

package provide cntlmonLDAS 1.0

## ********************************************************
##
## Name: cntlmon::cmd2mgr
##
## Description:
## retrieve file contents
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::cmd2mgr { comment mgrcmd api args } {

    if { [ regexp {(bootstrap|remove|shutdown)} $mgrcmd ] } {
        set ldasapis [ join $::API_LIST ,]
        if { ! [ regexp $api $ldasapis ] } {
            set result "3\n$api is not on LDAS API list ( $ldasapis )"
            return $result
        }
    }

    set commentstr "\"$comment\""
    switch $mgrcmd {
        bootstrapAPI { set cmd "mgr::$mgrcmd $api $commentstr" }
        removeAPI    { set cmd "mgr::$mgrcmd $api $args" }
        shutdownAPI  { set cmd "mgr::$mgrcmd $api $commentstr" }
        addAPI       { set cmd "mgr::$mgrcmd $api $args" }
        sHuTdOwN     { set cmd "mgr::$mgrcmd" }
        makeFtpDirectory { set cmd "mgr::$mgrcmd $api $args" }
        chopQueueTo  { set cmd "mgr::chopQueueTo $args" }
        default   { addLogEntry "invalid cmd $mgrcmd $api $args" 2;
            return "3\ninvalid cmd $mgrcmd" }
    }

    set result ""
    set client [ uplevel { namespace tail [ namespace current ] } ]
    set ipaddr [ set ::${client}::ipaddr ]

    ;## API reports shutdown but LDAS shutdown needs an email gif
    if { [ catch {
        if { [ string match sHuTdOwN $mgrcmd ] } {
            set subject "LDAS shutting down NOW on $::LDAS_SYSTEM !"
            set body $comment
            addLogEntry "Subject: $subject; Body: $body" email
            set sid [ sock::open manager emergency ]
            puts $sid "$::MGRKEY NULL NULL eval \{$cmd\}"
            close $sid
            set result "'$cmd' issued. Check status page after 2 minutes."
        } else {
            if { [ regexp -nocase {addAPI|removeAPI} $mgrcmd ] } {
        	;## put a trace to update rsc file when API_LIST is updated from mgr
        	trace variable ::API_LIST w "cntlmon::updateRscFileAPI $mgrcmd $api $args"
            }
            set cmd "\{$cmd\}"
            set result [ getEmergData manager $cmd ]
        }
        set result [ split $result \n ]
        set result [ join [ lrange $result 0 end-1 ] ]
        addLogEntry "$mgrcmd $api $args executed @$ipaddr $comment. $result" blue
        set result "0\n$mgrcmd $api $args executed @$ipaddr $comment. $result"
    } err ] } {
        set result "3\n$mgrcmd $api ($comment) error: $err"
        addLogEntry $result email
    }
    return $result
}

## ********************************************************
##
## Name: cntlmon::updateRscFileAPI
##
## Description:
## update resource file API_LIST and API host if any
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::updateRscFileAPI { mgrcmd api args } {

    if { [ catch {
        set rscfile [ file join $::TOPDIR LDASapi.rsc ]
        if { [ string match removeAPI $mgrcmd ] } {
            set cmd1 "set ::API_LIST [ list $::API_LIST ]"
            cntlmon::saveResource $api $rscfile {{set ::API_LIST}} [ list $cmd1 ]
            trace vdelete ::API_LIST w "cntlmon::updateRscFileAPI $mgrcmd $api "
        } else {
            set cmd1 "set ::API_LIST [ list $::API_LIST ]"
            set pat1 "set ::API_LIST"
            set pat2 "set ::[ string toupper $api ]_API_HOST"
            set host [ lindex $args 0 ]
            set cmd2 "set ::[ string toupper $api ]_API_HOST $host"
            cntlmon::saveResource $api $rscfile [list $pat1 $pat2 ] [ list $cmd1 $cmd2 ]
            trace vdelete ::API_LIST w "cntlmon::updateRscFileAPI $mgrcmd $api $host"
        }
    } err ] } {
        addLogEntry $err red

    }
}

## ********************************************************
##
## Name: cntlmon::useradmin
##
## Description:
## handle add/update/delete users through client
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::useradmin { auth_user admincmd args } {

    if { ! [ regexp {(addUser|updateUser|deleteUser|blockUser|blockDso)} $admincmd ] } {
        set result "3\n$invalid command $admincmd"     
        return $result
    } 
    if { ! $::userAdminOK && ! [ regexp block $admincmd ] } {
        return "3\nIt is not possible to add/update/delete a user at this time."
    } 
    
    if { [ llength $args ] == 1 } {
        set args [ lindex $args 0 ]
    }
    if { [ regexp {(addUser|updateUser|deleteUser|blockUser)} $admincmd ] } {
        set data [ binaryDecrypt $::CHALLENGE [ decode64 $args ] ]
    } else {
        set data $args
    }
    foreach { name password email fullname phone expires } $data { break }
    
    set expires [ clock format [ clock scan $expires -base [ clock seconds ] ] -format "%Y-%m-%d" ]
    if { [ string length $password ] == 32 && [ regexp -nocase {^[\dabcdef]+} $password ] } {
        ;## md5 encrypted already 
    } else {
        set password [ key::md5 $password ]
    }
    switch -regexp -- $admincmd {
        updateUser  { set cmd "\{ puts \$cid \[ mgr::updateUserInfo $name $password $email [ list $fullname ] [ list $phone ] [ list $expires ] \]\}" }
        addUser     { set cmd "\{ puts \$cid \[ mgr::addUser $name $password $email [ list $fullname ] [ list $phone ] [ list $expires ] \] \}" }
        blockUser   { set option ""
            if { [ regexp -nocase {unblock} $admincmd ] } { 
        	set option unblock
            } 
            set cmd "\{ puts \$cid \[ mgr::blockUser $name $option \] \}" }
        blockDso   {  set option ""
            if { [ regexp -nocase {unblock} $admincmd ] } { 
        	set option unblock
            } 
            set name [ lindex $args 0 ]
            set cmd "\{ puts \$cid \[ mgr::blockDso $name $option \] \}" }            
        default     { set cmd "\{ puts \$cid \[ mgr::updateUserInfo $name $password $email [ list $fullname ] [ list $phone ] [ list $expires ] 1 \] \}" }
    } 	                                  
    set result ""
    set client [ uplevel { namespace tail [ namespace current ] } ]
    set ipaddr [ set ::${client}::ipaddr ]

    if { [ catch {
       	set mgrmsg [ getEmergData manager $cmd ]        
        # set result [ split $result \n ]
        # set result [ join [ lrange $result 0 end-1 ] ]
        addLogEntry "$admincmd $name executed by $auth_user @$ipaddr. '$mgrmsg'"
        set myresult {}
        if { ! [ string length $mgrmsg ] } {
            set mgrmsg "Updated."
        } 
        if { [ regexp -nocase blockDso $admincmd ] } {
            regexp {Updated\.\n(.+)} [ getBlockedDSOs ] -> myresult
        } else {
            regexp {Updated\.\n(.+)} [ getUsersQueue ] -> myresult
        }
        set result "0\n$mgrmsg\n$myresult"
    } err ] } {
        ;## if error is for user admin, extract the text msg
        if { ! [ regexp {^3\n(.+)} $err -> error ] } {
            set error $err
        } 
        set result "3\n$admincmd for user $name error: $error"
        addLogEntry $result red
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::getRscFname
##
## Description:
## determine the filename of rsc file, global or API 
##
## Parameters:
## type - [ global | <api> ]
## 
## Usage:
##
## Comment:  

proc cntlmon::getRscFname { type rscapi } {

    if { [ string equal global $type ] } {
        set rscfile [ file join $::TOPDIR LDASapi.rsc ]
        if { ! [ file exists $rscfile ] } {
            set rscfile [ file join $::LDAS bin LDASapi.rsc ]
        } 
    } else {
        set rscfile [ file join $::TOPDIR ${rscapi}API LDAS${rscapi}.rsc ]
        if { ! [ file exists $rscfile ] } {
            set rscfile [ file join $::LDAS lib ${rscapi}API LDAS${rscapi}.rsc ]
        } 
    }
    return $rscfile   
}

## ******************************************************** 
##
## Name: cntlmon::evalArrayValues
##
## Description:
## parse resource file and return a list of variables
##
## Parameters:
## client 
## Usage:
##
## Comment:  
;## get value of array indices

proc cntlmon::evalArrayValues {} {

    uplevel {   
        catch { $interp eval unset $name }
        if { [ catch {
            set value "[ $interp eval $line ]"
        } err ] } {
            addLogEntry "Unable to get values for resource $name: $err" red
            set value unknown
        }
        set indices [ $interp eval array names $name ]
    }
} 

## ******************************************************** 
##
## Name: cntlmon::getResourceVars
##
## Description:
## parse resource file and return a list of variables
##
## Parameters:
## client 
## Usage:
##
## Comment:  
## returns list of vars that need real time values and the
## lines in rsc file.

proc cntlmon::getResourceVars { fname rscapi } {

    if { ! [ regexp $rscapi $::API_LIST ] } {
        return -code error "$rscapi is not on LDAS API list ( $::API_LIST )"
    }
    
    if { [ catch {
        set state 0
        set varlist [ list ]
        set interp [ interp create ]
        set fd [ open $fname r ]
        set lines [ read $fd  ]
        close $fd
        $interp eval source $::TOPDIR/cntlmonAPI/cntlmon.state
        $interp eval source $::TOPDIR/LDASapi.rsc 
        $interp eval source $fname
        
        ;## some screening to get the modifable real time values
        set lines [ split $lines \n ]
        set rsclines [ list ]
        set longlines [ list ]
        set desctext [ list ]
        
        foreach line $lines {
            
            if { [ regexp {\#\#\s+Name:} $line ] } {
                lappend rsclines $line
                continue
            }
            ;## found a desc line, implies var follows
            ;## pick up multiline desc 
            if { [ regexp {^[;\#\#]+[\s]+desc=([^!]+)} $line -> desc ] } {
        	set state 1
        	set desctext $desc
        	#lappend rsclines $line
        	if { [ regexp {mod=([^!]+)} $line -> mod ] } {
        	    set mod no
        	} else {
        	    set mod yes
        	}
        	continue
            }
            if { $state == 1 } {
        	if { [ regexp {^[;\#]+\s*(.+)$} $line -> desc ] } {
        	    set desctext $desc
        	    continue
        	}
            }

            if { [ regexp {^;\#\# real time values} $line ] } {
        	set state 2
        	set longlines [ list ]
        	continue
            }
            
            if { [ regexp {^[\s\t\n]*$} $line ] } {
        	continue
            }
            set namelist [ list ]
            if { $state == 3  } {
        	lappend longlines $line
        	continue
            } 
            if { $state == 1 || $state == 2 } {    
        	if { [ regexp {\\[\t\s\n]*$} $line ] } {
        	    lappend longlines $line
        	    continue
        	} else {
        	    ;## process the multiple line 
        	    if { [ llength $longlines ] } {
        		lappend longlines $line 
        		regsub -all {\\} [ join $longlines \ ] {} line
        		set longlines [ list ]                    
        	    }
        	}
        	if { [ regexp {(^set (\S+))\s+(.+)$} $line -> pattern name value ] } {
        	    if { [ string equal cntlmon $rscapi ] } {                    
        		set value [ set $name ]  
        		if { [ regexp {([^\(]+)\(([^\)]+)\)} $name -> arrname index ] } {
        		    set name $arrname
        		    set indices $index                            
        		} else {
        		    set indices [ list ]
        		}                 
        	    } else {
        		if { [ regexp {([^\(]+)\(([^\)]+)\)} $name -> arrname index ] } {                         	
        		    cntlmon::evalArrayValues                            
        		} else {
        		    set indices [ list ]
        		} 
        		
        	    }
        	    set namelist [ list $name  $value ]   
        	} elseif { [ regexp {(array set\s+(\S+))\s+} $line -> pattern name ] } {
        	    lappend namelist $name [ list [ $interp eval array get $name ] ]
        	} 
        	foreach { name value } $namelist {
        	    lappend varlist $name
        	    set finaldesc [ join $desctext ] 
        	    if { [ string length $finaldesc ] } {
        		lappend rsclines ";## desc=[ join $desctext ] ! mod=$mod\n$line" 
        	    }                   
        	}
        	set longlines [ list ] 
        	;## similar resource use the same desc
        	# set desctext [ list ]           
        	continue													
            }
        }
    } err ] } {
        addLogEntry $err red
    }
    catch { interp delete $interp }
    return [ list $varlist $rsclines ]

}

## ******************************************************** 
##
## Name: cntlmon::getAPIresource
##
## Description:
## return rsc data to client
##
## Parameters:
## client 
## Usage:
##
## Comment: 
## retrieves global data and API specific resource  

proc cntlmon::getAPIresource { rscapi types } {

    set seqpt ""
    set result ""
    if { [ catch {
        foreach type $types {
            set varlist [ list ]
            set rscfile [ getRscFname $type $rscapi ]
            foreach { vars lines } [ getResourceVars $rscfile $rscapi ] { break }
            append result "[ join $lines \n ]\n"        
            set varlist [ concat $varlist $vars ]        
            set mark ";## real time values"
            if { [ string equal cntlmon $rscapi ] } {
        	append result "$mark\n[ realTimeRscValues $varlist ]\n"
            } else {
        	set seqpt "realTimeRscValues for $rscapi rsc"
        	set msg "\{puts \$cid \[ realTimeRscValues \{$varlist\} ]\}"
        	append result "$mark\n[ getEmergData $rscapi $msg ]\n"
            }
        }
        ;## escape the $vars e.g. ::EXTRA_ENV
        regsub -all {\$} $result "\\$" result 
        set result "0\n[ string trim $result \n ]"               
    } err ] } {
        addLogEntry $err 2
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::saveResource
##
## Description:
## update resource file with user modifications
##
## Parameters:
## client 
## Usage:
##
## Comment: 
## fname can be api specific or global  
## only update rsc if there are cmds
## only 1 pattern per line

proc cntlmon::saveResource { rscapi fname patterns cmdlist } {

    if { [ catch {
        
        if { [ llength $cmdlist ] } {
            set localname [ file tail $fname ]
            if { [ string match LDASapi.rsc $localname ] } {
        	set localrsc [ file join $::TOPDIR $localname ]
            } else {
        	set localrsc [ file join $::TOPDIR ${rscapi}API $localname ]
            }
            if { [ regexp "$::LDAS/(lib|bin)" $fname ] } {
        	file copy $fname $localrsc 
            } 
            set fd [ open $localrsc r ]
            set data [ read -nonewline $fd ]
            set data [ split $data \n ]
            close $fd 
            set i 0
            set rsctext ""
            set maxlen [ llength $patterns ]
            ;## ensure uniqueness of each pattern
            foreach line $data {
        	set idx 0
        	foreach pat $patterns {	
        	    if { [ string first "$pat " $line ] > -1 } {
        		if { [ regexp {array set (\S+)} $line -> arrname ] } {
        		    set line "$pat\t\[ list [ eval "array get $arrname" ] \]"
        		} else {
        		    set value [ eval $pat ]
        		    set len [ llength $value ]
        		    if { $len > 1 } {
        			set line "$pat \[ list $value \]"
        		    } elseif { $len == 1 } {
        			set line "$pat $value"
        		    } else {
        			set line "$pat {}"
        		    }
        		}
        		break
        	    } 
        	}
        	append rsctext $line\n	
            }       
            bak $localrsc 2 
            set fd [ open $localrsc w ]
            set rsctext [ string trim $rsctext \n ]
            puts $fd $rsctext 
            close $fd   
        }
    } err ] } {
        catch { close $fd }
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: cntlmon::setAPIresource
##
## Description:
## update rsc vars with data from client
## if data is to be saved, request the API to do it.
##
## Parameters:
## client 
## Usage:
##
## Comment:   

proc cntlmon::setAPIresource { rscapi patterns cmds { save "apply" } { type global } { get 1 } } {
    
    if { [ catch {
        
        ;## update in memory
        set result ""
        if { [ string equal cntlmon $rscapi ] } {
            cmdSet $cmds
        } else {
            set socketcmd "\{ puts \$cid \[ cmdSet [ list $cmds ] \] \}" 
            set sock_result [ getEmergData $rscapi $socketcmd ]    
            append result "$rscapi resources updated"
        }       
        if { [ string match save $save ] } {        
            set rscfile [ getRscFname $type $rscapi ]     
            if { [ string equal cntlmon $rscapi ] } {
        	saveResourceToFile cntlmon $rscfile $patterns
            } else {
        	set socketcmd "\{ puts \$cid \[ saveResourceToFile $rscapi $rscfile [ list $patterns ] \] \}" 
        	set sock_result [ getEmergData $rscapi $socketcmd ]    
        	append result " and saved in $sock_result"
            }                    
        }
        addLogEntry $result
        if { $get } {
            regexp {0\n(.+)} [ getAPIresource $rscapi [ list specific global ] ] -> newdata
            append result "|$newdata"
        }
        set result "0\n$result "
    } err ] } {
        addLogEntry $err 2
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::addAPIResources
##
## Description:
## add new resource vars to API
## with optionally updates to resource file
##
## Parameters:
##
## Comment:   

proc cntlmon::addAPIResources { rscapi descs cmds { save "apply" } { type global } { get 1 } } {
    
    if { [ catch {
        
        ;## update in memory
        set result ""
        if { [ string equal cntlmon $rscapi ] } {
            cmdSet $cmds
        } else {
            set socketcmd "\{ puts \$cid \[ cmdSet [ list $cmds ] \] \}" 
            set sock_result [ getEmergData $rscapi $socketcmd ]    
            append result "New resources added to $rscapi memory"
        }       
        if { [ string match save $save ] } {        
            set rscfile [ getRscFname $type $rscapi ]     
            if { [ string equal cntlmon $rscapi ] } {
        	addResourcesToFile $rscfile $descs $cmds
            } else {
        	set socketcmd "\{ puts \$cid \[ addResourcesToFile $rscfile [ list $descs ] [ list $cmds ] \] \}" 
        	set sock_result [ getEmergData $rscapi $socketcmd ]    
        	append result " and saved in $sock_result"
            }                    
        }
        addLogEntry $result
        if { $get } {
            regexp {0\n(.+)} [ getAPIresource $rscapi [ list specific global ] ] -> newdata
            append result "|$newdata"
        }
        set result "0\n$result "
    } err ] } {
        addLogEntry $err 2
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::deleteAPIResources
##
## Description:
## add new resource vars to API
## with optionally updates to resource file
##
## Parameters:
##
## Comment:   

proc cntlmon::deleteAPIResources { rscapi descs patterns cmds { save "apply" } { type global } { get 1 } } {
    
    if { [ catch {
        
        ;## update in memory
        set result ""
        if { [ string equal cntlmon $rscapi ] } {
            cmdSet $cmds
        } else {
            set socketcmd "\{ puts \$cid \[ cmdSet [ list $cmds ] \] \}" 
            set sock_result [ getEmergData $rscapi $socketcmd ]    
            append result "Resources deleted from $rscapi memory"
        }       
        if { [ string match save $save ] } {        
            set rscfile [ getRscFname $type $rscapi ]     
            if { [ string equal cntlmon $rscapi ] } {
        	deleteResourcesFromFile $rscfile $descs $patterns
            } else {
        	set socketcmd "\{ puts \$cid \[ deleteResourcesFromFile $rscfile [ list $descs ] [ list $patterns ] \] \}" 
        	set sock_result [ getEmergData $rscapi $socketcmd ]    
        	append result " and saved in $sock_result"
            }                    
        }
        addLogEntry $result
        if { $get } {
            regexp {0\n(.+)} [ getAPIresource $rscapi [ list specific global ] ] -> newdata
            append result "|$newdata"
        }
        set result "0\n$result "
    } err ] } {
        addLogEntry $err 2
        set result "3\n$err"
    }
    return $result
}
## ******************************************************** 
##
## Name: cntlmon::cmd2api
##
## Description:
## direct an cmd to api via emergency socket 
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::cmd2api { api args } {

    set result ""
    set client [ uplevel { namespace tail [ namespace current ] } ]
    set ipaddr [ set ::${client}::ipaddr ]

    if { [ catch {
        set cmd "\"$args\""
        set result [ getEmergData $api $cmd ]            
        ;## dont log clear text user info 
        if { [ regexp -nocase {error} $result ] } {
            error $result
        }
        set result [ split $result \n ]
        set result [ join [ lrange $result 0 end-1 ] ]
        addLogEntry "$cmd executed at $api; $result"    
        set result "0\n$api $args; $result"
    } err ] } {
        ;## if error is for user admin, extract the text msg
        if { ! [ regexp {^3\n(.+)} $err -> error ] } {
            set error $err
        }
        set result "3\n$cmd $api error: $err"
        addLogEntry $result
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::manageUsers
##
## Description:
## add a list of users to ldas users queue via manager
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::manageUsers { mgrcmd args } {

    if { [ catch {	
        set namelist [ list ]
        addLogEntry "received cgi cmd $mgrcmd"	
        if { ! [ regexp -nocase {(addUser|updateUserInfo|updateOrAddUser)} $mgrcmd ] } {
            error "invalid command $mgrcmd"
        }
        if { [ llength $args ] == 1 } {
            set args [ lindex $args 0 ]
        }

        ;## validate user and password
        if { [ regexp -- {-username\s+(\S+)[\s\n\t]*-password\s+(\S+)[\s\n\t]*-data\s+([\}\{\w\+\/=\n]*)} $args -> username password data ] } {
            ;## this extra puts seem to enable the cgi user to be decoded properly
            set fnull [ open /dev/null w ]
            puts $fnull "username $username '[ string trim [ decrypt $username $::CLIENTKEY ] ]', password $password '[ string trim [ decrypt $password $::CLIENTKEY ] ]'"
            close $fnull
            
            addLogEntry "puts to dev null" purple
            ;## assign a new variable as using the old one results in trailing chars causing invalid password
            set username1 [ string trim [ decrypt $username $::CLIENTKEY ] ]
            set password1 [ string trim [ decrypt $password $::CLIENTKEY ] ]	
            if { [ string first " " $username1 ] != -1 } {
        	set username1 [ string range $username1 0 \
        			    [ expr [ string first " " $username1 ] - 1 ] ]
            }
            if { [ string first " " $password1 ] != -1 } {
        	set password1 [ string range $password1 0 \
        			    [ expr [ string first " " $password1 ] - 1 ] ]
            }
            if { $::DEBUG == 0xce } {
        	puts "username $username '$username1', password $password '$pass/uysqword1'"
            }
            ;## cgi user or control user
            if { [ string match cgiuser $username1 ] } {
        	set password2 [ key::md5 $password1 ]	
        	set key [ expr abs([ clock clicks ]) ]
        	set password3 [ key::md5 $password2$key ]
            } else {
        	set password3 [ base64::encode64 [ binaryEncrypt $::CHALLENGE $password1 ] ]
        	set key ""
            }
            validateLogin $username1 $password3 $key
        } else {
            error "request format must be -username <username> -password <password> -data <data>"
        }

        ;## remove the {} or it wont decrypt

        regsub {\{} $data {} data
        regsub {\}} $data {} data
        set data [ 	decrypt $data $::CLIENTKEY ]

        set len [ llength $data ]
        if { $len < 6 } {
            error "Invalid user info."
        }

        set cmd "\{ puts \$cid \[ cmdSet \[ list "
        set text ""
        foreach var { name password email fullname phone expires flag } {
            set $var ""
        }

        if { [ string match updateUserInfo $mgrcmd ] } {
            foreach { name password email fullname phone expires flag } $data { 
        	lappend namelist $name
        	if { [ regexp {\S+} $flag ] } {
        	    set password [ key::md5 $password ]
        	    append text "\[ list mgr::$mgrcmd $name $password $email [ list $fullname ] $phone $expires $flag \] "
        	} else {
        	    addLogEntry "invalid data for user $name, rest of the data skipped" orange
        	    break
        	}
        	;## there are garbage in the data so need to initialize and skip
        	foreach item [ list name password email fullname phone expires flag ] {
        	    set $item ""
        	}
            }
        } else {
            foreach { name password email fullname phone expires } $data {
        	lappend namelist $name
        	if { [ regexp {\S+} $expires ] } {
        	    set password [ key::md5 $password ]
        	    append text "\[ list mgr::$mgrcmd $name $password $email [ list $fullname ] $phone $expires \] "
        	} else {
        	    addLogEntry "invalid data for user $name, rest of the data skipped" orange
        	    break
        	}
        	;## there are garbage in the data so need to initialize and skip
        	foreach item [ list name password email fullname phone expires ] {
        	    set $item ""
        	}
            }
        }
        if { ! [ string length $text ] } {
            error "Invalid user data"
        }
        append cmd "$text \] \] \}"
        if { $::DEBUG == 0xce } {
            puts "cmd to manager\n'$cmd'"
        }
        set reply [ getEmergData manager $cmd ]
        set result ""
        if { $::DEBUG == 0xce } {
            puts "manager reply '$reply'"
        }
        foreach line [ split $reply \n ] {
            if { [ regexp "puts \$cid [ myName ]" $line ] } {
        	break
            } 
            set line [ string trim $line ; ]
            append result "\{$line\} "
        }
        if { [ string length $result ] > 5 } {
            error $result
        }
    } err ] } {
        if { $::DEBUG } { leakLogger }
        addLogEntry "cgi request $mgrcmd for users [ join $namelist , ] failed: $err" red
        return -code error $err
    }
    if { $::DEBUG } { leakLogger }
    addLogEntry "cgi request $mgrcmd for users [ join $namelist , ] OK"	 green
    return "0"
}
