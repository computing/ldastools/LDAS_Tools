## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon server Tcl Script.

##
## This script returns html file data  for server.
## related to mpiAPI information.
## 
## ******************************************************** 

;## does ps on machine and formats the return for gui
;## will have to ssh for remote machine
package provide cntlmonJobs 1.0

set ::JOB_STATS_COLS 18
if	{ [ info exist ::JOB_STATS_COLNAMES ] } {
	set ::JOB_STATS_COLS_NEW [ llength $::JOB_STATS_COLNAMES ]
}
set ::REJECTED_JOB_COLS 4


## ******************************************************** 
##
## Name: cntlmon::getCmds 
##
## Description:
## get user cmds submitted for the jobs from the LDAS logs.
##
## Parameters:
##
## Usage:
##
## Comment:
## runcode needs to be append back to jobid for manager's array

proc cntlmon::getCmds { jobs } {
	if	{ [ catch {
		set cmd "\{ puts \$cid \[ cmdGet \[ list "
		set text ""
		foreach job $jobs {
			append text "\{ set ::${::RUNCODE}${job}::usrcommand \} "
		}
		append cmd "$text \] \] \}"
		set reply [ getEmergData manager $cmd ]
		set result ""
		set i 0
		set lines [ split $reply \n ] 
		foreach line $lines {
			if	{ ! [ string length $line ] } {
				continue
			}
			if	{ [ regexp "puts \$cid [ myName ]" $line ] } {
				break
			} 
			if	{ [ regexp {can't read} $line ] } {
				set line "no user command found"
			}
			array set newq [ list [ lindex $jobs $i ],cmd $line ]
			incr i 1
		}
	} err ] } {
		return -code error "[ myName ]: $err"
	}
    # debugPuts "found [ array size newq ] jobs from [ llength $lines ] lines"
	return [ array get newq ]
}

## ******************************************************** 
##
## Name: cntlmon::getLDASJobInfo
##
## Description:
## get all active jobs from the manager's QUEUE(COMMAND) 
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getLDASJobInfo {} {
    if  { [ catch { 
		set msg "\{puts \$cid \[ mgr::activeJobStatusSummary list \]\}"
        set result [ getEmergData manager $msg ]
		set joblist [ list ]
		set result [ split $result \n ]
		set result [ lindex $result 0 ]
        foreach [ list running queued ] $result { break }
		foreach job $running {
			foreach [ list jobid cmd api user time ] $job { break }
			regexp {\d+} $jobid jobid
			foreach { total_time api_time } [ split $time : ] { break }
			# puts "jobid=$jobid cmd=$cmd api=$api user=$user addr=$addr"
			set jobdata($jobid) "$cmd $api $user $total_time $api_time"
		}	
		set result ""	
		if	{ [ array size jobdata ] } { 
			set joblist [ lsort -integer [ array names jobdata ] ]
			array set newq [ getCmds $joblist ]
			foreach jobid $joblist {
				append result "$jobid $jobdata($jobid) [ list $newq($jobid,cmd) ]\n"
			}
		}
        set result "0\n$result"
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::setLDASJobChgs
##
## Description:
## send kill job and adjust priority requests to mpi 
##
## Parameters:
##
## Usage:
##
## Comment:
## returns the current job queue after modifications

proc cntlmon::setLDASJobChgs { killlist } {
  
    if  { [ catch { 
        set result ""
        foreach job $killlist {
            set msg "\{puts \$cid \[ mgr::abortJob $job $job ]\}"
            set result [ getEmergData manager $msg ]
            addLogEntry "aborted job $job $result"      
        }
        set result [ getLDASJobInfo ]   
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::validateUser
##
## Description:
## validate user in the users.queue
##
## Parameters:
##
## Usage:
##
## Comment:

;## valid as an AND situation, error if there is one invalid user 
proc cntlmon::validateUser { userid } {

    if	{ [ regexp {^\.\+$} $userid ] } {
		return
	}
    if  { [ catch {
	    set queuefile [ usersQueueFile ]
	    if	{ [ regexp crypt $queuefile ] } {
			set queues [ decryptFile $queuefile $::MGRKEY ]
	    } else {
		    set fd [ open $queuefile r ]
	        set queues [ read $fd ]
		    close $fd
	    }
        set userlist [ split $userid \| ]
        set exactusers [ list ]
        foreach user $userlist {
    	    if	{ [ regexp {[\.\+\*]+} $user ] } {
		        if	{ ![ regexp -- "-name $user" $queues ] } {
			        error "Invalid user(s) $user"
		        }
	        } else {
                lappend exactusers $user
            }
        }
        foreach line [ split $queues \n ] {
            if	{ [ regexp -- {-name (\S+)} $line -> queueUser ] } {
        	    set index [ lsearch -exact $exactusers $queueUser ] 
        	    if  { $index > -1 } {
            	    set exactusers [ lreplace $exactusers $index $index ]
				    if	{ ! [ llength $exactusers ] } {
					    break 
				    }
        	    }
		    }
        }
        if  { [ llength $exactusers ] } {
            error "Invalid user(s) $exactusers"
        }
    } err ] } {
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: cntlmon::job_statsColumns
##
## Description:
## set the cmd that list the job columns from job_stats.log
##
## Parameters:
##
## Usage:
##
## Comment:
## done via uplevel

proc cntlmon::job_statsColumns {} {

	uplevel {
		if	{ ! [ info exist ::JOB_STATS_COLNAMES ] } {
			set cmds "list jobid job_start_time user cmd status queue_t manager_t diskcache_t \
			frame_t metadata_t datacond_t mpi_t wrapper_t eventmon_t ligolw_t asstmgr_t total_t queue_size"
		} else {
			set cmds "list $::JOB_STATS_COLNAMES"
		}
		set numcats [ expr [ llength $cmds ] - 1 ]
	}
}

## ******************************************************** 
##
## Name: cntlmon::job_statsColsCheck
##
## Description:
## check number of columns for this line in job_stats.log
##
## Parameters:
##
## Usage:
##
## Comment:
## done via uplevel

proc cntlmon::job_statsColsCheck {} {

	uplevel {
		if	{ $len != $::JOB_STATS_COLS } {
			if	{ [ info exist ::JOB_STATS_COLS_NEW ] } {
                if  { $len != $::JOB_STATS_COLS_NEW } {
				    error "expected $::JOB_STATS_COLS_NEW columns, got $len"
                }
			} else {
                error "expected $::JOB_STATS_COLS columns, got $len"
            }
		}
	}
}

## ******************************************************** 
##
## Name: cntlmon::jobCompletionTime
##
## Description:
## job completion time
##
## Parameters:
##
## Usage:
##
## Comment:
## done via uplevel
## this time conversion slows down cntlmon
## ok to treat use gpstime to add to delta secs

proc cntlmon::jobCompletionTimeX {} {

	uplevel {
        set startutc [ utcTime $job_start_time ]
        set endutc [ expr round($startutc + $total_t - $queue_t) ]
        set completed_time [ gpsTime $endutc ]
	}
}

proc cntlmon::jobCompletionTime {} {

	uplevel {
        set completed_time  [ expr round($job_start_time + $total_t - $queue_t) ]
	}
}

## ******************************************************** 
##
## Name: cntlmon::jobStatsGraph
##
## Description:
## gathers data for graph of job statistics with optional filter
##
## Parameters:
##
## Usage:
##
## Comment:
## dont put in artificial endpoints
## job completion time = gpsTime (( gps2utc of job start time ) + total time - queue time )

proc cntlmon::jobStatsGraph { userid range { jobtypes { pass fail rejected } } { cmdtypes .+} { rejectReason .+ } } {

	if	{ [ catch {
		;## validate user from users.queue updated by manager
		cntlmon::validateUser $userid
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/job_stats.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			lappend filelist [ file join $::LDASLOG job_stats.log ]
		}
		# debugPuts "files $filelist"
		
		set jobstatus(pass) 0
		set jobstatus(fail) 0
		set jobtotal 0
		set jobdata(fail) [ list ]
		set jobdata(pass) [ list ]
		set failjobs [ list ]
		set passjobs [ list ]
		set pfailed 0.0
		set rejected 0
		set rejectlist [ list ]
		set reasons [ list ]
		
		cntlmon::job_statsColumns
			
		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if	{ [ catch { 
                    set len [ llength $line ]
					cntlmon::job_statsColsCheck
					foreach [ eval $cmds ] $line { break }
				} err ] } {
					addLogEntry "$file error at '$line': $err" red
					continue
				}

                cntlmon::jobCompletionTime
                
				if	{ $completed_time < $start_time || $completed_time > $end_time } {
					continue
				}
    		    
                if { ! [ regexp "$userid" $user ] } {
					continue
				}
				if	{ ! [ regexp -nocase $cmdtypes $cmd ] } {
					continue
				}		
				incr jobtotal 1
				incr jobstatus($status) 1
				lappend jobdata($status) [ list $completed_time $jobstatus($status) ]				
				regexp {(\d+)} $jobid -> jobid 
                lappend ${status}jobs [ list $completed_time $jobid ]
			}
		}
		foreach status { pass fail } {

			if	{ [ llength $jobdata($status) ] } {
				# set jobdata($status) [ lsort -index 0 -real $jobdata($status) ]
				set jobdata($status) [ join $jobdata($status) ]
			} 
			if	{ [ llength ${status}jobs ] } {
				set ${status}jobs [ lsort -index 0 -real [ set ${status}jobs ] ]
				set ${status}jobs [ join [ set ${status}jobs ] ]
			}		
		}
		set numFail $jobstatus(fail) 
		if	{ $jobtotal } {
			set pfailed [ format "%.5f" [ expr ( $numFail *1.0 / $jobtotal ) * 100.0 ] ]
		}
		if	{ [ regexp {reject} $jobtypes ] } { 
			foreach { rejected reasons rejectlist } [ cntlmon::rejectedJobsGraph $userid $range $cmdtypes $rejectReason ] { break }
		}
		set result "0\n$jobtotal $numFail $pfailed $rejected [ list $jobdata(pass) $jobdata(fail) $passjobs $failjobs $rejectlist $reasons ]"
	} err ] } {
		set result "3\n$err"
	}
	return $result	
}

## ******************************************************** 
##
## Name: cntlmon::metadataGraph
##
## Description:
## gathers data for insertions or queries
##
## Parameters:
##
## Usage:
##
## Comment:
## 

proc cntlmon::metadataGraph { userid range { filter ".+" } { dbcmds {putMetaData getMetaData} } \
{ database  .+ } { cmdtypes .+ } } {

	if	{ [ catch {
		cntlmon::validateUser $userid
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		foreach cmd $dbcmds {
			set done 0
			set jobtotal($cmd) 0
			set jobdata($cmd) [ list ]
			foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/$cmd.log.* ] ] {
				regexp {(\d+)$} [ file tail $file ] -> jobendtime 
				if	{ $jobendtime < $start_time } {
					continue
		 		}
				if	{ $jobendtime > $end_time } {
					lappend filelist $file 
					set done 1
					break 
				} else {
					lappend filelist $file
				}
			}
			if	{ ! $done } {
				lappend filelist [ file join $::LDASLOG $cmd.log ]
			}
		}
		#debugPuts "files $filelist"
				
		set result "0\n"
		foreach file $filelist {
			regexp {(getMetaData|putMetaData)} $file -> cmd 
            set data [ dumpFile $file ]
            set data [ split $data \n ]
            foreach line $data {
                catch { unset completed_time user ldascmd dbname rows }
                if	{ [ catch {
				    set len [ llength $line ]
                    if  { [ string equal putMetaData $cmd ] } {
                        switch $len {
                            6  { foreach { completed_time user ldascmd dbname rows table } $line { break } }
                            4  { foreach { completed_time user rows table } $line { break } }
                            default { error "expected 4 or 6 columns, got $len columns" }
                        }
                    } else {
                        switch $len {
                            5  { foreach { completed_time user ldascmd dbname rows } $line { break } }
                            3  { foreach { completed_time user rows } $line { break } }
                            default { error "expected 3 or 5 columns, got $len" }
                        }
                    }
			    } err ] } {
				    addLogEntry "$file error at '$line': $err" red
			    }
				
				if	{ $completed_time < $start_time || $completed_time > $end_time } {
					continue
				} 
				if { ! [ regexp "$userid" $user ] } {
					continue
				}
                if  { [ info exist ldascmd ] } {
                    if  { ! [ regexp -nocase $cmdtypes $ldascmd ] } {
					    continue
                    }
                }
                if  { [ info exist dbname ] && ! [ regexp $dbname $database ] } {
                    continue
                }
				if	{ [ string match putMetaData $cmd ] } {
					if { ! [ regexp "^$filter$" $table ] } {
						continue
					}
				}
				incr jobtotal($cmd) $rows
				;## always accumulate as pass
				lappend jobdata($cmd) [ list $completed_time $jobtotal($cmd) ]
            }
		}
		foreach cmd $dbcmds {
			if	{ [ llength $jobdata($cmd) ] } {
			;## already in time order
				set jobdata($cmd) [ join $jobdata($cmd) ]
			} 
			append result "$cmd $jobtotal($cmd) [ list $jobdata($cmd) ] "
		}			
	} err ] } {
        addLogEntry $err 2
		set result "3\n$err"
	}
	return $result	
}

## ******************************************************** 
##
## Name: cntlmon::memoryUsageGraph
##
## Description:
## gathers data for API memory allocations
##
## Parameters:
##
## Usage:
##
## Comment:
## 

proc cntlmon::memoryUsageGraph { api range } {

	if	{ [ catch {
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 	
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/memusage/$api.mem.* ] ] {
				regexp {(\d+)$} [ file tail $file ] -> apiendtime 
				if	{ $apiendtime < $start_time } {
					continue
		 		}
				if	{ $apiendtime > $end_time } {
					lappend filelist $file 
					set done 1
					break 
				} else {
					lappend filelist $file
				}
		}
		if	{ ! $done } {
				lappend filelist [ file join $::LDASLOG $api.mem ]
		}
		# debugPuts "files $filelist"
				
		set result "0\n$range "
		set jobtotal 0
		set t0 $start_time 
		set stats [ list ]
		set membase 0
		set restarts [ list ]
        set coretimes [ list ]
		set set 0
        set cpudata [ list ]
		set threaddata [ list ]
        
		foreach file $filelist {
			regexp {([.]+).mem} [ file tail $file ] api
            set data [ dumpFile $file ]
            set data [ split $data \n ]
            foreach line $data {
            	set initflag ""
				set init 0
                set cpu ""
                set thread ""
               if	{ [ catch {
					set len [ llength $line ]
					if	{ $len < 2 || $len > 5 } {
						error "expected 2-5 columns, got $len" red
					}
			        foreach { gpstime memory cpu thread initflag } $line { break }
                } err ] } {
					addLogEntry "$file error at '$line': $err" red
					continue
				}
				if	{ $gpstime < $start_time || $gpstime > $end_time } {
					continue
				} 
				if	{ [ string match init $initflag ] || [ string match init $cpu ] ||
                      [ string match init $thread ]  } {
					incr set 1
					lappend restarts $gpstime
				} else {
					if	{ [ info exist memmin ] } {
						if	{ $memory < $memmin } {
							set memmin $memory
						} 
					} else {
						set memmin $memory
					}
					if	{ [ info exist memmax ] } {
						if	{ $memory > $memmax } {
							set memmax $memory
						} 
					} else {
						set memmax $memory
					}
					incr jobtotal 1
				} 
				lappend apidata($set) [ list $gpstime $memory ]
                if  { [ regexp {^[\.\d]+$} $cpu ] } {
                    lappend cpudata $gpstime $cpu
                }
                
                if  { [ regexp {^(\d+)$} $thread ] } {
                    lappend threaddata $gpstime $thread
                }
			}
		}
        set coretimes [ cntlmon::coreTimes $api $range ]
		if	{ $jobtotal } {
			append result "$jobtotal $memmin $memmax [ list $restarts ] "
			if	{ [ array exist apidata ] } {
				append result "\{"
				set sets [ lsort -integer [ array names apidata ] ]
				;## already in time order
				foreach set $sets {
					set apidata($set) [ join $apidata($set) ]
					append result "[ list $apidata($set) ] "
				}
                append result "\}"
			}
            append result " [ list $cpudata ]"
            append result " [ list $coretimes ]"
            append result " [ list $threaddata ]"
		} else {
			append result "0"
		}
	} err ] } {
		set result "3\n$err"
	}
	return $result			

}

## ******************************************************** 
##
## Name: cntlmon::queueStatsGraph
##
## Description:
## gather data for queue stats graph
##
## assume statistics file is of the following format:
##
##
## Usage:
##
## Comment:
## 
proc cntlmon::queueStatsGraph { range } {

	if	{ [ catch {

		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/job_stats.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			set currfile [ file join $::LDASLOG job_stats.log ]
			if	{ [ file exist $currfile ] } {
				lappend filelist $currfile
			}
		}
		# debugPuts "files to search: $filelist"
		
		set jobtotal 0
		set jobdata(fail) [ list ]
		set jobdata(pass) [ list ]
	    set queuesize [ list ]
		cntlmon::job_statsColumns

		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if	{ [ catch { 
					set len [ llength $line ]
					cntlmon::job_statsColsCheck
					foreach [ eval $cmds ] $line { break }
				} err ] } {
					addLogEntry "$file error at '$line': $err" red
					continue
				}
                cntlmon::jobCompletionTime

				if	{ $completed_time < $start_time || $completed_time > $end_time } {
					continue
				}
				regexp {(\d+)} $jobid -> jobid
				incr jobtotal 1
				lappend jobdata($status) [ list $completed_time $jobid ]
				
				if	{ [ info exist ymin ] } {
					if	{ $queue_size < $ymin } {
						set ymin $queue_size
					} 
				} else {
					set ymin $queue_size
				}
				if	{ [ info exist ymax ] } {
					if	{ $queue_size > $ymax } {
						set ymax $queue_size
					} 
				} else {
					set ymax $queue_size
				}
				lappend queuesize [ list $completed_time $queue_size ]
			}
		}

		foreach status { pass fail } {
			if	{ [ llength $jobdata($status) ] } {
				# set jobdata($status) [ lsort -index 0 -real $jobdata($status) ]
				set jobdata($status) [ join $jobdata($status) ]
			} 
		}
		if	{ ! $jobtotal } {
			set pfailed 0.0
			set numFail 0
			set result "0\n0 0 0"
		} else {
			set numFail [ expr [ llength $jobdata(fail) ] / 2 ]
			scan "$numFail $jobtotal" "%f %f" fnumFail fjobtotal
			set pfailed [ format "%.2f" [ expr ( $fnumFail / $fjobtotal ) * 100.0 ] ]
			# set queuesize [ lsort -index 0 -real $queuesize ]
			set queuesize [ join $queuesize ]	
			if	{ $ymin == $ymax } {
				set ymax [ expr $ymin + 5 ]
			}
			set result "0\n$jobtotal $numFail $pfailed $ymin $ymax [ list $jobdata(pass) $jobdata(fail) $queuesize ]"
		}
	} err ] } {
		set result "3\n$err"
	}
	return $result	
}

## ******************************************************** 
##
## Name: cntlmon::APITimeGraph
##
## Description:
## gathers data for plotting times jobs spent in each API and queue.
##
## assume statistics file is of the following format:
##
## Column01: GPS start time of the job
## Column02: username
## Column03: LDASjob Command name; e.g. getMetaData, putMetaData,
##          dataPipeline.inspiral, dataPipeline.stochastic, ...
## Column04: Pass or Fail
## Column05: seconds in Queue
## Column06: seconds in managerAPI
## Column07: seconds in diskCacheAPI
## Column08: seconds in frameAPI
## Column09: seconds in metaDataAPI
## Column10: seconds in dataConditionAPI
## Column11: seconds in mpiAPI
## Column12: seconds in wrapperAPI
## Column13: seconds in eventMonitorAPI
## Column14: seconds in lightWeightAPI
## Column15: total seconds in all APIs	
## Parameters:
## userid 	- ldas user id
## range  	- gpstime range
## cmdtypes - regexp of the usercmds e.g. inspiral|stochastic, getMetaData
## apilist 	- list of apis to select 
##
## Usage:
##
## Comment:
## 
proc cntlmon::APITimeGraph { userid range cmdtypes jobtypes categories args } {

	if	{ [ catch {
		;## validate user from users.queue updated by manager
		cntlmon::validateUser $userid
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/job_stats.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			set currfile [ file join $::LDASLOG job_stats.log ]
			if	{ [ file exist $currfile ] } {
				lappend filelist $currfile
			}
		}
		# debugPuts "files to search: $filelist"
		
		set jobtotal 0
		set jobdata(fail) [ list ]
		set jobdata(pass) [ list ]
	
		set datalist [ list ]
        if	{ [ llength $args ] } {
			set hist [ lindex $args 0 ]
		}
		foreach category $categories {
			set ${category}times [ list ]
			lappend datalist ${category}times
		}
		cntlmon::job_statsColumns

		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if	{ [ catch { 
					set len [ llength $line ]
					cntlmon::job_statsColsCheck
					foreach [ eval $cmds ] $line { break }
				} err ] } {
					addLogEntry "$file error at '$line': $err" red
					continue
				}
                cntlmon::jobCompletionTime
				if	{ $completed_time < $start_time || $completed_time > $end_time } {
					continue
				}	
				if { ! [ regexp "$userid" $user ] } {
					continue
				}
				if	{ ! [ regexp -nocase $cmdtypes $cmd ] } {
					continue
				}							
				if	{ ! [ regexp -nocase $jobtypes $status ] } {
					continue
				}
				regexp {(\d+)} $jobid -> jobid
				incr jobtotal 1	
				lappend jobdata($status) [ list $completed_time $jobid ]
				;## only count if this is not zero
				foreach category $categories {
					set ytime [ set ${category}_t ] 
					if	{ $ytime } {	
						lappend ${category}times [ list $completed_time $ytime ]                        								
						if	{ [ info exist ymin ] } {
							if	{ $ytime < $ymin } {
								set ymin $ytime
							} 
						} else {
							set ymin $ytime
						}
						if	{ [ info exist ymax ] } {
							if	{ $ytime > $ymax } {
								set ymax $ytime
							} 
						} else {
							set ymax $ytime
						}		
					}
				}
			}
		}

		if	{ ! $jobtotal } {
			set result "0\n[ list $categories ] $range 0 0 0"
		} else {
			set numfailed [ llength $jobdata(fail) ]
			set numpassed [ llength $jobdata(pass) ]
			set pfailed [ format "%.2f" [ expr double ( $numfailed )/$jobtotal * 100.0 ] ]
			foreach status { pass fail } {
				if	{ [ llength $jobdata($status) ] } {
					#set jobdata($status) [ lsort -index 0 -real $jobdata($status) ]
					set jobdata($status) [ join $jobdata($status) ]
				} 
			}
            ;## if no job in the categories, set arbitrary ymin and ymax
            if  { ! [ info exist ymin ] } {
                set ymin 0
                set ymax 5
            }

			set result "0\n[ list $categories ] $range $jobtotal $ymin $ymax $numpassed $numfailed $pfailed "
            if  { ! [ info exist hist ] } {
			    append result "[ list $jobdata(pass) $jobdata(fail) ] "
            }
			foreach entry $datalist {
				if	{ [ llength [ set $entry ]]  } {
					set $entry [ join [ set $entry ] ]
                    append result " [ list $entry [ set $entry ] ] "
				} else {
					append result " [ list $entry [ list ] ]"
				}
			}
		}
	} err ] } {
		set result "3\n$err"
	}
	return $result	
}

## ******************************************************** 
##
## Name: cntlmon::rejectedJobsGraph
##
## Description:
## gather data for rejected jobs graph
##
##
## Usage:
##
## Comment:
## called from jobs stats graph

proc cntlmon::rejectedJobsGraph { userid range cmdtypes rejectReason } {

	if	{ [ catch {

		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/rejected_jobs.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			set currfile [ file join $::LDASLOG rejected_jobs.log ]
			if	{ [ file exist $currfile ] } {
				lappend filelist $currfile
			}
		}
		# debugPuts "files to search: $filelist"
		
	    set rejected 0
		set cmds "list gpstime ldascmd user reason"
		set numcats [ expr [ llength $cmds ] - 1 ]
		set ymin 0
		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if  { [ catch {
				    set len [ llength $line ]
				    if	{ $len != $::REJECTED_JOB_COLS } {
						error "expected $::REJECTED_JOB_COLS columns, got $len"
					}
					foreach [ eval $cmds ] $line { break }
				} err ] } {
					addLogEntry "$file error at '$line', $err" red
					continue
				}
				;## remove when all is in gpstime
				if	{ $gpstime < $start_time || $gpstime > $end_time } {
					continue
				}
				if { ! [ regexp "$userid" $user ] } {
					continue
				}
				if	{ ! [ regexp -nocase $cmdtypes $ldascmd ] } {
					continue
				}
				if	{ ! [ string length $reason ] } {
					set reason $ldascmd
				}
				if 	{ ! [ regexp $rejectReason $reason ] } {
					continue
				}
				if	{ ! [ info exist reasonCnt($reason) ] } {
					set reasonCnt($reason) 0
				}
				incr reasonCnt($reason) 
				incr rejected 1
				lappend rejected_data [ list $gpstime $rejected ]
			}
		}

		if	{ ! $rejected } {
			set result "0 {} {}"
		} else {
			# set rejected_data [ lsort -index 0 -real $rejected_data ]
			set rejected_data [ join $rejected_data ]
			if	{ ! [ array exist reasonCnt ] } {
				array set reasonCnt [ list $rejectReason 0 ]
			}
			set result "$rejected [ list [ array get reasonCnt ] ] [ list $rejected_data ]"
		}
	} err ] } {
		return -code error $err
	}
	return $result	
}

## ******************************************************** 
##
## Name: cntlmon::killSock
##
## Description:
## submit an LDAS job to manager
##
##
## Usage:
##
## Comment:
## only returns error 

proc cntlmon::killSock {sid email page} {
    if  { [ catch {
        catch {close $sid}
        set subject "$page: error sending LDAS job"
        set body "Timeout on connection to LDAS manager."
        mailTo $email $subject $body
    } err ] } {
        addLogEntry "$page sock timed out: $err" red
    }
}

## ******************************************************** 
##
## Name: cntlmon::getClearDBresults
##
## Description:
## reads data from ssh/rsh pipe when there is data to read
##
## Parameters:
##
## Usage:
##
## Comment:
## closes socket for error or when script on the end of
## pipe is finished. Works with Bwatch.

proc cntlmon::getClearDBresults { fid client cmdId page userinfo cmdinfo } {
    if  { [ catch {
		set lines [ read $fid ]
        if  { [ regexp -nocase {<pre>(error!.+)</pre>} $lines -> msg ] } {
            error $msg
        }
        ::${client}::reply $cmdId "2\n$lines"
        cntlmon::submitLDASjob $page $userinfo $cmdinfo $client $cmdId
    } err ] } {
        ::${client}::reply $cmdId "3\n$err"     
    } 
    catch { close $fid }
}

## ******************************************************** 
##
## Name: ${API}::startClearDBPipe
##
## Description:
## retrieve usage of nodes
## via a pipe to ssh 
##
## Usage:
## var identifies a monitor e.g. Bwatch, Nsearch      
## on beowulf nodes
##
## Comments:
## This is done with repeat in the beowulf script
## so it keeps the pipe open until it is canceled
## or if script exits
## pipe fid is keep in client

proc cntlmon::startClearDBPipe { client cmdId page userinfo cmdinfo } {
    if  { [ catch {
        if	{ ! [ regexp -- {-database\s+[\{]*([^\s\}]+)[\}]*} $cmdinfo -> dbname ] } {
			set dbname $::DATABASE_NAME
		}
        set fid [ securePipe ldas $::METADATA_API_HOST ]
        puts $fid "cd $::TOPDIR/cntlmonAPI && /usr/bin/env HOST=$::METADATA_API_HOST PATH=$::LDAS/bin:$::env(PATH) \
		        LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		        DB2INSTANCE=ldasdb LD_PRELOAD= $::LDAS/bin/del_db_process.tcl $dbname version=LDASTest"  
        flush $fid
        set times 0
        ;## wait for pipe to be set up
        after 100
        addLogEntry "clearing database $dbname for $page for $client" blue
        ;## must use :: for client namespace 
        fileevent $fid readable [ list cntlmon::getClearDBresults $fid $client $cmdId \
            $page $userinfo $cmdinfo ]
        set result "2\nClearing database $dbname test data ... DO NOT RUN putMetaData or putStandAlone Test yet."  
    } err ] } {
        set result "3\n$err"
        return -code error $err
    }    
    ::${client}::reply $cmdId $result
}


## ******************************************************** 
##
## Name: cntlmon::submitLDASjob
##
## Description:
## reads data from ssh/rsh pipe when there is data to read
##
## Parameters:
##
## Usage:
##
## Comment:
## closes socket for error or when script on the end of

proc cntlmon::submitLDASjob { page userinfo cmdinfo { client "" } { cmdId -1 } } {

    if 	{[catch {
        set first 0
        if  { $cmdId < 0 } {
            set client [ uplevel { namespace tail [ namespace current ] } ]
            set cmdId  [ uplevel { set cmdId } ]
            set first 1
        }
        if  { [ regexp -nocase {putStandAlone|putMetaData} $page ] && $first} {   
            cntlmon::startClearDBPipe $client $cmdId $page $userinfo $cmdinfo
       	} else { 
        	if { [ regexp -nocase {createRDS} $page ] } { 
            	if	{ ! [ regexp -nocase -- {-usertype [\{\s]*([^\{\}\s\t]*)[\s\}]*} $cmdinfo -> userType ] } {
                	set userType ""
                } 
				addLogEntry "userType $userType" purple
            	if	{ ! [ regexp -nocase -- {-outputdir (\S+)} $cmdinfo -> rdsOutput ] } {
                	set rdsOutput ""
                } else {
                	set rdsOutput "[ join $rdsOutput ]/*[ join ${userType} ]*"
                }
        		catch { exec $::TEST_CLEANUP RDS $rdsOutput } err 
                addLogEntry "exec $::TEST_CLEANUP RDS $rdsOutput error: $err" purple
            }
            regexp -- {email (\S+)} $userinfo -> email 
            regexp -- {-password\s+(\S+)} $userinfo -> passwd
            if	{ [ string match x.509 [ set ::${client}::serverKey ] ] } {
            	regsub -all -- {-password (\S+)} $userinfo "-password x.509" userinfo
                regsub -all -- {-email (\S+)} $userinfo "-email persistent_socket" userinfo
                
                if	{ [ info exist ::USE_GLOBUS_CHANNEL ] && $::USE_GLOBUS_CHANNEL } {
                	set sid [ cntlmon::openGSIClientChannel $::GLOBUS_MANAGER_API_HOST $::TCLGLOBUS_HOST_PORT \
                    	cntlmon::getGSIChannelJobStatus $client $cmdId $cmdinfo]  
                    puts $sid "ldasJob \{$userinfo\} \{$cmdinfo\}" 
            		flush $sid                  
                } 
            } else {
            	set passwd [ binaryDecrypt $::CHALLENGE [ base64::decode64 $passwd ] ]
            	regsub -all -- {-password (\S+)} $userinfo "-password $passwd" userinfo
            	regsub -all -- {-email (\S+)} $userinfo "-email $::LTESTHOST:$::LTESTPORT" userinfo
       	    	set sid [ sock::open manager operator ]  
            	fconfigure $sid -blocking off
       	    	set aid [ after 60000 [list cntlmon::killSock $sid $email $page ]]
            	puts $sid "ldasJob \{$userinfo\} \{$cmdinfo\}" 
            	flush $sid 
		    	catch { unset ::${page} }   
            	addLogEntry "submitted '$cmdinfo'"
            	# trace variable ::${page} w [ list cntlmon::getJobStatus $sid $aid $client $cmdId $cmdinfo ]
            	fileevent $sid readable [ list cmd::receive $sid $page [ list cntlmon::getJobStatus $sid $aid $client $cmdId $cmdinfo ] ]
           	}
        }
    } err]} {
        catch {close $sid}
        addLogEntry $err 2
        return "3\n$err"
    } 
}

## ******************************************************** 
##
## Name: cntlmon::coreTimes
##
## Description:
## find list of gpstimes that api has dumped core
##
##
## Usage:
##
## Comment:

proc cntlmon::coreTimes { api range } {

	if	{ [ catch {

		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
        set coretimes [ list ]
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/coretimes.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			set currfile [ file join $::LDASLOG coretimes.log ]
			if	{ [ file exist $currfile ] } {
				lappend filelist $currfile
			}
		}
		# debugPuts "files to search: $filelist"
		
		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
            foreach line $data {
                if  { [ catch {
                    set len [ llength $line ]
                    if  { $len != 2 } {
                        error "expected 2 columns, got $len"
                    }
                    foreach { gpstime thisapi } $line { break }
                } err ] } {
                    addLogEntry "$file error at '$line': $err" red
                }
                if  { [ string match $api $thisapi ] } {
				    if	{ $gpstime < $start_time || $gpstime > $end_time } {
					    continue
				    }
                    lappend coretimes $gpstime
                }
            }
        }
    } err ] } {
        return -code error $err
    }
    return $coretimes
}

## ******************************************************** 
##
## Name: cntlmon::getJobStatus
##
## Description:
## get mgr msg upon job submission e.g. job number
##
## Parameters:
##
## Usage:
##
## Comment:
## for non-persistent sockets only, close socket once jobid has
## been assigned.

proc cntlmon::getJobStatus { sid aid client cmdId cmdinfo var } {

    if  { [ catch {
	    set reply [ set $var ]
        if  { $::DEBUG >= 2 } {
            addLogEntry "$client:$cmdId '$cmdinfo', mgr reply '$reply'" purple
        }
        if  { [regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ]} {
		    set ::${jobid}(client) $client:$cmdId
            set msg "2\n$client:$cmdId: submitted '$cmdinfo' request as job $jobid"
            ::${client}::reply $cmdId $msg
        } else {
            error "No job Id for '$cmdinfo' for $client:$cmdId: '$reply'"
        }
    } err ] } {
        addLogEntry $err 2
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${client}::${cmdId} }
    }
    catch { close $sid }
    after cancel $aid
   	catch { unset $var } 
}

## ******************************************************** 
##
## Name: cntlmon::submitDBqueryJob
##
## Description:
## submit getMetaData and get notified of its completion
##
## Parameters:
##
## Usage:
##
## Comment:
## closes socket for error or when script on the end of
## keep the unused parms timeopt, histopt and binsize 
## until next release to obsolete out

proc cntlmon::submitDBqueryJob { page timeopt histopt binsize userinfo cmdinfo { client "" } { cmdId 0 } } {

    if 	{ [catch {
        regexp -- {email (\S+)} $userinfo -> email 
        regexp -- {-password\s+(\S+)} $userinfo -> passwd
        if  { ! [ string length $client ] } {
            set client [ uplevel { namespace tail [ namespace current ] } ]
            set cmdId  [ uplevel { set cmdId } ]
            if	{ [ string match x.509 [ set ::${client}::serverKey ] ] } {
            	regsub -all -- {-password (\S+)} $userinfo "-password x.509" userinfo
                regsub -all -- {-email (\S+)} $userinfo "-email persistent_socket" userinfo
                set x509 1
            } else {
            	set passwd [ binaryDecrypt $::CHALLENGE [ base64::decode64 $passwd ] ]
            	regsub -all -- {-password (\S+)} $userinfo "-password $passwd" userinfo
            	regsub -all -- {-email (\S+)} $userinfo "-email $::LHOST:$::LPORT" userinfo
            }
        }
        set init 0
        if	{ [ info exist x509 ] } {            
            if	{ [ info exist ::USE_GLOBUS_CHANNEL ] && $::USE_GLOBUS_CHANNEL } {
                set sid [ cntlmon::openGSIClientChannel $::MANAGER_API_HOST $::TCLGLOBUS_HOST_PORT \
                cntlmon::getGSIDBQueryResult $client $cmdId $cmdinfo]  
                puts $sid "ldasJob \{$userinfo\} \{$cmdinfo\}" 
            	flush $sid 
             } else {
             	error "Resource ::USE_GLOBUS_CHANNEL is undefined"
             }
        } else { 
       			set sid [ sock::open manager operator ]  
        		fconfigure $sid -blocking off
       			set aid [ after 100000 [list cntlmon::killSock $sid $email $page ]]
        		set var DBquery${client}${cmdId}
        		catch { unset ::$var }
        		# trace variable ::$var w [ list cntlmon::getJobStatus $sid $aid $client $cmdId $cmdinfo ]
        		fileevent $sid readable [ list cmd::receive $sid $var [ list cntlmon::getJobStatus $sid $aid $client $cmdId $cmdinfo ] ]
        		puts $sid "ldasJob \{$userinfo\} \{$cmdinfo\}" 
        		flush $sid 
        } 
        if  { ! [ info exist ::${client}::${cmdId}(cmdInfo) ] } {
            set ::${client}::${cmdId}(userInfo) $userinfo
            set ::${client}::${cmdId}(cmdInfo) $cmdinfo 
            regexp -- {-database \{([^\}]+)\}.+from\s+(\S+)[^']+'(\d+)'\)\s+[^']+'(\d+)} $cmdinfo -> \
                    dbname dbtable starttime endtime
            set ::${client}::${cmdId}(DBoption) [ list $dbname $dbtable $starttime $endtime $timeopt $histopt $binsize ]
            set msg "2\n$client:$cmdId submitting $cmdinfo ..."   
            set init 1        
        } 
    } err]} {
        catch {close $sid}
        addLogEntry $err 2
        return "3\n$err"
    } 
    if  { $init } {
        return $msg
    }
}

## ******************************************************** 
##
## Name: cntlmon::serverCfg
##
## Description:
## configure db query socket to get email results from mgr
## for getMetaData job.
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::serverCfg  { sid addr port  } {

    fconfigure $sid -buffering line -blocking off
    catch { unset ::DBview }
    # trace variable ::DBview w [ list cntlmon::getDBqueryResult $sid ]
    fileevent $sid readable [ list cmd::receive $sid DBview [list cntlmon::getDBqueryResult $sid ] ]
    return
}

## ******************************************************** 
##
## Name: cntlmon::serverCfgTest
##
## Description:
## configure ldas test socket to get email results from mgr
## for LDAS test job.
##
## Parameters:
#
## Usage:
##
## Comment:

proc cntlmon::serverCfgTest  { sid addr port  } {

    fconfigure $sid -buffering line -blocking off
    catch { unset ::LDASTest }
    # trace variable ::LDASTest w [ list cntlmon::getLDASTestResult $sid ]
    fileevent $sid readable [ list cmd::receive $sid LDASTest [ list cntlmon::getLDASTestResult $sid ] ]
    return
}

## ******************************************************** 
##
## Name: cntlmon::openJobPort
##
## Description:
## open a port on server to receive job email from mgr
## for getMetaData job.
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::openJobPort {} {
    if  { [ catch {
        set localport 0
        set lsid [socket -server cntlmon::serverCfg -myaddr [info hostname] $localport ]
        foreach {::LADDR ::LHOST ::LPORT} [ fconfigure $lsid -sockname] {break}
        
        set lsid [socket -server cntlmon::serverCfgTest -myaddr [info hostname] $localport ]
        foreach {::LTESTADDR ::LTESTHOST ::LTESTPORT} [ fconfigure $lsid -sockname] {break}
    } err ] } {    
        addLogEntry $err 2
    } 
    puts "open port $::LADDR $::LHOST $::LPORT to receive getMetaData results\n\
open port $::LTESTADDR $::LTESTHOST $::LTESTPORT to receive LDAS Test results"

}

## ******************************************************** 
##
## Name: cntlmon::getDBqueryResult
##
## Description:
## process results from getMetaData into data sets
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getDBqueryResult { sid args } {

    set seqpt ""
    if  { [ catch {
    	if	{ [ info exist ::USE_GLOBUS_CHANNEL ] && $::USE_GLOBUS_CHANNEL } {
        	set reply [ lindex $args end ]
        } else {
        	set var [ lindex $args 0 ]
        	set reply [ set $var ]
        }
        if  { $::DEBUG >= 2 } {
            addLogEntry "$args, reply='$reply'" purple
        }
		unset $var 
        set jobid none
        regexp -nocase "(${::RUNCODE}\\d+)" $reply -> jobid
        if  { [ regexp -nocase {error!} $reply ] } {
            if  { [ array exist ::${jobid} ] } {
                set client [ set ::${jobid}(client) ]
                regexp {(client\d+):(\d+)} $client -> client cmdId
                error $reply
            } else {
                error "No client registered for job $jobid"
            }
        }
        set pat "(${::RUNCODE}\\d+).+\nYour results:\n(\[^\\n\]+)\n.+($::PUBDIR/\[^\\n\]+)"        
        if  {[regexp -nocase -- $pat $reply -> jobid fname jobdir ]} {
		    set fname [ file join $jobdir $fname ]
            if  { [ array exist ::${jobid} ] } {
                set client [ set ::${jobid}(client) ]
                regexp {(client\d+):(\d+)} $client -> client cmdId
                foreach { dbname dbtable starttime endtime timeopt histopt binsize } \
                    [ set ::${client}::${cmdId}(DBoption) ] { break }
                if  { [ info exist ::${client}::${cmdId}(cancel) ] } {   
                    regexp {(\S+)\s+is not null} [ set ::${client}::${cmdId}(cmdInfo) ] -> valuecol          
                    error "Request: 'db=$dbname, table=$dbtable, col=$valuecol from $starttime-$endtime' aborted by user."
                }
                ;## get data from fname
                set seqpt "cntlmon::getMetaDataFile $fname $jobid"
                cntlmon::getMetaDataFile $fname $jobid
                set seqpt "cntlmon::formDataset $jobid $client $cmdId"
                set rc  [ cntlmon::formDataset $jobid $client $cmdId $endtime ]
                if  { ! $rc } {
                ;## all done 
                    set seqpt done
                    set results "0\n"              
                    append results "$dbname $dbtable $starttime $endtime $timeopt $histopt $binsize \
                    [ set ::${client}::${cmdId}(limits) ]  \
                    [ list [ set ::${client}::${cmdId}(timedata) ] ]"
                    ::${client}::reply $cmdId $results    
                    unset ::${client}::${cmdId}
                } else { 
                    set cmdInfo [ set ::${client}::${cmdId}(cmdInfo) ]
                    set userinfo [ set ::${client}::${cmdId}(userInfo) ]
                    foreach { col lasttime } [ set ::${client}::${cmdId}(lastset) ] { break }
                    regsub {>= timestamp\('\d+'\)} $cmdInfo ">= timestamp\('$lasttime'\)" cmdInfo
                    addLogEntry "submitting again $cmdInfo"
                    cntlmon::submitDBqueryJob DBview $timeopt $histopt $binsize $userinfo $cmdInfo $client $cmdId
                }
            } else {
                error "No job record for client"
            }
	    } else {
            error "Unable to locate job products from reply" 
        }
    } err ] } {
        addLogEntry  "$seqpt $err" orange
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${jobid} }
        catch { unset ::${client}::${cmdId} }
    }
    
    catch { close $sid }

}

## ******************************************************** 
##
## Name: cntlmon::getGSIDBqueryResult
##
## Description:
## process results from getMetaData into data sets
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getGSIDBQueryResult { client cmdId cmdinfo sid reply args } {

    set seqpt ""
    if  { [ catch {
        set jobid none        
        set pat "(${::RUNCODE}\\d+).+\nYour results:\n(\[^\\n\]+)\n.+($::PUBDIR/\[^\\n\]+)"        
        if  {[regexp -nocase -- $pat $reply -> jobid fname jobdir ]} {
		    set fname [ file join $jobdir $fname ]
                               
                foreach { dbname dbtable starttime endtime timeopt histopt binsize } \
                    [ set ::${client}::${cmdId}(DBoption) ] { break }
                if  { [ info exist ::${client}::${cmdId}(cancel) ] } {   
                    regexp {(\S+)\s+is not null} [ set ::${client}::${cmdId}(cmdInfo) ] -> valuecol          
                    error "Request: 'db=$dbname, table=$dbtable, col=$valuecol from $starttime-$endtime' aborted by user."
                }
                ;## get data from fname
                set seqpt "cntlmon::getMetaDataFile $fname $jobid"
                cntlmon::getMetaDataFile $fname $jobid
                set seqpt "cntlmon::formDataset $jobid $client $cmdId"
                set rc  [ cntlmon::formDataset $jobid $client $cmdId $endtime ]
                if  { ! $rc } {
                ;## all done 
                    set seqpt done
                    set results "0\n"              
                    append results "$dbname $dbtable $starttime $endtime $timeopt $histopt $binsize \
                    [ set ::${client}::${cmdId}(limits) ]  \
                    [ list [ set ::${client}::${cmdId}(timedata) ] ]"
                    ::${client}::reply $cmdId $results    
                    unset ::${client}::${cmdId}
                } else { 
                    set cmdInfo [ set ::${client}::${cmdId}(cmdInfo) ]
                    set userinfo [ set ::${client}::${cmdId}(userInfo) ]
                    foreach { col lasttime } [ set ::${client}::${cmdId}(lastset) ] { break }
                    regsub {>= timestamp\('\d+'\)} $cmdInfo ">= timestamp\('$lasttime'\)" cmdInfo
                    addLogEntry "submitting again $cmdInfo"
                    cntlmon::submitDBqueryJob DBview $timeopt $histopt $binsize $userinfo $cmdInfo $client $cmdId
                }
	    } elseif  { [regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ]} {
        	set msg "2\n$client:$cmdId: submitted '$cmdinfo' request as job $jobid"
            ::${client}::reply $cmdId $msg
        } else {
        	error "Error '$cmdinfo' for $client:$cmdId: '$reply'"
        }
    } err ] } {
        addLogEntry  "$seqpt $err" orange
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${client}::${cmdId} }
    }
    
}

## ******************************************************** 
##
## Name: cntlmon::getMetaDataFile
##
## Description:
## extract data from getMetaData file
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getMetaDataFile { fname jobid } {

    if  { [ catch {
	    set filwd [ openILwdFile $fname r ] 
	    set contp [ readElement $filwd ]
	    closeILwdFile $filwd
        file delete -force $fname 
        set contp [ refContainerElement $contp 0 ]
        set numCols [ getElementAttribute $contp "size" ]
	    for { set i 0 } { $i < $numCols } { incr i 1 } {
		    set elmcontp [ refContainerElement $contp $i ]
            set colname [ string tolower [ getElementAttribute $elmcontp name ] ]
	        set entry [ getElement $elmcontp ascii none ]
            if  { ! [ regexp {>([^<]+)<} $entry -> entry ] } {
                set entry [ list ]
            }
		    ;##puts "i=$i, elmcontp = $elmcontp"
		    switch -regexp -- $elmcontp {	
                {^_[0-9a-f]+_p_LdasElement$} {
                    set ::${jobid}(db_$colname) [ split $entry \, ]
                }
			    {^_[0-9a-f]+_p_LdasArrayBase$} {
	                set ::${jobid}(db_$colname) [ string trim $entry ]
       		    }
			    default { addLogEntry "cannot process ilwd type $elmcontp" 2 }	
		    }
	    }
    } err ] } {
        return -code error "Error extracting data for $jobid from $fname: $err"
    }
}

## ******************************************************** 
##
## Name: cntlmon::formDataset
##
## Description:
## form the data from ilwd into xy values and histogram
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::formDataset { jobid client cmdId endtime  } {

    if  { [ catch {
        set rc -1
        set cols [ array name ::${jobid} db* ]
        set cols [ string trim $cols ]
        ;## find the time field first
        set timeindex [ lsearch -regexp $cols insertion_time ]
        set timecol [ lindex $cols $timeindex ]
        regsub {^db_} $timecol {} timecol
        set cols [ lreplace $cols $timeindex $timeindex ]
        regsub {^db_} $cols {} cols
  
        set index 0

	    foreach x [ set ::${jobid}(db_$timecol) ] {
            regexp {(\d+)-(\d+)-(\d+)[\s-]+(\d+).(\d+).(\d+)} $x -> year mon day hour min sec
            set gpstime [ utc2gps [ format "$year-$mon-$day $hour:$min:$sec" ] 0 ]
            set y [ lindex [ set ::${jobid}(db_$cols) ] $index ]
		    lappend ::${client}::${cmdId}(timedata) $gpstime $y
            lappend xlist $gpstime
		    incr index 1
	    }
        catch { set lastx $x }
        set numRows $index 

        if  { ! [ info exist ::${client}::${cmdId}(numRows) ] } {
            set ::${client}::${cmdId}(numRows) 0
        }
        incr ::${client}::${cmdId}(numRows) $numRows
        set totalrows [ set ::${client}::${cmdId}(numRows) ]
        ;## no rows
        if  { !$numRows } {
            if  { ! [ info exist ::${client}::${cmdId}(timedata) ] } {                
                set ::${client}::${cmdId}(limits) "$timecol $cols $totalrows 0 10 0 10" 
                set ::${client}::${cmdId}(timedata) [ list ]
                set rc 0
                error "no rows"
            }
        }
                
        set temp [ lsort -unique -integer $xlist ]
        set xmin [ lindex $temp 0 ]
        set xmax [ lindex $temp end ]

        set temp [ lsort -unique -real [ set ::${jobid}(db_$cols) ] ]
        set ymin [ lindex $temp 0 ]
        set ymax [ lindex $temp end ]
        set orgymin $ymin
        
        if  { [ info exist ::${client}::${cmdId}(limits) ] } {
            foreach { timecol valuecol last_numRows prev_xmin prev_xmax prev_ymin prev_ymax } \
                [ set ::${client}::${cmdId}(limits) ] { break }
            
            if  { $xmin > $prev_xmin } {
                set xmin $prev_xmin
            }
            if  { $xmax < $prev_xmax } {
                set xmax $prev_xmax
            }
            if  { $ymin > $prev_ymin } {
                set ymin $prev_ymin
            }
            if  { $ymax < $prev_ymin } {
                set ymax $prev_ymin
            }
        } 
        set ::${client}::${cmdId}(limits) "$timecol $cols $totalrows $xmin $xmax $ymin $ymax" 
        if  { $::DEBUG > 1 } {
            addLogEntry "numRows=$numRows,totalrows=$totalrows, xmin=$xmin,xmax=$xmax,ymin=$ymin,ymax=$ymax, \
            [ llength  [ set ::${client}::${cmdId}(timedata) ] ]"
        }
        if  { $numRows >= $::MAXROWS } {
            set rc 2
            set ::${client}::${cmdId}(lastset) [ list $timecol $lastx ]
            error "more"
        }            
    
        set ::${client}::${cmdId}(limits) "$timecol $cols $totalrows $xmin $xmax $ymin $ymax" 
  
        ;## remove this job
	    unset ::${jobid}
        set rc 0
    } err ] } {
        catch { unset ::${jobid} }
        if  { $rc < 0 } { 
            return -code error $err
        }
    }
    return $rc
}


## ******************************************************** 
##
## Name: cntlmon::cancelDBQuery
##
## Description:
## form the data from ilwd into xy values and histogram
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::cancelDBQuery { page client cmdId } {

    addLogEntry "cancelling DB query for $client cmdId $cmdId" blue
    set ::${client}::${cmdId}(cancel) 1 
    return "2\nCancelling in progress ...."
}

## ******************************************************** 
##
## Name: cntlmon::rejectedJobsCount 
##
## Description:
## count rejected jobs by cmd, user and dso
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::rejectedJobsCount {} {
    uplevel {
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/rejected_jobs.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			set currfile [ file join $::LDASLOG rejected_jobs.log ]
			if	{ [ file exist $currfile ] } {
				lappend filelist $currfile
			}
		}
		debugPuts "rejected files to search: $filelist"
		set cmds "list gpstime cmd user reason"
		set numcats [ expr [ llength $cmds ] - 1 ]
		set ymin 0
        set dso none
		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if  { [ catch {
				    set len [ llength $line ]
				    if	{ $len != $::REJECTED_JOB_COLS } {
						error "expected $::REJECTED_JOB_COLS columns, got $len"
					}
					foreach [ eval $cmds ] $line { break }
				} err ] } {
					addLogEntry "$file error at '$line', $err" red
					continue
				}
				;## remove when all is in gpstime
				if	{ $gpstime < $start_time || $gpstime > $end_time } {
					continue
				}
				incr rejected_totals 1
				if	{ [ regexp {(dataPipeline|dataStandAlone)} $cmd ] } {
					if	{ [ regexp {(\S+)test} $user -> dso ] } {
						if	{ ! [ info exist cmds4($cmd,$dso,$user,rejected) ] } {
							set cmds4($cmd,$dso,$user,rejected) 0
						}
						incr cmds4($cmd,$dso,$user,rejected) 1
					}
                    if	{ ! [ string equal $dso none ] } {
						if	{ ! [ info exist cmds3($cmd,$dso,rejected) ] } {
							set cmds3($cmd,$dso,rejected) 0
						}
						incr cmds3($cmd,$dso,rejected) 1
                    }	
				} else {
					if	{ ! [ info exist cmds3($cmd,$user,rejected) ] } {
						set cmds3($cmd,$user,rejected) 0
					}
					incr cmds3($cmd,$user,rejected) 1
					if	{ ![ info exist users3($user,$cmd,rejected) ] } {
						set users3($user,$cmd,rejected) 0
					}
					incr users3($user,$cmd,rejected) 1
				}
				if	{ ! [ info exist cmds2($cmd,rejected) ] } {
					set cmds2($cmd,rejected) 0
				}
				incr cmds2($cmd,rejected) 1
				if	{ ! [ info exist users2.1($user,rejected) ] } {
					set users2.1($user,rejected) 0
				}
				incr users2.1($user,rejected) 1
			}
		}		
	}
}

## ******************************************************** 
##
## Name: cntlmon::jobSummary
##
## Description:
## count jobs by status, cmd, dso and user.
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::jobSummary { range { queue_also 0} } {

	if	{ [ catch {
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
        set queuedata [ list ]
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/job_stats.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			lappend filelist [ file join $::LDASLOG job_stats.log ]
		}
		debugPuts "files $filelist"
		
		set jobtotals 0
		set passed_totals 0
		set failed_totals 0
		set rejected_totals 0
		
		cntlmon::job_statsColumns

		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if	{ [ catch { 
                    set len [ llength $line ]
					cntlmon::job_statsColsCheck			        
					foreach [ eval $cmds ] $line { break }
				} err ] } {
					addLogEntry "$file error at '$line': $err" red
					continue
				}
                cntlmon::jobCompletionTime
				if	{ $completed_time < $start_time || $completed_time > $end_time } {
					continue
				}
				incr jobtotals 1
                set status "${status}ed"
				incr ${status}_totals 1
				if  { $queue_also } {
                    lappend queuedata $completed_time $queue_size
                }
				if	{ [ regexp {(dataPipeline|dataStandAlone)\(libldas(.+)\.so\)} $cmd -> cmd dso ] } {

					if	{ ![ info exist cmds4($cmd,$dso,$user,$status) ] } {
						set cmds4($cmd,$dso,$user,$status) 0
					}
					incr cmds4($cmd,$dso,$user,$status) 1
					#incr users($cmd,$dso,$user,$status) 1
					if	{ ![ info exist cmds3($cmd,$dso,$status) ] } {
						set cmds3($cmd,$dso,$status) 0
					}
					incr cmds3($cmd,$dso,$status) 1	
					
					if	{ ![ info exist users3.1($user,$cmd,$dso) ] } {
						set users3.1($user,$cmd,$dso) 0
					}
					incr users3.1($user,$cmd,$dso) 1		
				} else {
					if	{ ![ info exist cmds3($cmd,$user,$status) ] } {
						set cmds3($cmd,$user,$status) 0
					}
					incr cmds3($cmd,$user,$status) 1
				}
                if	{ ![ info exist users3($user,$cmd,$status) ] } {
				    set users3($user,$cmd,$status) 0
				}
				incr users3($user,$cmd,$status) 1
                
				if	{ ! [ info exist cmds2($cmd,$status) ] } {
					set cmds2($cmd,$status) 0
				}
				incr cmds2($cmd,$status) 1
				
				#if	{ ! [ info exist cmds1($cmd) ] } {
				#	set cmds1($cmd) 0
				#}
				#incr cmds1($cmd) 1
				
				#if	{ ! [ info exist users1($user) ] } {
				#	set users1($user) 0
				#}
				#incr users1($user) 1
                
				if	{ ! [ info exist users2($user,$cmd) ] } {
					set users2($user,$cmd) 0
				}
				incr users2($user,$cmd) 1	
				if	{ ! [ info exist users2.1($user,$status) ] } {
					set users2.1($user,$status) 0
				}
				incr users2.1($user,$status) 1				
			}
		}
		
		cntlmon::rejectedJobsCount 
		
		;## now format data into tree format
		set textlist [ list ]
		set userlist [ list ]
		
		lappend textlist "\"Job Summary\""
		lappend textlist "/cmd"
		lappend textlist "/user"
		lappend textlist "/cmd/passed:$passed_totals"
		lappend textlist "/cmd/failed:$failed_totals"
		# lappend textlist "/cmd/total:$jobtotals"
		lappend textlist "/cmd/rejected:$rejected_totals"
		
		lappend textlist "/user/passed:$passed_totals"
		lappend textlist "/user/failed:$failed_totals"
		# lappend textlist "/user/totals:$jobtotals"
		lappend textlist "/user/rejected:$rejected_totals"
		
		set total 0
		#foreach { cmd } [ lsort -dictionary [ array names cmds1 ] ] {
		#	set count $cmds1($cmd)
		#	lappend textlist "/cmd/$cmd/total:$count"
		#}

		set total 0
		foreach entry [ lsort -dictionary [ array names cmds2 ] ] {			
			foreach { cmd status } [ split $entry , ] { break }
			set count $cmds2($entry)
			lappend textlist "/cmd/$cmd/$status:$count"
		} 
		
		foreach entry [ lsort -dictionary [ array names cmds3 ] ] {			
			foreach { cmd dso status } [ split $entry , ] { break }
			set count $cmds3($entry)
			lappend textlist "/cmd/$cmd/$dso/$status:$count"
		} 
		foreach { entry } [ lsort -dictionary [ array names cmds4 ] ] {
			set count $cmds4($entry)
			foreach { cmd dso user status } [ split $entry , ] { break }
			lappend textlist "/cmd/$cmd/$dso/$user/$status:$count"
			lappend userlist "/user/$user/$cmd/$dso/$status:$count"
		}
		;## counts by user
		
		#foreach { user } [ lsort -dictionary [ array names users1 ] ] {
		#	set count $users1($user)
		#	lappend textlist "/user/$user/total:$count"
		#}
	
		foreach entry [ lsort -dictionary [ array names users2 ] ] {
			foreach { user cmd } [ split $entry , ] { break }
			set count $users2($entry)
			lappend textlist "/user/$user/$cmd:$count"
		}
		foreach entry [ lsort -dictionary [ array names users2.1 ] ] {
			foreach { user status } [ split $entry , ] { break }
			set count [ set users2.1($entry) ]
			lappend textlist "/user/$user/$status:$count"
		}
		
		foreach entry [ lsort -dictionary [ array names users3 ] ] {
			foreach { user cmd status } [ split $entry , ] { break }
			set count $users3($entry)
			lappend textlist "/user/$user/$cmd/$status:$count"
		}
		
		foreach entry [ lsort -dictionary [ array names users3.1 ] ] {
			foreach { user cmd dso } [ split $entry , ] { break }
			set count [ set users3.1($entry) ]
			lappend textlist "/user/$user/$cmd/$dso:$count"
		}
		
		set textlist [ concat $textlist $userlist ]
        if  { $queue_also } {
            lappend textlist $queuedata
        }
		set result "0\n$textlist"
	} err ] } {
		set result "3\n$err"
	}
	regsub -all {/+} $result {/} result
	return $result	
}


## ******************************************************** 
##
## Name: cntlmon::getLDASTestResult
##
## Description:
## process results from getMetaData into data sets
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getLDASTestResult { sid var } {

    set seqpt ""
    if  { [ catch {
        
        set reply [ set $var ]
        if  { $::DEBUG >= 2 } {
            addLogEntry "reply='$reply'"
        }
		unset $var  
        fileevent $sid readable {}
        if  { [ regexp -nocase "(${::RUNCODE}\\d+)" $reply -> jobid ] } {
            if  { [ array exist ::${jobid} ] } {
                set client [ set ::${jobid}(client) ]
                regexp {(client\d+):(\d+)} $client -> client cmdId
            }
            set msg "0\n$reply"
            ::${client}::reply $cmdId "0\n$reply"
        } else {
            error "No client registered for job"
        }          
    } err ] } {
        addLogEntry  "$seqpt $err" orange
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${jobid} }
        catch { unset ::${client}::${cmdId} }
        ;## unset jobid
    }
    ;## send notification from cntlmonAPI and reply from manager
    close $sid
} 

## ******************************************************** 
##
## Name: cntlmon::getGSIClientJobStatus
##
## Description:
## get mgr msg upon job submission e.g. job number
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getGSIClientJobStatus { sid client cmdId cmdinfo args } {

    if  { [ catch {
	    set reply [ lindex $args 0 ]
        if  { [regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ]} {
		    set ::${jobid}(client) $client:$cmdId
            set msg "2\n$client:$cmdId: submitted '$cmdinfo' request as job $jobid"
            if	{ [ string equal ::$client [ namespace children :: $client ] ] } {
            	::${client}::reply $cmdId $msg
            } else {
            	error "$client not connected, reply discarded"
            }
        } else {
            error "No job Id for '$cmdinfo' for $client:$cmdId: '$reply'"
        }
    } err ] } {
        addLogEntry $err 2
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${client}::${cmdId} }
    }
}

## ******************************************************** 
##
## Name: cntlmon::getGSIChannelJobStatus
##
## Description:
## get mgr msg upon job submission e.g. job number
##
## Parameters:
##
## Usage:
##
## Comment:
## dont close socket since it is persistent

proc cntlmon::getGSIChannelJobStatus { client cmdId cmdinfo sid reply args } {

    if  { [ catch {
        if  { $::DEBUG > 1  } {
            addLogEntry "$client:$cmdId '$cmdinfo', mgr reply '$reply'" purple
        }
        if  { [ regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid  ] } {
        	if	{ ! [ info exist ::${jobid}(client) ]  } {
		    	set ::${jobid}(client) $client:$cmdId
            	set msg "2\n$client:$cmdId: submitted '$cmdinfo' request as job $jobid"
            	::${client}::reply $cmdId $msg      
            }      
        } 
        if	{ [ regexp {Subject} $reply ] } {
        	set msg "0\n$reply"
            ::${client}::reply $cmdId $msg
        }
        if	{ ! [ info exist jobid ] } {
            error "No job Id for '$cmdinfo' for $client:$cmdId: '$reply'"
        }
    } err ] } {
        addLogEntry $err 2
        catch { ::${client}::reply $cmdId "3\n$err" }
        catch { unset ::${client}::${cmdId} }
        catch { unset ::${jobid} }
    }
}
