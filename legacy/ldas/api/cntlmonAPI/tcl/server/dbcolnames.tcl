#! /ldcg/bin/tclsh

proc execdb2cmd { cmd } {
	uplevel {
		set start [ clock seconds ]
		set cmd "exec $::db2 [list $cmd ]"
        if  { ! [ regexp -nocase {connect} $cmd ] } {
		    #puts $cmd
        }
		catch { eval $cmd } data
		puts $data
	}
}

set ::TOPDIR [ lindex $argv 0 ]
source [ file join $::TOPDIR cntlmonAPI cntlmon.state ]
source [ file join $::TOPDIR LDASapi.rsc ]
source [ file join $::TOPDIR metadataAPI LDASdsnames.ini ]

if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
puts "db2=$db2"


set auto_path "$::LDAS/lib $auto_path"
set API test
package require generic

set dbname [ lindex $argv 1 ]
foreach { dbuser dbpasswd } [ set ::${dbname}(login) ] { break }

set cmd "connect to $dbname user ldasdb using [decode64 $dbpasswd]"
execdb2cmd $cmd
source /ldas/doc/db2/doc/text/dbtables.tcl
source [ file join $::DB2SCRIPTS dbtables.tcl ]

if  { [ catch {
set cmd "SELECT tabname,colname,typename from syscat.columns where \
tabschema = 'LDASDB' and typename in ('REAL','INTEGER', 'TIMESTAMP') \
order by tabname,colname"

execdb2cmd $cmd 
set state 0

set data [ split $data \n ]
foreach line $data {
	if	{ [ regexp -- {^[\-\s]+$} $line ] } {
		set state 1
		continue
	}
	if	{ $state } {
		foreach { table colname type } $line { break }
		if	{ [ regexp {^\d+$} $table ] } {
			break
		}
		# puts "table $table, colname $colname, type $type"
		set table [ string tolower $table ]
		set colname [ string tolower $colname ]
		if	{ [ regexp -nocase {creator_db|insertion_time} $colname ] } {
			continue
		}
        lappend ::tabcols($table) $colname
	}
}

set fd [ open dbtabcols.tcl w ]
puts $fd "package provide dbtabcols 1.0\n"

foreach table [ lsort [ array names  ::tabcols ] ] {
	puts $fd "array set ::TABCOLS \[ list $table [ list [ set ::tabcols($table) ] ] \]"
}
close $fd

} err ] } {
    puts $err
}
