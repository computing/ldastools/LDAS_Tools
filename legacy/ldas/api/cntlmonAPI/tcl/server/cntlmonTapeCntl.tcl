## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Observatory (LIGO) controlmon server Tcl Script.
##
## This script handles commands made to ldas manager
## and tape controlller.
## 
## ******************************************************** 

;## does ps on machine and formats the return for gui
;## will have to ssh for remote machine

package provide cntlmonTapeCntl 1.0

;## desc=tape controller alert text 
set ::TAPEALERT "Alert: tape controller may not be running;\n\
Please contact the LDAS person on duty at your site for \
information on how to handle this problem." 

## ******************************************************** 
##
## Name: cntlmon::updateTapeLog
##
## Description:
## return log data to client every interval
##
## Parameters:
## client 
## Usage:
## 
## Comment:
## No checking for tape controller 
## monitor is done by ckTapeCntl
## if there are no clients, the monitoring is cancelled
## ******************************************************** 

proc cntlmon::updateTapeLog {} {
    ;## broadcast to all interested parties 
    if  { [ catch {
        if  { [ info exist ::cntlmon::tapecontrol(clients) ] &&
              [ llength $::cntlmon::tapecontrol(clients) ] } {
            set data ""
            set fd [ open [ file join $::TAPECNTLDIR logs $::TAPELOG ] r ]
            set data [ read $fd ]
            close $fd
            set lastcheck [ clock format [ clock seconds ] \
			-format "%B %d %Y %H:%M:%S" ]
		    set msg "0\nmonitor=on tapecontrol=on state=1|Last displayed at: $lastcheck"
		    set data "$msg|$data"
		    notifyClients $data ::cntlmon::tapecontrol
            set ::cntlmon::tapecontrol(updateId) [ after $::TAPELOGINTERVAL \
            cntlmon::updateTapeLog ]
        } else {
            error "no clients"
        }
    } err ] } {
        addLogEntry "error $err"
        ;## catch if no afterId
        catch { monitorCleanup } 
    }
}

## ******************************************************** 
##
## Name: cntlmon::chkTapeCntl
##
## Description:
## this is a background process to see if tape library file
## changes as a sign tape controller is running
##
## Parameters:
## previous file mod time
##
## Usage:
##
## Comment:
## the monitoring will stop if there are no changes
## to the tape library file thus assuming tape controller
## may not be running
## if no errors, it reschedules itself if there are still clients
## and there are no errors during the process

proc cntlmon::chkTapeCntl { prevtime } {
	if	{ [ catch {
        if  { [ info exist ::cntlmon::tapecontrol(clients) ] && 
		      [ llength $::cntlmon::tapecontrol(clients) ] } {
    	    set curtime [ file mtime [ file join $::TAPECNTLDIR logs $::TAPELIBRARY ] ]
    	    set ::cntlmon::tapecontrol(tmdiff) [ expr $curtime - $prevtime ] 
    	    set lastcheck [ clock format [ clock seconds ] -format "%B %d %Y %H:%M:%S" ]
		    set ::cntlmon::tapecontrol(lastMonitorTime) $lastcheck
    	    ;##puts "last monitored at $lastcheck"
		
            ;## broadcast to all interested parties 
            set data ""
            set fd [ open [ file join $::TAPECNTLDIR logs $::TAPELOG ] r ]
            set data [ read $fd ]
            close $fd
			set host $::cntlmon::tapecontrol(host)
            #puts "curtime $curtime, prevtime $prevtime"
    	    if  { ! $::cntlmon::tapecontrol(tmdiff) } {
                foreach [ list cntlstate thost ] [ isTapeCntlOn $host ] { break }
        	    set alert "3\nmonitor=off tapecontrol=$cntlstate state=0 host=$host|$::TAPEALERT|$data"
                addLogEntry "Subject: tape controller failed to update log file; \
			    Body: last detected $lastcheck. Monitor is off." 3
			    notifyClients $alert ::cntlmon::tapecontrol
                monitorCleanup
			    ;## monitor stops here
    	    } else {
			    set msg "0\nmonitor=on tapecontrol=on state=1 host=$host|\
			    Last monitored at: $::cntlmon::tapecontrol(lastMonitorTime)"
			    set data "$msg|$data"
			    notifyClients $data ::cntlmon::tapecontrol
                set ::cntlmon::tapecontrol(afterId) \
				[ after $::cntlmon::tapecontrol(delay) [ list cntlmon::chkTapeCntl $curtime ] ]
            }
        } else {
			addLogEntry "no clients, monitoring stopped"
		}
	} err ] } {
        addLogEntry "error $err, monitoring stopped" 
        monitorCleanup
	}
}

## ******************************************************** 
##
## Name: cntlmon::startTapeCntl
##
## Description:
## starts Tape Controller scripts 
##
## Parameters:
## opt is START or RESTART
##
## Usage:
##
## Comment:
## if the tape control program is already running
## there is no action taken. 
## must execssh with cmd in bg or it does not return

proc cntlmon::startTapeCntl { opt host } {

	if	{ [ string match $host $::CNTLMON_API_HOST ] } {
		set host ""
	}
	set cmd [ file join set $::TAPECNTLDIR bin [ set ::${opt}TAPECNTL ] ]
	if	{  ! [ string length $host ] } { 
		catch { shellPipe "|$cmd &" } err 
	} else {
		catch { execssh $host "$cmd &" } err 
	}
	addLogEntry "executed $cmd,$err"
}

## ******************************************************** 
##
## Name: cntlmon::stopTapeCntl
##
## Description:
## stops Tape Controller scripts 
##
## Parameters:
## opt is STOP
##
## Usage:
##
## Comment:
## if the tape control program is already running
## there is no action taken. 
## must execssh with cmd in bg or it does not return

proc cntlmon::stopTapeCntl { host } {

	if	{ [ string match $host $::CNTLMON_API_HOST ] } {
		set host ""
	}
	set cmd [ file join $::TAPECNTLDIR bin $::STOPTAPECNTL ]
	if	{  ! [ string length $host ] } { 
		catch { shellPipe "|$cmd &" } err 
	} else {
       	set err [ execssh $host "$cmd &" ]
	}
    set status "executed $cmd to stop tape controller $err"
	addLogEntry $status
    return $status
}

## ******************************************************** 
##
## Name: cntlmon::getPid 
##
## Description:
## Given a program name, return the pid if the program
## is running.  Given a pid, returns the program name!
##
## Usage:
##
## Comments:
## Used to get information needed to be able to shutdown
## erstwhile processes.
## Depends on the existence of the "ps" system call.
## If the manager is trying to get the pid of a remote API
## then execssh call will be used to get the pids.

proc cntlmon::getPid { { prog "" } { host "" } } { 
     set tmp  [ list ]
     set data [ list ] 
     if { [ catch {
           if { ! [ string length $host ] } {             
              set data [ shellPipe "|/bin/ps -Ao fname,args,pid" ]
           } else {
              catch { [ execssh $host {/bin/ps -Ao fname,args,pid} ] } data 
           }
     } err ] } {
        return -code error "[ myName ]: $err"
     }   
     
     set data [ lrange [ split $data "\n" ] 1 end ]
     
     if { [ catch {
        foreach line $data {
           set name {}
           set pid  {}
           #if { [ llength $line ] != 2 } { continue }
		   #foreach { name pid } $line { break }
           
		   if	{ [ regexp {(\S+)[\s\t]*(.+)[\s\t]+(\d+)} $line -> name args pid ] } {
                if { ! [ regexp -nocase {[a-z]} $name ] } {
                    set pid $name
                    set name <defunct>
                }
           ;## we are only interested in the base name of
           ;## the process.
                set name [ lindex $name 0 ]
           ;## c++ programmers can wreck us!
                if { [ string equal $name c++ ] } {
                    continue
                } 
		    
                if { [ regexp ^$name  $prog ] || \
                    [ regexp ^$prog $name  ] || \
				    [ regexp $prog  $args  ] } {
                    lappend tmp $pid
                } elseif {   
                    [ regexp ^$pid\$ $prog ] || \
                    [ regexp ^$prog\$ $pid ] } {
                    lappend tmp $name
                }
		   }
        }
     } err ] } {
        ;## the ps call sometimes fails in a strange way
        ;## producing an error in the regexp compilation.
        if { [ regexp {compile regular} $err ] } {
           set vars "(name: '$name' pid: '$pid' prog: '$prog')"
           set err "$err $vars possible failure of ps call."
        }   
        return -code error "[ myName ]: $err"
     }
    
     set tmp [ lsort -dictionary $tmp ]  
     return $tmp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::isTapeCntlOn
##
## Description:
## checks if the tape control script is running 
##
## Parameters:
##
## Usage:
##
## Comment:
## returns 1 if on, 0 if off

proc cntlmon::isTapeCntlOn { { hosts "" } } {

	if	{ ! [ string length $hosts ] } {
		set hosts $::LDASmachines
	}
	foreach host $hosts {
		set thost $host
		if	{ [ string match $host $::CNTLMON_API_HOST ] } {
			set thost ""
		} 
		if 	{ [ catch {
       		set pids [ cntlmon::getPid $::TAPECNTL $thost ]
    	} err ] } {
     		return -code error "getPid $err"
   		}   
		if	{ [ string length $pids ] } {
			addLogEntry "tapecontrol is running on $host, pids=$pids" blue
			return "on $host"
		}
	}
    return off
}

## ******************************************************** 
##
## Name: cntlmon::getTapeCntlState
##
## Description:
## gets state of tape controller and monitor 
## automatically adds client to log update list 
## if state is 1  
##
## Parameters:
##
## Usage:
##
## Comment:
## returns TC and TM states

proc cntlmon::getTapeCntlState {} {

    if  { [ catch {
        set cntlState off
        set monState off 
        set msg ""
		set host ""
		if	{ ! [ array exists ::cntlmon::tapecontrol ] } {
			array set ::cntlmon::tapecontrol [ list state 0 ]
		}
        foreach [ list cntlState host ] [ isTapeCntlOn ] { break }
        if  { [ string match on $cntlState ] } {
			array set ::cntlmon::tapecontrol [ list host $host ]
            if  { [ info exist ::cntlmon::tapecontrol(afterId) ] } {
                set monState on
                array set ::cntlmon::tapecontrol [ list state 1 ]
                ;## automatically registerClient for update
	            set client [ uplevel { namespace tail [ namespace current ] } ]
                set cmdId  [ uplevel { set cmdId } ]
                cntlmon::registerClient $client $cmdId ::cntlmon::tapecontrol
                set msg "$client registered for updates"
            } else {
                set monState off
                array set ::cntlmon::tapecontrol [ list state 2 ]
            }
        } else {
            array set ::cntlmon::tapecontrol [ list state 0 ]
        }
        set result "0\nmonitor=$monState tapecontrol=$cntlState \
        state=$::cntlmon::tapecontrol(state) host=$host|$msg|<html></html>"  
    } err ] } {
        set result "3\nmonitor=$monState tapecontrol=$cntlState \
        state=$::cntlmon::tapecontrol(state) host=$host|$err|<html></html>"
		addLogEntry $err 2
    }
    return $result  
}

## ******************************************************** 
##
## Name: cntlmon::startTapeController
##
## Description:
## starts tape controller and monitor
##
## Parameters:
## client 
## Usage:
##
## Comment:
## called by client

proc cntlmon::startTapeController { startopt delay thost } {
    
    if  { [ catch {
        startTapeCntl $startopt $thost
		after 1000
		set host ""
		foreach [ list cntlstate host ] [ isTapeCntlOn $thost ] { break }
        if  { [ string match on $cntlstate ] } {    
            set client [ uplevel { namespace tail [ namespace current ] } ]
            set cmdId  [ uplevel { set cmdId } ]
			array set ::cntlmon::tapecontrol [ list host $host ]
            startTapeMonitor $delay $client $cmdId 
            array set ::cntlmon::tapecontrol [ list state 1 ]
			set state 1
        } else {
			error "tape controller start error"
		}		 
    } err ] } {
		foreach [ list cntlstate host ] [ isTapeCntlOn $thost ] { break }
        if  { [ string match on $cntlstate ] } {
			array set ::cntlmon::tapecontrol [ list host $host ]
            set state 2
        } else {
            set state 0
        }
        array set ::cntlmon::tapecontrol [ list state $state ]
        return "3\nmonitor=off tapecontrol=off state=$state|$err|<html></html>"
    }
    return "0\nmonitor=on tapecontrol=$cntlstate state=$state host=$host|Tape Controller & Monitoring are On|\
    <html></html>"
}


## ******************************************************** 
##
## Name: cntlmon::startTapeMonitor 
##
## Description:
## starts tape monitor and maybe tape controller server
##
## Parameters:
## client 
## Usage:
##
## Comment:
## called by client

proc cntlmon::startTapeMonitor { delay { client "" } { cmdId "" } } {
  
    if  { [ catch {
        set result ""
        ;## start tape monitoring 
        set ::cntlmon::tapecontrol(tmdiff) -1
        set ::cntlmon::tapecontrol(delay) $delay
        set ::cntlmon::tapecontrol(clients) [ list ]
        
        ;## this has to run slightly later for an initial check so client is registered.
        set curtime [ file mtime [ file join $::TAPECNTLDIR logs $::TAPELIBRARY ] ]
        set ::cntlmon::tapecontrol(afterId)  [ after $delay cntlmon::chkTapeCntl $curtime ]
        addLogEntry "Tape Controller monitor started."
            
        ;## if called by a client, register the client for notification
        if  { ! [ string length $client ] } {
		    set client [ uplevel { namespace tail [ namespace current ] } ]
            set cmdId  [ uplevel { set cmdId } ]
        }
        cntlmon::registerClient $client $cmdId ::cntlmon::tapecontrol
        
        ;## quick updatewith logs, next one launch after 1 min.
		set result "0\nmonitor=on tapecontrol=on state=1|\
		Tape controller is checked every [ expr $::cntlmon::tapecontrol(delay)/60000 ] minutes|\
		<html></html>"
		set ::cntlmon::tapecontrol(updateId) \
       		[ after 1000 cntlmon::updateTapeLog ]
    } err ] } {
		set result "3\nmonitor=off tapecontrol=on state=2|$err"
    }
    return $result
}


## ******************************************************** 
##
## Name: cntlmon::stopTapeController
##
## Description:
## starts tape controller and monitor
##
## Parameters:
## client 
## Usage:
##
## Comment:
## called by client

proc cntlmon::stopTapeController { host } {
    if  { [ catch {
        stopTapeCntl $host
        stopTapeMonitor $host
    } err ] } {
        addLogEntry $err
        foreach [ list cntlstate host ] [ isTapeCntlOn $host ] { break }
        return "3\nmonitor=off tapecontrol=$cntlstate state=0|$err|<html></html>"
    }
    return "0\nmonitor=off tapecontrol=off state=0|Tape Controller & Monitor are Off|\
    <html></html>"  
}


## ******************************************************** 
##
## Name: cntlmon::stopTapeMonitor 
##
## Description:
## stops tape monitor and maybe controller
##
## Parameters:
## client 
## Usage:
##
## Comment:   
     
proc cntlmon::stopTapeMonitor { host } {

    if  { [ catch {
        foreach [ list cntlstate host ] [ isTapeCntlOn $host ] { break }
        if  { [ string match on $cntlstate ] } {
            set state 2
        } else {
            set state 1
        }
        set fd [ open [ file join $::TAPECNTLDIR logs $::TAPELOG ] r ]
        set data [ read $fd ]
        close $fd
        set msg "0\nmonitor=off tapecontrol=$cntlstate state=$state host=$host|\
        Monitor turned off|$data"
        notifyClients $msg ::cntlmon::tapecontrol
        after 1000
        monitorCleanup 
    } err ] } {
        return "3\nmonitor=off tapecontrol=$cntlstate state=$state host=$host|\
        Monitor turned off error: $err|<html></html>"
    }
    return "0\nmonitor=off tapecontrol=$cntlstate state=$state host=$host"
}    

## ******************************************************** 
##
## Name: cntlmon::monitorCleanup
##
## Description:
## cleanup the monitoring and reset to state 2
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc cntlmon::monitorCleanup {} {
    catch { after cancel $::cntlmon::tapecontrol(afterId) }
    catch { after cancel $::cntlmon::tapecontrol(updateId) }	
    unset ::cntlmon::tapecontrol
    array set ::cntlmon::tapecontrol [ list state 0 ]
}

## ******************************************************** 
##
## Name: cntlmon::refuseUpdates 
##
## Description:
## remove client from the monitor updates
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc cntlmon::refuseUpdates { client } {
    if  { [ catch { 
        cntlmon::unregisterClient $client ::cntlmon::tapecontrol
    } err ] } {
        return "3\nmonitor=off tapecontrol=off state=0|$err|<html></html>"
    }
    return "0\nmonitor=off tapecontrol=off state=0|$client removed from monitor|<html></html>"
}
