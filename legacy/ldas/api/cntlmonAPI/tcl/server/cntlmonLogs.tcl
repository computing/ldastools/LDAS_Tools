## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon server Tcl Script.
##
## This script has log processing functions for server.
## 
## ******************************************************** 

package provide cntlmonLogs 1.0

package require tail

## ******************************************************** 
##
## Name: cntlmon::queryLogs
##
## Description:
## cancel the long query
##
## Parameters:
## apilist - list of apilist whose logs are grepped for pattern
## Usage:
##
## Comment:
## 
##  
proc cntlmon::queryLogs { cmd time_jobid pat numLines apilist regexpOpt { runcode "" } } {

    if  { [ catch {
		if	{ [ string length $runcode ] } {
        	set result [ $cmd $time_jobid $pat $numLines $apilist $regexpOpt $runcode ]
		} else {
			set result [ $cmd $time_jobid $pat $numLines $apilist $regexpOpt ]
		}
        set result "0\n$result"
    } err ] } {
        set result "3\n$err"
        addLogEntry "queryLogs error: $err"
    }  
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::queryLogsPipe
##
## Description:
## long query in background
##
## Parameters:
## apilist - list of apilist whose logs are grepped for pattern
## Usage:
##
## Comment:
## 
##  

proc cntlmon::queryLogsPipe { args } {

    if  { [ catch {
		set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]
        foreach { host pid port } $::logCacheUtils { break }
        set logPage [ lindex $args 2 ]
        addLogEntry "exec bgcntlmon ${client}:$cmdId $args >& ${client}_${logPage}.log &" blue
        
        cntlmon::dropdotfile
        ;## do this right before logs to be more up-to-date
        sendPort "logCacheUtils::buildLogCache" $host $port
        catch { exec bgcntlmon ${client}:$cmdId $args >& ${client}_${logPage}.log & } err 
        set result "2\nWorking on it ..."
    } err ] } {
        set result "3\n$err"
        addLogEntry $err 2
		set result "3\n$result"
    } 
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::cancelQuery
##
## Description:
## cancel the long query
##
## Parameters:
## var - can be an api e.g. metadata
##  or the variable traced e.g. 
## ::client1::tail_/ldas_outgoing/logs/LDASmetadata.log.html
## Usage:
## Comment:
## 
##  

proc cntlmon::cancelQuery { var } {
   
    if  { [ catch {  
        set name ""      
        if  { ! [ regexp {tail_(.+)} $var match logfile ] } {
            ;## it is an api, so construct the variable name from caller
            set logfile [ file join $::LDASLOG LDAS${var}.log.html ]
            set procname ""
            set cmdId ""
            foreach { procname cmdId }  [ info level -1 ] { break } 
            if  { [ regexp {^(client\d+)} $procname match name ] } {
                set var ::${name}::tail_$logfile
            }
        } 
        puts "cancelQuery, var=$var,logfile=$logfile"
        catch { cancelTail $logfile $var }
        ;## remove the var from client
        catch { unset $var } 
        set result "0\nQuery terminated."
        addLogEntry "Traced cancelled on $var."
    } err ] } {
        set result "3\ncancel error: $err"
        addLogEntry "Error cancelling trace on $var $err" 2
    }
    return $result
}  

## ******************************************************** 
##
## Name: cntlmon::localPipeCmd
##
## Description:
## set up local pipe to background task
##
## Parameters:
## callback - client callback
## cmdId 
## args - cmd and arguments
##
## Comment:
## 
##  

proc cntlmon::localPipeCmd { callback cmdId args } {

	if	{ [ catch {
		set fid [ open "| $args" a+ ]
		fconfigure $fid -blocking off
        fconfigure $fid -buffering full
		set junk [ read $fid ]
        ;## now make sure we got authenticated (this will throw
        ;## a "broken pipe" exception if auth failed).
        if { [ catch {
           fconfigure $fid -buffering line
           puts $fid hostname
        } err ] } {
           if { [ regexp {broken pipe} $err ] } {
              set err "pipe failed for client $callback,cmd $cmdId,$args"
           }
           return -code error $err
        }
        fconfigure $fid -buffering full		
        set junk [ read $fid ]
		set uniqid [ key::time ]
		set ::$uniqid {}
		fileevent $fid readable [ list cntlmon::readsshPipe $fid ::$uniqid $callback $cmdId ]
	} err ] } {
		addLogEntry $err 2
        return -code error $err
    } 
	return $fid
}

## ******************************************************** 
##
## Name: cntlmon::cancelLocalPipe
##
## Description:
## cancel local pipe by terminating process 
##
## Parameters:
## callback - client callback
## cmdId 
## args - cmd and arguments
##
## Comment:
## 
##  

proc cntlmon::cancelLocalPipe { fid } {

    if  { [ catch {
		set result ""
		set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]
		if	{ [ info exist ::${client}::pid($fid) ] } {
			set pid [ set ::${client}::pid($fid) ]
			catch { kill -9 $pid } err
            addLogEntry "kill -9 $pid $err" purple
			catch { unset ::${client}::pid($fid) }
			close $fid
		} 
		set result "0\nCancelled"
		addLogEntry "local pipe $fid for process $pid cancelled."
	} err ] } {
		addLogEntry $err 2
        set result "3\n$err"
    } 
	return $result
}
			
## ******************************************************** 
##
## Name: cntlmon::logFilterPid
##
## Description:
## register Pid for bgcntlmon
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::logFilterPid { client page pid } {

    if  { [ catch {
        regexp {(client\d+):(\d+)} $client -> client cmdId
        catch { set ::${client}::$page $pid }
        addLogEntry "$client log filter pid $pid"
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: cntlmon::cancelLogFilter
##
## Description:
## send interim filter status to client
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::cancelLogFilter { page } {

    if  { [ catch {
        set client [ uplevel { namespace tail [ namespace current ] } ]
        set cmdId  [ uplevel { set cmdId } ]
        set pid [ set ::${client}::$page ]
        catch { kill -9 $pid } err1
        addLogEntry "kill -9 $pid $err1" purple
        ::${client}::reply $cmdId "3\n$page cancelled $err1"
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: cntlmon::logFilterStatus
##
## Description:
## send interim filter status to client
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::logFilterStatus { client result } {

    if  { [ catch {
        regexp {(client\d+):(\d+)} $client -> client cmdId
        set callback "::${client}::reply"
        if	{ [ string length [ info proc $callback ] ] } {
        	$callback $cmdId "2\n$result"
        } else {
        	addLogEntry "$client $result" blue
        }
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: cntlmon::logFilterDone 
##
## Description:
## got notified by bgcntlmon that log filter is finished
##
## Parameters:
##
## Usage:
##
## Comments:


proc cntlmon::logFilterDone { clientcmd logPage result } {

    if  { [ catch {
        regexp {(client\S+):(\d+)} $clientcmd -> clientTag cmdId
        addLogEntry "$clientcmd, result '$result'" purple
        regexp {(client\d+)} $clientTag -> client
        set callback ::${client}::reply
        catch { unset ::${clientTag}_${logPage}_done }
        if  { [ regexp -nocase -- {^error} $result ] } {
            set result1 "3\n$result"
        } elseif { [ string length $result ] } {
            set firstfile "$::SITE_HTTP[lindex $result 0 ]"
            set result1 "0\n$firstfile\n[ dumpFile [ lindex $result 0 ] ]"
        } else {
            set result1 "0\nNo logs were found."
        }
        if  { [ info exist ::DEBUG_bgcntlmon ] && !$::DEBUG_bgcntlmon } {
            catch { file delete -force ${client}_${logPage}.log }
        }
        file delete -force $::CLIENTLOGDIR/$clientcmd
        
        ;## return result to test report or client for log filter        
        ;## do not reply to client if this is from test report
        regexp {(client\d+)} $client -> client_namespace
        if	{ [ string length [ info proc $callback ] ] &&
              ! [ info exist ::${clientTag}testReportLock ] } {
        	$callback $cmdId $result1            
        } else {
            addLogEntry "setting ::${clientTag}_${logPage}_done to $result" purple
            set ::${clientTag}_${logPage}_done  $result
        }
    } err ] } {
        addLogEntry $err 2
        catch { set ::${clientTag}_${logPage}_done  $err }
    }
    if  { [ file exist ${clientTag}_${logPage}.log ] && ! [ file size ${clientTag}_${logPage}.log ] } {
        file delete -force ${clientTag}_${logPage}.log
    }
}
## ********************************************************	

## ******************************************************** 
##
## Name: cntlmon::diskcacheLogs
##
## Description:
## got notified by bgcntlmon that log filter is finished
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::diskcacheLogs { client logLevel } {

	if	{ [ catch {
		set patterns [ array names ::DISKCACHE_LOG_PATTERN_DESC ]
		foreach pattern $patterns {
			set total($pattern) 0
		}

		set logfiles [ glob -nocomplain /ldas_outgoing/logs/cmonClient/${client}_Log-Filter-${logLevel}_Page*.html ]

		set total_count 0
		set diskcache_all_total 0
		set text ""
        
		foreach logfile $logfiles {
			foreach pattern $patterns {
				set rc [ catch { exec grep -c $pattern $logfile} data ]
    			if 	{ !$rc } {
					if	{ [ regexp {(\d+)} $data -> count ] } {
						incr total($pattern) $count
					} 
				}
			}

			set rc [ catch { eval exec grep -E diskcacheAPI.+lines $logfile } data ]
			if	{ !$rc } {
				puts $data
				regexp {diskcacheAPI\s+\((\d+) lines\)} $data -> diskcache_total
				puts $diskcache_total
				incr diskcache_all_total $diskcache_total
			}
		}
		
		set text "Total in diskcache $diskcache_all_total lines\n"
		foreach item [ array names total ] {
			append text "Found [ set total($item) ] for '$::DISKCACHE_LOG_PATTERN_DESC($item)'\n" 
			incr total_count [ set total($item) ]
		}

		set diff [ expr $diskcache_all_total - $total_count ]
		if	{ $diff } {
			append text "Found $diff extra lines\n"
		}
	} err ] } {
    	addLogEntry $err red	
	}
    return $text
}

## ******************************************************** 
##
## Name: cntlmon::non_diskcacheLogs
##
## Description:
## got notified by bgcntlmon that log filter is finished
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::non_diskcacheLogs { client logLevel } {

	
	if	{ [ catch {
    	set result ""
		set logfiles [ glob -nocomplain /ldas_outgoing/logs/cmonClient/${client}_Log-Filter-${logLevel}_Page*.html ]
        set pattern "lines"
        
		foreach logfile $logfiles {
			set rc [ catch { exec grep -c $pattern $logfile} data ]
    		if 	{ !$rc } {
				if	{ [ regexp {(\S+)\s+(\d+)\s lines\)} $data -> api count ] } {
					append result "$api $count errors\n"
				}
			} else {
            	set error "grep error on $logfile: $err"
            	addLogEntry $error red
                append result "$error\n"
            }
		}
        if	{ ! [ string length $result ] } {
        	set result "No errors in all APIs except diskcache."
        }
	} err ] } {
    	addLogEntry $err red
        set result $err	
	}
    return [ string trim $result \n ]
}

## ******************************************************** 
##
## Name: cntlmon::getAPILeaks
##
## Description:
## add a list of users to ldas users queue via manager
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getAPILeaks { {apilist {}} } {
	
    if	{ ! [ llength $apilist ] } {
		set apilist $::API_LIST
	}
	
    set cmd "\{ puts \$cid \[ leaksSummary \] \}"
    
    set result ""
    foreach api $apilist {
    	if	{ [ catch {
            append result "set ::LEAKS($api) \"[ getEmergData $api $cmd ]\"\n"
		} err ] } {
			append result "set ::LEAKS($api) \"$api leaksSummary error: $err\"\n" 
		}
	}
    return [ string trim $result \n ]
}

## ******************************************************** 
##
## Name: cntlmon::getUsage
##
## Description:
## get usage data for test report
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getUsage { range } {

    set usagetext ""
    if  { [ catch {
        set queue_also 1
        set usage [ cntlmon::jobSummary $range $queue_also ]
        set queuedata [ lindex $usage end ] 
        
        ;## individual usage counts
        regexp {/cmd/passed:(\d+).+/cmd/failed:(\d+).+/cmd/rejected:(\d+)} $usage -> passed failed rejected
        
        set usagelist [ split $usage ]
       
        foreach line $usagelist {
            if  { [ regexp {/cmd/([^/\(\)]+)/(passed|failed|rejected):(\d+)} $line -> cmd status utotal ] } {
                set Cmd($cmd,$status) $utotal  
            }
        }
        set n 0

        foreach cmditem [ lsort -dictionary [ array names Cmd ] ] {
            set cmditem [ split $cmditem , ]
            set cmd [ lindex $cmditem 0 ]
            set type [ lindex $cmditem 1 ]
            foreach type [ list passed failed rejected ] {
                if  { ![ info exist Cmd($cmd,$type) ] } {
                    set Cmd($cmd,$type) 0
                }
            }
            if  { ![ info exist done_$cmd ] } {
                append usagetext "<b>$cmd</b> P [ set Cmd(${cmd},passed) ] F [ set Cmd(${cmd},failed) ] R [ set Cmd(${cmd},rejected) ] "
                set done_$cmd 1
                incr n 1
                if  { $n >=3 && [ expr $n/3 ] } {
                    append usagetext "<br>"
                    set n 0 
                }
            }
        }
        ;## compute queue mode and max
        foreach { queueMin queueMax } [ cntlmon::computeYMinMax $queuedata 0 0 ] { break }  
        set queueMode [ cntlmon::modeY $queuedata ] 
    } err ] } {
        addLogEntry $err red
        return -code error $err 
    }
    return [ list $passed $failed $rejected $queueMode $queueMax $usagetext ]
}

## ******************************************************** 
##
## Name: cntlmon::diskUsage
##
## Description:
## get disk usage data for test report
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::diskUsage {} {

    uplevel {
    if  { [ catch {  
		set prevfile ~install/public_html/nightly_test_results/index.html
		set previous ""
		if	{ [ file exist $prevfile ] } {     
        	set disk_fd [ open ~install/public_html/nightly_test_results/index.html r ]
        	set previous [ read -nonewline $disk_fd ]
        	close $disk_fd
		}    
        
        set scratchHistory ""
        if  { [ regexp {/scratch disk usage.+<div class="pre"><font size=-1>([^\<]+)</font></div></td>} $previous -> scratchHistory ] } {
            set scratchHistory [ split [ string trim $scratchHistory \n ] \n ]  
            set historySize [ llength $scratchHistory ]
            if  { $historySize > 30 } {
                set scratchHistory [ lrange $scratchHistory [ expr $historySize-30 ] end ]
            }
        }        
        set outgoingHistory ""
        if  { [ regexp {/ldas_outgoing disk usage.+<font size=.+>(.+)</font></div>.+/scratch disk usage} $previous -> outgoingHistory ] } {           
            set outgoingHistory [ split [ string trim $outgoingHistory \n ] \n ]  
            set historySize [ llength $outgoingHistory ]
            if  { $historySize > 30 } {
                set outgoingHistory [ lrange $outgoingHistory [ expr $historySize-30 ] end ]
            }
        }
        ;## check disk space
        catch { exec df $::TOPDIR } disk_usage
        regexp {Mounted on(.+)} $disk_usage -> disk_usage
        set disk_usage [ string trim $disk_usage \n ]
        regsub  \n $disk_usage " " disk_usage
        regsub  {\s+} $disk_usage "   " disk_usage
        set now [ clock format [ clock scan now ] -format "%m/%d/%y %I:%M %p" ]
        set disk_usage "[ string trim $disk_usage ] $now"
        lappend outgoingHistory $disk_usage
        set diskUsage [ join $outgoingHistory \n ]
        append results  "\nset ::DISK_USAGE [ list $diskUsage ]"

        ;## add usage of /scratch
        set rc [ catch { exec df /scratch } scratch_usage ]
        if  { !$rc } {
            regexp {scratch\s+(.+)} $scratch_usage -> scratch_usage
            regsub -all {/scratch} $scratch_usage "" scratch_usage
            set scratch_usage "[ string trim $scratch_usage ] $now"
            lappend scratchHistory $scratch_usage
            set scratchUsage [ join $scratchHistory \n ]
            append results "\nset ::SCRATCH_USAGE [ list $scratchUsage ]"
        } else {
            append results "\nset ::SCRATCH_USAGE  \"Not mounted\""
        }                                
    } err ] } {
        return -code error $err
    }
    } 
}

## ******************************************************** 
##
## Name: cntlmon::computeYMinMax
##
## Description:
## compute the Y min and max values
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::computeYMinMax { set ymin ymax } {

	foreach { x y } $set {	
		if	{ $ymin > $y } {
			set ymin $y
		}
		if	{ $y > $ymax } {
			set ymax $y
		} 
	}
	return "$ymin $ymax"
} 

## ******************************************************** 
##
## Name: cntlmon::modeY 
##
## Description:
## get the mode of the Y values in xy list
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::modeY {samples} {
    
    if  { ! [ llength $samples ] } {
        return 0
    }
    set xlist [ list ]
    set ylist [ list ]
	foreach { x y } $samples {
        lappend ylist $y
    }
    foreach y $ylist {
        if  { ! [ info exist count($y) ] } {
            set count($y) 1
        } else {
            incr count($y) 
        }
    }
    set temp [ list ]
    foreach { item cnts }  [ array get count ] {
        lappend temp [ list $item $cnts ]
    }
    set temp [ lsort -decreasing -index 1 -integer $temp ]
      
    foreach item $temp { 
        foreach { x y } $item { break }
        if  { $x } {
            break
        } 
    }
    return $x
   
}

## ******************************************************** 
##
## Name: cntlmon::cpuThreadData 
##
## Description:
## generates max cpu value per api and max thread and most frequent thread use
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::cpuThreadData { range } {

    if  { [ catch {
        set results ""
        foreach api $::API_LIST {
            set data [ cntlmon::memoryUsageGraph $api $range ]
            set data [ split $data \n ]
            set data [ lindex $data 1 ]
            set cpudata [ lindex $data $::cntlmon::OFFSET_CPUDATA ]
            foreach { mincpu maxcpu } [ cntlmon::computeYMinMax $cpudata 0 0 ] { break } 
            set threaddata [ lindex $data $::cntlmon::OFFSET_THREADDATA ]
            foreach { minthread maxthread } [ cntlmon::computeYMinMax $threaddata 0 0 ] { break } 
            append results "\nset ::MAXCPU($api) [ format "%0.02f%%" $maxcpu ]"
            append results "\nset ::MAXTHREAD($api) $maxthread\nset ::MODETHREAD($api) [ cntlmon::modeY $threaddata ]"   
        }
    } err ] } {
        return -code error $err
    }
    return $results
}

## ******************************************************** 
##
## Name: cntlmon::leastSquareInit
##
## Description:
## intialize parms for computing least square fit
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::leastSquaresInit {} {

	uplevel {
		catch { unset xlist }
		catch { unset ylist }
		catch { unset newpoints }
		catch { unset a }
		catch { unset b }
		catch { unset xdelta  }
		catch { unset ydelta }
	}

}

## ******************************************************** 
##
## Name: cntlmon::leastSquareCoefficients
##
## Description:
## compute least square coefficients
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::leastSquareCoefficients  {} {

         uplevel {
                 ;## split into x and y
                 set xlist [ list ]
                 set ylist [ list ]

                 set sumx [ expr double(0) ]
                 set sumy [ expr double(0) ]
                 set sumtsq  [ expr double(0) ]
                 set sumty [ expr double(0) ]
                 
                 if { [ expr [llength $points] % 2 ] != 0 } {
                         ;## An odd number of points was found in the list
                         ;## This is an internal error condition that
                         ;## should print a fatal message
					error "Data set does not have even number of points"
                 }

                 set numItems [ expr [ llength $points ] / 2 ]
                 foreach { x y } $points {
                         set x [ expr double($x) ]
                         set y [ expr double($y) ]
                         lappend xlist $x
                         lappend ylist $y
                         set sumx [ expr $sumx + $x ]
                         set sumy [ expr $sumy + $y ]
                 }

                 set meanx  [ expr $sumx / $numItems ]
                 foreach { x y } $points {
                         set t [ expr double($x) - $meanx ]
                         set sumtsq [ expr $sumtsq + ( $t * $t ) ]
                         set sumty [ expr $sumty + ( $t * $y ) ]
                 }

                 if      { $sumtsq != 0.000000 } {
                         set b [ expr $sumty / $sumtsq ]
                         set a [ expr ($sumy - $b*$sumx) / $numItems ]
                 } else {
                         set b 0.0
                         set a [ expr $sumy/$numItems ]
                 }
				 set sigma [ expr sqrt(1.0 / $sumtsq) ]
				 set sumchi2 0.0
				 foreach { x y } $points {
				 	set tempsum [ expr $y - $a - $b * $x ]
				 	set sumchi2 [ expr $sumchi2 + $tempsum * $tempsum ]
				 }
				 set sigdat [ expr sqrt($sumchi2/($numItems-2)) ]
				 set sigma  [ expr $sigma * $sigdat ] 
         }
}

## ******************************************************** 
##
## Name: leastSquarePoints
##
## Description:
## compute new data set based on known a and b
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:
## This is done in the context of caller

proc cntlmon::leastSquarePoints {} {
	uplevel {
		set newpoints [ list ]
		if	{ $a == 0.00000 && $b == 0.00000 } {
			set constanty [ lindex $data 1 ]
			set x [ lindex $xlist 0 ]
			set y $constanty 
			lappend newpoints $x 
		    lappend newpoints $y
			set x [ lindex $xlist end ]
			set y $constanty 
			lappend newpoints $x
			lappend newpoints $y	
			if	{ [ info exist ymin ] } {
				if	{ $y < $ymin } {
					set ymin $y
				}
		  	} else {
				set ymin $y
		  	}
			set y [ expr $y + 10.0 ]
	 	  	if	{ [ info exist ymax ] } {
				if	{ $y > $ymax } {
					set ymax $y
				}
		  	} else {
				set ymax $y
		  	}	
		} else {
		  set x1 [ lindex $xlist 0 ]
		  set x2 [ lindex $xlist end ]
		  foreach x [ list $x1 $x2 ] {
		  	set y [ expr $a + $b * $x ] 
		  	lappend newpoints $x
		  	lappend newpoints $y			
		  	if	{ [ info exist ymin ] } {
				if	{ $y < $ymin } {
					set ymin $y
				}
		  	} else {
				set ymin $y
		  	}
	 	  	if	{ [ info exist ymax ] } {
				if	{ $y > $ymax } {
					set ymax $y
				}
		  	} else {
				set ymax $y
		  	}
		  }
		}
	}
}

## ******************************************************** 
##
## Name: cntlmon::fit2points
##
## Description:
## compute new data set based on known a and b
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:
## done in context of caller

proc cntlmon::fit2points {} {
	uplevel {
		;## split into x and y
		set numItems 0.0
		set xlist [ list ]
		set ylist [ list ]
	
		foreach { x y } $points {
			set x [ expr $x * 1.0 ]
			set y [ expr $y * 1.0 ]
			lappend xlist $x
			lappend ylist $y
		}
		set xstart [ lindex $xlist 0 ]
		set xend [ lindex $xlist end ]
		set ystart [ lindex $ylist 0 ]
		set yend [ lindex $ylist end ]
		set b [ expr ( $yend - $ystart )/( $xend - $xstart ) * 1.0 ]
		set a [ expr $ystart - ( $b * $xstart ) ]	
	}
}

## ******************************************************** 
##
## Name: cntlmon::fit2points
##
## Description:
## compute slope for a least square fitted line
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:

proc cntlmon::slope { data } {
	
	set xstart [ lindex $data 0 ]
	set ystart [ lindex $data 1 ]
	set num [ llength $data ]
	set xend [ lindex $data [ expr $num - 2 ] ]
	set yend [ lindex $data end ]
	#puts "xstart=$xstart, ystart=$ystart, xend=$xend yend=$yend, xdelta [ expr $xend - $xstart ], ydelta=[ expr $yend - $ystart ]"
	return "[ expr $xend - $xstart ] [ expr $yend - $ystart ]"
	
}

## ******************************************************** 
##
## Name: cntlmon::apiMemoryData
##
## Description:
## computes least square fit, sigma, slope and #jobs for apis.
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:

proc cntlmon::apiMemoryData { range } {

    if  { [ catch {
        set results ""
        foreach api $::API_LIST {
        set data [ cntlmon::memoryUsageGraph $api $range ]
        set data [ split $data \n ]
        set data [ lindex $data 1 ]
        set apidata [ lindex $data $::cntlmon::OFFSET_APIDATA ]
		set set 1
		set i 2
        set jobs [ lindex $data $::cntlmon::OFFSET_JOBS ]
        append results "\nset ::APIJOBS($api) $jobs"
        set ymin [ lindex $data $::cntlmon::OFFSET_YMIN ] 
		set ymax [ lindex $data $::cntlmon::OFFSET_YMAX ] 
        if  { [ string length $ymax ] } { 
            append results "\nset ::MAXAPIMEM($api) [ format %0.2f [ expr $ymax/1024.0] ]"
        } else {
            append results "\nset ::MAXAPIMEM($api) [ format %0.2f 0.0 ]"
        }
        set leastsqfit ""
        set color pass
		if	{ $jobs } {
		    foreach data $apidata { 
			set numPoints [ expr [ llength $data ]/2 ]
			cntlmon::leastSquaresInit
          
			if	{ $numPoints > 1 } {
				set points $data
				if	{ $numPoints > 2  } {
				;## set least square fit
					cntlmon::leastSquareCoefficients
				} else {
					cntlmon::fit2points
				}
				;## dont extraplolate put in begin and end points 
				cntlmon::leastSquarePoints 
				foreach { xdelta ydelta } [ cntlmon::slope $newpoints ] { break }
				
				if	{ $ydelta } {
					set rate $b
					set jobrate [ expr $ydelta/$numPoints ]
				} else {    
					set rate 0.0
                    set jobrate 0.0
				}
                set rate [ format %.3f $rate ]
                if  { $rate < 0.010 } {
                    set ystart [ lindex $ylist 0 ]
                    set yend   [ lindex $ylist end ]
                    if  { [ expr abs($ystart - $yend) ] < 0.010 } {
                        set rate 0.000
                        set jobrate 0.0
                        set xstart [ lindex $xlist 0 ]
                        set xend   [ lindex $xlist end ]
                        set newpoints [ list $xstart $ystart $xend $yend ]
                    }
                }
                if  { $set > 1 } {
                    append leastsqfit "<br>"
                }
				append leastsqfit "[ format "Period $set: %s KB/sec" $rate ]"
                if  { $rate > $::MEM_LEAK_YELLOW_LIMIT } {
                    set color warn
                }
                if  { $rate > $::MEM_LEAK_RED_LIMIT } {
                    set color fail
                }
				if	{ [ info exist sigma ] } {
					append leastsqfit " ( +/- [ format %.3f $sigma ] KB/sec )"
				}
				append leastsqfit " [ format "%.3f KB/job" $jobrate ]"
			}
			incr set 1
		}            

        } else {
			set jobrate 0
            append leastsqfit " [ format "Period $set: %s KB/sec" 0.0 ] \
            ( +/- [ format %.3f 0.0 ] KB/sec ) [ format "%.3f KB/job" $jobrate ]"
        }

        ;## set color based on memory leak
        append results "\nset ::LEASTSQFIT($api) [ list $leastsqfit ]"
        append results "\nset ::COLOR_MEM($api) $color"
        }
    } err ] } {
        set fd [ open temp w ]
        puts $fd $results
        close $fd
        return -code error $err
    }
    return $results
}

## ******************************************************** 
##
## Name: cntlmon::logPatterns
##
## Description:
## computes least square fit, sigma, slope and #jobs for apis.
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:

proc cntlmon::logPatterns { client logLevel patternvar numLines { showNotFound { 0 } } } {

	if	{ [ catch {
		set patterns [ array names $patternvar ]
		foreach pattern $patterns {
			set total($pattern) 0
		}

		set logfiles [ glob -nocomplain $::LDASLOG/cmonClient/${client}_${logLevel}_Page*.html ]
		set total_count 0
		set text ""
        
		foreach logfile $logfiles {
			foreach pattern $patterns {
				set rc [ catch { exec grep -c -i -E $pattern $logfile} data ]
    			if 	{ !$rc } {
					if	{ [ regexp {(\d+)} $data -> count ] } {
						incr total($pattern) $count
					} 
				}
			}
		}
		
		foreach item [ array names total ] {
            set found [ set total($item) ]
            if  { $found || $showNotFound } {
                append text "Found [ set total($item) ] for '[ set ${patternvar}($item) ]'\n"
                incr total_count [ set total($item) ]
            }
		}
        set other [ expr $numLines - $total_count ]
        if  { $other < 0 } {
            set other 0
        }
        ;## rule out manager counts for the same error
        set logfile1 [ glob -nocomplain $::LDASLOG/cmonClient/${client}_${logLevel}_Page1.html ]
        set rc [ catch { exec grep managerAPI $logfile1 } data ]
        if  { !$rc } {
            if  { [ regexp {managerAPI \((\d+) lines} $data -> managerCount ] } {
                set other [ expr $other - $managerCount ]
                if  { $other < 0 } {
                    set other 0
                }
            }
            append text "Found $other others (see logs)"
        } else {
            append text "Found $other others (see logs)"
        }
	} err ] } {
    	addLogEntry $err red	
	}
    return "$total_count [ list $text ]"
}

## ******************************************************** 
##
## Name: cntlmon::testReportLogRequest
##
## Description:
## computes least square fit, sigma, slope and #jobs for apis.
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:

proc cntlmon::testReportLogRequest { client range tag patternvar apilist } {

	set text ""
    if  { [ catch {
        set cmdId  1
        foreach { host pid port } $::logCacheUtils { break }
        set logPage Log-Filter-1
        ;## qualify the client name
        set qualifiedName ${client}
        set fname ${qualifiedName}_${logPage}
        catch { file delete [ glob -nocomplain $::LDASLOG/${fname}*.html ] } err
        set args "grepMailRedballs $range $logPage mail.gif|telephone.gif|ball_red.gif [ list $apilist ] -nocase"
        addLogEntry "exec bgcntlmon ${qualifiedName}:$cmdId $args >& ${fname}.log &" blue
        
        cntlmon::dropdotfile
        ;## do this right before logs to be more up-to-date
        sendPort "logCacheUtils::buildLogCache" $host $port
        catch { unset ::${$fname}_done }
        set ::${qualifiedName}testReportLock 1
        catch { exec bgcntlmon ${qualifiedName}:$cmdId $args >& ${fname}.log & } err 
        vwait ::${fname}_done
        unset ::${qualifiedName}testReportLock
        set result [ set ::${fname}_done ]
        set index [ lsearch -regexp $result index\.html ] 
        set found 0
        
        set response ""
        ;## get totals from index.html if more than 1 page
        set total_counts 0
        
		;## if there are no logs, define vars as none
		if	{ [ string length $result ] } {
		;## get totals from index.html if more than 1 page
		if	{ $index > -1 } {
			set file [ lindex $result $index ]
            set url $file
    		set rc [ catch { exec grep lines $file } data ]
    		if	{ !$rc } {
                if	{ [ regexp -nocase -- {found (\d+) total lines} $data -> total ] } {
				    set total_counts $total
				}
                set text "set ::LOG_TOTALS($tag) \[ list total $total "
                ;## the api totals are in one line
                foreach api $apilist {
                    if  { [ regexp -nocase -- "($api)\\s+(\\d+)" $data -> apiname count ] } {
                        append text " $apiname $count "
                    }
                }
                append text " \]"
                set total_counts $total
    		}
		} else {
			set file $result
            set url $file
			set rc [ catch { exec grep lines $file } data ]            
    		if	{ ! $rc } {
                set text "set ::LOG_TOTALS($tag) \[ list "
        		set data [ split $data \n ]
        		foreach line $data {
        			if	{ [ regexp {found\s+(\d+)} $line -> grand_total ] } {
                        set counts(total) $grand_total
            		} elseif	{ [ regexp {>(\S+)API\s+\((\d+) lines\)} $line -> api count ] } {
                        set counts($api) $count
            		}
        		}
    		}
            #regsub -all {diskcache} $apilist {} api_no_diskcache
			foreach item [ concat total $apilist ] {
				if	{ [ info exist counts($item) ] } {
					append text " $item $counts($item) "
                    set found 1
    			} else {
    				append text "$item 0 "
    			}
			}
            append text " \]"
            set total_counts $counts(total)
        }
        ;## scan for common errors
        if	{ $total_counts } {
        	foreach { errtotal details } [cntlmon::logPatterns ${qualifiedName} $logPage $patternvar $total_counts ] { break }
        	append text "\nset ::[ string toupper ${tag} ]_ERRS \"$details\""
        }
		if	{ [ regexp {tandem|ldas-suntest|ldasbox} $::LDAS_SYSTEM ] } {
			set wwwtag ""
		} else {
			set wwwtag www.
		}
        set text "set ::LOGURL($tag) \"<a href='http://$wwwtag$::GLOBUS_MANAGER_API_HOST$url'>Log details</a>\"\n$text"
		;## if no logs were found
		} else {
			set text "set ::LOGURL($tag) \"\"\nset ::LOG_TOTALS($tag) \"total 0 \"\nset ::[ string toupper ${tag} ]_ERRS \"No errors found.\""
		}
    } err ] } {
    	addLogEntry $err red
        catch { unset ::${qualifiedName}testReportLock }
		return -code error $err
    } 
    return [ string trim $text ]
}

## ******************************************************** 
##
## Name: cntlmon::testReport
##
## Description:
## report of testing within a given time frame
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::testReport { range { archive 0 } } {
    set seqpt ""
	if	{ [ catch {
        if  { [ catch {
            set client [ uplevel { namespace tail [ namespace current ] } ]
            set cmdId  [ uplevel { set cmdId } ]
        } err ] } {
            set client "client[clock seconds]"
            set cmdId 1
        }  
    	set results ""
        regexp {(\d+)-(\d+)} $range -> startgps endgps ] 
        set reportdate [ clock format [ clock seconds ] -format "%m/%d/%y %I:%M %p" ]
		if	{ ! [ string length $startgps ] } {	
        	set yesterday [ clock scan "18:00 yesterday" ]
            set now [ clock scan "9:00 am today" ]            
            set day [ clock format $yesterday -format "%a" ]
            
			if      { [ regexp {Sat|Sun|Mon} $day ] } {
                set startutc [clock scan "18:00 last Friday"]
        		set start [ clock format $startutc -format "%m/%d/%y %I:%M %p" ]
                set startstr [ clock format $startutc -format "%Y%m%d%I%M" ]
			} else {
                set startutc $yesterday
        		set start [ clock format $yesterday -format "%m/%d/%y %I:%M %p" ]
                set startstr [ clock format $startutc -format "%Y%m%d%I%M" ]
			}
            set end [ clock format $now -format "%m/%d/%y %I:%M %p" ]
            set endutc $now
            set endstr [ clock format $endutc -format "%Y%m%d%I%M" ]
            set startgps [ utc2gps $start 0 ]
	        set endgps   [ utc2gps $end 0 ]
		} else {
            set start [ gps2utc $startgps 0 ]
            set startutc [ clock scan $start ]
            set startstr [ clock format $startutc -format "%Y%m%d%I%M" ]
            set end [ gps2utc $endgps 0 ]
            set endutc [ clock scan $end ]
            set endstr [ clock format $endutc -format "%Y%m%d%I%M" ]
        }
        set delta [ expr $endutc - $startutc ]
        set reportname  $::LDASLOG/cmonClient/${client}testReport_${startstr}-${endstr}.html  
          
        set results "set ::LDAS_SYSTEM [ string toupper $::LDAS_SYSTEM ]"
        append results "\nset ::TCL_VERSION $::tcl_patchLevel\nset ::REPORTDATE [ list $reportdate ]"
        if  { [ info exist ::cntlmon::db2version ] } {
            append results "\nset ::DB2VERSION $::cntlmon::db2version" 
        } 
        if  { [ info exist ::cntlmon::globus_version ] } {
            append results "\nset ::GLOBUS_VERSION $::cntlmon::globus_version" 
        }      
        if  { [ info exist ::cntlmon::tclglobus_version ] } {
            append results "\nset ::TCLGLOBUS_VERSION $::cntlmon::tclglobus_version" 
        }
        append results "\nset ::LDAS_VERSION $::LDAS_VERSION"
        append results "\nset ::STARTDATE \"$start\""
        append results "\nset ::ENDDATE \"$end\""
        append results "\nset ::LOG_ERRORS none"
        ;## log and job purge times can be updated via cmonClient
        append results "\nset ::LOG_PURGE_DAYS [ format "%0.2f" [ expr $::LOGS_PURGE_BEFORE/(3600.0 * 24) ] ]"
        append results "\nset ::JOB_PURGE_HOURS [ format "%0.2f" [ expr $::JOBS_PURGE_BEFORE/3600.0 ] ]"
        append results "\nset ::CLEANUP_HOURS [ format "%0.2f" [ expr $::CLEAR_OUTPUT_PERIOD/3600000.0 ] ]"
        
        ;## find the most recent log file to grep WAIT
        set loop_delay unknown
        set testlogs [ glob -nocomplain /ldas_outgoing/test/loop2/*.log ]
        set this_mtime 0
        foreach logfile $testlogs {        
            set file_mtime [ file mtime $logfile ]
            if  { $file_mtime > $this_mtime } {
                set this_mtime $file_mtime
                set this_file $logfile
            }
        }
        if  { [ string length $this_file ] } {
        	catch { exec grep WAIT $this_file | tail -1 } data
            if	{ [ regexp {WAIT: ([\d\.]+)} $data -> this_loop_delay ] } {
                set loop_delay $this_loop_delay
            }
        }
		append results "\nset ::LOOP_DELAY $loop_delay"
        ;## API summary
        append results "\n[ cntlmon::getAPILeaks ] "
        
        ;## API job summary
        foreach { passed failed rejected queueMode queueMax usage } [ cntlmon::getUsage $startgps-$endgps ] { break }
        append results "\nset ::QUEUE_MODE $queueMode\nset ::QUEUE_MAX $queueMax"
        set total [ expr $passed + $failed + $rejected ]
        if  { $total } {
            set percent_passed [ format "%0.5f" [ expr ($passed*100.0)/$total ] ]
            set percent_failed [ format "%0.5f" [ expr ($failed*100.0)/$total ] ]
            set percent_rejected [ format "%0.5f" [ expr ($rejected*100.0)/$total ] ]
			set jobrate [ expr int($total/($delta/3600.0)) ]
        } else {
            set percent_passed 0.0
            set percent_failed 0.0
            set percent_rejected 0.0
			set jobrate 0
        }
        
        ;## set color of jobs
        if  { $failed || $rejected } {
            if  { $passed } {
                append results "\nset ::COLOR_JOBS warn"
            } else {
                append results "\nset ::COLOR_JOBS fail"
            }
        } else {
            append results "\nset ::COLOR_JOBS pass"
        }
        ;## get max cpu and thread usage
        append results "\n[ cntlmon::apiMemoryData $startgps-$endgps ] "
        append results "\n[ cntlmon::cpuThreadData $startgps-$endgps ] "
        
        ;## get disk usage
        cntlmon::diskUsage 
        
        ;## generate the html by substituing data

        set results [ string trim $results \n ] 
        if  { $::DEBUG > 1 } {
            set fd [ open "testReportResults.dat" w ]
            puts $fd [ string trim $results \n ]
            close $fd
        }
            
        set fd [ open /ldas/doc/testing/nightly_test_report.html r ]
        set data [ read -nonewline $fd ]
        close $fd
        
        ;## generate the html by substituing data
        
        append results "\nset ::JOBS_PASSED $passed\nset ::JOBS_FAILED $failed\nset ::JOBS_REJECTED $rejected"
        append results "\nset ::PERCENT_PASSED $percent_passed\n\
        set ::PERCENT_FAILED $percent_failed\n\
        set ::PERCENT_REJECTED $percent_rejected"
        append results "\nset ::JOB_RATE $jobrate\nset ::JOBS_TOTAL $total\nset ::USAGE [ list $usage ]"
        set apilist { manager datacond eventmon frame ligolw metadata }
        
        set qualifiedName ${client}non_diskcache${startstr}-${endstr}
        append results "\n[ cntlmon::testReportLogRequest $qualifiedName $startgps-$endgps non_diskcache ::KNOWN_ERRS_LOG_DESC $apilist ]"
        set qualifiedName ${client}diskcache${startstr}-${endstr}
        append results "\n[ cntlmon::testReportLogRequest $qualifiedName $startgps-$endgps diskcache ::DISKCACHE_LOG_PATTERNS diskcache ]"
        set interp [ interp create ]
        set seqpt "results '$results'"
        $interp eval $results
        
        ;## if archived report use the log urls in archive directory
        if  { $archive } {
            set url_diskcache [ $interp eval set ::LOGURL(diskcache) ]            
            set url_non_diskcache [ $interp eval set ::LOGURL(non_diskcache) ]
            set reportdir $::LDASLOG/cmonClient
            set rc [ regsub $reportdir $url_diskcache $::LDASARC/cmonClient/$::LDAS_VERSION archiveReport ]     
            if  { $rc } {
                $interp eval [ list set ::LOGURL(diskcache) $archiveReport ]
            }
            set rc [ regsub $reportdir $url_non_diskcache $::LDASARC/cmonClient/$::LDAS_VERSION archiveReport ] 
            if  { $rc } {
                $interp eval [ list set ::LOGURL(non_diskcache) $archiveReport ]
            }
        }
        set seqpt "$interp eval [ list subst $data ]"
        set data [ $interp eval [ list subst $data ] ]
        set fd [ open $reportname w ]
        puts $fd $data
        close $fd

        if  { $archive } {
            cntlmon::archiveReport $reportname $url_diskcache $url_non_diskcache
        }
        interp delete $interp
	} err ] } {
    	addLogEntry $err red
        set fd [ open testReportErrors.dat w ]
        puts $fd "'$seqpt'\n\n$err\n$results"
        close $fd
        return "3\n$err"
    }
	if	{ [ regexp {tandem|ldas-suntest|ldasbox} $::LDAS_SYSTEM ] } {
		set wwwtag ""
	} else {
		set wwwtag www.
	}
    if  { ! $archive } {
        set msg "0\nYour report is http://$wwwtag${::GLOBUS_MANAGER_API_HOST}${reportname} on your browser"
    } else {
        set msg "0\nYour report is archived as http://$wwwtag${::GLOBUS_MANAGER_API_HOST}$::LDASARC/cmonClient/$::LDAS_VERSION/[ file tail $reportname ]"
    }
    return $msg
}


## ******************************************************** 
##
## Name: cntlmon::archiveReport
##
## Description:
## archive report if requested by client
##
## Parameters:
##
## Usage:     
##
## Comments:
## symbolic link of logs and style sheets
proc cntlmon::archiveReport { fname url_diskcache url_non_diskcache } {

    if  { [ catch {
        ;## archive directory set up ball gifs and style sheets
        if  { ! [ file exist $::LDASARC/cmonClient ] } {
            file mkdir $::LDASARC/cmonClient
            styleSheets $::LDASARC/cmonClient
            if  { ! [ file exist $::LDASARC/cmonClient/ball_red.gif ] } {
                gifBalls $::LDASARC/cmonClient
            }
        }
        set gifs [ glob -nocomplain $::LDASARC/cmonClient/*.gif ]
        set reportArchive $::LDASARC/cmonClient/$::LDAS_VERSION
        if  { ! [ file exist $reportArchive ] } { 
            file mkdir $reportArchive
            styleSheets $reportArchive 0 $::LDASARC/cmonClient
            foreach image $gifs {
                set imagefile [ file tail $image ]
                file link -symbolic $reportArchive/$imagefile $image
            }
        }
        file rename -force $fname $reportArchive
        ;## copy logs also if present
        if  { [ string length $url_diskcache ] } {
            if { ! [ regexp -- "($::LDASLOG/cmonClient/client\[^_\]+)" $url_diskcache -> diskcache_pattern ] } {
                error "regexp failed to get diskcache pattern for $url_diskcache"
            }
            eval file rename -force [ glob -nocomplain $diskcache_pattern*.html ] $reportArchive
        }
        
        if  { [ string length $url_non_diskcache ] } {
            if  { ! [ regexp -- "($::LDASLOG/cmonClient/client\[^_\]+)" $url_non_diskcache -> non_diskcache_pattern ] } {
                error "regexp failed to get diskcache pattern for $url_non_diskcache"
            }
            eval file rename -force [ glob -nocomplain $non_diskcache_pattern*.html ] $reportArchive
        }
	
		if	{ [ regexp {(client\d+)[^\d]+(\d+)-(\d+)} $fname -> client start end ] } {
			set pattern "$client*$start*$end*Log-Filter*.html"
		} else {
			set pattern "*Log-Filter*.html"
		}
        ;## update the log filter index pages (if more than 1 page ) to point to archive path
        foreach indexfile [ glob -nocomplain  $reportArchive/$pattern ] {
            set fd [ open $indexfile r ]
            set logdata [ read -nonewline $fd ]
            close $fd
            if  { [ regsub -all $::LDASLOG/cmonClient $logdata $reportArchive logdata ] } {  
                set fd [ open $indexfile w ]
                puts $fd $logdata
                close $fd
            } 
        }        
    } err ] } {
        return -code error $err
    }
}   
