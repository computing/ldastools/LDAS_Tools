#!/usr/bin/env tclsh
;## $Id: QualityChannnel.tcl,v 1.1 2001/10/16 15:45:07 emaros Exp $
;## Test sending of data from frameAPI to datacondAPI

;## ---------------------------------------------------------------------
;## Establish default values.
;## ---------------------------------------------------------------------

set user no_user_specified
set pwrd no_password_specified
set email no_email_specified

set host ldas-dev.ligo.caltech.edu
set base_port 10000

set frame_time last
set interferometer H
set interferometer_number 2
set base_channel_name LSC-AS_Q

;## ---------------------------------------------------------------------
;## Procedure to send command to managerAPI
;## ---------------------------------------------------------------------
proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    regsub -- { -password\s+(\S+)} $cmd { -password **** } display_cmd
    puts $display_cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## ---------------------------------------------------------------------
;## Process user's resource file
;## ---------------------------------------------------------------------

set rc "~/.datacondAPI.rc"
if { [ file isfile $rc ] && [ file readable $rc ] } {
    source $rc
}

;## ---------------------------------------------------------------------
;## Process command line arguments
;## ---------------------------------------------------------------------

if { $argc > 0 } {
    set user [ lindex $argv 0 ]
    if { $argc > 1} {
	set pwrd [ lindex $argv 1 ]
	if { $argc > 2 }{
	    set email [ lindex $argv 2 ]
	}
    }
}

set port [ expr $base_port + 1 ]

source "utility.tcl"

;## ---------------------------------------------------------------------
;## Command to be executed
;## ---------------------------------------------------------------------

set channel_name "$interferometer$interferometer_number:$base_channel_name"
regsub -all -- {:} $channel_name {\\:} channel_alias
set start_time 680895416
set end_time 680896086
set site H2
set sql "SELECT start_time as StartSec, start_time_ns as StartNanoSec, end_time as StopSec, end_time_ns as StopNanoSec FROM SEGMENT WHERE ( ( Start_Time >= $start_time and Start_Time <= $end_time ) OR ( end_time <= $end_time AND end_time >= $start_time ) ) AND ( segment_group LIKE '$site:%' )"

set cmd "
ldasJob { -name $user -password $pwrd -email $email } {
    dataPipeline
    -inputformat { ilwd }
    -returnprotocol { file:/results.ilwd }
    -outputformat { ilwd ascii }
    -qualitychannel { { 1024 $start_time 0 $end_time 0 { $sql } qc push } }
    -algorithms { value(qc); }
    -datacondtarget { }
    -resultname { None }
    -resultcomment { None }
}
"
;## ---------------------------------------------------------------------
;## Execute the command
;## ---------------------------------------------------------------------

;#puts $cmd
sendCmd $cmd
