#!/usr/bin/env tclsh
;## $Id: FrameToDatacond.tcl,v 1.3 2001/04/05 23:36:35 emaros Exp $
;## Test sending of data from frameAPI to datacondAPI

;## ---------------------------------------------------------------------
;## Establish default values.
;## ---------------------------------------------------------------------

set user no_user_specified
set pwrd no_password_specified
set email no_email_specified

set host ldas-dev.ligo.caltech.edu
set base_port 10000

set frame_time last
set interferometer H
set interferometer_number 2
set base_channel_name LSC-AS_Q

;## ---------------------------------------------------------------------
;## Procedure to send command to managerAPI
;## ---------------------------------------------------------------------
proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    regsub -- { -password\s+(\S+)} $cmd { -password **** } display_cmd
    puts $display_cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## ---------------------------------------------------------------------
;## Process user's resource file
;## ---------------------------------------------------------------------

set rc "~/.datacondAPI.rc"
if { [ file isfile $rc ] && [ file readable $rc ] } {
    source $rc
}

;## ---------------------------------------------------------------------
;## Process command line arguments
;## ---------------------------------------------------------------------

if { $argc > 0 } {
    set user [ lindex $argv 0 ]
    if { $argc > 1} {
	set pwrd [ lindex $argv 1 ]
	if { $argc > 2 }{
	    set email [ lindex $argv 2 ]
	}
    }
}

set port [ expr $base_port + 1 ]

source "utility.tcl"

;## ---------------------------------------------------------------------
;## If the frame_time is set to last, then calculate what last really means
;## ---------------------------------------------------------------------

if { [ string match $frame_time "last" ] } {
    set frame_time [ get_latest ]
}
;## ---------------------------------------------------------------------
;## Command to be executed
;## ---------------------------------------------------------------------

set channel_name "$interferometer$interferometer_number:$base_channel_name"
regsub -all -- {:} $channel_name {\\:} channel_alias

set cmd "
ldasJob { -name $user -password $pwrd -email $email } {
    dataPipeline
    -inputformat { ilwd }
    -returnprotocol { file:/results.ilwd }
    -outputformat { ilwd ascii }
    -times $frame_time
    -interferometers  $interferometer
    -framequery { Adc($channel_name) }
    -datacondtarget {}
    -resultname { None }
    -resultcomment { None }
    -aliases raw=_ch0
    -algorithms {
	x = slice(raw,0,100,1);
	phi = value(0.0);
	f = value(0.5);
	z = mix(phi,f,x);
    }
}
"
;## ---------------------------------------------------------------------
;## Execute the command
;## ---------------------------------------------------------------------

;#puts $cmd
sendCmd $cmd
