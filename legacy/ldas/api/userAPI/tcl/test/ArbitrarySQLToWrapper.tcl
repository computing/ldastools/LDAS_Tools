#!/usr/bin/env tclsh
;## $Id: ArbitrarySQLToWrapper.tcl,v 1.2 2001/10/16 15:43:45 emaros Exp $
;## Test sending of data from frameAPI to datacondAPI

;## ---------------------------------------------------------------------
;## Establish default values.
;## ---------------------------------------------------------------------

set user no_user_specified
set pwrd no_password_specified
set email no_email_specified

set host ldas-dev.ligo.caltech.edu
set base_port 10000

set frame_time last
set interferometer H
set interferometer_number 2
set base_channel_name LSC-AS_Q

;## ---------------------------------------------------------------------
;## Procedure to send command to managerAPI
;## ---------------------------------------------------------------------
proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    regsub -- { -password\s+(\S+)} $cmd { -password **** } display_cmd
    puts $display_cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## ---------------------------------------------------------------------
;## Process user's resource file
;## ---------------------------------------------------------------------

set rc "~/.datacondAPI.rc"
if { [ file isfile $rc ] && [ file readable $rc ] } {
    source $rc
}

;## ---------------------------------------------------------------------
;## Process command line arguments
;## ---------------------------------------------------------------------

if { $argc > 0 } {
    set user [ lindex $argv 0 ]
    if { $argc > 1} {
	set pwrd [ lindex $argv 1 ]
	if { $argc > 2 }{
	    set email [ lindex $argv 2 ]
	}
    }
}

set port [ expr $base_port + 1 ]

source "utility.tcl"

;## ---------------------------------------------------------------------
;## Command to be executed
;## ---------------------------------------------------------------------

set channel_name "$interferometer$interferometer_number:$base_channel_name"
regsub -all -- {:} $channel_name {\\:} channel_alias

set cmd "
ldasJob { -name $user -password $pwrd -email $email } {
    dataPipeline
    -inputformat { ilwd }
    -returnprotocol { file:/results.ilwd }
    -outputformat { ilwd ascii }
    -dbquery {{select program,version,cvs_repository,cvs_entry_time,comment,is_online,node,username,unix_procid,start_time,end_time,jobid,domain,param_set,ifos from process fetch first 10 rows only} pass none }
    -datacondtarget mpi
    -resultname { None }
    -resultcomment { None }
    -dynlib /ldcg/lib/lalwrapper/libldastrivial.so
}
"
;## ---------------------------------------------------------------------
;## Execute the command
;## ---------------------------------------------------------------------

;#puts $cmd
sendCmd $cmd
