<!-- -*- mode: HTML -*- -->
<HTML>
<HEAD>

<!-- Please preserve look n' feel! -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<TITLE>Sample conditionData meta macros</TITLE>

<META name="description" content="Description of the conditionData User Command">

<META name="keywords" content="macros, commands, user">

</HEAD>

<BODY BGCOLOR="#DDDDDD" TEXT="#000000">
<a name="top">
<table>
<td width=100%><td>
<IMG SRC="/doc/gifs/LIGO.gif" ALIGN=right ALT="LIGO.gif">
</table>

<h3> The <i>conditonData</i> User Command (Meta Macro) </h3>
<b>All user commands have the form:</b>
<p>
<tt><font color="red">
<b>ldasJob</b> { -name {} -password {} -email {} } { <b>userCmd</b> -opt1 {} ... }
</tt></font>
<p>
Which is in the format of a 
<a href=http://www.tcl.tk/software/tcltk/><b>Tcl</b></a> command,
<b>ldasJob</b>, with two required arguments:
<ol>
<li>
A Tcl list of <b>user information</b> consisting of username,
password, and e-mail address.  All fields must be filled or
the command will be rejected.<br>
Client software which would like to receive responses from the
LDAS system on a port instead of through email can provide a
<b>hostname:port</b> combination as the email argument.  A
further feature of this method is that the hostname can be replaced
with the magic key <b>!host!</b>, and the LDAS system will use
the IP address it finds by doing a peername lookup on the
connected socket.
<li>
A <b>user command</b> in the form of a Tcl list, for which there
exists a &quot;meta&quot; macro file which can be expanded into Tcl code
which can then be evaluated by the LDAS system.
<br>
The user command consists of the command name, <b><i>conditionData</i></b>,
followed by a series of options and their values.  To determine which
options are required for a given process pipeline you will need to
study the descriptions of the various options carefully.
<br>
Some option fields will accept a &quot;null&quot; argument consisting of
a matching pair of braces <b>&quot;{}&quot;</b> with no interposed space.
<br>
Meta macros consist of a prototype declaration of the arguments
for the given user command, and a template describing the calling
order of API specific blocks of Tcl code which are concatenated
into a larger block comprising the complete request, which can
then be distributed by the assistant manager for interpretation
by the low level API's.
<br>
These API specific blocks are maintained as API specific macro
files consisting of immediately interpretable Tcl code.
</ol>
There are individual API specific getData user commands for
retrieving frame data, metadata, and LIGO lightweight data:
<ul>
<a href="#conditiondata"><li><b><i>conditionData</i></b></a>
</ul>
<!-- bottom of page pointer section --> 
<center>
<h2>
<a href="#conditiondata">conditionData</a>
<IMG SRC="/doc/gifs/arrow_right.gif" ALT="arrow_right.gif">
</h2>
</center>
<!-- Here we put the stuff which will become the linked pages --> 
<hr>
<ul>
<li>
<a name="conditiondata">
<b><i>conditionData</i></b>
<p>
Acquires input data from the frame API, metadata API, disk files
and URL's and performs mathematical transformations upon it using
the datacond API.  The results of these transforms can be output
to disk or to URL's, or sent to the metadata API for ingestion into
the database.
<br>
Data extracted from the frame API is concatenated as possible by
default.  This default behaviour can be modified by using the
-concatenate option with an argument of 0.
<br>
Data products can be sent to other API's by using the
<b><i><a href="actions.html#output">output()</a></i></b> action of the
<a href=#algorithms>-algorithms</a> option.
<br>
Data objects recieved by other API's can often be
inspected by setting the <b>::DEBUG</b> level in the appropriate
API('s) to <b>219</b> (0xdb), the data will then be placed in the
job result area of the LDAS installation.
<p>

Calling convention (all on a single line):
<p>
<b>ldasJob</b>&nbsp;
<b>{&nbsp;</b>
-name&nbsp;<i>"username"</i>&nbsp;
-password&nbsp;<i>"********"</i>&nbsp; 
-email&nbsp;<i>"user@foobar.edu"</i><b>&nbsp;}</b>&nbsp; 
<b>{&nbsp;<i>conditionData</i></b>&nbsp; 
<a href=#inputprotocol>-inputprotocol</a>
&nbsp;<font color="red"> <i>{Tcl list}</i></font>&nbsp;

<a href=#returnprotocol>-returnprotocol</a>
&nbsp;<font color="red"> <i>URL</i></font>&nbsp;

<a href=#outputformat>-outputformat</a>
&nbsp;<font color="red"> <i>data_format</i></font>&nbsp;

<a href=#datacondtarget>-datacondtarget</a>
&nbsp;<font color="red"> <i>{API name}</i></font>&nbsp; 

<a href=#framequery>-framequery</a>
&nbsp;<font color="red"> <i>{Tcl list}</i></font>&nbsp; 

<a href=#responsefunction>-responsefunction</a>
&nbsp;<font color="red"> <i>{Tcl list}</i></font>&nbsp;

<a href=#responsefiles>-responsefiles</a>
&nbsp;<font color="red"> <i>{Tcl list}</i></font>&nbsp;

<a href=#tarball>-tarball</a>
&nbsp;<font color="red"> <i>{http or ftp URL}</i></font>&nbsp;

<a href=#aliases>-aliases</a>
&nbsp;<font color="red"> <i>{Tcl list}</i></font>&nbsp;

<a href=#algorithms>-algorithms</a>
&nbsp;<font color="red"> <i>{Tcl list}</i></font>&nbsp;

<a href=#setsingledc>-setsingledc</a>
&nbsp;<font color="red"> <i>{Boolean}</i></font>&nbsp;
<b>}</b>
<p>
<font color="green">Option Descriptions:</font>
<ul>
<p>
<a name=inputprotocol>
<b>-inputprotocol:</b> &nbsp; &nbsp; 
                   <tt>
                   http ftp file port
                   </tt>
<p>
<ul>
<tt>
<li><b>Optional</b><br>
The argument to the <b>-inputprotocol</b> option conforms
to the usual browser conventions for URI's for determining
the location of the results of the user request.<br>
When the URI is of type <b>http</b> or <b>ftp</b>
the LDAS system will attempt to retrieve the data referred
to by the URI description, and will make a local copy of
it in the result directory assigned to the user command.
On completion or failure of the user command, the local copy
of the input data will be removed.
When the URI is of type <b>port</b>, the system will attempt
to read ilwd binary data from the referenced port.
When the URI is of type <b>file</b>, the data is locally
available, and will be read from the local file system if it
exists and is readable.
The possible formats of the argument are:
<p>
<li>http://host:port/dir/...<p> 
<li>ftp://host:port/dir/...<p>
<li>file:/path/... (note, only one "/")<p>
<li>port:hostname:portnumber<p>
<p><font color="red">
NOTE: Embedded spaces in the argument to the -inputprotocol
option will cause the request to fail.</font>
</tt>
</ul>
<p>
<!--#include virtual="/subst.cgi?opt_returnprotocol.cgi"-->
<!--#include virtual="/subst.cgi?opt_outputformat.cgi"-->
<!--#include virtual="/subst.cgi?opt_framequery.cgi"-->
<!--#include virtual="/subst.cgi?opt_aliases.cgi"-->
<!--#include virtual="/subst.cgi?opt_algorithms.cgi"-->

<a name=responsefunction>
<b>-responsefunction:</b> &nbsp; &nbsp; <tt>{ Full Path to File }</tt>
<p>
<ul>
<font color=red>
Optional: <i>Deprecated in favor of -responsefiles, q.v.</i>
</font><br>
<tt>
See -responsefiles option below.
</tt>
<p>
</ul>

<a name=responsefiles>
<b>-responsefiles:</b> &nbsp; &nbsp; <tt>{ Full Path to File(s) }</tt>
<p>
<ul>
<font color=red>
Optional: <i>When an external ilwd responsefile(s) is/are required,
the full path to the file is specified by this option.  The syntax
allows for some degree of flexibility in the disposition of the
files.  See below.</i>
</font><br>
<tt>
This option is used to specify the location and disposition of
files containing data and/or coefficients which are not provided
by the data as received from the frame API or which cannot be
calculated from the frame derived data by the data-conditioning
API or extracted from the database.<br>
The files referred to by this option can be injected into the data
stream for the job either within the data-conditioning API by the
use of the "push" option, or can be attached to the output of the
data-conditioning API for transmission to the metadata or wrapper
API's by use of the "pass" option.  Use of the "push" option
requires that an additional argument in the form of an "alias" for
the data-conditioning API be provided.<br>
Examples:<br>

<pre>
-responsefiles { 
                file:/MayMDC/al.ilwd,<font color=red>push</font>,al 
                file:/MayMDC/bl.ilwd,push,bl
                file:/MayMDC/am.ilwd,push,am 
                file:/MayMDC/bm.ilwd,push,bm
                file:/MayMDC/ah.ilwd,push,ah 
                file:/MayMDC/bh.ilwd,push,bh
                file:/MayMDC/resp.bin,<font color=red>pass</font>
               }
</pre>
Here the "push" elements are ilwd data files which are used to
populate the values al, bl, am, bm, ah, and bh.  The contents of
the files can then be referenced in the call chain algorithm by
referring to the appropriate variable.<br>
The "pass" element is passed on to the api pointed to by the
-datacondtarget option and is not used in calculations performed by
the data conditioning API.
</tt>
<p>
</ul>
<p>
<!--#include virtual="/subst.cgi?opt_tarball.cgi"-->
<a name=datacondtarget>
<b>-datacondtarget:</b> &nbsp; &nbsp; <tt>{API name}</tt>
<p>
<ul>
<font color=red>
Default: <i>datacond</i>
</font>
<p>
<tt>
The default API for datacond results.
<p>
When the underscore <b>_</b> is used as the protocol argument in an
<a href=http://www.ldas-dev.ligo.caltech.edu/doc/userAPI/html/actions.html#output>output()</a>
action, the value of <b>-datacondtarget</b> will be used as the
protocol for the output.
<p>
In a conditionData command, results are normally written to disk
in plain ilwd format, or returned to the frame API for output as
frames.  If output actions use the <b>_</b> default protocol
specifier, then their output is written to disk in plain ilwd
format or XML, whichever is specified via the <b>-outputformat</b>
option, or as specified by the <b>output()</b> action format
specifier (see the documentation for the output() action).
<p>
<b>NOTE:</b><br>
A straightforward method of getting wrapper formatted ilwd
written to disk is to use the
<a href=http://www.ldas-dev.ligo.caltech.edu/doc/userAPI/html/dataStandAlone.html#datastandalone>dataStandAlone</a>
user command.
</tt>
</ul>
<p>
<a name=examples>
<p><font color="green">Examples of conditionData commands:</font>
<p>
Simple but complete example which reads a simple ilwd text file
from the LDAS public file area as input and returns the daq rate
and the data array multiplied by the daq gain found in that file.
<p>
This example presents a Tcl script which can be run from any
computer with a network connection to submit a user command
to the LDAS system:
<p>
<ul>
<pre>
#!/bin/sh
# use -*-Tcl-*- \
exec tclsh "$0" "$@"

# convenient formatted command definition
set cmd "ldasJob
   { -name foo -password **** -email foo@foobar.edu }
   {
      conditionData
         -inputprotocol {
            file:/ldas_outgoing/jobs/NORMAL2000/data.ilwd
         }   
         -aliases {
            x=channel_0:data;
            m=channel_0:gain;
            rate=channel_0:rate;
         }
         -algorithms { 
            output(rate,,rate,"daq rate from input");
            # this is a comment
            mrate = mul(m,x);
	    output(mrate,,final,"final result");
         }
   }"

# strip comments and compact cmd into user command format
set cmd [ split $cmd "\n" ]
foreach line $cmd {
   set line [ string trim $line ]
   if { [ regexp {^#} $line ] } {
      continue
   }
   lappend tmp $line
}   
set cmd [ join $tmp "\n" ]
regsub -all -- {[\n\s]+} $cmd { } cmd

# connect, issue command, print reply from manager,
# and disconnect
set sid [ socket ldas-dev.ligo.caltech.edu 10001 ]
puts  $sid $cmd
flush $sid
puts [ read $sid ]
close $sid
</pre>
<b><i>Which submits this command to the system:</i></b>
<li>
<b>ldasJob</b>&nbsp;
   <b>{&nbsp;</b>
      -name
         &nbsp;<i>"foo"</i>&nbsp;
      -password
         &nbsp;<i>"****"</i>&nbsp; 
      -email
         &nbsp;<i>"foo@foobar.edu"</i><b>&nbsp;}</b>&nbsp; 
   <b>{&nbsp;<i>conditionData</i></b>&nbsp; 
      -inputprotocol
         &nbsp;<font color="red"><i>file:/ldas_outgoing/jobs/NORMAL2000/data.ilwd</i></font>&nbsp; 
      -aliases&nbsp;{
      &nbsp;<font color="red"><i>x=channel_0:data; m=channel_0:gain; rate=channel_0:rate</i></font>&nbsp;}
      -algorithms&nbsp;{
         &nbsp;<font color="red"><i>output(rate,,rate,"daq rate from input"); r = mul(m,x); output(r,,final,"final result");</i></font>&nbsp;}
   <b>&nbsp;}</b>
<p>
<i><b>Input:</b></i>
<pre>
&lt;?ilwd?&gt;
&lt;ilwd name='trivial data file with one channel' size='1'&gt;
   &lt;ilwd name='channel_0' size='3'&gt;
      &lt;int_2u name='rate'&gt;2048&lt;/int_2u&gt;
      &lt;int_2u name='gain'&gt;2&lt;/int_2u&gt;
      &lt;int_2u name='data' dims='8'&gt;12 12 12 13 13 12 12 11&lt;/int_2u&gt;
   &lt;/ilwd&gt;
&lt;/ilwd&gt;   
</pre>
<p>
<i><b>Result:</b></i>
<pre>
&lt;?ilwd?&gt;
&lt;ilwd size='1'&gt;
   &lt;ilwd size='2'&gt;
      &lt;int_2u name='rate' comment='daq_rate_from_input'&gt;2048&lt;/int_2u&gt;
      &lt;int_2u name='data_x_gain' comment='result of multiplying the data array by the gain' dims='8'&gt;24 24 24 26 26 24 24 22&lt;/int_2u&gt;
   &lt;/ilwd&gt;
&lt;/ilwd&gt;   
</pre>
</ul>
<p>
<a name=setsingledc>
<b>-setsingledc:</b> &nbsp; &nbsp; <tt>{Boolean}</tt>
<p>
<ul>
<tt>
<li><b>Optional, defaults to "0" (<i>false</i>)</b><br>
If this flag is 0 (<i>false</i>), then each frame formatted output from the
data conditioning API will be returned as a seperate frame object.
If this flag is 1 (<i>true</i>), then frame formatted output from the
data conditioning API is combined into a single frame object.
</tt>
</ul>
<p>
<a name=framefilenames>
<b><font color=green>Frame File Naming Convention</font></b>
<p>
When the frame cache consists of a mixed collection of frames in
an inconsistent hierarchy of subdirectories the appropriate frame
file(s) for fulfilling a given request are determined by parsing
the frame file names according to an installation-specific naming
convention.
The LDAS system provides a default naming convention which is described
in detail in the document
<em>
<a href="http://www.ligo.caltech.edu/docs/T/T010150-00.pdf">
Naming Convention for Frame Files Which Are to be Processed by LDAS
</a>.
</em>
<br>
In summary, the required format of frame-file names is
<p>
<ul>
<li> <b><em>S</em>-<em>D</em>-<em>G</em>-<em>T</em>.gwf</b>
</ul>
<p>
where
<p>
<ul>
<li> <b><em>S</em></b> indicates the source of the data eg. <b>H</b> for
Hanford frames, <b>L</b> for Livingston frames, <b>G</b> for GEO frames,
<b>HL</b> for combined Hanford-Livingston data.
<li> <b><em>D</em></b> is a description of the contents of a file. Some typical
values include:
  <ul>
    <li> <b>R</b> full-rate "raw" data
    <li> <b>T</b> second-trend data
    <li> <b>M</b> minute-trend data
    <li> <b>RDS_R_L1</b> Level 1 reduced data
  </ul>
<li> <b><em>G</em></b> is the GPS time (integer sumber of seconds) at the
beginning of the first frame in the file. This is either a 9- or 10-digit
number.
<li> <b><em>T</em></b> is the total time interval from <b><em>G</em></b> to the
time-stamp of the last sample contained in the frame file, rounded up to
the nearest whole second.
</ul>
<p>
<b>Examples:</b>
<p>
<ul>
<li> <b>H-R-693960000-16.gwf</b> <br>
16 seconds of raw data from Hanford.
<li> <b>L-T-693960000-60.gwf</b> <br> 
1 minute of second-trend data from Livingston.
<li> <b>H-RDS_L1-693960000-16.gwf</b> <br>
16 seconds of Level 1 reduced data from Hanford.
</ul>

<center>
<h2>
<IMG SRC="/doc/gifs/arrow_up.gif" ALT="arrow_up.gif">
<a href="#top">Return to Top</a>
<IMG SRC="/doc/gifs/arrow_up.gif" ALT="arrow_up.gif">
</h2>
</center>

<!-- End of conditionData --> 

</BODY>

<!-- VI POWERED! -->

</HTML>
