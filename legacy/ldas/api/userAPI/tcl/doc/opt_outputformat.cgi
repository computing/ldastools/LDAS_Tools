<a name=outputformat>
<b>-outputformat:</b> &nbsp; &nbsp; <tt>frame ilwd LIGO_LW</tt>
<p>
<ul>
<font color=red>
Default: <i><b>{ilwd binary}</b></i>
</font>
<p>
<tt>
The argument of the <b>-outputformat</b> option is the data type to
use in formatting the result of the user request.<br>
The possible output formats which the system can produce include:
<p>
<ul>
 <li><b>frame</b>
 <li><b>{ilwd ascii}</b>
 <li><b>{ilwd binary}</b>
 <li><b>{frame ilwd ascii}</b>
  <ul>
   <li>for concatFrameData <b>only</b>, causes a fully frame
       formatted ilwd object to be created and written to disk.
  </ul>     
 <li><b>{frame ilwd binary}</b>
  <ul>
   <li>for concatFrameData <b>only</b>, causes a fully frame
       formatted ilwd object to be created and written to disk.
  </ul>     
 <li><b>LIGO_LW</b> (an XML format)
</ul>
</tt>
</ul>
<p>
