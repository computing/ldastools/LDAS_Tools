<!-- -*- mode: html; -*- -->
<a name=algorithms>
<b>-algorithms:</b>&nbsp;&nbsp;<tt>{ <a href=actions.html>action syntax</a> }</tt>
<p>
<ul>
<font color=red>
Required
</font><br>
<tt>
The algorithms option is constructed from a series of mathematical
"actions" which are defined within the data conditioning API.
These "actions" are entered in the form of a semi-colon delimited
list.
The "action" calls are used to develop complex algorithms by
"chaining" multiple actions which are evaluated from left to right.
The results of actions can be assigned to variables or "printed"
out using a special action: <b><i><a href="actions.html#output">output()</a></i></b>, provided for that
purpose.
</tt>
</ul>
<p>
