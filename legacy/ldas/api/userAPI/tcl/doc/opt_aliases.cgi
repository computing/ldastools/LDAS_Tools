<!-- -*- mode: html; -*- -->
<!--REQUIRED_OPTIONAL=Optional-->
<a name=aliases>
<b>-aliases:</b>&nbsp;&nbsp;<tt>{ <i>&lt;alias0&gt; = &lt;regex0&gt;;
&lt;alias1&gt; = &lt;regex1&gt;</i> ... }</tt>
<p>
<ul>
<li><b>$REQUIRED_OPTIONAL</b><br>
Provides assignment of aliases to variables ingested by the
dataconditioning API.
<p>
This option allows aliases to be specified for variables
ingested by the dataconditioning API. The argument to <b>-aliases</b>
consists of a semicolon-delimited list whose elements are of the form
<i>&lt;alias&gt; = &lt;regex&gt;</i>, where <i>&lt;alias&gt;</i>
is an alphanumeric string beginning with an alphabetic character,
and <i>&lt;regex&gt;</i> is a regular expression (under Unix, type
<tt>'man 7 regex'</tt> for a description of regular expressions).
Each regular expression must match the name
of <i>exactly one</i> variable ingested by the dataconditioning API,
otherwise an error is reported and the job will not proceed.
Note that a regular expression is deemed to match a variable name if
it matches any substring of it.  For example, <b>FOO</b> matches
<b>FOO</b>, <b>123-FOO</b> and <b>FOOBAR</b>.
<p>
Aliases may be used in place of variable names in the body of the
<a href=#algorithms>-algorithms</a> option. Before the commands
in the body of <a href=#algorithms>-algorithms</a> are executed, each
occurrence of an alias is replaced by the unique variable name which
matched the regular expression on the right-hand side of the assignment.
<p>
<i>Implicit aliases</i>
<p>
In addition to user-defined aliases, the implicit aliases <tt>_ch0</tt>,
<tt>_ch1</tt>, ..., <tt>_chN</tt> are assigned to variables in the order
in which they are ingested by the dataconditioning API.
Making use of the implicit aliases requires knowlege
of the order of ingestion of the objects, which in most cases is
undefined and may change even on subsequent runs of identical jobs.
However, implicit aliases can be used in the
<a href=#algorithms>-algorithms</a> option in the same way as
user-defined aliases. See the <a href=#examples>sample user commands</a>
and the example below.
<p>
<i>Example:</i>
<p>
ADC channel data read from frame files is ingested into the
data conditioning API with a variable name based on the channel name
and its start-time, eg. data from channel <b><tt>H2:LSC-AS_Q</tt></b>
starting at GPS time <tt>693960000s 0ns</tt> will be ingested with the
variable name "<tt>H2\\:LSC-AS_Q::AdcData:693960000:0:Frame</tt>"
(note that embedded colons must be escaped with a backslash).
In the following user command, all occurrences of "<tt>gw</tt>" in
<a href=#algorithms>-algorithms</a> will be replaced by
"<tt>H2\\:LSC-AS_Q::AdcData:693960000:0:Frame</tt>". If data
from channel <b><tt>L1:LSC-AS_Q</tt></b> was also ingested,
an error would occur
because the regular expression on the right-hand side of "gw = LSC-AS_Q"
would match more than one variable name:
<b>
<pre>
conditionData
  -framequery { R H {} 693960000-693960001 Adc(H2:LSC-AS_Q) }
  -aliases { gw = LSC-AS_Q; }
  -algorithms {
    gw2 = resample(gw, 1, 8);
    output(gw2,, gw2.ilwd, gw2, Downsampled gw channel);
  }
  -datacondtarget datacond
</pre>
</b>
<font color=red>
<b>Note</b></font> that when using <b><tt>output()</tt></b> to create
<b><i>frame</i></b> formatted results, that the <b>name</b>
field should not contain the backslashes as used in the datacond
API variable names. Datacond API variable names are derived from
frame channel names, but they are not identical! Frame channel
names do not have backslashes protecting the colon after the
interferometer i.d.
<p>
<ul>
Example:
<p>
<b><tt>L1:LSC-AS_Q</tt></b> is the name of a frame channel.
<p>
<b><tt>L1\:LSC-AS_Q::AdcData:693960000:0:Frame</tt></b> is the name
of a datacond variable.
</ul>
</ul>
<p>
