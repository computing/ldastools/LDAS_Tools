<!-- -*- mode: html; -*- -->
<!--REQUIRED_OPTIONAL=Required-->
<a name=framequery>
<b>-framequery:</b> &nbsp; &nbsp; <tt>{R H {} 666666666-666666669 Adc(1,2,5-15)}</tt>
<p>
<ul>
<font color=red>
The <b><i>-framequery</i></b> option is REQUIRED for getFrameData,
getFrameElements, and concatFrameData user commands, but is OPTIONAL
for conditionData, dataStandAlone, and dataPipeline user commands.
</font><br>
The <b><i>-framequery</i></b> argument is a complex list of frame
API query atoms.
The query atoms consist of unique identifying strings (which are
<i>not</i> case sensitive) with indices or channel names grouped
in parentheses.  The query atom strings consist of the unique
parts of the accessor function names from the frame API c++ code.
(See: <a href=../../frameAPI/html/tcl/tclindex.html>frameAPI.so</a>)
<p>
The <b><i>-framequery</i></b> syntax now supports a complex
associative format which allows the simultaneous retrieval
of unrelated data elements from multiple sources.<br>
In the simplest case, a single frame repository containing
frames from more than one instrument can be queried to retrieve
a common time range from each specified interferometer:
<p>
<ul>
<font color=brown>
<li>
<b>-framequery { {} {H L} {} 600000000-600000007 Adc(0)}</b>
</font><br>
This query will return the data for Adc 0 from both Hanford and
Livingston over the gps time period 600000000 to 600000007.
<font color=brown>
<li>
<b>-framequery { { {} H {} 600000000-600000007 Adc(H2:LSC-AS_Q)}{ {} L {} 600000000-600000007 Adc(L1:LSC-AS_Q)}}</b>
</font><br>
This query will return the data from both Hanford and Livingston
over the gps time range 600000000 to 600000007 for the H2 and L1
<i>so called gravitational wave channels.</i>
</ul>
<p>
The five fields of the complex framequery are:
<ol>
<li><a href=#fq1>frame type</a>
<li><a href=#fq2>interferometers</a>
<li><a href=#fq3>frames</a>
<li><a href=#fq4>times</a>
<li><a href=#fq5>channels (and/or structures for which accessors exist)</a>
</ol>
Any field which is not used must contain a pair of braces "{}"
as a placeholder so that all sublists of the <b><i>-framequery</i></b>
always consist of five elements.
<p>
<font color=red>
<b>The framequery option supports the slicing syntax Adc(channel!start!range[type]!)
or Proc(channel!start!range[type]!):</b><br>
</font>
The "slicing" syntax allows a subset of an Adc or Proc data channel
to be obtained. Slicing is performed by appending two floating-point
numeric arguments, the <b>start</b> and <b>range</b>, to the channel
specification (the <a href=#fq5>fifth</a> field of the framequery), 
delimited by <b>!'s</b>. The <b>start</b> is the absolute
starting position of the slice along the x-axis of the channel,
and the <b>range</b> is the extent of the slice.<br>
Valid [type] specifications are <b>TIME</b>, <b>FREQ</b>, and
<b>TIMEFREQ</b>. When no type specifier is given, time is assumed,
and this may generate errors if frequency series data is found.<br>
The units of the both numbers are taken from the <b>unitX</b> field
for the specified channel in the frame file when the data is operated
on by an another API.
This is usually seconds for time-series data and Hertz for
frequency-series data.
<p>
Note that the extent of the x-axis of a channel is not related
to the GPS time of the frame. The first value on the x-axis is given by
the <b>startX</b> field of the channel, and the extent is
given by the number of samples in the channel times the <b>dx</b> field
of the channel. While <b>startX</b> may have any value, it is usually 0,
although there are cases where it could be negative, such as for a
2-sided power spectrum.
<p>
The sliced data will contain all samples whose
x-coordinate satisfy <b>start</b> &lt;= x &lt; <b>start + range</b>.
An error will be generated if the start of the slice is less than
<b>startX</b>, or if the slice extends past the end of the channel.
<p>
<b>
Examples of slicing:
</b>
<p>
Suppose we are accessing a channel from frame H-R-600000000.gwf:
<p>
<b>Adc(H2:LSC_AS-Q!0.3!0.4TIME!)</b>
<p>
The <b>!</b> syntax determines a start-time and range within the
channel (since <b>startX</b> is zero for time-series data,
the start-time may be interpreted as an offset from the GPS start-time
of the framequery).
The syntax requires that the channel name be followed
by a <b>!</b>, followed immediately by a valid time offset into
the data array for the channel, followed by another <b>!</b>,
followed immediately by a range, followed by a final <b>!</b>.
<p>
This example will return a 0.4 second long slice of the channel data
from GPS time 600000000.300000000 through 600000000.700000000.<br>
The ILWD object containing this data slice will have the correct
calculated start-time and times-span for this slice of data, as well as
the correct number of data points in the data array.
<p>
Suppose we have a Proc frame with a Proc structure containing
frequency spectrum data with Hertz as the x-axis units. 
We can request a specific frequency band by specifying a
<b><i>start-frequency</i></b> and a <b><i>frequency range</i></b>:
<p>
<b>Proc(0!1024!1024FREQ!)</b>
<p>
This will return a Proc structure containing the data at all frequency
bins greater than or equal to 1024 Hz and less than 2048 Hz, that is,
all frequency data in the semi-open interval [0, 1024) Hz.
Any metadata associated
with the original object will be passed though unmodified.
<p>
<font color=red>NOTE on frequency slicing:</font><br>
The directive <b><tt>Adc(CAL-CAV_GAIN!0!2048.0001FREQ!)</tt></b>
is interpreted by LDAS as
<b><i>all frequencies &gt;= 0 and &lt; 2048.0001</i></b>.<br>
Note the strict inequality for the upper limit. The frequency
of a bin is determined by:
<pre>
     f_k = startX + k*df, k = 0, 1, ..., N-1
</pre>
so you just need to make sure your request is consistent with this
scheme. The reason some users are adding 0.0001 is becuase they have
f-sequences that have a bin that they want at EXACTLY 2048 Hz (say),
so they need to specify an upper limit above this ie. the upper
limit must be strictly greater than 2048 and less than or equal to
2048 + df Hz. If adding 0.0001 works for you fine, but that's obviously
not the only choice.

This might seem a strange convention but the reason it's chosen is:
<p>
<ol>
<li> to be consistent with the way we specify time intervals, which are 
also given as semi-open intervals.
<p>
<li>so that it's easy to pull out sub-sequences of frequency series
and/or concatenate them in a non-overlapping way ie:<p>
<ul>
<tt>[ ... ) [ ... ) [ ... )</tt>
</ul>
</ol>
<p>
<font color=red>
The framequery option support for downsampling syntax
Adc(channel!resample!q!) or Proc(channel!resample!q!):<br>
</font>
Suppose we are asking for gravity wave channel data over an
hour, but want to downsample the data by a factor of 8
before it is sent to the dataconditioning API:
<p>
<b>Adc(H2:LSC_AS-Q!resample!8!)</b>
<p>
In the case of resampling, the first field after the channel name contains the
literal string "resample", and the second field contains the downsampling
factor <b>q</b>. (see the 
<a href=conditionData.html#resample>resampling algorithm</a>
documentation.)
<p>
<font color=brown>
It is intended that resampling be applied after data has been time
sliced based on the time range part of the framequery option.<br>
As of the 0.2.0 release of LDAS, this has been imperfectly implemented,
and it is important that users wishing to make effective use of the
resampling syntax provide feedback to the developers via the problem
tracking system:<p>
<a href=http://www.ldas-sw.ligo.caltech.edu/ProblemTracking.html>LDAS Problem Tracking System</a>
</font>
<p>
<font color=red>
Note that the only supported resampling factors are 2, 4, 8,
and 16.
</font>
<b>The -framequery option by-the-numbers:</b>
</font>
<p>
<a name=fq1>
The <b>first</b> element of the complex -framequery option is a
list of the <b>type</b> of the frames which should be retrieved.
The <b>type</b> refers to the frame attribute referenced by the second
field in the proposed frame spec.  This filed defaults internall to
<b>R</b>, or "Raw".<br>
Another example of a frame type would be mT, or "Minute Trend".<br>
 separate output container will be created for each interferometer
specified.
<p>
<a name=fq2>
The <b>second</b> element of the complex -framequery option is a
list of the interferomenters which produced the data that is wanted.
A separate output container will be created for each interferometer
specified.
<p>
<a name=fq3>
The <b>third</b> element of the complex -framequery option is a Tcl
list of frame file names or URL's.<br>
This option is generally only used when a single specific input frame
file (or a single file from, say each of two interferometers) is
needed, and is provided primarily for the application of data
conditioning or pipelining of test data in the form of frame files.<br>
This option will become the mecahnism for the reading of calibration
or other process data from frame files at a later time.
<p>
<a name=fq4>
The <b>fourth</b> element of the -framequery option, <b>times</b>,
is a Tcl list of GPS timestamps and ranges. The <b>times</b> argument
must have the form <b><i>a-b</i></b> where <b><i>a</i></b> and
<b><i>b</i></b> are valid GPS times in whole numbers of seconds.
Due to historical precedent, this range should be interpreted as a
request for data including the second starting at <b><i>a</i></b>
until the <i>end</i> of the second starting at <b><i>b</i></b>,
thus the actual interval of time being requested is <b><i>[a, b+1)</i></b>.
For example, <b>-times 666666666-666666681</b> represents the equivalent of
specifying the names of 16 1-second frame files, and represents the 17
seconds of data from time 666666666 up to but not including 666666682.
<br>
The syntax supports a gap filling flag: 
<b>666666666-666666681:allow_gaps</b> which the LDAS system recognises
as an indication that it should allow missing frame files, and to account
for them in the data conditioning stage of the user command by filling
in missing data points as specified by the 
<a href=conditionData.html#fillgaps>fillgaps</a> action in the
<a href=#algorithms>algorithms</a> option.<br>
<font color=red>
Note that the <a href=#allowgaps>-allowgaps</a> option to the
dataPipeline user command overrides all the syntactical subtleties
implied here and will cause a single data segment spanning all
specified time ranges to be created.
<p>
Example:
<p><b>
-framequery { {} H {} {666666666-666666669:allow_gaps 666667660-666667665} full(0)}
</b><p>
Will, with the <a href=#allowgaps><b>-allowgaps</b></a>
option specified, ultimately result in a single frame of 1000
seconds duration.
</font>
<p>
<font color=brown>
Note that the <b>times</b> element of the <b>-framequery</b>
option returns the data spanning the range of time from the
top of the starting second to the bottom of the ending second.
</font>
<p>
<a name=fq5>
The <b>fifth</b> element of the -framequery option supports a
shorthand notation for frame structure accessor methods exposed
to the Tcl layer.  The most commonly used methods are the <b>Adc()</b>
and <b>Proc()</b> accessors, which retrieves Adc (time serialised data)
or Proc (time, freq, or time-freq domain) channel structures from frames.The argument to this accessor method can be an integer (referring to
the <b><i>ith</i></b> channel) OR the specific name of a channel:<p>
<pre>
     Adc(12)
     Adc(H2:LSC-AS_Q)
</pre>
An argument of <b>&quot;full()&quot;</b> as an item in the
framequery option list will result in either a <b>copy</b> of the
frame if the return format is specified as &quot;frame&quot;,
a <b>full text dump</b> of the frame if the return format is
specified as &quot;ilwd&quot;, and a <b>full XML dump</b> if the
format is specified as LIGO_LW.
<p>
