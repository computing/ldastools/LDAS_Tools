#!/usr/bin/env perl
# -*- mode: perl; -*-
my($x);

sub readfile($)
{
    my($filename) = @_;
    my($pos) = 0;

    open HTML, $filename;
    while(<HTML>)
    {
	chop;
	/^\s*\<\!--\#include virtual=\"\/?(.*)\?(.*)\"--\>\s*$/ and do {
	    system("/usr/bin/env QUERY_STRING='$2' perl $ENV{SRCDIR}/$1");
	    next;
	};
	print "$_\n";
    }
    close HTML;
}

for ( $x = 0; $x <= $#ARGV; $x++ )
{
    my($old_fh) = select(STDOUT);
    $| = 1; # Make unbuffered
    select($old_fh);
    readfile($ARGV[$x]);
}
