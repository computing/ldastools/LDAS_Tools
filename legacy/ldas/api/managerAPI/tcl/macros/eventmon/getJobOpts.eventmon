## ********************************************************
##
## Name: getJobOpts.eventmon
##
## Description:
## This is a generic macro for eventmonAPI to receive the
## job options. 
## This is a secondary command ( next in the sequence of
## of a user command). 
##
##
## Comments:
## A. API should send response indicating cmd was successful
##  or any errors that have occurred.
## an after here instead of vwait does not work
## since mainline afters are processed ahead of background after
## ******************************************************** 

## ******************************************************** 
##
## Name: getJobOpts
##
## Description:
## entry point to macro
## process products if available, if not, returns but
## set up a trace to wake up when products are available
## or when job is timed out and all vars for jobid are deleted.
## 
## Parameters:
## None.
##
## Usage:
##
## Comments:
## cid trace is temporarily disabled
## clean up if this is a bypass

proc getJobOpts { jobid } {
     set seqpt {}
	set cid [ uplevel { set cid } ]
	set ::${jobid}(cid) $cid
	
	;## terminate pipeline if no dynlib
	set dll [ set ::${jobid}(-dynlib) ]
	set subj ""
    set test [ set ::${jobid}(-datacondtarget) ]	
    if 	{ ! [ regexp {^(mpi|wrapper)$} $test ] || 
			[ string length $dll ] < 3 } {
		set rc 0 
		foreach { rc msg } [ eventmon::killJob $jobid $rc ] { break }
		unset ::${jobid} 
		set ::jobid {} 
		set ::$cid [ list 0 0 0 ]
		reattach $jobid $cid
		return
    }
	
	;## if there is an exception, abort Job
	if	{ [ info exist ::${jobid}(exception) ] && [ llength [ set ::${jobid}(exception) ]] } {
		if	{ ! [ regexp {(?n)^\s*$} [ join ::${jobid}(exception) ] ] } {
			set err "Thread errors: [ join [ set ::${jobid}(exception) ] ": " ]"
			addLogEntry $err red
			set rc 3
        	foreach { rc msg }  [ eventmon::killJob $jobid $rc ] { break }
			unset ::$jobid
			set ::jobid {}
			set msg [ list 3 "${jobid}: [ myName ]:$err" error! ] 
			set ::$cid $msg
			reattach $jobid $cid
			return
		}
    }
	
	if  { ! [ info exist ::${jobid}(product) ] } {
        trace variable ::${jobid}(product) w "processJobProducts $jobid $cid"  
        set afterid  [ after $::MAXMACROTIME \
            [ list eventmon::killJob $jobid 3 "macro timed out" ] ] 
        array set ::$jobid [ list afterId $afterid ]
        addLogEntry "after id is [set ::${jobid}(afterId)]"
		return 
    }
    
	set result [ handleReturnCode $jobid ]
    ;## unset here if job is processed
    catch { unset ::$jobid }
    catch { set ::jobid {} }
	set ::$cid $result
	reattach $jobid $cid
}

set msg [ getJobOpts $jobid ]
return $msg

