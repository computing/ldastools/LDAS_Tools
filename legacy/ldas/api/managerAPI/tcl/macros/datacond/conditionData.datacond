## ********************************************************
##
## Name: conditionData.datacond
##
## Description:
##
## Parameters:
## returnformat - "frame" for binary, "ilwd" for text.
## returnprotocol - ftp, http, mail, file or port, as available.
## times - a list of gps times, or "now" or "next".
## ifos - a list of interferometers which supply conforming data.
## dataquery - see usage section.
##
## Usage:
## 
## Comments:
##
## ******************************************************** 

## ******************************************************** 
## Initialise global variables from the user-supplied and
## default options.  These are not all required.  It is
## not a requirement to initialise them at the global level.
##
## ******************************************************** 

proc conditionPipelineData { jobid } {

	 set cid [ uplevel { set cid } ]
     set ::${jobid}(cid) $cid

     set algos   [ set ::${jobid}(-algorithms)     ]
     set targ    [ set ::${jobid}(-datacondtarget) ]
     set rp      [ set ::${jobid}(-returnprotocol) ]
     set ip      [ set ::${jobid}(-inputprotocol)  ]
     
     ;## PR #2185 -frametarget frame timeout debacle
     set frametarget [ set ::${jobid}(-frametarget) ]
     if { [ regexp -nocase {frame} $frametarget ] } {
        if { ! [ info exists ::{$jobid}_DATABUCKET ] } {
           if { ! [ string length [ lindex $ip 0 ] ] } {
              set msg "no input data for datacond API. "
              append msg "frame data written directly to disk."
              set ::$cid [ list 4 $msg results ]
              reattach $jobid $cid
              return
           }
        }
     }
     
     if { ! [ string length $rp ] } {
        set rp http://${jobid}.ilwd
        set ::${jobid}(-returnprotocol) $rp
     }
     
     if { [ info exists   ::${jobid}(-dbquery) ] } {
        set dbquery [ set ::${jobid}(-dbquery) ]
     } else {
        set dbquery [ list ]
     }

     if { [ info exists ::${jobid}(-dbqualitychannel) ] } {
        set dbqchan [ set ::${jobid}(-dbqualitychannel) ]
     } else {
        set dbqchan [ list ]
     }
     
     if { [ info exists ::${jobid}(-dbspectrum) ] } {
        set dbspect [ set ::${jobid}(-dbspectrum) ]
     } else {
        set dbspect [ list ]
     }

     if { [ info exists ::${jobid}(-dbntuple) ] } {
        set dbtuple [ set ::${jobid}(-dbntuple) ]
     } else {
        set dbtuple [ list ]
     }

     if { ! [ string length $algos   ] && \
          ! [ string length $dbquery ] && \
          ! [ string length $dbspect ] && \
          ! [ string length $dbtuple ] && \
          ! [ string length $dbqchan ] } {
        set ::$cid [ list 0 0 0 ]
        reattach $jobid $cid
        return 
     }
     
     if { ! [ string length $targ ] } {
        set ::${jobid}(-datacondtarget) datacond
     }
     
     set of   [ set ::${jobid}(-outputformat) ]
     set targ [ set ::${jobid}(-datacondtarget) ]
     
     if { [ regexp -nocase {frame} $of ] && \
          [ string equal datacond $targ ] } {
        set ::${jobid}(-datacondtarget) frame
     }
     
     ;## process request
     
     if { [ catch {
        dc::run $jobid 
      } err ] } {
      	 set ::$cid [ list 3 $err error! ]
         reattach $jobid $cid
      }
}
## ********************************************************

;#barecode
conditionPipelineData $jobid
