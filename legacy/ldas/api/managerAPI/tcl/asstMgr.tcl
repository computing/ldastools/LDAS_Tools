## ******************************************************** 
##
## Name: createAssistant 
##
## Description:
## Namespace model for assistant manager creation. This
## procedure generates a namespace named with a
## command key from the manager command queue.
##
## The newly created namespace has all the functions
## required by an assistant manager, and inherits all
## of the functions of the calling namespace, which
## includes the genericAPI.tcl, but does this without having
## to duplicate an instance of the calling namespace,
## as a spawned interpreter would have to do.
##
## Usage:
##
## Comments:
## This option was chosen in preference to the slave
## interpreter model because it is far easier on system
## resources without compromising any features.
##
## Note that the code inside the procs is not evaluated
## until the generated proc is called.
##
## When the namespace has completed it's task it is
## deleted, and all of it's resources are freed.
##
## during the time that the namespace has real work to do,
## it has control over the manager process, therefore all
## socket communications are handled so as to prevent their
## blocking the event loop.
## Note also that the body of this proc does not need to
## be evaluated uplevel because it is in it's own
## namespace.
##

;#barecode
package provide asstMgr 1.0
;#end

proc createAssistant { { name "" } { cmdname "" } { cmd "" } } {
     
     if { ! [ info exists ::assistant_queue ] } {
        set ::assistant_queue [ list ]
     }
     
     set now [ clock clicks -milliseconds ]
     
     if { ! [ string length $name ] } {
        foreach { name cmdname cmd } \
           [ lindex $::assistant_queue 0 ] { break }     
        if { ! [ string length $name ] } {
           set ::assistant_queue [ lreplace $::assistant_queue 0 0 ]
           return {}
        }
     } else {
        mgr::dropPrettyCommand $name $cmd
        lappend ::assistant_queue [ list $name $cmdname $cmd ]
        if { ! [ info exists ::queuetime($name) ] } {
           set ::queuetime($name) $now
        }
        return {}
     }
     
     if { [ regexp ::${name} [ namespace children ] ] } {
        addLogEntry "Namespace \"$name\" already exists." red
        return {}
     }
     
     ;## new throttling code
     ;## there is usually a bootstrapLock on the manager
     ;## so we switch on MORE THAN ONE bootstrap lock.
     set pipelinethrottle [ mgr::throttlePipelines $cmd ]
     if { $::mgr::activejobs >= $::NUMBER_OF_ASSISTANT_MANAGERS || \
          [ llength [ info vars ::__bootstraplock_* ] ] > 1    || \
          $pipelinethrottle } {
        if { ! [ info exists ::__t::S$name ] } {
           ::__t::start $name
        }
        
        ;## if things get stuck in some API, try promoting one job,
        ;## but not unless there really is more than one job!
        mgr::promoteJobs $pipelinethrottle
        return {}
     } else {
        set ::assistant_queue [ lreplace $::assistant_queue 0 0 ]
        if { [ info exists ::__t::S$name ] } {
           set qt [ ::__t::mark $name ]
           addLogEntry "Throttled $name for $qt secs." green
           unset ::__t::S$name
        } else {
           set qt 0.00
        }
        
        if { [ info exists ::queuetime($name) ] } {
           set Qt [ expr { $now - $::queuetime($name) } ]
           set qt [ expr { $qt + ($Qt / 1000.0) } ]
           unset ::queuetime($name)
        }
        
        incr ::mgr::activejobs
     }
     
     ;## private variables
     namespace eval $name {
        set name [ namespace tail [ namespace current ] ]
        set cmd         null
        set meta        {}
        set api         manager
        set sid         {}
        set logname     {}
        set data        {}
        set usermessage {}
        set starttime(gps) [ gpsTime ]
     }
     
     ;## did the user specify a tarball?
     set tar_rx {\s+-tarball\s+(\S{6,})\s+}
     if { [ regexp -nocase -- $tar_rx $cmd -> tarball ] } {
        set ::${name}::tarball $tarball   
     }

     ;## are we using a persistent socket?
     set ps_rx {\s+-persistentsocket\s+(\S+)\s+}
     if { [ regexp -nocase -- $ps_rx $::QUEUE($name,cmd) -> ps ] } {
        set ::${name}::persistentsocket $ps
     }
     
     set ::${name}::starttime(job)     $now
     set ::${name}::starttime(manager) $now
     set ::${name}::usrcmdname         $cmdname
     set ::${name}::usrcommand         $cmd
     set ::${name}::queuetime          $qt
	 set ::${name}::userSubject	 	   ""
	 
	 if	{ [ catch {
	 	array set tmp [ lrange $cmd 1 end ]
	 	if	{ [ info exist tmp(-subject) ] } {
	 		set ::${name}::userSubject     [ set tmp(-subject) ]
	 	}
	} err ] }  {
		addLogEntry "err $err" red
		return -code error $err
	}
;#end

## ******************************************************** 
##
## Name: ${name}::getcmd 
##
## Description:
## Reads the command off the manager's command queue that
## is keyed with the name of the namespace.
##
## Usage:
##        Called internally by ${name}::seq.
##
## Comments:
## Tcl exception thrown when the command returned is less
## than 4 characters in length.

proc ${name}::getcmd {  } {
     if { [ catch {
        set cmd {}
        set name [ namespace tail [ namespace current ] ]
        set jobid $name
        set cmd $::QUEUE($jobid,cmd)
        
        if { [ string length $cmd ] < 4 } {
           set msg    "ABORT! No command received."
           addLogEntry $msg red
        } 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::getsid 
##
## Description:
## Opens a connection to an API, returning the socket i.d.
## (a string of the form sockNN) of the channel opened.
##
## Usage:
##          set sid [ ${name}::getsid $API ]
##
## Comments:
## Connects to the operator socket of the named API by
## default.

proc ${name}::getsid { { api "" } { service "operator" } } {
     set name [ namespace tail [ namespace current ] ]
     set jobid $name
     ;## how we issue commands directly to the manager
     if { [ string equal $api manager ] } {
        set service emergency
     }   
     
     if { [ info exists ::${name}::sid ] } {
        set sid [ lindex [ set ::${name}::sid ] 1 ]
        
        catch { ::unset ::assistant_socket($name) }
        
        if { [ catch { ::close $sid } err ] } {
           set msg "error closing channel $sid: '$err'"
        } else {
           set msg "closed channel $sid"
        }
        
        if { [ info exists ::DEBUG_ASSISTANT_SOCKETS ] && \
             [ string equal 1 $::DEBUG_ASSISTANT_SOCKETS ] } {
           addLogEntry $msg blue
        }
        
        set ::${name}::sid [ list ]
     }

     if { [ catch {
        set sid [ sock::open $api $service ]
        #set sid [ mgr::connect2API $api $service $::API_SOCKET_TIMEOUT_MS ]
        set ::${name}::sid [ list $api $sid ]
     } err ] } {
        if { [ catch {
           set msg "$err, trying to restart $api API now..."
           addLogEntry $msg red
           set ::${name}::api NULL
           mgr::bootstrapAPI $api
           set ::${name}::api $api
           if { [ info exists ::__bootstraplock_$api ] } {
              set sid booting
           } else {
              set sid [ sock::open $api $service ]
              #set sid [ mgr::connect2API $api $service $::API_SOCKET_TIMEOUT_MS ]
           }
           set ::${name}::sid [ list $api $sid ]
        } err ] } {
           set msg    "Could not restart $api API "
           append msg "($err) ABORTING!"
           set msg2 "LDAS $api API is unavailable at this time."
           set ::${name}::api $api
           set ::${name}::errorapi $api
           set ::${name}::usermessage $msg2
           return -code error $msg
        }
        set msg "$api API restarted successfully. $name continuing."
        addLogEntry $msg blue
     }
     
     set sid [ lindex [ set ::${name}::sid ] 1 ]
     
     if { [ info exists ::DEBUG_ASSISTANT_SOCKETS ] && \
          [ string equal 1 $::DEBUG_ASSISTANT_SOCKETS ] } {
        if { ! [ string equal booting $sid ] } {
           addLogEntry "opened channel $sid to $api API" green      
        }
     }
     if { ! [ string equal booting $sid ] } {
        fconfigure $sid -blocking off 
     }
     return $sid   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: testBgCmdNamespace
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc testBgCmdNamespace { name api cmd } {
     
     if { [ catch {
        regexp {\d+} $name job
        set jobid $::RUNCODE$job
        if { [ llength [ namespace children :: $name ] ] } {
           ::${name}::bgcmd $api $cmd
        }
     } err ] } {
        addLogEntry $err red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::bgcmd 
##
## Description:
## This is the procedure which allows the manager to
## return.
##
## It is also the place where all of the timing data
## is accumulated.
##
## Usage:
##           Called by ${name}::seq
##
## Comments:
##

proc ${name}::bgcmd { api { cmd "" } } {
     set name [ namespace tail [ namespace current ] ]
     set jobid $name
     if { [ catch {
        if { [ llength [ info vars ::__bootstraplock_* ] ] > 1 } {
           after 1000 [ list testBgCmdNamespace $name $api $cmd ]
           return {}
        }
        
        if { [ catch {
           set sid [ ${name}::getsid $api ]
        } err ] } {   
           return {}
        }
        if { [ llength [ info vars ::__bootstraplock_* ] ] > 1 } {
           after 1000 [ list testBgCmdNamespace $name $api $cmd ]
           return {}
        }
        
        if { [ string equal manager $api ] } {
           puts $sid "$::MGRKEY \{$name $cmd\}"
        } else {
           puts $sid "$::MGRKEY $name \{$cmd\}"
        }
        
        ;## update timing statistics info
        ::${name}::timeStats
        set ::${name}::starttime($api) [ clock clicks -milliseconds ]
        
        ;## new cmd::receive style call
        if { [ regexp {^sock\d+$} $sid ] } {
           fileevent $sid readable \
              [ list cmd::receive $sid assistant_socket($name) "${name}::sockhandler" ]
        } else {
           return -code error "bizarre socket i.d. received: '$sid'"
        }
        
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"        
        }
     }
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::timeStats
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc ${name}::timeStats { } {
     
     if { [ catch {
        set name [ namespace tail [ namespace current ] ]
        set jobid $name

        ;## update the delta t for the previous api visited
        ;## by this user command.
        foreach api [ array names ::${name}::starttime ] {
           if { [ string equal job $api ] } { continue }
           if { [ string equal gps $api ] } { continue }
           set now [ clock clicks -milliseconds ]
           set then [ set ::${name}::starttime($api) ]
           unset ::${name}::starttime($api)
           set dt [ expr { $now - $then } ]
           if { [ info exists ::${name}::deltat($api) ] } {
              set _dt [ set ::${name}::deltat($api) ]
              set ::${name}::deltat($api) [ expr { $dt + $_dt } ]
           } else {
             set ::${name}::deltat($api) $dt
           }
        }
 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::sockhandler
##
## Description:
## Asynchronous handler for the socket owned by this
## assistant manager.
##
## Reads the socket and then attempts to parse the data
## into a list of three elements:
## 1. retval - a numerical error value
## 2. msg - a helpful message from the remote api
## 3. subject - a subject to be used in e-mail to the user
## ;#ol
##
## Goes to great lengths not to fail and return garbage to
## the user!
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc ${name}::sockhandler { args } {

     if { [ catch { 
        set retval  [ list ]
        set msg     [ list ]
        set subject [ list ]
        set name [ namespace tail [ namespace current ] ]
        set jobid $name
      
        ;## how to handle a trace on an array element
        set var   [ lindex $args 0 ]
        set index [ lindex $args 1 ]
        set sid [ lindex [ set ::${name}::sid ] 1 ]
        set rawdata [ set ::${var}($index) ]
        set origdata $rawdata
        
        ;## this should fix all "list element in braces/quotes..."
        ;## errors.
        regsub -all -- {([\}\"])([\:\.\,\?\'\!\)])} $rawdata {\1 \2} rawdata
        ;## we can step over a stray fileevent trigger, a strange
        ;## occurrence which happens in bursts at long intervals.
        if { [ catch { llength $rawdata } err ] || \
             [ string length [ join $rawdata ] ] < 3 } {
           if { ! [ eof $sid ] } {
              return {}
           }   
        }
        
        ;## safe because we just verified it
        if { [ llength $rawdata ] == 1 } {
           set rawdata [ lindex $rawdata 0 ]
        }
        set data $rawdata 
        
        ;## Sept 2002 we saw errors from the metadata API where the
        ;## cmd::receive at the manager seemed to get {0 0 0} and
        ;## it was not info complete??
        ;## this will handle it, but it is a bit of a mystery.
        if { [ regexp {partial data read: ' (.+) '} $rawdata -> data ] } {
           if { [ llength $data ] == 1 } {
              set data [ lindex $data 0 ]
           }
           set data [ list [ lindex $data 0 ] $rawdata [ lindex $data 2 ] ]
        }
        
        foreach { retval msg subject } $data { break }   
        set retval  [ string trim $retval  ]
        set msg     [ string trim $msg     ]
        set subject [ string trim $subject ]
        
        if { [ string length $subject ] } {
		   if	{ [ string equal "error!" $subject ] } {
		   		set subject "$name [ set ::${name}::userSubject ] $subject"
			} else {
				set subject "$name $subject" 
			}
        } else {
           set subject "$name [ set ::${name}::userSubject ]"
        }
        
        set data [ list $retval $msg $subject ]
        
        set ::${name}::usermessage [ list $msg $subject ]
        
        switch -exact -- $retval {
           0 { ;## next sequence block (collect field 1 msg)
               if { [ string length $msg ] > 1 } {
		        lappend ::${name}::tmpmsg $msg
			}
               if { [ string length $subject ] > 1 } {
                  lappend ::${name}::tmpsubject $subject
               }
             }
           1 { ;## error condition, try to continue
               addLogEntry $msg red
		     lappend ::${name}::tmpmsg $msg
               if { [ string length $subject ] > 1 } {
                  lappend ::${name}::tmpsubject $subject
                  if { [ regexp -nocase {error!$} $subject ] } {
                     set ::${name}::errorapi [ set ::${name}::api ]
                  }
               }
             }
           2 { ;## some useful message follows, continue
               addLogEntry $msg blue
		     lappend ::${name}::tmpmsg $msg
               if { [ string length $subject ] > 1 } {
                  lappend ::${name}::tmpsubject $subject
               }
             }
           3 { ;## abort this assistant manager
               set ::${name}::errorapi [ set ::${name}::api ]
               ${name}::predelete $subject $msg
             }
           4 { ;## short circuit to avoid empty calls
               if { [ string length $msg ] > 1 } { addLogEntry $msg }
               set ${name}::cmd {}
             }
           5 { ;## allow a temporary detach of the job code
               if { [ string length $msg ] > 1 } { addLogEntry $msg }
               return {}
             }
     default { ;## unexpected return value. generate report and abort
     		addLogEntry "args '$args' raw data '$origdata'" purple 
     		addLogEntry "bad return '$retval' msg '$msg' subject '$subject'" red
            ${name}::badReturnValue $retval $msg $subject
            }
        } ;## end of switch
        
        if { [ string length [ namespace which ${name}::seq ] ] } {
           ${name}::seq
        }
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        }   
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::badReturnValue
##
## Description:
## Handler for default condition in switch case of
## ${name}::sockhandler
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc ${name}::badReturnValue { retval msg subject } {
     
     if { [ catch {
        
        set name [ namespace tail [ namespace current ] ]
        set jobid $name
        set api [ set ::${name}::api ]

        if { ! [ string length $retval$msg ] } {
           set msg    "unexpected socket closure in '$api' API."
           append msg " job control lost."
        } else {
           set data [ uplevel set rawdata ]
           set msg "Received unexpected return value from '$api':\n'$data'"
        }
        
        addLogEntry "JOB ABORTED (LDAS system error): $msg" red
        
        set ::${name}::errorapi $api
        
        ;## prepare the report to return to the user via e-mail
        set report [ queryLogArchive $jobid ]
        set rname  [ publicFile $name ${jobid}_error_report.html $report ]
        set rname  [ url2file $name http://${jobid}_error_report.html ]
        foreach { url file } $rname { break }
        gifBalls [ file dirname $file ]
        append msg "\nerror report:\n$url"
        append subject " [ set ::${jobid}::userSubject ] error!"
        
        ${name}::predelete $subject $msg
        
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::log
##
## Description:
## Make a log entry in the seperate log file for this
## assistant manager. The log file is named consistently
## with the key of the command from which the asst gets
## it's name.
##
## Usage:
##
## Comments:
## This should probably be considered a debugging aid.
## the LDASmgr.log should probably receive these entries.

proc ${name}::log { { msg "" } } {
     set name [ namespace tail [ namespace current ] ]
     set jobid $name
     if { [ catch {
     addLogEntry $msg 0 $name [ gpsTime ] $name
     } err ] } {
     catch { addLogEntry $err 2 }
     return -code error "${name}::log: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::seq 
##
## Description:
## Manages the sequencing of the assistant managers' job.
##
## Pops the current API and command block off the command
## stack, and tries to send it to the remote API.  If the
## send fails the job is cancelled and appropriate error
## messages propagate.
##
## Usage:
##
## Comments:
## An assistant manager connection error does not force
## a bootstrap of the API due to the likelihood of
## thrashing.

proc ${name}::seq { } {

     set name [ namespace tail [ namespace current ] ]
     set jobid $name
     set errlvl 0
     set api [ list ]
     if { [ catch {
        if { [ string equal [ set ${name}::cmd ] null ] } {
           if { [ catch {
              set ${name}::cmd [ ${name}::getcmd ]
           } err ] } {
              if { [ regexp {localiseInputURLs} $err ] } {
                 set errlvl 2
                 set api manager
                 return -code error $err
              } else {
                 set api manager
                 return -code error $err
              }
           }
           
           if { ! [ string length ${name}::cmd ] } {
              set errlvl 2
              return -code error "null command received"
           }   
           
           ;## I'm not too happy about this "counting", but
           ;## since the manager built it it should be ok.
           if { [ catch {
              append ${name}::meta [ stack::pop ${name}::cmd 3 ]
              append ${name}::meta "\n"
              append ${name}::meta [ stack::pop ${name}::cmd 2 ]
           } err ] } {
              set errlvl red
              set err "$err: in macro code used by this job!"
              return -code error $err
           }
        }   
        
        ;## this block will catch the "stack exhausted" condition
        ;## when the command is finished.
        if { [ info exists ::SYSTEM_SHUTDOWN_IN_PROGRESS ] } {
           set ${name}::cmd {}
        }
        set errlvl blue
        foreach { api block } [ stack::pop ${name}::cmd 2 ] { break }
        set ::${name}::api $api
        set ::${name}::block $block
        set errlvl 2
        set block "[ set ${name}::meta ]\n$block"
        ${name}::bgcmd $api $block
     } err ] } {
        ;## we don't want to see the error from stack::pop
        ;## when the command is exhausted!
        if { ! [ regexp -nocase {Stack.+exhausted} $err ] } {
           addLogEntry $err $errlvl
        } elseif { [ info exists ::SYSTEM_SHUTDOWN_IN_PROGRESS ] } {
           if { [ llength [ namespace children :: $name ] ] } {
              set subject "$name [ set ::${name}::userSubject ] error!"
              set msg "$::LDAS_SYSTEM System Shutting Down NOW!" 
              append msg "\nJob $name has been aborted."
              ${name}::predelete $subject $msg
              ${name}::delete
           }
        } elseif { [ llength [ namespace children :: $name ] ] } {
           if { [ string equal 2 $errlvl ] } {
              set msg "$api API fault: $err"
              set subject "$name [ set ::${name}::userSubject ] error!"
              ${name}::predelete $subject $msg
           }
           ${name}::delete 
           if { [ string equal 2 $errlvl ] } {
              ::mgr::zombies $api
           }
        }
        return {}
     }     
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::delete 
##
## Description:
## Self destruct assistant! Deletes the namespace, closes
## the log file, and unsets the queue.
##
## Usage:
##
## Comments:
## If the message returned for mailing to the user consists
## entirely of a comma seperated list of filenames, or if
## ::${name}::usermessage is a list of two elements, and the
## second element is a list of filenames, then the results
## will be "attached" to the user's mail notification.
##
## The -subject option may be supported by user commands or
## may not be as required.

proc ${name}::delete { } {
     set sid        [ list ]
     set msg        [ list ]
     set subject    [ list ]
     set atachments [ list ]
     set name [ namespace tail [ namespace current ] ]
     set jobid $name
     
     ::unset ::QUEUE($jobid,cmd)      
     ;## we can catch here if the user command was ill-formed
     ;## (mismatched braces, etc...)
     if { [ catch {
        set sid [ lindex [ set ::${name}::sid ] 1 ]
     } err ] } {
       set sid [ list ]
     }
     
     if { [ info exists ::assistant_socket($name) ] } {
        ::unset ::assistant_socket($name)
     }
     
     if { [ string length $sid ] } {
        if { [ catch { ::close $sid } ] } {
           addLogEntry "error closing channel \"$sid\"" red
        } else {
           addLogEntry "closed channel $sid" purple
        }
        set ::${name}::sid [ list ]
     }
     
     set logname [ file join $::LDASLOG LDAS${name}.log ]
     
     if { [ catch {
        set attachments {}
        
        foreach { msg subject } [ set ::${name}::usermessage ] { break }
        ;## to prevent tacking on a 0 from a pass-thru
        if { [ string equal 0 $msg ] } { set msg [ list ] }

	   if { [ info exists ::${name}::tmpmsg ] } {
	      
           set tmpmsg [ set ::${name}::tmpmsg ]
           
           ;## test for a '0' state on the last returned message
           ;## described in PR 2527
           if { [ string equal $msg $tmpmsg ] || \
                [ string equal $msg [ lindex $tmpmsg end ] ] } {
              set msg [ list ]
           }     
           
           set tmpmsg [ join [ set ::${name}::tmpmsg ] "\n\n" ]
           
           if { [ string length $msg ] } {
              set msg "$msg\n\n$tmpmsg"
           } else {
              set msg $tmpmsg
           }
        }

        if { [ string length [ lrange $subject 1 end ] ] < 3 } {
           if { [ info exists ::${name}::tmpsubject ] } {
              set subjects [ set ::${name}::tmpsubject ]
              foreach subject $subjects {
                 if { [ string length [ lrange $subject 1 end ] ] > 3 } {
                 break
                 }
                 set subject "$name results"
              }
           } else {
              set subject "$name results"
           }
        }
	   
	   ;## handle missing return message as gracefully
        ;## as possible, but this is bad news.
        if { [ string length $msg ] < 3 } {
           set oldmsg $msg
           if { [ string equal 0 [ string trim $msg ] ] } {
              set msg    "An LDAS system error occurred! "
              append msg "Your job has been aborted. "
              append msg "It is likely that there is "
              append msg "something seriously wrong with "
              append msg "this LDAS installation. "
              append msg "It is likely that no more jobs "
              append msg "can be processed until this "
              append msg "LDAS installation is restarted. "
              append msg "Please contact the responsible "
              append msg "LDAS manager administrator: "
              append msg "$::manager_email "
              append msg "($::LDAS_SYSTEM running LDAS version "
              append msg "$::LDAS_VERSION)"
              set subject "$subject LDAS system error!"
           } else {
              set msg    "An LDAS system error occurred! "
              append msg "Your job has been aborted. "
              append msg "(Possibly at your request or by an admin.). "
              append msg "*** original system message: '$oldmsg' ***"
              append msg " ($::LDAS_SYSTEM running LDAS version "
              append msg "$::LDAS_VERSION)"
              set subject "$subject error!"
           }
        }
        
        if { [ array exists ::${name}::starttime ] } {
           ;## get data for last API
           ::${name}::timeStats
           ;## generate report
           set timingdata [ mgr::jobTimingReport $name ]
           if { [ string length $timingdata ] } {
              append msg "\n\n$timingdata"
           }
        } 
        ;## this gets unset in mailTo portMsgPersistentSocket
        if { [ catch {
           set user  [ ::mgr::job2name  $name ]
           set email [ ::mgr::job2email $name ]
                     
           ;## PR 2920: using globus sockets 
  		   if	{ [ info exist ::${name}_globusHandle ] } {
           		set use_globus 1
           		set persist [ set ::${name}::persistentsocket ]
           		if	{ ! [ string equal not_persistent $persist ] } {
                	RawGlobus::sendCmd "Subject:\n$subject\n$msg" [ set ::${name}_globusHandle ]
                    RawGlobus::close [ set ::${name}_globusHandle ]
                    unset ::${name}_globusHandle
                } else {
                	mailTo $email $subject $msg $attachments
                }
           } else {
          		mailTo $email $subject $msg $attachments
           }
           set drop [ jobDirectory ]/email.$jobid
           set fid [ open $drop w ]
           puts $fid "<html>\n<pre>\n$user ${email}\n${subject}\n$msg"
           ::close $fid
        } err ] } {
           catch { ::close $fid }
           addLogEntry $err red
        }

        ;## PR #2749 persistent socket cleanup
     	if { [ info exists ::${jobid}::persistentsocket ] } {
        	set sid [ set ::${jobid}::persistentsocket ]
        	addLogEntry "cleanup persistent socket $sid" purple
        	::unset ::${jobid}::persistentsocket
        	catch { close $sid }
     	} 
        namespace delete [ namespace current ]
        mgr::deleteJobidFromUsersQueue $jobid
     
     	if	{ [ info exist use_globus ] } {
        	mgrGlobus::deleteUserFromUsersQueue $user
        }
     } err ] } {
        addLogEntry $err red
        if { [ regexp $name [ namespace current ] ] } {
           namespace delete [ namespace current ]
        }   
     }
     
     ;## jobid array may have been deleted at this time
     if	{ [ info exist ::userSid($sid) ] } {
     	set sidJobId [ set ::userSid($sid) ]
        if	{ [ info exist ::${jobid}(userSid) ] } {
        	set jobSid [ set ::${jobid}(userSid) ]
            unset ::${jobid}(userSid)
        } else {
        	set jobSid ""
        }
        if	{ ! [ string equal $sidJobId $jobid ] } {
        		addLogEntry "$sid's jobid $sidJobId does not match $jobid, expected '$jobSid'" red
        } 
        unset ::userSid($sid)
     }
     	    
     addLogEntry "assistant manager \"$name\" destroyed."
     
     catch { unset ::$jobid }
     set assistants [ namespace children :: ${::RUNCODE}* ]
     set ::mgr::activejobs [ llength $assistants ]
     catch { leakLogger }
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${name}::predelete
##
## Description:
## When a job is being aborted, the state might not be
## exactly sane, so try and step through things and collect
## useful info along the way.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc ${name}::predelete { subject body { level red } } {
     
     if { [ catch {
         set name [ namespace tail [ namespace current ] ]
         set jobid [ string trim $name : ]
         
         if { [ info exists ::${jobid}::sid ] } {
            set sid [ lindex [ set ::${jobid}::sid ] 1 ]
            if { [ string length $sid ] } {
               if { [ info exists ::assistant_socket($jobid) ] } {
                  ::unset ::assistant_socket($jobid)
                  if { [ catch {
                     ::close $sid
                  } err ] } {
                     set sockerr $err
                  }
               }
               set ::${jobid}::sid [ list ]
            }
         }
         
         set ::${jobid}::usermessage [ list $body $subject ]
         set ::${jobid}::cmd {}
        
         ;## caller must call ::${jobid}::seq now to trigger
         ;## job deletion sequence.
		
         if { [ catch {
            mgr::garbageCollect $jobid
         } err ] } {
            addLogEntry "mgr::garbageCollect: $err" red
         }
         
         if { [ regexp -nocase {mail} $level ] } {
            addLogEntry "Subject: ${subject}; Body: $body" email 
         } else {
            addLogEntry $body red
         }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

;#barecode
     ;## log successful assistant creation.
     addLogEntry "Namespace \"$name\" created"
     ::mgr::getURLsForJob $name
     return {}
} ;## end of createAssistant



