#!/ldcg/bin/tclsh
## ****************************************************
## This script is used to test the performance of
## the frame API by generating a variety of reduced
## ilwd frame objects from 100 identical input frames.
## Each iteration converts 100 input frames into 100
## reduced ilwd frame objects and emits statistics
## describing the time requirements of several steps
## in the process.
## ****************************************************

;## fool the generic API so we can package require it.
set API manager
package require generic
package require genericAPI
package require frameAPI

;## The first time through we are going to run
;## readDaqFrame, on all subsequent calls we are
;## going to run updateDaqFrame.
set mode d

;## the list of ADC index ranges to iterate over
foreach range {0-0 0-4 0-9 0-24 0-49 0-99 0-199 0-299 0-413} {

   ;## reset the statistical registers for the per-range
   ;## stats
   set open_frame_file       {}
   set update_daq_frame      {}
   set daq_activate_all_adcs {}
   set get_frame_attributes  {}
   set init_raw_frame        {}
   set insert_adc_chans      {}

   ;## update the range of ADC's to be registered.
   __t::start
   if { [ regexp {u} $mode ] } {
      regexp {(\d+)-(\d+)} $range -> first last
      daqTriggerADCIndexRange $framep $first $last 1
      lappend trig_adc_index_range [ __t::mark ]
   }

   ;## iterate over 100 files
   foreach file [ glob frames/Data0/* ] {
      ;## open the file 
      __t::start
      set fp [ openFrameFile $file $mode ]
      lappend open_frame_file [ __t::mark ]
      
      if { [ regexp {u} $mode ] } {
         __t::start
         updateDaqFrame $framep $fp
         lappend update_daq_frame [ __t::mark ]
         closeFrameFile $fp
      } else {   
         catch { destructFrame $framep }
         __t::start
         set framep [ readDaqFrame $fp ]
         lappend read_daq_frame [ __t::mark ]
         ;## might generate a Bus error or segfault
         ;##closeFrameFile $fp
         set detectorp [ frameDetector $framep ]
         regexp {(\d+)-(\d+)} $range -> first last
         __t::start
         daqTriggerADCIndexRange $framep $first $last 1
         lappend trig_adc_index_range [ __t::mark ]
         set mode u
      }

      set retval {}
      __t::start
      foreach attr {Name Run Frame GTime ULeapS LocalTime Dt} {
         lappend retval $attr [ getFrame$attr $framep ]
      }
   
      foreach { name value } $retval {
         lappend ptrs $value
         set value [ getElement $value ]
         regexp -- {>(.+)<} $value -> value
         if { [ llength $value ] == 2 } {
            foreach "GTimeS GTimeN" $value { break }
         } else {
            set $name $value
         }
      } 
      lappend get_frame_attributes [ __t::mark ]
      
      __t::start
      set ilwdp [ createFrame $Name $Run $Frame $GTimeS $GTimeN $ULeapS $LocalTime $Dt ]

      set raw [ createRawData rawdata ]
      insertFrameData $ilwdp $raw
      insertFrameData $ilwdp $detectorp
      lappend init_raw_frame [ __t::mark ]
   
      __t::start
      insertadcchanlist $framep $ilwdp $range
      lappend insert_adc_chans [ __t::mark ]
   
      destructElement $ilwdp
      destructElement $raw
      foreach ptr $ptrs { destructElement $ptr }
      set ptrs {}
   }

   ;## print the statistics after everything is done.
   puts "\nFrame to ILWD conversion with channels $range:"
   puts "open frame file: [ stats $open_frame_file ]%"
   puts "read frame file into structure: [ stats $update_daq_frame ]%"
   puts "extract frame attributes: [ stats $get_frame_attributes ]%"
   puts "initialise raw frame: [ stats $init_raw_frame ]%"
   puts "insert [ expr { $last + 1 } ] ADC's into new frame: [ stats $insert_adc_chans ]%"
}

puts "\nstats taken over all runs (one data point per ADC range):"
puts "hash adc channels: [ stats $trig_adc_index_range ]%"
puts "initialise frame structure (only done once): $read_daq_frame"

