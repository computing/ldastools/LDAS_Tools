#!/bin/sh
#
#   Find the CSD of (chan2 - chan1) and chan0. Chan0 is first downsampled
#   by 1/64
#
#   Usage: e3csd ... username password email
#
# The following lines are ignored by tclsh because of this backslash \
exec tclsh "$0" "$@"

source e3common

#############################################################################
#
# proc go { ret stime duration chan0 chan1 chan2 fft_length overlap
#           interferometers datacondtarget host socket user passwd email }
#
#    - execute the command
#
#############################################################################
proc go { ret stime duration chan0 chan1 chan2 fft_length overlap input interferometers datacondtarget host socket user passwd email } {

set times [ calc_times $stime $duration ]

set srate 256
set numsamples [ expr $duration*$srate ]
set fft_secs [ expr $fft_length/$srate ]
set overlap_ratio [ expr $overlap*100/$fft_length ]

# Process the channel name into the actual name of the data
# as referenced in the datacon API - easier to guarantee we're
# acting on the right data this way
set ch0alias [ mkchannel $chan0 $stime ]
set ch1alias [ mkchannel $chan1 $stime ]
set ch2alias [ mkchannel $chan2 $stime ]

#
# Edit between the quotes to change the command
#
set cmd "
ldasJob {-name $user -password $passwd -email $email} {
  dataPipeline
    -subject {csd($chan2 - $chan1, $chan0)}
    -times $times                          # span $duration seconds
    -inputprotocol $input
    -interferometers $interferometers
    -returnprotocol http://results.ilwd
    -outputformat $ret                     # ignored for metadata target
    -resultcomment { CSD of ($chan2 - $chan1) and $chan0 }
    -targetapi datacond
    -datacondtarget $datacondtarget
    -aliases {
       ch0alias = $ch0alias;
       ch1alias = $ch1alias;
       ch2alias = $ch2alias;
    }
    -algorithms {
      #
      # Calculate the CSD of (chan2 - chan1) and chan0
      #
      # For this run the input parameters are
      #
      #     stime      : $stime      
      #     chan0      : $chan0      (MUST be a fast channel)
      #     chan1      : $chan1
      #     chan2      : $chan2
      #     fft_length : $fft_length
      #     overlap    : $overlap
      #

      # Seismic sample rates are 256 Hz - resample IFO down by 1/64
      resamp_ch0 = resample(ch0alias, 1, 64);

      clear(ch0alias);

      # I need to take the difference of ch2 and ch1

      ch2_minus_ch1 = sub(ch2alias, ch1alias);

      #
      # Now take the csd of the sequences with
      #
      #   - sequence length of $duration*$srate = $numsamples samples ($duration seconds)
      #   - fft length of $fft_length ($fft_secs seconds)
      #   - overlap of $overlap (overlap/fft_length ratio ${overlap_ratio}%)
      #   - default windowing (Hanning)
      #   - mean detrending
      #
      c = csd(ch2_minus_ch1, resamp_ch0, $fft_length, _, $overlap, 1);
    } 
    -framequery { 
      # No spaces allowed!
      adc($chan2,$chan1,$chan0)
    }
}
"

sendcmd $cmd $host $socket
} 

##############################################################################
#
# Main
#
##############################################################################

if { $argc != 15 } {
  puts "Usage: e3csd ... username password email"
  exit
}

# Get cmd line args
set arg_idx 0

# Start time
set stime [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

# Duration in seconds
set duration [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

# :NOTE: Must be fast channel (16384 Hz)
set chan0 [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

# Should be seismic channels, could be anything with a rate of 256
set chan1 [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set chan2 [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set fft_length [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set overlap [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set input [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set interferometers [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set datacondtarget [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set host [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set socket [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set user [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set passwd [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

set email [ lindex $argv $arg_idx ]
set arg_idx [ expr $arg_idx + 1 ]

# -outputformat {ilwd ascii}
set ret "{ilwd ascii}"

# Run the command
go $ret $stime $duration $chan0 $chan1 $chan2 $fft_length $overlap $input $interferometers $datacondtarget $host $socket $user $passwd $email

exit
