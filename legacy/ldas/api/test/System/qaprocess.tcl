package provide qaprocess 1.0

namespace eval ::QA::Process {
    variable ProcessTable
    variable CurrentProcessId 1

    array set ProcessTable [list]
    variable Children [list]

    ;##------------------------------------------------------------------
    ;## Create process in the background
    ;##------------------------------------------------------------------
    namespace export SpawnInBackground
    proc SpawnInBackground {cmd} {
	variable Children
	variable CurrentProcessId
	variable ProcessTable

	incr CurrentProcessId
	set ProcessTable($CurrentProcessId,Start) [clock seconds]
	if [catch {set pipe [open "|$cmd" r]} err] {
	    set ProcessTable($CurrentProcessId,Exit) $err
	    set ProcessTable($CurrentProcessId,Stop) [clock seconds]
	} else {
	    lappend Children $CurrentProcessId
	    set ProcessTable($CurrentProcessId,FileId) $pipe
	    fconfigure $pipe -buffering none -blocking 0
	    fileevent $pipe readable [list ::QA::Process::processReader $pipe $CurrentProcessId]
	}
	return $CurrentProcessId
    }

    ;##------------------------------------------------------------------
    ;## Kill - Send the signal to the process
    ;##------------------------------------------------------------------
    namespace export Kill
    proc Kill {args} {
	variable ProcessTable

	if {[lindex $args 0] == -0} {
	    set args [lreplace $args 0 0]
	}
	if {[info exists ProcessTable([lindex $args 0],Stop)]} {
	    return 1
	}
	return 0
    }
    ;##------------------------------------------------------------------
    ;## Wait till a process terminates
    ;##------------------------------------------------------------------
    namespace export Wait
    proc Wait {children} {
	variable Children

	set retval [list]
	set newlist [list]

	if {[llength $children] <= 0} {
	    ;##----------------------------------------------------------
	    ;## No arguments passed so search all processes
	    ;##----------------------------------------------------------
	    foreach child $Children {
		if {[Kill -0 $child] == 0} {
		    lappend newlist $child
		} else {
		    lappend retval $child
		}
	    }
	} else {
	    ;##----------------------------------------------------------
	    ;## Look for specific pids.
	    ;##----------------------------------------------------------
	    while {[llength $children] > 0} {
		set newchildren [list]
		foreach child $children {
		    ;##--------------------------------------------------
		    ;## See if the process is being managed
		    ;##--------------------------------------------------
		    set pos [lsearch -exact $Children $child]
		    if {$pos == -1} {
			;##----------------------------------------------
			;## Not being managed
			;##----------------------------------------------
			lappend retval $child
		    } else {
			;##----------------------------------------------
			;## It is being managed. Check to see if the
			;##   process is still active.
			;##----------------------------------------------
			if {[Kill -0 $child] == 0} {
			    lappend newchildren $child
			} else {
			    lappend retval $child
			    set Children [lreplace $Children $pos $pos]
			}
		    }
		} ;## foreach child $children
		set children $newchildren
		if {[llength $children] > 0} {
		    vwait ::QA::Process::ProcessTable([lindex $children 0],Stop)
		}
	    } ;## while {[llength $children] > 0}
	}
	return retval
    }

    ;##------------------------------------------------------------------
    ;## Return the amount of wall time a process took
    ;##------------------------------------------------------------------
    namespace export Walltime
    proc Walltime {Pid} {
	variable ProcessTable
	set walltime 0
	if {[info exists ProcessTable($Pid,Start)] \
		&& [info exists ProcessTable($Pid,Stop)]} {
	    set walltime [expr $ProcessTable($Pid,Stop) \
			      - $ProcessTable($Pid,Start)]
	}
	return $walltime
    }

    namespace export ProcessCB
    proc ProcessCB {Pid Script} {
	variable ProcessTable

	set ProcessTable($Pid,Script) $Script
    }

    proc processReader {Pipe ProcessId} {
	variable ProcessTable
	variable WaitDone

	if {[eof $Pipe]} {
	    catch {close $Pipe}
	    set ProcessTable($ProcessId,Stop) [clock seconds]
	    if {[info exists ProcessTable($ProcessId,Script)]} {
		;##------------------------------------------------------
		;## Schedule the callback to be called
		;##------------------------------------------------------
		after 100 $ProcessTable($ProcessId,Script)
	    }
	    return
	}
	if {[gets $Pipe line] == -1} {
	    return
	}
	if {[info exists ProcessTable($ProcessId,Output)]} {
	    set ProcessTable($ProcessId,Output) \
		[concat $ProcessTable($ProcessId,Output) "\n" $line]
	} else {
	    set ProcessTable($ProcessId,Output) $line
	}
    }
} ;## namespace ::QA::Process
