#! /bin/sh
site=$1
echo "seeding $site with L1 segments"
../onejob.tcl -s $1 "putMetaData -ingestdata %FILE(ifolocked_s2_L1.xml) -database ldas_tst"
echo "seeding $site with H1 segments"
../onejob.tcl -s $1 "putMetaData -ingestdata %FILE(ifolocked_s2_H1.xml) -database ldas_tst"
echo "seeding $site with H2 segments"
../onejob.tcl -s $1 "putMetaData -ingestdata %FILE(ifolocked_s2_H2.xml) -database ldas_tst"
