#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

source fr2ilwd.rsc 

set nfstext "NFS"
set loctext "Local"

proc main {} {
    set outfile {}

    ;## Get command line arguments
    set idx [lsearch -exact $::argv "-"]
    if {$idx >= 0} {
        set outfile stdout
        set ::argv [lreplace $::argv $idx $idx]
    }
    set ::argc [llength $::argv]

    if {$::argc <= 1} {
        puts stderr "Error: Missing filename for comparison"
        return
    }

    if {$::argc > 2} {
        puts stderr "Error: Too many arguments"
        return
    }

    set file [lindex $::argv 0]
    set oldfile [lindex $::argv 1]
	set ::result_directory [ file dirname $file ]
	
    if {[file isdirectory [lindex $::argv end]]} {
        set oldfile [file join [lindex $::argv end] [ file tail $file] ]
		puts "oldfile $oldfile"
    }

    if {![string match stdout $outfile]} {
        set outfile [file rootname $file].stats
        if {[file exists $outfile]} {
            set mtime [clock format [file mtime $outfile] -format "%H%M%S"]
            file rename -force -- $outfile ${outfile}.${mtime}
        }
    }

    compare $file $oldfile $outfile

    return
}


proc compare {new old out} {

    if {![file exists $old]} {
        puts stderr "Error: ${old}: No such file."
        return -code continue
    }
    if {![file exists $new]} {
        puts stderr "Error: ${new}: No such file."
        return -code continue
    }
    if {[file tail $new] != [file tail $old]} {
        ;## Warning
        puts stderr "Warning: $old and $new do not have the same name."
    }

    foreach age {new old} {
	puts "file $age [ set $age ]"
        set fd [open [set $age] r]
        set data [split [read $fd] "\n"]
        close $fd

        foreach line $data {
          if {[regexp {(nfs|local) file system} $line var]} {
                set var [string range $var 0 2]
				puts "file system $var"
                continue
            }
			if	{ [ regexp -nocase {channel} $line ] } {
				foreach item $line {
					lappend columns_${age} $item
				}
				set numColumns_$age [ llength [ set columns_$age ] ]
				puts "set numColumns_${age} columns [ set columns_$age ]"
				continue
			}

			if	{ ![ info exist columns_$age ] } {
				continue
			}
            set conv [eval scan \"$line\" \"%s %d %d %f %f %f %f %f %f %f\" [ set columns_$age ] ]
			foreach item [ set columns_$age ] {
				puts "item $item [ set $item ]"
			}
            if {$conv != [ set numColumns_$age ] } {continue}
			set functions [ lrange [ set columns_$age ] 3 end ]
			puts "functions $functions"
            foreach var $functions {
                set ${var}_${age}($channel,$dims,$duration) [set $var]
				puts "set ${var}_${age}($channel,$dims,$duration) [set $var]"
    	   	}
			
		}

		if	{ [ set numColumns_$age ] != [ set numColumns_$age ] } {
			puts "old and new columns differ" 
			exit
		}
			
	}
    ;## calculate the delta between the total time

	  set fd [ open $::result_directory/fr2ilwd.stats  w ]
	  puts $fd "frame to ilwd test statistics"
	  puts $fd [ clock format [clock seconds] -format {%B %d, %Y}]
	  puts $fd "New file [ set new ]\nOld file [ set old ]\n"
	  
	  puts $fd [ format "%-10.10s\t%10.10s\t%15.15s\t\t%15.15s\t%10.10s\t%10.10s" \
			channel dims duration delta total_average %faster ]
		foreach channel $::DATA_TYPES {
			foreach dims $::ITERATIONS {
        		foreach time $::TIMESERIES  {
					set delta [ expr [ set total_new($channel,$dims,$time) ] - [ set total_old($channel,$dims,$time) ] ]
					set average [ expr ([ set total_new($channel,$dims,$time) ] + [ set total_old($channel,$dims,$time) ]/2.0) ]	
									
					puts $fd [ format "%-10.10s\t%10.10s\t%15.15s\t%15.15f\t%10.10f\t%10.10f"\
						 $channel $dims $time $delta $average [ expr ($delta/$average) *100] ]
				}
			}
       	}
    close $fd 
 
    #getStats
    #outStats $new $old $out

    return
}

proc outStats {new old out} {
    if {[string match stdout $out]} {
        set fout $out
    } else {
        set fout [open $out w]
    }

    puts $fout "frame to ilwd test statistics"
    puts $fout [clock format [clock seconds] -format {%B %d, %Y}]
    puts $fout "New file: [clock format [file mtime $new] -format {%B %d, %Y}]"
    puts $fout "Old file: [clock format [file mtime $old] -format {%B %d, %Y}]"
    puts $fout ""

    puts $fout [format "%-7s%-10s%-10s" " " Mean "Std Dev"]
    puts $fout [format "%-6s%- 10.6f%- 10.6f" NFS [lindex $::statsnfsG 0] [lindex $::statsnfsG 1]]
    puts $fout [format "%-6s%- 10.6f%- 10.6f" Local [lindex $::statslocG 0] [lindex $::statslocG 1]]

    foreach fstype {nfs loc} {
        puts $fout ""
        puts $fout [format "%-7s%-10s%-10s" [set ::${fstype}text] "Mean" "Std Dev"]
        foreach chann $::Channels {
            puts $fout [format "%-6s%- 10.6f%- 10.6f" $chann \
                [lindex [set ::stats${fstype}($chann)] 0] [lindex [set ::stats${fstype}($chann)] 1]]
        }
    }

    foreach fstype {nfs loc} {
        puts $fout ""
        puts $fout [eval format "%-9s%-10s%-10s%-10s%-10s%-10s%-10s%-10s" \"[set ::${fstype}text]\" [lrange $::Times 0 end]]
        puts -nonewline $fout [format "%-8s" Mean]
        foreach time $::Times {
            puts -nonewline $fout [format "%- 10.6f" [lindex [set ::stats${fstype}($time)] 0]]
        }

        puts $fout ""
        puts -nonewline $fout [format "%-8s" "Std Dev"]
        foreach time $::Times {
            puts -nonewline $fout [format "%- 10.6f" [lindex [set ::stats${fstype}($time)] 1]]
        }

        puts $fout "\n"
    }

    if {![string match stdout $out]} {
        close $fout
    }

    return
}

proc getStats {} {
    foreach fstype {nfs loc} {
        foreach time $::Times {
            set ::stats${fstype}($time) [stats [set ::delta${fstype}($time)]]
        }
        foreach chann $::Channels {
            set ::stats${fstype}($chann) [stats [set ::delta${fstype}($chann)]]
        }

        set ::stats${fstype}G [stats [set ::delta${fstype}G]]
    }
}

proc stats {samples} {
    set mean 0.0
    set S2   0.0
    set cov  0.0
    set N [ llength $samples ]

    if { $N < 3 } {
        return -code error "stats: sample size too small: ($N items)"
    }

    ;## calculate the arithmetic mean
    set mean [ expr ([ join $samples + ]) / $N. ]

    ;## calculate the standard deviation
    foreach s $samples {
        set S2 [ expr { $S2+pow(($s-$mean),2) } ]
    }
    set S2 [ expr { $S2/($N-1) } ]
    set S  [ expr { sqrt($S2) } ]

    return [list $mean $S]
}

main

;## framestats.tcl results-0805/fr2ilwd.log results-0722
