#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Establish GLOBALS
#------------------------------------------------------------------------
namespace eval ::QA::compression::test {
    ;##------------------------------------------------------------------
    ;##------------------------------------------------------------------
    variable RunData
    array set RunData {
	S1:StartTime   714384016
	S1:FrameLength 16
	E9:StartTime   727550000
	E9:FrameLength 16
	S2:StartTime   730524800
	S2:FrameLength 16
	S3:StartTime   751800016
	S3:FrameLength 16
    }

    variable UncompressedSize
    array set UncompressedSize {}
    ;##------------------------------------------------------------------
    ;##------------------------------------------------------------------
    variable CompTypes {
	raw
	gzip
	diff_gzip
	zero_suppress_int_float
	zero_suppress_short
	zero_suppress_otherwise_gzip
    }
    variable CompLevels

    array set CompLevels {
	raw 1
	gzip 9
	diff_gzip 9
	zero_suppress_int_float 1
	zero_suppress_short 1
	zero_suppress_otherwise_gzip 9
    }

    ;##------------------------------------------------------------------
    ;##------------------------------------------------------------------
    variable RDSCommand {
	createRDS
        -times {$queryTime}
        -type {$type}
        -compressiontype {$comptype}
        -compressionlevel {$complevel}
        -channels {$adcquery}
    }

    variable RDSDefaultCompressionCommand {
	createRDS
	-subject {default compression}
        -times {$queryTime}
        -type {$type}
        -compressiontype {$comptype}
        -channels {$adcquery}
    }

    variable type R
    variable comptype [list]
    variable complevel [list]
    variable adcFile ""
    variable adcquery [list H2:LSC-AS_Q \
			   H2:LSC-AS_I \
			   H2:LSC-REFL_DC \
			   H2:LSC-REFL_Q!2 \
			   H2:LSC-REFL_I!2 \
			   H2:LSC-POB_Q!2]
    variable startTime $RunData(S3:StartTime)
    variable jobLength $RunData(S3:FrameLength)
    variable queryTime "${startTime}-[expr $startTime + $jobLength -1]"


    ;##------------------------------------------------------------------
    ;##------------------------------------------------------------------
    variable  FrameDataCommand {
	getFrameData
	-outputformat {ilwd ascii}
	-framequery {{} {} {$FrameFilename} {} full()}
    }
    variable FrameFilename [list]

    proc Cleanup { } {
	;##--------------------------------------------------------------
	;## Cleanup the output files associated with this test
	;## NOTE: Failures are quietly ignored.
	;##--------------------------------------------------------------
	catch {[file delete -force \
		    [glob -nocomplain \
			 [file join [OutputDir] compression-*.ilwd]]]} err
    }

    proc Diff {raw new} {
	if {[catch {exec /ldcg/bin/diff -I "name='time'" $raw $new} msg]} {
	    return -code error $msg
	}
    }


    proc Fetch {OutputFilenameRoot Command Compression Level \
		    {Code ok} {PopulateSizeArray 1}} {
	;##--------------------------------------------------------------
	;## Variables needed by this routine
	;##--------------------------------------------------------------
	variable RDSCommand
	variable FrameDataCommand
	variable FrameFilename
	variable UncompressedSize
	;##--------------------------------------------------------------
	;## Variable needed to fill in RDSCommand
	;##--------------------------------------------------------------
	variable type
	variable comptype $Compression
	variable complevel $Level
	variable adcFile
	variable adcquery
	variable queryTime
	;##--------------------------------------------------------------
	;## Variables needed by FrameDataCommand
	;##--------------------------------------------------------------
	variable FrameFilename

	variable CompressedFramename \
	    [file join [OutputDir] \
		 compression-${OutputFilenameRoot}.ilwd]

	;##--------------------------------------------------------------
	;## First generate the RDS request
	;##--------------------------------------------------------------
	if {[catch {SubmitJob jobid [subst $Command]} err]} {
	    return -code $Code $err
	}
	;##--------------------------------------------------------------
	;## Gather info about the completed job
	;##--------------------------------------------------------------
	set outputs $jobid(outputs)
	if { $PopulateSizeArray } {
	    ;##----------------------------------------------------------
	    ;## Ensure that the output is not malformed
	    ;##----------------------------------------------------------
	    set token [::http::geturl [lindex $outputs 0] -validate 1]
	    if	{ $Level == 1 } {
		if {[info exists UncompressedSize($comptype)]} {
		    ;##--------------------------------------------------
		    ;## Make sure file is of expected size
		    ;##--------------------------------------------------
		    if {$UncompressedSize($comptype) \
			    != [set ${token}(totalsize)]} {
			return -code $Code \
			    "Default compression size is incorrect"
		    }
		} else {
		    ;##--------------------------------------------------
		    ;## First time seeing this type of file. Record its
		    ;##   size for later use.
		    ;##--------------------------------------------------
		    set UncompressedSize($comptype) [set ${token}(totalsize)]
		}
	    }
	    ::http::cleanup $token
	}
	;##--------------------------------------------------------------
	;## Convert the frame into an ascii ilwd for comparison
	;##--------------------------------------------------------------
	regsub {http://[^/]+} [lindex $outputs 0] {} FrameFilename
	if {[catch {SubmitJob jobid [subst $FrameDataCommand]} err]} {
	    return -code $code $err
	}
	;##--------------------------------------------------------------
	;## Retrieve the ilwd file 
	;##--------------------------------------------------------------
	set outputs $jobid(outputs)
	LJcopy [lindex $outputs 0] $CompressedFramename
	;##--------------------------------------------------------------
	;## Return empty list to indecate nothing bad has happened
	;##--------------------------------------------------------------
	list
    }

    proc Init { } {
	variable RDSCommand
	variable comptype raw
	variable complevel 1
	variable CompressedFramename

	;##--------------------------------------------------------------
	;## Remove possible residuals from previous testing
	;##--------------------------------------------------------------
	Cleanup
	;##--------------------------------------------------------------
	;## Retrieve the base ilwd
	;##--------------------------------------------------------------
	if {[catch {Fetch base $RDSCommand raw 1 error 0} err]} {
	    error "ERROR: Unable to initialize test: $err"
	}
	variable BaseFramename $CompressedFramename
    }
}

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path ". /ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
puts "auto_path $auto_path, [ auto_execok LDASJobH ] "

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit
#------------------------------------------------------------------------
# Package allowing the parsing of XML files
#------------------------------------------------------------------------
package require http
#------------------------------------------------------------------------
# Prepare to use the TCL test harness
#------------------------------------------------------------------------
package require qatest
#------------------------------------------------------------------------
# Prepare to communicate with LDAS
#------------------------------------------------------------------------
package require LDASJob

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Main entry point into testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::compression::test {
    namespace import ::tcltest::*
    namespace import ::QA::TclTest::*

    variable TestName compresstest
    ;##------------------------------------------------------------------
    ;## Prepare to run the tests
    ;##------------------------------------------------------------------
    Init
    ;##------------------------------------------------------------------
    ;## Start with the testing of compression
    ;##------------------------------------------------------------------
    foreach comptype $CompTypes {
	;##--------------------------------------------------------------
	;## Go through all permutations of compression levels
	;##--------------------------------------------------------------
	for {set complevel 1} \
	    {$complevel <= $CompLevels($comptype)} \
	    {incr complevel} {
		test :${TestName}:${comptype}:${complevel}: {} -body {
		    ;##--------------------------------------------------
		    ;## Submit RDS job using compression
		    ;##--------------------------------------------------
		    catch {Fetch "$comptype.$complevel" \
			       $RDSCommand \
			       $comptype $complevel} err
		    if {[string length $err] > 0} {
			puts "DEBUG: err: $err"
			return $err
		    }
		    ;##--------------------------------------------------
		    ;## Compare to expected value
		    ;##--------------------------------------------------
		    Diff $BaseFramename $CompressedFramename
		    catch {[file delete -force $CompressedFramename]} err
		    list
		} -result {}
	    }
	;##--------------------------------------------------------------
	;## PR 2080
	;##
	;## Validate that the default compression mode generates files
	;##   of the correct size.
	;##--------------------------------------------------------------
	test :${TestName}:${comptype}:default: {} -body {
	    catch {Fetch "${comptype}.default" \
		       $RDSDefaultCompressionCommand \
		       $comptype $complevel} err
	    if {[string length $err] > 0} {
		puts "DEBUG: err: $err"
		return $err
	    }
	    ;##----------------------------------------------------------
	    ;## Compare to expected value
	    ;##----------------------------------------------------------
	    Diff $BaseFramename $CompressedFramename
	    catch {[file delete -force $CompressedFramename]} err
	    list
	} -result {}
    }

    #========================================================================
    # Testing is complete
    #========================================================================
    catch {[file delete -force $BaseFramename]} err

    Cleanup           ;## Local cleanup
    cleanupTests      ;## tcltest cleanup
}
namespace delete ::QA::compression::test
