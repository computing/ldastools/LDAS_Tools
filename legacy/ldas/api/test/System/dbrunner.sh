#!/bin/sh

use_ligotools="/ligotools/bin/use_ligotools"
test -x $use_ligotools && eval `$use_ligotools`

site=dev
if [ "$#" -eq 1 ] ; then
    site=$1
fi

while true; do
    #./dbntuple.tcl -s $site
    #./dbqchannel.tcl -s $site
    #./dbquery.tcl -s $site
    #./dbspectrum.tcl -s $site -match '1:DBSPECTRUM:GOOD:PASS:WRAPPER'
    ./dbspectrum.tcl -s $site -match '2:DBSPECTRUM:EMPTY:PASS:WRAPPER'
done
