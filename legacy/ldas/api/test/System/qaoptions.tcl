package provide qaoptions 1.0

namespace eval QA::Options {
    variable Options
    variable OptionList
}

#------------------------------------------------------------------------
# Allow for adding test specific options
#------------------------------------------------------------------------
proc ::QA::Options::Add {Short Long Type Description Script} {
    variable Options
    variable OptionList

    if {[ info exists Short ] && $Short != "" } {
	lappend Options $Short
	set OptionList(type,$Short) $Type
	set OptionList(desc,$Short) $Description
	set OptionList(script,$Short) $Script
    }

    if {[ info exists Long ] && $Long != ""} {
	lappend Options $Long
	set OptionList(type,$Long) $Type
	set OptionList(desc,$Long) $Description
	set OptionList(script,$Long) $Script
    }
}

#------------------------------------------------------------------------
# Parse the options and return the list of unhandled options
#------------------------------------------------------------------------
proc ::QA::Options::Parse { argvparm } {
    upvar $argvparm argv
    variable OptionList
    variable Value

    set Value ""
    set argc [llength $argv]

    for {set idx 0} {$idx < $argc} {} {
	set opt [lindex $argv $idx]
	if [info exists ::QA::Options::OptionList(type,$opt)] {
	    set argv [lreplace $argv $idx $idx]
	    incr argc -1
	    switch -exact -- $OptionList(type,$opt) {
		"" {
		    if [info exists $OptionList(script,$opt)] {
			eval $OptionList(script,$opt)
		    }
		}
		string {
		    set Value [lindex $argv $idx]
		    set argv [lreplace $argv $idx $idx]
		    incr argc -1
		    eval $OptionList(script,$opt)
		}
		default {
		    puts "DEBUG: Bad type: $OptionList(type,$opt)"
		}
	    }
	} else {
	    incr idx
	}
    }
}
#------------------------------------------------------------------------
# Routine to print out usage message when used with tcltest harness
#------------------------------------------------------------------------
proc ::QA::Options::TclTestUsage { } {
    global ::tcltest::Option
    global ::tcltest::Usage
    global ::tcltest::Option
    global ::tcltest::Verify

    variable Options
    variable OptionList

    foreach opt $Options {
	#::tcltest::Option $opt 0 $OptionList(desc,$opt)
	set Option($opt) {}
	set Usage($opt) $OptionList(desc,$opt)
	set Verify($opt) AcceptAll
	# puts [format "$opt $OptionList(type,$opt) \t $OptionList(desc,$opt)."]
    }
}
