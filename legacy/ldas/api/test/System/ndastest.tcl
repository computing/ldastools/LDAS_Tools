#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./ndastest.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*ndas*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::ndastest::test {

	set cmdstem {
    	dataPipeline
    	-subject { stochastic job for H2:LSC-AS_Q and L1:LSC-AS_Q }
    	-dynlib libldasstochastic.so
    	-filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)
    	-np 2
    	-multidimdatatarget ligolw
    	-metadataapi ligolw
    	-concatenate 1
    	-realtimeratio 0.9
   	    -datacondtarget wrapper
    	-datadistributor WRAPPER
    	-outputformat { ilwd ascii }
    	-database {$dbname}

    	-framequery {
        $framequery
    	}
    	-responsefiles {
        	file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        	file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    	}
    	-aliases {
        gw1 = H2\\:LSC-AS_Q::ProcData;
        gw2 = L1\\:LSC-AS_Q::ProcData;
    	}
	
    	-algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

        # Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    	}
	}
	
	# S3 data RDS has no NDAS frames for this period
     # requires RDS frames in 
     # /scratch/test/frames/S3_RDS/LHO/H-RDS_R_L1-751800016-16.gwf
     # or run after dords.s3 
	# so need S2 data for ndas verification for the last 2 tests
	set queryList {
	"ndas1 - source: raw frame for createRDS"
    	{{ R {} {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
	"ndas2 - source: createRDS Level 1 frames"
    	{{ RDS_R_L1 {} {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
	"ndas3 - source: createRDS Level 2 frames"
    	{{ RDS_R_L2 {} {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
	"ndas4 - source: createRDS merged frames"
    	{{ RDS_MERGED_L1 HL {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
    "ndas5 - source: raw frames for ndas"
    	{{ R {} {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
	"ndas6 - source: ndas frames"
    	{{ NDAS GHLV {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
	}
			
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--database} {string} \
    {database name} \
    { set ::QA::ndastest::test::dbname $::QA::Options::Value }
	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::ndastest::test {	

    	##-------------------------------------------------------------------
    	## Bring commonly used items into the local namespace
    	##-------------------------------------------------------------------
    	namespace import ::tcltest::*
    	namespace import ::QA::Debug::*
    	namespace import ::QA::TclTest::SubmitJob
    	namespace import ::QA::URL::List
	
	proc ndasdiff { diff_file } {
		if	{ [ catch {
		;# Read diff file
			set fid [open $diff_file r]
			set raw [split [read -nonewline $fid] "\n"]
			close $fid

			;# Collect data
			set section 0
			set line 0
			set age new
			foreach l $raw {
    				if {[regexp {(^\d+(,\d+)?c\d+(,\d+$)?)|(^---$)} $l]} {
        			;# Store # of lines for previous segment
        			set data($section,$age) $line
        				if {[regexp {^---$} $l]} {
            			set age old
        				} else {
            				incr section
            				set age new
        				}
        				set line 0
        				continue
    				}
    				incr line
    				set data($section,$age,$line) [split [string trimleft $l "<> "] ","]
			}

			;# Compare values
			;# Only compare first 2 sections
			;# as the rest will always differ
			set sect 1
			set max 0
			set save1 0
			set save2 0
			
			for {set line 1} {$line <= $data($sect,new)} {incr line} {
    				foreach val1 $data($sect,new,$line) val2 $data($sect,old,$line) {
        				set diff [expr {abs($val1 - $val2)}]
        				set avg  [expr {(abs($val1) + abs($val2)) / 2}]
        				set diff [expr {$diff / $avg}]
        				if 	{$diff > $max} {
            				set max $diff
            				set save1 $val1
            				set save2 $val2
        				}
    				}
			}
			set result "Largest Error 1: [format {%e} $max] ($save1 $save2)"

			set sect 2
			set max 0
			set save1 0
			set save2 0
			for 	{set line 1} {$line <= $data($sect,new)} {incr line} {
   	 			set val1 [lindex $data($sect,new,$line) end]
    				if 	{![string length $val1]} {
        				set val1 [lindex $data($sect,new,$line) end-1]
    				}

    				set val2 [lindex $data($sect,old,$line) end]
    				if 	{![string length $val2]} {
        				set val2 [lindex $data($sect,old,$line) end-1]
    				}

    				set diff [expr {abs($val1 - $val2)}]
    				set avg  [expr {(abs($val1) + abs($val2)) / 2}]
    				set diff [expr {$diff / $avg}]
    				if 	{$diff > $max} {
        				set max $diff
        				set save1 $val1
        				set save2 $val2
    				}
			}
			append result "\nLargest Error 2: [format {%e} $max] ($save1 $save2)"
		} err ] } {
			return -code error $err
		}
		return $result
	}
	
	
	set i 1

	foreach { desc framequery } $queryList {
		set cmd [ subst $cmdstem ]
		set outputdir ""
		set result ".*Your results:.+results*.xml.*"
		test ndastest:dataPipeline:wrapper:"$desc"  { i }  -body {
			if 	{[catch {SubmitJob job $cmd} err]} {
				LJdelete job
	    			return $err
			}
			set outputdir $job(outputDir)
			regexp {(/ldas_outgoing/jobs.+)} $outputdir -> outputdir 
			set reply $job(jobReply)
			set sortcmd "exec sort [ glob $outputdir/*.xml ] >& $::TMP/ndas$i"
			catch { eval $sortcmd } err
			LJdelete job
			set reply
    		} -match regexp -result $result	
		incr i
	}
	
	;## verification of xml files from raw and > level 0 frames
	set result ""
	for	{ set i 1 } { $i < 5 } { incr i 2 } {
		set j [ expr $i + 1 ]
		test ndastest:dataPipeline:wrapper:verifyndas:ndas$i-ndas$j {} -body { 
			catch { exec diff $::TMP/ndas$i $::TMP/ndas$j > $::TMP/ndas$i$j } err
			foreach line [ split $err \n ] {
				if	{ ! [ regexp {(wrapperAPI|process:process_id:0|child process exited abnormally)} $line ] } {
					set errflag 1
				}
			}
			if	{ ! [ info exist errflag ] } {
				set err ""
			} 
			set err
		} -match exact -result $result	
	}	

;## "Largest Error 1 (gcc-3.3.3): 2.670140e-01 (4.6543826e-04 6.0886529e-04)\n"
;## set	passtext    "Largest Error 1: 2.670140e-01 (4.6543826e-04 6.0886529e-04)\n"
;## append passtext "Largest Error 2: 2.108760e-06 (1.4819121093750001e+04 1.4819152343749999e+04)"

#set	passtext "Largest Error 1 (gcc-3.4.3): 2.216890e-01 (1.0296344e-04 8.2415230e-05)\n"
#append passtext "Largest Error 2 (gcc-3.4.3): 3.581250e-06 (5.8627944335937500e+03 5.8627734375000000e+03)"

;## gcc 3.3.6
#set	passtext "Largest Error 1: 2.670101e-01 (4.6544586e-04 6.0887286e-04)\n"
#append	passtext "Largest Error 2: 1.054381e-06 (1.4819117187499999e+04 1.4819132812500000e+04)"

## libfftw3-3.1.a
#set	passtext "Largest Error 1: 2.546553e-01 (1.4322062e-05 1.1086810e-05)"
#append passtext "Largest Error 2: 4.996982e-07 (5.8629125976562504e+03 5.8629155273437501e+03)"

;## gcc 4.0.3
#set	passtext "Largest Error 1: 2.541234e-01 (1.4317439e-05 1.1089225e-05)"
#append passtext "\nLargest Error 2: 4.996983e-07 (5.8629111328125000e+03 5.8629140625000000e+03)"

;## sunbuild replaced gateway 2/08/07
	set	result "Largest Error 1: 2.540225e-01 (1.4326955e-05 1.1097734e-05)"
	append result "\nLargest Error 2: 1.499096e-06 (5.8629033203125000e+03 5.8629121093750000e+03)"
	
	test ndastest:dataPipeline:wrapper:verify:ndas5-ndas6 {} -body { 
		catch { exec diff $::TMP/ndas5 $::TMP/ndas6 > $::TMP/ndas56 } err
		set data [ ndasdiff $::TMP/ndas56 ]
		set data
	} -match exact -result $result
	#catch { cd $::TMP; file delete -force ndas1 ndas2 ndas3 ndas4 ndas5 ndas56 ndas6 }

	flush [outputChannel]
    
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::ndastest::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::ndastest::test
	
