#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval ::QA::descMetaData::test {
	set ::QA::descMetaData::test::dbname ldas_tst
}

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--database} {string} \
    {database name} \
    { set ::QA::descMetaData::test::dbname $::QA::Options::Value 
	   puts "database $::QA::descMetaData::test::dbname" }
       
#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::descMetaData::test {
    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::TclTest::SubTests
    namespace import ::QA::URL::List

    ##-------------------------------------------------------------------
    ## Local variable declaration
    ##-------------------------------------------------------------------
    
    set JobDirPattern {[^/]+/[^/]+}
    set ErrorType1 {Error.+-type,-column must be none for -key}
    set ErrorType2 {\-key options are primary,foreign or none}
    set ErrorType3 {-type must be all or none}
    set ErrorType4 {-outputformat.+invalid}
    set ErrorType5 {Error.+no function specified}
    set outputformat LIGO_LW
    set returnprotocol "file://descMeta.xml"
    set cmd_descMetaData { descMetaData -database $dbname -outputformat [ list $outputformat ] -returnprotocol $returnprotocol}

    array set DefaultOptions {
        -table     "all"
        -column    "none"
        -type      "none"
        -key       "none"
    }

    array set Tests {
        AllCols:opt:-subject   "get column names from all columns"

        OneCol:opt:-table     "frameset"
        OneCol:opt:-column    "frameset_group"
        OneCol:opt:-subject   "get colname from frameset table - OK"

        PrimaryKey:opt:-table     "segment_definer"
        PrimaryKey:opt:-key       "primary"
        PrimaryKey:opt:-subject   "get primary key from segment_definer table"

        ForeignKey:opt:-table     "segment"
        ForeignKey:opt:-key       "foreign"
        ForeignKey:opt:-subject   "get foreign key from segment table"

        ForeignKeyNoRows:opt:-table     "process"
        ForeignKeyNoRows:opt:-key       "foreign"
        ForeignKeyNoRows:opt:-subject   "get foreign key from process table (none)"

        DataAttributesAll:opt:-table     "all"
        DataAttributesAll:opt:-type      "all"
        DataAttributesAll:opt:-subject   "get data attributes from all tables"

        DataAttributesOne:opt:-table     "filter"
        DataAttributesOne:opt:-type      "all"
        DataAttributesOne:opt:-subject   "get data attributes from one table (truncated results)"

        DataAttributesOneIlwd:outputformat   {ilwd ascii}
        DataAttributesOneIlwd:returnprotocol "file://descMeta.ilwd"
        DataAttributesOneIlwd:expected       "/ldas_outgoing/jobs/${JobDirPattern}/descMeta.ilwd"
        DataAttributesOneIlwd:opt:-table     "filter"
        DataAttributesOneIlwd:opt:-type      "all"
        DataAttributesOneIlwd:opt:-subject   "get data attributes from one table into ilwd ascii"

        ExTypeKey:expected       $ErrorType1
        ExTypeKey:opt:-table     "filter"
        ExTypeKey:opt:-type      "all"
        ExTypeKey:opt:-key       "primary"
        ExTypeKey:opt:-subject   "exception: type and key together"

        ExNoOpts:expected      $ErrorType5
        ExNoOpts:opt:-table    "none"
        ExNoOpts:opt:-subject  "exception: no options specified"

        ExKeyCol:expected      $ErrorType1
        ExKeyCol:opt:-table    "sngl_ringdown"
        ExKeyCol:opt:-column   "all"
        ExKeyCol:opt:-key      "primary"
        ExKeyCol:opt:-subject  "exception: column and key together"

        ExInvalidKey:expected      $ErrorType2
        ExInvalidKey:opt:-table    "xxxx"
        ExInvalidKey:opt:-key      "garbage"
        ExInvalidKey:opt:-subject  "exception: invalid key"

        ExInvalidType:expected      $ErrorType3
        ExInvalidType:opt:-table    "xxxx"
        ExInvalidType:opt:-type     "type"
        ExInvalidType:opt:-subject  "exception: invalid type"

        ExInvalidFormatILwdBinary:outputformat   {ilwd binary}
        ExInvalidFormatILwdBinary:expected      $ErrorType4
        ExInvalidFormatILwdBinary:opt:-table    "filter"
        ExInvalidFormatILwdBinary:opt:-type     "all"
        ExInvalidFormatILwdBinary:opt:-subject  "exception: invalid type"

        ExInvalidFormatILwdJunk:outputformat   {ilwd junk}
        ExInvalidFormatILwdJunk:expected      $ErrorType4
        ExInvalidFormatILwdJunk:opt:-table    "filter"
        ExInvalidFormatILwdJunk:opt:-type     "all"

        ExInvalidFormatFrame:outputformat   {frame}
        ExInvalidFormatFrame:expected      $ErrorType4
        ExInvalidFormatFrame:opt:-table    "filter"
        ExInvalidFormatFrame:opt:-type     "all"
    }

    SubTests Tests subtests
    foreach subtest $subtests {
        if {[llength [array names Tests "$subtest:*"]] <= 0} {
            break
        }
        if {[info exists Tests($subtest:returnprotocol)]} {
            ;## Custom
            set returnprotocol [subst $Tests($subtest:returnprotocol)]
        } else {
            ;## Default
            set returnprotocol "file://descMeta.xml"

        }
        if {[info exists Tests($subtest:outputformat)]} {
            ;## Custom
            set outputformat $Tests($subtest:outputformat)
        } else {
            ;## Default
            set outputformat LIGO_LW
        }
        if {[info exists Tests($subtest:expected)]} {
            ;## Custom
            set expected [subst $Tests($subtest:expected)]
        } else {
            ;## Default
            set expected "/ldas_outgoing/jobs/${JobDirPattern}/descMeta.xml"
        }

        test descMetadata:descMetaData:$subtest {} -body {
            ::QA::Command::ExtractOptions $subtest Tests opts
            ::QA::Command::OptionSubstitution DefaultOptions opts opts
            set scmd [ subst [concat $cmd_descMetaData [array get opts]]]
            Puts 5 "scmd $scmd"
            Flush
            if {[catch {SubmitJob job $scmd} err]} {
                return $err
            }
            set outfile [lindex $job(outputs) 0]
            LJdelete job
            return $outfile
        } -match regexp -result $expected 
    }

    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests

} ;## namespace - ::QA::descMetaData::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::descMetaData::test
