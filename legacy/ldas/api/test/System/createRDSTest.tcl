#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for createRDS user command 
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./createRDSTest_s3.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*createRDS_S3*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
package require tconvert

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::createRDSTest::test {

	;# E7 data
	#set startTime  693960000
	#set endTime    693967199

	;# S1 data
	#set startTime  714384000
	#set endTime    714387584

	;# E9 data LHO
	#set startTime  727491488
	#set endTime    727713056

	;# E9 data LLO
	#set startTime  727489712
	#set endTime    727722944

	;# E9 tri-conincident data
	#set startTime  727549920
	#set endTime    727553535

	;# S2 data
	#set startTime  730522192
	#set endTime    730525792
		
	;# S3 data
	set startTime  	751800000
	set endTime    	751803600

	set site          dev
	set user          createrdstest
	set type          R
	set usejobdirs    0
	set adjust        1 
	set outputdir     ""
	set usertype      ""
	set filechecksum  0
	set framechecksum 0
	set timecheck     1
	set datavalid     1
	set compressiontype  raw
	set compressionlevel 6
	#!orig! set rdsDir  "/frame10/rds/S1"

	set defaultJobLength 64
	set minJobLength     16
	set frameLength      16

	set jobCtr      0
	set rdsTime     0
	set procTime    0
	set totalHours  0
	set hourCtr     0
	set hourMod     2
	set hourWait    600
	set oneHour     3600
	set errWait     5

	set adcChanList [list]
	set adcFileList [list]

	set nullCmd "null or malformed command"
	set md5Err  "md5Challenge"
	set sockErr "socket closure"
#set noGPS   "file not found"
	set noGPS   "no such file"
	set fullQue "limited queue size"
	set noData  "no data found"
	set aborted "aborted after \[\\d\\.\]+ seconds"
	set timeOut "Timeout waiting"
	set exists  "file already exists"
	set badfile "Verification exit code"
	set user createrdstest
	set dump 0
	
	set usercmd {
    	createRDS
	   -subject {$subject}
        -times {$queryTime}
        -type {$type}
        -usertype {$usertype}
        -outputdir {$outputdir}
        -usejobdirs {$usejobdirs}
        -compressiontype {$compressiontype}
        -compressionlevel {$compressionlevel}
        -filechecksum {$filechecksum}
        -frametimecheck {$timecheck}
        -framedatavalid {$datavalid}
        -channels {$adcquery}
	}
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--adjust} {string} \
    {adjust frame time} \
    { set ::QA::createRDSTest::test::adjust $::QA::Options::Value }
::QA::Options::Add {} {--startTime} {string} \
    {frame start time} \
    { set ::QA::createRDSTest::test::startTime $::QA::Options::Value }
::QA::Options::Add {} {--compressionType} {string} \
    {compression type} \
    { set ::QA::createRDSTest::test::compressionType $::QA::Options::Value }
::QA::Options::Add {} {--homeDir} {string} \
    {home directory} \
    { set ::QA::createRDSTest::test::homeDir $::QA::Options::Value }  
::QA::Options::Add {} {--dump} {string} \
    {home directory} \
    { set ::QA::createRDSTest::test::dump $::QA::Options::Value }       
::QA::Options::Add {} {--adjust} {string} \
    {frame end time} \
    { set ::QA::createRDSTest::test::endTime $::QA::Options::Value }
::QA::Options::Add {} {--endTime} {string} \
    {database name} \
    { set ::QA::createRDSTest::test::endTime $::QA::Options::Value }
::QA::Options::Add {} {--adcFileList} {string} \
    {adc file list} \
    { lappend ::QA::createRDSTest::test::adcFileList $::QA::Options::Value } 
::QA::Options::Add {} {--useJobDirs} {string} \
    {use job directory - boolean} \
    { set ::QA::createRDSTest::test::usejobdirs $::QA::Options::Value }     
::QA::Options::Add {} {--defaultJobLength} {string} \
    {default frame output length} \
    { set ::QA::createRDSTest::test::defaultJobLength $::QA::Options::Value }
::QA::Options::Add {} {--minJobLength} {string} \
    {default minimum job length} \
    { set ::QA::createRDSTest::test::minJobLength $::QA::Options::Value }
::QA::Options::Add {} {--outputdir} {string} \
    {job output directory} \
    { set ::QA::createRDSTest::test::outputdir $::QA::Options::Value }
::QA::Options::Add {} {--type} {string} \
    {frame type} \
    { set ::QA::createRDSTest::test::type $::QA::Options::Value }   
::QA::Options::Add {} {--usertype} {string} \
    {createRDS frame user type} \
    { set ::QA::createRDSTest::test::usertype $::QA::Options::Value }
::QA::Options::Add {} {--user} {string} \
    {ldas user name} \
    { set ::QA::createRDSTest::test::user $::QA::Options::Value }
::QA::Options::Add {} {--datavalid} {string} \
    {check for frame valid data - boolean} \
    { set ::QA::createRDSTest::test::datavalid $::QA::Options::Value }
::QA::Options::Add {} {--filechecksum} {string} \
    {frame checksum} \
    { set ::QA::createRDSTest::test::filechecksum $::QA::Options::Value }               
::QA::Options::Add {} {--framechecksum} {string} \
    {frame file checksum} \
    { set ::QA::createRDSTest::test::framechecksum $::QA::Options::Value }   
::QA::Options::Add {} {--timecheck} {string} \
    {time check - boolean} \
    { set ::QA::createRDSTest::test::timecheck $::QA::Options::Value }
::QA::Options::Add {} {--compressionLevel} {string} \
    {frame compression level} \
    { set ::QA::createRDSTest::test::compressionLevel $::QA::Options::Value }    
                	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::createRDSTest::test {

	##-------------------------------------------------------------------
    	## Bring commonly used items into the local namespace
    	##-------------------------------------------------------------------
    	namespace import ::tcltest::*
    	namespace import ::QA::Debug::*
    	namespace import ::QA::TclTest::SubmitJob
    	namespace import ::QA::URL::List
	namespace import ::QA::Rexec
		
	proc buildQuery {adcChanList} {
		if	{ [ catch {
    			set query [list]
    			foreach line $adcChanList {
        			set line [string trim $line]	
        			# Skip blank and comment lines
        			if {![string length $line] || [regexp {^\#} $line]} {
            			continue
        			}

        			set adc {}
        			set dec {}
        			foreach {adc dec} $line {break}
        			if 	{![string length $dec] || $dec == 1} {
            			lappend query $adc
        			} else {
            			lappend query "${adc}!$dec"
        			}
    			}
		} err ] } {
			return -code error $err 
	 	}
    		return $query
	}
	
	proc openLogFile {logFile} {
		if	{ [ catch {
    		if {[file exists $logFile]} {
        		set timestamp [clock format [file mtime $logFile] -format "%m%d%H%M%S"]
        		catch {file rename -force -- $logFile $logFile.${timestamp}}
    		}
    		set oid [open $logFile a]

    		set clockSeconds [clock seconds]
    		set locTime [clock format $clockSeconds -format %c]
    		set utcTime [clock format $clockSeconds -format %c -gmt 1]
    		#Puts 1 "\nStart reduced dataset:  LOC $locTime  UTC $utcTime"
    		puts $oid "Start reduced dataset:  LOC $locTime  UTC $utcTime"
    		flush $oid
		} err ] } {
			return -code error $err
		}
    		return $oid
	}
	
	proc closeLogFile {oid rdsTime procTime wallTime jobCtr} {
	
		if	{ [ catch {
    		#Puts 1 "\nTotal data processed  :  $rdsTime seconds ([duration $rdsTime])"
    		#Puts 1 "Total processing time :  $procTime seconds ([duration $procTime])"
    		#Puts 1 "Total elapsed time    :  $wallTime seconds ([duration $wallTime])"
    		#Puts 1 "Total jobs submitted  :  $jobCtr jobs"
    		puts $oid "Total data processed  :  $rdsTime seconds ([duration $rdsTime])"
    		puts $oid "Total processing time :  $procTime seconds ([duration $procTime])"
    		puts $oid "Total elapsed time    :  $wallTime seconds ([duration $wallTime])"
    		puts $oid "Total jobs submitted  :  $jobCtr jobs"

    		set clockSeconds [clock seconds]
    		set locTime [clock format $clockSeconds -format %c]
    		set utcTime [clock format $clockSeconds -format %c -gmt 1]
    		puts $oid "End reduced data set :  LOC $locTime  UTC $utcTime"

    		catch {close $oid}
		} err ] } {
			return -code error $err
		}
	}
	
	proc checkExistingFrames {} {
				
		set time [ clock format [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
		set outputdir [ set ::QA::createRDSTest::test::outputdir ]
		if	{ [ catch {
			;## check for frames before submitting job
			#set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
			#\"ls -ilR  $outputdir\""
			set cmd "exec ls -ilR $outputdir"
			catch { eval $cmd } data
			puts $::QA::createRDSTest::test::oid "$time: check frames in $outputdir\n$data"
		} err ] } {
			puts $::QA::createRDSTest::test::oid "$time: $err"
		}
	}
	
	
	proc runTest { cmd queryTime subject user { varlist ""} } { 
 
		test $subject {} -body {  
	    	if 	{[catch {SubmitJob job $cmd $user } err]} {
				set jobnum none 
				catch { set jobnum $job(jobnum) err }
				checkExistingFrames
				LJdelete job
				return [ list $jobnum $err ]
	    		}                
			set retval [list]
    			foreach var $varlist {
        			lappend retval $job($var)
    			}
			# Puts 1 $::QA::createRDSTest::test::oid "\njob $job(jobnum)\n$job(jobReply)"	
    			LJdelete job
    	} -result {}
		return $retval
    }
	
	proc duration {time {format "dhms"}} {
    		array set div {d 86400 h 3600 m 60 s 1}
    		array set str {d day h hour m minute s second}

    		if 	{$time < 0} {
       	 	set time 0
    		} elseif {$time < 1} {
        		set time [format "%g" $time]
    		} elseif {$time >= 1} {
        		set time [string trimleft $time 0]
    		}
	
    		set retval [list]
   	 	foreach u [split [string tolower $format] {}] {
        	foreach {secs frac} [split $time .] {break}
        	set n [expr {$secs / $div($u)}]

        	if {[string equal "s" $u]} {
            	set n $time
        	}

        	if {$n == 0} {
            	continue
        	}

        	if {$n != 1} {
            	append str($u) "s"
        	}

        	lappend retval "$n $str($u)"
        	set time [expr {$time - $n * $div($u)}]
    	}

    	if {![string length $retval]} {
        	lappend retval "0 $str([string index $format end])s"
    	}

   	 	return [join $retval ", "]
	}
	
	;## main here
	
	if 	{$adjust} {
    		set startTime [expr {$startTime + 16}]
    		set endTime [expr {$endTime - 16}]
	}

	;## Read adc list from file
	if 	{![llength $adcFileList]} {
    		return -code error "No channel list provided."
	}
	
	foreach adcFile [join $adcFileList] {
    		if {[catch {open $adcFile r} fid]} {
        		return -code error "Error opening file '$adcFile': $fid"
    		}
    		lappend adcChanList [split [read -nonewline $fid] "\n"]
    		catch {close $fid}
	}
	
	if 	{![llength $adcChanList]} {
    		return -code error "Empty channel list."
	}
	set adcquery [buildQuery [join $adcChanList]]

	set logFile [ file rootname [file tail $::argv0]]_${site}_${::IFO}.log
	set logFile [ file join $::TMP [ file tail $logFile ] ]
	Puts 1 "logfile $logFile"

	;## outputs details to log file
	set oid [openLogFile $logFile]
	set __tMark [clock seconds]

	set jobLength $defaultJobLength
	set timeS $startTime
	set timeE [expr {$timeS + $jobLength - 1}]

	if	{ ! [ info exist outputdir ] } {
		set outputdir /scratch/test/system/$::SITE
	}

	set user "-user [ set ::QA::createRDSTest::test::user ]"

	while {$timeS < $endTime} {
    		set __tStart [clock seconds]

    		if 	{[expr {fmod($timeS,$frameLength)}] != 0} {
        		puts $oid "GPS time $timeS is not divisible by $frameLength"
        		flush $oid
        		break
    		}

    		while {$timeE > $endTime} {
        		set timeE [expr {$timeE - $frameLength}]
        		set jobLength [expr {$timeE - $timeS + 1}]
    		}
    		if 	{$jobLength < $minJobLength} {
        		puts $oid "Query time ($timeS-$timeE) is less than the minimum of $minJobLength seconds"
        		flush $oid
        		break
    		}

    		set queryTime "$timeS-$timeE"

    		puts $oid "\n--------------"
    		puts $oid "rdsTime      :  $rdsTime seconds ([duration $rdsTime])"
    		puts $oid "procTime     :  $procTime seconds ([duration $procTime])"
    		puts $oid "JobCtr       :  [incr jobCtr]"
    		puts $oid "JobLength    :  $jobLength seconds"
    		puts $oid "queryTime    :  $queryTime  [tconvert $timeS]\n"

    		set subject "createRDSTest:frame:createRDS:$queryTime:$::IFO"
    		set cmd [ subst $usercmd ]
    
    		;## error handling
    		if 	{ [ catch { runTest $cmd $queryTime $subject $user [list status jobReply outputs jobnum jobTime] } msg ] } {
        		set jobError ""
        		if 	{[llength $msg] != 2} {
            		set jobError [join $msg]
            		puts $oid "\nError: [string trim $jobError]"
            		flush $oid
        		} else {
            		foreach {jobnum jobError} $msg {break}
            		puts $oid [format "Error: %05d\n%s" $jobnum [string trim $jobError]]
            		flush $oid
        		}

        		if 	{ [regexp -nocase -- "$noGPS|$noData" $jobError] && [regexp {\d{9}} $jobError missingGPS] } {
            		puts $oid "missing GPS:  $missingGPS"
            		flush $oid

            		if 	{ $missingGPS > $timeS } {
                		set timeE [expr {$missingGPS - $frameLength - 1}]
                		set jobLength [expr {$timeE - $timeS + 1}]
                		if 	{$jobLength >= $minJobLength} {
                    		puts $oid "Adjusting job length to remove frame gap"
                    		flush $oid
                    		continue
                		}
            		} elseif {$missingGPS < $timeS} {
                		set timeS [expr {$timeS + $frameLength}]
            		} else {
                		set timeS [expr {$missingGPS + 2*$frameLength}]
            		}

            		;## $missingGPS <= $timeS || $jobLength < $minJobLength
            		puts $oid "Advancing job start time to find next frame"
            		flush $oid

            		set jobLength $defaultJobLength
            		set timeE [expr {$timeS + $jobLength - 1}]
            		continue
        	  	} elseif {[regexp -nocase -- $exists $jobError] && [regexp {(\d{9})\-\d+\.gwf} $jobError -> existingGPS]} {
            		puts $oid "existing GPS: $existingGPS"
            		flush $oid

            		set jobLength $defaultJobLength
            		set timeS [expr {$existingGPS + $frameLength}]
            		set timeE [expr {$timeS + $jobLength - 1}]
            		continue
        		} elseif {[regexp -nocase -- $badfile $jobError] && [regexp {(\d{9})\-\d+\.gwf} $jobError -> badGPS]} {
            		puts $oid "bad GPS: $badGPS"
            		flush $oid

            		set jobLength $defaultJobLength
            		set timeS [expr {$badGPS + 2*$frameLength}]
            		set timeE [expr {$timeS + $jobLength - 1}]
            		continue
        		} elseif {[regexp -nocase -- "$sockErr|$fullQue|$nullCmd|$md5Err|$aborted|$timeOut" $jobError]} {
            		puts $oid "\nresubmitting job in $errWait seconds..."
            		after [expr {$errWait * 1000}]
            		continue
        		}
        		break
    		}

    		foreach {status reply outFrames jobnum jobTime} $msg {break}
    		set nFramesOut [llength $outFrames]
   
    		puts $oid "Job $jobnum took $jobTime secs\n#output frames:  $nFramesOut\n[ join $outFrames \n]"

    		set rdsTime [expr {$rdsTime + $nFramesOut * $frameLength}]
    		set procTime [expr {$procTime + $jobTime}]


    		if 	{$nFramesOut != ($jobLength / $frameLength)} {
        		puts $oid "Invalid number of output frames!"
        		break
    		}

    		;## verify output frames are continuous
    		foreach frame $outFrames {
    			if	{ [ regexp {(\d+)-(\d+).gwf} $frame -> frametime delta ] } {
				if	{ ! [ info exist previousframe ] } {
					set prevousframe $frametime 
					set previous_delta $delta
					continue				
				}
				if	{ $frametime != [ expr $previousframe + $previous_delta ] } {
					puts $oid "error $frametime not continuous with $previousframe + $delta"
				} else {
					puts $oid "no gaps $previousframe + $delta to $frametime"
				}
    			}
    		}
    		set jobLength $defaultJobLength
    		set timeS [expr {$timeS + $jobLength}]
    		set timeE [expr {$timeS + $jobLength - 1}]
    
	} ;## end while loop

	puts $oid "\nGrand Total data processed  :  $rdsTime seconds ([duration $rdsTime])"
	puts $oid "Grand Total processing time :  $procTime seconds ([duration $procTime])"
	puts $oid "Grand Total elapsed time    :  $wallTime seconds ([duration $wallTime])"
	puts $oid "Grand Total jobs submitted  :  $jobCtr jobs"

	flush $oid
	set wallTime [expr {[clock seconds] - $__tMark}]
	closeLogFile $oid $rdsTime $procTime $wallTime $jobCtr

	# flush [outputChannel]
     flush $::tcltest::outputChannel
    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::createRDSTest::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::createRDSTest::test
exit
	
	
	
		
