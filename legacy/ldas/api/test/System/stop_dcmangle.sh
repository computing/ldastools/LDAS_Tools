#!/bin/sh
platform=`uname`
ldasname=`cat /ldas/lib/cmonClient/ldas_version.tcl`
if	[ "$platform" = "Linux" ] || [ "$ldasname" = "set ::version 1.8.0" ]
then
	/ldcg/bin/tclsh /ldas_usr/ldas/test/bin/stop_dcmangle.tcl &
else
	/ldcg/bin/64/tclsh /ldas_usr/ldas/test/bin/stop_dcmangle.tcl &
fi
