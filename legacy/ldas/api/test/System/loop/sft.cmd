dataPipeline
    -subject { sftRequest }
    -returnprotocol http://testGetSFTs.xml

    -np 2
    -multidimdatatarget ligolw
    -datacondtarget wrapper
    -database ldas_tst

    -concatenate 0
    -autoexpand 1
    -allowgaps 1

    -dynlib libldasknownpulsardemod.so
    -filterparams (2,1,730522208,2.44140625e-4,7127040,60,245760,16,5.147162148,0.376696069,0.0,1283.5,2,-8.663317101e-14,0.0,0.0,0.0,0.0,'n',0,0.0,0.0,2.0,0.0,1,0.0,0.0,0.0,0.0,0.0,0.0,1,"1","LLO","J1939",input,16,216.0,1281.7,3.6,1,1)

    -framequery {
        { SFT_H2_60_P_1 Z {} {730522208-730523947} Proc(0!1281.7!3.6FREQ!) }
    }
    -responsefiles {
        file:/lal/share/lal/earth03.ilwd,pass
        file:/lal/share/lal/sun03.ilwd,pass
    }
    -aliases sft
    -algorithms {
        output(_chN,,,_chN,_chN data);
    }
