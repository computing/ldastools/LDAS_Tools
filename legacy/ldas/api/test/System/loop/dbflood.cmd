putStandAlone
    -subject {database flood}
    -metadataapi metadata
    -multidimdatatarget ligolw
    -returnprotocol http://dbflood
    -database ldas_tst
    -wrapperdata {
        http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/jobs/LDASTest/putStandAlone/dbflood/process_1.ilwd
        http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/jobs/LDASTest/putStandAlone/dbflood/output_1.ilwd
        http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/jobs/LDASTest/putStandAlone/dbflood/process_2.ilwd

    }
