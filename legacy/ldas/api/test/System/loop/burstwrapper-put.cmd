putStandAlone
    -subject {putStandAlone burstwrapper loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/burstwrapper/process_1.ilwd  
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/burstwrapper/process_2.ilwd 
    }
    -database {ldas_tst}
