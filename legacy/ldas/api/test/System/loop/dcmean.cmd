conditionData
    -returnprotocol { http://mean.ilwd }
    -outputformat { ilwd ascii }
    -framequery { R H {} 730524000-730524063 Adc(H1:LSC-AS_Q) }
    -aliases { asq = LSC-AS_Q; }
    -algorithms {
        m = mean(asq);
        output(m,_,_,m,mean of asq);
    }
