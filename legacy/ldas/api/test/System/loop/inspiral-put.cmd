putStandAlone
    -subject {putStandAlone inspiral loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
	-multidimdatatarget ligolw
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/inspiral/process_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/inspiral/output_1.ilwd 
		file:/ldas_outgoing/jobs/LDASTest/putStandAlone/inspiral/output_2.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/inspiral/process_2.ilwd 
    }
    -database {ldas_tst}
