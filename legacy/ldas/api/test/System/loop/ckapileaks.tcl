#! /bin/sh
# the next line restarts using wish \
exec tclsh "$0" "$@"

proc getAPILeaks { {apilist {}} } {
	
    if	{ ! [ llength $apilist ] } {
		set apilist $::API_LIST
	}
	
	foreach api $apilist {
    	catch { file delete /ldas_outgoing/${api}API/leaks.log }
    }
    set cmd checkLeaks
    
    foreach api $apilist {
    	catch { exec echo "*** $api ***" >> leaks_report }
    	if	{ [ catch {
            getEmergData $api $cmd
            catch { exec cat /ldas_outgoing/${api}API/leaks.log >> leaks_report }
		} err ] } {
			catch { exec echo "error reaching $api: $err" >> leaks_report } 
		}
	}
	set nedit [ auto_execok nedit ]
	if	{ [ string length $nedit ] } {
		catch { exec nedit leaks_report & } 
	}
	puts $cid "View /ldas_outgoing/cntlmonAPI/leaks_report"
    
}

catch { file delete leaks_report }
getAPILeaks
