# inspiral_test_macho.pipe
dataPipeline
    -subject { Macho Inspiral Search }

    -dynlib libldasinspiral.so
    -filterparams (2097152,7,1048576,70.0,131072,69.0,8,0.9,1,8.0,20.0,4,2,0,1,1,(0.5,0.6))

    # to use with larger clusters:
    # -filterparams (2097152,7,1048576,70.0,131072,69.0,8,0.9,1,8.0,20.0,4,30,0,2)

    -np 4
    -datacondtarget wrapper
    -metadatatarget datacond
    -multidimdatatarget ligolw
    #-metadataapi metadata
    -metadataapi tee
    -database ldas_tst

    -framequery {
        {R L {} 730523744-730525792 adc(L1:LSC-AS_Q!resample!4!)}
        {CAL_REF_V03_L1 L {} 731488397-731488460 proc(L1:CAL-CAV_GAIN!0!7000.0001!,L1:CAL-RESPONSE!0!7000.0001!)}
        {CAL_FAC_V03_L1 L {} 730523744-730525792 proc(L1:CAL-OLOOP_FAC,L1:CAL-CAV_FAC)}
    }
    -responsefiles {
        /ldas_outgoing/jobs/responsefiles/l1-test-tmpltbank-macho.ilwd,pass
    }
    -aliases {
        rawchan = LSC-AS_Q;
        cav_gain = CAL-CAV_GAIN;
        resp = CAL-RESPONSE;
        cav_fac = CAL-CAV_FAC;
        oloop_fac = CAL-OLOOP_FAC;
    }
    -algorithms {
        chan = slice( rawchan, 10, 8388608, 1);
        output(chan,_,_,_,channel);
        output(cav_gain,_,_,_,CAL-CAV_GAIN);
        output(resp,_,_,_,CAL-RESPONSE);
        cav_fac_cplx = float( cav_fac );
        cav_fac_cplx = complex( cav_fac_cplx );
        output(cav_fac_cplx,_,_,_,CAL-CAV_FAC);
        oloop_fac_cplx = float( oloop_fac );
        oloop_fac_cplx = complex( oloop_fac_cplx );
        output(oloop_fac_cplx,_,_,_,CAL-OLOOP_FAC);
    }
