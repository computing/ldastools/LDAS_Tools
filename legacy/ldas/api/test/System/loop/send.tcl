#!/bin/sh
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}
#exec tclsh "$0" ${1+"$@"}

source ./joblist.tcl
foreach port [ concat $meta_data $pipelines ] {
    set sid [ socket [ info hostname ] $port ]
    puts $sid [lindex $::argv 0]
    flush $sid
    puts [gets $sid]
    close $sid
}
