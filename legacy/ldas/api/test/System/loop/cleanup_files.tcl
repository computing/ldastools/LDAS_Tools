#! /ldcg/bin/tclsh

proc cleanup_files {} {
	
	;## remove old data
	set curtime [ clock seconds ]
	;## purge any log specified time
	
	foreach file [ glob -nocomplain * ] {
		set modtime [ file mtime $file ]
		if	{ $modtime < $::LIMIT } {
			set time [ clock format $modtime -format "%m-%d-%Y %H:%M:%S" ] 
			puts "purge $file $time"
			file delete $file
		}
	}
}

set ::LIMIT [ clock scan "01/01/05" ]
cleanup_files
