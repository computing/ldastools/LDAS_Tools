#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
exec tclsh "$0" ${1+"$@"}

lappend ::auto_path /ldas/lib
if      { ! [ regexp "/ldas/bin" $::env(PATH) ] } {
        set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

source /ldas_usr/ldas/test/bin/AllTest.rsc

;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

package require LDASJob

cd $::WORKDIR

set ::env(PATH) "$::BINDIR:$::env(PATH)"

;## script to run createRDS gmendell style

set subdirs [ list ]
# set workdirs [ list TESTCODEDEV_L1 TESTCODEDEV_L3 TESTCODEDEV_MD5SUMREGEX ]
set workdirs [ list TESTCODEDEV_L1 TESTCODEDEV_L3 ]

foreach workdir $workdirs {
	if	{ ! [ file exist $workdir ] } {
		file mkdir $workdir
		file copy $::BINDIR/createrds.rsc.$workdir $workdir/createrds.rsc
		puts "created $workdir"
	}
}

#set workdirs [ lsort [ glob TESTCODEDEV_MD5SUMREGEX* ] ]

set shutdown "./shutdown_$argv0"
if	{ [ file exist $shutdown ] } {
	file delete $shutdown
}

proc cleanup { workdir } {

	catch { exec touch ${::WORKDIR}/$workdir/sHuTdOwN }
	after 100000
	set tmpname $::outputdir/$::output_subdir($workdir)[clock seconds]
	catch { exec mv -f $::outputdir/$::output_subdir($workdir) $tmpname } err 
	puts "mv $::outputdir/$::output_subdir($workdir) to $tmpname $err"
	catch { exec rm -rf $::outputdir/$::output_subdir($workdir)tmp & } err
    puts "$workdir $::outputdir/$::output_subdir($workdir) removed \
		[ file exist $::outputdir/$::output_subdir($workdir) ]"
	after 10000
	catch { file delete ${::WORKDIR}/${workdir}/sHuTdOwN }
	catch { file delete [ glob -nocomplain ${::WORKDIR}/$workdir/createrds.*.lock ] } err
	catch { file delete ${::WORKDIR}/${workdir}/createrds.rec } 

}


proc update_rscfile { rscfile } {

	set fd [ open $rscfile r ]
	set text ""
	set lines [ read -nonewline $fd ]
	close $fd 
	foreach line [ split $lines \n ] {
		if	{ [ regexp {set[\s\t]*filechecksum\s+(\d+)} $line -> checksum ] } {
			if	{ ! $checksum } {
				set checksum 1
			} else {
				set checksum 0
			}
			puts "[pwd]/$rscfile checksum set to $checksum"
			append text "set\tfilechecksum $checksum\n"
		} else {
			append text "$line\n"
		}
	}
	set fd [ open $rscfile w ]
	puts -nonewline $fd [ string trim $text \n ]
	close $fd
}

proc runrds {subdir} {
	
	cd ${::WORKDIR}/$subdir 
	;## update filechecksum
	set rscfile createrds.rsc 
	update_rscfile $rscfile	
	puts "updated $rscfile"
	catch { exec createrds.tcl >& createrds.out & } data
	if	{  [ regexp {(\d+)} $data -> pid ] } {
	    set ::thispid($subdir) $pid
		puts "restarted createRDS in $subdir pid $::thispid($subdir)"
	}
	cd $::WORKDIR  

}

proc check_shutdown {} {

	if	{ [ file exist $::shutdown ] } {
		puts "test cancelled"
		foreach subdir $::workdirs {
			cleanup $subdir
			catch { exec kill -9 $::thispid($subdir) } err
		}
		exit
	}
	after 60000 check_shutdown
}

proc get_md5sum_dir { md5sumregexp outputdir } {

	if	{ [ catch {
			if	{ [ regexp {s,(\S+),(\S+),([gi]*)} $md5sumregexp -> md5pat md5result global ] } {
            	puts "md5pat $md5pat, md5result $md5result global $global"
           	;## throw error for  spaces
           		if	{ ! [ string is print $md5result ] } {
           			error "md5sum directory specification contains non-printable characters '$md5result'"
           		}
                set flags ""
                if	{ [ string first g $global ] > -1 } {
                	append flags "-all "
                }
                if	{ [ string first i $global ] > -1 } {
                	append flags "-nocase "
                }
                append flags "--"
                if	{ ! [ string length $md5result ] } {
                	error "No md5sum replacement specified in -md5sumregexp $md5sumregexp"
                }
                set rc [ eval regsub $flags $md5pat $outputdir $md5result dir ]
                puts "'regsub $flags $md5pat $outputdir $md5result rc=$rc, dir $dir'"
			} else {
				error "invalid md5sum specification $md5sumregexp, must be s,regexp,replacement,\[gi\]"
			}
	} err ] } {
		return -code error $err
	}
	return $dir
}

;## main

foreach workdir $::workdirs {
	set interp [ interp create ]
	$interp eval source $::BINDIR/AllTest.rsc 
	catch { $interp eval source $::WORKDIR/AllTest.rsc }
	$interp eval source $workdir/createrds.rsc 
	set ::outputdir [ $interp eval set outputdir ] 
	set subdir [ $interp eval set ::userType ]
	set ::output_subdir($workdir) "H-${subdir}-7960"
	puts "output dir $::outputdir $::output_subdir($workdir)"
	cleanup $workdir
	set use_diff_md5sumdir [ $interp eval info exist md5sumregexp ] 
	if	{ $use_diff_md5sumdir } {
		set md5sumdir [ get_md5sum_dir [ $interp eval set md5sumregexp ] $::outputdir/$::output_subdir($workdir) ]  
		catch { exec rm -rf $md5sumdir }
	}
}

killer  createrds\.tcl

foreach subdir $::workdirs {
	runrds $subdir
}

after 60000 check_shutdown 
after 100000

while 1 {
	set index 0
	set done 0
	check_shutdown 
    foreach subdir $::workdirs {
		if	{ [ file exist ${::WORKDIR}/${subdir}/logs/createrds.log ] } {
			catch { exec tail -2 ${::WORKDIR}/${subdir}/logs/createrds.log } data
			puts "$subdir $data"
			if	{ [ regexp {No new data in disk cache, waiting} $data ] } {
				set done 1
			}
		} else {
		    puts "No files ${::WORKDIR}/${subdir}/logs/createrds.log"
			set done 1
		}
		if	{ $done } {
			cleanup $subdir
			catch { exec kill -9 $::thispid($subdir) } err
			puts "removed $subdir pid $::thispid($subdir) $err"
			runrds $subdir
		}
	}
	after 1000000
}
