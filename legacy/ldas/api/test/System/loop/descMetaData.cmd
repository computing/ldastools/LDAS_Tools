descMetaData
    #-outputformat {ilwd ascii}
    -outputformat {LIGO_LW}
    -database ldas_tst
	-subject {get primary keys from segment table}
	-table  "segment"
    -column "none"
    -type   "none"
    -key    "primary"
	-returnprotocol http://foo.xml
	-metadatatarget metadata
