#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

if	{ $argc == 1 } {
	set ::SITE [ lindex $argv 0 ]
}

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

set cwd [ pwd ]
if	{ ! [ string equal $cwd $::DMTDIR ] } {
	cd $::DMTDIR 
}

set localport 0
set start_time [ clock format [ clock seconds ] -format "%x %X %Z" -gmt 0 ]

;## starts and monitors dmt runs

proc getPid { pattern } {
	set pids [ list ]
	catch { exec ps -A -o "pid,args" | grep $pattern } data
	foreach line [ split $data \n ]  {
		if	{ [ regexp {(\d+).+sh} $line -> pid ] } {
			lappend pids $pid
		}
	}
	return $pids 
}

proc startdmt {} {	

	set msg "$::start_time: restarting dmt"
	set now [ clock seconds ]
	catch { file copy -force dmt.log dmt.log.$now }
	catch { exec SeqInsert purge &} err
	after 2000
    append msg "\nremoved any files in SeqInsert queue: $err"
    catch { exec SeqInsert shutdown & } err
    append msg "\nshut down any running SeqInsert server: $err"
	after 5000 
	killer insertTrigsLoop
	killer SeqInsert	
	catch { exec insertTrigsLoop.test ldas_tst $::TRIGGERS_DIR  100000 >& dmt.log & } err
	append msg "\n new insertTrigsLoop $err"
	puts $msg
	cd $::cwd
}

;## restart dmt if log file has not been updated for 30 min
proc monitorDMT {} {

	if	{ [ file exist dmt.log ] } {
		set fmodtime [ file mtime dmt.log ]
		set curtime [ clock seconds ]
		if	{ [ expr $curtime - $fmodtime ] > 1800 } {
			set pids [ getPid deltriggers ] 
			if	{ ! [ string length $pids ] } {				
		 		startdmt
			}
		} else {
			catch { exec tail -100 dmt.log | grep -c finish } finished
			catch { exec tail -100 dmt.log | grep -c submit } submitted
			catch { exec tail -100 dmt.log | grep -c queued } queued
			regexp {(\d+)} $finished -> finished
			regexp {(\d+)} $submitted -> submitted
			regexp {(\d+)} $queued -> queued
			if	{ ! $submitted && ! $finished && $queued } {
				startdmt
			}
		}
	} else {
		startdmt
	}
}

;#@# check if ldas is up

proc checkldas {} {
	
	catch { exec  pgrep managerAPI } data
	if	{ [ regexp {^(\d+)$} $data ] } {
		return 1
	}
	return 0
}

proc serverOpen {{port 0}} {
    set lsid [socket -server serverCfg -myaddr [info hostname] $port]
    foreach {::LADDR ::LHOST ::LPORT} [fconfigure $lsid -sockname] {break}
    return lsid
}

proc serverCfg  {sid addr port} {
    fconfigure $sid -buffering line -blocking off
    fileevent $sid readable [list getResult $sid]
    return
}

proc serverClose {sid} {
    catch {close $sid}
    return
}

proc getResult {sid} {
    set i 0
    set data {}
    set datum {}

    while {1} {
        if {[catch {read $sid} datum]} {
            break
        }
        if {[string length $datum]} {
            append data $datum
            set i 10
            continue
        } else {
            incr i 10
            if {$i > $::TIMEOUT_RECV} {break}
        }
        if {[eof $sid]} {break}
        after $i
    }

    ;## strip timing data
    set timing ""
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set timing [string range $data $idx end]
        set data [string replace $data $idx end]
    }
    regsub -all {\n} $data " " data
    set reply [string trim $data]
    fileevent $sid readable {}

    set msg {}
    set cmd {}
    set rest {}
    regexp -- {(\S+)\s*(.*)} $reply -> cmd rest
    if {![string length $cmd]} {
        catch {close $sid}
        return
    }
    if {![string length $rest]} {
        set rest {{}}
    }
    set cmd [string toupper $cmd]
	eval $cmd
	
    catch {close $sid}
    return
}
;## restart dev tests if no activity in 30 minutes 
;## unless ldas is down 
;## check dmt every 10 minutes
;## will not restart if dmt is already up

;## starts dmt once and monitors it

set ::SID [serverOpen $localport]
puts "SOCK: $::LADDR $::LHOST $::LPORT"
set cwd [ pwd ]
regsub {/export/ldcg_server} $cwd {} cwd
puts "work directory $cwd"
startdmt

after 10000

;## check every 5 mins
while 1 {
	set status [ checkldas ] 
	set start_time [ clock format [ clock seconds ] -format "%x %X %Z" -gmt 0 ]
	if	{ $status } {
		monitorDMT
	}
	after 600000
}
	
	



