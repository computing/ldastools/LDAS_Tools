getMetaData
    -returnprotocol { http://getmeta.xml }
    #-outputformat {ilwd ascii}
    -outputformat {LIGO_LW}
    -database ldas_tst
    #-sqlquery {select * from runlist fetch first 1 rows only}
    -sqlquery {select * from sngl_inspiral fetch first 1000 rows only}

