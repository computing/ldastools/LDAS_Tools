dataPipeline
    -subject { sftJob }
    -returnprotocol file:/H-SFT_H2_60_P-730524884_60.gwf

    -np 2
    -multidimdatatarget frame
    -database ldas_tst
    -datacondtarget wrapper

    #-outputdir /ldas_outgoing/testSFToutputdir
    -usertype SFT_H2_60_P
    -usejobdirs 0

    -dynlib libldasknownpulsardemod.so
    -filterparams (0,1,730524884,2.44140625e-4,245760,60,245760,1,0.0,0.0,0.0,1,0.5,0.0,0.0,0.0,0.0,0.0,'n',3,0.0,0.0,2.0,0.0,1,0.0,0.0,0.0,0.0,0.0,0.0,1,"H2","LHO","NA","H2:LSC-AS_Q")

    -framequery { 
        { R H {} 730524868-730524959 Adc(H2:LSC-AS_Q!resample!4!)  }
        { R H http://www.ldas-sw.ligo.caltech.edu/~ldas/P-clean_locks_H2_seg-693961017-2048.gwf {} Adc(H2:qc1) }
    }
    -aliases {
        rgw=H2\:LSC-AS_Q::ProcData;
        qc1=H2\:qc1::AdcData;
    }
    -algorithms {
        sqc1 = slice(qc1,0,2048,1);
        srgw = slice(rgw,65546,245760,1);
        output(srgw,,,,slice rgw);
    }
