conditionData
    -framequery {
      { R H {} 855975200-855975560 Adc(H1:LSC-AS_Q) }
    }
    -aliases {
      asq = LSC-AS_Q;
    }
    -algorithms {
      x = slice(asq, 0, 64, 1);
      srate = getSampleRate(x);
      st = getStepSize(x);
      x2 = getElement(x, 2);
      setElement(x, 2, 1.0);
      x2new = getElement(x, 2);
      output(srate, _, _, srate, foo);
      output(st, _, _, st, foo);
      output(x2, _, _, x2, foo);
      output(x2new, _, _, x2new, foo);

      x = psd(x, 64);
      st = getStepSize(x);
      x2 = getElement(x, 2);
      setElement(x, 2, 1.0);
      x2new = getElement(x, 2);
      output(st, _, _, st, foo);
      output(x2, _, _, x2, foo);
      output(x2new, _, _, x2new, foo);
    }
    -outputformat { ilwd ascii }
    -datacondtarget datacond

