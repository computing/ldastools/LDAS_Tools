#! /ldcg/bin/tclsh

set auto_path "/ldas/lib $auto_path"

source /ldas_outgoing/cntlmonAPI/cntlmon.state
source /ldas_outgoing/LDASapi.rsc
source /ldas_outgoing/diskcacheAPI/LDASdiskcache.rsc

set errors [ list ]

foreach dir $::MOUNT_PT {
	if	{ ! [ file exist $dir ] } {
		lappend errors $dir
	}
}

if	{ [ llength $errors ] } {
	puts "ERROR mount points not found : [ join $errors ,  ]"
} else {
	puts "All mount points exist"
}
