#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
## The next line tells sh to execute the script using tclshexe \
# \
exec  ${TCLSH} "$0" ${1+"$@"}

puts "auto_path $auto_path, [ auto_execok use_ligotools ] "

;## override some defaults
source /ldas_usr/ldas/test/bin/AllTest.rsc
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 
;## specify location of tclglobus and globus if not a default
set ::env(TCLGLOBUS_DIR) /ldcg/lib

if	{ [ info exist ::env(LD_LIBRARY_PATH) ] } {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH)" 
} else {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib" 
}

package require LDASJob
package require tconvert

set ::HOST [info hostname]
set ::PORT 10001
set ::LATE_INIT 25
set ::WIPE_INIT 20
set ::WIPE_BUFF 100
set ::WIPE_KILL 3600
set ::WAIT_INIT 60
set ::WAIT_MODS 0
set ::WAIT_INCR 10
set ::WAIT_AUTO 0
set ::ITER_INIT 0
set ::SYNC_INIT "off"
set ::USER_INIT "guess"
set ::SITE_INIT "guess"
set ::CODE_INIT "guess"
set ::TIMEOUT_RECV 50
set ::TIMESTAMP "%m/%d/%y-%H:%M:%S"
set ::PAUSED 0
set ::FIRELOG 0
set ::MEMTEST 0
set ::LOGFILE ""
set ::DATFILE ""
set ::CMDFILE ""
set ::CMD ""
set ::DB "ldas_tst"
set ::SITEFILE "/etc/ldasname"
set ::PWFILE "~/.ldaspw"
set ::APILIST [list queue diskcache frame datacond mpi eventmon metadata ligolw total]
set ::jobHId 0

# set ::PUTSTANDALONE_DIR /ldas_outgoing/jobs/LDASTest/putStandAlone
;## use RUNCODE from cntlmon due to mix up of roman numerals in name
source /ldas_usr/ldas/test/bin/AllTest.rsc

if	{ ! [ file exist ::PUTSTANDALONE_DESTDIR ] } {
	file mkdir $::PUTSTANDALONE_DESTDIR 
	foreach dso  $::PUTSTANDALONE_DSOS {
		file mkdir $::PUTSTANDALONE_DESTDIR/$dso
	}
}

;## from genericAPI
namespace eval key {}

## ******************************************************** 
##
## Name: key::time
## 
## Description:
## Produces a unique key by manipulating the output of the
## Tcl clock function.
##
## Usage:
##       set unique_key [ key::time ]
##
## Comments:
## The value returned cannot be used for calculating
## elapsed time, it is not a valid timestamp. It is
## only a key.  Keys generated with this function
## should sort in time order, however.

proc key::time {} {
	 if	{ [ catch {
     	set key   [ expr { int(pow(2,31))+[clock clicks] } ]
     } err ] } {
     	set key [ expr { pow(2,31)+[clock clicks] } ]
     }
     set begin [ expr { [ string length $key ] -8 } ] 
     set end   [ expr { [ string length $key ] -3 } ] 
     set key   [ string range $key $begin $end ]
     regsub -all -- {-} $key {} key
     
     ;## leapSecs calls getCurlUrl, which calls key::time
     if { [ info exists ::leapdates ] } {
        set key [ gpsTime ]$key
     } else {
        set key [ clock seconds ]$key
     }
     
     return [ format "%016s" $key ]
}

## ******************************************************** 
proc bgerror {err} {
    set trace {}
    puts stderr "\nError: $err"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
    }

    exit 1
}

proc serverOpen {{port 0}} {
    set lsid [socket -server serverCfg -myaddr [info hostname] $port]
    foreach {::LADDR ::LHOST ::LPORT} [fconfigure $lsid -sockname] {break}
    return lsid
}

proc serverCfg  {sid addr port} {
    fconfigure $sid -buffering line -blocking off
    fileevent $sid readable [list getResult $sid]
    return
}

proc serverClose {sid} {
    catch {close $sid}
    return
}

proc myname {args} {
    set name [lindex [info level -1] 0]
    return $name
}

proc jobdir {jobid} {
    set rc ""
    set id ""
    regexp {^(\D+)(\d+)$} $jobid -> rc id
    if {![string length $id]} {
        return -code error "[myname]: missing job id number"
    }

    set myriad [expr {$id / 10000}]
    set jobdir "[file join ${rc}_${myriad} ${jobid}]"

    return $jobdir
}

proc getSecs {when} {
    # Find a .xxx pattern in time value
    set then_ms 0
    if {[regexp {\.(\d+)} $when -> then_ms]} {
        # Truncate to nearest 10th of a second
        set then_ms [string index $then_ms 0]
        set then_ms [expr { $then_ms * 100 }]
        regsub {\.\d+} $when {} when
    }

    if {[catch {clock scan $when} then_s]} {
        return -code error $then_s
    }

    # Get current time
    timestamp now_s now_ms

    set secs [expr {($then_s - $now_s) * 1000 + $then_ms - $now_ms}]
    if {$secs < 0} {
        set secs 0
    }

    return $secs
}

proc contCmd {{when ""} args} {
    if {![string length $when]} {
        if {$::PAUSED} {
            set ::afterId [after 0 runMonitor]
            putsLog "CONT: [tconvert now]"
            set ::PAUSED 0
        }
        return ""
    }

    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    set ::contId [after $secs [myname]]

    return ""
}

proc pauseCmd {{when ""} args} {
    if {![string length $when]} {
        if {!$::PAUSED} {
            catch {after cancel $::afterId}
            putsLog "PAWS: [tconvert now]"
            set ::PAUSED 1
        }
        catch {after cancel $::contId}
        return ""
    }

    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    after $secs [myname]

    return ""
}

proc stopCmd {{when ""} args} {
    if {![string length $when]} {
        serverClose $::SID
        printStat
        putsLog "STOP: [tconvert now]"
		
		if	{ [ regexp -nocase -- {putStandAlone} $::CMD ] } {
			file delete [ glob -nocomplain $::PUTSTANDALONE_DESTDIR/${::DSO}/process_*m_*.ilwd ]
		}
        set ::forever 1
        return ""
    }

    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    after $secs [myname]

    return ""
}

proc statCmd {{when ""} args} {
    if {![string length $when]} {
        printStat
        return ""
    }

    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    after $secs [myname]

    return ""
}

proc clearCmd {{when ""} args} {
    if {![string length $when]} {
        array set ::STAT {
            Send 0
            ErrS 0
            ErrR 0
            RejQ 0
            Skip 0
            Wipe 0
            SendList {}
            RecvList {}
            ErrList  {}
            MissList {}
        }
            #Info 0

        foreach api $::APILIST {
            set ::STAT($api) {}
            #set ::TIMESUM($api) 0.0
            #set ::TIMESQR($api) 0.0
        }
        array unset ::SENDTIME

        putsLog "STAT: Clear [tconvert now]"
        return ""
    }

	;## remove putStandAlone created input files
	foreach dir [ glob -nocomplain $::PUTSTANDALONE_DESTDIR/* ] {
		if	{ [ file isdirectory $dir ] } {
			putsLog "clearCmd: deleting files from $dir"
			catch { eval file delete -force $::PUTSTANDALONE_DESTDIR/${dir}/process_1m_*.ilwd }
			catch { eval file delete -force $::PUTSTANDALONE_DESTDIR/${dir}/process_2m_*.ilwd }
		}
	}
	
    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    after $secs [myname]

    return ""
}

proc cleanCmd {{when ""} args} {
    if {![string length $when]} {
        foreach jobid $::STAT(RecvList) {
            set dir [file join / ldas_outgoing jobs [jobdir $jobid]]
            file delete [glob -nocomplain -directory $dir *.gwf]
        }
        return ""
    }

    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    after $secs [myname]

    return ""
}

proc wipeCmd {{wipe ""} args} {
    if {![string length $wipe]} {
        return $::WIPE
    }

    if {[string equal "init" $wipe]} {
        set wipe $::WIPE_INIT
    }

    if {![string is integer $wipe]} {
        return -code error "bad argument '$wipe'"
    }

    if {$wipe < 0 || $wipe > $::WIPE_BUFF} {
        return -code error "value must be between 0 and $::WIPE_BUFF"
    }

    if {[catch {set ::WIPE $wipe} msg]} {
        return -code error $msg
    }
    putsLog "WIPE: $::WIPE"

    return $::WIPE
}

proc lateCmd {{late ""} args} {
    if {![string length $late]} {
        return $::LATE
    }

    if {[string equal "init" $late]} {
        set late $::LATE_INIT
    }

    if {![string is integer $late]} {
        return -code error "bad argument '$late'"
    }

    if {$late < 0 || $late > 100} {
        return -code error "value must be between 0 and 100"
    }

    if {[catch {set ::LATE $late} msg]} {
        return -code error $msg
    }
    putsLog "LATE: $::LATE"

    return $::LATE
}

proc waitCmd {{wait ""} args} {
    if {![string length $wait]} {
        return $::WAIT
    }

    if {[string equal "init" $wait]} {
        set wait $::WAIT_INIT
    }

    set oper ""
    regexp {([+-]?)([0-9.]+)?} $wait -> oper wait

    #if {![string is integer $wait]} {
    #    return -code error "bad argument '$wait'"
    #}

    if {[string length $oper]} {
        if {![string length $wait]} {
            set wait $::WAIT_INCR
        }

        #Don't put braces around expression
        set wait [expr $::WAIT $oper $wait]

        if {$wait < 0} {
            set wait 0.0
        }
    } else {
        ;## reset wait mod counter
        set ::WAIT_MODS 0
    }

    set wait [format "%.1f" $wait]

    if {![info exists ::WAIT] || ($wait != $::WAIT)} {
        if {[catch {set ::WAIT $wait} msg]} {
            return -code error $msg
        }
        putsLog "WAIT: $::WAIT"
    }

    return $::WAIT
}

proc iterCmd {{iter ""} args} {
    if {![string length $iter]} {
        return $::ITER
    }

    if {[string equal "init" $iter]} {
        set iter $::ITER_INIT
    }

    set oper ""
    regexp {([+-]?)(\d+)?} $iter -> oper iter

    if {![string is integer $iter]} {
        return -code error "bad argument '$iter'."
    }

    if {[string length $oper]} {
        if {![string length $iter]} {
            set iter 1
        }

        #Don't put braces around expression
        set iter [expr $::ITER $oper $iter]

        if {$iter < 0} {
            set iter 0
        }
    }

    if {[catch {set ::ITER $iter} msg]} {
        return -code error $msg
    }
    putsLog "ITER: $::ITER"

    if {$::ITER == 0} {
        pauseCmd
    }

    return $::ITER
}

proc syncCmd {{sync ""} args} {
    if {![string length $sync]} {
        if {[string is true $::SYNC]} {
            return "On"
        } else {
            return "Off"
        }
    }

    if {[string equal "init" $sync]} {
        set sync $::SYNC_INIT
    }

    if {![string is boolean $sync]} {
        return -code error "'$sync' is not a valid boolean value."
    }

    if {[catch {set ::SYNC [string is true $sync]} msg]} {
        return -code error $msg
    }

    if {[string is true $::SYNC]} {
        set sync On
    } else {
        set sync Off
    }

    putsLog "SYNC: $sync"

    return $sync
}

proc siteCmd {{site ""} args} {
    if {![string length $site]} {
        return $::SITE
    }

    if {[string equal "init" $site]} {
        set site $::SITE_INIT
    }

    if {[string equal "guess" $site]} {
        set site "${::HOST}:${::PORT}"

        if {[file readable $::SITEFILE]} {
            set fid [open $::SITEFILE r]
            gets $fid line
            close $fid

            regexp {\-(\w+)$} [string trim $line] -> site

            switch -exact -- $site {
                la {set site llo}
                wa {set site lho}
            }
        }
    }

    set ::SITE $site
    putsLog "SITE: $::SITE"
    return $::SITE
}

proc codeCmd {{code ""} args} {
    if {![string length $code]} {
        return $::RUNCODE
    }

    if {[string equal "init" $code]} {
        set code $::CODE_INIT
    }

    if {[string equal "guess" $code]} {
		catch { exec grep RUNCODE /ldas_outgoing/cntlmonAPI/cntlmon.state } data
		regexp -- {set\s+::RUNCODE\s+(\S+)} $data -> code		
    }

    set ::RUNCODE $code
    putsLog "CODE: $::RUNCODE"
    return $::RUNCODE
}

proc userCmd {{user ""} args} {
    if {![string length $user]} {
        return $::USER
    }

    if {[string equal "init" $user]} {
        set user $::USER_INIT
    }

    if {[string equal "guess" $user]} {
        set user $::tcl_platform(user)

        if {[file readable $::PWFILE]} {
            set fid [open $::PWFILE r]
            set data [split [read -nonewline $fid] "\n"]
            close $fid

            set userlist [list]
            foreach line $data {
                lappend userlist [lindex $line 0]
            }

            set user [file rootname [file tail $::CMDFILE]]test
            if {[string equal "sfttest" $user]} {
                set user "knownpulsardemodtest"
            }
            if {[lsearch $userlist $user] == -1} {
                set user [lindex $userlist 0]
            }
        }
    }

    set ::USER $user
    putsLog "USER: $::USER"
    return $::USER
}

proc fileCmd {{file ""} args} {
    if {![string length $file]} {
        return $::CMDFILE
    }

    if {![file exists $file]} {
        return -code error "Can't find user-command file: '$file'"
    }

    if {[catch {
        set fid [open $file r]
        set ::CMD [read -nonewline $fid]
        close $fid
        #source $file
    } err]} {
        catch {close $fid}
        return -code error "Error while reading user-command file: $::file: $err"
    }

    set ::CMDFILE $file
    putsLog "FILE: $::CMDFILE"
    return $::CMDFILE
}

proc getResult {sid} {
    set i 0
    set data {}
    set datum {}

    while {1} {
        if {[catch {read $sid} datum]} {
            break
        }
        if {[string length $datum]} {
            append data $datum
            set i 10
            continue
        } else {
            incr i 10
            if {$i > $::TIMEOUT_RECV} {break}
        }
        if {[eof $sid]} {break}
        after $i
    }

    ;## strip timing data
    set timing ""
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set timing [string range $data $idx end]
        set data [string replace $data $idx end]
    }
    regsub -all {\n} $data " " data
    set reply [string trim $data]
    fileevent $sid readable {}

    set msg {}
    set cmd {}
    set rest {}
    regexp -- {(\S+)\s*(.*)} $reply -> cmd rest
    if {![string length $cmd]} {
        catch {close $sid}
        return
    }
    if {![string length $rest]} {
        set rest {{}}
    }
    set cmd [string toupper $cmd]
    set cmdList {CLEAN CLEAR CODE CONT ITER FILE HELP LATE PAUSE PAWS PUTS SET SITE STAT STOP SYNC USER WAIT WIPE}
    if {[lsearch -exact $cmdList $cmd] != -1} {
        switch -exact -- $cmd {
            CLEAN {catch {eval cleanCmd $rest} msg}
            CLEAR {catch {eval clearCmd $rest} msg}
            CODE  {catch {eval codeCmd $rest} msg}
            CONT  {catch {eval contCmd $rest} msg}
            ITER  {catch {eval iterCmd $rest} msg}
            FILE  {catch {eval fileCmd $rest} msg}
            HELP  {set msg $cmdList}
            LATE  {catch {eval lateCmd $rest} msg}
            PAUSE {catch {eval pauseCmd $rest} msg}
            PAWS  {catch {eval pauseCmd $rest} msg}
            SITE  {catch {eval siteCmd $rest} msg}
            STAT  {catch {eval statCmd $rest} msg}
            STOP  {catch {eval stopCmd $rest} msg}
            SYNC  {catch {eval syncCmd $rest} msg}
            USER  {catch {eval userCmd $rest} msg}
            WAIT  {catch {eval waitCmd $rest} msg}
            WIPE  {catch {eval wipeCmd $rest} msg}

            SET   {catch {eval set $rest} msg}
            PUTS  {catch {eval set msg "$rest"} msg}
            default {}
        }
        if {[string length $msg]} {
			catch { puts $sid $msg } err
        }
        catch {close $sid}
        return
    }
    if {[regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid]} {
        if {[info exists ::WIPE_JOBID] && $::WIPE_JOBID == $jobid} {
            after cancel $::WIPE_AFTER

            ;## this is the reply from a rmJobFiles command
            putsLog "WIPE: $reply"

            if {![regexp -- {error!|aborting!} $reply]} {
                set ::WIPE_LIST [lreplace $::WIPE_LIST 0 $::WIPE]
            }
            unset ::WIPE_JOBID
        } else {
            ;## this is the reply from a regular job
            putsLog "RECV: $reply"

            lappend ::STAT(RecvList) $jobid
						
			;## remove the input process ilwd for putStandAlone jobs
			if	{ [ info exist ::PUTSTANDALONE_FILES($jobid) ] } {
				set jobfiles [ set ::PUTSTANDALONE_FILES($jobid) ]
				foreach inputfile $jobfiles {
					if	{ [ file exist $inputfile ] } {
						file delete -force $inputfile
						putsLog "getResult: deleted $inputfile"
					} else {
						putsLog "getResult: cannot find $inputfile"
					}
				}
				unset ::PUTSTANDALONE_FILES($jobid)
			}

            set idx [lsearch $::STAT(SendList) $jobid]
            if {$idx != -1} {
                set ::STAT(SendList) [lreplace $::STAT(SendList) $idx $idx]
            } else {
                lappend ::STAT(MissList) $jobid
            }

            if {[regexp -- {error!|aborting!} $reply]} {
                incr ::STAT(ErrR)
                lappend ::STAT(ErrList) $jobid
            }

            putsDat [jobTimeData $jobid $timing]

            if {$::MEMTEST} {
                ;## Special handling for memory corruption test
                after 0 [list memtest $jobid $reply]
            }
			
            if {$::WIPE > 0} {
                lappend ::WIPE_LIST $jobid
                if {[llength $::WIPE_LIST] > $::WIPE_BUFF} {
                    after 0 {rmJobFiles [lrange $::WIPE_LIST 0 $::WIPE]}
                }
            }

            # if {$::SYNC && !$::PAUSED} {
            #    after 0 {sendCmd [subst -nobackslashes $::CMD]}
            #}
        }
    } else {
        ;## Unknown text
        putsLog "JUNK: $reply"
    }

    catch {close $sid}
    return
}

proc getResultPersistent { data } {

    ;## strip timing data
    set timing ""
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set timing [string range $data $idx end]
        set data [string replace $data $idx end]
    }
    regsub -all {\n} $data " " data
    set reply [string trim $data]

    set msg {}
    set cmd {}
    set rest {}
    regexp -- {(\S+)\s*(.*)} $reply -> cmd rest
    if {![string length $cmd]} {
        return
    }
    if {![string length $rest]} {
        set rest {{}}
    }

    if {[regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid]} {
        if {[info exists ::WIPE_JOBID] && $::WIPE_JOBID == $jobid} {
            after cancel $::WIPE_AFTER

            ;## this is the reply from a rmJobFiles command
            putsLog "WIPE: $reply"

            if {![regexp -- {error!|aborting!} $reply]} {
                set ::WIPE_LIST [lreplace $::WIPE_LIST 0 $::WIPE]
            }
            unset ::WIPE_JOBID
        } else {
            ;## this is the reply from a regular job
            putsLog "RECV: $reply"

            lappend ::STAT(RecvList) $jobid
						
			;## remove the input process ilwd for putStandAlone jobs
			if	{ [ info exist ::PUTSTANDALONE_FILES($jobid) ] } {
				eval file delete -force [ set ::PUTSTANDALONE_FILES($jobid) ]
				putsLog "getResultPersistent: deleted [ set ::PUTSTANDALONE_FILES($jobid) ] files"
				unset ::PUTSTANDALONE_FILES($jobid)
			}

            set idx [lsearch $::STAT(SendList) $jobid]
            if {$idx != -1} {
                set ::STAT(SendList) [lreplace $::STAT(SendList) $idx $idx]
            } else {
                lappend ::STAT(MissList) $jobid
            }

            if {[regexp -- {error!|aborting!} $reply]} {
                incr ::STAT(ErrR)
                lappend ::STAT(ErrList) $jobid
            }

            putsDat [jobTimeData $jobid $timing]

            if {$::MEMTEST} {
                ;## Special handling for memory corruption test
                # after 0 [list memtest $jobid $reply]
				memtest $jobid $reply
            }
			
            if {$::WIPE > 0} {
                lappend ::WIPE_LIST $jobid
                if {[llength $::WIPE_LIST] > $::WIPE_BUFF} {
                    after 0 {rmJobFiles [lrange $::WIPE_LIST 0 $::WIPE]}
                }
            }

            #if {$::SYNC && !$::PAUSED} {
            #    after 0 {sendCmd [subst -nobackslashes $::CMD]}
            #}
        }
    } else {
        ;## Unknown text
        putsLog "JUNK: $reply"
    }

    return
}

proc tail {filename {nlines 10}} {
    if {![string length $filename]} {
        return ""
    }
    if {![file exists $filename]} {
        return ""
    }

    if {[catch {open $filename r} fid]} {
        return ""
    }

    if {[catch {file size $filename} filesize]} {
        return ""
    }

    set guess 256
    set offset 0
    set nbytes [expr {$guess * $nlines}]

    while {1} {
        set offset [expr {$offset + $nbytes}]
        if {$offset > $filesize} {
            set offset $filesize
        }

        if {[catch {seek $fid -$offset end} err]} {
            return ""
        }
        if {[catch {read -nonewline $fid} raw]} {
            return ""
        }
        set data [split $raw "\n"]

        ;## No more data to read
        if {$offset == $filesize} {
            break
        }

        ;## Do we have enough data?
        if {[llength $data] > $nlines} {
            break
        }
    }
    catch {close $fid}

    set n [expr {$nlines - 1}]
    return [join [lrange $data end-$n end] "\n"]
}

proc jobTimeData {jobid data} {
    if {[string equal "HEADER" $jobid]} {
        lappend result Jobid StartGPS
        foreach api $::APILIST {
            if {[regexp {queue|total} $api]} {
                lappend result [string totitle $api]
                continue
            }
            lappend result ${api}API
        }
        lappend result Error
        return $result
    }

    if {[info exists ::SENDTIME($jobid)]} {
        set startgps [tconvert $::SENDTIME($jobid)]
        array unset ::SENDTIME $jobid
    } else {
        set startgps 0
    }

    set errorAPI ""
    foreach api $::APILIST {
        set $api 0.0
    }

    foreach line [split $data "\n"] {
        if {[regexp {Wait Time\(queue\):[ \t]+([0-9\.]+)} $line -> queue]} {
            continue
        }
        if {[regexp {managerAPI\(total\):[ \t]+([0-9\.]+)} $line -> total]} {
            continue
        }
        if {[regexp {^(.+)API:[ \t]+([0-9\.]+)(.*)$} $line -> api time notes]} {
            set $api $time
            if {[regexp -nocase {error} $notes]} {
                set errorAPI ${api}API
            }
            continue
        }
    }

    if {![string length $data]} {
        set errorAPI Queue
    }

    lappend result $jobid $startgps
    foreach api $::APILIST {
        set apitime [set $api]
        lappend result $apitime

        if {![string length $errorAPI]} {
            lappend ::STAT($api) $apitime
            #set ::TIMESUM($api) [expr {$::TIMESUM($api) + $apitime}]
            #set ::TIMESQR($api) [expr {$::TIMESQR($api) + pow($apitime,2)}]
        }
    }
    if {[string length $errorAPI]} {
        lappend result $errorAPI
    }

    return $result
}

proc sendCmd {cmd} {
    ;## strip comments
	;## substitue the data field in process ilwds to make process unique
	;## /ldas_outgoing/jobs/LDASTest/putStandAlone/ is hard-coded in the cmd file
	if	{ [ regexp -nocase {putStandAlone} $cmd ] } {
		if	{ ![ regexp -nocase {burstwrapper|waveburst} $::DSO ] } {
			foreach { file1 file2} [ substitue_process_data ] { break }
			;## putsLog "file1 $file1 [ file exist $file1 ] , file2=$file2 [ file exist $file2 ]"
			;## use the process data modified files
			set rc1 [ regsub -- "file:/ldas_outgoing/jobs/LDASTest/putStandAlone/$::DSO/process_1.ilwd" $cmd "file:$file1" cmd ]
			# putsLog "rc1=$rc1: cmd $cmd"
			set rc2 [ regsub -- "file:/ldas_outgoing/jobs/LDASTest/putStandAlone/$::DSO/process_2.ilwd" $cmd "file:$file2" cmd ]
			# putsLog "rc2=$rc2: cmd $cmd"
			catch { regsub "file:/ldas_outgoing/jobs/LDASTest/putStandAlone/$::DSO/output_1.ilwd" $cmd \
			"file:$::PUTSTANDALONE_SRCDIR/$::DSO/output_1.ilwd" cmd } err
			catch { regsub "file:/ldas_outgoing/jobs/LDASTest/putStandAlone/$::DSO/output_2.ilwd" $cmd \
			"file:$::PUTSTANDALONE_SRCDIR/$::DSO/output_2.ilwd" cmd } err
		}
	}
	
    foreach line [split $cmd "\n"] {
        set line [string trim $line]
        if {[regexp {^\s*#} $line]} {
            continue
        }
        lappend tmp $line
    }
    set cmd [join $tmp "\n"]
    regsub -all -- {[\s\n\t]+} $cmd { } cmd
    regsub -- {(-database\s+)\S+(\s+)} $cmd "\\1$::DB\\2" cmd
	
	;## burstwrapper to write each ontput to a different directory
	if	{ [ regexp -- {file:/dso-test/burstwrapper/dev/jobH(\d+).730524507.bin} $cmd ] } {
		incr  ::jobHId 1
		if	{ $::jobHId > 4 } {
			set ::jobHId 1
		}
		regsub -- {fileOutput:/dso-test/burstwrapper/dev/jobH(\d+).730524507.bin} $cmd \
			"fileOutput:/dso-test/burstwrapper/dev/jobH${::jobHId}.730524507.bin" cmd
	}
			
	;## burstwrapper
	
    #regsub -- {(-database\s+)[\{]?[^\}]+[\}]?(\s+)?} $cmd "\\1$::DB\\2" cmd

    ;## Old way
    #if {[catch {
    #    set sid [socket $::HOST $::PORT]
    #    puts $sid $cmd
    #    flush $sid
    #    set aid [after $::TIMEOUT_SEND [list killSock $sid]]
    #    fileevent $sid readable [list getJobid $sid $aid]
    #} err]} {
    #    catch {close $sid}
    #    putsLog "SEND: Error sending job: $err"
    #    incr ::STAT(ErrS)
    #} else {
    #    putsLog "SEND: Success"
    #    incr ::STAT(Send)
    #}

    ;## New way
	set unique [ key::time ]
	if	{ [ info exist ::LDAS_VERSION_8 ] } {
		catch { LJrun job${unique} -nowait -email $::EMAIL -manager $::SITE \
		-user $::USER $cmd  } errmsg
	} else {
    	catch { LJrun job${unique} -nowait -email $::EMAIL -manager $::SITE \
		-user $::USER -globus $::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $cmd  } errmsg
	}

    if {$LJerror} {
        if { [info exists ::job${unique}(error)]} {
		# puts stderr $errmsg
		;## sometimes gives error 'too many nested evaluations (infinite loop?)'
            catch { set errmsg [ set ::job${unique}(error) ] } err			
        } else {
			set errmsg unknown
		}
        LJdelete job${unique}

        putsLog "SEND: Error sending job: $errmsg"
        incr ::STAT(ErrS)

        if {$::WAIT_AUTO && [regexp {Timeout} $errmsg]} {
            waitCmd +
            incr ::WAIT_MODS 1
        }

        if {[regexp {limited queue size} $errmsg]} {
            incr ::STAT(RejQ)
        }

        if {$::SYNC && !$::PAUSED} {
            after [expr {int($::WAIT) * 1000}] {sendCmd [subst -nobackslashes $::CMD]}
        }

        return
    }

    set jobid [ set ::job${unique}(jobid) ]
    set ldasver [ set ::job${unique}(LDASVersion) ]
    set ::SENDTIME($jobid) [ set ::job${unique}(startTime) ]
	
    putsLog "SEND: $jobid ($ldasver)"
    lappend ::STAT(SendList) $jobid
    incr ::STAT(Send)
		
	if	{ $::PERSISTENT_SOCKET } {
		catch { LJwait job${unique} } err
		if {$LJerror} {
        	if { [info exists ::job${unique}(error)] } {
            	catch { set errmsg [ set ::job${unique}(error) ] } err
				putsLog "RECV: $errmsg"
				incr ::STAT(ErrS)
			} else {
				set errmsg unknown
			}
        } elseif  { [ info exist ::job${unique}(jobReply) ] } {
			getResultPersistent  [ set ::job${unique}(jobReply) ]
		} 
	}
    LJdelete job${unique}
	
    if {$::WAIT_MODS > 0} {
        waitCmd -
        incr ::WAIT_MODS -1
    }

    if {$::ITER > 0} {
        iterCmd -1
    }
	if	{ [ info exist file1 ] } {
		set ::PUTSTANDALONE_FILES($jobid) [ list $file1 $file2 ]
	}
    return
}

proc rmJobFiles {jidList} {
    if {[info exists ::WIPE_JOBID]} {
        return
    }
    set ::WIPE_JOBID 0

    set cmd {
        rmJobFiles
            -jobids {$jidList}
            -extensions {all}
    }
    set unique [ key::time ]
	if	{ [ info exist ::LDAS_VERSION_8 ] } { 
		catch {LJrun jobw${unique} -email $::EMAIL -manager $::SITE \
		-user $::USER $cmd } errmsg
	} else {
    	catch { LJrun jobw${unique} -email $::EMAIL -manager $::SITE \
		-user $::USER -globus $::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $cmd } errmsg
	}
    if {$LJerror} {
        if {[info exists ::jobw${unique}(error)]} {
            set errmsg [ set ::jobw${unique}(error) ]
        }
        LJdelete jobw${unique}

        putsLog "WIPE: Error sending job: $errmsg"
        unset ::WIPE_JOBID

        return
    }
    
    ;## tcl sockets just wait for response from socket
    set jobid [ set ::jobw${unique}(jobid) ]
    set ldasver [ set ::jobw${unique}(LDASVersion) ]
    
    if  { ! $::USE_GLOBUS_CHANNEL } {
        putsLog "WIPE: $jobid ($ldasver)"
        incr ::STAT(Wipe)

        set ::WIPE_JOBID $jobid
        set ::WIPE_AFTER [after [expr {$::WIPE_KILL * 1000}] {wipe_kill}]
    } else {
        ;## globus sockets response is from same socket
        putsLog "rmJobFiles for $jobid succeeded: [ set jobw${unique}(jobReply) ]"
    }
	LJdelete jobw${unique}
    
	# this sometimes results in file not found for the job
	# clean_putStandAlone
    return
}

proc wipe_kill {args} {
    after cancel $::WIPE_AFTER

    ;## In rare circumstances, the WIPE_JOBID might not exist
    if {![info exists ::WIPE_JOBID]} {
        return
    }

    putsLog "WIPE: $::WIPE_JOBID failed to return after $::WIPE_KILL seconds."
    unset ::WIPE_JOBID

    ;## Retry
    #after 0 {rmJobFiles [lrange $::WIPE_LIST 0 $::WIPE]}

    return
}

proc memtest {jobid reply} {
    ;## Special handlind for potention memory corruption test
    ;##
    ;## Expected value: -2.2270608572370152e-03
    ;## Verified on 10/8/2003
    ;##   ldasbox3 (0.7.139)
    ;##   ldas-dev (0.7.139)
    ;##   ldas-cit (0.7.133)
    ;##   ldas-mit (0.7.133)
    ;##   ldas-la  (0.7.133)
    ;##   ldas-wa  (0.7.133)
    set expected [expr -2.2270608572370152e-03]
    set errmsg ""

    if {[regexp -- {error!|aborting!} $reply]} {
        set errmsg "Job failed. $reply"
		putsLog "FAIL: $jobid - $errmsg"
		return
	}
	
    if 	{ ! [regexp -- {Your results:\s+(\S+)\s+can be found at:\s+(\S+)} $reply -> file url] } {
		set errmsg "Error parsing result message."
		putsLog "FAIL: $jobid - $errmsg"
		return
	}
	set rc [ catch { LJread $url/$file } data ]
    if  { $rc} {
		putsLog "FAIL: $jobid LJread failed - $data"
		return
	}
	
	set data [ split $data "\n" ]
    if 	{[regexp {>(\S+)<} [lindex $data 2] -> current]} {
       	if 	{$current != $expected} {
          	set errmsg "Unexpected value '$current'."
		}
    } else {
       	set errmsg "Error parsing ilwd data."
    } 
    if {[string length $errmsg]} {
        putsLog "FAIL: $jobid - $errmsg"
    } else {
        putsLog "PASS: $jobid"
    }
    return
}

#proc killSock {sid} {
#    fileevent $sid readable {}
#    catch {close $sid}
#    putsLog "SEND: Error sending job: Timeout on confirmation."
#    incr ::STAT(ErrS)
#    waitCmd +
#    incr ::WAIT_MODS 1
#
#    return
#}

proc timestamp { {s_p ""} {ms_p ""} } {
    if {[string length $s_p]} {
        upvar $s_p secs
    }
    if {[string length $ms_p]} {
        upvar $ms_p msec
    }

    set secs [clock seconds]
    set ms [clock clicks -milliseconds]
    set base [expr { $secs * 1000 }]
    set msec [expr { $ms - $base }]

    if { $msec > 1000 } {
        set diff [expr { $msec / 1000 }]
        incr secs $diff
        incr msec [expr { -1000 * $diff }]
    }

    # Truncate to nearest 10th of a second
    set msec [expr { ($msec / 100) * 100 }]

    return $secs.[format %03d $msec]
}

proc runMonitor {{target ""}} {
    set currtime [timestamp]

    if {![string length $target]} {
        set target $currtime
    }

    if {$currtime >= $target} {
        set tolerance [expr {$::WAIT * ($::LATE / 100.)}]
        if {$currtime >= ($target + $tolerance)} {
            foreach {target_s target_ms} [split [format "%.1f" $target] "."] {break}
            putsLog "SKIP: [clock format $target_s -format {%H:%M:%S}].$target_ms"
            incr ::STAT(Skip)
        } else {
            if {$::FIRELOG} {
                foreach {target_s target_ms} [split [format "%.1f" $target] "."] {break}
                putsLog "FIRE: [clock format $target_s -format {%H:%M:%S}].$target_ms"
            }
            after 0 {sendCmd [subst -nobackslashes $::CMD]}
        }

        set target [expr {$target + $::WAIT}]
    }

    if {$::SYNC || $::PAUSED} {
        return
    }

    set ::afterId [after 100 [list runMonitor $target]]
}

proc putsLog {msg} {
    if {[string equal -length 5 "BEGN:" $msg]} {
        if {[file exists $::LOGFILE]} {
            set timestamp [clock format [file mtime $::LOGFILE] -format "%m%d%H%M%S"]
            catch {file rename -force -- $::LOGFILE $::LOGFILE.${timestamp}}
        }
        set ::LFID [open $::LOGFILE {WRONLY CREAT EXCL}]

        if {[file exists $::DATFILE]} {
            set timestamp [clock format [file mtime $::DATFILE] -format "%m%d%H%M%S"]
            catch {file rename -force -- $::DATFILE $::DATFILE.${timestamp}}
        }
        set ::DFID [open $::DATFILE {WRONLY CREAT EXCL}]
    }

    ;## Comment out the following because it is not needed
    ;## If the file is renamed or removed, LFID is still valid
    ;## so there is no need to check for its existance
    #if {![file exists $::LOGFILE]} {
    #    catch {close $::LFID}
    #    set ::LFID [open $::LOGFILE {WRONLY CREAT EXCL}]
    #}

    foreach {time_s time_ms} [split [format "%.1f" [timestamp]] "."] {break}
    catch {puts $::LFID "[clock format $time_s -format $::TIMESTAMP].$time_ms: $msg"}
    catch {flush $::LFID}

    if {[string equal -length 5 "STOP:" $msg]} {
        catch {close $::LFID}
        catch {close $::DFID}
    }
    return
}

proc putsDat {msg} {
    if {![file exists $::DATFILE]} {
        catch {close $::DFID}
        set ::DFID [open $::DATFILE {WRONLY CREAT EXCL}]
    }

    catch {puts $::DFID "[join $msg \t]"}
    catch {flush $::DFID}

    return
}

proc printStat {} {
    set numRecv [llength $::STAT(RecvList)]
    set numLost [llength $::STAT(SendList)]
    #set numPass [expr {$numRecv - $::STAT(ErrR)}]

    putsLog "STAT: Jobs send : $::STAT(Send)"
    #putsLog "STAT: Jobs info : $::STAT(Info)"
    putsLog "STAT: Jobs recv : $numRecv"
    putsLog "STAT: Jobs lost : $numLost"
    putsLog "STAT: Skipped   : $::STAT(Skip)"
    putsLog "STAT: Rejected  : $::STAT(RejQ)"
    putsLog "STAT: Wiped     : $::STAT(Wipe)"
    putsLog "STAT: Send err  : $::STAT(ErrS)"
    putsLog "STAT: Recv err  : $::STAT(ErrR)"
    putsLog "STAT: Missing   : $::STAT(MissList)"
    putsLog "STAT: Lost jobs : $::STAT(SendList)"
    putsLog "STAT: Failed    : $::STAT(ErrList)"

    foreach api $::APILIST {
        ;## The following equation is credited to Teviet Creighton
        ;##
        ;## S = Sum of the samples
        ;## V = Sum of the samples^2
        ;## N = Number of samples
        ;## Mean = S / N
        ;## Variance =  [ V - (S^2 / N) ] / (N-1)
        ;## StdDev = sqrt(Variance)
        ;##
        #set mean [expr {$::TIMESUM($api) / $numPass}]
        #set sdev 0.0
        #set var [expr {($::TIMESQR($api) - (pow($::TIMESUM($api),2) / $numPass)) / ($numPass - 1)}]
        #set sdev [expr {sqrt($var)}]

        set spaces [expr {10 - [string length $api]}]
        foreach {mean sdev} [stats $::STAT($api)] {break}
        putsLog "STAT: [string totitle $api][string repeat { } $spaces]: [format {%6.2f} $mean] [format {%6.2f} $sdev]"
    }

    return
}

proc stats {samples} {
    set N [llength $samples]

    if {$N == 0} {
        return [list 0.0 0.0]
    }
    if {$N == 1} {
        return [list $samples 0.0]
    }

    set sum 0.0
    foreach sample $samples {
        set sum [expr {$sum + $sample}]
    }
    set mean [expr {$sum / $N}]

    set ep 0.0
    set var 0.0
    foreach sample $samples {
        set s [expr {$sample - $mean}]
        set ep [expr {$ep + $s}]
        set var [expr {$var + pow($s,2)}]
    }

    set var [expr {($var - pow($ep,2) / $N) / ($N - 1)}]
    set sdev [expr {sqrt($var)}]

    return [list $mean $sdev]
}

proc printUsage {args} {
    set scriptname [file tail $::argv0]
    puts stderr "Usage: $scriptname [-p] [-h] [-w secs] [-P port] [-l log] [-d dat] [-s site] [-c code] [-db db] [-f cmdfile] cmdfile"
    exit 1
}

;## only for putStandAlone to insert every time 
proc get_process_data {} { 

	regexp {(\w+)\-put} [ file tail $::CMDFILE ] -> ::DSO
	putsLog "DSO $::DSO"

	set ::SRC1  $::PUTSTANDALONE_SRCDIR/$::DSO/process_1.ilwd
	set ::SRC2  $::PUTSTANDALONE_SRCDIR/$::DSO/process_2.ilwd
	set ::PATTERN {processgroup:process:node\' size=\'\d+\'>\S+<}

	if	{ [ catch {
		set fd [ open $::SRC1 r ]
		set ::PROCESS_1_DATA [ read -nonewline $fd  ]
		close $fd
		set fd [ open $::SRC2 r ]
		set ::PROCESS_2_DATA [ read -nonewline $fd  ]
		close $fd
	} err ] } {
		puts stderr $err
	}
}
	
proc substitue_process_data {} {
	
	;## using time can result in insertion duplicates if jobs are spaced too close together
	;## set curtime [ gpsTime now ]
	
	#regexp {0\.(\d{9})} [expr rand()] -> unique
	
	if	{ [ catch {
		set unique [ key::time ]
		set len [ string length $unique ]
		set file1	$::PUTSTANDALONE_DESTDIR/$::DSO/process_1m_${unique}.ilwd
		set file2	$::PUTSTANDALONE_DESTDIR/$::DSO/process_2m_${unique}.ilwd
		regsub -all $::PATTERN $::PROCESS_1_DATA "processgroup:process:node' size='$len'>$unique<" ::PROCESS_1_DATA
		set fd [ open $file1 w ]
		puts $fd $::PROCESS_1_DATA
		close $fd
		regsub -all $::PATTERN $::PROCESS_2_DATA "processgroup:process:node' size='$len'>$unique<" ::PROCESS_2_DATA
		set fd [ open $file2 w ]
		puts $fd $::PROCESS_2_DATA
		close $fd		
	} err ] } {
		puts stderr $err
		return -code error $err
	}
	return "$file1 $file2"
}

;## file may not exist so just catch error 
proc clean_putStandAlone {} {
	
	;## remove old putstandalone data
	set curtime [ clock seconds ]
	;## purge any log over 1 hours
	set patlist [ list process_1m process_2m ] 
	foreach dir [ list stochastic inspiral tfclusters knownpulsardemod slope stackslide ] {
		foreach pat $patlist {
			foreach file [ glob -nocomplain $::PUTSTANDALONE_DESTDIR/${dir}/${pat}*.ilwd ] {
				catch {
        			set mtime [ file mtime $file ]
        			if      { [ expr $curtime - $mtime ] >  $::PUTSTANDALONE_INPUT_RETAIN } {
							putsLog "clean_putStandAlone: deleted $file"
                			file delete -force $file
        			}
				}
			}
		}
	}
}
	
## ***********************************************************
proc gpsTime {{time "now"}} {
    ;## just return something that already looks like gps time
    if {[regexp {^[6-9]\d{8}$} $time]} {
        return $time
    }

    ;## the difference between the UNIX epoch and GPS epoch.
    set epochdiff 315964800

    ;## the difference between GPS and TAI
    set gpsdiff 19

    ;## 1972-01-01 00:00:00 UTC was 1972-01-01 00:00:10 TAI.
    set offset 10

    ;## assumes 00:00:00.
    set leapdates {
        07/01/1972
        01/01/1973
        01/01/1974
        01/01/1975
        01/01/1976
        01/01/1977
        01/01/1978
        01/01/1979
        01/01/1980
        07/01/1981
        07/01/1982
        07/01/1983
        07/01/1985
        01/01/1988
        01/01/1990
        01/01/1991
        07/01/1992
        07/01/1993
        07/01/1994
        01/01/1996
        07/01/1997
        01/01/1999
    }

    set index [ llength $leapdates ]

    ;## quick short-circuit for 'now'.
    if {[string equal "now" $time]} {
        set time [clock seconds]
    } else {
        ;## build the lookup table in gmt seconds
        foreach date $leapdates {
            lappend leapsecs [clock scan $date -gmt 1]
        }

        ;## canonicalise input to UNIX epoch seconds
        if {![regexp {^\d{9,10}$} $time]} {
            if {[catch {clock scan $time -gmt 1} time]} {
                return -code error $time
            }
        }

        foreach sec [lsort -integer $leapsecs] {
            if {$time < $sec} {
                set index [lsearch $leapsecs $sec]
                break
            }
        }
    }

    set taidiff [expr {$offset + $index}]
    set gpstime [expr {$time + $taidiff - $epochdiff - $gpsdiff}]

    return $gpstime
}
## ***********************************************************

;### MAIN ###

;## Process command-line arguments
set pauseonstart 0
set localport 0
set ::PERSISTENT_SOCKET 1

if	{ [ file exist ../AllTest.rsc ] } {
	source ../AllTest.rsc
} else {
	set ::USE_GSI 1
	set ::USE_GLOBUS_CHANNEL 1
}

for {set idx 0} {$idx < $::argc} {incr idx} {
    set opt [lindex $::argv $idx]
    switch -glob -- $opt {
        -c {set ::CODE_INIT [lindex $::argv [incr idx]]}
        -d {set ::DATFILE [lindex $::argv [incr idx]]}
        -db {set ::DB [lindex $::argv [incr idx]]}
        -f {set ::CMDFILE [lindex $::argv [incr idx]]}
		-g {set ::USE_GLOBUS_CHANNEL [lindex $::argv [incr idx]] }
		-gsi { set ::USE_GSI [lindex $::argv [incr idx]] }
        -h - --help {printUsage}
        -i {set ::ITER_INIT [lindex $::argv [incr idx]]}
        -l {set ::LATE_INIT [lindex $::argv [incr idx]]}
        -M {set ::MEMTEST 1}
        -o {set ::LOGFILE [lindex $::argv [incr idx]]}
        -p {set pauseonstart 1}
        -P {set localport [lindex $::argv [incr idx]]}
        -s {set ::SITE_INIT [lindex $::argv [incr idx]]}
        -u {set ::USER_INIT [lindex $::argv [incr idx]]}
        -w {set ::WAIT_INIT [lindex $::argv [incr idx]]}
        -W {set ::WIPE_INIT [lindex $::argv [incr idx]]}
        -y {set ::SYNC_INIT "on"}
        -* {
            ;## Unknown option
            puts stderr "Error: Invalid option '$opt'."
            printUsage
        }
        default {
            ;## Assume cmdfile
            set ::CMDFILE $opt
        }
    }
}

if	{ $::USE_GLOBUS_CHANNEL } {
	set ::EMAIL "persistent_socket"
	set ::PERSISTENT_SOCKET 1
} 

if {![string length $::CMDFILE]} {
    puts stderr "Error: A user-command file was not specified."
    ;## Print Usage
    exit 1
}

if {![string length $::LOGFILE]} {
    set ::LOGFILE [file rootname [file tail $::CMDFILE]].log
}

if {![string length $::DATFILE]} {
    set ::DATFILE [file rootname [file tail $::CMDFILE]].dat
}

set ::SID [serverOpen $localport]
putsLog "BEGN: [tconvert now]"
putsLog "SOCK: $::LADDR $::LHOST $::LPORT"
putsDat [jobTimeData HEADER ""]
if	{ ! [ info exist ::EMAIL ] } {
	set ::EMAIL "${::LADDR}:${::LPORT}"
}

if {[catch {fileCmd $::CMDFILE} err]} {
    puts stderr $err
    exit 1
}

foreach var {site code user wait late iter sync wipe} {
    if {[catch {${var}Cmd init} err]} {
        puts stderr $err
        exit 1
    }
}

clearCmd
pauseCmd

;## read in the cmd file for putStandAlone
if	{ [ regexp -nocase -- {\-put} $::CMDFILE  ] } {
	get_process_data 
}

if {!$pauseonstart} {
    contCmd
}

vwait ::forever
exit 0

