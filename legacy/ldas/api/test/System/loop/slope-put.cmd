putStandAlone
    -subject {putStandAlone slope loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
	-multidimdatatarget ligolw
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/slope/process_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/slope/output_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/slope/process_2.ilwd 
    }
    -database {ldas_tst}
