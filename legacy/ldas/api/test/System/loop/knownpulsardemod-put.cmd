putStandAlone
    -subject {putStandAlone knownpulsardemod loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
	-multidimdatatarget frame
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/knownpulsardemod/process_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/knownpulsardemod/output_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/knownpulsardemod/process_2.ilwd 
    }
    -database {ldas_tst}
