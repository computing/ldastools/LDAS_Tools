conditionData
    -datacondtarget metadata
    -database {ldas_tst}
    -aliases {gw=P2\:LSC-AS_Q::AdcData;}
    -framequery {
       DT_512 P {} {610000000-610000511} Adc(P2:LSC-AS_Q)
    }
    -algorithms {
        x = slice(gw, 0, 128, 1);
        p = psd( x, 128 );
        output(p,_,_,psd,spectrum);

        rgw = resample(gw, 1, 8);
        p = psd( rgw, 262144 );
        output(p,_,_,,spectrum data);
    }
