dataPipeline
    -subject { waveburst DSO }
    -usertag { wavebursttest }

    -dynlib libldaswaveburst.so
    -filterparams (-ch1,H1:LSC-AS_Q,-ch2,L1:LSC-AS_Q,-WaveletType,3,-WaveletTreeType,1,-WaveletLevel,6,-WaveletBorder,1,-WaveletHPFilterLength,60,-WaveletLPFilterLength,60,-nonZeroFraction,0.10,-timeWindowPixels,1,-freqWindowPixels,0,-coincidenceLevel,0,-minAmp4SinglePixels,4.0,-minAmp4ClusterExtension,1.5,-halo,1,-minClusterSize,1,-minLikelihood,2.5,-minConfidence,0.0,-minPower,0.0,-minEnergy,0.0,-simulationType,0,-pixelMixer,1,-pixelSwapOne,0,-pixelSwapTwo,0,-o1,0,-o2,0,-p1,0,-p2,0,-co1,0,-co2,0,-cl1,0,-cl2,0,-outputToFile,PT,-reuseCluster,1,-strain,0.0,-timeshift_step,15,-timeshift_max,1,-noise_rms_flag,1,-duration,120,-nstrides,1,-path,/dso-test/waveburst,-debug_flag,0,-usertag,wavebursttest,-clean,1,-nsubintervals,8,-wavefilter,1,-wf_LPFilterLength,60,-wf_HPFilterLength,60,-offset,2,-extradeep,3,-interpolate,0,-int_n,3,-int_m,1,-int_extradeep,3,-ratio_limit,1.0)

    -np 3

    -framequery {
        { R {} {} 730523632-730523755 Adc(H1:LSC-AS_Q) }
        { R {} {} 730523632-730523755 Adc(L1:LSC-AS_Q) }
        { CAL_FAC_V03_H1 H {} 730523632-730523755 Proc(H1:CAL-OLOOP_FAC,H1:CAL-CAV_FAC) }
        { CAL_REF_V03_H1 H {} 734073939-734074002 Proc(H1:CAL-CAV_GAIN!0!2048.0001!,H1:CAL-RESPONSE!0!2048.0001!) }
        { CAL_FAC_V03_L1 L {} 730523632-730523755 Proc(L1:CAL-OLOOP_FAC,L1:CAL-CAV_FAC) }
        { CAL_REF_V03_L1 L {} 731488397-731488460 Proc(L1:CAL-CAV_GAIN!0!2048.0001!,L1:CAL-RESPONSE!0!2048.0001!) }
    }
    -aliases {
        I1a=H1\:LSC-AS_Q;
        I2a=L1\:LSC-AS_Q;
        g1=H1\:CAL-OLOOP_FAC;
        g2=L1\:CAL-OLOOP_FAC;
        alph1=H1\:CAL-CAV_FAC;
        alph2=L1\:CAL-CAV_FAC;
        C1a=H1\:CAL-CAV_GAIN;
        C2a=L1\:CAL-CAV_GAIN;
        R1a=H1\:CAL-RESPONSE;
        R2a=L1\:CAL-RESPONSE;
    }
    -algorithms {
        gammas1=double(g1);
        gammas1=complex(gammas1);
        gammas2=double(g2);
        gammas2=complex(gammas2);
        alphas1=double(alph1);
        alphas1=complex(alphas1);
        alphas2=double(alph2);
        alphas2=complex(alphas2);
        B = Symlet(60, 1);
        b = Biorthogonal(60, 0);
        W = WaveletForward( I1a, b, 1 );
        a = GetLayer(W,0);
        I1= tseries(a,8192.0,730523632);
        W = WaveletForward( I1, B, 6 );
        a = getSequence(W);
        S = slice(a,0,1015808,1);
        tf= tseries(S,8192.0,730523632);
        output(tf,,wrapper,H1\:LSC-AS_Q,one_time);
        C1=slice(C1a,2048,32,4096);
        output(C1,,wrapper,H1\:LSC-AS_Q,one_C);
        R1=slice(R1a,2048,32,4096);
        output(R1,,wrapper,H1\:LSC-AS_Q,one_R);
        output(gammas1,,wrapper,H1\:LSC-AS_Q,one_Gamma);
        output(alphas1,,wrapper,H1\:LSC-AS_Q,one_Alpha);
        W = WaveletForward( I2a, b, 1 );
        a = GetLayer(W,0);
        I2= tseries(a,8192.0,730523632);
        W = WaveletForward( I2, B, 6 );
        a = getSequence(W);
        S = slice(a,0,1015808,1);
        tf= tseries(S,8192.0,730523632);
        output(tf,,wrapper,L1\:LSC-AS_Q,two_time);
        C2=slice(C2a,2048,32,4096);
        output(C2,,wrapper,L1\:LSC-AS_Q,two_C);
        R2=slice(R2a,2048,32,4096);
        output(R2,,wrapper,L1\:LSC-AS_Q,two_R);
        output(gammas2,,wrapper,L1\:LSC-AS_Q,two_Gamma);
        output(alphas2,,wrapper,L1\:LSC-AS_Q,two_Alpha);
    }

