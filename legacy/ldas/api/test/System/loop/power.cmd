dataPipeline
    -subject {power online running on H2:LSC-AS_Q }
    -usertag {E9_online_power_H2:LSC-AS_Q}

    -dynlib libldaspower.so
    -filterparams (2048,719,1024,3,2,2,100.0,1.0,512,2.0,0.5,32,1.0e-13,10,H2:LSC-AS_Q,0,useMedian,2)

    -np 4
    -datacondtarget wrapper
    -metadatatarget datacond
    -multidimdatatarget ligolw
    -realtimeratio 0.9
    -database ldas_tst

    -framequery { { R H {} 730524507-730524867 Adc(H2:LSC-AS_Q) } }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/E7-H2-AS_Q-1024-131073.ilwd,pass
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/hpf150_a.ilwd,push,a1
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/hpf150_b.ilwd,push,b1
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/H2S1a.ilwd,push,a2
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/H2S1b.ilwd,push,b2
    }
    -aliases {
        x = H2\:LSC-AS_Q::AdcData;
    }
    -algorithms {
        zz = slice(x,0,5914624,1);
        gw = tseries(zz, 16384.0, 730524507, 0);
        gw = linfilt(b1,a1,gw);
        gw = linfilt(b2,a2,gw);
        gw = meandetrend(gw);
        p = psd( gw, 2048 );
        gw = resample(gw, 1, 8);
        gw = slice(gw,2048,737280,1);
        output(gw,_,wrapper,:primary,resampled gw timeseries);
        pr = psd( gw, 2048 );
        output(pr,_,wrapper,,spectrum data);
        output(p,ilwd_ascii,pzs.ilwd,p,spectrum data);
    }

