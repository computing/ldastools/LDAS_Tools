dataPipeline
    -subject {slope online running on H2:LSC-AS_Q }
    -usertag {E9_online_slope_H2:LSC-AS_Q}

    -dynlib libldasslope.so
    -filterparams (-c,,-ifo,H2,-smw,64,-pw,300,-sr,16384,-t,3.5,-sdtype,2,-adapt,yes)

    -np 2
    -datacondtarget wrapper
    -metadatatarget datacond
    -multidimdatatarget ligolw
    -realtimeratio 0.9
    -database ldas_tst

    -framequery { { R H {} 730524146-730524506 Adc(H2:LSC-AS_Q) } }
    -responsefiles {
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/hpf150_a.ilwd,push,a1
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/hpf150_b.ilwd,push,b1
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/H2S1a.ilwd,push,a2
        file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic/H2S1b.ilwd,push,b2
    }
    -aliases {
        x = H2\:LSC-AS_Q::AdcData;
    }
    -algorithms {
        zz = slice(x,0,5914624,1);
        gw = tseries(zz, 16384.0, 730524146, 0);
        gw = linfilt(b1,a1,gw);
        gw = linfilt(b2,a2,gw);
        gw = meandetrend(gw);
        p = psd( gw, 2048 );
        gw = slice(gw,16384,5898240,1);
        output(gw,_,wrapper,:primary,resampled gw timeseries);
        output(p,ilwd_ascii,pzs.ilwd,p,spectrum data);
    }

