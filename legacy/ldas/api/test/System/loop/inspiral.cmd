# inspiral_test_ns.pipe
dataPipeline
    -subject { Neutron Star Inspiral Search }

    -dynlib libldasinspiral.so
    -filterparams (1048576,12,524288,70.0,65536,69.0,8,0.9,1,8.0,20.0,4,2,0,1,1,(1.4,1.5))

    # to use with larger clusters:
    # -np 22
    # -filterparams (1048576,12,524288,70.0,65536,69.0,8,0.9,1,8.0,20.0,4,30,0,2)

    -np 4
    -datacondtarget wrapper
    -metadatatarget datacond
    -multidimdatatarget ligolw
    -metadataapi metadata
    -database ldas_tst

    -dbspectrum {
        channel=slice(P2:LSC-AS_Q,0,128,1)
        spectrum_type=Welch
        spectrum_length=65
        start_time=610000000
        start_time_ns=0
        start_frequency=0
        delta_frequency=128
        pushpass=push
        alias=spect
    }
    -framequery {
        {R L {} 730524011-730525675 adc(L1:LSC-AS_Q!resample!4!)}
        {CAL_REF_V03_L1 L {} 731488397-731488460 proc(L1:CAL-CAV_GAIN!0!7000.0001!,L1:CAL-RESPONSE!0!7000.0001!)}
        {CAL_FAC_V03_L1 L {} 730524011-730525675 proc(L1:CAL-OLOOP_FAC,L1:CAL-CAV_FAC)}
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/l1-test-tmpltbank-ns.ilwd,pass
    }
    -aliases {
        rawchan = LSC-AS_Q;
        cav_gain = CAL-CAV_GAIN;
        resp = CAL-RESPONSE;
        cav_fac = CAL-CAV_FAC;
        oloop_fac = CAL-OLOOP_FAC;
    }
    -algorithms {
        value(spect);
        chan = slice( rawchan, 10, 6815744, 1);
        output(chan,_,_,_,channel);
        output(cav_gain,_,_,_,CAL-CAV_GAIN);
        output(resp,_,_,_,CAL-RESPONSE);
        cav_fac_cplx = float( cav_fac );
        cav_fac_cplx = complex( cav_fac_cplx );
        output(cav_fac_cplx,_,_,_,CAL-CAV_FAC);
        oloop_fac_cplx = float( oloop_fac );
        oloop_fac_cplx = complex( oloop_fac_cplx );
        output(oloop_fac_cplx,_,_,_,CAL-OLOOP_FAC);
    }
