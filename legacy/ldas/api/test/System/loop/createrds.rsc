;## Resource file for createrds.tcl and createrdsGUI.tcl, created Thu May 26 19:33:05 2005
;## Last modified on Thu May 26 19:33:05 2005

;## Start and end time
;## All data in the interval [startTime, endTime) will be processed
set startTime 796068000 
set endTime   796070560 

;## Maximum length of each data set, in seconds
set maxJobLength 512

;## Maximum number of jobs to run at one time
set maxJobs 2

;## File of channels to reduce
set adcFile adcdecimate_H-RDS_R_L3-S4.txt

;## LDAS site to run at
set site dev

;## User to run as
set user mlei

;## Email notification list (must be a space-separated
;## list of email addresses enclosed in double quotes)
#set notifyList "johnson_b@ligo-wa.caltech.edu gmendell@ligo-wa.caltech.edu"
set notifyList ""

;## Frame type(s) to read data from
set type [ list R ]

;## Input IFO for each type. If this is an empty list then
;## this gets set based on the channel list.  Otherwise this
;## must be a list of the same length as the type list above.
set ifoList [ list H ]

;## 1 = TRUE 0 = FALSE. Must be 1 if length of type list > 1.
;## If 1 use framequery syntax with createRDS cmd.
set useFrameQuery 0

;## Output frame type of reduced data (see leaveUserTypeAsIs below)
set userType RDSS4DEVTEST0720_R_L3

;## 1 = TRUE 0 = FALSE; if FALSE and userType does not end in a number a
;## number is appended to the end of the userType one level higher than
;## the input type. If the level of the input type cannot be determined
;## a 1 is appened. However if leaveUserTypeAsIs is TRUE userType is not
;## change at all, but used exactly as set above.
set leaveUserTypeAsIs 0

;## Exclude any existing data of this user type.
;## If set to "Y", the script will not attempt to reduce
;## data which is already available in reduced form in LDAS
set excludeRDS N 

;## Duration of input frame files in seconds
set frameLength 32

;## 1 = TRUE 0 = FALSE; dynamically update the frameLength when DOWNSAMPLING to adjust segment lengths
set updateFrameLength 0

;## seconds to wait between updates of the frame cache
set diskCacheInterval 1800

;## If includedirs is not an empty list, reduce input data from these directories only:
set includedirs [ list ]

;## If excludedirs is not an empty list, do not reduce input data from any of these directories:
set excludedirs [ list  ]

;## Option for output to go into job directories rather than the outputdir
;## The default value is 0 (FALSE)
set usejobdirs 0

;## Directory where reduced data will be put
set outputdir /ldas_outgoing/jobs/output/rdsTest/frames

;## Optional comma delimited sed options to substitute into outputdir to give path to md5sum files.
;## Example: set md5sumregexp "s,frames,meta/frames,i"
;## The default value is an empty string and the md5sumregexp option is not used)
set md5sumregexp "s,frames,meta/frames,i" 

;## Use subdirectories of the form, e.g., H-nnnn... where number of n's = 9 - string length of secPerBin?
set binByTime Y

;## If binByTime, seconds of data to put into each directory (must be a multiple of 10 between 10 and 10000000; default is 100000)
set secPerBin 100000

;## Compression type, raw or gzip
set compressiontype gzip

;## Compression level
set compressionlevel 1

;## 1 = TRUE 0 = FALSE; if TRUE frame file checksums are validated before running job
set	filechecksum 1

;## 1 = TRUE 0 = FALSE; if TRUE frame times are validated against the frame name before running job
set frametimecheck 1

;## 1 = TRUE 0 = FALSE; if TRUE datavalid flags are checked for every channel before running job
set framedatavalid 0

;## Number of frames to put in each output file
set framesperfile 1

;## Number of seconds of data to put into each frame
set secperframe 256

;## 1 = TRUE 0 = FALSE; if TRUE align the normal length output frame files
;## so that their start times mod framesperfile*secperframe is zero. (Note
;## that short frames, if allowed, will not be aligned.)
set alignFrameStartTimes 1

;## 1 = TRUE 0 = FALSE; if TRUE allow short frame with less than secperframe in a frame
set allowshortframes 1

;## 1 = TRUE 0 = FALSE; if FALSE never allow short frames when running on the latest data segment
set allowShortForLatestSeg 0

;## If allowshortframes is TRUE, fill in a segment with short frames if its end time is this old (in seconds).
set fillSegmentsIfThisOld 1200

;## 1 = TRUE 0 = FALSE; if TRUE generate frame checksums on a per frame basis
set generatechecksum 1

;## 1 = TRUE 0 = FALSE; if TRUE fill any missing channel datavalid aux vec's in the output frames
set fillmissingdatavalid 0