#!/bin/sh
# \
exec tclsh "$0" ${1+"$@"}
#exec tclsh "$0" ${1+"$@"}

source ./joblist.tcl

set wait ""
set meta_wait ""

;## make it close to 20 for getmeta and dcmean
if { [ llength $::argv ] > 0 } {
    set wait [ lindex $::argv 0 ]
    set meta_wait [format "%.3f" [ expr $wait * 2.0 / [ llength $pipelines ] * [ llength $meta_data ] ]]
    puts "pipeloop $wait getmeta/dcmean $meta_wait"
}

foreach port $meta_data {
    set sid [ socket [ info hostname ] $port ]
    puts $sid [list wait $meta_wait]
    flush $sid
    puts "$port - [ gets $sid ]"
    close $sid
}

foreach port $pipelines {
    set sid [ socket [ info hostname ] $port ]
    puts $sid [list wait $wait]
    flush $sid
    puts "$port - [ gets $sid ]"
    close $sid
}

