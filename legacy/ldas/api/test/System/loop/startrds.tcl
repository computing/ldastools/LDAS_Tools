#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc

;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

foreach dir [ list $::RDSDIR ] {
	if	{ ! [ file exist $dir ] } {
		file mkdir $dir 
	}
	catch { exec ln -f AllTest.rsc $dir/AllTest.rsc } 
}

catch {exec /ldas/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent --shell=tclsh check} err
eval $err
if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    puts $err
    puts "need to setup env(SSH_AUTH_SOCK) for this test"
    exit
}

;## specify location of tclglobus and globus if not a default
set ::env(TCLGLOBUS_DIR) /ldcg/lib

if	{ [ info exist ::env(LD_LIBRARY_PATH) ] } {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH)" 
} else {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib" 
}

set ::env(PATH) "/ldcg/bin/64:/ldcg/bin64:/ldcg/bin:/ldas/bin:/ldas_usr/ldas/test/bin:$::env(PATH)"

set basiccmd "/usr/bin/env PATH=$::env(PATH) LD_LIBRARY_PATH=$::env(LD_LIBRARY_PATH) "

;## needs to run in background 

cd $::RDSDIR
catch { eval exec "$basiccmd rdsclean.tcl delete" } err

catch { eval exec "$basiccmd dords_loop.s5 lho L1 S3 H >& dords_${::SITE}_H_L1-S3.log &" } data1
catch { eval exec "$basiccmd dords_loop.s5 llo L1 S3 L >& dords_${::SITE}_L_L1-S3.log &" } data2

catch { eval exec "$basiccmd dords_loop.s5 lho L1 S5 H >& dords_${::SITE}_H_L1-S5.log &" } data3
catch { eval exec "$basiccmd dords_loop.s5 lho L3 S5 H >& dords_${::SITE}_H_L3-S5.log &" } data4

catch { eval exec "$basiccmd dords_loop.s5 lho L4 S5 H-H1 >& dords_${::SITE}_H1_L4-S5.log &" } data5
catch { eval exec "$basiccmd dords_loop.s5 lho L4 S5 H-H2 >& dords_${::SITE}_H2_L4-S5.log &" } data6

catch { eval exec "$basiccmd dords_loop.s5 llo L1 S5 L >& dords_${::SITE}_L_L1-S5.log &" } data7
catch { eval exec "$basiccmd dords_loop.s5 llo L3 S5 L >& dords_${::SITE}_L_L3-S5.log &" } data8
