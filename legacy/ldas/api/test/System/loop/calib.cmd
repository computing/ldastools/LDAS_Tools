dataPipeline
    -subject { E9:H1:LSC-AS_Q:727577888-727578207 }
    -usertag { E9_test }

    -dynlib libldasinspiral.so
    -filterparams (262144,7,131072,100.0,65536,69.0,8,1,(36.0,0.0),(10000.0,0.0),0,5,0,2)

    -np 6
    -datacondtarget wrapper
    -multidimdatatarget ligolw
    -database ldas_tst

    -framequery {
        { R H {} 727577888-727578207 adc(H1:LSC-AS_Q!resample!4!) }
        { SenseMonitor_H1_M H {} 727577872-727640271 adc(H1:CAL-CAV_FAC.mean,H1:CAL-OLOOP_FAC.mean) }
        { CAL_REF H {} 715156759-715156822 Proc(H1:CAL-CAV_GAIN!0!2048.0001!,H1:CAL-RESPONSE!0!2048.0001!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/bank-random.ilwd,pass
    }
    -aliases {
        chan = LSC-AS_Q;
        cav_fac = CAL-CAV_FAC;
        oloop_fac = CAL-OLOOP_FAC;
        cav_gain = CAL-CAV_GAIN;
        resp = CAL-RESPONSE;
    }
    -algorithms {
        rchan = slice(chan, 131072, 1048576, 1);
        output(rchan,_,_,,channel);

        spec = psd( rchan, 262144 );
        output(spec,_,_,,spectrum);

        cav_fac_cplx = float( cav_fac );
        cav_fac_cplx = complex( cav_fac_cplx );
        output(cav_fac_cplx,_,_,,CAL-CAV_FAC);

        oloop_fac_cplx = float( oloop_fac );
        oloop_fac_cplx = complex( oloop_fac_cplx );
        output(oloop_fac_cplx,_,_,,CAL-OLOOP_FAC);

        output(cav_gain,_,_,,CAL-CAV_GAIN);
        output(resp,_,_,,CAL-RESPONSE);
    }

