putStandAlone
    -subject {putStandAlone tfclusters loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
	-multidimdatatarget ligolw
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/tfclusters/process_1.ilwd  
		file:/ldas_outgoing/jobs/LDASTest/putStandAlone/tfclusters/output_1.ilwd
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/tfclusters/process_2.ilwd 
    }
    -database {ldas_tst}
