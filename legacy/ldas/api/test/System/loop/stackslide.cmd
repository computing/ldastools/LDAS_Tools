dataPipeline
    -subject { sftRequest }
    -returnprotocol http://testGetSFTs.xml

    -np 2
    -multidimdatatarget ligolw
    -datacondtarget wrapper
    -database ldas_tst

    -concatenate 0
    -autoexpand 1
    -allowgaps 1

    -dynlib libldasstackslide.so
    -filterparams (730522208,0,4096,1800,16,60,60,1282.0,3.0,180.0,1,60,1282.0,3.0,180.0,30,60,1282.5,2.0,120.0,"H2","LHO","hwinjectE10Pulsar0",0,"H2:LSC-AS_Q",input,0,0,0,32,0,0,0,0,0,0,0,0,0,1,1282.0,3.0,180.0,0,1,1,0,0,0,0,1.248816734,1.248816734,0,1,-0.981180225,-0.981180225,0,1,1,-4.15E-12,-4.15E-12,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

    -framequery {
        { SFT_H2_60_P_1 Z {} {730522208-730524007} Proc(0!1282.0!3.0FREQ!) }
    }
    -responsefiles {
        file:/lal/share/lal/earth00-04.ilwd,pass
        file:/lal/share/lal/sun00-04.ilwd,pass
    }
    -aliases sft
    -algorithms {
        output(_chN,,,_chN,_chN data);
    }
