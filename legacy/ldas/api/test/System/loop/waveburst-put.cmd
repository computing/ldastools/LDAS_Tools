putStandAlone
    -subject {putStandAlone waveburst loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/waveburst/process_1.ilwd  
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/waveburst/process_2.ilwd 
    }
    -database {ldas_tst}
