package provide qatest 1.0

namespace eval ::tcltest {
    variable auto_path {}
    variable verbose "ps"
}

proc ::tcltest::processCmdLineArgsAddFlagsHook {} {
    return [list -auto_path]
}

proc ::tcltest::processCmdLineArgsHook {flagArray} {
    array set flag $flagArray
    if {[info exists flag(-auto_path)]} {
        #set ::tcltest::auto_path $flag(-auto_path)
        set ::auto_path [concat $flag(-auto_path) $::auto_path]
    }

    return
}

if {[info proc ::QA::Options::TclTestUsage] != "" } {
    ::QA::Options::TclTestUsage
}

namespace eval QA::TclTest {
    namespace export DebugPuts
    proc DebugPuts {level string} {
	if {[::tcltest::configure -debug] >= $level} {
	    puts $string
	}
	return
    }

    namespace export IsJobInErrorState
    proc IsJobInErrorState { JobIDName } {
	global $JobIDName

	if { [info exists $JobIDName] } {
	    upvar 0 $JobIDName jobid

	    if {[string compare $jobid(status) error] == 0} {
		return 1
	    }
	}
	return 0
    }

    namespace export OutputDir
    proc OutputDir { } {
	return [::tcltest::configure -tmpdir]
    }

    namespace export SubmitJob
    proc SubmitJob {JobIDName Command args} {
	global $JobIDName

	;##--------------------------------------------------------------
	;## Parse through the options
	;## NOTE: Currently there are no supported options
	;##--------------------------------------------------------------
	#set argc [llength $args]
	#for {set idx 0} {$idx < $argc} {incr idx} {
	#puts "idx $idx [lindex $args $idx]"
	#    switch -exact -- [lindex $args $idx] {
	#	default {
	#	    puts "ERROR: SubmitJob: Bad option: [lindex $args idx]"
	#	}
	#    }
	#}
	;##--------------------------------------------------------------
	;## Make sure there are no artifacts left over from a previous
	;##   run
	;##--------------------------------------------------------------
	if [info exists $JobIDName] {
	    ;##----------------------------------------------------------
	    ;## Remove resource previously bound to this job
	    ;##----------------------------------------------------------
	    LJdelete $JobIDName
	}
	;##--------------------------------------------------------------
	;## Submit the job
	;##--------------------------------------------------------------
	catch { eval LJrun $JobIDName -nowait \
		   -manager ${::HOST}:${::PORT} \
		   -globus $::USE_GLOBUS_CHANNEL \
		   -gsi $::USE_GSI [ join $args ] \$Command} err
		   
	if {[info exists err] && ! [regexp -nocase -- $::QA::JobIDPattern $err]} {
	    set err [string trim $err]
	    error "Error executing LJrun: $err"
	}
	upvar 0 $JobIDName jobid
	if {[IsJobInErrorState $JobIDName]} {
	    ;##----------------------------------------------------------
	    ;## This was an unexpected error
	    ;##----------------------------------------------------------
	    set errmsg [string trim $jobid(error)]
	    error "Error from LJrun: $errmsg"
	}

	DebugPuts 1 $jobid(jobid)
	;##--------------------------------------------------------------
	;## Wait for the job to complete
	;##--------------------------------------------------------------
	catch {LJwait $JobIDName}
	if {[IsJobInErrorState $JobIDName]} {
	    ;##----------------------------------------------------------
	    ;## This was an unexpected error
	    ;##----------------------------------------------------------
	    set errmsg [trimReply $jobid(error)]
	    error "Error from LJrun: $errmsg"
	}
    }

    namespace export SubTests
    proc SubTests {TestArray SubTestNames} {
	upvar $TestArray testinfo
	upvar $SubTestNames subtests

	set subtests [list]
	foreach subtest [array names testinfo] {
	    if {[regexp {^(;)?\#} $subtest]} {
		## Skip anything that looks like a comment in the array
		continue
	    }
	    lappend subtests [lindex [split $subtest :] 0]
	}
	set subtests [lsort -dictionary -unique $subtests]
    }
}

;## The following order is significant
set auto_path_save $::auto_path
package require tcltest 2.2
set ::auto_path ". $auto_path"
