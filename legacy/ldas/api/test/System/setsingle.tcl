#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib .
package require qalib
package require LDASJob

qaInit

proc jobdir {reply} {
    set jobdir ""
    regexp {here:\s*(\S+)\s} $reply -> jobdir
    return $jobdir
}

set cmd {
    conditionData
        #-outputformat {ilwd ascii}
        -datacondtarget frame
        -setsingledc $::setsingle
        -aliases {
            AS_Q=H2\\:LSC-AS_Q::AdcData;
            LVEA_SEISX=H0\\:PEM-LVEA_SEISX::AdcData;
            X_SEISX=H0\\:PEM-MX_SEISX::AdcData;
        }
        -algorithms {
            rAS_Q = resample(AS_Q, 1, 64);
            clear(AS_Q);

            psdAS_Q = psd(rAS_Q, 8192, _, 4096, 1);
            clear(psdAS_Q);

            diff = sub(X_SEISX, LVEA_SEISX);
            psd1 = psd(X_SEISX, 8192, _, 4096, 1);
            output(psd1,_,_,psd1,PSD);

            psdDiff = psd(diff, 8192, _, 4096, 1);
            output(psdDiff,_,_,psd2,PSD);

            psdDiff = psd(diff, 8192, _, 4096, 1);
            output(psdDiff,_,_,psd3,PSD);
        }
        -framequery { R H {} 693960000-693960031:allow_gaps Adc(H2:LSC-AS_Q,H0:PEM-LVEA_SEISX,H0:PEM-MX_SEISX) }
}

set ::setsingle 1
catch {LJrun job1 -nowait -manager ${::SITE} $cmd} err
if {$LJerror} {
    puts $err
    puts $::job1(error)
    exit 1
}

puts $::job1(jobid)

catch {LJwait job1} err
if {$LJerror} {
    puts $err
    puts $::job1(error)
    exit 1
}

set ::setsingle 0
catch {LJrun job2 -nowait -manager ${::SITE} $cmd} err
if {$LJerror} {
    puts $err
    puts $::job2(error)
    exit 1
}

puts $::job2(jobid)

catch {LJwait job2} err
if {$LJerror} {
    puts $err
    puts $::job2(error)
    exit 1
}

#sleep a bit
after 30000

###
###
###
set ::jobid $::job1(jobid)
set ::jobdir [jobdir $::job1(jobReply)]
set ::fname "Z-P_LDAS_$::job1(jobnum)_datacondAPI-688011000-32"
set cmd {
    getFrameData
        -outputformat {ilwd ascii}
        -returnprotocol http://${::fname}
        -framequery { {} {} $::jobdir/${::fname}.gwf {} full }
}

catch {LJrun job3 -nowait -manager ${::SITE} $cmd} err
if {$LJerror} {
    puts $err
    puts $::job3(error)
    exit 1
}

puts $::job3(jobid)

catch {LJwait job3} err
if {$LJerror} {
    puts $err
    puts $::job3(error)
    exit 1
}

puts $::job3(outputs)
LJcopy $::job3(outputs) [pwd]

set ::jobid $::job2(jobid)
set ::jobdir [jobdir $::job2(jobReply)]
set ::fnameList [list \
    "Z-P_LDAS_$::job2(jobnum)_datacondAPI_psd1-688011000-32" \
    "Z-P_LDAS_$::job2(jobnum)_datacondAPI_psd2-688011000-32" \
    "Z-P_LDAS_$::job2(jobnum)_datacondAPI_psd3-688011000-32"
]

set num 4
foreach ::fname $::fnameList {
    catch {LJrun job$num -nowait -manager ${::SITE} $cmd} err
    if {$LJerror} {
        puts $err
        puts [set ::job${num}(error)]
        exit 1
    }

    puts [set ::job${num}(jobid)]

    catch {LJwait job${num}} err
    if {$LJerror} {
        puts $err
        puts [set ::job${num}(error)]
        exit 1
    }

    puts [set ::job${num}(outputs)]
    LJcopy [set ::job${num}(outputs)] [pwd]
    incr num
}

