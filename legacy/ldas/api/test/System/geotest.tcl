#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::geotest::test {
    ;##==================================================================
    ;## Bring commonly used items into the local namespace
    ;##==================================================================
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::TclTest::SubTests
    namespace import ::QA::URL::List
    namespace import ::QA::Rexec
    
    ;##==================================================================
    ;## Declaration of namespace variables
    ;##==================================================================
    variable TestName        geotest
    variable DefaultExpected "Your results:"
    variable DefaultArgs
    variable ShortTimes
    variable DefaultTimes    {714096132-714096251}
    variable DefaultChannels {G1:DER_H_QUALITY!1 G1:DER_H_HP-EP!1}
    variable ChannelsDownsample
    variable DownsamplePattern {(G1:DER_H_HP-EP!)(\d+)}

    variable PipelineTests
    variable DefaultPipelineOptions

    ##-------------------------------------------------------------------
    ## RDS initialization
    ##-------------------------------------------------------------------
    array set DefaultRDSOptions {
        -times            $DefaultTimes
        -type             {R}
        -usertype         {}
        -outputdir        {}
        -usejobdirs       {0}
        -compressiontype  {raw}
        -compressionlevel {6}
        -filechecksum     {0}
        -frametimecheck   {1}
        -framedatavalid   {1}
	-metadatacheck    0
        -channels         $DefaultChannels
    }

    array set RDSTests {
        # {This is the control test. It does nothing special.}
        control:opt:-subject   {success without resampling}

        # {Short frame files}
        shortframes:expected      "yield a final frame file which is shorter in length than the rest"
        shortframes:opt:-subject {exception shorter frames}
        shortframes:opt:-times   $ShortTimes

        # {Testing of downsampling of factor 2}
        downsample2:opt:-subject    {with resamping factor 2}
        downsample2:opt:-channels  $ChannelsDownsample(2)

        # {Testing of downsampling of factor 4}
        downsample4:opt:-subject    {with resamping factor 4}
        downsample4:opt:-channels  $ChannelsDownsample(4)

        # {Testing of downsampling of factor 8}
        downsample8:opt:-subject    {with resamping factor 8}
        downsample8:opt:-channels  $ChannelsDownsample(8)

        # {Testing of downsampling of factor 16}
        downsample16:opt:-subject    {with resamping factor 16}
        downsample16:opt:-channels  $ChannelsDownsample(16)
    }

    ##-------------------------------------------------------------------
    ## Pipeline initialization
    ##-------------------------------------------------------------------
    array set DefaultPipelineOptions {
        -filterparams {}
        -datacondtarget datacond
        -outputformat {ilwd ascii}
        -framequery { { R G {} 714096132-714096251 Adc(G1:DER_H_QUALITY) } }
        -aliases {
            x = G1\\:DER_H_QUALITY::AdcData;
         }
        -algorithms {
            output(x,_,der_data_h.ilwd,x,input data);
        }
    }

    array set PipelineTests {
        # {no metadata}
        nometa:expected "No metadata for data pipeline 4\n\nYour results:"
        nometa:opt:-subject {tfclu online running on G1:DER_H_QUALITY datacond only }

        # {dataond resamle 8}
        resample8:expected "No metadata for data pipeline"
        resample8:opt:-subject {Downsample by a factor of 8}
        resample8:opt:-dynlib          /ldas/lib/wrapperAPI/libio.so
        resample8:opt:-filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
        resample8:opt:-datacondtarget  wrapper
        resample8:-metadatatarget  datacond
        resample8:opt:-multidimdatatarget  ligolw
        resample8:opt:-database        ldas_tst
        resample8:opt:-state           {}
        resample8:opt:-np 3
        resample8:opt:-aliases {
            gw = G1\\:DER_H_QUALITY::AdcData;
        }
        resample8:opt:-algorithms {
            rgw = resample(gw, 1, 8);
            output(rgw,_,_,,resampled gw timeseries);
	}

        # {dataond resample 48}
        resample48:expected "No metadata for data pipeline"
        resample48:opt:-framequery { { LIGO_RDS G {} {715608032-715608271} Adc(G1:DER_H_QUALITY,G1:DER_H_HP-EP)} } \

        resample48:opt:-subject {Downsample by a factor of 8}
        resample48:opt:-dynlib          /ldas/lib/wrapperAPI/libio.so
        resample48:opt:-filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
        resample48:opt:-datacondtarget  wrapper
        resample48:-metadatatarget  datacond
        resample48:opt:-multidimdatatarget  ligolw
        resample48:opt:-database        ldas_tst
        resample48:opt:-state           {}
        resample48:opt:-np 3
        resample48:opt:-aliases {
            gw = G1\\:DER_H_QUALITY::AdcData;
        }
        resample48:opt:-algorithms {
            rgw = resample(gw, 1, 48);
            output(rgw,_,_,,resampled gw timeseries);
	}
    }

    ##-------------------------------------------------------------------
    ## Finish initializing some variables
    ##-------------------------------------------------------------------
    set ShortTimes [split $DefaultTimes -]
    set ShortTimes [join [list [lindex $ShortTimes 0] \
                              [expr ([lindex $ShortTimes 1] - 1)]] -]

    foreach r {2 4 8 16} {
        regsub -all -- $DownsamplePattern $DefaultChannels "\\1$r" \
            ChannelsDownsample($r)
        Puts 10 "INFO: Downsampling $r: $ChannelsDownsample($r)"
    }
    ;##==================================================================
    ;## Start testing
    ;##==================================================================
    ##-------------------------------------------------------------------
    ## Attempt to query for the list of channels in the GEO frames
    ##-------------------------------------------------------------------
    test $TestName:getChannels:GeoQuery {} -body {
        set cmd "getChannels -returnprotocol http://daq -interferometer G -frametype {R} -metadata 1"
        append cmd " -time [lindex [split $DefaultTimes -] 0]"
        Puts 5 "INFO: cmd: $cmd"
        if {[catch {SubmitJob job $cmd} err]} {
            return $err
        }
        set result $job(jobReply)
        LJdelete job
        return $result
    } -match regexp -result "channel list for.+can be found at"

    ##-------------------------------------------------------------------
    ## RDS Testing using the GEO frames
    ##-------------------------------------------------------------------
    set subtests [list]
    foreach subtest [array names RDSTests] {
        if {[regexp {^(;)?\#} $subtest]} {
            ## Skip anything that looks like a comment in the array
            continue
        }
        lappend subtests [lindex [split $subtest :] 0]
    }
    set subtests [lsort -dictionary -unique $subtests]
    foreach subtest $subtests {
        ##---------------------------------------------------------------
        ## Setup some variable
        ##---------------------------------------------------------------
        if {[info exists RDSTests($subtest:expected)]} {
            set expected $RDSTests($subtest:expected)
        } else {
            set expected $DefaultExpected
        }
        ##---------------------------------------------------------------
        ## Put in the options specific for this command
        ##---------------------------------------------------------------
        ::QA::Command::ExtractOptions $subtest RDSTests opts
        ::QA::Command::OptionSubstitution DefaultRDSOptions opts opts
        set cmd "createRDS [subst [array get opts]]"
        Puts 5 "INFO: cmd: $cmd"

        test $TestName:createRDS:$subtest {} -body {
            if {[catch {SubmitJob job $cmd} err]} {
                return $err
            }
            set result $job(jobReply)
            LJdelete job
            return $result
        } -match regexp -result $expected
    }
    

    ##-------------------------------------------------------------------
    ## Pipeline testing of GEO frames
    ##-------------------------------------------------------------------
    SubTests PipelineTests subtests
    foreach subtest $subtests {
        ##---------------------------------------------------------------
        ## Setup some variable
        ##---------------------------------------------------------------
        if {[info exists PipelineTests($subtest:expected)]} {
            set expected $PipelineTests($subtest:expected)
        } else {
            set expected $DefaultExpected
        }
        ##---------------------------------------------------------------
        ## Put in the options specific for this command
        ##---------------------------------------------------------------
        ::QA::Command::ExtractOptions $subtest PipelineTests opts
        ::QA::Command::OptionSubstitution DefaultPipelineOptions opts opts
        set cmd "dataPipeline [subst [array get opts]]"
        Puts 5 "INFO: cmd: $cmd"

        test $TestName:dataPipeline:wrapper:$subtest {} -body {
            if {[catch {SubmitJob job $cmd} err]} {
                return $err
            }
            set result $job(jobReply)
            LJdelete job
            return $result
        } -match regexp -result $expected
    }

    ##-------------------------------------------------------------------
    ## Test that there is not an excessive amount of file descriptors
    ## :TODO: Make this test conditional if SITE is on the same box
    ## :TODO:   as where the tests are being run
    ## :TODO: Most of this should be a utilty script on the server side.
    ##-------------------------------------------------------------------

    test geotest:lsof {} -body {
        ##---------------------------------------------------------------
        ## PR3067 - Check for open file descriptors
        ##---------------------------------------------------------------
        if {[catch {::QA::ConnectToAgent} err]} {
            return $err
        }
        if {[catch {source /ldas_outgoing/LDASapi.rsc} err]} {
            return $err
        }
        regexp {(\d+)} [ glob -nocomplain ${::TOPDIR}/frameAPI/.frame*.lock ] -> fpid
        set cmd "lsof -p $fpid | grep -c S1GEO"
        Puts 5 "INFO: Rexec: $::DISKCACHE_API_HOST ldas $cmd"
        catch {Rexec $::DISKCACHE_API_HOST ldas $cmd} result
        return $result
    } -match regexp -result {^0\s+}

    #====================================================================
    # Testing is complete
    #====================================================================
    cleanupTests

} ;## namespace - ::QA::geotest::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::geotest::test
