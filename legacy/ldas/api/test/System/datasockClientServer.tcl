#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for datasock sending and receiving of ilwd objects
# assumes both client and server mode in this script.
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

if { [info exists env(PREFIX)] } {
    lappend ::auto_path $env(PREFIX)/lib/genericAPI
} elseif { [file isdirectory /ldas/lib/genericAPI] } {
    lappend ::auto_path /ldas/lib
}

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set auto_path "[file dirname $argv0] $::auto_path"
}

if	{ [ file exist /ldas_outgoing/cntlmonAPI/cntlmon.state ] } {
	source /ldas_outgoing/cntlmonAPI/cntlmon.state
} else {
	set ::LDAS ./ldas
	set ::API test
}
if	{ [ file exist /ldas_outgoing/LDASapi.rsc ] } {
	source /ldas_outgoing/LDASapi.rsc
} else {
	source /ldas/lib/LDASapi.rsc
}

set API test_server

package require generic

;## suppress logging
proc addLogEntry { args } { return {} }

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # command requested by driver
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        
    proc operator { cid args } {
        if  { [ catch {
            set errlvl 0
            set result "done!"
            set cmd [ cmd::result $cid ]
            regexp {^\{(.*)\}$} $cmd -> cmd
            regexp {^\"(.*)\"$} $cmd -> cmd
            set cmd "::QA::datasockClientServer::test::$cmd"
            if  {[catch { set result [eval $cmd] } errmsg ]} {
                set result $errmsg
            }
            catch { puts $cid "$errlvl\n$result" }
            catch { close $cid }
        } err ] } {
            puts  "[ info level 0 ] error: $err"
            catch { close $cid }
        }
    }
        
#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------

namespace eval ::QA::datasockClientServer::test {
    set dimset [ list ]
    set default_dimset { 1 10 100 1000 10000 100000 1000000 }
    set default_types { char_s char_u int_2s int_2u int_4s int_4u int_8s int_8u real_4  \
        real_8 lstring_2 lstring_4 lstring_8 lstring_16 complex_8 complex_16}    
    set localhost [ exec uname -n ]
    set serverp ""
    set ilwddir /ldas_usr/ldas/test/datasock
    set numSent 0 
    set numRecv 0
    set dimslimit 1000000
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--ilwddir} {string} \
    {string} \
    { set ::QA::datasockClientServer::test::ilwddir $::QA::Options::Value } 
::QA::Options::Add {} {--ilwdformat} {string} \
    {ilwd format binary or ascii} \
    { set ::QA::datasockClientServer::test::ilwdfmt $::QA::Options::Value ;
      set ::QA::datasockClientServer::test::ilwdfmtCL 1 }   
 ::QA::Options::Add {} {--resultdir} {string} \
    {ascii} \
    { set ::QA::datasockClientServer::test::resultdir $::QA::Options::Value }   
 ::QA::Options::Add {} {--testmode} {string} \
    {test_client or test_server} \
    { set ::QA::datasockClientServer::test::testmode $::QA::Options::Value }                                        	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::datasockClientServer::test {   

	##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
    
    set localhost [ exec uname -n ]
    set TESTCLIENTPORT 7000
    #array set test_client  "host     $localhost"
    array set test_client  "operator  [ expr { $TESTCLIENTPORT +  0} ]"
    array set test_client  "emergency [ expr { $TESTCLIENTPORT +  1 } ]"
    array set test_client  "data      [ expr { $TESTCLIENTPORT +  2 } ]"

    set TESTSRVRPORT 9000
    array set test_server  "host      $localhost"
    array set test_server  "operator  [ expr { $TESTSRVRPORT +  0} ]"
    array set test_server  "emergency [ expr { $TESTSRVRPORT +  1 } ]"
    array set test_server  "data      [ expr { $TESTSRVRPORT +  2 } ]" 

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # open a data listening socket
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    proc serverInit {} {
        set name [ namespace current ]
        set port [ set ::${name}::test_server(data) ]
        Puts 1 "server port $port"
        if { ![string length [ set ::${name}::serverp ] ] } {
            set ::${name}::serverp [datasocket -server "::${name}::serverConfig ::${name}::serverHandler" $port]
            Puts 1 "serverp [ set ::${name}::serverp ] for port $port"
            after 250
        }
    }

    proc serverConfig {service sid addr port} {
        if  { [ catch {
            fileevent $sid readable [list $service $sid $addr $port]
            fconfigure $sid -blocking on -buffering none -translation binary -encoding binary
        } err ] } {
            return -code error $err
        }
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # configure server socket when doing clientInit
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    proc serverHandler {sid addr port} {
        set name [ namespace current ]
        fileevent $sid readable {}

        if {[catch {
            set sender [lindex [fconfigure $sid -peername] 0]

            set datap [recvData $sid]
            catch {close $sid}

            ;## Connect to the sender
            set clientp [retry_connect $addr [ set ::${name}::test_client(data)] ]

            ;## Send the data back
            sendData $clientp $datap
            catch {close $clientp}
            destructElement $datap
        } err]} {
            Puts 0 "Error: $err:\n$::errorInfo"
        }
    }

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # client sends data to server port
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc sendData {sid datap} {
        set name [ namespace current ]
        if  {[catch {data_puts $sid $datap} errmsg]} {
            set sockerr [fconfigure $sid -error]
            Puts 1 "[myName]: $errmsg: $sockerr"
            return -code error "[myName]: $errmsg: $sockerr"
        }

        Puts 5 "SEND [incr ::${name}::numSent ]"
        return
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # client port to receive the data sent back by server 
    # after reading it off data socket
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc clientConfig {service cid addr port} {
    
        if  { [ catch {
            fileevent $cid readable [list $service $cid $addr $port]
            fconfigure $cid -blocking on -buffering none -translation binary -encoding binary
        } err ] } {
           return -code error $err
        }
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # handler of client socket
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
    proc clientHandler {cid addr port} {
        set name [ namespace current ]
        if  { [ catch {
            fileevent $cid readable {}
            set datap [recvData $cid]
            catch {close $cid}
            set ::${name}::elemp $datap
        } err ] } {
            Puts 0 "$cid $addr $port error: $err"
        }
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # retry connection to data socket
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc retry_connect {server port} {
        set maxtries 1000
        for {set retry 1} {$retry <= $maxtries} {incr retry} {
            if {[catch {datasocket $server $port} msg]} {
                Puts 5 "retry_connect to $server:$port: try $retry of $maxtries: $msg"
                after 100
                continue
            }
            return $msg
        }

        return -code error "Could not connect to $server:$port after $maxtries tries."
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # send object to server listening socket
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc clientSend {server datap thread} {
    
        if  { [ catch {
            set name [ namespace current ]
            set ::${name}::elemp {}
            set sid [retry_connect $server [ set ::${name}::test_server(data) ] ]

            __t::start
            set cpu_begin [exectime]

            sendData $sid $datap
            catch {close $sid}

            ;## Wait for the data to come back
            vwait ::${name}::elemp

            set cputime [expr {[exectime] - $cpu_begin}]
            set walltime [__t::mark]
        } err ] } {
            return -code error $err
        }
        return [list [ set ::${name}::elemp ] $cputime $walltime]
    }

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # compare the elements 
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
    proc cmpElementFiles { fname1 fname2 type dims iter} {
        set exitcode 0
        if  {[catch { exec diff $fname1 $fname2 } msg ]} {
            set exitcode -1
            if {[string equal "CHILDSTATUS" [lindex $::errorCode 0]]} {
                set exitcode [lindex $::errorCode 2]
            }
        }   

        switch -exact -- $exitcode {
            0 { }
            1 { Puts 0 "Error: element data differ: $type, $dims dims, iteration $iter: $msg" }
            default { Puts 0 "Error: [myName]: $msg" }
        }
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # output the stats 
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
    proc outPut {fout type dims cputimes walltimes} {
        foreach timetype { wall cpu } {
            set result [ mystats [ set ${timetype}times ] ]
            foreach {mean stddev cov} $result {break;}
                if { [ string equal $timetype "wall" ] } {
                set rate [ computeRate $type $dims $mean ]
                puts -nonewline $fout [ format "%10s\t%10s\t%s\t%.6f\t%.6f\t%3.2f\t" $type $dims $rate $mean $stddev $cov ]
            } else {
                puts -nonewline $fout [ format "%.6f\t%.6f\t%3.2f\t" $mean $stddev $cov ]
            }
        }
        puts $fout ""
        flush $fout
    }
    ;## to avoid out of memory, write element to temp file
    ;## compare the files, then delete the temp file
    proc outFile { objectp } {
        set fname [file join $::TMP ${objectp}.ilwd]
        set fout [ openILwdFile $fname "w" ]
        writeElement $fout $objectp ascii none
        closeILwdFile $fout
        return $fname
    }

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # compute round trip transfer rate
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc computeRate { type dims time } {
    
        if { [ regexp {char} $type ] } {
            set size 1
        } else {
            regexp {(\d+)} $type -> size
        }
        if  { [ string length $time ] } {
            set rateB [ expr {2 * ($dims * $size) / $time} ]
            set rateMB [ expr {$rateB / (1024.0 * 1024.0)} ]
        } else {
            set rateMB 0.0
            Puts 0 "dims $dims size $size time $time has bad time value"
        }

        return $rateMB
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # obtain ilwd object from file
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc getILwd { type dims withcont ilwdfmt } {
    
        set name [ namespace current ]
        set filename [ file join [ set ::${name}::ilwddir ] ${ilwdfmt} ${type}_${dims}.ilwd ] 
        Puts 5 "filename: $filename"
        set filwd "nofd"
        if  {[catch {
            set filwd [ openILwdFile $filename "r" ]
            Puts 5 "opened file: $filename"
            set datap [ readElement $filwd ]
            Puts 5 "read element: $datap"
        } err]} {
            catch { closeILwdFile $filwd }
            return -code error $err
        }

        catch { closeILwdFile $filwd }
        return $datap
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # read data off data socket
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    proc recvData {sid} {
        set name [ namespace current ]
        if  {[catch {data_gets $sid} elemp]} {
            Puts 1 "[myName]: $elemp"
            return -code error "[myName]: $elemp"
        }

        Puts 5 "RECV [incr ::${name}::numRecv ]"
        return $elemp
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # sendElements - sends element across data socket and validates the return
    # initiated by client
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc sendElements {server dimset types thread samples withcont ilwdfmt} {
       
        if  { [ catch { 
            set name [ namespace current ]
            namespace import [ namespace parent $name ]::*
            ;## open the result file
            ;## create the results directory
            set resultdir [ set ::${name}::resultdir ]
            set dimslimit [ set ::${name}::dimslimit ]
            if  { ! [ file exist $resultdir ] } {
                file mkdir $resultdir
            }

            if  { $withcont } {
                set conttext "with container"
                set wc "wc"
            } else {
                set conttext "without container"
                set wc "nc"
            }

            if  { $thread } {
                set t {_t}
                set threadtext "threaded"
            } else {
                set t {}
                set threadtext "non-threaded"
            }

            __t::start
            set hostname [ set ::${name}::localhost ]
            set fname "$resultdir/${hostname}_${server}_${wc}${t}.[clock format [ clock seconds ] -format "%H%M%S" ]"
            set fout [ open $fname a ]
            puts $fout "Data Socket Element Test ${hostname}->$server ${conttext} ${threadtext} ${samples} samples\n"

            foreach entry [ concat type dims rate wall_mean wall_stddev wall_cov cpu_mean cpu_stddev cpu_cov] {
                puts -nonewline $fout [ format "%s\t" $entry ]
            }
            puts $fout ""

            lappend timings "open_log_file [__t::mark]"
            
            if  { [ string equal all $dimset ] } {
                set dimset [ set ::${name}::default_dimset ]
            }
            
            if  { $withcont } {
                set contFlag "withContainer"
            } else {
                set contFlag "noContainer"
            }
            set start_time [ clock seconds ]
            ;## now send the elements
            if  {[catch {
                foreach dims $dimset {
                    Puts 5 "sending dims=$dims on $::tcl_platform(os)"

                    foreach type $types {
                    test datasock:$hostname->$server:dims$dims:$type:$contFlag:$ilwdfmt {} -body {
                        set cputimeL {}
                        set walltimeL {}

                        __t::start
                        set contp [ getILwd $type $dims $withcont $ilwdfmt]
                        lappend timings "getILwd_${type}_${dims} [__t::mark]"
                        
                        if  { $withcont } {
                            set datap $contp
                        } else {
                            set datap [ refContainerElement $contp 0 ]
                        }

                        if  { $dims < $dimslimit } {
                            __t::start
                            Puts 5 "$datap in [ outFile $datap ]"
                            set fname1 [ outFile $datap ]
                            lappend  timings "fileio1_${type}_${dims} [__t::mark]"

                            set fileio2 0
                            set filecmp 0
                        }             
                        ;## create a client
                        for { set ntimes 0 } { $ntimes < $samples } { incr ntimes 1 } {
                        ;## timing starts here
                        ;## send element to the server
                        ;## CPU time in microsecs
                        ;## repeat sending over 10 times to get the variance
                        ;## and standard deviation
                            set retval [clientSend $server $datap $thread]
                            set elemp [lindex $retval 0]
                            lappend cputimeL [lindex $retval 1]
                            lappend walltimeL [lindex $retval 2]
                            ;## out of memory for higher dims when comparing
                            if  { $dims < $dimslimit } {
                                __t::start
                                set fname2 [ outFile $elemp ]
                                set fileio2 [expr $fileio2 + [__t::mark]]

                                __t::start
                                cmpElementFiles $fname1 $fname2 $type $dims $ntimes
                                set filecmp [expr $filecmp + [__t::mark]]
                                file delete $fname2
                            }
                            destructElement $elemp
                        }  
                        destructElement $contp
                        set numsent [ set ::${name}::numSent ]
                        set numrecv [ set ::${name}::numRecv ]
                        if  { $numsent != $numrecv } {                        
                            error "sent $numsent but received $numrecv objects"
                        }
                        if { $dims < $dimslimit } {
                            file delete $fname1
                            lappend timings "fileio2total_${type}_${dims} $fileio2"
                            lappend timings "filecmptotal_${type}_${dims} $filecmp"
                        }
                        __t::start
                        outPut $fout $type $dims $cputimeL $walltimeL
                        lappend timings "output_${type}_${dims} [__t::mark]"
                        lappend timings "totalwall_${type}_${dims} [expr [join $walltimeL "+"]]"
                        list
                    } -result {}    
                    }
                }
            } err]} {
                Puts 0 "error: $err"
                set ::${name}::Error "Error: $err"
            }
            ;## compute time taken
            set secs [ expr [ clock seconds ] - $start_time ]
            set mins [ expr $secs / 60 ]
            set hour [ expr $mins / 60 ]
            set mins [ expr $mins % 60 ]
            set secs [ expr $secs % 60 ]
            puts $fout "Test completed in $hour:$mins:$secs. [ set ::${name}::Error ]"

            close $fout
        } err ] } {
            Puts 0 "[ info level 0 ]: $err"
        }
    }
    
    ;## we use this one rather than in genericAPI to handle
    ;## cpu values of 0 seconds.
    ;## maybe obsolete by use of genericAPI function stats
    proc mystats { samples } {
    set mean 0.0
    set S2 0.0
    set cov 0.0

    set N [ llength $samples ]

    if { $N <= 1 } {
        return [ list [ lindex $samples 0 ] 0 100 ]
    }

    ;## calculate the arithmetic mean
    set mean [ expr ([ join $samples + ]) / $N. ]

    ;## calculate the standard deviation
    foreach s $samples {
        set S2 [ expr { $S2+pow(($s-$mean),2) } ]
    }
    set S2 [ expr { $S2/($N-1) } ]
    set S [ expr { sqrt($S2) } ]

    ;## calcualte the % coefficient of variation
    if { $mean != 0 } {
        set cov [ expr { ($S/$mean)*100 } ]
    }

    ;## return the values in a formatted list
    return [ list $mean $S $cov ]
    }

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # command requested by driver
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc clientInit { server dimset types thread samples withcont ilwdfmt } {
    
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*  
         
        if {[string equal $dimset "all"]} {
            set ::${name}::dimset [ set ::${name}::default_dimset ]
        } else {
            set ::${name}::dimset [split $dimset ,]
        }

        if {[string equal $types "all"]} {
            set types [ set ::${name}::default_types ]
        } else {
            set types [split $types ,]
        }

        set ::${name}::Error ""
        Puts 1 "dimset=$dimset, types=$types, localhost=[ set ::${name}::localhost ]"

        ;## Start a server to receive objects back
        if {! [string length [ set ::${name}::serverp ] ] } {
            set ::${name}::serverp [datasocket -server "::${name}::clientConfig ::${name}::clientHandler" [ set ::${name}::test_client(data) ] ]
            after 250
        }

        
        sendElements $server [ set ::${name}::dimset ] $types $thread $samples $withcont $ilwdfmt
        set hostname [ set ::${name}::localhost]
        set msg "Test completed, results in ${hostname}_$server.times. [ set ::${name}::Error ]\n"
        Puts 1 $msg
        return $msg
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # cleanup
    #
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
           
    proc cleanup {} {    
        if  {[catch {
            set name [ namespace current ]
            catch { close [ set ::${name}::sock_operator ] }
            catch { close [ set ::${name}::sock_emergency ] }
            set serverp [ set ::${name}::serverp ]
            if  { [ string length $serverp ] } {
                close $serverp
                set ::${name}::serverp ""
            }
        } err] } {
            Puts 1 "[info level 0 ]: $err"
        }
        set ::${name}::serverp ""
        set ::${name}::done 1
    }
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # open up a tcl socket (operator) to receive cmds from test driver.
    # no need to have emergency socket
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc serverOpen { callback {port 0}} {
        set name [ namespace current ]
        set lsid [socket -server $callback -myaddr [info hostname] $port]
        foreach {addr host port } [fconfigure $lsid -sockname] {break}
        Puts 1 "addr $addr host $host port $port sid $lsid"
        return $lsid
    }

    proc serverCfgOperator  {sid addr port } {
    
	    fconfigure $sid -buffering line -blocking off
        fileevent $sid readable [ list ::operator $sid ]	
    }
    
    proc serverCfgEmergency  {sid addr port } {
    
	    fconfigure $sid -buffering line -blocking off
        fileevent $sid readable [ list ::emergency $sid ]	
    }
    
    proc serverClose {sid} {
        catch {close $sid}
        return
    }
    set sock_operator [ serverOpen operator [ set [ set testmode ](operator) ] ]  
    
    Puts 1 "test mode $testmode [ array get $testmode ] sock_operator $sock_operator "
    ;## make ssh happy
    catch {close stdin}

    vwait ::QA::datasockClientServer::test::done
    
    cleanup
        
    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::datasockClientServer::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::datasockClientServer::test
exit
