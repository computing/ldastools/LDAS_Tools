package provide qaS3 1.0

namespace eval QA {
    set CurrentDataSet S3

    set DataSet(${CurrentDataSet},start) 751800000
    set DataSet(${CurrentDataSet},dt) 16
    foreach site [list H L] {
	for { set level 1 } { $level <= 3 } { incr level } {
	    set DataSet(${CurrentDataSet},$site,level$level) \
		[ ::QA::LoadChannelInfo \
		      adcdecimate_${site}-RDS_R_L${level}-${CurrentDataSet}.txt
		 ]
	} ;# for
    } ;# foreach 
} ;# namespace - QA
