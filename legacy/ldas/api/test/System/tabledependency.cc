#include <iostream>
#include <list>
#include <memory>
#include <set>
#include <string>

#include "dbaccess/Table.hh"

int
main( int ArgC, char** ArgV )
try
{
  //---------------------------------------------------------------------
  // Get list of tables
  //---------------------------------------------------------------------
  std::set< std::string >	table_names_outputted;
  std::list< std::string >	table_names_result;
  std::list< std::string >	table_names;
  DB::Table::GetTableNames( table_names );

  //---------------------------------------------------------------------
  // generate a list of insertions that will work
  //---------------------------------------------------------------------
  while( table_names.size( ) > 0 )
  {
    for ( std::list< std::string >::iterator
	    cur = table_names.begin( ),
	    last = table_names.end( );
	  cur != last;
	   )
    {
      bool	resolved = true;
      std::auto_ptr< DB::Table > tbl( DB::Table::CreateTable( *cur ) );
      const std::vector< DB::Table::ForeignKey >& fk
	= tbl->GetForeignKeys( );
      for ( std::vector< DB::Table::ForeignKey>::const_iterator
	      fk_cur = fk.begin( ),
	      fk_last = fk.end( );
	    fk_cur != fk_last;
	    ++fk_cur )
      {
	if ( table_names_outputted.find( fk_cur->GetForeignTable( ) )
	     == table_names_outputted.end( ) )
	{
	  resolved = false;
	  break;
	}
      }
      if ( resolved == false )
      {
	//---------------------------------------------------------------
	// Unresolved dependency so need to wait till satisfied.
	//---------------------------------------------------------------
	++cur;
	continue;
      }
      table_names_result.push_back( *cur );
      table_names_outputted.insert( *cur );
      cur = table_names.erase( cur );
    }
  }
  for ( std::list< std::string >::const_iterator
	  cur = table_names_result.begin( ),
	  last = table_names_result.end( );
	cur != last;
	++cur )
  {
    std::cout << *cur << std::endl;
  }
}
catch( const std::exception& e )
{
  std::cerr << "ERROR: Caught exception: " << e.what( ) << std::endl;
  exit( 1 );
}
catch( ... )
{
  std::cerr << "ERROR: Caught an unknown exception" << std::endl;
  exit( 1 );
}
