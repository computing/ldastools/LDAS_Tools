#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

## cd /ldas_outgoing/logs/archive; ls -lrt will get in ascending order
## extract insertion logs from metadataAPI log
## 
## 673043414-673047893
;## for leapseconds

set auto_path "/ldas/lib . $auto_path"
source /ldas_outgoing/cntlmonAPI/cntlmon.state
source /ldas_outgoing/LDASapi.rsc

set API cntlmon
package require generic 

if	{ $argc != 3 } {
	puts "$argv0 <database> <range> <previous-results-dir>"
	exit
}
cd ./logs
if	{ ! [ regexp {logs} [ pwd ] ] } {
	puts "please run in the logs directory"
}
set dsname [ lindex $argv 0 ]
set range [ lindex $argv 1 ]
set prevdir [ lindex $argv 2 ]

set dirname "results-[ clock format [ clock seconds ] -format "%m%d" ]"

#catch { eval file delete -force $dirname }

if	{ ! [ file exist $dirname ] } {
	puts "creating directory $dirname"
	file mkdir $dirname
} 
cd $dirname
puts "working in directory $dirname"
set limit 10000
set pat "putMetaData inserted.+$::dsname"
puts "doing grepLogs $range $pat $limit {metadata}"

;## grepLogs may get stuck
;## if so generate the log html via cmonClient
;## regular expression putMetaData&inserted.+dev_1
;## for metadataAPI only
;## save the log file and scp up to dev
set ::env(RUNDIR) $::TOPDIR 
set result [ grepLogs $range $pat {metadata} ]

set fname insert${range}.log.html
puts "writing logs to $fname"
set fd [ open $fname w ]
puts $fd $result
close $fd

puts "running createplot.tcl"

catch { exec ../../createplot.tcl $fname } err
puts "createplot.tcl done\n$err"
set curfile [ glob *.txt ]
set prevfile [ glob ../$prevdir/*.txt ] 
puts "curfile $curfile,prevfile $prevfile."
catch { exec ../../dbstats.tcl $curfile $prevfile } err
puts "dbstats.tcl $curfile $prevfile done\n$err"

regexp {results[-\.](\S+)} $prevdir -> mmdd
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.$mmdd

;## compare to previous results 1.5.0 1.6.0 1.7.0
set txtfile [ glob *txt ]

set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.170/*.txt ]"
catch { eval $cmd } 
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.1.7.0

set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.180/*.txt ]"
catch { eval $cmd } 
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.1.8.0

set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.190/*.txt ]"
catch { eval $cmd } 
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.1.9.0
