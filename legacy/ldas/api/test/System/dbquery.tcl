#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::dbqchannel::test {}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::dbquery::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
	set TYPE DT_512
	set TIMES "610000000-610000511"
	set fname /ldas_outgoing/jobs/LDASTest/qchan.ilwd
	set user ldas
	set index 0
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
		source /ldas/lib/genericAPI/LDASapi.rsc 
	} else {
		set ::METADATA_API_HOST metaserver
	}
	QA::ConnectToAgent
	
	set cmdInit {
    dataPipeline
    -dynlib libldasinspiral.so
    -filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
    -datacondtarget wrapper
    -database ldas_tst
    -multidimdatatarget ligolw
    -responsefiles {
        file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
    }
    -aliases {gw=P2\\:LSC-AS_Q::AdcData;}
    -algorithms {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,,resampled gw timeseries);
        p = psd( rgw, 262144 );
        output(p,_,metadata,,spectrum data);
        output(p,_,_,,spectrum data);
    }
    -framequery {
        {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
    }}
	
	set cmdstem {
        dataPipeline
        -subject {$subject}
        -dynlib {$dynlib}
        -filterparams (0)
        -dbquery { {$sql} $pushpass query }
        -datacondtarget $dctarget
        -multidimdatatarget ligolw
        -database ldas_tst
        #-responsefiles {
        #    file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
        #}
        -aliases {gw=P2\\:LSC-AS_Q::AdcData;}
        -algorithms {$algo}
        -framequery {
            {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
        }
    }
	
	proc runTest {} {
		uplevel {
			set cmd [ subst $cmdstem ]
			Puts 1 "cmd $cmd"
			test dbquery:dataPipeline:$index:$subject:frameV4  {} -body {
				if 	{[catch {SubmitJob job $cmd} err]} {
					LJdelete job
					Puts 1 "Error: $err"
	    			return $err
				}
				set reply $job(jobReply)
				Puts 1 "$job(jobid)\nreply: $reply\nexpected $expected"
				LJdelete job
				return $reply
    		} -match regexp -result $expected
		}
	}
	
    set index 0
    set subject {DBQUERY:INIT:wrapper}
    set jobid none
	foreach cmd [ list $cmdInit ] {
		if 	{[catch {
			set cmd [ subst $cmd ]
			Puts 1 "cmd $cmd"
            test dbquery:dataPipeline:$index:$subject  {} -body {
                if  { [ catch {SubmitJob job $cmd} err]} {
                    Puts 1 "Error $err"
			        LJdelete job
	    	        return $err
		        }		       
			    set jobid [string trim $job(jobid) ${RUNCODE}]
			    Puts 0 "$job(jobid) ($job(LDASVersion)) INIT"
			    LJdelete job
            }
		} err] } {
			LJdelete job
			Puts 0 "Fail to initialize: $err, cannot run wrapper jobs"
		}
	}

	set index 1
	set dynlib libldastrivial.so
	set sql {select impulse_time, mchirp, sigmasq, search, ifo from sngl_inspiral fetch first 1 rows only}

	;## 1 - good types, push, wrapper
	set subject {DBQUERY:SIMPLE:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(query,_,_,,database query);
    }
	set expected "dbquery cannot be sent to MPI"
	runTest

	;## 2 - good types, pass, wrapper
	set subject {DBQUERY:SIMPLE:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "Cannot send arbitrary query results to target"
	runTest

	set dynlib {}
	;## 3 - good types, push, ligolw
	set subject {DBQUERY:SIMPLE:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(query,_,_,,database query);
    }
	set expected "can be found at:"
	runTest 

	;## 4 - good types, pass, ligolw
	set subject {DBQUERY:SIMPLE:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest

	set dynlib libldastrivial.so
	set sql {select impulse_time, mchirp, sigmasq, search, ifo from sngl_inspiral where search='_not_there_' fetch first 1 rows only}
	set index 2
	
	;## 5 - empty data, push, wrapper
	set subject {DBQUERY:EMPTY:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(query,_,_,,database query);
    }
	set expected "dbquery cannot be sent to MPI"
	runTest

	;## 6 - empty data, pass, wrapper
	set subject {DBQUERY:EMPTY:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "Cannot send arbitrary query results to target"
	runTest

	set dynlib {}
	;## 7 - empty data, push, ligolw
	set subject {DBQUERY:EMPTY:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(query,_,_,,database query);
    }
	set expected "can be found at:"
	runTest

	;## 8 - empty data, pass, ligolw
	set subject {DBQUERY:EMPTY:PASS:LIGOLW:wrapper}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest

	set dynlib libldastrivial.so
	set sql "select * from summ_spectrum where ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = $jobid and program = 'datacondAPI'))) fetch first 1 rows only"
	set index 3
	
	;## 9 - binary data, push, wrapper
	set subject {DBQUERY:BINARY:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(query,_,_,,database query);
    }
	set expected "dbquery cannot be sent to MPI"
	runTest

	;## 10 - binary data, pass, wrapper
	set subject {DBQUERY:BINARY:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "Cannot send arbitrary query results to target"
	runTest

	set dynlib {}
	;## 11 - binary data, push, ligolw
	set subject {DBQUERY:BINARY:PUSH:LIGOLW:wrapper}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(query,_,_,,database query);
    }
	set expected "can be found at:"
	runTest

	;## 12 - binary data, pass, ligolw
	set subject {DBQUERY:BINARY:PASS:LIGOLW:wrapper}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest 

   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests

} ;## namespace - ::QA::dbquery::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::dbquery::test	
