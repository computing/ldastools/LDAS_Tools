#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::dbqchannel::test {}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::dbqchannel::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
	;## use ldas_tst as trival dso can only go into ldas_tst
	set dbname ldas_tst
	set TYPE DT_512
	set TIMES "610000000-610000511"
	set fname /ldas_outgoing/jobs/LDASTest/qchan.ilwd
	set user ldas
	set index 0
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
		source /ldas/lib/genericAPI/LDASapi.rsc 
	} else {
		set ::METADATA_API_HOST metaserver
	}
	QA::ConnectToAgent
	
	;## Init
	set cmdInit {
    	dataPipeline
    	-datacondtarget metadata
    	-database $dbname
    	-aliases {gw=P2\\:LSC-AS_Q::AdcData;}
    	-algorithms {
        x = slice(gw, 0, 128, 1);
        p = psd(x, 128);
        output(p,_,_,,spectrum data);
    	}
    	-framequery {
        {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
    	}
	}
	
    set cmdstem {
        dataPipeline
        -subject {$subject}
        -dynlib {$dynlib}
        -filterparams (0)
        -dbqualitychannel {
            $sample_rate
            $start_time
            $start_time_ns
            $end_time
            $end_time_ns
            {$sql}
            $pushpass
            qchan
        }
        -datacondtarget $dctarget
        -multidimdatatarget ligolw
        -database $dbname
        -aliases {gw=P2\\:LSC-AS_Q::AdcData;}
        -algorithms {$algo}
        -framequery {
            {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
    	}
	}
	
	set cmdPut "
    putMetaData
    -ingestdata $fname
    -database $dbname"
		
	proc dumpIlwd {fname jobid} {
    set fid [open $fname w]
    puts -nonewline $fid \
"<?ilwd?>
<ilwd name='ligo:ldas:file' size='2'>
    <ilwd name='processgroup:process:table' size='17'>
        <int_4s name='processgroup:process:creator_db'>1</int_4s>
        <int_4s name='processgroup:process:cvs_entry_time'>683950510</int_4s>
        <int_4s name='processgroup:process:is_online'>0</int_4s>
        <int_4s name='processgroup:process:unix_procid'>[pid]</int_4s>
        <int_4s name='processgroup:process:start_time'>686775928</int_4s>
        <int_4s name='processgroup:process:end_time'>0</int_4s>
        <int_4s name='processgroup:process:param_set' nullmask='g'>0</int_4s>
        <int_4s name='processgroup:process:jobid'>$jobid</int_4s>
        <lstring name='processgroup:process:program' size='16'>SegGener        </lstring>
        <lstring name='processgroup:process:version' size='3'>1.2</lstring>
        <lstring name='processgroup:process:cvs_repository' size='74'>/ldcg_server/common/repository_gds/dmt/src/monitors/SegGener/SegGener.cc,v</lstring>
        <lstring name='processgroup:process:comment' size='40'>Segment generation from an OSC condition</lstring>
        <lstring name='processgroup:process:node' size='8'>fortress</lstring>
        <lstring name='processgroup:process:username' size='16'>jzweizig        </lstring>
        <lstring name='processgroup:process:ifos' size='12'>H0 H1 H2    </lstring>
        <lstring name='processgroup:process:domain'></lstring>
        <ilwd name='processgroup:process:process_id'>
            <char dims='20'>process:process_id:0</char>
        </ilwd>
    </ilwd>
    <ilwd name='segmentgroup:segment:table' size='8'>
        <ilwd name='segmentgroup:segment:process_id' size='5'>
            <char dims='20'>process:process_id:0</char>
            <char dims='20'>process:process_id:0</char>
            <char dims='20'>process:process_id:0</char>
            <char dims='20'>process:process_id:0</char>
            <char dims='20'>process:process_id:0</char>
        </ilwd>
        <int_4s dims='5' name='segmentgroup:segment:creator_db'>1 1 1 1 1</int_4s>
        <int_4s dims='5' name='segmentgroup:segment:version'>$jobid $jobid $jobid $jobid $jobid</int_4s>
        <int_4s dims='5' name='segmentgroup:segment:start_time'>680894217 680894625 680895239 680895267 680895315</int_4s>
        <int_4s dims='5' name='segmentgroup:segment:start_time_ns'>0 0 0 0 0</int_4s>
        <int_4s dims='5' name='segmentgroup:segment:end_time'>680894218 680894626 680895240 680895268 680895316</int_4s>
        <int_4s dims='5' name='segmentgroup:segment:end_time_ns'>0 0 0 0 0</int_4s>
        <lstring dims='5' name='segmentgroup:segment:segment_group' size='68'>H2:IFOLocked\\,H2:IFOLocked\\,H2:IFOLocked\\,H2:IFOLocked\\,H2:IFOLocked</lstring>
    </ilwd>
</ilwd>
"
    close $fid
    file attributes $fname -permissions 0644
	}
	
   	proc clearDB { { mydb "" } } {
                upvar user user
                if      { ! [ string length $mydb ] } {
                        upvar dbname dbname
                        set mydb $dbname
                }
                set err [ Rexec $::METADATA_API_HOST $user "/usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
                DB2INSTANCE=ldasdb del_db_process.tcl $mydb unix_procid=18298" ]
                Puts 1 "$mydb cleanup $err"
    }
	
	Puts 1 "clearing out database $dbname test data ..."
	clearDB
	
	proc runTest {} {
		uplevel {
			set cmd [ subst $cmdstem ]
			Puts 1 "cmd $cmd"
			test dbqchannel:dataPipeline:$index:$subject  {} -body {
				if 	{[catch {SubmitJob job $cmd} err]} {
					LJdelete job
					Puts 1 "Error: $err"
	    			return $err
				}
				set reply $job(jobReply)
				Puts 1 "$job(jobid)\nreply $reply\nexpected $expected"
				LJdelete job
				return $reply
    		} -match regexp -result $expected
		}
	}

	set index 0
	foreach cmd [ list $cmdInit $cmdPut ] {
		if 	{[catch {
			set cmd [ subst $cmd ]
			Puts 1 "cmd $cmd"
			SubmitJob job $cmd
			incr index 
			set jobid [string trim $job(jobid) ${RUNCODE}]
			Puts 0 "$job(jobid) ($job(LDASVersion)) INIT$index"			
		} err]} {
			LJdelete job
			Puts 1 "Fail to initialize: $err; Aborting Test"
	    	return $err
		}
	}

	dumpIlwd $fname $jobid

	set dynlib libldastrivial.so
	set sample_rate 1024
	set start_time 680894200
	set start_time_ns 0
	set end_time 680895300
	set end_time_ns 0
	set sql "select start_time as startsec, end_time as StopSec, start_time_ns as StartnanoSec, end_time_ns as stopNanosec from segment where version=$jobid"

	set index 1
	;## 1 - good types, push, wrapper
	set subject {DBQCHANNEL:GOOD:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "produced no products"
	runTest

	;## 2 - good types, pass, wrapper
	set subject {DBQCHANNEL:GOOD:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "produced no products"
	runTest 
	
	set dynlib {}
	;## 3 - good types, push, ligolw
	set subject {DBQCHANNEL:GOOD:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "can be found at:"
	runTest

	;## 4 - good types, pass, ligolw
	set subject {DBQCHANNEL:GOOD:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest
	
	set index 2
	set dynlib libldastrivial.so
	set sql {select start_time as startsec, end_time as StopSec, start_time_ns as StartnanoSec, end_time_ns as stopNanosec from segment where segment_group='_not_there_'}

	;## 5 - empty data, push, wrapper
	set subject {DBQCHANNEL:EMPTY:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "produced no products"
	runTest
	
	;## 6 - empty data, pass, wrapper
	set subject {DBQCHANNEL:EMPTY:PASS:wrapper}	
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "produced no products"
	runTest 

	set dynlib {}
	;## 7 - empty data, push, ligolw
	set subject {DBQCHANNEL:EMPTY:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "can be found at:"
	runTest
	
	;## 8 - empty data, pass, ligolw
	set subject {DBQCHANNEL:EMPTY:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest

	set dynlib libldastrivial.so
	set sql "select start_time as startsec, end_time as stopsec, spectrum as startnanosec, spectrum as stopnanosec from summ_spectrum where ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = $jobid))) fetch first 1 rows only"
	set index 3
	
	;## 9 - binary data, push, wrapper
	set subject {DBQCHANNEL:BINARY:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "type is not appropriate"
	runTest

	;## 10 - binary data, pass, wrapper
	set subject {DBQCHANNEL:BINARY:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "type is not appropriate"
	runTest

	set dynlib {}
	;## 11 - binary data, push, ligolw
	set subject {DBQCHANNEL:BINARY:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "type is not appropriate"
	runTest

	;## 12 - binary data, pass, ligolw
	set subject {DBQCHANNEL:BINARY:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "type is not appropriate"
	runTest

	set dynlib libldastrivial.so
	set sql "select start_frequency as startsec, delta_frequency as stopsec, mimetype as startnanosec, channel as stopnanosec from summ_spectrum where ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = $jobid))) fetch first 1 rows only"
	set index 3
	
	;## 13 - bad data, push, wrapper
	set subject {DBQCHANNEL:BAD:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "type is not appropriate"
	runTest 

	;## 14 - bad data, pass, wrapper
	set subject {DBQCHANNEL:BAD:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "type is not appropriate"
	runTest 

	set dynlib {}
	;## 15 - bad data, push, ligolw
	set subject {DBQCHANNEL:BAD:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "type is not appropriate"
	runTest

	;## 16 - bad data, pass, ligolw
	set subject {DBQCHANNEL:BAD:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "type is not appropriate"
	runTest 
 
	set dynlib libldastrivial.so
	set sql "select start_time as startsec, end_time as StopSec, start_time_ns as StartnanoSec, end_time_ns as stopNanosec, substr(segment_group, 1, 2) as IFO from segment where version=$jobid"
	set index 4
	
	;## 17 - ifo data, push, wrapper
	set subject {DBQCHANNEL:IFO:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "produced no products"
	runTest

	;## 18 - ifo data, pass, wrapper
	set subject {DBQCHANNEL:IFO:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "produced no products"
	runTest

	set dynlib {}
	;## 19 - ifo data, push, ligolw
	set subject {DBQCHANNEL:IFO:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "can be found at:"
	runTest
	
	;## 20 - ifo data, pass, ligolw
	set subject {DBQCHANNEL:IFO:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest 

	set dynlib libldastrivial.so
	set sql "select start_time as startsec, end_time as StopSec, start_time_ns as StartnanoSec, end_time_ns as stopNanosec, version from segment where version=$jobid"
	set index 5
	
	;## 21 - extra data, push, wrapper
	set subject {DBQCHANNEL:EXTRA:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "produced no products"
	runTest

	;## 22 - extra data, pass, wrapper
	set subject {DBQCHANNEL:EXTRA:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	runTest

	set dynlib {}
	;## 23 - extra data, push, ligolw
	set subject {DBQCHANNEL:EXTRA:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "can be found at:"
	runTest 

	;## 24 - extra data, pass, ligolw
	set subject {DBQCHANNEL:EXTRA:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "can be found at:"
	runTest 

	set dynlib libldastrivial.so
	set sql "select start_time, end_time, start_time_ns, end_time_ns from segment where version=$jobid"
	
	set index 6
	;## 25 - wrong data, push, wrapper
	set subject {DBQCHANNEL:WRONG:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(qchan,_,_,,quality channel);
    }
	set expected "Unknown column name"
	runTest

	;## 26 - wrong data, pass, wrapper
	set subject {DBQCHANNEL:WRONG:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "Unknown column name"
	runTest
	
	set dynlib {}
	;## 27 - wrong data, push, ligolw
	set subject {DBQCHANNEL:WRONG:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(qchan,_,_,,quality channel);
    }
	set expected "Unknown column name"
	runTest

	;## 28 - wrong data, pass, ligolw
	set subject {DBQCHANNEL:WRONG:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "Unknown column name"
	runTest
	
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests

} ;## namespace - ::QA::dbqchannel::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::dbqchannel::test	
