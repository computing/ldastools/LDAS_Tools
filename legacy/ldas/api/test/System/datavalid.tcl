#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
use_ligotools="/ligotools/bin/use_ligotools"
# \
test -x $use_ligotools && eval `$use_ligotools`
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}

if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib
package require LDASJob
package require tconvert

proc sendCmd {} {	
	uplevel {
		puts $cmd		
		catch {LJrun job -email ${::LADDR}:${::LPORT} -manager $::SITE -user $::USER $cmd} err	
		ckLJrun
	}

}

proc validateResult { reply } {

	if	{ [ info exist ::expected ] } {	 	
		if	{ [ regexp -nocase -- $::expected $reply -> fname filedir ] } {
			set ::output $filedir/$fname
			set files [ file tail $::output ]
			set dir  [ file dir $::output ]
			set ::resultfiles [ list ]
			foreach file $files {
				if	{ [ file exist $dir/$file ] && [ file size $dir/$file ] } {	
					lappend ::resultfiles $dir/$file
				} else {
					puts "ERROR: no output $dir/$file found in job directory"
					set error 1
				} 
			}
			if	{ ! [ info exist error ] } {	
				puts "-----------------PASS\n"
				incr ::passed 1
			} else {
				incr ::failed 1
			}  
		} elseif { [ regexp -nocase -- $::expected $reply ] } {
			puts "----------------------PASS\n"
			incr ::passed 1
		} else {
			puts "----------------------FAIL, expected '$::expected'\n"
			incr ::failed 1
		}
	}

}

proc dumpRDSFrameData {} {
	uplevel {
	set cmd "exec $::FRDUMP -H -e -l -proc [ join $::resultfiles " " ]"
	puts $cmd
	catch { eval $cmd } data
	puts "$::FRDUMP of proc data\n$data"		
	}
}

source testlib.tcl
set auto_path "/ldas/lib $auto_path"
set API metadata
source /ldas_outgoing/LDASapi.rsc
source /ldas_outgoing/cntlmonAPI/cntlmon.state

if	{ [ regexp {cit|dev|mit|llo|lho} $::LDAS_SYSTEM ] } {
	set LOOPDIR /ldas_usr/ldas/test/loop
	set TESTDIR /ldas_usr/ldas/test/database
} else {
	set LOOPDIR /ldas_outgoing/test/loop
	set TESTDIR [ pwd ]
}

set ::HOST [ exec uname -n ]
set ::PORT 10001
set ::WAIT 40000
set ::WAIT_MODS 0Time
set ::WAIT_INCR 10000
set ::TIMEOUT_RECV 50
set ::TIMEOUT_SEND 300000
set ::LOGFILE [file rootname [file tail $::argv0]].log
set ::TIMESTAMP {%m/%d/%y-%H:%M:%S}
set ::FRDUMP /ldas/bin/framecpp_dump_objects

if	{ ! [ string equal $::HOST $::MANAGER_API_HOST ] } {
	set SITE $::MANAGER_API_HOST
} else {
	set SITE [ exec uname -n ]
}

package require generic
set passed 0
set failed 0
set localport 0
set start_time [ gpsTime now ]

;## open a port to listen for returns
set sid [serverOpen $localport ]
puts "BEGN: [gpsTime] $::LHOST $::LADDR $::LPORT"
puts "WAIT: $::WAIT"
clearCmd
set curdir [ pwd ]
set ::USER mlei

if  { [ regexp {box|suntest} $::SITE ] } {
    set ::SITE $::HOST:10001
}
puts "site $::SITE"

;## getFrameData

set cmd "getFrameData \
	-subject {dataValid frames test }
    -returnprotocol http://results \
    -outputformat LIGO_LW \
    -framequery {R C {} 781034000-781034015 Adc(C1:PSL-126MOPA_DMON,C1:IOO-MC_COMMON_GAINCALC,C1:IOO-MC_F_MON)}"

set expected "Your results:\nresults.xml" 
sendCmd

set cmd "getFrameData \
	-subject {dataValid frames test }
    -returnprotocol http://results \
    -outputformat frame \
    -framequery {R C {} 781034080-781034095 Adc(C1:PSL-126MOPA_DMON,C1:IOO-MC_COMMON_GAINCALC,C1:IOO-MC_F_MON)}"

set expected "Your results:\n(\.+.gwf).+($::TOPDIR\[^\n\]+)" 
sendCmd

catch { exec FrCheck -i $::output } data
puts "FrCheck -i $::output\n$data"

catch { exec $::FRDUMP -H -a C1\:IOO-MC_COMMON_GAINCALC -e -l adc $::output } data
puts "$::FRDUMP -i $::output\n$data"

;## createRDS partial frame exception
set cmd "createRDS \
	-subject {dataValid frames test }
    -times {781034016-781034100} \
    -type {R} \
    -usertype {} \
    -outputdir {} \
    -usejobdirs {0} \
    -compressiontype {raw} \
    -compressionlevel {6} \
    -filechecksum {0} \
    -frametimecheck {1} \
    -framedatavalid {1} \
    -channels {C1:PSL-126MOPA_DMON!2 C1:IOO-MC_L!2 C1:SUS-PRM_LLVMon!2}"
    
set expected "yield a final frame file which is shorter in length than the rest"
sendCmd

;## createRDS
set cmd "createRDS \
	-subject {dataValid frames test }
    -times {781034016-781034127} \
    -type {R} \
    -usertype {} \
    -outputdir {} \
    -usejobdirs {0} \
    -compressiontype {raw} \
    -compressionlevel {6} \
    -filechecksum {0} \
    -frametimecheck {1} \
    -framedatavalid {1} \
    -channels {C1:PSL-126MOPA_DMON!2 C1:IOO-MC_L!2 C1:SUS-PRM_LLVMon!2}"
    
set expected "Your results:\n(C-R\[^\n\]+)\n.+($::TOPDIR\[^\n\]+)"
sendCmd
dumpRDSFrameData

;## createRDS with resampling ( request 16 sec after and 16 sec before to support the 2 extra frames
;## for resample and to avoid gaps.

set cmd "createRDS \
    -times {781034016-781034127} \
    -type {R} \
    -usertype {} \
    -outputdir {} \
    -usejobdirs {0} \
    -compressiontype {raw} \
    -compressionlevel {6} \
    -filechecksum {0} \
    -frametimecheck {1} \
    -framedatavalid {1} \
    -channels {C1:PSL-126MOPA_DMON!2 C1:IOO-MC_L!2 C1:SUS-PRM_LLVMon!2 C1:PEM-stacis_EEEX_pzt!2}"
    
set expected "Your results:\n(C-R\[^\n\]+)\n.+($::TOPDIR\[^\n\]+)"
sendCmd
dumpRDSFrameData

;## guild command: getChannels
set cmd "getChannels -returnprotocol http://daq -interferometer C -frametype R -time 781034000 -metadata 1"

set expected "Your results:\nC-R-781034000-16.channels" 
sendCmd

;## dataPipeline
set cmd { dataPipeline 
    -subject         {dataValid frames test}
    -dynlib          libldastrivial.so
    -filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
    -datacondtarget  wrapper
    -metadatatarget  datacond
    -multidimdatatarget  ligolw
    -database        ldas_tst
    -state           {}
    -np 3
    -framequery {
        { R C {} {781034000-781034128} Adc(C1:SUS-MC2_LLYAW_GAIN,C1:SUS-ITMX_OL_YAW,C1:SUS-MC3_LLVMon)}
    }
    -responsefiles {}
    -aliases {
        gw = C1\:SUS-ITMX_OL_YAW::AdcData;
    }
    -algorithms {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,,resampled gw timeseries);       
    } }



set expected "produced no products" 
sendCmd

puts "$::passed passed, $::failed failed"
