#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

set client [ lindex $argv 0 ]
set loglevel [ lindex $argv 1 ]
set patterns [ list "hash updated" "does not exist" missing "overlap error(s)" duplicate "MOUNT_PT updated" \
"errors!; Body:" "update dt:" "Connection timed out" ]

foreach pattern $patterns {
	set total($pattern) 0
}

set logfiles [ glob /ldas_outgoing/logs/cmonClient/${client}_Log-Filter-${loglevel}_Page*.html ]
puts "logfiles $logfiles"

set total_count 0
set diskcache_all_total 0

foreach logfile $logfiles {
	foreach pattern $patterns {
		set rc [ catch { exec grep -c $pattern $logfile} data ]		
    	if 	{ !$rc } {
			if	{ [ regexp {(\d+)} $data -> count ] } {
				incr total($pattern) $count
			} 
		}
	}

	set rc [ catch { eval exec grep -E diskcacheAPI.+lines $logfile } data ]
	if	{ !$rc } {
		regexp {diskcacheAPI\s+\((\d+) lines\)} $data -> diskcache_total
		incr diskcache_all_total $diskcache_total
	} else {
		puts "grep error for $pattern in $logfile"
	}
}

set text "Total in diskcache $diskcache_all_total lines\n"

foreach item [ lsort [ array names total ] ] {
	append text "Found [ set total($item) ] for '$item'\n" 
	incr total_count [ set total($item) ]
}

set diff [ expr $diskcache_all_total - $total_count ]
if	{ $diff } {
	append text "Found $diff extra lines\n"
}
puts [ string trim $text \n ]

set fd [ open /ldas_outgoing/diskcacheAPI/diskcache.log r ]
set data [ read -nonewline $fd ]
close $fd

set data [ split $data \n ]
set succeeded 0
set failed 0
set retries [ list ]
set numLines [ llength $data ]
set fail_tries [ list ]
set count 0
set last_int_percent 0

foreach line $data {
	set percent [ expr ($count * 1.0)/$numLines * 100.0  ]
	# puts "percent $percent, count $count, numLines $numLines, mod5 [ expr int($percent)%5 ] "
	set int_percent [ expr int($percent) ]
	if	{ ! [ expr $int_percent%25 ] && ( $int_percent != $last_int_percent ) } {
		puts "${int_percent}% done ..."
		set last_int_percent $int_percent
	}
	incr count 
	if	{ [ regexp {Succeeded in acquiring lock after (\d+) tries} $line -> tries ] } {
		incr succeeded 
		lappend retries $tries
	} elseif { [ regexp {Failed to acquire lock after (\d+) tries} $line -> tries ] } {
		incr failed 
		lappend fail_tries $tries
	}
}

set retries [ lsort -integer -unique $retries ]
set fail_tries [ lsort -integer -unique $fail_tries ]
puts "Successful retry ranges: $retries\nFailed retries $fail_tries times"
puts "succeeded $succeeded count $count failed $failed \
success rate for acquiring lock [ expr ($succeeded * 1.0/$count) * 100.0 ]%"
