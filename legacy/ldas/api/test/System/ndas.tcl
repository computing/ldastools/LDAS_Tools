#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

lappend ::auto_path /ldas/lib ..
if      { ! [ regexp "/ldas/bin" $::env(PATH) ] } {
        set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

package require LDASJob

set ::SITE dev
set ::USER ""
set ::VLEVEL 0
set ::USE_GLOBUS_CHANNEL 0

set ::CMD {
    dataPipeline
    -subject { stochastic job for H2:LSC-AS_Q and L1:LSC-AS_Q }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget ligolw
    -metadataapi ligolw
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst

    -framequery {
        $framequery
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

        # Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    }
}

# S2 data RDS
#set ::queryList {
#    {{ R {} {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#
#    {{ RDS_R_L1 {} {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#
#    {{ RDS_R_L2 {} {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#
#    {{ RDS_MERGED_L1 HL {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#
#    {{ NDAS GHLV {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#}

# S3 data RDS has no NDAS frames for this period
# so need S2 data for ndas verification for the last 2 tests
set ::queryList {
    {{ R {} {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}

    {{ RDS_R_L1 {} {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}

    {{ RDS_R_L2 {} {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}

    {{ RDS_MERGED_L1 HL {} 751800016-751803568 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
    
    {{ R {} {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}

    {{ NDAS GHLV {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
}

# Use this to test only S2 data for ndas only
#set ::queryList {
#    {{ R {} {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#
#    {{ NDAS GHLV {} 730524100-730524982 Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }}
#}

proc sendJob {site cmd {varlist ""}} {
    set useropt ""
    if {[string length $::USER]} {
        set useropt "-user $::USER"
    }

    catch {eval LJrun job -nowait -manager $site $useropt -globus $::USE_GLOBUS_CHANNEL \$cmd} errmsg
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg "$::job(error)"
        }
        LJdelete job
        return -code error "Error from LJrun: [string trim $errmsg]"
    }

    vputs [string trim $::job(jobInfo)] "$::job(jobid) ($::job(LDASVersion))"

    catch {LJwait job} errmsg
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg "$::job(error)"
        }
        LJdelete job
        return -code error "Error from LDAS: [string trim $errmsg]"
    }

    vputs [string trim $::job(jobReply)] [strip $::job(jobReply)]

    set retval [list]
    foreach var $varlist {
        lappend retval $::job($var)
    }
    LJdelete job

    return $retval
}

proc vputs {msg1 {msg2 ""} args} {
    if {$::VLEVEL > 0} {
        puts stdout $msg1
    } elseif {[string length $msg2]} {
        puts stdout $msg2
    }
    flush stdout

    return ""
}

proc strip {data} {
    regsub -all -- {[\n\s\t]+} $data { } data
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set data [string replace $data $idx end]
    }
    return [string trim $data]
}

proc printUsage {args} {
    exit 1
}

;## MAIN ###
for {set idx 0} {$idx < $::argc} {incr idx} {
    set opt [lindex $::argv $idx]
    switch -glob -- $opt {
        -s {set ::SITE [lindex $::argv [incr idx]]}
        -u {set ::USER [lindex $::argv [incr idx]]}
        -v {incr ::VLEVEL 1}
        -* {
            ;## Unknown option
            puts stderr "Error: Invalid option '$opt'."
            printUsage
        }
         default {}
    }
}

foreach framequery $::queryList {
    if {[catch {sendJob $::SITE [subst -nobackslashes $::CMD]} errmsg]} {
        return -code error "$errmsg"
    }
}

exit 0
