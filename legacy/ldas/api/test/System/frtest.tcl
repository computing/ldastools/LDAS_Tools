#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::frtest::test {
}
#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

::QA::Options::Add {} {--dump} {string} \
    {Directory where to dump script files} \
    {set ::QA::frtest::test::DumpDirectory $::QA::Options::Value}
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if { [ regexp -- {[-]?-dump} $argv ] } {
    ;##------------------------------------------------------------------
    ;## Setup for dumping commands
    ;##------------------------------------------------------------------
    ::QA::Options::Parse ::argv
    set ::argc [llength $::argv]
} else {
    ;##------------------------------------------------------------------
    ;## Normal execution
    ;##------------------------------------------------------------------
    package require qalib
    qaInit
}

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::frtest::test {
    ;##------------------------------------------------------------------
    ;## Establish namespace variables
    ;##------------------------------------------------------------------
    variable DumpDirectory
    variable Result
    variable Match

    proc ExpectedResults {Command Output SubTest} {
        variable Result [list]
        variable Match exact

        if {[expr ([string compare $Command getFrameElements] == 0) \
                 && ([string compare $Output frame] == 0) \
                 && ([regexp -- {^[12]frame$} $SubTest]) ] } {
            
            set Result "*getFrameElements -outputformat option syntax uses the 'frame' keyword to produce seperate ilwd files for each input frame when used in combination with the 'ilwd' and 'ascii' or 'binary' keywords. when 'frame' is used by itself it is ambiguous for the getFrameElements user command.*"
            set Match glob
        }
    }
    ;##------------------------------------------------------------------
    ;## Establish private variables
    ;##------------------------------------------------------------------
    set framedir [file join "/" scratch test frames assorted]
    set commandList {
        getFrameData
        getFrameElements
        concatFrameData
    }

    set formatList {
        ilwd
        {ilwd ascii}
        {ilwd binary}
        LIGO_LW
        {LIGO_LW base64}
        frame
        {frame ilwd}
        {frame ilwd ascii}
        {frame ilwd binary}
    }

    set frameList1 [ list \
                         [file join $framedir P-DT_1-600000000-1.gwf] \
                         [file join $framedir P-DT_16-600000000-16.gwf] \
                         [file join $framedir P-DT_64-600000000-64.gwf] \
                        ]

    set frameList2 [ list \
                         [file join $framedir P-DT_1-600000001-1.gwf] \
                         [file join $framedir P-DT_16-600000016-16.gwf] \
                         [file join $framedir P-DT_64-600000064-64.gwf] \
                        ]

    set type {}
    set ifo P
    set query {Adc(0)}
    set times {}

    set cmd {
        $command
        -returnprotocol http://results
        -outputformat [list $format]
        -framequery [list [list $type $ifo $frame $times $query]]
    }

    ;##""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    ;## Check if the request is to make scripts for use by cmon server.
    ;##""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    if {[info exists DumpDirectory]} {
        if {[string length $DumpDirectory] > 0} {
            ;## :TODO: Ensure the directory actually exists before trying
            ;## :TODO:   to write to it
            ;##--------------------------------------------------------------
            ;## Dump the scripts to the specified directory
            ;##--------------------------------------------------------------
            set format LIGO_LW
            set frame {}
            ;##--------------------------------------------------------------
            ;## :TODO: Times should come from a run file of data
            ;##--------------------------------------------------------------
            set times {855975200-855975201}
            set type R
            ;##--------------------------------------------------------------
            ;## :TODO: IFO needs to be dynamic because not all sites have
            ;## :TODO:   Hanford datasets.
            ;##--------------------------------------------------------------
            set ifo H
            set query Adc(H2:LSC-AS_Q)
            foreach command $commandList {
                set fid [open [file join $DumpDirectory ${command}.cmd] w]
                puts $fid [string trim [subst $cmd]]
                close $fid
            }
            exit 0
        } else {
            puts stderr "ERROR: DumpDirectory not specified"
        }
    }
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    ;##------------------------------------------------------------------
    ;## Test - 1 frame file
    ;##------------------------------------------------------------------
    Puts 1 ""
    Puts 1 "************************************"
    Puts 1 "* TEST SUITE: frtest: 1 frame file *"
    Puts 1 "************************************"
    Flush
    foreach command $commandList {
        foreach format $formatList {
            foreach frame $frameList1 {
                
                ExpectedResults $command $format 1frame
                test frtest:1frame:${command}:[list ${format}]:[file tail ${frame}]:frameV4 {} -body {
                    set scmd [subst $cmd]
                    if {[catch {SubmitJob job $scmd} err]} {
                        return $err
                    }
                    ::QA::URL::Check $job(outputs)
                    LJdelete job
                    list
                } -match $Match -result $Result
                Flush
            }
        }
    }
    ;##------------------------------------------------------------------
    ;## Test - 2 frame file
    ;##------------------------------------------------------------------
    Puts 1 ""
    Puts 1 "************************************"
    Puts 1 "* TEST SUITE: frtest: 2 frame file *"
    Puts 1 "************************************"
    Flush
    foreach command $commandList {
        foreach format $formatList {
            foreach frame1 $frameList1 frame2 $frameList2 {
                set frame [list $frame1 $frame2]

                ExpectedResults $command $format 2frame
                test frtest:2frame:${command}:[list ${format}]:[file tail $frame1],[file tail $frame2]:frameV4 {} -body {
                    set scmd [subst $cmd]
                    if {[catch {SubmitJob job $scmd} err]} {
                        return $err
                    }
                    ::QA::URL::Check $job(outputs)
                    LJdelete job
                    list
                } -match $Match -result $Result
                Flush
            }
        }
    }

    ;##------------------------------------------------------------------
    ;## Test - full query
    ;##------------------------------------------------------------------
    Puts 1 ""
    Puts 1 "**********************************"
    Puts 1 "* TEST SUITE: frtest: full query *"
    Puts 1 "**********************************"
    Flush

    set query {full()}
    set command getFrameData
    foreach format $formatList {
        foreach frame $frameList1 {

            ExpectedResults $command $format fullquery
            test frtest:fullquery:${command}:[list ${format}]:[file tail ${frame}]:frameV4 {} -body {
                set scmd [subst $cmd]
                if {[catch {SubmitJob job $scmd} err]} {
                    return $err
                }
                ::QA::URL::Check $job(outputs)
                LJdelete job
                list
            }  -match $Match -result $Result
            Flush
        }
    }

    #====================================================================
    # Testing is complete
    #====================================================================
    cleanupTests

} ;## namespace :QA::frtest::test
