#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1502::test {
    set ValidateMode 1
    set Start 730000016    
} ;## namespace ::QA::pr2122::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--startgps} {string} \
    {frame gps start time} \
    { set ::QA::pr1502:test $::QA::Options::Value }


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr1502::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730000016

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 1
    set Finish [ expr $Start + $Dt - 1]

    set basiccmd { conditionData
        -subject { PSD }
        -framequery { R H {} $Start-$Finish Adc(H1\:LSC-AS_Q) }
        -aliases {gw = _ch0;}
        -algorithms {
	    x = slice(gw, 0, 16384, 1);
	    output(x, _, result.ilwd, x1, time series 1);
	    p = psd(x, 8192);
	    output(p, _, result.ilwd, p1, psd 1);
	
	    x = slice(gw, 16384, 16384, 1);
	    output(x, _, result.ilwd, x2, time series 2);
	    p = psd(x, 16384);
	    output(p, _, result.ilwd, p2, psd 2);
        }
        -datacondtarget frame
        -setsingledc 1
        -returnprotocol { http://result.ilwd }
        -outputformat { ilwd ascii }
    }
    set cmd [ subst $basiccmd ]
    test regression:pr1502:conditionData:Start {} -body {
        if  { [ catch {
            SubmitJob job $cmd
        } err ] } {
            set jobid none
            if { [ info exists job(jobid) ] } {
	            set jobid "$job(jobid): "
            }
            if  { [ info exist job(error) ] } {
                set joberror $job(error)
                error "${jobid}Error message is: $joberror"
            } else {
                Puts 1 $job(jobreply)
            }
        }
        LJdelete job

    } -match regexp -result {}
    
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr1502::test    
