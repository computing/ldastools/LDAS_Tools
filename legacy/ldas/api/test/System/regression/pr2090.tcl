#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2090::test {

	set cmdstem {createRDS
    -subject { pr2090 - Exception mis-handling of read-protected frames }
	-times ${Start}-${end}
	-type ${type}
	-channels H1:LSC-AS_Q
	-usertype AS_Q
	[join $args]
    }
    
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2090
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr2090::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    if  { [ catch {
        set retval 1
        set Start 730000000
        set dt 16
        set end [expr $Start + $dt - 1]
        set ifos H
        set type No_Read_Permissions
    #--------------------------------------------------------------------
    # Establish a timeout for the job
    #--------------------------------------------------------------------
        set args [ list -framechecksum 0 -filechecksum 0 \
			      -metadatacheck 0 ]
        set cmd [ subst $cmdstem ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set expected "EACCES|Permission denied"
        
        ;## results: _0000000100495e00_p_tid:/scratch/test/frames/assorted/H-No_Read_Permissions-730000000-16.gwf: \
        ;## permission_denied: (EACCES) requested access to the file or directory was denied.
        
        test regression:$thispr:$ldascmd:start=$Start:type=$type:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputs outputs               
                    set outputs $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
        } -match regexp -result $expected
        
    } err ] } {
        Puts 0 $err
    }
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2090::test
