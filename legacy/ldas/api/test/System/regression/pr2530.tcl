#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2530::test {
    set cmdstem { createRDS
    -subject { pr2530 - framedatavalid to flag bad data}
	-times $TimeRange
	-type { R }
	-usertype { RDS_R_L }
	-usejobdirs 0
	-channels { L1:LSC-AS_Q L1:LSC-REFL_Q L1:IFO-SV_STATE_VECTOR }
	-compressiontype { gzip }
	-compressionlevel { 1 }
	-filechecksum { $FileChecksum }
	-frametimecheck { $FrameTimeCheck }
	-framedatavalid { $FrameDataValid }
	[join $args]
    }
} ;## namespace ::QA::pr2530::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2530 - ---no-metadata-check option
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2530::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------

    set tr "751791904-751792159"
    set args [list -framechecksum 0]

    proc  Check {TimeRange FileChecksum FrameTimeCheck FrameDataValid expected args} {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
    #--------------------------------------------------------------------
    # Generate an RDS in our working directory
    #--------------------------------------------------------------------
        if  { [ catch {
            set cmd [ subst [ set ::${name}::cmdstem ] ]
            set ldascmd createRDS 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            test regression:[ set ::${name}::thispr ]:$ldascmd:FileChecksum=$FileChecksum:FrameTimeCheck=$FrameTimeCheck:FrameDataValid=$FrameDataValid {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar reply reply                    
                    set reply $Job(jobReply)
                    Puts 5 $reply
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
        } err ] } {
            return -code error $err
        }
    }
    
    if  { [ catch {
        Check $tr 0 0 0 ".+" $args
    } err ] } {
        Puts 0 $err 
    }
    
    if  { [ catch {
        Check $tr 1 1 1 "The dataValid field for channel 'L1:LSC-REFL_Q' in file '.*' is non-zero" $args
    } err ] } {
        Puts 0 $err 
    } 
    
    if  { [ catch {
        Check $tr 0 0 1 "The dataValid field for channel 'L1:LSC-REFL_Q' in file '.*' is non-zero" $args
    } err ] } {
        Puts 0 $err 
    } 
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2530::test
