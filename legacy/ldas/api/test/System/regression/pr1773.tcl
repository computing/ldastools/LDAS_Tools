#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1773::test {

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set framedir [file join "/" scratch test frames assorted]
    set SpectrumStartSecond 609999999
    set SpectrumStartNanosecond 995117188

    set cmdstem { dataPipeline
	-subject { pr1773 - FrDump6 core dumps with inspiral shared object}
	-dynlib {libldasinspiral.so}
	-filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,1,1,1,(4.0,4.0))
	-dbspectrum {
	    channel={resample(P2:LSC-AS_Q,1,8)}
	    spectrum_type=Welch
	    spectrum_length=131073
	    start_time=${SpectrumStartSecond}
	    start_time_ns=${SpectrumStartNanosecond}
	    start_frequency=0.0000000000000000e+00
	    delta_frequency=7.8125000000000000e-03
	    pushpass=push
	    alias=spec
	}
	-datacondtarget wrapper
	-multidimdatatarget frame
	-database ldas_tst
	-responsefiles {
	    file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
	}
	-aliases {gw=P2\:LSC-AS_Q::AdcData;}
	-algorithms {
	    rgw = resample(gw, 1, 8);
	    output(rgw,_,_,:primary,resampled gw timeseries);
	    output(spec,_,_,spectrum:psd,spectrum data);
	}
	-framequery { {} {} {$frame} {} {Adc(P2:LSC-AS_Q)} }
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1773
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1773::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    set frame [file join $framedir P-DT_512-610000000-512.gwf]
    
    if  { [ catch {
        set cmd [ subst -nobackslashes $cmdstem ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        set expected ".+"
            
        test regression:$thispr:$ldascmd:Start=${SpectrumStartSecond}:wrapper:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    Puts 0 $joberror
                    return -code error $joberror
                }
                return $reply
        } -match regexp -result $expected

        if  { [ info exist Job ] } {
	#----------------------------------------------------------------
	# Copy the file from the server to the local disk
	#----------------------------------------------------------------
	        if { ! [ regexp {^([^:]*):(.*)$} $job(manager) -> host port ] } {
	            set host $job(manager)
                Puts 0 $Job(jobReply)
	        }
            test regression:$thispr:$ldascmd:Start=${SpectrumStartSecond}:wrapper:validate {} -body {
	            foreach frame $Job(outputs) {
	                regexp {^.*/([^/]*)$} $frame -> base_name
	                if { [ regexp {^/} $frame ] } {
		# Add http protocol so LJcopy will be able to
		#   retrieve the file
		                set frame "http://${host}${frame}"
	                }
	                LJcopy $frame $base_name
	                set cmd "$::FRDUMP $base_name"
	                set exit_status [ ProgramExitStatus $cmd ]
	                if { [ expr $exit_status ] } {
                        return -code error "Failed to $::FRDUMP: $frame"
                    }
                }
                file delete $base_name
	        } -result {}
        }
	} err ] } {
        Puts 0 $err
    }

    #--------------------------------------------------------------------
    LJdelete job
    
    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1773::test
