#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2127::test {

    set cmdstem { createRDS
        -subject { pr2127 - incomplete outputs for last frame before gap }
        -times { ${Start}-${Finish} }
        -type { R }
        -channels { H1:LSC-AS_Q }
        -compressiontype { raw }
    }

} ;## namespace ::QA::pr2406::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2127 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2127::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730000064

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 32
    set Finish [ expr $Start + $Dt - 1]

    set expected "Verification exit code: FILE_OPEN_ERROR"
    set cmd [ subst $cmdstem ]
    set ldascmd [ lindex $cmd 0 ]
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    set subject [ string trim $subject ]
    Puts 5 "cmd $cmd"
    
    test regression:$thispr:$ldascmd:INVALID_FRAME_STRUCTURE:submit {} -body {
        
        if  { [ catch {
            upvar Job Job
            SubmitJob Job $cmd
            set reply $Job(jobReply)
            Puts 5 $reply
        } err] } {
            if  { [ info exist Job(error) ] } {                        
                set joberror $Job(error)
            } else {
                set joberror $err
            }
            LJdelete Job
            Puts 5 "Error: $joberror"
            return -code error $joberror
        }
        LJdelete Job
        Puts 5 "Succeeded: $reply"
        return $reply
    } -match regexp -result $expected

   flush [outputChannel]
   ;##==================================================================
   ;## Testing is complete
   ;##==================================================================

    cleanupTests
} ;## namespace ::QA::pr2127::test
