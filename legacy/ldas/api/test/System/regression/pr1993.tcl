#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
    . /ldas/libexec/setup_tclsh.sh
# \
    exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1993::test {


    #------------------------------------------------------------------------
    # First part of command to seed the data values
    #------------------------------------------------------------------------
    set cmdstem1 { dataPipeline
	-subject { pr1993 - ndas frames }
	-dynlib libldasstochastic.so
	-filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)
	-np 2
	-multidimdatatarget ligolw
	-metadataapi ligolw
	-concatenate 1
	-realtimeratio 0.9
	-datacondtarget wrapper
	-datadistributor WRAPPER
	-outputformat { ilwd ascii }
	-database ldas_tst
	#-setsingleem 1

	$framequery
	-responsefiles {
	    file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
	    file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
	}
	-aliases {
	    gw1 = H2\:LSC-AS_Q::ProcData;
	    gw2 = L1\:LSC-AS_Q::ProcData;
	}
	-algorithms {
	    z1 = value(gw1);
	    clear(gw1);
	    z2 = value(gw2);
	    clear(gw2);
	    z1 = slice(z1, 2058, 1804280, 1);
	    z2 = slice(z2, 2058, 1804280, 1);
	    
	    # Patch to make psd's work
	    z1s = slice(z1, 0, 1794048, 1);
	    z2s = slice(z2, 0, 1794048, 1);

	    psd1 = psd(z1s, 16384, _, 8192);
	    output(psd1,_,_,,spectrum1);
	    psd2 = psd(z2s, 16384, _, 8192);
	    output(psd2,_,_,,spectrum2);
	    output(z1,_,_,,data1);
	    output(z2,_,_,,data2);
	}
    }
    set cmdstem2 { getFrameData
        -subject { pr1993 - ndas frames }
        -returnprotocol http://results
        -outputformat LIGO_LW
        $framequery 
    }
    set expected(cmdstem1) "channel: L1:LSC-AS_Q does not exist in frame: 803"
    set expected(cmdstem2) "channel: L1:LSC-AS_Q does not exist in frame: 803"
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1993
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1993::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    if  { [ catch {
        set start 730524400
        set end 730525282
        set ifos GHLV
        set types RDS
        set framequery "-framequery {{${types} $ifos {} ${start}-${end} Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!)}}"
	
        foreach cmdstem [ list cmdstem1 cmdstem2 ] {
	    set cmd [ subst -nobackslashes [ set $cmdstem ] ]
	    set ldascmd [ lindex $cmd 0 ] 
	    regexp -- {-subject \{([^\}]+)} $cmd -> subject
	    set subject [ string trim $subject ]
	    Puts 5 "cmd $cmd"
	    set expected_result $expected($cmdstem)
	    
	    #------------------------------------------------------------
	    # Establish a timeout for the job
	    #------------------------------------------------------------
	    
	    test regression:$thispr:$ldascmd:start=$start:end=$end:frame:submit {} -body {
		if  { [ catch {
		    upvar Job Job
		    SubmitJob Job $cmd
		    upvar outputs outputs               
		    set outputs $Job(outputs)
		    set reply $Job(jobReply)
		} err] } {
		    if  { [ info exist Job(error) ] } {                        
			set joberror $Job(error)
		    } else {
			set joberror $err
		    }
		    LJdelete Job
		    return -code error $joberror
		}
		LJdelete Job
		return $reply
	    } -match regexp -result $expected_result
        }
        
	#--------------------------------------------------------------------
	# All finished with the check
	#--------------------------------------------------------------------

    } err ] } {
        Puts 0 $err
    }

    flush [outputChannel]  
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1993::test
