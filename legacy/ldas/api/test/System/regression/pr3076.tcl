#!/bin/sh
# -*- mode: tcl; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
exec /usr/bin/env tclsh "$0" ${1+"$@"}

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr3076::test {
    set ValidateMode 1
} ;## namespace ::QA::pr3076::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--validateMode} {string} \
    {dataPipeline or datacond} \
    { if    { [ string equal datacond $::QA::Options::Value ] } { \
            set ::QA::pr3076::test::ValidateMode 2 \
      } else { \
            set ::QA::pr3076::test::ValidateMode 1 \
      } }

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr3076::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    

    #--------------------------------------------------------------------
    # Expect manager to return unknown user error and close socket
    # user noname must have a username/password in ~install/.ldaspw
    # or ~ldas/.ldaspw if running as ldas
    #--------------------------------------------------------------------
	
    ## test unknown user
    set retval 1

    set basiccmd {getMetaData
    	-returnprotocol file:/foo
    	-outputformat {LIGO_LW}
    	-database {ldas_tst}
   	 	-sqlquery {select * from process fetch first 100 rows only} }
    
    set cmd [ subst $basiccmd ]
    set expected "unknown user"

    test regression:pr3076:getMetaData:unknownUser {} -body {
        if 	{ [catch {
            SubmitJob job $cmd -user noname -globus 0 -gsi 0
        } err] } {
            if  { [ info exist job(error) ] } {
                set joberror $job(error)
            }
		    LJdelete job
	        return -code error $err
        }
        if  { [ info exist job(jobReply) ] } {
            set reply $job(jobReply)
        } else {
            set reply none
        }
        LJdelete job
        return $reply    
    } -match regexp -result $expected


    #--------------------------------------------------------------------
    # Expect manager to return invalid options error and close socket
    #--------------------------------------------------------------------
	
    ## test invalid option
    set retval 1

    #====================================================================
    # Execute a command 
    # specify md5sum substitutions
    #====================================================================
    set basiccmd {getMetaData
    	-returnprotocol file:/foo
    	-outputformat {LIGO_LW} junk here 
    	-database {ldas_tst}
   	 	-sqlquery {select * from process fetch first 100 rows only} }
    set cmd [ subst $basiccmd ]
    
    set expected "validateopts"
    test regression:pr3076:getMetaData:invalidOption {} -body {
        if 	{ [catch {
            SubmitJob job $cmd
        } err] } {
            if  { [ info exist job(error) ] } {
                set joberror $job(error)
            } else {
                set joberror $err
            }
		    LJdelete job
	        return -code error $err
        }
    } -match regexp -result $expected
    
    flush [outputChannel]
    ##==================================================================
    ## Testing is complete
    ##==================================================================
    cleanupTests
} ;## namespace ::QA::pr3076::test

namespace delete ::QA::pr3076::test
