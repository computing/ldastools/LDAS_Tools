#------------------------------------------------------------------------
# Global Variables
#------------------------------------------------------------------------
set ::USE_GLOBUS_CHANNEL 0 

if [info exists ::env(FRDUMP)] {
    set ::FRDUMP $::env(FRDUMP)
} else {
    set ::FRDUMP /ldas/bin/framecpp_dump_objects
}

if [info exists ::env(FRDUMPTOC)] {
    set ::FRDUMPTOC $::env(FRDUMPTOC)
} else {
    set ::FRDUMPTOC /ldas/bin/framecpp_dump_toc
}

if [info exists ::env(FRVERIFY)] {
    set ::FRVERIFY $::env(FRVERIFY)
} else {
    set ::FRVERIFY /ldas/bin/framecpp_verify
}

if [info exists ::env(FRSAMPLE)] {
    set ::FRSAMPLE $::env(FRSAMPLE)
} else {
    set ::FRSAMPLE /ldas/bin/framecpp_sample
}

if [info exists ::env(FRQUERY)] {
    set ::FRQUERY $::env(FRQUERY)
} else {
    set ::FRQUERY /ldas/bin/framecpp_query
}

if [info exists ::env(FRCHECK)] {
    set ::FRCHECK $::env(FRCHECK)
} else {
    set ::FRCHECK [ auto_execok FrCheck ]
}

#------------------------------------------------------------------------
# Proceedure to return the exit status of running a command
#------------------------------------------------------------------------
proc ProgramExitStatus { Command } {
    set Command "$Command > /dev/null 2>&1 ; echo $?"
    exec /bin/sh -c $Command
}

#------------------------------------------------------------------------
# Proceedure to format the jobid if it exists
#------------------------------------------------------------------------
proc GetJobIdLeader { Job } {
    if { [ catch {
	set jobid " [ eval uplevel set ${Job}(jobid) ]:"
    } err ] } {
	set jobid [ list ]
    }
    return $jobid
}

proc IsVerbose { } {
    if { [ info exists ::env(TEST_VERBOSE_MODE) ] } {
	if { $::env(TEST_VERBOSE_MODE) == "true" } {
	    return 1
	}
    }
    return 0
}

#------------------------------------------------------------------------
# Proceedure to set the LEADER variable using the script name
#------------------------------------------------------------------------

proc SetLeader {} {
    set ::LEADER [string range $::argv0 \
		      [ expr [ string length [ file dirname $::argv0] ] + 1 ] \
		      end ]
    set ::LEADER [ string toupper [ file rootname $::LEADER ] ]
}

#------------------------------------------------------------------------
# Proceedure to set the global variable JobId based on the job context
#------------------------------------------------------------------------

proc SetJobId {Job} {
    upvar $Job job
    if [ info exists job(jobid) ] {
	set ::JobId "$job(jobid): "
    } else {
	set ::JobId ""
    }
}

#------------------------------------------------------------------------
# Put a continuation line
#------------------------------------------------------------------------
proc puts_cont { Text } {
    if { [ IsVerbose ] } {
	puts "-- $Text"
    }
}
#------------------------------------------------------------------------
# Poceedure to print failed message to screen
#------------------------------------------------------------------------

proc puts_fail { Text } {
    puts_cont "FAIL: $Text"
}

#------------------------------------------------------------------------
# Poceedure to print failed message to screen
#------------------------------------------------------------------------

proc puts_info { Text } {
    puts_cont "INFO: $Text"
}

#------------------------------------------------------------------------
# Poceedure to print failed message to screen
#------------------------------------------------------------------------

proc puts_pass { Text } {
    puts_cont "PASS: $Text"
}
