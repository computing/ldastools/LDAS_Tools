#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

# pr2946.tcl Synopsis: 	When downsampling, LDAS createRDS jobs need to be enhanced to handle arbitary start/end times, 
# as long as sufficient data exist before and after these times.
#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2946::test {
    ;## included output for frame 
    set FRAME_OUTPUTDIR  /ldas_outgoing/frames/RDSVerify
    set FRAME_FILES($FRAME_OUTPUTDIR) [ list \
					    H-PR2946-793059328-1024.gwf \
					    H-PR2946-793060352-1024.gwf ]

	set cmdstem { createRDS
    -subject { pr2946 support arbitrary times for downsampling }
	-framequery {
	    { RDS_R_L3 H {} 793059328-793061375
		Chan(H1:LSC-DARM_ERR!4,H1:IFO-SV_STATE_VECTOR,H1:IFO-ACTIVITY_TYPE,H1:IFO-ACTIVITY_STATE,H1:IFO-ACTIVITY_INDEX) } }
	-usertype { PR2946 }
	-outputdir $FRAME_OUTPUTDIR 
	-usejobdirs { 0 }
	-compressiontype { gzip }
	-compressionlevel { 1 }
	-filechecksum { 1 }
	-frametimecheck { 1 }
	-framedatavalid { 0 }
	-framesperfile { 1 }
	-secperframe { 1024 }
	-allowshortframes { 1 }
	-generatechecksum { 1 }
	-fillmissingdatavalid { 0 }
    }
} ;## namespace ::QA::pr2946::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2946::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
#------------------------------------------------------------------------
# Needs rwxrwx*** on /ldas_outgoing/frames/RDSVerify for this to work on sunbuild
#------------------------------------------------------------------------

    proc testCleanup {} {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
        foreach outputdir [ array names ::${name}::FRAME_FILES ] {
	        foreach file [ set ::${name}::FRAME_FILES($outputdir) ] {
	            catch { file delete [ file join $outputdir $file ] }
            }
        }
        catch { file delete -force [ set ::${name}::MD5SUMDIR ] }
        catch { file delete -force [ set ::${name}::MD5SUMDIR2 ] }
    }  

#------------------------------------------------------------------------
# Verify that frames are in output
#------------------------------------------------------------------------

    proc ValidateFrameOutput { outputdir } {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
	    foreach file [ set ::${name}::FRAME_FILES($outputdir) ] {
    	    if	{ ! [ file exist $outputdir/$file ] } {
                return -code error "$outputdir/$file does not exist"
            }
        }
    }

    testCleanup

    #====================================================================
    # Execute a command 
    #====================================================================
    set cmd [ subst $cmdstem ]
    set ldascmd [ lindex $cmd 0 ]
    set expected "Your results:.+can be found at"
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    
    test regression:$thispr:$ldascmd:$subject:submit {} -body {
            if 	{ [catch {
                SubmitJob job $cmd
            } err] } {
                if  { [ info exist job(error) ] } {
                    set joberror $job(error)
                }
				LJdelete job
                return -code error $err
            }
            if  { [ info exist job(jobReply) ] } {
                upvar jobreply jobreply
                set jobreply $job(jobReply)
            } 
            LJdelete job
            return $jobreply
    } -match regexp -result $expected
        
    if  { [ info exist jobreply ]  } {
        test regression:$thispr:$ldascmd:$subject:validate {} -body {
        if  { [ string length $FRAME_OUTPUTDIR ] } {
            ValidateFrameOutput $FRAME_OUTPUTDIR
        }      
    } -result {}
    }
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2946::test
