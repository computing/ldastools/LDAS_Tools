#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2008::test {

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730524100

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 883
    set Finish [ expr $Start + $Dt - 1]

    set cmdstem { conditionData
    -subject { pr2008 - complex framequeries of merged RDS frames produce incomplete output }
	-framequery [ list $Query ]
	-algorithms {
	    output(_ch0, _, _, ch0, comment0);
	    output(_ch1, _, _, ch1, comment1);
	}
	-datacondtarget datacond
	-outputformat { ilwd ascii }
    }
    
    set query(case1) {
        { RDS_R_L1 H {} $Start-$Finish Adc(H2:LSC-AS_Q!resample!8!) }
		{ RDS_R_L1 L {} $Start-$Finish Adc(L1:LSC-AS_Q!resample!8!) }
	}
    
    set query(case2) {
        { RDS_R_L1 {} {} $Start-$Finish \
	        Adc(H2:LSC-AS_Q!resample!8!,L1:LSC-AS_Q!resample!8!) }
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2008
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2008::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    #========================================================================
    # Execute a command 
    #========================================================================
    foreach case [ list case1 case2 ] {
        if  { [ catch {
            set Query [ subst -nobackslashes [ set query($case) ] ]
            set cmd [ subst -nobackslashes $cmdstem ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            set expected "Your results.+results.ilwd"
            
            test regression:$thispr:$ldascmd:Start=$Start:DeltaT=$Dt:frame:$case:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputs outputs               
                    set outputs $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
        } err ] } {
            Puts 0 $err 
        }
    }
    flush [outputChannel]  
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2008::test
