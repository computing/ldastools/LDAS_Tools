#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr3170::test {
       
    set  FRAME_FILES [ list \
        H-RDS_R_L3_@system@_@jobid@-793059328-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793059392-128.gwf \
        H-RDS_R_L3_@system@_@jobid@-793059520-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793059584-128.gwf \
        H-RDS_R_L3_@system@_@jobid@-793059712-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793059776-256.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060032-256.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060288-128.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060416-192.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060608-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060672-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060736-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060800-64.gwf \
        H-RDS_R_L3_@system@_@jobid@-793060864-256.gwf \
        H-RDS_R_L3_@system@_@jobid@-793061120-256.gwf \
    ]
    
	set cmdstem { getFrameData
    -subject { $thispr frame should process all frame files}
    -returnprotocol http://results
	  -outputformat frame
	  -framequery {
	    { RDS_R_L3 H {} 793059328-793061375
		Adc(H1:LSC-DARM_ERR) } }

    }    

} ;## namespace ::QA::pr3170::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr3170 - ---no-metadata-check option
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr3170::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    #====================================================================
    # Execute a command 
    #====================================================================
    set cmd [ subst $cmdstem ]
    set ldascmd [ lindex $cmd 0 ]
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    
    test regression:$thispr:$ldascmd:$subject {} -body {
        if 	{ [catch {
            SubmitJob job $cmd
            regexp {[(]\s*(\S+)\s+running LDAS version\s+} $job(jobInfo) -> jobSystem
            regsub -all -- {-} $jobSystem {_} jobSystem
            if  { ! [ info exist job(jobid) ] } {
                return -code error "No jobid"
            }
            set jobid $job(jobid)
            if  { [ info exist $job(outputDir) ] } {
                regsub -all {http://(\d.)+} $outputdir {} outputdir
                regsub -all -- {-} $jobid {_} jobid
    #--------------------------------------------------------------------
    # Check for the existence of each frame file.
    #--------------------------------------------------------------------
                foreach file $FRAME_FILES {
	                regsub -all @system@ $file $jobSystem file
	                regsub -all @jobid@ $file $jobid file
    	            if  { ! [ file exist $outputdir/$file ] } {
	                    return -code error "Unable to find file: $outputdir/$file"
                    }
                }
            } else {
                error "No variable job(outputDir) for $jobid"
            }
            LJdelete job
        } err] } {
            if  { [ info exist job(error) ] } {                        
                set joberror $job(error)
            }
            LJdelete job
            return $joberror
        }
    } -result {}
    
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr3170::test
