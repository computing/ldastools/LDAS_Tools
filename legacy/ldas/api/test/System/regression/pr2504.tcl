#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
exec /usr/bin/env tclsh "$0" ${1+"$@"}

set ::auto_path "/ldas/lib . $auto_path"

if      { ! [ regexp "/ldas/bin" $::env(PATH) ] } {
        set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

package require LDASJob

source "$::env(SRCDIR)/regression_tools.tcl"

#========================================================================
# Setting of global variables
#========================================================================
SetLeader

#========================================================================
# The main test loop
#========================================================================
proc main { } {
    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------

    set retval 1

    #--------------------------------------------------------------------
    # Sanity checks
    #--------------------------------------------------------------------
    if { $::env(USER) != "ldas" } {
	puts_fail "$::LEADER: This test must be run as user ldas"
	return 0
    }
    

    #--------------------------------------------------------------------
    # Run through a series of tests
    #--------------------------------------------------------------------

    set retval [ expr [ Check G-RDS_R_L3-758224320-60.gwf.bad ] && $retval ]
    set retval [ expr [ Check G-RDS_R_L3-758353440-60.gwf.bad ] && $retval ]
    set retval [ expr [ Check G-RDS_R_L3-758197560-60.gwf.bad ] && $retval ]

    #--------------------------------------------------------------------
    # return the status
    #--------------------------------------------------------------------

    return $retval
}

#========================================================================
# Should not have to modify any variables below this point
#========================================================================

#------------------------------------------------------------------------
# Handles errors that occur in the background
#------------------------------------------------------------------------
proc bgerror { msg } {
     catch { set trace $::errorInfo }
     if { [ string length $trace ] } {
        set msg $trace
     }
     puts stderr "bgError: $msg"
}

proc Reader { Pipe } {
    if {[eof $Pipe]} {
	set ::CheckRetval 1
	return
    }
    set ::CheckVerify [split [read $Pipe] \n]
}

#------------------------------------------------------------------------
# Validation
#------------------------------------------------------------------------
proc Check { File } {
    set ::Task "$::env(FRVERIFY) --verbose $File:"
    set ::CheckRetval 1
    set file "/ldas_outgoing/test/frames/BadData/$File"
    #--------------------------------------------------------------------
    # Check using $::FRVERIFY
    #--------------------------------------------------------------------
    set after_id [after [expr 60 * 1000] {
	puts_fail "${::LEADER}: ${::Task} Timed out"
	set ::CheckRetval 0
    }]
    set ::CommandPipe [open "|$::env(FRVERIFY) --verbose $file" r]
    fconfigure $::CommandPipe -buffering line -blocking 0
    fileevent $::CommandPipe readable [list Reader $::CommandPipe]
    vwait ::CheckRetval
    after cancel $after_id
    catch {close $Pipe}
    #--------------------------------------------------------------------
    # :TODO: If successful, then verify that the correct error was generated
    #--------------------------------------------------------------------
    if { $::CheckRetval } {
    }
    #--------------------------------------------------------------------
    # Cleanup local copy of the file
    #--------------------------------------------------------------------
    file delete $File
    LJdelete job
    return $::CheckRetval
}

#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
exit [ expr ! [ main ] ]
