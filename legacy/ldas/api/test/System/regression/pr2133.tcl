#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
    . /ldas/libexec/setup_tclsh.sh
# \
    exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2133::test {

    set cmdstem { concatFrameData
	-subject { pr2133 - concatFrameData files fail FrCheck }
	-returnprotocol { http://results.gwf }
	-outputformat { frame }
	-framequery {
	    { R H {} ${Start}-${finish} Adc(H1:LSC-AS_Q,H2:LSC-AS_Q) }
	}
    }
    set deltaList { 16 32 1024 }
} ;## namespace ::QA::pr2406::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2133 - framequery for MDC frames
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2133::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    set Start 730524000
    
    foreach delta $deltaList {
        set finish [ expr $Start + $delta - 1 ]
        set cmd [ subst $cmdstem ]
        set ldascmd [ lindex $cmd 0 ]
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set expected "Your results.+gwf"
        test regression:$thispr:$ldascmd:start$Start:delta$delta:submit {} -body {
	    
            if  { [ catch {
                upvar Job Job
                SubmitJob Job $cmd
                upvar reply reply
                upvar outputs outputs
                set reply $Job(jobReply)
                set outputs $Job(outputs)
            } err] } {
                if  { [ info exist Job(error) ] } {                        
                    set joberror $Job(error)
                } else {
                    set joberror $err
                }
                LJdelete Job
                Puts 0 $joberror
                return -code error $joberror
            }
            LJdelete Job
            Puts 5 $reply
            return $reply
        } -match regexp -result $expected

        if { [ info exist reply ] } {
            test regression:$thispr:$ldascmd:Start$Start:delta$delta:validate {} -body {
		set frcheck 1
		foreach pat [configure -skip] {
		    if { [ string match $pat ":FrCheck:" ] } {
			set frcheck 0
			break
		    }
		}
		foreach remote_file $outputs {
		    regexp {^.*/([^/]*)$} $remote_file -> file
		    if { [ regexp {^/} $remote_file ] } {
			# Add http protocol so LJcopy will be able to
			#   retrieve the file
			set remote_file "http://${host}${remote_file}"
		    }
		    #----------------------------------------------------
		    # Make sure the file looks like a frame file
		    #----------------------------------------------------
		    if  { ! [ regexp {.*[.]gwf} $file match ] } {
			continue
		    }
		    LJcopy $remote_file $file
		    #----------------------------------------------------
		    # Check using $::FRVERIFY
		    #----------------------------------------------------
                    set errors ""
		    set exit_status [ ProgramExitStatus "$::FRVERIFY --no-metadata-check $file" ]
		    set lretval [ expr ! $exit_status ]
		    if { $exit_status } {
                        append errors "$::FRVERIFY failed on $file with exit status: $exit_status\n"
                    } else {
                        Puts 5 "$::FRVERIFY succeeded on $file"
                    }

		    #----------------------------------------------------
		    # Check using FrCheck
		    #----------------------------------------------------
		    if { $frcheck } {
			set exit_status [ ProgramExitStatus \
					      "$::FRCHECK -i $file" ]
			set lretval [ expr ! $exit_status ]
			if  { $exit_status } {
			    append errors "$::FRCHECK failed on $file with exit status: $exit_status\n"
			} else {
			    Puts 5 "$::FRCHECK succeeded on $file"
			}
		    }
                }
                set errors [ string trim $errors \n ]
                if  { [ string length $errors ] } {
                    return -code error $errors
                }
            } -result {}
	    #------------------------------------------------------------
	    # Cleanup local copy of the file
	    #------------------------------------------------------------
	    file delete $file
	}
    }
    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr2133::test
