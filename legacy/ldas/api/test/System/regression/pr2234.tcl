#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2234::test {

    set filename \
	/ldas_outgoing/test/frames/BadData/T-R_bad_header-600000000-1.gwf

    set  cmdstem { conditionData
	-subject { pr2234 - PSD non-frame file exceptions }
	-framequery { {} {} {$filename} {} Adc(H1:LSC-AS_Q) }
	-aliases {gw = _ch0;}
	-algorithms {
	    x = slice(gw, 0, 16384, 1);
	    output(x, _, result.ilwd, x1, time series 1);
	    p = psd(x, 8192);
	    output(p, _, result.ilwd, p1, psd 1);
	    
	    x = slice(gw, 16384, 16384, 1);
	    output(x, _, result.ilwd, x2, time series 2);
	    p = psd(x, 16384);
	    output(p, _, result.ilwd, p2, psd 2);
	}
	-datacondtarget frame
	-setsingledc 1
	-returnprotocol { http://result.ilwd }
	-outputformat { ilwd ascii }
    }
} ;## namespace ::QA::pr2234::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2234 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr2234::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    set cmd [ subst $cmdstem ]
    set ldascmd [ lindex $cmd 0 ]
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    set subject [ string trim $subject ]
    Puts 5 "cmd $cmd"
    ;## expect an exception from frame
    set expected "Don't have read access for file"
    test regression:$thispr:$ldascmd:$subject:submit {} -body {
        
        if  { [ catch {
            upvar Job Job
            SubmitJob Job $cmd
            set reply $Job(jobReply)
        } err] } {
            if  { [ info exist Job(error) ] } {                        
                set joberror $Job(error)
            } else {
                set joberror $err
            }
            LJdelete Job
            Puts 5 $joberror
            return -code error $joberror
        }
        LJdelete Job
        Puts 5 $reply
        return $reply
    } -match regexp -result $expected
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2234::test
