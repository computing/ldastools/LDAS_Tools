#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2122::test {
    set ValidateMode 1
} ;## namespace ::QA::pr2122::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--validateMode} {string} \
    {dataPipeline or datacond} \
    { if    { [ string equal datacond $::QA::Options::Value ] } { \
            set ::QA::pr2122::test::ValidateMode 2 \
      } else { \
            set ::QA::pr2122::test::ValidateMode 1 \
      } }

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2122::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob

    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------
        
    set retval 1
    set Start 730522210
    set TestCasesStop 4
    
    #========================================================================
    # Should not have to modify any variables below this point
    #========================================================================
    #------------------------------------------------------------------------
    # First part of command to seed the data values
    #------------------------------------------------------------------------
    set LEADER "PR2122"
    set DatacondOutputMode 2 ;# Intended for debugging mode

    set PrimaryValue { output(rgw,_,_,:primary,resampled gw timeseries - primary); }
    set ThirdValue { output(rgw,frame,frame,rgw,resampled gw timeseries - third); }
    set FourthValue { output(rgw,ilwd_ascii,rgw.ilwd,rgw,resampled gw timeseries - fourth); }
    
    ;## missing \ in H2:\LSC will result in datacond exception of no such symbol
    ;## and never makes it to mpi
  
    set basiccmd { dataPipeline
           -subject {PR2122 INSPIRAL DEMO}
	   $mode_options
	   -framequery { R H {} {${Start}-${End}} Adc(H2:LSC-AS_Q) }
           -aliases { gw = H2\\:LSC-AS_Q::AdcData; }
           -algorithms {
	       rgw = resample(gw, 1, 8);
	       p = psd( rgw, 262144 );
	       $primary_top
	       output(p,_,_,spectrum:psd,spectrum data);
	       $third
	       $fourth
	       $primary_bottom } }
           
    proc Validate { Start Case Mode } {

        set name [ namespace current ]
    	namespace import [ namespace parent $name ]::*
        
        set DeltaT 512
        set End [ expr $Start + $DeltaT - 1 ]
        set leader "[ set ::${name}::LEADER ]: Case $Case"
        set retval 1
   
        set primary_top ""
        set primary_bottom ""
        set third ""
        set fourth ""
        set PrimaryValue [ set ::${name}::PrimaryValue ]
        set ThirdValue [ set ::${name}::ThirdValue ]
        set FourthValue [ set ::${name}::FourthValue ]
        
        switch $Case {
            0   {   set primary_top $PrimaryValue
	                set third $ThirdValue
	                set fourth $FourthValue
                }
            1   {   set primary_top $PrimaryValue
	                set fourth $FourthValue
                }
            2   {   set primary_top $PrimaryValue
	                set third $ThirdValue
                }
            3   {   set primary_bottom $PrimaryValue
	                set third $ThirdValue
	                set fourth $FourthValue
                }
            default {   return -code error "$leader: Unsupported case $Case" }
        }
        if  { $Mode == [ set ::${name}::DatacondOutputMode ] } {
	        set mode_options {
	            -dynlib {}
	            -datacondtarget datacond
	            -outputformat { ilwd ascii }
	        }
            set modetext datacond
        } elseif { $Mode == [ set ::${name}::ValidateMode ] } {
	        set mode_options {
	            -dynlib libldasinspiral.so
	            -filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
	            -multidimdatatarget ligolw
	            -database           ldas_tst
	            -np                 3
	            -responsefiles { file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass }
	        }
            set modetext wrapper
        } else {
	        return -code error  "${leader}: FAILED: Unsupported mode: $Mode"
        }
        
        set cmd [ subst [ set ::${name}::basiccmd ] ]
        test regression:pr2122:dataPipeline:$Start:Case$Case:$modetext {} -body {
            if 	{ [catch {
                SubmitJob job $cmd
            } err] } {
                if  { [ info exist job(error) ] } {
                    set joberror $job(error)
                }
				LJdelete job
	    		return -code error $err
            }
            if  { [ info exist job(jobreply) ] } {
                set reply $job(jobreply)
            } else {
                set reply none
            }
            set jobid $job(jobid)
            Puts 2 "$jobid got $reply"
            LJdelete job
            list
        } -result {}
    }

    # end Validate
    
    #--------------------------------------------------------------------
    # Run through a series of tests
    #--------------------------------------------------------------------
    for { set x 0 } { $x < $TestCasesStop } { incr x 1 } {
    	Validate $Start $x $ValidateMode 
    }
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2122::test

namespace delete ::QA::pr2122::test
