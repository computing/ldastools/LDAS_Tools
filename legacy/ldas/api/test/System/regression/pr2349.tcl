#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2349::test {

    set cmdstem1 { cacheGetFilenames 
    -subject { pr2349 -	framequery diffs for MDC frames ifos HL workaround }
	-ifos HL
	-times {${start}-${end}}
	-types {RDS_MERGED_L1}
	-channels {L1:LSC-AS_Q}
	-allowgaps { 0 }
    }
    
    set cmdstem2 { cacheGetFilenames
    -subject { pr2349 -	framequery diffs for MDC frames empty ifos standard }
	-ifos {}
	-times {${start}-${end}}
	-types {RDS_MERGED_L1}
	-channels {L1:LSC-AS_Q}
	-allowgaps { 0 }
    }

} ;## namespace ::QA::pr2406::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2349 - framequery for MDC frames
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2349::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    set retval 1
    set start 730524048
    set dt 64
    set end [expr $start + $dt - 1]
    set pat "/HL-RDS_MERGED_L1-"
    set file_count [ expr ( $dt / 16 ) ];
    #--------------------------------------------------------------------
    # Establish a timeout for the job
    #--------------------------------------------------------------------
    ;## no need to time out as this is done by manager and will not work
    ;## for globus
    set expected "$pat.+gwf"
    
    foreach case [ list cmdstem1 cmdstem2 ] {
        set cmd [ subst [ set $case ] ]
        set ldascmd [ lindex $cmd 0 ]
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set tag [ lindex $subject end ]
        test regression:$thispr:$ldascmd:$tag:submit {} -body {
        
        if  { [ catch {
            upvar Job Job
            SubmitJob Job $cmd
            set reply $Job(jobReply)
            Puts 5 $reply
        } err] } {
            if  { [ info exist Job(error) ] } {                        
                set joberror $Job(error)
            } else {
                set joberror $err
            }
            LJdelete Job
            return -code error $joberror
        }
        LJdelete Job
        return $reply
    } -match regexp -result $expected
  
    if  { [ info exist reply ] } {
        test regression:$thispr:$ldascmd:$tag:validate {} -body {
	        set afc [ regsub -all -- $pat $reply {} - ];
	        if  { $afc == $file_count } {
	            Puts 5 "cacheGetFilename: $tag: File count is correct"
            } else {
                return -code error "cacheGetFilename: $tag: File count is wrong: $afc instead of $file_count"
            }
        } -result {}
    }
    
    }   
   ;##==================================================================
   ;## Testing is complete
   ;##==================================================================
    flush [outputChannel]
    cleanupTests
} ;## namespace ::QA::pr2127::test
