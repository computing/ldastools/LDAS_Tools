#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2823::test {
       
    set Start 751800032
    set Dt 32
    set Finish [ expr $Start + $Dt - 1]
    set Type pr2823_ConcatFrame_S3_1
    set Channels [list H1:\LSC-AS_I]
    set UserTag RDS_pr2823_1
    set metadatacheck_opts [ list -1 0 1 ]
    set frametimecheck_opts [ list -1 0 1 ]
    
    set cmdstem { createRDS
    -subject { $thispr - reconcile FrProcData and FrVect metadata }
    -times { ${Start}-${Finish} }
    -type { $Type }
    -channels $Channels
    }
} ;## namespace ::QA::pr2823::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2823::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

;## frame is /scratch/test/frames/PRFrames/H-PR2823_ConcatFrame_S3_1-751800032-32.gwf

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------

    set Start 751800032
    set Dt 32
    set Finish [ expr $Start + $Dt - 1]
    set Type PR2823_ConcatFrame_S3_1
    set Channels [list H1\:LSC-AS_I]
    set cmd [ subst $cmdstem ]
    set ldascmd [ lindex $cmd 0 ]
     
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    set subject [ string trim $subject ]
    
    test regression:$thispr:$ldascmd:$subject {} -body {
        if  { [ catch {
            SubmitJob job $cmd
            if  { [ info exist job(outputs) ] } {
    #====================================================================
    # Read the output and make sur that it looks correct
    #--------------------------------------------------------------------
            regexp {^([^:]*):(.*)$} $job(manager) -> host port
            
            set error ""
            foreach frame $job(outputs) {
	            regexp {^.*/([^/]*)$} $frame -> base_name
	            if  { [ regexp {^/} $frame ] } {
	            # Add http protocol so LJcopy will be able to
	            #   retrieve the file
	                set frame "http://${host}${frame}"
	            }
	            LJcopy $frame $base_name
	            set output [ exec $::FRDUMP -S $base_name ]
	            set object ""
	            foreach line [ split $output "\n" ] {
	                regexp {tRange:\s+(\d+)$} $line -> tRange
	                if { [info exists tRange] } {
		                if  { $tRange != $Dt } {
		                    append error "tRange =?= dt ( $tRange =?= $Dt)\n"
		                } else {
		                    Puts 5 "tRange =?= dt ( $tRange =?= $Dt) "
		                }
		                unset tRange
	                }
	            }
            }
            if  [ string length $error ] } {
                return -code error [ string trim $error \n ]
            }
            LJdelete job
        } err ] } {
            set joberror $err
            if  { [ info exist job(error) ] } {
                set joberror $job(error)
            }
		    LJdelete job
            return -code error $joberror
        }
    } -result {}
    
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2823::test
