#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
    . /ldas/libexec/setup_tclsh.sh
# \
    exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1146::test {

    set deltalist [ list 16 32 600 ]
    
    #--------------------------------------------------------------------
    # Run through a series of tests
    #--------------------------------------------------------------------

    set start 714096000 ;# Start time of frame
    
    set cmdstem { concatFrameData
        -subject { pr1146 - broken for multi-frame files }
	-returnprotocol { http://results.gwf }
	-outputformat { frame }
	-framequery {
	    { R G {} ${Start}-${finish} Adc(G1:DER_H_HP-EP) }
	}
    }
    
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1146
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1146::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    proc Check { Start DeltaT } {
	
        if  { [ catch {
            set name [ namespace current ]
            set finish [ expr $Start + $DeltaT - 1 ]
	    #====================================================================
	    # Execute a command 
	    #====================================================================
	    
            set cmd [ subst -nobackslashes [ set ::${name}::cmdstem ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            set expected "Your results:.+$DeltaT.+gwf"
            
            test regression:[ set ::${name}::thispr ]:$ldascmd:start=$Start:Delta=$DeltaT:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd        
                    upvar url url
                    set url $Job(outputs)
                    set reply $Job(jobReply)
                    #Puts 5 $reply
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
            
            if  { [ info exist url ] } {
                test regression:[ set ::${name}::thispr ]:$ldascmd:start=$Start:Delta=$DeltaT:validate {} -body {
		    set frcheck 1
		    foreach pat [configure -skip] {
			if { [ string match $pat ":FrCheck:" ] } {
			    set frcheck 0
			    break
			}
		    }
		    foreach remote_file $url {
			regexp {^.*/([^/]*)$} $remote_file -> file
			if  { [ regexp {^/} $remote_file ] } {
			    # Add http protocol so LJcopy will be able to
			    #   retrieve the file
			    set remote_file "http://${host}${remote_file}"
			}
			#------------------------------------------------
			# Make sure the file looks like a frame file
			#------------------------------------------------
			if  { ! [ regexp {.*[.]gwf} $file match ] } {
			    continue
			}
			LJcopy $remote_file $file
			#------------------------------------------------
			# Check using $::FRVERIFY
			#------------------------------------------------
			set exit_status [ ProgramExitStatus \
					      "$::FRVERIFY --no-metadata-check $file" ]
                        if  { [ expr $exit_status ] } {
			    append errors  "Failed in $::FRVERIFY: $file with exit status $exit_status\n"
                        }
			#------------------------------------------------
			# Check using FrCheck
			#------------------------------------------------
			if { $frcheck } {
			    set exit_status [ ProgramExitStatus \
						  "$::FRCHECK -i $file" ]
			    if  { [ expr $exit_status ] } {
				append errors "Failed in $::FRCHECK: $file with exit status $exit_status\n"
			    }
			}
			#------------------------------------------------
			# Cleanup local copy of the file
			#------------------------------------------------
			file delete $file
                    }
		} -result {}
            }
        } err ] } {
            return -code error $err
        }
    }
    
    #********************************************************************
    # Test basic conditions
    #********************************************************************
    set startlist [ list $start [ expr $start + 60 - 16] [ expr $start + 60 - 10 ] ]
    
    foreach thisstart $startlist {
        foreach delta $deltalist {
            if  { [ catch {
                Check $thisstart $delta
            } err ] } {
                Puts 0 "Error: $thisstart $delta $err"
            }
        }
    }
    
    #========================================================================
    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1146::test
