#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1532::test {

    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------

    set retval 1
    set FrameStart 729363600
	   
    set cmdstem1 { concatFrameData
        -subject { pr1532 frame ilwd metadata should indicate frame spec version }
	    -framequery {
	        { SenseMonitor_V02_H1_M H {} ${FrameStart}-${FrameFinish}
		    adc(H1:CAL-OLOOP_FAC.mean) }
	    }
	    -returnprotocol result.ilwd
	    -outputformat { ilwd ascii }
    }
	set cmdstem2 { getFrameData
        -subject { pr1532 frame ilwd metadata should indicate frame spec version }
	    -framequery {
	        { SenseMonitor_V02_H1_M H {} ${FrameStart}-${FrameFinish}
		    adc(H1:CAL-OLOOP_FAC.mean) }
	    }
	    -returnprotocol result.ilwd
	    -outputformat { ilwd ascii }
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1532
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1532::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
        
    proc Validate { URL Name Desc FrameStart DeltaT } {
    
        if  { [ catch {
            set name [ namespace current ]
            set data [ LJread $URL ]
            
            set thispr [ set ::${name}::thispr ]
           
    #--------------------------------------------------------------------
    # Examine the time offsets
    #--------------------------------------------------------------------
            test regression:[ set ::${name}::thispr ]:$Desc:start=$FrameStart:Delta=$DeltaT:validate {} -body {
                set errors ""
                set seen 0
                foreach line [ split $data "\n" ] {
	                if  { [ regexp "name='${Name}'" $line ->] } {
	                    if  { ! [ regexp {metadata='.*FrameCPPVersion=[^:]+.*'} $line -> ] } {
		                    append errors "$Desc: FrameCPPVersion not found in metadata\n"
                        }
	                    if  { ! [ regexp {metadata='.*DataFormatVersion=[^:]+.*'} $line -> ] } {
		                    append errors "$Desc: DataFormatVersion not found in metadata\n"
                        }
	                    if { ! [ regexp {metadata='.*FrameLibraryMinorVersion=[^:]+.*'} $line -> ] } {
		                    append errors "$Desc: FrameLibraryMinorVersion not found in metadata\n"
                        }
                        set seen 1
                    }
                }
                if  { ! $seen } {
	                append errors "$Desc: Unable to find element named '${Name}'"
                }
                if  { [ string length $errors ] } {
                    return -code error $errors
                }
            } -result {}
	    } err ] } {
            return -code error $err
        }
    }


    proc Check { FrameStart Desc cmdstem expected  } {
    
        if  { [ catch {
            set name [ namespace current ]
            set DeltaT 180
            set FrameFinish [ expr $FrameStart + $DeltaT - 1 ]

    #====================================================================
    # Execute a command 
    #====================================================================
    
            set cmd [ subst -nobackslashes [ set ::${name}::${cmdstem} ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
                   
            test regression:[ set ::${name}::thispr ]:$ldascmd:start=$FrameStart:Delta=$DeltaT:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd        
                    upvar url url
                    set url $Job(outputs)
                    set reply $Job(jobReply)
                    #Puts 5 $reply
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    upvar nodata nodata
                    set nodata 1
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
       
            if  { [ info exist url ] } {
                Validate $url $Desc $ldascmd $FrameStart $DeltaT 
            }
        } err ] } {
            return -code error $err
        }
    }
    
    if  { [ catch {
        Check $FrameStart frame_group cmdstem1 "Your results:.+result.ilwd"
        Check $FrameStart SenseMonitor_H1::Frame cmdstem2 "Your results:.+H-SenseMonitor_V02_H1_M.+.ilwd"
    } err ] } {
        Puts 0 $err
    }
    

    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1532::test
