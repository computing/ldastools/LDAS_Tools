#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2077::test {

#========================================================================
# This tests for pr2077. The channel which is choosen needs to be 
#========================================================================
#========================================================================
# Establish ranges - These values should be changed as needed at the
#  test data evolves.
#========================================================================

    set FrameStart 729363600

#------------------------------------------------------------------------
# These values should not be altered
#------------------------------------------------------------------------

    set Offset 1
    set calcTimeOffset [ expr 60 - $Offset ]
    set Start [ expr $FrameStart + $Offset ]
    set DeltaT 180
    set Finish [ expr $Start + $DeltaT - 1 ]

    set fp_rx {([-+]?([0-9]*\.)?[0-9]+([eE][-+]?[0-9]+)?)}
    
    set cmdstem { concatFrameData
    -subject { pr2077 "coarsely" sampled channels give wrong start-time }
    -framequery {
	{ SenseMonitor_V02_H1_M H {} ${Start}-${Finish}
	    adc(H1:CAL-OLOOP_FAC.mean) }
    }
    -returnprotocol result.ilwd
    -outputformat { ilwd ascii }
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2077 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2077::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    set expected ".+"
    
    if  { [ catch {
        set cmd [ subst $cmdstem ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set expected "Your results.+result.ilwd"
        test regression:$thispr:$ldascmd:Start=$Start:submit {} -body {
            if  { [ catch {
                upvar Job Job
                SubmitJob Job $cmd
                upvar outputs outputs               
                set outputs $Job(outputs)
                set reply $Job(jobReply)
            } err] } {
                if  { [ info exist Job(error) ] } {                        
                    set joberror $Job(error)
                } else {
                    set joberror $err
                }
                LJdelete Job
                return -code error $joberror
            }
            LJdelete Job
            return $reply
        } -match regexp -result $expected
    } err ] } {
        Puts 0 "Error $err"
    }
    
    if  { [ info exist outputs ] } {
        
#========================================================================
# Examine the ilwd that was generated
#========================================================================

        set url $outputs
        set data [LJread $url]

#------------------------------------------------------------------------
# Examine the time offsets
#------------------------------------------------------------------------
        test regression:$thispr:$ldascmd:Start=$Start:validate {} -body {
            regexp "timeOffset\[^>\]*\[>\]$fp_rx" $data match timeOffset
            if  { [ expr $timeOffset - $calcTimeOffset ] != 0 } {
                return -code error "Wrong timeOffset: $timeOffset instead of $calcTimeOffset"
            } else {
                Puts 5 "timeOffset $timeOffset, calcTimeOffset $calcTimeOffset diff=[ expr $timeOffset - $calcTimeOffset ]"
            }
        } -result {}
    }
    
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2077::test
