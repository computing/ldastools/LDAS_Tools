#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

lappend ::auto_path /ldas/lib

;## these ldas modules are needed to give value to the metalist and channelspecs
package require genericAPI
package require frameAPI

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1705::test {

    set framedir [file join "/" scratch test frames]
    set output pr1705.out
    set EXPECTED_OUTPUT /ldas_usr/ldas/test/misc/pr1705.expected
    
    set cmdstem {	getChannels
    -subject { pr1705 - list proc channels when querying RDS data }
	-returnprotocol http://results.txt
	-filename $Filename
	-channeltypes $channel
	-metadata $meta
    }
    
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1705
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1705::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    proc parse { data } {
        set channels(names) [ list ]
        foreach item $data {
	        set channel [ lindex $item 0 ]
	        lappend channels(names) $channel
	        set type    [ lindex $item 1 ]
	        set channels($channel,ctype) $type
	        foreach attrib [ lrange $item 2 end ] {
                foreach [ list name value ] \
		            [ split $attrib = ] { break }
                    set channels($channel,$name) $value
	        }
        }
        return [ array get channels ]
    }

    proc Check { Filename meta channel { expected "Your results.+channels|No channels found" } } {

        set name [ namespace current ]
       
        if  { [ catch {
            set cmd [ subst -nobackslashes [ set ::${name}::cmdstem ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            
            if  { [ string equal INVALID_META $meta ] } {
                set expected "unknown metadata type.+INVALID_META"
            } elseif	{ [ string equal INVALID $channel ] } {
                set expected "unknown channel type.+INVALID"
            } 
            
            test regression:[ set ::${name}::thispr ]:$ldascmd:Filename=$Filename:meta=$meta:channel=$channel:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd        
                    upvar url url
                    set url $Job(outputs)
                    if  { ![ string length $url ] } {
                        upvar nodata nodata
                        set nodata 1
                    }
                    set reply $Job(jobReply)
                    #Puts 5 $reply
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    upvar nodata nodata
                    set nodata 1
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
 
            if  { ![ info exist nodata ] && [ info exist url ] } {
	#----------------------------------------------------------------
	# Get the channel list file and parse it
	# only if -metadata is not 0
	#----------------------------------------------------------------
        
                test regression[ set ::${name}::thispr ]:$ldascmd:Filename=$Filename:meta=$meta:channel=$channel:validate {} -body {
                    set errors ""
    	            if	{ [ string length $url ] && ! [ string equal 0 $meta ] }  {      
	                    set data [LJread $url]
	                    array set channels [ parse $data ]
	                    #Puts 5 "$Filename meta $meta channelspec $channel\nchannels [ array get channels ]"
                        Puts 1 "$Filename meta $meta channelspec $channel found [ array size channels ] channels"
	                    foreach chan $channels(names) {
		                    if  { [ expr ! [ regexp {^(ADC|PROC|SIM|SER)$} \
				                $channels($chan,ctype) match ] ] } {
                                append errors "Unknown data type: $channels($chan,type)"
                            } 
                        }
                        if  { [ string length $errors ] } {
                            return -code error $errors
                        }
                    }
                }   
		    }
	    } err ] } {
            return -code error $err
	    }	 
    }
    
    #--------------------------------------------------------------------
    # Actual tests to run
    #--------------------------------------------------------------------

    set filelist \
	[list \
	     [file join $framedir S2 MERGED HL-RDS_MERGED_L1-730524992-16.gwf] \
	     [file join $framedir DMTGen Z-DMTGen-730000000-16.gwf] ]
    
    set metalist [ list 0 1 2  ADC PROC SIM SER \
		       ALL EXTENDED_ADC EXTENDED_PROC EXTENDED_SIM \
		       EXTENDED_SER EXTENDED_ALL INVALID_META ]
    
    set channelspecList [ list ADC PROC SER SIM INVALID ]
    
    foreach meta $metalist {
    	foreach channel $channelspecList {
	        foreach file $filelist {
                if  { [ catch {
                    Check $file $meta $channel
                } err ] } {
                    Puts 0 $err
                }
            }
        }
    }
       
    #--------------------------------------------------------------------
    # Finished testing
    #--------------------------------------------------------------------
    catch { close $fout }
    
    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1705::test
