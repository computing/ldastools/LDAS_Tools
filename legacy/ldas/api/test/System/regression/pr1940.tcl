#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1940::test {

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730000000

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 16
    set Finish [ expr $Start + $Dt - 1]
    
    set cmdstem { conditionData
        -subject { pr1940 - read file without read permission }
        -framequery {
	        { No_Read_Permissions H {} $Start-$Finish Adc(H1:LSC-AS_Q) }
        }
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1940
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr1940::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    #========================================================================
    # Execute a command 
    #========================================================================
    if  { [ catch {
        set cmd [ subst -nobackslashes $cmdstem ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            set expected "Don't have read access for file"
            
            test regression:$thispr:$ldascmd:Start=$Start:DeltaT=$Dt:frame:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
    } err ] } {
        Puts 0 $err 
    }
    flush [outputChannel]  
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr1940::test
