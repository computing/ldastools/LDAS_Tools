#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2848::test {

    set Start 751800032
    set Dt 32
    set Finish [ expr $Start + $Dt - 1]
    set Type PR2848_ConcatFrame_S3_1
    set Channels [list H1\:LSC-AS_I]
    
	set cmd { createRDS
        -subject { $thispr - frame core dumps on frame with only FrProc }
        -times { ${Start}-${Finish} }
        -type { $Type }
        -channels $Channels
    }
    
} ;## namespace ::QA::pr2848::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2848 - frame core dumps on frame with only FrProc data
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2848::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    #====================================================================
    # Execute a command 
    #====================================================================

    set expected "Your results:.+can be found at"
    set cmd [ subst [ set $cmd ] ]
    set ldascmd [ lindex $cmd 0 ]

    test regression:$thispr:$ldascmd:$subject {} -body {
        if 	{ [catch {
            SubmitJob job $cmd                    
        } err] } {
            if  { [ info exist job(error) ] } {                        
                set joberror $job(error)
            }
            LJdelete job
            return $joberror
        }
    } -match regexp -result $expected
    
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2848::test
