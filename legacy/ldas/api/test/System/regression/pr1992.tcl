#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1992::test {
    set retval 1
    set Start 730522210
    
    set cmdstem { dataPipeline
	-subject { 
        pr1992 - datacond loops on truncated ilwd 
    }
	-dynlib           libldasinspiral.so
	-filterparams    (262144,7,131072,200.0,65536,69.0,8,0.9,1,(49.0,0.0),(2000.0,0.0),0,13,0,2)

	-datacondtarget  wrapper
	-metadatatarget  datacond
	-multidimdatatarget  ligolw
	-database        ldas_tst
	-realtimeratio   0.5
	-state {}
	-np 4
	
	-framequery {
	    R H {} {${Start}-${Finish}} Adc(H1:LSC-AS_Q!resample!4!)
	}
	-responsefiles {
	    file:/scratch/test/no_mount_frames/BadData/E7-H1-AS_Q-2048-131073.ilwd,pass
	    file:/ldas_outgoing/jobs/responsefiles/bank-1.0_6.0_0.97_TaylorT1_E7-H1.ilwd,pass
	}
	-aliases {
	    chan = H1\:LSC-AS_Q::ProcData;
	}
	-algorithms {
	    p = psd( chan, 262144 );
	    output(chan,_,_,:primary,resampled gw timeseries);
	    output(p,_,_,spectrum:psd,spectrum data);
	}
    }
}
   
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1992
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr1992::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    if  { [ catch {
        set delta_t 256
        set Finish [ expr $Start + $delta_t - 1 ]
    #--------------------------------------------------------------------
    # Establish a timeout for the job
    #--------------------------------------------------------------------
    #========================================================================
    # Execute a command 
    #========================================================================
    
        set cmd [ subst -nobackslashes $cmdstem ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set expected {_[[:xdigit:]]+_p_\S+:\s+end_of_file}
            
        test regression:$thispr:$ldascmd:Start=$Start:DeltaT=$delta_t:frame:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
        } -match regexp -result $expected
    } err ] } {
        Puts 0 $err 
    }
    flush [outputChannel]  
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr1992::test
