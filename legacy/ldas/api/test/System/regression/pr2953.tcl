#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::PR2953::test {
    set ValidateMode 1
} ;## namespace ::QA::PR2953::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--validateMode} {string} \
    {dataPipeline or datacond} \
    { if    { [ string equal datacond $::QA::Options::Value ] } { \
            set ::QA::PR2953::test::ValidateMode 2 \
      } else { \
            set ::QA::PR2953::test::ValidateMode 1 \
      } }

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::PR2953::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob

#========================================================================
# Establish ranges - These values should be changed as needed at the
#  test data evolves.
#========================================================================

    ;## included output for frame 
    set FRAME_OUTPUTDIR  /ldas_outgoing/frames/RDSVerify
    set FRAME_FILES($FRAME_OUTPUTDIR) [ list H-RDS_R_L1-751791568-16.gwf H-RDS_R_L1-751791584-16.gwf \
    H-RDS_R_L1-751791600-16.gwf H-RDS_R_L1-751791616-16.gwf ]
    set MD5SUMDIR /ldas_outgoing/md5/RDSVerify

    ;## test for PR 3046 - reduceRawFrames to support md5sumregexp
    set FRAME_OUTPUTDIR2  /ldas_outgoing/frames/H-RDSS4DEVTEST0720_R_L3-7960/frames
    set FRAME_FILES($FRAME_OUTPUTDIR2) [ list H-RDSS4DEVTEST0720_R_L3-796068608-256.gwf \
    H-RDSS4DEVTEST0720_R_L3-796068864-256.gwf  ]
    set MD5SUMDIR2 /ldas_outgoing/meta/frames/

    set startgps 751791568
    set endgps 751791631
    
	set cmdcase1 { createRDS
   	-times {751791568-751791631}
    -subject {md5sum output in output dir}
    -type {R}
    -usertype {}
    -outputdir $FRAME_OUTPUTDIR 
    -usejobdirs {0}
    -compressiontype {raw}
    -compressionlevel {6}
    -filechecksum {0}
    -frametimecheck {1}
    -framedatavalid {1}
    -channels {H2:LSC-AS_Q H2:LSC-AS_I!2 H2:LSC-REFL_Q!4 H2:LSC-REFL_I!2 H2:LSC-POB_Q!2}
    -md5sumregexp {s,/frames/,/md5/,g}

    }
    
    set cmdcase2 { createRDS
   	-times {751791568-751791631}
    -subject {md5sum output in frame output dir}
    -type {R}
    -usertype {}
    -outputdir {/ldas_outgoing/frames/RDSVerify}
    -usejobdirs {0}
    -compressiontype {raw}
    -compressionlevel {6}
    -filechecksum {0}
    -frametimecheck {1}
    -framedatavalid {1}
    -channels {H2:LSC-AS_Q H2:LSC-AS_I!2 H2:LSC-REFL_Q!4 H2:LSC-REFL_I!2 H2:LSC-POB_Q!2}
    }
    
    set cmdcase3 { createRDS
   	-times {751791568-751791631}
    -subject {md5sum output in output dir not under /ldas_outgoing}
    -type {R}
    -usertype {}
    -outputdir {/ldas_outgoing/frames/RDSVerify}
    -usejobdirs {0}
    -compressiontype {raw}
    -compressionlevel {6}
    -filechecksum {0}
    -frametimecheck {1}
    -framedatavalid {1}
    -channels {H2:LSC-AS_Q H2:LSC-AS_I!2 H2:LSC-REFL_Q!4 H2:LSC-REFL_I!2 H2:LSC-POB_Q!2}
	-md5sumregexp {s,FRAMES/RDSVerify,md5,i}
    }
    
    set cmdcase4 { createRDS
   	-times {751791568-751791631}
    -subject {user not in ::USERS_WHO_CAN_CREATE_RDS_DIRS}
    -type {R}
    -usertype {}
    -outputdir $FRAME_OUTPUTDIR 
    -usejobdirs {0}
    -compressiontype {raw}
    -compressionlevel {6}
    -filechecksum {0}
    -frametimecheck {1}
    -framedatavalid {1}
    -channels {H2:LSC-AS_Q H2:LSC-AS_I!2 H2:LSC-REFL_Q!4 H2:LSC-REFL_I!2 H2:LSC-POB_Q!2}
    -md5sumregexp {s,/frames/,/md5/,g}
    }
    
    set cmdcase5 { createRDS
   	-times {751791568-751791631}
    -subject {nonprintable chars in replacement string}
    -type {R}
    -usertype {}
    -outputdir $FRAME_OUTPUTDIR 
    -usejobdirs {0}
    -compressiontype {raw}
    -compressionlevel {6}
    -filechecksum {0}
    -frametimecheck {1}
    -framedatavalid {1}
    -channels {H2:LSC-AS_Q H2:LSC-AS_I!2 H2:LSC-REFL_Q!4 H2:LSC-REFL_I!2 H2:LSC-POB_Q!2}
    -md5sumregexp "s,/frames/,$nonprint,g"
    }

    set cmdcase6 { createRDS
   	-times {751791568-751791631}
    -subject {invalid md5sum specification}
    -type {R}
    -usertype {}
    -outputdir $FRAME_OUTPUTDIR 
    -usejobdirs {0}
    -compressiontype {raw}
    -compressionlevel {6}
    -filechecksum {0}
    -frametimecheck {1}
    -framedatavalid {1}
    -channels {H2:LSC-AS_Q H2:LSC-AS_I!2 H2:LSC-REFL_Q!4 H2:LSC-REFL_I!2 H2:LSC-POB_Q!2}
    -md5sumregexp "s,/frames/,,g"
    }
    
    set cmdcase7 { createRDS
	-times 796068608-796069119
    -subject {specify md5sum substitutions}
    -type { R } 
    -usertype { RDSS4DEVTEST0720_R_L3 } 
	-outputdir $FRAME_OUTPUTDIR2
	-usejobdirs { 0 } 
    -channels { H2:LSC-AS_Q H2:LSC-DARM_ERR
	H2:IFO-SV_STATE_VECTOR H2:IFO-ACTIVITY_TYPE H2:IFO-ACTIVITY_STATE 
	H2:IFO-ACTIVITY_INDEX H1:LSC-AS_Q H1:LSC-DARM_ERR H1:IFO-SV_STATE_VECTOR
	H1:IFO-ACTIVITY_TYPE H1:IFO-ACTIVITY_STATE H1:IFO-ACTIVITY_INDEX }
	-compressiontype { gzip } 
    -compressionlevel { 1 } 
    -filechecksum { 1 }
	-frametimecheck { 1 } 
    -framedatavalid { 0 } 
    -framesperfile { 1 }
	-secperframe { 256 } 
    -allowshortframes { 1 } 
    -generatechecksum { 1 }
	-fillmissingdatavalid { 0 } 
    -md5sumregexp { s,frames,meta/frames,i }
    }
    
    set cmdcase8 { createRDS
	-times 796068608-796069119
    -subject {md5sum not specified, default output dir}
    -type { R } 
    -usertype {RDSS4DEVTEST0720_R_L3 } 
	-outputdir $FRAME_OUTPUTDIR2
	-usejobdirs { 0 } 
    -channels { H2:LSC-AS_Q H2:LSC-DARM_ERR
	H2:IFO-SV_STATE_VECTOR H2:IFO-ACTIVITY_TYPE H2:IFO-ACTIVITY_STATE 
	H2:IFO-ACTIVITY_INDEX H1:LSC-AS_Q H1:LSC-DARM_ERR H1:IFO-SV_STATE_VECTOR
	H1:IFO-ACTIVITY_TYPE H1:IFO-ACTIVITY_STATE H1:IFO-ACTIVITY_INDEX }
	-compressiontype { gzip } 
    -compressionlevel { 1 } 
    -filechecksum { 1 }
	-frametimecheck { 1 } 
    -framedatavalid { 0 } 
    -framesperfile { 1 }
	-secperframe { 256 } 
    -allowshortframes { 1 } 
    -generatechecksum { 1 }
	-fillmissingdatavalid { 0 } 
    }
    
	set cmdcase9 { createRDS
	-times 796068608-796069119
    -subject {md5sum different from frame directory}
    -type { R } 
    -usertype { RDSS4DEVTEST0720_R_L3 } 
	-outputdir $FRAME_OUTPUTDIR2
	-usejobdirs { 0 } 
    -channels { H2:LSC-AS_Q H2:LSC-DARM_ERR
	H2:IFO-SV_STATE_VECTOR H2:IFO-ACTIVITY_TYPE H2:IFO-ACTIVITY_STATE 
	H2:IFO-ACTIVITY_INDEX H1:LSC-AS_Q H1:LSC-DARM_ERR H1:IFO-SV_STATE_VECTOR
	H1:IFO-ACTIVITY_TYPE H1:IFO-ACTIVITY_STATE H1:IFO-ACTIVITY_INDEX }
	-compressiontype { gzip } 
    -compressionlevel { 1 } 
    -filechecksum { 1 }
	-frametimecheck { 1 } 
    -framedatavalid { 0 } 
    -framesperfile { 1 }
	-secperframe { 256 } 
    -allowshortframes { 1 } 
    -generatechecksum { 1 }
	-fillmissingdatavalid { 0 } 
    -md5sumregexp { s,frames,meta/frames,g }
    }
    array set frame_output_dir [ list \
        cmdcase1 $FRAME_OUTPUTDIR \
        cmdcase2 $FRAME_OUTPUTDIR \
        cmdcase3 {} \
        cmdcase4 {} \
        cmdcase5 {} \
        cmdcase6 {} \
        cmdcase7 $FRAME_OUTPUTDIR2 \
        cmdcase8 $FRAME_OUTPUTDIR2 \
        cmdcase9 $FRAME_OUTPUTDIR2 \
    ]
    set nonprint  [ binary format H 1234 ]
    
    array set expected [ list \
        cmdcase1 "Your results:.+md5sum" \
        cmdcase2 "Your results:.+can be found at" \
        cmdcase3 "Your results:.+md5sum" \
        cmdcase4 "Your results:.+can be found at" \
        cmdcase5 "non-printable" \
        cmdcase6 "must be s,regexp,replacement" \
        cmdcase7 $FRAME_OUTPUTDIR2 \
        cmdcase8 $FRAME_OUTPUTDIR2 \
        cmdcase9 $FRAME_OUTPUTDIR2 \
    ]
    
    array set md5sumDir [ list \
        cmdcase1 $MD5SUMDIR \
        cmdcase2 $FRAME_OUTPUTDIR \
        cmdcase3 {} \
        cmdcase4 {} \
        cmdcase5 {} \
        cmdcase6 {} \
        cmdcase7 $MD5SUMDIR2 \
        cmdcase8 $FRAME_OUTPUTDIR2 \
        cmdcase9 $MD5SUMDIR2 \
    ] 

#------------------------------------------------------------------------
# Needs rwxrwx*** on /ldas_outgoing/frames/RDSVerify for this to work on sunbuild
# or run as user ldas to enable cleanup 
# cannot output in jobs directory as md5sumregexp must be specified
#------------------------------------------------------------------------

    proc testCleanup {} {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
	    foreach outputdir [ array names ::${name}::FRAME_FILES ] {
		    foreach file [ set ::${name}::FRAME_FILES($outputdir) ] {
    		    catch { file delete [ file join $outputdir $file ] } err
                #Puts 1 "delete $file $err"
    	    }
            catch { eval file delete [ glob -nocomplain $outputdir/*.md5 ] } err
            #Puts 1 "delete $outputdir/*.md5 $err" 
        }
        catch { file delete -force [ set ::${name}::MD5SUMDIR ] }
        catch { file delete -force [ set ::${name}::MD5SUMDIR2 ] }
    }  

#------------------------------------------------------------------------
# Verify that frames are in output
#------------------------------------------------------------------------

    proc ValidateFrameOutput { outputdir } {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
        Puts 5 "ValidateFrameOutput files [ set ::${name}::FRAME_FILES($outputdir) ]"
	    foreach file [ set ::${name}::FRAME_FILES($outputdir) ] {
    	    if	{ ! [ file exist $outputdir/$file ] } {
                return -code error "$outputdir/$file does not exist"
            }
        }
    }

#------------------------------------------------------------------------
# Verify that location of md5sum file per job specification
#------------------------------------------------------------------------

    proc Validate { jobReply outputdir Desc expected_md5sumDir } {
        set seen 0
        set md5file [ file tail $outputdir ].md5
    
        Puts 5  "$Desc jobReply $jobReply" 
	    if	{ [ regexp {md5sum (\S+) is in (\S+)} $jobReply -> md5file md5sumoutdir ] } {
            regsub {^http://[\d\.]+} $md5sumoutdir {} md5sumoutdir 
		    if { ! [ file exist $md5sumoutdir/$md5file ] } {
        	    return -code error "Error: $Desc md5sum $md5file not found in $md5sumoutdir"
    	    } elseif { ! [ regexp $expected_md5sumDir $md5sumoutdir ] } {
                return -code error "$Desc found md5sum in md5sum directory $md5sumoutdir/${md5file}\n
                expected it in $expected_md5sumDir"
            } else {
        	    Puts 5 "$Desc found md5sum in md5sum directory $md5sumoutdir/${md5file}"
            }
        } elseif { ! [ file exist $outputdir/$md5file ] } {
    	    return -code error "Error: $Desc md5sum $md5file not found in $outputdir"
	    } elseif { ! [ regexp  $expected_md5sumDir $outputdir ] } {
            return -code error "Error: $Desc md5sum $md5file found in $outputdir, expected it in $expected_md5sumDir"
        } else {
    	    Puts 5 "$Desc found md5sum in frame output directory $outputdir/$md5file"
        }
    }

    set case 1
    foreach cmd [ lsort [ info vars cmd* ] ] {
        testCleanup
        set cmd [ subst [ set $cmd ] ]
        set ldascmd [ lindex $cmd 0 ]
        Puts 5 "case $case ldascmd $ldascmd cmd \n$cmd"
        set expected_result $expected(cmdcase$case) 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ join $subject - ]
        Puts 5 "subject $subject, expected_result $expected_result"
        test regression:PR2953:$ldascmd:case$case:$subject:submit {} -body {
            if 	{ [catch {
                SubmitJob job $cmd
            } err] } {
                if  { [ info exist job(error) ] } {
                    set joberror $job(error)
                }
				LJdelete job
                return -code error $err
            }
            upvar jobreply jobreply
            set jobreply ""
            if  { [ info exist job(jobReply) ] } {
                upvar jobreply jobreply
                set jobreply $job(jobReply)
            } 
            if  { [ info exist job(outputDir) ] } {
                upvar outputDir outputDir 
                set outputDir $job(outputDir)
            } 
            LJdelete job
            return $jobreply
        } -match regexp -result "$expected_result"
        
        if  { [ info exist outputDir ] }  {
            regexp {(${::QA::LDAS_OUTGOING_ROOT}.+)$} $outputDir -> outputDir
            Puts 5 "outputDir $outputDir\n[ glob -nocomplain $outputDir ]"
        }
        if  { [ info exist jobreply ] && [ string length $jobreply ] } {
            Puts 5 "jobreply $jobreply\n$outputDir\n[ glob -nocomplain $outputDir ]"
            test regression:PR2953:$ldascmd:case$case:$subject:validate {} -body {
                if  { [ string length $frame_output_dir(cmdcase$case) ] } {
                    ValidateFrameOutput $frame_output_dir(cmdcase$case) 
                    Validate $jobreply $frame_output_dir(cmdcase$case) case$case $md5sumDir(cmdcase$case)
                }      
            } -result {}
        }
        incr case
    }
    
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2953::test
