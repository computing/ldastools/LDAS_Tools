#!/bin/sh
# -*- mode: tcl; indent-tabs-mode: nil; -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
    exec /usr/bin/env tclsh "$0" ${1+"$@"}

#------------------------------------------------------------------------
# Load in packages
#------------------------------------------------------------------------

if { [info exists env(SRCDIR)] } {
    foreach path $env(SRCDIR) {
     lappend ::auto_path $path
    }
}
if { [info exists env(PREFIX)] } {
    lappend ::auto_path $env(PREFIX)/lib/genericAPI
} elseif { [file isdirectory /ldas/lib/genericAPI] } {
    lappend ::auto_path /ldas/lib/genericAPI
}
lappend ::auto_path . /ldas/lib/test /ldas/lib
if { ! [ regexp "/ldas/bin" $::env(PATH) ] } {
    set ::env(PATH) "/ldas/bin:$::env(PATH)"
}


package require LDASJob
;#package require genericAPI

package require qalib

#--------------------------------------------------------------------
# Establish defaults for the global variables
#--------------------------------------------------------------------
set ::TestNames [list L0L1 L0L3 L1L3]
set ::Step 512
set ::Start [expr \
                 $::QA::DataSet($::QA::CurrentDataSet,start) + \
                 ( 2 * $::QA::DataSet($::QA::CurrentDataSet,dt) )]
             
set ::Iterations 7
set ::Prefix(L0L1) "L0 -> L1"
set ::Prefix(L0L3) "L0 -> L3"
set ::Prefix(L1L3) "L1 -> L3"

#--------------------------------------------------------------------
# Clean out information from previous runs
#--------------------------------------------------------------------
proc Cleanup {} {

    QA::Rexec $::DISKCACHE_API_HOST ldas [ subst { rm -rf $::OUTPUT_DIR } ]
    QA::Rexec $::DISKCACHE_API_HOST ldas [ subst { mkdir -p $::OUTPUT_DIR } ]
}

proc DoRDS {TimeRange UserType ChannelList OutputDir FileChecksum args} {
    set retval [eval ::QA::RDSCreate \
		    {"RDSEfficiency: Creation of $UserType: "} \
		    $TimeRange [list $ChannelList] R $UserType H \
		    -outputdir $OutputDir \
		    -usejobdirs 0 \
		    -compressiontype { gzip } \
		    -compressionlevel { 1 } \
		    -filechecksum { $FileChecksum } \
		    -frametimecheck { 1 } \
		    -framedatavalid { 0 } \
		    -framesperfile { 1 } \
		    -secperframe { 256 } \
		    -allowshortframes { 1 } \
		    -generatechecksum { 1 } \
		    -fillmissingdatavalid { 0 } $args]
    return $retval
}

proc Init {} {
    #--------------------------------------------------------------------
    # Cleanup any previous runs
    #--------------------------------------------------------------------
    QA::ConnectToAgent
    if { ![ info exist ::MANAGER_API_HOST ] } {
        set ::MANAGER_API_HOST [ exec uname -n ]
    }
    if { ! [ info exists ::env(LDASMANAGER) ] } {
        set ::env(LDASMANAGER) ${::MANAGER_API_HOST}:10001
    }
    #--------------------------------------------------------------------
    # Get LDAS resource values
    #--------------------------------------------------------------------

    if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
        source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    }

    if { [ info exist ::DISKCACHE_API_HOST ] } {
        if { ! [ info exist ::OUTPUT_DIR ] } {
            set ::OUTPUT_DIR [ ::QA::OutputDir $::DISKCACHE_API_HOST RDSEfficiency ]
        }
        Cleanup
    } elseif { ! [ info exist ::OUTPUT_DIR ] } {
        set ::OUTPUT_DIR [ file join /ldas_outgoing frames RDSEfficiency ]
    }

}

proc DoSet {Name Start Step Iterations UserType ChannelList FileChecksum} {
    set leader "FileChecksum: ${FileChecksum}::"
    set retval 1
    set end [expr $Start + $Step - 1]
    for {set i 0} {$i < $Iterations} {incr i 1} {
        DoRDS "${Start}-${end}" \
            $UserType \
            $ChannelList \
            $::OUTPUT_DIR \
            $FileChecksum \
            -no-delete
        #----------------------------------------------------------------
        # Record the total time it took to do the job
        #----------------------------------------------------------------
        if {[info exists job(jobTime)]} {
            set rtr [expr $Step / $job(jobTime)]
            ::QA::puts_info "$leader Job completed in: $job(jobTime) seconds ($rtr times real time)"
            set ::RawData($Name,$i,$FileChecksum,total) $job(jobTime)
            set ::RawData($Name,$i,$FileChecksum,frame) $job(frameTime)
        } else {
            ::QA::puts_info "$leader Job did not complete."
        }
        LJdelete job
        set Start [expr $Start + $Step]
        set end [expr $end + $Step]
    }
    return $retval
}

#--------------------------------------------------------------------
# Process args - process run options
#--------------------------------------------------------------------
proc getOptions {} {
    ;## Process command-line arguments
    ;## defaults

    set argx [list]
    for {set idx 0} {$idx < $::argc} {incr idx} {
        switch -exact -- [lindex $::argv $idx] {
            -i -
            --iterations {set ::Iterations [lindex $::argv [incr idx]]}
            --start {set ::Start [lindex $::argv [incr idx]]}
            --step {set ::Step [lindex $::argv [incr idx]]}
            --sec-per-frame {set ::SecPerFrame [lindex $::argv [incr idx]]}
            default {lappend argx [lindex $::argv $idx]}

        }
    }
}

proc main { } {
    set retval 1

    getOptions
    Init

    for {set filechecksum 0} {$filechecksum < 2} {incr filechecksum 1} {
        foreach level [list 1 3] {
            set user_type FCHKSUM_${filechecksum}_R_L${level}
            ;# L0 -> L$level
            set retval [expr $retval && \
                            [DoSet L0L${level} $::Start \
                                 $::Step \
                                 $::Iterations \
                                 $user_type \
                                 $::QA::DataSet($::QA::CurrentDataSet,H,level${level}) \
                                 $filechecksum]]
        }
    }

    for {set filechecksum 0} {$filechecksum < 2} {incr filechecksum 1} {
        foreach level [list 3] {
            set user_type FCHKSUM_${filechecksum}_L1_L${level}
            ;# L1 -> L3
            set retval [expr $retval && \
                            [DoSet L1L${level} $::Start \
                                 $::Step \
                                 $::Iterations \
                                 $user_type \
                                 $::QA::DataSet($::QA::CurrentDataSet,H,level${level}) \
                                 $filechecksum]]
        }
    }

    foreach name $::TestNames {
        for {set cksum 0} {$cksum < 2} {incr cksum 1} {
            set ft 0
            set tt 0
            for {set i 0} {$i < $::Iterations} {incr i 1} {
                set ft [expr $ft + $::RawData($name,$i,$cksum,frame)]
                set tt [expr $tt + $::RawData($name,$i,$cksum,total)]
            }
            set ft [expr $ft / $::Iterations]
            set tt [expr $tt / $::Iterations]
            set ::Stats($name,$cksum,frame) $ft
            set ::Stats($name,$cksum,total) $tt
        }
    }

    ;# :TODO: Print column headers for statistical information
    ;# Print statistical information
    foreach name $::TestNames {
        for {set cksum 0} {$cksum < 2} {incr cksum 1} {
            puts [format "%-10.10s %d %4d %7.2f %7.2f%% %7.2f" \
                      $::Prefix($name) \
                      $cksum \
                      $::Step \
                      $::Stats($name,$cksum,total) \
                      [expr ($::Step/$::Stats($name,$cksum,total)) * 100] \
                      $::Stats($name,$cksum,frame) \
                     ]
        }
    }
    return $retval
}

exit [expr ![main]]