package provide qaE11pre1 1.0

namespace eval QA {
    set CurrentDataSet E11pre1

    set DataSet(${CurrentDataSet},start) 781034000
    set dataSet(${CurrentDataSet},end) 781034688
    set DataSet(${CurrentDataSet},dt) 16

    set DataSet($CurrentDataSet,C,level1) \
	[list \
	     C1:SUS-SRM_SENSOR_UR!8 \
	     C1:PEM-Accel_X \
	     C1:Vac-IPEE_I \
	]
    set DataSet($CurrentDataSet,C,level2) \
	[list \
	     C1:SUS-SRM_SENSOR_UR \
	     C1:PEM-Accel_X \
	     C1:Vac-IPEE_I \
	]
    set DataSet($CurrentDataSet,C,level3) \
	[list \
	     C1:SUS-SRM_SENSOR_UR \
	     C1:PEM-Accel_X \
	     C1:Vac-IPEE_I \
	]
} ;# namspace - QA    