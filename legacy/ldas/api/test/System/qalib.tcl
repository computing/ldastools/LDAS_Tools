package provide qalib 1.0

#========================================================================
# Now that the autopath has been properly set up, 
#========================================================================
package require qadebug
package require qailwd
package require qaoptions
package require http
catch {package require LDASJob} err
#========================================================================
# Definition of global variables
#========================================================================

#------------------------------------------------------------------------
# Globus variables
#------------------------------------------------------------------------
proc TraceUpdate {VarName Index Op} {
    upvar 1 $VarName var
    switch $Op {
	w {
	    switch $VarName {
		::TMP {
		    set ::env(LDASTESTTMP) $var
		}
	    }
	}
    }
}
	
trace variable ::TMP w TraceUpdate

if [info exists ::env(TMP)] {
    set ::TMP $::env(TMP)
} elseif [info exists ::env(TEMP)] {
    set ::TMP $::env(TEMP)
} elseif [file isdirectory /var/tmp] {
    set ::TMP /var/tmp
} else {
    set ::TMP /tmp
}

set ::DATA_OUTPUT_DIR $env(HOME)

#------------------------------------------------------------------------
# Standard routines
#------------------------------------------------------------------------
namespace eval QA {
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## namespace DB
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval DB {
	namespace eval DB2 {
	    ;##----------------------------------------------------------
	    ;## Local variables
	    ;##----------------------------------------------------------
	    set DB2INSTANCE ldasdb
	    set user ldas
	    set Database ldas_tst

	    ;##----------------------------------------------------------
	    ;## Conclude
	    ;##----------------------------------------------------------
	    namespace export Conclude
	    proc Conclude { } {
	    }

	    ;##----------------------------------------------------------
	    ;## Exec
	    ;##----------------------------------------------------------
	    namespace export Exec
	    proc Exec {args} {
		variable DB2INSTANCE
		variable user

		set cmd [concat \
			     "/usr/bin/env \"PATH=$::LDAS/bin:$::env(PATH)\"" \
			     " \"DB2INSTANCE=${DB2INSTANCE}\" $::db2 -tf " \
			     [lindex $args end]]
		switch [llength $args] {
		    1 {
			set err [::QA::Rexec $::METADATA_API_HOST $user $cmd]
		    }
		    2 {
			::QA::DB::PutsCmd [lindex $args 0] $cmd
			flush [lindex $args 0]
		    }
		}
	    }

	    ;##----------------------------------------------------------
	    ;## Init
	    ;##----------------------------------------------------------
	    proc Init { } {
		;##------------------------------------------------------
		;## Get information about running DB2
		;##------------------------------------------------------
		if [ file exist $::TOPDIR/cntlmonAPI/LDASdb2utils.rsc ] {
		    set TOPDIR $::TOPDIR
		    source $::TOPDIR/cntlmonAPI/LDASdb2utils.rsc
		} else {
		    set ::db2 /usr2/$env(DB2INSTANCE)/sqllib/bin/db2
		}
		source [ file join $::TOPDIR metadataAPI LDASdsnames.ini ]
	    }

	    ;##----------------------------------------------------------
	    ;## Open
	    ;##----------------------------------------------------------
	    proc Open { Database } {
		variable user

		if [catch {set fid \
			       [::QA::Remote::Pipe \
				    ${user} $::METADATA_API_HOST] } err ] {
		    error $err
		} else {
		    foreach {dbuser dbpasswd} \
			[set ::${Database}(login)] { break }
		    ::QA::Remote::PutsCmd $fid "setenv DB2INSTANCE ldasdb"
		    set ::QA::DB::Info(CmdPrefix,$fid) "$::db2 "
		    set ::QA::DB::Info(CmdFilePrefix,$fid) "$::db2 -tf "
		    ::QA::DB::PutsCmd $fid "connect to $Database user $dbuser using [decode64 $dbpasswd]"
		}
		return $fid
	    }
	}

	;##--------------------------------------------------------------
	;## PutsCmd
	;##--------------------------------------------------------------
	namespace export PutsCmd
	proc PutsCmd { fid cmd { wait 1 } } { 
	    if [info exists ::QA::DB::Info(CmdPrefix,$fid)] {
		set prefix $::QA::DB::Info(CmdPrefix,$fid)
	    } else {
		set prefix ""
	    }
	    if [info exists ::QA::DB::Info(CmdSuffix,$fid)] {
		set suffix $::QA::DB::Info(CmdSuffix,$fid)
	    } else {
		set suffix ""
	    }
	    set cmd "${prefix}${cmd}${suffix}"
	    if { [catch {
            set reply [ ::QA::Remote::PutsCmd $fid $cmd $wait ]
        } err] } {
		    return -code error $err
	    }
	    return $reply
	}
	
	namespace export PutsCmd
	proc PutsCmdFile { fid cmd { wait 1 } } { 
	    if [info exists ::QA::DB::Info(CmdFilePrefix,$fid)] {
		set prefix $::QA::DB::Info(CmdFilePrefix,$fid)
	    } else {
		set prefix ""
	    }
	    if [info exists ::QA::DB::Info(CmdFileSuffix,$fid)] {
		set suffix $::QA::DB::Info(CmdFileSuffix,$fid)
	    } else {
		set suffix ""
	    }
	    set cmd "${prefix}${cmd}${suffix}"
	    if [catch {::QA::Remote::PutsCmd $fid $cmd $wait} err] {
		return -code error $err
	    }
	    set var ::QA::Remote::Data$fid
	    if [info exists $var] {
		upvar $var varup
		set reply $varup
		unset $var
	    } else {
		set reply ""
	    }
	    return $reply
	}
    }
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## namespace Command
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval Command {
	variable OptionKey {opt}

	namespace export ExtractOptions
	proc ExtractOptions {Testname TestArray OptionArray} {
	    variable OptionKey

	    upvar $TestArray test_array
	    upvar $OptionArray option_array

	    if {[array exists option_array]} {
		unset option_array
	    }
	    foreach opt [array names test_array "${Testname}:${OptionKey}:*"] {
		set opt [lindex [split $opt :] end]
		set option_array($opt) $test_array($Testname:${OptionKey}:$opt)
	    }
	}

	namespace export OptionSubstitution
	proc OptionSubstitution {Default Custom Output} {
	    upvar $Default dflt
	    upvar $Custom custom
	    upvar $Output output

	    if {[array exists retval]} {
		unset retval
	    }
	    array set retval [array get dflt]
	    array set retval [array get custom]
	    if {[array exists output]} {
		unset output
	    }
	    array set output [array get retval]
	}
    }
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## namespace DiskCache
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval DiskCache {
	proc WaitOnCache {Query Timeout} {
	    ;## Query - disk cache query
	    ;## Timout - Maximum time in seconds for which to await
	    ;##          the arrival of the data.

	    ;##----------------------------------------------------------
	    ;## Initialize
	    ;##  - Record the current time
	    ;##----------------------------------------------------------
	    set start [clock seconds]
	    set job "WaitOnCache$start"
	    set cmd "cacheGetFilenames -allowgaps 0 $Query"
	    while 1 {
		;##------------------------------------------------------
		;## Query the diskcache for the files
		;##------------------------------------------------------
		::QA::puts_info "job: $job cmd: $cmd"
		if {! [catch {::QA::RunLDASJob "" $cmd -job-name $job -no-delete -quiet} err]} {
		    if [info exists $job] {
			upvar $job jobup
			::QA::puts_info "status: $jobup(status)"
			if {! [string equal $jobup(status) "error"]} {
			    ;## Completed successfully
			    LJdelete $job
			    return
			}
		    }
		} else {
		    ::QA::puts_info "err: $err"
		}
		LJdelete $job
		;##------------------------------------------------------
		;## Check to see if the allotted time has been exceeded.
		;##------------------------------------------------------
		if ([expr ( [clock seconds] - $start ) >= $Timeout ]) {
		    set wait [expr [clock seconds] - $start]
		    error "The diskcache query still reports gaps after $wait seconds"
		}
		;##------------------------------------------------------
		;## Sleep for N seconds
		;##------------------------------------------------------
		::QA::puts_info "waiting to try again"
		after 5000
	    }
	}
    }
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## LAL functions
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval LAL {
	##---------------------------------------------------------------
	## Routine to reset where lal has been installed
	##---------------------------------------------------------------
	proc SetBase {NewBase} {
	    set ::QA::LAL::Base $NewBase
	    set ::QA::LAL::earth00_04 $NewBase/share/lal/earth00-04.ilwd
	    set ::QA::LAL::sun00_04 $NewBase/share/lal/sun00-04.ilwd

	    set ::QA::LAL::earth03 $NewBase/share/lal/earth03.ilwd
	    set ::QA::LAL::earth04 $NewBase/share/lal/earth04.ilwd

	    set ::QA::LAL::sun03 $NewBase/share/lal/sun03.ilwd
	    set ::QA::LAL::sun04 $NewBase/share/lal/sun04.ilwd
	}

	SetBase "/lal"
    }
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## Math functions
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval Math {
	proc Mean {Samples} {
	    set mean 0.0
	    set n [ llength $Samples ]
	    
	    if { $n > 0 } {
		set mean [ expr ([ join $Samples + ]) / $n. ]
	    }

	    return $mean	    
	}
    }

    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## namespace Remote
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval Remote {
	set Data ""

	proc Pipe {User Host} {
	    set cmd "|ssh -t ${User}@${Host}"
	    ::QA::Debug::Puts 10 "::QA::Remote::Pipe: $cmd"
	    if [catch {set fid \
			   [open $cmd r+ ]} err ] {
		::QA::Debug::Puts 10 "ERROR ::QA::Remote::Pipe: $err"
		error $err
	    } else {
		#fconfigure $fid -blocking off -buffering line
        fconfigure $fid -buffering none -translation binary -blocking 0
		fileevent $fid readable [list ::QA::Remote::ReadData $fid]
	    }
	    return $fid
	}

	proc PutsCmd { fid cmd { wait 1 } } { 
	    set reply "" 
	    if { [ catch {
		::QA::Debug::Puts 15 "cmd '$cmd' waiting"
		puts $fid $cmd
		set var ::QA::Remote::Data$fid
		flush $fid
		if { $wait } {
		    vwait $var
		    set reply [ set $var ]
		    unset $var
		}
	    } err ] } {			
		return -code error $err
	    }
	    return $reply
	}

	proc ReadDataX { fid args } {
	    if { [ eof $fid ] } {
		catch { close $fid }
		::QA::Debug::Puts 1 "readdata set eof"
		set ::QA::Remote::Data eof
		return
	    }
	    set result ""
	    set var ::QA::Remote::Data$fid
	    while { [gets $fid data] > -1 } {
		::QA::Debug::Puts 15 "data '$data'"
		set result [concat $result $data]
	    }
	    if [info exists $var ] {
		upvar $var varup
		set $var \
		    [concat $varup $result]
	    } else {
		set $var $result
	    }
	}
    
	proc ReadData { fid args } {
	    if { [ eof $fid ] } {
		catch { close $fid }
		::QA::Debug::Puts 1 "readdata set eof"
		set ::QA::Remote::Data eof
		return
	    }
        if  { [ catch {
	        set var ::QA::Remote::Data$fid
            set result [ read -nonewline $fid ]
            if  { [ string length $result ] } {
		        ::QA::Debug::Puts 0 "$result"
            }
            set $var $result
        } err ] } {
            set $var $err
        }
	}
    
	proc WaitForDone { fid } {
	    set key "_=DONE=_"
	    set var ::QA::Remote::Data$fid
	    set result [PutsCmd $fid "echo $key"]
	    while { ! [regexp -- ".*$key.*" $result] } {
		;# Read more data from the stream
		::QA::Remote::ReadData $fid
		upvar $var varup
		set result $varup
		unset $var
	    }
	}
    }
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## namespace RDS
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval RDS {
	variable valid_options \
	    [ list \
		  allowshortframes \
		  filechecksum \
		  framechecksum \
		  framedatavalid \
		  framesperfile \
		  generatechecksum \
		  outputdir \
		  secperframe \
		 ]
    } ;# namespace RDS
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## namespace URL
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval URL {
	##---------------------------------------------------------------
	## This procedure takes an arbitrary number of urls and runs
	##   a sanity check on them
	##---------------------------------------------------------------
	namespace export Check
	proc Check {args} {
	    namespace import ::checkURL
	    namespace import ::QA::Debug::Puts

	    foreach url $args {
		if {[llength $url] > 1} {
		    foreach u $url {
			Puts 5 $u
			checkURL $u
		    }
		} else {
		    Puts 5 $url
		    checkURL $url
		}
	    }
	}

	namespace export List
	proc List {args} {
	    namespace import ::checkURL
	    namespace import ::QA::Debug::Puts

	    foreach url $args {
		if {[llength $url] > 1} {
		    foreach u $url {
			Puts 5 $u
		    }
		} else {
		    Puts 5 $url
		}
	    }
	}

	namespace export Read
	proc Read { URL OutVar } {
	    upvar $OutVar retval
	    
	    set retval [string trim [LJread $URL ]]
	}
    } ;## namespace URL
    #====================================================================
    # These are the quality assurance routines
    #====================================================================

    #--------------------------------------------------------------------
    # Common variables - These are order dependent
    #--------------------------------------------------------------------

    set ec 0
    set ExitCode(OK)		$ec
    set ExitCode(USER)		[ incr ec ]
    set ExitCode(NO_AGENT)	[ incr ec ]
    set ExitCode(NO_OUTPUTDIR)	[ incr ec ]
    set ExitCode(LAST)		$ec

    set LDAS_ROOT "/ldas"
    set LDAS_OUTGOING_ROOT "/ldas_outgoing"
    set SSH_AGENT_MGR "${LDAS_ROOT}/bin/ssh-agent-mgr"
    set AGENT_FILE "${LDAS_OUTGOING_ROOT}/managerAPI/.ldas-agent"
    set TIMEOUT 10  ;# Maximum number of seconds to wait for a command
    set OUTPUT_PATH [ list /scratch/test/system /ldas_outgoing/frames ]

    #--------------------------------------------------------------------
    # Put a continuation line
    #--------------------------------------------------------------------
    proc puts_cont { Text } {
	if { [ IsVerbose ] } {
	    puts "-- $Text"
	}
    }

    #--------------------------------------------------------------------
    # Poceedure to print failed message to screen
    #--------------------------------------------------------------------

    proc puts_fail { Text } {
	puts_cont "FAIL: $Text"
    }

    #--------------------------------------------------------------------
    # Poceedure to print informational message to screen
    #--------------------------------------------------------------------

    proc puts_info { Text } {
	puts_cont "INFO: $Text"
    }

    #--------------------------------------------------------------------
    # Poceedure to print passed message to screen
    #--------------------------------------------------------------------

    proc puts_pass { Text } {
	puts_cont "PASS: $Text"
    }

    #--------------------------------------------------------------------
    # Connect to the ssh-agent so as not to be prompted for password
    #--------------------------------------------------------------------
    proc ConnectToAgent { { Start 0 } } {

	#----------------------------------------------------------------
	# Verify user is the ldas user
	#----------------------------------------------------------------
	if { ! [ string match ldas $::env(USER) ] } {
	    pute $QA::ExitCode(USER) "This program must be run as user 'ldas'"
	}
	#----------------------------------------------------------------
	# Get information about the the ssh-agent
	#----------------------------------------------------------------
	if { [ catch {set agent_env [ exec $QA::SSH_AGENT_MGR --shell=tclsh --agent-file=$QA::AGENT_FILE check 2> /dev/null ] } err ] } {
	    # Attempt to start the agent
	    if { [ catch {
		if { $Start == 0 } { return -code error "Autostart disabled" }
		set agent_env [ exec $QA::SSH_AGENT_MGR --shell=tclsh --agent-file=$QA::AGENT_FILE start 2> /dev/null ] } err ] } {
		pute $QA::ExitCode(NO_AGENT) "Unable to connect to the ssh-agent"
	    }
	}
	eval $agent_env
    }

    #--------------------------------------------------------------------
    # pute - prints a error message onto stderr and exits
    #  ExitCode - The exit value
    #  Message - Text of message to display on stderr before exiting
    #--------------------------------------------------------------------
    proc pute { ExitCode Message } {
	puts stderr $Message
	exit $ExitCode
    }

    #--------------------------------------------------------------------
    # IsVerbose - Check for verbose mode
    #--------------------------------------------------------------------
    proc IsVerbose { } {
	if { [ info exists ::env(TEST_VERBOSE_MODE) ] } {
	    if { $::env(TEST_VERBOSE_MODE) == "true" } {
		return 1
	    }
	}
	return 0
    }

    #--------------------------------------------------------------------
    # LoadChannelInfo - Reads information from an RDS description file
    #--------------------------------------------------------------------
    proc LoadChannelInfo { Filename } {
	if { [file exists $Filename] } {
	    set filename $Filename
	} elseif { [info exists ::env(SRCDIR)] && \
		       [file exists [file join $::env(SRCDIR) $Filename ]] } {
	    set filename [file join $::env(SRCDIR) $Filename]
	} else {
	    foreach path $::auto_path {
		if {[file exists [file join $path $Filename]]} {
		    set filename [file join $path $Filename]
		    break
		}
	    }
	}
	set fd [open $filename r]
	set retval [list]
	while {[gets $fd line] >= 0} {
	    if { [regexp -- {(\S+)\s+(\S+)} $line -> channel resample ] } {
		if { $resample != 1 } {
		    set channel "${channel}!${resample}"
		}
		lappend retval $channel
	    }
	}
	close $fd;
	return $retval
    }
    #--------------------------------------------------------------------
    # OutputDir - Determine an output directory
    #--------------------------------------------------------------------
    proc OutputDir { Host Path } {
	foreach base $QA::OUTPUT_PATH {
	    set tandem [TandemName $Host]
	    switch -regexp -- $base {
		^/scratch/ {
		    set base [file join $base $tandem]
		}
	    }
	    set path [file join $base $Path]
	    switch -regexp -- [ QA::Rexec $Host ldas "mkdir -p $path" ] {
		^$ { }
		default { continue }
	    }
	    set file [file join $path \
			  [concat "." [info hostname] "-" [pid] ] ]
	    switch -regexp -- [ QA::Rexec $Host ldas "touch $file" ] {
		^$ { }
		default { continue }
	    }
	    QA::Rexec $Host ldas "rm -f $file"
	    return $path
	}
	return -code error \
	    -errorcode $QA::ExitCode(NO_OUTPUTDIR) \
	    "Unable to find a writable output directory"
    }

    #--------------------------------------------------------------------
    # Generate an RDS
    #--------------------------------------------------------------------
    proc RDSCreate { Leader TimeRange ChannelList Type UserType ifo args } {
	set nodelete ""
	foreach val $args {
	    #------------------------------------------------------------
	    # Determine if the argument is an option or a value
	    #------------------------------------------------------------
	    switch -glob -- $val {
		-no-delete {
		    set nodelete "-no-delete"
		}
		-* {
		    set option $val
		    set val ""
		}
	    }
	    #------------------------------------------------------------
	    # Assign the value to the option
	    #------------------------------------------------------------
	    if {[info exists option]} {
		regsub -- {^-} $option "" option_name
		# Puts 1 "option $option_name val $val"
		set $option_name $val
	    }
	}
	set cmd [subst { createRDS
	    -times { $TimeRange }
	    -type { $Type }
	    -channels { $ChannelList }
	    -usertype { $UserType } } ]
	unset val
	foreach opt $::QA::RDS::valid_options {
	    if { [info exists $opt] } {
		upvar 0 $opt val
		set cmd "$cmd -$opt $val"
		unset val
	    }
	}
	if { ![info exists dumpdir] } {
	    return [eval ::QA::RunLDASJob {$Leader} {$cmd} $nodelete]
	} elseif { ![ string length $dumpdir ] } {
	    return [eval ::QA::RunLDASJob {$Leader} {$cmd} $nodelete]
	} else {
	    set fname [ file join $dumpdir createRDS_${ifo}_$UserType.cmd ]
	    set fd [ open $fname w ]
	    puts -nonewline $fd $cmd
	    close $fd
	    return 1
	}
    }
    #--------------------------------------------------------------------
    # Execute a command on a remote host
    #   Host - remote host of connection
    #   User - remote user of connection
    #   Command - Command to execute on the remote host
    #--------------------------------------------------------------------
    namespace export Rexec
    proc Rexec { Host User Command } {
	set ssh_host ""
	set rsh_cmd ""
	if { [ llength $Host ] == 2 } {
	    set ssh_host [ lindex $Host 0 ]
	    set rsh_host [ lindex $Host 1 ]
	} else {
	    set ssh_host [ lindex $Host 0 ]
	    set rsh_host [ lindex $Host 0 ]
	}
	if { ! [ string match $ssh_host $rsh_host ] } {
	    set rsh_cmd "rsh $rsh_host"
	}
	if [ catch { set retval [ exec ssh $ssh_host $rsh_cmd $Command ] } err ] {
	    return $err
	}
	return $retval
    }

    #--------------------------------------------------------------------
    # RunLDASJob - Running LDAS commands via LJrun
    #--------------------------------------------------------------------
    proc RunLDASJob { Leader Command args } {
	set delete_job 1
	set expected_error ""
	set job_name job
	set retval 1
	set verbose 1
	
	if	{ ! [ info exist ::USE_GLOBUS_CHANNEL ] } {
		set ::USE_GLOBUS_CHANNEL 0
	}
	foreach val $args {
	    #------------------------------------------------------------
	    # Determine if the argument is an option or a value
	    #------------------------------------------------------------
	    switch -glob -- $val {
		-* {
		    set option $val
		    set val ""
		}
	    }
	    #------------------------------------------------------------
	    # Assign the value to the option
	    #------------------------------------------------------------
	    switch -exact -- $option {
		-error-msg {
		    set expected_error $val
		}
		-no-delete {
		    set delete_job 0
		}
		-job-name {
		    set job_name $val
		}
		-quiet {
		    set verbose 0
		}
	    }
	}

	set after_id [ after [ expr $::QA::TIMEOUT * 1000 ] {
	    LJrun abort_job -globus $::USE_GLOBUS_CHANNEL {
		puts_fail "Timed out"
		abortJob -stopjob $job(jobid)
	    }
	}]

	catch { LJrun $job_name -globus $::USE_GLOBUS_CHANNEL -gsi 1 { $Command } } err
	upvar 0 $job_name job
	after cancel $after_id
	if { $LJerror && $verbose } {
	    if [ info exists job(jobid) ] {
		set jobid "$job(jobid): ";
	    } else {
		set jobid "";
	    }
	    if { [ string length $expected_error] > 0 } {
		if { ! [regexp $expected_error $job(error) match] } {
		    puts_fail "${Leader}: ${jobid}Wrong error message was generated - $job(error)"
		    set retval 0
		}
	    } else {
		puts_fail "${Leader}: ${jobid}$job(error)"
		set retval 0
	    }
	} elseif { [string length $expected_error] > 0 } {
	    puts_fail "${Leader}: No error message generated"
	    set retval 0
	}
	if { $retval && $verbose } {
	    puts_pass "${Leader}"
	}
	#--------------------------------------------------------------------
	# All finished with the check
	#--------------------------------------------------------------------
	if { $delete_job } {
	    LJdelete job
	}
	return $retval
    }

    #--------------------------------------------------------------------
    # Map symbolic names to tandem systems
    #--------------------------------------------------------------------
    proc TandemNameX {Host} {
	set tandem site
	if {[regexp {^(ldas-suntest|ldasbox|suntest|testbox)(\d+)(\..*)*$} $Host -> base tandem net]} {
	    switch -exact -- $tandem {
		1 { set tandem i}
		2 { set tandem ii}
		3 { set tandem iii}
		4 { set tandem iv}
		5 { set tandem v}
		6 { set tandem vi}
		7 { set tandem vii}
		8 { set tandem viii}
		9 { set tandem ix}
		10 { set tandem x}
	    }
	    set tandem "tandem-$tandem"
	}
	return $tandem
    }
	
    proc TandemName {Host} {
	
	catch { exec cat /etc/ldasname } tandem
	return $tandem
	
    }

    #--------------------------------------------------------------------
    # UseDataSet - Establish the set of data to be used
    #--------------------------------------------------------------------
    proc UseDataSet { DataSetName } {
	foreach var [ array names ::QA::DataSet ${DataSetName},* ] {
	    set var [ join [lreplace [split $var ,] 0 0 ] , ]
	    set ::QA::ActiveDataSet($var) $::QA::DataSet(${DataSetName},$var)
	}
    }
    proc ImportVariables { Source Dest } {
	variable v

	foreach v [info vars ${Source}::*] {
	    set new [subst $Dest]::[namespace tail $v]
	    if [array exists $v] {
		array set $new [array get $v]
	    } else {
		variable $new [subst $[subst $v]]
	    }
	}
    }

    #--------------------------------------------------------------------
    # Compares two version numbers. Useful for sorting.
    #--------------------------------------------------------------------
    proc VersionCompare {LHS RHS} {
	set rhs [split $RHS .]
	set lhs [split $LHS .]

	while { [llength $rhs] > 0 \
		    && [llength $lhs] > 0 } {
	    set r [lindex $rhs 0]
	    set l [lindex $lhs 0]
	    if { $l < $r } {
		return -1
	    } elseif { $l > $r } {
		return 1
	    } else {
		set rhs [lreplace $rhs 0 0]
		set lhs [lreplace $lhs 0 0]
	    }
	}
	set r [llength $rhs]
	set l [llength $lhs]
	    if { $l < $r } {
		return -1
	    } elseif { $l > $r } {
		return 1
	    } else {
		return 0
	    }
    }

    #--------------------------------------------------------------------
    # Establish options local to this file
    #--------------------------------------------------------------------
    Options::Add {-s} {--site} {string} \
	{specifies the site of the managerAPI} \
	{ set ::SITE $::QA::Options::Value}
    Options::Add {-P} {--port} {string} \
	{managerAPI port to be used for communication} \
	{ set ::PORT $::QA::Options::Value}
    Options::Add {-h} {--host} {string} \
	{specifies the host of the managerAPI} \
	{ set ::HOST $::QA::Options::Value}
    Options::Add {-i} {--ifo} {string} \
	{specifies the IFO} \
	{ set ::IFO $::QA::Options::Value}
    Options::Add {-e} {--email} {string} \
	{specifies the email address for responces} \
	{ set ::EMAIL $::QA::Options::Value}
    Options::Add {-u} {--user} {string} \
	{execute the commands as the specified user} \
	{ set ::USER $::QA::Options::Value}
    Options::Add {-p} {--pass} {string} \
	{??? :TODO: Document this option} \
	{ set ::PASS $::QA::Options::Value}
    Options::Add {-b} {--base} {string} \
	{specifies the base port of the LDAS system} \
	{ set ::BASE $::QA::Options::Value}
    Options::Add {-c} {--code} {string} \
	{command to be executed} \
	{ set ::RUNCODE $::QA::Options::Value}
    Options::Add {} {--enable-globus} {string} \
	{Specifies if globus should be used (0 or 1)} \
	{ set ::USE_GLOBUS_CHANNEL $::QA::Options::Value}
    Options::Add {} {--enable-gsi} {string} \
	{Specifies if gsi enscryption should be used (0 or 1)} \
	{ set ::USE_GSI $::QA::Options::Value}
    Options::Add {} {--tmp-dir} {string} \
	{Specifies directory to be used for temporary files} \
	{ set ::TMP $::QA::Options::Value}
    Options::Add {} {--top-dir} {string} \
	{Specifies top directory for LDAS configuration} \
	{ set ::TOPDIR $::QA::Options::Value}
    Options::Add {} {--data-output-dir} {string} \
	{Specifies directory to be used for data files intended to be kept} \
	{ set ::DATA_OUTPUT_DIR $::QA::Options::Value}
    Options::Add {} {--manager-reply-timeout} {string} \
	{timeout secs for job if LDASJobH does not hear back from manager} \
	{ set ::env(MANAGER_REPLY_TIMEOUT) $::QA::Options::Value}	
	
} ;# namespace

#------------------------------------------------------------------------
# Information for different data sets
# Each data set should define:
#  start - Start time for the data set
#  level1 - The list of channels used for level 1 RDS
#  level2 - The list of channels used for level 2 RDS
#  level3 - The list of channels used for level 3 RDS
#------------------------------------------------------------------------
package require qaE11pre1
package require qaS3
package require qaS4
package require qaS5

#------------------------------------------------------------------------
# Older QA Library routines
#------------------------------------------------------------------------
proc qaInit {args} {
    set ::QA::PROGRAM [file rootname [lindex [file split $::argv0] end]]

    set ::USE_GLOBUS_CHANNEL 0

    array set host {
        dev  ldas-dev.ligo.caltech.edu
        cit  ldas-cit.ligo.caltech.edu
        llo  ldas.ligo-la.caltech.edu
        lho  ldas.ligo-wa.caltech.edu
        mit  ldas.mit.edu
        test ldas-test.ligo.caltech.edu
        corona corona.ligo.caltech.edu
        ldasbox1 ldasbox1.ligo.caltech.edu
        ldasbox2 ldasbox2.ligo.caltech.edu
        ldasbox3 ldasbox3.ligo.caltech.edu
        ldasbox4 ldasbox4.ligo.caltech.edu
        ldasbox5 ldasbox5.ligo.caltech.edu
        ldas-suntest1 ldas-suntest1.ligo.caltech.edu
        tandem-ii ldas-suntest2.ligo.caltech.edu
        tandem-iii ldas-suntest3.ligo.caltech.edu
        tandem-iv ldas-suntest4.ligo.caltech.edu
        tandem-v ldas-suntest5.ligo.caltech.edu
    }

    array set port {
        dev  10001
        cit  10001
        lho  10001
        llo  10001
        mit  10001
        test 10001
        corona 12001
        ldasbox1 10001
        ldasbox2 10001
        ldasbox3 10001
        ldasbox4 10001
        ldasbox5 10001
        ldas-suntest1 10001
        tandem-ii 10001
        tandem-iii 10001
        tandem-iv 10001
        tandem-v 10001
    }

    array set ifo {
        dev  H
        cit  H
        lho  H
        llo  L
        mit  H
        test H
        corona H
        ldasbox1 H
        ldasbox2 H
        ldasbox3 H
        ldasbox4 H
        ldasbox5 H
        ldas-suntest1 H
        tandem-ii H
        tandem-iii H
        tandem-iv H
        tandem-v H
    }

    array set code {
        dev LDAS-DEV
        cit LDAS-CIT
        lho LDAS-WA
        llo LDAS-LA
        mit LDAS-MIT
        test LDAS-TEST
        corona CORONA
        ldasbox1 BOX-I
        ldasbox2 BOX-II
        ldasbox3 BOX-III
        ldasbox4 BOX-IV
        ldasbox5 BOX-V
        ldas-suntest1 LDAS-SUNTESTI
        tandem-ii TANDEM-II
        tandem-iii TANDEM-III
        tandem-iv TANDEM-IV
        tandem-v TANDEM-V
    }

    ;## Defaults
    set ::SITE dev
    set ::USER ""
    set ::PASS ""
    set ::EMAIL ""
    set ::BASE 10000
    set ::RUNCODE ""
    set ::HOST ""
    set ::PORT ""
    set ::IFO ""
    set ::RCFILE ~/.ldasrc
    set ::TOPDIR /ldas_outgoing

    ;##------------------------------------------------------------------
    ;## Source rc file
    ;##------------------------------------------------------------------
    catch {source $::RCFILE} err

    ;##------------------------------------------------------------------
    ;## Process command-line arguments
    ;##------------------------------------------------------------------
    ::QA::Options::Parse ::argv
    set ::argc [llength $::argv]

    ;##------------------------------------------------------------------
    ;## Load basic configuration information
    ;##------------------------------------------------------------------
    if [ file readable $::TOPDIR/LDASapi.rsc ] {
	source $::TOPDIR/LDASapi.rsc
    }

    ;##------------------------------------------------------------------
    ;## Finish setting variables
    ;##------------------------------------------------------------------
    foreach var {HOST PORT IFO RUNCODE} arr {host port ifo code} {
        if {![string length [set ::$var]]} {
            set ::$var [set ${arr}($::SITE)]
        }
    }
    set ::QA::JobIDPattern "^$::RUNCODE\\d+\$"

    return
}

proc bgerror {err} {
    set trace {}
    puts stderr "\nError: $err"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
    }

    exit 1
}

proc myName { {level "-1"} } {
    if {$level > 0} {
        return {}
    }

    set name [lindex [info level $level] 0]
    return $name
}

;## new version of myName
;## also returns namespace info
proc myname {args} {
    if {[info level] < 2} {return ::}

    set cmd [lindex [info level -1] 0]
    set nsp [uplevel 1 {namespace current}]

    return [string trimleft [namespace eval $nsp namespace origin $cmd] ":"]
}

proc jobdir {jobid} {
    regexp {\d+} $jobid id
    if {![string length $id]} {
        return -code error "[myName]: missing job id number"
    }

    set jobid "${::RUNCODE}${id}"
    set myriad [expr {$id / 10000}]
    set jobdir "${::RUNCODE}_${myriad}/${jobid}"

    return $jobdir
}

proc serverOpen {{port 0}} {
    set ::LSID [socket -server serverCfg -myaddr [info hostname] $port]
    foreach {::LADDR ::LHOST ::LPORT} [fconfigure $::LSID -sockname] {break}
    return
}

proc serverCfg  {sid addr port} {
    fconfigure $sid -buffering line
    fileevent $sid readable [list set ::SID $sid]
    return
}

proc serverClose {} {
    catch {close $::LSID}
    return
}

proc getURL {URL {validate 0} {timeout 0}} {
    while {1} {
        set token [http::geturl $URL -validate $validate -timeout $timeout]

        set status [http::status $token]
        switch -exact -- $status] {
            timeout {
                set errmsg "[myName]: timeout after ${timeout}ms"
                http::cleanup $token
                return -code error $errmsg
            }

            error {
                set errmsg [http::error $token]
                http::cleanup $token
                return -code error $errmsg
            }

            reset {}
            ok {}
            default {}
        }

        switch -regexp -- [http::ncode $token] {
            {1\d\d} -
            {2\d\d} {
                ;## ok
                break
            }

            {3\d\d} {
                ;## redirection
                array set meta [set ${token}(meta)]
                http::cleanup $token
                set URL $meta(Location)
                continue
            }

            {4\d\d} -
            {5\d\d} {
                ;## error
                set errmsg [http::code $token]
                http::cleanup $token
                return -code error $errmsg
            }

            default {
                ;## unknown
                set errmsg "[myName]: unknown http error code: [http::ncode $token]"
                http::cleanup $token
                return -code error $errmsg
            }
        }
    }

    return $token
}

proc checkURL {url} {
    set token [getURL $url 1]
    http::cleanup $token
}

proc getResultURL {sid} {
    if {[eof $sid]} {
        catch {close $sid}
        return -code error "[myName]: got unexpected eof."
    }

    while {[gets $sid line] != -1} {
        append data "$line\n"
    }
    catch {close $sid}

    set state {}
    set files {}
    set path {}
    foreach line [split $data "\n"] {
        switch -regexp -- $line {
            {Subject:} { set state subject }
            {Your results:} { set state files }
            {can be found at:} { set state url }

            default {
                switch -exact -- $state {
                    subject {
                        if {[regexp {error\!} $line]} {
                            return -code error "[myName]: error in reply: [join $data]"
                        }
                    }

                    files {
                        set files [concat $files [split $line]]
                    }

                    url {
                        regexp {^([^\{\}\s]+)} $line -> path
                        set state done
                    }

                    default {}
                }
            }
        }
    }

    if {![string length $path]} {
        return -code error "[myName]: can't find url in reply: [join $data]"
    }

    if {![string length $files]} {
        return -code error "[myName]: can't find files in reply: [join $data]"
    }

    foreach fname $files {
        ;## Check if fname is already at end of path
        if {[string equal $fname [file tail $path]]} {
            lappend retval $path
        } else {
            lappend retval [join "$path $fname" /]
        }
    }
    return $retval
}

proc getResult {sid {trim ""}} {
    if {[eof $sid]} {
        catch {close $sid}
        return -code error "[myName]: got unexpected eof."
    }

    while {[gets $sid line] != -1} {
        append data "$line\n"
    }
    catch {close $sid}

    if {[string length $trim]} {
        ;## strip timing data
        set idx [string first "=====" $data]
        if {$idx >= 0} {
            set data [string trim [string replace $data $idx end]]
        }
    }

    if {[regexp {error\!} $data]} {
        return -code error "[myName]: error in reply:\n$data"
    }

    if {[string length $trim]} {
        set state {}
        set msg {}
        foreach line [split $data "\n"] {
            if {[regexp -- {Subject:} $line]} {
                set state subject
                continue
            }
            if {[string equal subject $state]} {
                set state {}
                continue
            }
            append msg "$line\n"
        }
        set data [string trim $msg]
    }

    #set data [join $data]
    return $data
}

proc trimReply {reply} {
    ;## strip timing data
    set idx [string first "=====" $reply]
    if {$idx >= 0} {
        set reply [string trim [string replace $reply $idx end]]
    }

    set state {}
    set msg {}
    foreach line [split $reply "\n"] {
        if {[regexp -- {Subject:} $line]} {
            set state subject
            continue
        }
        if {[string equal "subject" $state]} {
            set state {}
            continue
        }
        append msg "$line\n"
    }

    return [string trim $msg]
}

proc getJobid {sid} {
    if {[eof $sid]} {
        catch {close $sid}
        return -code error "[myName]: got unexpected eof"
    }

    while {[gets $sid line] != -1} {
        if {[regexp -- "(${::RUNCODE}\\d+)" $line -> jobid]} {
            catch {close $sid}
            return $jobid
        }
    }

    catch {close $sid}
    return -code error "[myName]: can't find jobid in reply"
}

;## Valid values for show:
;## 0 - Don't show cmd or reply
;## 1 - Show reply only
;## 2 - Show cmd only
;## 3 - Show cmd and reply
proc sendCmd {cmd {show 0}} {
    set show [expr {int(fmod($show,4))}]

    if {$show >= 2} {
        regsub -- { -password\s+(\S+)} $cmd { -password **** } display_cmd
        puts stderr $display_cmd
    }

    ;## strip comments
    foreach line [split $cmd "\n"] {
        set line [string trim $line]
        if {[regexp {^\s*#} $line]} {
            continue
        }
        lappend tmp $line
    }
    set cmd [join $tmp "\n"]
    regsub -all -- {[\n\s\t]+} $cmd { } cmd

    set sid [socket $::HOST $::PORT]
    puts $sid $cmd
    flush $sid
    set reply [read $sid]
    close $sid

    if {int(fmod($show,2))} {
        puts stderr $reply
    }

    if {[regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid]} {
        return $jobid
    } else {
        return -code error "[myName]: $reply"
    }

    #return {}
}

proc getFrameCache {} {
    set cmd "
        ldasJob {
            -name $::USER
            -password $::PASS
            -email ${::LADDR}:${::LPORT}
        } {
            getFrameCache
                -returnprotocol http://foo
        }
    "

    set jobid [sendCmd $cmd]
    if {![string length $jobid]} {
        return
    }

    vwait ::SID
    if {[catch {getResultURL $::SID} url]} {
        return -code error $url
    }

    set token [getURL $url]
    foreach {dir update time1 time2} [http::data $token] {
        lappend times $time1 $time2
    }
    http::cleanup $token

    set times [lsort $times]
    #return [list [lindex $times 0] [lindex $times end]]

    if {[llength $times] == 2} {
        return $times
    }

    set retval [list]
    set starttime [lindex $times 0]
    set endtime [lindex $times 1]
    while {![string length $starttime] && ![string length $endtime]} {
        set times [lrange $times 2 end]
        if {![llength $times]} {break}
        set starttime [lindex $times 0]
        set endtime [lindex $times 1]
    }
    foreach  {time1 time2} [lrange $times 2 end] {
        if {![string length $time1] && ![string length $time2]} {
            continue
        }
        if {$time1 != ($endtime + 1)} {
            lappend retval $starttime $endtime
            set starttime $time1
        }
        set endtime $time2
    }

    if {$endtime != [lindex $retval end]} {
        lappend retval $starttime $endtime
    }

    return $retval
}

proc getFrameElements {{times now} {query full} {format {ilwd ascii}}} {
    if {[string equal "now" $times]} {
        set times [gpsTime]
    }

    set frame {{}}
    if {![regexp {^\d{9}} $times]} {
        set frame $times
        set times {{}}
    }

    set cmd "
        ldasJob {
            -name $::USER
            -password $::PASS
            -email ${::LADDR}:${::LPORT}
        } {
            getFrameElements
                -returnprotocol http://foo
                -outputformat [list $format]
                -framequery [list "{} {} $frame $times $query"]
        }
    "

    set jobid [sendCmd $cmd]
    if {![string length $jobid]} {
        return
    }

    vwait ::SID
    set url [getResultURL $::SID]
    #if {[catch {getResultURL $::SID} msg]} {
    #    return -code error $msg
    #}

    set token [getURL $url]
    set data [http::data $token]
    http::cleanup $token
    return $data
}

proc getFrameData {{times now} {query full} {format {ilwd ascii}}} {
    if {[string equal "now" $times]} {
        set times [gpsTime]
    }

    set frame {{}}
    if {![regexp {^\d{9}} $times]} {
        set frame $times
        set times {{}}
    }

    set cmd "
        ldasJob {
            -name $::USER
            -password $::PASS
            -email ${::LADDR}:${::LPORT}
        } {
            getFrameData
                -returnprotocol http://foo
                -outputformat [list $format]
                -framequery [list "{} {} $frame $times $query"]
        }
    "

    set jobid [sendCmd $cmd]
    if {![string length $jobid]} {
        return
    }

    vwait ::SID
    set url [getResultURL $::SID]
    #if {[catch {getResultURL $::SID} msg]} {
    #    return -code error $msg
    #}

    set token [getURL $url]
    set data [http::data $token]
    http::cleanup $token
    return $data
}

proc getLwILwd {input {jobid ""} {format ascii}} {
    if {[string length $jobid]} {
        set input "file:/jobs/${::RUNCODE}${jobid}/${input}"
    }

    set cmd "
        ldasJob {
            -name $::USER
            -password $::PASS
            -email ${::LADDR}:${::LPORT}
        } {
            getLwILwd
                -returnprotocol http://foo
                -input $input
                -ilwdformat [list $format]
        }
    "

    set jobid [sendCmd $cmd]
    if {![string length $jobid]} {
        return
    }

    vwait ::SID
    set url [getResultURL $::SID]
    #if {[catch {getResultURL $::SID} msg]} {
    #    return -code error $msg
    #}

    set token [getURL $url]
    set data [http::data $token]
    http::cleanup $token
    return [list $url $data]
}

proc randomFrame {frame1 frame2} {
    if {![string is integer $frame1] || ![string is integer $frame2]} {
        return -code error "[myName]: Error: Arguments must be integer type"
    }
    set min [expr {$frame1 < $frame2 ? $frame1 : $frame2}]
    set range [expr {abs($frame2 - $frame1) + 1}]
    return [expr {int(rand() * $range) + $min}]
}

proc sayit {num} {
    #if in teens allways use 'th'
    if {[expr {($num / 10) % 10}] == 1} {
        return "${num}th"
    }

    switch -exact -- [expr {$num % 10}] {
        1 {return "${num}st"}
        2 {return "${num}nd"}
        3 {return "${num}rd"}
        default {return "${num}th"}
    }
}

## ***********************************************************
##
## Name: gpsTime
##
## Description:
## Stand alone version of the generic API GPS function.
## Calculates equivalent GPS time given UTC time or "now".
##
## Usage:
##
## Comments:
##
##
## ***********************************************************
proc gpsTime {{time "now"}} {
    ;## just return something that already looks like gps time
    if {[regexp {^[6-9]\d{8}$} $time]} {
        return $time
    }

    ;## the difference between the UNIX epoch and GPS epoch.
    set epochdiff 315964800

    ;## the difference between GPS and TAI
    set gpsdiff 19

    ;## 1972-01-01 00:00:00 UTC was 1972-01-01 00:00:10 TAI.
    set offset 10

    ;## assumes 00:00:00.
    set leapdates {
        07/01/1972
        01/01/1973
        01/01/1974
        01/01/1975
        01/01/1976
        01/01/1977
        01/01/1978
        01/01/1979
        01/01/1980
        07/01/1981
        07/01/1982
        07/01/1983
        07/01/1985
        01/01/1988
        01/01/1990
        01/01/1991
        07/01/1992
        07/01/1993
        07/01/1994
        01/01/1996
        07/01/1997
        01/01/1999
    }

    set index [ llength $leapdates ]

    ;## quick short-circuit for "now".
    if {[string equal "now" $time]} {
        set time [clock seconds]
    } else {
        ;## build the lookup table in gmt seconds
        foreach date $leapdates {
            lappend leapsecs [clock scan $date -gmt 1]
        }

        ;## canonicalise input to UNIX epoch seconds
        if {![regexp {^\d{9,10}$} $time]} {
            if {[catch {clock scan $time -gmt 1} time]} {
                return -code error $time
            }
        }

        foreach sec [lsort -integer $leapsecs] {
            if {$time < $sec} {
                set index [lsearch $leapsecs $sec]
                break
            }
        }
    }

    set taidiff [expr {$offset + $index}]
    set gpstime [expr {$time + $taidiff - $epochdiff - $gpsdiff}]

    return $gpstime
}
## ***********************************************************

## ********************************************************
##
## Name: parseILWD
##
## Description:
## Parses an ILWD object and returns a description of it.
##
## Usage:
##       array set tree [ parseILWD $ilwd_string ]
##       foreach branch [ lsort -dictionary [ array names tree ] ] {
##           puts "$branch --> $tree($branch)"
##       }
##
## Comments:
## When a container contains a container, a branch
## branches, so branches need to be "array set" when
## their llength exceeds 2, so when you parse the output
## you will need a recursive handler.  Some time soon

proc parseILWD { {ilwd ""} } {
    ;## really crude, rapid, effective garbage rejection.
    if {![ regexp {^<.+>$} $ilwd ]} {
        return -code error "[myName]: Garbage received."
    }
    set ilwd [ ilwd::preprocess $ilwd ]
    return [ ilwd::parse $ilwd ]
}

proc extractILWD { {ilwd ""} {branch ""} {levels 0} } {
    set data [parseILWD $ilwd]
    return [ilwd::extract $data $branch $levels]
}

## ********************************************************
#------------------------------------------------------------------------
# Proceedure to return the exit status of running a command
#------------------------------------------------------------------------
proc ProgramExitStatus { Command } {
    set Command "$Command > /dev/null 2>&1 ; echo $?"
    exec /bin/sh -c $Command
}

#========================================================================
# Check if the user needs to override any of the defaults
#========================================================================
if { [ file exists "$env(HOME)/.ldas/qaconfig" ] } {
    source "$env(HOME)/.ldas/qaconfig"
}
#========================================================================
# Commands to be carried out immediately
#========================================================================
#------------------------------------------------------------------------
# Must be last package so all options have been specified
#------------------------------------------------------------------------
package require qatest

if { [ info exist ::env(USE_GLOBUS_CHANNEL) ] } {
	set ::USE_GLOBUS_CHANNEL $::env(USE_GLOBUS_CHANNEL)
} else {
    set ::USE_GLOBUS_CHANNEL 0
}

if { [ info exist ::env(USE_GSI) ] } {
	set ::USE_GSI $::env(USE_GSI)
} else {
    set ::USE_GSI 0
}

if [info exists ::env(FRDUMP)] {
    set ::FRDUMP $::env(FRDUMP)
} else {
    set ::FRDUMP framecpp_dump_objects
}

if [info exists ::env(FRDUMPTOC)] {
    set ::FRDUMPTOC $::env(FRDUMPTOC)
} else {
    set ::FRDUMPTOC framecpp_dump_toc
}

if [info exists ::env(FRVERIFY)] {
    set ::FRVERIFY $::env(FRVERIFY)
} else {
    set ::FRVERIFY framecpp_verify
}

if [info exists ::env(FRSAMPLE)] {
    set ::FRSAMPLE $::env(FRSAMPLE)
} else {
    set ::FRSAMPLE framecpp_sample
}

if [info exists ::env(FRQUERY)] {
    set ::FRQUERY $::env(FRQUERY)
} else {
    set ::FRQUERY framecpp_query
}

if [info exists ::env(FRCHECK)] {
    set ::FRCHECK $::env(FRCHECK)
} else {
    set ::FRCHECK [ auto_execok FrCheck ]
}
