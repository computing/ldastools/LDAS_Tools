#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}	

set auto_path "/ldcg/lib/64 /ldcg/lib64 /ldcg/lib $auto_path"
source /ldas_usr/ldas/test/bin/AllTest.rsc

source /ldas_outgoing/cntlmonAPI/cntlmon.state
source /ldas_outgoing/LDASapi.rsc

## ******************************************************** 
##
## Here be resource variables, arrrrggghhh!
##
## ******************************************************** 
set ::MYNAME [file rootname [file tail $::argv0]]
set ::INTERVAL 1000
set ::APIDIR "/ldas_outgoing/diskcacheAPI"
set ::TMPDIR [file join /tmp $::MYNAME]
set ::LOGFILE ${::MYNAME}.log
set ::RSCFILE "LDASdiskcache.rsc"
set ::CACHEFILE ".frame.cache"
set ::SHUTDOWN "sHuTdOwN"
set ::DEBUG 1
set ::EXCLUDE [list /ldas_outgoing/frames/RDSVerify \
/ldas_outgoing/mirror/frames \
/scratch/test/frames /scratch/test/system \
/scratch/mirror/frames \
$::FRAME_TOPDIR /ldas_outgoing/jobs/output/rdsTest ]
			
set ::STARTTIME ""
set ::ENDTIME ""
	  
## ******************************************************** 

## ******************************************************** 
proc bgerror {err} {
    set trace {}
    puts stderr "\nError: $err"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
    }
    exit 1
}
## ******************************************************** 

## ******************************************************** 
proc readrsc {} {
    ;## Work on copy of resource file
    file copy -force -- [file join $::APIDIR $::RSCFILE] $::TMPDIR
	set rscfile [file join $::TMPDIR $::RSCFILE]
    set fid [open $rscfile r]
    set data [read -nonewline $fid]
    close $fid
	set interp [ interp create ]
	$interp eval source $rscfile
	set ::MOUNT_PT [ $interp eval set ::MOUNT_PT ]
	interp delete $interp
    return $data
}
## ******************************************************** 

## ******************************************************** 
proc multiline { singleLine } {

	if	{ [ catch {
		set multiLine [ split $::MOUNT_PT ]
		set multiLine [ string trim [ join $multiLine " \\\n" ] \\\n ]
	} err ] } {
		return -code error $error
	}
	return $multiLine
}
## ******************************************************** 

## ******************************************************** 
proc writersc {data} {

	set rscfile [file join $::TMPDIR $::RSCFILE]
    set fid [open $rscfile w]
	puts $fid $data
    close $fid
		
	# use rename instead of copy to speed up the change
	# so diskcacheAPI would not be reading partial data
	if	{ [ file exist $rscfile ] } {
    	#catch { file copy -force -- $rscfile $::APIDIR } err
		catch { file copy -force -- $rscfile $::RSC_TMPFILE } err 
		catch { file rename -force -- $::RSC_TMPFILE $::RSC_REALFILE }
		if	{ [ string length $err ] } {
			puts $::logfid "Error: copy $rscfile $::APIDIR: $err"
		}
		catch { exec ls -l [ file join $::APIDIR $::RSCFILE ] } err1
		puts $::logfid "rsc updated: $err1"
	} else {
		puts $::logfid "$rscfile does not exist"
	}
    return
}
## ******************************************************** 

## ******************************************************** 
proc mangle {} {

	if	{ [ catch {
    	set data [readrsc]
		set now [ clock format  [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
    	set toremove [expr {int(rand() * ([llength $::MOUNT_PT] - 1)) + 1 - $::IGNORE}]
    	set removed [list]
		set updated 0
		set removed_len 0
		
    	while { $removed_len < $toremove } {
        	set idx [expr {int(rand() * [llength $::MOUNT_PT])}]
#puts $::logfid "$now toremove $toremove idx $idx, length of mount_pt [llength $::MOUNT_PT] "
#flush $::logfid
        	set dir [lindex $::MOUNT_PT $idx]
			if	{ ![ string length [ string trim $dir ] ] } {
				continue
			}
        	if 	{[lsearch -exact $::EXCLUDE $dir] >= 0} {
			puts $::logfid "$dir excluded"
			flush $::logfid
            	;## Skip excluded directories
            	continue
        	}
        	lappend removed $dir
			# puts $::logfid "$now: removed $idx directory $dir from ::MOUNT_PT" 
			# flush $::logfid
        	set ::MOUNT_PT [lreplace $::MOUNT_PT $idx $idx]
			set removed_len [ llength $removed ]
			if	{ $removed_len > $::REMOVE_LIMIT } {
				break
			}
    	}
		puts $::logfid "$now: $removed_len mounts removed '$removed'" 
		flush $::logfid
		set mount_point_output [ multiline $::MOUNT_PT ]		
		set rc [ regsub -- {\n(set\s+::MOUNT_PT\s+[^\]\}]*[\]\}])} $data "\nset ::MOUNT_PT \[ list $mount_point_output \]" data ]
		
		if	{ [ llength $removed ] } {			
			puts $::logfid "new mount points '$mount_point_output'"
			flush $::logfid
    			writersc $data
			after $::INTERVAL [list restore $removed]
		} else {		
    		after $::INTERVAL [list mangle]
		}
	} err ] } {
		puts $::logfid "Error: $err"
		flush $::logfid
	}	
    return
}
## ******************************************************** 

## ******************************************************** 
proc restore {removed} {
   
	if	{ [  llength $removed ] } {
		set data [readrsc]
    	eval lappend ::MOUNT_PT $removed
		set mount_point_output [ multiline $::MOUNT_PT ]	
		set rc [ regsub -- {\n(set\s+::MOUNT_PT\s+[^\]\}]*[\]\}])} $data "\nset ::MOUNT_PT \[ list $mount_point_output \]" data ]
		puts $::logfid "restored [ llength $removed ] mount points '$mount_point_output'"
		flush $::logfid
    	writersc $data
		set now [ clock format  [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
		puts $::logfid "$now: restored $removed"
		flush $::logfid
	}
    after $::INTERVAL [list mangle]
    return
}
## ******************************************************** 

## ******************************************************** 
proc shutdown {} {
    if {![file exists [file join $::TMPDIR $::SHUTDOWN]]} {
        after 1000 [list shutdown]
        return
    }

    foreach id [after info] {
        after cancel $id
    }

    ;## Restore original rsc file
    after 5000
    file copy -force -- [file join $::TMPDIR ${::RSCFILE}.orig] [file join $::APIDIR $::RSCFILE]
	;## to wakeup diskcache
	catch { exec touch [file join $::APIDIR $::RSCFILE] } 
	
    file delete -- [file join $::TMPDIR $::SHUTDOWN]
    set ::forever 1
	
    return
}
## ******************************************************** 

## ******************************************************** 
proc issueShutdown {} {

	set shutdownfile [file join $::TMPDIR $::SHUTDOWN]
    if { ![file exists $shutdownfile ] } {
		catch { exec touch $shutdownfile } 
    }
}

## ******************************************************** 

## ******************************************************** 
proc scheduleStart {} {

	if	{ [ catch { 
	catch { exec crontab -r } err
	# puts "removing existing crontab if any $err"
	set startsecs [ clock scan $::STARTTIME ]
	set start [ clock format  $startsecs -format "%m-%d-%Y %H:%M:%S" ]
	regexp {(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)} $start -> mon day year hour min sec
	set text "# user LDAS run dcmangle at $::STARTTIME\n"
	append text "[ string trimleft $min ] [ string trimleft $hour ] \
		[ string trimleft $day ] [ string trimleft $mon ] * /usr1/ldas/dcmangle.tcl\n"
	
	;## default run time is 24 hours
	if	{ ! [ string length $::ENDTIME ] } {
		set stopsecs [ expr $startsecs + (60*60*24) ]
	} else {
		set stopsecs [ clock scan $::ENDTIME ]
	}
	set stop [ clock format $stopsecs -format "%m-%d-%Y %H:%M:%S" ]
	regexp {(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)} $stop -> mon day year hour min sec
	append text "[ string trimleft $min ] [ string trimleft $hour ] \
		[ string trimleft $day ] [ string trimleft $mon ] * touch /tmp/dcmangle/sHuTdOwN"
		
	set cronfile $::env(HOME)/$::tcl_platform(user)cron
	set fd [ open $cronfile w ]
	puts $fd $text
	close $fd
	catch { exec crontab $cronfile  } err
	puts "dcmangle to run from $start-$stop, submitted crontab $err"
	catch { exec crontab -l } data
	puts "listing crontab\n$data"
	} err ] } {
		puts $err
	}
}

## ********************************************************

## ******************************************************** 
proc printUsage {args} {
    exit 1
}
## ******************************************************** 

## ******************************************************** ##
;### MAIN ###
for {set idx 0} {$idx < $::argc} {incr idx} {
    set opt [lindex $::argv $idx]

    set ival 0
    if {! [regexp {^(--[^=]+)=(.*)$} $opt -> opt val]} {
        set val [lindex $::argv [expr {$idx + 1}]]
        set ival 1
    }

    switch -glob -- $opt {
        -a -
        --api-dir {
            set ::APIDIR $val
            incr idx $ival
        }

        -d -
        --debug {
            set ::DEBUG 1
        }

        -i -
        --interval {
            set ::INTERVAL $val
            incr idx $ival
        }

        -l -
        --logfile {
            set ::LOGFILE $val
            incr idx $ival
        }

        -s -
        --shutdown {
            set ::SHUTDOWN $val
            incr idx $ival
        }

        -t -
        --temp-dir {
            set ::TMPDIR $val
            incr idx $ival
        }

        -x -
        -exclude-list {
            set ::EXCLUDE [concat $val]
            incr idx $ival
        }
		-st -
        -starttime {
            set ::STARTTIME [concat $val]
            incr idx $ival
        }
		-et -
        -endtime {
            set ::ENDTIME [concat $val]
            incr idx $ival
        }

        -* {
            puts stderr "Error: Invalid option: '$opt'"
            printUsage
        }

        default {
            puts stderr "Error: Invalid option: '$opt'"
            printUsage
        }
    }
}

if {$::INTERVAL < 60000} {
    set ::INTERVAL [expr {$::INTERVAL * 1000}]
}

;## Create temp directory
if {![file isdirectory $::TMPDIR]} {
    catch {file rename -- $::TMPDIR ${::TMPDIR}.sav}
    catch {file mkdir $::TMPDIR}
}

if {$::DEBUG} {
    set ::logfid [open [file join $::TMPDIR $::LOGFILE] w]
}

if	{ [ string length $::STARTTIME ] } {
	scheduleStart 
	exit
}

set ::RSC_TMPFILE  [ file join $::APIDIR ${::RSCFILE}.tmp ]
set ::RSC_REALFILE [ file join $::APIDIR $::RSCFILE ]

;## clear out any residual resource file
catch { eval file delete -force [ glob -nocomplain $::TMPDIR/*rsc* ] }
puts "any rsc files remaining [ glob -nocomplain $::TMPDIR/*rsc* ]"

;## Save cache and rsc files
file copy -force -- [file join $::APIDIR $::CACHEFILE] $::TMPDIR
file copy -force -- [file join $::APIDIR $::RSCFILE] [file join $::TMPDIR ${::RSCFILE}.orig]
catch { exec ls -l $::TMPDIR } data

;## Find number of excludes
set ::IGNORE 0
if {[llength $::EXCLUDE]} {
    set data [readrsc]

    foreach dir $::MOUNT_PT {
        if {[lsearch -exact $::EXCLUDE $dir] >= 0} {
            incr ::IGNORE
        }
    }
}

;## set a limit to remove mount points if the mount point list is very large
if	{ [ expr [ llength $::MOUNT_PT ]/10 ] } {
	set ::REMOVE_LIMIT 10
} else {
	set ::REMOVE_LIMIT 0
}
puts "::REMOVE_LIMIT=$::REMOVE_LIMIT"

set rscfile [file join $::APIDIR $::RSCFILE]

;## skip this if on diskcache host
catch { exec uname -n } hostname
if	{ ! [ string equal $::DISKCACHE_API_HOST $hostname ] } {
	catch { exec /usr/bin/env \
    LD_LIBRARY_PATH=$::LDAS/lib:/ldcg/lib \
    PATH=$::LDAS/bin:/ldcg/bin:/bin:/usr/bin \
	$::LDAS/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent \
	--shell=/ldcg/bin/tclsh check} err
	catch { eval $err }
	if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    	puts $::logfid "$err\nneed to setup env(SSH_AUTH_SOCK) for this test"  
	} else {
;## verify mount points exist on diskcacheAPI host

	set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
        \"$::BINDIR/verifymountpts.tcl\""
	#catch { eval $cmd } err
	#puts $::logfid "$cmd\n$err"
	#flush $::logfid
	}
} else {
	catch { exec $::BINDIR/verifymountpts.tcl } err
	puts $::logfid "checking mount points: $err"
	flush $::logfid
}

after 0 [list shutdown]
after 0 [list mangle]
vwait ::forever

if {$::DEBUG} {
    close $::logfid
}

exit 0
## ******************************************************** ##
