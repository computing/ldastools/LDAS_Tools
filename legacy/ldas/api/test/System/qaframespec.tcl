package provide QAFrameSpec 1.0

namespace eval QA::FrameSpec {
    variable CURRENT_SPEC

    set CURRENT_SPEC Version6

    namespace eval Version6 {
	set structList {
	    Event
	    FrameH
	    AdcData
	    Detector
	    History
	    Msg
	    ProcData
	    SerData
	    SimData
	    SimEvent
	    StatData
	    Summary
	}
	
	array set FrameH {
	    name        lstring
	    run         int_4s
	    frame       int_4u
	    dataQuality int_4u
	    GTimeS      int_4u
	    GTimeN      int_4u
	    ULeapS      int_2u
	    dt          real_8
	}

	array set AdcData {
	    name          lstring
	    comment       lstring
	    channelGroup  int_4u
	    channelNumber int_4u
	    nBits         int_4u
	    bias          real_4
	    slope         real_4
	    units         lstring
	    sampleRate    real_8
	    timeOffset    real_8
	    fShift        real_8
	    phase         real_4
	    dataValid     int_2u
	}

	array set Detector {
	    name         lstring
	    prefix       char
	    longitude    real_8
	    latitude     real_8
	    elevation    real_4
	    armXazimuth  real_4
	    armYazimuth  real_4
	    armXaltitude real_4
	    armYaltitude real_4
	    armXmidpoint real_4
	    armYmidpoint real_4
	    localTime    int_4s
	}

	array set Event {
	    name           lstring
	    comment        lstring
	    inputs         lstring
	    GTimeS         int_4u
	    GTimeN         int_4u
	    timeBefore     real_4
	    timeAfter      real_4
	    eventStatus    int_4u
	    amplitude      real_4
	    probability    real_4
	    statistics     lstring
	    nParam         int_2u
	    parameters     real_4
	    parameterNames lstring
	}

	array set History {
	    name    lstring
	    time    int_4u
	    comment lstring
	}

	array set Msg {
	    alarm    lstring
	    message  lstring
	    severity int_4u
	    GTimeS   int_4u
	    GTimeN   int_4u
	}

	array set ProcData {
	    name          lstring
	    comment       lstring
	    type          int_2u
	    subType       int_2u
	    timeOffset    real_8
	    tRange        real_8
	    fShift        real_8
	    phase         real_4
	    fRange        real_8
	    BW            real_8
	    nAuxParam     int_2u
	    auxParam      real_8
	    auxParamNames lstring
	}

	array set SerData {
	    name       lstring
	    timeSec    int_4u
	    timeNsec   int_4u
	    sampleRate real_4
	    data       lstring
	}

	array set SimData {
	    name       lstring
	    comment    lstring
	    sampleRate real_4
	    timeOffset real_8
	    fShift     real_8
	    phase      real_4
	}

	array set SimEvent {
	    name           lstring
	    comment        lstring
	    inputs         lstring
	    GTimeS         int_4u
	    GTimeN         int_4u
	    timeBefore     real_4
	    timeAfter      real_4
	    amplitude      real_4
	    nParam         int_2u
	    parameters     real_4
	    parameterNames lstring
	}

	array set StatData {
	    name           lstring
	    comment        lstring
	    representation lstring
	    timeStart      int_4u
	    timeEnd        int_4u
	    version        int_4u
	}

	array set Summary {
	    name    lstring
	    comment lstring
	    test    lstring
	    GTimeS  int_4u
	    GTimeN  int_4u
	}

	set exceptions {
	    Event:GTimeS
	    Event:GTimeN
	    FrameH:GTimeS
	    FrameH:GTimeN
	    Msg:GTimeS
	    Msg:GTimeN
	    SerData:timeSec
	    SerData:timeNsec
	    SimEvent:GTimeS
	    SimEvent:GTimeN
	    Summary:GTimeS
	    Summary:GTimeN
	}

	set nested {
	    Detector
	    History
	}

	namespace export Init
	proc Init { Namespace } {
	    ::QA::ImportVariables [namespace current] $Namespace
	    ::QA::ImportVariables [namespace parent] $Namespace
	}
    } ;# namespace - Version6

    namespace eval Version5 {
	set structList {
	    FrameH
	    AdcData
	    Detector
	    Event
	    History
	    Msg
	    ProcData
	    SerData
	    SimData
	    SimEvent
	    StatData
	    Summary
	}

	array set FrameH {
	    name        lstring
	    run         int_4s
	    frame       int_4u
	    dataQuality int_4u
	    GTimeS      int_4u
	    GTimeN      int_4u
	    ULeapS      int_2u
	    dt          real_8
	}

	array set AdcData {
	    name          lstring
	    comment       lstring
	    channelGroup  int_4u
	    channelNumber int_4u
	    nBits         int_4u
	    bias          real_4
	    slope         real_4
	    units         lstring
	    sampleRate    real_8
	    timeOffsetS   int_4s
	    timeOffsetN   int_4u
	    fShift        real_8
	    phase         real_4
	    dataValid     int_2u
	}

	array set Detector {
	    name         lstring
	    longitude    real_8
	    latitude     real_8
	    elevation    real_4
	    armXazimuth  real_4
	    armYazimuth  real_4
	    armXaltitude real_4
	    armYaltitude real_4
	    armXmidpoint real_4
	    armYmidpoint real_4
	    localTime    int_4s
	    dataQuality  int_4u
	    qaBitList    lstring
	}

	array set Event {
	    name        lstring
	    comment     lstring
	    inputs      lstring
	    GTimeS      int_4u
	    GTimeN      int_4u
	    timeBefore  real_4
	    timeAfter   real_4
	    eventStatus int_4u
	    amplitude   real_4
	    probability real_4
	    statistics  lstring
	    nParam      int_2u
	}

	array set History {
	    name    lstring
	    time    int_4u
	    comment lstring
	}

	array set Msg {
	    alarm    lstring
	    message  lstring
	    severity int_4u
	    GTimeS   int_4u
	    GTimeN   int_4u
	}

	array set ProcData {
	    name        lstring
	    comment     lstring
	    sampleRate  real_8
	    timeOffsetS int_4u
	    timeOffsetN int_4u
	    fShift      real_8
	    phase       real_4
	}

	array set SerData {
	    name       lstring
	    timeSec    int_4u
	    timeNsec   int_4u
	    sampleRate real_4
	    data       lstring
	}

	array set SimData {
	    name       lstring
	    comment    lstring
	    sampleRate real_4
	    fShift     real_8
	    phase      real_4
	}

	array set SimEvent {
	    name       lstring
	    comment    lstring
	    inputs     lstring
	    GTimeS     int_4u
	    GTimeN     int_4u
	    timeBefore real_4
	    timeAfter  real_4
	    amplitude  real_4
	}

	array set StatData {
	    name           lstring
	    comment        lstring
	    representation lstring
	    timeStart      int_4u
	    timeEnd        int_4u
	    version        int_4u
	}

	array set Summary {
	    name    lstring
	    comment lstring
	    test    lstring
	    GTimeS  int_4u
	    GTimeN  int_4u
	}

	#array set TrigData {
	#    name          lstring
	#    comment       lstring
	#    inputs        lstring
	#    GTimeS        int_4u
	#    GTimeN        int_4u
	#    timeBefore    real_4
	#    timeAfter     real_4
	#    triggerStatus int_4u
	#    amplitude     real_4
	#    probability   real_4
	#    statistics    real_4
	#}

	set exceptions {
	    Event:GTimeS
	    Event:GTimeN
	    FrameH:GTimeS
	    FrameH:GTimeN
	    Msg:GTimeS
	    Msg:GTimeN
	    SimEvent:GTimeS
	    SimEvent:GTimeN
	    SerData:timeSec
	    SerData:timeNsec
	    Summary:GTimeS
	    Summary:GTimeN
	    ProcData:timeOffsetS
	    ProcData:timeOffsetN
	}

	set nested {
	    Detector
	    History
	}
	namespace export Init
	proc Init { Namespace } {
	    ::QA::ImportVariables [namespace current] $Namespace
	    ::QA::ImportVariables [namespace parent] $Namespace
	}
    } ;# namespace - Version5
}
