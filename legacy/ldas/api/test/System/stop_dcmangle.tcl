#!/bin/sh
# \
exec tclsh "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc

;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

if	{ [ string equal Linux $::tcl_platform(os) ] } {
	catch { exec /bin/touch /tmp/dcmangle/sHuTdOwN }
} else {
	catch { exec /ldcg/bin/touch /tmp/dcmangle/sHuTdOwN }
}

;## do not use LD_PRELOAD_64 on an opteron
if	{ [ string equal i86pc $::tcl_platform(machine) ] } {
	catch { unset ::env(LD_PRELOAD_64) }
}

if	{ [ file exist $::DCMANGLE_CACHE ] } {
	;## a recent dcmangle cache
	catch { exec /ldas/bin/cacheDump $::DCMANGLE_CACHE > $::WORKDIR/cache.dcmangle } err1 
	catch { exec /ldas/bin/cacheDump > $::WORKDIR/cache.end } err2 
	# catch { exec tkdiff  ./cache.dcmangle ./cache.end & } 
	# puts "Cache difference dcmangle\n$data"
}
