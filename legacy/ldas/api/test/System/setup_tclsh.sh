#========================================================================
# This is a helper script intended to choose which tclsh to use.
#========================================================================
TCLSH=""
WISH=""
LIGOTOOLS_OPT="`echo \"$*\" | sed -e 's/^.*\(--with\(out\)*-ligotools\(=[^ \t]*\)*\).*$/\1/g'`"
case x$LIGOTOOLS_OPT in
*=*)
  LIGOTOOLS="`echo $LIGOTOOLS_OPT | sed -e 's/^[^=]*=//g'`"
  LIGOTOOLS_OPT="`echo $LIGOTOOLS_OPT | sed -e 's/=.*$//g'`"
  ;;
x)
  ;;
*)
  ;;
esac
case x$LIGOTOOLS_OPT in
x--with-ligotools)
  if test -z "${LIGOTOOLS}" -a -d "/ligotools"
  then
    LIGOTOOLS="/ligotools"
  fi
  use_ligotools="$LIGOTOOLS/bin/use_ligotools"
  test -x $use_ligotools && eval `$use_ligotools`
  ;;
x--without-ligotools)
  LIGOTOOLS=""
  ;;
*)
  LIGOTOOLS=""
  ;;
esac
case x$LIGOTOOLS in
x)
  ;;
*)
  if test -x ${LIGOTOOLS}/bin/tclshexe
  then
    TCLSH="${LIGOTOOLS}/bin/tclshexe"
  fi
  if test -x ${LIGOTOOLS}/bin/wishexe
  then
    WISH="${LIGOTOOLS}/bin/wishexe"
  fi
  ;;
esac
case x$TCLSH in
x)
  TCLSH="`head -1 /ldas/bin/managerAPI | sed -e 's/#!\s*//g'`"
  ;;
esac
case x$WISH in
x)
  WISH="`echo $TCLSH | sed -e 's/tclsh/wish/g'`"
  ;;
esac
echo TCLSH: $TCLSH WISH: $WISH LIGOTOOLS: $LIGOTOOLS argc: $# args: $*
#------------------------------------------------------------------------
# Make sure certain things are in the path
#------------------------------------------------------------------------
case "$PATH" in
*/ldas/bin*)
  ;;
*)
  PATH="/ldas/bin:${PATH}"; export PATH
  LD_LIBRARY_PATH="/ldas/lib:${LD_LIBRARY_PATH}"; export LD_LIBRARY_PATH
  ;;
esac
