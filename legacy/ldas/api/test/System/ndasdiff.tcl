#!/ldcg/bin/tclsh
#!/bin/sh
# \
exec tclsh "$0" ${1+"$@"}

;# Read diff file
set fid [open [lindex $argv 0] r]
set raw [split [read -nonewline $fid] "\n"]
close $fid

;# Collect data
set section 0
set line 0
set age new
foreach l $raw {
    if {[regexp {(^\d+(,\d+)?c\d+(,\d+$)?)|(^---$)} $l]} {
        ;# Store # of lines for previous segment
        set data($section,$age) $line

        if {[regexp {^---$} $l]} {
            set age old
        } else {
            incr section
            set age new
        }

        set line 0
        continue
    }

    incr line
    set data($section,$age,$line) [split [string trimleft $l "<> "] ","]
}

;# Compare values
;# Only compare first 2 sections
;# as the rest will always differ
set sect 1
set max 0
set save1 0
set save2 0
for {set line 1} {$line <= $data($sect,new)} {incr line} {
    foreach val1 $data($sect,new,$line) val2 $data($sect,old,$line) {
        set diff [expr {abs($val1 - $val2)}]
        set avg  [expr {(abs($val1) + abs($val2)) / 2}]
        set diff [expr {$diff / $avg}]
        if {$diff > $max} {
            set max $diff
            set save1 $val1
            set save2 $val2
        }
    }
}
puts "Largest Error 1: [format {%e} $max] ($save1 $save2)"

set sect 2
set max 0
set save1 0
set save2 0
for {set line 1} {$line <= $data($sect,new)} {incr line} {

    set val1 [lindex $data($sect,new,$line) end]
    if {![string length $val1]} {
        set val1 [lindex $data($sect,new,$line) end-1]
    }

    set val2 [lindex $data($sect,old,$line) end]
    if {![string length $val2]} {
        set val2 [lindex $data($sect,old,$line) end-1]
    }

    set diff [expr {abs($val1 - $val2)}]
    set avg  [expr {(abs($val1) + abs($val2)) / 2}]
    set diff [expr {$diff / $avg}]
    if {$diff > $max} {
        set max $diff
        set save1 $val1
        set save2 $val2
    }
}
puts "Largest Error 2: [format {%e} $max] ($save1 $save2)"

