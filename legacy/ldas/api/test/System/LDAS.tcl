package provide LDAS 1.0

namespace eval LDAS {
    variable Info

    set ldas_header_filename "/ldas/include/ldas/ldasconfig.hh"
    ;##==================================================================
    ;## Read the header file descibing how LDAS was compiled
    ;##==================================================================
    if { ! [catch {set fd [open $ldas_header_filename r]} err] } {
        while {[gets $fd line] >= 0} {
	    if { ! [regexp {^\#define} $line]} {
		continue
	    }
	    regsub -- {^(\#define)\s+((LDAS_)?(\S+))\s+[\"](.*)[\"]$} $line {set Info(\4) {\5}} line
	    ;##==========================================================
	    ;## Put the information into the array for use by other tcl
	    ;##   applications
	    ;##==========================================================
	    eval $line
	}
	close $fd
    }
} ;## namespace LDAS
