#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::wrapperState::test {}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::wrapperState::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
    
    proc runTest {} {
		uplevel {
			set cmd [ subst $cmdstem ]
			Puts 1 "cmd $cmd"
			test wrapperState:dataPipeline:"$expected"  {} -body {
				if 	{[catch {SubmitJob job $cmd} err]} {
					LJdelete job
					Puts 1 "Error $err"
	    			return $err
				}
				set reply $job(jobReply)
                set state $job(jobnum)
				LJdelete job
				Puts 1 "got $reply\nexpected $expected"
				return $reply
    		} -match regexp -result $expected
		}
	}
    
    set cmdstem {
    dataPipeline
        -subject {state test: $expected}
        -dynlib {/ldas/lib/wrapperAPI/libstate.so}
        -filterparams {(0)}
        -datacondtarget wrapper
        #-multidimdatatarget ligolw
        -state {$state}
        -np 2
        -framequery {R H {} 730524000 Adc(H1:LSC-AS_Q)}
        -aliases {gw=H1\\:LSC-AS_Q::AdcData;}
        -algorithms {
            rgw = resample(gw, 1, 2048);
            output(rgw,_,wrapper,rgw,resampled data);
        }
	}
	set expected "State.ilwd"
    set state ""
    runTest
    	
    set expected "produced no products"
    runTest
       
    flush [outputChannel]
    
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests    

};## namespace - ::QA::wrapperState::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::wrapperState::test
