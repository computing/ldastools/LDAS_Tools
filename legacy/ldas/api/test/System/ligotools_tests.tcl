#!/usr/bin/env tclshexe
#---------------------------------------------
# Tests of functionality required by LIGOtools
# Exits with code 0 if all tests pass
# Written 5 Aug 2002 by Peter Shawhan
#---------------------------------------------

package require ldasjob
set script [file tail [info script]]
set retcode 0

#-- Make sure the user specified the manager
if { ! [info exists env(LDASMANAGER)] || [llength $argv] != 0 } {
    puts "Usage:  $script -manager <manager>"
    puts " e.g.:  $script -manager dev"
    exit -1
}

#=====================================================================
# Test behavior of 'null' user command'

puts "\n------ Testing behavior of 'null' user command"

LJrun job null

#-- Check if there was a problem before even trying to submit the job
if { ! [info exists job(jobInfo)] } {
    puts "Error: $job(error)"
    puts "\nABORTING TEST SCRIPT\n"; exit -1
}

#-- OK, we attempted to submit the job.  Check whether the "error message"
#-- that came back is what we expect.
if { [regexp {^.{1,2}-name .+ -password \*+ } $job(jobInfo)] } {
    puts "OK"
} else {
    #-- If we get here, the response is not what it should have been
    puts "Error: Response from LDAS is not what it should have been.  Response was:"
    puts $job(jobInfo)
    set retcode 1
}

#=====================================================================
# All tests done

puts "\n------ Summary"

if { $retcode == 0 } {
    puts "All tests passed"
} else {
    puts "**** One or more tests failed ****"
}

puts ""
exit $retcode
