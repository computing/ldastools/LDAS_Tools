#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib .
package require qalib
package require qatest
package require LDASJob

qaInit

set ::tcltest::debug 1
set ::setsingle "1"

#set ::format "ilwd ascii"
#set ::dctarget "datacond"

set ::format frame
set ::dctarget "frame"

#set ::times 693960000-693960031 ;## E7 Frames
set ::times 693960000-693960031 ;## E7 Frames

set ChannelListLHO {
    H1:LSC-AS_Q
    H1:LSC-AS_I
    H1:LSC-REFL_Q
    H1:LSC-REFL_I
    H1:LSC-POB_Q
    H1:LSC-POB_I
    H2:LSC-AS_Q
    H2:LSC-AS_I
    H2:LSC-REFL_Q
    H2:LSC-REFL_I
    H2:LSC-POB_Q
    H2:LSC-POB_I
    H1:LSC-AS_DC
    H1:LSC-REFL_DC
    H2:LSC-AS_DC
    H2:LSC-REFL_DC
    H1:LSC-DARM_CTRL
    H2:LSC-DARM_CTRL
    H2:ASC-WFS1_QY
    H2:ASC-WFS1_QP
    H2:ASC-ETMX_Y
    H2:ASC-ETMX_P
    H2:ASC-ETMY_Y
    H2:ASC-ETMY_P
    H1:ASC-QPDX_DC
    H1:ASC-QPDY_DC
    H2:ASC-QPDX_DC
    H2:ASC-QPDY_DC
    H1:IOO-MC_F
    H2:IOO-MC_F
    H2:PSL-FSS_RCTRANSPD_F
    H2:PSL-PMC_TRANSPD_F
    H0:PEM-LVEA_SEISX
    H0:PEM-LVEA_SEISY
    H0:PEM-LVEA_SEISZ
    H0:PEM-MX_SEISX
    H0:PEM-MX_SEISZ
    H0:PEM-MY_SEISY
    H0:PEM-MY_SEISZ
    H0:PEM-EX_SEISX
    H0:PEM-EX_SEISZ
    H0:PEM-EY_SEISY
    H0:PEM-EY_SEISZ
    H0:PEM-PSL2_ACCX
    H0:PEM-BSC4_ACCX
    H0:PEM-BSC4_ACCY
    H0:PEM-BSC5_ACCX
    H0:PEM-BSC6_ACCY
    H0:PEM-BSC7_ACCX
    H0:PEM-BSC7_ACCY
    H0:PEM-BSC8_ACCX
    H0:PEM-BSC8_ACCY
    H0:PEM-BSC9_ACC1X
    H0:PEM-BSC10_ACC1Y
    H0:PEM-HAM9_ACCX
    H0:PEM-PSL2_MIC
    H2:PSL-ISS_MC1_F
    H0:PEM-BSC5_MIC
    H0:PEM-BSC6_MIC
    H0:PEM-BSC9_MIC
    H0:PEM-BSC10_MIC
    H0:PEM-BSC1_MAG1Y
    H0:PEM-BSC9_MAGX
    H0:PEM-BSC10_MAGY
    H0:PEM-COIL_MAGZ
    H0:PEM-LVEA2_V1
    H0:PEM-LVEA2_V3
    H0:PEM-MX_V1
    H0:PEM-MY_V2
    H0:PEM-EX_V1
    H0:PEM-EY_V2
    H0:PEM-LVEA_BPO5
    H0:PEM-LVEA_RAIN
    H0:PEM-LVEA_TEMPO5
    H0:PEM-LVEA_WDIR
    H0:PEM-LVEA_WIND
    H1:PSL-126MOPA_DMON
    H1:PSL-126MOPA_DTEC
    H1:PSL-126MOPA_DTMP
    H1:PSL-126MOPA_HTEMP
    H1:PSL-126MOPA_LMON
    H1:PSL-126MOPA_LTEC
    H1:PSL-126MOPA_LTMP
    H1:PSL-FSS_FAST
    H1:PSL-FSS_RCTEMP
    H1:PSL-FSS_RMTEMP
    H2:PSL-126MOPA_DMON
    H2:PSL-126MOPA_DTEC
    H2:PSL-126MOPA_DTMP
    H2:PSL-126MOPA_HTEMP
    H2:PSL-126MOPA_LMON
    H2:PSL-126MOPA_LTEC
    H2:PSL-126MOPA_LTMP
    H2:PSL-FSS_FAST
    H2:PSL-FSS_RCTEMP
    H2:PSL-FSS_RMTEMP
    H1:ASC-ETMX_QPDXPitch
    H1:ASC-ETMX_QPDXYaw
    H2:ASC-ETMX_OptLevPitch
    H2:ASC-ETMX_OptLevYaw
    H2:ASC-ETMY_OptLevPitch
    H2:ASC-ETMY_OptLevYaw
    H2:ASC-ITMX_OptLevPitch
    H2:ASC-ITMX_OptLevYaw
    H2:ASC-ITMY_OptLevPitch
    H2:ASC-ITMY_OptLevYaw
}

set ChannelListLLO {
    L1:LSC-AS_Q
    L1:LSC-AS_I
    L1:LSC-REFL_Q
    L1:LSC-REFL_I
    L1:LSC-POB_Q
    L1:LSC-POB_I
    L1:LSC-AS_DC
    L1:LSC-REFL_DC
    L1:LSC-DARM_CTRL
    L1:ASC-QPDX_DC
    L1:ASC-QPDY_DC
    L1:ASC-ITMX_OPLEV_YERROR
    L1:ASC-ITMX_OPLEV_PERROR
    L1:ASC-ITMY_OPLEV_YERROR
    L1:ASC-ITMY_OPLEV_PERROR
    L1:ASC-ETMX_OPLEV_YERROR
    L1:ASC-ETMX_OPLEV_PERROR
    L1:ASC-ETMY_OPLEV_YERROR
    L1:ASC-ETMY_OPLEV_PERROR
    L1:ASC-BS_OPLEV_YERROR
    L1:ASC-BS_OPLEV_PERROR
    L1:ASC-RM_OPLEV_YERROR
    L1:ASC-RM_OPLEV_PERROR
    L1:ASC-MMT3_OPLEV_YERROR
    L1:ASC-MMT3_OPLEV_PERROR
    L1:IOO-MC_F
    L1:PSL-FSS_RCTRANSPD_F
    L1:PSL-PMC_TRANSPD_F
    L0:PEM-LVEA_SEISX
    L0:PEM-LVEA_SEISY
    L0:PEM-LVEA_SEISZ
    L0:PEM-EX_SEISX
    L0:PEM-EX_SEISZ
    L0:PEM-EY_SEISY
    L0:PEM-EY_SEISZ
    L0:PEM-PSL1_ACCX
    L0:PEM-BSC1_ACCY
    L0:PEM-BSC2_ACCX
    L0:PEM-BSC2_ACCY
    L0:PEM-BSC3_ACCX
    L0:PEM-BSC4_ACCX
    L0:PEM-BSC5_ACCX
    L0:PEM-HAM3_ACCX
    L0:PEM-PSL1_MIC
    L0:PEM-BSC2_MIC
    L0:PEM-BSC5_MIC
    L0:PEM-LVEA_MAG1X
    L0:PEM-LVEA_MAG1Y
    L0:PEM-LVEA_MAG1Z
    L0:PEM-EX_MAG1X
    L0:PEM-EY_MAG1X
    L1:GDS-EY_TO2
    L0:PEM-LVEA_V1
    L0:PEM-LVEA_V3
    L0:PEM-EX_V1
    L0:PEM-EX_V2
    L0:PEM-LVEA_AirTmp
    L0:PEM-LVEA_BP5
    L0:PEM-LVEA_Wind
    L0:PEM-LVEA_WDir
    L0:PEM-LVEA_Rain
    L1:PSL-FSS_FAST
    L1:PSL-FSS_RMTEMP
    L1:PSL-FSS_RCTEMP
    L1:PSL-126MOPA_LMON
    L1:PSL-126MOPA_DMON
    L1:PSL-126MOPA_HTEMP
    L1:ASC-ETMX_OptLevPitch
    L1:ASC-ETMX_OptLevYaw
    L1:ASC-ETMY_OptLevPitch
    L1:ASC-ETMY_OptLevYaw
    L1:ASC-ITMX_OptLevPitch
    L1:ASC-ITMX_OptLevYaw
    L1:ASC-ITMY_OptLevPitch
    L1:ASC-ITMY_OptLevYaw
}

proc buildAliasAdc {channels {suffix ""}} {
    set aliasList [list]
    upvar $channels chanList

    foreach chan $chanList {
        regsub {:} $chan {\:} echan
        lappend aliasList "${chan}${suffix}=${echan}::AdcData;"
    }

    return [join $aliasList "\n"]
}

proc buildAliasProc {channels {suffix ""} {p 2} } {
    set aliasList [list]
    upvar $channels chanList

    foreach chan $chanList {
        regsub {:} $chan {\:} echan
        lappend aliasList "${chan}${suffix}=resample(${echan},1,${p})::ProcData;"
    }

    return [join $aliasList "\n"]
}

proc buildAlgorithm {channels {action ""} {q 16}} {
    set algorithm [list]
    upvar $channels chanList

    foreach chan $chanList {
        switch -exact -- $action {
            resample {
                #lappend algorithm "size = size($chan);"
                #lappend algorithm "output(size,_,_,size,size of $chan);"
                lappend algorithm "rch = resample(${chan}, 1, $q);"
                lappend algorithm "output(rch,_,_,rch,resampled channel ${chan});"
                #lappend algorithm "clear(rch);"
            }

            psd {
            }

            stat {
                lappend algorithm "size = size(${chan}_r);"
                lappend algorithm "output(size,_,_,size,Size of frame resampled ${chan});"
                lappend algorithm "min = min(${chan}_r);"
                lappend algorithm "output(min,_,_,min,Min of frame resampled ${chan});"
                lappend algorithm "max = max(${chan}_r);"
                lappend algorithm "output(max,_,_,max,Max of frame resampled ${chan});"
                lappend algorithm "mean = mean(${chan}_r);"
                lappend algorithm "output(mean,_,_,mean,Mean of frame resampled ${chan});"
                lappend algorithm "rms = rms(${chan}_r);"
                lappend algorithm "output(rms,_,_,rms,RMS of frame resampled ${chan});"

                lappend algorithm "rch = resample(${chan}, 1, $q);"
                lappend algorithm "diff = sub(rch, ${chan}_r);"

                lappend algorithm "size = size(rch);"
                lappend algorithm "output(size,_,_,size,Size of dc resampled ${chan});"
                lappend algorithm "min = min(rch);"
                lappend algorithm "output(min,_,_,min,Min of dc resampled ${chan});"
                lappend algorithm "max = max(rch);"
                lappend algorithm "output(max,_,_,max,Max of dc resampled ${chan});"
                lappend algorithm "mean = mean(rch);"
                lappend algorithm "output(mean,_,_,mean,Mean of dc resampled ${chan});"
                lappend algorithm "rms = rms(rch);"
                lappend algorithm "output(rms,_,_,rms,RMS of dc resampled ${chan});"

                lappend algorithm "abs = abs(diff);"
                lappend algorithm "min = min(abs);"
                lappend algorithm "output(min,_,_,min,Min of diff ${chan});"
                lappend algorithm "max = max(abs);"
                lappend algorithm "output(max,_,_,max,Max of diff ${chan});"
                lappend algorithm "mean = mean(diff);"
                lappend algorithm "output(mean,_,_,mean,Mean of diff ${chan});"
                lappend algorithm "rms = rms(diff);"
                lappend algorithm "output(rms,_,_,rms,RMS of diff ${chan});"
            }

            some {
                set channum [lsearch -exact $chanList $chan]
                if {$channum % 2} {
                    lappend algorithm "rch = resample(${chan}, 1, $q);"
                    lappend algorithm "output(rch,_,_,rch,resampled channel ${chan});"
                    #lappend algorithm "clear(rch);"
                } else {
                    lappend algorithm "output(${chan},_,_,${chan},channel ${chan});"
                    #lappend algorithm "clear(${chan});"
                }
            }

            default {
                lappend algorithm "output(${chan},_,_,${chan},channel ${chan});"
                #lappend algorithm "clear(${chan});"
            }
        }
    }

    return [join $algorithm "\n"]
}

set ::testScript {
    catch {LJrun job -nowait -manager $::SITE $::cmd}
    if {$LJerror} {
        set errmsg $::job(error)
        LJdelete ::job
        error "Error from LJrun: $errmsg"
    }

    #::tcltest::DebugPuts 1 $::job(jobid)
    puts $::job(jobid)
    catch {LJwait ::job}
    if {$LJerror} {
        set errmsg [trimReply $::job(error)]
        LJdelete ::job
        error "Error from LJrun: $errmsg"
    }

    set ::urlList $::job(outputs)
    LJdelete ::job
    foreach url $::urlList {
        #::tcltest::DebugPuts 1 $url
        puts $url
        checkURL $url
    }

    list
}

set ::cmd {
    concatFrameData
        -outputformat { $::format }
        -returnprotocol { http://reduced }
        -framequery { R $::ifo {} $::times Adc($::channels) }
}

set ::vcmd {
    conditionData
        -outputformat {ilwd ascii}
        -aliases { $::alias }
        -algorithms { $::algorithm }
        -framequery { 
            { R $::ifo {} $::times Adc($::adcChannels) }
            { {} {} {$::frameurls} {} Proc($::procChannels) }
        }
}

# FrameAPI without resample
foreach ifo3 {LHO LLO} {
    set ::ifo [string index $ifo3 1]
    set ::channels "1-[eval llength \$::ChannelList$ifo3]"
    ::tcltest::test "1:frameAPI:without resample:$ifo3 ${::channels} by index" {} $::testScript {}
    set ::channels [eval join \$::ChannelList$ifo3 ","]
    ::tcltest::test "1:frameAPI:without resample:$ifo3 channels by name" {} $::testScript {}
}

# FrameAPI with resample
set urlList {}
foreach ifo3 {LHO LLO} {
    set ::ifo [string index $ifo3 1]
    set ::channels ""
    set first 1
    set last [eval llength \$::ChannelList$ifo3]
    for {set idx $first} {$idx <= $last} {incr idx} {
        append ::channels "${idx}!resample!2!,"
    }
    set ::channels [string trim $::channels ","]
    ::tcltest::test "2:frameAPI:with resample:$ifo3 $first-$last by index" {} $::testScript {}

    set ::channels ""
    foreach chan [eval concat \$::ChannelList$ifo3] {
        append ::channels "${chan}!resample!2!,"
    }
    set ::channels [string trim $::channels ","]
    ::tcltest::test "2:frameAPI:with resample:$ifo3 channels by name" {} $::testScript {}

    ;## Verify frame from ::urlList
    set save $::cmd
    set ::cmd $::vcmd
    set ::frameurls [join $::urlList]
    set ::adcChannels [eval join \$::ChannelList$ifo3 ","]
    set ::procChannels "0-[expr {[eval llength \$::ChannelList$ifo3] - 1}]"
    set ::alias [buildAliasAdc ChannelList$ifo3]
    append ::alias [buildAliasProc ChannelList$ifo3 _r 2]
    set ::algorithm [buildAlgorithm ChannelList$ifo3 stat 2]
    ::tcltest::test "2:datacondAPI:Verify resample of $ifo3 channels" {} $::testScript {}

    set ::cmd $save
}

#set ::cmd {
#    dataPipeline
#        -datacondtarget {$::dctarget}
#        -setsingledc {$::setsingle}
#        -aliases {$::alias}
#        -algorithms {$::algorithm}
#        -framequery { R $::ifo {} $::times Adc($::channels) }
#}

set ::cmd {
    conditionData
        -datacondtarget {$::dctarget}
        -setsingledc {$::setsingle}
        -aliases {$::alias}
        -algorithms {$::algorithm}
        -framequery { R $::ifo {} $::times Adc($::channels) }
}

# datacondAPI all/some resampled
foreach ifo3 {LHO LLO} {
    set ::ifo [string index $ifo3 1]
    set ::alias [buildAliasAdc ChannelList$ifo3]
    set ::channels [eval join \$::ChannelList$ifo3 ","]

    set ::algorithm [buildAlgorithm ChannelList$ifo3 resample]
    ::tcltest::test "3:dataconcAPI:all resampled:$ifo3 channels by name" {} $::testScript {}

    set ::algorithm [buildAlgorithm ChannelList$ifo3 some]
    ::tcltest::test "3:dataconcAPI:some resampled:$ifo3 channels by name" {} $::testScript {}
}

