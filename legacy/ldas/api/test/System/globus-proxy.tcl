#! /bin/sh
## The next line tells sh to execute the script using tclsh \
exec tclsh "$0" "$@"

;## ************ THIS PACKAGE ASKS FOR THE PASSPHRASE FROM TERMINAL SO
;## IT HAS TO BE RUN IN THE FOREGROUND 

set packages [ package names ]

if	{ [ regexp -nocase Tk $packages ] } {
	set ::WISH 1
} else {
	set ::WISH 0
}

proc debugPuts { msg } {
	
	if	{ $::DEBUG } {	
		puts $msg
    } 
}

namespace eval RawGlobusProxy { 
	set gridprogs [ list grid-proxy-info grid-proxy-init grid-proxy-destroy ]
    set info ".info"
}

## ******************************************************** 
##
## Name: initProxy
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc RawGlobusProxy::initProxy {} {

	set data ""
	if	{ [ string length $::passphrase ] } {
		set fd [ ::open $::Globus::info w ]
        puts $fd $::passphrase
        ::close $fd 
        set cmd "exec grid-proxy-init -debug -verify -pwstdin < $::RawGlobusProxy::info"
        catch { eval $cmd } data
        debugPuts "grid-proxy-init data '$data'"
        file delete -force $::Globus::info
    	if	{ [ regexp -nocase {ERROR:} $data ] } {
        	return -code error $data
        } else {
        	return $data
        }
    } else {
    	return -code error "No pass phrase was entered to create a valid proxy."
    }
}

## ******************************************************** 
##
## Name: initProxyDialog
##
## Description:
## dialog to get passphrase to allow user to initialize a new proxy
##
## Usage:
##
## Comments:

if	{ $::WISH } {

proc RawGlobusProxy::initProxyDialog {} {      

	if	{ [ catch {
    	set ::passphrase ""
        catch { unset ::doinitProxy }
		set dlg [ Dialog $::wproxyDialog -parent . -modal local \
          	-separator 1 -title   "Enter passphrase for your proxy: " \
        	-side bottom -default 0 -cancel 2 ]
        set dlgframe [$dlg getframe]
        
        $dlg add -name 0 -text ACCEPT -command "set ::doinitProxy 1; destroy $dlg"   
        $dlg add -name cancel -command "destroy $dlg" 
        set passwidget  [ LabelEntry $dlgframe.passw -label "Your pass phrase: " -labelwidth 20 -labelanchor e \
                   -textvariable ::passphrase -editable 1 -width 50 -labeljustify right \
                   -helptext "The passphrase associated with your proxy" -show *]
        pack $passwidget -side top -fill x -expand 1
        $dlg draw
        if	{ [ info exist ::doinitProxy ] } {
        	set rc [ initProxy ]
        } 
    } err ] } {
    	return -code error $err
    }
}

} else {

;## done with tclsh

proc RawGlobusProxy::initProxyDialog {} {
	
    if	{ [ catch {
    	catch { exec /bin/stty -echo }
    	puts -nonewline "Enter passphrase for your proxy: " 
        flush stdout 
        set ::passphrase [ gets stdin ]
        catch { exec /bin/stty echo }
 		puts $::passphrase
        set ::doinitProxy 1
        initProxy
    } err ] } {
    	puts $err
    }
}


}
        
## ******************************************************** 
##
## Name: getProxyInfo
##
## Description:
## check on user proxy
##
## Usage:
##
## Comments:

proc RawGlobusProxy::getProxyInfo {} {

	if	{ [ catch {
    	set rc 0
    	catch { exec grid-proxy-info -identity -timeleft } data
        debugPuts "grid-proxy-info $data"
        if	{ [ regexp {ERROR.+find a valid proxy} $data ] } {
        	set rc 1
        }
        if	{ [ regexp {\n-1} $data ] } {
            set data "Your proxy has expired"
            puts "data expired"
            set rc 1
        }
 	} err ] } {
    	return -code error $err
    }
    return [ list $rc $data ]
}
            
        
## ******************************************************** 
##
## Name: verifyProxy
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc RawGlobusProxy::verifyProxy {} {

    set msg ""
    if	{ [ catch { 
		foreach prog $::RawGlobusProxy::gridprogs {
    		set progpath [ auto_execok $prog ]
			if	{ ![ string length $progpath ] } {
				return -code error  "$prog not found; unable to verify"
    		}
		}
		
        foreach { rc data } [ getProxyInfo ] { break }
        debugPuts "rc $rc data $data"
    	if	{ $rc } {
        	set rc [ initProxyDialog ]
            if	{ [ info exist ::doinitProxy ] } {
            	foreach { rc data } [ getProxyInfo ] { break }
                debugPuts "from getProxyInfo2  rc=$rc, data =$data"
                if	{ $rc } {
                	error $data 
                }
            }
        } else {
        	set ::doinitProxy 1
        }
        
        ;## if user has not cancel, check the time left on proxy		
        if	{ [ info exist ::doinitProxy ] } {
			if	{ [ regexp  {CN=([^\d]+)[^\n]+\n(\d+)} $data -> username timeleft] } {
				regsub -all {\s+} $username "_" username
        		set ::user [ string trim $username _ ]
        		debugPuts "user $::user, timeleft $timeleft"
			}

    		if	{ ![ string length $timeleft ] } {
				error "grid-proxy-info failed for -timeleft option."
			} elseif { $timeleft > 60 } {
        		set walltime [ clock format [ expr $timeleft + [ clock seconds ] ] -format "%m-%d-%Y %H:%M:%S" ]
    			set msg "This proxy is valid until $walltime."
    		}
            unset ::doinitProxy
        } else {
        	set ::user cancelled
        }
   	} err ] } {
    	puts stderr $err
    	return -code error $err 
    }
    debugPuts $msg
    return $::user 
}

set ::DEBUG 1
set ::USE_GLOBUS 1

set ::TCLGLOBUS_DIR /ldcg/lib

puts "pid [ pid ]"
debugPuts " tclglobus $::TCLGLOBUS_DIR"

if	{ [ catch {
	RawGlobusProxy::verifyProxy 
    exit 0
} err ] } {
	puts $err
	exit -1
}
