#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# This attempt make a Proc frame file with samples of the various
#    data types
#
# - Sequence
# - TimeSeries
# - Frequency series
#   - DFT
#   - PSD
#   - CSD
#   - transfer function (use complex FrequencySeries)
#     and then read it back
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::procReadWrite::test {
    ##-------------------------------------------------------------------
    ## Local variable declaration
    ##-------------------------------------------------------------------
    set stime [ expr 730524016 ]
    set frameLen 16
    set numFrames 2
    set duration [ expr $numFrames*$frameLen ]
    set chans Adc(H0:PEM-LVEA_SEISZ,H1:LSC-AS_Q,H1:LSC-AS_I)
    
    set etime [ expr $stime + $duration - 1 ]
    set times $stime-$etime

    set fetime [ expr $stime + 1 ]

    set procWrite {
        dataPipeline
        -datacondtarget frame
        -setsingledc 1
        -framequery { R H {} $times $chans }
        -aliases {
            asq = LSC-AS_Q;
            asi = LSC-AS_I;
        }
        -algorithms {
            # Sequence
            s1 = ramp(16384, 0.1, 1.0);
            s2 = ramp(16384, -1.0, 0.1);

            # Real TimeSeries
            ts1 = slice(asq, 0, 16384, 1);
            ts2 = slice(asi, 0, 16384, 1);

            # Complex TimeSeries      
            ph = div(3.14159265, 2.0);
            ts3 = mix(ph, 0.2, ts1);

            # FrequencySequence (and transfer function)
            sc = complex(s1, s2);
            f = fseries(sc, -64.0, 1.0, $stime, 0, $fetime, 0);

            # PSD and CSD
            p = psd(ts1, 16384);
            p3 = psd(ts3, 16384);
            c = csd(ts1, ts2, 16384);

            output(c, _, _, csd, CSD);
            output(f, _, _, fs1, FrequencySequence);
            output(p, _, _, psd, PSD);
            output(p3, _, _, psd3, PSD);
            output(ts1, _, _, ts1, TimeSeries 1);
            output(ts2, _, _, ts2, TimeSeries 2);
            output(ts3, _, _, ts3, TimeSeries 3);
        }
    }

    set procRead {
        conditionData
        -datacondtarget frame
        -outputformat { ilwd ascii }
        -setsingledc 1
        -framequery {
            { {} {} file:$outfile {} Proc(csd!0!8193!,fs1!-64!16384!,psd!0!8193!,psd3!-8192!8192!,ts1,ts2,ts3) }
        }      
        -algorithms {
            output(_ch0, _, _, ch0, comment);
            output(_ch1, _, _, ch1, comment);
            output(_ch2, _, _, ch2, comment);
            output(_ch3, _, _, ch3, comment);
            output(_ch4, _, _, ch4, comment);
            output(_ch5, _, _, ch5, comment);
            output(_ch6, _, _, ch6, comment);
        }
    }

    
    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
    ##-------------------------------------------------------------------
    ## Test the writing of proc data
    ##-------------------------------------------------------------------
    test {procReadWrite:ProcWrite} {} -body {
        set scmd [subst $procWrite]
        if {[catch {SubmitJob job $scmd} err]} {
            return $err
        }
        List $job(outputs)
        set outfile [lindex $job(outputs) 0]
        list
    } -result {}
    ##-------------------------------------------------------------------
    ## Test the reading of proc data
    ##-------------------------------------------------------------------
    test {procReadWrite:ProcRead} {} -body {
        set scmd [subst $procRead]
        if {[catch {SubmitJob job $scmd} err]} {
            return $err
        }
        List $job(outputs)
        list
    } -result {}

    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests

} ;## namespace - ::QA::procReadWrite::test

namespace delete ::QA::procReadWrite::test
