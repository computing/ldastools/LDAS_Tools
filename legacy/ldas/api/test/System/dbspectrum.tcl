#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::dbspectrum::test {}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::dbspectrum::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
	set TYPE DT_512
	set TIMES "610000000-610000511"
	
	set cmdInit {
		dataPipeline
    	-datacondtarget metadata
    	-database ldas_tst
    	-aliases {gw=P2\\:LSC-AS_Q::AdcData;}
    	-algorithms {
        	rgw = resample(gw, 1, 8);
        	p = psd( rgw, 262144 );
        	output(p,_,_,,spectrum data);
    	}
    	-framequery {
        	{$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
    	}
	}
		
	set cmdstem {
		dataPipeline
        -subject {$subject}
        -dynlib {$dynlib}
        -filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
        -dbspectrum {
            channel={$channel}
            spectrum_type=$spectrum_type
            spectrum_length=$spectrum_length
            start_time=$start_time
            start_time_ns=$start_time_ns
            start_frequency=$start_frequency
            delta_frequency=$delta_frequency
            pushpass=$pushpass
            alias=$alias
        }
        -datacondtarget $dctarget
        -multidimdatatarget ligolw
        -database ldas_tst
        -responsefiles {
            file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
        }
        -aliases {gw=P2\\:LSC-AS_Q::AdcData;}
        -algorithms {$algo}
        -framequery {
            {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
        }
   	}

	;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	;## submit job and returns reply
	;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	proc runTest {} {
		uplevel {
			set cmd [ subst $cmdstem ]
			Puts 1 "cmd $cmd"
			test dbspectrum:dataPipeline:$index:$subject:frameV4 {} -body {
				if 	{[catch {SubmitJob job $cmd} err]} {
					LJdelete job
					Puts 1 "Error: $err"
	    			return $err
				}
				set reply $job(jobReply)
				Puts 1 "$job(jobid)\nreply $reply\nexpected $expected"
				LJdelete job
				return $reply
    		} -match regexp -result $expected
		}
	}
	
	;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	;## clear database
	;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
   	proc clearDB { { mydb "" } } {
        upvar user user
         if      { ! [ string length $mydb ] } {
                 upvar dbname dbname
                 set mydb $dbname
         }
         set err [ Rexec $::METADATA_API_HOST $user "/usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
                DB2INSTANCE=ldasdb del_db_process.tcl $mydb unix_procid=18298" ]
        Puts 1 "$mydb cleanup $err"
    }
	
	set dynlib libldasinspiral.so
	set channel {resample(P2:LSC-AS_Q,1,8)}
	set spectrum_type Welch
	set spectrum_length 131073
	set start_time 609999999
	set start_time_ns 995117188
	set start_frequency 0.0000000000000000e+00
	set delta_frequency 7.8125000000000000e-03

	set index 0
	foreach cmd [ list $cmdInit ] {
        set subject {DBSPECTRUM:INIT:wrapper}
        test dbspectrum:dataPipeline:$index:$subject:frameV4  {} -body {
		    if 	{[catch {
			    set cmd [ subst $cmd ]
			    Puts 1 "cmd $cmd"
			    SubmitJob job $cmd
			    incr index 
			    set jobid [string trim $job(jobid) ${RUNCODE}]
			    Puts 0 "$job(jobid) ($job(LDASVersion)) INIT$index"			
		    } err]} {
			    LJdelete job
			    Puts 1 "Fail to initialize: $err; Aborting Test"
	    	    return $err
		    }
	    }
    }
	
    set index 1
	;## 1 - good binary, push, wrapper
	set subject {DBSPECTRUM:GOOD:PUSH:wrapper}
    	set pushpass push
    	set dctarget wrapper
    	set alias spect
    	set algo {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,:primary,resampled gw timeseries);
        output(spect,_,_,spectrum:psd,spectrum data);
     }
	set expected "rows into ldas_tst database"
	runTest

	;## 2 - good binary, pass, wrapper
	set subject {DBSPECTRUM:GOOD:PASS:wrapper}
    	set pushpass pass
    	set dctarget wrapper
    	set alias "spectrum:psd"
    	set algo {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,:primary,resampled gw timeseries);
    	}
	set expected "rows into ldas_tst database"
	runTest

	set dynlib {}
	;## 3 - good binary, push, ligolw
	set subject {DBSPECTRUM:GOOD:PUSH:LIGOLW}
    	set pushpass push
    	set dctarget ligolw
    	set alias spect
    	set algo {
        output(spect,_,_,,spectrum data);
    }
    set expected "can be found at:"
    runTest

	;## 4 - good binary, pass, ligolw
	set subject {DBSPECTRUM:GOOD:PASS:LIGOLW}
    	set pushpass pass
    	set dctarget ligolw
    	set alias spect
    	set algo {}
	set expected "can be found at:"
	runTest

	set dynlib libldasinspiral.so
	set channel {_not_there_}
	set spectrum_type {_not_there_}
	set spectrum_length 0
	set start_time 0
	set start_time_ns 0
	set start_frequency 0.0000000000000000e+00
	set delta_frequency 0.0000000000000000e+00

	set index 2
	;## 5 - empty binary, push, wrapper
	set subject {DBSPECTRUM:EMPTY:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set aliase spect
    set algo {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,:primary,resampled gw timeseries);
        output(spect,_,_,spectrum:psd,spectrum data);
    }
    set expected "not exactly one row resulting from spectrum query"
    runTest 
    catch {sendjob} reply

	;## 6 - empty binary, pass, wrapper
	set subject {DBSPECTRUM:EMPTY:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set alias "spectrum:psd"
    set algo {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,:primary,resampled gw timeseries);
    }
    set expected "not exactly one row resulting from spectrum query"
    runTest
	
	set dynlib {}
	;## 7 - empty binary, push, ligolw
	set subject {DBSPECTRUM:EMPTY:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set alias spect
    set algo {
        output(spect,_,_,,spectrum data);
    }
    set expected "not exactly one row resulting from spectrum query"
    runTest

	;## 8 - empty binary, pass, ligolw
	set subject {DBSPECTRUM:EMPTY:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
    set alias spect
    set algo {}
	set expected "not exactly one row resulting from spectrum query"
	runTest
	
	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
	
} ;## namespace - ::QA::dbspectrum::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::dbspectrum::test	


