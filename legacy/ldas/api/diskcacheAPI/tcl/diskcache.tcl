## -*- mode: tcl; tcl-indent-level: 3; indent-tabs-mode: nil;  -*-
## ********************************************************
## diskcache.tcl Version 1.0
##
## Maintain the array ::disk::cache.
##
## disk::cache(dirs) is a list of directories and the gps
## timestamps of the frames in those directories with hash
## information for rapid location of input frame data and
## identification of dropouts.
##
## the disk cache API provides the frame API with filenames
## and metadata which are used to extract necessary data
## products for use by other API's.
##
## ********************************************************

;#barecode

package provide diskcache 1.0
load /ldas/lib/diskcacheAPI/libdiskcacheAPI.so

namespace eval disk      {}
namespace eval cache     {
   ;## options are defined with a name, a regex pattern
   ;## which matches them, and a default value if applicable.
   ;## the variables will not get substituted at this point.
   ;## note that the REQUIRED options have no default and
   ;## will raise exceptions if not provided.
   set options {
      -interferometers {.*} {}
      -frames          {.*} {}
      -times           {.*} {}
      -concatenate     {^-?[01]$} 0
      -framequery      {.+}     {}
      -allowgaps       {^[01]$} 0
      -frametype       {.+}     R
      -jobid           {\d+}    $jobid
      -exception       {^[01]$} 0
   }
   set cacheDirty 0
   ##--------------------------------------------------------------------
   ## Start with the resource modify time being zero so it will force
   ##   the first scan of hot variables.
   ##--------------------------------------------------------------------
   set ResourceFileModifyTime 0.0
}
;#end

if { [ info exist ::TRAP_SIGNAL ] && $::TRAP_SIGNAL } {
   package require Signal

   ;## unable to do getThreadList within signal or in background
   ;## write out the cache before exiting
   proc handleInterrupt { args } {
      puts stderr "[ clock format [ clock seconds] -format "%x %X %Z" ] caught interrupt $args"
   }

   signal add SIGINT "handleInterrupt sigint"
   signal add SIGHUP "handleInterrupt sighup"
   signal add SIGTERM "handleInterrupt sigterm"
   signal add SIGABRT "handleInterrupt sigabrt"
   puts "Has handlers for signals\n[ signal print ]"
} else {
   puts "Use default signal handlers"
}

## ********************************************************
##
## Name: diskcache::init
##
## Description:
## The variable ::disk::cache contains a metadata
## representation of all directories found under the
## directories listed by ::MOUNT_PT which contain
## frame files whose names are consistent with the
## proposed frame naming convention.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc diskcache::init { args } {

   if { [ catch {
      set ::number_of_running_threads 0
      set ::last_reorder_dir        [ list ]
      set ::cache::nextupdateindex 0
      set ::get_dir_cache_last_called 0
      set ::cache::queuestart [ clock clicks -milliseconds ]

      ;## the diskcacheAPI does not use a data socket.
      catch { closeDataSock }

      ;## this regexp will return 6 elements when it
      ;## matches:
      ;## given:
      ;##          G-R-6666666666-1.gwf
      ;## returns:
      ;##          G-R-6666666666-1.gwf G R 6666666666 1 gwf
      ;##
      set ::official_fn_rx {^([^-]+)-([^-]+)-(\d{9,10})-(\d+)\.(gwf)$}

      ;## resource variable which causes the complex filename
      ;## list for gappy concatenation requests to be dumped
      ;## in the log with a purple ball.
      if { ! [ info exists ::DEBUG_CONCAT_RETURN_LIST ] } {
         set ::DEBUG_CONCAT_RETURN_LIST 0
      }

      if { ! [ info exists ::DEBUG_SCAN_RATE ] } {
         set ::DEBUG_SCAN_RATE 0
      }

      ;## Mount point entries in this list will not generate email
      ;## as their subdirectories update. Purple log entries will
      ;## be made instead.
      if { ! [ info exists ::IGNORE_REMOVED_DIRS_UNDER_MTPT ] } {
         set ::IGNORE_REMOVED_DIRS_UNDER_MTPT [ list ]
      }

      ;## how long to wait, in seconds, between repeated
      ;## error reports on a given directory
      if { ! [ info exists ::ERROR_REPORT_INTERVAL ] } {
         set ::ERROR_REPORT_INTERVAL 3600
      }

      if { ! [ info exists ::DEBUG_CACHE_SYNCHRONIZE ] } {
         set ::DEBUG_CACHE_SYNCHRONIZE 0
      }

      if { ! [ info exists ::SCANNED_FILENAME_EXTENSIONS ] } {
         set ::SCANNED_FILENAME_EXTENSIONS .gwf
      }

      if { ! [ info exists ::EXCLUDE_THESE_DIRS_FROM_UPDATES ] } {
         set ::EXCLUDE_THESE_DIRS_FROM_UPDATES [ list ]
      }

      if { ! [ info exists ::DISKCACHE_HASHFILE_NAME_BINARY ] || \
               ! [ string length [ string trim $::DISKCACHE_HASHFILE_NAME_BINARY ] ] } {
         set ::DISKCACHE_HASHFILE_NAME_BINARY .frame.cache
      }

      if { ! [ info exists ::DISKCACHE_HASHFILE_NAME_ASCII ] || \
               ! [ string length [ string trim $::DISKCACHE_HASHFILE_NAME_ASCII ] ] } {
         set ::DISKCACHE_HASHFILE_NAME_ASCII frame_cache_dump
      }

      if { ! [ info exist ::MOUNT_PT_LOOP_INTERVAL_MS ] || \
             $::MOUNT_PT_LOOP_INTERVAL_MS < 500 } {
         set ::MOUNT_PT_LOOP_INTERVAL_MS 500
      }       

      if { ! [ info exist ::CACHE_WRITE_DELAY_SECS ] } {
         set ::CACHE_WRITE_DELAY_SECS 60
      }
      ;## enable bgLoop again after 1 hour if nothing happens
      if { ! [ info exist ::CACHE_WRITE_RESUME_SECS ] } {
         set ::CACHE_WRITE_RESUME_SECS 3600
      }

      ;## warn about directories that take a really long time
      ;## to update.
      if { ! [ info exists ::DIR_SLOW_UPDATE_WARNING_THRESHHOLD ] } {
         set ::DIR_SLOW_UPDATE_WARNING_THRESHHOLD 60
      }

      ;## how long to wait for running threads to complete before
      ;## giving up on an attempt to rebuild the cache from scratch.
      if { ! [ info exists ::WAIT_N_SECONDS_FOR_THREADS_TO_COMPLETE ] } {
         set ::WAIT_N_SECONDS_FOR_THREADS_TO_COMPLETE 10
      }

      ;## how many directories should be updated simultaneously?
      ;## there may be a practical limit of about one thread per
      ;## available cpu when directories are actually changing.
      if { ! [ info exists ::NUMBER_OF_RUNNING_THREADS_PERMITTED ] } {
         set ::NUMBER_OF_RUNNING_THREADS_PERMITTED 4
      }

      ;## Should the mount point conflicts be checked for?
      if { ! [ info exists ::CHECK_FOR_MOUNT_PT_CONFLICT ] } {
         set ::CHECK_FOR_MOUNT_PT_CONFLICT 1
      }
      
      ;## enable/disable thread diagnostic info
      if { [ regexp {8.4} $::tcl_version ] } {
         trace add variable ::DEBUG_THREADS { write } "cache::debugThreads"
      } else {
         trace variable ::DEBUG_THREADS w "cache::debugThreads"
      }
      cache::debugThreads

      updateFileExtList \
          [ cache::extensionListMangler $::SCANNED_FILENAME_EXTENSIONS ]

      cache::hashFile readInitial
   } err ] } {
      return -code error "[ myName ]: $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: ${API}::atExit
##
## Description:
## write cache to disk
##
## Usage:
##
##
## Comments:

proc ${API}::atExit {} {
}

## ********************************************************
##
## Name: cache::pushHashfileName
##
## Description:
## Called whenever the on-disk hashfile is read (currently
## only at startup), this procedure relays the value of
## ::DISKCACHE_HASHFILE_NAME to the manager so that this
## file can be forced to be world readable.
##
## Per PR 3047 - diskcache API should push hashfile name
##
## Parameters:
##
## Usage:
##
## Comments:
##
proc cache::pushHashfileName { args } {

   if { [ catch {
      set fname $::DISKCACHE_HASHFILE_NAME_BINARY
      set sid [ sock::open manager emergency ]
      fconfigure $sid -blocking off
      puts $sid "$::MGRKEY NULL NULL set ::DISKCACHE_HASHFILE_NAME_BINARY $fname"
      ::close $sid
      set sid [ sock::open manager emergency ]
      fconfigure $sid -blocking off
      puts $sid "$::MGRKEY NULL NULL set ::DISKCACHE_HASHFILE_NAME_ASCII $::DISKCACHE_HASHFILE_NAME_ASCII"
      ::close $sid
   } err ] } {
      catch { ::close $sid }
      addLogEntry "[ myName ]: $err" email
   }

}
## ********************************************************

## ********************************************************
##
## Name: cache::getOptArray
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getOptArray { jobid } {
   set args     [ list ]
   set errs     [ list ]
   set opts     [ list ]

   if { [ catch {
      ;## automagically get all arguments which are
      ;## relevant to this API
      foreach { name rx default } $::cache::options {
         set name [ string trim $name "=" ]
         set name [ string tolower $name ]
         if { [ info exists ::${jobid}($name) ] } {
            set arg [ set ::${jobid}($name) ]
            if { [ regexp -- $rx $arg ] } {
               lappend args $name $arg
            } else {
               lappend errs "$name '$arg'"
            }
         }
         lappend opts $name $default
      }

      if { [ llength $errs ] } {
         return -code error "malformed arguments: $errs"
      }

      foreach { opt val } [ expandOpts ] {
         set opt [ string trim $opt - ]
         lappend arglist $opt $val
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $arglist
}
## ********************************************************

## ********************************************************
##
## Name: cache::runJob
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::runJob { jobid } {

   if { [ catch {

      regexp {\d+} $jobid job
      set jobid ${::RUNCODE}$job

      array set opt [ cache::getOptArray $jobid ]

      set concat     $opt(concatenate)
      set ag         $opt(allowgaps)
      set framequery $opt(framequery)

      if { [ info exists opt(exception) ] } {
         set exception  $opt(exception)
      }

      if { $concat != -1 } {
         cache::concatGetFilenames $jobid $framequery $ag
      } elseif { [ info exists exception ] } {
         cache::getFilenames $jobid $framequery $exception
      } else {
         cache::getFilenames $jobid $framequery
      }

      cache::pushResults $jobid
      cache::cleanup $jobid

   } err ] } {
      cache::cleanup $jobid
      return -code error "[ myName ]: $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::getFilenames
##
## Description:
## Given a -framequery, try and satisfy it with a list of
## lists of filenames which correspond to passes through
## the frame API toplevel accessor models, one list per
## pass.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getFilenames { jobid framequery { exception 0 } } {
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid ${::RUNCODE}$job
        set ::cache::state($job,framequery) $framequery
        set ::cache::state($job,gapflag) 0
        set ::cache::state($job,concat)  0

        if { ! [ info exists ::cache::state($job,atoms) ] } {
           set atoms [ cache::parseQuery $jobid $framequery ]
           foreach [ list ifos types frames times channels ] [ join $atoms ] {

              set ::${jobid}(-channels) $channels

              if { [ llength $frames ] && \
                   [ string length [ lindex $frames 0 ] ] } {
                  lappend files \
                     [ cache::find $jobid {} {} $frames $times ]
                  continue
              }
              foreach type $types {
                 foreach ifo $ifos {
                    lappend files \
                        [ cache::find $jobid $ifo $type {} $times $exception ]
                    set files \
                        [ cache::collapseRetval $jobid $ifos $files ]
                 }
              }
           }
           set ::cache::state($job,filenames) [ list $files ]
           set ::cache::state($job,atoms) $atoms
        } else {
           set atoms [ set ::cache::state($job,atoms) ]
           set files [ set ::cache::state($job,filenames) ]
        }

        if { $::DEBUG_CONCAT_RETURN_LIST } {
           addLogEntry $files purple
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }

     return [ list $atoms $files ]
}
## ********************************************************

## ********************************************************
##
## Name: cache::collapseRetval
##
## Description:
## This needs to take into account that complex framequeries
## May require two sets of the same data in order for the
## frame API to process them properly.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::collapseRetval { jobid ifos retval } {

   if { [ catch {

      set collapsed 0

      ;## if more than one ifo is being iterated over,
      ;## then some rearrangement of the frame file sets
      ;## may be required.
      if { [ llength $ifos ] > 1 } {
         ;## if more than one set of frames was actually
         ;## retrieved, collect the frames into a single
         ;## group, since the time range will be identical,
         ;## and the frame API now handles mixed groups
         ;## of frames properly.
         if { [ llength $retval ] > 1 } {
            set last [ lindex $retval end ]
            set nexttolast [ lindex $retval end-1 ]
            if { [ string equal $last $nexttolast ] } {
               set retval [ lrange $retval 0 end-1 ]
               set collapsed 1
            }
            ;## if no collapsing was done above, there still
            ;## may be some repackaging of the frame filename
            ;## list required.
            if { $collapsed == 0 } {
               set time1     [ lindex $last       0 ]
               set time2     [ lindex $nexttolast 0 ]
               if { [ string equal $time1 $time2 ] } {
                  set files1 [ lindex $last       1 ]
                  set files2 [ lindex $nexttolast 1 ]
                  set flag   [ lindex $last       2 ]
                  set retval \
                      [ list $time1 [ concat $files1 $files2 ] $flag ]
               }
            }
         }
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $retval
}
## ********************************************************

## ********************************************************
##
## Name: cache::concatGetFilenames
##
## Description:
## Retrieve filenames associated with a -framequery from
## a concatFrameData call.
## The trick here is to get the groups (potentially many)
## of files into a nested list which can be parsed by the
## frame API and associated with the original -framequery
## by indexing.
##
## Parameters:
##
## Usage:
##
## Comments:
## global_gaps is just the -allowgaps option
##

proc cache::concatGetFilenames { jobid framequery global_gaps } {

   if { [ catch {

      regexp {\d+} $jobid job
      set jobid ${::RUNCODE}$job

      set ::cache::state($job,framequery) $framequery
      set ::cache::state($job,gapflag) $global_gaps
      set ::cache::state($job,concat) 1

      if { ! [ info exists ::cache::state($job,atoms) ] } {

         set atoms [ cache::parseQuery $jobid $framequery ]
         set ::cache::state($job,atoms) $atoms
         set retval [ list ]

         foreach { ifos types frames times channels } [ join $atoms ] {

            if { [ regsub :allow_gaps $times {} _times ] || \
                     $global_gaps } {
               set allow_gaps 1
            } else {
               set allow_gaps 0
            }

            if { [ llength $frames ] && \
                     [ string length [ lindex $frames 0 ] ] } {
               set files \
                   [ cache::find $jobid {} {} $frames $_times ]
               if { ! [ regexp {\d{9,10}} $_times ] } {
                  set times {}
               }

               lappend retval [ list $_times $files $allow_gaps ]
               continue
            }

            foreach type $types {
               foreach ifo $ifos {
                  foreach timerange $times {
                     lappend retval [ cache::concatCollect \
                                          $jobid $timerange $global_gaps $ifo $type ]
                     set retval \
                         [ cache::collapseRetval $jobid $ifos $retval ]
                  }
               }
            }
         }

         set retval [ list $retval ]
         set ::cache::state($job,filenames) $retval
      } else {
         set retval $::cache::state($job,filenames)
      }

      if { $::DEBUG_CONCAT_RETURN_LIST } {
         addLogEntry $retval purple
      }
      if { ! [ string length $retval ] } {
         return -code error "No files matched framequery: '$framequery'"
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   if { ! [ string length $retval ] } {
      set retval [ list {} {} {} ]
   }
   return $retval
}
## ********************************************************

## ********************************************************
##
## Name: cache::concatCollect
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::concatCollect { jobid times global_gaps ifo type } {

   if { [ catch {
      set retval [ list ]

      foreach trange $times {

         if { $global_gaps || \
                  [ regsub :allow_gaps $trange {} trange ] } {
            set allow_gaps 1
            set exception  0
         } else {
            set allow_gaps 0
            set exception  1
         }

         lappend filenames \
             [ cache::findByTime $jobid $trange $ifo $type $exception ]
      }

      ;## determine the total time range among all specified
      ;## ranges and or times
      set ttrange [ split $trange {- } ]
      set ttrange [ lindex $ttrange 0 ]-[ lindex $ttrange end ]

      set filenames [ join $filenames ]

      ;## contruct a list of three elements for the
      ;## current time range
      if { [ string length $filenames ] } {
         lappend retval $ttrange $filenames $allow_gaps
      }

      if { ! [ info exists allow_gaps ] } {
         set msg "unknown error while parsing -framequery option"
         return -code error $msg
      }

      if { ! [ string length $retval ] && ! $allow_gaps } {
         set msg "no files matched, and allow_gaps is not set. args: "
         append msg "(times: '$times' ifo: '$ifo' type: '$type')"
         return -code error $msg
      }

   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $retval
}
## ********************************************************

## ********************************************************
##
## Name: cache::parseQuery
##
## Description:
## Parse the -framequery option into multiple associated
## requests if it looks like a compound query.
##
## The -frames, -times and -ifos options are NOT used
## when a compound -framequery is detected.
##
## Compound arguments are always formatted as a list of
## 4 elements with empty elements represented as "{}".
##
## The elements must be presented in correct order:
##
## { types ifos frames times query }
##
## Parameters:
##
## Usage:
##
##   supports -framequery options in several formats:
##
##  -framequery { F {H L} {} 600000000 Adc(0) }
##  -framequery { { F H {} 600000000 Adc(0) } { R L {} 600000000 Adc(100) } }
##
## Comments:
##

proc cache::parseQuery { jobid args } {

   if { [ llength $args ] == 1 } {
      ;## Determine whether this is a simple query or a complex query
      set qlist [ lindex $args 0 ]
      if { [ llength $qlist ] == 5 && \
           [ llength [ lindex $qlist 0 ] ] != 5 } {
         ;## This is a simple query
      } else {
         ;## This is a complex query, so modify args to be a list of
         ;## the component simple-queries
         set args $qlist
      }
   }
   
   set retval [ list ]
   if { [ catch {

      foreach arg $args {
         if { ! [ string length $arg ] } { continue }
         foreach [ list ifos types frames times channels ] \
                 [ cache::analyzeQueryElement $jobid $arg ] { break }
         cache::validateFrameQuery $frames $times $channels
         lappend retval [ list $ifos $types $frames $times $channels ]
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $retval
}
## ********************************************************

## ********************************************************
##
## Name: cache::validateFrameQuery
##
## Description:
## Beginnings of a -framequery validator/errorhandler
## intended to produce sensible error messages for the
## errant user.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::validateFrameQuery { frames times channels } {

   if { [ catch {
      set time_rx  {^(\d{9,10})([\,\-])?(\d+)?}
      set frame_rx {\d{9,10}\-\d+\.gwf$}
      set chan_rx  {^[a-zA-Z]+\([^\)]*\)$}
      set frame   [ string trim [ lindex $frames   0 ] ]
      set time    [ string trim [ lindex $times    0 ] ]
      set channel [ string trim [ lindex $channels 0 ] ]
      set errors  [ list ]

      if { [ info exists ::DEBUG_VALIDATE_QUERY ] && \
               [ string equal 1 $::DEBUG_VALIDATE_QUERY ] } {
         addLogEntry "frame: '$frame' time: '$time' channel: '$channel'" purple
      }

      ;## timerange tests
      if { [ regexp $time_rx $time -> begin token next ] } {
         if { [ string length $token ] } {
            if { ! [ string length $next ] } {
               set errmsg "timerange spec in -framequery option "
               append errmsg "seems to be invalid: '$times'"
            } elseif { [ string length $next ] < 9 } {
               set errmsg "timerange spec in -framequery option "
               append errmsg "includes impossibly small value: '$next'"
            } elseif { [ string length $next ] > 10 } {
               set errmsg "timerange spec in -framequery option "
               append errmsg "includes impossibly large value: '$next'"
            }
            if { [ info exists errmsg ] } {
               lappend errors $errmsg
            }
         }
      } elseif { [ string length $time ] } {
         set errmsg "timerange spec in -framequery option "
         append errmsg "appears to be invalid: '$times'"
         lappend errors $errmsg
      }

      ;## frame name test
      if { [ string length $frame ] } {
         if { ! [ regexp $frame_rx $frame ] } {
            set errmsg "frames spec in -framequery option "
            append errmsg "appears to be invalid: '$frames'."
            lappend errors $errmsg
         }
      }

      ;## channel spec test
      if { ! [ regexp $chan_rx $channel ] } {
         set errmsg "channel spec in -framequery option "
         append errmsg "appears to be invalid: '$channels'."
         if { [ regexp -nocase {^[a-z]+\{} $channel ] } {
            append errmsg " it looks like you are using braces "
            append errmsg "where parentheses are required."
         }
         lappend errors $errmsg
      }

      if { [ llength $errors ] } {
         return -code error $errors
      }

   } err ] } {
      return -code error "[ myName ]: $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::analyzeQueryElement
##
## Description:
## Calculates the frame files and object list for a
## -framequery option.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::analyzeQueryElement { jobid args } {

   if { [ llength $args ] == 1 } {
      set args [ lindex $args 0 ]
   }

   set frames_tmp [ list ]

   if { [ catch {
      set types           [ list ]
      set frames          [ list ]
      set channels        [ list ]
      set interferometers [ list ]
      set times           [ list ]
      foreach { typs ifos frames tims ques } $args { break }

      if { ! [ string length $typs ] } { set typs R }

      ;## ifo spec overrides channel names now...
      if { ! [ string length [ lindex $ifos 0 ] ] } {
         set temp [ cache::channelsToIfos $ques ]
         if { [ llength $temp ] > 0 } {
            set ifos $temp
         }
         ::unset temp
      }

      if { [ llength $ifos ] == 0 } { set ifos 0 }

      set typ_length [ llength $typs   ]
      set ifo_length [ llength $ifos   ]
      set frm_length [ llength $frames ]
      set tim_length [ llength $tims   ]
      set que_length [ llength $ques   ]

      ;## any given framequery item can only refer to a single
      ;## frame family.  so one framequery is required for each
      ;## frame family accessed by a single job.
      set name short
      foreach frame $frames {
         set file_name_atoms [ cache::analyzeFilename $frame ]
         if { [ string length $file_name_atoms ] } {
            array set temp $file_name_atoms
            if { ! [ string equal official $temp(type) ] } {
               set err    "Files with names that do not adhere to the "
               append err "official frame naming convention are no "
               append err "longer supported by LDAS.  The filename "
               append err "you specified: '$frame' seems to be of "
               append err "type '$temp(type)', which is no longer "
               append err "supported.  Please rename your frames to "
               append err "follow the official frame naming "
               append err "convention.  The official convention "
               append err "frame names of the form: "
               append err "IFO-TYPE-GPSTIME-DURATION.gwf"
               return -code error $err
            }
            set name $temp(type)
            set dt $temp(tdt)
            set t0 $temp(gps)
            set frame_tmp $frames
            break
         }
      }

      if { [ info exists frames_tmp ] && \
               [ string length $frames_tmp ] } {
         set frames $frames_tmp
      }

      ;## specific channels from explicitly named
      ;## frames.
      if { $frm_length && $que_length } {
         set interferometers {}
         set ifo_length 0
         set types           {}
         set typ_length 0

         ;## if user specified a time range
         if { [ string length [ lindex $tims 0 ] ] } {
            set times $tims
            ;## otherwise the time range should be the duration
            ;## of the named frame as determined from the frame name
            ;## when a single frame name is specified.
         } elseif { [ info exists t0 ] && [ llength $frames ] == 1 } {
            set times $t0-[ expr {$t0 + $dt - 1} ]
            ;## otherwise, punt!
         } else {
            set times [ list ]
         }

         set channels $ques
      }

      ;## specific channels over some time range with
      ;## ifo(s) specified.  this is slightly complicated
      ;## by the fact that frames will have channels for
      ;## more than one ifo, as in H0, H1 and H2 channels in
      ;## a Hanford frame...
      if { $typ_length && $ifo_length && $tim_length && $que_length } {
         foreach ifo $ifos {
            set types $typs
            set interferometers $ifos
            set times $tims
            set frames {}
            set channels $ques
         }
      }

   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return [ list $interferometers $types $frames $times $channels ]
}
## ********************************************************

## ********************************************************
##
## Name: cache::find
##
## Description:
## Find a frame or range of frames given their names.
##
## Parameters:
## frames - a list or a range of frame names [ xxxx-xxxx ]
##
## Usage:
##     get abspathlist [ cache::find $framelist ]
##
## Comments:
## The frame files are stored according to this schema:
## There is an exported directory called /export.
## There are subdirectories called home(n) where
## 0 lte n lte N.
## Each of these directories contains up to 3600 frame
## files, plus trend files.  Every 8 hours all the
## directories contain all new data files.
## The data files are named according to this convention:
## [HLC]-(gpstime).[FT].  H,L, and C are Hanford,
## Livingston, and CalTech respectively.  A file extension
## of "F" indicates a frame file, an extension of "T"
## indicates a trend file.
## So an example file location would be:
## /export/home9/data0/H-600000000.F .
## The latest frame could be in any of n possible
## directories.
##
## The mount point of the exported directory is in the
## global variable MOUNT_PT.

proc cache::find { jobid { ifo "" } { type R } { frames "" } { times "" } { exception 0 } } {
   if { [ catch {
      set done 0
      set globbed 0

      if { ! [ string equal 0 $::waiting_for_cache_read ] } {
         set err "recently restarted ${::API}API not yet initialized. "
         append err "please retry job in a few seconds."
         return -code error $err
      }

      set tmp [ list ]

      if { ! [ string length $frames ] } {
         set tmp \
             [ cache::findByTime $jobid $times $ifo $type $exception ]
         if { [ llength $tmp ] && [ string length $tmp ] } {
            set done 1
            set globbed 1
         } else {
            set tmp [ list ]
         }
      }

      ;## maybe we have filenames in $frames
      if { ! $done } {
         foreach frame $frames {
            set frame [ cache::findFileOnDisk $jobid $frame ]
            if { [ string length $frame ] } {
               lappend tmp $frame
            }
         }
      }

      if { [ llength $tmp ] && [ string length $tmp ] } {
         set done 1
      } else {
         set tmp [ list ]
      }

      set tmp [ cache::sortFrames $tmp 1 ]
      set n [ llength $tmp ]
      if { $n } {
         debugPuts "$n frame(s) match query"
      } else {
         set msg "no frames match query: (frames: '$frames' "
         append msg "times: '$times' ifo: '$ifo' type: '$type')"
         return -code error $msg
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $tmp
}
## ********************************************************

## ********************************************************
##
## Name: cache::findFileOnDisk
##
## Description:
## Find a file given only a filename.  Return all matches
## with absolute paths.
## Return an empty string on failure to match.
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::findFileOnDisk { jobid filename } {

   if { [ catch {

      ;## filename is relatively or absolutely visible
      ;## from working directory
      if { [ file exists $filename ] } {

         if { [ file isdirectory $filename ] } {
            set err "requested file: $filename is a directory!"
            return -code error $err
         }

         ;## we only test for filetype on explicitly named
         ;## files.
         set type [ fileType $filename ]
         if { [ lsearch $type frame ] < 0 } {
            set err "specified file: $filename is not a frame file!"
            append err "file type seems to be: $type "
            return -code error $err
         }

      }

   } err ] } {
      if { [ string length $err ] } {
         return -code error "[ myName ]: $err"
      }
   }
   return $filename
}
## ********************************************************

## ********************************************************
##
## Name: cache::analyzeFilename
##
## Description:
## Encapsulates all the file naming conventions ever
## proposed, with heaviest weight to the "current" proposal.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::analyzeFilename { filename } {

   if { [ catch {
      set atoms [ list ]

      if { [ string equal .gwf [ file extension $filename ] ] } {
         set atoms [ cache::analyzeOfficialFilename $filename ]
      } else {
         return {}
      }

      if { [ llength $atoms ] == 6 } {
         lappend atoms type official
         lappend atoms ifo  [ lindex $atoms 1 ]
         lappend atoms typ  [ lindex $atoms 2 ]
         lappend atoms gps  [ lindex $atoms 3 ]
         ;## we will have to use the tdt and
         ;## getFrameNumber to set the "N" and "dt" later
         lappend atoms frn  1
         lappend atoms tdt  [ lindex $atoms 4 ]
         lappend atoms ext  gwf
      } else {
         set atoms [ list ]
      }

   } err ] } {
      if { [ string length $err ] } {
         return -code error "[ myName ]: $err"
      }
   }
   return $atoms
}
## ********************************************************

## ********************************************************
##
## Name: cache::analyzeOfficialFilename
##
## Description:
## Analyze filenames adhereing to the current proposed
## frame file naming specification, which looks like:
##
## S-G-T-D.gwf
##
## where "S" is the interferometer (Site), "G" is the
## GPS time stamp, "T" is the dt of the file, and "D"
## is what used to be the extension.
##
## "N" can now only be conned at access time by using
## the getFrameNumber on an open frame file pointer.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::analyzeOfficialFilename { filename } {

   if { [ catch {
      set atoms [ list ]

      set tail [ file tail [ string trim $filename ] ]
      set tail [ string trim $tail ]
      set atoms [ regexp -inline -- $::official_fn_rx $tail ]

      ;## if the pattern matched, but the filename was
      ;## somehow malformed... should not be possible
      if { [ llength $atoms ] } {
         if { [ regsub -all -- {-} $tail {} foo ] != 3 } {
            set msg "malformed 'official' frame filename: '$filename'"
            return -code error $msg
         }
      }

   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $atoms
}
## ********************************************************

## ********************************************************
##
## Name: cache::findByTimeUnThreaded
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::findByTimeUnThreaded { jobid times ifo type { exception 0 } } {

   if { [ catch {
      set fnames [ list ]
      set seqpt [ list ]
      set ext [ lindex $::SCANNED_FILENAME_EXTENSIONS 0 ]
      if { $exception == 0 } {
         set gaps 1
      } else {
         set gaps 0
      }

      if { [ info exists ::${jobid}(-rds) ] } {
         set rdsflag  [ set ::${jobid}(-rds) ]
         set channels [ set ::${jobid}(-channels) ]
         set resample [ resamplingRequested $jobid $channels ]
      } else {
         set rdsflag 0
         set channels [ list ]
         set resample 0
      }

      if { [ info exists ::${jobid}(-allowgaps) ] } {
         set gaps [ set ::${jobid}(-allowgaps) ]
      }

      foreach timerange [ cache::parseTimeSpec $times ] {
         foreach [ list start end ] $timerange {
            if { $rdsflag } {
               set seqpt "getRDSFrameFiles($ifo $type $start $end $ext $resample):"
               foreach [ list files errors ] \
                   [ getRDSFrameFiles $ifo $type $start $end $ext $resample ] \
                   { break }

            } else {
               set seqpt "getFrameFiles($ifo $type $start $end $ext $gaps):"
               foreach [ list files errors ] \
                   [ getFrameFiles $ifo $type $start $end $ext $gaps ] \
                   { break }
            }
            if { [ string length [ lindex $files 0 ] ] } {
               lappend fnames $files
            } else {
               set files "(no files found)"
            }
            if { [ string length [ lindex $errors 0 ] ] } {
               return -code error [ list $files $errors ]
            }
         }
         set seqpt [ list ]
      }

      set fnames [ join $fnames ]

      set caller [ uplevel myName ]
      if { ! [ string equal cache::find $caller ] } {
         set n [ llength $fnames ]
         if { $n } {
            debugPuts "$n frame names calculated which match query"
         } else {
            set msg "no frames match query: "
            append msg "(times: '$times' ifo: '$ifo' type: '$type')"
            return -code error $msg
         }
      }

   } err ] } {
      return -code error "[ myName ]:$seqpt $err"
   }
   return $fnames
}
## ********************************************************

## ********************************************************
##
## Name: cache::findByTime
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## Threaded version

proc cache::findByTime { jobid times ifo type { exception 0 } } {

   if { [ catch {
      set ext [ lindex $::SCANNED_FILENAME_EXTENSIONS 0 ]
      set seqpt [ list ]
      if { $exception == 0 } {
         set gaps 1
      } else {
         set gaps 0
      }

      if { [ info exists ::${jobid}(-rds) ] } {
         set rdsflag  [ set ::${jobid}(-rds) ]
         set channels [ set ::${jobid}(-channels) ]
         set resample [ resamplingRequested $jobid $channels ]
      } else {
         set rdsflag 0
         set channels [ list ]
         set resample 0
      }
      catch { unset ::${jobid}(threads_done) }
      set ::${jobid}(fnames) [ list ]
      if { [ info exists ::${jobid}(-allowgaps) ] } {
         set gaps [ set ::${jobid}(-allowgaps) ]
      }
      set ::${jobid}(caller) [ uplevel myName ]
      set ::${jobid}(times) $times
      set ::${jobid}(threads_created) 0
      set ::threadcount($jobid) [ list ]
      foreach timerange [ cache::parseTimeSpec $times ] {
         foreach [ list start end ] $timerange {
            if { $rdsflag } {
               set seqpt "getRDSFrameFiles_t($ifo $type $start $end $ext $resample):"
               set tid [ getRDSFrameFiles_t $ifo $type $start $end $ext $resample ]
               addLogEntry "$seqpt $tid created" purple
               lappend ::threadcount($jobid) $tid
               incr ::${jobid}(threads_created)
               catch { ::unset ::$tid }
               # after 500 [ list cache::findByTimeCallback $jobid $tid $ifo $type ]
               after 20
               ::setAlert $tid ::$tid
               ::setTIDCallback $tid "cache::findByTimeCallback $jobid $tid $ifo $type"
            } else {
               set seqpt "getFrameFiles_t($ifo $type $start $end $ext $gaps):"
               set tid [ getFrameFiles_t $ifo $type $start $end $ext $gaps ]
               incr ::${jobid}(threads_created)
               lappend ::threadcount($jobid) $tid
               addLogEntry "$seqpt $tid created" purple
               catch { ::unset ::$tid }
               after 20
               ::setAlert $tid ::$tid
               ::setTIDCallback $tid "cache::findByTimeCallback $jobid $tid $ifo $type"
            }
         }
      }
      set ::${jobid}(threads_done) 0
      trace add variable ::${jobid}(threads_done) { write } [ list cache::findByTimeDone $jobid $tid $ifo $type ]
      vwait ::${jobid}(result)

      if { [ info exist ::${jobid}(errors) ] } {
         error [ set ::${jobid}(errors) ]
      }
   } err ] } {
      return -code error "[ myName ]:$seqpt $err"
   }
   return [ set ::${jobid}(result) ]
}

## ********************************************************
##
## Name: cache::findByTimeCallback
##
## Description:
## callback to reap getFrameFiles thread
## Parameters:
##
## Usage:
##
## Comments:

proc cache::findByTimeCallback { jobid tid ifo type args } {

   set seqpt {}

   if { ! [ info exist ::$tid ] } {
      ;## addLogEntry "::$tid does not exist" purple
      return
   }
   if { [ catch {
      set safe  0
      set thread_state [ getThreadStatus $tid ]
      if { [ string equal FINISHED $thread_state ] || \
               [ string equal $thread_state $::TID_FINISHED ] } {
         set now [ clock seconds ]
         catch { set reaper [ getThreadFunction $tid ] }
         set seqpt "$reaper ($tid):"
         catch { ${reaper}_r $tid } result
         ::unset ::$tid
         foreach [ list files errors ] $result { break }
         addLogEntry "$tid reaped, $jobid '$files' '$errors'" purple
         if { [ string length [ lindex $files 0 ] ] } {
            lappend ::${jobid}(fnames) $files
         } else {
            lappend ::${jobid}(fnames) "(No frame files found)"
            if { [ string length [ lindex $errors 0 ] ] } {
               set errors [ list "No frame files found ($errors)" ]
            } else {
               set errors [ list "No frame files found" ]
            }
         }
         if { [ string length [ lindex $errors 0 ] ] } {
            lappend ::${jobid}(errors) [ list $files $errors ]
         }
         set seqpt [ list ]
         ;## this enables the trace
         incr ::${jobid}(threads_done) 1
      }
   } err ] } {
      addLogEntry "[ myName ]:$seqpt $err" red
      return -code error "[ myName ]:$seqpt $err"
   }
}

## ********************************************************
##
## Name: cache::findByTimeDone
##
## Description:
## callback to reap getFrameFiles thread
## Parameters:
##
## Usage:
##
## Comments:

proc cache::findByTimeDone { jobid ifo type args } {

   if { [ catch {
      set threads_done [ set ::${jobid}(threads_done) ]
      set threads_created [ set ::${jobid}(threads_created) ]

      if { $threads_done >= $threads_created } {

         set fnames [ join [ set ::${jobid}(fnames) ] ]

         set caller [ set ::${jobid}(caller) ]

         if { ! [ string equal cache::find $caller ] } {
            set n [ llength $fnames ]
            if { $n } {
               debugPuts "$n frame names calculated which match query"
            } else {
               set msg "no frames match query: "
               set times [ set ::${jobid}(times) ]
               append msg "(times: '$times' ifo: '$ifo' type: '$type')"
               addLogEntry "no match $msg" purple
               set ::${jobid}(result) $msg
            }
         }
         unset ::${jobid}(threads_done)
         unset ::threadcount($jobid)
         ;## make this one last to enable wakeup from vwait
         set ::${jobid}(result) $fnames
      }
   } err ] } {
      return -code error "[ myName ]:$err"
   }
}

## ********************************************************
##
## Name: cache::parseTimeSpec
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::parseTimeSpec { times } {

   if { [ catch {
      set timeranges [ list ]
      set times [ split $times " ," ]
      foreach time $times {
         if { [ regexp {(\d{9,10})-(\d{9,10})} $time -> start end ] } {
            ;## do nothing
         } elseif { [ regexp {^\d{9,10}$} $time start ] } {
            set end [ expr { $start + 1 } ]
         } else {
            return -code error "invalid time spec: '$time'"
         }
         lappend timeranges [ list $start $end ]
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $timeranges
}
## ********************************************************


## ********************************************************
##
## Name: cache::updateDirsAsync
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::updateDirsAsync { jobid cid } {

   if { [ catch {
      RequestCacheRebuild
   } err ] } {
      set msg "[ myName ]: $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::updateHotVariables
##
## Description:
##   Variables are considered hot if and only if modifying
##   their value in the resource file should be automaticly
##   injested into the running API
##
## Parameters:
##
## Usage:
##
## Comments:
##   Currently, the following variables are hot:
##   ::EXCLUDE_THESE_DIRS_FROM_UPDATES
##   ::MOUNT_PT
##   ::SCANNED_FILENAME_EXTENSIONS
##

proc cache::updateHotVariables { args } {

   if { [ catch {
      set seqpt [ list ]
      set rscfile LDASdiskcache.rsc

      #------------------------------------------------------------------
      # Check for the existance of the system specific resource file.
      #------------------------------------------------------------------
      if { ! [ file exists $rscfile ] } {
         #---------------------------------------------------------------
         # Create the resouce file in a directory where an
         #   LDAS administrator can modify the values.
         # Seed with the values of distributed template form of the file.
         #---------------------------------------------------------------
         set sysrscfile ${::LDASLIB}/${::API}API/$rscfile
         file copy -force $sysrscfile $rscfile
         set subject "$::LDAS_SYSTEM ${::API}API warning!"
         set body "$::LDAS_SYSTEM ${::API}API has created the\n"
         append body "required system file [pwd]/${rscfile}.\n"
         set msg "Subject: ${subject}; Body: $body"
         ;## addLogEntry $msg email
         logMailEntry $subject $msg
      }
      set rsc_mtime [ file mtime $rscfile ]
      if { $rsc_mtime != $::cache::ResourceFileModifyTime } {
         #---------------------------------------------------------------
         # Something has changed, take action
         #---------------------------------------------------------------
         #---------------------------------------------------------------
         # Provide scoping
         # AND let the TCL interpreter handle multi line syntax
         #---------------------------------------------------------------
         set interp [ interp create ]
         if [ catch {
            $interp eval source $::TOPDIR/cntlmonAPI/cntlmon.state
            $interp eval source $::TOPDIR/LDASapi.rsc
            $interp eval source $rscfile
         
            #------------------------------------------------------------
            # Handling of ::EXCLUDE_THESE_DIRS_FROM_UPDATES
            #------------------------------------------------------------
            set n [ $interp eval set ::EXCLUDE_THESE_DIRS_FROM_UPDATES ]
            if { [ string length $n ] } {
               set n [ concat $n ]
               addLogEntry "DEBUG: EXCLUDE_THESE_DIRS_FROM_UPDATES: $::EXCLUDE_THESE_DIRS_FROM_UPDATES =?= $n" blue
               if { ! [string equal $n $::EXCLUDE_THESE_DIRS_FROM_UPDATES ] || \
                     $::cache::ResourceFileModifyTime == 0.0 } {
                  #------------------------------------------------------
                  # Some difference exists, so reset the
                  #   value of ::EXCLUDE_THESE_DIRS_FROM_UPDATES
                  #------------------------------------------------------
                  set ::EXCLUDE_THESE_DIRS_FROM_UPDATES $n
               }
            }

            #------------------------------------------------------------
            # Handling of ::MOUNT_PT
            #------------------------------------------------------------
            set n [ $interp eval set ::MOUNT_PT ]
            if { [ string length $n ] } {
               set n [ concat $n ]
               set n [ cache::uniqueNotSorted $n ]
               if { ! [string equal $n $::MOUNT_PT ] || \
                     $::cache::ResourceFileModifyTime == 0.0 } {
                  #------------------------------------------------------
                  # Some difference exists, so reset the
                  #   value of ::MOUNT_PT
                  #------------------------------------------------------
                  set ::MOUNT_PT $n
               }
            }

            #------------------------------------------------------------
            # Handling of ::SCANNED_FILENAME_EXTENSIONS
            #------------------------------------------------------------
            set oldext [ string trim [ getFileExtList ] ]
            set test [ $interp eval set ::SCANNED_FILENAME_EXTENSIONS ]
            set extlist [ cache::extensionListMangler $test ]

            ;## heal bad lisitification
            set seqpt "updateFileExtList($extlist):"
            updateFileExtList $extlist
            set seqpt [ list ]

            if { ! [ string equal $oldext $extlist ] } {
               set subject "$::LDAS_SYSTEM ::SCANNED_FILENAME_EXTENSIONS updated"
               set body "$::LDAS_SYSTEM ::SCANNED_FILENAME_EXTENSIONS updated"
               append body " from ***-> '$oldext'\n"
               append body "to ***-> '$extlist'"
               set msg "Subject: ${subject}; Body: $body"
               ;## addLogEntry $msg email
               logMailEntry $subject $msg
            }

         } err ] {
            #------------------------------------------------------------
            # Cleanup the interpreter that was allocated
            #------------------------------------------------------------
            global errorInfo

            set einfo $errorInfo
            catch { interp delete $interp } ignore
            #------------------------------------------------------------
            # Rethrow the error
            #------------------------------------------------------------
            error $err $einfo
         }
         interp delete $interp
         set ::cache::ResourceFileModifyTime $rsc_mtime
      }
   } err ] } {
      return -code error "[ myName ]:$seqpt $err"
   }
}

## ********************************************************
##
## Name: cache::constructName
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::constructName { name } {

   if { [ catch {

      set first \$dir/\$ifo-\$type-\$stime-\$dt.gwf
      set last  \$dir/\$ifo-\$type-\$timestamp-\$dt.gwf
      set first [ uplevel subst $first ]
      set last  [ uplevel subst $last  ]

   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return [ list $first $last ]
}
## ********************************************************

## ********************************************************
##
## Name: cache::killJob
##
## Description:
## Cleanup any pending action associated with an aborted
## job.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::killJob { jobid } {

   if { [ catch {
      ;## reap any threads
      catch { cache::cleanup $jobid }
      catch { unset ::${jobid} }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::cleanup
##
## Description:
## Normal cleanup routine called at end of successful job.
##
## Parameters:
##
## Usage:
##
## Comments:
## This is more a proof-of-concept than anything really
## useful. so far.

proc cache::cleanup { jobid } {

   if { [ catch {
      regexp {\d+} $jobid job
      set jobid $::RUNCODE$job

      foreach item [ array names ::cache::state $job,* ] {
         ::unset ::cache::state($item)
      }
      if { [ info exists ::threadcount($jobid) ] } {
         if { [ llength [ set ::threadcount($jobid) ] ] } {
            set msg "threads running under this jobid will be "
            append msg "orphaned and garbage collected as possible."
            foreach tid $::threadcount($jobid) {
               catch { getThreadStatus $tid } state
               if { [ regexp {RUNNING} $state ] } {
                  set ::runningorphans($tid) $jobid
                  after $::REAP_THREAD_DELAY cache::reapOrphanThread $tid
               }
               if { ! [ regexp {invalid_tid} $state ] } {
                  append msg " (thread '$tid' is in state '$state')"
               }
               if { [ regexp {FINISHED} $state ] } {
                  catch { set reaper [ getThreadFunction $tid ] }
                  catch { ${reaper}_r $tid } result
                  addLogEntry "reaped finished thread $reaper $tid: $result" purple
               }
            }
            append msg " running threads will be reaped when state "
            append msg " reaches FINISHED."
            addLogEntry $msg red
         }
         ::unset ::threadcount($jobid)
      }
   } err ] } {
      addLogEntry $err red
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::sendResults
##
## Description:
## Return result of disk cache query via socket.
## Will always be called outside of the context within
## which errors can propagate anywhere other than the
## disk cache API's log.
##
## Parameters:
##
## Usage:
##
## Comments:
## the frame API connects to the diskcache API emergency
## port and issues the command "cache::sendResults $jobid"
## Closing the socket is unnecessary, since it is part of
## the emergency handler.
##

proc cache::sendResults { jobid } {

   if { [ catch {
      set results [ list ]
      regexp {\d+} $jobid job
      set jobid $::RUNCODE$job

      set sid [ uplevel set cid ]

      if { [ info exists   ::cache::state($job,filenames) ] } {
         set results [ set ::cache::state($job,filenames) ]
      } else {
         return -code error "no files for job $jobid"
      }

      puts $sid $results
      cache::cleanup $jobid
   } err ] } {
      cache::cleanup $jobid
      addLogEntry "[ myName ]: $err" red
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::pushResults
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::pushResults { jobid } {

   if { [ catch {
      set results [ list ]
      regexp {\d+} $jobid job
      set jobid $::RUNCODE$job

      set sid [ sock::open frame emergency ]

      if { [ info exists   ::cache::state($job,filenames) ] } {
         set results [ set ::cache::state($job,filenames) ]
      } else {
         return -code error "no files found for job $jobid"
      }
      if { ! [ string length $results ] } {
         return -code error "no files found for job $jobid"
      }

      puts $sid "$::MGRKEY set ::filecache$job $results"
      ::close $sid

      ;## 12/18/07 preStageData disabled in diskcache

   } err ] } {
      catch { ::close $sid }
      return -code error $err
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::sortFrames
##
## Description:
## Sort frames in a group according to the frame filename
## independent of the directory name, which was messing
## up the sorting previously.
## Takes the unsorted list, returns the uniquified sorted
## list.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::sortFrames { frames { unique 0 } } {

   if { [ catch {
      set tmp [ list ]
      if { $unique } {
         foreach frame $frames {
            set tail [ file tail $frame ]
            if { ! [ regexp {\-(\d{9,10})} $tail -> timestamp ] } {
               set msg    "invalid frame filename: '$tail' "
               append msg "matches no known frame naming convention."
               return -code error $msg
            }
            lappend tmp [ list $frame $timestamp ]
         }
         set tmp [ lsort -integer -unique -index 1 $tmp ]
         set frames [ list ]
         foreach frame $tmp {
            lappend frames [ lindex $frame 0 ]
         }
      } else {
         set frames [ lsort -dictionary $frames ]
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $frames
}
## ********************************************************

## ********************************************************
##
## Name: diskCacheRDSQuery
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc diskCacheRDSQuery { jobid } {

   ;## based on Mary's observation that the trace
   ;## sometimes fails...
   set cid [ uplevel set cid ]

   if { [ catch {
      set frames   [ list ]
      set times    [ set ::${jobid}(-times)    ]
      set ifo      [ set ::${jobid}(-ifo)      ]
      set type     [ set ::${jobid}(-type)     ]
      set channels [ set ::${jobid}(-channels) ]
      set rdsflag  [ set ::${jobid}(-rds)      ]

      if { [ info exists ::${jobid}(-framequery) ] } {
         set framequery [ set ::${jobid}(-framequery) ]
         set framequery [ join [ join $framequery ] ]
      } else {
         set framequery [ list ]
      }

      ;## make sure we send just the list of filenames,
      ;## no timerange or gapflag, to the frame API.
      set ::${jobid}(-concatenate) -1

      ;## for now, missing frames are an exception condition
      if { ! [ info exists ::${jobid}(-exception) ] } {
         set ::${jobid}(-exception) 1
      }

      ;## PR# 2807 fix
      if { [ string length $framequery ] < 9 } {

         ;## we need to  make the channels spec pass transparently
         ;## through the validateFrameQuery code if it looks even
         ;## nearly right!!
         if { [ string length [ join [ join $channels ] ] ] } {
            set channels [ list Fake($channels) ]
         }

         set ::${jobid}(-framequery) \
             [ list $type $ifo $frames $times $channels ]
      }

      set query [ set ::${jobid}(-framequery) ]
      if { [ string length [ join $query ] ] < 3 } {
         set msg "no -framequery option, bypassing diskcacheAPI"
         set ::$cid [ list 2 $msg {} ]
      } else {
         ;## have to pass a flag to indicate RDS and resampling!!
         cache::runJob $jobid
         set ::$cid [ list 0 0 0 ]
      }

   } err ] } {
      set ::$cid [ list 3 "[ myName ]: $err" error! ]
      addLogEntry $err red
   }
   reattach $jobid $cid
}
## ********************************************************

## ********************************************************
##
## Name: resamplingRequested
##
## Description:
## Parse channel query to determine whether resampling
## is being requested.
##
## returns '1' or '0'.
##
## Parameters:
##
## Usage:
## Resampling channel queries look like:
##
##            H_CHAN-Q!resample!2!
##
## Comments:
##

proc resamplingRequested { jobid channels } {

   if { [ catch {
      #set resample_rx {!(resample)!(2|4|8|16)!}
      ;## the above regexp caused use of the RDS shorthand
      ;## resampling notation to fail.
      set resample_rx {\!(16|2|4|8|1)}

      if { [ regexp -nocase $resample_rx $channels ] } {
         set resampling 1
      } else {
         set resampling 0
      }

   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $resampling
}
## ********************************************************

## ********************************************************
##
## Name: cache::channelsToIfos
##
## Description:
## Takes all forms of channel spec and generates a sorted
## list of the associated interferometers.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::channelsToIfos { args } {

   if { [ llength $args ] == 1 } {
      set args [ lindex $args 0 ]
   }

   if { [ catch {
      set ifos [ list ]
      foreach chanspec $args {
         ;## clean up something like Adc(...)
         regexp {\S+\(([^\)]+)\)} $chanspec -> chanspec
         set chanspec [ join [ split $chanspec " ," ] ]
         foreach channel $chanspec {
            set channel [ string trim $channel ]
            set ifo [ string index $channel 0 ]
            if { [ regexp {^[A-Z]$} $ifo ] } {
               lappend ifos $ifo
            }
         }
      }
      set ifos [ lsort -unique $ifos ]
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $ifos
}
## ********************************************************

## ********************************************************
##
## Name: cache::beginThreadedGlob
##
## Description:
## Start a glob thread, after 30 milliseconds start event
## handling on it.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::beginThreadedGlob { dir args } {

   ;## null string in update list, iterate!
   if { ! [ string length $dir ] } {
      return 1
   }

   ;## a thread for this directory is already
   ;## running from a previous round, iterate!
   if { [ info exists ::thread($dir,tid) ] && \
            [ string length $::thread($dir,tid) ] } {
      return 1
   }

   ;## sanitise in case of operator error!!
   if { ! [ info exists ::NUMBER_OF_RUNNING_THREADS_PERMITTED ] || \
            ! [ string length $::NUMBER_OF_RUNNING_THREADS_PERMITTED ] } {
      set ::NUMBER_OF_RUNNING_THREADS_PERMITTED 4
   }
   set limit $::NUMBER_OF_RUNNING_THREADS_PERMITTED
   ;## if we already have a full complement of threads
   ;## running, defer. do not iterate.
   if { $::number_of_running_threads >= $limit } {
      return 0
   }

   if { [ catch {
      set now [ clock seconds ]

      set ::thread($dir,start) $now

      ;## START THREAD
      set tid [ getDirEnt_t $dir ]
      incr ::number_of_running_threads 1
      set ::thread($dir,tid) $tid
      set ::thread($dir,tidstate) RUNNING
      catch { ::unset ::$tid }

      if { [ regexp {8.4} $::tcl_version ] } {
         trace add variable ::$tid { write } [ list cache::endThreadedGlob $dir ]
      } else {
         ::trace variable ::$tid w [ list cache::endThreadedGlob $dir ]
      }
      ::setAlert $tid ::$tid
   } err ] } {
      addLogEntry "'$err' ($::errorInfo)" red
   }
   return 1
}

## ********************************************************
##
## Name: cache::emailsub
##
## Description:
## Parses return values from c++ layer and sets email
## subject line appropriately.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::emailsub { errors } {

   if { [ catch {
      if { [ regexp {Time overlap} $errors ] } {
         set sub "time overlap error(s)!"
      } elseif { [ regexp {Duplicate start} $errors ] } {
         set sub "duplicate start time!"
      } elseif { [ regexp {symbolic links}  $errors ] } {
         set sub "symlink in ::MOUNT_PT!"
      } elseif { [ regexp {not a directory} $errors ] } {
         set sub "invalid ::MOUNT_PT directory!"
      } elseif { [ regexp {No such file}    $errors ] } {
         set sub "::MOUNT_PT entry does not exist!"
      } elseif { [ regexp {(ADDED|REMOVED)} $errors ] } {
         set sub "hash updated!"
      } else {
         set sub "errors!"
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $sub
}
## ********************************************************

## ********************************************************
##
## Name: cache::parseErrorStrings
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The LDASdiskcache.rsc resource variable:
##
##    ::IGNORE_REMOVED_DIRS_UNDER_MTPT
##
## can be set to a list of directory names that are otherwise
## defined in the ::MOUNT_PT resource variable to reduce the
## number of email level log messages that are generated.
##
## Any ::MOUNT_PT entry that is also in the
## ::IGNORE_REMOVED_DIRS_UNDER_MTPT list will only generate
## email when a CONFLICTING DATA error occurs. Log entries
## that would otherwise raise email due to REMOVED type
## errors will be logged with a purple ball.
##
## Note that it is always the case that ADDED type 'errors'
## are logged with blue balls unless another type of error
## occurred under the same mount point, when they are
## logged as part of the real error report to facilitate
## diagnostic analysis.

proc cache::parseErrorStrings { mtpt errors } {

   if { [ catch {
      set ignores $::IGNORE_REMOVED_DIRS_UNDER_MTPT
      set level blue
      set ::cache::cacheDirty 1
      if { ! [ regexp -- {(ADDED|REMOVED)} $errors ] } {
         set level email

         ;## handle JUST edited ::MOUNT_PT
         set rx {is\s+not\s+in\s+MOUNT_PT\s+list.+first\.}
         set rstring "has been removed from the ::MOUNT_PT."
         if { [ regsub -all $rx $errors $rstring errors ] } {
            uplevel set errors $errors
            set level blue
         }

      } else {
         # cache::hashFile write
         set ::cache::cacheDirty 1
      }

      ;## PR #2738 - admins complained about excessive email
      if { [ regexp {REMOVED} $errors ] } {
         if { [ lsearch $ignores $mtpt ] == -1 } {
            set level email
         } elseif { [ regexp {CONFLICTING} $errors ] } {
            set level email
         } else {
            set level purple
         }
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $level
}
## ********************************************************

## ********************************************************
##
## Name: cache::hashFile
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::hashFile { { action NULL } } {

addLogEntry "cache::hashFile: 0" purple
   switch -exact $action {
      readInitial {
         set ::waiting_for_cache_read 1
         if { ! [info exists ::RUNCODE] || ! [string length $::RUNCODE] } {
            after 1000 cache::hashFile $action
            return
         }
      }
   }
addLogEntry "cache::hashFile: 1" purple
   if { [ catch {
      set seqpt [ list ]

      if { ! [ info exists ::cache_file_last_updated ] } {
         set ::cache_file_last_updated 0
      }

      set bhashfile $::DISKCACHE_HASHFILE_NAME_BINARY
      set ahashfile $::DISKCACHE_HASHFILE_NAME_ASCII
      set now [ clock seconds ]
      switch -exact $action {
         readInitial {
            cache::pushHashfileName
            cache::afterBootlock
            set seqpt "readDirCache($bhashfile): "
            readDirCache $bhashfile
            set ::waiting_for_cache_read 0
         }
         read {
            cache::pushHashfileName
            set ::waiting_for_cache_read 1
            ::bootLock ON
            set seqpt "readDirCache($bhashfile): "
            set tid [ readDirCache_t $bhashfile ]
            incr ::number_of_running_threads 1
            set seqpt [ list ]
            set ::thread(cachefile,tid) $tid
            set now [ clock seconds ]
            set ::thread(cachefile,start) $now
            set ::thread(cachefile,tidstate) RUNNING
            catch { ::unset ::$tid }
            if { [ regexp {8.4} $::tcl_version ] } {
               ::trace add variable ::$tid { write } \
                   [ list cache::readCacheFileCallback $tid ]
            } else {
               ::trace variable ::$tid w \
                   [ list cache::readCacheFileCallback $tid ]
            }
            ::setAlert $tid ::$tid
         }
         delete {
            if { [ file exists $bhashfile ] } {
               bak $bhashfile
               file delete -force $bhashfile
            }
            if { [ file exists $ahashfile ] } {
               bak $ahashfile
               file delete -force $ahashfile
            }
         }
         default {
            set err "unknown action: '$action'"
            return -code error $err
         }
      }
   } err ] } {
      if { [ regexp {readDirCache} $seqpt ] } {
         set ::waiting_for_cache_read 0
         ::bootLock OFF
      }
      addLogEntry "$seqpt$err" red
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::readHashFileCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::readCacheFileCallback { tid args } {

   if { [ catch {
      set seqpt {}
      set thread_state [ set ::$tid ]

      set ::thread(cachefile,tidstate) $thread_state

      if { [ string equal FINISHED $thread_state ] || \
               [ string equal $thread_state $::TID_FINISHED ] } {
         set now [ clock seconds ]
         set seqpt "readDirCache_r($tid):"
         catch { readDirCache_r $tid } result
         ::unset ::$tid
         incr ::number_of_running_threads -1

         set ::waiting_for_cache_read 0
         ::bootLock OFF

         ;## the thread failed to get a directory listing
         ;## and threw an exception
         if { [ regexp {_p_tid} $result ] } {
            addLogEntry $result red
            set exception 1
         } else {
            set exception 0
         }

         set ::thread(cachefile,tid)      [ list ]
         set start $::thread(cachefile,start)
         set ::thread(cachefile,start)    [ list ]
         set ::thread(cachefile,tidstate) [ list ]
         set ::thread(cachefile,dt) [ expr { $now - $start } ]
         set dt $::thread(cachefile,dt)
         if { [ string equal 0 $exception ] } {
            set msg "instantiated hash from file in $dt seconds"
            addLogEntry $msg blue
         }
      }
   } err ] } {
      set ::waiting_for_cache_read 0
      ::bootLock OFF
      addLogEntry $err red
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::getCachePerSpec
##
## Description: threads the request to get frame cache
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getCachePerSpec { jobid { ifos all } { types all } } {

   if { [ catch {
      set retval [ list ]
      set tid [ getDirCache_t $ifos $types ]
      addLogEntry "$tid created for getDirCache_t $ifos $types" purple
      catch { ::unset ::$tid }
      after 20
      ::setAlert $tid ::$tid
      ::setTIDCallback $tid "cache::getCachePerSpecCallback $jobid $tid $ifos $types"
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $retval
}
## ********************************************************

## ********************************************************
##
## Name: cache::getCachePerSpecCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## callback by thread state change
proc cache::getCachePerSpecCallback { jobid tid { ifos all } { types all } args } {

   if { ! [ info exist ::$tid ] } {
      return
   }

   if { [ catch {
      set state [ set ::$tid ]
      if { [ array exist ::$jobid ] } {
         set cid [ set ::${jobid}(cid) ]
      }
      set flag 1
      if { [ string equal $state $::TID_FINISHED ] || \
               [ string equal FINISHED $state ] } {
         set retval [ list ]
         set data [ getDirCache_r $tid ]
         set msg "$tid created for getDirCache_t $ifos $types reaped, cache size [ string length $data ]"
         catch { ::unset ::$tid }
         unset flag
         addLogEntry $msg purple
         if { ! [ string length $data ] } {
            error "No data in frame cache"
         }
         if { [ info exist cid ] } {
            foreach [ list a b c d ] $data {
               append retval "$a $b $c [ list $d ]\n"
            }
            set retval [ string trim $retval ]

            set outfname frame.cache
            set rp [ set ::${jobid}(-returnprotocol)  ]
            publicFile $jobid $outfname $retval
            set msg [ macroReturnMsg $jobid $rp $outfname ]
            lappend msg "current frame cache"
            set ::$cid $msg
            unset ::$jobid
            ::reattach $jobid $cid
         } else {
            error "$jobid does not exist for $tid, $tid reaped"
         }
      }
   } err ] } {
      set msg [ list 3 "getFrameCache: $err" error! ]
      addLogEntry $err 2
      if { [ info exist flag ] } {
         catch { ::unset ::$tid }
      }
      if { [ info exist cid ] } {
         set ::$cid $msg
         addLogEntry "::$cid set to [ set ::$cid ]" purple
         unset ::$jobid
         ::reattach $jobid $cid
      }
   }
}

## ********************************************************
##
## Name: cache::afterBootlock
##
## Description:
## Helper function that defers initialization steps until
## AFTER the has file read is complete.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::afterBootlock { args } {
   if { [ string equal 0 $::waiting_for_cache_read ] } {
      after  3000 [ list bgLoop cacheupdate cache::updateHotVariables 15 ]
      after  5000 [ list DumpCacheDaemonStart ]
      after 30000 [ list ScanMountPointListContinuously ]
   } else {
      after 1000 cache::afterBootlock
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::waitOnAllThreads
##
## Description:
## Toggles on each call
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::waitOnAllThreads { } {

   if { [ catch {
      if { ! [ info exists ::WAITING_FOR_ALL_THREADS ] } {
         set correct $::NUMBER_OF_RUNNING_THREADS_PERMITTED
         set ::WAITING_FOR_ALL_THREADS $correct
         set ::NUMBER_OF_RUNNING_THREADS_PERMITTED 0
      } else {
         set correct $::WAITING_FOR_ALL_THREADS
         unset ::WAITING_FOR_ALL_THREADS
         set ::NUMBER_OF_RUNNING_THREADS_PERMITTED $correct
      }
   } err ] } {
      return -code error "[ myName ]: $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::uniqueNotSorted
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::uniqueNotSorted { data } {

   if { [ catch {

      ;## uniquify without sorting
      set temp [ list ]
      foreach element $data {
         if { [ lsearch -exact $temp $element ] == -1 } {
            lappend temp $element
         }
      }

   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $temp
}
## ********************************************************

## ********************************************************
##
## Name: cache::getTimeIntervals
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getTimeIntervals { jobid cid ifos types start end } {

   if { [ catch {

      set ifosXtypes [ list ]

      foreach ifo $ifos {
         foreach type $types {
            lappend ifosXtypes ${ifo}-$type
         }
      }

      set ext [ lindex $::SCANNED_FILENAME_EXTENSIONS 0 ]

      set seqpt "getIntervalsList_t($ifosXtypes $start $end):"
      set tid [ getIntervalsList_t $ifosXtypes $start $end $ext ]
      incr ::number_of_running_threads 1
      set seqpt [ list ]
      catch { ::unset ::$tid }

      #::setAlert $tid ::$tid
      #::setTIDCallback $tid "cache::getTimeIntervalsCallback $jobid $cid $tid"
      #::trace variable ::$tid w \
          #   [ list cache::getTimeIntervalsCallback $jobid $cid $tid ]
      after 500 [ list cache::getTimeIntervalsCallback $jobid $cid $tid ]
      #set seqpt "setAlert($tid ::$tid):"
      #::setAlert $tid ::$tid
   } err ] } {
      return -code error "[ myName ]:$seqpt $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::getTimeIntervalsCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getTimeIntervalsCallback { jobid cid tid { iter 0 } } {

   if { [ catch {
      set safe  0
      set seqpt {}
      set thread_state [ getThreadStatus $tid ]
      if { [ string equal FINISHED $thread_state ] || \
               [ string equal $thread_state $::TID_FINISHED ] } {
         set now [ clock seconds ]
         set seqpt "getIntervalsList_r($tid):"
         catch { getIntervalsList_r $tid } result
         catch { ::unset ::$tid }
         incr ::number_of_running_threads -1
         set safe 1
         if { ! [ string length $result ] } {
            set msg [ list 3 NO_MATCH error! ]
         } else {
            set msg [ list 0 $result results ]
         }
         ;## the thread failed to get a directory listing
         ;## and threw an exception
         if { [ regexp {_p_tid} $result ] } {
            addLogEntry $result red
            set msg [ list 3 "[ myName ]: $result" error! ]
         }
      } else {
         if { $iter >= 300 } {
            set safe 1
            set err "thread $tid is still in state "
            append err "$thread_state after 5 minutes!! "
            append err "thread appears to be stuck."
            addLogEntry $err red
            return -code error $err
         }
         incr iter
         after 1000 \
             [ list cache::getTimeIntervalsCallback $jobid $cid $tid $iter ]
      }
   } err ] } {
      if { $safe == 0 } {
         catch { ::unset ::$tid }
         incr ::number_of_running_threads -1
      }
      set msg [ list 3 "[ myName ]: $err" error! ]
   }
   if { [ info exists msg ] } {
      set ::$cid $msg
      cache::cleanup $jobid
      unset ::$jobid
      reattach $jobid $cid
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::extensionListMangler
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::extensionListMangler { raw } {

   if { [ catch {
      set bads [ list ]
      set mangled [ list ]
      set raw [ lsort -dictionary -unique $raw ]
      set i [ lsearch $raw .gwf ]
      if { $i != -1 } {
         set raw [ lreplace $raw $i $i ]
      }
      foreach ext $raw {
         if { ! [ regexp {^\.} $ext ] } {
            lappend bads $ext
         } elseif { [ regsub -all {\.} $ext {} junk ] > 1 } {
            lappend bads $ext
         }
      }
      if { [ llength $bads ] } {
         set err    "Bad filename extensions detected in "
         append err "::SCANNED_FILENAME_EXTENSIONS list.\n"
         append err "extensions must begin with a '.' and\n"
         append err "have no additional '.'.\n"
         append err "Bad items in list:\n $bads"
         return -code error $err
      }
      set mangled [ concat .gwf $raw ]
   } err ] } {
      return -code error "[ myName ]: $err"
   }
   return $mangled
}
## ********************************************************

## ********************************************************
##
## Name: cache::getDirEntries
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getDirEntries { jobid cid mountpoint } {

   if { [ catch {

      set seqpt "getDirEnt_t($mountpoint):"
      set tid [ getDirEnt_t $mountpoint ]
      incr ::number_of_running_threads 1
      set seqpt [ list ]
      catch { ::unset ::$tid }
      #::setAlert $tid ::$tid
      #::setTIDCallback $tid "cache::getDirEntriesCallback $jobid $cid $tid"
      #::trace variable ::$tid w \
          #   [ list cache::getTimeIntervalsCallback $jobid $cid $tid ]
      after 500 [ list cache::getDirEntriesCallback $jobid $cid $tid ]
      #set seqpt "setAlert($tid ::$tid):"
      #::setAlert $tid ::$tid
   } err ] } {
      return -code error "[ myName ]:$seqpt $err"
   }
}
## ********************************************************

## ********************************************************
##
## Name: cache::getDirEntriesCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::getDirEntriesCallback { jobid cid tid { iter 0 } } {

   if { [ catch {
      set safe  0
      set seqpt {}
      set thread_state [ getThreadStatus $tid ]
      if { [ string equal FINISHED $thread_state ] || \
               [ string equal $thread_state $::TID_FINISHED ] } {
         set now [ clock seconds ]
         set seqpt "getDirEntries_r($tid):"
         catch { getDirEnt_r $tid } result
         catch { ::unset ::$tid }
         incr ::number_of_running_threads -1
         set safe 1
         if { ! [ string length $result ] } {
            set msg [ list 3 NO_MATCH error! ]
         } else {
            set msg [ list 0 $result results ]
         }
         ;## the thread failed to get a directory listing
         ;## and threw an exception
         if { [ regexp {_p_tid} $result ] } {
            addLogEntry $result red
            set msg [ list 3 "[ myName ]: $result" error! ]
         }
      } else {
         if { $iter >= 300 } {
            set safe 1
            set err "thread $tid is still in state "
            append err "$thread_state after 5 minutes!! "
            append err "thread appears to be stuck."
            addLogEntry $err red
            return -code error $err
         }
         incr iter
         after 1000 \
             [ list cache::getDirEntriesCallback $jobid $cid $tid $iter ]
      }
   } err ] } {
      if { $safe == 0 } {
         catch { ::unset ::$tid }
         incr ::number_of_running_threads -1
      }
      set msg [ list 3 "[ myName ]: $err" error! ]
   }
   if { [ info exists msg ] } {
      set ::$cid $msg
      cache::cleanup $jobid
      unset ::$jobid
      reattach $jobid $cid
   }
}
## ********************************************************

## ********************************************************
##
## Name: logMailEntry
##
## Description:
## decides if a mail gif log needs to be written
## and writes one out if needed
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cache::logMailEntry { subject msg } {
     set caller [ lindex [ info level -1 ] 0 ]
     set now [ clock seconds ]
     if { [ info exist ::mailerrors($subject) ] } {
        foreach { time errmsg } [ set ::mailerrors($subject) ] { break }
        if { [ expr $now - $time ] <= $::ERROR_REPORT_INTERVAL } {
           if { $::DEBUG >= 3 } {
              foreach { diff str1 str2 } [ stringdiff $errmsg $msg ] { break }
              if { [ string length $diff ] } {
                 puts "now=$now, last sent $time\n$str1\n$str2\n$diff"
              } else {
                 addLogEntry "Skip same email '$subject'" purple
                 return
              }
           } elseif { [ string equal $errmsg $msg ] } {
              addLogEntry "Skip same email '$subject'" purple
              return
           }
        }
     }
     uplevel {
        addLogEntry $msg email
     }
     set ::mailerrors($subject) [ list $now $msg ]
}
## ********************************************************

## ********************************************************
##
## Name: cache::reapOrphanThread
##
## Description:
## reap orphan threads for jobs that are aborted or timed out
##
##
## Parameters:
##
## Usage:
##
## Comments:

proc cache::reapOrphanThread { tid  { duration 0 } } {
     if { [ catch {
        set state [ getThreadStatus $tid ]
        # addLogEntry "tid $tid state $state, duration $duration" purple
        if { [ regexp {FINISHED} $state ] } {
           catch { set reaper [ getThreadFunction $tid ] }
           catch { ${reaper}_r $tid } result
           addLogEntry "reaped finished thread $tid: $result" purple
        } else {
           if { $duration < $::THREAD_TIMEOUT } {
              incr duration $::REAP_THREAD_DELAY
              after $::REAP_THREAD_DELAY \
                cache::reapOrphanThread $tid $duration
           } else {
              addLogEntry "$tid still not in FINISHED state after $duration secs, unable to reap." red
           }
        }
     } err ] } {
        addLogEntry "$tid error: $err" red
     }
}
## ******************************************************** 

## ********************************************************
##
## Name: stringdiff
##
## Description:
## compares strings
##
## Parameters:
##
## Usage:
##
## Comments:
proc stringdiff { A B } {
     regsub -all {[\s\t\n]+} $A { } A
     regsub -all {[\s\t\n]+} $B { } B
     set C [ list ]
     foreach a [ split $A {} ] b [ split $B {} ]  {
        if [ string equal $a $b ] {
           append C " "
        } else {
           append C *
        }
     }
     return [ list $C $A $B ]
}
## ********************************************************

## ********************************************************
##
## Name: cache::debugThreads
##
## Description:
## enable or disable thread diagnostic information from C++
##
## Usage:
##
##
## Comments:

proc cache::debugThreads { args } {
     if { [ catch {
        if { $::DEBUG_THREADS < 0 || $::DEBUG_THREADS > 1 } {
           error "must be boolean: 0 or 1"
        }
        setTIDDebugging $::DEBUG_THREADS
        if { $::DEBUG_THREADS } {
           addLogEntry "Thread diagnostics enabled" purple
        } else {
           addLogEntry "Thread diagnostics disabled" purple
        }
     } err ] } {
        set body "::DEBUG_THREADS value $::DEBUG_THREADS $err; resource not set."
        set subject "$::LDAS_SYSTEM failed to update resource for thread diagnostics"
        addLogEntry "Subject: ${subject}; Body: $body" email
        return -code error $err
     }
}
## ********************************************************
