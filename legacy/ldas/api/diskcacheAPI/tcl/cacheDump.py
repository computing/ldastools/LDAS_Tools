import sys
sys.path.insert(0, "/ldas/lib/diskcacheAPI" )
import diskscan

usage = "Usage: python cacheDump.py path_to_cache_file"

if len(sys.argv) < 2:
	print "\nError: Did not specify the cache file location."
	print usage
	print
	sys.exit(1)

diskscan.readDirCache(sys.argv[1])
data = diskscan.getDirCache( )
lines  = data.split('}')
for line in lines:
	line = line.strip()
	if line:
		# Split eats up the last } character
		print "%s}" %(line)
