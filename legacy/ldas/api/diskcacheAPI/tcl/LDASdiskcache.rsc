## -*- mode: tcl -*-
## ********************************************************
##
## Name: LDASdiskcache.rsc
##
## This is the diskcache API specific resource file.  It contains
## resource information which is only used by the diskcache API.
##
## If this file is not found the diskcache API and hence the
## local LDAS installation will not be able to locate frame
## files.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
##
## ********************************************************
;#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES

;## desc=list of toplevel directories where frame files are found (beware of spaces after line continuation character!)
set ::MOUNT_PT [ list \
/scratch/test/frames \
/ldas_outgoing/frames/RDSVerify \
/ldas_outgoing/test/test_uncompressed \
/ldas_outgoing/mirror/frames \
/data/ide1/LHO \
/data/ide1/LLO \
/data/ide2/LHO \
/data/ide2/LLO \
/data/ide3/LHO \
/data/ide3/LLO \
/data/ide4/LHO \
/data/ide4/LLO \
/data/ide5/LHO \
/data/ide5/LLO \
/data/ide6/LHO \
/data/ide6/LLO \
/data/ide7/LHO \
/data/ide7/LLO \
/data/ide8/LHO \
/data/ide8/LLO \
/data/ide9/LHO \
/data/ide9/LLO \
/data/ide10/LHO \
/data/ide10/LLO \
/data/ide11/LHO \
/data/ide11/LLO \
/data/ide12/LHO \
/data/ide12/LLO \
/data/ide15/LHO \
/data/ide15/LLO \
/data/ide16/LHO \
/data/ide16/LLO \
/data/ide17/LHO \
/data/ide17/LLO \
/archive/E12 \
/archive/E11 \
/archive/frames/S4/L0 \
/archive/frames/S4/L1 \
/archive/frames/S4/L3/LHO \
/archive/frames/S4/L3/LLO \
/archive/TEST /archive/E10  \
/archive/frames/S4/L3/GEO \
]

;## desc=list of active mount points that we don't want extra email about
set ::IGNORE_REMOVED_DIRS_UNDER_MTPT [ list /frames/full ]

;## desc=how many seconds to allow in a sigle framequery
set ::FRAMELIMIT 86400

;## desc=set this to 0 to supress logging of diagnostic
set ::CACHE_DIAGNOSTIC_LOGGING 0

;## desc=how long to sleep (ms) before starting a new ::MOUNT_PT entry scan thread.
set ::DELAY_BETWEEN_THREAD_SPAWNS_MS 100

;## desc=if the thread updating a ::MOUNT_PT entry takes this long, log it
set ::DIR_SLOW_UPDATE_WARNING_THRESHHOLD 1800

;## desc=how many ::MOUNT_PT update threads can run simultaneously
set ::NUMBER_OF_RUNNING_THREADS_PERMITTED 10

;## desc=how long to wait for existing threads to complete before aborting rebuild
set ::WAIT_N_SECONDS_FOR_THREADS_TO_COMPLETE 10

;## desc=report progress scanning through ::MOUNT_PT
set ::DEBUG_SCAN_RATE 1

;## desc=list of directories to be excluded from update scans
set ::EXCLUDE_THESE_DIRS_FROM_UPDATES [ list A4 Burst-MDC E4 E6 minute-trend second-trend ]

;## desc=name that will be used for C++ binary hash file on disk
set ::DISKCACHE_HASHFILE_NAME_BINARY .frame.cache

;## desc=name that will be used for ascii hash file on disk
set ::DISKCACHE_HASHFILE_NAME_ASCII frame_cache_dump

;## desc=list of filename extensions that should be included in hash
set ::SCANNED_FILENAME_EXTENSIONS [ list .gwf ]

;## desc=how many seconds to delay repeating email reports of cache problems
set ::ERROR_REPORT_INTERVAL 3600

;## The ::CHECK_FOR_MOUNT_PT_CONFLICT was added initially as a diagnostic
;## tool for PR2753. The conclusion is that looking up of duplicates is
;## not the source of the observed slow down. The default value of 1
;## should be used used unless experimenting with system performance.
;## desc=1 if system should check for mount point conflicts, 0 otherwise
set ::CHECK_FOR_MOUNT_PT_CONFLICT 1

;## desc=turn on debugging of c++ thread information
set ::DEBUG_THREADS 1

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list \
vmemoryuse default \
core default \
cputime default \
datasize default \
filesize default \
memorylocked default \
descriptors default \
maxproc default \
]

;## desc=Length  of  time  in  seconds  for  (l)stat  calls  to  complete
set ::STAT_TIMEOUT 60

;## desc=Length of time in milliseconds to wait before retrying to acquire mutex lock
set ::RWLOCK_INTERVAL_MS 250

;## desc=Length of time in milliseconds to try and acquire mutex lock
set ::RWLOCK_TIMEOUT_MS 0

;## desc=Establish the level of lock debugging
;## The debugging level has two components. The first is
;## 0 - Disabled
;## 1 - Keep track of the state of locks
;## 2 - Same as 1 plus keeps a memory version of the text string
;## 3 - Same as 2 plus keeps track of all historical locks used by a thread.
;## The Second component specifies where the log message is saved.
;## 0000 - Create a C++ string with the value.
;## 1000 - Same as 0000 plus create a C global buffer (CDeadLockInfo) with info.
;## 2000 - Same as 1000 plus write out to error log each time the state
;##        has changed.
;##
;## Example: The value 1001 keeps track of active locks and writes out to the
;##          global CDeadLockInfo.
set ::DEBUG_DEADLOCK_DETECTOR_LEVEL 0

;## desc=install user defined signal handlers
set ::TRAP_SIGNAL 0

;## desc=set delay for bgLoop to write cache to disk
set ::CACHE_WRITE_DELAY_SECS 60

;## desc=set seconds to enable bgLoop again in case things are stuck
set ::CACHE_WRITE_RESUME_SECS 3600

;## desc=Specify size of the stack to give each thread.
set ::THREAD_STACK_SIZE 32768

;## desc=frequency in millisecs between mount point checks
set ::MOUNT_PT_LOOP_INTERVAL_MS 500

;## desc=Upper bound value in seconds for which to consider a directory to
;## be active.
;## A value of zero suppresses asyncronous updates of active directories.
set ::HOT_DIRECTORY_AGE_SEC 300

;## desc=Delay between hot directories check
set ::HOT_DIRECTORY_SCAN_INTERVAL_SEC 4

;## desc=Flag controlling if StatVFS is used
set ::DISKCACHE_ENABLE_STATVFS 1
