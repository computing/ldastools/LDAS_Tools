// diskcacheAPI Header Files
#include "diskcachecmd.hh"
   
// System Header Files   
#include <sys/types.h>   
#include <pthread.h>
#include <signal.h>
#include <unistd.h> // sleep

#include <csignal>   

#include <cassert>   

#include <algorithm>   
#include <iostream>   
#include <iterator>   
#include <list>   
#include <sstream>   
#include <vector>
   
#include "general/types.hh"
#include "general/ReadWriteLock.hh"
   
using namespace std;
using namespace General;   

   
   //#define ENABLE_DEBUG
   
   
const string scanDirectory( const string& dir )   
{
   try
   {
      string subdir_list( getDirEnt( dir.c_str() ) );

      return subdir_list;
   }
   catch( const exception& exc )
   {
      string msg( "Caught exception: ");
      msg += exc.what();

      return msg;   
   }
   catch( ... )
   {
      string msg( "Caught unknown exception." );
      return msg;
   }

   
   return "";
}

   
const string writeCache()
{
   static const CHAR* TestCacheFile( "test.cache.threaded" );

   
   try
   {
      // Write cache to the file   
      writeDirCache( TestCacheFile );
   }
   catch( const exception& exc )
   {
      string msg( "Caught exception: " );
      msg += exc.what();
   
      return msg;
   }
   catch( ... ) 
   {
      string msg( "Caught unknown exception." );
      return msg;
   }

   
   return "";
}

   
// Create thread per MOUNT_PT entry
typedef struct 
{
    bool        started;   
    bool        finished;
    bool        checked;

    string      directory;
    string      results;
} ProcessArgs;
   
   
typedef struct
{
   ProcessArgs args;
   pthread_t   id;
}TIDInfo;


// Function called within the thread
static void* _process( void *arg )
{
   ProcessArgs *pa = (ProcessArgs*)arg;
   

#ifdef ENABLE_DEBUG
   int pid( getpid() );   
   
   cout << "Scan MOUNT_PT dir=\"" << pa->directory
        << "\" pid=" << pid << endl;   
#endif   
   
   
   // Do a deep scan
   pa->results += scanDirectory( pa->directory );

   
#ifdef ENABLE_DEBUG
   cout << "Done with \"" << pa->directory
        << "\" pid=" << pid << " writing cache to the file..."
        << endl;      
#endif   


   // Write current cache to the file
   pa->results += writeCache();

   
   pa->finished = true;
   
   return NULL;
}

   
INT_4U Counter( 0 );   
ReadWriteLock::lock_type CounterLock;
   
   
void check_thread( TIDInfo& thread_info )
{
   // Start new thread if it's not started yet
   if( thread_info.args.started == false )
   {
      // Create new thread:
      cout << "Start thread for " << thread_info.args.directory << endl;
   
      assert( pthread_create( &( thread_info.id ), 
                              NULL,
                              _process,
                              &( thread_info.args ) ) == 0 );
   
      thread_info.args.started = true;
   }
   else if( thread_info.args.finished == true )
   {
      // Thread finished, was it counted as "processed thread"?
      if( thread_info.args.checked == false )
      {
         assert( pthread_join( thread_info.id, 0 ) == 0 );   
         thread_info.args.checked = true;
   
         // WRITE-lock the counter
         ReadWriteLock lock( CounterLock, ReadWriteLock::WRITE );
         ++Counter;
   
         cout << "Counter=" << Counter << " after " << thread_info.args.directory << endl;
      }
   }

   
   return;
}

   
   
int main()
{
   General::ReadWriteLock::Initialize( CounterLock );
   
   
   // To be consistent with genericAPI:
   pthread_setcanceltype( PTHREAD_CANCEL_ASYNCHRONOUS, 0 );
   
   // Do not want any signal handling to happen.
   sigset_t	mask;

   sigemptyset(&mask);
   pthread_sigmask(SIG_SETMASK, &mask, NULL);

   
   // From LDAS-DEV
   const CHAR* mount_pt_list( "/ldas_outgoing/mirror/frames /ldas_outgoing/mirror/dmt /ldas_outgoing/frames /ldas_outgoing/test/frames /ldas_outgoing/test/test_uncompressed /data/ide1/LHO /data/ide1/LLO /data/ide2/LHO /data/ide2/LLO /data/ide3/LHO /data/ide3/LLO /data/ide4/LHO /data/ide4/LLO /data/ide5/LHO /data/ide5/LLO /data/ide6/LHO /data/ide6/LLO /data/ide7/LHO /data/ide7/LLO /data/ide8/LHO /data/ide8/LLO /data/ide9/LHO /data/ide9/LLO /data/ide10/LHO /data/ide10/LLO /data/ide11/LHO /data/ide11/LLO /data/ide12/LHO /data/ide12/LLO /data/ide15/LHO /data/ide15/LLO /data/ide16/LHO /data/ide16/LLO /data/ide17/LHO /data/ide17/LLO /archive/S3 /archive/LLO /archive/LHO /archive/GEO/full/S1" );
   
   // Only IDE's
   // const CHAR* mount_pt_list( "/data/ide1/LHO /data/ide1/LLO /data/ide2/LHO /data/ide2/LLO /data/ide3/LHO /data/ide3/LLO /data/ide4/LHO /data/ide4/LLO /data/ide5/LHO /data/ide5/LLO /data/ide6/LHO /data/ide6/LLO /data/ide7/LHO /data/ide7/LLO /data/ide8/LHO /data/ide8/LLO /data/ide9/LHO /data/ide9/LLO /data/ide10/LHO /data/ide10/LLO /data/ide11/LHO /data/ide11/LLO /data/ide12/LHO /data/ide12/LLO /data/ide15/LHO /data/ide15/LLO /data/ide16/LHO /data/ide16/LLO /data/ide17/LHO /data/ide17/LLO" );


   // Gappy data only
   //const CHAR* mount_pt_list( "/data/ide1/S2 /data/ide2/S2 /data/ide3/S2 /data/ide4/S2 /data/ide5/S2 /data/ide6/S2 /data/ide7/S2 /data/ide8/S2 /data/ide9/S2 /data/ide10/S2 /data/ide11/S2 /data/ide12/S2 /data/ide15/S2 /data/ide16/S2 /data/ide17/S2" );
   
   
   list< string > dirs;
   istringstream dirs_stream( mount_pt_list );   
   
   copy( istream_iterator< string >( dirs_stream ),
         istream_iterator< string >(), 
         back_inserter( dirs ) );  

   // How many MOUNT_PT entries are to scan
   const size_t num_mount_pts( dirs.size() );
   cout << "Size = " << num_mount_pts << endl;
   
   
   // Thread specific data
   vector< TIDInfo > threads_info( num_mount_pts );
   
   // Index into thread vector
   vector< TIDInfo >::iterator thread_iter( threads_info.begin() );
   
   for( list< string >::const_iterator iter = dirs.begin(), end_iter = dirs.end();
        iter != end_iter; ++iter )
   {
      thread_iter->args.started  = false;   
      thread_iter->args.finished = false;
      thread_iter->args.checked  = false;   
      thread_iter->args.directory = ( *iter );
   
      ++thread_iter;
   }
   

   //copy( dirs.begin(), dirs.end(), ostream_iterator< string >( cout, " " ) );
   
   
   // Set MOUNT_PT list
   string mount_entries( updateMountPtList( mount_pt_list ) );
   cout << "Set MOUNT_PT list: " << mount_entries << endl;
   
   
   // Scan each entry in the list, write cache to the file after each scan
   while( 1 )
   {
      for_each( threads_info.begin(), threads_info.end(), check_thread );

   
      // Check how many threads have finished
      {
         // Read lock the counter
         ReadWriteLock lock( CounterLock, ReadWriteLock::READ );
         if( Counter == num_mount_pts )
         {
            break;
         }
      }
   }   
   
   
   for( vector< TIDInfo >::const_iterator iter = threads_info.begin(),
           end_iter = threads_info.end(); iter != end_iter; ++iter )
   {
      cout << "\"" << iter->args.directory << "\": " << iter->args.results << endl;
   }
   
   return 0;
}
