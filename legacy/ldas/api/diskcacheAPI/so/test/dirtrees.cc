// System Header Files
extern "C" {
#include <unistd.h>
#include <fcntl.h>
}

#include <stdexcept>
#include <sstream>   
#include <algorithm>   
#include <iterator>   
#include <string>   
#include <list>   
   

// General Header Files   
#include "general/unittest.h"
#include "general/System.hh"
#include "general/types.hh"   

#include "genericAPI/StatFork.hh"
#include "genericAPI/StatPool.hh"

// API header files
#include "Commands.hh"
#include "diskcachecmd.hh"
#include "diskcacheAPI.hh"
#include "MountPointScanner.hh"
#include "TCL.hh"
   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true       
   
General::UnitTest	Test;

using namespace diskCache::Commands;

using diskCache::MountPointScanner;

typedef MountPointManagerSingleton::mount_point_name_container_type
mount_point_container_type;
   
namespace 
{
  class FrameDir
  {
  public:
    typedef struct s_frame_desc_type {
      INT_8U s_frame_start;
      INT_8U s_frame_dt;
      INT_4U s_frame_count;

      inline s_frame_desc_type( INT_8U Start,
				INT_8U Dt,
				INT_4U Count )
	: s_frame_start( Start ),
	  s_frame_dt( Dt ),
	  s_frame_count( Count )
      {
      }
    } frame_desc_type;
    typedef std::list< frame_desc_type > frame_desc_container_type;

    FrameDir( const std::string& Path,
	      const std::string& FrameDesc,
	      INT_8U FrameStart,
	      INT_8U FrameDt,
	      INT_4U Count );

    FrameDir( const std::string& Path,
	      const std::string& FrameDesc,
	      const frame_desc_container_type& FrameList );

    ~FrameDir( );

    inline const std::string& CWD( ) const
    {
      return m_cwd;
    }

    void Instantiate( );
  private:
    class filename_info
    {
    public:
      filename_info( const FrameDir& Dir );
      virtual ~filename_info( );
      void Eval( const std::string& DirectoryPath );
      virtual void operator()( const std::string& Filename ) = 0;
    private:
      const FrameDir&	m_frame_dir_ref;
    };

    class create_filename
      : public filename_info
    {
    public:
      create_filename( const FrameDir& Dir );
      virtual void operator()( const std::string& Filename );
    };

    class destroy_filename
      : public filename_info
    {
    public:
      destroy_filename( const FrameDir& Dir );
      virtual void operator()( const std::string& Filename );
    };

    friend void filename_info::Eval( const std::string& );

    std::string	m_cwd;
    std::string	m_frame_desc;
    frame_desc_container_type	m_frame_info;

    std::string	m_parent;

    void create( const std::string& Filename );
  };
      
  void two_second_bug( );
  void env_mount_point( );
}

char	CurrentWorkingDir[ 1024 ];

int
main( int argc, char **argv )
{
  //---------------------------------------------------------------------
  // Setup for testing
  //---------------------------------------------------------------------
  Test.Init( argc, argv );

  diskCache::Cache::SDGTx::RegisterSearchEngine( );

  try
  {
    //------------------------------------------------------------------
    // Establish polling method
    //------------------------------------------------------------------
    diskCache::RWLOCK_TIMEOUT = 0;
    //------------------------------------------------------------------
    // Establish the current working directory
    //------------------------------------------------------------------
    if ( getcwd( CurrentWorkingDir, sizeof( CurrentWorkingDir ) ) == (char*)NULL )
    {
      std::ostringstream	msg;

      msg << "getwd failed: " << General::System::ErrnoMessage( )
	;
      throw std::runtime_error( msg.str( ) );
    }
    //------------------------------------------------------------------
    // Start testing
    //------------------------------------------------------------------
    two_second_bug( );
    env_mount_point( );

  }
  catch( const std::exception& Exception )
  {
    Test.Check( false )
      << "Caught std::exception: " << Exception.what( )
      << std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << "Caught an unexpected exception" << std::endl;
  }

  //---------------------------------------------------------------------
  // Exit with PASS/FAIL
  //---------------------------------------------------------------------
  Test.Exit();
   
  return 0;   
}

namespace
{
  //=====================================================================
  // Classes
  //=====================================================================
  //---------------------------------------------------------------------
  FrameDir::filename_info::
  filename_info( const FrameDir& Dir )
    : m_frame_dir_ref( Dir )
  {
    
  }

  FrameDir::filename_info::
  ~filename_info( )
  {
  }

  void FrameDir::filename_info::
  Eval( const std::string& DirectoryPath )
  {
    std::ostringstream	fn;
    for ( frame_desc_container_type::const_iterator
	    cur = m_frame_dir_ref.m_frame_info.begin( ),
	    last = m_frame_dir_ref.m_frame_info.end( );
	  cur != last;
	  ++cur )
    {
      INT_8U		start = cur->s_frame_start;

      for( INT_4U x = 0; x < cur->s_frame_count; ++x )
      {
	fn.str( "" );
	fn << DirectoryPath << "/"
	   << m_frame_dir_ref.m_frame_desc << "-"
	   << start << "-"
	   << cur->s_frame_dt
	   << ".gwf";
	operator()( fn.str( ) );
	start += cur->s_frame_dt;
      }
    }
  }
  //---------------------------------------------------------------------
  FrameDir::create_filename::
  create_filename( const FrameDir& Dir )
    : filename_info( Dir )
  {
  }

  void FrameDir::create_filename::
  operator()( const std::string& Filename )
  {
    int fd = ::open( Filename.c_str( ), O_RDWR | O_CREAT, 0660 );
    if ( fd > 0 )
    {
      ::close( fd );
    }
  }
  //---------------------------------------------------------------------
  FrameDir::destroy_filename::
  destroy_filename( const FrameDir& Dir )
    : filename_info( Dir )
  {
  }

  void FrameDir::destroy_filename::
  operator()( const std::string& Filename )
  {
    ::unlink( Filename.c_str( ) );
  }
  //---------------------------------------------------------------------
  FrameDir::
  FrameDir( const std::string& Path,
	    const std::string& FrameDesc,
	    INT_8U FrameStart,
	    INT_8U FrameDt,
	    INT_4U Count )
    : m_cwd( Path ),
      m_frame_desc( FrameDesc ),
      m_parent( "." )
  {
    m_frame_info.push_back( frame_desc_type( FrameStart, FrameDt, Count ) );
  }

  FrameDir::
  FrameDir( const std::string& Path,
	    const std::string& FrameDesc,
	    const frame_desc_container_type& FrameList )	    
    : m_cwd( Path ),
      m_frame_desc( FrameDesc ),
      m_frame_info( FrameList ),
      m_parent( "." )
  {
  }

  FrameDir::
  ~FrameDir( )
  {
    //-------------------------------------------------------------------
    // Calculate the name of the directory
    //-------------------------------------------------------------------
    std::string		full_path;

    full_path = m_parent + "/" + m_cwd;
    //-------------------------------------------------------------------
    // Remove the files
    //-------------------------------------------------------------------
    destroy_filename	df( *this );

    df.Eval( full_path );

    //-------------------------------------------------------------------
    // Remove the directory
    //-------------------------------------------------------------------
    rmdir( full_path.c_str( ) );
  }

  void FrameDir::
  Instantiate( )
  {
    //-------------------------------------------------------------------
    // Calculate the name of the directory
    //-------------------------------------------------------------------
    std::string		full_path;

    full_path = m_parent + "/" + m_cwd;
    //-------------------------------------------------------------------
    // Create the directory
    //-------------------------------------------------------------------
    mkdir( full_path.c_str( ), 0777 );

    //-------------------------------------------------------------------
    // Create the files
    //-------------------------------------------------------------------
    create_filename	cf( *this );

    cf.Eval( full_path );
  }

  void FrameDir::
  create( const std::string& Filename )
  {
    
  }

  //=====================================================================
  // Helper functions
  //=====================================================================

  void
  ScanMountPoints( const mount_point_container_type& MountPoints )
  {
    diskCache::Cache::SDGTx::file_extension_container_type
      extensions;

    extensions.push_back( ".gwf" );

    updateFileExtList( extensions );

    MountPointManagerSingleton::UpdateResults	status;
    MountPointScanner::ScanResults		results;

    updateMountPtList( status, MountPoints );

    //-------------------------------------------------------------------
    // Setup the scanner
    //-------------------------------------------------------------------
    MountPointScanner::Concurrency( 1 );
    MountPointScanner::Scan( MountPoints, results );
    diskCache::TCL::Translate( std::cerr, results );
  }

  //=====================================================================
  // Tests
  //=====================================================================
  //---------------------------------------------------------------------
  void
  two_second_bug( )
  {
    //-------------------------------------------------------------------
    // Create directory
    //-------------------------------------------------------------------
    FrameDir::frame_desc_container_type	fd;

    fd.push_back( FrameDir::s_frame_desc_type( 820063693, 16, 2 ) );
    fd.push_back( FrameDir::s_frame_desc_type( 820063725, 2, 1 ) );
    fd.push_back( FrameDir::s_frame_desc_type( 820063727, 32, 2 ) );

    FrameDir	top( "two_second_bug", "H-H2_RDS_C02_LX", fd );

    top.Instantiate( );

    //-------------------------------------------------------------------
    // Generate the hash
    //-------------------------------------------------------------------
    std::ostringstream			mountpts;
    diskCache::Cache::QueryAnswer	answer;
    std::string				answer_str;

    mountpts << CurrentWorkingDir << "/" << top.CWD( );

    mount_point_container_type	mounts;

    mounts.push_back( mountpts.str( ) );

    ScanMountPoints( mounts );

    getDirCache( answer, "all", "all" );
#if WORKING
    diskCache::TCL::Translate( answer, answer_str );
    std::cerr << "DEBUG: getDirCache( all, all ): "
	      << answer_str
	      << std::endl;

    getDirCache( answer, "H", "all" );
    diskCache::TCL::Translate( answer, answer_str );
    std::cerr << "DEBUG: getDirCache( H, all ): "
	      << answer_str
	      << std::endl;
#if 0
    //-------------------------------------------------------------------
    // Scan for the file
    //-------------------------------------------------------------------
    std::cerr << "DEBUG: getFrameFiles:"
	      << " " << getFrameFiles( "H", "H2_RDS_C02_LX",
				       820063725, 820063726,
				       ".gwf",
				       true )
	      << std::endl;
    std::cerr << "DEBUG: getRDSFrameFiles (non-resampled):"
	      << " " << getRDSFrameFiles( "H", "H2_RDS_C02_LX",
					  820063725, 820063726,
					  ".gwf",
					  false )
	      << std::endl;
    std::cerr << "DEBUG: getRDSFrameFiles (resampled):"
	      << " " << getRDSFrameFiles( "H", "H2_RDS_C02_LX",
					  820063725, 820063726,
					  ".gwf",
					  true )
	      << std::endl;
#endif /* 0 */
#endif /* WORKING */
  }

  void
  env_mount_point( )
  {
    static const char* const method_name = "env_mount_point";

    const char* const MOUNT_POINT_LIST = ::getenv( "MOUNT_POINT_LIST" );

    if ( MOUNT_POINT_LIST == (const char* const)NULL )
    {
      Test.Message( ) << "Test " << method_name << " skipped because the environment variable MOUNT_POINT_LIST is unassigned"
		      << std::endl;
      return;
    }

    std::string	mount_points( MOUNT_POINT_LIST );

    mount_point_container_type	mounts;

    mounts.push_back( mount_points );

    ScanMountPoints( mounts );
  }
}
