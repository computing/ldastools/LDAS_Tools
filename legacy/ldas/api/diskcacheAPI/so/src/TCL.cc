#include <iomanip>
#include <list>
#include <string>

#include "genericAPI/TCL.hh"

#include "diskcacheAPI/Cache/QueryAnswer.hh"

#include "MountPointManager.hh"
#include "MountPointManagerSingleton.hh"
#include "MountPointScanner.hh"

using diskCache::MountPointManager;
using diskCache::MountPointScanner;

namespace
{
   std::string
   ms_format( const double DT )
   {
      std::ostringstream	msg;

      INT_4U sec( (INT_4U)DT );
      INT_4U hrs( sec / 3600 );
      sec %= 3600;
      INT_4U min( sec / 60 );
      sec %= 60;
      INT_4U ms = INT_4U( ( DT - INT_4U( DT ) ) * 1000000.0 );


      msg.fill( '0' );

      msg << std::setw( 2 ) << hrs
          << ":" << std::setw( 2 ) << min
          << ":" << std::setw( 2 ) << sec
          << "." << std::setw( 3 ) << ms
         ;

      return msg.str( );
   }

  class mpm_scr_functor
  {
  public:
    typedef MountPointManager::ScanResults::dir_state dir_state;
    mpm_scr_functor( std::ostream& Stream );

    bool Empty( ) const;

    void operator( )( const std::string& Dir,
		      dir_state State,
		      INT_4U Added, INT_4U Deleted );

  private:
    bool		m_first;
    std::ostream&	m_stream;
  };

  mpm_scr_functor::
  mpm_scr_functor( std::ostream& Stream )
    : m_first( true ),
      m_stream( Stream )
  {
  }

  inline bool mpm_scr_functor::
  Empty( ) const
  {
    return m_first;
  }

  inline void mpm_scr_functor::
  operator( )( const std::string& Dir,
	       dir_state DirState,
	       INT_4U Added,
	       INT_4U Deleted )
  {
    using ::diskCache::MountPointManager;

    if ( ( Added == 0 )
	 && ( Deleted == 0 ) )
    {
      //-----------------------------------------------------------------
      // There is nothing to be done.
      //-----------------------------------------------------------------
      return;
    }

    if ( m_first )
    {
      m_first = false;
    }
    else
    {
      m_stream << " ";
    }
    m_stream
      << "{ " << Dir
      ;
    switch( DirState )
    {
    case MountPointManager::ScanResults::DIRECTORY_STATE_DELETED:
      m_stream << " REMOVED";
      break;
    case MountPointManager::ScanResults::DIRECTORY_STATE_MODIFIED:
      m_stream << " UPDATED";
      break;
    case MountPointManager::ScanResults::DIRECTORY_STATE_NEW:
      m_stream << " NEW";
      break;
    }
    m_stream << " DIRECTORY WITH ";

    if ( Deleted > 0 )
    {
      m_stream << Deleted << " " << ( ( Deleted == 1 ) ? "FILE" : "FILES" )
	       << " REMOVED"
	;
    }
    if ( Added > 0 )
    {
      m_stream << Added << " " << ( ( Added == 1 ) ? "FILE" : "FILES" )
	       << " ADDED"
	;
    }
    m_stream << " }";
  }

} // namespace - anonymous

namespace
{
  template< typename IN, typename OUT >
  void format( const IN& Input, OUT& Output );

  template<>
  inline void
  format( const MountPointManager::UpdateResults& Results,
	  std::ostream& Output )
  {
#if 0
    ::GenericAPI::TCL::List( Output,
			    Results.s_deleted.begin( ),
			    Results.s_deleted.end( ) );
    Output << " ";
#endif /* 0 */
    ::GenericAPI::TCL::List( Output,
			     Results.s_errors.begin( ),
			     Results.s_errors.end( ) );
    Output << " ";
#if WORKING
#else /* WORKING */
    Output << "{ }";	// Errors
#endif /* WORKING */
  }

  template<>
  inline void
  format( const MountPointManager::ScanResults& Results,
	  std::ostream& Output )
  {
    mpm_scr_functor	f( Output );

    Results( f );

    if ( f.Empty( ) )
    {
      Output
	<< "{ }"
	;
    }
  }

  template<>
  inline void
  format( const MountPointScanner::ScanResults& Results,
	  std::ostream& Output )
  {
    MountPointScanner::ScanResults::timer_delta_type
      dt( Results.Stop( ) );
    INT_4U
      ms( INT_4U( dt * 1000 ) );

    Output
      << Results.MountPointCount( ) << " mount points, "
      << Results.DirectoryCount( ) << " directories, "
      << Results.FileCount( ) << " files,"
      << " scanned in " << ms << " ms "
      << "(" << ms_format( dt ) << ")."
      ;
  }
}

namespace GenericAPI
{
  namespace TCL
  {
    template<>
    void
    Translate( std::string& Answer,
	       const diskCache::Cache::QueryAnswer& QueryResult )
    {
      std::ostringstream	msg;

      List<std::ostream>( msg,
			  QueryResult.Filenames( ).begin( ),
			  QueryResult.Filenames( ).end( ) );
      msg << " ";
      List<std::ostream>( msg,
			  QueryResult.Errors( ).begin( ),
			  QueryResult.Errors( ).end( ) );

      Answer = msg.str( );
    }

    template<>
    void
    Translate( std::ostream& Output,
	       const MountPointManager::UpdateResults& Results )
    {
      format( Results, Output );
      Output << std::endl
	;
    }

    template<>
    void
    Translate( std::string& Output,
	       const MountPointManager::UpdateResults& Results )
    {
      std::ostringstream	msg;

      format<MountPointManager::UpdateResults,std::ostream>( Results, msg );

      Output = msg.str( );
	;
    }

    template<>
    void
    Translate( std::string& Output,
	       const MountPointManager::ScanResults& Results )
    {
      std::ostringstream	msg;

      format<MountPointManager::ScanResults,std::ostream>( Results, msg );

      Output = msg.str( );
	;
    }

    template<>
    void
    Translate( std::ostream& Output,
	       const MountPointScanner::ScanResults& Results )
    {
      format( Results, Output );
      Output << std::endl
	;
    }

    template<>
    void
    Translate( std::string& Output,
	       const MountPointScanner::ScanResults& Results )
    {
      std::ostringstream	msg;

      format<MountPointScanner::ScanResults,std::ostream>( Results, msg );

      Output = msg.str( );
    }

  } // namespace - GenericAPI::TCL
} // namespace - diskCache
