// -*-Mode: C++;-*-

//#! commandtype="EVENT_MONITOR"

//!ignore_begin:

%include "dc_tcl.swig"

%{
#include "genericAPI/config.h"

#include "genericAPI/SymbolMapper.hh"
#include "genericAPI/SymbolMapperTCL.hh"

#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "diskcacheAPI.hh"
#include "diskcachecmd.hh"
#include "DumpCacheDaemon.hh"
#include "MountPointScanner.hh"
#include "ScanMountPointsDaemon.hh"

  using namespace std;

  typedef diskCache::Cache::ExcludedDirectoriesSingleton::
    directory_container_type directory_container_type;

  static void
  SetCacheWriteDelay( INT_4U Value )
  {
    diskCache::DumpCacheDaemon::Interval( Value );
  }

  static void
  SetCacheWriteDelaySeconds( INT_4U Value )
  {
    diskCache::DumpCacheDaemon::Interval( Value * 1000 );
  }

  static void
  SetFilenameBinary( const std::string& Filename )
  {
    diskCache::DumpCacheDaemon::FilenameBinary( Filename );
  }

  static void
  SetFilenameAscii( const std::string& Filename )
  {
    diskCache::DumpCacheDaemon::FilenameAscii( Filename );
  }

  static void
  set_concurrency( diskCache::MountPointScanner::concurrency_type Value )
  {
    diskCache::MountPointScanner::Concurrency( Value );
  }

  static void
  set_excluded_dirs( const std::list< std::string >& Value )
  {
    diskCache::Cache::ExcludedDirectoriesSingleton::Update( Value );
  }

  static void
  set_mount_pt( const std::list< std::string >& Value )
  {
    diskCache::MountPointManagerSingleton::UpdateResults
      results;

    diskCache::MountPointManagerSingleton::Update( Value,
						   results );
  }

  static void
  set_scan_mount_points_daemon_interval( diskCache::ScanMountPointsDaemon::
					 interval_type Value )
  {
    diskCache::ScanMountPointsDaemon::Interval( Value );
  }

%}

%init %{
  //---------------------------------------------------------------------
  // Initialize the bulk of the diskcache
  //---------------------------------------------------------------------
  diskCache::Initialize( );
%}

//!ignore_end:
%define DOCSTRING_DumpCacheDaemonStart
"Launch daemon process responsible for updating backup versions of cache"
%enddef
#if defined(SWIGPYTHON)
%feature("autodoc", "1")
%feature(DOCSTRING_DumpCacheDaemonStart)
#else
//------------------------------------------------------------------------------
//: Launch daemon process responsible for updating backup versions of cache
//
//!usage: DumpCacheDaemonStart
//
//------------------------------------------------------------------------------
#endif
void DumpCacheDaemonStart( );

//------------------------------------------------------------------------------
//: Continuously scan list of mount points for changes.
//
// This is the main task to start scanning for new information that should
// be cached.
//
//!usage: ScanMountPointListContinuously
//
//------------------------------------------------------------------------------
void ScanMountPointListContinuously( );

//------------------------------------------------------------------------------
//
//: Invoke "slow" scan (recursive collection of subdirectories and 
//: frame data files) on directory listed in MOUNT_PT list (rsc file variable).
//   
// This function gets recursive content of the passed to it directory. It
// should be called only for directories that appear in the MOUNT_PT list.   
// The same function is used to introduce new MOUNT_PT directory or to rescan
// already existing directory in the global hash for new data.
//   
//!usage: set string [ getDirEnt dirpath ]
//   
//!param: const char* dirpath - A full path to the directory.
//   
//!return: const string - A list of all identified and scanned subdirectories
//+        that have frame data.   
//      
string getDirEnt( const string dirpath );

//------------------------------------------------------------------------------
//
//: Invoke "slow" scan (recursive collection of subdirectories and 
//: frame data files) on directory listed in MOUNT_PT list 
//: (rsc file variable). - Threaded.   
//   
// This function gets recursive content of the passed to it directory. It
// should be called only for directories that appear in the MOUNT_PT list.   
// The same function is used to introduce new MOUNT_PT directory or to rescan
// already existing directory in the global hash for new data.
//      
//!usage: set tidp [ getDirEnt_t dirpath ]
//   
//!param: const char* dirpath - A full path to the directory.
//   
//!return: tid* tidp - Thread pointer.
//         
tid* getDirEnt_t( const char* dirpath,
                  Tcl_Interp* interp = 0, const char* flag = "" );   

           
//------------------------------------------------------------------------------
//
//: Invoke "slow" scan (recursive collection of subdirectories and 
//: frame data files) on directory listed in MOUNT_PT list 
//: (rsc file variable). - Thread return value.
//   
// This function gets recursive content of the passed to it directory. It
// should be called only for directories that appear in the MOUNT_PT list.   
// The same function is used to introduce new MOUNT_PT directory or to rescan
// already existing directory in the global hash for new data.
//      
//!usage: set string [ getDirEnt_r tidp ]
//   
//!param: tid* tidp - A pointer to the thread.
//   
//!return: const string - A list of all identified and scanned subdirectories
//+        that have frame data.      
//            
string getDirEnt_r( tid* t );   

   
%inline %{
#if OLD
  //---------------------------------------------------------------------
  //   
  //: Get TCL formatted lists for each mount point with name or mount point,
  //: number of directories and number of files for data matching the specified
  //: ifo and type.  Return lists are of the form:
  //:   {mountpoint_name number_of_dirs number_of_files }
  //
  //!usage: set string [ getHashNumbers ]
  //
  //!param: const char* ifo - An ifo to look up. Default is "all".
  //!param: const char* type - A type to look up. Default is "all".
  //
  //!return: string - A list for each mount point with name, number of 
  //+        directories and number of files under that mount point:
  //+        {mountpoint_name number_of_dirs number_of files }
  //   
  string
  getHashNumbers( const char* ifo = "all", const char* type = "all" )
  {
  }
#endif /* OLD */
%}

   
   
//-----------------------------------------------------------------------
//   
//: Write content of global frame data hash to binary file.   
//
//!usage: writeDirCache file
//   
//!param: const char* filename - Name of the file to write frame hash to.
//+       Default is an empty string (C++ will use default file name).      
//
//!return: Nothing.
//
void writeDirCache( const char* filename = "",
		    diskCache::Streams::Interface::version_type Version
		    = diskCache::Streams::Interface::VERSION_NONE );   
   
tid* writeDirCache_t( const char* filename = "",
		      diskCache::Streams::Interface::version_type Version
		      = diskCache::Streams::Interface::VERSION_NONE,
		      Tcl_Interp* interp = 0, const char* flag = "" );
void writeDirCache_r( tid* TID );
   
//-----------------------------------------------------------------------
//   
//: Write content of global frame data hash to ascii file.   
//
//!usage: writeDirCache file
//   
//!param: const char* filename - Name of the file to write frame hash to.
//+       Default is an empty string (C++ will use default file name).      
//
//!return: Nothing.
//
void writeDirCacheAscii( const char* filename = "",
			 diskCache::Streams::Interface::version_type Version
			 = diskCache::Streams::Interface::VERSION_NONE );   
   
tid* writeDirCacheAscii_t( const char* filename = "",
			   diskCache::Streams::Interface::version_type Version
			   = diskCache::Streams::Interface::VERSION_NONE,
			   Tcl_Interp* interp = 0, const char* flag = "" );
void writeDirCacheAscii_r( tid* TID );
   
//------------------------------------------------------------------------------
//   
//: Write content of global frame data hash to binary and ascii file.
//
//!usage: writeDirCacheFiles file
//   
//!param: const char* binary_filename - Name of the file to write
//+       binary frame hash to.
//+       Default is an empty string (C++ will use default file name).      
//
//!param: const char* ascii_filename - Name of the file to write
//+       ascii frame hash to.
//+       Default is an empty string (C++ will use default file name).      
//
//!return: Nothing.
//
void writeDirCacheFiles( const char* bfilename = "",
			 const char* afilename = "" );
   
tid* writeDirCacheFiles_t( const char* bfilename = "",
			   const char* afilename = "",
			   Tcl_Interp* interp = 0, const char* flag = "" );
void writeDirCacheFiles_r( tid* TID );
   
//------------------------------------------------------------------------------
//   
//: Read content of global frame data hash from binary file. - Threaded.
//
//!usage: set tid [ CacheRead_t filename ]
//   
// Tcl layer can specify different files for read and write operations. 
// This function forces all read and write operations to be sequencial.
//   
// ATTN: This function destroys existing hash before reading a new one from the
//       given file. Caller must assure there are no running threads that might
//       access global frame data hash at the time "CacheRead" is called.
//    
//!param: const char* filename - Name of the file to read frame hash from.
//+       Default is an empty string (C++ will use default file name).      
//
//!return: tid* - Thread pointer.
//
tid* CacheRead_t( const char* filename = "",
                     Tcl_Interp* interp = 0, const char* flag = "");      
   
   
//------------------------------------------------------------------------------
//   
//: Read content of global frame data hash from binary file. - Thread return value.
//
//!usage: CacheRead_r tidp
//   
// Tcl layer can specify different files for read and write operations. 
// This function forces all read and write operations to be sequencial.
//   
// ATTN: This function destroys existing hash before reading a new one from the
//       given file. Caller must assure there are no running threads that might
//       access global frame data hash at the time "CacheRead" is called.
//    
//!param: tid* - Thread id.
//
//!return: Nothing.
//
void CacheRead_r( tid* );         
   
   
//------------------------------------------------------------------------------
//   
//: Update list of excluded directories (as they appear in
//: the resource file): check if existing data hash relies on such directories
//:                     already, remove those directories recursively from
//:                     global hash.   
//
//!usage: set dir_list [ excludedDirList dirs_to_exclude ]
//   
//!param: const CHAR* dir_list - A list of directories to exclude (as they appear
//+       in API resource file variable).
//   
//!return: string - Sorted Tcl list of all removed subdirectories, followed
//+        by error messages if any:
//+        {Directories excluded by pattern 'dir_list': dir1 dir2 ...} {error1 error2 ...}
//         
string excludedDirList( const directory_container_type& dir_list );


//------------------------------------------------------------------------------
//   
//: Delete global frame data hash.
//
//!usage: deleteDirCache
//   
// ATTN: This function destroys existing hash in memory. 
//       Caller must assure there are no running threads that might
//       access global frame data hash at the time "deleteDirCache" is called.
//    
//!return: Nothing.
//
void deleteDirCache();         

tid* deleteDirCache_t( Tcl_Interp* interp = 0, const char* flag = "" );   
void deleteDirCache_r( tid* t );   

 //-----------------------------------------------------------------------
 //   
 //: Bind a Tcl Varialbe to the C++ variable for mount pt conflict checking
//
//!usage: bindToCheckForMountPTConflictVariable varName
//
//!param: varName - name of variable to be used within TCL to control if
//+		new time hashes need to be verified agaist all existing
//+		time hashes.
//   
//!return: 1 if binding took place, 0 otherwise.
//
bool bindToCheckForMountPTConflictVariable( Tcl_Interp* Interp, char* Variable );

%inline %{
  int
  setRWLockAcquisitionTimeout( int Value )
  {
    int retval = diskCache::RWLOCK_TIMEOUT;

    diskCache::RWLOCK_TIMEOUT = Value;
    return retval;
  }

  //----------------------------------------------------------------------------
  //: Rebuild the frame cache from scratch
  //
  //   
  //!usage: RebuildCache
  //   
  //----------------------------------------------------------------------------
  void
  RequestCacheRebuild( )
  {
    diskCache::MountPointScanner::Rebuild( );
  }  
  
%}
