#include "config.h"

#include <time.h>

#include <iostream>
#include <memory>
#include <cerrno>

#include "general/mutexlock.hh"
#include "general/regex.hh"
#include "general/regexmatch.hh"

#include "ilwd/ldascontainer.hh"

#include "genericAPI/thread.hh"
#include "genericAPI/util.hh"

#include "DBError.hh"
#include "DBPull.hh"
#include "dbeasy.h"
#include "dbeasystmt.h"

namespace
{
  //: Parses out the table name from the SQL query
  //
  //!param: const std::string& SQL - Query in which to search for the table
  //+	name.
  //!return: std::string - name of the table
  std::string
  get_table_name( const std::string& SQL )
  {
    static const Regex	pattern( "^select [^\\(\\)]* from (\\S+)\\s*[^,]",
				 REG_EXTENDED | REG_ICASE );

    RegexMatch	regex_result;
    regex_result.match( pattern, SQL.c_str( ) );
    if ( regex_result.getNMatches( ) == 1 )
    {
      return regex_result.getSubString( 0 );
    }
    return "result_table";
  }

  void
  retry_sleep( const int Iteration )
  {
    struct timespec	req;
    struct timespec	rem;

    static const REAL_8	divisor = 3.0;
    static const REAL_8	engineering_constant = 5.0;
    static const REAL_8	average_job_length = 2.0;
    REAL_8		multiplier = 1;

    for ( int x = 0; x < Iteration; ++x )
    {
      multiplier *= engineering_constant;
    }

    REAL_8	sleep_time =
      ( average_job_length / divisor ) * multiplier;

    req.tv_sec = int( sleep_time );		// Get integer component
    sleep_time -= req.tv_sec;			// Subtract integer component
    req.tv_nsec = int(sleep_time * 1000000000);	// Multiply by a billion

    while( nanosleep( &req, &rem ) != 0 )
    {
      int error = errno;
      switch( error )
      {
      case EINTR:
	//---------------------------------------------------------------
	// An interupt has happened. This is a recoverable error.
	//---------------------------------------------------------------
	req.tv_sec = rem.tv_sec;
	req.tv_nsec = rem.tv_nsec;
	break;
      default:
	//---------------------------------------------------------------
	// An error has occured that prevents the proper execution.
	//   Just return to the use.
	//---------------------------------------------------------------
	return;
	break;
      }
    }
  }

  inline bool
  retry( int NumberOfTries, const char* Error, const Regex& Pattern )
  {
    RegexMatch	regex_result;

    if ( ( NumberOfTries == 0 ) ||
	   ( regex_result.match( Pattern, Error ) == false ) )

    {
      return false;
    }
    return true;
  }

} // namespace - anonymous

//-----------------------------------------------------------------------
// DBPull handles all read requests (those that do not modify) for the
// database. This includes SQL statements like SELECT (not SELECT INTO).
//
//!param: const std::string Database - Name of the database to use.
//!param: const std::string User - Name of the database user.
//!param: const std::string Password - Database password for User.
//!param: const std::string Query - SQL query statement to query the database.
//!param: const int MaximumResultRowCount - The maximum number of rows to
//+	retrieve from the database.
//!param: const int NumberOfTries - Maximum number of tries to obtain the
//+	information from the database
//!param: const std::string RetryRegex - Regular expression to use when an
//	error occurs to determine if the request should be retried.
//!param: bool RegisterILwd - true if the resulting ILwd should be
//+	registered.
//
//!return: std::string - This is a list of two elements. The first has any
//+	error message as a reult of multiple tries. The second is an ILwd
//+	containing the result data.
std::string metadataAPI::
DBPull( const std::string Database,
	const std::string User,
	const std::string Password,
	const std::string Query,
	const int MaximumResultRowCount,
	const int NumberOfTries,
	const std::string RetryRegex )
{
  //---------------------------------------------------------------------
  // Create a container to hold the result
  //---------------------------------------------------------------------
  int tries_remaining( NumberOfTries );

  std::auto_ptr< ILwd::LdasContainer >	result;
  const Regex	pattern( RetryRegex, REG_EXTENDED | REG_ICASE );
  //---------------------------------------------------------------------
  // Establish the connection and statement handles.
  //---------------------------------------------------------------------

  dbEasy	connection( Database, User, Password );
  dbEasyStmt	statement( connection );
  DBError	error;

  //---------------------------------------------------------------------
  //:TODO: Loop at most tries_remaining times
  //---------------------------------------------------------------------
  int iteration = 0;
  while ( tries_remaining )
  {
    --tries_remaining;
    try	
    {
      //-----------------------------------------------------------------
      // Initialize the container holding the results
      //-----------------------------------------------------------------
      result.reset( new ILwd::LdasContainer( "ligo:ldas:file" ) );
      ILwd::LdasContainer*
	table( new ILwd::LdasContainer( "ldas::table" ) );

      result->push_back( table,
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
      //-----------------------------------------------------------------
      // Initialize the table container with the table name and the query
      //-----------------------------------------------------------------
  
      table->setName( 1, get_table_name( Query ), true );
      table->setComment( Query );

      //-----------------------------------------------------------------
      // Prepare the statement
      //-----------------------------------------------------------------

      statement.Prepare( Query );

      //-----------------------------------------------------------------
      // :TODO: Do any parameter binding
      //-----------------------------------------------------------------
      //-----------------------------------------------------------------
      // Execute the statement
      //-----------------------------------------------------------------

      statement.Submit( );
      for ( int x = statement.GetNumberOfColumns( ); x > 0; x-- )
      {
	statement.Bind( x );
      }

      //-----------------------------------------------------------------
      // Find out if there are any results
      //-----------------------------------------------------------------

      bool truncated = statement.FetchMulti( MaximumResultRowCount );

      //-----------------------------------------------------------------
      // Move data into the container.
      //-----------------------------------------------------------------
      for ( int
	      col = 1,
	      maxcol = statement.GetNumberOfColumns( ); col <= maxcol; col++ )
      {
	table->push_back( statement.ReleaseBoundColumnData( col ),
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
      }

      //-----------------------------------------------------------------
      // Properly set the metadata
      //-----------------------------------------------------------------

      result->setMetadata( "rows", statement.GetNumberOfRows( ) );
      result->setMetadata( "cols", statement.GetNumberOfColumns( ) );
      result->setMetadata( "truncated", truncated );

      //-----------------------------------------------------------------
      // Done with the request
      //-----------------------------------------------------------------
      break;
    }
    catch( const LdasException& e )
    {
      std::ostringstream	err;

      err << e[ 0 ].getMessage( ) << " (SQL: " << Query << ")";
      error.AppendMessage( err.str( ) );
      if ( retry( tries_remaining, e[ 0 ].getMessage( ).c_str( ), pattern ) ==
	   false )
      {
	// Throw the complete list of error messages.
	// Throw the complete list of error messages.
	throw SwigException( error.GetMessage( ), __FILE__, __LINE__ );
      }
      retry_sleep( iteration );
      ++iteration;
    }
    catch( const SwigException& e )
    {
      std::ostringstream	err;

      err << e.getResult( ) << " (SQL: " << Query << ")";
      error.AppendMessage( err.str( ) );
      if ( retry( tries_remaining, e.getResult( ).c_str( ), pattern ) == false )
      {
	// Throw the complete list of error messages.
	throw SwigException( error.GetMessage( ), __FILE__, __LINE__ );
      }
      retry_sleep( iteration );
      ++iteration;
    }
    catch( const std::exception& e )
    {
      error.AppendMessage( e.what( ) );
      if ( retry( tries_remaining, e.what( ), pattern ) == false )
      {
	// Throw the complete list of error messages.
	throw SwigException( error.GetMessage( ), __FILE__, __LINE__ );
      }
    }
  }

  //---------------------------------------------------------------------
  // Return the ILwd Container
  //---------------------------------------------------------------------
  std::ostringstream	oss;
  oss << " { " << error.GetMessage( ) << " } "
      << makeTclPointer( result.get( ), "LdasContainer" );
  Registry::elementRegistry.registerObject( result.release( ) );

  return oss.str( );
}

#if HAVE_TCL
CREATE_THREADED7( metadataAPI::DBPull, std::string, \
		  const std::string /* Database */, \
		  const std::string /* User */, \
		  const std::string /* Password */, \
		  const std::string /* Query */, \
		  const int /* MaximumResultRowCount */, \
		  const int /* NumberOfTries */, \
		  const std::string /* RetryRegex */ )
#endif /* HAVE_TCL */

