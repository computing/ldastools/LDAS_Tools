/* -*- C++ -*- */
#ifdef __GNUG__
#pragma implementation "dbeasy.h"
#endif	/* __GNUG__ */

#include "config.h"

#include "dberr.hh"
#include "dbeasy.h"

//-----------------------------------------------------------------------
//!param: std::string Database - Name of the Database to connect to.
//!param: std::string User - Name of Database user.
//!param: std::string Password - Password for Database user.
//-----------------------------------------------------------------------
dbEasy::
dbEasy(std::string Database, std::string User, std::string Password)
  :  m_db(Database, User, Password),
     m_max_binding_count(0)
{
}

//-----------------------------------------------------------------------
dbEasy::
~dbEasy(void)
{
}

