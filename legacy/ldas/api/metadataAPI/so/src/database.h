/* -*- C++ -*- */

#ifndef DATABASE_H
#define	DATABASE_H

#ifdef __GNUG__
#pragma interface
#endif

#include <string>

#include "general/mutexlock.hh"

#include "odbc.h"

//-----------------------------------------------------------------------
//: Maintains information for connection to an ODBC database
//
class dbDatabase
{
public:
  //: Constructor
  dbDatabase(std::string DBName, std::string User, std::string Password);

  //: Desctructor
  ~dbDatabase(void);
  
  //: Close the database
  bool Close(void);

  //: Accessor
  SQLHENV GetHenv(void) const;

  //: Accessor
  SQLHDBC GetHdbc(void) const;

  //: Start a transaction
  void TransactionBegin( );

  //: End a transaction committing all changes
  void TransactionCommit( );

  //: End a transaction WITHOUT committing changes.
  void TransactionRollback( );

private:
  //: An ODBC Environment Handle
  static SQLHENV	m_henv;

  //: Number of open databases
  static int		m_ref_count;

  //: class wide lock
  //
  // m_lock is shared between all instances and is used to ensure only one
  // instance is modifying the critical sections (any of the other static
  // memebers of this class).
  static pthread_mutex_t	m_lock;
  
  // An ODBC Database Connection Handle
  SQLHDBC		m_hdbc;

  // Has the Open() method been called without the Close() method?
  bool			m_is_open;

  //: Constructor
  //
  // The default constructor is private to prevent others from using it.
  dbDatabase(void);

  //: Open the database
  bool open(std::string DBName, std::string Username, std::string Password);

  //: Determine if an error has happened
  void eval_retcode(std::string Filename, int LineNumber,
		    RETCODE Retcode) const;
};

// Retrieve the SQL environment handle.
//
//!return: const SQLHENV - Return the SQLHENV handle associated with this
//+                        database.
inline SQLHENV dbDatabase::
GetHenv(void) const
{
  return m_henv;
}

// Retrieve the SQL database connection handle.
//
//!return: const SQLHDBC - Return the SQLHDBC handle associated with this
//+                        database.
inline SQLHDBC dbDatabase::
GetHdbc(void) const
{
  return m_hdbc;
}

#endif	/* DATABASE_H */
