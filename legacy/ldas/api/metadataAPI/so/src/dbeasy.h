/* -*- C++ -*- */

#ifndef DBCONNECT_H
#define	DBCONNECT_H

#ifdef __GNUG__
#pragma interface
#endif

#include "database.h"

//-----------------------------------------------------------------------
// Rules to modify the source code so perceps will be happy with it.
//-----------------------------------------------------------------------

//!ppp: eval `cat "$ENV{SRCDIR}/p_rules.pl"`; # Perceps include file

//-----------------------------------------------------------------------
//: Maintain information about the database connection
//
class dbEasy
{
public:
  //: Create a new connection to a database.
  dbEasy(std::string Database, std::string User, std::string Password);
  //: Destroy a connection to a database
  ~dbEasy(void);
  //: Retrieve a handle to the dbDatabase instance.
  const dbDatabase& GetDB(void) const;
  //: Retrieve the maximum number of rows that can be returned.
  int GetMaxBindingCount(void) const;
  //: Set the maximum number of rows that can be returned.
  void SetMaxBindingCount(int Count);

  //: Start a transaction
  void TransactionBegin( );

  //: End a transaction committing all changes
  void TransactionCommit( );

  //: End a transaction WITHOUT committing changes.
  void TransactionRollback( );

private:
  //: Database connection.
  dbDatabase	m_db;
  //: maximum number of rows to return.
  int		m_max_binding_count;
};

//-----------------------------------------------------------------------
//
//!return: const dbDatabase& - reference to a dbDatabase object
//
inline const dbDatabase& dbEasy::
GetDB(void) const
{
  return m_db;
}

//-----------------------------------------------------------------------
//
//!return: int - number of rows that can be returned.
inline int dbEasy::
GetMaxBindingCount(void) const
{
  return m_max_binding_count;
}

//-----------------------------------------------------------------------
//
//!param: int Count - Number of rows to return.
inline void dbEasy::
SetMaxBindingCount(int Count)
{
  m_max_binding_count = (Count >= 0) ? Count : 0;
}


inline void dbEasy::
TransactionBegin( )
{
  m_db.TransactionBegin( );
}

inline void dbEasy::
TransactionCommit( )
{
  m_db.TransactionCommit( );
}

inline void dbEasy::
TransactionRollback( )
{
  m_db.TransactionRollback( );
}
#endif	/* DBCONNECT_H */
