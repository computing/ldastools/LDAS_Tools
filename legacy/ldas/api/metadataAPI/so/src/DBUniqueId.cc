#include "config.h"

#include <assert.h>

#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "DBUniqueId.hh"
#include "uniqueids.h"

using namespace metadataAPI;

#define MessageDebug( a, b ) std::cerr << a << std::endl

#if 0
#define AT( a ) MessageDebug( a << ": " << __FILE__ << " " << __LINE__ , 20 )
#else
#define AT( a )
#endif

// This is the number of uniqueid to place in the pool when the pool is
// reloaded. The current value is 100.
const INT_4U DBUniqueId::UNIQUE_ID_CACHE_SIZE = 100;

pthread_mutex_t			DBUniqueId::m_lock = PTHREAD_MUTEX_INITIALIZER;
std::queue< DBUniqueId::uniqueid_type >	DBUniqueId::m_ids;

DBUniqueId::
DBUniqueId( )
{
}

//-----------------------------------------------------------------------
//
// This functions retrieves a single unique id from the pool. If the
// pool is emtpy, then fill it by querying the database for another
// set of uniqueids.
//
//!param: const std::string& Database - database to connect to if pool
//+	needs to be filled.
//!param: const std::string& User - user to use to connect to database
//+	if pool needs to be filled.
//!param: const std::string& Password - database password to use if pool
//+	needs to be filled.
//
//!return: std::string - UniqueId appropriate for insertion into DB2
//
//-----------------------------------------------------------------------
std::string DBUniqueId::
GetUniqueId( const std::string& Database,
	     const std::string& User,
	     const std::string& Password )
{
  //---------------------------------------------------------------------
  // Ensure one at a time access
  //---------------------------------------------------------------------

  MutexLock	lock( m_lock );

  //---------------------------------------------------------------------
  // Check to see if que need to be filled
  //---------------------------------------------------------------------

  if ( queue_needs_filling( ) )
  {
    fill_queue( Database, User, Password );
  }

  std::string result( m_ids.front( ) );
  m_ids.pop( );

  return result;
}

//-----------------------------------------------------------------------
//
// This functions retrieves a group of unique ids from the pool. If the
// pool is emtpy, then fill it by querying the database for another
// set of uniqueids.
//
//!param: const std::string& Database - database to connect to if pool
//+	needs to be filled.
//!param: const std::string& User - user to use to connect to database
//+	if pool needs to be filled.
//!param: const std::string& Password - database password to use if pool
//+	needs to be filled.
//!param: int Count - Number of uniqueids to retrieve
//!param: std::vector< std::string >& UniqueIds - a vector of uniqueids
//+	 appropriate for insertion into DB2
//-----------------------------------------------------------------------
void DBUniqueId::
GetUniqueId( const std::string& Database,
	     const std::string& User,
	     const std::string& Password,
	     int Count,
	     std::vector< std::string >& UniqueIds )
{
  //---------------------------------------------------------------------
  // Ensure one at a time access
  //---------------------------------------------------------------------
  MutexLock	lock( m_lock );

  while ( --Count >= 0 )
  {
    if ( queue_needs_filling( ) )
    {
      fill_queue( Database, User, Password );
    }
    UniqueIds.push_back( m_ids.front( ) );
    m_ids.pop( );
  }
}

//-----------------------------------------------------------------------
// This method is responsible for filling the pool of uniqueids. It
// queries the database and stores the results in the pool.
//
//!param: const std::string& Database - database to connect
//!param: const std::string& User - user to use to connect to database
//!param: const std::string& Password - database password to use
//-----------------------------------------------------------------------
void DBUniqueId::
fill_queue( const std::string& Database,
	    const std::string& User,
	    const std::string& Password )
{
  //---------------------------------------------------------------------
  // Clear out any old values. This is needed when emptying the queue
  //   due to id aging.
  //---------------------------------------------------------------------
  while ( ! m_ids.empty( ) )
  {
    m_ids.pop( );
  }
  //---------------------------------------------------------------------
  // Fill the queue by submitting a query requesting a block of
  //   UniqueIds
  //---------------------------------------------------------------------
  
  dbEasy	connection( Database, User, Password );
  dbUniqueIds	query( connection, UNIQUE_ID_CACHE_SIZE );

  query.Bind( 1 );

  //---------------------------------------------------------------------
  // Get the list of unique ids
  //---------------------------------------------------------------------
  (void)query.FetchMulti( UNIQUE_ID_CACHE_SIZE );
  std::auto_ptr< ILwd::LdasElement >
    column_data( query.ReleaseBoundColumnData( 1 ) );
  const ILwd::LdasContainer*
    uniqueid_set( dynamic_cast< ILwd::LdasContainer* >( column_data.get( ) ) );
  if ( uniqueid_set )
  {
    //-------------------------------------------------------------------
    // Add the unique ids into the cache
    //-------------------------------------------------------------------
    for ( INT_4U
	    cur = 0,
	    end = uniqueid_set->size( );
	  cur != end;
	  ++cur )
    {
      //-----------------------------------------------------------------
      // Move the unique id into the queue
      //-----------------------------------------------------------------
      m_ids.push( std::string( static_cast< const ILwd::LdasArray< CHAR >* >
			       ( uniqueid_set->operator[]( cur ) )->getData( ),
			       DB2_UNIQUE_ID_SIZE ) );
    }
  }

  //---------------------------------------------------------------------
  // If after filling the queue is still empty, then throw an exception
  //---------------------------------------------------------------------
  if ( m_ids.empty( ) )
  {
    std::ostringstream	oss;
    
    oss << "Unable to obtain unique ids";
    
    throw std::runtime_error( oss.str( ) );
  }
}
