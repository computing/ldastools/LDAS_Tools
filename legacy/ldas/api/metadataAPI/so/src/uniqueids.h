/* -*- C++ -*- */

#ifndef UNIQUEIDS_H
#define	UNIQUEIDS_H

#include "canned.h"

//: Canned query to retrieve a set of unique ids from DB2.
class dbUniqueIds : public dbCannedQuery
{
public:
  //: Constructor
  dbUniqueIds(const dbEasy& Env, int Count);
};

#endif	/* UNIQUEIDS_H */

