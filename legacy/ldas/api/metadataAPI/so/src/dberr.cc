#ifdef __GNUG__
#pragma implementation "dberr.hh"
#endif

#include "config.h"

#include <iostream>
#include <sstream>
#include "dbeasy.h"
#include "dberr.hh"

//!param: const std::string& Filename - Filename where error origionated.
//!param: int LineNumber - Line number where error origionated.
//!param: SQLHENV Henv - SQLHENV handle for error message.
//!param: SQLHDBC Hhdbc - SQLHHDBC handle for error message.
//!param: SQLHSTMT Hstmt - SQLHSTMT handle for error message.

dbErr::
dbErr( const std::string& Filename, int LineNumber,
       SQLHENV Henv, SQLHDBC Hdbc, SQLHSTMT Hstmt,
       int ErrorCode )
{
  SQLCHAR	buffer[SQL_MAX_MESSAGE_LENGTH + 1];
  SQLCHAR	sqlstate[SQL_SQLSTATE_SIZE + 1];
  SQLINTEGER	sqlcode;
  SQLSMALLINT	length;
  
  std::ostringstream	error_text;

  error_text << "ODBC Error(s): ";
  
  while ( SQLError( Henv, Hdbc, Hstmt,
		    sqlstate, &sqlcode, buffer,
		    SQL_MAX_MESSAGE_LENGTH + 1, &length ) == SQL_SUCCESS )
  {
    error_text << "SQLSTATE: " << sqlstate << std::endl;
    error_text << "Native ErrorCode: " << sqlcode << std::endl;
    if (length > 0)
    {
      error_text << buffer << std::endl;
    }
  }
  addError( Library::METADATAAPI, ErrorCode, error_text.str( ), "",
	    Filename.c_str( ), LineNumber );
}

#if 0
// Print the error messages to a user specified string.
//!param: char* result - User specified buffer to receive the error messages.
//!param: int size - number of bytes allocated to result
void dbErr::
Print(char *Result, int Size) const
{
  int	len (((int)m_error_text.length() < (Size - 1))
	     ? m_error_text.length()
	     : Size - 1);

  strncpy(Result, m_error_text.c_str(), len);
  Result[len] = '\0';
}
#endif /* 0 */

#if 0
const char* dbErr::
what( ) const throw()
{
  return m_error_text.c_str();
}
#endif /* 0 */
