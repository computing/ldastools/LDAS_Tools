#ifndef METADATAAPI_DBUNIQUEID_HH
#define METADATAAPI_DBUNIQUEID_HH

#include <string>
#include <queue>

#include "general/mutexlock.hh"
#include "general/Singleton.hh"
#include "general/types.hh"

namespace metadataAPI
{
  //: Size of uniqueid field in DB2
  const INT_4U		DB2_UNIQUE_ID_SIZE = 13;

  //: Maintains a pool of unique ids
  //
  // This class maintains a pool of uniqueids as needed by DB2. Each time
  // GetUniqueId() is called, a single uniqueid retrived from the pool.
  // Once the pool is empty, the class will query DB2 to retrieve another
  // batch of uniqueids.
  //
  // This class is implemented as a singleton.
  class DBUniqueId
  {
    SINGLETON_TS_DECL( DBUniqueId );

  public:
    //: uniqueid storage
    typedef std::string		uniqueid_type;
    //: The maximum number of uniqueids to cache.
    static const INT_4U		UNIQUE_ID_CACHE_SIZE;

    //: Retrieve a uniqueid from the pool
    static std::string GetUniqueId( const std::string& Database,
				    const std::string& User,
				    const std::string& Password );

    //: Retrieve multiple uniqueids from the pool
    static void GetUniqueId( const std::string& Database,
			     const std::string& User,
			     const std::string& Password,
			     int Count,
			     std::vector< std::string >& UniqueIds );
  private:
    //: lock protection for critical sections
    //
    // Since this class is a singlton, multiple threads need to be able to
    // access this class concurrently. This lock provides the mechanism
    // to insure thread safety for critical sections.
    static pthread_mutex_t		m_lock;
    //: pool of uniqueids
    //
    // This is the pool of UniqueIds that have been retrieved from the
    // database. It is first in, first out.
    static std::queue< uniqueid_type >	m_ids;

    //: Fill uniqueid pool by querying the database.
    static void fill_queue( const std::string& Database,
			    const std::string& User,
			    const std::string& Password );

    //: Check if pool of uniqueids needs filling
    static bool queue_needs_filling( );
  };

  //---------------------------------------------------------------------
  // Determines if the pool needs filling by checking if the pool is
  // empty.
  //
  //!return: true if the pool needs filling, false otherwise.
  //---------------------------------------------------------------------
  inline bool DBUniqueId::
  queue_needs_filling( )
  {
    //:TODO: Need to expire information based on time limit ( 2 hours )
    return ( ( m_ids.empty( ) ) );
  }
}

#endif /* METADATAAPI_DBUNIQUEID_HH */
