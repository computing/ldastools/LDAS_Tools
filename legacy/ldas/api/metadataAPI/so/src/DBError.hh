#ifndef METADATAAPI__DBERROR_HH
#define METADATAAPI__DBERROR_HH

#include <sstream>
#include <string>

namespace metadataAPI
{
  //: Class to describe errors for DB* calls
  class DBError
  {
  public:
    //: Constructor
    DBError( );

    //: Add body of error to error stream
    void AppendMessage( const std::string& Message );

    //: Get the contents of the error stream
    std::string GetMessage( ) const;

  private:
    //: Counter of iteration. Starts from 1
    int			m_iteration;
    //: Buffer to hold formatted error messages.
    std::ostringstream	m_message;
  };

  //!return: std::string - Contents of the error stream.
  inline std::string DBError::
  GetMessage( ) const
  {
    return m_message.str( );
  }
}

#endif /*METADATAAPI__DBERROR_HH */
