#ifdef __GNUG__
#pragma implementation "query.h"
#endif

#include "config.h"

#include <stdio.h>

#include "odbc.h"

#include "database.h"
#include "dberr.hh"
#include "query.h"
#include "colinfo.h"

#if 0
#define	AT(msg) cout << msg << " - at - " << __FILE__ << ":" << __LINE__ << endl << flush
#else
#define	AT(msg)
#endif
/*
 * wxRecordSet
 */
 
const char*  dbQuery::WILDCARD = "%";

//!param: const dbDatabase& DB - Database where to execute queries
dbQuery::
dbQuery(const dbDatabase& DB)
  : m_db(DB),
    m_hstmt(SQL_NULL_HSTMT),
    m_row_count(0),
    m_col_count(-1)
{
}

dbQuery::
~dbQuery(void)
{
  if (SQL_NULL_HSTMT != m_hstmt)
  {  
    eval_retcode(__FILE__, __LINE__, SQLFreeStmt(m_hstmt, SQL_DROP));
    m_hstmt = SQL_NULL_HSTMT;
  }
}

//!param: SQLUSMALLINT Column - Column to bind to
//!param: SQLSMALLINT TargetType - SQLType of Target
//!param: SQLPOINTER TargetValuePtr - Pointer to Target
//!param: SQLINTEGER BufferLength - Length of Target
//!param: SQLINTEGER* StrLen_or_IndPtr - Buffer to store actual length
void dbQuery::
Bind(SQLUSMALLINT Column,
     SQLSMALLINT TargetType, SQLPOINTER TargetValuePtr,
     SQLINTEGER BufferLength, SQLLEN* StrLen_or_IndPtr)
{
  eval_retcode(__FILE__, __LINE__,
	       SQLBindCol(GetHstmt(), Column+1,
			  TargetType, TargetValuePtr,
			  BufferLength, StrLen_or_IndPtr));
}

//!param: SQLUSMALLINT ParamNum - Offset of the parameter
//!param: SQLSMALLINT CType - CType of Data
//!param: SQLPOINTER Data - Pointer to data for parameter
//!param: SQLUINTEGER ParamSize - Number of bytes referenced by Data
//!param: SQLSMALLINT DataType - SQL datatype
//!param: SQLINTEGER *FAR Length - :TBD:
void dbQuery::
BindParameter(SQLUSMALLINT ParamNum, SQLSMALLINT CType, SQLPOINTER Data,
	      SQLUINTEGER ParamSize, SQLSMALLINT DataType,
	      SQLLEN *FAR Length)
{
  SQLSMALLINT	data_type;
  SQLULEN	param_size;
  SQLSMALLINT	decimal_digits;
  SQLSMALLINT	nullable;

  eval_retcode(__FILE__, __LINE__,
	       SQLDescribeParam(GetHstmt(),
				ParamNum+1, &data_type,
				&param_size,
				&decimal_digits,
				&nullable));
  if (ParamSize != 0) param_size = ParamSize;
  if (
#ifdef SQL_BLOB
      (data_type != SQL_BLOB) &&
#endif	/* SQL_BLOB */
      (DataType != SQL_DEFAULT)
      )
  {
    data_type = DataType;
  }
  eval_retcode(__FILE__, __LINE__,
	       SQLBindParameter(GetHstmt(), ParamNum+1,
				SQL_PARAM_INPUT,
				CType, data_type, param_size, decimal_digits,
				Data, param_size,
				Length));
}

void dbQuery::
Fetch(void)
{
  eval_retcode(__FILE__, __LINE__, SQLFetch(GetHstmt()));
  m_row_count++;
}

//!param: int Column - Offset of column
void dbQuery::
GetColInfo(int Column)
{
  if (m_col_count == -1)
  {
    prep_col_info();
  }
}

//!param: int Column - Offset of column
//!return: std::string - Name associated with column
std::string dbQuery::
GetColumnName(int Column) const
{
  if ((Column < 0) || (Column >= (int)m_cols.size()))
  {
    throw_bad_column(Column);
  }
  return m_cols[Column].GetName();
}

//!param: int Column - Offset of column
SQLUINTEGER dbQuery::
GetColumnSize(int Column) const
{
  if ((Column < 0) || (Column >= (int)m_cols.size()))
  {
    throw_bad_column(Column);
  }
  return m_cols[Column].GetSize();
}

SQLSMALLINT dbQuery::
GetColumnType(int Column) const
{
  if ((Column < 0) || (Column >= (int)m_cols.size()))
  {
    throw_bad_column(Column);
  }
  return m_cols[Column].GetDataType();
}

void dbQuery::
GetData(SQLSMALLINT Column, SQLSMALLINT TargetType,
	SQLPOINTER TargetValue,
	SQLLEN BufferLength,
	SQLLEN* RetLength) const
{
  RETCODE ret;
  ret = SQLGetData(m_hstmt, Column+1,
		   TargetType, TargetValue, BufferLength, RetLength);
  eval_retcode(__FILE__, __LINE__, ret);
}

int dbQuery::
GetParamCount(void) const
{
  SQLSMALLINT	params;

  eval_retcode(__FILE__, __LINE__, SQLNumParams(GetHstmt(), &params));
  return params;
}

void dbQuery::
Prepare( const std::string& Query)
{
  AT( "dbQuery::Prepare - start" );
  //---------------------------------------------------------------------
  // Make sure there is a valid SQLHSTMT
  //---------------------------------------------------------------------

  AT( "dbQuery::Prepare - allocStmt" );
  allocStmt();

  //---------------------------------------------------------------------
  // Submit the query for validation
  //---------------------------------------------------------------------

  AT( "dbQuery::Prepare - SQLPrepare" );
  eval_retcode(__FILE__, __LINE__,
	       SQLPrepare(m_hstmt,
			  (SQLCHAR*)(Query.c_str()), Query.size()));
}

void dbQuery::
Submit(std::string Query)
{
  //---------------------------------------------------------------------
  // Check to see if there is a query that needs to be prepared.
  //---------------------------------------------------------------------
  if (Query.size())
  {
    Prepare(Query);
  }
  //---------------------------------------------------------------------
  // Submit the query for execution
  //---------------------------------------------------------------------
  eval_retcode(__FILE__, __LINE__, SQLExecute(m_hstmt));

  //---------------------------------------------------------------------
  // Process the results.
  //---------------------------------------------------------------------
  GetColInfo();
}

//-----------------------------------------------------------------------
// dbQuery - Protected Methods
//-----------------------------------------------------------------------

void dbQuery::
allocStmt(void)
{
  AT( "dbQuery::allocStmt - start" );
  if (SQL_NULL_HSTMT != m_hstmt)
  {
    AT( "dbQuery::allocStmt - release old handle" );
    //-------------------------------------------------------------------
    // A Statement Handle currently exists; release it
    //-------------------------------------------------------------------
    eval_retcode(__FILE__, __LINE__, SQLFreeStmt(m_hstmt, SQL_DROP));
    AT( "dbQuery::allocStmt - set handle to NULL" );
    m_hstmt = SQL_NULL_HSTMT;
  }

  //---------------------------------------------------------------------
  // Allocate a new statement handle
  //---------------------------------------------------------------------

  m_col_count = -1;
  m_row_count = 0;
  AT( "dbQuery::allocStmt - get new handle" );
  eval_retcode(__FILE__, __LINE__,
	       SQLAllocStmt(GetDatabase().GetHdbc(), &m_hstmt));
}

void dbQuery::
eval_retcode( const char* const Filename,
	      int LineNumber, RETCODE Retcode ) const
{
  AT( "dbQuery::eval_retcode - start" );
  if ((Retcode == SQL_SUCCESS) || (Retcode == SQL_SUCCESS_WITH_INFO))
  {
    AT( "dbQuery::eval_retcode - succeed" );
    return;
  }
  if (Retcode == SQL_NEED_DATA)
  {
    AT( "dbQuery::eval_retcode - SQL_NEED_DATA" );
    throw dbErrNeedData( Filename, LineNumber,
			 GetDatabase().GetHenv(),
			 GetDatabase().GetHdbc(),
			 GetHstmt() );
  }
  if (Retcode == SQL_NO_DATA)
  {
    AT( "dbQuery::eval_retcode - SQL_NO_DATA" );
    throw dbErrNoData(Filename, LineNumber,
		      GetDatabase().GetHenv(),
		      GetDatabase().GetHdbc(),
		      GetHstmt());
  }
  AT( "dbQuery::eval_retcode - General exception" );
  AT( Filename );
  AT( LineNumber );
  throw dbErr(Filename, LineNumber,
	      GetDatabase().GetHenv(),
	      GetDatabase().GetHdbc(),
	      GetHstmt());
}

void dbQuery::
prep_col_info(void)
{
  m_cols.clear();
  eval_retcode(__FILE__, __LINE__, SQLNumResultCols(m_hstmt, &m_col_count));

  for (int i = 0; i < m_col_count; i++)
  {
    const int	STR_LEN = 256;
    SQLCHAR	col_name[STR_LEN];
    SQLSMALLINT	col_name_size;
    SQLSMALLINT	data_type;
    SQLULEN	column_size;
    SQLSMALLINT	decimal_digits;
    SQLSMALLINT	nullable;
      
    eval_retcode(__FILE__, __LINE__,
		 SQLDescribeCol(m_hstmt, i + 1,
				col_name, STR_LEN, &col_name_size,
				&data_type, &column_size,
				&decimal_digits, &nullable));
    m_cols.push_back(dbColInfo((char*)col_name,
			       data_type, column_size, decimal_digits,
			       nullable));
  }
}

void dbQuery::
throw_bad_column(int Column) const
{
  eval_retcode(__FILE__, __LINE__,
	       SQLDescribeCol(m_hstmt, Column + 1,
			      (SQLCHAR*)NULL, 0,
			      (SQLSMALLINT*)NULL,
			      (SQLSMALLINT*)NULL,
			      (SQLULEN*)NULL,
			      (SQLSMALLINT*)NULL,
			      (SQLSMALLINT*)NULL));
}

