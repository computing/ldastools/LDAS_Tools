#include "config.h"

#include <time.h>

#include <iostream>
#include <memory>
#include <cerrno>

#include "general/Memory.hh"
#include "general/gpstime.hh"
#include "general/unordered_map.hh"
#include "general/mutexlock.hh"
#include "general/regex.hh"
#include "general/regexmatch.hh"
#include "general/toss.hh"

#include "ilwd/elementid.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "dbaccess/Table.hh"

#include "genericAPI/thread.hh"
#include "genericAPI/registry.hh"

#include "DBError.hh"
#include "DBPush.hh"
#include "DBUniqueId.hh"
#include "dbeasy.h"
#include "dbeasystmt.h"
#include "dberr.hh"

using metadataAPI::DBUniqueId;

#define MessageDebug( a, b ) std::cerr << a << std::endl

#define LM_DEBUG 0
#if LM_DEBUG
#define AT( a ) MessageDebug( a << ": " << __FILE__ << " " << __LINE__ , 20 )
#else
#define AT( a )
#endif

namespace
{
  class Table;
  struct DBLoginInfo;

  static const INT_2U	DB2_UNIQUE_ID_SIZE( 13 );

  typedef std::vector< Table >	table_list_type;

  //: Retrieve the unique id from the ILwd type
  template <class T>
  std::string unique_id_get( const T& Data, int Row );

  //: Specialization for ILwd::LdasString type
  template <>
  inline std::string
  unique_id_get( const ILwd::LdasString& Data,
		 int Row )
  {
    return Data[ Row ];
  }

  //: Specialization for ILwd::LdasContainer type
  template <>
  inline std::string
  unique_id_get( const ILwd::LdasContainer& Data,
		 int Row )
  {
    const ILwd::LdasElement* uid_element( Data[ Row ] );
    if ( ( uid_element->getElementId( ) != ILwd::ID_CHAR_U ) &&
	 ( uid_element->getElementId( ) != ILwd::ID_CHAR ) )
    {
      throw std::runtime_error( "Bad type for unique id field" );
    }
    const ILwd::LdasArray<CHAR>*
      source( static_cast< const ILwd::LdasArray<CHAR>* >( uid_element ) );

    std::string retval( source->getData( ), source->getDimension( 0 ) );
    return retval;
  }

  //: Set the unique id for the ILwd type
  template <class T>
  void unique_id_set( T& Data, int Row, const std::string& Value );

  //: Specialization for ILwd::LdasString type
  template <>
  inline void
  unique_id_set( ILwd::LdasString& Data,
		 int Row,
		 const std::string& Value )
  {
    Data[ Row ] = Value;
  }

  //: Specialization for ILwd::LdasContainer type
  template <>
  inline void
  unique_id_set( ILwd::LdasContainer& Data,
		 int Row,
		 const std::string& Value )
  {
    ILwd::LdasElement* uid_element( Data[ Row ] );
    if ( ( uid_element->getElementId( ) != ILwd::ID_CHAR_U ) &&
	 ( uid_element->getElementId( ) != ILwd::ID_CHAR ) )
    {
      throw std::runtime_error( "Bad type for unique id field" );
    }
    ILwd::LdasContainer::iterator	pos( Data.find( uid_element ) );
    std::auto_ptr< ILwd::LdasArray<CHAR_U> >
      element( new ILwd::LdasArray<CHAR_U>( (const CHAR_U*)( Value.data( ) ),
					    Value.length( ),
					    uid_element->getNameString( ) ) );
    pos = Data.erase( pos );
    Data.insert( pos, element.release( ),
		 ILwd::LdasContainer::NO_ALLOCATE,
		 ILwd::LdasContainer::OWN );
  }
  
  //: Test if table has a field
  //
  // This function catches exceptions normally thrown when searching for
  // a field within a table. This allows for testing of fields within
  // conditional statements.
  //
  //!param: const DB::Table& Table - Table to query for field.
  //!param: const std::string& FieldName - Name of field being searched in
  //+	Table.
  //!ret: true if the specified table has the requested field, false otherwise.
  inline bool
  has_field( const DB::Table& Table, const std::string& FieldName )
  {
    try
    {
      if ( Table.HasField( FieldName ) >= 0 )
      {
	return true;
      }
    }
    catch( ... )
    {
    }
    return false;
  }

  //: Check if LdasElement is to be interpreted as a comment
  //
  //!param: const ILwd::LdasElement& Element - LdasElement to be checked
  //!ret: true if the given LdasElement represents a comment LdasElement,
  // false otherwise.
  inline bool
  is_comment( const ILwd::LdasElement& Element )
  {
    if ( General::cmp_nocase( Element.getNameString( ).substr( 0, 7 ),
			      "comment" ) == 0 )
    {
      return true;
    }
    return false;
  }

  //: Simple structure to keep the database login information
  struct DBLoginInfo {
    const std::string s_database;
    const std::string s_user;
    const std::string s_password;

    // Constructor
    DBLoginInfo( const std::string& Database,
		 const std::string& Uesr,
		 const std::string& Password );
  };

  inline DBLoginInfo::
  DBLoginInfo( const std::string& Database,
	       const std::string& User,
	       const std::string& Password )
    : s_database( Database ),
      s_user( User ),
      s_password( Password )
  {
  }

  typedef General::unordered_map< std::string, std::string >
  unique_id_map_type;

  //: Local representation of column data
  class TableColumn
  {
  public:
    //: Constructor
    //
    //!param: const DB::Table* TableName - Definition of table containing
    //+		Column
    TableColumn( const DB::Table* Table,
		 ILwd::LdasElement& Column );

    const TableColumn& operator=( const TableColumn& Source );

    const std::string GetName( ) const;
    INT_4S GetRowCount( ) const;
    DB::Field::field_type	GetType( ) const;
    const ILwd::LdasElement&	RefData( ) const;
    ILwd::LdasElement&		RefData( );

  private:
    TableColumn( );
    const DB::Table*		m_table;
    ILwd::LdasElement&		m_data;
    DB::Field::field_type	m_type;
    mutable INT_2S		m_name_pos;
  };

  inline DB::Field::field_type	TableColumn::
  GetType( ) const
  {
    return m_type;
  }

  inline const ILwd::LdasElement& TableColumn::
  RefData( ) const
  {
    return m_data;
  }

  inline ILwd::LdasElement& TableColumn::
  RefData( )
  {
    return m_data;
  }

  //: Local representation of a Table
  class Table
  {
  public:
    typedef TableColumn			column_type;
    typedef std::vector< column_type* >	column_list_type;
    //: Constructor
    Table( ILwd::LdasContainer& Tbl );

    //: Copy Constructor
    Table( const Table& Source );

    //: Assignment operator
    const Table& operator=( const Table& Source );

    //: Destructor
    ~Table( );

    const std::string GetName( ) const;

    const std::string& GetSQL( ) const;

    bool HasField( const std::string& Name ) const;

    static bool IsTable( const ILwd::LdasContainer* Table );

    const column_list_type& RefColumns( ) const;

    column_list_type& RefColumns( );

  private:
    //: Disallow Default constructor
    Table( );

    ILwd::LdasContainer& m_table;
    
    //: SQL command to execute to get data into the database.
    std::string		m_sql;

    //: List of columns
    column_list_type	m_columns;

    //: Number of rows in the table
    INT_4S		m_rows;

    mutable INT_2S	m_name_pos;
  };

  inline const std::string Table::
  GetName( ) const
  {
    return m_table.getName( m_name_pos );
  }

  inline const std::string& Table::
  GetSQL( ) const
  {
    return m_sql;
  }

  inline bool Table::
  IsTable( const ILwd::LdasContainer* Table )
  {
    if ( ( Table != (const ILwd::LdasContainer*)NULL ) &&
	 ( is_comment( *Table ) == false ) )
    {
      if ( ( ( Table->getNameSize( ) >= 2 ) &&
	     ( Table->getName( 1 ).compare( "table" ) == 0 ) ) ||
	   ( ( Table->getNameSize( ) >= 3 ) &&
	     ( Table->getName( 2 ).compare( "table" ) == 0 ) ) )
      {
	return true;
      }
    }
    return false;
  }

  inline const Table::column_list_type& Table::
  RefColumns( ) const
  {
    return m_columns;
  }

  inline Table::column_list_type& Table::
  RefColumns( )
  {
    return m_columns;
  }

  static const DBLoginInfo NULLDBLogin( "", "", "" );

  void parse_uniqueids( const std::string& TableName,
			Table::column_type& Column,
			unique_id_map_type& UniqueIds,
			const DBLoginInfo& DBLogin = NULLDBLogin );

  template< class T >
  inline void
  map_unique_id( const std::string& TableName,
		 const std::string& ColumnName,
		 const T& Data,
		 const INT_4S Rows,
		 unique_id_map_type& UniqueIds,
		 const DBLoginInfo& DBLogin )
  {
    AT( "map_unique_id: Rows: " << Rows );
    bool is_part_of_foreign_key( DB::Table::ReferenceTable( TableName )->
				 IsPartOfForeignKey( ColumnName ) );

    std::string	symbolic_key( TableName );
    symbolic_key += ":";
    symbolic_key += ColumnName;
    symbolic_key += ":";

    std::string	symbolic_group_key( TableName );
    symbolic_key += "group:";
    symbolic_key += ColumnName;
    symbolic_key += ":";

    INT_2U	symbolic_key_size( symbolic_key.length( ) );
    INT_2U	symbolic_group_key_size( symbolic_group_key.length( ) );

    AT( "map_unique_id: symbolic_key: " << symbolic_key << " " << symbolic_key_size );
    for ( INT_4S row = 0;
	  row != Rows;
	  ++row )
    {
      const std::string unique_id( unique_id_get( Data, row ) );
      AT( "map_uniqueid: unique_id: " << unique_id );
      //-----------------------------------------------------------------
      // Determine if the unique id needs to be set
      //-----------------------------------------------------------------
      if ( unique_id.size( )  == 0 )
      {
	AT( "map_uniqueid: autofill" );
	//---------------------------------------------------------------
	// Try to autofill
	//---------------------------------------------------------------
	if ( is_part_of_foreign_key )
	{
	  std::ostringstream	oss;

	  oss << "Must supply unique id value for "
	      << TableName << ":" << ColumnName << " - row " << row
	      << " since it is part of foreign key";

	  General::toss< std::runtime_error >( "map_unique_id",
					       __FILE__,
					       __LINE__,
					       oss.str( ) );
	}
	//---------------------------------------------------------------
	// Get a new unique id
	//---------------------------------------------------------------
	std::string	uid( DBUniqueId::GetUniqueId( DBLogin.s_database,
						      DBLogin.s_user,
						      DBLogin.s_password ) );
	std::ostringstream oss;
	oss << symbolic_key << row;
	UniqueIds[ oss.str( ) ] = uid;
      }
      else if ( ( unique_id.compare( 0, symbolic_key_size, symbolic_key ) ==
		  0 ) ||
		( unique_id.compare( 0,
				     symbolic_group_key_size,
				     symbolic_group_key ) == 0 ) )
      {
	//---------------------------------------------------------------
	// Have Symbolic name
	//---------------------------------------------------------------
	std::string	uid( DBUniqueId::GetUniqueId( DBLogin.s_database,
						      DBLogin.s_user,
						      DBLogin.s_password ) );
	UniqueIds[ unique_id ] = uid;
	AT( "map_uniqueid: symbolic name: " << uid );
      }
      else if ( unique_id.size( )  == DB2_UNIQUE_ID_SIZE )
      {
	//---------------------------------------------------------------
	// Appears to already have a valid unique id specifier.
	//   Record its location
	//---------------------------------------------------------------
	std::ostringstream oss;
	oss << symbolic_key << row;
	UniqueIds[ oss.str( ) ] = unique_id;
	AT( "map_uniqueid: size: " << unique_id );
      }
    }
  }

  void parse_table_set( ILwd::LdasContainer&	TableSet,
			const DBLoginInfo&	DBLogin,
			table_list_type&	tables );

  template< class T >
  inline void
  query_unique_id( T& Data,
		   const std::string& TableName,
		   const std::string& ColumnName,
		   const INT_4S Rows,
		   const unique_id_map_type& UniqueIds,
		   const unique_id_map_type::const_iterator& End )
  {
    for ( INT_4S row = 0;
	  row != Rows;
	  ++row )
    {
      std::string	unique_id( unique_id_get( Data, row ) );
      AT( "Looking up: " << unique_id << " for: " << TableName << ":" << ColumnName );
      //-----------------------------------------------------------------
      // Look for the uniqueid
      //-----------------------------------------------------------------
      const unique_id_map_type::const_iterator
	cur( UniqueIds.find( unique_id ) );
      //-----------------------------------------------------------------
      // Only change those for which there is a mapping
      //-----------------------------------------------------------------
      if ( cur != End )
      {
	//---------------------------------------------------------------
	// Set the value
	//---------------------------------------------------------------
	AT( "Setting UniqueId to: " << (*cur).second );
	unique_id_set( Data, row, (*cur).second );
	unique_id = unique_id_get( Data, row );
      }
      //-----------------------------------------------------------------
      // Verify that the unique id has the proper number of characters
      //-----------------------------------------------------------------
      AT( "UniqueId length: " << unique_id.length( ) );
      if ( unique_id.length( ) != DB2_UNIQUE_ID_SIZE )
      {
	std::ostringstream	err;

	err << "The entry at row " << ++row << " for "
	    << TableName << ":" << ColumnName
	    << " does not appear to be unique id data (";
	Data.write( err, ILwd::ASCII );
	err << ")";
	throw std::runtime_error( err.str( ) );
      }
    }
  }

  void
  get_table_sets( ILwd::LdasContainer* Request,
		  std::list< ILwd::LdasContainer* >& TableSets );

  void
  retry_sleep( const int Iteration )
  {
    struct timespec	req;
    struct timespec	rem;

    static const REAL_8	divisor = 3.0;
    static const REAL_8	engineering_constant = 5.0;
    static const REAL_8	average_job_length = 2.0;
    REAL_8		multiplier = 1;

    for ( int x = 0; x < Iteration; ++x )
    {
      multiplier *= engineering_constant;
    }

    REAL_8	sleep_time =
      ( average_job_length / divisor ) * multiplier;

    req.tv_sec = int( sleep_time );		// Get integer component
    sleep_time -= req.tv_sec;			// Subtract integer component
    req.tv_nsec = int(sleep_time * 1000000000);	// Multiply by a billion

    while( nanosleep( &req, &rem ) != 0 )
    {
      int error = errno;
      switch( error )
      {
      case EINTR:
	//---------------------------------------------------------------
	// An interupt has happened. This is a recoverable error.
	//---------------------------------------------------------------
	req.tv_sec = rem.tv_sec;
	req.tv_nsec = rem.tv_nsec;
	break;
      default:
	//---------------------------------------------------------------
	// An error has occured that prevents the proper execution.
	//   Just return to the use.
	//---------------------------------------------------------------
	return;
	break;
      }
    }
  }


  inline bool
  retry( int NumberOfTries, const char* Error, const Regex& Pattern )
  {
    RegexMatch	regex_result;

    if ( ( NumberOfTries == 0 ) ||
	   ( regex_result.match( Pattern, Error ) == false ) )

    {
      return false;
    }
    return true;
  }
} // namespace - anonymous

//-----------------------------------------------------------------------
// DBPush handles insertion of table data into the database.
//
//!param: const std::string Database - Name of the database to use.
//!param: const std::string User - Name of the database user.
//!param: const std::string Password - Database password for User.
//!param: ILwd::LdasElement* - ILwd containing table information.
//!param: const int MaximumNumberOfTries - The maximum number of
//+	times to try and insert the data into the database.
//!param: const std::string RetryRegex - Regular expression to use when an
//	error occurs to determine if the request should be retried.
//
//!return: std::string - this is a triplicate of
//+	<table name> <rows> <wall time>
std::string metadataAPI::
DBPush( const std::string Database,
	const std::string User,
	const std::string Password,
	ILwd::LdasElement* Tables,
	const int MaximumNumberOfTries,
	const std::string RetryRegex )
{
  using Registry::elementRegistry;

  //---------------------------------------------------------------------
  // Remove the object from the registry to protect other threads from
  //   the manipulation of this object.
  //---------------------------------------------------------------------
  elementRegistry.removeObject( Tables );
  
  //---------------------------------------------------------------------
  // Set up the object for auto deletion when the call exits
  //---------------------------------------------------------------------
  std::auto_ptr< ILwd::LdasElement >	auto_delete_tables( Tables );

  //---------------------------------------------------------------------
  // Set up regular expression pattern to match errors against
  //---------------------------------------------------------------------
  const Regex	pattern( RetryRegex, REG_EXTENDED | REG_ICASE );
  RegexMatch	regex_result;
  DBError	error;

  AT( "DBPush" );
  //---------------------------------------------------------------------
  // List of tables that constitute the transaction
  //---------------------------------------------------------------------
  DBLoginInfo	db_login( Database, User, Password );

  table_list_type	tables;

  //---------------------------------------------------------------------
  // Make sure Tables is not a null pointer
  //---------------------------------------------------------------------
    
  ILwd::LdasContainer*
    request( dynamic_cast< ILwd::LdasContainer*>( Tables ) );
  AT( "DBPush" );
  if ( request == (ILwd::LdasContainer*)NULL )
  {
    return "";
  }

  //---------------------------------------------------------------------
  // Establish local variables
  //---------------------------------------------------------------------

  AT( "DBPush" );
  std::string	results;	// Number of rows generated for each table

  //---------------------------------------------------------------------
  // Parse the ilwd into table sets
  //---------------------------------------------------------------------

#if LM_DEBUG
  std::cerr << "Dump Input: ";
  request->write( 2, 2, std::cerr, ILwd::ASCII );
  std::cerr << std::endl;
#endif /* LM_DEBUG */

  AT( "DBPush" );
  std::list< ILwd::LdasContainer*>	table_sets;
  get_table_sets( request, table_sets );
  if ( table_sets.size( ) <= 0 )
  {
    throw std::invalid_argument( "Unable to find transaction within ilwd" );
  }

  for ( std::list< ILwd::LdasContainer* >::iterator
	  cur = table_sets.begin( ),
	  end = table_sets.end( );
	cur != end;
	++cur )
  {
    parse_table_set( *(*cur), db_login, tables );
  }

  //---------------------------------------------------------------------
  // Establish the connection and statement handles.
  //---------------------------------------------------------------------

  AT( "DBPush" );
  dbEasy	connection( db_login.s_database,
			    db_login.s_user,
			    db_login.s_password );
  dbEasyStmt	statement( connection );

  //---------------------------------------------------------------------
  // Try to insert data into the database
  //---------------------------------------------------------------------

  AT( "DBPush" );
  int iteration = 0;
  int max = MaximumNumberOfTries;
  while ( max > 0 )
  {
    --max;
    try
    {
      //-----------------------------------------------------------------
      // Result buffer
      //-----------------------------------------------------------------
      std::ostringstream	oss;
      
      //-----------------------------------------------------------------
      // Begin the transaction
      //-----------------------------------------------------------------
      AT( "DBPush" );

      connection.TransactionBegin( );

      //-----------------------------------------------------------------
      // Loop over the tables
      //-----------------------------------------------------------------
      AT( "DBPush" );
      
      INT_4S	row_count( -1 );
      for ( table_list_type::const_iterator
	      beg = tables.begin( ),
	      cur = tables.begin( ),
	      end = tables.end( );
	    cur != end;
	    ++cur )
      {
	try
	{
	  struct timeval		now;
	  REAL_8			start;
	    
	  gettimeofday( &now, (struct timezone*)NULL );
	  start = now.tv_sec + REAL_8( now.tv_usec ) / REAL_8( 1000000 );

	  //-------------------------------------------------------------
	  // Initialize variables on a per table basis
	  //-------------------------------------------------------------
	  row_count = -1;

	  //-------------------------------------------------------------
	  // Prepare the SQL for this table
	  //-------------------------------------------------------------
	  AT( "DBPush" );

	  statement.Prepare( (*cur).GetSQL( ) );

	  //-------------------------------------------------------------
	  // Bind columns to parameters in SQL statement
	  //-------------------------------------------------------------
	  AT( "DBPush" );
	    
	  INT_4U	col_offset = 1;
	  const Table::column_list_type	cols( (*cur).RefColumns( ) );

	  for ( Table::column_list_type::const_iterator
		  cur_col = cols.begin( ),
		  end_col = cols.end( );
		cur_col != end_col;
		++cur_col, ++col_offset )
	  {
	    if ( row_count == -1 )
	    {
	      row_count = (*cur_col)->GetRowCount( );
	    }
	    statement.BindParameter( col_offset,
				     &( (*cur_col)->RefData( ) ) );
	  }
	    
	  //-------------------------------------------------------------
	  // Put the data into the database
	  //-------------------------------------------------------------
	  AT( "DBPush" );

	  statement.Submit( );

	  AT( "DBPush: " << (*cur).GetName( ) << " " << statement.GetNumberOfRows( ) );
	  if ( cur != beg )
	  {
	    oss << " ";
	  }
	  REAL_8	end;
	  gettimeofday( &now, (struct timezone*)NULL );
	  end = now.tv_sec + REAL_8( now.tv_usec ) / REAL_8( 1000000 );
	  oss << (*cur).GetName( )		// Table name
	      << " " << row_count		// Number of rows
	      << " " << ( end - start );	// Wall time
	}
	catch( const LdasException& e )
	{
	  std::ostringstream	err;
	    
	  err << e[ 0 ].getMessage( ) << " (SQL: " << (*cur).GetSQL( ) << ")";
	  throw LdasException( e[ 0 ].getLibrary( ), e[ 0 ].getCode( ),
			       err.str( ), e[ 0 ].getInfo( ),
			       e[ 0 ].getFile( ), e[ 0 ].getLine( ) );
	}
	catch( const std::exception& e )
	{
	  std::ostringstream	err;
	    
	  err << e.what( ) << " (SQL: " << (*cur).GetSQL( ) << ")";
	  throw LdasException( Library::METADATAAPI,
			       dbErr::ERROR_GENERAL_DB_ERROR,
			       err.str( ), "",
			       __FILE__, __LINE__ );
	}
	catch( ... )
	{
	  std::ostringstream	err;
	    
	  err << "Unknown exception: (SQL: " << (*cur).GetSQL( ) << ")";
	  throw LdasException( Library::METADATAAPI,
			       dbErr::ERROR_GENERAL_DB_ERROR,
			       err.str( ), "",
			       __FILE__, __LINE__ );
	}
      }
	
      //-----------------------------------------------------------------
      // End the transaction
      //-----------------------------------------------------------------
      AT( "DBPush" );

      connection.TransactionCommit( );
      results = oss.str( );
      break;	// Terminate the loop
    }
    catch( const LdasException& e )
    {
      //-----------------------------------------------------------------
      // Rollback the transaction
      //-----------------------------------------------------------------
      AT( "DBPush: " << e[ 0 ].getMessage( ) );
      connection.TransactionRollback( );
      error.AppendMessage( e[ 0 ].getMessage( ) );
      if ( retry( max, e[ 0 ].getMessage( ).c_str( ), pattern ) == false )
      {
	// Throw the complete list of error messages.
	throw SwigException( error.GetMessage( ),
			     __FILE__, __LINE__ );
      }
      retry_sleep( iteration );
      ++iteration;
    }
    catch( const SwigException& e )
    {
      //-----------------------------------------------------------------
      // Rollback the transaction
      //-----------------------------------------------------------------
      AT( "DBPush: " << e.getResult( ) );
      connection.TransactionRollback( );
      error.AppendMessage( e.getResult( ) );
      if ( retry( max, e.getResult( ).c_str( ), pattern ) == false )
      {
	// Throw the complete list of error messages.
	throw SwigException( error.GetMessage( ),
			     __FILE__, __LINE__ );
      }
      retry_sleep( iteration );
      ++iteration;
    }
    catch( const std::exception& e )
    {
      //-----------------------------------------------------------------
      // Rollback the transaction
      //-----------------------------------------------------------------
      AT( "DBPush: " << e.what( ) );
      connection.TransactionRollback( );
      error.AppendMessage( e.what( ) );
      if ( retry( max, e.what( ), pattern ) == false )
      {
	// Throw the complete list of error messages.
	throw SwigException( error.GetMessage( ),
			     __FILE__, __LINE__ );
      }

      retry_sleep( iteration );
      ++iteration;
    }
    catch( ... )
    {
      //-----------------------------------------------------------------
      // Rollback the transaction
      //-----------------------------------------------------------------
      AT( "DBPush: Unknown exception" );

      connection.TransactionRollback( );
      error.AppendMessage( "Caught an unknown exception" );
      throw;
    }
  }
  std::ostringstream oss;
  oss << " { " << error.GetMessage( ) << " } " << results;
  AT( "DBPush: " << oss.str( ) );
  return oss.str( );
}

#if HAVE_TID
CREATE_THREADED6( metadataAPI::DBPush, std::string, \
		  const std::string /* Database */, \
		  const std::string /* User */, \
		  const std::string /* Password */, \
		  ILwd::LdasElement* /* Tables */, \
		  int /* MaximumNumberOfTries */, \
		  const std::string /* RetryRegex */ )
#endif /* HAVE_TID */

namespace
{
  TableColumn::
  TableColumn( const DB::Table* Table,
	       ILwd::LdasElement& Column )
    : m_table( Table ),
      m_data( Column ),
      m_name_pos( 0 )
  {
    const std::string& name0( m_data.getName( 0 ) );
    static const std::string	group( "group" );

    if ( ( name0.length( ) >= group.length( ) ) &&
	 ( name0.substr( name0.length( ) - group.length( ),
			 group.length( ) ).compare( group )
	   == 0 ) &&
	 ( m_data.getName( 1 ).compare( Table->GetName( ) ) == 0 ) )
    {
      m_name_pos = 2;
    }
    else if ( ( name0.compare( Table->GetName( ) ) == 0 ) &&
	      ( has_field( *Table, m_data.getName( 1 ) ) ) )
    {
      m_name_pos = 1;
    }
    if ( has_field( *Table, GetName( ) ) == false )
    {
      std::ostringstream	oss;
      
      oss << "The table '" << Table->GetName( )
	  << "' has no field named '"
	  << GetName( ) << "'";
      throw std::range_error( oss.str( ) );
    }
    DB::Field&	field( Table->GetField( GetName( ) ) );
    m_type = field.GetType( );
    if ( m_data.getElementId( ) == ILwd::ID_ILWD )
    {
      ILwd::LdasContainer&
	container( static_cast< ILwd::LdasContainer& >( m_data ) );
      // Purge out all comment columns
      ILwd::LdasContainer::iterator	cur( container.begin( ) );
      while ( cur != container.end( ) )
      {
	if ( is_comment( *(*cur) ) )
	{
	  cur = container.erase( cur );
	}
	else
	{
	  ++cur;
	}
      }
    }
  }

  const TableColumn& TableColumn::
  operator=( const TableColumn& Source )
  {
    m_table = Source.m_table;
    m_data = Source.m_data;
    m_name_pos = Source.m_name_pos;
    return *this;
  }

  const std::string TableColumn::
  GetName( ) const
  {
    return m_data.getName( m_name_pos );
  }

  INT_4S TableColumn::
  GetRowCount( ) const
  {
    switch( m_data.getElementId( ) )
    {
    case ILwd::ID_CHAR:
    case ILwd::ID_CHAR_U:
    case ILwd::ID_INT_2S:
    case ILwd::ID_INT_2U:
    case ILwd::ID_INT_4S:
    case ILwd::ID_INT_4U:
    case ILwd::ID_INT_8S:
    case ILwd::ID_INT_8U:
    case ILwd::ID_REAL_4:
    case ILwd::ID_REAL_8:
    case ILwd::ID_COMPLEX_8:
    case ILwd::ID_COMPLEX_16:
      {
	const ILwd::LdasArrayBase&
	  array( static_cast<const ILwd::LdasArrayBase&>( m_data) );
	return( array.getDimension( 0 ) );
      }
      break;
    case ILwd::ID_LSTRING:
      {
	const ILwd::LdasString&
	  str( static_cast<const ILwd::LdasString&>( m_data) );
	return( str.size( ) );
      }
      break;
    case ILwd::ID_ILWD:
      {
	const ILwd::LdasContainer&
	  ilwd( static_cast<const ILwd::LdasContainer&>( m_data) );
	return( ilwd.size( ) );
      }
      break;
    default:
      // Done here to appease the warnings produced by the compiler for
      //   unhandled cases.
      break;
    }
    throw std::runtime_error( "Unknown datatype for data insertion" );
  }

  Table::
  Table( ILwd::LdasContainer& Tbl )
    : m_table( Tbl ),
      m_rows( -1 ),
      m_name_pos( 0 )
  {
    const std::string& name0( m_table.getName( 0 ) );
    static const std::string	group( "group" );

    if ( ( name0.length( ) >= group.length( ) ) &&
	 ( name0.substr( name0.length( ) - group.length( ),
			 group.length( ) ).compare( group )
	   == 0 ) )
    {
      m_name_pos = 1;
    }
    else if ( ( m_table.getNameSize( ) >= 3 ) &&
	      ( m_table.getName( 2 ).compare( "table" ) == 0 ) )
    {
      m_name_pos = 1;
    }
#if LM_DEBUG
    std::cerr << "Dump Table: ";
    Tbl.write( 2, 2, std::cerr, ILwd::ASCII );
    std::cerr << std::endl;
#endif /* LM_DEBUG */
    //-------------------------------------------------------------------
    // Local varaibles
    //-------------------------------------------------------------------

    // Name of the table
    const std::string		table_name( GetName( ) );
    // Used for validation of information
    const DB::Table*	db_table( DB::Table::ReferenceTable( table_name ) );

    //-------------------------------------------------------------------
    // Generate the SQL command needed to insert this table of data
    //-------------------------------------------------------------------
    m_sql = "insert into ";
    m_sql += table_name;
    m_sql += " ( ";
    for ( ILwd::LdasContainer::const_iterator
	    cur = m_table.begin( ),
	    end = m_table.end( );
	  cur != end;
	  ++cur )
    {
      AT( "Table: " << (*cur)->getNameString( ) );
      if ( ( (*cur) == (ILwd::LdasElement*)NULL ) ||
	   ( (*cur)->getNameSize( ) < 3 ) || 
	   ( is_comment( *(*cur ) ) ) )
      {
	continue;
      }
      if ( m_columns.size( ) )
      {
	m_sql += ", ";
      }
      m_columns.push_back( new TableColumn( db_table, *(*cur) ) );
      m_sql += m_columns.back( )->GetName( );
      if ( m_rows == -1 )
      {
	m_rows = m_columns.back( )->GetRowCount( );
      }
      else
      {
	if ( m_rows != m_columns.back( )->GetRowCount( ) )
	{
	  std::ostringstream	oss;

	  oss << "Wrong number of rows: "
	      << table_name << "." << m_columns.back( )->GetName( )
	      << " has " << m_columns.back( )->GetRowCount( )
	      << " row(s) instead of "
	      << m_rows
	      << " row(s)";
	  throw std::out_of_range( oss.str( ) );
	}
      }
    }
    //-------------------------------------------------------------------
    // Check for any uniqueids that need to be dynamically created
    //-------------------------------------------------------------------
    INT_4U	fields( db_table->GetFieldCount( ) );
    for ( INT_4U cur( 0 ); cur != fields; ++cur )
    {
      const DB::Field& f( db_table->GetField( cur ) );
      AT( "Check: " << f.GetName( ) << ": type: " << ( f.GetType( ) == DB::Field::FIELD_UNIQUE_ID ) << " Required: " << f.IsRequired( ) << " Has Field: " << HasField( f.GetName( ) ) );
      if ( ( f.GetType( ) == DB::Field::FIELD_UNIQUE_ID ) &&
	   ( !db_table->IsPartOfForeignKey( f.GetName( ) ) ) &&
	   ( !HasField( f.GetName( ) ) ) )
      {
	// This is a required unique id field.
	// Create the column
	std::ostringstream	cn;

	cn << GetName( ) << "group:"
	   << GetName( ) << ":"
	   << f.GetName( );
	std::auto_ptr<ILwd::LdasContainer>
	  column( new ILwd::LdasContainer( cn.str( ) ) );
	// Fill the column with logical names
	AT( "Adding field: " << cn.str( ) );

	char		name[ 256 ];
	std::string	table_name( GetName( ) );
	std::string	column_name( f.GetName( ) );

	for ( INT_4S x = 0; x < m_rows; x++ )
	{
	  sprintf( name,
		   "%s:%s:%d",
		   table_name.c_str( ),
		   column_name.c_str( ),
		   x );
	  column->push_back( new ILwd::LdasArray< CHAR_U >
			     ( (CHAR_U*)name, strlen( name ),
			       cn.str( ) ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
	}
	// Put the column into the table
	m_table.push_back( column.release( ),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
	m_columns.push_back( new TableColumn( db_table,
					      *( m_table.back( ) ) ) );
	if ( m_columns.size( ) )
	{
	  m_sql += ", ";
	}
	m_sql += m_columns.back( )->GetName( );
      }
    }
    //-------------------------------------------------------------------
    // Put in the parameter markers
    //-------------------------------------------------------------------
    m_sql += " ) values (";
    for ( INT_4U x = 0; x < m_columns.size( ); ++x )
    {
      if ( x )
      {
	m_sql += ",";
      }
      m_sql += "?";
    }
    m_sql += ")";
    AT( "Table::Table: " << m_sql );
  }

  Table::
  Table( const Table& Source )
    : m_table( Source.m_table ),
      m_sql( Source.m_sql ),
      m_name_pos( Source.m_name_pos )
  {
    for ( column_list_type::const_iterator
	    cur = Source.m_columns.begin( ),
	    end = Source.m_columns.end( );
	  cur != end;
	  ++cur ) 
   {
      m_columns.push_back( new TableColumn( *(*cur) ) );
    }
  }

  Table::
  ~Table( )
  {
    General::Purge( m_columns );
  }

  const Table& Table::
  operator=( const Table& Source )
  {
    const_cast< ILwd::LdasContainer& >( m_table ) = Source.m_table;
    m_sql = Source.m_sql;
    m_name_pos = Source.m_name_pos;
    for ( column_list_type::const_iterator
	    cur = Source.m_columns.begin( ),
	    end = Source.m_columns.end( );
	  cur != end;
	  ++cur ) 
   {
      m_columns.push_back( new TableColumn( *(*cur) ) );
    }
    return *this;
  }

  bool Table::
  HasField( const std::string& Name ) const
  {
    for ( column_list_type::const_iterator
	    cur( m_columns.begin( ) ),
	    end( m_columns.end( ) );
	  cur != end;
	  ++cur )	
    {
      if ( Name.compare( (*cur)->GetName( ) ) == 0 )
      {
	AT( "Name: " << Name << " GetName: " << (*cur)->GetName( ) );
	return true;
      }
    }
    return false;
  }

  void
  parse_uniqueids( const std::string& TableName,
		   Table::column_type& Column,
		   unique_id_map_type& UniqueIds,
		   const DBLoginInfo& DBLogin )
  {
    AT( "parse_uniqueids: Table: " << TableName << " Column: " << Column.GetName( ) );
    //-------------------------------------------------------------------
    // If there are no UniqueIds, then this is the pass to generate them.
    //   Otherwise, lookup the keys and replace with the appropriate
    //   value.
    //-------------------------------------------------------------------
    bool		substitute_mode( ( &DBLogin == &NULLDBLogin )
					 ? true : false );
    ILwd::LdasElement&	data( Column.RefData( ) );
    ILwd::ElementId	element_id( data.getElementId( ) );

    if ( substitute_mode )
    {
      AT( "parse_uniqueids: Substitute mode" );
      const unique_id_map_type::const_iterator
     	end_unique_ids( UniqueIds.end( ) );

      switch( element_id )
      {
      case ILwd::ID_LSTRING:
	query_unique_id( static_cast< ILwd::LdasString& >( data ),
			 TableName,
			 Column.GetName( ),
			 Column.GetRowCount( ),
			 UniqueIds,
			 end_unique_ids );
	break;
      case ILwd::ID_ILWD:
	query_unique_id( static_cast< ILwd::LdasContainer& >( data ),
			 TableName,
			 Column.GetName( ),
			 Column.GetRowCount( ),
			 UniqueIds,
			 end_unique_ids );
	break;
      default:
	throw std::runtime_error( "Unsupported column type for UniqueId" );
      }
    }
    else
    {
      AT( "parse_uniqueids: Map mode" );
      switch( element_id )
      {
      case ILwd::ID_LSTRING:
	AT( "parse_uniqueids: map: ILwd::ID_LSTRING" );
	map_unique_id( TableName,
		       Column.GetName( ),
		       static_cast< ILwd::LdasString& >( data ),
		       Column.GetRowCount( ),
		       UniqueIds,
		       DBLogin );
	break;
      case ILwd::ID_ILWD:
	AT( "parse_uniqueids: map: ILwd::ID_ILWD" );
	map_unique_id( TableName,
		       Column.GetName( ),
		       static_cast< ILwd::LdasContainer& >( data ),
		       Column.GetRowCount( ),
		       UniqueIds,
		       DBLogin );
	break;
      default:
	AT( "parse_uniqueids: map: Default" );
	throw std::runtime_error( "Unsupported column type for UniqueId" );
      }
    }
  }

  void
  get_table_sets( ILwd::LdasContainer* Request,
		  std::list< ILwd::LdasContainer* >& TableSets )
  {
    using namespace ILwd;

#if 0
    LdasContainer*	retval( Request );
    if ( retval != (LdasContainer*)NULL )
    {
      for ( LdasContainer::iterator
	      cur = retval->begin( ),
	      end = retval->end( );
	    cur != end;
	    ++cur )
      {
	if ( Table::IsTable( dynamic_cast< ILwd::LdasContainer* >( *cur ) ) )
	{
	  TableSets.push_back( retval );
	  AT( "TableSet size: " << TableSets.size( ) );
	  return;
	}
	if ( dynamic_cast< LdasContainer* >( *cur ) )
	{
	  get_table_sets( *cur, TableSets );
	  return;
	}
      }
    }
#else
    //-------------------------------------------------------------------
    // Perform breath first search
    //-------------------------------------------------------------------
    std::queue< LdasContainer* >	prospects;

    prospects.push( Request );
    while ( prospects.size( ) > 0 )
    {
      for ( INT_4U
	      remaining = prospects.size( );
	    remaining > 0;
	    --remaining )
      {
	LdasContainer*
	  ts( dynamic_cast< LdasContainer* >( prospects.front( ) ) );
	prospects.pop( );
	for ( LdasContainer::const_iterator
		cur = ts->begin( ),
		end = ts->end( );
	      cur != end;
	      ++cur )
	{
	  LdasContainer*
	    tc( dynamic_cast< LdasContainer* >( *cur ) );
	  
	  if ( tc == (LdasContainer*)NULL )
	  {
	    continue;
	  }
	  if ( Table::IsTable( tc ) )
	  {
	    // Found at least one table container which is sufficient to
	    //   declare the outer container a table set container
	    TableSets.push_back( ts );
	    break;
	  }
	  else
	  {
	    // Maybe this container is a table set container. Put it
	    //   into the list to try the next go around
	    prospects.push( tc );
	  }
	} // for
      } // for
      if ( TableSets.size( ) > 0 )
      {
	// Some tables have been found
	return;
      }
    }
#endif
  }

  void
  parse_table_set( ILwd::LdasContainer& TableSet,
		   const DBLoginInfo& DBLogin,
		   table_list_type& Tables )
  {
    AT( "DBPush" );
    for ( ILwd::LdasContainer::const_iterator
	    cur = TableSet.begin( ),
	    end = TableSet.end( );
	  cur != end;
	  ++cur )
    {
      ILwd::LdasContainer*
	table( dynamic_cast< ILwd::LdasContainer*>( *cur ) );

      //-----------------------------------------------------------------
      // Validate that the object is a container, has at least three
      //   components to its name and the the 3rd element is "table".
      //-----------------------------------------------------------------
      if ( Table::IsTable( table ) == false )
      {
	//---------------------------------------------------------------
	// Not a table element. Go to the next element
	//---------------------------------------------------------------
	continue;
      }

      //-----------------------------------------------------------------
      // Add table to set of tables to make the transaction
      //-----------------------------------------------------------------
      
      Tables.push_back( Table( *table ) );
    }

    //-------------------------------------------------------------------
    // :TODO: Generate any unique ids that are required by the table set
    //-------------------------------------------------------------------

    AT( "DBPush" );
    unique_id_map_type	unique_ids;
    
    //-------------------------------------------------------------------
    // First pass is to capture any uniqueid fields
    //-------------------------------------------------------------------

    AT( "DBPush" );
    for ( table_list_type::iterator
	    cur_table = Tables.begin( ),
	    end_table = Tables.end( );
	  cur_table != end_table;
	  ++cur_table )
    {
      for ( Table::column_list_type::iterator
	      cur_col = (*cur_table).RefColumns( ).begin( ),
	      end_col = (*cur_table).RefColumns( ).end( );
	    cur_col != end_col;
	    ++cur_col )
      {
	//---------------------------------------------------------------
	// Weed out any non-uniqueid fields
	//---------------------------------------------------------------
	AT( "DBPush: Type: " << (*cur_col)->GetType( ) );
	if ( (*cur_col)->GetType( ) != DB::Field::FIELD_UNIQUE_ID )
	{
	  AT( "Skipping: " << (*cur_table).GetName( ) << ":" << (*cur_col)->GetName( ) );
	  continue;
	}
	//---------------------------------------------------------------
	// Generate a list of unique ids
	//---------------------------------------------------------------
	AT( "DBPush: UniqueId Column: " << (*cur_col)->GetName( ) );
	parse_uniqueids( (*cur_table).GetName( ),
			 *(*cur_col),
			 unique_ids,
			 DBLogin );
      }
    }

    //-------------------------------------------------------------------
    // Second pass is to replace primary and foreign keys with actual
    //   actual values
    //-------------------------------------------------------------------

    AT( "DBPush" );
    for ( table_list_type::iterator
	    cur_table = Tables.begin( ),
	    end_table = Tables.end( );
	  cur_table != end_table;
	  ++cur_table )
    {
      for ( Table::column_list_type::iterator
	      cur_col = (*cur_table).RefColumns( ).begin( ),
	      end_col = (*cur_table).RefColumns( ).end( );
	    cur_col != end_col;
	    ++cur_col )
      {
	//---------------------------------------------------------------
	// Weed out any non-uniqueid fields
	//---------------------------------------------------------------
	if ( (*cur_col)->GetType( ) != DB::Field::FIELD_UNIQUE_ID )
	{
	  AT( "Skipping: " << (*cur_table).GetName( ) << ":" << (*cur_col)->GetName( ) );
	  continue;
	}
	//---------------------------------------------------------------
	// Substitute unique ids
	//---------------------------------------------------------------
	parse_uniqueids( (*cur_table).GetName( ),
			 *(*cur_col),
			 unique_ids );
      }
    }
  }

} // namespace - anonymous
