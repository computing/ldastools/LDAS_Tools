#ifndef METADATAAPI_DBPULL_HH
#define	METADATAAPI_DBPULL_HH

#include <string>

#include "genericAPI/threaddecl.hh"

namespace ILwd
{
  class LdasContainer;
}

namespace metadataAPI
{
  //: Fetches row from a database.
  std::string DBPull( const std::string Database,
			       const std::string User,
			       const std::string Password,
			       const std::string Query,
			       const int MaximumResultRowCount,
			       const int NumberOfTries,
			       const std::string RetryRegex );
#if HAVE_TID
  //: Threaded version of DBPull.
  CREATE_THREADED7_DECL( DBPull, std::string, \
			 const std::string /* Database */, \
			 const std::string /* User */, \
			 const std::string /* Password */, \
			 const std::string /* Query */, \
			 const int /* MaximumResultRowCount */, \
			 const int /* NumberOfTries */, \
			 const std::string /* RetryRegex */ );
#endif /* HAVE_TID */
} // namespace - metadataAPI

#endif /* METADATAAPI_DBPULL_HH */
