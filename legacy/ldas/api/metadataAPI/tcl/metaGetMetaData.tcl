## ******************************************************** 
## getMetaData.tcl Version 1.0
##
## getMetaData cmd functions 
##
## Provides query retrieval functions for the LDAS.
## Contains subset of cmds in metadata.tcl
## to be run in a slave interp.
##
## Release Date: Not Available
##
## This module requires the following sub-modules:
##   1. genericAPI.tcl (stack and queue manipulation functions)
##   2. metaschema.tcl (output sql for descMetaData)
##   3. metadataILwd.tcl (creates ilwd output for getMetaData)
## ;#ol
## ******************************************************** 

package provide getMetaData 1.0


## ******************************************************** 
##
## Name: metadata::submitReaper
##
## Description:
## output results of a query to ilwd file
##
## Parameters:
## easyInstance     stmt instance name
## ilwd filename    filename for ilwd
## tag              tag to identify the results
##
## Usage:
##  metadata::outIlwd $stmtp "results.out" 
##
## Comments:
## uses ilwd namespace procs to write ilwd output
## of metadata results as native ilwd 
## pointer to ilwd object is set in ::metadata::ilwdObject
## which must be destroyed by caller.

proc metadata::submitReaper { tid jobid args } {
	
    if	{ ! [ info exist ::$tid ] } {
    	;## addLogEntry "::$tid does not exist" purple
        return
    }
	if	{ [ catch {
        set state [ set ::$tid ]
        if 	{ [ string equal FINISHED $state ] ||
              [ string equal $state $::TID_FINISHED ]  } {

		    set result [ DBPull_r $tid ]  
            ::unset ::$tid
            foreach { DBmsg datap } $result { break }
                     
            ;## log as non-fatal if there is msg
            if  { [ string length [ string trim $DBmsg ] ] } {
                regsub -nocase -all -- {iteration} $DBmsg {*** Iteration} DBmsg
                addLogEntry $DBmsg orange
            }

		    log "[myName] $tid completed for jobid $jobid $datap"
		    outILwdFile $jobid $datap
        }
	} err ] } {
		set msg "DBPull_r $tid error: $err"
        regsub -nocase -all -- {iteration} $err {*** Iteration} msg        
        catch { ::unset ::$tid }
	    set ::${jobid}(errors) $msg
        addLogEntry $msg 2 
		log "$jobid data: $jobid [ array get ::${jobid} ]" 5
		cleanupJob $jobid [ myName ]
	}		
}

## ******************************************************** 
##
## Name: metadata::sendData
##
## Description:
##
## Parameters:
##
## Usage:
## 
## Returns:
##    
## Comments:

proc metadata::sendData { jobid api data } {
    if  { [ catch {
        
        ;## if this was a request for spectrum data for the
        ;## data conditioning API.
        #set ::${jobid}_DATABUCKET {}
        #lappend ::${jobid}_DATABUCKET $data
        
		foreach { api host port service } [ validService $api data ] { break }
        ;## assumes a list of ilwd pointers is returned 

            ::dataSend $jobid $api $data
            if  { $::DEBUG == 2 } {
                set ilwdfile ${jobid}to${api}.ilwd
                set target [ ilwd::writeFile $jobid $data $ilwdfile ascii none ]
            }
            destructElement $data

    } err ] } {
        catch { emptyDataBucket $jobid }
        return -code error $err
    }                      
}

## ******************************************************** 
##
## Name: metadata::sendData_t
##
## Description:
##
## Parameters:
##
## Usage:
## 
## Returns:
##    
## Comments:

proc metadata::sendData_t { jobid api datap } {

    if  { [ catch {
        
        ;## if this was a request for spectrum data for the
        ;## data conditioning API.
        #set ::${jobid}_DATABUCKET {}
        #lappend ::${jobid}_DATABUCKET $datap
		foreach { api host port service } [ validService $api data ] { break }
        if  { $::DEBUG == 2 } {
            set ilwdfile ${jobid}to${api}.ilwd
            set target [ ilwd::writeFile $jobid $datap $ilwdfile ascii none ]
        }
        ;## assumes a list of ilwd pointers is returned 
        # foreach ilwdp [ processDataBucket $jobid ] {
			ilwd::setjob $datap $jobid
			set tid [ sendDataElement_t $datap $port $host ]  
 
            ::setAlert $tid ::$tid
            ::setTIDCallback $tid "metadata::sendDataReaper $tid $jobid $datap"
            
            ;## after $::THREAD_CK_DELAY [ list metadata::sendDataReaper $tid $jobid $datap ]
			debugPuts "jobid '[ ilwd::getjob $datap ]' attached to $datap, thread $tid"
        # }        
    } err ] } {
        catch { emptyDataBucket $jobid }
        return -code error $err
    }                      
}

proc metadata::unsetTraceVar { tid jobid args } {

	addLogEntry "unset tid=$tid jobid=$jobid args $args" purple

}

## ******************************************************** 
##
## Name: metadata::sendDataReaper
##
## Description:
## wait for send data to finish
##
## Parameters:
## easyInstance     stmt instance name
## ilwd filename    filename for ilwd
## tag              tag to identify the results
##
## Usage:
##  metadata::outIlwd $stmtp "results.out" 
##
## Comments:
## uses ilwd namespace procs to write ilwd output
## of metadata results as native ilwd 
## pointer to ilwd object set in ::metadata::ilwdObject
## is destroyed by thread 

proc metadata::sendDataReaper { tid jobid datap args } {

    set myName [ myName ]
    set seqpt ""
    if	{ ! [ info exist ::$tid ] } {
    	;## addLogEntry "called by [ info level -1 ] ::$tid does not exist" purple
    	return
    }
	if	{ [ catch {
        set state [ set ::$tid ]
        if 	{ [ string equal FINISHED $state ] ||
              [ string equal $state $::TID_FINISHED ]  } {
		    sendDataElement_r $tid
            ::unset ::$tid
            set seqpt "destructElement $datap"
            destructElement $datap
            if  { [ string length [ info commands $datap ] ] } {
                 debugPuts "rename $datap"
                 rename $datap {}
            }
            addLogEntry "$jobid $tid $datap completed for sendDataElement_r" purple
            set seqpt "cleanup"
            cleanupJob $jobid $myName	
        }
	} err ] } {
		log "$seqpt $tid $err" 2
        catch { ::unset ::$tid }
        catch { ::unset $datap }
        catch { cleanupJob $jobid $myName }
	}	
}

## ******************************************************** 
##
## Name: metadata::dataOut
##
## Description:
## output results from sqlquery  
##
## Parameters:
## stmtp        - sql query just executed 
##
## Usage:
##  dataOut $stmtp 
##
## Comments:
##  output data in format specified 

proc metadata::dataOut { jobid numRows ilwdp { filename "" } } {

#   output to user terminal or default to ilwd file
	set seqpt "dataOut filename=$filename, ilwdp=$ilwdp"
	set ::${jobid}(product) ""
	set rc 1
	if	{ [ catch {
		foreach { retformat type } [ set ::${jobid}(format) ] { break }
    	switch $retformat {
        LIGO_LW {
           	set seqpt "sendData $jobid ligolw $ilwdp"
            after 0 [ list metadata::sendData_t $jobid ligolw $ilwdp ]
			set rc 0
            }
        ilwd { 
			set targ [ set ::${jobid}(-metadatatarget) ]
			if	{ [ regexp {^(datacond)$} $targ ] } {
				;## set comment on main container with "<dboperation> <pushpass> <alias>"				
                ;## pass sql to datacond for quality channel
                if	{ [ info exist ::${jobid}(dboperation) ] } {
					set dboperation [ set ::${jobid}(dboperation) ]
                    set alias [ set ::${jobid}(alias) ]
					set pushpass [ set ::${jobid}(pushpass) ]                    
                    setElementMetadata $ilwdp dboperation $dboperation
                    setElementMetadata $ilwdp alias $alias 
                    setElementMetadata $ilwdp pushpass $pushpass                    
                }
           		set seqpt "sendData $jobid datacond $ilwdp"
				sendData $jobid datacond $ilwdp
			} else {      
				append ::${jobid}(products) $filename 
                ;## remove object here if ilwd is output
                destructElement $ilwdp
			 }
			}
        default { error "bad -outputformat option $retformat" }
		}
    } err ] } {
		return -code error "$seqpt: $err"
	}
    ;## this is necessary to destroy the object 
    ;## object is registered so it can be destroyed by killJob
	;## done when deleteSlave is called 
    ;## catch { destructElement $ilwdp }

	return $rc 	
}
## ******************************************************** 

## ******************************************************** 
##
## Name: metadata::outIlwdFile
##
## Description:
## output results of a query to ilwd file
##
## Parameters:
## easyInstance     stmt instance name
## ilwd filename    filename for ilwd
## tag              tag to identify the results
##
## Usage:
##  metadata::outIlwd $stmtp "results.out" 
##
## Comments:
## uses ilwd namespace procs to write ilwd output
## of metadata results as native ilwd 
## pointer to ilwd object is set in ::metadata::ilwdObject
## which must be destroyed by caller.

proc metadata::outILwdFile { jobid mainContp } {

    ;## 1 => data truncation, 0 => no more data
	set rc 0
    set seqpt ""
	if	{ [ catch {
        set numRows [ getElementMetadata $mainContp rows ]
        set numCols [ getElementMetadata $mainContp cols ]
        set datatrunc [ getElementMetadata $mainContp truncated ]
              
        ;## create row container to hold result tables
		;## if query is from single table show table name
		set tabname ""
		set sql [ set ::${jobid}(-sqlquery) ]
		regexp -nocase -- {^select [^\(\)]* from (\S+).*[^,]} $sql -> tabname
		if	{ [ regexp -nocase {(join\s|as\s)} $sql ] } {
			set tabname ""
		}
		if	{ [ string length $tabname ] } {
			set tabname "$tabname:table"
		} else {
			set tabname "result_table:table"
		}
        set ::${jobid}(mainContp) $mainContp
        # registerObj $jobid $mainContp 
       
        set rowContp [ refContainerElement $mainContp 0 ] 
		regexp {(\d+)} $jobid -> njobid
		setJobId $rowContp $njobid
		
        ;## create ilwd for result cols and bind ilwd data to it
        ;## now add the LDASarray for each result column to row container
		
		set ::${jobid}(Rows) $numRows
		append ::${jobid}(product) "Retrieved $numRows rows, $numCols columns for this query via DBPull"  
		addLogEntry [ set ::${jobid}(product) ] blue
        
	    runningStatsFile $jobid getMetaData $numRows
		set targ [ set ::${jobid}(-metadatatarget) ]
		set filename ""
		if	{ ! [ regexp {^(datacond)$} $targ ] } {
			foreach { format type } [ set ::${jobid}(format) ] { break }
			if	{ [ regexp {ilwd} $format ] } {
        		set url [ set ::${jobid}(-returnprotocol) ]
				if	{ [ string length $url ] } {
   	    			foreach { target filename } [ url2file $jobid $url ilwd ] { break } 
					log "target $target filename $filename"
				} 
			}
		}
    ;## fetch next one to see if there is more than MAXROWS
    ;## already done by dbEasy, 1 ==> data truncation
        foreach { format type } [ set ::${jobid}(format) ] { break }
        set seqpt "metadata::endIlwd $jobid $mainContp $filename format=$format type=$type"
        # debugPuts $seqpt
        metadata::endIlwd $jobid $mainContp $filename $type
		set seqpt "dataOut $jobid $numRows $mainContp $filename"
		set rc 0
        set rc [ dataOut $jobid $numRows $mainContp $filename ]
    } err ] } {
        log "$seqpt: $err" 2
	    set ::${jobid}(errors) "error: $seqpt $err"
        return -code error $err
    }
    if	{ $rc } {
		cleanupJob $jobid [ myName ]
	}
}
## ********************************************************

## ******************************************************** 
##
## Name: metadata::escape
##
## Description:
## translate disallowd chars into escape chars for XML
##
## Parameters:
## xmlstr           string for xml document
##
## Usage:
##  metadata::escape $xmlstr
##
## Comments:
## Translates the following:
##
##  < &lt;
##  > &gt;
##  & &amp;
##  " &quot;
##  ' &apos;
##  returns the translated string 
##  Also handles the "section" character, which Phil
##  abuses endlessly...

proc metadata::escape { xmlstr } {
    foreach { a b } $::ilwd::esc {
       regsub -all $a $xmlstr $b xmlstr
       }
    return $xmlstr
}

## ******************************************************** 
##
## Name: metadata::setOptArgs
##
## Description:
## assemble optional args, putting quotes around strings
##
## Parameters:
## ::metadata::opts
##
## Usage:
##  metadata::setQuotes $str 
##
## Comments:
## put double quotes around string if not quoted
##
##  returns the translated string 
##  
proc metadata::setOptArgs {} {

    uplevel {
        set newargs ""
        array set opts $args
        foreach entry [ array names opts ] {        
            if { [ regexp -- {-comment} $entry ] } {
                set opts($entry) \
                [ metadata::escape $opts($entry) ]
            }
            regsub -- {^-} $entry {} attr
            append newargs "$attr='$opts($entry)' " 
        }
        unset opts
        set newargs [ string trim $newargs ]
    }
}

## ******************************************************** 
##
## Name: metadata::createRow
##
## Description:
## Creates the row container 
## returns new row container
## 
## Usage:
##
## Comments:
## Creates the row container with name string and comments
## provided

proc metadata::createRow { mainContp { tablegroup "ldas" } { tablename "" } args } { 
   
    set seqpt {}
    if { [ catch { 
       setOptArgs
       set begline "<ilwd name='$tablegroup:$tablename' size='0' $newargs></ilwd>"  
       set elemp [ putElement $begline ]
       ;##add row container to group or main container
       set size [ getContainerSize $mainContp ]
       addContainerElement $mainContp $elemp
       set rowContp [ refContainerElement $mainContp $size ]
       destructElement $elemp
    } err ] } {
       catch { destructElement $elemp }
       return -code error "[ myName ]:$seqpt $err"
    }
    return $rowContp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: metadata::endIlwd
##
## Description:
## writes end tag for outermost container
## returns the ilwd object.  
## Also writes to filename if provided 
##
## Usage:
##
## Comments:
## Assumes local file. 
## uses genericAPI to write ilwd to file

proc metadata::endIlwd { jobid mainContp { filename "" } { format ascii } { compress none } } {
   
    set seqpt ""
   	if	{ [ catch {         
    	if { ! [ regexp {^$} $filename ] } {
          set seqpt "ilwd::writeFile $jobid $mainContp $filename $format $compress" 
          ilwd::writeFile $jobid $mainContp $filename $format $compress  
        }
    } err ] } {
          return -code error "[ myName ]:$seqpt $err"
	}
    return -code ok 
}

## ******************************************************** 
##
## Name: metadata::outputformatValid
##
## Description:
## Validates getMetaData user cmd parameters
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::outputformatValid { jobid } {

	set ::${jobid}(format) ilwd

    if { [ info exists ::${jobid}(-outputformat) ] } {
        set of [ set ::${jobid}(-outputformat) ]
		if	{ [ string length $of ] } {
			set ::${jobid}(format) $of
		}
    } else {
        set of [ list ]
    }
    set of [ string trim $of ]

    if  { ! [ regexp -nocase -- {^(ligo_lw|ilwd|ilwd binary|ilwd ascii|frame)$} $of ] } {
        set msg "-outputformat $of invalid\n"
        append msg "must be 'frame', 'LIGO_LW', 'ilwd ascii or ilwd binary'."
        return -code error $msg
    } 
    if  { [ regexp {^frame$} $of ] } {
        set msg "-outputformat frame is not currently supported."
        return -code error $msg
    } 
	foreach { format type } [ set ::${jobid}(format) ] { break }

	if	{ ! [ info exist type ] || ![ string length $type ] } {
		set ::${jobid}(format) [ list $format ascii ]
	}
}

## ******************************************************** 
##
## Name: metadata::getMetaDataValid
##
## Description:
## Validates getMetaData user cmd parameters
## 
## Parameters:
##
## Usage:
##
## Comments:
## Note that it does not rely on the current global value
## of $::jobid.  When this code throws an exception, the
## metadata API's operator knows to return an errorlevel of
## "3" (abort job) to the manager. see LDASgwrap


proc metadata::getMetaDataValid { jobid } {

    if  { [ catch {
        metadata::outputformatValid $jobid 
        set sqlquery [ string trim [ set ::${jobid}(-sqlquery) ] ]
    
        if { ! [ regexp -nocase {for (read|fetch) only} $sqlquery ] } {
            set ::${jobid}(-sqlquery) "$sqlquery for read only"
        }
   	    if  { [ regexp "^\[ \t]*$" $sqlquery ] } {
        	set errmsg "SQL must not be blanks"
        	addLogEntry $errmsg 2
        	return -code error $errmsg
        } 
	    if    { [regexp -nocase -- {^-|drop|^[ \t]+$} $sqlquery] } {
		    set errmsg "non-queries not allowed: $sqlquery"
            addLogEntry $errmsg 2
		    error $errmsg
   	    }  
    } err ] } {
        return -code error $err
    }
    return -code ok    
}

## ********************************************************

## ********************************************************
## Here list the interface cmds 
## ********************************************************

## ********************************************************
##
## Name: metadata::getMetaData
##
## Description:
## submit sql and return the specified number of rows 
##  from the result set; the user can set next set of
##  rows by calling getRowSet. When done, destroy
##  the statement by calling destructStmts.
##
## Parameters: 
## args: 
## -sqlquery  - args has sql string command
## -outputformat   [ ilwd | LIGO_LW ]
## -returnprotocol [ <fileURL> | <httpURL> | <mailURL> | <ftpURL> ] 
##
## Usage:
## e.g. getData "select * from binaryinspiral" 10
##
## Comments:
## Does not destruct stmt objects unless ::keep is 1
## 

proc metadata::getMetaData { jobid } {

    set ::${jobid}(begintic) 1.0
    __t::start $jobid
    if    { [ catch { 
          set dbname [ set ::${jobid}(-database) ]
          set dbquery [ set ::${jobid}(-sqlquery) ] 
          foreach { dbuser dbpasswd } [ set ::${dbname}(login) ] { break }
          set tid [ DBPull_t $dbname $dbuser [ decode64 $dbpasswd ] $dbquery \
            $::MAXROWS $::MAX_TRIES $::pattern_getMetaData ]
          ::setAlert $tid ::$tid 
          ::setTIDCallback $tid "metadata::submitReaper $tid $jobid" 
                 
          ;## after $::THREAD_CK_DELAY [ list metadata::submitReaper $tid $jobid ]
          log "started thread $tid"  
    } err ] } {
        set ::${jobid}(errors) $err
		log $err 2
		return -code error $err
	}
}
## ********************************************************

## ******************************************************** 
##
## Name: metadata::descMetaData
##
## Description:
## user command to handle request on table names, column
##  names and 
##
## Parameters:
## args consists of:
## -table   [ all | <tabname> ]
## -column  [ all | none | <colname> ]
## -type    [ all | none ]
## -key     [ primary | foreign | none ]
## -outputformat [ ilwd | LIGO_LW ]
## -returnprotocol [ fileURL | mailURL | ftpURL | httpURL ]
##
## Usage:
## 
## Comments:
## deduce the sql for the options and pass to getMetaData
##
## standard returns: errlvl set and result string set
##
## ******************************************************** 

proc metadata::descMetaData { jobid } {

    set ::${jobid}(begintic) 1.0
    __t::start $jobid
    if    { [ catch { 
          set dbname [ set ::${jobid}(-database) ]
          foreach { dbuser dbpasswd } [ set ::${dbname}(login) ] { break }
          metaschema::init
    	  foreach entry { Key Type Table Column } {
                metaschema::set${entry} [ set ::${jobid}(-[ string tolower $entry ]) ]
          }
          set dbquery [ metaschema::makeQuery ]
          array set  ::${jobid} [ list -sqlquery $dbquery ]
          set tid [ DBPull_t $dbname $dbuser [ decode64 $dbpasswd ] $dbquery \
            $::MAXROWS $::MAX_TRIES $::pattern_getMetaData ]
          ::setAlert $tid ::$tid 
          ::setTIDCallback $tid "metadata::submitReaper $tid $jobid"
           
          ;## after $::THREAD_CK_DELAY [ list metadata::submitReaper $tid $jobid ]
          log "started thread $tid"  
    } err ] } {
        set ::${jobid}(errors) $err
		log $err 2
		return -code error $err
	}
}    
    return -code ok   
}
