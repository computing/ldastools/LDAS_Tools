## ********************************************************
## This is the metadata API specific resource file.  It contains
## resource information which is only used by the metadata API.
##
## This file should be located and named as follows:
##
##    /ldas/lib/metadataAPI/LDASmetadata.rsc
##
## If this file is not found there the metadata API will
## be non-functional
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
## ********************************************************

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES 

;## desc=database schema, mod=no
set ::DBSCHEMA IBMdb2

# desc=max number of rows in a query
set ::MAXROWS 10000

;## desc=local debug
set ::DEBUG 1

;## desc=max number of tries for deadlock txns
set ::MAX_TRIES 3

;## desc=time out for job if no data arrives (msecs)
set ::METADATA_MAX_JOB_TIME 100000

;## desc=max time before dataRecv threads are forced to finish
set  ::THREAD_TIMEOUT_SECS  50

;## desc=path to lsof executable
set ::PATH_TO_LSOF /usr/sbin/lsof

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default core default cputime default datasize default \
filesize default memorylocked default descriptors default maxproc default ]
