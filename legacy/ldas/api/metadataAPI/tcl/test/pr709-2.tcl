#!/usr/bin/env tclsh
;## $Id: pr709-2.tcl,v 1.2 2001/02/20 19:19:51 isalzman Exp $
;## Test to verify fix of pr667

;## ---------------------------------------------------------------------
;## Establish default values.
;## ---------------------------------------------------------------------

set user no_user_specified
set pwrd no_password_specified
set email no_email_specified

set host ldas-dev.ligo.caltech.edu
set port 10001

;## ---------------------------------------------------------------------
;## Procedure to send command to managerAPI
;## ---------------------------------------------------------------------
proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    regsub -- { -password\s+(\S+)} $cmd { -password **** } display_cmd
    puts $display_cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## ---------------------------------------------------------------------
;## Process user's resource file
;## ---------------------------------------------------------------------

set rc "~/.datacondAPI.rc"
if { [ file isfile $rc ] && [ file readable $rc ] } {
    source $rc
}

;## ---------------------------------------------------------------------
;## Process command line arguments
;## ---------------------------------------------------------------------

if { $argc > 0 } {
    set user [ lindex $argv 0 ]
    if { $argc > 1} {
	set pwrd [ lindex $argv 1 ]
	if { $argc > 2 }{
	    set email [ lindex $argv 2 ]
	}
    }
}

;## ---------------------------------------------------------------------
;## Command to be executed
;## ---------------------------------------------------------------------


set cmd "
ldasJob { -name $user -password $pwrd -email $email } {
    getMetaData
    -returnprotocol { file:/results }
    -returnformat { ilwd }
    -sqlquery { SELECT start_time,binarydata FROM gds_trigger WHERE binarydata is not null and length(binarydata) > 0 FETCH first 3 rows only FOR read only }
}
"
;## ---------------------------------------------------------------------
;## Execute the command
;## ---------------------------------------------------------------------

;#puts $cmd
sendCmd $cmd
