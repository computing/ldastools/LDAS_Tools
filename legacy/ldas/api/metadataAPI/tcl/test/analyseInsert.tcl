#! /ldcg/bin/tclsh

## cd /ldas_outgoing/logs/archive; ls -lrt will get in ascending order
## extract insertion logs from metadataAPI log
## 
## 673043414-673047893
;## for leapseconds
set ::env(RUNDIR) /ldas_outgoing
set ::PUBDIR /ldas_outgoing/jobs
set ::LDASLOG /ldas_outgoing/logs
set API metadata
set auto_path "/ldas/lib $auto_path"
source /ldas_outgoing/LDASapi.rsc
set ::LDASARC $::LDASLOG/archive
set ::RUNCODE [string toupper [exec cat /etc/ldasname]]
package require generic 

if	{ $argc != 3 } {
	puts "$argv0 <database> <range> <previous-results-dir>"
	exit
}
cd ./logs
if	{ ! [ regexp {logs} [ pwd ] ] } {
	puts "please run in the logs directory"
}
set dsname [ lindex $argv 0 ]
set range [ lindex $argv 1 ]
set prevdir [ lindex $argv 2 ]

set dirname "results-[ clock format [ clock seconds ] -format "%m%d" ]"
if	{ ! [ file exist $dirname ] } {
	puts "creating directory $dirname"
	file mkdir $dirname
}
cd $dirname
puts "working in directory $dirname"
set limit 10000
set pat "putMetaData inserted.+$::dsname"
set result [ grepLogs $range $pat $limit {metadata} ]
set fname insert${range}.log.html
set fd [ open $fname w ]
puts $fd $result
close $fd

catch { exec ../../createplot.tcl $fname } err
puts "createplot.tcl done\n$err"
set curfile [ glob *.txt ]
set prevfile [ glob ../$prevdir/*.txt ] 
puts "curfile $curfile,prevfile $prevfile."
catch { exec ../../dbstats.tcl $curfile $prevfile } err
puts "dbstats.tcl $curfile $prevfile done\n$err"

regexp {results-(\S+)} $prevdir -> mmdd
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.$mmdd

;## compare to previous results 0.9.0, 1.0.0, 1.1.0
set txtfile [ glob *txt ]

#set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.090/*.txt ]"
#catch { eval $cmd } 
#set statsfile [ glob *stats ]
#file rename -force $statsfile $statsfile.0.9.0

#set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.100/*.txt ]"
#catch { eval $cmd } 
#set statsfile [ glob *stats ]
#file rename -force $statsfile $statsfile.1.0.0

set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.110/*.txt ]"
catch { eval $cmd } 
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.1.1.0

set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.120/*.txt ]"
catch { eval $cmd } 
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.1.2.0

set cmd "exec ../../dbstats.tcl $txtfile [ glob ../results.130/*.txt ]"
catch { eval $cmd } 
set statsfile [ glob *stats ]
file rename -force $statsfile $statsfile.1.3.0
