#! /ldcg/bin/tclsh

## cd /ldas_outgoing/logs/archive; ls -lrt will get in ascending order
## 
## 1. Record the counts displayed in the database page.
## 2. Start the DMT via putMetaDataDMT.
## 3. Record the end time of the last cmd to metadata
## Expected results:
## 745 cmds to manager
## 745 cmds from manager to ligolw
## 745 insertions by metadata
## search for red balls if the above is not met
## 673043414-673047893
;## for leapseconds
set ::PUBDIR /ldas_outgoing/jobs
set ::LDASLOG /ldas_outgoing/logs
set API metadata
set auto_path "/ldas/lib $auto_path"
source /ldas_outgoing/LDASapi.rsc
set ::LDASARC $::LDASLOG/archive
package require generic 

set limit 10000

if  { $argc > 0 } {
    set range [ lindex $argv 0 ]
}
set dsname [ lindex $argv 1 ]
puts "checking logs in gps time $range for database $dsname"

proc matchJobs { joblist1 joblist2 } {
    set notin {}
    foreach jobid $joblist1 {
        if  { [ lsearch -exact $joblist2 $jobid ] == -1 } {
            lappend notin $jobid
        }
    }
    puts "Found [llength $notin] missing jobs <$notin>"
    return $notin
}

proc dumpJobs { joblist } {
    puts "joblist=$joblist"
    foreach jobid $joblist {
        puts "jobid=$jobid, [ queryLogArchive $jobid ]"
    }
}

proc doresult { result api pat } {
    set result [ split $result \n ]
    set len [ llength $result ]
    set ::${api}($pat) [ list ]
    foreach line $result {
        if  { [ regexp  {<img src=.+NORMAL(\d+)} $line match jobid ] } {
                lappend ::${api}($pat) $jobid
        }             
    }
    puts "Found [ llength [ set ::${api}($pat) ]] for $pat in $api"
}

set pat "putMetaData inserted.+$::dsname.+gds_trigger"
set result [ grepLogs $range $pat $limit {metadata} ]
doresult $result metadata inserted
set result [ grepLogs $range {cmd=putMetaData } $limit {metadata} ]
doresult $result metadata received
set result [ grepLogs $range {ball_red.gif} $limit {metadata} ]
puts "Errors found in metadataAPI:\n$result"
puts $result
doresult $result metadata error 
set result [ grepLogs $range Trig $limit {ligolw} ]
doresult $result ligolw received
set result [ grepLogs $range Trig $limit {manager} ]
doresult $result manager received

set metadata_inserted [ llength $::metadata(inserted) ]
set metadata_received [ llength $::metadata(received) ]
puts "metadataAPI received $metadata_received, inserted $metadata_inserted"
set notin_metadata [ list ]
if  { $metadata_inserted != $metadata_received } {
    puts "Jobs not inserted in metadataAPI"
    set notin_metadata [ matchJobs [ set ::metadata(received) ] [ set ::metadata(inserted)] ]    
} else {
    set notin_metadata [ list ]
}
 
set ligolw_received [ llength $::ligolw(received) ]
set manager_received [ llength $::manager(received) ]
if  { $ligolw_received != $manager_received } {
    puts "Jobs in manager not received by ligolwAPI"
    matchJobs [ set ::manager(received) ] [ set ::ligolw(received) ]
}

if  { $ligolw_received != $metadata_received } {
    puts "Jobs not sent from ligolw to metadataAPI"
    set notsent_metadata [ matchJobs [ set ::ligolw(received) ] [ set ::metadata(received)] ]    
} else {
    set notsent_metadata [ list ]
}
  
foreach job $notin_metadata {
    puts "job=$job"
    puts [ queryJobLogs $job ".+" 1000 {manager ligolw metadata}]

}

foreach job $notsent_metadata {
    puts "job=$job"
    puts [ queryJobLogs $job ".+" 1000 {manager ligolw metadata}]

}
