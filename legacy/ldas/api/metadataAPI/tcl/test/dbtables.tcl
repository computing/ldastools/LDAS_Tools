#! /ldcg/bin/tclsh

set ::Tables {
	process
	runlist
	calib_info
	exttrig_search
	process_params
	frameset_chanlist
	frameset_writer
	frameset
	frameset_loc
	segment_definer
	segment
	summ_value
	summ_statistics
	summ_spectrum
	summ_csd
	summ_comment
	summ_mime
	filter
	filter_params
	sim_inst
	sim_inst_params
	sim_type 
	sim_type_params
	gds_trigger
	sngl_inspiral
	sngl_burst
	sngl_ringdown
	sngl_unmodeled
	sngl_dperiodic
	sngl_datasource
	sngl_transdata
	sngl_mime
	sngl_unmodeled_v
	multi_inspiral
	multi_burst
	coinc_sngl
	search_summary
	search_summvars
	waveburst
	waveburst_mime
}

set tbl_level0 "process runlist calib_info sim_type"
set tbl_level1 "process_params frameset_chanlist frameset_writer segment_definer filter search_summary \
sim_inst sim_type_params"
set tbl_level2 "frameset segment filter_params gds_trigger sngl_inspiral sngl_burst sngl_ringdown \
sngl_unmodeled sngl_dperiodic multi_inspiral multi_burst exttrig_search sim_inst_params waveburst"
set tbl_level3 "frameset_loc summ_value summ_statistics summ_spectrum summ_comment summ_csd \
summ_mime sngl_unmodeled_v coinc_sngl search_summvars waveburst_mime"
set tbl_level4 "sngl_transdata sngl_mime sngl_datasource"
