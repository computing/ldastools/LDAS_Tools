## ********************************************************
##
## Name: examples2.sql 
##
## Description:
## verifies putMetaData insertions
## presented as user command options
## used as input to test driver also
## ******************************************************** 
;#barecode

;##
;## get all rows from each table
;##
-sqlquery {select * from process} 

;##
-sqlquery {select * from process_params order by program} 

;##
-sqlquery {select * from frameset_chanlist} 
 
;##
-sqlquery {select * from frameset_writer} 

;## 
-sqlquery {select * from frameset} 

;## 
-sqlquery {select * from frameset_loc} 

;##
-sqlquery {select * from segment_definer} 

;##
-sqlquery {select * from segment} 

;##
-sqlquery {select * from summ_value} 

;##
-sqlquery {select * from summ_statistics} 

;## 
;##-sqlquery {select * from summ_spectrum} 

;## 
-sqlquery {select * from summ_comment} 

;##
-sqlquery {select * from filter fetch first 100 rows only} 

;## 
-sqlquery {select * from filter_params} 

;##
;##-sqlquery {select * from gds_trigger} 

;## 
-sqlquery {select * from sngl_inspiral} 

;##
-sqlquery {select * from sngl_burst} 

;## 
-sqlquery {select * from sngl_ringdown order by event_id}

;##
-sqlquery {select * from sngl_unmodeled} 

;## 
-sqlquery {select * from sngl_dperiodic} 

;##
-sqlquery {select * from sngl_datasource} 

;## 
;##-sqlquery {select * from sngl_transdata} 

;## 
-sqlquery {select * from sngl_unmodeled_v} 

;##
-sqlquery {select * from multi_inspiral} 

;## 
-sqlquery {select * from multi_burst}
  
;## 
-sqlquery {select * from coinc_sngl}  

;#end
