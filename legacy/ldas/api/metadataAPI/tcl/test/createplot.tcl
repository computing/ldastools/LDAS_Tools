#! /ldcg/bin/tclsh

## output txt file for importing to excel spreadsheet
## output html file for web display

proc getMem {} {
	catch { exec top | grep Memory } data
	if	{ [ regexp {Memory} $data ] } {
		return $data
	}
	return "No memory info"
}

puts [lindex $argv 0 ]
set infile [ lindex $argv 0 ]
set ftime [ clock format [ clock seconds ] -format "%Y%m%d" ]
set verstr [ exec ls -ld /ldas/lib ]
if	{ ! [ regexp {stow_pkgs/(ldas-[\d.]+)} $verstr match version ] } {
	set version "ldas.0.1.alpha"
} 

set fd [open $infile r]
set count 0

set lines [ read $fd ]
set lines [ split $lines \n]
close $fd

set tables {}
set dbnames {}
set outlog "createplot.log"
set flog [ open $outlog w ]
set dimset { 1 10 100 1000 }

source /ldas/doc/db2/doc/text/dbtables.tcl 

#set origtables { process runlist process_params frameset_chanlist frameset_writer segment_definer filter \
#frameset segment filter_params gds_trigger sngl_inspiral sngl_burst sngl_ringdown sngl_unmodeled sngl_dperiodic multi_inspiral multi_burst \
#frameset_loc summ_value summ_statistics summ_spectrum summ_comment summ_csd summ_mime \
#sngl_transdata sngl_unmodeled_v sngl_mime coinc_sngl sngl_datasource }

foreach line $lines {  

    if  { [ regexp {inserted\s+(\d+) rows into database (\S+) since start up} $line -> totrows dbname ] } {
        if  { [ lsearch -exact $dbnames $dbname ] == -1 } {
            lappend dbnames $dbname   
		}
        array set ::${dbname} [ list totrows $totrows ]
        continue
    }
    if  { [ regexp {putMetaData inserted\s+(\d+) rows into (\S+) database table (\S+) at the rate of\s+([\d.]+) rows/sec, took walltime\s+([\d.]+) secs} \
                $line -> dims dbname table rate walltime ] } { 
		puts $flog "dbname=$dbname,walltime=$walltime,dims=$dims,table=$table,rate=$rate"
        set ${dbname}_${table}_${dims}(walltime) $walltime
		set ${dbname}_${table}_${dims}(rate) $rate
		set ${dbname}_${table}_${dims}(cputime) 0.0
        
        ;## define tables to check for missing data
        if  { ! [ info exist ::${dbname}(tables) ] } {
            array set ::${dbname} [ list tables [ list ] ]
        }
        if  { [ lsearch -exact [ set ::${dbname}(tables) ] $table ] == -1 } {
            lappend ::${dbname}(tables) $table
		}
        continue     
    }   
}

foreach dbname $dbnames {
    set outfile "insert.$dbname.$version.$ftime.txt"
    set fout [ open $outfile w ]
    foreach var { rate walltime cputime } {
        puts $fout "The $var\t[join $dimset \t]\n"
    
    ;## use this to check out missing data 
        foreach table $::Tables {
        puts -nonewline $fout "$table\t"
        foreach dimtype $dimset {
            if  { [ info exist ${dbname}_${table}_${dimtype}(${var}) ] } {
				puts "$var, $table, $dimtype, value [set ${dbname}_${table}_${dimtype}(${var})]"
                puts -nonewline $fout "[ format "%.3f" [set ${dbname}_${table}_${dimtype}(${var})] ]\t"
            } else {
                puts -nonewline $fout "\t"
                puts "Error: no dims=$dimtype for $table, var=$var"
            }
        }
        puts $fout ""
        }
        puts $fout "\n"
    }
    close $fout
}

;## output the html file
set fd [ open /etc/ldasname r ]
set domain [ read $fd ]
regexp {^(.+)\n} $domain -> domain
close $fd
set memory [ getMem ]
puts "dbname $::dbnames"
foreach dbname $::dbnames {

    set htmlfile "$domain.$dbname.$version.$ftime.html"
    set fhtml [ open $htmlfile w ]

    ;## output the heading
    set currtime [ clock format [clock seconds] -format "%B %d %Y %H:%M:%S" ]

    set heading "\
<!doctype html public \"-/w3c/dtd html 4.0 transitional/en\">\n\
<html>\n\
<head>\n\
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n\
   <meta name=\"GENERATOR\" content=\"Mozilla/4.5 \[en\] (X11; I; SunOS 5.6 sun4u) \[Netscape\]\">\n\
   <title>$domain database $dbname Insertion Rates</title>\n\
</head>\n\
<body>\n\
\n\
<pre><b><font size=-1>$domain Database $dbname Insertion Rates</font></b></pre>\n\
<font size=-2>\n\
<table cellpadding=1>\n\
<tr>\n\
    <th align=left width=150>Test Period</th>\n\
    <td>$currtime</td>\n\
</tr>\n\
<tr>\n\
    <th align=left width=150>Input data</th>\n\
    <td>Ilwd with dimensions 1,10,100,1000</td>\n\
    <td>\n\
</tr>\n\
<tr>\n\
    <th align=left width=150>Platform</th>\n\
    <td>[ exec uname -a ]<BR>$memory</td>\n\
    <td>\n\
</tr>\n\
<tr>\n\
    <th align=left width=150>LDAS software</th>\n\
    <td>$version, IBM DB2 7.1</td>\n\
    <td>\n\
</tr>\n\
</table></font>\n\
\n\
<pre><i><font size=-1>Results are displayed in the order inserted.</i></font></pre>"

    puts $fhtml $heading

    puts $fhtml "<table border type=1><font size=-1>"
    puts $fhtml "<tr>\n\
    <th align=left width=200>Table</th>\n\
    <th width=100>#rows per block insert</th>\n\
    <th width=100>CPU time (secs)</th>\n\
    <th width=100>Wall Time (secs)</th>\n\
    <th width=100>Rate (#rows/sec)</th>\n\
</tr>"

    set bgcolor(1) " bgcolor=\"#ffe6ef\""
    set bgcolor(10) " bgcolor=\"#ffe6ab\""
    set bgcolor(100) " bgcolor=\"#ffe6cd\""  
    set bgcolor(1000) " bgcolor=\"#ffe669\"" 

    foreach table [ set ::${dbname}(tables) ] {
	    foreach dimtype $dimset {	
		    puts $fhtml "<th align=left width=100>$table</th>\n\             
    	<td align=right$bgcolor($dimtype)>$dimtype</td>\n\
    	<td align=right$bgcolor($dimtype)>[ format "%.3f" [set ${dbname}_${table}_${dimtype}(cputime) ] ]</td>\n\
   	 	<td align=right$bgcolor($dimtype)>[ format "%.3f" [set ${dbname}_${table}_${dimtype}(walltime)] ]</td>\n\
    	<td align=right$bgcolor($dimtype)>[ format "%.3f" [set ${dbname}_${table}_${dimtype}(rate) ]  ]</td>\n\
		</tr>"
	    }
    puts $fhtml "\n"
    }

    puts $fhtml "</table></font>\n\
<p>Summary: \n\
</font>\n\
</body>\n\
</html>"

    close $fhtml
}
exit
