#! /bin/sh
# working directory is /ldas_outgoing/data
# must source ~/etc/cshrc.metaserver
# ln -s /usr1/ldas/mlei/LDAS/LDASapi.rsc LDASapi.rsc

db2=/usr2/${DB2INSTANCE}/sqllib/bin/db2
echo "db2=$db2"
#dimset="1 10 100 1000"

# tables are inserted by levels, the next level depends on the first
tbl_level0="process runlist"
tbl_level1="process_params frameset_chanlist frameset_writer segment_definer filter"
tbl_level2="frameset segment filter_params gds_trigger sngl_inspiral sngl_burst sngl_ringdown sngl_unmodeled sngl_dperiodic multi_inspiral multi_burst"
tbl_level3="frameset_loc summ_value summ_statistics summ_spectrum summ_comment summ_csd summ_mime sngl_transdata sngl_unmodeled_v sngl_mime coinc_sngl"
tbl_level4="sngl_datasource"

# creates ilwds based on new xrefs in db and insert them into db
insert_tables()
{
# make the source code using include file with db data
echo "recompiling ilwds for tables $table_list"
for table in $table_list
do
	make ${table}ilwd
done
echo "generating ilwds for tables $table_list"
for dims in $dimset
do
	for table in $table_list
	do
		${table}ilwd $dims > ${table}${dims}.ilwd
		echo "${table}${dims}.ilwd generated"
		metadata_test.tcl -data ${table}${dims}.ilwd -type ${mode} -cmd putMetaData -database ${db}
		sleep 10
	done
done
}

# extract uniqueId etc. xref into include files
extract_dbref()
{
echo "extracting db info for $table_list"	
$db2 -tf get_refs.sql > dbrefs.out
echo "converting db info to include file for ilwd"
# generate the include files for ilwd
xrefs.tcl dbrefs.out
}

# extract event uniqueIds xref into include files
extract_dbevents()
{
$db2 -tf get_event_id.sql > dbevents.out
echo "converting db info to include file for ilwd"
# generate the include files for ilwd
eventIds.tcl dbevents.out
}

# main script
if	[ $# -lt 2 ] 
then 
	echo "insertall.sh <dsname> [ user | meta ]"
	exit 1
fi

if  [ ${1} =  "" ] 
then 
	echo "Please specify database name"
	exit 1
fi

database=$1
echo "database is $1, mode is $2"
mode=$2
db=$1 

if	[ $mode != "user"  -a  $mode != "meta" ] 
then
	echo "Valid mode is user or meta"
	exit 1
fi

# prompt for database password 
# to avoid storing in script, turn off echo
savedstty=`stty -g`
stty -echo 
echo "Enter password for database user ldasdb: "
read dbpasswd
# turn on echo again
stty $savedstty

# first clear the database
echo "deleting all rows from $1"
$db2 connect to $1 user ldasdb using $dbpasswd

# dont do it for now on production database
$db2 -tf delall.sql

# insert process table first
echo "inserting process table"
dimset="1000"
table_list="process"
insert_tables

dimset="1 10 100 1000"
# insert level one tables into database
echo "inserting level one tables"
sleep 10
extract_dbref
table_list=$tbl_level1
insert_tables 

# insert level two tables into database

extract_dbref
table_list=$tbl_level2
insert_tables $tbl_level2
	
#insert level three tables into database
extract_dbref
extract_dbevents
table_list=$tbl_level3
insert_tables $tbl_level3

table_list=$tbl_level4
for table in $table_list
do
	make ${table}ilwd
done
echo "generating ilwds for tables $table_list"
for dims in $dimset
do
	for table in $table_list
	do
		${table}ilwd $dims > ${table}${dims}.ilwd
		echo "${table}${dims}.ilwd generated"
		$db2 'delete from sngl_datasource'
		metadata_test.tcl -data ${table}${dims}.ilwd -type ${mode} -cmd putMetaData -database ${db}
		sleep 10
	done
done
# insert remaining level 0 tables 
dimset="1 10 100"
table_list=$tbl_level0
insert_tables

$db2 terminate
echo "insertions done, see LDASmetadata.log.html for rates"
exit 0
