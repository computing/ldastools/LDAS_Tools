#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
use_ligotools="/ligotools/bin/use_ligotools"
# \
test -x $use_ligotools && eval `$use_ligotools`
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}
if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib
package require LDASJob
package require tconvert

# must source ~/etc/cshrc.metaserver
# This test validates that the data retrieved is the same
# as the data inserted

source testlib.tcl
source /ldas_outgoing/LDASapi.rsc
source ./dbtables.tcl

set SITE gateway
set RUNCODE [string toupper [exec cat /etc/ldasname]]
set TESTDIR [ pwd ]
set dimset { 1 10 100 1000 }
set curdir [ pwd ]
puts "arguments \{$argv.\}"
set ::USER mlei
puts "site $::SITE"

# make the source code using include file with db data
proc insert_tables {table_list} {
    foreach table $table_list {
        puts "recompiling ilwds for table $table"
        catch { exec make ${table}ilwd } err
        puts $err

        puts "generating ilwds for tables $table"
        catch { exec ${table}ilwd $::dims > ${table}${::dims}.ilwd } err

        puts "${table}${::dims}.ilwd generated, inserting data"
        #catch { exec metadata_test.tcl -data ${table}${::dims}.ilwd \
		#-type user -cmd putMetaData -database $::dsname \
		#-email ${::LADDR}:${::LPORT}} err
		set cmd "${::putMetaDataCmd}$::curdir/${table}${::dims}.ilwd"
		catch {LJrun job -nowait -email ${::LADDR}:${::LPORT} -manager $::SITE \
			-user $::USER $cmd} err
        #puts $err
		if {$LJerror} {
        	if {[info exists ::job(error)]} {
            	set err $::job(error)
        	}
        	LJdelete job
			exit
    	}
		if 	{ [ regexp "(${::RUNCODE}\\d+)" $err -> jobid ] } {
			lappend ::STAT(SendList) $jobid
			puts "sent jobs $::STAT(SendList)"
		}
		LJdelete job   
    }
	vwait ::done
	puts "$table_list inserted"
}

# extract uniqueId etc. xref into include files
proc extract_dbref { table_list } {
    puts  "extracting db info for $table_list"
	file delete -force dbrefs.out 
    set cmd "-tf get_refs.sql > dbrefs.out"
	# set fd [ open dbrefs.out r ]
	# puts "dbrefs.out\n[read $fd ]"
	# close $fd
	
    set cmd "exec $::db2 $cmd"
    # puts "cmd=$cmd"
    catch { eval $cmd } err
    # puts $err
    puts "extract_dbref, converting db info to include file for ilwd"
    # generate the include files for ilwd
    catch { exec xrefs.tcl dbrefs.out } err
    puts $err
}

# extract event uniqueIds xref into include files
proc extract_dbevents {} {
    set cmd "-tf get_event_id.sql >dbevents.out"
    set cmd "exec $::db2 $cmd"
    puts "cmd=$cmd"
    catch { eval $cmd } err
    puts "extract_dbevents, converting db info to include file for ilwd"
    # generate the include files for ilwd
    catch { exec eventIds.tcl dbevents.out } err
    puts $err
}

;## invoke analyse script for insertions
;## ssh needs the following env setup to lgoin without password
;## SSH_AUTH_SOCK=/tmp/ssh-gaR13363/agent.13363
proc analyseX {} {
	uplevel {
;## retrieve the logs to get the insertion rates
		catch { execssh gateway \
         /usr/bin/env PATH=$::LDAS/bin:/usr/bin:/bin:$::env(PATH) \
		 LD_LIBRARY_PATH=$::LDAS/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		 "$TESTDIR/analyseInsert.tcl $dsname $start_time-$end_time $prevdir \
         >& $TESTDIR/analyseInsert.log &" } err
		puts "starting analyseInsert.tcl, $err, see $TESTDIR/analyseInsert.log " 
	}
}

proc analyse {} {
	uplevel {
		catch { exec analyseInsert.tcl $dsname $start_time-$end_time $prevdir \
         	>& $TESTDIR/analyseInsert.log & } err
		puts $err
	}
}

;## main line begins
if { ! [ string match metaserver [ exec uname -n ] ] } {
    puts "please execute this script on metaserver"
    exit
}
if	{ $argc < 3 } {
	puts "$argv0 <database> <dims> <previous-results-dir> <validate \[0|1\]>"
	exit
}
# tables are inserted by levels, the next level depends on the first
#set tbl_level0 "process runlist calib_info sim_type"
#set tbl_level1 "process_params frameset_chanlist frameset_writer segment_definer filter search_summary \
#sim_inst sim_type_params"
#set tbl_level2 "frameset segment filter_params gds_trigger sngl_inspiral sngl_burst sngl_ringdown \
#sngl_unmodeled sngl_dperiodic multi_inspiral multi_burst exttrig_search sim_inst_params waveburst"
#set tbl_level3 "frameset_loc summ_value summ_statistics summ_spectrum summ_comment summ_csd \
#summ_mime sngl_unmodeled_v coinc_sngl search_summvars waveburst_mime"
#set tbl_level4 "sngl_transdata sngl_mime sngl_datasource"

set ::PUBDIR /ldas_outgoing/jobs
set ::LDASLOG /ldas_outgoing/logs
set API metadata
set auto_path "/ldas/lib $auto_path"
source /ldas_outgoing/LDASapi.rsc
set ::LDASARC $::LDASLOG/archive
package require generic 

if	{ [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	set db2 /usr2/$env(DB2INSTANCE)/sqllib/bin/db2
}
set ::DB2PATH [ file dir $db2 ]
puts "DB2PATH $DB2PATH"

set dsname [ lindex $argv 0 ]
set dimset [ split [ lindex $argv 1 ] , ]
puts "dims $dimset"
set prevdir [ lindex $argv 2 ]
set validate 0
if	{ $argc == 4 } {
	set validate [ lindex $argv 3 ]
}

if  { ! [ string match metaserver [ exec uname -n ] ] } {
    puts "please run this test from dataserver"
    exit 
}
#if  { ! [ info exist ::env(SSH_AUTH_SOCK) ] } {
#    puts "please set env SSH_AUTH_SOCK for log extraction"
#    exit 
#}

if	{ ! [ regexp {database} [ pwd ] ] } {
	puts "please run in the test/database directory"
	exit
}

set putMetaDataCmd "putMetaData \
    -database $::dsname \
	-ingestdata file:"

set ::HOST gateway
set ::PORT 10001
set ::WAIT 40000
set ::WAIT_MODS 0
set ::WAIT_INCR 10000
set ::TIMEOUT_RECV 50
set ::TIMEOUT_SEND 300000
set ::LOGFILE [file rootname [file tail $::argv0]].log
set ::TIMESTAMP {%m/%d/%y-%H:%M:%S}

set localport 0
set start_time [ gpsTime now ]
;## open a port to listen for returns
set sid [serverOpen $localport ]
puts "BEGN: [gpsTime] $::LHOST $::LADDR $::LPORT"
puts "WAIT: $::WAIT"
clearCmd
set cmd "connect to $dsname user ldasdb using [decode64 $::dblock]"
set cmd "exec $::db2 $cmd"
catch { eval $cmd } data
puts $data

foreach ::dims $dimset {
# first clear the database
puts "deleting all rows from all tables at $dsname"
set cmd "-tf ./delall.sql"
set cmd "exec $::db2 $cmd"
#puts "cmd=$cmd"
catch { eval $cmd } err
puts $err

# delete files from previous run
foreach table [concat $tbl_level0 $tbl_level1 $tbl_level2 $tbl_level3 $tbl_level4] {
    file delete -force ${table}.h
    file delete -force ${table}ilwd.o
    file delete -force ${table}ilwd
}
file delete -force events.h events_by_table.h commonilwd.o

# insert into the database rows of each table
set level 0
foreach tablist [ list $tbl_level0 $tbl_level1 $tbl_level2 $tbl_level3 $tbl_level4 ] {
    insert_tables $tablist
    after 5000
    extract_dbref $tablist
    if { $level == 3  } {
        extract_dbevents
    }
    incr level 1
}

set end_time [ gpsTime now ] 
puts "metadata test ran from $start_time-$end_time"

if	{ !$validate } {
	continue
}
set failed [ list ]
;## retrieve all the table data 

foreach table [ concat $tbl_level0 $tbl_level1 $tbl_level2 $tbl_level3 $tbl_level4 ]  {
	set cmd "getMetaData \
    	-returnprotocol file:/getMeta \
    	-outputformat {LIGO_LW} \
    	-database {$::dsname} \
    	-sqlquery {select * from $table }"
		
	catch {LJrun job -email ${::LADDR}:${::LPORT} -manager $::SITE -user $::USER $cmd} err	
	
    #catch { exec ./metadata_test.tcl -data 1 -type user -cmd getMetaData -database $dsname \
    #    -sql "text:-sqlquery {SELECT * from $table}" -ext LIGO_LW -email ${::LADDR}:${::LPORT} } err
	
    #puts $err
	if {$LJerror} {
        if {[info exists ::job(error)]} {
            set err $::job(error)
        }
        LJdelete job		
        incr ::STAT(ErrS)
        continue
    }
	LJdelete job
    if { [ regexp "(${::RUNCODE}\\d+)" $err -> jobid ] } {
		lappend ::STAT(SendList) $jobid
		set ${table}(getMeta) $jobid 
    }
}
vwait ::done

foreach table [ concat $tbl_level0 $tbl_level1 $tbl_level2 $tbl_level3 $tbl_level4 ]  {
	set jobid [ set ${table}(getMeta) ]
	set output [jobDirectory $jobid]/getMeta.xml
	chkfile $output
	puts "getMetaData job $jobid output is $output"
	;## dims 1000 takes a long time to validate
	if	{ $::dims >= 1000 } {
		continue
	}
    catch { exec ./ilwdValidate.tcl ${table}${::dims}.ilwd $output } err
    ;## this error maybe suppressed
    puts $err
    set err [ split $err \n ]
    set last [ lindex $err end ]
    if { [ regexp {validate} $last ] } {
        puts "Test passed for table $table."
    } else {
        puts "Test failed for table $table."
        foreach line $err {
            if { [ regexp {mismatched} $line ] } {
                puts "$table: $line"
            }
        }
        lappend failed $table
    }
}

if { [ llength $failed ] } {
    puts "failed tables: $failed."
} else {
    puts "All tables passed."
}
;## now move on to next set of dims 
;## end for each dims
}

set end_time [ gpsTime now ]
;## now get the insertion rates 
analyse 
puts "metadata test done"
