#! /ldcg/bin/tclsh
puts "deldmt.tcl performs the following:\n\
1. Selects the process table for process matching <colname>=<value>\n\
2. Deletes the children tables process_params, \
gds_trigger and segment that reference the process;\n\
3. Deletes the process row\n"

proc execdb2cmd { cmd } {
	uplevel {
		set cmd "exec $::db2 $cmd"
		# puts $cmd
		catch { eval $cmd } data
		puts $data
	}
}

set TOPDIR /ldas_outgoing
set LDAS /ldas
if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
puts "db2=$db2"

set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl

set dsname [ lindex $argv 0 ]
set colparms [ lindex $argv 1 ]
puts $colparms
if	{ [ regexp {([^=]+)=(.+)} $colparms  -> colname value ] } {
	puts "colname $colname, value=$value"
} else {
	puts "deldmt.tcl <database> <colname>=<value>, e.g. deldmt.tcl mit_test program=SegGener"
	exit
}
if	{ ! [ regexp {^\d+$} $value ] } {
	set value "'$value'"
}

if  { $argc == 3 } {
    set delete [ lindex $argv 2 ]
} else {
	set delete 1
}
set cmd "connect to $dsname user ldasdb using [ decode64 $::dblock ]"
execdb2cmd $cmd

source dbtables.tcl

set childtables [concat $tbl_level4 $tbl_level3 $tbl_level2 $tbl_level0 $tbl_level1]

# first select the process
puts "selecting $colname from process from database $dsname"
set cmd "select process_id from process where $colname=$value"
execdb2cmd $cmd

set data [ split $data \n ]
set process_id ""
set processes [ list ]

foreach line $data {
	puts $line
	if	{ [ regexp {^(x'\d+')$} $line -> process_id ] } {
		lappend processes $process_id
	}
}
puts "processes $processes"
set processes [ join $processes , ]
if	{ ! [ llength $processes ] } {
	puts "No rows found in process table for $colname=$value"
	exit
}

foreach table $childtables {
    set cmd "select count(*) from $table where process_id in ($processes)"
	execdb2cmd $cmd
	if  { $delete } {
		set cmd "delete from $table where process_id in ($processes)"
		execdb2cmd $cmd
		set cmd "select count(*) from $table where process_id in ($processes)"
	}	
} 

;## delete process
if  { $delete } {
	set cmd "delete from process where $colname=$value"
	execdb2cmd $cmd

	;## verify the deletion
	set cmd "select count(*) from process where $colname=$value"
	execdb2cmd $cmd
}
set cmd terminate
execdb2cmd $cmd




