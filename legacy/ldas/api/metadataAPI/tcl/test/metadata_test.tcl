#! /ldcg/bin/tclsh 

## ******************************************************** 
##  metadata_test.tcl Version 1.0
##
##  test sending cmds to operator socket concurrently
##  
##  run this
## ******************************************************** 

;#barecode
;## long records like summ_spectrum takes 13-15 secs to insert
;## adapted from cmd::result in genericAPI
;## stolen some code from genericAPI

proc myName { { level "-1" } } { 
     
     if { $level > 0 } {
        return -code error "myName: called with level > 0 ($level)."
     }

     if { [ catch {
        set name [ lindex [ info level $level ] 0 ]
     } err ] } {
        set name $::API
     }
     set name
}

proc expandOpts { { opts "opts" } } {
     set options [ list ]
     set matches [ list ]
     ;## get inputs and massage them into a
     ;## well-formed list
     set inputs [ uplevel set args ]
     regsub -all -- {\s+} $inputs { } inputs
     regexp {^\{(.+)\}$}    $inputs  -> inputs
     ;## get defaults and do the same
     set defaults [ uplevel subst \$$opts ]
     regsub -all -- {\s+} $defaults { } defaults
         
     ;## and GO! (trimming loose spaces from values)
     foreach { opt def } $defaults {
        set opt     [ string trim $opt  ]
        set def     [ string trim $def  ] 
        
        set matched 0
        foreach { name val } $inputs {
           set name [ string trim $name ]
           regsub -- {[-]+} $name "-" name
           set val  [ string trim $val  ]
           ;## disallow abbreviations
           if { [ string match $name $opt ] } {
              ;## next test fails if ambiguous item is last
              ;## option on command line...
              if { [ lsearch -exact $matches $name ] > -1 } {
                 return -code error "[ myName ]: ambiguous option: \"$name\""
              }
              
              ;## use the specific opts instead of supporting abbreviations
              lappend matches $name
              set options [ concat $options [ list $opt $val ] ]
              set matched 1
              break
           }
        }
        if { $matched == 0 } {
           set options    [ concat $options [ list $opt $def ] ]
        }
     }   
     return $options
}

## ******************************************************** 
##
## Name: dumpFile
##
## Description:
## return the contents of a file in a form suitable for
## "more" or "less" etc.
##
## Usage:
##       set data [ dumpFile filename ]
##
## Comments:
## This is an efficient slurper of files.
proc dumpFile { { file "" } } {
     if { ! [ string length $file ] } {
        return {}
        }
     if { ! [ file exists $file ] } {
        return {}
        }
     if { [ catch { set fid [ open $file r ] } err ] } {
        return -code error $err
        }
     set size [ file size $file ]
     set data [ read $fid $size ]
     catch { ::close $fid }
     set data [ string trim $data ]
     return $data
}

proc readData { sid cmd index { timeout 30000 } } {

    set data ""
    set i 0 
    while { 1 } {
        after $i;
        if  { [ gets $sid line ] < 0 } {
            if  { [ eof $sid ] } {
                break
            }
            incr i 1
            if  { $i > $timeout } { 
                puts "timed out"  
                break
            }
            continue
        } else {     
              set i 0
        }
        append data "$line\n"
        set line ""
    }
    ;## must close socket since cmd is done
    catch { close $sid }
    puts "Results for job $index, cmd=$cmd\n$data"
    catch { close $sid }
	incr ::job_completed 1
	if	{ $::job_completed == $::job_sent } {
		set ::DONE 1
	}
}

proc bgerror { msg } {
     puts stderr "${::API}API bgerror: $msg"
     set strlist [ split $msg ]
     set index [ lsearch $strlist "*sock*" ]
     if  { $index > -1 } {
         set sid [ lindex $strlist $index ]
         regsub -all  {["]} $sid {} sid 
         puts "bgerror sid = $sid."
    }
}

proc sendCmd { cmd index } {
    puts "sending job $index, cmd = $cmd"
    if { [ catch { 
        set sid [ socket $::host $::port ]
        fconfigure $sid -buffering line
        } ] } {
        set sid "unreachable"
        set msg "could not connect to metadata at $::host:$::port"
        return -code error "socket open:\n$msg"
        }    
    puts $sid $cmd
	incr ::job_sent 1
    fileevent $sid readable [ list readData $sid $cmd $index ]    
}

proc setLdasSystemName {} {
if { ! [ info exists ::LDAS_SYSTEM ] } {
	if	{ [ file exist /etc/ldasname ] } {
		set fd [ open /etc/ldasname r ]
		set ::LDAS_SYSTEM [ read $fd ]
		close $fd
	} else {
		set ::LDAS_SYSTEM localhost
	}
}
set ::LDAS_SYSTEM [ string trim $::LDAS_SYSTEM ]

    switch -exact -- $::options(-type) {
        meta { set ::dest metadata; source ./LDASapi.rsc }
        user { set ::dest manager; 
            if  { [ file exist /ldas_outgoing/LDASapi.rsc ] } {
                source /ldas_outgoing/LDASapi.rsc 
            } else {
                source /ldas/bin/LDASapi.rsc 
            }
         }
        default { error "invalid type $type" }
    }
       set hosts [ info vars ::*_API_HOST ]
        foreach host $hosts {
           regexp {::([^_]+)} $host -> api
           set host [ set $host ]
           set api [ string tolower $api ]
           set ::${::LDAS_SYSTEM}($api) $host
        }  
        
    ;## set standard ports for all API's
     set i 0
     foreach api $::API_LIST {
        array set ::$api "host [ set ::${::LDAS_SYSTEM}($api) ]"
        foreach sock { operator emergency data } {
           array set ::$api "$sock [ expr { $::BASEPORT + [ incr i ] } ]"
        }
    }   
}
 
;## set up environment 
set API metadata
set auto_path "/ldas/lib $auto_path"

set me [exec uname -n]   

set user mlei
set email "mlei@ligo.caltech.edu"
set password "******"


set job_sent 0
set job_completed 0

;## argv specifies options
;## 
;## -times <iterations
;## -cmd [ getMetaData | putMetadata | descMetaData
;## -data [ all | #num ]
;## -type [ meta | user ]
;## -sql [ file:<fname> | text:-sqlquery {...} ]
;## -ext { ilwd | LIGO_LW ]
;## -ilwdformat [ ascii | binary ]
;## -database <dsname> 
;## 

    set args $argv
    set opts {
            -times  1
            -data   ""
			-type   ""
			-cmd    ""
            -ext    ""
            -sql    ""
            -outputformat {ilwd ascii}
			-returnformat {ilwd ascii}
            -database   ""
			-email "\{\}"
    }
    set tmp [ expandOpts ]
	array set ::options $tmp
  
setLdasSystemName
unset opts
puts "dest $dest"
# metadata (standalone) or manager host 
    set ::host [ set [ string toupper $dest]_API_HOST ]
    set ::port [ set ::${::dest}(operator) ]
puts "cmds are send to $dest at $host:$port"

# nfs problem may require the full pathname of file  
puts "-ext $::options(-ext)"
source $::options(-cmd).tcl
set ::DONE ""
puts "entering vwait"
vwait ::DONE 
puts "exiting, DONE=$::DONE"
    
