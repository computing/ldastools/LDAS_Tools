## ******************************************************** 
##  metadata_testOper.tcl Version 1.0
##
##  this script is read in by metadata_test.tcl to
##  do user cmd testing for getMetaData 
##  test sending cmds to operator socket concurrently
## ******************************************************** 
source testlib.tcl

proc setupDMT {} {
    set fd [ open "xmlist" r ]
    set xmls [ read $fd ]
    set xmls [ split $xmls \n ]
    close $fd
    set ::dmtfiles {}
    foreach xml $xmls {
        set xml [ split $xml ]
        set fname [ lindex $xml end ]
        if  { [ string length $fname ] > 0 } {
            ;##set ::dmtfiles [ linsert $::dmtfiles 0 $fname ]
            lappend ::dmtfiles $fname
        }
    }
    ;##puts $::dmtfiles
}

;#barecode

set index 0
set jobcnt 0

;## test insertion of all table data

;## default directory for data files
if  { [ info exist env(ILWDDIR) ] } {
    set basedir $env(ILWDDIR)
} else {
    set basedir [ pwd ]
}

if  { [ string length $::options(-database) ] } {
    set dbcmd "-database $::options(-database)"
} else {
    set dbcmd ""
}     
set key foo

;## use only to simulate John's dmt 
setupDMT
#set email "\{\}"
set email $::options(-email)
puts "email $email"

;;##one file test
;##set ::dmtfiles {TrigMgr20001017042257000020010000.xml}


    foreach fname $::dmtfiles {
		;##set ingestdata "ftp://ldas.ligo-wa.caltech.edu/pub/incoming/try1.xml"
		;##set ingestdata "file:/ldas_outgoing/data/mlei.xml"
		;##set ingestdata "http://www.ldas.ligo-wa.caltech.edu/ldas_outgoing/jobs/dmt/$fname"
	
		;##set ingestdata "http://www.ligo-wa.caltech.edu/~jzweizig/toldas/$fname"
		set ingestdata "http://www.ligo.caltech.edu/~mlei/triggers"
   		;## user cmd or standalone cmd 
    	set cmd "ldasJob \{-name $user -password $password -email $email \}\
		\{putMetaData -ingestdata $ingestdata $dbcmd\}"                    
    	puts "file submitting job $index, cmd=$cmd"
   		sendCmdX $cmd 1
		;## make it sequential
		vwait ::done
    }

;## may need to extend ::HTTP_TIMEOUT in ligolwAPI from 30 to 60 seconds during busy times
