#! /ldcg/bin/tclsh
## ********************************************************
##
## Name: spectrum.tcl
##
## Comments:
## Functions required by spectrumQuery.metadata macro to
## provide spectrum metadata to the data cnditioning API
## from the database tables.
##
## There are two forms of option which can be used to
## retrieve metadata; -dbspectrum and -dbquery.
##
## The -dbspectrum option requires a list of assignments
## to variable names of the form x=why, and all values
## required for a unique database query are required to
## be provided, including:
##
## 1. channel - 
## 2. spectrum_type - welch or ?
## 3. spectrum_length - number of data points (?)
## 4. start_time - data start time integer seconds 
## 5. start_time_ns - data start time integer nanoseconds
## 6. start_frequency - 
## 7. delta_frequency - 
## 8. pushpass - push or pass
## 9. alias  - an alias for the data in the dc API
## ;#ol
##
## The -dbquery option has three parts:
##
## 1. a freeform sql
## 2. pushpass
## 3. alias
## ;#ol
##
## Either form can be compounded to produce multiple
## return objects from a single call.
##
## This file is part of the metadata API.
##
## ******************************************************** 
;#barecode
package provide metadataSpectrum 1.0
;#end

## ******************************************************** 
##
## Name: createSqlFromArgs
##
## Description:
## Parses the -dbspectrum option for users 
## who do not create
## their own sql, and returns an sql query.
##
## Parameters:
##
## Usage:
## createSqlFromArgs { channel=foo start_time=123 ... }
## opts would look like
## 
##       set opts {
##           channel {}
##           spectrum_type {}
##           spectrum_length {}
##           start_time {}
##           start_time_ns {}
##           start_frequency {}
##           delta_frequency {}
##           pushpass {} 
##           alias {}
##        }
##
## Comments:
##

proc createSqlFromArgs { queries opts } {
     if { [ catch {                
        set n 0
        ;## multiple queries are possible
		;## the first one in set has its val in
		;## the next item due to the split
		set flag 0
        foreach query $queries {
			;## just one query
			if	{ [ llength $query ] == 1 } {
				set query $queries
				set flag 1
			}
            set items [ split $query = ]
            set name ""
            set val ""
            set prevname ""
            foreach item $items {
	            foreach { name val } $item { break }
	            if	{ [ string length $val ] } {
		            array set $n [ list $prevname $name ]
		            set prevname $val
                } else {
                    array set $n [ list $prevname $name ]
                    set prevname $name             
	            }
            }
			#addLogEntry "n=$n,[array get $n]"
            incr n
			if	{ $flag } {
				break
			}
        }
        for { set i 0 } { $i < $n } { incr i } {
           ;## verify that all required options were set
           foreach { option default } $opts {
              if { ! [ info exists ${i}($option) ] } { 
                 set msg "query set $i has no value provided for option: $option"
                 error $msg
              } elseif { ! [ string length [ set ${i}($option) ] ] } {
                 set msg "query set $i has null value provided for option: $option"
                 error $msg
              }
           }
           foreach { sql pushpass alias } \
                [ createSpectrumSql [ array get $i ] ] {
                if  { ! [ regexp -nocase {^push|pass$} $pushpass ] } {
                    error "Invalid push/pass option: $pushpass"
                }
                lappend sqls [ list $sql ] $pushpass $alias 
           }  
        }        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $sqls
}
## ******************************************************** 

## ******************************************************** 
##
## Name: parseDbSpectrum
##
## Description:
## Parses the -dbspectrum option for users who do not create
## their own sql, and returns an sql query.
##
## Parameters:
##
## Usage:
## parseDbSpectrum { channel=foo start_time=123 ... }
##
## Comments:
##

proc parseDbSpectrum { query } {
     if { [ catch {  	    
           set opts {
           channel {}
           spectrum_type {}
           spectrum_length {}
           start_time {}
           start_time_ns {}
           start_frequency {}
           delta_frequency {}
           pushpass {} 
           alias {}
          } 
		  set sqls [ createSqlFromArgs $query $opts ]
		  ;## addLogEntry "spectrum queries: $sqls"
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $sqls
}
## ******************************************************** 

## ******************************************************** 
##
## Name: parseQualityChannel
##
## Description:
## Parses the -qualitychannel option for users who do not create
## their own sql, and returns an sql query.
##
## quality channel args:
##
##	1.sample_rate=<integer> 
##  2.start_time=<integer> 
##  3.start_time_ns=<integer> 
##  4.end_time=<integer> 
##  5.end_time_ns=<integer> 
##  6.sql=<valid sql string> 
##  7.name=<string> 
##  8.pushpass=<push or pass> 
##
## Parameters:
##
## Usage:
## parseQualityChannel { channel=foo start_time=123 ... }
##
## Comments:
##

proc parseQualityChannel { qualchans } {
     if { [ catch { 
	    set flag 0
	 	foreach qualchan $qualchans {
			if	{ [ llength $qualchan ] == 1 } {
				set qualchan $qualchans
				set flag 1
			}
        	if  { [ llength $qualchan ] > 1 } {
            	set sql [ lindex $qualchan 5 ]
				set pushpass [ lindex $qualchan 6 ]
                if   { ! [ regexp -nocase {^push|pass$} $pushpass ] } {
                    error "Invalid push/pass option: $pushpass"
                }
            	set name [ lindex $qualchan 7 ]
				lappend sqls [ list $sql ] $pushpass $name 
        	}
			if	{ $flag } {
				break
			}
		}
		addLogEntry "quality channel queries: $sqls" 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $sqls
}
## ******************************************************** 

## ******************************************************** 
##
## Name: parseDbQuery
##
## Description:
## Parses the option -dbquery for users who can build their
## own SQL.
##
## Parameters:
##
## Usage:
## parseDbQuery {{SELECT * FROM FOO WHERE (n=1)},push,foo_n1}
##
## Comments:
## If calling this generates an "extra chars after close
## brace" error, the input needed to be wrapped in an extra
## set of braces.
## Multiple requests are possible.
##

proc parseDbQuery { args } {
     
     if { [ catch {
	 	set flag 0
        if { [ llength $args ] == 1 } { 
           set args [ lindex $args 0 ] 
        }
        foreach sql $args {
		   if { [ llength $sql ] != 3 } { 
           	   set sql $args
			   set flag 1
           }
		   foreach { sql pushpass alias } $sql { break }
           set sql [ string trim $sql ]
           set pushpass [ string trim $pushpass ]
           if   { ! [ regexp -nocase {^push|pass$} $pushpass ] } {
                error "Invalid push/pass option: $pushpass"
           }
           set alias [ string trim $alias ]
		   set sql [ subst -nocommands -nobackslashes $sql ]
           lappend sqls [ list $sql ] $pushpass $alias 
		   if	{ $flag } {
		   		break
		   }
        }		
		addLogEntry "dbQuery sqls $sqls"
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $sqls
}
## ******************************************************** 

## ******************************************************** 
##
## Name: createSpectrumSql
##
## Description:
## Passed a list of key/value pairs of of SUMM_SPECTRUM
## table keys, returns an SQL for retrieving the spectrum
## data.
##
## Parameters:
##
## Usage:
##
## Comments:
## Called by parseDbSpectrum
##

proc createSpectrumSql { args } {
     
     if { [ llength $args ] == 1 } { 
        set args [ lindex $args 0 ] 
     }
    
     set sql {
             SELECT *
             FROM SUMM_SPECTRUM
             WHERE
                channel='$channel'
                AND spectrum_type='$spectrum_type'
                AND start_time=$start_time
                AND start_time_ns=$start_time_ns
                AND start_frequency=$start_frequency
                AND delta_frequency=$delta_frequency
                AND spectrum_length=$spectrum_length
             ORDER BY start_time, spectrum_type
             FETCH FIRST 1 ROWS ONLY
             }
     
     if { [ catch {
         
        foreach { name value } $args {
           set $name $value
        }
         
        set sql [ subst -nocommands -nobackslashes $sql ]
     
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $sql $pushpass $alias ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: processQuerySet
##
## Description:
## process a set of dbqueries or a set of spectrum queries
## foreach query in the type of query set
##  issue a getMetaData cmd to retrieve rows and send off to target
##
## Parameters:
##
## Usage:
##
## Comments:
## emptyDataBucket is done in slave
## do not move this code for documentation

proc metadata::processQuerySet {} {
 uplevel {     
        if { [ llength $query ] > 0 } {
           foreach { sqlquery pushpass alias } $query {
		   	  set sqlquery [ lindex $sqlquery 0 ]
		   	  if 	{ ! [ regexp -nocase {for (read|fetch) only} $sqlquery ] } {
       				append sqlquery " for read only"
    		  }
              set ::${jobid}(-sqlquery) $sqlquery
              ;## these two will be used by the data ouput
              ;## routine to call setPushPassAndAlias to
              ;## modify the comment attirbute of the ilwd.
              set ::${jobid}(pushpass) $pushpass
              set ::${jobid}(alias) $alias
			  set ::${jobid}(dboperation) $operation
    	      set cmd "getMetaData [ array get ::${jobid} ]"
    	      after 0 [ list metadata::evalCmd $jobid $cmd ]
           }
        }
    } 
}

## ******************************************************** 
##
## Name: metadata::databaseQueryMacroProcess
##
## Description:
## Just a quick built-in test to make sure that there are
## no really glaring bugs in the parsers.
##
## Parameters:
##
## Usage:
##
## Comments:
## Requires visual inspection of the results, for now.

proc metadata::databaseQueryMacroProcess { jobid } {
     set cid [ set ::${jobid}(cid) ]
     if { [ catch {
		set ::${jobid}(format) ilwd
		if	{ [ info exists ::${jobid}(-returnformat) ] } {
			set rf [ set ::${jobid}(-returnformat) ]
			set ::${jobid}(format) $rf
		} 
    	if { [ info exists ::${jobid}(-outputformat) ] } {
        	set of [ set ::${jobid}(-outputformat) ]
			set ::${jobid}(format) $of
    	} 	
		set queryset [ list ]
		set querycnt 0
        set dbquery [ set ::${jobid}(-dbquery) ]
        if  { [ llength $dbquery ] } {
            set ::${jobid}(spectrumflag) 0
            set query [ parseDbQuery $dbquery ]
			lappend queryset [ list dbquery $query ]
			incr querycnt [ expr [ llength $query ]/ 3 ]
		}
		set dbntuple [ set ::${jobid}(-dbntuple) ]
        if  { [ llength $dbntuple ] } {
            set ::${jobid}(spectrumflag) 0
            set query [ parseDbQuery $dbntuple ]
			lappend queryset [ list dbntuple $query ]
			incr querycnt [ expr [ llength $query ]/ 3 ]
		}
		set dbspect [ set ::${jobid}(-dbspectrum) ]
        if  { [ llength $dbspect ] } {
            set ::${jobid}(spectrumflag) 1
            set query [ parseDbSpectrum $dbspect ]
			lappend queryset [ list dbspectrum $query ]
			incr querycnt [ expr [ llength $query ]/ 3 ]
		}
		set qualchans [ set ::${jobid}(-dbqualitychannel) ]
        if  { [ llength $qualchans ] } {
			set ::${jobid}(spectrumflag) 0
            set query [ parseQualityChannel $qualchans ]
			lappend queryset [ list qualitychannel $query ]
			incr querycnt [ expr [ llength $query ]/ 3 ]
		}
		;## debugPuts "$querycnt queries, queryset $queryset."
		set ::${jobid}(numQueries) $querycnt
		if	{ $querycnt } {
			# trace vdelete ::$cid w "reattach $jobid $cid"
			set ::${jobid}(cmd) ckdatabaseQueryCompletion
			foreach queryitem $queryset {
				foreach { operation query } $queryitem { break }
				;## debugPuts "operation $operation, query=$query."
				processQuerySet 
			}
		} else {
            unset ::$jobid
            set ::jobid {}  
            set ::$cid [ list 0 0 0 ]
            ;## debugPuts "no errors,$querycnt queries."
            reattach $jobid $cid
        }
     } err ] } {
        addLogEntry $err 2	
	 	unset ::$jobid
		set ::jobid {}	
		set msg [ list 3 "metadataAPI:$jobid: $err" error! ]
		set ::$cid $msg
		reattach $jobid $cid
    }
}


## ******************************************************** 
##
## Name: spectrumTestHarness
##
## Description:
## Just a quick built-in test to make sure that there are
## no really glaring bugs in the parsers.
##
## Parameters:
##
## Usage:
##
## Comments:
## Requires visual inspection of the results, for now.

proc spectrumTestHarness { } {
     
     if { [ catch {
         
        if { ! [ llength [ info commands ::myName ] ] } {
           proc myName {} { return spectrumTestHarness }
        }
        
        if { ! [ llength [ info commands ::addLogEntry ] ] } {
           proc addLogEntry { args } { puts stderr $args }
        }
        
        set dbquery {{{select * from sngl_inspiral where 
			search='stranger' fetch first 1 rows only} pass dbquery0}
			{{select * from sngl_inspiral fetch first 5 rows only} pass dbquery5}}

		set dbntuple {{{select impulse_time, mchirp, sigmasq, search, ifo from sngl_inspiral 
	   			where search='stranger' fetch first 1 rows only} pass dbquery}
				{{select impulse_time, mchirp, sigmasq, search, ifo from sngl_inspiral 
	   			fetch first 1 rows only} pass dbquery}}
				            
        set dbspectrum { {channel=foo
                         spectrum_type=welch
                         spectrum_length=10
                         start_time=600000000
                         start_time_ns=500000000
                         start_frequency=10
                         delta_frequency=10
                         pushpass=pass 
                         alias=foo}
                         {channel=bar
                         spectrum_type=power
                         spectrum_length=5
                         start_time=666666666
                         start_time_ns=600000000
                         start_frequency=5
                         delta_frequency=5
                         pushpass=push 
                         alias=bar}
                       }
		set dbqualchan 	{{ 1024 680896086 0 680896086 0 
          	      {SELECT start_time as StartSec, start_time_ns as StartNanoSec, end_time as StopSec, end_time_ns as StopNanoSec
          		  FROM SEGMENT
                  WHERE ( ( ( Start_Time >= 680895416 and Start_Time <= 680896086 ) OR ( end_time <= 680896086 AND end_time >= 680895416 ) ) AND
                  ( segment_group LIKE 'H2:%' ) )}
                  pass qualchan1}
                  { 1024 680896086 0 680896086 0 
          	      {SELECT start_time as StartSec, start_time_ns as StartNanoSec, end_time as StopSec, end_time_ns as StopNanoSec
          		  FROM SEGMENT
                  WHERE ( ( Start_Time >= 999999990 and Start_Time <= 999999999 ) AND
                  ( segment_group LIKE 'H2:%' ) )}
                  pass qualchan0}
                  }	
		#set dbqualchan [ list $::qualchan1 $::qualchan0 ]
		set dbqualchan { 1024 680896086 0 680896086 0 
          	      {SELECT start_time as StartSec, start_time_ns as StartNanoSec, end_time as StopSec, end_time_ns as StopNanoSec
          		  FROM SEGMENT
                  WHERE ( ( ( Start_Time >= 680895416 and Start_Time <= 680896086 ) OR ( end_time <= 680896086 AND end_time >= 680895416 ) ) AND
                  ( segment_group LIKE 'H2:%' ) )}
                  pass qualchan1}
		set dbspectrum {channel=bar
                         spectrum_type=power
                         spectrum_length=5
                         start_time=666666666
                         start_time_ns=600000000
                         start_frequency=5
                         delta_frequency=5
                         pushpass=push 
                         alias=bar}
		set dbquery {{select * from sngl_inspiral fetch first 5 rows only} pass dbquery5}	
		
		   foreach { sql pp alias } [ parseDbQuery $dbquery ] {
              puts "dbquery,sql=$sql\npp=$pp\nalias=$alias\n"
           }
		   foreach { sql pp alias } [ parseDbQuery $dbntuple ] {
              puts "dbntuple,sql=$sql\npp=$pp\nalias=$alias\n"
           }
		   foreach { sql pp alias } [ parseDbSpectrum $dbspectrum ] { 
              puts "dbspectrum,sql=$sql\npp=$pp\nalias=$alias\n"
           }
		   foreach { sql pp alias } [ parseQualityChannel $dbqualchan ] {
              puts "dbqualchan,sql=$sql\npp=$pp\nalias=$alias\n"
           }  
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 
