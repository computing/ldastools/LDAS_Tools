## ******************************************************** 
##
##  Name: mpi.tcl Version 1.0
##
## Description:
## Provides Tcl procedures specific to the mpi API.
##
## Comments:
## The mpi API provides an interface for user commands to
## start and manage MPI jobs using calls to mpirun and the
## wrapperAPI.
##
## This module sources the following sub-modules:
##   1. lam.tcl (LAM specific code, in it's own namespace)
## ;#ol
##
## All user commands enter the mpi API through a call to
## mpi::newJob, passing in the jobid, the priority, and
## the mpiargs option list.
##
## mpi::newJob validates and parses the option list, and
## passes control to mpi::startJob, which build the command
## for the wrapper API, opens a dedicated server channel
## for use by that wrapper, and loads the mpi API queue entry
## for the job.  mpi::startJob then passes control to
## mpi::run, which instantiates a new wrapper API by
## calling exec or by an ssh call.
##
## From that point on the wrapper controls the job. 
##
## The mpi queues are structured in this way:
##
## naming convention: ::mpi::queue($jobid,$item)
##
## Where $jobid is the integer part of the job i.d. string
## (i.e. NORMAL12345) and item is one of the following:
##
## 1.  job - integer part of job i.d. useful for lookups
## 2.  pri - the priority as integer number of nodes
## 3.  nod - list of nodes assigned to the job
## 4.  stt - start time of job
## 5.  cmd - the user command in parsed form
## 6.  cid - socket i.d. of the jobstate server for the job
## 7.  oid - operator socket i.d. for the job
## 7.  usr - job runs as this user
## 8.  unm - the real username that submitted the job
## 9.  run - communication status of wrapper API
## 10. bal - boolean state of active load balancing
## 11. end - error message
## 12. fid - unused (!)
## 13. aft - after i.d. of the abortIfHung loop
## 14. afq - after i.d. of the mpi::queueJob loop
## 15. ero - error string, and trigger for end of job
## 16. mas - wrapper master node number (i.e. n0)
## ;#ol
##
## See the documentation for the mpi user commands for
## usage information.
##
## ******************************************************** 
;#barecode

package provide mpi 1.0
;## if some c++ code becomes necessary, it will exist
;## as the Tcl extension package named mpiAPI.
;##package require mpiAPI

package require lam

namespace eval mpi {
   set errlvl 1
   set jobs {}
   set trigger {}
   set users $::MPIUSERS
   set queuenames [ list job pri nod stt cmd oid usr unm dso run bal end aft afq qus ero mas ]
   
   ;## options are defined with a name, a regex pattern
   ;## which matches them, and a default value if applicable.
   ;## the variables will not get substituted at this point.
   ;## note that the REQUIRED options have no default and
   ;## will raise exceptions if not provided.
   ;## The regexes are evaluated in a case-insensitive context.
   
   set options {
       -inputformat        {^ilwd$} ilwd
       -inputprotocol      {^\S+$}  file:/sample_data.ilwd
       -returnprotocol     {^\S+$}  http://mpiResult
       -returnformat       {^(ilwd(\s+(ascii|binary))?|LIGO_LW)$} {ilwd ascii}
       -priority           {^([lL]ow|[nN]ormal|[hH]igh)$} low
       -np                 {^\d+$} 3
       -nolocal            {^[01]$} 1
       -machinefile        {^\S+$} $::MACHINEFILE
       -inputFile=         {.*} _null
       -mpiAPI=            {.*} mpi
       -jobID=             {^\d+$} $jobid
       -nodelist=          {^\([0-9\,]*\)$} ($nodes)
       -dynlib=            {^\S+$} {}
       -dataAPI=           {^\S+$} datacond
       -resultAPI=         {^\S+$} eventmon
       -userTag=           {.*} _null
       -filterparams=      {.+} {}
       -memoryUsageLimit=  {^\d{1}\.\d{1,5}$} 1.0
       -realTimeRatio=     {^\d{1}\.\d{1,5}$} 1.0
       -doLoadBalance=     {^(0|1|T|TRUE|F|FALSE)$} $bal
       -dataDistributor=   {^(W|WRAPPER|S|SEARCHMASTER)$} WRAPPER
       -communicateOutput= {^(A|ALWAYS|O|ONCE)$} ALWAYS
   }
 }
;#end


## ******************************************************** 
##
## Name: mpi::init
##
## Description:
## Entry point for the LDASgwrap script to initialise
## the mpi API. Initialises the mpi API queue with the
## "NULL" job, which is the donor and receptor of all
## unallocated cluster nodes, and which is always set
## to the doLoadBalancing state.  The priority of the
## NULL job is always the total number of nodes available
## to the system.
##
## Parameters:
##
## Usage:
##
## Comments:
## NUMNODES is a global read-only variable storing the
## total number of nodes available for the system to work
## with.
##
## See "roVar" in genericAPI.tcl.

proc mpi::init { } {
     
     ;## the mpiAPI does not use a data socket.
     closeListenSock data
     addLogEntry "unused data port $::mpi(data) closed" blue
     set ::mpi(jobstate) [ lindex $::mpi(data) 0 ]
     set ::mpi(data) [ list ]
     
     if { [ string equal -nocase lam $::MPI ] } {
        if { ! [ info exists ::LAMHOME ] || \
             ! [ file executable $::LAMHOME/bin/lamboot ] } {
           if { [ info exists ::LAMHOME ] } {
              set lh $::LAMHOME
           } else {
              set lh UNDEFINED
           }
           set msg    "::LAMHOME not defined or ::LAMHOME/bin/lamboot\n"
           append msg "not executable. Make sure the LAMHOME variable\n"
           append msg "is set to the root directory of the LAM\n"
           append msg "installation in the file LDASmpi.rsc\n"
           append msg "::LAMHOME in LDASmpi.rsc is: $lh"
           return -code error $msg
        }
     }
    
     if { ! [ info exists ::DC_WRAPPER_COMM_PORT_OFFSET ] } {
        set msg    "::DC_WRAPPER_COMM_PORT_OFFSET not defined\n"
        append msg "please set ::DC_WRAPPER_COMM_PORT_OFFSET in\n"
        append msg "in the LDASapi.rsc file"
        return -code error $msg
     }
    
     ;## extra flags, like -v, that can be passed to mpirun
     if { ! [ info exists ::MPIRUN_EXTRA_FLAGS ] } {
        set ::MPIRUN_EXTRA_FLAGS {}
     }
     
     ;## the rsh command used by the mpi API.
     ;## sometimes this will need to be set to ssh.
     if { ! [ info exists ::MPI_RSH_COMMAND ] || \
          ! [ string length $::MPI_RSH_COMMAND ] } {
        set ::MPI_RSH_COMMAND /usr/bin/rsh
     }
     set ::LAMRSH $::MPI_RSH_COMMAND

     if { ! [ info exists ::MPI_KILL_COMMAND ] || \
          ! [ string length $::MPI_KILL_COMMAND ] } {
        set ::MPI_KILL_COMMAND "/usr/bin/killall -KILL -v"
     }
     
     ;## if a single communicator does not reply within
     ;## a time defaulting to 10 minutes, the job is
     ;## terminated.  Value is in seconds.
     if { ! [ info exists ::NODE_COMM_TIMEOUT ] || \
          ! [ regexp {^\d+$} $::NODE_COMM_TIMEOUT ] } {
        set ::NODE_COMM_TIMEOUT 600
     }

     ;## This value is used to modify the default value
     ;## of retries for cmd::result.
     ;## a value of 100 is normal, but a very busy mpi
     ;## wrapper combo may need to be set to 200... so
     ;## THAT is the default here.
     if { ! [ info exists ::MPI_WRAPPER_COMMAND_TIMEOUT ] || \
          ! [ regexp {^\d+$} $::MPI_WRAPPER_COMMAND_TIMEOUT ] } {
        set ::MPI_WRAPPER_COMMAND_TIMEOUT 200
     }
     
     ;## how long to wait for a wrapper to complete it's init
     if { ! [ info exists ::MPI_WRAPPER_INIT_TIMEOUT ] || \
          ! [ regexp {^\d+$} $::MPI_WRAPPER_INIT_TIMEOUT ] } {
        set ::MPI_WRAPPER_INIT_TIMEOUT 10
     }
     
     ;## how many seconds to keep a job waiting for nodes when
     ;## none are immediately available.
     if { ! [ info exists ::MPI_QUEUE_TIMEOUT ] || \
          ! [ regexp {^\d+$} $::MPI_QUEUE_TIMEOUT ] } {
        set ::MPI_QUEUE_TIMEOUT 60
     }
     
     ;## how many seconds to wait before declaring lamboot a
     ;## failure
     if { ! [ info exists ::LAMBOOT_TIMEOUT ] || \
          ! [ regexp {^\d+$} $::LAMBOOT_TIMEOUT ] } {
        set ::LAMBOOT_TIMEOUT 60
     }
     
     ;## if ::MPI_MULTIPLE_NODES is set to 1, then
     ;## nodes will get used by more than one job.
     if { ! [ info exists ::MPI_MULTIPLE_NODES ] || \
          ! [ string equal 1 $::MPI_MULTIPLE_NODES ] } {
        set ::MPI_MULTIPLE_NODES 0
     }
     
     ;## if nodes can be used by more than one job, how
     ;## many jobs can run on one node?
     if { ! [ info exists ::MPI_NODE_SHARE_LIMIT ] || \
          $::MPI_NODE_SHARE_LIMIT < 1 } {
        set ::MPI_NODE_SHARE_LIMIT 2
     }     
     
     ;## run recon after all lamboots... or not. defaultis to
     ;## run.
     if { ! [ info exists ::RUN_RECON_AFTER_LAMBOOT ] || \
          ! [ string equal 0 $::RUN_RECON_AFTER_LAMBOOT ] } {
        set ::RUN_RECON_AFTER_LAMBOOT 1
     }
     
     ;## how many times will we try to read data off of a
     ;## single connection from the wrapper/mpirun processes
     if { ! [ info exists ::LAM_SHELL_LOG_INFINITE_LOOP ] || \
          ! [ regexp {^\d+$} $::LAM_SHELL_LOG_INFINITE_LOOP ] } {
        set ::LAM_SHELL_LOG_INFINITE_LOOP 1000
     }
     
     ;## if ::KILL_ALL_LAMD_AT_RESTART is set to 1, then
     ;## all preexisting lamd processes will be killed.
     if { ! [ info exists ::KILL_ALL_LAMD_AT_RESTART ] } {
        set ::KILL_ALL_LAMD_AT_RESTART 0
     }
     
     ;## limit the amount of memory used on any single
     ;## node by this job.
     if { ! [ info exists ::MEMORY_USAGE_LIMIT ] } {
        set ::MEMORY_USAGE_LIMIT 1.0
     }
     
     ;## delay factor for lamboot timing based delay
     ;## used by 'dumb' lam::boot
     if { ! [ info exists ::LAMBOOT_RATE_NODES_PER_SECOND ] } {
        set ::LAMBOOT_RATE_NODES_PER_SECOND 4
     }
     
     set ::MPIHOST $::MPI_API_HOST
     
     ;## define ::WRAPPERHOST in the LDASmpi.rsc if
     ;## the mpi API does not run on the beowulf gateway.
     if { ! [ info exists ::WRAPPERHOST ] || \
          ! [ string length $::WRAPPERHOST ] } { 
        set ::WRAPPERHOST $::MPIHOST
     }   
     
     ;## if data is not coming from the datacond API define
     ;## ::DATAHOST in the LDASmpi.rsc.
     if { ! [ info exists ::DATAHOST ] || \
          ! [ string length $::DATAHOST ] } { 
        if	{ [ array exist ::datacond ] } {
        	set ::DATAHOST $::datacond(host)
        	set ::DATAPORT $::datacond(data)
        }
     }
     
     ;## if results do NOT go to the eventmon API data
     ;## socket, define ::RESULTHOST and ::RESULTPORT
     ;## to point to a client capable of recieving data
     ;## objects.
     if { ! [ info exists ::RESULTHOST ] || \
          ! [ string length $::RESULTHOST ] } {
        if	{ [ array exist ::eventmon ] } {
        	set ::RESULTHOST $::eventmon(host)
        	set ::RESULTPORT $::eventmon(data)
        }
     }

     if { ! [ info exists ::MACHINEFILE ] } {
        set ::MACHINEFILE [ list ]
     }
     
     mpi::initNodes
     
     set port [ lindex $::mpi(jobstate) 0 ]
     set cid \
        [ socket -server "mpi::cfgJobstate $port" $port ]
    
     set ::mpi(jobstate) [ list $port $cid ]
     
     addLogEntry \
        "port $port (jobstate) opened on $::mpi(host) as $cid" blue
     
     set ::mpi::queue(NULL,cid) $::mpi(jobstate)

     mpi::watchWrapperLog
     
     set ::mpi::waitforlamboot 1
     after 5000 mpi::killAllMpirun $::MGRKEY STARTUP
     after 20000 mpi::updateCmonNodelist
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::initNodes
##
## Description:
## Break out from mpi::init so that node names can be
## dynamically adjusted.
##
## Before calling, set ::NODENAMES to the new node list.
##
## Parameters:
##
## Usage:
##  
## Comments:
##

proc mpi::initNodes { } {
     
     if { [ catch {
        
        set ::NUMNODES [ llength $::NODENAMES ]
        
        set nulllist [ list ]
        
        ;## initialise the list of all nodes as job "NULL".
        ;## excluding node 0, which is the master, so we
        ;## don't allocate it!
        ;## BUT, what if the first node in the list is NOT
        ;## the mpi host?  And what if we want to distribute
        ;## the masters across other nodes?
        ;## PR #2354 fix:
        set nodenames [ lsort -unique [ lrange $::NODENAMES 1 end ] ]
        
        foreach name [ lrange $::NODENAMES 1 end ] {
           set i [ lsearch $nodenames $name ]
           ;## DAMN OFF BY ONE!! PR #2443
           if { [ llength $nodenames ] > 1 } {
              set i [ expr $i + 1 ]
           }
           lappend nulllist [ list $name n$i 0 ]
        }
        
        if { ! [ llength $nulllist ] } {
           set msg "*** no slave nodes configured on this system ***"
           addLogEntry $msg blue
        }
        
        ;## synchronize with running system
        if { [ info exists ::mpi::queue(NULL,nod) ] } {
           set oldnulllist $::mpi::queue(NULL,nod)
           foreach item $oldnulllist {
              foreach { name node score } $item {
                 if { $score } {
                    regsub "$name\\s+(n\\d+)\\s+0" $nulllist \
                           "$name \\1 $score" nulllist
                 }
              }
           }
        }
        
        foreach name $::mpi::queuenames {
           set ::mpi::queue(NULL,$name) [ list ]
        }
     
        set ::mpi::queue(NULL,bal) 1
        set ::mpi::queue(NULL,nod) $nulllist
        set ::mpi::queue(NULL,pri) $::NUMNODES

     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::diagnostic
##
## Description:
## Indicate idle nodes, average active # of nodes, what
## else? Should return a web page with links to other
## web pages with specific diagnostic info... :TODO:
##
## Parameters:
##
## Usage:
##
## Comments:
## The wrapper API can only be RESPONDED to, the initiation
## of communication by the mpi API is forbidden.  This
## condition precludes the scheduling of diagnostics by the
## mpi API...

proc mpi::diagnostic { } {
     set qtext [ mpi::dumpFormattedQueue ]
     return $qtext
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::priority
##
## Description:
## Registers the priority levels as the number of nodes
## which can be assigned.
##
## Job "NULL" is the reference job which "owns" the system,
## and it has priority ::NUMNODES
##
## Parameters:
##
## Usage:
##
## Comments:
## Jobs are allowed a fractional part of ::NUMNODES based
## on priority assigned by the manager.

proc mpi::priority { { jobid "" } } {
     
     if { [ catch {
        ;## initialise priority level info
        if { ! [ info exists ::priority_high ] } {
           ;## associate priority names with divisors
           set levels {
                      low    4
                      normal 2
                      high   1
                      }
     
           foreach { name div } $levels {
              set ::priority_$name [ expr { $::NUMNODES / $div } ]
           }
        }
     
        if { [ regexp {\d+} $jobid job ] } {
           set bal [ set ::mpi::queue($job,bal) ]
           set pri [ set ::mpi::queue($job,pri) ]
           if { $bal } {
              if { $pri < $::priority_low } {
                 set ::mpi::queue($job,pri) $::priority_low
              }
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::newJob
##
## Description:
## Entry point to the mpi API for user commands.
##
## Parameters:
## jobid - the LDAS job i.d.
## priority - probably not an arg at all...
## mpiargs - the important stuff... see parseOpts
## Usage:
##
## Comments:
## The users queue contains a priority for each user.  The
## user MAY specify a lower prority as an option.

proc mpi::newJob { jobid pri } {
     
     if { [ catch {
        set abort 0
        regexp {\d+} $jobid job
        
        set cid [ uplevel #1 set cid ]
        trace vdelete ::$cid w "reattach $jobid $cid"
        
        ;## set the -memoryUsagelimit= option for the wrapper
        set ::${jobid}(-memoryusagelimit) $::MEMORY_USAGE_LIMIT
        
        set unm [ set ::${jobid}(-userid) ]
        
        ;## automagically get all arguments into args
        foreach { name rx default } $::mpi::options {
           set name [ string trim $name "=" ]
           set name [ string tolower $name ]
           if { [ info exists ::${jobid}($name) ] } {
              lappend args $name [ set ::${jobid}($name) ]
           } else {
              lappend args $name $default
           }
        }
        
        mpi::talkToEventmon $jobid
        
        foreach { bal pri ifile tag dht dpt rht rpt mht mpt } \
           [ mpi::preProcessArgs $jobid $args ] { break }
        
        mpi::unreasonableRequest $pri
        
        ;## get the calculated host and port info
        regsub {data[aA][pP][iI]\s+\S+}   $args \
           "dataapi ($dht,$dpt)"   args
        regsub {result[aA][pP][iI]\s+\S+} $args \
           "resultapi ($rht,$rpt)" args
        regsub {mpi[aA][pP][iI]\s+\S+}    $args \
           "mpiapi ($mht,$mpt)"    args
        ;## and the input file as appropriate
        regsub {input[fF]ile\s+\S+}       $args \
           "inputfile $ifile"      args
        ;## and the userTag 02/04/02
        regsub -all {\s+} $tag {__sPaCe__} tag
        
        ;## userTag option is a freeform string
        set uti [ lsearch $args *user*ag ]
        if { $uti > -1 } {
           set utj [ expr { $uti + 1 } ]
           set args [ lreplace $args $uti $utj -usertag '$tag' ]
        }
       
        ;## initialise the state queue entries
        foreach name $::mpi::queuenames {
           if { [ info exists $name ] } {
              set ::mpi::queue($job,$name) [ set $name ]
           } else {   
              set ::mpi::queue($job,$name) [ list ]
           }   
        } 
        
        ;## keep the operator socket i.d. for use
        ;## by the callback
        set ::mpi::queue($job,oid) $cid
       
        ;## this element is the flag on successful wrapper
        ;## communication.
        set ::mpi::queue($job,run) stalled
        
        mpi::queueJob $jobid 0 $args
        
        ;## from here on we're in the event loop!
        trace variable ::mpi::queue($job,end) w \
           [ list mpi::newJobCallback $jobid ]
       
     } err ] } {
        mpi::abortJobInDcApi $jobid
        set dt [ mpi::stopJob $jobid ]
        if { [ regexp {MsgSocket\s+error:} $err ] } {
           mpi::wrapperLog $err red
        }
        addLogEntry $err red
        addLogEntry "Aborted job $jobid after $dt seconds wall time" red
        set ::$cid [ list 3 "[ myName ]: $err" error! ]
        reattach $jobid $cid
     }     
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::unreasonableRequest
##
## Description:
## Correct calculation for detecting requests for more
## nodes than are available on the system.
##
## Parameters:
##
## Usage:
##
## Comments:
## Fix for PR #2315

proc mpi::unreasonableRequest { requested } {
     
     if { [ catch {
        
        if { $::MPI_MULTIPLE_NODES == 0 } {
           set factor 1
        } else {
           set factor $::MPI_MULTIPLE_NODES
        }

        set N $::NUMNODES

        set available [ expr { $factor * $N } ]
        
        ;## reject requests for more nodes than we have!
        if { $requested > $available } {
           set msg "number of nodes requested: '${requested}' exceeds "
           append msg "number of nodes available on this system: "
           append msg "${available}."
           return -code error $msg
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::newJobCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::newJobCallback { jobid args } {
     
     if { [ catch {
        set stopjoberr 0
        regexp {\d+} $jobid job
        
        ;## unregister this callback so it doesn't
        ;## fire multiple times.
        if { [ info exists ::mpi::queue($job,end) ] } {
           unset ::mpi::queue($job,end)
        }
        set ::mpi::queue($job,end) 1
        
        mpi::wrapperMasterUnassign $jobid
        
        if { [ info exists ::mpi::queue($job,aft) ] } {
           after cancel $::mpi::queue($job,aft)
           unset ::mpi::queue($job,aft)
        }
        
        if { [ info exists ::mpi::queue($job,afq) ] } {
           after cancel $::mpi::queue($job,afq)
           unset ::mpi::queue($job,afq)
        }
        
        if { [ info exists ::mpi::queue($job,shell) ] } {
           set shell $::mpi::queue($job,shell)
           if { [ regexp {^file\d+$} $shell ] } {
              catch { ::close $shell }
           }
           unset ::mpi::queue($job,shell)
        }
        
        ;## the operator socket i.d.
        if { [ info exists ::mpi::queue($job,oid) ] } {
           set cid $::mpi::queue($job,oid)
        }
        
        ;## if there was an error
        if { [ info exists ::mpi::queue($job,ero) ] && \
             [ string length [ set ::mpi::queue($job,ero) ] ] } {
           set err [ set ::mpi::queue($job,ero) ]
           return -code error $err
        }
        ;## otherwise just get along
        if { [ info exists cid ] } {
           set ::$cid [ list 0 0 0 ]
        }
        
        if { [ catch {
           set dt [ mpi::stopJob $jobid ]
        } err ] } {
           set stopjoberr 1
           return -code error $err
        }
        
        set msg "job completed after $dt seconds wall time"
        addLogEntry $msg

        catch { file delete -force /tmp/mschema.$jobid }
     } err ] } {
        mpi::abortJobInDcApi $jobid
        set dt [ mpi::killJob $jobid $stopjoberr ]
        addLogEntry $err red
        mpi::wrapperLog $err red
        addLogEntry "Aborted job $jobid after $dt seconds wall time" red
        catch { file delete -force /tmp/mschema.$jobid }
        set err "[ myName ]: $err"
        if { [ info exists cid ] } {
           set ::$cid [ list 3 $err error! ] 
        } else {
           if { [ llength [ array names ::mpi::queue $job,* ] ] } {
              set msg "possible orphan array elements: "
              append msg [ array names ::mpi::queue $job,* ]
              addLogEntry $msg red
           }
        }
     }
     if { [ info exists cid ] } {
        reattach $jobid $cid
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::preProcessArgs
##
## Description:
## The values of some arguments need to be known early.
##
## Parameters:
##
## Usage:
##
## Comments:
## This will almost certainly go away.

proc mpi::preProcessArgs { jobid args } {
     
     if { [ llength $args ] == 1 } { 
        set args [ lindex $args 0 ] 
     }
    
     if { [ catch {
        foreach { datah datap } \
           [ mpi::getHostAndPort $jobid data   $args ] { break }
        foreach { resh  resp  } \
           [ mpi::getHostAndPort $jobid result $args ] { break }
        
        set mpih $::mpi(host)
        set mpip [ lindex $::mpi(jobstate) 0 ]
        
        ;## handle the load-balancing option
        if { [ regexp -nocase -- {doLoadBalance\s+T} $args ] } {
           set bal 1
        } else {
           set bal 0
        }
       
        ;## handle inputFile existence
        regexp -nocase -- {inputFile\s+(\S+)} $args -> ifile
        if { ! [ string length $ifile ] || \
               [ string equal _null $ifile ] || \
               [ string equal "{}" $ifile  ] } {
           set ifile \\\{\\\}
        }       
        
        ;## handle userTag, which is a, gak, free form string!
        set i [ lsearch $args -usertag ]
        incr i
        set tag [ lindex $args $i ]
        set tag [ string trim $tag ]
        if { ! [ string length $tag ] || \
               [ string equal _null $tag ] || \
               [ string equal "{}" $tag  ] } {
           set tag \\\{\\\}
        } 
       
        mpi::priority
        set pri low
        regexp -nocase -- {priority\s+(low|normal|high)} $args -> pri 
        set pri [ set ::priority_$pri ]
        if { ! $bal } {
           regexp -- {-np\s+(\d+)} $args -> pri
        } else {
           regsub -- {-np\s+\d+} $args "-np $::NUMNODES" args
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list \
        $bal $pri $ifile $tag $datah $datap $resh $resp $mpih $mpip ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::getHostAndPort
##
## Description:
## Returns the host and data port number associated with
## one of the API based services required by the wrapper
## API.
##
## Parameters:
## service - data, result, or mpi (so far!)
## Usage:
##
## Comments:
## Called by mpi::preProcessArgs

proc mpi::getHostAndPort { jobid service args } {
     
     if { [ catch {
        set api _null
        set uc [ string toupper $service ]
        set rx ${service}API\\s+(\\S+)
        regexp -nocase -- $rx $args -> api
        if { ! [ string equal _null $api ] } {
           if { [ catch {
              set ${service}host [ set ::${api}(host) ]
           } err ] } {
              return -code error "'$api' API is not registered"
           }
           if { [ string equal data $service ] } {
              set offset $::DC_WRAPPER_COMM_PORT_OFFSET
              set ${service}port [ expr { $::BASEPORT + $offset } ]
           } elseif { [ string equal result $service ] } {
              set ${service}port $::eventmon(data)
           } elseif { [ string equal mpi $service ] } {
              set ${service}port [ lindex [ set ::${api}(jobstate) ] 0  ]
           } else {
              set ${service}port [ lindex [ set ::${api}(data) ] 0 ]
           }
        } elseif { [ info exists ::${uc}HOST ] && \
                   [ info exists ::${uc}PORT ] } {
             set ${service}host [ set ::${uc}HOST ]
             set ${service}port [ set ::${uc}PORT ]
        } else {
           return -code error "'-${service}API=' option undefined"
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list [ set ${service}host ] \
                   [ set ${service}port ] ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::queueJob
##
## Description:
## Prevents new mpi jobs being started while a lamboot is
## in progress.
##
## Parameters:
##
## Usage:
##
## Comments:
## This never times out.
##

proc mpi::queueJob { jobid background args } {
     if { [ catch { 
        if { [ llength $args ] == 1 } { 
           set args [ lindex $args 0 ]
        }
        
        set now [ clock seconds ]
        
        regexp {\d+} $jobid job
        set jobid  $::RUNCODE$job
        
        ;## default -np value is '3'
        if { ! [ regexp -- {-np\s+(\d+)} $args -> want ] } {
           set want 3
        }
        
        ;## this is handled in the manager API to conserve system
        ;## resources (mgr::commandSanityChecks), but is here to
        ;## make debugging possible.
        if { $want < 2 } {
           set err "minimum possible value of '-np' option is '2'"
           return -code error $err
        }
        
        ;## this logic gives jobs that request few nodes
        ;## somewhat higher priority, because the likelihood
        ;## that a small number of nodes is available is
        ;## always higher than for any larger number of nodes.
        ;## SETTING ::MPI_QUEUE_TIMEOUT TO 0 DISABLES QUEUEING!!
        set waitfornodes 0
        if { $::MPI_QUEUE_TIMEOUT } {
           set used 0
           foreach node $::mpi::queue(NULL,nod) {
              incr used [ lindex $node end ] 
           }
           set have [ llength $::NODENAMES ]

           if { $::MPI_MULTIPLE_NODES } {
              set have [ expr $have * $::MPI_NODE_SHARE_LIMIT ]
              set have [ expr { $have - $used } ]
           } else {
              set have [ expr { $have - $used } ]
           }
           
           if { ! [ mpi::wrapperMasterAvailable ] } { set have 0 }
           
           if { $want > $have } {
              set waitfornodes 1
              set msg "this job has been queued until $want nodes "
              append msg "are available"
              if { ! [ info exists ::mpi::queue($job,afq) ] || \
                   ! [ string length $::mpi::queue($job,afq) ] } {
                 set ::mpi::queue($job,qus) $now
                 addLogEntry $msg green
              } elseif { [ info exists ::mpi::queue($job,qus) ] && \
                         [ string length $::mpi::queue($job,qus) ] } {
                 set started $::mpi::queue($job,qus)
                 set waited [ expr { $now - $started } ]
                 if { $waited >= $::MPI_QUEUE_TIMEOUT } {
                    set msg    "aborting job, insufficient resources "
                    append msg "available.\n"
                    append msg "job has been queued for more than "
                    append msg "$::MPI_QUEUE_TIMEOUT seconds.\n"
                    append msg "try again later."
                    return -code error $msg
                 }
              }
           }
        }
        
        if { $::mpi::waitforlamboot || $waitfornodes } {
           set background 1
           set ::mpi::queue($job,afq) [ after 2000 \
              [ list mpi::queueJob $jobid $background $args ] ]
        } else {
           mpi::startJob $jobid $background $args
        }
     } err ] } {
        if { [ llength [ array names ::mpi::queue $job,* ] ] } {
           if { $background } {
              set ::mpi::queue($job,ero) "[ myName ]: $err"
              set ::mpi::queue($job,end) 1
           } else {
              return -code error "[ myName ]: $err"
           }
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::wrapperMasterAvailable
##
## Description:
## determine whether, given the current node allocation
## state, a wrapper master can be assigned.
##
## conditional considerations are that no single physical
## node may be used as both a master and slave, unless
## it is declared multiple times in the nodelist, in which
## case it is treated as if it were more than one physical
## node.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::wrapperMasterAvailable { args } {
     
     if { [ catch {
        set bool 1
        ;## have to check for existence of ::wrappermaster($node)
        ;## Do we have a node with score 0 if all gateway
        ;## masters are used up? If not, set $have to 0!
        set N $::N_WRAPPER_MASTERS_ON_GATEWAY
        if { [ info exists ::wrappermaster(n0) ] && \
             $::wrappermaster(n0) >= $N } {
           ;## if a node has a score and is not already dedicated
           ;## to masters, we don't touch it, else...
           set N $::N_WRAPPER_MASTERS_ON_INTERNAL
           foreach [ list name node score ] \
              [ lindex $::mpi::queue(NULL,nod) 0 ] { break }
           
           if { [ info exists ::wrappermaster($node) ] && \
                $::wrappermaster($node) >= $N } {
           }     
           
           if { $score != 0 } {
              set bool 0
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $bool
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::startJob
##
## Description:
## Enter a new job, assign initial priority and nodes, and
## so forth...
##
## Parameters:
## jobid - the original assistant manager assignment
## priority - the priority for the user who requested the job
## mpi_opts - options passed to mpirun
## wrapper_opts - options passed to wrapper API
##
## Usage:
##
## Comments:
## The manager issues the priority based on an entry in the
## users queue.
##
## The growing of np has been disabled because LAM addresses
## are dynamic. 

proc mpi::startJob { jobid background args } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set pri [ set ::mpi::queue($job,pri) ]
        set tmp [ set ::mpi::queue($job,bal) ]
        set ::mpi::queue($job,bal) 1
        mpi::allocNodes NULL $job [ expr { $pri - 1 } ]
        set ::mpi::queue($job,bal) $tmp
        unset tmp
        
        if { ! [ llength [ set ::mpi::queue($job,nod) ] ] } {
           return -code error "No nodes available, try again later!"
        }
        
        foreach { job_opts mpirun_opts wrapper_opts } \
                [ mpi::parseOpts $jobid $args ]  \
                { break }
        
        set temp [ eval mpi::validateOpts $jobid $wrapper_opts ]
     
        set cmd [ list mpirun $mpirun_opts wrapperAPI $wrapper_opts ] 
        set port [ lindex [ set ::mpi::queue(NULL,cid) ] 0 ]
        set cid  [ set ::mpi::queue(NULL,cid) ]
        
        ;## update the queue entry
        foreach name $::mpi::queuenames {
           if { [ info exists $name ] } {
              set ::mpi::queue($job,$name) [ set $name ]
           }   
        }   
        
        mpi::setUser $jobid
        
        set ::mpi::queue($job,stt) [ clock clicks -milliseconds ] 
        lappend ::mpi::queue($job,stt) [ gpsTime ] 
        
        if { [ string length $::mpi::queue($job,usr) ] } {
           mpi::run $jobid $port $mpirun_opts $wrapper_opts
        } else {
           trace variable ::mpi::queue($job,usr) w \
           [ list mpi::run $jobid $port $mpirun_opts $wrapper_opts ]
        }
        
        if { [ info exists ::DEBUG_LOG_NODE_USAGE ] } {
           set msg "node usage: $::mpi::queue(NULL,nod)"
           addLogEntry $msg purple
        }
        
        set to [ expr { $::MPI_WRAPPER_INIT_TIMEOUT * 1000 } ]
        set ::mpi::queue($job,aft) \
           [ after $to mpi::abortIfHung $jobid ]
     
     } err ] } {
        if { [ llength [ array names ::mpi::queue $job,* ] ] } {
           if { $background } {
              set ::mpi::queue($job,ero) "[ myName ]: $err"
              set ::mpi::queue($job,end) 1
           } else {
              return -code error "[ myName ]: $err"
           }
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::setUser
## Assign the next available user to the job.
##
## Description:
## 
## Parameters:
##
## Usage:
##
## Comments:
## Will time out in ::WAITFORUSER minutes, throwing an
## exception.  
## If ::WAITFORUSER is not defined in the LDASmpi.rsc
## file, or is set to 0, system deadlocks will occur if
## the number of jobs in the API exceeds the number of
## search users available!!

proc mpi::setUser { jobid { i 0 } } {
     set mins 0

     ;## define ::WAITFORUSER to be an integer number
     ;## of minutes in the LDASmpi.rsc if you REALLY
     ;## want to wait for a user... this is only practical
     ;## if the nuber of available users is less than the
     ;## number of jobs which are allowed simultaneously.
     if {  ! [ info exists ::WAITFORUSER ] \
        || ! [ string length $::WAITFORUSER ] } {
        set timeout 0
     } else {
        set timeout $::WAITFORUSER
     } 
     
     if { [ catch {
        regexp {\d+} $jobid job
        
        if { ! [ info exists ::mpi::queue($job,usr) ] } {
           return {}
        }
        
        ;## try and assign a user for the wrapper job
        ;## if there seem to be users available
        if { [ llength $::mpi::users ] } {
           ;## if the first user is a null
           if { ! [ string length [ lindex $::mpi::users 0 ] ] } {
              ;## if the next user is valid
              if { [ string length [ lindex $::mpi::users 1 ] ] } {
                 ;## trim off the null
                 set ::mpi::users [ lrange $::mpi::users 1 end ]
              } else {
                 ;## otherwise re-initialise the users list to null
                 ;## because it is corrupt
                 set ::mpi::users [ list ]
              }   
           }
               
           if { [ string length [ lindex $::mpi::users 0 ] ] } {
              set ::mpi::queue($job,usr) [ lindex $::mpi::users 0 ]
              set ::mpi::users [ lrange $::mpi::users 1 end ]
              return {}
           }
        }
        
        ;## if were not going to wait for a user to become
        ;## available.
        if { ! $timeout } { return {} }
        
        if { ! [ expr { [ incr i ] % 60 } ] } {
           set mins [ expr { $i / 60 } ]
          debugPuts "waited $mins minutes for a username..."
        }
        if { $mins == $timeout } {
           set err "timed out after $mins min. waiting for a search user"
           if { [ llength [ array names ::mpi::queue $job,* ] ] } {
              set ::mpi::queue($job,ero) $err
              set ::mpi::queue($job,end) 1
           }
           return {}
        }
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        } else {
           return {}
        }
     }
     after 1000 mpi::setUser $jobid $i
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::unsetUser
##
## Description:
## Undoes what mpi::setUser does.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::unsetUser { jobid } {
     
     regexp {\d+} $jobid job
     
     if { [ catch {
         if { [ info exists ::mpi::queue($job,usr) ] } {
            set user $::mpi::queue($job,usr)
            if { [ string length $user ] } {
               ;## we used to reuse users ASAP by using index "0"
               ;## instead of "end".
               set ::mpi::users [ linsert $::mpi::users end $user ]
               ;## unset will destroy trace so mpi::run does
               ;## not get called if the job was aborted
               unset ::mpi::queue($job,usr)
               set ::mpi::queue($job,usr) [ list ]
            }   
         }

         ;## close the file opened by lam::shell
         if { [ info exists ::mpi::queue($job,shell) ] } {
            set shell $::mpi::queue($job,shell)
            if { [ regexp {^file\d+$} $shell ] } {
               catch { ::close $shell }
            }
            ::unset ::mpi::queue($job,shell)
         }
         if { [ info exists ::mpi::queue($job,retry) ] } {
            ::unset ::mpi::queue($job,retry)
         }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::stopJob
##
## Description:
## Unallocates all nodes and unregisters state info related
## to the specified job.
##
## Parameters:
##
## Usage:
##
## Comments:
## Call this AFTER a job is killed in the wrapper API.

proc mpi::stopJob { jobid } {
     
     set items $::mpi::queuenames
     
     if { [ catch {
        
        regexp {\d+} $jobid job
        
        if { [ info exists ::mpi::queue($job,aft) ] } {
           after cancel $::mpi::queue($job,aft)
           unset ::mpi::queue($job,aft)
        }
        
        if { [ info exists ::mpi::queue($job,afq) ] } {
           after cancel $::mpi::queue($job,afq)
           unset ::mpi::queue($job,afq)
        }
        
        mpi::wrapperMasterUnassign $jobid
        
        set ::mpi::queue($job,bal) 1
        
        mpi::allocNodes $job NULL all
        
        mpi::unsetUser $jobid
        
        if { [ info exists ::mpi::queue($job,stt) ] } {
           set endtime [ clock clicks -milliseconds ]
           set starttime [ lindex [ set ::mpi::queue($job,stt) ] 0 ]
           
           if { [ regexp {\d+} $starttime ] } {
              set dt [ expr { $endtime - $starttime } ]
              set dt [ expr { $dt / 1000.0 } ]
           }
        }
        foreach item [ array names ::mpi::queue $job,* ] {
           unset ::mpi::queue($item)
        }
     } err ] } {
        foreach item [ array names ::mpi::queue $job,* ] {
           catch { unset ::mpi::queue($item) }
        }
        addLogEntry $err red
     }
     
     if { [ info exists dt ] } {
        return $dt
     } else {
        return "unknown"
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::killJob
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::killJob { jobid { stopjoberr 0 } } {
     
     if { [ string equal NULL $jobid ] } { return {} }
     
     if { [ catch {
        set dt unknown
        regexp {\d+} $jobid job
        set jobid  $::RUNCODE$job
        
        set usr  [ list ]
        set errs [ list ]
        if { [ info exists ::mpi::queue($job,usr) ] } {
           set usr [ set ::mpi::queue($job,usr) ]
           set bal [ set ::mpi::queue($job,bal) ]
        }
        
        if { [ string length $usr ]  } {
           
           ;## if the error was a lal status error, then
           ;## the wrapperAPI will have exited cleanly, so
           ;## there is no need to force a kill of all
           ;## the process and do a lamboot
           set lal_status_error {Status\s+code}
           set wrapper_caught_error {Rank\s+\d+}
           set comm_timeout_error \
              {[nN]ode\s+(\S+)\s+has\s+not\s+communicated\s+for\s+(\d+)\s+seconds}
           set errmsg   [ set ::mpi::queue($job,ero) ]
           set lalerror [ regexp $lal_status_error $errmsg ]
           set mpierror [ regexp $wrapper_caught_error $errmsg ]
           set comerror [ regexp $comm_timeout_error $errmsg ]
           
           set ::mpi::waitforlamboot 1
           
           if { ! $lalerror && ! $mpierror && ! $comerror } {
           
              if { $bal } {
                 set nodes [ set ::mpi::queue(NULL,nod) ]
              } else {
                 set nodes [ set ::mpi::queue($job,nod) ]
              }
    
              set tmp $::mpi(host)
              foreach node $nodes {
                 foreach [ list name node score ] $node {
                    lappend tmp $name
                 }
              }
           
              set nodes [ lsort -dictionary -unique $tmp ]
              set tmp [ list ]
           
              set errs [ mpi::killJobProcesses $jobid $usr $nodes ]
               
              if { ! [ regexp {atExit} [ uplevel myName ] ] } { 
                 mpi::prestartLamds $jobid $usr
              } 
           ;## end of 'if not lalerror and not mpierror'
           } else {
              ::lam::clean $jobid $usr
           }
        } ;## end of 'if string length usr'   
           
        ;## if we did not wind up here because of an
        ;## error in mpi::stopJob.
        if { ! $stopjoberr } {
           set dt [ mpi::stopJob $jobid ]
        } else {
           set ::mpi::queue($job,bal) 1 
           mpi::allocNodes $job NULL all
           mpi::unsetUser $jobid
           foreach item [ array names ::mpi::queue $job,* ] {
              unset ::mpi::queue($item)
           }
        }
        
        if { [ string length $errs ] > 6 } {
           return -code error $errs
        }
        
     } err ] } {
        set ::mpi::waitforlamboot 0
        if { [ string length $err ] } {
           addLogEntry $err blue
        }
     }
     return $dt
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::killJobProcesses
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::killJobProcesses { jobid user nodes } {
     
     if { [ catch {
        set errs [ list ]
         
        set wrong_user {mpirun\(\d+\):\s+Operation\s+not\s+permitted\s*}
           
        set no_wrapper_killed {wrapperAPI:\s+no\s+process\s+killed*}
           
        foreach node $nodes {
           if { [ catch {
              foreach process [ list mpirun wrapperAPI ] {
                 if { [ string equal mpirun $process ] && \
                    ! [ string equal $::WRAPPERHOST $node ] } {
                    continue
                 }
                 eval ::exec $::MPI_RSH_COMMAND -n -l $user $node \
                 $::MPI_KILL_COMMAND $process
              }
           } err ] } {
              regsub -all $wrong_user $err {} err
              if { ! [ regexp $no_wrapper_killed $err ] } {
                 lappend errs "${user}@${node}:${process}: $err"
              }
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $errs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::abortJobInDcApi 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::abortJobInDcApi { jobid } {
     
     if { [ catch {
        if { [ regexp {\d+} $jobid job ] } {
           set sid [ sock::open datacond emergency ]
           fconfigure $sid -blocking off
           puts $sid "$::MGRKEY \"if \{ \[ info exists ::dc::state($job,abort) \] \} \{ set ::dc::state($job,abort) 1 \}\""
           ::close $sid
        }   
     } err ] } {
        addLogEntry "datacond API unreachable!!: $err" red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::knownJobids 
##
## Description:
## Parses the ::mpi::queue state array to get all of the
## current jobid's
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::knownJobids { } {
     set jobids [ list ]
     if { [ catch {
        set data [ array names ::mpi::queue ]
        foreach item $data {
           set item [ split $item "," ]
           set item [ lindex $item 0 ]
           if { [ string length $item ] } {
              if { [ lsearch $jobids $item ] == -1 } {
                 lappend jobids $item
              }   
           }   
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $jobids
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::atExit 
##
## Description:
## Run when the mpi API does a controlled shutdown so that
## whatever cleanup is possible is done.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::atExit { args } {
     
     if { [ catch {
        set jobids [ mpi::knownJobids ]
        
        foreach job $jobids {
           if { [ string equal NULL $job ] } {
              continue
           }
           mpi::abortJobInDcApi $job
           mpi::killJob $job
        }
        
        foreach usr $::MPIUSERS {
           if { [ catch {
              addLogEntry "calling lam::halt for user '$usr'" blue
              lam::halt $usr
           } err ] } {
              addLogEntry $err red
           }
        }

     } err ] } {
        puts stderr "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::cfgJobstate
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::cfgJobstate { port cid address - } {
     fileevent  $cid readable "jobstate $port $cid"
     fconfigure $cid -blocking off
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::allocNodes
##
## Description:
## If a receptor has a higher priority than a donor,
## and has nodes available, then nodes are transferred
## from the donor to the borrower.
## The "NULL" job is a special case representing the system
## resources known to MPI.
##
## Parameters:
## donor - the job we want to borrow nodes from
## receptor - the job we want to assign more nodes to
## n - the number of nodes we want to assign
##
## Usage:
##
## Comments:
## Requires a node switching command for MPICH.
## all allocations/deallocations are borrownode actions
## involving the "null" job.

proc mpi::allocNodes { donor receptor n } {
     set nodes [ list ]
     
     if { ! [ string equal NULL $donor ] } {
        set jobid $donor
        regexp {\d+} $jobid job
     }
     
     if { [ catch {
        ;## if either the donor or receptor has -doLoadBalance
        ;## set to FALSE, do not reallocate.
        ;## mpi::stopJob and mpi::startJob override.
        if { [ expr { [ set ::mpi::queue($donor,bal) ] + \
                      [ set ::mpi::queue($receptor,bal) ] } ] == 2 } {
     
        set N 0
     
        ;## get donor and receptor allocations
        if { ! [ info exists ::mpi::queue($donor,nod) ] } {
           return {}
        }
        
        set dn [ llength $::mpi::queue($donor,nod) ]
        set rn [ llength $::mpi::queue($receptor,nod) ]
        
        ;## get donor and receptor priorities
        set dp $::mpi::queue($donor,pri)
        set rp $::mpi::queue($receptor,pri)
        
        if { [ string equal -nocase all $n ] } {
           set n $dn
        }
        
        if { [ string equal NULL $donor ] } {
           if { $rp >= [ expr { $rn + $n } ] } {
              if { $dn >= $n } {
                 set N $n
              } else {
                 set N $dn
              }
           } else {      
              set N [ expr { $rp - $rn } ]
           }
        
        ;## if specified donor can be tapped due to low priority
        } elseif { $rp >= $dp } {
           if { $dn >= $n } {
              set N $n
           } else {
              set N $dn
           }
        ;## :TODO: be very aggressive and tap unspecified
        ;## low priority donors?
        } else {
        }
        
        if { $N } {
           set nodes \
               [ lsort -dictionary \
               [ lrange $::mpi::queue($donor,nod) 0 [ expr $N-1 ] ] ] 
           set ln [ llength $nodes ]
           set lr [ llength [ set ::mpi::queue($receptor,nod) ] ]
           
           if { [ string equal NULL $donor ] } {
              set nodes [ mpi::nullDonor $nodes ]
           } else {
              set ::mpi::queue($donor,nod) \
                  [ lsort -dictionary \
                  [ lrange $::mpi::queue($donor,nod) $N end ] ]
           }       
           
           if { [ string equal NULL $receptor ] } {
              mpi::nullReceptor $nodes
           } else {   
              set ::mpi::queue($receptor,nod) \
                  [ lsort -dictionary \
                  [ concat $::mpi::queue($receptor,nod) $nodes ] ]
           }       
        }
     }
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        }   
     }
     return $nodes
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::nullDonor 
##
## Description:
## Handle the node allocation When the nodes are to be got
## from the NULL job pool of unused nodes.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::nullDonor { nodes } {
     
     if { [ catch {
        foreach node $nodes {
           foreach { name node } $node { break }
           set i       0
           set skip    0
           set matched 0
           
           foreach el $::mpi::queue(NULL,nod) {
              foreach { nameN nodeN score } $el { break }
              if { [ string equal $name $nameN ] } {
                 set matched 1
                 break
              }
              incr i
           }
           if { ! $matched } { continue }
           
           ;## if we are NOT allowing multiple jobs on a
           ;## given node
           if { ! $::MPI_MULTIPLE_NODES } {
              if { ! [ info exists temp ] } {
                 set temp [ list ]
              }
              
              if { ! $score } {
                 lappend temp [ list $nameN $nodeN ]
              } else {
                 set skip 1
              }
           }

           if { $skip } {
              set skip 0
           } else {
              set ::mpi::queue(NULL,nod) \
                  [ lreplace $::mpi::queue(NULL,nod) $i $i \
                  [ list $name $node [ incr score ] ] ]
              set ::mpi::queue(NULL,nod) \
                  [ lsort -index 2 -integer $::mpi::queue(NULL,nod) ]
           }
        }
        
        ;## switch on ::MPI_MULTIPLE_NODES
        if { ! [ info exists temp ] } {
           foreach node $nodes {
              lappend temp [ lrange $node 0 1 ]
           }
        } else {
           if { [ llength $temp ] != [ llength $nodes ] } {
              mpi::nullReceptor $temp
              set temp [ list ]
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $temp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::nullReceptor 
##
## Description:
## Handle the node allocation When the nodes are to be given
## back to the NULL job pool of unused nodes.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::nullReceptor { nodes } {
     
     if { [ catch {
        foreach node $nodes {
           set name [ lindex $node 0 ]
           set i 0
           set matched 0
           foreach el $::mpi::queue(NULL,nod) {
              foreach { nameN nodeN score } $el { break }
              if { [ string equal $name $nameN ] } {
                 set matched 1
                 if { $score } {
                    break
                 }
              }
              incr i
           }

           if { $matched && $score } {
              set ::mpi::queue(NULL,nod) \
                  [ lreplace $::mpi::queue(NULL,nod) $i $i \
                  [ list $nameN $nodeN [ incr score -1 ] ] ]
              set ::mpi::queue(NULL,nod) \
                  [ lsort -index 2 -integer $::mpi::queue(NULL,nod) ] 
           } elseif { $matched } {
              ;## node sharing and we are strange
           } else {
              set subject "mpiAPI: node '$node' removed"
              set msg "node '$node' has been retired from beowulf"
              append msg " cluster at ${::LDAS_SYSTEM}."
              addLogEntry "Subject: ${subject}; Body: $msg" email
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::handleRequest
##
## Description:
## Handle communications from the wrapperAPI.
## Parses the request, determines the correct response,
## and responds to the wrapper.
##
## Parameters:
## request - a "\n" delimited list of request strings
## generated by the wrapper API.
##
## Usage:
##
## Comments:
## Is an error always fatal to a job?

proc mpi::handleRequest { request } {
     set stopjob 0
     set cmd [ list ]
     if { [ catch {

        ;## special regex for detecting dead communicator warning
        set warn_rx {[nN]ode\s+(\S+)\s+has\s+not\s+communicated\s+for\s+(\d+)\s+seconds}
        
        ;## special regexp for job synchronization errors.
        ;## matches: jobID, expected: 4292 received: 4272
        ;## will call mpi::abortJobInDcApi on the current job
        ;## to prevent propagation of the synch error to
        ;## modulo N jobs.
        set sync_rx {jobID,\s+expected:\s+(\d+)\s+received:\s+(\d+)}
        
        foreach item $request {
           
           foreach [ list jobid type iter option1 option2 option3 ] \
                   $item { break }
           
           regexp {\d+} $jobid job

           if { ! [ info exists ::mpi::queue($job,job) ] } {
              set msg "Unknown jobid in wrapper request $jobid"
              return -code error $msg
           }
           
           switch -exact -- $type {
              request {
                      set op $option1
                      set N  $option2
                      set cmd [ mpi::order $job $op $iter $N ]
                      }
              warning {
                      set msg $option1
                       if { [ regexp $warn_rx $msg -> node seconds ] } {
                          if { $seconds >= $::NODE_COMM_TIMEOUT } {
                             set cmd [ mpi::order $job kill $iter {} ]
                             lappend ERROR $msg
                          } else {
                             set cmd [ mpi::order $job cont $iter {} ]
                          }
                       } else {
                          set cmd [ mpi::order $job cont $iter {} ]
                       }
                      }
                error {
                      set msg $option1
                         if { [ regexp {Rank\s+\d+} $option1 ] } {
                            lappend ERROR $msg
                         }
                         if { [ regexp $sync_rx $msg -> j1 j2 ] } {
                            mpi::abortJobInDcApi $j1
                         }
                      }
             progress {
                      set percent $option1
                      set msg "progress: $percent"
                      set cmd [ mpi::order $job cont $iter {} ]
                      }
                using {
                      set N $option1
                      set M $option3
                      set nodes $option2
                      set msg "using $N of $M nodes (nodes: $nodes)"
                      set cmd [ mpi::order $job cont $iter {} ]
                      }
      projected_ratio {
                      set percent $option1 
                      set msg "projected ratio: $percent"
                      set cmd [ mpi::order $job cont $iter {} ]
                      }
         initializing {
                      set ::mpi::queue($job,run) running
                      }
           finalizing {
                      set finalized 1
                      }
              default { ;## can't get here!
                      addLogEntry "bad request type: $type ($request)" 2
                      }
           } ;## end of switch

        } ;## end of foreach on items in request   
     
        ;## the end of a normal job
        if { [ info exists finalized ] } {
           set ::mpi::queue($job,end) 1
           set cmd {}
        }
        
        ;## if an error condition was reported, callback time,
        ;## otherwise ping the eventmon API.
        if { [ info exists ERROR ] } {
           if { [ llength [ array names ::mpi::queue $job,* ] ] } {
              set ::mpi::queue($job,ero) $ERROR
              set ::mpi::queue($job,end) 1
           } else {
              ;## job is already gone, do nothing
           }
        } elseif { ! [ info exists ::mpi::queue($job,run) ] } {
           ;## job is already gone, do nothing
        } else {
           ;## we are running happily
           mpi::resetHungJobTimeout $jobid
           mpi::pingEventmonAPI $jobid
        }
     
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::order
##
## Description:
## Form an order for the wrapper API based on a request.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::order { jobid operation iter N } {
     regexp {\d+} $jobid job
     if { [ catch {
        switch -exact -- $operation {
           add {
               set nodes [ mpi::allocNodes NULL $job $N ]
               set N [ llength $nodes ]
               set nodelist ([ join [ split $nodes ] "," ])
               set cmd "${job}.$iter:add $N $nodelist"
               }
           sub {
               set nodes [ mpi::allocNodes $job NULL $N ]
               set N [ llength $nodes ]
               set nodelist ([ join [ split $nodes ] "," ])
               set cmd "${job}.$iter:sub $N $nodelist"
               }
          kill {
               set cmd "${job}.$iter:kill"
               }
          cont {
               set cmd "${job}.$iter:cont"
               }
       default {
                return -code error "bad command: $op"
               }
         } ;## end of switch       
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::parseWrapperRequest
##
## Description:
## Parses requests from the wrapper API.
##
## Parameters:
##
## Usage:
##
## Comments:
## The wrapper API will always send at least 2 commands
## together. :TODO: how do we decide to continue or kill??
##

proc mpi::parseWrapperRequest { request } {
     
     if { ! [ string length $request ] } {
        return -code error "[ myName ]: null command recieved"
     }
     
     set parsed_request [ list ]
     
     ;## the possible request types and the regex patterns
     ;## that will parse their contents.
     ;## as of 08/19/02 the finalizing message looks like:
     ;## 10560205.2:finalizing totalTime=18.4403 0 ( ) ...
     ;## so we can parse out the wrapper runtime.
     array set rx [ list \
              request         {(add|sub)\s+(\d+)} \
              warning         {(.+)} \
              error           {(.+)} \
              progress        {(\d{1,3}\.\d{1,2}\%)} \
              using           {(\d+)[^\d]+([0-9\,\-]+)[^\d]+(\d+)} \
              finalizing      {(.*)} \
              initializing    {(.+)} \
              projected_ratio {(\d{1}\.\d{1,5})} \
              ]
     
     foreach item $request {
        
        ;## each request consists of a pair of 'things', a
        ;## class and a message
        foreach { class msg } $item { break }
        
        ;## every class looks like $job.$iter:$type
        if { [ regexp {(\d+)\.(\d+):(.+)} $class -> jobid i type ] } {
           set jobid $::RUNCODE$jobid
        }   
        
        set v1 {}
        set v2 {}
        set v3 {}
        
        ;## validate that the request type is one we know about
        set test_rx ([ join [ array names rx ] | ])
        if { ! [ regexp -- $test_rx $type ] } {
           return -code error "Invalid request type: '$type'"
        }
        
        ;## split the known type if possible:
        regexp [ set rx($type) ] $msg -> v1 v2 v3
        
        if { [ catch {
           switch -exact -- $type {
              request { ;## v1 is operation, v2 is N or {}, v3 is {}
                      }
                using { ;## v1 is N, v2 is nodes, v3 is N available
                      }
              warning { ;## v1 is message, v2 is {}, v3 is {}
                      }
                error { ;## v1 is message, v2 is {}, v3 is {} 
                      }
             progress { ;## v1 is nnn.mm%, v2 is {}, v3 is {}
                         if { ! [ info exists v1 ] } {
                            set msg "invalid numeric format in $type."
                            return -code error $msg
                         }
                      }
      projected_ratio { ;## v1 is n.mmmmm, v2 is {}, v3 is {}
                        if { ! [ info exists v1 ] } {
                           set msg "invalid numeric format in $type."
                           return -code error $msg
                        } 
                      }
         initializing { ;## will be used to set run state to 'running'
                      }
           finalizing { ;## end of job
                        if { [ regexp {totalTime} $msg ] } {
                           mpi::reportWrapperRuntime $jobid $msg
                        }
                      }
              default {
                      set msg "invalid type: '$type' in item: '$item'"
                      return -code error $msg
                      }
           } ;## end of switch
           
           mpi::filterLogEntries $jobid $type $item
           
        } err ] } {
           return -code error "[ myName ]: $err"
        }

        lappend parsed_request [ list $jobid $type $i $v1 $v2 $v3 ]
     }
     return $parsed_request
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::filterLogEntries 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::filterLogEntries { jobid type message } {
     
     if { [ catch {
        set log 0
        set level green
        switch -exact -- $type {
              request {
                       set log 1
                      }
                using {
                       if { ! [ info exists ::DO_NOT_LOG_USING ] || \
                            [ string equal 0 $::DO_NOT_LOG_USING ] } {
                          set log 1
                       }     
                      }
              warning {
                       set log 1
                       set level red
                      }
                error {
                       set log 1
                       set level red
                      }
             progress {
                       if { ! [ info exists ::DO_NOT_LOG_PROGRESS ] || \
                            [ string equal 0 $::DO_NOT_LOG_PROGRESS ] } {
                          set log 1
                       }  
                      }
      projected_ratio {
                       if { ! [ info exists ::DO_NOT_LOG_RATIO ] || \
                            [ string equal 0 $::DO_NOT_LOG_RATIO ] } {
                          set log 1
                       }  
                      }
         initializing {
                       set log 1
                      }
           finalizing {
                       set log 1
                      }
              default {
                      set msg "invalid type: '$type' in item: '$item'"
                      return -code error $msg
                      }
           } ;## end of switch
        
        if { $log == 1 } {
           addLogEntry $message $level     
        }
        mpi::wrapperLog $message $level
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::extractEmbeddedDsoMail
##
## Description:
## Extract embedded email messages that authors might put
## into their DSO code.
##
## Truncate message length if necessary to $maxlen + 1.
##
## Parameters:
##
## Usage:
## A DSO author can embed messaging code in status or
## error messages of the form:
##
##  Email(user@host|host:port,'256 char message')
##
## A log entry will be created with this content:
##
## address:$address msg:'$msg'
##
## Comments:
##

proc mpi::extractEmbeddedDsoMail { jobid request { maxlen 256 } } {
     
     if { [ catch {

        regexp {\d+} $jobid job
        
        ;## regular expression pattern to match entire
        ;## embedded message.  we need to match in a
        ;## single shot to make the regsub work.
        set msg_rx {Email\(\s*(.+[@:]\S+)\s*'(.+)'\s*\)}

        ;## make sure resource variable is well-formed and sane
        if { [ info exists ::MAX_EMBEDDED_DSO_EMAIL_CHARLEN ] } {
           set maxlen $::MAX_EMBEDDED_DSO_EMAIL_CHARLEN
        }
        if { [ regexp {^\d+$} $maxlen ] } {
           if { $maxlen < 256 } { set maxlen 256 }
        } else {
           set maxlen 256
           set ::MAX_EMBEDDED_DSO_EMAIL_CHARLEN $maxlen
           set msg "badly formatted ::MAX_EMBEDDED_DSO_EMAIL_CHARLEN"
           append msg ": '$maxlen' must be an integer value >= 256"
           append msg " (falling back to 256 chars)"
           addLogEntry $msg red
        }
        
        ;## parse out and remove Email message strings from
        ;## request.  as many as exist!!
        while { [ regexp $msg_rx $request -> address msg ] } {
              set msg [ string trim $msg ]
           if { [ string length $msg ] > $maxlen } {
              set msg [ string range $msg 0 $maxlen ]
              append msg "(long message truncated)"
           }
           set subject "wrapperAPI $::mpi::queue($job,dso)"
           
           set tmp [ set ::${::API}_email ]
           set ::${::API}_email $address
           addLogEntry "Subject: $subject ; Body: '$msg'" email
           set ::${::API}_email $tmp
           
           regsub $msg_rx $request {} request
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $request
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::parseOpts
##
## Description:
## Parse -mpiargs options and assign them to the proper
## class: mpirun or wrapperAPI.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::parseOpts { jobid args } {
     
     if { [ catch {
        set tmp          [ list ]
        set opts         [ list ]
        set job_opts     [ list ]
        set mpirun_opts  [ list ]
        set wrapper_opts [ list ]
     
        ;## make sure the jobid arg to the wrapper is
        ;## an integer!
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        regsub -all -- $jobid $args $job args
         
        if { [ llength $args ] == 1 } {
           set args [ lindex $args 0 ]
        }
        
        set bal [ set ::mpi::queue($job,bal) ]
        foreach [ list np nodes ] [ mpi::nodeList $jobid ] { break }
        
        set xnp [ llength $::NODENAMES ] 
         
        ;## calculate the options list
        foreach { name rx default } $::mpi::options {
           set name [ string trim $name "=" ]
           set name [ string tolower $name   ]
           lappend opts $name $default
        }
        
        foreach { opt val } [ expandOpts ] {
           set opt [ string trim $opt - ]
           set $opt $val
        }   
        
        ;## should never get here because the queueing code will
        ;## not permit it, but better safe than sorry!
        if { $np > $xnp } {
           set ::mpi::queue($job,pri) 0
           set msg "you requested $np nodes, but there are only "
           append msg "$xnp beowulf nodes on this system"
           return -code error $msg
        }
        
        switch -exact -- $::MPI {
                 mpich {
                       lappend mpirun_opts -machinefile $machinefile
                       lappend mpirun_opts -nolocal $nolocal
                       lappend mpirun_opts -np $np
                       }
                   lam {
                       lappend mpirun_opts $nodes
                       }
               default {
                       }
        }
        
        ;## format -nodelist option
        set i 0
        foreach node $nodes {
           lappend tmp [ incr i ]
        }
        set nodes [ join [ lrange $tmp 0 end-1 ] "," ]
      
        foreach { name rx default } $::mpi::options {
           set value \
              [ set [ string tolower [ string trim $name "-=" ] ] ]
           if { [ regexp {\,} $value ] } {
              if { ! [ regexp {\((.+)\)} $value ] } {
                 set value ($value)
              }   
           }
           
           ;## dynlib handler
           if { [ regexp {dynlib} $name ] } {
              set value [ mpi::findDynLib $jobid $value ]
           }
           
           if { [ regexp {=} $name ] } {
              lappend wrapper_opts $name$value
           } else {
              lappend job_opts $name $value
           }
        }
        
        if { $bal } { set bal TRUE } else { set bal FALSE }
        set bal [ string toupper $bal ]
        
        set mpirun_opts [ subst -nobackslashes -nocommands $mpirun_opts ]
        regsub -all -- {\s+} $mpirun_opts { } mpirun_opts
        
        set wrapper_opts [ subst -nocommands -nobackslashes $wrapper_opts ]
        regsub -all -- {\s+} $wrapper_opts { } wrapper_opts

        set job_opts [ subst -nocommands -nobackslashes $job_opts ]

        regsub -all -- {\s+} $job_opts { } job_opts
     
     } err ] } {
        return -code error "[ myName ]: $err"
     }   
     return [ list $job_opts $mpirun_opts $wrapper_opts ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::nodeCalc
##
## Description:
## Helper for mpi::parseOpts which produces the correct
## mpirun option for node assignment.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::nodeCalc { bal nodes } {
     
     if { [ catch {
        set commworld [ llength $::NODENAMES ]
        
        ;## if we are not doing load balancing the nodes
        ;## are always n1, n2, n3 ... but we don't modify
        ;## nod because we would break scavenging.
        if { ! $bal } {
           set i 0
           foreach node [ split $nodes "," ] {
              lappend tmp [ incr i ]
           }
           set np [ llength $tmp ]
           
           ;## if we have permission to start multiple mpi
           ;## communicators on each node
           if { $::MPI_MULTIPLE_NODES } {
              set np [ expr { $commworld } ]
           }

           ;## finally, build the real node list
           set nodes [ join $tmp "," ]
        }
        
        ;## and adjust the node list for lam's mpirun
        if { [ string equal $::MPI lam ] } {
           if { $bal } { set np $commworld }
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $np $nodes ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::nodeList
##
## Description:
## Generate a cslist to be passed as a -nodeList= arg to
## the wrapper API from the ::mpi::queue($job,nod) element.
##
## When we initialise tmp with n0 the wrapper master runs
## on the master node, However the master node can become
## saturated by this and so at some level of saturation we
## want to start handing off master responsibilities to other
## nodes.
##
## Parameters:
##
## Usage:
##
## Comments:
## :TODO: add a ::wrappermaster($node) array or ?? 

proc mpi::nodeList { jobid } {
     if { [ catch {
        
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set tmp n0
        
        ;## if you want wrapper masters to be distributed
        ;## throughout the cluster instead of having all of
        ;## them running on the 'gateway' beowulf node, then
        ;## define ::DISTRIBUTE_WRAPPER_MASTERS to be '1'.
        if { [ info exists ::DISTRIBUTE_WRAPPER_MASTERS ] } {
           set distribute $::DISTRIBUTE_WRAPPER_MASTERS
           if { [ string equal 1 $distribute ] } {
              ;## put the wrapper master on a random node
              set tmp [ mpi::wrapperMasterAssign $jobid ]
           }
        } else {
           if { [ array exists ::wrappermaster ] } {
              ::unset ::wrappermaster
           }
        }
        
        set nodes [ set ::mpi::queue($job,nod) ]
        foreach node $nodes {
           lappend tmp [ lindex $node 1 ]
        }
        set nodes $tmp
        set np [ llength $nodes ]
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $np $nodes ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::wrapperMasterAssign
##
## Description:
## Try and assign a wrapper master based on all current
## heuristics and options.
##
## The tricky part is treating the master differently at
## start time, but then treat it like any other node at
## cleanup time.
##
## Note that it is necessary to set ::DISTRIBUTE_WRAPPER_MASTERS
## to '1' for this code to be actively used.
##
## Avoid assigning a node already in the $job,nod list
## as the wrapper master! There must be at least one
## node with a score of zero for a job to start.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::wrapperMasterAssign { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set master n0
 
        set N $::N_WRAPPER_MASTERS_ON_GATEWAY
        
        ;## first, handle the beowulf gateway allocations
        if { [ info exists ::wrappermaster(n0) ] } {
           if { $::wrappermaster(n0) < $N } {
              incr ::wrappermaster(n0)
           ;## and if it's full, go looking for a candidate in
           ;## the cluster.
           } else {
              ;## multiple masters can run on the same internal node,
              ;## but a master can NEVER run on a compute node.
              set master [ lindex $::mpi::queue(NULL,nod) 0 ]
              foreach [ list name node score ] $master { break }
              ;## there is a test in mpi::queueJob to make sure that
              ;## the master is not also used as a working node.
              set master [ list $name $node [ expr { $score + 1 } ] ]
              set ::mpi::queue(NULL,nod) \
                 [ lreplace $::mpi::queue(NULL,nod) 0 0 $master ]
              set ::mpi::queue(NULL,nod) \
                 [ lsort -index 2 -integer $::mpi::queue(NULL,nod) ]
              set master $node
              if { [ string length $master ] } {
                 if { [ info exists ::wrappermaster($master) ] } {
                    incr ::wrappermaster($master)
                 } else {
                    set ::wrappermaster($master) 1
                 }
              } else {
                 set err "no nodes available for wrapper master. "
                 append err "current process node allocation: "
                 append err "$::mpi::queue(NULL,nod)"
                 if { [ array exists ::wrappermaster ] } {
                    append err " wrapper master status: "
                    append err "[ array get ::wrappermaster ]"
                 }
                 return -code error $err
              }
           }      
        } else {
           set ::wrappermaster(n0) 1
        }
        
        set ::mpi::queue($job,mas) $master
        
     } err ] } {
        mpi::wrapperMasterUnassign $jobid
        return -code error "[ myName ]: $err"
     }
     return $master
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::wrapperMasterUnassign
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::wrapperMasterUnassign { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job

        if { [ info exists ::mpi::queue($job,mas) ] && \
             [ string length $::mpi::queue($job,mas) ] } {
           set master $::mpi::queue($job,mas)
           unset ::mpi::queue($job,mas)
           if { [ info exists ::wrappermaster($master) ] } {
              if { $::wrappermaster($master) > 0 } {
                 incr ::wrappermaster($master) -1
                 ;## short circuit if wrapper master is n0
                 if { [ string equal n0 $master ] } { return {} }
                 
                 set i 0
                 foreach node $::mpi::queue(NULL,nod) {
                    foreach [ list name node score ] $node { break }
                    if { [ string equal $node $master ] } {
                       set score [ expr { $score - 1 } ]
                       if { $score < 0 } {
                          set err "ERROR: score is less than 0: '$score'"
                          return -code error $err
                       }
                       set node [ list $name $node $score ]
                       break
                    }
                    incr i
                 }
                 
                 set ::mpi::queue(NULL,nod) \
                    [ lreplace $::mpi::queue(NULL,nod) $i $i $node ]
                 set ::mpi::queue(NULL,nod) \
                    [ lsort -index 2 -integer $::mpi::queue(NULL,nod) ]
              
              } else { 
                 set err "bad count for ::wrappermaster($master): "
                 append err "'$::wrappermaster($master)'"
                 return -code error $err
              }
           } else {
              set err "::wrappermaster($master) does not exist!"
              return -code error $err
           }
        }
     
     } err ] } {
       if { [ string length $err ] } {
          addLogEntry $err red
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::run
##
## Description:
## Wrapper for mpirun script.
## Returns the start time of the job.
##
## Parameters:
##
## Usage:
##   mpirun -np 5 wrapperAPI -nodelist="{1-2,4}" \
##                           -dynlib="/path/file.a" \
##                           -mpiAPIport="{maasim,10000}" \
##                           -dataAPI="{datahost,5678}" \
##                           -resultAPI="{reshost,9101}" \
##                           -filterparams="{1,4.5,67.8f, -9.1e-1}" \
##                           -nodeDutyCycle=4 \
##                           -realTimeRatio=0.9 \
##                           -doLoadBalance=TRUE
##   
## Comments:
## mpirun returns immediately.

proc mpi::run { args } { 
     
     set jobid     [ lindex $args 0 ]
     set port      [ lindex $args 1 ]
     set mpi_args  [ lindex $args 2 ]
     set wrap_args [ subst -nobackslashes -nocommands [ lindex $args 3  ] ]
     set tracejunk [ string length [ lindex $args 4 ] ]
     
     if { [ catch {
        regexp {\d+} $jobid job
        set usr $::mpi::queue($job,usr)
        if { ! [ string length $usr ] } {
           return -code error "No search users available, try again later."
        }
        
        if { [ string equal $::MPI mpich ] } {
           return -code error "MPICH no longer supported!"
        } elseif { [ string equal $::MPI lam ] } {
           lam::ssh $jobid $mpi_args $wrap_args
        }   
        
        set starttime [ clock clicks -milliseconds ]
        lappend starttime [ gpsTime ]
        set ::mpi::queue($job,stt) $starttime
        after 0 mpi::dropNodeUsageReport [ lindex $starttime end ]
        
     } err ] } {
        if { $tracejunk } {
           if { [ llength [ array names ::mpi::queue $job,* ] ] } {
              set ::mpi::queue($job,ero) $err
              set ::mpi::queue($job,end) 1
           }
        } else {
           if { [ regexp -nocase -- {env: no such file} $err ] } {
              set err "mpirun command not found on $::WRAPPERHOST"
           }   
           return -code error "[ myName ]: $err"
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::dropNodeUsageReport
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::dropNodeUsageReport { gpstime } {
     
     if { [ catch {
       
        set nodes 0
        
        foreach node $::mpi::queue(NULL,nod) {
           incr nodes [ lindex $node end ]
        }
        
        ;## write data point to file, archive as necessary
        set fname $::LDASLOG/${::API}.nod
        set arc   $::LDASARC/nodeusage/${::API}.nod.$gpstime
        if { [ file exists $fname ] && [ file size $fname ] > 1000000 } {
           if { ! [ file exists [ file dirname $arc ] ] } {
              file mkdir [ file dirname $arc ]
           }
           file rename -force $fname $arc
        }
        
        set fid [ open $fname a+ ]
        puts $fid "$gpstime $nodes"
        ::close $fid
        
     } err ] } {
        addLogEntry $err red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::validateOpts
##
## Description:
## Validate format of mpirun and mpi API command strings.
##
## Parameters:
##
## Usage:
##
## Comments:
## nodeDutyCycle is N >= 1
## slaveReportCycle defaults to 1
##
## recommended that doLoadBalancing be set to 0 when
## the dataDistributor method is "C".
##
## nodeDutyCycle is only siginificant if doLoadBalance
## is true.

proc mpi::validateOpts { jobid args } {
    
     if { ! [ string length $args ] } {
        return -code error "[ myName ]: empty arg list recieved"
     }
     if { [ llength $args ] == 1 } { 
        set args [ lindex $args 0 ] 
     }
    
     set errors [ list ]
     set retval [ list ]
     
     foreach { name rx default } $::mpi::options {
        set name [ string trim $name "-=" ]
        set name [ string tolower $name   ]
        set opt_rx($name) $rx
     }
     
     foreach arg $args {
        if { ! [ regexp {(.+)=(.+)} $arg -> name value ] } {
           if { [ regexp {(.+)=} $arg ] } {
              lappend errors "required option missing: $arg"
           } else {
              lappend errors "malformed option: $arg"
           }   
           continue
        }
        
        set name  [ string trim $name         ]
        set name  [ string trimleft $name "-" ]
        set name  [ string tolower $name      ]
        set value [ string trim $value        ]
        
        regexp {\{(.+)\}} $name  -> name
        regexp {\"(.+)\"} $name  -> name
        
        if { [ catch {
           set rx [ set opt_rx($name) ]
        } err ] } {
           lappend errors "invalid option passed: '$arg'"
           continue
        }   
        
        if { ! [ regexp -nocase -- $rx $value ] } {
           if { ! [ string length $value ] } {
              lappend errors "required option missing: $arg"
           } else {
              lappend errors "malformed option: $arg"
           }
        } else {
           if { [ string equal dynlib $name ] } {
              if { [ catch {
                 set value [ mpi::findDynLib $jobid $value ]
              } err ] } {
                 lappend errors $err
              }
           }
           lappend retval $name $value
        }   
     }
     
     if { [ string length $errors ] > 3 } {
        return -code error "[ myName ]: $errors"
     } else {
        return $retval
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::findDynLib
##
## Description:
## Make use of resource variable ::DYNLIB_DIRECTORY to
## try and find the specified dso based on a standard
## location for dso's.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::findDynLib { jobid soname } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set apidir [ apiDirectory ]
        ;## a relative path is ALWAYS taken relative to
        ;## ::DYNLIB_DIRECTORY, and a bare filename IS
        ;## a relative path.
        if { ! [ regexp {^\/} $soname ] } {
           if { [ info exists ::DYNLIB_DIRECTORY ] } {
              set dir $::DYNLIB_DIRECTORY
              set dir [ string trim $dir / ]
              set dir /$dir/
              set soname [ string trim $soname / ]
              ;## is ::DYNLIB_DIRECTORY sane?
              if { ! [ file isdirectory $dir ] } {
                 set msg "::DYNLIB_DIRECTORY variable defined"
                 append msg " in ${apidir}/LDASmpi.rsc "
                 append msg "is not a directory: '$dir'"
                 return -code error $msg
              }
              ;## prepend ::DYNLIB_DIRECTORY and test for
              ;## existence.
              set dsoname $dir$soname
              if { ! [ file exists $dsoname ] } {
                 set msg "file not found: '$dsoname'"
                 return -code error $msg
              } else {
                 set soname $dsoname
              }
           ;## relative path and no ::DYNLIB_DIRECTORY defined.   
           } else {
              set msg    " The variable ::DYNLIB_DIRECTORY has"
              append msg " not been defined in ${apidir}/LDASmpi.rsc,"
              append msg " so I have no way of locating dso's not"
              append msg " specified by absolute paths."
              return -code error $msg
           }
        ;## an absolute path with a leading '/' is always
        ;## taken at face value.
        } elseif { ! [ file exists $soname ] } {
           set msg "dso file not found: '$soname'"
           return -code error $msg
        }
        
        set ::mpi::queue($job,dso) $soname 
     
     } err ] } {
        after 0 mpi::dynLibError $err
        return -code error $err
     }
     return $soname
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::dynLibError
##
## Description:
## Sends email every hour while fatal ::DYNLIB_DIRECTORY
## errors exist.
##
## Parses errors from mpi::findDynlib.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::dynLibError { args } {
     
     if { [ catch {
        set now [ clock seconds ]
        set report [ list ]
        
        set error $args
        
        if { [ info exists ::mpi::dynlib_error ] } {
           set tests $::mpi::dynlib_error
           set ::mpi::dynlib_error [ list ]
           foreach [ list time type ] $tests {
              if { $now - $time <= 3600 } {
                 lappend ::mpi::dynlib_error $time $type   
                 lappend report $type 
              }
           }
        } else {
           set ::mpi::dynlib_error [ list ]
        }
        
        if { [ regexp {not a directory} $error ] } {
           if { [ lsearch $report notdirectory ] < 0 } {
              lappend ::mpi::dynlib_error $now notdireactory
              set subject "$::LDAS_SYSTEM $::API ::DYNLIB_DIRECTORY"
              append subject " is not a directory!"
              set error "Subject: ${subject}; Body: $error"
              addLogEntry $error email
           }
        } elseif { [ regexp {no way of locating} $error ] } {
           if { [ lsearch $report undefined ] < 0 } {
              lappend ::mpi::dynlib_error $now undefined
              set subject "$::LDAS_SYSTEM $::API ::DYNLIB_DIRECTORY"
              append subject " not defined!"
              set error "Subject: ${subject}; Body: $error"
              addLogEntry $error email
           }
        }
        
     } err ] } {
        addLogEntry $err email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: jobstate
##
## Description:
## Socket handler for communicating with wrapperAPI
## this socket persists in an open state for the life
## of the job.
## All communications from the wrapperAPI are formatted
## consistently as described in the wrapper API specification.
## The first token of the message from the wrapper API is
## parsed to determine the job i.d., the communication
## sequential number, and the type of the communication.
## non-error communication types always result in a response
## ordering the continuation of the job.
##
## Parameters:
##
## Usage:
## Typical message showing structure:
##   12345.4:progress 12.00%
##     |   |     |       |
##    job seq  ttype  content
##
## Comments:
##

proc jobstate { port cid } {
     fileevent  $cid readable {}
     set retries $::MPI_WRAPPER_COMMAND_TIMEOUT
     ;## get the request
     set request [ cmd::result $cid $retries ]
    
     if { ! [ string length $request ] } {
        catch { ::close $cid }
        set msg "Null command recieved from wrapper on port $port"
        addLogEntry $msg
        return {}
     }

     if { ! [ regexp {^(\d+)\.(\d+):(\S+)} $request ] } {
        catch { ::close $cid }
        addLogEntry "Garbage recieved on port $port: $request"
        return {}
     }
     
     regexp {^(\d+)\.(\d+):(\S+)} $request -> job seq ttype
     set jobid $::RUNCODE$job
     
     if { [ info exists ::DEBUG_JOBSTATE_MESSAGES ] && \
          [ string equal 1 $::DEBUG_JOBSTATE_MESSAGES ] } {
        addLogEntry "request from wrapper API: $request" blue
     }
     
     foreach { type content } $request {
    
        if { [ regexp -- {(\d+)\.(\d+):(\S+)} $type -> jobid seq ttype ] } {
           set jobid $::RUNCODE$jobid
        }

        ;## sanitize errors from c++ so they do
        ;## not look bizarre to Tcl...
        if { [ llength $content ] == 1 } { 
           set content [ lindex $content 0 ]
        }
        regsub -all {[\{\[]} $content ( content
        regsub -all {[\}\]]} $content ) content
        regsub -all {(\")(\S)} $content {\1 \2} content
        
        set content [ split $content "\n" ]
    
        foreach item $content {
           if { ! [ string length [ join $item ] ] } {
              continue
           }
           lappend tmp [ list $type $item ]
        }
     }
    
     if { [ catch {
        ;## is this the right place for this filter?
        set request [ mpi::extractEmbeddedDsoMail $jobid $tmp ]
        set request [ mpi::parseWrapperRequest $request ]
        set reply   [ mpi::handleRequest $request ]
     } err ] } {
        regexp {\d+} $jobid job
        if { [ llength [ array names ::mpi::queue $job,* ] ] } {
           set ::mpi::queue($job,ero) $err
           set ::mpi::queue($job,end) 1
           set reply [ mpi::order $jobid kill $seq 0 ]
        } else {
           addLogEntry "$err (request: '$request')" blue
        }
     }
     
     if { [ info exists reply ] && [ string length $reply ] } {
        debugPuts "reply to wrapperAPI: $reply"
   
        if { [ regexp {kill} $reply ] } {
           set level 2
        } else {
           set level 0
        }
     
        mpi::wrapperLog "response: '$reply'" $level
        puts $cid $reply
     }
     
     catch { ::close $cid }
}
## ********************************************************

## ******************************************************** 
##
## Name: mpi::dumpFormattedQueue
##
## Description:
## Debugging routine which returns formatted ::mpi::queue
## snapshot in HTML with timestamp.
##
## Parameters:
##
## Usage:
##
## Comments:
## Returns the html formatted text by default.  Writes
## to a file passed as an optional parameter as possible.
##
## Might be kinda neat as a CGI...
##

proc mpi::dumpFormattedQueue { jobid { file "" } } {
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$jobid
     
     set tmp [ list ]
     
     set time [ gpsTime ]
     set t_utc [ utcTime $time ]
     set t_local [ clock format $t_utc -format "%x-%X %Z" ]
     set t_gmt   [ clock format $t_utc -format "%x-%X %Z" -gmt 1 ]
     
     if { [ catch {
        set data [ array get ::mpi::queue ]
        
        foreach { name val } $data {
           lappend tmp [ list $name $val ]
        }
        
        set data $tmp
        
        set data [ lsort -dictionary -index 0 $data ]
        
        set dom $::LDAS_SYSTEM
        
           set tmp "<html>\n"
        append tmp "<head>\n"
        append tmp "<title>::mpi::queue contents at $dom</title>\n"
        append tmp "</head>\n"
        append tmp "<body>\n"
        append tmp "<BODY BGCOLOR='#DDDDDD' TEXT='#000000'>\n"
        append tmp "<h2>LDAS ::mpi::queue at $dom</h2>\n"
        append tmp "<h3><font color='red'>$time</font>\n"
        append tmp "<font color='green'>$t_local</font>\n"
        append tmp "<font color='brown'>$t_gmt</font>\n"
        append tmp "</h3>\n"

        foreach item $data {
           set oldname {}
           foreach { name value } $item {
              set name [ split $name "," ]
              set line "<font color=red><b>[ lindex $name 0 ]</b>"
              append line "&nbsp;"
              append line "<i>[ lindex $name 1 ]</i></font>"
              append line "&nbsp;&nbsp;\{$value\}<br>"
              append line "\n"
              append tmp $line
           }
        }

        append tmp "</body>\n"
        set data $tmp
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     ;## output to file if filename was given, else return
     ;## html formatted text as string.
     if { [ string length $file ] } {
        if { [ catch {
           ;## see docs for publicFile
           publicFile $jobid $file $data
        } err ] } {
           return -code error "[ myName ]: $err"
        }   
     } else {
        return $data
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::watchWrapperLog
##
## Description:
## The mpi API manages the logs for the wrapper API since
## the wrapper API cannot be registered with the manager
## ( due to it's transient nature ).
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::watchWrapperLog { { interval 900 } } {
     
     set code    "if { \[ catch {\n"
     append code "   mpi::manageWrapperLog\n"
     append code "} err \] } {\n"
     append code "   addLogEntry \"mpi::watchWrapperLog: \$err\" red\n"
     append code "}"
     
     bgLoop watchlogs "$code" $interval
     
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::manageWrapperLog
##
## Description:
## Do the actual management as required by
## mpi::watchWrapperLog.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::manageWrapperLog { args } {
     if { [ catch {
        set tail wrapper
        set log $::LDASLOG/LDAS$tail.log.html
        ;## get useful file mod time values
        if { [ file exists $log ] } {
           file stat $log fstat
           set mtime $fstat(mtime)
           set gtime [ gpsTime $mtime ]
           set dt [ expr [ clock seconds ] - $mtime ]
           
           if { [ file size $log ] >= 100000 } {
              ;## the logfile gets rotated and archived.
              set base $::LDASLOG/LDAS$tail
              file delete -force $base.2.html
              if { [ file exists $base.1.html ] } {
                 catch { file rename -force $base.1.html $base.2.html }
              }
              if { [ file exists $base.log.html ] } {
                 catch { file copy -force $base.log.html $base.1.html }
                 set arc [ file join $::LDASARC ${tail}API LDAS${tail}.$gtime ]
                 catch { file rename -force $base.log.html $arc }
                 file delete -force $base.log.html
                 catch { archiveIndex $arc }
              }   
           }
        } ;## if file doesn't exist, we're done   
     } err ] } {
        addLogEntry $err email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::wrapperLog
##
## Description:
## Used to send log entries to the LDASwrapper.log.html
## file.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::wrapperLog { msg level } {
     if { [ catch {
        set jobid  [ uplevel set jobid ]
        set caller [ uplevel myName ]
        set time   {}
        addLogEntry $msg $level $caller $time wrapper
     } err ] } {
        addLogEntry $err email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::pingEventmonAPI 
##
## Description:
## Let eventmon API know that all is still well with the
## current jobid, so don't time out!!
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::pingEventmonAPI { jobid } {
     
     if { [ catch {
        if { ! [ info exists ::__t::Seventmonping ] } {
           ::__t::start eventmonping
           return {}
        }
        if { [ ::__t::mark eventmonping ] > 300 } {
           set sid [ sock::open eventmon emergency ]
           fconfigure $sid -blocking off
           puts $sid \
              "$::MGRKEY set ::${jobid}(updatedTime) [ clock seconds ]"
           ::close $sid
           ::__t::start eventmonping
        }   
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::killAllMpirun 
##
## Description:
## Kill all mpirun procs running on all beowulf nodes.
## This is necessary because mpirun can get into a state
## where it is spinning a tight loop with no lam daemons
## running.  Probably a bug in the lam mpirun which needs
## to be fixed.
## 
## Parameters:
## password - the manager key, known locally only by it's
## one way md5 hash.
##
## Usage:
##
## Comments:
##

proc mpi::killAllMpirun { password jobid { users "" } } {
     set retval [ list ]
     set errs [ list ]
     if { [ catch {
        
        set kill $::MPI_KILL_COMMAND
        
        if { ! [ string length $users ] } {
           set users $::MPIUSERS
           if { [ info exists ::USER_LDAS_HAS_SUDO ] && \
                [ string equal 1 $::USER_LDAS_HAS_SUDO ] } {
              set users ldas
              set kill [ subst -nobackslashes $::MPI_SUDO_KILL_COMMAND ]
           }     
        }
             
        if { [ string equal $::MGRKEY $password ] } {
           set ::mpi::waitforlamboot 1
           ::bootLock ON
           set i [ llength [ concat $::mpi(host) $::NODENAMES ] ]
           set j [ llength $users ]
        
           set N [ expr { $i * $j } ]
           
           set start [ clock clicks -milliseconds ]
           
           set nodes [ lsort -dictionary -unique $::NODENAMES ]
           
           foreach usr $users {
              addLogEntry "cleaning up for user $usr" blue 
              foreach host $nodes {
                 if { $::KILL_ALL_LAMD_AT_RESTART } {
                    ;## remove /tmp/lam-${usr}@$host directory
                    ;## created by lamd.
                    if { [ catch {
                       set retvaln [ mpi::cleanupTmp $usr $host ]
                       if { [ string length $retvaln ] } {
                          lappend retval $retvaln
                       }  
                    } err ] } {
                       if { ! [ regexp {No match} $err ] } {
                          lappend errs $err
                       }
                    }
                    set programs [ list mpirun wrapperAPI lamd ]
                 } else {
                    set programs [ list mpirun wrapperAPI ]
                 }
                 
                 set wrong_user \
                    {lamd\(\d+\):\s+Operation\s+not\s+permitted\s*}

                 set no_kills {no process killed}
                 
                 foreach process $programs {
                    if { [ string equal mpirun $process ] && \
                       ! [ string equal $::WRAPPERHOST $host ] } {
                       continue
                    }   
                    if { [ catch {
                       if { [ string equal ldas $users ] } {
                          set retvaln [ eval ::exec \
                             $::MPI_RSH_COMMAND -n $host \
                             $kill $process ]
                       } else {
                          set retvaln [ eval ::exec \
                             $::MPI_RSH_COMMAND -n -l $usr $host \
                             $kill $process ]
                       }
                       if { [ string length $retvaln ] } {
                          lappend retval \
                             "${usr}@${host}:${process}: $retvaln"
                       }   
                    } err ] } {
                       regsub -all $wrong_user $err {} err
                       if { ! [ regexp $no_kills $err ] } {
                          lappend errs "${usr}@${host}:${process}: $err"
                       }
                    }
                 }
              }
           }
           
           if { [ string equal ldas $users ] } {
              set users $::MPIUSERS
           }
            
           set end [ clock clicks -milliseconds ]
           set t [ format "%0.3f" [ expr { ($end - $start)/1000.0 } ] ]
           addLogEntry "ran kill $N times in $t seconds" blue
           
           mpi::prestartLamds $jobid $users
          
        } else {
           set retval "called with incorrect password: $password"
        }
     } err ] } {
        set ::mpi::waitforlamboot 0
        ::bootLock OFF
        return -code error "[ myName ]: $err"
     }
     if { [ string length $errs ] } {
        addLogEntry $errs red
     }
     if { [ info exists retval ] && [ string length $retval ] } {
        addLogEntry $retval blue
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::cleanupTmp
##
## Description:
## Remove the tmp directory with the sockets/pipes used
## by lamd.
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::cleanupTmp { usr host } {
     
     if { [ catch {
        set retval [ eval ::exec \
            $::MPI_RSH_COMMAND -n -l $usr $host \
            /bin/rm -rf /tmp/lam-${usr}@* ]
        if { [ string length $retval ] } {
           set retval \
           "${usr}@${host}: $retval"
        }
     } err ] } {
        return -code error \
        "[ myName ]: ${usr}@${host}: $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::abortIfHung
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::abortIfHung { jobid } { 
     if { [ catch {
        set ::mpi::waitforlamboot 1
        regexp {\d+} $jobid job
        if { [ info exists ::mpi::queue($job,run) ] } {
           set state [ set ::mpi::queue($job,run) ] 
           if { [ string equal stalled $state ] } {
              set err "Job stalled. possible lam error, see logs."
              append err " Wrapper API failed to report initialisation."
              set ::mpi::queue($job,ero) $err
           } else {
              set err "Job stalled in wrapper API (no error reported)."
              append err " Possible communication error when trying "
              append err "to send data to eventmon API."
              set ::mpi::queue($job,ero) $err
           }
        }
     } err ] } {
        set ::mpi::waitforlamboot 0
        addLogEntry "$err (see mpi.log file!)" email
     }
     after 0 mpi::newJobCallback $jobid
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::resetHungJobTimeout 
##
## Description:
## Resets wrapper hang timeout to the LONG value defined
## by ::HUNG_WRAPPER_TIMEOUT after successful initialisation
## within the time defined by ::WRAPPER_INIT_TIMEOUT.
##
## Parameters:
##
## Usage:
##
## Comments:
## This addresses PR# 1884.

proc mpi::resetHungJobTimeout { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set to [ expr { $::HUNG_WRAPPER_TIMEOUT * 1000 } ]
        if { [ info exists ::mpi::queue($job,aft) ] } {
           after cancel [ set ::mpi::queue($job,aft) ]
           unset ::mpi::queue($job,aft)
        }
        if { [ llength [ array names ::mpi::queue $job,* ] ] } {
           set ::mpi::queue($job,aft) \
              [ after $to mpi::abortIfHung $jobid ]
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::talkToEventmon
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::talkToEventmon { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job

     set singleframe [ set ::${jobid}(-setsingleem) ]
     
     if { $singleframe } {
        if { [ catch {
           set sid [ ::sock::open eventmon emergency ]
           set cmd "$::MGRKEY \"setSingleFrame $job 1;"
           append cmd "catch \{set $jobid\}\""
           puts $sid $cmd
           ::close $sid 
        } err ] } {
           catch { ::close $sid }
           set msg "unable to set single frame in eventmon: '$err'"
           append msg " will default to multi-frame"
           addLogEntry $msg red
        }
     }      

     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::liveNodelistUpdate
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::liveNodelistUpdate { jobid action { nodes 0 } } {
     
     if { [ catch {
        if { [ llength $nodes ] == 1 } {
           set nodes [ lindex $nodes 0 ]
        }
        if { ! [ string length $nodes ] } {
           set nodes 0
        }
        set updated [ mpi::updateRscFile $action $nodes ]
        if { $updated } {
           mpi::initNodes
           ;## PR1634
           mpi::prestartLamds $jobid
           ;## PR2123
           mpi::updateCmonNodelist
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::updateRscFile
##
## Description:
##
## Parameters:
## action - add, remove, undo, and update
## nodes - list of nodes for add/remove
##
## Usage:
##
## Comments:
## The wrapper master is protected from removal from the
## zeroth position in the ::NODENAMES list.
##

proc mpi::updateRscFile { action nodes } {
     
     if { [ catch {
        
        set master [ lindex $::NODENAMES 0 ]
        
        if { ! [ file exists LDASmpi.rsc ] } {
           file copy ${::LDAS}/lib/mpiAPI/LDASmpi.rsc LDASmpi.rsc
        }
        
        switch -exact $action {
               add {
                    if { [ string equal 0 $nodes ] } {
                       set msg "no nodes specified for adding!"
                       return -code error $msg
                    }
                    bak LDASmpi.rsc
                    set fid1 [ open LDASmpi.rsc.bak r ]
                    set fid2 [ open LDASmpi.rsc w ]
                    while { ! [ eof $fid1 ] } {
                       gets $fid1 line
                       if { [ regexp {^set :*NODENAMES} $line ] } {
                          foreach node $nodes {
                             regsub {\]} $line "$node ]" line
                          }
                          set nodeline $line
                       }
                       puts $fid2 $line
                    }
                    ::close $fid1
                    ::close $fid2
                   }
            remove {
                    set nodeline \
                       [ mpi::removeNodesFromRscFile $master $nodes ]
                   }
              undo {
                    if { ! [ string equal 0 $nodes ] } {
                       set msg "error: nodes specified with"
                       append msg " 'undo': '$nodes'."
                       return -code error $msg
                    }
                    if { [ file exists LDASmpi.rsc.bak ] } {
                       file delete -force LDASmpi.rsc
                       file rename -force LDASmpi.rsc.bak LDASmpi.rsc
                       set fid [ open LDASmpi.rsc r ]
                       while { ! [ eof $fid ] } {
                          gets $fid line
                          if { [ regexp {^set :*NODENAMES} $line ] } {
                             set nodeline $line
                             break
                          }
                       }
                       ::close $fid
                    } else {
                       set msg "cannot undo, no file LDASmpi.rsc.bak"
                       return -code error $msg
                    }
                   }
            update {
                    if { ! [ string equal 0 $nodes ] } {
                       set msg "error: nodes specified with"
                       append msg " 'undo': '$nodes'."
                       return -code error $msg
                    }
                    if { ! [ file exists LDASmpi.rsc ] } {
                       set msg "file not found: LDASmpi.rsc"
                       return -code error $msg
                    }
                    set fid [ open LDASmpi.rsc r ]
                    while { ! [ eof $fid ] } {
                       gets $fid line
                       if { [ regexp {^set :*NODENAMES} $line ] } {
                          set nodeline $line
                          break
                       }
                    }
                    ::close $fid
                   }
           default {
                    set msg "unsupported action: '$action'"
                    return -code error $msg
                   }
        }
        
        if { [ info exists nodeline ] } {
           regsub {:*NODENAMES} $nodeline ::NODENAMES nodeline
           eval $nodeline
           addLogEntry "::NODENAMES updated!! ($::NODENAMES)" blue
           set updated 1
        } else {
           set updated 0
        }
        
     } err ] } {
        catch { ::close $fid  }
        catch { ::close $fid1 }
        catch { ::close $fid2 }
        return -code error "[ myName ]: $err"
     }
     return $updated
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::removeNodesFromRscFile
##
## Description:
## Remove a physical node entry or a virtual node entry
## from the LDASmpi.rsc file, and report accurately what
## has been done.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::removeNodesFromRscFile { master nodes } {
     
     if { [ catch {
         
        set nodeline [ list ]
        
        if { [ string equal 0 $nodes ] } {
           set msg "no nodes specified for removal!"
           return -code error $msg
        }
        bak LDASmpi.rsc
        set fid1 [ open LDASmpi.rsc.bak r ]
        set fid2 [ open LDASmpi.rsc w ]
        while { ! [ eof $fid1 ] } {
              gets $fid1 line
              if { [ regexp {^set :*NODENAMES} $line ] } {
                 foreach node $nodes {
                    if { [ regexp {^n\d+$} $node ] } {
                       ;## node is virtual, get physical node
                       set node [ mpi::virtual2physical $node ]
                       set switch --
                    } else {
                       ;## node is physical, wipe it!
                       set switch -all
                    }
                    regsub $switch "$node " $line {} line
                    if { [ string equal $master $node ] } {
                       regsub {list} $line "list $node" line
                    }
                 }
                 set nodeline $line
              }
              puts $fid2 $line
           }
           ::close $fid1
           ::close $fid2

     } err ] } {
        catch { ::close $fid1 }
        catch { ::close $fid2 }
        return -code error "[ myName ]: $err"
     }
     return $nodeline
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::prestartLamds
##
## Description:
## 0.2 seconds required for EACH of i*j iterations
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::prestartLamds { jobid { users "" } } {
     
     if { [ catch {
        
        set ::mpi::waitforlamboot 1
        
        if { [ regexp {\d+} $jobid job ] } {
           set jobid $::RUNCODE$job
        }

        if { ! [ string length $users ] } {
           set users $::MPIUSERS
        }
           
        ;## write the bootschema with the LAM universe
        set bschema [ pwd ]/bootschema.lam
        set fid [ open $bschema w 0644 ]
        puts $fid [ string trim [ join $::NODENAMES "\n" ] ]
        ::close $fid
         
        set host $::WRAPPERHOST
         
        foreach user $users {
           addLogEntry "running lamboot for user $user" blue
           lam::boot $jobid $user $host
        }
        
        foreach user $users {
           if { ! [ info exists ::mpi::queue($user,lamstate) ] } {
              vwait ::mpi::queue($user,lamstate)
           }
           set state $::mpi::queue($user,lamstate)
           
           if { [ regexp {(ERROR\!\!|ERROR\:)} $state ] } {
              lappend errors $state
           }
           addLogEntry $state blue
        }
        
        if { [ info exists errors ] } {
           return -code error $errors
        }
        
     } err ] } {
        if { [ llength $users ] == 1 } {
           after 0 mpi::autoRemoveNode $err
        }
        set subject "$::LDAS_SYSTEM $::API lamboot error!"
        addLogEntry "Subject: ${subject}; Body: $err" email
     }
     set ::mpi::waitforlamboot 0
     ::bootLock OFF
}
## ********************************************************

## ******************************************************** 
##
## Name: mpi::autoRemoveNode
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::autoRemoveNode { args } {
     
     if { [ catch {
        set node_crash_rx {(No route to host|protocol failure)}
        
        if { [ info exists ::NODE_AUTO_REMOVE_REGEXP ] } {
           if { [ catch {
              regexp -- $::NODE_AUTO_REMOVE_REGEXP testing_regexp_now
           } err ] } {
              set err $::errorInfo
              set subject "$::LDAS_SYSTEM $::API [ myName ] error!"
              set msg "error parsing ::NODE_AUTO_REMOVE_REGEXP pattern:\n"
              append msg "'$::NODE_AUTO_REMOVE_REGEXP'\n"
              append msg "($err)\n"
              append msg "Using default value:\n$node_crash_rx\n"
              append msg "instead!"
              addLogEntry "Subject: ${subject}; Body: $msg" email
           }
           if { [ string length $::NODE_AUTO_REMOVE_REGEXP ] < 15 } {
              set err "value of ::NODE_AUTO_REMOVE_REGEXP seems "
              append err "unusually short:\n"
              append err "'$::NODE_AUTO_REMOVE_REGEXP'\n"
              append err "Using default value:\n$node_crash_rx\n"
              append err "until specified pattern is at least 15 "
              append err "chars long!"
              set subject "$::LDAS_SYSTEM $::API [ myName ] error!"
              addLogEntry "Subject: ${subject}; Body: $err" email
              set ::NODE_AUTO_REMOVE_REGEXP $node_crash_rx
           }
           set node_crash_rx $::NODE_AUTO_REMOVE_REGEXP
        }
        
        set node_rx {remote node \"([^\"]+)}
        
        set msg $args
        
        ;## only auto remove if ::AUTO_REMOVE_BEOWULF_NODES_ON_ERROR
        ;## is defined and set to "1"
        if { [ info exists ::AUTO_REMOVE_BEOWULF_NODES_ON_ERROR ] } {
           if { [ string equal 1 $::AUTO_REMOVE_BEOWULF_NODES_ON_ERROR ] } {
              set auto_remove 1
           } else {
              set auto_remove 0
           }
        } else {
           set auto_remove 0
        }
        
        if { ! $auto_remove } { return {} }
        
        ;## node has become unreachable!!
        if { [ regexp -nocase $node_crash_rx $msg ] } {
           ;## might be able to automagically remove the node
           if { [ regexp -nocase $node_rx $msg -> node ] } {
              if { [ catch {
                 after 0 \
                    [ list mpi::liveNodelistUpdate ERROR remove $node ]
                 set sys $::LDAS_SYSTEM
                 set body    "LDAS mpi API at $sys has automatically\n"
                 append body "removed node '$node' from LDASmpi.rsc\n"
                 append body "and from active node list in currently\n"
                 append body "running mpi/wrapper API's because it\n"
                 append body "appears to have become unusable."
                 set subject "$sys Beowulf node '$node' deactivated!"
                 set msg "Subject: ${subject}; Body: $body" 
                 addLogEntry $msg email
              } err ] } {
                 addLogEntry $err email
              }
           }
        }
        
     } err ] } {
        if { [ string length $err ] } {
           set subject "$::LDAS_SYSTEM $::API [ myName ] error!"
           addLogEntry "Subject: ${subject}; Body: $err" email
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::reportWrapperRuntime
##
## Description:
## Connect to manager API and cause wrapper API timing
## value to be populated.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::reportWrapperRuntime { jobid args  } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid  $::RUNCODE$job
        
        if { [ info exists ::mpi::queue($job,nod) ] } {
           set N   $::mpi::queue($job,nod)
           set N [ llength $N ]
        } else {
           set N 0
        }
 
        if { [ regexp {totalTime=(\S+)} $args -> tT ] } {
           set tT [ format %.3f $tT ]

           mpi::dropBeowulfReport $jobid $tT $N
           
           set sid [ sock::open manager emergency ]
           puts $sid \
              "$::MGRKEY NULL NULL mgr::oneLineJobTimingReport $jobid wr $tT"
           close $sid
        }
        
     } err ] } {
        catch { ::close $sid }
        addLogEntry $err red
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: mpi::killWrapperMaster
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::killWrapperMaster { jobid } {
     if { [ catch {
        regexp {\d+} $jobid job
        
        if { [ info exists ::mpi::queue($job,mas) ] } {
           set usr    $::mpi::queue($job,usr)
           set master $::mpi::queue($job,mas)
           set master [ mpi::virtual2physical $master ]
           catch { ::eval ::exec \
              $::MPI_RSH_COMMAND -n -l $usr $master \
              $::MPI_KILL_COMMAND wrapperAPI ]
           }
        }
        
     } err ] } {
        addLogEntry $err red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::virtual2physical
##
## Description:
## Given a virtual node name (i.e. n2) return the physical
## node that it maps to.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::virtual2physical { virtual } {
     
     if { [ catch {
        foreach node $::mpi::queue(NULL,nod) {
           foreach [ list name node score ] $node { break }
           if { [ string equal $node $virtual ] } {
              set physical $name
              break
           }
        }
        if { ! [ info exists physical ] } {
           set err "virtual node '$virtual' does not map to "
           append err "any physical node!"
           return -code error $err
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $physical
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::physical2virtual
##
## Description:
## Given a physical node name (i.e. node12) return a list
## of all virtual nodes that exist for it.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::physical2virtual { physical } {
     
     if { [ catch {
        set virtual [ list ]
        
        foreach node $::mpi::queue(NULL,nod) {
           foreach [ list name node score ] $node { break }
           if { [ string equal $name $physical ] } {
              lappend virtual $node
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $virtual
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::updateCmonNodelist
##
## Description:
## Sends the current virtual node assignment state to the
## cntlmonAPI server via it's emergency port whenever the
## virtual node list changes.
##
## UPDATE:
## this was apparently not making the cntlmonAPI happy,
## so it was changed to just drop a file into the mpiAPI
## 'home' directory.
##
## UPDATE AGAIN:
## kitchen sink model... we now do everything...
##
## Parameters:
##
## Usage:
##
## Comments:
## PR #2123

proc mpi::updateCmonNodelist { args } {
     
     ;## push to the cntlmon API's emergency port
     if { [ catch {
        set sid [ ::sock::open cntlmon emergency ]
        #set cmd [ list $::MGRKEY set ::beowulfNodes $map ]
        set cmd [ list $::MGRKEY set ::beowulfNodes $::NODENAMES ]
        puts $sid $cmd
        ::close $sid
        set msg "updated ::beowulfNodes in cntlmonAPI to '$::NODENAMES'"
        addLogEntry $msg blue
     } err ] } {
        catch { ::close $sid }
        set msg "unable to connect to cntlmonAPI emergency port: "
        append msg $err
        addLogEntry $msg red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::dropBeowulfReport 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::dropBeowulfReport { jobid wrt N } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set dso $::mpi::queue($job,dso)
        set stt $::mpi::queue($job,stt)
        set stt [ lindex $stt 1 ]
        set uname $::mpi::queue($job,unm)
        ;## write data point to file, archive as necessary
        set megabyte [ expr { 1024 * 1024 } ]
        set fname $::LDASLOG/beowulf.report
        set arc   $::LDASARC/beowulf/beowulf.report.\$now
        if { [ file exists $fname ] && \
             [ file size $fname   ] > $megabyte } {
           if { ! [ file exists [ file dirname $arc ] ] } {
              file mkdir [ file dirname $arc ]
           }
           set now [ gpsTime ]
           set arc [ subst -nocommands $arc ]
           file rename -force $fname $arc
        }
        set fid [ open $fname a+ ]
        puts $fid "$stt $N $wrt $uname $dso" 
        ::close $fid

     } err ] } {
        addLogEntry $err orange
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mpi::usrNodeStats
##
## Description:
## Called immediately after a job completes without an
## error with the update flag set to '1' to recored the
## last time the user/node combination was used
## successfully.
##
## Called when a failure occurs on a node with the update
## flag set to '0' to find out how long ago the last
## successful use of the user/node combination was.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mpi::usrNodeStats { jobid user node { update 0 } } {
     
     if { [ catch {
        set elapsed 0
        if { ! [ array exists ::usernodestats ] } {
           set users $::MPIUSERS
           set nodes $::NODENAMES
           foreach usr $users {
              foreach nod $nodes {
                 set ::usernodestats($usr,$nod,time) 0
              }
           }
        }
        
        set now [ clock seconds ]
        
        if { [ info exists ::usernodestats($user,$node,time) ] } {
           set last [ set ::usernodestats($user,$node,time) ]
           if { $update } {
              if { $last } {
                 set elapsed [ expr { $now - $last } ]
              }
              set ::usernodestats($user,$node,time) $now
           } else {
              if { $last } {
                 set elapsed [ expr { $now - $last } ]
              }
           }
        # time entry does not exist yet for this user/node combo   
        # so we initialise it to 'now'.
        } elseif { $update } {
           set ::usernodestats($user,$node,time) $now
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $elapsed
}
## ******************************************************** 

