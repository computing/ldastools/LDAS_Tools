## ********************************************************
## Name: LDASmpi.rsc
##
## This is the mpi API specific resource file.  It contains
## resource information which is only used by the mpi API.
##
## This file should be located and named as follows:
##
##    @prefix@/lib/mpiAPI/LDASmpi.rsc
##
## If this file is not found there the mpi API and hence the
## wrapper API will be non-functional.
##
## Note: on a standalone LDAS system running on a Solaris
## system, the ::MPI_KILL_COMMAND will be:
##
##          "/usr/bin/pkill -KILL"
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
## ********************************************************
;#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES 

;## desc=MPI can be lam or mpich (mpich deprecated).
set ::MPI lam

;## desc=use this rsh base command (might need to use ssh).
set ::MPI_RSH_COMMAND /usr/bin/rsh

;## desc=path to lsof executable
set ::PATH_TO_LSOF /usr/sbin/lsof

;## desc=lam processes can be killed en-masse if user ldas has sudo
set ::USER_LDAS_HAS_SUDO 1

;## desc=use this rsh base command (might need to use ssh).
set ::MPI_RSH_COMMAND /usr/bin/rsh

;## desc=the LAMHOME env var points to the root of the lam installation. 
set ::LAMHOME /usr

;## desc=globbing style kill command for this platform.
set ::MPI_KILL_COMMAND "/usr/bin/killall -KILL -v"

;## desc=globbing style sudo kill command for this platform.
set ::MPI_SUDO_KILL_COMMAND "sudo pkill -9 -u \[ join \$::MPIUSERS , \]"

;## desc=command mpirun issue to start wrapper
set ::WRAPPER_API_CMD "\$::LDAS/bin/wrapperAPI"

;## desc=additional flags besides -O and -nw for mpirun, like -v
set ::MPIRUN_EXTRA_FLAGS "-ger -v"

;## desc=if set to 1, wrapper masters will be on random nodes.
set ::DISTRIBUTE_WRAPPER_MASTERS 1

;## desc=if using distriuted wrapper masters, how many on gateway?
set ::N_WRAPPER_MASTERS_ON_GATEWAY 8

;## desc=if using distriuted wrapper masters, how many on nodes (UNIMPLEMENTED)?
set ::N_WRAPPER_MASTERS_ON_INTERNAL 4

;## desc=flags to use when calling lamboot
set ::LAM_BOOT_FLAGS "-b -v -s -H"

;## desc=whether to run recon after lamboot. usually we DO (1 or 0).
set ::RUN_RECON_AFTER_LAMBOOT 1

;## desc=number of iteration to 'gets' on a wrapper/mpirun stdio message.
set ::LAM_SHELL_LOG_INFINITE_LOOP 1000

;## options mpirun send as -x arguments
set ::MPIRUN_DASHX_OPTIONS {}

;## desc=node names for LAM ! mod=no
set ::NODENAMES [ list node0 node1 node2 node3 node4 node5 node6 node7 node8 ]

# run multiple communicators on a one node machine,
# or allow multiple jobs to run on common nodes as
# required to have all jobs run.
# Performance benchmarking can only be done when
# MPI_MULTIPLE_NODES is FALSE (0).
;## desc=multiple jobs on common nodes etc ! mod=no
set ::MPI_MULTIPLE_NODES 1

# if ::MPI_MULTIPLE_NODES is true (1), then ::MPI_NODE_SHARE_LIMIT
# is the maximum number of jobs that can share the resources of a
# single virtual node.
;## desc=if nodes can be used by more than one job (see LDASmpi.rsc)
set ::MPI_NODE_SHARE_LIMIT 2

;## desc=default maximum fractional memory per node per job ! mod=no
set ::MEMORY_USAGE_LIMIT 1.0

;## desc=number of seconds to queue a job waiting for nodes (0 disables queueing)
set ::MPI_QUEUE_TIMEOUT 60

;## desc=list of users for multiple lam jobs ! mod=no
set ::MPIUSERS [ list search01 search02 search03 search04 search05 search06 search07 search08 search09 search10 search11 search12 search13 search14 search15 search16 ]

;## desc=seconds to allow for wrapper to return init message
set ::MPI_WRAPPER_INIT_TIMEOUT 10

;## desc=seconds to allow for wrapper to reply to a command
set ::MPI_WRAPPER_COMMAND_TIMEOUT 200

;## desc=timeout (in seconds) for wrapper progress report updates
set ::HUNG_WRAPPER_TIMEOUT 360

;## desc=seconds after which a communicator is deemed dead
set ::NODE_COMM_TIMEOUT 600

;## desc=whether to do a clean sweep of lamd's at startup (EXPENSIVE).
set ::KILL_ALL_LAMD_AT_RESTART 1

;## desc=whether to automatically remove nodes that fall off the network
set ::AUTO_REMOVE_BEOWULF_NODES_ON_ERROR 0

;## desc=regexp pattern used by ::AUTO_REMOVE_BEOWULF_NODES_ON_ERROR
set ::NODE_AUTO_REMOVE_REGEXP {(No\s+route\s+to\s+host|protocol\s+failure)}

;## desc=do not log wrapper 'using' messages
set ::DO_NOT_LOG_USING 1

;## desc=do not log wrapper 'progress' messages
set ::DO_NOT_LOG_PROGRESS 1

;## desc=do not log wrapper 'projected_ratio' messages
set ::DO_NOT_LOG_RATIO 1

# desc=for mpich
set ::MACHINEFILE [ file join foo bar machines ]

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default core default cputime default datasize default \
filesize default memorylocked default descriptors default maxproc default ]
