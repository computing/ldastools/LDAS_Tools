#ifndef FrameApiDetectorHH
#define FrameApiDetectorHH

#include <deque>
#include <string>

#include "framecpp/FrDetector.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

const std::string detectorProcName( "detectProc" );   
const std::string detectorSimName( "detectSim" );      
   
   
std::string getAttribute( const FrameCPP::FrDetector& detector,
			  std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrDetector& detector,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index );

FrameCPP::FrDetector* container2detector( const ILwd::LdasContainer& c );

ILwd::LdasContainer*
detector2container( const FrameCPP::FrDetector& sum,
		    const std::string& ilwd_name = "" );

void insertDetector( ILwd::LdasContainer& fr, ILwd::LdasContainer& d );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiDetectorHH
