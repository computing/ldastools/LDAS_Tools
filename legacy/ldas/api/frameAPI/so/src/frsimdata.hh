#ifndef FrameApiSimDataHH
#define FrameApiSimDataHH

#include <deque>
#include <string>

#include "framecpp/FrSimData.hh"
#include "framecpp/Time.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrSimData& simData,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrSimData& simData,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime,
	 const REAL_8& dt );

FrameCPP::FrSimData* container2simData( const ILwd::LdasContainer& c );

ILwd::LdasContainer* simData2container( const FrameCPP::FrSimData& sim,
                                        const FrameCPP::Time& gtime,
                                        const REAL_8& dt );

void
insertSimData( ILwd::LdasContainer& fr,
	       ILwd::LdasContainer& sim,
	       const bool validateTime = true );

//!exc: SwigException      
ILwd::LdasContainer*
concatSimData( const ILwd::LdasContainer* c1,
	       const ILwd::LdasContainer* c2 );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiSimDataHH
