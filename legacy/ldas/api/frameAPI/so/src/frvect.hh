#ifndef FrameApiVectHH
#define FrameApiVectHH

#include <algorithm>
#include <deque>
#include <memory>

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#endif /*  HAVE_LDAS_PACKAGE_ILWD */

#include "framecpp/Common/Container.hh"
#include "framecpp/FrVect.hh"

#include "genericAPI/swigexception.hh"

#include "query.hh"
//!ignore_begin:

namespace FrameAPI
{
  namespace FrVect
  {
    typedef FrameCPP::Common::Container< FrameCPP::FrVect > Container;
    typedef std::list< const Container* >
    FrVectList;
    
    void appendStructures( Container& Dest, const Container& Source );

    FrameCPP::FrVect* concat( const FrVectList& List );

    void copy( void* Dest, const Container& Source );

    void* createVector( INT_2U Type, INT_4U Size );

    INT_4U getSamples( Container::const_iterator Start,
 		       Container::const_iterator Stop );
  } // namespace - FrVect
} // namespace - FrameAPI

std::string getAttribute( const FrameCPP::FrVect& vect, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index );
#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasArrayBase* getData( const FrameCPP::FrVect& vect,
			      std::deque< Query >& dq,
                              std::vector< size_t >& start, size_t index );

FrameCPP::FrVect* array2vect( const ILwd::LdasArrayBase& ab );
ILwd::LdasArrayBase* vect2array( const FrameCPP::FrVect& v );

//!exc: SwigException   
//!exc: std::bad_alloc   
ILwd::LdasArrayBase* concatElementData( const ILwd::LdasContainer* c1,
					const ILwd::LdasContainer* c2 );
//!ignore_end:

//----------------------------------------------------------------------
//
//: Concatenate two vectors. (Heavy weight copy).
//
//!usage_ooi: res = concatVect( d1, d2 );
//
//!param: const ILwd::LdasArrayBase& d1 - first vector.
//!param: const ILwd::LdasArrayBase& d2 - second vector.
//
//!return: ILwd::LdasArray&lt; T >* ret - concatenaed vector container.
//
//!exc: SwigException   
//!exc: std::bad_alloc   
//!exc: LdasException   
//!todo: parameters should be ILwd::LdasArray< T >&
//----------------------------------------------------------------------
template< class T >
ILwd::LdasArray< T >* concatVect(
   const ILwd::LdasArrayBase& d1, const ILwd::LdasArrayBase& d2 )
{
  //-------------------------------------------------------------------
  // We already know that the arrays are the right type.
  //-------------------------------------------------------------------
  const INT_4U dim_pos( 0 );
  const ILwd::LdasArray< T >& base_1
    (dynamic_cast< const ILwd::LdasArray< T >& >( d1 ) );
  const ILwd::LdasArray< T >& base_2
    (dynamic_cast< const ILwd::LdasArray< T >& >( d2 ) );
  const INT_4U dims_1( d1.getDimension( dim_pos ) );
  const INT_4U dims_2 ( d2.getDimension( dim_pos ) );
  const INT_4U dims( dims_1 + dims_2 );

  //-------------------------------------------------------------------
  // Allocate space for the array
  //-------------------------------------------------------------------
  std::unique_ptr<ILwd::LdasArray< T > > array
    ( new ILwd::LdasArray< T >
      ( new T[ dims ], dims, 
	d1.getNameString(),
	d1.getUnits( dim_pos ),
	false /* Don't allocate */, true /* own the pointer */,
	d1.getDx( dim_pos ),
	d1.getStartX( dim_pos ),
	d1.getDataValueUnit() ) );
  //-------------------------------------------------------------------
  // Copy data into the array.
  //-------------------------------------------------------------------
  std::copy( base_1.getData(),
	     base_1.getData() + dims_1,
	     array->getData() );
  std::copy( base_2.getData(),
	     base_2.getData() + dims_2,
	     array->getData() +  dims_1 );
  
  std::string comment1( d1.getComment() );
  std::string comment2( d2.getComment() );
  if( comment1.empty() == false || comment2.empty() == false )
  {
    // First comment is not empty
    if( comment1.empty() == false )
    {
      if( comment2.empty() == false )
      {
	comment1 += ':';
      }
    }
    comment1 += comment2;
    array->setComment( comment1 );
  }
  
  return array.release( );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#endif // FrameApiVectHH
