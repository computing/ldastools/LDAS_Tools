/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "ldas_tools_config.h"

// System Header Files
#include <cmath>
#include <sstream>   

// Local Header Files
#include "frserdata.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "util.hh"


using FrameCPP::FrSerData;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

#if ! CORE_API
using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
#endif /* ! CORE_API */
   
using namespace std;   
   
//!ignore_begin:


enum
{
    FR_NAME,
    FR_TIMESEC,
    FR_TIMENSEC,
    FR_SAMPLERATE,
    FR_DATA,
    FR_SERIAL,
    FR_TABLE
};


QueryHash initSerDataHash()
{
    QueryHash h;
    h[ "name"       ] = FR_NAME;
    h[ "timesec"    ] = FR_TIMESEC;
    h[ "timensec"   ] = FR_TIMENSEC;
    h[ "samplerate" ] = FR_SAMPLERATE;
    h[ "data"       ] = FR_DATA;
    h[ "serial"     ] = FR_SERIAL;
    h[ "table"      ] = FR_TABLE;
    return h;
}


static QueryHash serDataHash( initSerDataHash() );    
    
    
//-----------------------------------------------------------------------------
#if CORE_API
std::string getAttribute( const FrSerData& serData, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        serDataHash.find( q.getName().c_str() );
    if ( iter == serDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << serData.GetName();
                break;
                
            case FR_TIMESEC:
                res << serData.GetTime().getSec();
                break;
                
            case FR_TIMENSEC:
                res << serData.GetTime().getNSec();
                break;
                
            case FR_SAMPLERATE:
                res << serData.GetSampleRate();
                break;

            case FR_DATA:
                res << serData.GetData();
                break;

            case FR_SERIAL:
                return getAttribute( serData.RefSerial(), dq );
                
            case FR_TABLE:
	        return getAttribute( serData.RefTable(), dq );

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_SERIAL:
                return getAttribute(
				    getContained( serData.RefSerial(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;
                
            case FR_TABLE:
                return getAttribute(
				    getContained( serData.RefTable(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}
#endif /* CORE_API */


//-----------------------------------------------------------------------------
#if ! CORE_API
LdasElement* getData( const FrSerData& serData, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime, const REAL_8& dt )
{
    if ( dq.size() == 0 )
    {
        return serData2container( serData, gtime, dt );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        serDataHash.find( q.getName().c_str() );
    if ( iter == serDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_SERIAL:
            return getData(
			   getContained( serData.RefSerial(), q, start, index ),
                dq, start, index + 1 );
            break;
                
        case FR_TABLE:
            return getData(
			   getContained( serData.RefTable(), q, start, index ),
                dq, start, index + 1 );
            break;

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}
#endif /* ! CORE_API */

//!ignore_end:

//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrSerData.
//
//!usage_ooi: FrSerData* serData = container2serData( c );
//
//!param: const LdasContainer& c  - serData container.
//
//!return: FrSerData* serData - FrameCPP::FrSerData newly allocated.
//
//!exc: SwigException - invalid container format.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
FrSerData* container2serData( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
    if ( strcasecmp( c_name.c_str(), "SerData" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrSerData container." );
    }
    
    const LdasArray< INT_4U >& te = findArrayType< INT_4U >( c, "time" );
    FrSerData* ser = new FrSerData( c.getName( 0 ),
				FrameCPP::Time( te.getData()[ 0 ], te.getData()[ 1 ] ),
				*findArrayType< REAL_4 >( c, "sampleRate" ).getData() );
    ser->SetData( findLStringType( c, "data" ).getString() );
    
    try
    {
        const LdasContainer* serial( findContainerType( c, "serial" ) );
        if ( serial != 0 )
        {
            for ( unsigned int i = 0; i < serial->size(); ++i )
            {
                FrVect* v = array2vect(
                    dynamic_cast< const LdasArrayBase& >( *(*serial)[ i ] ) );
                ser->RefSerial().append( *v );
                delete v;
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete ser;
      throw SWIGEXCEPTION( "invalid_input_format" );
    }
    
    try
    {
        const LdasContainer* table( findContainerType( c, "table" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
                FrTable* t = container2table(
                    dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) );
                ser->RefTable().append( *t );
                delete t;
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete ser;
      throw SWIGEXCEPTION( "invalid_data_format" );
    }

    return ser;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert FrSerData to ILWD.
//
//!usage_ooi: LdasContainer* serDataCotnainer = serData2container( ser, gtime, dt, c);
//
//!param: const FrSerData& ser - FrameCPP::FrSerData object to convert.
//!param: const Time& gtime - FrameCPP::Frame time.
//!param: const REAL_8& dt - FrameCPP::Frame delta time.
//!param: LdasContainer* c - optional result container.
//
//!return: LdasContainer* serDataCotnainer - serial data container pointer
//+	(newly allocated or `c').
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* serData2container( const FrSerData& ser, const Time& gtime,
                                  const REAL_8& dt, LdasContainer* c )
{
    if ( c == NULL )
      {
	c = new LdasContainer( "::SerData" );
      }
    c->setName( 0,  ser.GetName() );

    ostringstream ss;
    ss.precision( REAL_8_DIGITS + 1 );
    ss << gtime.getSec();
    c->appendName( ss.str() );

    ss.str( "" );
    ss << gtime.getNSec();
    c->appendName( ss.str() );

    c->appendName( "Frame" ); 

    INT_4U time[ 2 ] = { ser.GetTime().getSec(),
                         ser.GetTime().getNSec() };
    LdasArray< INT_4U >* e_time( new LdasArray< INT_4U >( time, 2, "time" ) );
    LdasArray< REAL_4 >* sampleRate(
        new LdasArray< REAL_4 >( ser.GetSampleRate(), "sampleRate" ) );
    LdasString* data( new LdasString( ser.GetData(), "data" ) );
    LdasArray< REAL_8 >* e_dt( new LdasArray< REAL_8 >( dt, "dt" ) );

    c->push_back( e_time,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( sampleRate,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( data,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( e_dt,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    size_t dataSize( ser.RefSerial().size() );
    if ( dataSize > 0 )
    {
        LdasContainer* serial(
            new LdasContainer( ":serial:Container(SerData):Frame" ) );
        c->push_back( serial,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrSerData::const_iterator iter( ser.RefSerial().begin() );
        for( size_t i = dataSize; i != 0; --i, ++iter )
        {
            serial->push_back( vect2array( **iter ),
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::OWN );
        }
    }

    size_t tableSize( ser.RefTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer* table(
            new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrSerData::const_table_iterator iter( ser.RefTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Insert Serial Data container into Frame container.
//
//!usage_ooi: insertSerData( fr, ser );
//
//!param: LdasContainer& fr - frame container.
//!param: LdasContainer& ser - serial data container.
//!param: const bool validateTime - A flag to enable/disable time stamp
//+       validation for data insertion. True to enable, false to disable.       
//+       Default is TRUE.      
//
//!exc: SwigException - invalid container data.
//-----------------------------------------------------------------------------
#if ! CORE_API
void insertSerData( LdasContainer& fr, LdasContainer& ser,
   const bool validateTime )
{
    // Find the GTime & dt elements of the frame container. 
    LdasElement* fr_gtime = fr.find( "gtime" );
    LdasElement* fr_dt = fr.find( "dt" );
    LdasElement* ser_dt = ser.find( "dt" );

    // Check to make sure the frame data is valid
    if ( fr_gtime == 0 || fr_gtime->getElementId() != ILwd::ID_INT_4U )
    {
       throw SWIGEXCEPTION( "bad_frame: gtime" );
    }
   
    if( fr_dt == 0 || fr_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check to make sure the ser data is valid
    if ( ser_dt == 0 || ser_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_serdata: dt" );
    }

    // Check the characteristics of the GTime element.
    LdasArray< INT_4U >& gtime(
        dynamic_cast< LdasArray< INT_4U >& >( *fr_gtime ) );
    if ( gtime.getNDim() != 1 || gtime.getDimension( 0 ) != 2 )
    {
        throw SWIGEXCEPTION( "bad_frame: gtime" );
    }

    // Check the characteristics of the Frame Dt element.
    LdasArray< REAL_8 >& fdt( dynamic_cast< LdasArray< REAL_8 >& >( *fr_dt ) );
    if ( fdt.getNDim() != 1 || fdt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check the characteristics of the ser Dt element.
    LdasArray< REAL_8 >& adt(
        dynamic_cast< LdasArray< REAL_8 >& >( *ser_dt ) );
    if ( adt.getNDim() != 1 || adt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_serdata: dt" );
    }

    // Make sure there is a RawData object in the frame
    LdasElement* t = fr.find( "rawdata", 1 );
    if ( t == 0 || t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "no_rawdata" );
    }
    LdasContainer& rawdata( dynamic_cast< LdasContainer& >( *t ) );

    // If 'fdt' is not zero, then we must have some data in the frame.  Check
    // to make sure the start times and length for the data matches that of the
    // frame.
    if( validateTime && fdt.getData()[ 0 ] != 0 )
    {
        INT_4U time;
        
        // Check the GPS seconds.
        std::string tmp( ser.getName( 3 ) );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(seconds): "
                                 "frame vs. serdata" );
        }

        // Check the GPS nanoseconds.
        tmp = ser.getName( 4 );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 1 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(nanoseconds): "
                                 "frame vs. serdata" );
        }

        // Check the length
        if ( fdt.getData()[ 0 ] < adt.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_length: frame vs. serdata" );
        }
    }

    // Find the serdata container in rawdata if it exists
    t = rawdata.find( "serdata", 1 );
    if ( t == 0 )
    {
        // it didn't exist so create it.
        rawdata.push_back( LdasContainer( ":serdata:Container(SerData)" ) );
        t = rawdata.back();
    }
    // Check the serdata container
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_rawdata" );
    }

#ifdef RESET_FRAME_TIME      
    // If this is the first data element in the frame, set the frame GPS time
    // and length to correspond to the inserted object.
    if( gtime.getData()[ 0 ] == 0 &&
        gtime.getData()[ 1 ] == 0 && 
        fdt.getData()[ 0 ] == 0 )
    {
        std::string tmp( ser.getName( 3 ) );
        gtime.getData()[ 0 ] = strtoul( tmp.c_str(), 0, 10 );
        tmp = ser.getName( 4 );
        gtime.getData()[ 1 ] = strtoul( tmp.c_str(), 0, 10 );
        fdt.getData()[ 0 ] = adt.getData()[ 0 ];
    }    
#endif   

    // OK, everything checks out so add the serdata
    dynamic_cast< LdasContainer& >( *t ).push_back( ser );
    return;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Concatenate two serial data objects.
//
//!usage_ooi: LdasContainer* concatSerDataContainer = 
//+	concatSerData( c1, c2 );
//
//!param: const LdasContainer* c1 - first serial data object.
//!param: const LdasContainer* c2 - second serial data object.
//
//!return: LdasContainer* concatSerDataContainer - newly allocated
//+	concatenated serial data object.
//
//!exc: SwigException - invalid serial data container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* concatSerData(
   const LdasContainer* c1, const LdasContainer* c2 )
try
{
    // Make sure this is frame data.
    if ( c1->getName( 5 ) != "Frame" ||
         c2->getName( 5 ) != "Frame" )
    {
        throw SWIGEXCEPTION( "bad_serdata: not_frame_data" );
    }

    string c1_name( c1->getName( 2 ) );
    string c2_name( c2->getName( 2 ) );   
   
    if ( strcasecmp( c1_name.c_str(), "serdata" ) != 0 ||
         strcasecmp( c2_name.c_str(), "serdata" ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_types" );
    }

    c1_name = c1->getName( 0 );
    c2_name = c2->getName( 0 );   
   
    if ( strcasecmp( c1_name.c_str(), c2_name.c_str() ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_channels" );
    }
   
    // Check the characteristics of the 'time' element.
    const LdasArray< INT_4U >& t1( findArrayType< INT_4U >( *c1, "time" ) );
    const LdasArray< INT_4U >& t2( findArrayType< INT_4U >( *c2, "time" ) );
    if( t1.getNDim() != 1 || t1.getDimension( 0 ) != 2 ||
        t2.getNDim() != 1 || t2.getDimension( 0 ) != 2 )
    {
       throw SWIGEXCEPTION( "bad_serdata: time" ); 
    }
    const Time time1( t1.getData()[ 0 ], t1.getData()[ 1 ] );
    const Time time2( t2.getData()[ 0 ], t2.getData()[ 1 ] );

    const LdasArray< REAL_8 >& sr1( findArrayType< REAL_8 >( *c1, "samplerate" ) ); 
    const LdasArray< REAL_8 >& sr2( findArrayType< REAL_8 >( *c2, "samplerate" ) );       

    if ( sr1 != sr2 ||
         sr1.getNDim() != 1 || sr1.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "incompatible_serdata: samplerate" );
    }
    REAL_8 sr( sr1.getData()[ 0 ] );

    const LdasArray< REAL_8 >& d1( findArrayType< REAL_8 >( *c1, "dt" ) ); 
    const LdasArray< REAL_8 >& d2( findArrayType< REAL_8 >( *c2, "dt" ) );    
    if ( d1.getNDim() != 1 || d1.getDimension( 0 ) != 1 ||
         d2.getNDim() != 1 || d2.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_serdata: dt" );
    }
    REAL_8 dt1( d1.getData()[ 0 ] );
    REAL_8 dt2( d2.getData()[ 0 ] );

    // What if the strtoul fails? Should use strstream?
    INT_4U gs1( strtoul( c1->getName( 3 ).c_str(), 0, 10 ) );
    INT_4U gs2( strtoul( c2->getName( 3 ).c_str(), 0, 10 ) );
    INT_4U gn1( strtoul( c1->getName( 4 ).c_str(), 0, 10 ) );
    INT_4U gn2( strtoul( c2->getName( 4 ).c_str(), 0, 10 ) );

    // Calculate what the start time for the 2nd container should be
    INT_4U nextgs = gs1 + static_cast< INT_4U >( dt1 );
    INT_4U nextgn = gn1 + static_cast< INT_4U >( fmod( dt1, 1.0 ) * 1e9 );
    if ( nextgn >= 1000000000 )
    {
        ++nextgs;
        nextgn -= 1000000000;
    }
    if ( nextgs != gs2 || nextgn != gn2 )
    {
        throw SWIGEXCEPTION( "incompatible_serdata: The second FrSerData "
                             "structure does not start where the first "
                             "ended." );
    }

    // Calculate what the start time for the 2nd container should be
    const Time nextg( time1.getSec() + static_cast< INT_4U >( dt1 ),
       time1.getNSec() + static_cast< INT_4U >( fmod( dt1, 1.0 ) * 1e9 ) );

    if ( nextg != time2 )
    {
        throw SWIGEXCEPTION( "incompatible_serdata: The second FrSerData "
                             "structure does not start where the first "
                             "ended( time )" );
    }

    const LdasContainer* cn1( findContainerType( *c1, "serial" ) );
    const LdasContainer* cn2( findContainerType( *c2, "serial" ) );   
    if ( cn1 == 0 || cn1->size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_serdata: serial" );
    }

    const LdasElement* elem1( ( *cn1 )[ 0 ] );
    const LdasArrayBase* data1( dynamic_cast< const LdasArrayBase* >( elem1 ) );
    //:TODO: Is it correct duration test?
    REAL_8 calcdt( data1->getDimension( 0 ) / sr );
    if ( fabs( calcdt - dt1 ) > 1e-9 )
    {
        throw SWIGEXCEPTION( "incomplete_serdata: serial" );
    }

    LdasContainer* concatContainer( 0 );
    try
    {
       // Now create a copy of the first container and replace its data with
       // the new data.
       concatContainer = new LdasContainer( *c1 );
       LdasContainer* cdata( dynamic_cast< LdasContainer* >(
          concatContainer->find( "serial", 1 ) ) );
       cdata->erase( cdata->begin(), cdata->end() );
       cdata->push_back( concatElementData( cn1, cn2 ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

       // Update the 'dt' field also.
       LdasElement* e( concatContainer->find( "dt" ) );
       const REAL_8 newdt[] = { dt1 + dt2 };
       dynamic_cast< LdasArray< REAL_8 >& >( *e ).setData( newdt );
   
       // Concatenate the 'data' field.
       e = concatContainer->find( "data" );
       if( e == 0 || e->getElementId() != ILwd::ID_LSTRING )
       {
          throw SWIGEXCEPTION( "bad_serdata: data" );
       }
       const LdasString& data( findLStringType( *c2, "data" ) );   
       dynamic_cast< LdasString& >( *e ).setString(
          dynamic_cast< const LdasString& >( *e ).getString() + 
          "\n" + data.getString() );

       // Concatenate 'table' data if it's present in the container:
       LdasElement* elem = concatContainer->find( "table", 1 );
       if( elem != 0 )
	 {
	   if( elem->getElementId() != ILwd::ID_ILWD )
	     {
	       throw SWIGEXCEPTION( "bad_serdata: table" );
	     } 
	   cn1 = findContainerType( *c1, "table" );
	   cn2 = findContainerType( *c2, "table" );
	   if( cn1->size() == 0 || cn2 == 0 ||
	       cn1->size() != cn2->size() ) 
	     {
	       throw SWIGEXCEPTION( "incompatible_serdata: table" );
	     }

	   cdata = dynamic_cast< LdasContainer* >( elem );
	   cdata->erase( cdata->begin(), cdata->end() );
	   for ( unsigned int i = 0; i < cn1->size(); ++i )
	     {
	       cdata->push_back( concatTable
				 ( dynamic_cast< const LdasContainer* >
				   ( ( *cn1 )[ i ] ),
				   dynamic_cast< const LdasContainer* >
				   ( ( *cn2 )[ i ] ) ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
	     }
	 }
    }
    catch(...)
    {
       delete concatContainer;
       throw;
    }
 
    return concatContainer;
}
catch( std::bad_cast& )
{
   throw SWIGEXCEPTION( "serdata: invalid_input_format" );
}
#endif /* ! CORE_API */
