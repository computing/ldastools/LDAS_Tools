/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "config.h"

// System Header Files
#include <sstream>      
   
#include "framecpp/Time.hh"

#include "frstatdata.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "util.hh"

using FrameCPP::FrStatData;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
   
using namespace std;   
   
//!ignore_begin:

enum
{
    FR_NAME,
    FR_COMMENT,
    FR_REPRESENTATION,
    FR_TIMESTART,
    FR_TIMEEND,
    FR_VERSION,
    FR_DATA,
    FR_TABLE
};


QueryHash initStatDataHash()
{
    QueryHash h;
    h[ "name"        ] = FR_NAME;
    h[ "comment"     ] = FR_COMMENT;
    h[ "representation" ] = FR_REPRESENTATION;
    h[ "timestart"   ] = FR_TIMESTART;
    h[ "timeend"     ] = FR_TIMEEND;
    h[ "version"     ] = FR_VERSION;
    h[ "data"        ] = FR_DATA;
    h[ "table"        ] = FR_TABLE;
    return h;
}


static QueryHash statDataHash( initStatDataHash() );    
    
    
//-----------------------------------------------------------------------------
std::string getAttribute( const FrStatData& statData, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        statDataHash.find( q.getName().c_str() );
    if ( iter == statDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << statData.GetName();
                break;
                
            case FR_COMMENT:
                res << statData.GetComment();
                break;

            case FR_REPRESENTATION:
                res << statData.GetRepresentation();
                break;
                
            case FR_TIMESTART:
                res << statData.GetTimeStart();
                break;
                
            case FR_TIMEEND:
                res << statData.GetTimeEnd();
                break;
                
            case FR_VERSION:
                res << statData.GetVersion();
                break;

            case FR_DATA:
                return getAttribute( statData.RefData(), dq );

            case FR_TABLE:
                return getAttribute( statData.RefTable(), dq );

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_DATA:
                return getAttribute( getContained( statData.RefData(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute( getContained( statData.RefTable(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}


//-----------------------------------------------------------------------------
LdasElement* getData( const FrStatData& statData, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        return statData2container( statData );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        statDataHash.find( q.getName().c_str() );
    if ( iter == statDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_DATA:
            return getData( getContained( statData.RefData(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;

        case FR_TABLE:
            return getData( getContained( statData.RefTable(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;
                
        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrStatData.
//
//!usage_ooi: FrStatData* statData = container2statData( c );
//
//!param: const LdasContainer& c - statData container to convert.
//!result: FrStatData* statData - newly allocated conversion result FrameCPP::FrStatData.
//!exc: SwigException - bad FrStatData container data.
//
//-----------------------------------------------------------------------------
FrStatData* container2statData( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
    if ( strcasecmp( c_name.c_str(), "StatData" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrStatData container." );
    }
    
    FrStatData* stat = new FrStatData(
        c.getName( 0 ),
        findLStringType( c, "comment" ).getString(),
        findLStringType( c, "representation" ).getString(),
        *findArrayType< INT_4U >( c, "timeStart" ).getData(),
        *findArrayType< INT_4U >( c, "timeEnd" ).getData(),
        *findArrayType< INT_4U >( c, "version" ).getData() );
    
    try
    {
        const LdasContainer* data( findContainerType( c, "data" ) );
        if ( data != 0 )
        {
            for ( unsigned int i = 0; i < data->size(); ++i )
            {
		FrStatData::data_type::value_type
		    d( array2vect( dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) ) );

                stat->RefData().append( d );
            }
        }

        const LdasContainer* table( findContainerType( c, "table" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
		FrStatData::table_type::value_type
		    t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) ) );

                stat->RefTable().append( t );
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete stat;
      stat = (FrameCPP::FrStatData*)NULL;
      throw SWIGEXCEPTION( "invalid_format" );
    }
    
    return stat;
}


//-----------------------------------------------------------------------------
//
//: Convert FrStatData to ILWD.
//
//!usage_ooi: LdasContainer* statDatacontainer = statData2container( stat );
//
//!param: const FrStatData& stat - FrameCPP::FrStatData to convert.
//!result: LdasContainer* statDatacontainer - converted newly allocated result.
//
//-----------------------------------------------------------------------------
LdasContainer* statData2container( const FrStatData& stat )
{
    LdasContainer* c = new LdasContainer( "::StatData:Frame" );
    c->setName( 0, stat.GetName() );
    
    LdasString* comment( new LdasString( stat.GetComment(), "comment" ) );
    LdasString* representation( new LdasString( stat.GetRepresentation(), "representation" ) );
    LdasArray< INT_4U >* timeStart(
        new LdasArray< INT_4U >( stat.GetTimeStart(), "timeStart" ) );
    LdasArray< INT_4U >* timeEnd(
        new LdasArray< INT_4U >( stat.GetTimeEnd(), "timeEnd" ) );
    LdasArray< INT_4U >* version(
        new LdasArray< INT_4U >( stat.GetVersion(), "version" ) );

    c->push_back( comment,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( representation,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( timeStart,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( timeEnd,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( version,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    size_t dataSize( stat.RefData().size() );
    if ( dataSize > 0 )
    {
        LdasContainer* data(
            new LdasContainer( ":data:Container(Vect):Frame" ) );
        c->push_back( data,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrStatData::const_iterator iter( stat.RefData().begin() );
        for( size_t i = dataSize; i != 0; --i, ++iter )
        {
            data->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    size_t tableSize( stat.RefTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer* table(
            new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrStatData::const_table_iterator iter( stat.RefTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    return c;
}


