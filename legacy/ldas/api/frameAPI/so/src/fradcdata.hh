#ifndef FrameApiAdcDataHH
#define FrameApiAdcDataHH

#include <deque>
#include <list>
#include <string>

#include "framecpp/FrAdcData.hh"
#include "framecpp/Time.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

#if HAVE_LDAS_PACKAGE_ILWD
namespace ILwd
{
  class LdasContainer;
  class LdasElement;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

namespace FrameAPI
{
  namespace FrAdcData
  {
    void appendAuxData( FrameCPP::FrAdcData& Dest,
			const FrameCPP::FrAdcData& Source );

    FrameCPP::FrAdcData* cloneHeader( const FrameCPP::FrAdcData& Source );

    FrameCPP::FrAdcData* concat( const std::list< FrameCPP::Base* >& Segment );
  }
}

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasContainer*
adcData2container( const FrameCPP::FrAdcData& adc,
		   const FrameCPP::Time& gtime,
		   const REAL_8& dt, ILwd::LdasContainer* c = 0 );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

bool
areAdcsCompatable( const FrameCPP::FrAdcData& Adc1,
		   const FrameCPP::FrAdcData& Adc2 );

#if HAVE_LDAS_PACKAGE_ILWD
FrameCPP::FrAdcData*
container2adcData( const ILwd::LdasContainer& c );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

FrameCPP::FrAdcData*
concatAdcData( std::list< const FrameCPP::FrAdcData* > AdcData );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasContainer*
concatAdcData( const ILwd::LdasContainer* c1,
	       const ILwd::LdasContainer* c2 );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

std::string
getAttribute( const FrameCPP::FrAdcData& adcData,
	      std::deque< Query >& q,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrAdcData& adcData,
	 std::deque< Query >& q,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime,
	 const REAL_8& dt );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
void
insertAdcData( ILwd::LdasContainer& fr,
	       ILwd::LdasContainer& adc, 
	       const bool validateTime = true );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiAdcDataHH
