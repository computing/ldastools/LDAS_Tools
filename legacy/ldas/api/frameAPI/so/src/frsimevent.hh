#ifndef FrameApiSimEventHH
#define FrameApiSimEventHH

#include <string>
#include <deque>

#include "framecpp/FrSimEvent.hh"
#include "framecpp/Time.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrSimEvent& SimEvent,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD

ILwd::LdasElement*
getData( const FrameCPP::FrSimEvent& SimEvent,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime,
	 const REAL_8& dt );

FrameCPP::FrSimEvent*
container2simEvent( const ILwd::LdasContainer& c );

ILwd::LdasContainer*
simEvent2container( const FrameCPP::FrSimEvent& SimEvent,
		    const FrameCPP::Time& gtime,
		    const REAL_8& dt );

void insertSimEvent( ILwd::LdasContainer& fr, ILwd::LdasContainer& sim );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiSimEventHH
