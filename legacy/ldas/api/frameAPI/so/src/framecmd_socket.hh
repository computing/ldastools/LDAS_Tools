#ifndef FRAME_API__FRAME_CMD_SOCKET_HH
#define FRAME_API__FRAME_CMD_SOCKET_HH

#if HAVE_LIBOSPACE

// ObjectSpace Header Files
#include <ospace/network.h>
#include <ospace/stream.h>

namespace FrameCPP
{
  class FrameH;
}

//!exc: SwigException      
void sendFrame( os_tcp_socket* socket, FrameCPP::FrameH* frame );

//!exc: SwigException   
FrameCPP::FrameH* recvFrame( os_tcp_socket* socket );

#if WORKING
//!exc: SwigException      
void sendFrameBinary( os_tcp_socket* socket, FrameCPP::FrameH* frame );

CREATE_THREADED2V_DECL( sendFrameBinary, os_tcp_socket*, FrameCPP::FrameH* );

//!exc: SwigException      
FrameCPP::FrameH* recvFrameBinary( os_tcp_socket* socket );

CREATE_THREADED1_DECL( recvFrameBinary, FrameCPP::FrameH*, os_tcp_socket* );

#endif /* WORKING */

#endif /* FRAME_API__FRAME_CMD_SOCKET_HH */

