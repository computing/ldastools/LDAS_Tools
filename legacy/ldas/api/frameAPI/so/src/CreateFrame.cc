#include "config.h"

#include "framecpp/FrRawData.hh"

#include "CreateFrame.hh"
#include "frframe.hh"

using FrameCPP::FrRawData;
using FrameAPI::CreateFrame;

CreateFrame::
CreateFrame( const frame_files_type& FrameFiles,
	     const std::list< channel_input_type>& Channels,
	     bool Concatination )
  : ConditionData( FrameFiles, Channels, Concatination )
{
}

void CreateFrame::
Eval( std::list< FrameCPP::FrameH* >& Frames )
{
  setMode( MODE_FRAME );
  eval( );

  for ( channels_type::iterator c = m_channels.begin();
	c != m_channels.end();
	c++ )
  {
    for( segment_type::iterator s = (*c).m_data.begin();
	 s != (*c).m_data.end( );
	 s++ )
    {
      FrameCPP::FrameH*	frameh( (*s).GetFrameH( ) );
      General::GPSTime	start( frameh->GetGTime( ) );

      if ( dynamic_cast< FrameCPP::FrProcData* >( (*s).m_segment.get( ) ) )
      {
#if 0
	General::SharedPtr< FrameCPP::FrProcData >
	  proc( std::dynamic_pointer_cast< FrameCPP::FrProcData >
		( (*s).m_segment ) );
#else
	General::SharedPtr< FrameCPP::FrProcData >
	  proc( General::DynamicPointerCast< FrameCPP::FrProcData >
		( (*s).m_segment ) );
#endif

	frameh->RefProcData( ).append(  proc );
	frameh->RefProcData( ).back( )->
	  SetTimeOffset( frameh->RefProcData( ).back( )->GetTimeOffset( ) +
			 ( (*s).m_start - start ) );
      }
      else if ( dynamic_cast< FrameCPP::FrAdcData* >( (*s).m_segment.get( ) ) )
      {
	FrameCPP::FrameH::rawData_type
	  rd( frameh->GetRawData( ) );

	if ( ! rd  )
	{
	  rd.reset( new FrameCPP::FrameH::rawData_type::element_type( ) );
	}

	General::SharedPtr< FrameCPP::FrAdcData >
	  adc( General::DynamicPointerCast< FrameCPP::FrAdcData >
		( (*s).m_segment ) );

	rd->RefFirstAdc( ).append( adc );
	rd->RefFirstAdc( ).back( )->
	  SetTimeOffset( rd->RefFirstAdc( ).back( )->GetTimeOffset( ) +
			 ( (*s).m_start - start ) );
      }
      else
      {
	throw std::runtime_error( "cannot add unknown channel type to frame" );
      }
    }
  }
  foreachFrame( FrameAPI::Normalize );
  Frames = releaseFrames( );
}
