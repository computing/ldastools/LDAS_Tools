/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "ldas_tools_config.h"

// System Header Files
#include <sstream>   
#include <vector>
   
#include "framecpp/Time.hh"

#include "frevent.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "util.hh"

using FrameCPP::FrEvent;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasElement;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasArrayBase;
using ILwd::LdasString;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */
   
using namespace std;   
   
//!ignore_begin:

enum
{
    FR_NAME,
    FR_COMMENT,
    FR_INPUTS,
    FR_GTIMES,
    FR_GTIMEN,

    FR_TIMEBEFORE,
    FR_TIMEAFTER,
    FR_EVENTSTATUS,
    FR_AMPLITUDE,

    FR_PROBABILITY,
    FR_STATISTICS,

    FR_NPARAM,
    FR_PARAMETERS,
    FR_PARAMETERNAMES,

    FR_DATA,
    FR_TABLE
};


QueryHash initEventHash()
{
    QueryHash h;
    h[ "name"           ] = FR_NAME;
    h[ "comment"        ] = FR_COMMENT;
    h[ "inputs"         ] = FR_INPUTS;
    h[ "gtimes"         ] = FR_GTIMES;
    h[ "gtimen"         ] = FR_GTIMEN;

    h[ "timebefore"     ] = FR_TIMEBEFORE;
    h[ "timeafter"      ] = FR_TIMEAFTER;
    h[ "eventstatus"    ] = FR_EVENTSTATUS;
    h[ "amplitude"      ] = FR_AMPLITUDE;


    h[ "probability"    ] = FR_PROBABILITY;
    h[ "statistics"     ] = FR_STATISTICS;
    h[ "nparam"         ] = FR_NPARAM;
    h[ "parameters"     ] = FR_PARAMETERS;
    h[ "parameternames" ] = FR_PARAMETERNAMES;

    h[ "data"           ] = FR_DATA;
    h[ "table"          ] = FR_TABLE;
    return h;
}


static QueryHash eventHash( initEventHash() );    
    
    
namespace {
#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
    ILwd::LdasContainer*
    param2container( const FrEvent::ParamList_type& Params )
    {
	//---------------------------------------------------------------
	// Split the structure up into meaningful pieces
	//---------------------------------------------------------------
	std::vector< REAL_4 >		parameters;
	std::vector< std::string >	parameterNames;
	
	for ( FrEvent::ParamList_type::const_iterator
		  cur = Params.begin( ),
		  end = Params.end( );
	      cur != end;
	      ++cur )
	{
	    parameterNames.push_back( (*cur).first );
	    parameters.push_back( (*cur).second );
	}

	//---------------------------------------------------------------
	// Put into a container
	//---------------------------------------------------------------
	std::unique_ptr< ILwd::LdasContainer >
	    c( new ILwd::LdasContainer( "Param" ) );
	c->push_back( new ILwd::LdasArray< REAL_4 >( &( parameters[ 0 ] ),
						     1,
						     "parameters" ),
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
	c->push_back( new ILwd::LdasString( parameterNames,
					    "parameterNames" ),
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
	//---------------------------------------------------------------
	// Deliver to the caller
	//---------------------------------------------------------------
	return c.release( );
    }
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */

#ifdef WORKING
#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
    FrEvent::ParamList_type*
    param2container( const ILwd::LdasContainer& Param )
    {
	//---------------------------------------------------------------
	// Split the structure up into meaningful pieces
	//---------------------------------------------------------------
	std::vector< REAL_4 >		parameters;
	std::vector< std::string >	parameterNames;
	
	for ( FrEvent::ParamList_type::const_iterator
		  cur = Params.begin( ),
		  end = Params.end( );
	      cur != end;
	      ++cur )
	{
	    parameterNames.push_back( (*cur).first );
	    parameters.push_back( (*cur).second );
	}

	//---------------------------------------------------------------
	// Put into a container
	//---------------------------------------------------------------
	std::unique_ptr< ILwd::LdasContainer >
	    c( new ILwd::LdasContainer( "Param" ) );
	c->push_back( new ILwd::LdasArray< REAL_4 >( &( parameters[ 0 ] ),
						     1,
						     "parameters" ),
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
	c->push_back( new ILwd::LdasString( parameterNames,
					    "parameterNames" ),
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
	//---------------------------------------------------------------
	// Deliver to the caller
	//---------------------------------------------------------------
	return c.release( );
    }
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */
#endif /* WORKING */
}

//-----------------------------------------------------------------------------
#if ! CORE_API
std::string getAttribute( const FrEvent& event, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        eventHash.find( q.getName().c_str() );
    if ( iter == eventHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
	case FR_NAME:
	    res << event.GetName();
	    break;
	    
	case FR_COMMENT:
	    res << event.GetComment();
	    break;
	    
	case FR_INPUTS:
	    res << event.GetInputs();
	    break;
	    
	case FR_GTIMES:
	    res << event.GetGTime().getSec();
	    break;
	    
	case FR_GTIMEN:
	    res << event.GetGTime().getNSec();
	    break;
	    
	case FR_TIMEBEFORE:
	    res << event.GetTimeBefore();
	    break;
	    
	case FR_TIMEAFTER:
	    res << event.GetTimeAfter();
	    break;
	    
	case FR_EVENTSTATUS:
	    res << event.GetEventStatus();
	    break;
	    
	case FR_AMPLITUDE:
	    res << event.GetAmplitude();
	    break;
	    
	case FR_PROBABILITY:
	    res << event.GetProbability();
	    break;
	    
	case FR_STATISTICS:
	    res << event.GetStatistics();
	    break;
	    
	case FR_NPARAM:
	    res << event.GetParam( ).size( );
	    break;
	    
	case FR_DATA:
	    return getAttribute( event.RefData(), dq );

	case FR_TABLE:
	    return getAttribute( event.RefTable(), dq );

	default:
	    throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_DATA:
                return getAttribute( getContained( event.RefData(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute( getContained( event.RefTable(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
#if ! CORE_API
LdasElement* getData( const FrEvent& event, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime, const REAL_8& dt )
{
    if ( dq.size() == 0 )
    {
        return event2container( event, gtime, dt );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        eventHash.find( q.getName().c_str() );
    if ( iter == eventHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_DATA:
            return getData( getContained( event.RefData(), q, start, index ),
			    dq, start, index + 1 );
            break;

        case FR_TABLE:
            return getData( getContained( event.RefTable(), q, start, index ),
			    dq, start, index + 1 );
            break;

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}
#endif /* ! CORE_API */
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrameCPP::FrEvent.
//
//!usage_ooi: FrameCPP::FrEvent* event = container2event( c );
//
//!param: const LdasContainer& c - FrEvent LDAS container.
//
//!return: FrEvent* event - FrameCPP::FrEvent newly allocated object.
//
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
FrEvent* container2event( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
    if ( strcasecmp( c_name.c_str(), "Event" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrEvent container." );
    }
    
    const LdasArray< INT_4U >& te = findArrayType< INT_4U >( c, "GTime" );

    FrEvent::ParamList_type	params;
    std::unique_ptr< FrEvent >
	event( new FrEvent( c.getName( 0 ),
			    findLStringType( c, "comment" ).getString(),
			    findLStringType( c, "inputs" ).getString(),
			    FrameCPP::Time( te.getData()[ 0 ],
					    te.getData()[ 1 ] ),
			    *findArrayType< REAL_4 >( c, "timebefore" ).getData(),
			    *findArrayType< REAL_4 >( c, "timeafter" ).getData(),
			    *findArrayType< INT_4U >( c, "eventstatus" ).getData(),
			    *findArrayType< REAL_4 >( c, "amplitude" ).getData(),
			    *findArrayType< REAL_4 >( c, "probability" ).getData(),
			    findLStringType( c, "statistics" ).getString(),
			    params
	) );
    
    try
    {
        const LdasContainer* data( findContainerType( c, "data" ) );
        if ( data != 0 )
        {
            for ( unsigned int i = 0; i < data->size(); ++i )
            {
                FrVect* v = array2vect(
                    dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) );
                event->RefData().append( *v );
                delete v;
            }
        }
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "invalid_data_format" );
    }

    try
    {
        const LdasContainer*
	    table( findContainerType( c, "table" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
	      FrTable* t = container2table(
                    dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) );
	      event->RefTable().append( *t );
            }
        }
	delete table;
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "invalid_table_format" );
    }
    
    return event.release( );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert FrEvent to ILWD.
//
//!:usage_ooi: LdasContainer* eventContainer =
//+	event2container( event, gtime. dt );
//
//!param: const FrEvent& event - FrameCPP::FrEvent object to convert.
//!param: const Time& gtime - FrameCPP::Frame time.
//!param: const REAL_8& dt - FrameCPP::Frame delta time.
//
//!return: LdasContainer* eventContainer - newly allocated FrEvent container.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* event2container(
    const FrEvent& event, const Time& gtime, const REAL_8& dt )
{
    LdasContainer* c = new LdasContainer( "::Event" );
    c->setName( 0, event.GetName() );
    
    ostringstream ss;
    ss.precision( REAL_8_DIGITS + 1 );
    ss << gtime.getSec();
    c->appendName( ss.str() );

    ss.str( "" );
    ss << gtime.getNSec();
    c->appendName( ss.str() );

    c->appendName( "Frame" ); 

    LdasString* comment( new LdasString( event.GetComment(), "comment" ) );
    LdasString* inputs( new LdasString( event.GetInputs(), "inputs" ) );

    INT_4U time[ 2 ] = { event.GetGTime().getSec(),
                         event.GetGTime().getNSec() };
    LdasArray< INT_4U >* gTime( new LdasArray< INT_4U >( time, 2, "GTime" ) );

    LdasArray< REAL_4 >* timebefore(
        new LdasArray< REAL_4 >( event.GetTimeBefore(), "timeBefore" ) );
    LdasArray< REAL_4 >* timeafter(
        new LdasArray< REAL_4 >( event.GetTimeAfter(), "timeAfter" ) );
    LdasArray< INT_4U >* eventstatus(
        new LdasArray< INT_4U >( event.GetEventStatus(), "eventStatus" ) );
    LdasArray< REAL_4 >* amplitude(
        new LdasArray< REAL_4 >( event.GetAmplitude(), "amplitude" ) );

    LdasArray< REAL_4 >* probability(
        new LdasArray< REAL_4 >( event.GetProbability(), "probability" ) );
    LdasString* statistics(
        new LdasString( event.GetStatistics(), "statistics" ) );
    LdasArray< REAL_8 >* e_dt( new LdasArray< REAL_8 >( dt, "dt" ) );

    c->push_back( comment,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( inputs,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( gTime,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( timebefore,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( timeafter,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( eventstatus,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( amplitude,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( probability,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( statistics,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( e_dt,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( param2container( event.GetParam( ) ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    size_t dataSize( event.RefData().size() );
    if ( dataSize > 0 )
    {
        LdasContainer* data(
            new LdasContainer( ":data:Container(Vect):Frame" ) );
        c->push_back( data,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
        
        FrEvent::const_iterator iter( event.RefData().begin() );
        for( size_t i = dataSize; i != 0; --i, ++iter )
        {
            data->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    size_t tableSize( event.RefTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer* table(
            new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrEvent::const_table_iterator iter( event.RefTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    return c;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Insert FrEvent container into Frame container.
//
//!param: LdasContainer& fr - Frame container.
//!param: LdasContainer& event - FrEvent container.
//!param: const bool validateTime - A flag to enable/disable time stamp
//+       validation for data insertion. True to enable, false to disable.       
//+       Default is TRUE.    
//
//!exc: SwigException - invalid container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
void insertEvent( LdasContainer& fr, LdasContainer& event,
   const bool validateTime )
{
    // Find the GTime & dt elements of the frame container. 
    LdasElement* fr_gtime = fr.find( "gtime" );
    LdasElement* fr_dt = fr.find( "dt" );
    LdasElement* event_dt = event.find( "dt" );

    // Check to make sure the frame data is valid
    if( fr_gtime == 0 || fr_gtime->getElementId() != ILwd::ID_INT_4U )
    {
       throw SWIGEXCEPTION( "bad_frame: gtime" );
    }
   
    if( fr_dt == 0 || fr_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check to make sure the event data is valid
    if( event_dt == 0 || event_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_eventdata: dt" );
    }

    // Check the characteristics of the GTime element.
    LdasArray< INT_4U >& gtime(
        dynamic_cast< LdasArray< INT_4U >& >( *fr_gtime ) );
    if ( gtime.getNDim() != 1 || gtime.getDimension( 0 ) != 2 )
    {
        throw SWIGEXCEPTION( "bad_frame: gtime" );
    }

    // Check the characteristics of the Frame Dt element.
    LdasArray< REAL_8 >& fdt( dynamic_cast< LdasArray< REAL_8 >& >( *fr_dt ) );
    if ( fdt.getNDim() != 1 || fdt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check the characteristics of the event Dt element.
    LdasArray< REAL_8 >& adt(
        dynamic_cast< LdasArray< REAL_8 >& >( *event_dt ) );
    if ( adt.getNDim() != 1 || adt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_eventdata: dt" );
    }

    // If 'fdt' is not zero, then we must have some data in the frame.  Check
    // to make sure the start times and length for the data matches that of the
    // frame.
    if( validateTime && fdt.getData()[ 0 ] != 0 )
    {
        INT_4U time;
        
        // Check the GPS seconds.
        std::string tmp( event.getName( 3 ) );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(seconds): " 
                                 "frame vs. eventdata" );
        }

        // Check the GPS nanoseconds.
        tmp = event.getName( 4 );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 1 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(nanoseconds): " 
                                 "frame vs. eventdata" );
        }

        // Check the length
        if ( fdt.getData()[ 0 ] < adt.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_length: frame vs. eventdata" );
        }
    }

    // Find the eventdata container in rawdata if it exists
    LdasElement* t = fr.find( "eventdata", 1 );
    if ( t == 0 )
    {
        // it didn't exist so create it.
        fr.push_back( LdasContainer( ":eventdata:Container(Event)" ) );
        t = fr.back();
    }
    // Check the eventdata container
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_frame: malformed eventData" );
    }

#ifdef RESET_FRAME_TIME   
    // If this is the first data element in the frame, set the frame GPS time
    // and length to correspond to the inserted object.
    if( fdt.getData()[ 0 ] == 0 && 
        gtime.getData()[ 0 ] == 0 && gtime.getData()[ 1 ] == 0 )
    {
        std::string tmp( event.getName( 3 ) );
        gtime.getData()[ 0 ] = strtoul( tmp.c_str(), 0, 10 );
        tmp = event.getName( 4 );
        gtime.getData()[ 1 ] = strtoul( tmp.c_str(), 0, 10 );
        fdt.getData()[ 0 ] = adt.getData()[ 0 ];
    }    
#endif   

    // OK, everything checks out so add the eventdata
    dynamic_cast< LdasContainer& >( *t ).push_back( event );
    return;
}
#endif /* ! CORE_API */
