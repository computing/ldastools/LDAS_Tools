#ifndef FrameApiStatDataHH
#define FrameApiStatDataHH

#include <deque>
#include <string>

#include "framecpp/FrStatData.hh"

#include "ilwd/ldascontainer.hh"

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrStatData& statData,
	      std::deque< Query >& q,
	      std::vector< size_t >& start,
	      size_t index );

ILwd::LdasElement*
getData( const FrameCPP::FrStatData& statData,
	 std::deque< Query >& q,
	 std::vector< size_t >& start, size_t index );

FrameCPP::FrStatData* container2statData( const ILwd::LdasContainer& c );

ILwd::LdasContainer* statData2container( const FrameCPP::FrStatData& stat );

#endif // FrameApiStatDataHH
