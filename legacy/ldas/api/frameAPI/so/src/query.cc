// General Header Files
#include "config.h"

#include <general/regexmatch.hh>
#include <general/util.hh>

// Local Header Files
#include "query.hh"


const Regex Query::re_item(
    "^[[:space:]]*([a-zA-Z0-9_]+)[[:space:]]*(\\.|$)" );
const Regex Query::re_iquery(
    "^[[:space:]]*([a-zA-Z0-9_]+)[[:space:]]*\\{[[:space:]]*([0-9]+)"
    "[[:space:]]*\\}[[:space:]]*(\\.|$)" );
const Regex Query::re_query1(
    "^[[:space:]]*([a-zA-Z0-9_]+)[[:space:]]*\\{[[:space:]]*([a-zA-Z0-9_]+)"
    "[[:space:]]*=[[:space:]]*\"(([^\\\"]|\\\\.)*)\"[[:space:]]*\\}"
    "[[:space:]]*(\\.|$)" );
const Regex Query::re_query2(
    "^[[:space:]]*([a-zA-Z0-9_]+)[[:space:]]*\\{[[:space:]]*([a-zA-Z0-9_]+)"
    "[[:space:]]*=[[:space:]]*'(([^\\']|\\\\.)*)'[[:space:]]*\\}[[:space:]]*"
    "(\\.|$)" );


//
//: Constructor.
//
// Query string parsing.
//
//!exc: SwigException
//      
Query::Query( const char* start, const char** final )
        : mName(), mIsQuery( false ), mQuery(), mValue()
{
    RegexMatch rm( 6 );

    if ( rm.match( re_item, start ) )
    {
        mName = rm.getSubString( 1 );
    }
    else if ( rm.match( re_iquery, start ) )
    {
        mName = rm.getSubString( 1 );
        mIsQuery = true;
        mQuery = "index";
        mValue = rm.getSubString( 2 );
    }
    else if ( rm.match( re_query1, start ) )
    {
        mName = rm.getSubString( 1 );
        mIsQuery = true;
        mQuery = rm.getSubString( 2 );
        mValue = rm.getSubString( 3 );
    }
    else if ( rm.match( re_query2, start ) )
    {
        mName = rm.getSubString( 1 );
        mIsQuery = true;
        mQuery = rm.getSubString( 2 );
        mValue = rm.getSubString( 3 );
    }
    else
    {
        throw SWIGEXCEPTION( "invalid_query" );
    }

    string2lower( mName );
    string2lower( mQuery );
    string2lower( mValue );
    (*final) = rm.getSubEnd( 0 );
}

