#include "config.h"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldaselement.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "framecpp/FrameCPP.hh"
#include "framecpp/Time.hh"

#include "CreateFrameGroup.hh"
#include "frdetector.hh"
#include "frhistory.hh"
#include "framecmd.hh"
#include "fradcdata.hh"
#include "frprocdata.hh"

using FrameAPI::ConditionData;
using FrameAPI::CreateFrameGroup;

#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

namespace {

  //---------------------------------------------------------------------
  // channel2container
  //
  // Helper function to do convert from FrameCPP structure to ILwd
  //---------------------------------------------------------------------

#if HAVE_LDAS_PACKAGE_ILWD
  template< class channel_type >
  inline ILwd::LdasContainer*
  channel2container( const channel_type& Channel,
		     const FrameCPP::Time& Start,
		     REAL_8 Delta );

  template<>
  inline ILwd::LdasContainer*
  channel2container<>
  ( const FrameCPP::FrAdcData& Channel,
    const FrameCPP::Time& Start,
    REAL_8 Delta )
  {
    AT();
    return adcData2container( Channel, Start, Delta );
  }

  template<>
  inline ILwd::LdasContainer*
  channel2container<>
  ( const FrameCPP::FrProcData& Channel,
    const FrameCPP::Time& Start,
    REAL_8 /* Delta */ )
  {
    AT();
    return procData2container( Channel, Start );
  }
#endif /* HAVE_LDAS_PACKAGE_ILWD */

} // namespace - anonymous

CreateFrameGroup::
CreateFrameGroup( const frame_files_type& FrameFiles,
		  const std::list< channel_input_type>& Channels )
  : ConditionData( FrameFiles, Channels )
{
}

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement* CreateFrameGroup::
Eval( )
{
  eval( );
  
  const General::GPSTime&	min( getMin( ) );
  const General::GPSTime&	max( getMax( ) );

  //--------------------------------------------------------------------
  // Combine outputs into a frame_group ilwd
  //--------------------------------------------------------------------
  AT();
  std::unique_ptr<ILwd::LdasContainer>
    frame_group( new ILwd::LdasContainer( "frame_group" ) );
  
  for ( channels_type::iterator c = m_channels.begin();
	c != m_channels.end();
	c++ )
  {
    AT();
    for ( segment_type::iterator s = (*c).m_data.begin();
	  s != (*c).m_data.end();
	  s++ )
    {
      //----------------------------------------------------------------
      // Put into frame group, giving pointer ownership to frame group
      //----------------------------------------------------------------
      AT();
      std::unique_ptr<ILwd::LdasContainer>	c;
      FrameCPP::Time				start( (*s).m_start.GetSeconds( ),
						       (*s).m_start.GetNanoseconds( ) );

      if ( dynamic_cast< FrameCPP::FrAdcData* >( (*s).m_segment.get( ) ) )
      {
	FrameCPP::FrAdcData*
	  adc( dynamic_cast< FrameCPP::FrAdcData* >( (*s).m_segment.get( ) ) );

      c.reset( channel2container( *adc,
				  start,
				  (*s).m_dt ) );
      }
      else if ( dynamic_cast< FrameCPP::FrProcData* >( (*s).m_segment.get( ) ) )
      {
	FrameCPP::FrProcData*
	  proc( dynamic_cast< FrameCPP::FrProcData* >( (*s).m_segment.get( ) ) );

	c.reset( channel2container( *proc,
				    start,
				    (*s).m_dt ) );
      }

      frame_group->push_back( c.release( ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
    }
  }
  //--------------------------------------------------------------------
  // Add the metadata
  //--------------------------------------------------------------------
  AT();
  if ( min.GetNanoseconds( ) )
  {
    frame_group->setMetadata( "start_time", min );
  }
  else
  {
    frame_group->setMetadata( "start_time", min.GetSeconds( ) );
  }
  AT();
  frame_group->setMetadata( "delta_t", max - min );
  frame_group->setMetadata( "FrameCPPVersion",  FrameCPP::GetVersion( ) );
  frame_group->setMetadata( "DataFormatVersion",
			    FrameCPP::GetDataFormatVersion( ) );
  frame_group->setMetadata( "FrameLibraryMinorVersion",
			    FrameCPP::GetDataFormatVersion( ) );
  
  //--------------------------------------------------------------------
  // Allocate space for result
  //--------------------------------------------------------------------
  
  std::unique_ptr<ILwd::LdasContainer>	result( new ILwd::LdasContainer( ) );
  
  //--------------------------------------------------------------------
  // Add frame_group
  //--------------------------------------------------------------------

  result->push_back( frame_group.release( ),
		     ILwd::LdasContainer::NO_ALLOCATE,
		     ILwd::LdasContainer::OWN );
    
  //--------------------------------------------------------------------
  // Add any detector information that has been accumulated
  //--------------------------------------------------------------------
  if ( m_detectors.size( ) > 0 )
  {
    ILwd::LdasContainer*
      dc( new ILwd::LdasContainer( ":detectProc:Container(Detector):Frame" ) );
    
    result->push_back( dc,
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::OWN );
    for ( std::list< FrameCPP::FrDetector* >::const_iterator
	    d( m_detectors.begin( ) );
	  d != m_detectors.end( );
	  d++ )
    {
      dc->push_back( detector2container( *(*d) ),
		     ILwd::LdasContainer::NO_ALLOCATE,
		     ILwd::LdasContainer::OWN );
    }
  }
  //--------------------------------------------------------------------
  // Add any history that has been accumulated
  //--------------------------------------------------------------------
  if ( m_history.size( ) > 0 )
  {
    ILwd::LdasContainer*
      hc( new ILwd::LdasContainer( ":history:Container(History):Frame") );
    
    result->push_back( hc,
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::OWN );
    for ( std::list< FrameCPP::FrHistory* >::const_iterator
	    h( m_history.begin( ) );
	  h != m_history.end( );
	  h++ )
    {
      hc->push_back( history2container( *(*h) ),
		     ILwd::LdasContainer::NO_ALLOCATE,
		     ILwd::LdasContainer::OWN );
    }
  }
  //--------------------------------------------------------------------
  // Return the result
  //--------------------------------------------------------------------
#if LM_DEBUG
  result->write( 2, 2, std::cerr, ILwd::ASCII );
#endif
  AT();
  return result.release( );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */
