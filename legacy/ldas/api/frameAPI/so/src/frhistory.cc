#include "ldas_tools_config.h"

// System Header Files
#include <sstream>   
   
#include "frhistory.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "util.hh"

using FrameCPP::FrHistory;

#if ! CORE_API
using ILwd::LdasArrayBase;
using ILwd::LdasElement;
using ILwd::LdasArray;
using ILwd::LdasString;
using ILwd::LdasContainer;
#endif /* ! CORE_API */
   
using namespace std;   

//!ignore_begin:


enum
{
    FR_NAME,
    FR_TIME,
    FR_COMMENT
};


QueryHash initHistoryHash()
{
    QueryHash h;
    h[ "name"    ] = FR_NAME;
    h[ "time"    ] = FR_TIME;
    h[ "comment" ] = FR_COMMENT;
    return h;
}


static QueryHash historyHash( initHistoryHash() );    
    
    
#if ! CORE_API
//-----------------------------------------------------------------------------
std::string getAttribute( const FrHistory& history, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        historyHash.find( q.getName().c_str() );
    if ( iter == historyHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << history.GetName();
                break;
                
            case FR_TIME:
                res << history.GetTime();
                break;
                
            case FR_COMMENT:
                res << history.GetComment();
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    return res.str();
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
#if ! CORE_API
LdasElement* getData( const FrHistory& history, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        return history2container( history );
    }
    
    throw SWIGEXCEPTION( "bad_query" );
}
#endif /* ! CORE_API */
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrHistory.
//
//!usage_ooi: FrHistory* hist = container2history( c );
//!param: const LdasContainer& c - FrHistory container to convert.
//!return: FrHistory* hist - FrameCPP::FrHistory converted, newly allocated.
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
FrHistory* container2history( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
   
    if ( c.getNameSize() < 3 ||
         strcasecmp( c_name.c_str(), "History" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrHistory container." );
    }
    
    FrHistory* history = new FrHistory(
        c.getName( 0 ),
        *findArrayType< INT_4U >( c, "time" ).getData(),
        findLStringType( c, "comment" ).getString() );
    
    return history;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert FrHistory to ILWD.
//
//!usage_ooi: LdasContainer* histContainer = history2container( history );
//
//!param: const FrHistory& history - FrameCPP::FrHistory to convert.
//!return: LdasContainer* histContainer - newly allocated.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* history2container( const FrHistory& history )
{
    LdasContainer* c = new LdasContainer( "::History:Frame" );
    c->setName( 0, history.GetName() );
    
    LdasArray< INT_4U >* time(
        new LdasArray< INT_4U >( history.GetTime(), "time" ) );
    LdasString* comment(
        new LdasString( history.GetComment(), "comment" ) );

    c->push_back( time,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( comment,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Insert FrHistory container into Frame Container.
//
//!param: LdasContainer& fr - Frame container.
//!param: LdasContainer& h - FrHistory container.
//!exc: SwigException - invalid container data.
//-----------------------------------------------------------------------------
#if ! CORE_API
void insertHistory(
    LdasContainer& fr, LdasContainer& h )
{
    LdasElement* t = fr.find( "history", 1 );
    if ( t == 0 )
    {
        fr.push_back( LdasContainer( ":history:Container(History):Frame" ) );
        t = fr.back();
    }
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_ilwd_frame" );
    }

    dynamic_cast< LdasContainer& >( *t ).push_back( h );
}
#endif /* ! CORE_API */

