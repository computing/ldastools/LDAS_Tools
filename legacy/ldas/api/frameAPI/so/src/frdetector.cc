/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "config.h"

#include <string>

#include "ilwdfcs/FrDetector.hh"

#include "frdetector.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "frstatdata.hh"
#include "util.hh"

using FrameCPP::FrDetector;
using FrameCPP::FrStatData;
using FrameCPP::FrTable;
using FrameCPP::FrVect;

using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

using std::string;
//!ignore_begin:


#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

enum
{
    FR_NAME,
    FR_PREFIX,
    FR_LONGITUDE,
    FR_LATITUDE,
    FR_ELEVATION,
    FR_ARMXAZIMUTH,
    FR_ARMYAZIMUTH,
    FR_ARMXALTITUDE,
    FR_ARMYALTITUDE,
    FR_ARMXMIDPOINT,
    FR_ARMYMIDPOINT,
    FR_LOCALTIME,
    FR_AUX,
    FR_TABLE,
    FR_STATDATA
};


QueryHash initDetectorHash()
{
    QueryHash h;
    h[ "name"         ] = FR_NAME;
    h[ "prefix"	      ] = FR_PREFIX;
    h[ "longitude"    ] = FR_LONGITUDE;
    h[ "latitude"     ] = FR_LATITUDE;
    h[ "elevation"    ] = FR_ELEVATION;
    h[ "armxazimuth"  ] = FR_ARMXAZIMUTH;
    h[ "armyazimuth"  ] = FR_ARMYAZIMUTH;
    h[ "armxaltitude" ] = FR_ARMXALTITUDE;
    h[ "armyaltitude" ] = FR_ARMYALTITUDE;
    h[ "armxmidpoint" ] = FR_ARMXMIDPOINT;
    h[ "armymidpoint" ] = FR_ARMYMIDPOINT;
    h[ "localtime"    ] = FR_LOCALTIME;
    h[ "aux"          ] = FR_AUX;
    h[ "table"        ] = FR_TABLE;
    h[ "statdata"     ] = FR_STATDATA;
    return h;
}


static QueryHash detectorHash( initDetectorHash() );    

    
//-----------------------------------------------------------------------------
std::string getAttribute( const FrDetector& detector, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    std::ostringstream res;
    
    QueryHash::const_iterator iter =
        detectorHash.find( q.getName().c_str() );
    if ( iter == detectorHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << detector.GetName();
                break;
                
            case FR_PREFIX:
                res << detector.GetPrefix();
                break;
                
            case FR_LONGITUDE:
                res << detector.GetLongitude();
                break;
                
            case FR_LATITUDE:
                res << detector.GetLatitude();
                break;
                
            case FR_ELEVATION:
                res << detector.GetElevation();
                break;
                
            case FR_ARMXAZIMUTH:
                res << detector.GetArmXazimuth();
                break;

            case FR_ARMYAZIMUTH:
                res << detector.GetArmYazimuth();
                break;

            case FR_ARMXALTITUDE:
                res << detector.GetArmXaltitude();
                break;

            case FR_ARMYALTITUDE:
                res << detector.GetArmYaltitude();
                break;

	    case FR_ARMXMIDPOINT:
                res << detector.GetArmXmidpoint();
                break;

            case FR_ARMYMIDPOINT:
                res << detector.GetArmYmidpoint();
                break;

            case FR_LOCALTIME:
                res << detector.GetLocalTime();
                break;

            case FR_AUX:
                return getAttribute( detector.RefAux(), dq );

            case FR_TABLE:
                return getAttribute( detector.RefTable(), dq );
                
#if FRAME_SPEC_CURRENT <= 5
            case FR_STATDATA:
                return getAttribute( detector.refStatData(), dq );
#endif /* FRAME_SPEC_CURRENT <= 5 */

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_AUX:
                return getAttribute( getContained( detector.RefAux(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute( getContained( detector.RefTable(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;
                
#if FRAME_SPEC_CURRENT <= 5
            case FR_STATDATA:
                return getAttribute( getContained( detector.refStatData(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;
#endif /* FRAME_SPEC_CURRENT <= 5 */

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str( );
}


//-----------------------------------------------------------------------------
LdasElement* getData( const FrDetector& detector, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        return detector2container( detector );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        detectorHash.find( q.getName().c_str() );
    if ( iter == detectorHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_AUX:
            return getData( getContained( detector.RefAux(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;

        case FR_TABLE:
            return getData( getContained( detector.RefTable(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;
                
#if FRAME_SPEC_CURRENT <= 5
        case FR_STATDATA:
            return getData( getContained( detector.refStatData(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;
#endif /* FRAME_SPEC_CURRENT <= 5 */

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    return (LdasElement*)NULL;
}
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrDetector.
//
//!usage_ooi: FrDetector* detec = container2detector( c );
//
//!param: const LdasContainer& c - FrDetector container to convert.
//
//!return: FrDetector* detec - FrameCPP::FrDetector newly allocated object.
//
//!exc: SwigException - invalid detector container.
//
//-----------------------------------------------------------------------------
FrDetector* container2detector( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
    if ( c.getNameSize() < 3 ||
         strcasecmp( c_name.c_str(), "detector" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrDetector container." );
    }

    std::unique_ptr<FrDetector>
	d( new FrDetector( c.getName( 0 ),
			   findArrayType< CHAR >( c, "prefix" ).getData(),
			   *findArrayType< REAL_8 >( c, "longitude" ).getData(),
			   *findArrayType< REAL_8 >( c, "latitude" ).getData(),
			   *findArrayType< REAL_4 >( c, "elevation" ).getData(),
			   *findArrayType< REAL_4 >( c, "armXazimuth" ).getData(),
			   *findArrayType< REAL_4 >( c, "armYazimuth" ).getData(),
			   *findArrayType< REAL_4 >( c, "armXaltitude" ).getData(),
			   *findArrayType< REAL_4 >( c, "armYaltitude" ).getData(),
			   *findArrayType< REAL_4 >( c, "armXmidpoint" ).getData(),
			   *findArrayType< REAL_4 >( c, "armYmidpoint" ).getData(),
			   *findArrayType< INT_4S >( c, "localTime" ).getData() ) );
    
    try
    {
#if FRAME_SPEC_CURRENT <= 5
        const LdasContainer* statData( findContainerType( c, "statData" ) );
        if ( statData != 0 )
        {
            for ( unsigned int i = 0; i < statData->size(); ++i )
            {
                FrStatData* s = container2statData(
                    dynamic_cast< const LdasContainer& >(
                        *(*statData)[ i ] ) );
                d->refStatData().append( *s );
                delete s;
            }
        }
        
#endif /* FRAME_SPEC_CURRENT <= 5 */

        const LdasContainer* more( findContainerType( c, "aux" ) );
        if ( more != 0 )
        {
            for ( unsigned int i = 0; i < more->size(); ++i )
            {
                FrVect* v = array2vect(
                    dynamic_cast< const LdasArrayBase& >( *(*more)[ i ] ) );
                d->RefAux().append( *v );
                delete v;
            }
        }

        const LdasContainer* moreTable( findContainerType( c, "table" ) );
        if ( moreTable != 0 )
        {
            for ( unsigned int i = 0; i < moreTable->size(); ++i )
            {
                FrTable* t = container2table(
                    dynamic_cast< const LdasContainer& >( *(*moreTable)[ i ] ) );
                d->RefTable().append( *t );
                delete t;
            }
        }
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "invalid_input_format" );
    }
    
    return d.release( );
}


//-----------------------------------------------------------------------------
//
//: Convert FrDetector to ILWD.
//
//!usage_ooi: LdasContainer* detectContainer = detector2container( d );
//
//!param: const FrDetector& d - FrameCPP::Detctor object to convert.
//!param: const string& ilwd_name - FrDetector ILWD format name.   
//
//!return: LdasContainer* detectContainer - newly allocated LDAS container.
//
//-----------------------------------------------------------------------------
LdasContainer* detector2container( const FrDetector& d, 
   const std::string& ilwd_name )
{
    AT( );
    ILwdFCS::FrDetector
	detector( d.GetName( ),
		  d.GetPrefix( ),
		  d.GetLongitude( ),
		  d.GetLatitude( ),
		  d.GetElevation( ),
		  d.GetArmXazimuth( ),
		  d.GetArmYazimuth( ),
		  d.GetArmXaltitude( ),
		  d.GetArmYaltitude( ),
		  d.GetArmXmidpoint( ),
		  d.GetArmYmidpoint( ),
		  d.GetLocalTime( ) );

    AT( );
    LdasContainer* c = detector.Release( );
    AT( );
    c->setName( 1, ilwd_name, true );

    AT( );
#if FRAME_SPEC_CURRENT <= 5
    size_t statDataSize( d.refStatData().size() );
    if ( statDataSize > 0 )
    {
	AT( );
        LdasContainer* statData(
            new LdasContainer( ":statData:Container(StatData):Frame" ) );
        c->push_back( statData,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
	AT( );
        FrDetector::const_statData_iterator iter( d.refStatData().begin() );
        for( size_t i = statDataSize; i != 0; --i, ++iter )
        {
            statData->push_back( statData2container( **iter ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
        }
    }
#endif /* FRAME_SPEC_CURRENT <= 5 */

    AT( );
    size_t moreSize( d.RefAux().size() );
    if ( moreSize > 0 )
    {
	AT( );
        LdasContainer* more(
            new LdasContainer( ":more:Container(Vect):Frame" ) );
        c->push_back( more,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrDetector::const_aux_iterator iter( d.RefAux().begin() );
        for( size_t i = moreSize; i != 0; --i, ++iter )
        {
	    AT( );
            more->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    size_t auxSize( d.RefAux( ).size( ) );
    if ( auxSize > 0 )
    {
	AT( );
        LdasContainer*
	    aux( new LdasContainer( ":aux:Container(Vect):Frame" ) );

        c->push_back( aux,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrDetector::const_aux_iterator iter( d.RefAux().begin() );
        for( size_t i = auxSize; i != 0; --i, ++iter )
        {
            aux->push_back( vect2array( **iter ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
        }
    }
    
    AT( );
    size_t tableSize( d.RefTable( ).size() );
    if ( tableSize > 0 )
    {
	AT( );
        LdasContainer*
	    table( new LdasContainer( ":table:Container(Table):Frame" ) );

        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrDetector::const_table_iterator iter( d.RefTable( ).begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    AT( );
    return c;
}


//-----------------------------------------------------------------------------
//
//: Insert FrDetector container into Frame container.
//
//!usage_ooi: insertDetector( fr, d );
//
//!param: LdasContainer& fr - Frame container.
//!param: LdasContainer& d - detector container.
//
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------
void insertDetector( LdasContainer& fr, LdasContainer& d )
{
    // Make sure we have the correct number of names
    if ( d.getNameSize() < 3 )
    {
        throw SWIGEXCEPTION( "bad_detector" );
    }

    // Check for detectsim first
    const string& d_name( d.getName( 1 ) );
   
    if ( strcasecmp( d_name.c_str(), "detectsim" ) == 0 )
    {
        // Find the detectsim container in the frame
        LdasElement* t = fr.find( "detectsim", 1 );
        if( t == 0 )
        {
           fr.push_back( LdasContainer( ":detectSim:Container(Detector):Frame" ) );   
           t = fr.back();
        }
        else if( t->getElementId() != ILwd::ID_ILWD )
        {
            throw SWIGEXCEPTION( "bad_frame" );
        }
        
        dynamic_cast< LdasContainer& >( *t ).push_back( d );
    }
    else if ( strcasecmp( d_name.c_str(), "detectproc" ) == 0 )
    {
        // Find the detectproc container in the frame
        LdasElement* t = fr.find( "detectproc", 1 );
        if( t == 0 )
        {
           fr.push_back( LdasContainer( ":detectProc:Container(Detector):Frame" ) );   
           t = fr.back();
        }
        else if( t->getElementId() != ILwd::ID_ILWD )
        {
            throw SWIGEXCEPTION( "bad_frame" );
        }
        
        dynamic_cast< LdasContainer& >( *t ).push_back( d );   
    }
    else
    {
        throw SWIGEXCEPTION( "bad_detector" );
    }
}


 
