#include "config.h"

// System Header Files
#include <string>
#include <stdexcept>
#include <typeinfo>
   
#include "convert.hh"
#include "genericAPI/swigexception.hh"


using ILwd::LdasArray;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasString;
using ILwd::LdasElement;

using namespace std;   

//--------------------------------------------------------------------------------
//
//: Find LDAS Container by name; cast it to ILwd::LdasContainer.
//
// This functions tries to find an ILwd::LdasContainer in the container `c'
// by `name'. Then it tries to dynamicly cast found element to 
// const LdasContainer*.
//
//!usage_ooi: const ILwd::LdasContainer *ldasContainer
//+     = findContainerType( c, name );
//
//!param: const ILwd::LdasContainer& c - source container.
//!param: const std::string& name - target LDAS container name.
//
//!return: const LdasContainer* ldasContainer - found LDAS container.
//
//!exc: SwigException - not found.
//!exc: std::bad_cast - failed to dynamically cast to const LdasContainer* type.
//
//--------------------------------------------------------------------------------
const LdasContainer* findContainerType(
    const LdasContainer& c, const std::string& name )
{
    const LdasElement* c2 = c.find( name, 1 );
    if ( c2 != 0 && c2->getElementId() != ILwd::ID_ILWD )
    {
        string msg( "invalid_format: " );
        msg += name;
        throw SWIGEXCEPTION( msg );
    }
    
    return dynamic_cast< const LdasContainer* >( c2 );
}

//--------------------------------------------------------------------------------
//
//: Find LDAS String by name; cast it to ILwd::LdasString.
//
// This functions tries to find an ILwd::LdasString in the container `c'
// by `name'. Then it tries to dynamicly cast found element to
// const LdasString*.
//
//!usage_ooi: const ILwd::const LdasString &ldasString
//+     = findStringType( c, name );
//
//!param: const ILwd::LdasContainer& c - source container.
//!param: const std::string& name - target LDAS string name.
//
//!return: const LdasString& ldasString - found LDAS string.
//
//!exc: SwigException - not found or failed to dynamically cast to
//+ 	const LdasString* type.
//
//--------------------------------------------------------------------------------
const LdasString& findLStringType(
    const LdasContainer& c, const std::string& name )
{
    const LdasString* c2 = 0;
    
    try
    {
        c2 = dynamic_cast< const LdasString* >( c.find( name ) );
    }
    catch( std::bad_cast& )
    {
        string msg( "invalid_format: " );
        msg +=  name;
        throw SWIGEXCEPTION( msg );
    }
    
    if ( c2 == 0 )
    {
        string msg( "invalid_format: " );
        msg += name;
        msg += " is not found";
        throw SWIGEXCEPTION( msg );
    }

    return *c2;
}

