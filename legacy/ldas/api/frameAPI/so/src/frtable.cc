#include "ldas_tools_config.h"

// System Header Files
#include <cmath>
#include <sstream>   

#include "framecpp/Time.hh"

// Local Header Files
#include "frtable.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "util.hh"

using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

#if ! CORE_API
using ILwd::LdasArrayBase;
using ILwd::LdasElement;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
#endif /* ! CORE_API */
   
using namespace std;   
   
//!ignore_begin:


enum
{
  FR_NAME,
  FR_COMMENT,
  FR_NCOLUMN,
  FR_NROW,
  FR_COLUMNNAME,
  FR_COLUMN
};


QueryHash initTableHash()
{
  QueryHash h;
  h[ "name"       ] = FR_NAME;
  h[ "comment"    ] = FR_COMMENT;
  h[ "ncolumn"    ] = FR_NCOLUMN;
  h[ "nrow"       ] = FR_NROW;
  h[ "columnname" ] = FR_COLUMNNAME;
  h[ "column"     ] = FR_COLUMN;
  return h;
}


static QueryHash tableHash( initTableHash() );
    

//-----------------------------------------------------------------------------
//
// FIXME! :TODO: handle column names

#if CORE_API
std::string getAttribute( const FrTable& table, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
  if ( dq.size() == 0 )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }
    
  Query q = dq.front();
  dq.pop_front();
  ostringstream res;
    
  QueryHash::const_iterator iter = tableHash.find( q.getName().c_str() );
  if ( iter == tableHash.end() )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }

  if ( !q.isQuery() )
    {
      switch( iter->second )
        {
	case FR_NAME:
	  res << table.GetName();
	  break;
                
	case FR_COMMENT:
	  res << table.GetComment();
	  break;
                
	case FR_NCOLUMN:
	  res << table.GetNColumn();
	  break;
	case FR_NROW:
	  res << table.GetNRow();
	  break;
	case FR_COLUMN:
	  return getAttribute( table.RefColumn(), dq );
	default:
	  throw SWIGEXCEPTION( "bad_query" );
        }
    }
  else
    {
      switch( iter->second )
        {
	case FR_COLUMN:
	  return getAttribute( getContained( table.RefColumn(),
					     q, start, index ),
			      dq, start, index + 1 );
	  break;                
	default:
	  throw SWIGEXCEPTION( "bad_query" );
        }
    }

   return res.str();
}
#endif /* CORE_API */


//-----------------------------------------------------------------------------
#if ! CORE_API
LdasElement* getData( const FrTable& table, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
  if ( dq.size() == 0 )
    {
      return table2container( table );
    }
    
  Query q( dq.front() );
  dq.pop_front();
    
  QueryHash::const_iterator iter =
    tableHash.find( q.getName().c_str() );
  if ( iter == tableHash.end() )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }

  if ( !q.isQuery() )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }

  switch( iter->second )
    {
    case FR_COLUMN:
      return getData( getContained( table.RefColumn(),
				    q, start, index ),
		     dq, start, index + 1 );
      break;
    default:
      throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}
#endif /* ! CORE_API */
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrTable.
//
//!usage_ooi: FrTable* table = container2table( c );
//
//!param: const LdasContainer& c - FrTable container.
//
//!return: FrTable* table - FrameCPP::FrTable newly created object.
//
//!exc: SwigException - bad FrTable container.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
FrTable* container2table( const LdasContainer& c )
{
   const string& c_name( c.getName( 2 ) );
   if ( strcasecmp( c_name.c_str(), "table" ) != 0 )
   {
      throw SWIGEXCEPTION( "invalid_format: Not FrTable container." );
   }

   FrTable* table = new FrTable( findLStringType( c, "name" ).getString(),
			    *findArrayType< INT_4U >( c, "nRow" ).getData() );
   table->AppendComment( findLStringType( c, "comment" ).getString() );

   try
   {
      const LdasContainer* data( findContainerType( c, "column" ) );
      if ( data != 0 )
        {
	  for ( unsigned int i = 0; i < data->size(); ++i )
            {
	      FrTable::column_type::value_type
		c( array2vect( dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) ) );

	      table->RefColumn().append( c );
            }
        }
    }
  catch( std::bad_cast& )
    {
      delete table;
      table = ( FrameCPP::FrTable*)NULL;
      throw SWIGEXCEPTION( "invalid_data_format" );
    }

  return table;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert FrTable to ILWD.
//
// This converts a FrameCPP FrTable object into ILWD.  
// :TODO: add `columnName' container
//
//!usage_ooi: LdasContainer* tableContainer = table2container( table );
//
//!param: const FrTable& table - FrameCPP::FrTable to convert.
//
//!return: LdasContainer* tableContainer - newly allocated result.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* table2container( const FrTable& table )
{
  LdasContainer* c = new LdasContainer( "::Table" );
  c->setName( 0, table.GetName() );
    
  LdasString* comment( new LdasString( table.GetComment(), "comment" ) );
  LdasArray< INT_2U >* ncolumns( new LdasArray< INT_2U >( table.GetNColumn(), "nColumn" ) );
  LdasArray< INT_4U >* nrows( new LdasArray< INT_4U >( table.GetNRow(), "nRow" ) );
    
  c->push_back( comment,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( ncolumns, 
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( nrows,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );

  size_t dataSize( table.RefColumn().size() );
  if ( dataSize > 0 )
    {
      LdasContainer* data( new LdasContainer( ":column:Container(Vect):Frame" ) );
      c->push_back( data, 
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
        
      FrTable::const_iterator iter( table.RefColumn().begin() );
      for( size_t i = dataSize; i != 0; --i, ++iter )
        {
	  data->push_back( vect2array( **iter ),
		ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
        }
    }

  return c;
}
#endif /* ! CORE_API */


/*
  :
  name should be the same
  cat comments from both
  nRow should be the same
  cat column vects

  NOTE: concatTable() works on containers that are FrTables, not on the
  containers of FrTables.
  
  This means that concatTable() must be called repeatedly in a loop
  for every table in the container of FrTables. This is done in every
  referencing IGWD structure in FrameAPI.
  
*/
//-----------------------------------------------------------------------------
//
//: Concatenate two table containers.
//
//!usage_ooi: LdasContainer* concatTable = concatTable( c1, c2 );
//
//!param: const LdasContainer* c1 - first table container.
//!param: const LdasContainer* c2 - second table container to cat to the first one.
//
//!result: LdasContainer* concatTable - newly created result.
//
//!exc: SwigException - bad table container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* concatTable ( const LdasContainer* c1,
			     const LdasContainer* c2 )
try
{
  if ( c1 == 0 || c2 == 0 )
    {
      throw SWIGEXCEPTION( "bad_data" );
    }

  if ( c1->size() != c2->size() )
    {
      throw SWIGEXCEPTION( "bad_data" );
    }

  if ( c1->size() != 1 )
    {
      throw SWIGEXCEPTION( "unsupported_data" );
    }

  if ( strcasecmp( c1->getName( 2 ).c_str(), "table" ) != 0 ||
       strcasecmp( c2->getName( 2 ).c_str(), "table" ) != 0 )
    {
      throw SWIGEXCEPTION( "incompatible_types" );
    }

  if ( strcasecmp( c1->getName( 0 ).c_str(),
		   c2->getName( 0 ).c_str() ) != 0 )
    {
      throw SWIGEXCEPTION( "incompatible_tables: name" );
    }

  const LdasArray< INT_4U >& nr1( findArrayType< INT_4U >( *c1, "nRow" ) );
  const LdasArray< INT_4U >& nr2( findArrayType< INT_4U >( *c2, "nRow" ) );

  if ( nr1 != nr2 )
    {
      throw SWIGEXCEPTION( "incompatible_tables: nRow" );
    }

  const LdasContainer* cn1( findContainerType( *c1, "column" ) );
  const LdasContainer* cn2( findContainerType( *c2, "column" ) );  
  if( cn1 == 0 ||  cn1->size() == 0 )
    {
      throw SWIGEXCEPTION( "incompatible_tables: column size" );
    }

  LdasContainer* concatContainer( 0 );
  try
    {
      // Cat column vects
      //
      // :TODO: concatenate all columns (vectos), ie. all subcontainers in cn1 and cn2
      //
      // Now create a copy of the first container and replace its data with
      // the new data.
      concatContainer = new LdasContainer( *c1 );
      LdasContainer* cdata( dynamic_cast< LdasContainer* >( concatContainer->find( "column",
										   1 ) ) );
      cdata->erase( cdata->begin(), cdata->end() );
      cdata->push_back( concatElementData( cn1, cn2 ),
			ILwd::LdasContainer::NO_ALLOCATE,
			ILwd::LdasContainer::OWN );
   
      // Concatenate the comments

      LdasElement* e = concatContainer->find( "comment" );
      if( e == 0 || e->getElementId() != ILwd::ID_LSTRING )
	{
	  throw SWIGEXCEPTION( "bad_table: comment" );
	}
      const LdasString& comment( findLStringType( *c2, "comment" ) );   
      if( comment.getString().empty() == false )
	{
          dynamic_cast< LdasString& >( *e ).
	    setString( dynamic_cast< const LdasString& >( *e ).getString() + 
		       "\n" + comment.getString() );    
	}
    }
  catch(...)
    {
      delete concatContainer;
      concatContainer = (ILwd::LdasContainer*)NULL;
      throw;
    }
 
  return concatContainer;
}
catch( std::bad_cast& )
{
  throw SWIGEXCEPTION( "table: invalid_input_format" );
}
#endif /* ! CORE_API */
