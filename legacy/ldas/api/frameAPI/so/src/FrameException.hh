#ifndef FRAME_EXCEPTION_HH
#define	FRAME_EXCEPTION_HH

#include "genericAPI/swigexception.hh"

namespace FrameAPI
{
  class GapException: public SwigException
  {
  public:
    GapException( const char* Result, const char* File, int Line );
  };

  inline GapException::
  GapException( const char* Result, const char* File, int Line )
    : SwigException( Result, File, Line )
  {
  }
}

#define GAP_EXCEPTION( Result ) \
  FrameAPI::GapException( Result, __FILE__, __LINE__ )

#endif	/* FRAME_EXCEPTION_HH */
