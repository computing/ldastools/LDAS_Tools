/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ldas_tools_config.h"

// System Header Files
#include <sstream>   
   
#include "framecpp/Time.hh"

// Local Header Files
#include "frrawdata.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "frserdata.hh"
#include "fradcdata.hh"
#include "frmsg.hh"
#include "util.hh"

using FrameCPP::Common::SearchContainer;

using FrameCPP::FrAdcData;
using FrameCPP::FrMsg;
using FrameCPP::FrRawData;
using FrameCPP::FrSerData;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasArray;
using ILwd::LdasString;
using ILwd::LdasContainer;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
   
using namespace std;
   
namespace FrameAPI
{
    namespace FrRawData
    {
	//---------------------------------------------------------------
	//!param: FrameCPP::FrRawDaa* Primary - This will containe the
	//+		source plusthe infomation from Secondary
	//!param: FrameCPP::FrRawData* Secondary - Additional information to be
	//+		merged with Primary
	//!return: FrameCPP::FrRawData* - The union of Primary and Secondary

#if CORE_API

	FrameCPP::FrRawData*
	merge( FrameCPP::FrRawData* Primary,
	       const FrameCPP::FrRawData* Secondary )
	{
	    //-----------------------------------------------------------
	    // If BOTH the primary and the secondary are null, just
	    //   return a null pointer
	    //-----------------------------------------------------------
	    if ( ( Primary == (FrameCPP::FrRawData*)NULL ) &&
		 ( Secondary == (FrameCPP::FrRawData*)NULL ) )
	    {
		return (FrameCPP::FrRawData*)NULL;
	    }
	    //-----------------------------------------------------------
	    // If the primary is null and the secondary is non-null,
	    //   return a duplicate of the second
	    //-----------------------------------------------------------
	    if ( Primary == (FrameCPP::FrRawData*)NULL )
	    {
		return new FrameCPP::FrRawData( *Secondary );
	    }
	    //-----------------------------------------------------------
	    // If the primary is non-null and the secondary is null,
	    //   return the primary.
	    //-----------------------------------------------------------
	    if ( Secondary == (FrameCPP::FrRawData*)NULL )
	    {
		return Primary;
	    }
	    //-----------------------------------------------------------
	    // :TODO: Merging of data fields
	    //-----------------------------------------------------------
	    return Primary;
	}
#endif /* CORE_API */
    }
}
//!ignore_begin:


enum
{
    FR_NAME,
    FR_SERIAL,
    FR_ADC,
    FR_TABLE,
    FR_MSG,
    FR_MORE
};


#if CORE_API
QueryHash initRawDataHash()
{
    QueryHash h;
    h[ "name"    ] = FR_NAME;
    h[ "serdata" ] = FR_SERIAL;
    h[ "adcdata" ] = FR_ADC;
    h[ "table"   ] = FR_TABLE;
    h[ "msg"     ] = FR_MSG;
    h[ "more"    ] = FR_MORE;
    return h;
}


QueryHash rawDataHash( initRawDataHash() );    
#else
extern QueryHash rawDataHash;
#endif /* CORE_API */

typedef SearchContainer< FrAdcData, &FrAdcData::GetNameSlow > AdcDataContainer;

//-----------------------------------------------------------------------------

#if CORE_API

std::string getAttribute( const FrRawData& rawData, std::deque< Query >& dq,
                     	  std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        rawDataHash.find( q.getName().c_str() );
    if ( iter == rawDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << rawData.GetName();
                break;

            case FR_SERIAL:
                return getAttribute( rawData.RefFirstSer(), dq );
                
            case FR_ADC:
                return getAttribute( rawData.RefFirstAdc(), dq );

            case FR_TABLE:
                return getAttribute( rawData.RefFirstTable(), dq );

            case FR_MSG:
                return getAttribute( rawData.RefLogMsg(), dq );
                
            case FR_MORE:
                return getAttribute( rawData.RefMore(), dq );

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_SERIAL:
                return getAttribute( getContained( rawData.RefFirstSer(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;
                
            case FR_ADC:
                return getAttribute( getContained( rawData.RefFirstAdc(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute( getContained( rawData.RefFirstTable(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            case FR_MSG:
                return getAttribute( getContained( rawData.RefLogMsg(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;
                
            case FR_MORE:
                return getAttribute( getContained( rawData.RefMore(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}

#endif /* CORE_API */

//-----------------------------------------------------------------------------

#if ! CORE_API

LdasElement* getData( const FrRawData& rawData, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime, const REAL_8& dt )
{
    if ( dq.size() == 0 )
    {
        LdasContainer* c = rawData2container( rawData, gtime, dt );
        c->setName( 1, "rawData" );
        return c;
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        rawDataHash.find( q.getName().c_str() );
    if ( iter == rawDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_SERIAL:
            return getData( getContained( rawData.RefFirstSer(),
					  q, start, index ),
			    dq, start, index + 1, gtime, dt );
            break;
            
        case FR_ADC:
            return getData( getContained( rawData.RefFirstAdc(), q, start, index ),
			    dq, start, index + 1,
			    gtime, dt );
            break;

        case FR_TABLE:
            return getData( getContained( rawData.RefFirstTable(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;

        case FR_MSG:
            return getData( getContained( rawData.RefLogMsg(), q, start, index ),
			    dq, start, index + 1 );
            break;
                
        case FR_MORE:
            return getData( getContained( rawData.RefMore(), q, start, index ),
			    dq, start, index + 1 );
            break;

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}

#endif /* ! CORE_API */


#if 0
const FrAdcData& adcDataQuery(
    const Version::AdcDataContainer& c, Query& q, std::vector< size_t >& start,
    size_t index )
{
    if ( index == start.size() )
    {
        start.push_back( 0 );
    }

    if ( q.getQuery() == "channel" )
    {
        INT_4U channel = strtoul( q.getValue().c_str(), 0, 10 );
        AdcDataContainer::const_iterator iter = c.begin();
        while( iter != c.end() && (*iter)->getChannelNumber() != channel )
        {
            ++iter;
        }

        if ( iter == c.end() )
        {
            throw SWIGEXCEPTION( "not_found" );
        }

        start[ index ] = iter - c.begin();
        return **iter;
    }
    else
    {
#if FRAME_SPEC_CURRENT <= 5
        return getContained< FrAdcData, &FrAdcData::getName >
	    ( c, q, start, index );
#else
        return getContained< FrAdcData, &FrAdcData::GetName >
	    ( c, q, start, index );
#endif
    }
}
#endif /* 0 */
//!ignore_end:

//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrRawData.
//
//!usage_ooi: FrRawData* rawData = container2rawData( c );
//
//!param: const LdasContainer& c - FrRawData container.
//
//!return: FrRawData* rawData - newly allocated FrameCPP::FrRawData.
//
//!exc: SwigException - invalid FrRawData container data.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

FrRawData* container2rawData( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
   
    if ( c.getNameSize() < 3 ||
         strcasecmp( c_name.c_str(), "RawData" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not RawData container." );
    }
    
    FrRawData* rawData = new FrRawData( c.getName( 0 ) );

    try
    {
        const LdasContainer* serData( findContainerType( c, "serData" ) );
        if ( serData != 0 )
        {
            for ( unsigned int i = 0; i < serData->size(); ++i )
            {
		FrRawData::firstSer_type::value_type
		    s( container2serData( dynamic_cast< const LdasContainer& >( *(*serData)[ i ] ) ) );

                rawData->RefFirstSer().append( s );
            }
        }
        
        const LdasContainer* adcData( findContainerType( c, "adcData" ) );
        if ( adcData != 0 )
        {
            for ( unsigned int i = 0; i < adcData->size(); ++i )
            {
		FrRawData::firstAdc_type::value_type
		    a( container2adcData( dynamic_cast< const LdasContainer& >( *(*adcData)[ i ] ) ) );

                rawData->RefFirstAdc().append( a );
            }
        }

        const LdasContainer* table( findContainerType( c, "table" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
		FrRawData::firstTable_type::value_type
		    t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) ) );

                rawData->RefFirstTable().append( t );
            }
        }
        
        const LdasContainer* msg( findContainerType( c, "logMsg" ) );
        if ( msg != 0 )
        {            
            for ( unsigned int i = 0; i < msg->size(); ++i )
            {
		FrRawData::logMsg_type::value_type
		    m( container2msg( dynamic_cast< const LdasContainer& >( *(*msg)[ i ] ) ) );

                rawData->RefLogMsg().append( m );
            }
        }
        
        const LdasContainer* more( findContainerType( c, "more" ) );
        if ( more != 0 )
        {
            for ( unsigned int i = 0; i < more->size(); ++i )
            {
		FrRawData::more_type::value_type
		    v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*more)[ i ] ) ) );

                rawData->RefMore().append( v );
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete rawData;
      rawData = (FrameCPP::FrRawData*)NULL;
      throw SWIGEXCEPTION( "invalid_input_format" );
    }
    
    return rawData;
}

#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Convert FrRawData to ILWD.
//
//!usage_ooi: LdasContainer* rawDataContainer =
//+	rawData2container( rawData, gtime, dt );
//
//!param: const FrRawData& rawdata - FrameCPP::FrRawData to convert.
//!param: const Time& gtime - FrameCPP::Frame time.
//!param: const REAL_8& dt - FrameCPP::Frame delta time.
//!param: const std::vector<FrameCPP::Version::DaqFrame::adc_update_struct>* adcUpdateVect -
//+	If it is not zero, then it controls which ADC structures get converted.
//
//!return: LdasContainer* rawDataContainer - newly allocated FrRawData container.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

LdasContainer* rawData2container( const FrRawData& rawdata, const Time& gtime,
                                  const REAL_8& dt
#if DAQ_SUPPORT
				  , const std::vector<FrameCPP::Version::DaqFrame::adc_update_struct>* adcUpdateVect
#endif /* DAQ_SUPPORT */
				  )
{
    LdasContainer* c = new LdasContainer( "::RawData:Frame" );
    c->setName( 0, rawdata.GetName() );
    
    size_t serialSize( rawdata.RefFirstSer().size() );
    if ( serialSize > 0 )
    {
        LdasContainer* serData(
            new LdasContainer( ":serData:Container(SerData):Frame" ) );
        c->push_back( serData,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrRawData::const_firstSer_iterator iter( rawdata.RefFirstSer().begin() );
        for( size_t i = serialSize; i != 0; --i, ++iter )
        {
            serData->push_back( serData2container( **iter, gtime, dt ),
				ILwd::LdasContainer::NO_ALLOCATE,
				ILwd::LdasContainer::OWN );
        }
    }

#if DAQ_SUPPORT
    // Convert only activated ADC structures if we are using DaqFrame
    if ( adcUpdateVect ) {
      size_t adcSize( adcUpdateVect->size() );
      if ( adcSize > 0 )
	{
	  LdasContainer* adcdata(
				 new LdasContainer( ":adcData:Container(AdcData):Frame" ) );
	  c->push_back( adcdata,
			ILwd::LdasContainer::NO_ALLOCATE,
			ILwd::LdasContainer::OWN );
        
	  for( size_t i = adcSize; i != 0; --i )
	    {
	      adcdata->push_back( adcData2container( *((*adcUpdateVect)[i-1].adc), gtime, dt ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    }
	}
      
    } else {
#endif /* DAQ_SUPPORT */
      size_t adcSize( rawdata.RefFirstAdc().size() );
      if ( adcSize > 0 )
	{
	  LdasContainer* adcdata(
				 new LdasContainer( ":adcData:Container(AdcData):Frame" ) );
	  c->push_back( adcdata,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
        
	  FrRawData::const_firstAdc_iterator
	      iter( rawdata.RefFirstAdc().begin() );
	  for( size_t i = adcSize; i != 0; --i, ++iter )
	    {
	      adcdata->push_back( adcData2container( **iter, gtime, dt ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    }
	}
#if DAQ_SUPPORT
    }
#endif /* DAQ_SUPPORT */

    size_t tableSize( rawdata.RefFirstTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer* table(
            new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrRawData::const_firstTable_iterator iter( rawdata.RefFirstTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    size_t msgSize( rawdata.RefLogMsg().size() );
    if ( msgSize > 0 )
    {
        LdasContainer* msg(
            new LdasContainer( ":logMsg:Container(Msg):Frame" ) );
        c->push_back( msg,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrRawData::const_logMsg_iterator iter( rawdata.RefLogMsg().begin() );
        for( size_t i = msgSize; i != 0; --i, ++iter )
        {
            msg->push_back( msg2container( **iter ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
        }
    }
    
    size_t moreSize( rawdata.RefMore().size() );
    if ( moreSize > 0 )
    {
        LdasContainer* more(
            new LdasContainer( ":more:Container(Vect):Frame" ) );
        c->push_back( more,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrRawData::const_more_iterator iter( rawdata.RefMore().begin() );
        for( size_t i = moreSize; i != 0; --i, ++iter )
        {
            more->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    return c;
}

#endif /* ! CORE_API */

#if ! CORE_API

void insertRawData( LdasContainer& fr, LdasContainer& r )
{
    // Find the FrRawData container in the frame
    LdasElement* t = fr.find( "rawdata", 1 );
    if ( t != 0 )
    {
        throw SWIGEXCEPTION( "rawdata_exists" );
    }
    
    fr.push_back( r );
}

#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Concatenate two FrRawData ILWD containers.
//
//!usage_ooi: LdasContainer* concatRawDatacontainer = concatRawData( c1, c2 );
//
//!param: const LdasContainer* c1 - first FrRawData container.
//!param: const LdasContainer* c2 - second FrRawData container.
//
//!return: LdasContainer* concatRawDatacontainer - newly allocated LDAS container.
//
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

LdasContainer* concatRawData( const LdasContainer* c1,
   const LdasContainer* c2 )
try
{
   LdasContainer* rawData( 0 );
   try
   {
      rawData = new LdasContainer( *c1 );
      const LdasContainer* e1( findContainerType( *c1, "serData" ) );
      const LdasContainer* e2( findContainerType( *c2, "serData" ) );
      LdasContainer* cdata( 0 );
      LdasElement* elem( rawData->find( "serData", 1 ) );
      if( elem != 0 )
      {     
         if( elem->getElementId() != ILwd::ID_ILWD ||
             e1->size() == 0 || e2 == 0 )
         {
            throw SWIGEXCEPTION( "bad_rawdata: serData" );
         }
         if ( e1->size() != e2->size() )
         {
            throw SWIGEXCEPTION( "incompatible_rawdata: serData" );
         }
         cdata = dynamic_cast< LdasContainer* >( elem );
         cdata->erase( cdata->begin(), cdata->end() );
         for ( unsigned int i = 0; i < e1->size(); ++i )
         {
	   
            cdata->push_back( concatSerData
			      ( dynamic_cast< const LdasContainer* >
				( ( *e1 )[ i ] ),
				dynamic_cast< const LdasContainer* >
				( ( *e2 )[ i ] ) ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
         }
      }
       
      elem = rawData->find( "adcData", 1 );
      if( elem != 0 )
      {
         if( elem->getElementId() != ILwd::ID_ILWD )
         {
            throw SWIGEXCEPTION( "bad_rawdata: adcData" );
         } 
         e1 = findContainerType( *c1, "adcData" );
         e2 = findContainerType( *c2, "adcData" );
         if( e1->size() == 0 || e2 == 0 ||
             e1->size() != e2->size() ) 
         {
            throw SWIGEXCEPTION( "incompatible_rawdata: adcdata" );
         }

         cdata = dynamic_cast< LdasContainer* >( elem );
         cdata->erase( cdata->begin(), cdata->end() );
         for ( unsigned int i = 0; i < e1->size(); ++i )
         {
            cdata->push_back( concatAdcData
			      ( dynamic_cast< const LdasContainer* >
				( ( *e1 )[ i ] ),
				dynamic_cast< const LdasContainer* >
				( ( *e2 )[ i ] ) ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
         }
      }      

      elem = rawData->find( "table", 1 );
      if( elem != 0 )
      {
         if( elem->getElementId() != ILwd::ID_ILWD )
         {
            throw SWIGEXCEPTION( "bad_rawdata: table" );
         } 
         e1 = findContainerType( *c1, "table" );
         e2 = findContainerType( *c2, "table" );
         if( e1->size() == 0 || e2 == 0 ||
             e1->size() != e2->size() ) 
         {
            throw SWIGEXCEPTION( "incompatible_rawdata: table" );
         }

         cdata = dynamic_cast< LdasContainer* >( elem );
         cdata->erase( cdata->begin(), cdata->end() );
         for ( unsigned int i = 0; i < e1->size(); ++i )
         {
            
            cdata->push_back(  concatTable
			       ( dynamic_cast< const LdasContainer* >
				 ( ( *e1 )[ i ] ),
				 dynamic_cast< const LdasContainer* >
				 ( ( *e2 )[ i ] ) ),
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::OWN );
         }
      }
        
      elem = rawData->find( "logMsg", 1 );
      if( elem != 0 )
      {
         if( elem->getElementId() != ILwd::ID_ILWD )
         {
            throw SWIGEXCEPTION( "bad_rawData: logMsg" );
         }
         e1 = findContainerType( *c1, "logMsg" );
         e2 = findContainerType( *c2, "logMsg" );
         if ( e1->size() == 0 || e2 == 0 )
         {
            throw SWIGEXCEPTION( "incompatible_rawdata: logMsg" );
         }
   
         if( e2->size() != 0 )
         {
            cdata = dynamic_cast< LdasContainer* >( elem );
            // Append message log data of the second container:
            for ( unsigned int i = 0; i < e2->size(); ++i )
            {
               cdata->push_back( dynamic_cast< const LdasContainer* >(
                                 ( *e2 )[ i ] ) );
            }
         }      
      }

      elem = rawData->find( "more", 1 );
      if( elem != 0 )
      {
         if( elem->getElementId() != ILwd::ID_ILWD )
         {
            throw SWIGEXCEPTION( "bad_rawData: more" );
         }
         e1 = findContainerType( *c1, "more" );
         e2 = findContainerType( *c2, "more" );
         if ( e1->size() == 0 || e2 == 0 ||
              e1->size() != e2->size() )
         {
            throw SWIGEXCEPTION( "incompatible_rawdata: more" );
         }
   
         cdata = dynamic_cast< LdasContainer* >( elem );
         cdata->erase( cdata->begin(), cdata->end() );
         //:TODO: For now only one element container( 'more' ) is 
         // supported, this should change in a future ===> concatData
         // should be LdasContainer.
         cdata->push_back( concatElementData( e1, e2 ),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      }
   }
   catch(...)
   {
      delete rawData;
      rawData = (ILwd::LdasContainer*)NULL;
      throw;
   }

   return rawData;
}
catch( std::bad_cast& )
{
   throw SWIGEXCEPTION( "rawdata: invalid_format" );
}  
 
#endif /* ! CORE_API */
