#ifndef FrameApiProcDataHH
#define FrameApiProcDataHH

#include <deque>
#include <list>
#include <string>

#include "framecpp/FrProcData.hh"
#include "framecpp/Time.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

namespace FrameAPI
{
  namespace FrProcData
  {
    FrameCPP::FrProcData* cloneHeader( const FrameCPP::FrProcData& Source );
    FrameCPP::FrProcData* concat( const std::list< FrameCPP::Base* >& Segment );
  }
}

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasContainer*
concatProcData( const ILwd::LdasContainer* c1,
		const ILwd::LdasContainer* c2 );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
FrameCPP::FrProcData*
container2procData( const ILwd::LdasContainer& c );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

std::string
getAttribute( const FrameCPP::FrProcData& procData,
	      std::deque< Query >& q,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrProcData& procData,
	 std::deque< Query >& q,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
void
insertProcData( ILwd::LdasContainer& fr, ILwd::LdasContainer& proc,
		const bool validateTime = true );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasContainer*
procData2container( const FrameCPP::FrProcData& proc,
		    const FrameCPP::Time& gtime );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiProcDataHH
