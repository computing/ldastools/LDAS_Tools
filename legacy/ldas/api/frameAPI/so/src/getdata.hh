#ifndef getDataHH
#define getDataHH

// System Header Files   
#include <deque>
#include <sstream>      
   
// ILWD Header Files      
#if HAVE_LDAS_PACKAGE_ILWD
#include <ilwd/ldasarraybase.hh>
#endif /* HAVE_LDAS_PACKAGE_ILWD */

// GenericAPI Header Files      
#include <genericAPI/swigexception.hh>
   
#include "getattribute.hh"


#endif // getDataHH
