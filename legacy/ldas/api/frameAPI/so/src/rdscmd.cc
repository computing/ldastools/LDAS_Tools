#include "config.h"

#include "genericAPI/thread.hh"

#include "rdscmd.hh"
#include "rdsreduce.hh"   
#include "rdsresample.hh"
#include "RDSStreamFile.hh"
#include "RDSStreamFileResample.hh"   

namespace
{
  inline std::string
  format_results( const RDSStreamFile::result_type& Results )
  {
    std::ostringstream	retval;

    for( RDSStreamFile::result_type::const_iterator
	   first = Results.begin( ),
	   cur = Results.begin( ),
	   last = Results.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	retval << " ";
      }
      retval << *cur;
    }
    return retval.str( );
  }

  std::string
  reduceRawFrames( const char* frames, 
		   const char* channels,
		   const ::FrameAPI::RDS::FileOptions UserOptions )
  {
    RDSFrame::stream_type	output;
    RDSStreamFile*
      stream( new RDSStreamFile( UserOptions.OutputDirectory( ),
				 UserOptions.OutputType( ),
				 UserOptions.MD5SumOutputDirectory( ) ) );
    ReduceRawFrame	rds( frames, channels, UserOptions );
   
    output.reset( stream );
    rds.ProcessRequest( output );
    return format_results( stream->Results( ) );

  }

   
  std::string resampleRawFrames( const char* frames, 
				 const char* channels,
				 const char* resample,
				 const ::FrameAPI::RDS::FileOptions UserOptions )
  {
    RDSFrame::stream_type	output;
    RDSStreamFileResample*
      stream( new RDSStreamFileResample( UserOptions.OutputDirectory( ),
					 UserOptions.OutputType( ),
					 UserOptions.MD5SumOutputDirectory( ) ) );
    ResampleRawFrame		rds( frames, channels, resample, UserOptions );

    output.reset( stream );
    rds.ProcessRequest( output );
    return format_results( stream->Results( ) );
  }
} // namespace - anonymous
   
// RDS   
CREATE_THREADED3( reduceRawFrames, std::string, 
                  const char*, const char*,
		  const ::FrameAPI::RDS::FileOptions )
   
CREATE_THREADED4( resampleRawFrames, std::string, 
                  const char*, const char*, 
                  const char*,
		  const ::FrameAPI::RDS::FileOptions )

