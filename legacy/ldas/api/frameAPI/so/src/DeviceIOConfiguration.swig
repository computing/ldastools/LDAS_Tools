// -*- Mode: C++; c-basic-offset: 2; -*-

%{
#include <memory>
#include <sstream>

#include "general/ErrorLog.hh"

#include "DeviceIOConfiguration.hh"

using namespace FrameAPI;

%}

%typemap(in) DeviceIOConfiguration::descriptions_type* {
  try
  {
    std::unique_ptr< DeviceIOConfiguration::descriptions_type >
      retval( new DeviceIOConfiguration::descriptions_type );

  // :TODO: Parse the argument list
#if SWIGTCL
    retval = TclObjToDeviceIOConfigurationDescriptionsType( interp, $input );
#else
  // This forces a compilation error
    0 = 1; // ERROR: Unsupported Swig language at __FILE__ __LINE__
#endif  

    // Return the list.
    $1 = retval.release( );
  }
  catch( const std::exception& e )
  {
    std::ostringstream	msg;

#if SWIGTCL
    msg << "ERROR: Unable to convert Device I/O Configuration: " << $input
	<< " : " << e.what( );
    Tcl_SetResult( interp,
		   const_cast<char*>( msg.str( ).c_str( ) ),
		   TCL_VOLATILE );
    return TCL_ERROR;
#else
  // This forces a compilation error
    0 = 1; // ERROR: Unsupported Swig language at __FILE__ __LINE__
#endif  
  }
}

%typemap(freearg) DeviceIOConfiguration::descriptions_type* {
   delete $1;
}

%inline %{
  string
  ResetDeviceIOConfiguration( DeviceIOConfiguration::descriptions_type* Desc )
  {
    // Reset the device configuration list
    return DeviceIOConfiguration::Reset( *Desc );
  }

  string
  GetDeviceIOConfiguration( const char* Filename )
  {
    DeviceIOConfiguration::size_type	size;
    bool				use_memory_mapped_io;
    std::string				fs_type;

    DeviceIOConfiguration::GetConfiguration( Filename,
					     size,
					     use_memory_mapped_io,
					     fs_type );
    std::ostringstream oss;
    oss << Filename
	<< " " << size
	<< " " << use_memory_mapped_io
	<< " " << fs_type;

    return oss.str( );
  }
%}

// release the definitions for these things
%typemap(in) DeviceIOConfiguration::descriptions_type*;
%typemap(freearg) DeviceIOConfiguration::descriptions_type*;

%{
namespace
{
  int
  get_size( const char* SizeString )
  {
    static const int k = 1024;
    static const int M = k * k;
    static const int G = k * M;

    int		retval;
    std::string	mult;
    std::istringstream	SizeStream( SizeString );
    SizeStream >> retval >> mult;

    if ( mult.size( ) > 0 )
    {
      switch( mult[0] )
      {
      case 'k': retval *= k; break;
      case 'M': retval *= M; break;
      case 'G': retval *= G; break;
      }
    }
    return retval;
  }
}
namespace FrameAPI
{
  std::unique_ptr< DeviceIOConfiguration::descriptions_type >
  TclObjToDeviceIOConfigurationDescriptionsType( Tcl_Interp* Interp,
						Tcl_Obj* Obj )
  {
    std::ostringstream	msg;

    //-------------------------------------------------------------------
    // Setup the return value
    //-------------------------------------------------------------------
    std::unique_ptr< DeviceIOConfiguration::descriptions_type >
      retval( new DeviceIOConfiguration::descriptions_type );
    
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // [ list [ list dev1 cache1 mmap1 ] [ list dev2 cache2 mmap2 ] ... ]
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    Tcl_Obj	**deviceobjv;
    int	nitems;

    if ( Tcl_ListObjGetElements( Interp, Obj, &nitems, &deviceobjv )
	 != TCL_OK )

    {
      msg << "ERROR: Unable to split";
      throw std::runtime_error( msg.str( ) );
    }
    retval->resize( nitems );

    for ( int i = 0; i < nitems; ++i )
    {
      Tcl_Obj**	desclistobj;
      int		descnitems;

      if ( ( Tcl_ListObjGetElements( Interp, deviceobjv[ i ],
				     &descnitems, &desclistobj )
	     != TCL_OK )
	   || ( descnitems != 3 ) )
      {
	msg << "ERROR: Malformed structure";
	throw std::runtime_error( msg.str( ) );
      }
      int	data;

      (*retval)[ i ].s_name = Tcl_GetString( desclistobj[ 0 ] );
      (*retval)[ i ].s_characteristics.s_buffer_size =
	get_size( Tcl_GetString( desclistobj[ 1 ] ) );
      Tcl_GetIntFromObj( Interp, desclistobj[ 2 ], &data );
      (*retval)[ i ].s_characteristics.s_use_mmap_io = data;
    }
    return retval;
  } // func -   TclObjToDeviceIOConfigurationDescriptionsType( Tcl_Obj* Obj )
} // namespace - FrameAPI
%}
