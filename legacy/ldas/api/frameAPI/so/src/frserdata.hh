#ifndef FrameApiSerDataHH
#define FrameApiSerDataHH

#include <deque>
#include <string>

#include "framecpp/FrSerData.hh"
#include "framecpp/Time.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrSerData& serData,
	      std::deque< Query >& q,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD

ILwd::LdasElement*
getData( const FrameCPP::FrSerData& serData,
	 std::deque< Query >& q,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime,
	 const REAL_8& dt );

FrameCPP::FrSerData* container2serData( const ILwd::LdasContainer& c );

ILwd::LdasContainer*
serData2container( const FrameCPP::FrSerData& ser,
		   const FrameCPP::Time& gtime,
		   const REAL_8& dt, ILwd::LdasContainer* c = 0 );

void
insertSerData( ILwd::LdasContainer& fr,
	       ILwd::LdasContainer& ser,
	       const bool validateTime = true );

//!exc: SwigException      
ILwd::LdasContainer*
concatSerData( const ILwd::LdasContainer* c1,
	       const ILwd::LdasContainer* c2 );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiSerDataHH
