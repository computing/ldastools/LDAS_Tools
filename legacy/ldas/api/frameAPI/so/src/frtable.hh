#ifndef FrameApiTableHH
#define FrameApiTableHH

#include <deque>
#include <string>

#include "framecpp/FrTable.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

namespace FrameAPI
{
  namespace FrProcData
  {
    void merge( FrameCPP::FrTable& Primary,
		const FrameCPP::FrTable& Secondary );
  }
}

std::string
getAttribute( const FrameCPP::FrTable& table,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrTable& table,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index );

FrameCPP::FrTable* container2table( const ILwd::LdasContainer& c );

ILwd::LdasContainer* table2container( const FrameCPP::FrTable& table );

//!exc: SwigException      
ILwd::LdasContainer*
concatTable ( const ILwd::LdasContainer* c1,
	      const ILwd::LdasContainer* c2 );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiTableHH
