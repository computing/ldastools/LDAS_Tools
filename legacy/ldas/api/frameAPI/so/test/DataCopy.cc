#include "createFrameGroupCommon.hh"

#include <sstream>

int
main( int ArgC, char** ArgV )
{
  // --------------------------------------------------------------------
  //  Initialize
  // --------------------------------------------------------------------
  init_tests( );
  const char* destdir = getenv( "DATA_DESTDIR" );
  if ( destdir == (const char*)NULL )
  {
    std::cerr << "Please set the environment variable DATA_DESTDIR"
	      << " to be the root of where to copy the data"
	      << std::endl
      ;
    exit( 1 );
  }
  // --------------------------------------------------------------------
  //  
  // --------------------------------------------------------------------
  for ( std::list< test_data_type >::const_iterator
	  cur = test_data.begin( ),
	  last = test_data.end( );
	cur != last;
	++cur )
  {
    for ( ConditionData::frame_files_type::const_iterator
	    cur_file = cur->s_files.begin( ),
	    last_file = cur->s_files.end( );
	  cur_file != last_file;
	  ++cur_file )
    {
      std::string	dest( destdir );
      dest += *cur_file;
      std::string	ddir( dest.substr( 0, dest.rfind( '/' ) ) );

      std::ostringstream	cmd;

      cmd << "mkdir -p " << ddir;
      ::system( cmd.str( ).c_str( ) );

      cmd.str( "" );
      cmd << "cp " << *cur_file << " " << ddir ;
      ::system( cmd.str( ).c_str( ) );
    }
  }
  // --------------------------------------------------------------------
  //  return
  // --------------------------------------------------------------------
  return 0;
}
