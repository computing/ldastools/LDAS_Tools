#include "../src/config.h"

#include "general/unittest.h"
#include "general/regex.hh"
#include "general/regexmatch.hh"

#include "filereader.hh"

General::UnitTest	Test;

namespace
{
  void
  bad_file( const char* Filename, const char* Pattern )
  {
    std::ostringstream	p;
    p << "(file type: " << Pattern << ")";
    Regex		r( p.str( ).c_str( ), REG_EXTENDED );
    RegexMatch		compare( 1 );

    try
    {
      FileReader	fr( Filename );
    }
    catch( const std::exception& Exception )
    {
      bool condition = compare.match( r, Exception.what( ) );
      Test.Check( condition ) << "Caught std::exception: "
			      << Exception.what( )
			      << std::endl;
    }
    catch( ... )
    {
      Test.Check( false ) << "Caught unknown exception" << std::endl;
    }
  }
} // namespace - anonymous

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Initialize the testing environment
  //---------------------------------------------------------------------

  Test.Init( ArgC, ArgV );
  try
  {
    FrameCPP::Initialize( );

    //-------------------------------------------------------------------
    // Exception checks
    //-------------------------------------------------------------------
    bad_file( "stdFrameTest.o", "binary elf" );
    bad_file( "stdFrameTest", "text #! /bin/.*sh" );
  }
  catch( const std::exception& E )
  {
    std::ostringstream	msg;
    msg << E.what( );
    Test.Check( false ) << msg.str( );
  }

  //---------------------------------------------------------------------
  // Terminate in a way that make check understands.
  //---------------------------------------------------------------------

  Test.Exit( );
}
