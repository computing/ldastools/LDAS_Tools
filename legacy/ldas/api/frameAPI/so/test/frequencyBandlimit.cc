#include "../src/config.h"

#include <iostream>
#include <stdexcept>
#include <cstring>   
#include <algorithm>   
   
// LDAS Header Files
#include <general/util.hh>
#include <general/unittest.h>   
#include <general/objectregistry.hh>
#include <general/ldasexception.hh>
#include <genericAPI/util.hh>
#include <genericAPI/registry.hh>
#include <genericAPI/swigexception.hh>
#include <genericAPI/ilwdfile.hh>  
   
// ILWD Header Files
#include <ilwd/reader.hh>
#include <ilwd/util.hh>      
   
// FrameAPI Local Header Files
#include "framecmd.hh"
#include "convert.hh"   
   
#include <ospace/stream/tstream.h>   

using namespace ILwd;   
using namespace FrameCPP;   
using namespace std;  
   
   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true    
   
General::UnitTest freqBandTest;

static const char* procdata_file( "../../../../../api/test/frameAPI/procdata.gwf" );      
static void reportError( const string& error_msg );   
   
   
int main( int argc, char** argv )
try   
{
   // Initialize test
   freqBandTest.Init( argc, argv );
   
   // Read frame with ProcData
   FrameFile* fh( openFrameFile( procdata_file, "r" ) );
   FrameH* frame( readFrame( fh ) );
   closeFrameFile( fh );
   
   // Extract ProcData
   FrProcData* proc_data( getFrameProcData( frame, "0" ) );   

   try
   {
      // Test invalid_offset
      double offset( -1.0f );
      double delta( 0.0f );
   
      getFrameProcDataSlice( proc_data, offset, delta );
      reportError( "invalid_procdata_offset exception wasn't generated." );
   }
   catch( const SwigException& exc )
   {
      // If verbose mode is off, something goes really wrong with
      // /dev/null ---> generating os error that gets appended to the 
      // SwigException, which will make 'Check' to fail. Just search for 
      // the substring instead of comparing two strings. 
      const string expected_msg( "invalid_procdata_offset" );
      const string msg( exc.getResult() );
      string::const_iterator pos( search( msg.begin(), msg.end(), 
                                  expected_msg.begin(), expected_msg.end() ) );   
      
      freqBandTest.Check( pos != msg.end() )
         << " testing invalid_offset exception: got \"" 
         << msg << "\"" << endl;
   }
   

   try
   {
      // Test invalid_delta
      double offset( 0.0f );
      double delta( 18.0f );
   
      getFrameProcDataSlice( proc_data, offset, delta );
      reportError( " invalid_procdata_dt exception wasn't generated." );
   }
   catch( const SwigException& exc )
   {
      // If verbose mode is off, something goes really wrong with
      // /dev/null ---> generating os error that gets appended to the 
      // SwigException, which will make 'Check' to fail. Just search for 
      // the substring instead of comparing two strings.
      const string expected_msg( "invalid_procdata_dt" );
      const string msg( exc.getResult() );
      string::const_iterator pos( search( msg.begin(), msg.end(), 
                                  expected_msg.begin(), expected_msg.end() ) );
   
      freqBandTest.Check( pos != msg.end() )
         << " testing invalid_dt exception: got \"" 
         << msg << "\"" << endl;
   }
   
   
   // Ask for the whole range
   double offset( 0.0f );
   double delta( 17.0f );
   const unsigned int data_num1( 17 );
    
   // Newly created ProcData objects are taken care of automatically
   FrProcData* slice( getFrameProcDataSlice( proc_data, offset, delta ) );
   
   INT_4U index_offset( 0 );
   INT_4U index_delta( 17 );
   FrProcData* slice_index( getFrameProcDataSliceIndex( proc_data, 
      index_offset, index_delta ) );         
   
#if 0
   // Compare data:
   freqBandTest.Check( *slice == *slice_index )
      << " comparing results of getFrameProcDataSlice and "
      << "getFrameProcDataSliceIndex" << endl;
#endif
   
   
   if( slice->refData().getSize() == 0 )
   {
      reportError( "valid test#1: Data is missing for test ProcData." );
   }
   
   FrVect* slice_data( slice->refData()[ 0 ] );
   
   freqBandTest.Check( slice_data->getNData() == data_num1 )
      << " testing number of points for the whole range: "
      << "expected=" << data_num1
      << " received=" << slice_data->getNData() << endl;
   
   // Test range smaller than data
   offset = 2.0f;
   delta = 5.0f;
   const unsigned int data_num2( 5 );
   slice = getFrameProcDataSlice( proc_data, offset, delta );         
   if( slice->refData().getSize() == 0 )
   {
      reportError( "valid test#2: Data is missing for test ProcData" );
   }
   
   slice_data = slice->refData()[ 0 ];
   freqBandTest.Check( slice_data->getNData() == data_num2 )
      << " testing number of points for subrange: "
      << "expected=" << data_num2
      << " received=" << slice_data->getNData() << endl;   
   
   
   // Test range with offset falling between samples
   offset = 2.5f;
   delta = 5.0f;
   const unsigned int data_num3( 5 );
   slice = getFrameProcDataSlice( proc_data, offset, delta );         
   if( slice->refData().getSize() == 0 )
   {
      reportError( "valid test#3: Data is missing for test ProcData" );
   }
   
   slice_data = slice->refData()[ 0 ];
   freqBandTest.Check( slice_data->getNData() == data_num3 )
      << " testing number of points for subrange with offset between samples: "
      << "expected=" << data_num3
      << " received=" << slice_data->getNData() << endl;      
   
   Registry::elementRegistry.reset();                
   
   freqBandTest.Exit();
   return 0;
}
catch( const exception& e )
{
   reportError( e.what() );
}
catch( const LdasException& exc )
{
   reportError( SwigException( exc ).getResult() );
}      
catch( SwigException& e )
{
   reportError( e.getResult() );
}       
catch( ... )
{
   reportError( "unknown exception" );
}

   
void reportError( const string& error_msg )
{
   // cleanup memory
   Registry::elementRegistry.reset();                   
   
   freqBandTest.Check( false ) << error_msg << endl;
   freqBandTest.Exit();      

   return;
}
