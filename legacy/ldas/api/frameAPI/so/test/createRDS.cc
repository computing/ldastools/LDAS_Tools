#include "../src/config.h"

#include <stdlib.h>
#include <unistd.h>

#include "general/AtExit.hh"
#include "general/unittest.h"

#include "framecmd.hh"
#include "rdsreduce.hh"
#include "rdsresample.hh"
#include "RDSStreamFile.hh"
#include "RDSStreamFileResample.hh"
#include "createRDS.hh"
#include "Frame.hh"
#include "Channel.hh"

#include "createFrameGroupCommon.hh"

#define REDUCE		0
#define RESAMPLE	1

struct rds_data_type {
  INT_4U				s_start_time;
  INT_4U				s_end_time;
  std::list< std::string >		s_filenames;
  std::list< channel_info_type >	s_channels;
};

typedef std::list< rds_data_type >	test_type;

General::UnitTest	Test;

char cwd[2048];

namespace {
  inline std::string
  format_results( const RDSStreamFile::result_type& Results )
  {
    std::ostringstream	retval;

    for( RDSStreamFile::result_type::const_iterator
	   first = Results.begin( ),
	   cur = Results.begin( ),
	   last = Results.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	retval << " ";
      }
      retval << *cur;
    }
    return retval.str( );
  }
}

std::string
filenames( const std::list< std::string >& fn )
{
  std::string	retval;
  bool		first( true );

  retval = "{ ";
  for ( std::list< std::string >::const_iterator current( fn.begin( ) ),
	  end( fn.end( ) );
	current != end;
	current++ )
  {
    if ( ! first )
    {
      retval += ", ";
    }
    else
    {
      first = false;
    }
    retval += *current;
  }
  retval += "}";
  return retval;
}

bool
needs_resample( const std::list< channel_info_type >& ch )
{
  bool retval( false );
      
  for ( std::list< channel_info_type >::const_iterator current( ch.begin( ) ),
	  end( ch.end( ) );
	current != end;
	current++ )
  {
    if ( (*current).s_q != 1 )
    {
      retval = true;
      break;
    }
  }
  return retval;
}

std::string
channels( const std::list< channel_info_type >& ch )
{
  std::string	retval;
  bool		first( true );

  retval = "{ ";
  for ( std::list< channel_info_type >::const_iterator current( ch.begin( ) ),
	  end( ch.end( ) );
	current != end;
	current++ )
  {
    if ( ! first )
    {
      retval += ", ";
    }
    else
    {
      first = false;
    }
    retval += (*current).s_name;
  }
  retval += "}";
  return retval;
}

std::string
sample_rates( const std::list< channel_info_type >& ch )
{
  std::ostringstream	retval;
  bool			first( true );

  retval << "{ ";
  for ( std::list< channel_info_type >::const_iterator current( ch.begin( ) ),
	  end( ch.end( ) );
	current != end;
	current++ )
  {
    if ( ! first )
    {
      retval << ", ";
    }
    else
    {
      first = false;
    }
    retval << (*current).s_q;
  }
  retval << "}";
  return retval.str( );
}

void
test_set( const std::string& Function,
	  const std::string& Type,
	  const test_type& Set,
	  const INT_4U FramesPerFile = 0,
	  const INT_4U SecondsPerFrame = 0 )
{
  try
  {
    std::string	output_filenames;
    //-------------------------------------------------------------------
    // Do simple tests of frame reduction
    //-------------------------------------------------------------------
    for ( test_type::const_iterator cur_test( Set.begin( ) ),
	    last_test( Set.end( ) );
	  cur_test != last_test;
	  cur_test++ )
    {
      if ( (*cur_test).s_filenames.size( ) == 0 )
      {
	Test.Message( ) << "Skipping test because no files found" << std::endl;
	continue;
      }
      if ( needs_resample( (*cur_test).s_channels ) )
      {
#if RESAMPLE
	Test.Message( ) << "Resampling" << std::endl;
	//---------------------------------------------------------------
	// Resampling
	//---------------------------------------------------------------
	std::string	file_list( filenames( (*cur_test).s_filenames ) );
	std::string	channel_list( channels( (*cur_test).s_channels ) );
	std::string	sample_rate_list( sample_rates( (*cur_test).s_channels ) );
	INT_4U		start( (*cur_test).s_start_time );
	INT_4U		end( (*cur_test).s_end_time );

	Test.Message( ) << "file_list: " << file_list << std::endl;

	::FrameAPI::RDS::FileOptions
	    opts( start,		// Start Time
		  end,			// End Time
		  cwd,			// Output Directory
		  Type.c_str( ),	// Output Type
		  "",			// Compression - raw
		  0,			// Compression - level
		  false,		// Verify Checksum
		  false,		// Verify Frame Checksum
		  false,		// Verify Time Range
		  false,		// Verify Data Valid
		  FramesPerFile,	// FramesPerFile
		  SecondsPerFrame,	// SecondsPerFrame
		  true,			// AllowShortFrames
		  true,			// GenerateFrameChecksum
		  true,			// FillMissingDataValidArray
		  false,		// VerifyFilenameMetadata
		  cwd );		// MD5SumOutputDirectory
	RDSStreamFileResample*
	  stream( new RDSStreamFileResample( opts.OutputDirectory( ),
					     opts.OutputType( ),
					     opts.RDSLevel( ),
					     opts.MD5SumOutputDirectory( ) ) );
	RDSFrame::stream_type	output;

	output.reset( stream );
	ResampleRawFrame
	  request( file_list.c_str( ),			// Filename list
		   channel_list.c_str( ),		// Channel list
		   sample_rate_list.c_str( ),		// Sample Rates
		   opts );
	request.ProcessRequest( output );
	output_filenames = format_results( stream->Results( ) );
#else /* RESAMPLE */
	continue;
#endif /* RESAMPLE */
      }
      else
      {
#if REDUCE
	Test.Message( ) << "Reduce" << std::endl;
	//---------------------------------------------------------------
	// No Resampling
	//---------------------------------------------------------------
	std::string	file_list( filenames( (*cur_test).s_filenames ) );
	std::string	channel_list( channels( (*cur_test).s_channels ) );
	INT_4U		start( (*cur_test).s_start_time );
	INT_4U		end( (*cur_test).s_end_time );

	Test.Message( ) << "file_list: " << file_list << std::endl;
	Test.Message( ) << "range: " << start << "-" << end << std::endl;

	::FrameAPI::RDS::FileOptions
	    opts( start,		// Start Time
		  end,			// End Time
		  cwd,			// Output Directory
		  Type.c_str( ),	// Output Type
		  "",			// Compression - raw
		  0,			// Compression - level
		  false,		// Verify Checksum
		  false,		// Verify Frame Checksum
		  false,		// Verify Time Range
		  false,		// Verify Data Valid
		  FramesPerFile,	// FramesPerFile
		  SecondsPerFrame,	// SecondsPerFrame
		  true,			// AllowShortFrames
		  true,			// GenerateFrameChecksum
		  true,			// FillMissingDataValidArray
		  false,		// VerifyFilenameMetadata
		  cwd );		// MD5SumOutputDirectory
	RDSStreamFile*
	  stream( new RDSStreamFile( opts.OutputDirectory( ),
				     opts.OutputType( ),
				     opts.RDSLevel( ),
				     opts.MD5SumOutputDirectory( ) ) );
	RDSFrame::stream_type	  output;

	output.reset( stream );
	ReduceRawFrame
	  request( file_list.c_str( ),			// Filename list
		   channel_list.c_str( ),		// Channel list
		   opts );
	try
	{
	  request.ProcessRequest( output );
	}
	catch( const std::exception& Exception )
	{
	  Test.Message( ) << "MESSAGE: exception: " << Exception.what( )
			  << " File: " << __FILE__
			  << " Line: " << __LINE__
			  << std::endl
	    ;
	  throw;
	}
	output_filenames = format_results( stream->Results( ) );
#else /* REDUCE */
	continue;
#endif /* REDUCE */
      }
      //-----------------------------------------------------------------
      // Verify the md5sums generated
      //-----------------------------------------------------------------
      if ( output_filenames.length( ) > 0 )
      {
	const int md5sum_check = system( "md5sum --check --status test.md5" );
	Test.Check( md5sum_check == 0 )
	  << "Verification of md5sum values"
	  << std::endl;
#if 0
	unlink( "test.md5" );
#endif /* 0 */
      }
      //-----------------------------------------------------------------
      // Remove files that were generated
      //-----------------------------------------------------------------
      Test.Message( ) << "output_filenames: " << output_filenames << std::endl;
      if ( output_filenames.length( ) > 0 )
      {
	std::string::size_type	start( 0 );
	std::string::size_type	end( output_filenames.find_first_of( ' ' ) );

	while( 1 )
	{
	  std::string::size_type	len ( end - start );
	  std::string framefilename( output_filenames.substr( start, len ) );
	  //-------------------------------------------------------------
	  // Open each frame file to get the number of frames in the
	  //   file. Must be greater than 0. Added to test PR# 2846
	  //-------------------------------------------------------------
	  INT_4U	fc = INT_4U( 0 );
	  FrameFile*	reader( openFrameFile( framefilename.c_str( ), "r" ) );

	  try
	  {
	    fc = getFrameNumber( reader );
	  }
	  catch( ... )
	  {
	  }
	  Test.Check( fc ) << "Number of frames in file must be greater than zero ( Found: " << fc << " frame" << ( ( fc == 1 ) ? "" : "s" ) << " )" << std::endl;
	  closeFrameFile( reader );
	  //-------------------------------------------------------------
	  // Do the removal of the output frames
	  //-------------------------------------------------------------
	  Test.Message( ) << "Removing file: "
			  << framefilename
			  << std::endl;
#if 0
	  unlink( framefilename.c_str( ) );
#endif /* 0 */
	  if ( end == std::string::npos )
	  {
	    break;
	  }
	  start = end+1;
	  end = output_filenames.find_first_of( ' ', start );
	}
      } // if ( output_filenames.length( ) > 0 )
    }
    Test.Check( true ) << Function << std::endl;
  }
  catch( const SwigException& e )
  {
    Test.Check( false ) << Function << ": Caught swig exception: "
			<< e.getResult( )
			<< " (" << e.getInfo( ) << " )"
			<< std::endl;
  }
  catch( const std::exception& e )
  {
    Test.Check( false ) << Function << ": Caught exception: " << e.what( )
			<< std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << Function << ": Caught unknown exception"
			<< std::endl;
  }
}

void
standard( )
{
  static const char* func( "standard" );

  test_type set1;

  try {
#if REDUCE
    set1.push_back( rds_data_type( ) );
    set1.back( ).s_start_time = GetReduceStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetReduceEndTime( IFO_LHO, 1 );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_channels.push_back( channel_info_type( ) );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 1;
#endif

#if RESAMPLE
    set1.push_back( rds_data_type( ) );

    set1.back( ).s_start_time = GetResampleStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetResampleEndTime( IFO_LHO, 2 );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 2 ) );

    set1.back( ).s_channels.push_back( channel_info_type( ) );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 8;
#endif
  }
  catch( const std::runtime_error& e )
  {
    Test.Message( ) << func << ": Test skipped due to no data being available"
		    << std::endl;
  }

  test_set( func, "", set1 );
}

void
multi_ldas( )
{
  static const char* func( "multi_ldas" );

  test_type set1;

  try {
    rds_data_type new_elem;
    channel_info_type new_channel;

#if REDUCE
    set1.push_back( new_elem );
    set1.back( ).s_start_time = GetReduceStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetReduceEndTime( IFO_LHO, 2 );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LLO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LLO, 2 ) );
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 1;
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "L1:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 1;
#endif
#if RESAMPLE
    set1.push_back( new_elem );

    set1.back( ).s_start_time = GetResampleStartTime( IFO_LHO, 0 );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LLO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LLO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 2 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LLO, 2 ) );
    set1.back( ).s_end_time = GetResampleEndTime( IFO_LHO, 2 );

    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 8;
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "L1:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 8;
#endif

  }
  catch( const std::runtime_error& e )
  {
    Test.Message( ) << func << ": Test skipped due to no data being available"
		    << std::endl;
  }

  test_set( func, "", set1 );
}

void
multi_frames_per_file( )
{
  static const char* func( "multi_frames_per_file" );

  test_type set1;

  try {
    rds_data_type new_elem;
    channel_info_type new_channel;

#if REDUCE
    set1.push_back( new_elem );
    set1.back( ).s_start_time = GetReduceStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetReduceEndTime( IFO_LHO, 3 );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 2 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 3 ) );
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 1;
#endif

#if RESAMPLE
    set1.push_back( new_elem );
    set1.back( ).s_start_time = GetResampleStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetResampleEndTime( IFO_LHO, 3 );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 2 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 3 ) );
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 8;
#endif
  }
  catch( const std::runtime_error& e )
  {
    Test.Message( ) << func << ": Test skipped due to no data being available"
		    << std::endl;
  }

  std::string	desc;

  desc = func;
  desc += " - 2 frames per file";
  test_set(  desc, "mfpf_2_per_file_default_sec", set1, 2 );
  desc = func;
  desc += " - 1 frame per file 32 seconds in length";
  test_set(  desc, "mfpf_1_per_file_32_sec", set1, 1, 32 );
}

void
multi_frames_per_file_short( )
{
  static const char* func( "multi_frames_per_file_short" );

  test_type set1;

  try {
    rds_data_type new_elem;
    channel_info_type new_channel;

#if REDUCE
    set1.push_back( new_elem );
    set1.back( ).s_start_time = GetReduceStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetReduceEndTime( IFO_LHO, 3 ) - 2;
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 2 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 3 ) );
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 1;
#endif

#if RESAMPLE
    set1.push_back( new_elem );
    set1.back( ).s_start_time = GetResampleStartTime( IFO_LHO, 0 );
    set1.back( ).s_end_time = GetResampleEndTime( IFO_LHO, 3 ) - 2;
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 0 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 1 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 2 ) );
    set1.back( ).s_filenames.push_back( GetFrameName( IFO_LHO, 3 ) );
    set1.back( ).s_channels.push_back( new_channel );
    set1.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    set1.back( ).s_channels.back( ).s_q = 8;
#endif
  }
  catch( const std::runtime_error& e )
  {
    Test.Message( ) << func << ": Test skipped due to no data being available"
		    << std::endl;
  }

  std::string	desc;

  desc = func;
  desc += " - 2 frames per file (short)";
  test_set(  desc, "mfpf_2_per_file_default_sec_short", set1, 2 );
  desc = func;
  desc += " - 1 frame per file 32 seconds in length (short)";
  test_set(  desc, "mfpf_1_per_file_32_sec_short", set1, 1, 32 );
}

void
test_createRDS_cmd( )
{
  using namespace FrameAPI;

  std::string	func = "test_createRDS_cmd";

  start_type			start;
  end_type			end;

  frame_file_container_type	frame_files;
  channel_container_type	channels;

  
  try
  {
    start = GetReduceStartTime( IFO_LHO, 0 );
    end = GetReduceEndTime( IFO_LHO, 1 );

    frame_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    frame_files.push_back( GetFrameName( IFO_LHO, 1 ) );
  }
  catch( const std::runtime_error& E )
  {
    Test.Message( ) << func << ": Test skipped due to no data being available"
		    << std::endl;
    return;
  }

  channels.push_back( "H2:LSC-AS_Q" );
  Frame	frame;
  Frame::channel_type as_q;

  frame = createRDS( frame_files, start, end, channels );
  std::cerr << "DEBUG:"
	    << " frame.GetDt: " << frame->GetDt( )
	    << std::endl
    ;

  as_q = frame.GetChannel( channels[ 0 ] );
  std::cerr << "DEBUG:"
	    << " dx[0]: " << as_q->GetDx( )[ 0 ]
	    << " nx[0]: " << as_q->GetNx( )[ 0 ]
	    << " startX[0]: " << as_q->GetStartX( )[ 0 ]
	    << " unitX[0]: " << as_q->GetUnitX( )[ 0 ]
	    << " unitY: " << as_q->GetUnitY( )
	    << std::endl
    ;
}

void
test_scripts( )
{
  //---------------------------------------------------------------------
  // These test simulate what the nightly loop tests do
  //---------------------------------------------------------------------
  test_type set1;
  std::string	func = "test_scripts: resampleRawFrames";

  try
  {
    set1.push_back( rds_data_type( ) );
    set1.back( ).s_start_time = 751800272;
    set1.back( ).s_end_time =  751800400;
    //-------------------------------------------------------------------
    // Get the appropriate list of channels
    //-------------------------------------------------------------------
    GetChannelList( IFO_LLO, RUN_S3, 1, set1.back( ).s_channels );
    //-------------------------------------------------------------------
    // Get list of filenames
    //-------------------------------------------------------------------
    GetFrameNames( needs_resample( set1.back( ).s_channels ),
		   set1.back( ).s_start_time, set1.back( ).s_end_time,
		   IFO_LLO, RUN_S3, 0,
		   set1.back( ).s_filenames );
  }
  catch( const std::runtime_error& e )
  {
    Test.Message( ) << func << ": Test skipped due to no data being available"
		    << std::endl;
    return;
  }
  test_set(  func, "RDS_L1", set1, 1, 16 );
}

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Initialize the testing environment
  //---------------------------------------------------------------------

  Test.Init( ArgC, ArgV );
  try
  {
    FrameCPP::Initialize( );

    setenv( "LDASTMP", ".", 1 );	// Establish current directory as temp dir
    getcwd ( cwd, sizeof( cwd ) - 1 );

    //-------------------------------------------------------------------
    // Test the createRDS command by extracting some channels
    //-------------------------------------------------------------------

#if 0
    standard( );

    multi_ldas( );
    multi_frames_per_file( );
    multi_frames_per_file_short( );
#endif /* 0 */

    test_scripts( );

    test_createRDS_cmd( );

    //-------------------------------------------------------------------
    // Terminate in a way that make check understands.
    //-------------------------------------------------------------------
  }
  catch( const std::exception& E )
  {
    Test.Check( false ) << "Exception: " << E.what( )
			<< std::endl
      ;
  }
  catch( ... )
  {
    Test.Check( false ) << "Exception: Unknown"
			<< std::endl
      ;
  }
  General::AtExit::Cleanup( );
  Test.Exit( );
}
