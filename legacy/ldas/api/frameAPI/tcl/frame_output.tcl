## ******************************************************** 
##
## Name: frame_output.tcl
##
## Description:
## Routines for frame API to handle output of frame data
## from other API's.
##
## Another API connects to the frame API's data socket and
## drops formatted ilwd with the name filed of the outermost
## container set to the URL for the output.
## file, http, and ftp are supported.
##
## The actions in this module occur asynchronously relative
## to the user command which generates the data, however the
## jobid is used to provide context.
##
## Parameters:
##
## Usage:
## The comment field of the proc data object is used for
## passing the URL for the output.
##
## Hints for filling in options for other frame structures
## can be lappended to the comment filed and should be
## formatted like so:
##
## { [ frame|detector|history ] name=this dt=1.0 } { ... }
##
## Comments:
## Every incoming proc channel should come with a history.
##
## ******************************************************** 

;#barecode
set ::RCS_ID_frame_outputtcl {$Id: frame_output.tcl,v 1.61 2006/06/27 18:19:40 mlei Exp $}
set ::RCS_ID_frame_outputtcl [ string trim $::RCS_ID_frame_outputtcl "\$" ]

package provide frame_output 1.0

;#end

## ******************************************************** 
##
## Name: frame::bucketBucket 
##
## Description:
##
## Parameters:
##
## Usage:
##
## A 'dummy' ilwd looks like this:
##
## <?ilwd?>
## <ilwd metadata='ignore=yes' name='dummy'>
##     <lstring name='dummy' size='5'>dummy</lstring>
## </ilwd>
##
## Comments:
##

proc frame::bucketBucket { jobid { i 0 } { cid "" } } {
     
     if { [ catch {
        
        set errlvl 3
        
        if { ! [ string length $cid ] } {
           set cid [ uplevel #1 set cid ]
           if	{ [ regexp {8.4} $::tcl_version ] } {
           		foreach entry [ trace info variable ::$cid ] {
        			foreach { oplist cmd } $entry { break }
        			trace remove variable ::$cid $oplist $cmd
                }
			} else {
           		trace vdelete ::$cid w "reattach $jobid $cid"
            }
        }
        
        set filenames [ list ]
        
        set bucket ::${jobid}_DATABUCKET

        set timeout $::DATABUCKET_TIMEOUT
        
        set toerr    "The frame API asynchronous data handler timed\n"
        append toerr "out after polling for $timeout seconds.\n"
        append toerr "the options for this job indicated that the\n"
        append toerr "frame API would receive ilwd data from\n"
        append toerr "another API for conversion into frames,\n"
        append toerr "but no data was received.\n\n"
        append toerr "this is a bug which should be reported!\n\n"
        append toerr "please attach the user command options for\n"
        append toerr "the job to the problem report and refer to\n"
        append toerr "LDAS system $::LDAS_SYSTEM and jobid ${jobid}."
        
        set mterr    "The frame API found an empty data bucket for\n"
        append mterr "$::LDAS_SYSTEM jobid ${jobid}.\n\n"
        append mterr "the options for this job indicated that the\n"
        append mterr "frame API would receive ilwd data from\n"
        append mterr "another API for conversion into frames,\n"
        append mterr "but no data was received.\n\n"
        append mterr "this is a bug which should be reported!\n\n"
        append mterr "please attach the user command options for\n"
        append mterr "the job to the problem report and refer to\n"
        append mterr "LDAS system $::LDAS_SYSTEM and jobid ${jobid}."
        
        if { [ info exists $bucket ] } {
           if { [ llength [ set $bucket ] ] } {
              set ptrs [ processDataBucket $jobid ]
              if { ! [ string length $ptrs ] } {
                 set errlvl 1
                 return -code error $mterr
              }
              
		    set ptrs [ frame::handleDummyIlwd $ptrs ]
		    
		    if { [ string length $ptrs ] } {
		       frame::managePointers $jobid add $ptrs
                 debugPuts "Converting '$ptrs' to frames" 
                 set filenames \
                    [ frame::writeProcFrame $jobid $ptrs ]
           
                 set files {}
                 foreach filename $filenames {
                    append files "$filename\n"
                 }
			  set msg "Your results:\n$files"
              } else {
		       set msg 0
		    }
              
		    ::unset $bucket
              set ::$cid [ list 0 $msg 0 ]
              
              reattach $jobid $cid
              
              if { [ info exists ::$jobid ] } {
                 ::unset ::$jobid
              }
              
           } elseif { $i < $timeout } {
              incr i
              after 1000 [ list frame::bucketBucket $jobid $i $cid ]
           } else {
              set errlvl 1
              return -code error $toerr
           }
        } elseif { $i < $timeout } {
           incr i
           after 1000 [ list frame::bucketBucket $jobid $i $cid ]
        } else { 
           ;## timed out!
           set errlvl 1
           return -code error $toerr
        }
     } err ] } {
        addLogEntry "[ myName ]: $err" red
        set ::$cid [ list $errlvl $err error! ]
        reattach $jobid $cid
        after 0 frame::killJob $jobid 
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::handleDummyIlwd
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::handleDummyIlwd { ptrs } {
     
     if { [ catch {
        set temp [ list ]
        foreach ptr $ptrs {
	      set name [ getElementAttribute $ptr name ]
	      if { [ string equal dummy $name ] } {
		     frame::destructElementWrap $ptr
		 } else {
		    lappend temp $ptr
		 }
	   }
	   set ptrs $temp
	} err ] } {
        return -code error "[ myName ]: $err"
     }
	return $ptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::writeProcFrame
##
## Description:
## 
## Parameters:
##
## Usage:
##
## Comments:
## Every incoming pointer should have the URL of the output
## frame in it's comment field.
##

proc frame::writeProcFrame { { jobid "" } { dataptrs "" } } {
     
     if { ! [ string length [ join $dataptrs ] ] } {
        return {}
     }
     
     set frameptrs [ list ]
     set filenames [ list ]
     set seqpt [ list ]
     
     if { [ catch {

        foreach datap $dataptrs {
           set frameptrs [ frame::convertToFramePtrs $jobid $datap ]
           foreach { name url framep } $frameptrs {
              set dir [ set ::${jobid}(-outputdir) ]
              set dir [ string trim [ join $dir ] ]
              if { [ string length [ join $dir ] ] } {
                 set lurl $name
                 set fn   $name
              } else { 
                 ;## generate the local url 'lurl'
                 foreach { lurl fn } [ url2file $jobid $url ] { break }
              }
              ;## fname should be the full path
              foreach [ list framep method level ] $framep { break }
              set seqpt "frame::writeFile($jobid $fn $framep):"
              frame::writeFile $jobid $fn $framep $method $level
              set seqpt [ list ]
              
              ;## returnprotocol is a remote ftp site
              if { [ regexp {^ftp:/.+} $url ] } {
                 foreach [ list protocol host port path ] \
                    [ parseURL $url ] { break }
                 if { [ string length [ join $path ] ] } {
                    if { [ regexp {\.} $host ] } {
                       if { [ catch {
                          outputUrls $jobid [ list $fn $url ]
                       } err ] } {
                          addLogEntry $err red
                       } else {
                          set lurl $url
                       }
                    }
                 }
              }
              lappend filenames $lurl
           }
        }
        
     } err ] } {
        if { [ string length $err ] } {
           frame::managePointers $jobid destroy .+
           return -code error "[ myName ]:$seqpt $err"
        }
     }
     return $filenames
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::convertToFramePtrs
##
## Description:
## Contained ilwd's have the 0th element of their comment
## attribute set to the file:/frame_file_name.F that the
## object should be written to.
## Parameters:
##
## Usage:
##
## Comments:
## Returns pairs of url's and frame pointers.

proc frame::convertToFramePtrs { jobid ptr } {
     
     if { [ catch {
        set seqpt {}
        set name      [ list ]
        set contp     [ list ]
        set frameptrs [ list ]
        
        set of [ set ::${jobid}(-outputformat) ]
        set of [ string trim [ join $of ] ]
        
        ;## turn off logic for now...
        set of frame

        set seqpt "getContainerSize($ptr):"
        set size [ getContainerSize $ptr ]
        set seqpt "ilwd2frame($ptr):"
        
        if { [ info exists ::DEBUG_FRAMES_FROM_OTHER_APIS ] } {
           frame::0xdb $jobid $ptr http://${ptr}_dump.ilwd
        }
        
        foreach [ list method level ] \
           [ frame::ilwdCompressionMetadata $jobid $ptr ] { break }

        
        ;## catches if it's a package of frame ilwd objects
        ;## and unpacks it.
        if { [ catch { set framep [ ilwd2frame $ptr ] } ] } {
           set i 0
           while { $i < $size } {
              set seqpt "copyContainerElement($ptr $i):"
              set contp [ copyContainerElement $ptr $i ]
              frame::managePointers $jobid add $contp
              foreach [ list method level ] \
                 [ frame::ilwdCompressionMetadata $jobid $contp ] \
                 { break }

              if { [ catch {
                 set seqpt "getElementMetadata($contp outputname):"
                 set name [ getElementMetadata $contp outputname ]
                 if { ! [ string length $name ] } {
                    set name no_name_attribute
                 }
                 
                 ;## okay, this produces the correct URL
                 set url [ frame::mangleOutputName $jobid $name ]
                 regexp {^file:/(/\S+)} $url - name
                 
                 ;## no colons in file names please!!
                 regsub -all -- {(\:+)([^\/])} $url {_\2} url
                 if { ! [ regexp {^(file|ftp):\/+\S+$} $url ] } {
                    set url file:/X-no_output_metadata-0-0.gwf
                 }
                 
                 if { [ string equal -nocase frame $of ] } {
                    set seqpt "ilwd2frame-${i}($contp):"
                    set framep [ ilwd2frame $contp ]
                    set seqpt {}
                    
                    frame::managePointers $jobid add $framep
                    foreach [ list method level ] \
                       [ frame::ilwdCompressionMetadata $jobid $contp ] { break }
           
                    set framep [ list $framep $method $level ]
                    frame::managePointers $jobid delete $contp
                    lappend frameptrs $name $url $framep
                 } else {
                    lappend frameptrs $name $url $contp
                 }
              } err ] } {
                 set msg "error while converting '$name' error: '$err'"
                 append msg " ($::errorInfo)"
                 frame::0xdb $jobid $contp \
                    http://conversion_error_dump-${i}.ilwd
                 return -code error $msg
              }
              incr i
           }
        ;## otherwise just converts it and on we go   
        } else {
           frame::managePointers $jobid add $framep
           set seqpt "getElementMetadata($ptr outputname):"
           set name [ getElementMetadata $ptr outputname ]
           if { ! [ string length $name ] } {
              set name no_name_attribute
           }
           
           set url [ frame::mangleOutputName $jobid $name ]
           
           foreach [ list method level ] \
              [ frame::ilwdCompressionMetadata $jobid $ptr ] { break }
           
           set framep [ list $framep $method $level ]
           
           set frameptrs [ list $name $url $framep ]
        }
        frame::managePointers $jobid delete $ptr
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $frameptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::mangleOutputName
##
## Description:
##
## Parameters:
##
## Usage:
##
## Some notes about parseURL
##
##    parseURL http://foo.ilwd
##       http foo.ilwd {} {}
##
##    parseURL http://foo.bar.edu/this/that/foo.ilwd
##       http foo.bar.edu {} /this/that/foo.ilwd
##
## Comments:
##

proc frame::mangleOutputName { jobid url } {
     
     if { [ catch {
        set seqpt {}
        set of frame
        set rp [ set ::${jobid}(-returnprotocol) ]
        set rp [ string trim [ join $rp ] ]
        set dir [ set ::${jobid}(-outputdir) ]
        set dir [ string trim [ join $dir ] ]
        
        if { [ string length [ join $dir ] ] } {
           set dir [ frame::mddDirectory $jobid $dir ]
        }
        
        set url [ string trim $url ]
        if { ! [ regexp {^(file|ftp):\/+\S+$} $url ] } {
           set url file:/X-no_output_metadata-0-0.gwf
        }
        
        ;## parseURL returns:
        ;##
        ;##    protocol targ1 port targ2
        ;##
        ;## where targ1 is the machine, i.e. ftp.gun.org
        ;## and targ2 is the relative directory with
        ;## leading '/'.
        ;##
        ;## see genericAPI.tcl
        foreach [ list n1 n2 - n3 ] [ parseURL $url ] { break }
        
        foreach [ list r1 r2 - r3 ] [ parseURL $rp  ] { break }
        
        set protocol $n1
        
        set rpft [ file tail $rp ]
        if { [ regexp {.+-.+-\d{9,10}-\d+\.gwf} $rpft ] } {
           set filename $rpft
        } elseif { [ string equal .gwf [ file extension $url ] ] } {
           set filename [ file tail $url ]
           if { [ string length $dir ] } {
              set filename $dir/$filename
           }
        } else {
           set filename X-Failed_Filename_Mangling-0-0.gwf
        }
        
        set filename [ frame::insertUserType $jobid $filename ]
        set filename ${protocol}:/$filename
        
        if { [ regexp -nocase {ilwd} $of ] } {
           set rpext [ string tolower [ file extension $rp ] ]
           if { [ string equal .ilwd $rpext ] } {
              set filename [ file rootname $filename ].ilwd   
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $filename
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::insertUserType
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::insertUserType { jobid filename } {
     
     if { [ catch {
        set usertype [ set ::${jobid}(-usertype) ]
        set usertype [ string trim [ join $usertype ] ]
        
        ;## interpose the usertype if one was provided
        if { [ string length $usertype ] } {
           set filename [ split $filename - ]
           set type [ lindex $filename end-2 ]
           if { [ regexp {_\d+$} $type iterator ] } {
              set usertype $usertype$iterator
           }
           set filename [ lreplace $filename end-2 end-2 $usertype ]
           set filename [ join $filename - ]
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $filename
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::mddDirectory
##
## Description:
## Determines the correct directory to write the output
## multi-dim-data into.
##
## This was formerly strongly tied to a standard location
## for FTP toplevel, but we don't support FTP anymore, so
## the whole thing is getting an http face.
##
## Parameters:
##
## Usage:
##
## Comments:
## The directory MUST already exist!

proc frame::mddDirectory { jobid dir } {
     
     if { [ catch {
        set perjobdirs [ set ::${jobid}(-usejobdirs) ]
        set perjobdirs [ string trim [ join $perjobdirs ] ]
        set dir [ string trim $dir . ]
        
        ;## /ldas_outgoing/jobs/LDAS-SITE_N/LDAS-SITEN
        set jobdir [ jobDirectory ]
        
        if { [ string length [ join $dir ] ] } {
           ;## if the directory specified is NOT under ::PUBDIR
           ;## then we need to verify that the particular user
           ;## is authorised to write in the directory.
           if { ! [ string match ${::PUBDIR}* $dir ] } {
              set file ${::PUBDIR}/activejobs.tcl
              set jobinfo [ dumpFile $file ]
              set jobinfo [ lindex $jobinfo 0 ]
              set user NULL
              foreach job $jobinfo {
                 if { [ string equal $jobid [ lindex $jobinfo 0 ] ] } {
                    set user [ lindex $jobinfo 3 ]
                    break
                 }
              }
              if { [ info exists ::USER_CAN_WRITE_MDD_TO_DIRS($user) ] } {
                 if { [ lsearch $::USER_CAN_WRITE_MDD_TO_DIRS($user) $dir ] < 0 } {
                    set err    "user $user does not have permission "
                    append err "to write to $dir. if this user should "
                    append err "be allowed to write to that directory, "
                    append err "then ::USER_CAN_WRITE_MDD_TO_DIRS($user) "
                    append err "should be modified accordingly."
                    return -code error $err
                 }
              }
           }
           
           if { [ file isdirectory $dir ] && $perjobdirs } {
              ;## write into a per job directory hierarchy under
              ;## the specified -outputdir
              set jobdir \
                 [ join [ lrange [ file split $jobdir ] end-1 end ] / ]
              set dir $dir/$jobdir
              file mkdir $dir
           } elseif { [ file isdirectory $dir ] } { 
              set dir $dir
           } else {
              set msg "the directory specified: '$dir' does not exist "
              append msg "or is not a directory."
              return -code error $msg
           }
        } else {
           set dir $jobdir
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $dir
}
## ******************************************************** 

