## -*- mode: tcl -*-
## ********************************************************
## 
## Name: LDASframe.rsc
##
## This is the frame API specific resource file.
## It contains resource information which is only used by
## the frame API.
##
## If this file is not found in ::LDAS/lib/frameAPI
## the frame API will be non-functional.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
## ********************************************************
#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES 

;## desc=max memory size before forced restart of API
set ::MEMFLAG_MEGS 5000

;## desc=list of users who may write RDS files anywhere
set ::USERS_WHO_CAN_CREATE_RDS_DIRS [ list createrdstest ]

;## desc=ARRAY of users who may write RDS files to specified dirs
set ::USER_CAN_WRITE_RDS_TO_DIRS(nobody) [ list /only/here /and/there ]

;## desc=seconds to wait between start of new jobs
set ::FRAME_NEW_JOB_RATE 0

;## desc=honor requests for frames up to this many seconds
set ::FRAMELIMIT 86400

;## desc=list of users who may write RDS files anywhere
set ::USERS_WHO_CAN_CREATE_RDS_DIRS [ list createrdstest ]

;## desc=ARRAY of users who may write RDS files to specified dirs
set ::USER_CAN_WRITE_RDS_TO_DIRS(nobody) [ list /only/here /and/there ]

;## desc=frame name substitution rules for output files
set ::FRAME_FILENAME_REGSUBS [ list NORMAL {} ldas_ {} ]

;## desc=how often do we check the async bucket handler
set ::FRAME_BUCKET_CHECK_INTERVAL 3

;## desc=cache directory where user produced frames may be written
set ::FRAME_WRITABLE_CACHE /frame10

;## desc=establish the default file buffer size; 0 indicates OS default
set ::STREAM_BUFFER_SIZE 0

;## desc=establish use of memory mapped I/O for files if available
set ::ENABLE_MEMORY_MAPPED_IO 0

;## desc=establish a list of devices that have custom buffer sizes and memory mapped I/O characteristics; Should be of the form [ list [ list  <file>|<fstype> <buffer size> <i/o> ] ... ]
set ::DEVICE_IO_CONFIGURATION [ list ]

;## desc=should we do checksum verification of frame files?
set ::FRAMEFILECHECKSUM_FLAG 0

;## desc=should we do checksum verification of frames in files?
set ::FRAMECHECKSUM_FLAG 0

;## desc=should we do timerange sanity checking of frame files?
set ::FRAMETIMECHECK_FLAG 1

;## desc=should we do metadata sanity checking of frame files?
set ::FRAMEMETADATACHECK_FLAG 1

;## desc=should we do 'data valid' checking of frame files?
set ::FRAMEDATAVALID_FLAG 1

;## desc=should we allow short dt output files if there are gaps?
set ::FRAMEALLOWSHORT_FLAG 0

;## desc=should we generate checksum values for our output frames?
set ::FRAMEGENERATECKSUM_FLAG 0

;## desc=should we fill in missing datavalid flags in our output frames?
set ::FRAMEFILLDATAVALID_FLAG 0

;## desc=default frame file compression method
set ::FRAMEDEFAULTCOMPRESSIONMETHOD gzip

;## desc=default frame file compression level
set ::FRAMEDEFAULTCOMPRESSIONLEVEL 1

## SPECIAL DEBUGGING FLAGS ONLY BELOW THIS LINE

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_CREATE_FRAME_GROUP 0

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_FRAME_OUTPUT 0

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_QUERY_EXPANSION 0

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_FILECACHE 0

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_FILE2PTR 0

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_METHOD2PTR 0

;## desc=emit possibly helpful debugging messages if set to '1'
set ::DEBUG_CREATE_RDS 0

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default core default cputime default datasize default \
filesize default memorylocked default descriptors default maxproc default ]

;## desc=max secs of not receiving any frame file names from diskcache
set ::DELAY_RECV_FRAMES_FROM_DISKCACHE_SECS 120

;## desc=frame cmd receive socket timeout limit, originally 10000
set ::MAX_API_SOCKET_TIMEOUT_SECS 200000
