#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: linfiltperf04.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;## LinFilt performance test

;## LINFILTPERF04
;## Performance of various FIR filters

set substitution "a = slice(araw,0,1,1);"

foreach size { 5 10 15 20 25 30 35 40 45 50 } {
  
  set sub "
    
    b = slice(braw,0,$size,1);
    x = slice(xraw,0,1048576,1);

    t00 = user_time();

    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    
    t32 = user_time();
    t32 = sub(t32,t00);
    duration = div(t32,32);
    output(duration,_,_,time,LIR linear filter for b size ${size});
    "

  set substitution "$substitution$sub"

}

foreach type { R4 R8 C8 C16 } {
  
  set cmd "
  ldasJob { -name $user -password $pwrd -email $email } 
  { 
    conditionData 
    -inputprotocol  file:/ldas_outgoing/mdc/input/N23$type.sim
    -returnprotocol file:/ldas_outgoing/mdc/output/LINFILTPERF04$type.ilwd -outputformat { ilwd ascii } 
    -aliases { xraw = mdc_input_data:chan_01:data; araw = mdc_input_data:chan_03:data; braw = mdc_input_data:chan_02:data } 
    -algorithms
	{ 
      $substitution
    } 
  }
  "

sendCmd $cmd

}
