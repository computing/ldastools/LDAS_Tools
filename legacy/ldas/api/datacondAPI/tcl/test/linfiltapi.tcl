#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval linfiltapi {

	set postfixes { "R4" "R8" "C8" "C16" }
    
	set cmdstem { conditionData
    	-subject $subject
        -inputprotocol { file:/$ifile $efile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases { $aliases }
        -algorithms { $algo } 
     }
     
     set aliases_linfiltapi01a { xlong = mdc_input_data:chan_01:data; a = mdc_input_data:chan_03:data; 
     blong = mdc_input_data:chan_02:data }
     
     set aliases_linfiltapi01b $aliases_linfiltapi01a
     set aliases_linfiltapi01c $aliases_linfiltapi01a
     
     set aliases_linfiltapi02a { raw = mdc_input_data:chan_01:data; a = mdc_input_data:chan_03:data; 
     blong = mdc_input_data:chan_02:data; exp = $prefix:linfiltout:data } 
     
     set aliases_linfiltapi02b { raw = mdc_input_data:chan_01:data; a = mdc_input_data:chan_03:data; 
     blong = mdc_input_data:chan_02:data } 

     set algo_linfiltapi01a {
     	    x = slice(xlong,0,100,1);
            b = slice(blong,0,13,1);
            a0 = slice(a,0,0,1);
            y = linfilt(b,a0,x);
            output(y,_,_,y,linear filter output);
     }
     
     set algo_linfiltapi01b {
            x = slice(xlong,0,100,1);
            b = slice(blong,0,13,1);
            b0 = slice(b,0,0,1);
            y = linfilt(b0,a,x);
            output(y,_,_,y,linear filter output);
    }
    
	set algo_linfiltapi01c {
            x = slice(xlong,0,100,1);
            b = slice(blong,0,13,1);

            x0 = slice(x,0,0,1);
            y = linfilt(b,a,x0);

            output(y,_,_,y,linear filter output);
    } 	
    
    set algo_linfiltapi02a {
            x = slice(raw,0,100,1);
            b = slice(blong,0,13,1);
            y = linfilt(b,a,x);

            ry = real(y);
            rexp = real(exp);
            rdif = sub(ry,rexp);
            rmin = min(rdif);
            output(rmin,_,_,rmin,minimum of real difference);
            rmax = max(rdif);
            output(rmax,_,_,rmax,maximum of imag difference);
            rrms = rms(rexp);
            output(rrms,_,_,rrms,root mean square of real expected result);

            iy = imag(y);
            iexp = imag(exp);
            idif = sub(iy,iexp);
            imin = min(idif);
            output(imin,_,_,imin,minimum of imag difference);
            imax = max(idif);
            output(imax,_,_,imax,minimum of imag difference);
            irms = rms(iexp);
            output(irms,_,_,irms,root mean square of imag expected result);

            output(y,_,_,y,linear filter output);
    
    }
    
    set	algo_linfiltapi02b {
           	b = slice(blong,0,13,1);

            x = slice(raw,0,100,1);
            y = linfilt(b,a,x);
            output(y,_,_,y,whole);

            x1 = slice(raw,0,50,1);
            y1 = linfilt(b,a,x1,z);
            output(y1,_,_,y1,first half);
            x2 = slice(raw,50,50,1);
            y2 = linfilt(x2,z);
            output(y2,_,_,y2,second half);

            yA = slice(y,0,50,1);
            yB = slice(y,50,50,1);

            ryA = real(yA);
            ryB = real(yB);
            ry1 = real(y1);
            ry2 = real(y2);
            rd1 = sub(ryA,ry1);
            rd2 = sub(ryB,ry2);
            rmin1 = min(rd1);
            output(rmin1,_,_,rmin1,minimum of difference of real first halves);
            rmin2 = min(rd2);
            output(rmin2,_,_,rmin2,minimum of difference of real second halves);
            rmax1 = max(rd1);
            output(rmax1,_,_,rmax1,maximum of difference of real first halves);
            rmax2 = max(rd2);
            output(rmax2,_,_,rmax2,maximum of difference of real second halves);
            rrms1 = rms(ryA);
            output(rrms1,_,_,rrms1,root mean square of first half);
            rrms2 = rms(ryB);
            output(rrms2,_,_,rrms2,root mean square of second half);

            iyA = imag(yA);
            iyB = imag(yB);
            iy1 = imag(y1);
            iy2 = imag(y2);
            id1 = sub(iyA,iy1);
            id2 = sub(iyB,iy2);
            imin1 = min(id1);
            output(imin1,_,_,imin1,minimum difference of imag first halves);
            imin2 = min(id2);
            output(imin2,_,_,imin2,minumum difference of imag second halves);
            imax1 = max(id1);
            output(imax1,_,_,imax1,maximum difference of imag first halves);
            imax2 = max(id2);
            output(imax2,_,_,imax2,maximum difference of imag second halves);
            irms1 = rms(iyA);
            output(irms1,_,_,irms1,root mean square of first half);
            irms2 = rms(iyB);
            output(irms2,_,_,irms2,root mean square of second half);

            output(y,_,_,y,linear filter output);
	} 
    
    namespace export linfiltapi*
    
    proc linfiltapiTest { ifilepre code { needPrefix 0 } } {
    
    	if	{ [ catch {
        	set name [ namespace current ]
		set procname [ info level -1 ]
          namespace import [ namespace parent $name ]::*           
			
            set ofilepre [ string toupper $procname ]
            set cmdstem [ namespace eval $name set cmdstem ]
            set postfixes [ namespace eval $name set postfixes ]
            
            set algo [ namespace eval $name set algo_$procname ]
            set rc [ namespace eval $name info exist ifile_$procname ]
            
			foreach post $postfixes {
				set ppost ${code}$post
                	set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim  
                    set aliases [ namespace eval $name set aliases_$procname ]
				if	{ $needPrefix } {
            			set prefix $ofilepre$post                    
    					set efile file:/ldas_outgoing/mdc/input/$prefix.exp
                		set ofile ldas_outgoing/mdc/output/$prefix.ilwd
            		} else {
            			set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd    
                    	set extra_aliases ""	
                    	set efile ""	
            		}
                	set subject datacondMDC:conditionData:$procname:$ppost
                	set aliases [ subst $aliases ]
                	set algo [ subst $algo ]
                	set cmd [ subst $cmdstem ]
                	runTest $subject $cmd            
    		}
    	} err ] } {
    		return -code error $err
		}
	}
    
	proc linfiltapi01a {} {

		if	{ [ catch {
    		set ifilepre "N200"
        	set code A
        	set procname [ info level 0 ]
			linfiltapiTest $ifilepre $code 
    	}	err ] } {
    		return -code error $err
		}
	}

	proc linfiltapi01b {} {

		if	{ [ catch {	
		    set ifilepre "N200"
        	set code B
        	set procname [ info level 0 ]
			linfiltapiTest $ifilepre $code
    	}	err ] } {
    		return -code error $err
		}
	}

	proc linfiltapi01c {} {

		if	{ [ catch {
			set ifilepre "N200"
            set code C
			set procname [ info level 0 ]
 			linfiltapiTest $ifilepre $code
    	}	err ] } {
    		return -code error $err
		}    
	}

	proc linfiltapi02a {} {

		if	{ [ catch {
			set ifilepre "N8388608"
			set procname [ info level 0 ]
            set code A
			linfiltapiTest $ifilepre $code 1
		} err ] } {
    		return -code error $err
		}    
	}


	proc linfiltapi02b {} {

		if	{ [ catch {
			set ifilepre "N8388608"
			set procname [ info level 0 ]
            set code B
 			linfiltapiTest $ifilepre $code 0
 		} err ] } {
    		return -code error $err
		} 
	}

}
namespace import linfiltapi::*
