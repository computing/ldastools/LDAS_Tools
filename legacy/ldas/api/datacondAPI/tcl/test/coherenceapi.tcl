#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval coherenceapi {

	set postfixes { "R4" "R8" "C8" "C16" }
	set ifilepre "N10" ;## Use data file of size 2^10 = 1024
	set cmdstem { conditionData
      -subject $subject 
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases {
            data=mdc_input_data:chan_01:data;
            freqexp=${prefix}:freqout:data;
            specexp=${prefix}:specout:data
        }
        -algorithms { $algo }
    }
    set algo_coherenceapi01a {    	
   		x = slice(data, 0, 0, 1);
        spec = coherence(x, x);
        output(spec,_,_,spec,spectrum data);
    }
    
    set algo_coherenceapi01b {
    	x = slice(data, 0, 1024, 1);
            spec = coherence(x, x, 1024, _, 1025);
            output(spec,_,_,spec,spectrum data);
    }
    
    set algo_coherenceapi01c {
    	x = slice(data, 0, 1024, 1);
        spec = coherence(x, x, 1024, _, 1024);
       	output(spec,_,_,spec,spectrum data);
    }
    
    set algo_coherenceapi01d {
        x = slice(data, 0, 1024, 1);
        spec = coherence(x, x, 1024, _, 1025);
        output(spec,_,_,spec,spectrum data);
    }
    
    set algo_common {
    	rspec = real(spec);
       	ispec = imag(spec);
        minrspec = min(rspec);
        output(minrspec,_,_,minrspec,min real spectrum data);           
       	maxrspec = max(rspec);
        output(maxrspec,_,_,maxrspec,max real spectrum data);           
       	rmsrspec = rms(rspec);
        output(rmsrspec,_,_,rmsrspec,rms real spectrum data);
        minispec = min(ispec);           
        output(minispec,_,_,minispec,min imag spectrum data);           
        maxispec = max(ispec);
        output(maxispec,_,_,maxispec,max imag spectrum data);           
        rmsispec = rms(ispec);
        output(rmsispec,_,_,rmsispec,rms imag spectrum data);  
    }
    
    set algo_coherenceapi02a {
    	spec = coherence(x,x);
        output(spec,_,_,spec,spectrum data);
		$algo_common
    }
    
   	set algo_coherenceapi02b { 
    	x = slice(data, 0, 1024, 1);
        spec = coherence(x,x,256,_,0);
        output(spec,_,_,spec,spectrum data);
        $algo_common
    }
    
    set algo_coherenceapi02c { 
  		x = slice(data, 0, 1024, 1);
        spec = coherence(x,x,256,_,128,_);
        output(spec,_,_,spec,spectrum data);
		$algo_common
    }
      
	namespace export coherenceapi*
    
	proc coherenceapiTest { code } {
	
    	if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
            namespace import [ namespace parent $name ]::*           

            set ifilepre 	[ namespace eval $name set ifilepre ]
            set postfixes   [ namespace eval $name set postfixes ]
            set ofilepre 	[ string toupper $procname ]
            set cmdstem [ namespace eval $name set cmdstem ]
            set algo_common  [ namespace eval $name set algo_common ]
            
            foreach post $postfixes {
            	set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    			set ppost ${code}$post  ;## Need to change this for tests A, B, C
    			set prefix $ofilepre$ppost
    			set ofile ldas_outgoing/mdc/output/${prefix}.ilwd
            	set algo [ namespace eval $name set algo_${procname} ]
                set algo [ subst $algo ]
                
                set subject datacondMDC:conditionData:$procname:$post
                set cmd [ subst $cmdstem ]
                runTest $subject $cmd
            	after 1000
            }
      	} err ] } {
    		return -code error $err
		}
	}
            
	proc coherenceapi01a {} {

		if	{ [ catch {
			coherenceapiTest A
    	} err ] } {
    		return -code error $err
    	}
	}

	proc coherenceapi01b {} {

		if	{ [ catch {
			coherenceapiTest B
		} err ] } {
    		return -code error $err
    	}
	}

	proc coherenceapi01c {} {

		if	{ [ catch {
			coherenceapiTest C
		} err ] } {
    		return -code error $err
    	}
	}

	proc coherenceapi01d {} {

		if	{ [ catch {
			coherenceapiTest D
    	} err ] } {
    		return -code error $err
    	}
	}


	proc coherenceapi02a {} {

		if	{ [ catch {
			coherenceapiTest A
   		} err ] } {
			return -code error $err
    	}
	}

	proc coherenceapi02b {} {
	
		if	{ [ catch {
			coherenceapiTest B
   		} err ] } {
    		return -code error $error
    	}
	}

	proc coherenceapi02c {} {

		if	{ [ catch {
			coherenceapiTest C
   		} err ] } {
    		return -code error $error
    	}
	}
}
namespace import coherenceapi::*
