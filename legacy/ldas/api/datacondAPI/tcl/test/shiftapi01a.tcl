#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: shiftapi01a.tcl,v 1.3 2004/01/14 20:38:20 Philip.Charlton Exp $

;## SHIFTAPI01
;## shift api tests
;## Test exceptions

proc sendCmd {cmd} {
    regsub -all -- {#[^\n]*} $cmd {} cmd
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list ::user ::pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

proc go { testcase algo } {
  set testname "SHIFTAPI"
  set testnum "01"
  set ifilepre "N200"

  set postfixes { "R4" "R8" "C8" "C16" }
  set ofilepre $testname$testnum$testcase

  foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd

    set cmd "
    ldasJob { -name $::user -password $::pwrd -email $::email } {
      conditionData 
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases {
          srate = mdc_input_data:chan_01:rate;
          data = mdc_input_data:chan_01:data;
          len = mdc_input_data:chan_01:length;
        }
        -algorithms {

          x = tseries(data, 1.0, 610000000, 0);
          $algo
          output(y,_,_,y,Shifted output);

        }
    }"

    sendCmd $cmd
  }
}

;## Too few arguments
set algo "
          offset = value(0.1);
          order = value(2);
          y = shift();
"
go A $algo

;## Too few arguments
set algo "
          offset = value(0.1);
          order = value(2);
          y = shift(x);
"
go B $algo

;## Too many arguments
set algo "
          offset = value(0.1);
          order = value(2);
          x = slice(x, 0, 0, 1);
          y = shift(x, offset, order, z, x);
"
go C $algo

;## Zero-length data
set algo "
          offset = value(0.1);
          order = value(2);
          x = slice(x, 0, 0, 1);
          y = shift(x, offset, order);
"
go D $algo

;## Wrong data type
set algo "
          offset = value(0.1);
          order = value(2);
          x = value(1.0);
          y = shift(x, offset, order);
"
go E $algo

;## Wrong data type for offset
set algo "
          offset = value(-0.1);
          order = value(2);
          y = shift(x, x, order);
"
go F $algo

;## Negative offset
set algo "
          offset = value(-0.1);
          order = value(2);
          y = shift(x, offset, order);
"
go G $algo

;## Negative order
set algo "
          offset = value(0.1);
          order = value(-2);
          y = shift(x, offset, order);
"
go H $algo

;## Wrong data type for order
set algo "
          offset = value(0.1);
          order = value(1.0);
          y = shift(x, offset, order);
"
go I $algo


;## Bad continuity for state
set algo "
          offset = value(0.1);
          order = value(1.0);
          y = shift(x, offset, order, z);
          y = shift(x, z);
"
go J $algo

;## Bad data type for state
set algo "
          offset = value(0.1);
          order = value(1.0);
          y = shift(x, offset, order, z);
          r = resample(x, 1, 2, z);
          y = shift(x, z);
"
go K $algo

exit
