#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: mixerapi02b.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## MIXERAPI02
;## mixer api tests
;## correct values on broken sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N200"
set ofilepre "MIXERAPI02"
set idir ldas_outgoing/mdc/input
set odir ldas_outgoing/mdc/output

foreach post $postfixes {
    set ifile ${idir}/${ifilepre}${post}.sim
    set ofile ${odir}/${ofilepre}B${post}.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/${ifile}
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases { raw=mdc_input_data:chan_01:data }
        -algorithms {
            x0 = slice(raw,0,100,1);
            f = value(0.125);
            phi = value(0.1);

            rx0 = real(x0);
            ix0 = imag(x0);
            rvarX = variance(rx0);
            output(rvarX,_,_,rvarX,Variance of real input data);
            rrmsX = rms(rx0);
            output(rrmsX,_,_,rrmsX,RootMeanSquare of real input data);
            ivarX = variance(ix0);
            output(ivarX,_,_,ivarX,Variance of imag input data);
            irmsX = rms(ix0);
            output(irmsX,_,_,irmsX,RootMeanSquare of imag input data);

            z0 = mix(phi,f,x0);

            x1 = slice(x0,0,50,1);
            x2 = slice(x0,50,50,1);

            phi = value(0.1);

            z1 = mix(phi,f,x1);
            output(z1,_,_,z1,First half mixer);
            z2 = mix(phi,f,x2);
            output(z2,_,_,z2,Second half mixer);

            zA = slice(z0,0,50,1);
            zB = slice(z0,50,50,1);

            rzA = real(zA);
            rzB = real(zB);
            rz1 = real(z1);
            rz2 = real(z2);
            izA = imag(zA);
            izB = imag(zB);
            iz1 = imag(z1);
            iz2 = imag(z2);

            rdifA = sub(rzA,rz1);
            output(rdifA,_,_,rdifA,First half real differences);
            rminA = min(rdifA);
            output(rminA,_,_,rminA,Minimum first half real deviation);
            rmaxA = max(rdifA);
            output(rmaxA,_,_,rmaxA,Maximum first half real deviation);

            rdifB = sub(rzB,rz2);
            output(rdifB,_,_,rdifB,Second half real differences);
            rminB = min(rdifB);
            output(rminB,_,_,rminB,Minimum second half real deviation);
            rmaxB = max(rdifB);
            output(rmaxB,_,_,rmaxB,Maximum second half real deviation);

            idifA = sub(izA,iz1);
            output(idifA,_,_,idifA,First half imag differences);
            iminA = min(idifA);
            output(iminA,_,_,iminA,Minimum first half imag deviation);
            imaxA = max(idifA);
            output(imaxA,_,_,imaxA,Maximum first half imag deviation);

            idifB = sub(izB,iz2);
            output(idifB,_,_,idifB,Second half imag differences);
            iminB = min(idifB);
            output(iminB,_,_,iminB,Minimum second half imag deviation);
            imaxB = max(idifB);
            output(imaxB,_,_,imaxB,Maximum second half imag deviation);

            output(z0,_,_,mixerbroken,Broken mixer result);
        }
    } "

    sendCmd $cmd
}

