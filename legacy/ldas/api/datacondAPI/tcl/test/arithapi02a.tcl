#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: arithapi02a.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## ARITHAPI02A
;##
;## test mixed precision complex arithmetic
;## 

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set precs { float double }
set types { "real(idata)" "complex(idata, idata)" }
set ops { add sub mul div }

set ifilepre N200
set dtype R4
set ofilepre ARITHAPI02
set letter A
set length 200

set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
set prefix $ofilepre$letter$dtype
set ofile ldas_outgoing/mdc/output/$prefix.ilwd
set efile ldas_outgoing/mdc/input/$prefix.exp

set cmd "
  ldasJob {-name $user -password $pwrd -email $email }
  { conditionData
      -inputprotocol { file:/$ifile }
      -returnprotocol { file:/$ofile }
      -outputformat { ilwd ascii }
      -aliases { idata = mdc_input_data:chan_01:data; }
      -algorithms {
"

;## repeat for each data type
foreach lhs_prec $precs {

  foreach rhs_prec $precs {

    foreach lhs_type $types {

      foreach rhs_type $types {

        foreach op $ops {

          set cmd "${cmd}
        lhs = ${lhs_type};
        lhs = ${lhs_prec}(lhs);

        rhs = ${rhs_type};
        rhs = ${rhs_prec}(rhs);

        res = ${op}(lhs, rhs);
"
        }
      }
    }
  }
}

set cmd "${cmd}
        output(res, _, _, res, output of arithapi02a);
      }
    }"
sendCmd $cmd

