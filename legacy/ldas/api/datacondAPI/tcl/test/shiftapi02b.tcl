#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 
# \
exec tclsh "$0" "$@"

;## $Id: shiftapi02b.tcl,v 1.4 2004/01/14 20:38:20 Philip.Charlton Exp $

;## SHIFTAPI02
;## shift api tests
;## Test correct values for broken vs continuous shift with
;## MDC data

proc sendCmd {cmd} {
    regsub -all -- {#[^\n]*} $cmd {} cmd
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set testname "SHIFTAPI"
set testnum "02"
set ifilepre "N200"

set postfixes { "R4" "R8" "C8" "C16" }
set shift_wholes { 0 1 2 3 }
set testcases { D E F G }
set alpha 0.1

foreach shift_whole $shift_wholes testcase $testcases {
  set ofilepre $testname$testnum$testcase

  foreach post $postfixes {
    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd

    set cmd "
    ldasJob { -name $user -password $pwrd -email $email } {
      conditionData 
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases {
          srate = mdc_input_data:chan_01:rate;
          data = mdc_input_data:chan_01:data;
          len = mdc_input_data:chan_01:length;
        }
        -algorithms {

          x = tseries(data, 1.0, 6100000, 0);

          # Shift
          offset = add($shift_whole, $alpha);
          order = value(2);

          len0 = div(len, 2);

          # Break up the input and shift each separately, then rejoin
          x0 = slice(x, 0, len0, 1);
          x1 = slice(x, len0, len0, 1);

          y0 = shift(x0, offset, order, z);
          y1 = shift(x1, z);
          y_conc = concat(y0, y1);

          clear(z);

          # Now do the output in one step and find the difference
          y = shift(x, offset, order);
          maxdiff = sub(y, y_conc);
          maxdiff = abs(maxdiff);
          maxdiff = max(maxdiff);
          
          output(maxdiff, _, _, maxdiff, max difference of broken vs continuous shift-should be small);
        }
    }"

    sendCmd $cmd

  }
}
