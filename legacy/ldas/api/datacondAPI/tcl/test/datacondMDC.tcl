#!/bin/sh
#\
    . /ldas/libexec/setup_tclsh.sh
# \
    exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#
# example of running with -match; this can be done on ldas system or
# part of ldas system testing
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 \
# --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1 --manager-reply-timeout 120000
#
# to only verify email in spool directory 
# I noticed I can only grep successfully if I copied the spool mail to
# another spot on my local disk
# datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 \
# --qa-debug-level 1 --tests email --jobListFile ./datacondMDC.jobs \
# --maildir /usr1/lcldsk/mlei/mail.spool > & datacondMDC.emails &
#
# to only verify email in filtered mail folder
# datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 \
# --qa-debug-level 1 --tests email --jobListFile ./datacondMDC.jobs \
#  > & datacondMDC.emails &
#
# To run all tests, leave out --tests option.
#
# writes a temporary file /ldas_outgoing/jobs/datacondMDC.jobs so email
# verification can be done on a local system.
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

set ::TOPDIR /ldas

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::datacondMDC::test {
	
    set email ldas-email-test.ligo.caltech.edu
    set maildir $::env(HOME)/Mail/mdctest/cur
    set jobListFile /ldas_outgoing/jobs/datacondMDC.jobs
    set clearEmails 0
    set testscriptdir $::TOPDIR/lib/test/datacond_mdc
    set checkEmailsFlag 0
    
    set testscripts { 
    	arithapi.tcl
    	coherenceapi.tcl
    	complexapi.tcl
    	fftapi.tcl
		interpolateapi.tcl
    	linfiltapi.tcl
    	mixerapi.tcl
		pipeapi.tcl
    	psdapi.tcl
		realimagabsapi.tcl
    	resampleapi.tcl
    	shiftapi.tcl
    	siggenapi.tcl
    	signumapi.tcl
    	statapi.tcl
    	tseriesapi.tcl
    	waveletapi.tcl
    }

    set testList {}
    foreach test $testscripts {
    	lappend testList [ file tail [ file rootname $test ] ]
    }
    
    set mergeJobs 0
    
    ;## the following are for reference only
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## list of exception tests
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    set exceptionList {
	arithapi01a.tcl
	coherenceapi01a.tcl
	coherenceapi01b.tcl
	coherenceapi01c.tcl
	coherenceapi01d.tcl
	complexapi01a.tcl
	complexapi01b.tcl
	complexapi01c.tcl
	fftapi01a.tcl
	fftapi01b.tcl
	linfiltapi01a.tcl
	linfiltapi01b.tcl
	linfiltapi01c.tcl
	mixerapi01a.tcl
	mixerapi01b.tcl
	psdapi01a.tcl
	psdapi01b.tcl
	psdapi01c.tcl
	psdapi01d.tcl
	resampleapi01a.tcl
	resampleapi01b.tcl
	resampleapi01c.tcl
	resampleapi01d.tcl
	shiftapi01a.tcl
	signumapi01a.tcl
	signumapi01b.tcl
	signumapi01c.tcl
	signumapi01d.tcl
	statapi01.tcl
	tseriesapi01a.tcl
	waveletapi01a.tcl
	waveletapi01b.tcl
	waveletapi01c.tcl
	waveletapi01d.tcl
	waveletapi01e.tcl
	waveletapi01f.tcl
	waveletapi01g.tcl
	waveletapi01h.tcl
	waveletapi01i.tcl
	waveletapi01k.tcl
    }
    
    set successList {
	arithapi02a.tcl
	coherenceapi02a.tcl
	coherenceapi02b.tcl
	coherenceapi02c.tcl
	complexapi02a.tcl
	fftapi02a.tcl
	fftapi02b.tcl
	fftapi02c.tcl
	fftapi02d.tcl
	fftapi02e.tcl
	interpolateapi02a.tcl
	linfiltapi02a.tcl
	linfiltapi02b.tcl
	mixerapi02a.tcl
	mixerapi02b.tcl
	pipe01.tcl
	psdapi02a.tcl
	psdapi02b.tcl
	psdapi02c.tcl
	psdapi02d.tcl
	resampleapi02a.tcl
	resampleapi02b.tcl
	resampleapi02c.tcl
	resampleapi02d.tcl
	resampleapi02e.tcl
	realimagabsapi01.tcl
	shiftapi02a.tcl
	shiftapi02b.tcl
	siggenapi02a.tcl
	signumapi02a.tcl
	statapi02.tcl
	tseriesapi02a.tcl
	waveletapi02a.tcl
    }
    
    set email "-email ldas-email-test@ligo.caltech.edu"    

}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--tests} {string} \
    {testset} \
    { set ::QA::datacondMDC::test::testList $::QA::Options::Value }

::QA::Options::Add {} {--clearEmails} {string} \
    {flag to remove emails in filter directory} \
    { set ::QA::datacondMDC::test::clearEmails $::QA::Options::Value }

::QA::Options::Add {} {--maildir} {string} \
    {directory of filtered email} \
    { set ::QA::datacondMDC::test::maildir $::QA::Options::Value }

::QA::Options::Add {} {--mergeJobs} {string} \
    {merge existing datacond job record in file datacondMDC.jobs} \
    { set ::QA::datacondMDC::test::mergeJobs $::QA::Options::Value }  
    
::QA::Options::Add {} {--jobListFile} {string} \
    {file containing list of jobs submitted} \
    { set ::QA::datacondMDC::test::jobListFile $::QA::Options::Value }       

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::datacondMDC::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
    namespace import ::QA::Rexec

    ;## this proc is called from a filevent handler, so do not use upvar
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# verify each job has an email response
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   
    proc verifyEmail { maildir } {

	Puts 1 "using maildir $maildir"
    if	{ [ file isfile $maildir ] } {
		set filelist $maildir 
        set mailspool 1
    } else {
		set filelist [ glob -nocomplain $maildir/* ]
		set filelist [ lsort -dictionary $filelist ]
        set mailspool 0
    }
	set passed_error 0
	set passed_result 0 
	set failed 0
        set tests [ array names ::QA::datacondMDC::test::tests] 
        set numTests [ llength $tests ]
        
	foreach jobid [ lsort -dictionary $tests ] {
	    set testname [ set ::QA::datacondMDC::test::tests($jobid) ]
        
		test $testname:email {} -body {               
		;## do test here
		if  { [ regexp -nocase -- {02|pipeapi01|realimagabsapi01} $testname ] } {
		    set pass 1
		} else {
		    set pass 0
		}
        Puts 1 "$jobid test $testname expect to pass $pass"
		if  { $pass } {
		    set pattern "$jobid.+$testname"
		} else {
		    ;## set pattern "$jobid error!"
            set pattern "$jobid.+$testname.+error!"
		}
		regsub -all {\(} $pattern "\\\(" pattern
		regsub -all {\)} $pattern "\\\)" pattern
		set found 0
		set index 0
		;## do not enclose pattern as '$pattern' or "$pattern" for egrep
		foreach file $filelist {
			set rc [ catch { exec grep -nocase -E {--regexp=$pattern} $file } err ]
		    if  { !$rc } {
				Puts 1 "found $pattern in $file"
				set found 1
				if  { ! $pass } {
			    	set cmd1 "exec grep \"St9bad_alloc\" $file"
			    	set rc1 [ catch { eval $cmd1 } err1 ]
			    	if  { $rc1 } {
						Puts 1 "$testname: $jobid passed: $err"
						incr passed_error
			    	} else {
						set msg "$testname: $jobid failed: $err, $err1"                                    
						incr failed
						Puts 1 $msg
			    	}
				} else {
			    	incr passed_result 
				}
            	if	{ ! $mailspool } {
					set filelist [ lreplace $filelist $index $index ]
					Puts 1 "files remaining [ llength $filelist ]"
					break
            	}
		    } else {
				incr index 1
		    }
		}
		if  { ! $found } {
		    incr failed 1
		    Puts 1 "$testname has no email: $jobid failed"
            return "$testname has no email: $jobid failed"
		}
	    } -result {}           
    	}

	test "Overall Result" {} -body {
	    set total_emails [ expr $passed_error + $passed_result + $failed ]
	    Puts 1 "Total emails $total_emails, $passed_result expected to pass, \
		$passed_error expected to fail, got $failed unexpected results"    
	    
	    ;## if  { $total_emails == 419 && $passed_result == 137 && $passed_error == 282 && !$failed } {
	    ;## 	Puts 1 "TEST PASSED" }
            
            if	{ $failed } {
            	return -code error "TEST FAILED, expected 419 emails, 137 passed and 282 failed but got \
           	 	$total_emails emails total, $passed_result expected to pass, \
				$passed_error expected to fail and $failed unexpected results"
	    }
	} -result {} 
    }
    
    #set testList [concat $exceptionList $successList]
    Puts 1 "clear emails $clearEmails, maildir $maildir"
    if	{ $clearEmails } {
	Puts 1 "deleting emails"
	catch { eval file delete [ glob -nocomplain $maildir/* ] } err
	set emails [ glob -nocomplain $maildir/* ] 
	Puts 1 "delete email $err, remaining $emails"
    }
    
    Puts 1 "tests $testList"
    
    namespace export runTest
    
  	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# submit job to ldas
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::     
    proc runTest { test cmd } {
	
    	test $test {} -body {    
	    set cmd [ subst $cmd ]
	    Puts 1 "cmd $cmd"
	    if 	{[catch {SubmitJob job $cmd \
			     $::QA::datacondMDC::test::email } err]} {
                Puts 1 "Error: [ set job(command)]\n$err"
		LJdelete job
		return $err
	    }                
	    set jobid $job(jobid)
	    Puts 1 $jobid
	    LJdelete job
	    set ::QA::datacondMDC::test::tests($jobid) $test
	    list
    	} -match regexp -result {}

    }
   	
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# write list of tests submitted with job number to disk for sourcing later
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   
    proc writeTests {} {
     	if	{ [ catch {
	    set jobListFile [ set ::QA::datacondMDC::test::jobListFile ]
	    set fd [ open $jobListFile w ]
	    puts $fd "array set ::QA::datacondMDC::test::tests \
                	[ list [ array get ::QA::datacondMDC::test::tests ] ]"
	    close $fd
	} err ] } {
	    Puts 0 $err 
	}
    } 
    
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# file event handler for checking incoming mail
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   
    ;## this is a file event handler or after event handler
    ;## will force to verify email after no change in file mod time in an hour
    
    proc checkEmailsRecv { fd numTests maildir } {
	
    	set rc [ catch { eof $fd } ]
    	if	{ $rc } {
			fileevent $fd readable {}
            ::QA::datacondMDC::test::verifyEmail 			
            Puts 1 "encounter eof $fd"
			catch { close $fd }
            set ::$fd 1
            return
        }
        
        ;## no email 
        if	{ [ file isfile $maildir ] } {
        	set rc [ catch { exec grep -c LDAS-DEV $maildir } numEmails ]
            if	{ $rc } {            	
                return 
            }
        } else {
        	set mails [ glob -nocomplain $maildir/* ]
            set numEmails [ llength $mails ]
        }
        set mtime [ file mtime $maildir ]
        set curtime [ clock seconds ]
        set duration [ expr $curtime - $mtime ] 

        if	{ $numEmails && ( $numEmails >= $numTests )  || ( $duration >= 600 ) } {
	    Puts 1 "numTests $numTests #emails $numEmails, mtime $mtime curtime $curtime duration $duration"
			fileevent $fd readable {}
	   	 	::QA::datacondMDC::test::verifyEmail $maildir
            catch { close $fd }
            catch { after cancel [ set ::QA::datacondMDC::test::checkEmailsRecvAfterId ] }
            set ::$fd 1
        }  
    }
    
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# check if all emails have arrived
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   
     
    proc checkJobEmails {} {
	
    	upvar maildir maildir
        
        set numTests [ llength [ array names ::QA::datacondMDC::test::tests ] ]
		
        set fd [ open $maildir r ] 
        # Puts 1 "numTests $numTests, please put up your mail client"
		catch { unset ::$fd }
    	fileevent $fd readable [ list ::QA::datacondMDC::test::checkEmailsRecv $fd $numTests $maildir ]
        
        #set ::QA::datacondMDC::test::checkEmailsRecvAfterId \
		#	[ after  10000 [ list ::QA::datacondMDC::test::checkEmailsRecv $fd $numTests $maildir ] ]				
		vwait ::$fd
		Puts 1 "unset ::$fd" 
		unset ::$fd		
    }
    
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# copy the datacondMDC job file from ldas to local directory
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   
    proc getJobListFile {} {
	
		if	{ [ catch {
        	set jobListFile [ set ::QA::datacondMDC::test::jobListFile ]
			set url http://www.${::HOST}/$jobListFile
			Puts 1 "url $url"
            set localfile $::TMP/datacondMDC.jobs[pid]
			LJcopy $url $localfile
			set ::QA::datacondMDC::test::jobListFile $localfile
			Puts 1 "jobListFile [ set ::QA::datacondMDC::test::jobListFile ]: [ file exist [ set ::QA::datacondMDC::test::jobListFile ] ]"
		} err ] } {
        	Puts 1 $err
			return -code error $err
		}
	}
    
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	# main 
	#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   
    if	{ $mergeJobs } {
		catch { source [ set ::QA::datacondMDC::test::jobListFile ] } err
		Puts 1 "merged [ set ::QA::datacondMDC::test::jobListFile ]: $err"
    }
    
    if	{ [ string match "*email*" $testList ] } {
    	getJobListFile
		Puts 1 "sourcing $jobListFile"
	    source $jobListFile
	    checkJobEmails            
    } else { 
	
	foreach test $testList {
	    regexp {([^\d]+)} [ file rootname [ file tail $test ] ] -> testname
	    Puts 1 "testname $testname"
	    if	{ ! [ string length [ file extension $test ] ] } {
	    	set file [ file join  $testscriptdir ${testname}.tcl ]
	    } else {
        	set file $test
	    }
	    catch {file attributes $file -permissions u+x}
	    set errfile [file join /tmp [file rootname $file].[pid]]
	    source $file
	    set testListed [ info procs ${testname}0* ]
	    set testListed [ lsort -dictionary -unique $testListed ]
	    Puts 1 "tests $testListed"
	    foreach localtest $testListed {
        	eval $localtest
		after 2000
	    }
	    writeTests
	    ;## get any errors
	    set err {}
	    catch {
		set fd [open $errfile "r"]
		set err [read -nonewline $fd]
		close $fd
		file delete -- $errfile
	    }

	    if {[string length $err]} {
		Puts 1 "Errors for $file:\n$err\n"
	    }
	} 
    
    if	{ $checkEmailsFlag } {	
		getJobListFile
		checkJobEmails
	}
    
    }
    
    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
    
} ;## namespace - ::QA::datacondMDC::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::datacondMDC::test	

