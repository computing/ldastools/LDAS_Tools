#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: complexapi02a.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## COMPLEXAPI02A
;##
;## test complex() action exceptions when both args have zero length
;## 

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set precs { float double }

set ifilepre N200
set dtype R4
set ofilepre COMPLEXAPI01
set letter A
set length 200

set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
set prefix $ofilepre$letter$dtype
set ofile ldas_outgoing/mdc/output/$prefix.ilwd
set efile ldas_outgoing/mdc/input/$prefix.exp

set cmd "\
ldasJob { -name $user -password $pwrd -email $email } {
  conditionData
    -inputprotocol { file:/$ifile }
    -returnprotocol { file:/$ofile }
    -outputformat { ilwd ascii }
    -aliases { idata = mdc_input_data:chan_01:data; }
    -algorithms {
"

;## repeat for each data type
foreach prec $precs {

  set cmd "${cmd}
      lhs = ${prec}(idata);
      rhs = ${prec}(idata);

      res = complex(lhs, rhs);
      
      lhs2 = real(res);
      rhs2 = imag(res);

      ldiff = sub(lhs2, idata);
      ldiff = abs(ldiff);
      ldiff = max(ldiff);

      rdiff = sub(rhs2, idata);
      rdiff = abs(rdiff);
      rdiff = max(rdiff);

      output(ldiff, _, _, ldiff, output of complexapi02a);
      output(rdiff, _, _, rdiff, output of complexapi02a);
"
}

  set cmd "$cmd
    }
}"

sendCmd $cmd


