#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: coherenceapi02a.tcl,v 1.3 2004/01/14 20:38:20 Philip.Charlton Exp $

;## COHERENCEAPI02A
;## psd api tests
;## First test set: correct values on sequence
;## for length 1024, default fft length and overlap
;## Confirm output is sequence of (purely real) unity,
;## assissted by supplied statistics

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }

set ifilepre "N13" ;## Use data file of size 2^13 = 8192
set ofilepre "COHERENCEAPI02"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost A$post  ;## Need to change this for tests A, B, C
    set prefix $ofilepre$ppost
    set ofile ldas_outgoing/mdc/output/${prefix}.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases {
            data=mdc_input_data:chan_01:data;
        }
        -algorithms {
            x = slice(data, 0, 1024, 1);
            spec = coherence(x,x);
            output(spec,_,_,spec,spectrum data);
            rspec = real(spec);
            ispec = imag(spec);
            minrspec = min(rspec);
            output(minrspec,_,_,minrspec,min real spectrum data);           
            maxrspec = max(rspec);
            output(maxrspec,_,_,maxrspec,max real spectrum data);           
            rmsrspec = rms(rspec);
            output(rmsrspec,_,_,rmsrspec,rms real spectrum data);
            minispec = min(ispec);           
            output(minispec,_,_,minispec,min imag spectrum data);           
            maxispec = max(ispec);
            output(maxispec,_,_,maxispec,max imag spectrum data);           
            rmsispec = rms(ispec);
            output(rmsispec,_,_,rmsispec,rms imag spectrum data);           
        }
    } "

    sendCmd $cmd
}
