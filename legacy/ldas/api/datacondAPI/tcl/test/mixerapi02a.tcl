#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: mixerapi02a.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## MIXERAPI02
;## mixer api tests
;## correct values on unbroken sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N200"
set ofilepre "MIXERAPI02"
set idir ldas_outgoing/mdc/input
set odir ldas_outgoing/mdc/output

foreach post $postfixes {
    set ifile ${idir}/${ifilepre}${post}.sim
    set prefix ${ofilepre}A${post}
    set efile ${idir}/${prefix}.exp
    set ofile ${odir}/${prefix}.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/${ifile} file:/${efile} }
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases {raw=mdc_input_data:chan_01:data; exp=${prefix}:mixout:data }
        -algorithms {
            x = slice(raw, 0, 100, 1);
            f = value(0.125);
            phi = value(0.1);

            rx = real(x);
            ix = imag(x);
            rvarX = variance(rx);
            output(rvarX,_,_,rvarX,Variance of real input data);
            rrmsX = rms(rx);
            output(rrmsX,_,_,rrmsX,RootMeanSquare of real input data);
            ivarX = variance(ix);
            output(ivarX,_,_,ivarX,Variance of imag input data);
            irmsX = rms(ix);
            output(irmsX,_,_,irmsX,RootMeanSquare of imag input data);

            z = mix(phi, f, x);

            rz = real(z);
            rexp = real(exp);
            rdif = sub(rz,rexp);
            rminD = min(rdif);
            output(rminD,_,_,rminD,Minimum real deviation);
            rmaxD = max(rdif);
            output(rmaxD,_,_,rmaxD,Maximum real deviation);
            rvarD = variance(rdif);
            output(rvarD,_,_,rvarD,Variance of real deviation);
            rrmsE = rms(rexp);
            output(rrmsE,_,_,rrmsE,RootMeanSquare of real expected result);

            iz = imag(z);
            iexp = imag(exp);
            idif = sub(iz,iexp);
            iminD = min(idif);
            output(iminD,_,_,iminD,Minimum imag deviation);
            imaxD = max(idif);
            output(imaxD,_,_,imaxD,Maximum imag deviation);
            ivarD = variance(idif);
            output(ivarD,_,_,ivarD,Variance of imag deviation);
            irmsE = rms(iexp);
            output(irmsE,_,_,irmsE,RootMeanSquare of imag expected result);

            output(z,_,_,mix,Mixer result);
        }
    } "

    sendCmd $cmd
}

