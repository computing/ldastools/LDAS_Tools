#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: linfiltperf03.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;## LinFilt performance test

;## LINFILTPERF03
;## Performance of a zero-pole filter

set substitution "a = slice(araw,0,13,1); b = slice(braw,0,13,1);"

foreach size { 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 } {
  
  set sub "
    x = slice(xraw,0,$size,1);

    t00 = user_time();

    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x); y = linfilt(b,a,x);
    
    t32 = user_time();
    t32 = sub(t32,t00);
    duration = div(t32,32);
    output(duration,_,_,time,zero-pole linear filter on size ${size});
    "

  set substitution "$substitution$sub"

}

foreach type { R4 R8 C8 C16 } {
  
  set cmd "
  ldasJob { -name $user -password $pwrd -email $email } 
  { 
    conditionData 
    -inputprotocol  file:/ldas_outgoing/mdc/input/N23$type.sim
    -returnprotocol file:/ldas_outgoing/mdc/output/LINFILTPERF03$type.ilwd -outputformat { ilwd ascii } 
    -aliases { xraw = mdc_input_data:chan_01:data; araw = mdc_input_data:chan_03:data; braw = mdc_input_data:chan_02:data } 
    -algorithms
	{ 
      $substitution
    } 
  }
  "

sendCmd $cmd

}
