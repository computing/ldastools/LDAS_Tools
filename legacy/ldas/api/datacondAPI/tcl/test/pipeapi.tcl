#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval pipeapi {

	namespace export pipeapi*
	
	proc pipeapi01 {} {
	
	if	{ [ catch {
		set procname [ string toupper [ info level 0 ]]	
		set postfixes { 1 2 3 4 5 6 7 8 }
		set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
		
		foreach post $postfixes {

    	set ifile ldas_outgoing/mdc/input/PIPE01R4_$post.ilwd
    	set ofile ldas_outgoing/mdc/output/PIPE01R4_$post.ilwd
		set subject datacondMDC:conditionData:$procname:$post
		
		set cmd "conditionData
		-subject { $subject }
		-inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile } -outputformat { ilwd ascii }
        -aliases { raw = mdc_input_data:chan_01:data }
        -algorithms {

            cooked  = slice(raw,0,4194304,1);

            y10 = resample(cooked,1,4);
            y09 = resample(y10,1,2);
            y08 = resample(y09,1,2);
            y07 = resample(y08,1,2);

            y10 = mix(0.0,0.5,y10);
            y09 = mix(0.0,0.5,y09);
            y08 = mix(0.0,0.5,y08);
            y07 = mix(0.0,0.5,y07);

            y10 = resample(y10,1,4);
            y09 = resample(y09,1,4);
            y08 = resample(y08,1,4);
            y07 = resample(y07,1,4);

            tmp = real(y10);
            sr10 = all(tmp);
            tmp = imag(y10);
            si10 = all(tmp);

            tmp = real(y09);
            sr09 = all(tmp);
            tmp = imag(y09);
            si09 = all(tmp);

            tmp = real(y08);
            sr08 = all(tmp);
            tmp = imag(y08);
            si08 = all(tmp);

            tmp = real(y07);
            sr07 = all(tmp);
            tmp = imag(y07);
            si07 = all(tmp);

            p10 = psd(y10,256);
            output(p10,_,_,p10,512 Hz about 1024 Hz);
            p09 = psd(y09,256);
            output(p09,_,_,p09,256 Hz about 512 Hz);
            p08 = psd(y08,256);
            output(p08,_,_,p08,128 Hz about 256 Hz);
            p07 = psd(y07,256);
            output(p07,_,_,p07,64 Hz about 128 Hz);
        }"
		runTest $subject $cmd
        after 1000
		}
	} 	err ] } {
    	return -code error $err
	}
	}
}	
namespace import pipeapi::*	
	
		
	
