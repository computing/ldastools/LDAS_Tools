#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: resampleperf03.tcl,v 1.11 2004/01/14 20:38:20 Philip.Charlton Exp $

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;## Resample performance test

;## RESAMPLEPERF03
;## Upsample by 2

set substitution ""

foreach size { 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 } {
  
  set sub "
    x = slice(xraw,0,$size,1);

    t00 = user_time();

    y = resample(x,2,1);
    y = resample(x,2,1);
    y = resample(x,2,1);
    y = resample(x,2,1);
    
    t04 = user_time();
    t04 = sub(t04,t00);
    duration = div(t04,4);
    output(duration,_,_,time,upsample by 2 on size ${size});
    "

  set substitution "$substitution$sub"

}

foreach type { R4 R8 C8 C16 } {
  
  set cmd "
  ldasJob { -name $user -password $pwrd -email $email } 
  { 
    conditionData 
    -inputprotocol  file:/ldas_outgoing/mdc/input/N23$type.sim
    -returnprotocol file:/ldas_outgoing/mdc/output/RESAMPLEPERF03$type.ilwd -outputformat { ilwd ascii } 
    -aliases { xraw = mdc_input_data:chan_01:data } 
    -algorithms
	{ 
      $substitution
    } 
  }
  "

sendCmd $cmd

}
