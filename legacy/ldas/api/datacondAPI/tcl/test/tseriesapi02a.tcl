#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: tseriesapi02a.tcl,v 1.4 2004/01/14 20:38:20 Philip.Charlton Exp $

;## TSERIESAPI02
;##
;## test for tseries action
;## 

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { R4 R8 C8 C16 }

set ifilepre N200
set ofilepre TSERIESAPI02
set testcase A

;## repeat for each data type
foreach dtype $dtypes {

    set prefix $ofilepre$testcase$dtype
    set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    set ofile ldas_outgoing/mdc/output/$prefix.ilwd
    set efile ldas_outgoing/mdc/input/$prefix.exp

    set cmd "
    ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; }
        -algorithms {
            x = slice(idata,0,128,1);
            
            ts1 = tseries(x, 2048.0);
            ts2 = tseries(x, 2048.0, 6100000);
            ts3 = tseries(x, 2048.0, 6100000, 1000);
            ts4 = tseries(x, 2048.0, 6100000, 1000, 2.0);
            ts5 = tseries(x, 2048.0, 6100000, 1000, 2.0, 1.0);

            output(ts1,_,_,ts1,time series 1);
            output(ts2,_,_,ts2,time series 2);
            output(ts3,_,_,ts3,time series 3);
            output(ts4,_,_,ts4,time series 4);
            output(ts5,_,_,ts5,time series 5);
        }
    } "

    sendCmd $cmd
}
