#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: psdapi02c.tcl,v 1.10 2004/01/14 20:38:20 Philip.Charlton Exp $

;## PSDAPI02C
;## psd api tests
;## First test set: correct values on sequence
;## for length 1024 and fft length = 256 and overlap = 128

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;##set postfixes { "C8" }
set postfixes { "R4" "R8" "C8" "C16" }

set ifilepre "N13" ;## Use data file of size 2^13 = 8192
set ofilepre "PSDAPI02"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost C$post  ;## Need to change this for tests A, B, C
    set prefix $ofilepre$ppost
    set ofile ldas_outgoing/mdc/output/${prefix}.ilwd
    set efile ldas_outgoing/mdc/input/${prefix}.exp

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile file:/$efile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases {
            data=mdc_input_data:chan_01:data;
            specexp=${prefix}:specout:data
        }
        -algorithms {
            x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);

            spec = psd(x, 256, _, 128, _);
            output(spec,_,_,spec,spectrum data);
            output(specexp,_,_,specexp,expected spectra);

            specdif = sub(spec, specexp);
            output(specdif,_,_,specdif,diff of spec and specexp);

            specmin = min(specdif);
            output(specmin,_,_,specmin,minimum of difference);

            specmax = max(specdif);
            output(specmax,_,_,specmax,maximum of difference);

            specrms = rms(specexp);
            output(specrms,_,_,specrms,rms of expected result);
        }
    } "

    sendCmd $cmd
}
