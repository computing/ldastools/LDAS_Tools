#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: psdapi02d.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## PSD test
;##
;## test output of PSD on complex spike of frequency f0
;## (nyquist = 4096 Hz, f0 = 1024 Hz)

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { C8 C16 }

set ifilepre PSDAPI02D
set ofilepre PSDAPI02D
set length 8192
set fftlength 2048
set overlaplength 1024

;## repeat for each data type
foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    set prefix $ofilepre$dtype
    set ofile ldas_outgoing/mdc/output/$prefix.ilwd
    set efile ldas_outgoing/mdc/input/$prefix.exp

    set cmd "ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile file:/$efile }
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases {
            idata = mdc_input_data:chan_01:data;
            espec = $prefix:specout:data;
        }
        -algorithms {
            x = slice(idata,0,${length},1);
            rx = real(x);
            n1 = size(rx);
            output(n1,_,_,n1,length of input data before psd);

            spec = psd(x,${fftlength},_,${overlaplength},_);

            difspec = sub(spec,espec);
            minspec = min(difspec);
            output(minspec,_,_,minspec,minimum of difference for spectrum);

            maxspec = max(difspec);
            output(maxspec,_,_,maxspec,maximum of difference for spectrum);

            rmsspec = rms(spec);
            output(rmsspec,_,_,rmsspec,rms of spectrum);

            output(spec,_,_,spec,computed values of spectrum);
        }
    } "

    sendCmd $cmd
}
