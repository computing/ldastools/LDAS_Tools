#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: waveletapi02a.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## Test of wavelet actions WaveletForward, WaveletInverse, GetLayer
;## with all wavelet types (Haar, Biorthogonal, Daubechies, Symlet)
;## and data types (float, double).

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set wavelets { Biorthogonal Daubechies Symlet Haar }
set dtypes { R4 R8 }

foreach dtype $dtypes {

   set test_all "ts = tseries( idata, 2048. );
                 rms_in = rms(ts); "

   foreach wtype $wavelets {
     append test_all " 
                w = ${wtype}();
                u = WaveletForward( ts, w, -1);
                l1 = GetLayer(u,1);
                ts2 = WaveletInverse(u);
                tsdiff = sub(ts, ts2);
                absdiff = abs(tsdiff);
                maxd = max(absdiff);
                maxd = div(maxd,rms_in);
                output(maxd,_,_,maxd, ${wtype} ${dtype}:
                  maximum relative difference \(original-reconstructed\)/rms_in);               "

;## more output:
#append test_all "
#                output(u,_,${wtype}_forw.ilwd,u, WaveletForward output );
#                output(l1,_,${wtype}_l1.ilwd,l1, layer1 of wavelet );
#                output(ts2,_,${wtype}_inv.ilwd,ts2, WaveletInverse output);
#                output(tsdiff,_,${wtype}_diff.ilwd,tsdiff, difference);
#               "
   }

    set ifile /ldas_outgoing/jobs/mdc/input/N200$dtype.sim

    set cmd "ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/$ifile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data }
        -algorithms { $test_all }
    }"

#   puts $cmd
   sendCmd $cmd
   exec sleep 1
}
