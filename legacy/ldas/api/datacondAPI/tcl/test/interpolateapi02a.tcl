#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 
# \
exec tclsh "$0" "$@"

;## $Id: interpolateapi02a.tcl,v 1.4 2004/01/14 20:38:20 Philip.Charlton Exp $

;## INTERPOLATEAPI02
;## interpolate api tests
;## Test correct values for broken vs continuous interpolate
;## with random data

proc sendCmd {cmd} {
    regsub -all -- {#[^\n]*} $cmd {} cmd
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set testname "INTERPOLATEAPI"
set testnum "02"
set testcase "A"
set ifilepre "N200" ;## dummy data

set postfixes { "R4" "R8" "C8" "C16" }

set ofilepre $testname$testnum$testcase

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd

    set cmd "
    ldasJob { -name $user -password $pwrd -email $email } {
      conditionData 
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases {
          srate = mdc_input_data:chan_01:rate;
          data = mdc_input_data:chan_01:data;
          len = mdc_input_data:chan_01:length;
        }
        -algorithms {

          x = tseries(data, 16384.0, 6100000, 0);

          alpha = value(0.1);
          order = value(2);

          # Break up the input and interpolate each separately
          rx = real(x);
          len0 = size(rx);
          len0 = div(len0, 2);

          x0 = slice(x, 0, len0, 1);
          x1 = slice(x, len0, len0, 1);

          y0 = interpolate(x0, alpha, order, z);
          y1 = interpolate(x1, z);

          clear(z);

          # Now do the output in one step
          y = interpolate(x, alpha, order);

          # Break up the output
          ry0 = real(y0);
          iy0 = imag(y0);
          sizey0 = size(ry0);

          ry1 = real(y1);
          iy1 = imag(y1);
          sizey1 = size(ry1);

          y0ref = slice(y, 0, sizey0, 1);
          y1ref = slice(y, sizey0, sizey1, 1);

          ry0ref = real(y0ref);
          iy0ref = imag(y0ref);

          ry1ref = real(y1ref);
          iy1ref = imag(y1ref);

          a0 = sub(ry0ref, ry0);
          a0 = mul(a0, a0);

          a1 = sub(ry1ref, ry1);
          a1 = mul(a1, a1);

          b0 = sub(iy0ref, iy0);
          b0 = mul(b0, b0);
          b1 = sub(iy1ref, iy1);
          b1 = mul(b1, b1);

          z0 = add(a0, b0);
          z0 = sqrt(z0);

          z1 = add(a1, b1);
          z1 = sqrt(z1);

          max0 = max(z0);
          max1 = max(z1);
          
          output(x,_,_,xpre,raw data);
          output(y,_,_,y,interpolated data);

          output(max0,_,_,max0,max difference of broken vs continuous interpolate-should be small);
          output(max1,_,_,max1,max difference of broken vs continuous interpolate-should be small);

        }
    }"

    sendCmd $cmd
}
