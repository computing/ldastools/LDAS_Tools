#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval siggenapi {

	namespace export siggenapi*
     
	proc siggenapi02a {} {

		if	{ [ catch {
    	     
          set name [ namespace current ]
		set procname [ info level 0 ]
          namespace import [ namespace parent $name ]::*  
            
		set dtypes { R4 }

		set ifilepre N200
		set ofilepre SIGGENAPI02
		set testcase A
       
		;## repeat for each data type
		foreach dtype $dtypes {

    		set prefix $ofilepre$testcase$dtype
    		set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    		set ofile ldas_outgoing/mdc/output/$prefix.ilwd
    		set efile ldas_outgoing/mdc/input/$prefix.exp
		set subject datacondMDC:conditionData:$procname:$dtype
            
    		set cmd "conditionData
        -subject $subject 
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; }
        -algorithms {
            n1 = value(113);
            n2 = value(64);
            f1 = value(0.2);
            f2 = value(-0.1);
            phi1 = value(2.0);
            phi2 = value(-1.0);
            A1 = value(1.5);
            A2 = value(-0.5);
            x1 = value(3.5);
            x2 = value(-2.1);

            s1 = sine(n1, f1, phi1, A1, x1);
            s1 = sine(n2, f2, phi2, A2, x2);

            s2 = cosine(n1, f1, phi1, A1, x1);
            s2 = cosine(n2, f2, phi2, A2, x2);

            s3 = sawtooth(n1, f1, phi1, A1, x1);
            s3 = sawtooth(n2, f2, phi2, A2, x2);

            s4 = square(n1, f1, phi1, A1, x1);
            s4 = square(n2, f2, phi2, A2, x2);

            s5 = triangle(n1, f1, phi1, A1, x1);
            s5 = triangle(n2, f2, phi2, A2, x2);

            output(s1,_,_,_,signal 1);
            output(s2,_,_,_,signal 2);
            output(s3,_,_,_,signal 3);
            output(s4,_,_,_,signal 4);
            output(s5,_,_,_,signal 5);
    		} "
        	runTest $subject $cmd
          after 1000
    		}
    	} err ] } {
    		return -code error $err
	}  
	}
}
namespace import siggenapi::*
