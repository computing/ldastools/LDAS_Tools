#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: linfiltapi01a.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## LINFILTAPI01
;## linfilt api tests
;## First test set: correct values on sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N200"
set ofilepre "LINFILTAPI01"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost A$post
    set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

    set cmd " ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases { xlong = mdc_input_data:chan_01:data; a = mdc_input_data:chan_03:data; blong = mdc_input_data:chan_02:data }
        -algorithms {
            x = slice(xlong,0,100,1);
            b = slice(blong,0,13,1);

            a0 = slice(a,0,0,1);
            y = linfilt(b,a0,x);

            output(y,_,_,y,linear filter output);
        }
    } "

    sendCmd $cmd
}
