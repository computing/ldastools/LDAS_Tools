#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: complexapi01c.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## COMPLEXAPI01C
;##
;## test complex() action exceptions when both args have zero length
;## 

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set precs { float double }

set ifilepre N200
set dtype R4
set ofilepre COMPLEXAPI01
set letter C
set length 200

set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
set prefix $ofilepre$letter$dtype
set ofile ldas_outgoing/mdc/output/$prefix.ilwd
set efile ldas_outgoing/mdc/input/$prefix.exp

;## repeat for each data type
foreach prec $precs {

  set cmd "\
  ldasJob { -name $user -password $pwrd -email $email } {
    conditionData
      -inputprotocol { file:/$ifile }
      -returnprotocol { file:/$ofile }
      -outputformat { ilwd ascii }
      -aliases { idata = mdc_input_data:chan_01:data; }
      -algorithms {

        lhs = ${prec}(idata);
        lhs = slice(lhs, 0, 0, 1);

        rhs = ${prec}(idata);
        lhs = slice(rhs, 0, 0, 1);

        res = complex(lhs, rhs);

        output(res, _, _, res, output of arithapi01a);

      }
  }"

  sendCmd $cmd
}


