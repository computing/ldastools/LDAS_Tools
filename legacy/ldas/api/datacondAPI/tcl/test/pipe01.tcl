#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: pipe01.tcl,v 1.10 2004/01/14 20:38:20 Philip.Charlton Exp $

;## PIPE01

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { 1 2 3 4 5 6 7 8 }

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/PIPE01R4_$post.ilwd
    set ofile ldas_outgoing/mdc/output/PIPE01R4_$post.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile } -outputformat { ilwd ascii }
        -aliases { raw = mdc_input_data:chan_01:data }
        -algorithms {

            cooked  = slice(raw,0,4194304,1);

            y10 = resample(cooked,1,4);
            y09 = resample(y10,1,2);
            y08 = resample(y09,1,2);
            y07 = resample(y08,1,2);

            y10 = mix(0.0,0.5,y10);
            y09 = mix(0.0,0.5,y09);
            y08 = mix(0.0,0.5,y08);
            y07 = mix(0.0,0.5,y07);

            y10 = resample(y10,1,4);
            y09 = resample(y09,1,4);
            y08 = resample(y08,1,4);
            y07 = resample(y07,1,4);

            tmp = real(y10);
            sr10 = all(tmp);
            tmp = imag(y10);
            si10 = all(tmp);

            tmp = real(y09);
            sr09 = all(tmp);
            tmp = imag(y09);
            si09 = all(tmp);

            tmp = real(y08);
            sr08 = all(tmp);
            tmp = imag(y08);
            si08 = all(tmp);

            tmp = real(y07);
            sr07 = all(tmp);
            tmp = imag(y07);
            si07 = all(tmp);

            p10 = psd(y10,256);
            output(p10,_,_,p10,512 Hz about 1024 Hz);
            p09 = psd(y09,256);
            output(p09,_,_,p09,256 Hz about 512 Hz);
            p08 = psd(y08,256);
            output(p08,_,_,p08,128 Hz about 256 Hz);
            p07 = psd(y07,256);
            output(p07,_,_,p07,64 Hz about 128 Hz);
        }
    } "

    sendCmd $cmd
}
