#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: linfiltapi02b.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## LINFILTAPI02
;## linfilt api tests
;## Second test set: broken sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N8388608"
set ofilepre "LINFILTAPI02"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost B$post
    set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email } 
    { conditionData 
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii } 
        -aliases { raw = mdc_input_data:chan_01:data; a = mdc_input_data:chan_03:data; blong = mdc_input_data:chan_02:data } 
        -algorithms { 
            b = slice(blong,0,13,1);

            x = slice(raw,0,100,1);
            y = linfilt(b,a,x);
            output(y,_,_,y,whole);

            x1 = slice(raw,0,50,1);
            y1 = linfilt(b,a,x1,z);
            output(y1,_,_,y1,first half);
            x2 = slice(raw,50,50,1);
            y2 = linfilt(x2,z);
            output(y2,_,_,y2,second half);

            yA = slice(y,0,50,1);
            yB = slice(y,50,50,1);

            ryA = real(yA);
            ryB = real(yB);
            ry1 = real(y1);
            ry2 = real(y2);
            rd1 = sub(ryA,ry1);
            rd2 = sub(ryB,ry2);
            rmin1 = min(rd1);
            output(rmin1,_,_,rmin1,minimum of difference of real first halves);
            rmin2 = min(rd2);
            output(rmin2,_,_,rmin2,minimum of difference of real second halves);
            rmax1 = max(rd1);
            output(rmax1,_,_,rmax1,maximum of difference of real first halves);
            rmax2 = max(rd2);
            output(rmax2,_,_,rmax2,maximum of difference of real second halves);
            rrms1 = rms(ryA);
            output(rrms1,_,_,rrms1,root mean square of first half);
            rrms2 = rms(ryB);
            output(rrms2,_,_,rrms2,root mean square of second half);

            iyA = imag(yA);
            iyB = imag(yB);
            iy1 = imag(y1);
            iy2 = imag(y2);
            id1 = sub(iyA,iy1);
            id2 = sub(iyB,iy2);
            imin1 = min(id1);
            output(imin1,_,_,imin1,minimum difference of imag first halves);
            imin2 = min(id2);
            output(imin2,_,_,imin2,minumum difference of imag second halves);
            imax1 = max(id1);
            output(imax1,_,_,imax1,maximum difference of imag first halves);
            imax2 = max(id2);
            output(imax2,_,_,imax2,maximum difference of imag second halves);
            irms1 = rms(iyA);
            output(irms1,_,_,irms1,root mean square of first half);
            irms2 = rms(iyB);
            output(irms2,_,_,irms2,root mean square of second half);

            output(y,_,_,y,linear filter output);
        } 
    } "

    sendCmd $cmd
}
