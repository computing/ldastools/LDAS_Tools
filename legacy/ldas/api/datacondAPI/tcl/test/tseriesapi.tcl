#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval tseriesapi {

    	namespace export tseriesapi*
     
	proc tseriesapi01a {} {

	if	{ [ catch {
     	set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
		set actions {
    	"ts = tseries();"
    	"ts = tseries(1.0);"
    	"ts = tseries(1.0, 2048.0);"
    	"ts = tseries(x, 2048);"
    	"ts = tseries(x, x);"
    	"ts = tseries(x, 2048.0, 6000000.0);"
    	"ts = tseries(x, 2048.0, 6000000, 1000.0);"
    	"ts = tseries(x, 2048.0, 6000000, 1000, 1);"
    	"ts = tseries(x, 2048.0, 6000000, 1000, 1.0, 1);"
		}
    		set procname [ info level 0 ]
		set testname "TSERIESAPI"
		set testnum "01"
		set ifilepre "N200"
          
		set dtypes { R4 R8 C8 C16 }
          
		set testcases { A B C D E F G H I J }

		;## repeat for each data type
		foreach action $actions testcase $testcases {
        ;## response caused the ; to become | and so no match
          	 set tmpaction [ string range $action 0 end-1 ]
             if	{![ string length $action ] } {
               	set tmpaction no_action
             }
  			 set ofilepre $testname$testnum$testcase

  			foreach dtype $dtypes {
					set subject datacondMDC:conditionData:$procname:$tmpaction:$dtype
    				set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    				set ofile ldas_outgoing/mdc/output/$ofilepre$dtype.ilwd

    				set cmd "conditionData
        -subject { $subject }
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; }
        -algorithms {
            x = slice(idata,0,128,1);
            $action
            output(ts,_,_,ts,time series);
    		}"
            		runTest $subject $cmd
            		after 1000
    		}
        }
    	} err ] } {
    		return -code error $err
		}  
	}

	proc tseriesapi02a {} {

		if	{ [ catch {
    		set dtypes { R4 R8 C8 C16 }

		set ifilepre N200
		set ofilepre TSERIESAPI02
		set testcase A
		set procname [ info level 0 ]
          set name [ namespace current ]
          namespace import [ namespace parent $name ]::*
          
		;## repeat for each data type
		foreach dtype $dtypes {

    		set prefix $ofilepre$testcase$dtype
    		set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    		set ofile ldas_outgoing/mdc/output/$prefix.ilwd
    		set efile ldas_outgoing/mdc/input/$prefix.exp
			set subject datacondMDC:conditionData:$procname:$dtype
            
    		set cmd "conditionData
        -subject { $subject }
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; }
        -algorithms {
            x = slice(idata,0,128,1);
            
            ts1 = tseries(x, 2048.0);
            ts2 = tseries(x, 2048.0, 6100000);
            ts3 = tseries(x, 2048.0, 6100000, 1000);
            ts4 = tseries(x, 2048.0, 6100000, 1000, 2.0);
            ts5 = tseries(x, 2048.0, 6100000, 1000, 2.0, 1.0);

            output(ts1,_,_,ts1,time series 1);
            output(ts2,_,_,ts2,time series 2);
            output(ts3,_,_,ts3,time series 3);
            output(ts4,_,_,ts4,time series 4);
            output(ts5,_,_,ts5,time series 5);
    	} "
		  runTest $subject $cmd
            after 1000
		}
		} err ] } {
    			return -code error $err
		}  
	}
} 
namespace import tseriesapi::*
