#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval mixerapi {

	set postfixes { "R4" "R8" "C8" "C16" }
	set ifilepre "N200"
	set idir ldas_outgoing/mdc/input
	set odir ldas_outgoing/mdc/output
        
    set cmdstem { conditionData
        -subject $subject
        -inputprotocol { file:/${ifile}  $efile }
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases { $aliases }
        -algorithms { $algo } 
        }
        
    set aliases_mixerapi01a { raw=mdc_input_data:chan_01:data }
   	set aliases_mixerapi01b $aliases_mixerapi01a
    set aliases_mixerapi02a { raw=mdc_input_data:chan_01:data; exp=${prefix}:mixout:data }
    set aliases_mixerapi02b $aliases_mixerapi01a
    
    set algo_mixerapi01a {
        	x = slice(raw,0,100,1);
            phi = value(0.0);
            f = value(2.0);
            z = mix(phi,f,x);
            output(z,_,_,z,mix);
   	}
        
   	set algo_mixerapi01b {
        	x = slice(raw,0,0,1);
            phi = value(0.0);
            f = value(0.125);
            z = mix(phi,f,x);
            output(z,_,_,z,mix);
    }
        
    set algo_mixerapi02a {
        	x = slice(raw, 0, 100, 1);
            f = value(0.125);
            phi = value(0.1);

            rx = real(x);
            ix = imag(x);
            rvarX = variance(rx);
            output(rvarX,_,_,rvarX,Variance of real input data);
            rrmsX = rms(rx);
            output(rrmsX,_,_,rrmsX,RootMeanSquare of real input data);
            ivarX = variance(ix);
            output(ivarX,_,_,ivarX,Variance of imag input data);
            irmsX = rms(ix);
            output(irmsX,_,_,irmsX,RootMeanSquare of imag input data);

            z = mix(phi, f, x);

            rz = real(z);
            rexp = real(exp);
            rdif = sub(rz,rexp);
            rminD = min(rdif);
            output(rminD,_,_,rminD,Minimum real deviation);
            rmaxD = max(rdif);
            output(rmaxD,_,_,rmaxD,Maximum real deviation);
            rvarD = variance(rdif);
            output(rvarD,_,_,rvarD,Variance of real deviation);
            rrmsE = rms(rexp);
            output(rrmsE,_,_,rrmsE,RootMeanSquare of real expected result);

            iz = imag(z);
            iexp = imag(exp);
            idif = sub(iz,iexp);
            iminD = min(idif);
            output(iminD,_,_,iminD,Minimum imag deviation);
            imaxD = max(idif);
            output(imaxD,_,_,imaxD,Maximum imag deviation);
            ivarD = variance(idif);
            output(ivarD,_,_,ivarD,Variance of imag deviation);
            irmsE = rms(iexp);
            output(irmsE,_,_,irmsE,RootMeanSquare of imag expected result);

            output(z,_,_,mix,Mixer result);
    }
        
    set algo_mixerapi02b {
        	x0 = slice(raw,0,100,1);
            f = value(0.125);
            phi = value(0.1);

            rx0 = real(x0);
            ix0 = imag(x0);
            rvarX = variance(rx0);
            output(rvarX,_,_,rvarX,Variance of real input data);
            rrmsX = rms(rx0);
            output(rrmsX,_,_,rrmsX,RootMeanSquare of real input data);
            ivarX = variance(ix0);
            output(ivarX,_,_,ivarX,Variance of imag input data);
            irmsX = rms(ix0);
            output(irmsX,_,_,irmsX,RootMeanSquare of imag input data);

            z0 = mix(phi,f,x0);

            x1 = slice(x0,0,50,1);
            x2 = slice(x0,50,50,1);

            phi = value(0.1);

            z1 = mix(phi,f,x1);
            output(z1,_,_,z1,First half mixer);
            z2 = mix(phi,f,x2);
            output(z2,_,_,z2,Second half mixer);

            zA = slice(z0,0,50,1);
            zB = slice(z0,50,50,1);

            rzA = real(zA);
            rzB = real(zB);
            rz1 = real(z1);
            rz2 = real(z2);
            izA = imag(zA);
            izB = imag(zB);
            iz1 = imag(z1);
            iz2 = imag(z2);

            rdifA = sub(rzA,rz1);
            output(rdifA,_,_,rdifA,First half real differences);
            rminA = min(rdifA);
            output(rminA,_,_,rminA,Minimum first half real deviation);
            rmaxA = max(rdifA);
            output(rmaxA,_,_,rmaxA,Maximum first half real deviation);

            rdifB = sub(rzB,rz2);
            output(rdifB,_,_,rdifB,Second half real differences);
            rminB = min(rdifB);
            output(rminB,_,_,rminB,Minimum second half real deviation);
            rmaxB = max(rdifB);
            output(rmaxB,_,_,rmaxB,Maximum second half real deviation);

            idifA = sub(izA,iz1);
            output(idifA,_,_,idifA,First half imag differences);
            iminA = min(idifA);
            output(iminA,_,_,iminA,Minimum first half imag deviation);
            imaxA = max(idifA);
            output(imaxA,_,_,imaxA,Maximum first half imag deviation);

            idifB = sub(izB,iz2);
            output(idifB,_,_,idifB,Second half imag differences);
            iminB = min(idifB);
            output(iminB,_,_,iminB,Minimum second half imag deviation);
            imaxB = max(idifB);
            output(imaxB,_,_,imaxB,Maximum second half imag deviation);

            output(z0,_,_,mixerbroken,Broken mixer result);
    }
        
	namespace export mixerapi*
    
    proc mixerapiTest { code { needPrefix 0 } } {
    
    	if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
            namespace import [ namespace parent $name ]::*  
                               
            set idir [ namespace eval $name set idir ]
            set odir [ namespace eval $name set odir ]
            set cmdstem [ namespace eval $name set cmdstem ]
            set postfixes [ namespace eval $name set postfixes ]
            set ifilepre [ namespace eval $name set ifilepre ]
            set algo [ namespace eval $name set algo_$procname ]
            set rc [ namespace eval $name info exist ifile_$procname ]
           	set ofilepre [ string toupper $procname ]
            
			foreach post $postfixes {
                set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim  
                set aliases [ namespace eval $name set aliases_$procname ]
				if	{ $needPrefix } {
            		set prefix ${ofilepre}$post
    				set efile file:/ldas_outgoing/mdc/input/$prefix.exp
                	set ofile ldas_outgoing/mdc/output/$prefix.ilwd
            	} else {
            		set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd    
                    set efile ""		
            	}
                set subject datacondMDC:conditionData:$procname:$post
                set aliases [ subst $aliases ]
                set algo [ subst $algo ]
                set cmd [ subst $cmdstem ]
                runTest $subject $cmd            
    		}
    	} err ] } {
    		return -code error $err
		}
	}
    
    
    ;## domain error
	proc mixerapi01a {} {

		if	{ [ catch {
			mixerapiTest A
    	} err ] } {
    		return -code error $err
		}
	}

	;## invalid argument
	proc mixerapi01b {} {
	
    	if	{ [ catch {
			mixerapiTest B
    	} err ] } {
    		return -code error $err
		}    
	}

	proc mixerapi02a {} {

		if	{ [ catch {
    		mixerapiTest A 1
    	} err ] } {
    		return -code error $err
		}    
	}


	proc mixerapi02b {} {

		if	{ [ catch {    
			mixerapiTest B
    	} err ] } {
    		return -code error $err
		}    
	}

}

namespace import mixerapi::*
 
