#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: mixerperf01.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## MIXERPERF01
;## Performance of the mixer
;## Mixer performance test

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set substitution "phi = value(0.0); f = value(0.12345);"

foreach size { 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 } {

    set sub "
        x = slice(xraw,0,$size,1);

        t00 = user_time();

        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);
        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);
        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);
        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);

        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);
        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);
        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);
        y = mix(phi,f,x); y = mix(phi,f,x); y =  mix(phi,f,x); y = mix(phi,f,x);

        t32 = user_time();
        t32 = sub(t32,t00);
        duration = div(t32,32);
        output(duration,_,_,time,mixer on size ${size});
    "

    set substitution "$substitution$sub"

}

foreach type { R4 R8 C8 C16 } {

    set cmd " ldasJob { -name $user -pwrd $pwrd -email $email }
    { conditionData
        -inputprotocol  file:/ldas_outgoing/mdc/input/N23$type.sim
        -returnprotocol file:/ldas_outgoing/mdc/output/MIXERPERF01$type.ilwd
        -outputformat { ilwd ascii }
        -aliases { xraw = mdc_input_data:chan_01:data }
        -algorithms {
            $substitution
        }
    } "

    sendCmd $cmd
}
