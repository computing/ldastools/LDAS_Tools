#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

## ******************************************************** 
##
## Name: mdc_frame.tcl 
##
## Description:
## This script will convert files consisting of a single
## column of numerical data into valid ilwd frame text files.
## The resulting frame will have a single channel consisting
## of the data from the source file as Adc channel 0.
##
## Parameters:
##
## Usage:
## The source files should look like:
## mdc_gauss 2048 int_2s
## 12345
## 54321
## 23514
## ...
##
## where the three values on the first line are the "name"
## of the data, the daq rate, and the data type respectively.
##
## Comments:
##
## ******************************************************** 

;## $time should increment from 600000000
;## $ch should be a name descriptive of the data
;## $type defaults to int_2s
;## $size should be the llength of $data
set frame_template "<ilwd name='LIGO::Frame' size='8'>
    <int_4s name='run'>1</int_4s>
    <int_4u name='frame'>1</int_4u>
    <int_4u name='dataquality'>0</int_4u>
    <int_4u dims='2' name='GTime'>\${time} 0</int_4u>
    <int_2u name='ULeapS'>0</int_2u>
    <int_4s name='localTime'>0</int_4s>
    <real_8 name='dt'>\${dt}</real_8>
    <ilwd name='rawdata:rawData:RawData:Frame'>
        <ilwd name=':adcdata:Container(AdcData)'>
            <ilwd name='MDC_DATA-\${ch}::AdcData:\${time}:0:Frame' size='13'>
                <lstring name='comment'></lstring>
                <int_4u name='channelgroup'>0</int_4u>
                <int_4u name='channelnumber'>0</int_4u>
                <int_4u name='nBits'>16</int_4u>
                <real_4 name='bias'>0</real_4>
                <real_4 name='slope'>1</real_4>
                <lstring name='units' size='6'>counts</lstring>
                <real_8 name='sampleRate'>\${rate}</real_8>
                <int_4s dims='2' name='timeOffset'>0 0</int_4s>
                <real_8 name='fShift'>0</real_8>
                <int_2u name='dataValid'>0</int_2u>
                <real_8 name='dt'>\${dt}</real_8>
                <ilwd name=':data:Container(Vect):Frame'>
                    <\$type byteorder='big' dims='\${size}' format='ascii' name='a' units='count'>\${data}</\${type}>
                </ilwd>
            </ilwd>
        </ilwd>
    </ilwd>
</ilwd>"

set time 600000000
set i 0

foreach file $argv {
   set data [ list ]
   set size 0
   set fid [ open $file r ]
   foreach { ch rate type } [ gets $fid ] { break }
   while { [ gets $fid datum ] > -1 } {
      lappend data $datum
   }
   close $fid
   set size [ llength $data ]
   set dt [ expr { 1.0 * $size / $rate } ]
   set frame [ subst $frame_template ]
   set of MDC_Frame-${time}.ilwd
   set fid [ open $of w ]
   puts $fid $frame
   close $fid
   set time [ expr { $time + [ incr i ] } ]
}

;## Sample code for producing a binary frame file:
;## set fid  [ open mdc.ilwd r ]
;## set data [ read $fid ]
;## if { [ catch {
;##    set data [ putElement $data ]
;## } err ] } {
;##    debugPuts "$err in $data"
;##    return {}
;## }   
;## set ptr [ ilwd2frame $data ]
;## frame::ptr2file foo $ptr
