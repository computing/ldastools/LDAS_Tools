#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: pr370.tcl,v 1.7 2004/01/14 20:38:20 Philip.Charlton Exp $

;## Test script for problem report 370
;##

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;## values

set ifile ldas_outgoing/mdc/input/N200R4.sim
set ofile ldas_outgoing/mdc/output/pr370.ilwd

set cmd "ldasJob {-name $user -password $pwrd -email $email }
{ conditionData -inputprotocol file:/$ifile
    -returnprotocol file:/$ofile
    -outputformat { ilwd ascii }
    -aliases { idata = mdc_input_data:chan_01:data }
    -algorithms {
        y = fft(idata);
        y = fft(idata);
        output(y,_,_,y,all);
    }
}"

sendCmd $cmd
