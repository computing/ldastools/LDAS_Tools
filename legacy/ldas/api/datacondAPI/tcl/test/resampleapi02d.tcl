#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: resampleapi02d.tcl,v 1.11 2004/01/14 20:38:20 Philip.Charlton Exp $

;## RESAMPLEAPI02
;## resample api tests
;## correct upsampling on broken sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N8388608"
set ofilepre "RESAMPLEAPI02"
set idir ldas_outgoing/mdc/input
set odir ldas_outgoing/mdc/output

foreach post $postfixes {
    set ifile ${idir}/${ifilepre}${post}.sim
    set ofile ${odir}/${ofilepre}D${post}.ilwd

    set cmd " ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/${ifile}
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases { raw = mdc_input_data:chan_01:data }
        -algorithms {
            x0 = slice(raw,0,20,1);
            y0 = resample(x0,2,1);

            x1 = slice(x0,0,10,1);
            x2 = slice(x0,10,10,1);

            y1 = resample(x1,2,1,z);
            output(y1,_,_,y1,First half upsample x 2);
            y2 = resample(x2,z);
            output(y2,_,_,y2,Second half upsample x 2);

            yA = slice(y0,0,20,1);
            yB = slice(y0,20,20,1);

            ryA = real(yA);
            ryB = real(yB);
            ry1 = real(y1);
            ry2 = real(y2);
            iyA = imag(yA);
            iyB = imag(yB);
            iy1 = imag(y1);
            iy2 = imag(y2);

            rdifA = sub(ryA,ry1);
            output(rdifA,_,_,rdifA,First half real differences);
            rminA = min(rdifA);
            output(rminA,_,_,rminA,Minimum first half real deviation);
            rmaxA = max(rdifA);
            output(rmaxA,_,_,rmaxA,Maximum first half real deviation);

            rdifB = sub(ryB,ry2);
            output(rdifB,_,_,rdifB,Second half real differences);
            rminB = min(rdifB);
            output(rminB,_,_,rminB,Minimum second half real deviation);
            rmaxB = max(rdifB);
            output(rmaxB,_,_,rmaxB,Maximum second half real deviation);

            idifA = sub(iyA,iy1);
            output(idifA,_,_,idifA,First half imag differences);
            iminA = min(idifA);
            output(iminA,_,_,iminA,Minimum first half imag deviation);
            imaxA = max(idifA);
            output(imaxA,_,_,imaxA,Maximum first half imag deviation);

            idifB = sub(iyB,iy2);
            output(idifB,_,_,idifB,Second half imag differences);
            iminB = min(idifB);
            output(iminB,_,_,iminB,Minimum second half imag deviation);
            imaxB = max(idifB);
            output(imaxB,_,_,imaxB,Maximum second half imag deviation);

            output(y0,_,_,upborken,Broken upsample result);
        }
    } "

    sendCmd $cmd
}

