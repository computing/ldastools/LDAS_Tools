#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: signumapi02a.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## SIGNUMAPI02
;## signum api tests
;## correct processing of test sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

# actually test dont't use data from file 
# but LDAS expects some input
set ifile ldas_outgoing/jobs/mdc/input/N200R8.sim

set ofilepre "SIGNUMAPI02A"
set ofile ldas_outgoing/mdc/output/$ofilepre.ilwd

set test "
            m1 = mean( y );
            z = abs( y );
            m2 = mean( z );
            nz = sub( 1. , m2 );
            nz = mul( nz, 100.);
"

set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/$ifile
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -algorithms {
            d = ramp( 100, 1., -25. );
            d = tseries(d, 2048., 6100000);

            y = signum( d );
            $test
            output(m1,_,_,m1, expected value 0.49 );
            output(nz,_,_,nz, expected number of zeroes 1 );

            y = signum( d, -10.5 );
            $test
            output(m1,_,_,m1, expected value 0.70 );
            output(nz,_,_,nz, expected number of zeroes 0 );

            y = signum( d, 10., 20. );
            $test
            output(m1,_,_,m1, expected value 0.19 );
            output(nz,_,_,nz, expected number of zeroes 11 );
        }
    } "

    sendCmd $cmd
