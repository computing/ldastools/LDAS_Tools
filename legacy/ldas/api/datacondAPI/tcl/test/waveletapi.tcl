#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

;## this test causes runtime job timed out in datacond.
namespace eval waveletapi {
	
    set postfixes { R4 R8 }
	set ifilepre N200
	set ofilepre WAVELETAPI01
    
    set cmdstem { conditionData
    -subject $subject
     -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases { in = mdc_input_data:chan_01:data }
        -algorithms $algo
    }
    namespace export waveletapi*
    
	proc waveletapiTest { code algo } {

    	if	{ [ catch {        	
			set procname [ info level -1 ]
        	set name [ namespace current ]
            namespace import [ namespace parent $name ]::*
            set ifilepre 	[ namespace eval $name set ifilepre ]
            set ofilepre 	[ namespace eval $name set ofilepre ]
            set postfixes 	[ namespace eval $name set postfixes ]
        	set cmdstem [ namespace eval $name set cmdstem ]
        	foreach post $postfixes {
    			set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    			set ppost ${code}$post
    			set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd
				set subject datacondMDC:conditionData:$procname:$post
            
            	set cmd [ subst $cmdstem ]               
            	runTest $subject $cmd    
            after 1000  
            }   
    	} err ] } {
    		return -code error $err
		}
	}
            
	proc waveletapi01a {} {

		if	{ [ catch {
			set algo "{
            x = tseries( in, 16384. );
            y = WaveletForward(x);
            output(y,_,_,y,WaveletForward output);
    		} "
			waveletapiTest a $algo
    	} err ] } {
    		return -code error $err
		}  
	}

	proc waveletapi01b {} {

		if	{ [ catch {
			set algo "{
            x = tseries( in, 16384. );
            b = Biorthogonal( 12, 0 );
            y = WaveletForward( x, b, 2 );
            z = WaveletInverse( y, 1, 2 );
            output(z,_,_,z,wavelet inverse output);
    		} "	
			waveletapiTest b $algo
   	 	} err ] } {
    		return -code error $err
		}  
	}

	proc waveletapi01c {} {
	
		if	{ [ catch {

			set algo "{
            x = tseries( in, 16384. );
            w = Biorthogonal( 12, 0 );
            y = WaveletForward( x, w );
            z = GetLayer( y, 1, 2 );
            output(z,_,_,z, GetLayer output);
    		}"
        	waveletapiTest c $algo
    	} err ] } {
    		return -code error $err
		}      
	}

	proc waveletapi01d {} {
	
		if	{ [ catch {
			set algo "{
            x = getSequence( in );
            b = Biorthogonal( 12, 0 );
            y = WaveletForward( x, b, 2 );
            output(y,_,_,y,wavelet forward output);
    		} "
			waveletapiTest d $algo 
    	} err ] } {
    		return -code error $err
		}    
	}

	proc waveletapi01e {} {
	
		if	{ [ catch {
			set algo  "{
            x = tseries( in, 16384. );
            b = Biorthogonal( 12, 0 );
            y = WaveletForward( x, 10, 2 );
            output(y,_,_,y,wavelet forward output);
    		} "
			waveletapiTest e $algo 
    	} err ] } {
    		return -code error $err
		}    			
	}

	proc waveletapi01f {} {
	
		if	{ [ catch {
			set algo "{
            x = tseries( in, 16384. );
            b = Biorthogonal( 12, 0 );
            y = WaveletForward( x, b, b );
            output(y,_,_,y,wavelet forward output);
    		}"
        	waveletapiTest f $algo 
    	} err ] } {
    		return -code error $err
		}    		
	}

	proc waveletapi01g {} {
	
		if	{ [ catch {
			set algo "{
            x = tseries( in, 16384. );
            y = WaveletInverse( x );
            output(y,_,_,y,wavelet inverse output);
    		} "
			waveletapiTest g $algo 
    	} err ] } {
    		return -code error $err
		}    	 
	}

	proc waveletapi01h {} {

		if	{ [ catch {
			set algo "{
            x = tseries( in, 16384. );
            y = GetLayer( x, 0 );
            output(y,_,_,y,GetLayer output);

    		} "
			waveletapiTest h $algo 
    	} err ] } {
    		return -code error $err
		}    	 		
	}

	proc waveletapi01i {} {

		if	{ [ catch {
			set algo "{
            x = tseries( in, 16384. );
            b = Biorthogonal( 12, 1 );
            y = WaveletForward( x, b );
            z = GetLayer( y, 0. );
            output(z,_,_,z,GetLayer output);
    		}"
        	waveletapiTest i $algo 
    	} err ] } {
    		return -code error $err
		}    	 		
	}

	proc waveletapi01k {} {
    	
        if	{ [ catch {
        	set algo "{
            x = tseries( in, 16384. );
            b = Biorthogonal( 12, 0 );
            y = WaveletForward( x, b );
            z = GetLayer( y, 0 );
            output(z,_,_,z,GetLayer output);
            z = GetLayer( y, 1 );
            output(z,_,_,z,GetLayer output);
            z = GetLayer( y, 2 );
            output(z,_,_,z,GetLayer output);
            z = GetLayer( y, 4);
            output(z,_,_,z,GetLayer output);
        	}"
            waveletapiTest k $algo 
    	} err ] } {
    		return -code error $err
		} 
   	}  
    
    proc waveletapi02a {} {
    	
        if	{ [ catch {
        	set wavelets { Biorthogonal Daubechies Symlet Haar }
			set procname [ info level 0 ]
        	set name [ namespace current ]
            namespace import [ namespace parent $name ]::*
            set ifilepre 	[ namespace eval $name set ifilepre ]
            set dtypes 	[ namespace eval $name set postfixes ]
            
			foreach dtype $dtypes {

   				set test_all "ts = tseries( idata, 2048. );
                 rms_in = rms(ts); "

   				foreach wtype $wavelets {
     				append test_all " 
                	w = ${wtype}();
                	u = WaveletForward( ts, w, -1);
                	l1 = GetLayer(u,1);
                	ts2 = WaveletInverse(u);
                	tsdiff = sub(ts, ts2);
                	absdiff = abs(tsdiff);
                	maxd = max(absdiff);
                	maxd = div(maxd,rms_in);
                	output(maxd,_,_,maxd, ${wtype} ${dtype}:
                  	maximum relative difference \(original-reconstructed\)/rms_in);               "

				;## more output:
				#append test_all "
#                output(u,_,${wtype}_forw.ilwd,u, WaveletForward output );
#                output(l1,_,${wtype}_l1.ilwd,l1, layer1 of wavelet );
#                output(ts2,_,${wtype}_inv.ilwd,ts2, WaveletInverse output);
#                output(tsdiff,_,${wtype}_diff.ilwd,tsdiff, difference);
#               "
   				}

    			set ifile /ldas_outgoing/jobs/mdc/input/N200$dtype.sim
				set subject datacondMDC:conditionData:$procname:$dtype
                
    			set cmd "conditionData
            -subject $subject
        	-inputprotocol file:/$ifile
        	-outputformat { ilwd ascii }
        	-aliases { idata = mdc_input_data:chan_01:data }
        	-algorithms { $test_all }"

				runTest $subject $cmd
       		}
    	} err ] } {
    		return -code error $err
		}
    } 
}

namespace import waveletapi::*
