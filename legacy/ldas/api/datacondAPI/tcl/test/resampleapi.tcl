#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
namespace eval resampleapi {

	set postfixes { "R4" "R8" "C8" "C16" }
    
    set aliases_resampleapi01a { xlong = mdc_input_data:chan_01:data }
    set aliases_resampleapi01b $aliases_resampleapi01a
    set aliases_resampleapi01c $aliases_resampleapi01a
    set aliases_resampleapi01d $aliases_resampleapi01a
    
    set aliases_resampleapi02a { raw = mdc_input_data:chan_01:data; exp = $prefix:upsampleout:data }
    set aliases_resampleapi02b { raw = mdc_input_data:chan_01:data; exp = $prefix:downsampleout:data }
    set aliases_resampleapi02c { raw = mdc_input_data:chan_01:data }
    set aliases_resampleapi02d $aliases_resampleapi02c
    set aliases_resampleapi02e $aliases_resampleapi02c
    
    set algo_resampleapi01a {
    	x = slice(xlong,0,100,1);
        y = resample(x,0,1);
    	output(y,_,_,y,resample output);   	
    }
    
    set algo_resampleapi01b {
    	x = slice(xlong,0,100,1);
        y = resample(x,1,0);
    	output(y,_,_,y,resample output);   	
    }
    
    set algo_resampleapi01c {
        x = slice(xlong,0,100,1);
        y = resample(x,-2,3);
        output(y,_,_,y,resample output);
    }
    
    set algo_resampleapi01d {
         x = slice(xlong,0,13,1);
         y = resample(x,1,5);
         output(y,_,_,y,resample output);    
    }
    
    set algo_resampleapi02a {
            
            x = slice(raw,0,100,1);

            rx = real(x);
            rvarX = variance(rx);
            output(x,_,_,rvarX,Variance of real input data);
            ix = imag(x);
            ivarX = variance(ix);
            output(ivarX,_,_,ivarX,Variance of imag input data);

            y = resample(x,5,1);

            ry = real(y);
            rexp = real(exp);
            rdif = sub(ry,rexp);
            rminD = min(rdif);
            output(rminD,_,_,rminD,Minimum real deviation);
            rmaxD = max(rdif);
            output(rmaxD,_,_,rmaxD,Maximum real deviation);
            rvarD = variance(rdif);
            output(rvarD,_,_,rvarD,Variance of real deviation);
            rrmsE = rms(rexp);
            output(rrmsE,_,_,rrmsE,RootMeanSquare of real expected result);

            iy = imag(y);
            iexp = imag(exp);
            idif = sub(iy,iexp);
            iminD = min(idif);
            output(iminD,_,_,iminD,Minimum imag deviation);
            imaxD = max(idif);
            output(imaxD,_,_,imaxD,Maximum imag deviation);
            ivarD = variance(idif);
            output(ivarD,_,_,ivarD,Variance of imag deviation);
            irmsE = rms(iexp);
            output(irmsE,_,_,irmsE,RootMeanSquare of imag expected result);

            output(y,_,_,y,upsample,Upsample result);
   
    }
    
    set algo_resampleapi02b {
    	       x = slice(raw,0,100,1);

            rx = real(x);
            rvarX = variance(rx);
            output(rvarX,_,_,rvarX,Variance of real input data);
            ix = imag(x);
            ivarX = variance(ix);
            output(ivarX,_,_,ivarX,Variance of imag input data);

            y = resample(x,1,5);

            ry = real(y);
            rexp = real(exp);
            rdif = sub(ry,rexp);
            rminD = min(rdif);
            output(rminD,_,_,rminD,Minimum real deviation);
            rmaxD = max(rdif);
            output(rmaxD,_,_,rmaxD,Maximum real deviation);
            rvarD = variance(rdif);
            output(rvarD,_,_,rvarD,Variance of real deviation);
            rrmsE = rms(rexp);
            output(rrmsE,_,_,rrmsE,RootMeanSquare of real expected result);

            iy = imag(y);
            iexp = imag(exp);
            idif = sub(iy,iexp);
            iminD = min(idif);
            output(iminD,_,_,iminD,Minimum imag deviation);
            imaxD = max(idif);
            output(imaxD,_,_,imaxD,Maximum imag deviation);
            ivarD = variance(idif);
            output(ivarD,_,_,ivarD,Variance of imag deviation);
            irmsE = rms(iexp);
            output(irmsE,_,_,irmsE,RootMeanSquare of imag expected result);

            output(y,_,_,y,downsample,Downsample result);
    	}	
    
    	set algo_resampleapi02c {
            
            x0 = slice(raw,0,100,1);
            y0 = resample(x0,1,5);

            x1 = slice(x0,0,50,1);
            x2 = slice(x0,50,50,1);

            y1 = resample(x1,1,5,z);
            output(y1,_,_,y1,First half downsample x 5);
            y2 = resample(x2,z);
            output(y2,_,_,y2,Second half downsample x 5);

            yA = slice(y0,0,10,1);
            yB = slice(y0,10,10,1);

            ryA = real(yA);
            ryB = real(yB);
            ry1 = real(y1);
            ry2 = real(y2);
            iyA = imag(yA);
            iyB = imag(yB);
            iy1 = imag(y1);
            iy2 = imag(y2);

            rdifA = sub(ryA,ry1);
            output(rdifA,_,_,rdifA,First half real differences);
            rminA = min(rdifA);
            output(rminA,_,_,rminA,Minimum first half real deviation);
            rmaxA = max(rdifA);
            output(rmaxA,_,_,rmaxA,Maximum first half real deviation);

            rdifB = sub(ryB,ry2);
            output(rdifB,_,_,rdifB,Second half real differences);
            rminB = min(rdifB);
            output(rminB,_,_,rminB,Minimum second half real deviation);
            rmaxB = max(rdifB);
            output(rmaxB,_,_,rmaxB,Maximum second half real deviation);

            idifA = sub(iyA,iy1);
            output(idifA,_,_,idifA,First half imag differences);
            iminA = min(idifA);
            output(iminA,_,_,iminA,Minimum first half imag deviation);
            imaxA = max(idifA);
            output(imaxA,_,_,imaxA,Maximum first half imag deviation);

            idifB = sub(iyB,iy2);
            output(idifB,_,_,idifB,Second half imag differences);
            iminB = min(idifB);
            output(iminB,_,_,iminB,Minimum second half imag deviation);
            imaxB = max(idifB);
            output(imaxB,_,_,imaxB,Maximum second half imag deviation);

            output(y0,_,_,downbroken,Broken downsalmple result);
        }
        
        set algo_resampleapi02d {
        	  x0 = slice(raw,0,20,1);
            y0 = resample(x0,2,1);

            x1 = slice(x0,0,10,1);
            x2 = slice(x0,10,10,1);

            y1 = resample(x1,2,1,z);
            output(y1,_,_,y1,First half upsample x 2);
            y2 = resample(x2,z);
            output(y2,_,_,y2,Second half upsample x 2);

            yA = slice(y0,0,20,1);
            yB = slice(y0,20,20,1);

            ryA = real(yA);
            ryB = real(yB);
            ry1 = real(y1);
            ry2 = real(y2);
            iyA = imag(yA);
            iyB = imag(yB);
            iy1 = imag(y1);
            iy2 = imag(y2);

            rdifA = sub(ryA,ry1);
            output(rdifA,_,_,rdifA,First half real differences);
            rminA = min(rdifA);
            output(rminA,_,_,rminA,Minimum first half real deviation);
            rmaxA = max(rdifA);
            output(rmaxA,_,_,rmaxA,Maximum first half real deviation);

            rdifB = sub(ryB,ry2);
            output(rdifB,_,_,rdifB,Second half real differences);
            rminB = min(rdifB);
            output(rminB,_,_,rminB,Minimum second half real deviation);
            rmaxB = max(rdifB);
            output(rmaxB,_,_,rmaxB,Maximum second half real deviation);

            idifA = sub(iyA,iy1);
            output(idifA,_,_,idifA,First half imag differences);
            iminA = min(idifA);
            output(iminA,_,_,iminA,Minimum first half imag deviation);
            imaxA = max(idifA);
            output(imaxA,_,_,imaxA,Maximum first half imag deviation);

            idifB = sub(iyB,iy2);
            output(idifB,_,_,idifB,Second half imag differences);
            iminB = min(idifB);
            output(iminB,_,_,iminB,Minimum second half imag deviation);
            imaxB = max(idifB);
            output(imaxB,_,_,imaxB,Maximum second half imag deviation);

            output(y0,_,_,upborken,Broken upsample result);
     }
     
     set algo_resampleapi02e {
            xraw = sine($n, $f);
            x = tseries(xraw, $srate, 10000);

            x1 = slice(x, 0, $m, 1);
            x2 = slice(x, $m, $m, 1);

            x1 = resample(x1, $p, $q, z);
            x2 = resample(x2, z);

            x = concat(x1, x2);

            start = sub($n2, 100);
            x = slice(x, start, 100, 1);

            d = getResampleDelay(z);
            d = integer(d);

            y = sine($n2, $f2);
            start = sub($n2, d);
            start = sub(start, 100);
            y = slice(y, start, 100, 1);

            diff = sub(x, y);
            diff = abs(diff);
            diff = max(diff);

            output(diff, _, _, diff, Max diff between resampled and actual);
     }
     
     set cmdstem { conditionData
        -subject $subject 
        -inputprotocol { file:/${ifile} $efile }
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases { $aliases }
        -algorithms { $algo }
     }
     
     proc setvalues_resampleapi02e {} {
     
       uplevel {
     	;## Input params
		set p 1
		set q 2
		set n 16384
		set m [ expr $n/2 ]
		set srate 16384.0
		set dt [ expr 1.0/$srate ]
		set Nyq [ expr $srate/2.0 ]
		set freq [ expr 0.125*$Nyq ]
		set f [ expr $freq*$dt ]

		;## Output values
		set n2 [ expr $n*$p/$q ]
		set srate2 [ expr $srate*$p/$q ]
		set dt2 [ expr 1.0/$srate2 ]
		set Nyq2 [ expr $srate2/2.0 ]
		set f2 [ expr $freq*$dt2 ]
          
       }
     }
     
	namespace export resampleapi*
    
    	proc resampleapiTest { ifilepre { needefile 0 } } {
    
    	if	{ [ catch {
        	set name [ namespace current ]
		set procname [ info level -1 ]
          namespace import [ namespace parent $name ]::*  
                               
          set cmdstem [ namespace eval $name set cmdstem ]
          set postfixes [ namespace eval $name set postfixes ]
          set algo [ namespace eval $name set algo_$procname ]
          set rc [ namespace eval $name info exist ifile_$procname ]
          set ofilepre [ string toupper $procname ]
          
          if	{ [ string match "*02e*" $procname ] } {
          	set postfixes [ lrange $postfixes 0 1 ]
          } 
          if	{ [ string length [ info proc setvalues_$procname ] ] } {
          	setvalues_$procname
          }
          
		foreach post $postfixes {
          	set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim  
               set aliases [ namespace eval $name set aliases_$procname ]
			if	{ $needefile } {
            		set prefix ${ofilepre}$post
    				set efile file:/ldas_outgoing/mdc/input/$prefix.exp
                	set ofile ldas_outgoing/mdc/output/$prefix.ilwd
            	} else {
            		set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd    
                    set efile ""		
            	}
               set subject datacondMDC:conditionData:$procname:$post
               set aliases [ subst $aliases ]
               set algo [ subst $algo ]
               set cmd [ subst $cmdstem ]
               runTest $subject $cmd 
               after 1000           
    		}
    	} err ] } {
    		return -code error $err
	}  
     
     }
           
	proc resampleapi01a {} {

		if	{ [ catch {
     		set ifilepre "N200" 
			resampleapiTest $ifilepre 
   		} err ] } {
    			return -code error $err
		}
	}

	proc resampleapi01b {} {

		if	{ [ catch {
			set ifilepre "N200" 
			resampleapiTest $ifilepre 
    		} err ] } {
    			return -code error $err
		}
	}

	proc resampleapi01c {} {

		if	{ [ catch {
          	set ifilepre N200
			resampleapiTest $ifilepre 
    		} err ] } {
    			return -code error $err
		}
	}
		
	proc resampleapi01d {} {

		if	{ [ catch {       
			set ifilepre N200 
			resampleapiTest $ifilepre  
    		} err ] } {
    			return -code error $err
		}
	}

	proc resampleapi02a {} {

		if	{ [ catch {
			set ifilepre N8388608 
               set efile 1
			resampleapiTest $ifilepre $efile
    		} err ] } {
    			return -code error $err
		}
	}

	proc resampleapi02b {} {

		if	{ [ catch {
			set ifilepre N8388608 
            set efile 1
			resampleapiTest $ifilepre $efile
    		} err ] } {
    			return -code error $err
		}       
	}
		
	proc resampleapi02c {} {

		if	{ [ catch {
			set ifilepre N8388608 
			resampleapiTest $ifilepre
    		} err ] } {
    			return -code error $err
		}       
	}

	proc resampleapi02d {} {

		if	{ [ catch {
			set ifilepre N8388608
          	resampleapiTest $ifilepre
    	} err ] } {
    		return -code error $err
		}  
	}

	proc resampleapi02e {} {

		if	{ [ catch {
			set ifilepre N200
          	resampleapiTest $ifilepre
    	} err ] } {
    		return -code error $err
		}  
	}		
}
namespace import resampleapi::*
