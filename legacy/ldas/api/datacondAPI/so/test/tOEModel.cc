#include "datacondAPI/config.h"

#include <complex>

#include "general/unittest.h"

#include "OEModel.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

#include "random.hh"

using namespace datacondAPI;

General::UnitTest Test;

void test_specific()
{
    // OEModel()
    {
	OEModel oe;
	Test.Check(oe.getOrderB() == 0, "OEModel oe; oe.getOrderB() == 0");
	Test.Check(oe.getOrderF() == 0, "OEModel oe; oe.getOrderF() == 0");
	try
	{
	    udt* p = 0;
	    oe.getTheta(p);
	    Test.Check(false, "OEModel oe; oe.getTheta(p); did not throw");
	}
	catch (const std::invalid_argument& x)
	{
	    Test.Check(true, "OEModel oe; oe.getTheta(p); threw");
	}
    }
    // explicit OEModel(const int&)
    {
	OEModel oe(7);
	try
	{
	    udt* p = 0;
	    oe.getTheta(p);
	    Test.Check(false, "OEModel oe(7); oe.getTheta(p); did not throw");
	}
	catch (const std::invalid_argument& x)
	{
	    Test.Check(true, "OEModel oe(7); oe.getTheta(p); threw");
	}
	// int getOrder()
	Test.Check(oe.getOrderB() == 7, "OEModel oe(7); oe.getOrderB() == 7");
	Test.Check(oe.getOrderF() == 7, "OEModel oe(7); oe.getOrderF() == 7");
	// void getOrderB(udt*&)
	{
	    // null pointer
	    {
		udt* p = 0;
		oe.getOrderB(p);
		Test.Check(udt::Cast<Scalar<int> >(*p).GetValue() == 7, "OEModel oe(7); udt* p = 0; oe.getOrderB(p); udt::Cast<Scalar<int> >(*p).GetValue() == 7");
	    }
	    // valid type
	    {
		Scalar<int> n(0);
	        udt* p = &n;
		oe.getOrderB(p);
		Test.Check(n.GetValue() == 7, "OEModel oe(7); Scalar<int> n(0); udt* p = &n; oe.getOrderB(p); n.GetValue() == 7");
	    }
	    // invalid type
	    try
	    {
		Scalar<float> n(0);
		udt* p = &n;
		oe.getOrderB(p);
		Test.Check(false, "OEModel oe(7); Scalar<float> n(0); udt* p = &n; oe.getOrderB(p) (no throw)");
	    }
	    catch (const std::invalid_argument& x)
	    {
		Test.Check(true, "OEModel oe(7); Scalar<float> n(0); udt* p = &n; oe.getOrder(p) (threw)");
	    }
	}
    }
    // explicit OEModel(const udt&)
    {
	// valid type
	{
	    Scalar<int> n(7);
	    OEModel oe(n);
	    Test.Check(oe.getOrderB() == 7,
		       "Scalar<int> n(7); OEModel oe(n); oe.getOrderB() == 7");
	    Test.Check(oe.getOrderF() == 7,
		       "Scalar<int> n(7); OEModel oe(n); oe.getOrderF() == 7");
	    try
	    {
		udt* p = 0;
		oe.getTheta(p);
		Test.Check(false,
			   "Scalar<int> n(7); OEModel oe(n); oe.getTheta(p); did not throw");
	    }
	    catch (const std::invalid_argument& x)
	    {
		Test.Check(true, "Scalar<int> n(7); OEModel oe(n); oe.getTheta(p); threw");
	    }
	}
	// invalid type
	try
        {
	    Scalar<float> n(7.0);
	    OEModel oe(n);
	    Test.Check(false, "Scalar<float> n(7.0); OEModel oe(n); did not throw");
	}
	catch (const std::invalid_argument& x)
        {
	    Test.Check(true, "Scalar<float> n(7.0); OEModel oe(n); threw");
	}
    }
    // OEModel(const udt& x, const udt& n)
    {
	Scalar<float> x(1.0);
	// bad n and x
	try
	{
	    OEModel oe(x, x);
	    Test.Check(false, "Scalar<float> x(1.0); OEModel oe(x, x); did not throw");
	}
	catch (const std::invalid_argument& e)
        {
	    Test.Check(true, "Scalar<float> x(1.0); OEModel oe(x, x); threw");
	}
	// bad x
	try
	{
	    Scalar<int> n(1);
	    OEModel oe(x, n);
	    Test.Check(false, "Scalar<float> x(1.0); Scalar<int> n(1); OEModel oe(x, n); did not throw");
	}
	catch (const std::invalid_argument& e)
        {
	    Test.Check(true, "Scalar<float> x(1.0); Scalar<int> n(1); OEModel oe(x, n); threw");
	}
    }
    // int getOrder() implicit above
    // void getOrder(udt*&) implicit above
    // void getTheta(udt*&) (failure: no state) implicit above
    // void getFilter(udt*&)
    try
    {
	OEModel oe;
	udt* b = 0;
	oe.getFilterB(b);
	Test.Check(false, "OEModel oe; udt* b = 0; oe.getFilter(b); (no throw)");
    }
    catch (const std::invalid_argument& e)
    {
	Test.Check(true, "OEModel oe; udt* b = 0; oe.getFilter(b); (threw)");
    }
    // void setOrderB(const int&)
    {
	OEModel oe;
	oe.setOrderB(7);
	Test.Check(oe.getOrderB() == 7, "OEModel oe; oe.setOrderB(7); oe.getOrderB() == 7");
    }
    // void setOrderB(const udt&)
    {
	// valid type
	{
	    OEModel oe;
	    Scalar<int> n(7);
	    oe.setOrderB(n);
	    Test.Check(oe.getOrderB() == 7, "OEModel oe; Scalar<int> n(1); oe.setOrderB(n); oe.getOrderB() == 7");
	}
	// invalid type
	try
	{
	    OEModel oe;
	    Scalar<float> n(7.0);
	    oe.setOrderB(n);
	    Test.Check(false, "OEModel oe; Scalar<float> n(7.0); oe.setOrderB(n) (no throw)");
	}
	catch (const std::invalid_argument& x)
        {
	    Test.Check(true, "OEModel oe; Scalar<float> n(7.0); oe.setOrderB(n) (threw)");
	}
    }
    // void setTheta(const udt&)
    try
    {
	OEModel oe;
	Scalar<int> theta(0);
	oe.setTheta(theta, theta);
	Test.Check(false, "OEModel oe; Scalar<int> theta; oe.setTheta(theta, theta); (no throw)");
    }
    catch (const std::invalid_argument& x)
    {
	Test.Check(true, "OEModel oe; Scalar<int> theta; oe.setTheta(theta, theta); (no threw)");
    }
    // void apply(udt*& w, const udt& u)
    try
    {
	OEModel oe;
	udt* w = 0;
	Scalar<int> u(0);
	oe.apply(w, u);
	Test.Check(false, "OEModel oe; udt* w = 0; Scalar<int> u; oe.apply(w, u); (no throw)");
    }
    catch (const std::invalid_argument& x)
    {
	Test.Check(true, "OEModel oe; udt* w = 0; Scalar<int> u; oe.apply(w, u); (threw)");
    }
    // void refine(const udt& y, const udt& u)
    try
    {
	OEModel oe;
	Scalar<int> y(0);
	Scalar<int> u(0);
	oe.refine(y,u);
	Test.Check(false, "OEModel oe; Scalar<int> y; Scalar<int> u; oe.refine(y, u); (no throw)");
    }
    catch (const std::invalid_argument& e)
    {
	Test.Check(true, "OEModel oe; Scalar<int> y; Scalar<int> u; oe.refine(y, u); (threw)");
    }
    // merit(const udt& m, const udt& x)
    try
    {
	OEModel oe;
	udt* p = 0;
	Scalar<int> y(0);
	oe.merit(p, y, y);
	Test.Check(false, "OEModel oe; udt* p = 0; Scalar<int> y; oe.merit(p, y, y); (no throw)");
    }
    catch (const std::invalid_argument& e)
    {
	Test.Check(true, "OEModel oe; udt* p = 0; Scalar<int> y; oe.merit(p, y, y); (threw)");
    }
}

template<typename type>
bool equality(const std::valarray<type> a, const std::valarray<type> b)
{
    if (a.size() != b.size())
    {
	return false;
    }
    for (unsigned int i = 0; i < a.size(); ++i)
    {
	if (a[i] != b[i])
        {
	    return false;
	}
    }
    return true;
}

template<typename type>
static std::ostream& operator<<(std::ostream& a, const std::valarray<type>& b)
{
    a << "{ ";
    if (b.size() <= 10)
    {
	for (unsigned int i = 0; i < b.size(); ++i)
	{
	    a << b[i] << ' ';
	}
    }
    else
    {
	for (unsigned int i = 0; i < 9; ++i)
	{
	    a << b[i] << ' ';
	}
	a << "... " << b[b.size() - 1] << ' ';
    }
    a << "}";
    return a;
}

static gasdevstate seed;

void gasdev(std::valarray<float>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<double>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<std::complex<float> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<float>(gasdev(seed), gasdev(seed));
    }
}

void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}

template<typename type>
void test_generic(const std::string& name)
{
    Test.Message() << name << '\n';

    std::valarray<type> theta(4);
    {
	theta[0] =  1.00; // b_1
	theta[1] = -0.50; // b_{n_b}
	theta[2] =  0.50; // f_1
	theta[3] =  0.25; // f_{n_f}
    }
    // explict OEModel(const std::valarray<type>& theta)
    {	
	OEModel oe(theta, 2);
	Test.Check(oe.getOrderB() + oe.getOrderF() == (int) theta.size(),
		   "OEModel oe(theta); oe.getOrderB() + oe.getOrderF() == (int) theta.size()");
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "OEModel oe(theta); oe.getTheta(theta_out); equality(theta, theta_out)");

	// OEModel(const OEModel&)
	OEModel br(oe);
	Test.Check(br.getOrderB() + br.getOrderF() == (int) theta.size(),
		   "OEModel oe(theta); OEModel br(oe); br.getOrderB() + br.getOrderF() == (int) theta.size()");
	br.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "OEModel br(oe); br.getTheta(theta_out); equality(theta, theta_out)");

	// OEModel& operator=(const OEModel&)
	OEModel cr;
	cr = oe;
	Test.Check(cr.getOrderB() + cr.getOrderF() == (int) theta.size(),
		   "OEModel oe(theta); OEModel cr; cr = oe; cr.getOrderB() + cr.getOrderF() == (int) theta.size()");
	cr.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "OEModel cr; cr = oe; cr.getTheta(theta_out); equality(theta, theta_out)");

	// OEModel* Clone() const
	OEModel *p = oe.Clone();
	Test.Check(p != 0, "OEModel *p = oe.Clone(); p != 0");
	Test.Check(p->getOrderB() + p->getOrderF() == (int) theta.size(),
		   "OEModel *p = oe.Clone(); p->getOrderB() + p->getOrderF() == (int) theta.size()");
	p->getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "OEModel* p = oe.Clone(); p->getTheta(theta_out); equality(theta, theta_out)");	
	delete p;
    }

    // explicit OEModel(const udt&)
    {
	Sequence<type> theta_udt(theta);
	Scalar<int> n(2);
	OEModel oe(theta_udt, n);

	Test.Check(oe.getOrderB() + oe.getOrderF() == (int) theta.size(),
		   "OEModel oe(theta_udt); oe.getOrderB() + oe.getOrderF() == (int) theta.size()");
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "OEModel oe(theta_udt); oe.getTheta(theta_out); equality(theta, theta_out)");
    }

    std::valarray<type> u(7);
    const std::valarray<type>&
      const_u( static_cast< const std::valarray<type>& >( u ) );
    gasdev(u);

    std::valarray<type> y(7);
    gasdev(y);

    y[std::slice(1, 6, 1)] += const_u[std::slice(0, 6, 1)]; // elementary transfer function


    // OEModel(const std::valarray<type>& y, const std::valarray<type>& u, const int& order_b, const int& order_f)
    {
	OEModel oe(y, u, 2, 2);
	Test.Check(oe.getOrderB() == 2, "OEModel(y, u, 2, 2); oe.getOrderB() == 2");
	Test.Check(oe.getOrderF() == 2, "OEModel(y, u, 2, 2); oe.getOrderF() == 2");
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Message() << theta_out << '\n';
    }
    
    // OEModel(const udt& y, const udt& u, const udt& order_b, const udt& order_f)
    {
	Sequence<type> y_udt(y);
	Sequence<type> u_udt(u);
	Scalar<int> n_udt(2);
	OEModel oe(y_udt, u_udt, n_udt, n_udt);
	Test.Check(oe.getOrderB() == 2, "OEModel(y_udt, u_udt, n_udt, n_udt); oe.getOrderB() == 2");
	Test.Check(oe.getOrderF() == 2, "OEModel(y_udt, u_udt, n_udt, n_udt); oe.getOrderF() == 2");
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Message() << theta_out << '\n';

	// invalid x type
	try
	{
	    OEModel br(n_udt, n_udt, n_udt, n_udt);
	    Test.Check(false, "OEModel br(n_udt, n_udt, n_udt, n_udt); (no throw)");
	}
	catch (const std::invalid_argument& e)
	{
	    Test.Check(true, "OEModel br(n_udt, n_udt, n_udt, n_udt); (threw)");
	}
    }

    // void getTheta(const std::valarray<type>&) const implicit above

    // void getTheta(udt*&) const
    {
	OEModel oe(theta, 2);
	
	// null input
	{
	    udt* p = 0;
	    oe.getTheta(p);
	    Test.Check(p != 0, "OEModel oe(theta, 2); udt* p = 0; oe.getTheta(p); p != 0");
	    Test.Check(equality(udt::Cast<Sequence<type> >(*p), theta), "OEModel oe(theta); udt* p = 0; oe.getTheta(p); equality(udt::Cast<Sequence<type> >(*p), theta)");
	    delete p;
	}
	// valid input
	{
	    Sequence<type> theta_out;
	    udt* p = &theta_out;
	    oe.getTheta(p);
	    Test.Check(equality(theta_out, theta), "OEModel oe(theta, 2); udt* p = &theta_out; oe.getTheta(p); equality(udt::Cast<Sequence<type> >(*p), theta)");	    
	}
	// invalid input
	try
	{
	    Scalar<int> theta_not(0);
	    udt* p = &theta_not;
	    oe.getTheta(p);
	    Test.Check(false, "OEModel oe(theta, 2); Scalar<int> theta_not; udt* p = &theta_not; oe.getTheta(p); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "OEModel oe(theta); Scalar<int> theta_not; udt* p = &theta_not; oe.getTheta(p); (threw)");
	}
    }

    // void getFilterB(udt*&) const
    {
	OEModel oe(theta, 2);
	std::valarray<type> filter(3);
	filter[0] = type();
	filter[std::slice(1, 2, 1)] =
	  static_cast< const std::valarray<type>& >
	  (theta)[std::slice(0, 2, 1)];
	Test.Message() << "filter " << filter << '\n';
	
	// null input
	{
	    udt* p = 0;
	    oe.getFilterB(p);
	    Test.Check(p != 0, "OEModel oe(theta, 2); udt* p = 0; oe.getFilter(p); p != 0");
	    Test.Check(equality(udt::Cast<Sequence<type> >(*p), filter), "OEModel oe(theta, 2); udt* p = 0; oe.getFilterB(p); equality(udt::Cast<Sequence<type> >(*p), filter)");
	    delete p;
	}
	// valid input
	{
	    Sequence<type> filter_out;
	    udt* p = &filter_out;
	    oe.getFilterB(p);
	    Test.Message() << filter_out << " ~ " << filter << '\n';
	    Test.Check(equality(filter_out, filter), "OEModel oe(theta, 2); udt* p = &filter_out; oe.getFilterB(p); equality(udt::Cast<Sequence<type> >(*p), filter)");	    
	}
	// invalid input
	try
	{
	    Scalar<int> filter_not(0);
	    udt* p = &filter_not;
	    oe.getFilterB(p);
	    Test.Check(false, "OEModel oe(theta); Scalar<int> filter_not; udt* p = &filter_not; oe.getFilterB(p); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "OEModel oe(theta); Scalar<int> filter_not; udt* p = &filter_not; oe.getFilterB(p); (threw)");
	}
    }

    // void setOrderB(const int&)
    {
	OEModel oe(theta, 2);
	oe.setOrderB(3);
	Test.Check(oe.getOrderB() == 3, "oe.setOrderB(3); oe.getOrderB() == 3");
	try
	{
	    std::valarray<type> theta_out;
	    oe.getTheta(theta_out);
	    Test.Check(false, "oe.setOrderB(3); oe.getTheta(theta_out); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "oe.setOrderB(3); oe.getTheta(theta_out); (threw)");
	}
    }

    // void setOrderB(const udt&)
    {
	OEModel oe(theta, 2);
	Scalar<int> n(3);
	oe.setOrderB(n);
	Test.Check(oe.getOrderB() == 3, "Scalar<int> n(3); oe.setOrderB(n); oe.getOrdeBr() == 3");
	try
	{
	    std::valarray<type> theta_out;
	    oe.getTheta(theta_out);
	    Test.Check(false, "Scalar<int> n(3); oe.setOrderB(n); oe.getTheta(theta_out); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "Scalar<int> n(3); oe.setOrderB(n); oe.getTheta(theta_out); (threw)");
	}
	try
        {
	    Scalar<float> m(4.0);
	    oe.setOrderB(m);
	    Test.Check(false, "Scalar<float> m(4.0); oe.setOrderB(m); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "Scalar<float> m(4.0); oe.setOrderB(m); (threw)");
	}
    }

    // void setTheta(const std::valarray<type>&)
    {
	OEModel oe;
	oe.setTheta(theta, 2);
	Test.Check(oe.getOrderB() + oe.getOrderF() == (int) theta.size(), "oe.setTheta(theta, 2); oe.getOrderB() + oe.getOrderF() == (int) theta.size()");
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "oe.setTheta(theta, 2); oe.getTheta(theta_out); equality(theta, theta_out)");
    }

    // void setTheta(const udt&)
    {
	OEModel oe;
	Sequence<type> theta_udt(theta);
	oe.setTheta(theta_udt, 2);
	Test.Check(oe.getOrderB() + oe.getOrderF() == (int) theta.size(), "oe.setTheta(theta_udt); oe.getOrderB() + oe.getOrderF()== (int) theta.size()");
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "oe.setTheta(theta_udt); oe.getTheta(theta_out); equality(theta, theta_out)");
	try
	{
	    Scalar<int> n(0);
	    oe.setTheta(n, n);
	    Test.Check(false, "Scalar<int> n(0); oe.setTheta(n, n); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "Scalar<int> n(0); oe.setTheta(n, n); (threw)");
	}
    }

    // void apply(std::valarray<type>& y, const std::valarray<type>& x) const
    {
	OEModel oe(theta, 2);
	std::valarray<type> w;
	oe.apply(w, u);
	Test.Message() << w << " ~ " << u << '\n';
    }

    // void apply(udt*& y, const udt& x) const
    {
	// null pointer
	OEModel oe(theta, 2);
	Sequence<type> u_udt(u);
	udt* w = 0;
	oe.apply(w, u_udt);
	Test.Message() << dynamic_cast<std::valarray<type>&>(*w) << " ~ " << u << '\n';
	delete w;
    }
    {
	// valid pointer
	OEModel oe(theta, 2);
	Sequence<type> x_udt(u);
	Sequence<type> y_udt;
	udt* y = &y_udt;
	oe.apply(y, x_udt);
	Test.Message() << y_udt << " ~ " << u << '\n';
    }
    try
    {
	// invalid type (x)
	OEModel oe(theta, 2);
	Scalar<type> x_udt(0);
	Sequence<type> y_udt;
	udt* y = &y_udt;
	oe.apply(y, x_udt);
	Test.Check(false, "OEModel oe(theta, 2); Scalar<type> x_udt(0); Sequence<type> y_udt; udt* y = &y_udt; oe.apply(y, x_udt); (no throw)");

    }
    catch (std::exception& e)
    {
	Test.Check(true, "OEModel oe(theta, 2); Scalar<type> x_udt(0); Sequence<type> y_udt; udt* y = &y_udt; oe.apply(y, x_udt); (threw)");
    }
    try
    {
	// invalid type (y)
	OEModel oe(theta, 2);
	Sequence<type> x_udt(u);
	Scalar<int> y_udt(0);
	udt* y = &y_udt;
	oe.apply(y, x_udt);
	Test.Check(false, "OEModel oe(theta, 2); Sequence<type> x_udt(x); Scalar<int> y_udt; udt* y = &y_udt; oe.apply(y, x); (no throw)");
    }
    catch (std::invalid_argument& e)
    {
 	Test.Check(true, "OEModel oe(theta, 2); Sequence<type> x_udt(x); Scalar<int> y_udt; udt* y = &y_udt; oe.apply(y, x); (threw)");
   }

    // void refine(const std::valarray<type>& x)
    {
	OEModel oe(2);
	oe.refine(y, u);
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Message() << theta_out << '\n';

	const std::valarray<type>& y1 = y[std::slice(0, 5, 1)];
	const std::valarray<type>& u1 = u[std::slice(0, 5, 1)];
	const std::valarray<type>& y2 = y[std::slice(5, 2, 1)];
	const std::valarray<type>& u2 = u[std::slice(5, 2, 1)];

	OEModel br(2);
	br.refine(y1, u1);
	br.refine(y2, u2);
	std::valarray<type> theta_split;
	br.getTheta(theta_split);
	Test.Message() << theta_split << '\n';

	Test.Check(equality(theta_out, theta_split), "<split refinement>");	
    }

    // void refine(const udt& x)
    {
	OEModel oe(2);
	Sequence<type> x_udt(u);
	Sequence<type> y_udt(y);
	oe.refine(y_udt, x_udt);
	std::valarray<type> theta_out;
	oe.getTheta(theta_out);
	Test.Message() << theta_out << '\n';
    }
    try
    {
	OEModel oe(2);
	Scalar<int> x_udt(0);
	oe.refine(x_udt, x_udt);
	Test.Check(false, "OEModel oe(2); Scalar<int> x_udt(0); oe.refine(x_udt); (no throw)");
    }
    catch(std::invalid_argument& e)
    {
    	Test.Check(true, "OEModel oe(2); Scalar<int> x_udt(0); oe.refine(x_udt); (threw)");
    }

    // type merit (std::valarray<type>&)
    {
	OEModel oe(theta, 2);
	Test.Message() << "merit " << oe.merit(y, u) << '\n';
    }
    try
    {
	OEModel oe;
	oe.merit(y, u);
	Test.Check(false, "OEModel oe; oe.merit(x); (no throw)");
    }
    catch(std::invalid_argument& e)
    {
	Test.Check(true, "OEModel oe; oe.merit(x); (threw)");
    }

    // void merit(const udt& x)
    {
	OEModel oe(theta, 2);
	Scalar<type> m_udt(0);
	udt* m = &m_udt;
	Sequence<type> x_udt(u);
	Sequence<type> y_udt(y);
	oe.merit(m, y_udt, x_udt);
	Test.Message() << "merit " << m_udt.GetValue() << '\n';
    }

    // correctness

    {

	std::valarray<type> u(1000);
	gasdev(u);
	
	std::valarray<type> theta(4);
	gasdev(theta);
	theta[std::slice(2,2,1)] *= std::valarray<type>(0.1, 2); // keep it stable!

	OEModel filter(theta, 2);
	std::valarray<type> y;
	filter.apply(y, u);
	std::valarray<type> white(y.size());
	gasdev(white);
	y += white;

	Test.Message() << "     theta = " << theta << '\n';
	Test.Message() << "         u = " << u << '\n';
	Test.Message() << "         y = " << y << '\n';

	OEModel estimated(y, u, 2, 2);
	std::valarray<type> theta_out;
	estimated.getTheta(theta_out);
	
	Test.Message() << " theta_out = " << theta_out << '\n';

	Test.Message() << "     error = " << std::valarray<type>(theta_out - theta) << '\n';

    }

}


int main(int argc, char** argv)
try
{
    Test.Init(argc, argv);

    test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("complex<float>");
    test_generic<std::complex<double> >("complex<double>");

    Test.Exit();

}
catch (const std::exception& x)
{
    Test.Check(false, "unexpected std::exception");
    Test.Message() << x.what() << '\n';
    Test.Exit();
}
catch (...)
{
    Test.Check(false, "unexpected exception");
    Test.Exit();
}
