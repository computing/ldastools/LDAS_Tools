#ifndef FFT_CHECKER_HH
#define FFT_CHECKER_HH

#include <unistd.h>
#include <complex>

namespace General
{
  class UnitTest;
}

template<class TDataType_, class FDataType_>
void
TestFFT(const TDataType_* tdata,
	const std::complex<FDataType_>* fdata, const size_t n,
	const FDataType_ TOLERANCE,
	General::UnitTest& Test);

template<class TDataType_, class FDataType_>
void
TestIFFT(const TDataType_* tdata,
	 const std::complex<FDataType_>* fdata, const size_t n,
	 const FDataType_ TOLERANCE,
	 General::UnitTest& Test);

template<class TDataType_, class DataType_>
void
TestFFTandIFFT(const TDataType_* tdata, const size_t n,
	       const DataType_ TOLERANCE,
	       General::UnitTest& Test);

#endif	/* FFT_CHECKER_HH */
