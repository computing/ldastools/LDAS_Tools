#include "datacondAPI/config.h"

#include <valarray>

#include "general/unittest.h"	// Needed for doing "make check"

#include <filters/LDASConstants.hh>	// Constants
#include "token.hh"		// Common header for token testing programs.
// #include "Mixer.hh"		// Needed for computing mix expected results.
#include "ScalarUDT.hh"		// Needed for converting CallChain results.
#include "SequenceUDT.hh"	// Needed for converting CallChain results.
#include "DFTUDT.hh"		// Needed for testing results

General::UnitTest	Test;	// Class supporting testing

int
main(int ArgC, char** ArgV)
{
  using namespace datacondAPI;

  //---------------------------------------------------------------------
  // Initialize UnitTest class
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  Test.Message() << "math functions: start\n";

  try
  {
    //-------------------------------------------------------------------
    // Perform tests
    //-------------------------------------------------------------------

    CallChain cmds;
    Parameters args;

    cmds.AppendCallFunction("integer", args.set(1, "10"), "i");
    cmds.AppendCallFunction("double", args.set(1, "-1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "1", "2"), "cf");
    cmds.AppendCallFunction("dcomplex", args.set(2, "1", "2"), "cd");
    cmds.AppendCallFunction("svalarray", args.set(2, "d", "i"), "vf");
    cmds.AppendCallFunction("dvalarray", args.set(2, "d", "i"), "vd");
    cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "i"), "vcf");
    cmds.AppendCallFunction("zvalarray", args.set(2, "cd", "i"), "vcd");
    cmds.AppendCallFunction("sfvalarray", args.set(2, "d", "i"), "fvf");
    cmds.AppendCallFunction("dfvalarray", args.set(2, "d", "i"), "fvd");

    cmds.AppendCallFunction("double", args.set(1, "2"), "two");
    cmds.AppendCallFunction("svalarray", args.set(2, "two", "i"), "sf");
    cmds.AppendCallFunction("dvalarray", args.set(2, "two", "i"), "sd");

#define EXPAND(NAME)\
    \
    cmds.AppendCallFunction(#NAME, args.set(1, "two"), std::string(#NAME) + "_d");\
    cmds.AppendCallFunction(#NAME, args.set(1, "cf"), std::string(#NAME) + "_cf");\
    cmds.AppendCallFunction(#NAME, args.set(1, "cd"), std::string(#NAME) + "_cd");\
    cmds.AppendCallFunction(#NAME, args.set(1, "sf"), std::string(#NAME) + "_sf");\
    cmds.AppendCallFunction(#NAME, args.set(1, "sd"), std::string(#NAME) + "_sd");\
    cmds.AppendCallFunction(#NAME, args.set(1, "vcf"), std::string(#NAME)+ "_scf");\
    cmds.AppendCallFunction(#NAME, args.set(1, "vcd"), std::string(#NAME) + "_scd");\
    cmds.AppendIntermediateResult(#NAME "_scd", "result", "Final Result", "" );

    //EXPAND(cos)
    EXPAND(cosh)
    //EXPAND(exp)
    //EXPAND(log)
    //EXPAND(sin)
    EXPAND(sinh)
    //EXPAND(sqrt)

#undef EXPAND

    cmds.Execute();

#define EXPAND(NAME)\
    \
    check_value_variance(NAME(2.0), *cmds.GetSymbol(std::string(#NAME) + "_d"), std::string(#NAME) + "(double)");\
    check_value_variance(NAME(std::complex<float>(1, 2)), *cmds.GetSymbol(std::string(#NAME) + "_cf"), std::string(#NAME) + "(complex<float>)");\
    check_value_variance(NAME(std::complex<double>(1, 2)), *cmds.GetSymbol(std::string(#NAME) + "_cd"), std::string(#NAME) + "(complex<double>)");\
    check_valarray_variance(std::valarray<float>(NAME(2.0), 10), cmds.GetSymbol(std::string(#NAME) + "_sf"), std::string(#NAME) + "(Sequence<float>)");\
    check_valarray_variance(std::valarray<double>(NAME(2.0), 10), cmds.GetSymbol(std::string(#NAME) + "_sd"), std::string(#NAME) + "(Sequence<double>)");\
    check_valarray_variance(std::valarray<std::complex<float> >(NAME(std::complex<float>(1, 2)), 10), cmds.GetSymbol(std::string(#NAME) + "_scf"), std::string(#NAME) + "(Sequence<complex<float>>)");\
    check_valarray_variance(std::valarray<std::complex<double> >(NAME(std::complex<double>(1, 2)), 10), cmds.GetSymbol(std::string(#NAME) + "_scd"), std::string(#NAME) + "(Sequence<complex<double>>)");\

    //EXPAND(cos)
    EXPAND(cosh)
    //EXPAND(exp)
    //EXPAND(log)
    //EXPAND(sin)
    EXPAND(sinh)
    //EXPAND(sqrt)

#undef EXPAND

  }

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Terminate program with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Message() << "math functions: end\n";
  Test.Exit();
}
