#include "datacondAPI/config.h"

#include <complex>
   
#include "general/unittest.h"

#include "LineRemover.hh"

#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "random.hh"

using namespace datacondAPI;

General::UnitTest Test;

const double pi = 3.141592654;

gasdevstate seed;

void gasdev(std::valarray<float>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<double>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<std::complex<float> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<float>(gasdev(seed), gasdev(seed));
    }
}

void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}

template<typename T> struct complex_traits
{
    typedef std::complex<T> complex_type;
    typedef T real_type;
};

template<typename T> struct complex_traits<std::complex<T> >
{
    typedef typename complex_traits<T>::complex_type complex_type;
    typedef typename complex_traits<T>::real_type real_type;
};

void test_specific()
{
    {
        LineRemover lr(0.5, 3, 16);
        Test.Check(true) << "LineRemover(const double&, const std::size_t&, const std::size_t&)" << std::endl;
    }
    Test.Check(true) << "~LineRemover()" << std::endl;
    {
        LineRemover lr(Scalar<double>(0.5), Scalar<int>(3), Scalar<int>(16));
        Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&)" << std::endl;
        try
        {
              //LineRemover lr(Scalar<int>(0.5), Scalar<int>(3), Scalar<int>(16));   
            LineRemover lr(Scalar<int>(0), Scalar<int>(3), Scalar<int>(16));
            Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&) (failure)" << std::endl;
        }
        catch (std::logic_error)
        {
            Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&) (failure)" << std::endl;
        }
        try
        {
            LineRemover lr(Scalar<double>(0.5), Scalar<double>(3), Scalar<int>(16));
            Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&) (failure)" << std::endl;
        }
        catch (std::logic_error)
        {
            Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&) (failure)" << std::endl;
        }
        try
        {
            LineRemover lr(Scalar<double>(0.5), Scalar<int>(3), Scalar<double>(16));
            Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&) (failure)" << std::endl;
        }
        catch (std::logic_error)
        {
            Test.Check(true) << "LineRemover(const udt&, const udt&, const udt&) (failure)" << std::endl;
        }
        LineRemover lr2(lr);
        Test.Check(true) << "LineRemover(const LineRemover&)" << std::endl;
        lr = lr2;
        Test.Check(true) << "LineRemover& operator=(const LineRemover&)" << std::endl;
        lr = lr;
        Test.Check(true) << "LineRemover& operator=(const LineRemover&) (self-assignment)" << std::endl;
        {
            LineRemover* p = lr.Clone();
            Test.Check(p) << "LineRemover* Clone() const" << std::endl;
            delete p;
        }
        try
        {
            udt* b = 0;
            lr.getFilter(b);
            Test.Check(false) << "void getFilter(udt*&) const (failure)" << std::endl;
        }
        catch(std::logic_error)
        {
            Test.Check(true) << "void getFilter(udt*&) const (failure)" << std::endl;
        }
        {
            udt* w = 0;
            Sequence<double> u;
            try
            {
                lr.apply(w, u);
                Test.Check(false) << "void apply(udt*&, const udt&) (failure)" << std::endl;
            }
            catch(std::logic_error)
            {
                Test.Check(true) << "void apply(udt*&, const udt&) (failure)" << std::endl;
            }
        }
    }
}

template<typename type> void test_generic(const std::string& name)
{
    Test.Message() << name << std::endl;

    //:todo: establish correctness of object functionality

    try
    {
        LineRemover lr(0.5, 3, 16);
        Sequence<type> w, u;
        lr.apply(w, u);
        Test.Check(false) << "void apply(std::valarray<type>&, const std::valarray<type>&) (failure)" << std::endl;
    }
    catch (std::logic_error)
    {
        Test.Check(true) << "void apply(std::valarray<type>&, const std::valarray<type>&) (failure)" << std::endl;
    }
    {
        LineRemover lr(0.5, 4, 8);

        std::valarray<type> y(256), u(256);
        gasdev(y);
        gasdev(u);

        lr.refine(y, u);
        Test.Check(true) << "void refine(const std::valarray<type>&, const std::valarray<type>&)" << std::endl;
        lr.refine(y, u);
        Test.Check(true) << "void refine(const std::valarray<type>&, const std::valarray<type>&) (sequential)" << std::endl;

        lr.apply(y, u);
        Test.Check(true) << "void apply(std::valarray<type>&, const std::valarray<type>&)" << std::endl;
        lr.apply(y, u);
        Test.Check(true) << "void apply(std::valarray<type>&, const std::valarray<type>&) (sequential)" << std::endl;

        {
            udt* b = 0;
            lr.getFilter(b);
            Test.Check(b && (dynamic_cast<Sequence<typename complex_traits<type>::complex_type>&>(*b).size() == 9)) << "void getFilter(udt*&) const" << std::endl;
        }
    }
    {
        LineRemover lr(Scalar<double>(0.5), Scalar<int>(4), Scalar<int>(8));

        Sequence<type> y(256), u(256);
        gasdev(y);
        gasdev(u);

        lr.refine(y, u);
        Test.Check(true) << "void refine(const udt&, const udt&)" << std::endl;
        lr.refine(y, u);
        Test.Check(true) << "void refine(const udt&, const udt&) (sequential)" << std::endl;

        udt* w = 0;

        lr.apply(w, u);
        Test.Check(true) << "void apply(udt*&, const udt&)" << std::endl;
        lr.apply(w, u);
        Test.Check(true) << "void apply(udt*&, const udt&) (sequential)" << std::endl;
    }

    //:todo: size mismatch between apply arguments
    //:todo: size indivisible by factor
    //:todo: type mismatch between udt arguments
    //:todo: type mismatch between refine calls
    //:todo: type mismatch between apply calls
    //:todo: efficacy validation
}

int main(int argc, char** argv)
try
{
    Test.Init(argc, argv);

    test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("complex<float>");
    test_generic<std::complex<double> >("complex<double>");

    Test.Exit();

}
catch (const std::exception& x)
{
    Test.Check(false) << "unexpected std::exception " << x.what() << std::endl;
    Test.Exit();
}
catch (...)
{
    Test.Check(false) << "unexpected non-std exception" << std::endl;
    Test.Exit();
}
