#include "datacondAPI/config.h"

#include <iomanip>

#include <general/unittest.h>

#include "SequenceUDT.hh"
#include "CoherenceFunction.hh"
#include "CSDFunction.hh"
#include "WelchCSDEstimate.hh"
#include "WelchCSDSpectrumUDT.hh"

#include "token.hh"

using namespace General;
using namespace datacondAPI;

UnitTest Test;

int main(int ArgC, char** ArgV)
{
    Test.Init(ArgC, ArgV);

    try
    {
    }
    CATCH(Test)

    Test.Exit();
}
