//Testing program for the LinFilt class
//! author="Charlie Shapiro, Lee Samuel Finn"

// $Id: tLinFilt.cc,v 1.25 2005/12/01 22:55:03 emaros Exp $

#include "datacondAPI/config.h"

#include <iostream>
#include <complex>
#include <stdexcept>
#include <string>

#include "general/unittest.h"

#include "SequenceUDT.hh"
#include "TimeSeries.hh"
#include "LinFilt.hh"
#include "LinFiltState.hh"
#include "WelchCSDEstimate.hh"
#include "WelchSpectrumUDT.hh"

using namespace datacondAPI;
using General::GPSTime;
   
using std::valarray;   
using std::complex;
using std::endl;

General::UnitTest	Test;

// Forward declarations

bool testExcept(const std::string&,
                const datacondAPI::Sequence<double>&,
                const datacondAPI::Sequence<double>&);
                
bool testExcept(const std::string& name, 
		const datacondAPI::Sequence<double>&, 
		const datacondAPI::Sequence<double>&,
		const datacondAPI::Sequence<std::complex<double> >&);

bool testExceptions(void);
		
template <class Tout, class Tin> 
bool testTrivial(void);

template <class Tout, class Tin>
bool testFIR(std::string);

template <class Tout, class Tin>
bool testCFIR(std::string);

template <class Tout, class Tin>
bool testIIR(std::string);

template <class Tout, class Tin>
bool testCIIR(std::string);

template <class Tout, class Tin>
bool testPZFilter(std::string);

template <class Tout, class Tin>
bool testCPZ(std::string);

template<class T>
bool testZPG();

bool testTrivial(void);


void mkpath(std::string& path)
{
  const char* const srcdir = getenv("DATADIR");
  if (srcdir != 0) path = srcdir;
  else path = ".";
}

template<class T>
void TestSpec()
{
  bool pass = true;

  const std::string basename("tLinFilt.dat");

  const size_t aSize = 9;
  const size_t bSize = 9;

  const double a0[aSize] = {
    1.0000000000,
    -6.742264695,
    19.9735626,
    -33.94734086,
    36.1962698,
    -24.78747231,
    10.64448246,
    -2.620264891,
    0.283035192
  };

  const double b0[bSize] = {
    0.532010519,
    -4.25608415,
    14.89629453,
    -29.79258905,
    37.24073632,
    -29.79258905,
    14.89629453,
    -4.25608415,
    0.532010519
  };

  const valarray<double> a(a0, aSize);
  const valarray<double> b(b0, bSize);

  std::string path;
  mkpath(path);
  if (path.size() != 0)
  {
    path += "/";
  }

  path += basename;
  std::ifstream in(path.c_str());

  if (!in)
  {
    Test.Check(false) << "Couldn't open input file" << std::endl;
  }

  Sequence<T> dataIn(65536);
  for (size_t i = 0; in; ++i)
  {
    in >> dataIn[i];
  }

  LinFilt lf(b, a);

  Sequence<T> dataOut(65536);

  lf.apply(dataOut, dataIn);

  WelchCSDEstimate we(1024, 512);

  WelchSpectrum<T> specIn;
  WelchSpectrum<T> specOut;

  we.apply(specIn, dataIn);
  we.apply(specOut, dataOut);

  for (size_t k = 0; k < 30; ++k)
  {
      pass = pass && (specOut[k] < specIn[k]);
  }

  Test.Check(pass) << "Power below fc is reduced"
                   << std::endl;

  pass = true;
  for (size_t k = 100; k < specIn.size(); ++k)
  {
      const T diff = std::abs(specOut[k] - specIn[k]);
      pass = pass && (diff < 1.0);
  }

  Test.Check(pass) << "Power above fc is unaffected"
                   << std::endl;
}

int main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);
  if (Test.IsVerbose())
    std::cout << "$Id: tLinFilt.cc,v 1.25 2005/12/01 22:55:03 emaros Exp $" 
	 << std::endl << std::flush;

  try
  {

  //======================================================================
  // Check exceptions

  Test.Check(testExceptions()) 
    << "(all exception tests)" << std::endl << std::flush;

  //======================================================================
  // Check trivial filter
  Test.Check(testTrivial<double,double>())
    << "(all trivial filter tests <double>)" 
    << std::endl << std::flush;
  
  Test.Check(testTrivial<float,float>())
    << "(all trivial filter tests <float>)" 
    << std::endl << std::flush;
  
  //======================================================================
  // Check FIR filter
  Test.Check(testFIR<double,double>("<double,double>")) 
    << "(all FIR filter tests <double>)" 
    << std::endl << std::flush;

  Test.Check(testFIR<float,float>("<float,float>")) 
    << "(all FIR filter tests <float>)" 
    << std::endl << std::flush;

  Test.Check(testFIR<std::complex<double>,std::complex<double> >
	     ("<std::complex<double>,std::complex<double> >")) 
    << "(all FIR filter tests <std::complex<double> >)" 
    << std::endl << std::flush;

  Test.Check(testFIR<std::complex<float>,std::complex<float> >
	     ("<std::complex<float>,std::complex<float> >")) 
    << "(all FIR filter tests <std::complex<float> >)" 
    << std::endl << std::flush;

  Test.Check(testCFIR<float,float>
	     ("<std::complex<float>,std::complex<float> >")) 
    << "(all std::complex FIR filter tests <std::complex<float> >)" 
    << std::endl << std::flush;

  Test.Check(testCFIR<double,double>
	     ("<std::complex<double>,std::complex<double> >")) 
    << "(all std::complex FIR filter tests <std::complex<double> >)" 
    << std::endl << std::flush;

  //======================================================================
  // Check all-pole filter
  Test.Check(testIIR<double,double>("<double,double>")) 
    << "(all all-pole filter tests <double>)" 
    << std::endl << std::flush;

  Test.Check(testIIR<float,float>("<float,float>")) 
    << "(all all-pole filter tests <float>)" 
    << std::endl << std::flush;

  Test.Check(testIIR<std::complex<float>,std::complex<float> >
	     ("<std::complex<float>,std::complex<float> >")) 
    << "(all all-pole filter tests <std::complex<float> >)" 
    << std::endl << std::flush;

  Test.Check(testIIR<std::complex<double>,std::complex<double> >
	     ("<std::complex<double>,std::complex<double> >")) 
    << "(all all-pole filter tests <std::complex<double> >)" 
    << std::endl << std::flush;

  Test.Check(testCIIR<float, float>("<std::complex<float>,std::complex<float> >")) << "IIR filter std::complex<float>" << std::endl;
  Test.Check(testCIIR<double, double>("<std::complex<double>,std::complex<double> >")) << "IIR filter std::complex<double>" << std::endl;

  //======================================================================
  // Check full filter
  Test.Check(testPZFilter<double,double>("<double,double>")) 
    << "(all full filter tests <double>)" 
    << std::endl << std::flush;

  Test.Check(testPZFilter<std::complex<double> ,std::complex<double> >
	     ("<std::complex<double> ,std::complex<double> >")) 
    << "(all full filter tests <std::complex<double> >)" 
    << std::endl << std::flush;

  Test.Check(testPZFilter<float,float>("<float,float>")) 
    << "(all full filter tests <float>)" 
    << std::endl << std::flush;

  Test.Check(testPZFilter<std::complex<float> ,std::complex<float> >
	     ("<std::complex<float> ,std::complex<float> >")) 
    << "(all full filter tests <std::complex<float> >)" 
    << std::endl << std::flush;

  Test.Check(testCPZ<float, float>("<std::complex<float>,std::complex<float> >")) << "full filter std::complex<float>" << std::endl;
  Test.Check(testCPZ<double, double>("<std::complex<double>,std::complex<double> >")) << "full filter std::complex<double>" << std::endl;

  
  Test.Check(testZPG<double>()) << "ZPG filter, double" << endl;

  // New test that looks at spectrum of real E7 LIGO data
  TestSpec<float>();
  TestSpec<double>();

  }
  catch(std::exception& x)
  {
      Test.Check(false) << "caught exception:" << x.what() << std::endl;
  }
  catch(...)
  {
      Test.Check(false) << "caught non-standard exception" << std::endl;
  }

  Test.Exit();

}

//------------------------------------------------------------------------

bool testExcept(const std::string& name, 
		const datacondAPI::Sequence<double>& a, 
		const datacondAPI::Sequence<double>& b)
{
  bool pass;

  std::string aname("LinFiltState(b,a): ");
  aname += name;

  try 
    {
      LinFiltState state(b, a); 
      pass = false;
      Test.Check(pass) 
	<< aname 
	<< "(no exception thrown)" << std::endl;
    }
  catch (std::invalid_argument& r) 
    {
      pass = true;
      Test.Check(pass) << aname << "(" << r.what() << ")" 
		       << std::endl;
    }
  catch (std::exception& r) 
    { 
      pass = false;
      Test.Check(pass) 
	<< aname 
	<< "unexpected std::exception (" 
	<< r.what() << ")" << std::endl << std::flush;
    } 
  catch (...) 
    {
      pass = false;
      Test.Check(pass) 
	<< aname << "unexpected exception" << std::endl;
    }

  aname = "LinFilt(b,a): "; 
  aname += name;

  bool lfpass;
  try 
    {
      LinFilt lf(b,a);
      lfpass = false;
      Test.Check(lfpass) 
	<< aname 
	<< "(no exception thrown)" << std::endl;
    }
  catch (std::invalid_argument& r) 
    {
      lfpass = true;
      Test.Check(lfpass) << aname << "(" << r.what() << ")" 
		       << std::endl;
    }
  catch (std::exception& r) 
    { 
      lfpass = false;
      Test.Check(lfpass) 
	<< aname 
	<< "unexpected std::exception (" 
	<< r.what() << ")" << std::endl << std::flush;
    } 
  catch (...) 
    {
      lfpass = false;
      Test.Check(lfpass) 
	<< aname << "unexpected exception" << std::endl;
    }

  return lfpass && pass;
  
}

bool testExcept(const std::string& name, 
		const datacondAPI::Sequence<double>& a, 
		const datacondAPI::Sequence<double>& b,
		const datacondAPI::Sequence<std::complex<double> >& z)
{
  
  bool pass = true;
  std::string tname;

  /*
  tname = "LinFiltState(a,b,z): ";
  tname += name;

  try 
    {
      LinFiltState state(b, a, z);
      pass = false;
      Test.Check(pass)
	<< tname << " (no exception thrown)" << std::endl;
      return pass;
    } 
  catch (std::invalid_argument& r) 
    {
      pass = true;
      Test.Check(pass) 
	<< tname << " (" << r.what() << ")" << std::endl << std::flush;
    }
  catch (std::exception& r) 
    {
      Test.Check(pass) 
	<< tname 
	<< "unexpected std::exception (" << r.what() << ")" 
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(pass)
	<< tname 
	<< "unexpected exception" << std::endl;
    }
  */
  return pass;

}

template <class Tout, class Tin>
bool testTrivial(void)  {
  bool allPass = true;
  datacondAPI::Sequence<double> b(1.0,1);
  datacondAPI::Sequence<double> a(4.0,1);
  LinFiltState state(b,a);
  LinFilt lf(state);

  datacondAPI::Sequence<Tin> x(100);
  for (size_t k = 0; k < x.size(); k++)
    x[k] = 4*k;

  datacondAPI::Sequence<Tout> y;
  datacondAPI::udt* y_pointer=&y;
  lf.apply(y_pointer,x);

  for (size_t k = 0; k < y.size(); k++)
    if (y[k] != k) allPass = false;
    
  return allPass;  
}

bool testExceptions(void)
{

    bool allPass = true;
    datacondAPI::Sequence<double> b(1.0,5);
    datacondAPI::Sequence<double> a(0.0,3);
    
    // a[0] = 0;

    allPass = allPass && testExcept("a[0] == 0", a, b);

    // b = 0;

    a[0] = 5;
    b = 0;

    allPass = allPass && testExcept("b == 0", a, b);

    // b.size() == 0;

    b.resize(0);
  
    allPass = allPass && testExcept("b.size() == 0", a, b);

    // a.size() == 0;

    b.resize(5);
    b = 1;
    a.resize(0);

    allPass = allPass && testExcept("a.size() == 0", a, b);

    // z.size() != max(a.size(), b.size())

    a.resize(4);
    a = 1;
    b.resize(5);
    b = 1;
    std::complex<double> zero(0,0);
    datacondAPI::Sequence<std::complex<double> > z(zero,4);

    /*  
    allPass = allPass && 
      testExcept("z.size() < max(a.size(),b.size())", a, b, z);

    z.resize(6);
    for(unsigned i=0; i<z.size(); i++)
	{
		z[i] = zero;
	}
    allPass = allPass && 
      testExcept("z.size() > max(a.size(),b.size())", a, b, z);
    */

    // test exception on input size == 0
    a.resize(2);
    a = 1;
    b.resize(2);
    b = 1;

    bool pass(false);
    try
      {
      LinFiltState state(b,a);
      try
	{
	  LinFilt lf(state);
	  datacondAPI::Sequence<float> x(0);
	  datacondAPI::Sequence<float> y(1);
	  datacondAPI::udt* y_pointer = &y;
	  try {
	    lf.apply(y_pointer,x);
	  } catch (std::exception &r) {
	    pass = true;
	  }
	  Test.Check(pass)
	    << "zero length input to apply()" 
	    << std::endl << std::flush;
	}
      catch (std::exception& r)
	{
	  Test.Check(false) << "Failed at LinFilt(state) : "
			    << r.what()
			    << std::endl;
	  throw;
	}
      }
    catch (...)
      {
	Test.Check(false) << "Failed at LinFiltState state(b,a);" 
			  << std::endl;
	throw;
      }
	
    return pass && allPass;
}

template <class Tout, class Tin> 
bool testFIR(std::string ttstr)
{
  bool allPass = true;
  {
    datacondAPI::Sequence<double> b(5);
    for (size_t k = 0; k < 5; k++) b[k] = k;
    
    datacondAPI::Sequence<double> a(1.0,1);
    
    // Results from matlab
    Tin ax[] = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
    datacondAPI::Sequence<Tin> xseq(ax,11);
    const GPSTime StartTime(10000, 0);
    const std::string Name("H2:LSC-AS_Q");
    const double fs = 2048.0;

    datacondAPI::TimeSeries<Tin> x(xseq, fs);
    
    x.SetStartTime(StartTime);
    x.SetName(Name);
    
    Tin ay[] = {0, -5, -14, -26, -40, -30, -20, -10, 0, 10, 20};
    datacondAPI::Sequence<Tout> y0(ay,11);
    
    LinFilt lf(b,a);
    datacondAPI::TimeSeries<Tout> y;
    datacondAPI::udt* y_pointer = &y;
    lf.apply(y_pointer,x);

    bool pass = true;
    for (size_t k = 0; k < y0.size(); k++)
      if (y0[k] != y[k]) pass = false;
    Test.Check(pass) 
      << "FIR filter " << ttstr << std::endl;

    // Check meta-data
    Test.Check(y.GetStartTime() == x.GetStartTime())
	<< "Start times are equal" << std::endl;

    Test.Check(y.GetEndTime() == x.GetEndTime())
	<< "End times are equal" << std::endl;

    Test.Check(y.GetSampleRate() == x.GetSampleRate())
	<< "Sample rates are equal" << std::endl;

    const LinFiltState state = lf.getState();

    Test.Check(y.name() == std::string("linfilt(" + x.name() + "," + state.name() +")"))
	<< "Name is correct" << std::endl;

    allPass = allPass && pass;
  }

  // FIR filter with split input
  {
    datacondAPI::Sequence<double> b(5);
    for (size_t k = 0; k < 5; k++) b[k] = k;
    
    datacondAPI::Sequence<double> a(1.0,1);
    
    // Results from matlab
    const int nx(11);
    Tin ax[] = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
    datacondAPI::Sequence<Tin> x(ax,nx);
    Tout ay[] = {0, -5, -14, -26, -40, -30, -20, -10, 0, 10, 20};
    datacondAPI::Sequence<Tout> y0(ay,nx);

    const datacondAPI::TimeSeries<Tin>&
      const_x( static_cast< const datacondAPI::TimeSeries<Tin>& >(x) );
    std::slice y1(0,5,1);
    std::slice y2(5,nx-5,1);

    datacondAPI::Sequence<Tin> x1(const_x[y1]);
    datacondAPI::Sequence<Tin> x2(const_x[y2]);

    datacondAPI::Sequence<Tout> y(nx);
    datacondAPI::Sequence<Tout> yp;

    datacondAPI::Sequence<std::complex<double> > z;

    datacondAPI::udt* yp_pointer = &yp;

    LinFilt lf(b,a);

    lf.apply(yp_pointer,x1);
    y[y1] = yp;

    datacondAPI::Sequence<Tout> ya;
    datacondAPI::udt* ya_pointer = &ya;

    LinFiltState state(lf.getState());
    LinFilt la(state);

    la.apply(ya_pointer,x2);
    
    lf.apply(yp_pointer,x2);

    y[y2] = yp;

    bool pass(yp.size() == ya.size());
    Test.Check(pass) 
      << "FIR filter state (output length) " 
      << ttstr << std::endl << std::flush;

    pass = true;
    for (size_t k = 0; k < yp.size(); k++)
      if (std::abs(yp[k] - ya[k]) > 1e-6) pass = false;

    Test.Check(pass) << "FIR filter state (values) " 
		     << ttstr << std::endl << std::flush;

    pass = true;
    for (size_t k = 0; k < y0.size(); k++)
      if (y0[k] != y[k]) pass = false;
    Test.Check(pass) << "FIR filter, split input" 
		     << ttstr << std::endl << std::flush;

    allPass = allPass && pass;
  }
  return allPass;
}

// Std::Complex version of FIR
template <class Tout, class Tin> 
bool testCFIR(std::string ttstr)
{
  bool allPass = true;
  {
    datacondAPI::Sequence<double> b(5);
    for (size_t k = 0; k < 5; k++) b[k] = k;
    
    datacondAPI::Sequence<double> a(1.0,1);
    
    std::complex<Tin> ax[] = {
	std::complex<Tout>(-5, 1.4142136),
	    std::complex<Tout>(-4, 1.4142136),
	    std::complex<Tout>(-3, 1.4142136),
	    std::complex<Tout>(-2, 1.4142136),
	    std::complex<Tout>(-1, 1.4142136),
	    std::complex<Tout>(0, 1.4142136),
	    std::complex<Tout>(1, 1.4142136),
	    std::complex<Tout>(2, 1.4142136),
	    std::complex<Tout>(3, 1.4142136),
	    std::complex<Tout>(4, 1.4142136),
	    std::complex<Tout>(5, 1.4142136)
	    };

    // Construct a std::complex sequence with imag = 0
    datacondAPI::Sequence<std::complex<Tin> > x(ax,11);

    // Results from matlab
    std::complex<Tout> ay[] = {
	std::complex<Tout>(0, 0),
	    std::complex<Tout>(-5, 1.4142136),
	    std::complex<Tout>(-14, 4.2426407),
	    std::complex<Tout>(-26, 8.4852814),
	    std::complex<Tout>(-40, 14.142136),
	    std::complex<Tout>(-30, 14.142136),
	    std::complex<Tout>(-20, 14.142136),
	    std::complex<Tout>(-10, 14.142136),
	    std::complex<Tout>(0, 14.142136),
	    std::complex<Tout>(10, 14.142136),
	    std::complex<Tout>(20, 14.142136)
	    };

    datacondAPI::Sequence<std::complex<Tout> > y0(ay,11);
    
    LinFilt lf(b,a);
    datacondAPI::Sequence<std::complex<Tout> > y;
    datacondAPI::udt* y_pointer = &y;
    lf.apply(y_pointer,x);

    bool pass = true;
    for (size_t k = 0; k < y0.size(); k++)
    {  
        Tout diff = std::abs(y0[k]-y[k]);
	if (diff > 1E-05)
	{
	    pass = false;
	    Test.Message() << "k: " << k << "  y: " << y[k] << "  y0: " << y0[k] << "  diff = " << diff << std::endl;
	}
    }
      Test.Check(pass) 
	  << "FIR filter " << ttstr << std::endl;
      

    allPass = allPass && pass;
  }

  return allPass;
}

template <class Tout, class Tin>
bool testIIR(std::string ttstr) {
  bool allPass = true;
  {
    datacondAPI::Sequence<double> b(1.0,1);

    double aa[] = {1, 0.5};
    datacondAPI::Sequence<double> a(aa,2);
    
    // Results from matlab
    Tin ax[] = {0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
    datacondAPI::Sequence<Tin> x(ax,10);
    Tout ay[] = {0, 0, 1, 0.5, 0.75, 
		   0.625, -0.3125, 0.3125/2, -0.3125/4, 0.3125/8};
    datacondAPI::Sequence<Tout> y0(ay,10);

    LinFiltState state(b,a);
    LinFilt lf(state);
    datacondAPI::Sequence<Tout> y;
    datacondAPI::udt* y_pointer = &y;
    lf.apply(y_pointer, x);

    bool pass = true;
    for (size_t k = 0; k < y0.size(); k++)
      if (y0[k] != y[k]) pass = false;
    Test.Check(pass) << "all-pole filter " 
		     << ttstr << std::endl << std::flush;

    allPass = allPass && pass;

  }

  // all-pole filter with split input
  {
    datacondAPI::Sequence<double> b(1.0,1);

    double aa[] = {1, 0.5};
    datacondAPI::Sequence<double> a(aa,2);

    // Results from matlab
    const int nx(10);
    Tin ax[] = {0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
    datacondAPI::Sequence<Tin> x(ax,10);
    Tout ay[] = {0, 0, 1, 0.5, 0.75, 
		 0.625, -0.3125, 0.3125/2, -0.3125/4, 0.3125/8};
    datacondAPI::Sequence<Tout> y0(ay,10);

    const datacondAPI::Sequence<Tin>&
      const_x( static_cast< datacondAPI::Sequence<Tin>& >(x) );

    std::slice y1(0,5,1);
    std::slice y2(5,nx-5,1);

    datacondAPI::Sequence<Tout> y(nx);
    datacondAPI::Sequence<Tout> yp;
    datacondAPI::udt* yp_pointer = &yp;

    datacondAPI::Sequence<Tin> x1(const_x[y1]);
    datacondAPI::Sequence<Tin> x2(const_x[y2]);

    LinFilt lf(b,a);

    lf.apply(yp_pointer,x1);
    y[y1] = yp;

    lf.apply(yp_pointer,x2);
    y[y2] = yp;

    bool pass = true;
    for (size_t k = 0; k < y0.size(); k++)
      if (y0[k] != y[k]) pass = false;
    Test.Check(pass) << "all-pole filter, split input " 
		     << ttstr << std::endl << std::flush;

    allPass = allPass && pass;

    //    for (int k = 0; k < y0.size(); k++)
    //      cout << k << " " << y[k] << " " << y0[k] << std::endl << std::flush;

  }
  return allPass;
}

template<class Tout, class Tin>
bool testCIIR(std::string ttstr)
{

    bool allPass = true;

    {
	// filter set-up

	const double a_raw[] =
	{
	    1.0,
	    -0.5
	};

	Sequence<double> a(a_raw, 2);

	const double b_raw[] =
	{
	    1
	};

	Sequence<double> b(b_raw, 1);

	LinFiltState state(b, a);

	const std::complex<Tin> x_raw[] =
	{
	    std::complex<Tin>(2, 1),
	    std::complex<Tin>(0, 0),
	    std::complex<Tin>(0, 0),
	    std::complex<Tin>(0, 0),
	};
	
	const std::complex<Tout> y_raw[] =
	{
	    std::complex<Tout>(2, 1),
	    std::complex<Tout>(1, 0.5),
	    std::complex<Tout>(0.5, 0.25),
	    std::complex<Tout>(0.25, 0.125)

	};

	// continuous input

	Sequence<std::complex<Tin> > x(x_raw, 4);

	Sequence<std::complex<Tout> > y_expected(y_raw, 4);

	LinFilt filter(state);

	Sequence<std::complex<Tout> > y;
	datacondAPI::udt* y_pointer = &y;

	filter.apply(y_pointer, x);

	bool pass = true;

	for (unsigned int i = 0; i < y_expected.size(); ++i)
        {
	    // Test.Message() << y[i] << " ?= " << y_expected[i] << std::endl;
	    pass = pass && (y[i] == y_expected[i]);
	}

	allPass = allPass && pass;

	Test.Check(pass) << "IIR filter " << ttstr << std::endl;
   
	// split input

	Sequence<std::complex<Tin> > x_1(x_raw, 2);
	Sequence<std::complex<Tin> > x_2(x_raw + 2, 2);

	Sequence<std::complex<Tout> > y_expected_1(y_raw, 2);
	Sequence<std::complex<Tout> > y_expected_2(y_raw + 2, 2);

	filter.setState(state);

	pass = true;

	filter.apply(y_pointer, x_1);

	for (unsigned int i = 0; i < y_expected_1.size(); ++i)
        {
	    //Test.Message() << y[i] << " ?= " << y_expected_1[i] << std::endl;
	    pass = pass && (y[i] == y_expected_1[i]);
	}

	filter.apply(y_pointer, x_2);

	for (unsigned int i = 0; i < y_expected_1.size(); ++i)
        {
	    //Test.Message() << y[i] << " ?= " << y_expected_2[i] << std::endl;
	    pass = pass && (y[i] == y_expected_2[i]);
	}

	allPass = allPass && pass;

	Test.Check(pass) << "split IIR filter " << ttstr << std::endl;

    }

    return allPass;

}

template <class Tout, class Tin> 
bool testPZFilter(std::string ttstr)
{
  bool allPass = true;
  {
    double ab[] = {1, 0.5, 0.25};
    datacondAPI::Sequence<double> b(ab,3);
    double aa[] = {1, 0.5};
    datacondAPI::Sequence<double> a(aa,2);
    
    // Results from matlab
    Tin ax[] = {0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
    datacondAPI::Sequence<Tin> x(ax,10);
    Tout ay[] = {0, 0, 1, 1, 1.25,
		   1.125, 0.1875, 0.15625, -0.078125, 0.0390625};
    datacondAPI::Sequence<Tout> y0(ay,10);

    LinFiltState state(b, a);
    LinFilt lf(state);
    datacondAPI::Sequence<Tout> y;
    datacondAPI::udt* y_pointer = &y;
    lf.apply(y_pointer,x);

    bool pass = true;
    for (size_t k = 0; k < y0.size(); k++)
      if (y0[k] != y[k]) pass = false;
    Test.Check(pass) << "full filter " 
		     << ttstr << std::endl << std::flush;

    allPass = allPass && pass;

    //    for (int k = 0; k < y0.size(); k++)
    //      cout << k << " " << y[k] << " " << y0[k] << std::endl << std::flush;

  }

  // full filter with split input
  {
    double ab[] = {1, 0.5, 0.25};
    datacondAPI::Sequence<double> b(ab,3);
    double aa[] = {1, 0.5};
    datacondAPI::Sequence<double> a(aa,2);
    
    // Results from matlab
    Tin ax[] = {0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
    datacondAPI::Sequence<Tin> x(ax,10);
    Tout ay[] = {0, 0, 1, 1, 1.25,
		   1.125, 0.1875, 0.15625, -0.078125, 0.0390625};
    datacondAPI::Sequence<Tout> y0(ay,10);

    const datacondAPI::Sequence<Tin>&
      const_x( static_cast< const datacondAPI::Sequence<Tin>& >( x ) );

    std::slice y1(0,5,1);
    std::slice y2(5,5,1);
 
    LinFiltState state(b,a);
    LinFilt lf(state);
    datacondAPI::Sequence<Tout> y(10);
    datacondAPI::Sequence<Tout> yp;
    datacondAPI::Sequence<Tin> x1(const_x[y1]);
    datacondAPI::Sequence<Tin> x2(const_x[y2]);

    lf.apply(yp,x1);
    y[y1] = yp;
    lf.apply(yp,x2);
    y[y2] = yp;

    bool pass = true;
    for (size_t k = 0; k < y0.size(); k++)
      if (y0[k] != y[k]) pass = false;
    Test.Check(pass) << "full filter, split input " 
		     << ttstr << std::endl << std::flush;

    allPass = allPass && pass;

    //    for (int k = 0; k < y0.size(); k++)
    //      cout << k << " " << y[k] << " " << y0[k] << std::endl << std::flush;

  }
  return allPass;
}

template<class Tout, class Tin>
bool testCPZ(std::string ttstr)
{

    bool allPass = true;

    {
	// filter set-up

	const double a_raw[] =
	{
	    1.0,
	    0.5,
	    0.25
	};

	Sequence<double> a(a_raw, 3);

	const double b_raw[] =
	{
	    1.0,
	    0.5
	};

	Sequence<double> b(b_raw, 2);

	LinFiltState state(b, a);

	const std::complex<Tin> x_raw[] =
	{
	    std::complex<Tin>(2, 1),
	    std::complex<Tin>(0, 0),
	    std::complex<Tin>(0, 0),
	    std::complex<Tin>(0, 0),
	};
	
	const std::complex<Tout> y_raw[] =
	{
	    std::complex<Tout>(2, 1),
	    std::complex<Tout>(0, 0),
	    std::complex<Tout>(-0.5, -0.25),
	    std::complex<Tout>(0.25, 0.125)

	};

	// continuous input

	Sequence<std::complex<Tin> > x(x_raw, 4);

	Sequence<std::complex<Tout> > y_expected(y_raw, 4);

	LinFilt filter(state);

	Sequence<std::complex<Tout> > y;
	datacondAPI::udt* y_pointer = &y;

	filter.apply(y_pointer, x);

	bool pass = true;

	for (unsigned int i = 0; i < y_expected.size(); ++i)
        {
	    // Test.Message() << y[i] << " ?= " << y_expected[i] << std::endl;
	    pass = pass && (y[i] == y_expected[i]);
	}

	allPass = allPass && pass;

	Test.Check(pass) << "full filter " << ttstr << std::endl;
   
	// split input

	Sequence<std::complex<Tin> > x_1(x_raw, 2);
	Sequence<std::complex<Tin> > x_2(x_raw + 2, 2);

	Sequence<std::complex<Tout> > y_expected_1(y_raw, 2);
	Sequence<std::complex<Tout> > y_expected_2(y_raw + 2, 2);

	filter.setState(state);

	pass = true;

	filter.apply(y_pointer, x_1);

	for (unsigned int i = 0; i < y_expected_1.size(); ++i)
        {
	    // Test.Message() << y[i] << " ?= " << y_expected_1[i] << std::endl;
	    pass = pass && (y[i] == y_expected_1[i]);
	}

	filter.apply(y_pointer, x_2);

	for (unsigned int i = 0; i < y_expected_1.size(); ++i)
        {
	    // Test.Message() << y[i] << " ?= " << y_expected_2[i] << std::endl;
	    pass = pass && (y[i] == y_expected_2[i]);
	}

	allPass = allPass && pass;

	Test.Check(pass) << "split full filter " << ttstr << std::endl;

    }

    return allPass;

}

// Test the ZPG constructor
template<class T>
bool testZPG()
{
    bool allPass = true;
    bool pass = true;

    const complex<double> i(0.0, 1.0);

    const double gain = 3.0;
    Sequence<complex<double> > z(2);
    Sequence<complex<double> > p(3);

    z[0] = 1.0;
    z[1] = 1.0 + i;

    p[0] = 1.0;
    p[1] = 1.0 - 2.0*i;
    p[2] = 1.0 + 3.0*i;

    Sequence<double> a(6);
    a[0] = -50;
    a[1] = 80;
    a[2] = -49;
    a[3] = 23;
    a[4] = -5;
    a[5] = 1;

    Sequence<double> b(4);
    b[0] = -6;
    b[1] = 12;
    b[2] = -9;
    b[3] = 3;

    Sequence<double> out;
    Sequence<double> outCheck;

    const size_t n = 16;
    Sequence<T> in(n);
    
    for (size_t k = 0; k < in.size(); ++k)
    {
        in[k] = sin(2*3.14159*k/n);
    }

    // FIR
    {
        pass = true;

        // Empty poles array
        Sequence<complex<double> > pp;
        
        // Trivial IIR coeff array
        Sequence<double> aa(1.0, 1);

        LinFiltState lfZPG(z, pp, gain);
        LinFilt lf(lfZPG);
        
        lf.apply(out, in);

        LinFilt lfCheck(b, aa);
        
        lfCheck.apply(outCheck, in);
        
        for (size_t k = 0; k < out.size(); ++k)
        {
            const double diff = std::abs(out[k] - outCheck[k]);
            pass = pass && (diff < 1.0e-6);
        }

        Test.Check(pass) << "FIR ZPG vs. coefficients: values are correct"
                         << endl;
        allPass = allPass && pass;
    }

    // IIR
    {
        pass = true;

        // Empty zeroes array
        Sequence<complex<double> > zz;
        
        // Trivial FIR coeff array
        Sequence<double> bb(gain, 1);

        LinFiltState lfZPG(zz, p, gain);
        LinFilt lf(lfZPG);
        
        lf.apply(out, in);

        LinFilt lfCheck(bb, a);
        
        lfCheck.apply(outCheck, in);
        
        for (size_t k = 0; k < out.size(); ++k)
        {
            const double diff = std::abs(out[k] - outCheck[k]);
            pass = pass && (diff < 1.0e-6);
        }
            
        Test.Check(pass) << "IIR ZPG vs. coefficients: values are correct"
                         << endl;
        allPass = allPass && pass;
    }

    // General
    {
        pass = true;

        LinFiltState lfZPG(z, p, gain);
        LinFilt lf(lfZPG);
        
        lf.apply(out, in);

        LinFilt lfCheck(b, a);
        
        lfCheck.apply(outCheck, in);
        
        for (size_t k = 0; k < out.size(); ++k)
        {
            const double diff = std::abs(out[k] - outCheck[k]);
            pass = pass && (diff < 1.0e-6);
        }
            
        Test.Check(pass) << "General ZPG vs. coefficients: values are correct"
                         << endl;
        allPass = allPass && pass;
    }

    return allPass;
}
