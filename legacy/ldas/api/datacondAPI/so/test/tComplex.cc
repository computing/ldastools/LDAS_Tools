#include "datacondAPI/config.h"

#include <general/unittest.h>

#include "ScalarUDT.hh"
#include "Complex.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"
#include "complexFunction.hh"

#include "token.hh"

using namespace std;
using namespace General;
using namespace datacondAPI;

UnitTest Test;

template<class TExp, class TIn>
void VerifyComplex(const TIn& x, const udt& expUdt)
{
    // This checks that the result is the expected class
    const TExp& exp = dynamic_cast<const TExp&>(expUdt);

    Test.Check(true) << "Result has correct type" << endl;

    Test.Check(exp.size() == x.size()) << "Result has correct size" << endl;

    bool pass = true;
    for (size_t k = 0; k < x.size(); ++k)
    {
        pass = pass && (exp[k].real() == x[k]);
        pass = pass && (exp[k].imag() == 0);
    }

    Test.Check(pass) << "Result has correct values" << endl;
}

template<class TExp, class TIn>
void TestRealSequence()
{
    const size_t n = 100;
    
    TIn x;

    x.resize(n);
    for (size_t k = 0; k < x.size(); ++k)
    {
        x[k] = k*k;
    }

    // Try it using the UDT-based function
    const udt* expUdt = Complex(static_cast<const udt&>(x));

    VerifyComplex<TExp, TIn>(x, *expUdt);

    delete expUdt;

    // Try it using the Sequence-based function
    expUdt = Complex(x);

    VerifyComplex<TExp, TIn>(x, *expUdt);

    delete expUdt;
}

template<class TExp, class TIn>
void TestComplexSequence()
{
    const size_t n = 100;
    
    TIn x;

    x.resize(n);
    for (size_t k = 0; k < x.size(); ++k)
    {
        x[k] = k*k;
    }

    // Try it using the UDT-based function
    const udt* expUdt = Complex(static_cast<const udt&>(x));

    VerifyComplex<TExp, TIn>(x, *expUdt);

    delete expUdt;
}

void TestCallChainExcept()
{
    bool pass = true;
    string what;

    try {

        CallChain cmds;
        Parameters args;
        
        Sequence<float> x(10);
        Sequence<float> y(12);

        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        pass = false;
        what = "(NONE)";
    }
    catch(const std::exception& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Got exception: " << what << endl;

    try {

        CallChain cmds;
        Parameters args;
        
        Sequence<float> x(10);
        Sequence<double> y(10);

        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        pass = false;
        what = "(NONE)";
    }
    catch(const std::exception& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Got exception: " << what << endl;

    try {

        CallChain cmds;
        Parameters args;
        
        Sequence<complex<float> > x(10);
        Sequence<float> y(10);

        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        pass = false;
        what = "(NONE)";
    }
    catch(const std::exception& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Got exception: " << what << endl;

    try {

        CallChain cmds;
        Parameters args;
        
        Sequence<complex<float> > x(10);
        Sequence<complex<float> > y(10);

        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        pass = false;
        what = "(NONE)";
    }
    catch(const std::exception& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Got exception: " << what << endl;
    
}

template<class T>
void TestCallChain()
{
    {
        Test.Message() << "complex(x)" << endl;

        CallChain cmds;
        Parameters args;
        
        TimeSeries<T> x;
        x.resize(10, 1.0);
        x.SetStartTime(GPSTime(1000, 100));
        x.SetSampleRate(16384.0);
        
        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());

        cmds.AppendCallFunction("complex", args.set(1, "x"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");
        
        const TimeSeries<complex<T> >& z
            = dynamic_cast<const TimeSeries<complex<T> >&>(*zUdt);
        
        Test.Check(z.size() == x.size())
            << "Size is the same" << endl;

        Test.Check(z.GetSampleRate() == x.GetSampleRate())
            << "Sample rate is the same" << endl;

        Test.Check(z.GetStartTime() == x.GetStartTime())
            << "Start time is the same" << endl;

        bool pass = true;
        for (size_t k = 0; k < x.size(); ++k)
        {
            pass = pass && (x[k] == z[k].real());
            pass = pass && (z[k].imag() == 0);
        }

        Test.Check(pass)
            << "Values are correct" << endl;
    }

    {
        Test.Message() << "complex(scalar)" << endl;

        CallChain cmds;
        Parameters args;
        
        Scalar<double> x(5.0);
        
        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());

        cmds.AppendCallFunction("complex", args.set(1, "x"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");

        const Scalar<complex<double> >& z
            = dynamic_cast<const Scalar<complex<double> >&>(*zUdt);
        
        Test.Check(z.GetValue() == complex<double>(x.GetValue()))
            << "Value is correct" << endl;
    }

    {
        Test.Message() << "complex(scalar, scalar)" << endl;

        CallChain cmds;
        Parameters args;
        
        Scalar<double> x(5.0);
        Scalar<double> y(-3.0);
        
        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");

        const Scalar<complex<double> >& z
            = dynamic_cast<const Scalar<complex<double> >&>(*zUdt);
        
        Test.Check(z.GetValue() == complex<double>(x.GetValue(), y.GetValue()))
            << "Value is correct" << endl;
    }

    {
        Test.Message() << "complex(x, scalar)" << endl;

        CallChain cmds;
        Parameters args;
        
        TimeSeries<T> x;
        x.resize(16, 2.0);
        x.SetStartTime(GPSTime(1000, 100));
        x.SetSampleRate(16384.0);
        
        Scalar<double> y(5.0);

        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");
        
        const TimeSeries<complex<T> >& z
            = dynamic_cast<const TimeSeries<complex<T> >&>(*zUdt);
        
        Test.Check(z.size() == x.size())
            << "Size is the same" << endl;

        Test.Check(z.GetSampleRate() == x.GetSampleRate())
            << "Sample rate is the same" << endl;

        Test.Check(z.GetStartTime() == x.GetStartTime())
            << "Start time is the same" << endl;

        bool pass = true;
        for (size_t k = 0; k < x.size(); ++k)
        {
            pass = pass && (x[k] == z[k].real());
            pass = pass && (y.GetValue() == z[k].imag());
        }

        Test.Check(pass)
            << "Values are correct" << endl;
    }

    {
        Test.Message() << "complex(scalar, x)" << endl;

        CallChain cmds;
        Parameters args;
        
        TimeSeries<T> x;
        x.resize(16, 2.0);
        x.SetStartTime(GPSTime(1000, 100));
        x.SetSampleRate(16384.0);
        
        Scalar<double> y(5.0);

        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "y", "x"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");
        
        const TimeSeries<complex<T> >& z
            = dynamic_cast<const TimeSeries<complex<T> >&>(*zUdt);
        
        Test.Check(z.size() == x.size())
            << "Size is the same" << endl;

        Test.Check(z.GetSampleRate() == x.GetSampleRate())
            << "Sample rate is the same" << endl;

        Test.Check(z.GetStartTime() == x.GetStartTime())
            << "Start time is the same" << endl;

        bool pass = true;
        for (size_t k = 0; k < x.size(); ++k)
        {
            pass = pass && (y.GetValue() == z[k].real());
            pass = pass && (x[k] == z[k].imag());
        }

        Test.Check(pass)
            << "Values are correct" << endl;
    }

    {
        Test.Message() << "complex(x, y)" << endl;

        CallChain cmds;
        Parameters args;
        
        TimeSeries<T> x;
        x.resize(20, 1.0);
        x.SetStartTime(GPSTime(1000, 100));
        x.SetSampleRate(16384.0);

        Sequence<T> y;
        y.resize(20, -1.0);
        
        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");
        
        const TimeSeries<complex<T> >& z
            = dynamic_cast<const TimeSeries<complex<T> >&>(*zUdt);
        
        Test.Check(z.size() == x.size())
            << "Size is the same" << endl;

        Test.Check(z.GetSampleRate() == x.GetSampleRate())
            << "Sample rate is the same" << endl;

        Test.Check(z.GetStartTime() == x.GetStartTime())
            << "Start time is the same" << endl;

        bool pass = true;
        for (size_t k = 0; k < x.size(); ++k)
        {
            pass = pass && (x[k] == z[k].real());
            pass = pass && (y[k] == z[k].imag());
        }

        Test.Check(pass)
            << "Values are correct" << endl;
    }

    {
        Test.Message() << "complex(x, y)" << endl;

        CallChain cmds;
        Parameters args;
        
        TimeSeries<T> x;
        x.resize(20, 1.0);
        x.SetStartTime(GPSTime(1000, 100));
        x.SetSampleRate(16384.0);

        TimeSeries<T> y;
        y.resize(20, -1.0);
        y.SetStartTime(GPSTime(2000, 200));
        y.SetSampleRate(16384.0);
        
        cmds.Reset();

        cmds.AddSymbol("x", x.Clone());
        cmds.AddSymbol("y", y.Clone());

        cmds.AppendCallFunction("complex", args.set(2, "x", "y"), "z");

        cmds.AppendIntermediateResult( "z", "result", "Final Result", "" );
    
        cmds.Execute();

        const udt* const zUdt = cmds.GetSymbol("z");
        
        const TimeSeries<complex<T> >& z
            = dynamic_cast<const TimeSeries<complex<T> >&>(*zUdt);
        
        Test.Check(z.size() == x.size())
            << "Size is the same" << endl;

        Test.Check(z.GetSampleRate() == x.GetSampleRate())
            << "Sample rate is the same" << endl;

        Test.Check(z.GetStartTime() == x.GetStartTime())
            << "Start time is the same" << endl;

        bool pass = true;
        for (size_t k = 0; k < x.size(); ++k)
        {
            pass = pass && (x[k] == z[k].real());
            pass = pass && (y[k] == z[k].imag());
        }

        Test.Check(pass)
            << "Values are correct" << endl;
    }

}

int main(int ArgC, char** ArgV)
{
    Test.Init(ArgC, ArgV);

    try {
        Test.Message() << "Sequence<T>" << endl;
        TestRealSequence<Sequence<complex<float> >, Sequence<float> >();
        TestRealSequence<Sequence<complex<double> >, Sequence<double> >();

        TestComplexSequence<Sequence<complex<float> >, Sequence<complex<float> > >();
        TestComplexSequence<Sequence<complex<double> >, Sequence<complex<double> > >();

        Test.Message() << "TimeSeries<T>" << endl;
        TestRealSequence<TimeSeries<complex<float> >, TimeSeries<float> >();
        TestRealSequence<TimeSeries<complex<double> >, TimeSeries<double> >();
        TestComplexSequence<TimeSeries<complex<float> >, TimeSeries<complex<float> > >();
        TestComplexSequence<TimeSeries<complex<double> >, TimeSeries<complex<double> > >();

        Test.Message() << "FrequencySequence<T>" << endl;
        TestRealSequence<FrequencySequence<complex<float> >, FrequencySequence<float> >();
        TestRealSequence<FrequencySequence<complex<double> >, FrequencySequence<double> >();
        TestComplexSequence<FrequencySequence<complex<float> >, FrequencySequence<complex<float> > >();
        TestComplexSequence<FrequencySequence<complex<double> >, FrequencySequence<complex<double> > >();

        Test.Message() << "TimeBoundedFreqSequence<T>" << endl;
        TestRealSequence<TimeBoundedFreqSequence<complex<float> >, TimeBoundedFreqSequence<float> >();
        TestRealSequence<TimeBoundedFreqSequence<complex<double> >, TimeBoundedFreqSequence<double> >();
        TestComplexSequence<TimeBoundedFreqSequence<complex<float> >, TimeBoundedFreqSequence<complex<float> > >();
        TestComplexSequence<TimeBoundedFreqSequence<complex<double> >, TimeBoundedFreqSequence<complex<double> > >();

        Test.Message() << "CSDSpectrum<T>" << endl;
        TestRealSequence<CSDSpectrum<complex<float> >, CSDSpectrum<float> >();
        TestRealSequence<CSDSpectrum<complex<double> >, CSDSpectrum<double> >();
        TestComplexSequence<CSDSpectrum<complex<float> >, CSDSpectrum<complex<float> > >();
        TestComplexSequence<CSDSpectrum<complex<double> >, CSDSpectrum<complex<double> > >();

        Test.Message() << "WelchCSDSpectrum<T>" << endl;
        TestRealSequence<WelchCSDSpectrum<complex<float> >, WelchCSDSpectrum<float> >();
        TestRealSequence<WelchCSDSpectrum<complex<double> >, WelchCSDSpectrum<double> >();
        TestComplexSequence<WelchCSDSpectrum<complex<float> >, WelchCSDSpectrum<complex<float> > >();
        TestComplexSequence<WelchCSDSpectrum<complex<double> >, WelchCSDSpectrum<complex<double> > >();

        Test.Message() << "WelchSpectrum<T>" << endl;
        TestRealSequence<CSDSpectrum<complex<float> >, WelchSpectrum<float> >();
        TestRealSequence<CSDSpectrum<complex<double> >, WelchSpectrum<double> >();

        Test.Message() << "DFT<T>" << endl;
        TestComplexSequence<DFT<complex<float> >, DFT<complex<float> > >();
        TestComplexSequence<DFT<complex<double> >, DFT<complex<double> > >();

        TestCallChainExcept();
        TestCallChain<float>();
        TestCallChain<double>();
    }
    catch (const std::exception& e) 
    {
	Test.Check(false) << e.what() << std::endl;
    }

    Test.Exit();

    return 0;
}
