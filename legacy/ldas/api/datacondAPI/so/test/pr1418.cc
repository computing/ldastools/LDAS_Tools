//! author="Edward Maros"
// $Id: pr1418.cc,v 1.3 2005/12/01 22:55:02 emaros Exp $

/*
=========================================================================
=========================================================================
*/

#include "datacondAPI/config.h"

#include "general/unittest.h"
#include "general/util.hh"
#include "genericAPI/registry.hh"

#include "token.hh"

#include "CallChain.hh"

General::UnitTest	Test;

const std::string	PR( "PR1418" );

void
pr()
try {
  const ILwd::LdasElement* ilwd( read_ilwd( "H-663711990.ilwd", "DATADIR" ) );

  CallChain  cmds;
  Parameters args;
  std::string	gw( "LIGO::Frame:rawdata:rawData:RawData:Frame::adcdata:Container(AdcData):H2\\:LSC-AS_Q::AdcData:663711990:0:Frame" );
  
  cmds.Reset();

  cmds.IngestILwd( *ilwd );

  // rgw = resample(gw, 1 8);
#if 0
  // cmds.AppendCallFunction("integer", args.set(1, "1 8"), "t1");
  cmds.AppendCallFunction("integer", args.set(1, "1"), "t1");
  cmds.AppendCallFunction("integer", args.set(1, "8"), "t2");
  cmds.AppendCallFunction("resample",
			  args.set(3, gw.c_str(), "t1", "t2"), 
			  "rgw");
#else
  // cmds.AppendCallFunction("integer", args.set(1, "1 8"), "t1");
  cmds.AppendCallFunction("integer", args.set(1, "1 8"), "t1");
  cmds.AppendCallFunction("resample",
			  args.set(2, gw.c_str(), "t1"), 
			  "rgw");
#endif

  // output(rgw, ascii, rgw.ilwd, rgw, resampled gw data);
  cmds.AppendIntermediateResult("rgw",
				"rgw",
				"resampled gw data",
				"frame");
  // output(rgw, ascii, rgw.ilwd, rgw, resampled gw data);
  cmds.AppendIntermediateResult("rgw",
				"rgw",
				"resampled gw data",
				"frame");
  
  cmds.Execute();

  cmds.GetResults();
  Test.Check( true ) << PR << std::endl;
}
catch( const std::exception& e )
{
  Test.Check( false ) << PR << ": " << "Caught exception: " << e.what()
		      << std::endl;
}
catch( ... )
{
  Test.Check( false )
    << PR << ": " << "Caught unknown exception: "
    << std::endl;
}

int
main(int argc, char** argv)
{
  //---------------------------------------------------------------------
  // Do basic setup
  //---------------------------------------------------------------------

  Test.Init(argc, argv);

  //---------------------------------------------------------------------
  // Try what is the problem
  //---------------------------------------------------------------------

  pr();

  //---------------------------------------------------------------------
  // Exit with appropriate exit status
  //---------------------------------------------------------------------

  Test.Exit( );

  //---------------------------------------------------------------------
  // Should never get here, but if we do, consider it a failure.
  //---------------------------------------------------------------------

  return 1;
}
