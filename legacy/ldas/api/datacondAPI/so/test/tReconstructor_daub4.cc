//tReconstructor_daub4.cc

#include "datacondAPI/config.h"

#include <iostream.h>

#include "general/unittest.h"

#include "WaveletPSU.hh"
#include "daub4.hh"
#include "resolver.hh"
#include "result.hh"
#include "reconstructor.hh"

#include "SequenceUDT.hh"

using namespace datacondAPI::psu;

void EasyTest();
void RTest();
void PartReconstruct();

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  EasyTest();

  RTest();

  PartReconstruct();

  Test.Check(true) << "End of test harness reached." << endl; 

  Test.Exit();

} //end main


void EasyTest()
{
  Test.Message() << "Executing EasyTest." << endl;

  datacondAPI::Sequence<double> data(0.0, 4);
  data[0] = 1.0;

  Daub4 daub4wavelet;
  Result<double> easyResult; 

  Resolver easyTest(daub4wavelet, 1);

  Test.Message() << "Performing decomposition (EasyTest)" << endl;
  easyTest.applyFilter(easyResult, data);
  
  datacondAPI::Sequence<double> first(easyResult.getResult(0));

  datacondAPI::Sequence<double> second(easyResult.getResult(1));

  /*
  Test.Message() << "Highpass:" << endl;
  for (int i = 0; i < first.size(); i++)
    Test.Message() << first[i] << endl;
  
  Test.Message() << "Lowpass:" << endl;
  for (int j = 0; j < second.size(); j++)
    Test.Message() << second[j] << endl;
  */

  Reconstructor ercst;
  datacondAPI::Sequence<double> reconstruction;

  Test.Message() << "Performing reconstruction (EasyTest)." << endl;

  string what = "Reconstructor::apply";
  try
    {
      ercst.apply(reconstruction, easyResult);
    }
  catch(std::domain_error& e)
    {
      Test.Check(false) << e.what() << endl;
    }
  catch(...)
    {
      Test.Check(false) << "Caught unexpected exception.";
    }

  //display reconstructed signal
  Test.Message() << "Reconstructed signal: \n";
  for(int i = 0; i < reconstruction.size(); i++)
    Test.Message() << reconstruction[i] << endl;
  /*
  //check reconstructed signal
  Test.Message() << "Expected reconstructed signal: \n";
  for(int pqr = 0; pqr < data.size(); pqr++)
    Test.Message() << data[pqr] << endl;

  //difference between original and reconstructed signals
  datacondAPI::Sequence<double> difference(data - reconstruction);
  Test.Message() << "Difference between original and reconstructed:" << endl;
  for (int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(data == reconstruction);

  bool pass = true;
  for (int i = 0; i < check.size(); i++)
    pass = pass && check[i];
 

  Test.Check(pass) << "Reconstructed signal.\n";
  */

}

void RTest()
{
  Test.Message() << "Executing RTest." << endl;
  Test.Message() << "DECOMPOSITION for full reconstruction:" << endl;
  datacondAPI::Sequence<double> data(0.0, 48);
  data[4] = 1.0;

  Daub4 daub4Wavelet;
  Result<double> testResult;
  Resolver test(daub4Wavelet, 4);

  //  Test.Message()<< "Applying filter." << endl;
  try{
    test.applyFilter(testResult, data);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  //Test.Message() << "Filter applied to data." << endl;


  //check filtered signal
  datacondAPI::Sequence<double> first; 
  first = testResult.getResult(0);
  
  datacondAPI::Sequence<double> second; 
  second = testResult.getResult(1);
  
  datacondAPI::Sequence<double> third; 
  third = testResult.getResult(2); 

  datacondAPI::Sequence<double> fourth;
  fourth = testResult.getResult(3);

  datacondAPI::Sequence<double> lowpass;
  lowpass = testResult.getResult(4);

  
  Test.Message() << "Zeroth detail:" << endl;
  for (int i = 0; i < first.size(); i++)
    Test.Message() << first[i] << endl;
  
  Test.Message() << "First detail:" << endl;
  for (int j = 0; j < second.size(); j++)
    Test.Message() << second[j] << endl;

  Test.Message() << "Second detail:" << endl;
  for (int j = 0; j < third.size(); j++)
    Test.Message() << third[j] << endl;

  Test.Message() << "Third detail:" << endl;
  for (int j = 0; j < fourth.size(); j++)
    Test.Message() << fourth[j] << endl;
  
  Test.Message() << "Final lowpass approximation:" << endl;
  for (int k = 0; k < lowpass.size(); k++)
    Test.Message() << lowpass[k] << endl;
  /*
  //expected output
  datacondAPI::Sequence<double> e_first(0.0, 24);
  e_first[2] = 0.5;
  datacondAPI::Sequence<double> e_second(0.0, 12);
  e_second[1] = 0.25;
  datacondAPI::Sequence<double> e_third(0.0, 6);
  e_third[1] = -0.125;
  datacondAPI::Sequence<double> e_fourth(0.0, 3);
  e_fourth[1] = -0.0625;
  datacondAPI::Sequence<double> e_lowpass(0.0, 3);
  e_lowpass[1] = -0.0625;

 
  Test.Message() << "Expected first detail:" << endl;
  for (int i = 0; i < e_first.size(); i++)
    Test.Message() << e_first[i] << endl;

  Test.Message() << "Expected second detail:" << endl;
  for (int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected third detail:" << endl;
  for (int j = 0; j < e_third.size(); j++)
    Test.Message() << e_third[j] << endl;

  Test.Message() << "Expected fourth detail:" << endl;
  for (int j = 0; j < e_fourth.size(); j++)
    Test.Message() << e_fourth[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;
 
  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_first - first);
  datacondAPI::Sequence<double> second_difference(e_second - second);
  datacondAPI::Sequence<double> third_difference(e_third - third);
  datacondAPI::Sequence<double> fourth_difference(e_fourth - fourth);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - lowpass);

  Test.Message() << "Difference in first detail:" << endl;
  for (int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in second detail:" << endl;
  for (int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in third detail:" << endl;
  for (int j = 0; j < third_difference.size(); j++)
    Test.Message() << third_difference[j] << endl;

  Test.Message() << "Difference in second detail:" << endl;
  for (int j = 0; j < fourth_difference.size(); j++)
    Test.Message() << fourth_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_first(e_first == first);
  std::valarray<bool> check_second(e_second == second);
  std::valarray<bool> check_third(e_third == third);
  std::valarray<bool> check_fourth(e_fourth == fourth);
  std::valarray<bool> check_lowpass(e_lowpass == lowpass);

  bool pass1, pass2, pass3, pass4, pass5;
  pass1 = pass2 = pass3 = pass4 = pass5 = true;

  for (int i = 0; i < check_first.size(); i++)
    pass1 = pass1 && check_first[i];

  for (int i = 0; i < check_second.size(); i++)
    pass2 = pass2 && check_second[i];

  for (int i = 0; i < check_third.size(); i++)
    pass3 = pass3 && check_third[i];  

  for (int i = 0; i < check_fourth.size(); i++)
    pass4 = pass4 && check_fourth[i];

  for (int i = 0; i < check_lowpass.size(); i++)
    pass5 = pass5 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true
     && pass4 == true && pass5 == true)
    overallpass = true;

    Test.Check(overallpass) << "Filtered output." <<endl;
  */


  //Reconstruct the signal
  Test.Message() << "RECONSTRUCTION:" << endl;

  Reconstructor rcst;
  datacondAPI::Sequence<double> reconstruction;

  Test.Message() << "Applying synthesis filters for reconstruction." << endl;
  string what = "Reconstructor::apply method";
  try
    {
      rcst.apply(reconstruction, testResult);
    }
  catch(std::domain_error& d)
    {
      Test.Check(false) << what << " ( " << d.what() << " )"<< endl;
    }

  //check reconstructed signal
  datacondAPI::Sequence<double> original(0.0, 48);
  original[4] = 1.0;

  //display reconstruction
  Test.Message() << "Reconstructed signal:" << endl;
  for(int j = 0; j < reconstruction.size(); j++)
    Test.Message() << reconstruction[j] << endl;

  //difference between original and reconstructed signals
  datacondAPI::Sequence<double> difference(original - reconstruction);
  Test.Message() << "Difference between original and reconstructed:" << endl;
  for (int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(original == reconstruction);

  bool pass = true;
  for (int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Reconstructed signal.\n";

}


void PartReconstruct()
{
  Test.Message() << "DECOMPOSITION for partial reconstruction:" << endl;
  datacondAPI::Sequence<double> original(0.0, 24);
  original[3] = 1.0;

  Daub4 daub4Wavelet2;
  Result<double> testResult2;
  Resolver test2(daub4Wavelet2, 3);

  //Test.Message()<< "Applying filter." << endl;
  try{
    test2.applyFilter(testResult2, original);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  
  //decomposed signal
  datacondAPI::Sequence<double> zero = testResult2.getResult(0);
  
  datacondAPI::Sequence<double> one = testResult2.getResult(1);
  
  datacondAPI::Sequence<double> two = testResult2.getResult(2); 
  
  datacondAPI::Sequence<double> approx = testResult2.getResult(3);

  Test.Message() << "Zeroth detail:" << endl;
  for (int i = 0; i < zero.size(); i++)
    Test.Message() << zero[i] << endl;
  
  Test.Message() << "First detail:" << endl;
  for (int j = 0; j < one.size(); j++)
    Test.Message() << one[j] << endl;
  
  Test.Message() << "Second detail:" << endl;
  for (int j = 0; j < two.size(); j++)
    Test.Message() << two[j] << endl;
  
  Test.Message() << "Approximation:" << endl;
  for (int j = 0; j < approx.size(); j++)
    Test.Message() << approx[j] << endl;

  Reconstructor rcst;
  datacondAPI::Sequence<double> reconstruction;
  int level = 1;

  datacondAPI::Sequence<double> testseq = testResult2.getResult(level);

  /*
  cout << "input from level " << level << " to be reconstructed:" << endl;
  for(int y = 0; y < testseq.size(); y++)
    cout << testseq[y] << endl;
  */

  Test.Message() << "PARTIAL RECONSTRUCTION from level " << level << endl;

  string what = "Overloaded Reconstructor::apply method";
  try
    {
      rcst.apply(reconstruction, testResult2, level);
    }
  catch(std::invalid_argument& a)
    {
      Test.Check(false) << what << " (" << a.what() << " )" << endl;
    }
  catch(...)
    {
      Test.Check(false) << what << "(Caught unexpected exception.)" << endl;
    }


  //display partially reconstructed signal
  Test.Message() << "Partially reconstructed signal: " << endl;
  for(int t = 0; t < reconstruction.size(); t++)
    Test.Message() << reconstruction[t] << endl;

  //expected signals from different levels of reconstruction:
  /*
  //check partially reconstructed signal -> level = 0
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[3] = 0.5;
  expected[4] = -0.5;
  */

  /*
  //check partially reconstructed signal -> level = 1
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = -0.25;
  expected[3] = expected[4] = 0.25;
  */

  /*
  //check partially reconstructed signal -> level = 2
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = expected[3] = expected[4] = 0.125;
  expected[5] = expected[6] = expected[7] = expected[8] = -0.125; 
  */

  /*
  //check partially reconstructed signal -> level = 3
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = expected[3] = expected[4] = 0.125;
  expected[5] = expected[6] = expected[7] = expected[8] = 0.125; 
  */

  /*
   //difference between expected and reconstructed signals
  datacondAPI::Sequence<double> difference(expected - reconstruction);
  Test.Message() << "Difference = (expected - reconstructed):" << endl;
  for (int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(expected == reconstruction);

  bool pass = true;
  for (int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Partially reconstructed signal.\n";
  */

}
