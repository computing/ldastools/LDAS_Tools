#include "datacondAPI/config.h"

#include <valarray>

#include "general/unittest.h"	// Needed for doing "make check"

#include <filters/LDASConstants.hh>	// Constants
#include "token.hh"		// Common header for token testing programs.
// #include "Mixer.hh"		// Needed for computing mix expected results.
#include "ScalarUDT.hh"		// Needed for converting CallChain results.
#include "SequenceUDT.hh"	// Needed for converting CallChain results.
#include "DFTUDT.hh"		// Needed for testing results

General::UnitTest	Test;	// Class supporting testing

int
main(int ArgC, char** ArgV)
{
  using namespace datacondAPI;

  //---------------------------------------------------------------------
  // Initialize UnitTest class
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  Test.Message() << "real and imag: start\n";

  try
  {
    //-------------------------------------------------------------------
    // Perform tests
    //-------------------------------------------------------------------

    CallChain cmds;
    Parameters args;

    cmds.AppendCallFunction("integer", args.set(1, "10"), "i");
    cmds.AppendCallFunction("double", args.set(1, "-1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "1", "2"), "cf");
    cmds.AppendCallFunction("dcomplex", args.set(2, "1", "2"), "cd");
    cmds.AppendCallFunction("svalarray", args.set(2, "d", "i"), "vf");
    cmds.AppendCallFunction("dvalarray", args.set(2, "d", "i"), "vd");
    cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "i"), "vcf");
    cmds.AppendCallFunction("zvalarray", args.set(2, "cd", "i"), "vcd");
    cmds.AppendCallFunction("sfvalarray", args.set(2, "d", "i"), "fvf");
    cmds.AppendCallFunction("dfvalarray", args.set(2, "d", "i"), "fvd");

    cmds.AppendCallFunction("double", args.set(1, "2"), "two");
    cmds.AppendCallFunction("svalarray", args.set(2, "two", "i"), "sf");
    cmds.AppendCallFunction("dvalarray", args.set(2, "two", "i"), "sd");

    cmds.AppendCallFunction("real", args.set(1, "i"), "ri");
    cmds.AppendCallFunction("imag", args.set(1, "i"), "ii");
    cmds.AppendCallFunction("real", args.set(1, "d"), "rd");
    cmds.AppendCallFunction("imag", args.set(1, "d"), "id");
    cmds.AppendCallFunction("real", args.set(1, "cf"), "rcf");
    cmds.AppendCallFunction("imag", args.set(1, "cf"), "icf");
    cmds.AppendCallFunction("real", args.set(1, "cd"), "rcd");
    cmds.AppendCallFunction("imag", args.set(1, "cd"), "icd");
    cmds.AppendCallFunction("real", args.set(1, "vf"), "rvf");
    cmds.AppendCallFunction("imag", args.set(1, "vf"), "ivf");
    cmds.AppendCallFunction("real", args.set(1, "vd"), "rvd");
    cmds.AppendCallFunction("imag", args.set(1, "vd"), "ivd");
    cmds.AppendCallFunction("real", args.set(1, "vcf"), "rvcf");
    cmds.AppendCallFunction("imag", args.set(1, "vcf"), "ivcf");
    cmds.AppendCallFunction("real", args.set(1, "vcd"), "rvcd");
    cmds.AppendCallFunction("imag", args.set(1, "vcd"), "ivcd");
    cmds.AppendCallFunction("real", args.set(1, "fvf"), "rfvf");
    cmds.AppendCallFunction("imag", args.set(1, "fvf"), "ifvf");
    cmds.AppendCallFunction("real", args.set(1, "fvd"), "rfvd");
    cmds.AppendCallFunction("imag", args.set(1, "fvd"), "ifvd");

    cmds.AppendCallFunction("abs", args.set(1, "i"), "ai");
    cmds.AppendCallFunction("abs", args.set(1, "d"), "ad");
    cmds.AppendCallFunction("abs", args.set(1, "cf"), "acf");
    cmds.AppendCallFunction("abs", args.set(1, "cd"), "acd");
    cmds.AppendCallFunction("abs", args.set(1, "vf"), "avf");
    cmds.AppendCallFunction("abs", args.set(1, "vd"), "avd");
    cmds.AppendCallFunction("abs", args.set(1, "vcf"), "avcf");
    cmds.AppendCallFunction("abs", args.set(1, "vcd"), "avcd");
    cmds.AppendCallFunction("abs", args.set(1, "fvf"), "afvf");
    cmds.AppendCallFunction("abs", args.set(1, "fvd"), "afvd");
    
    cmds.AppendCallFunction("arg", args.set(1, "i"), "pi");
    cmds.AppendCallFunction("arg", args.set(1, "d"), "pd");
    cmds.AppendCallFunction("arg", args.set(1, "cf"), "pcf");
    cmds.AppendCallFunction("arg", args.set(1, "cd"), "pcd");
    cmds.AppendCallFunction("arg", args.set(1, "vf"), "pvf");
    cmds.AppendCallFunction("arg", args.set(1, "vd"), "pvd");
    cmds.AppendCallFunction("arg", args.set(1, "vcf"), "pvcf");
    cmds.AppendCallFunction("arg", args.set(1, "vcd"), "pvcd");
    cmds.AppendCallFunction("arg", args.set(1, "fvf"), "pfvf");
    cmds.AppendCallFunction("arg", args.set(1, "fvd"), "pfvd");

    cmds.AppendCallFunction("conj", args.set(1, "i"), "c_i");
    cmds.AppendCallFunction("conj", args.set(1, "d"), "c_d");
    cmds.AppendCallFunction("conj", args.set(1, "cf"), "ccf");
    cmds.AppendCallFunction("conj", args.set(1, "cd"), "ccd");
    cmds.AppendCallFunction("conj", args.set(1, "vf"), "cvf");
    cmds.AppendCallFunction("conj", args.set(1, "vd"), "cvd");
    cmds.AppendCallFunction("conj", args.set(1, "vcf"), "cvcf");
    cmds.AppendCallFunction("conj", args.set(1, "vcd"), "cvcd");

    cmds.AppendCallFunction("double", args.set(1, "i"), "double_i");
    cmds.AppendCallFunction("double", args.set(1, "d"), "double_d");
    cmds.AppendCallFunction("double", args.set(1, "cf"), "double_cf");
    cmds.AppendCallFunction("double", args.set(1, "cd"), "double_cd");
    cmds.AppendCallFunction("double", args.set(1, "vf"), "double_vf");
    cmds.AppendCallFunction("double", args.set(1, "vd"), "double_vd");
    cmds.AppendCallFunction("double", args.set(1, "vcf"), "double_vcf");
    cmds.AppendCallFunction("double", args.set(1, "vcd"), "double_vcd");

    cmds.AppendIntermediateResult("double_vcd", "result", "Final Result", "" );

    cmds.Execute();

    int ri = 10;
    int ii = 0;
    double rd = -1;
    double id = 0;
    float rcf = 1;
    float icf = 2;
    double rcd = 1;
    double icd = 2;
    std::valarray<float> rvf(-1.0, 10);
    std::valarray<float> ivf(0.0, 10);
    std::valarray<double> rvd(-1.0, 10);
    std::valarray<double> ivd(0.0, 10);
    std::valarray<float> rvcf(1.0, 10);
    std::valarray<float> ivcf(2.0, 10);
    std::valarray<double> rvcd(1.0, 10);
    std::valarray<double> ivcd(2.0, 10);
    std::valarray<float> rfvf(-1.0, 10);
    std::valarray<float> ifvf(0.0, 10);
    std::valarray<double> rfvd(-1.0, 10);
    std::valarray<double> ifvd(0.0, 10);

    int ai = 10;
    double ad = 1;
    float acf = abs(std::complex<float>(1,2));
    double acd = abs(std::complex<double>(1,2));
    std::valarray<float>  avf(1,10);
    std::valarray<double> avd(1,10);
    std::valarray<float>  avcf(abs(std::complex<float>(1,2)),10);
    std::valarray<double> avcd(abs(std::complex<double>(1,2)),10);
    std::valarray<float>  afvf(1,10);
    std::valarray<double> afvd(1,10);

    int pi = 0;
    double pd = 0;
    float pcf = arg(std::complex<float>(1,2));
    double pcd = arg(std::complex<double>(1,2));
    std::valarray<float> pvf(0.0,10);
    std::valarray<double> pvd(0.0,10);
    std::valarray<float> pvcf(arg(std::complex<float>(1,2)),10);
    std::valarray<double> pvcd(arg(std::complex<double>(1,2)),10);

    std::valarray<float> pfvf(LDAS_PI, 10);
    std::valarray<double> pfvd(LDAS_PI, 10);

    int c_i = 10;
    double c_d = -1;
    std::complex<float> ccf = conj(std::complex<float>(1,2));
    std::complex<double> ccd = conj(std::complex<double>(1,2));
    std::valarray<float> cvf(-1.0, 10);
    std::valarray<double> cvd(-1.0, 10);
    std::valarray<std::complex<float> > cvcf(std::complex<float>(1,2),10);
    std::valarray<std::complex<double> > cvcd(std::complex<double>(1,2),10);

    for (size_t k = 0; k < cvcf.size(); ++k)
    {
	cvcf[k] = conj(cvcf[k]);
    }

    for (size_t k = 0; k < cvcd.size(); ++k)
    {
	cvcd[k] = conj(cvcd[k]);
    }

    check_value(ri, *cmds.GetSymbol("ri"), "real(integer)");
    check_value(ii, *cmds.GetSymbol("ii"), "imag(integer)");
    check_value(rd, *cmds.GetSymbol("rd"), "real(double)");
    check_value(id, *cmds.GetSymbol("id"), "imag(double)");
    check_value(rcf, *cmds.GetSymbol("rcf"), "real(complex<float>)");
    check_value(icf, *cmds.GetSymbol("icf"), "imag(complex<float>)");
    check_value(rcd, *cmds.GetSymbol("rcd"), "real(complex<double>)");
    check_value(icd, *cmds.GetSymbol("icd"), "imag(complex<double>)");
    check_valarray(rvf, cmds.GetSymbol("rvf"), "real(valarray<float>)");
    check_valarray(ivf, cmds.GetSymbol("ivf"), "imag(valarray<float>)");
    check_valarray(rvd, cmds.GetSymbol("rvd"), "real(valarray<double>)");
    check_valarray(ivd, cmds.GetSymbol("ivd"), "imag(valarray<double>)");
    check_valarray(rvcf, cmds.GetSymbol("rvcf"), "real(valarray<complex<float>>)");
    check_valarray(ivcf, cmds.GetSymbol("ivcf"), "imag(valarray<complex<float>>)");
    check_valarray(rvcd, cmds.GetSymbol("rvcd"), "real(valarray<complex<double>>)");
    check_valarray(ivcd, cmds.GetSymbol("ivcd"), "imag(valarray<complex<double>>)");
    check_valarray(rfvf, cmds.GetSymbol("rfvf"), "real(fvalarray<float>)");
    check_valarray(ifvf, cmds.GetSymbol("ifvf"), "imag(fvalarray<float>)");
    check_valarray(rfvd, cmds.GetSymbol("rfvd"), "real(fvalarray<double>)");
    check_valarray(ifvd, cmds.GetSymbol("ifvd"), "imag(fvalarray<double>)");

    check_value(ai, *cmds.GetSymbol("ai"), "abs(integer)");
    check_value(ad, *cmds.GetSymbol("ad"), "abs(double)");
    check_value(acf, *cmds.GetSymbol("acf"), "abs(complex<float>)");
    check_value(acd, *cmds.GetSymbol("acd"), "abs(complex<double>)");
    check_valarray(avf, cmds.GetSymbol("avf"), "abs(valarray<float>)");
    check_valarray(avd, cmds.GetSymbol("avd"), "abs(valarray<double>)");
    check_valarray(avcf, cmds.GetSymbol("avcf"), "abs(valarray<complex<float>>)");
    check_valarray(avcd, cmds.GetSymbol("avcd"), "abs(valarray<complex<double>>)");
    check_valarray(afvf, cmds.GetSymbol("afvf"), "abs(fvalarray<float>)");
    check_valarray(afvd, cmds.GetSymbol("afvd"), "abs(fvalarray<double>)");

    check_value(pi, *cmds.GetSymbol("pi"), "arg(integer)");
    check_value(pd, *cmds.GetSymbol("pd"), "arg(double)");
    check_value(pcf, *cmds.GetSymbol("pcf"), "arg(complex<float>)");
    check_value(pcd, *cmds.GetSymbol("pcd"), "arg(complex<double>)");
    check_valarray(pvf, cmds.GetSymbol("pvf"), "arg(valarray<float>)");
    check_valarray(pvd, cmds.GetSymbol("pvd"), "arg(valarray<double>)");
    check_valarray(pvcf, cmds.GetSymbol("pvcf"), "arg(valarray<complex<float>>)");
    check_valarray(pvcd, cmds.GetSymbol("pvcd"), "arg(valarray<complex<double>>)");
    check_valarray(pfvf, cmds.GetSymbol("pfvf"), "arg(fvalarray<float>)");
    check_valarray(pfvd, cmds.GetSymbol("pfvd"), "arg(fvalarray<double>)");

    // conj

    check_value(c_i, *cmds.GetSymbol("c_i"), "conj(integer)");
    check_value(c_d, *cmds.GetSymbol("c_d"), "conj(double)");
    check_value(ccf, *cmds.GetSymbol("ccf"), "conj(complex<float>)");
    check_value(ccd, *cmds.GetSymbol("ccd"), "conj(complex<double>)");
    check_valarray(cvf, cmds.GetSymbol("cvf"), "conj(valarray<float>)");
    check_valarray(cvd, cmds.GetSymbol("cvd"), "conj(valarray<double>)");
    check_valarray(cvcf, cmds.GetSymbol("cvcf"), "conj(valarray<complex<float>>)");
    check_valarray(cvcd, cmds.GetSymbol("cvcd"), "conj(valarray<complex<double>>)");

    // double

      check_value(10.0, *cmds.GetSymbol("double_i"), "double(integer)");
      check_value(-1.0, *cmds.GetSymbol("double_d"), "double(double)");
      check_value(std::complex<double>(1.0, 2.0), *cmds.GetSymbol("double_cf"), "double(complex<float>)");
      check_value(std::complex<double>(1.0, 2.0), *cmds.GetSymbol("double_cd"), "double(complex<double>)");
      check_valarray(std::valarray<double>(-1.0, 10), cmds.GetSymbol("double_vf"), "double(valarray<float>)");
      check_valarray(std::valarray<double>(-1.0, 10), cmds.GetSymbol("double_vd"), "double(valarray<double>)");
      check_valarray(std::valarray<std::complex<double> >(std::complex<double>(1.0, 2.0), 10), cmds.GetSymbol("double_vcf"), "double(valarray<complex<float>>)");
      check_valarray(std::valarray<std::complex<double> >(std::complex<double>(1.0, 2.0), 10), cmds.GetSymbol("double_vcd"), "double(valarray<complex<double>>)");

  }

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Terminate program with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Message() << "real and imag: end\n";
  Test.Exit();
}
