//! author="Edward Maros"
// Test code for timing functions
// $Id: pr374.cc,v 1.12 2005/12/01 22:55:02 emaros Exp $

#include "datacondAPI/config.h"

#include <iostream>
#include <sstream>   

#include "general/unittest.h"

#include "genericAPI/registry.hh"

#include "token.hh"
#include "CallChain.hh"

using namespace std;   
General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
    int main(int argc, char** argv);
#ifdef __cplusplus
}
#endif	/* __cplusplus */

void pr374(int Length, int Index)
{
    ostringstream	length;
    ostringstream	index;

    COMPLEX_16* c = new COMPLEX_16[Length];
    ILwd::LdasArray<COMPLEX_16> idata(c, Length);
    if (c != 0) delete [] c;

    idata.setNameString("idata");
    idata.setComment("sample data");

    length << Length;
    index << Index;

    CallChain	cmds;
    Parameters	args;
  
    cmds.Reset();
    cmds.IngestILwd(idata);
  
    cmds.AppendCallFunction("integer", args.set(1, length.str().c_str()), "length");
    cmds.AppendCallFunction("integer", args.set(1, index.str().c_str()), "index");
    cmds.AppendCallFunction("integer", args.set(1, "0"), "i0");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "i1");
  
    cmds.AppendCallFunction("slice",
			    args.set(4, "idata", "i0", "length", "i1"), 
			    "x");
    cmds.AppendCallFunction("real",
			    args.set(1, "x"), 
			    "rx");
    cmds.AppendCallFunction("size",
			    args.set(1, "rx"), 
			    "n");
    cmds.AppendIntermediateResult("",
				  "n",
				  "length of input data before FFT",
				  "generic");
    cmds.AppendCallFunction("fft",
			    args.set(1, "x"), 
			    "y");

    try {

      cmds.Execute();
  
    }
    catch(...)
    {
      ILwd::LdasElement*	results(cmds.GetResults());
      ostringstream		os;
      
      if (results)
      {
	results->write(os, ILwd::ASCII);
	Test.Message() << os.str() << endl << flush;
      }
      throw;
    }
    ILwd::LdasElement*	results(cmds.GetResults());
    ostringstream	os;
      
    if (results)
    {
      results->write(os, ILwd::ASCII);
      Test.Message() << os.str() << endl << flush;
    }

    Registry::elementRegistry.reset(); // This removes the results!
}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);
    size_t last(10);
    size_t size = 2;
    for (size_t y = 2; y <= last; y++)
    {
	size *= 2;
	Test.Message() << "Testing size: 2^" << y << " (" << size << ")"
		       << "[ sizeof COMPLEX_16: " << sizeof(COMPLEX_16) << " ]"
		       << endl;
	try {
	    for (size_t i = 0; i < 5; i++)
	    {
		pr374(size, size/2);
	    }
	    Test.Check(true) << "Size = 2^" << y
			     << " (" << size << ")" << endl;
	}
	CATCH(Test);
    }
    Test.Exit();
    //---------------------------------------------------------------------
    // Should never get here, but if we do, consider it a failure.
    //---------------------------------------------------------------------
    return 1;
}
