#include "datacondAPI/config.h"

#include <complex>

#include "general/unittest.h"	// Needed for doing "make check"

#include "token.hh"		// Common header for token testing programs.
#include "ScalarUDT.hh"	// Needed for converting CallChain results.
#include "TimeSeries.hh"	// Needed for converting CallChain results.


General::UnitTest Test;	// Class supporting testing
using namespace datacondAPI;
using std::complex;
using std::endl;

template<class T, class TCoeffs>
void testFiltFilt()
{
    const size_t n = 10;
    TimeSeries<T> x;
    x.resize(n);
    for (size_t k = 0; k < x.size(); ++k)
    {
        x[k] = k;
    }


    T ydata[] = {
        1.437500000000000e+00,
        4.250000000000000e+00,
        8.000000000000000e+00,
        1.200000000000000e+01,
        1.600000000000000e+01,
        2.000000000000000e+01,
        2.400000000000000e+01,
        2.800000000000000e+01,
        2.731250000000000e+01,
        1.675000000000000e+01
    };

    T zdata[] = {
        3.940792083740234e-01,
        1.836841583251953e+00,
        3.513816833496094e+00,
        5.378616333007812e+00,
        7.039642333984375e+00,
        9.022277832031250e+00,
        1.040466308593750e+01,
        1.296606445312500e+01,
        1.318017578125000e+01,
        1.161132812500000e+01
    };

    TimeSeries<T> yy;
    yy.resize(n);
    for (size_t k = 0; k < yy.size(); ++k)
    {
        yy[k] = ydata[k];
    }

    TimeSeries<T> zz;
    zz.resize(n);
    for (size_t k = 0; k < zz.size(); ++k)
    {
        zz[k] = zdata[k];
    }

    Sequence<TCoeffs> b(3);
    Sequence<TCoeffs> a(2);

    b[0] = 1.0;
    b[1]= 0.75;
    b[2]= 0.25;

    a[0] = 1.0;
    a[1] = 0.5;

    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.ResetOrAddSymbol("b", b.Clone());
    cmds.ResetOrAddSymbol("a", a.Clone());
    cmds.ResetOrAddSymbol("x", x.Clone());

    // y = filtfilt(b, x);
    cmds.AppendCallFunction("filtfilt", args.set(2, "b", "x"),
                            "y");

    // z = filtfilt(b, a, x);
    cmds.AppendCallFunction("filtfilt", args.set(3, "b", "a", "x"),
                            "z");
    
    cmds.AppendIntermediateResult("y", "result", "Final Result", "");
    
    cmds.Execute();
    
    Test.Check(true) << "CallChain executed" << std::endl;

    const udt* const yUdt = cmds.GetSymbol("y");
    
    const TimeSeries<T>& y
        = dynamic_cast<const TimeSeries<T>&>(*yUdt);

    bool pass = true;
    for (size_t k = 0; k < y.size(); ++k)
    {
        pass = pass && (std::abs(y[k] - yy[k]) < 1.0e-12);
    }

    Test.Check(pass)
        << "Sequence filtered correctly" << std::endl;


    const udt* const zUdt = cmds.GetSymbol("z");
    
    const TimeSeries<T>& z
        = dynamic_cast<const TimeSeries<T>&>(*zUdt);

    pass = true;
    for (size_t k = 0; k < z.size(); ++k)
    {
        pass = pass && (std::abs(z[k] - zz[k]) < 1.0e-12);
    }

    Test.Check(pass)
        << "Sequence filtered correctly" << std::endl;

}

int
main(int ArgC, char** ArgV)
{
  using namespace datacondAPI;

  Test.Init(ArgC, ArgV);

  try {
      testFiltFilt<float, float>();
      testFiltFilt<float, double>();
      
      testFiltFilt<double, float>();
      testFiltFilt<double, double>();
      
      testFiltFilt<complex<float>, float >();
      testFiltFilt<complex<float>, double >();
      
      testFiltFilt<complex<double>, float>();
      testFiltFilt<complex<double>, double>();
  }
  catch(const std::exception& e)
  {
      Test.Check(false) << "Caught exception: " << e.what() << endl;
  }

  Test.Message() << "reverse: end\n";
  Test.Exit();
}
