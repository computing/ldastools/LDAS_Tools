//! author="Edward Maros"
// Test code for timing functions
// $Id: pr838.cc,v 1.5 2005/12/01 22:55:02 emaros Exp $

#include "datacondAPI/config.h"

#include <iostream>
#include <sstream>   

#include "general/unittest.h"

#include "token.hh"
#include "CallChain.hh"

#include <genericAPI/registry.hh>

using namespace std;
   
General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
    int main(int argc, char** argv);
#ifdef __cplusplus
}
#endif	/* __cplusplus */

void pr838(int Length, int Index)
{
    ostringstream	length;
    ostringstream	index;

    COMPLEX_16* c = new COMPLEX_16[Length];
    ILwd::LdasArray<COMPLEX_16> idata(c, Length);
    if (c != 0) delete [] c;

    idata.setNameString("idata");
    idata.setComment("sample data");

    length << Length;
    index << Index;

    CallChain	cmds;
    Parameters	args;
  
    cmds.Reset();

    cmds.SetReturnTarget("wrapper");

    cmds.IngestILwd(idata);
  
    cmds.AppendCallFunction("integer", args.set(1, length.str().c_str()), "length");
    cmds.AppendCallFunction("integer", args.set(1, index.str().c_str()), "index");
    cmds.AppendCallFunction("integer", args.set(1, "0"), "i0");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "i1");
  
    cmds.AppendCallFunction("slice",
			    args.set(4, "idata", "i0", "length", "i1"), 
			    "x");
    cmds.AppendCallFunction("psd",
			    args.set(1, "x"),
			    "p");
    cmds.AppendIntermediateResult("",
				  "p",
				  "PSD of slice - metadata format",
				  "metadata");
    cmds.AppendIntermediateResult("",
				  "p",
				  "PSD of slice - wrapper format (wrapper)",
				  "wrapper");
    cmds.AppendIntermediateResult("",
				  "p",
				  "PSD of slice - wrapper format",
				  "wrapper");
    cmds.AppendIntermediateResult("",
				  "p",
				  "PSD of slice - generic format",
				  "generic");

    cmds.Execute();
  
    ILwd::LdasContainer*	e;
    e = dynamic_cast<ILwd::LdasContainer*>(cmds.GetResults());

    e->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;

    Registry::elementRegistry.reset(); // This removes the results!
}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);
    size_t size;
    size = 20;
    Test.Message() << "Testing size: output formatting"
		   << endl;
    try {
        pr838(size, size/2);
        Test.Check(true) << endl;
    }
    catch(bad_alloc& e)
    {
	Test.Check(true)  << ": caught bad_alloc" << endl;
    }
    catch(exception& e)
    {
	Test.Check(false) << ": caught exception " << e.what() << endl;
    }
    catch(...)
    {
	Test.Check(false) << ": caught unexpected!" << endl;
    }
    Test.Exit();
    //---------------------------------------------------------------------
    // Should never get here, but if we do, consider it a failure.
    //---------------------------------------------------------------------
    return 1;
}
