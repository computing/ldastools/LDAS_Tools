#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "Kalman.hh"
#include "KalmanState.hh"

using namespace std;   
using namespace datacondAPI;

General::UnitTest Test;

void Exceptions()
{
	const int M = 4;
	const int N = 3;
	
	const Matrix<double> A(M,M);
	const Matrix<double> W(M,M);
	const Matrix<double> C(N,M);
	const Matrix<double> V(N,N);
	const Sequence<double> psi(M);
	const Matrix<double> P(M,M);

	bool pass = false;	

	string what = "Kalman constructor using Matricies and a Sequence";

	try{	
		Kalman test(A,W,C,V,psi,P);
		Test.Check(true) << what << endl;
	}
	catch( exception& u)
	{
		Test.Check(false) 
	        << what 
                << "caught unexpected std::exception ("
                << u.what()
                << ")"    
                << std::endl;
        }
	what = "Dimensions of matrix A are invalid";
	try{
		const Matrix<double> badA(N,N);
		Kalman testA(badA, W, C, V, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
		pass = true;
	}
	try{
		const Matrix<double> badA(M,N);
		Kalman testA(badA, W, C, V, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badA(N,M);
		Kalman testA(badA, W, C, V, psi, P);
		pass = false;
                Test.Check(pass) << what << std::endl;
	}
	catch(invalid_argument& i)
	{
		Test.Check(pass) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected std::exception (" 
			<< u.what()
			<< ")"                                              
			<< std::endl;
	}
	catch (...)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected exception"
			<< std::endl;
	}


	what = "Dimensions of matrix W are invalid";
	try{
		const Matrix<double> badW(N,N);
		Kalman testW(A, badW, C, V, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
		pass = true;
	}
	try{
		const Matrix<double> badW(M,N);
		Kalman testW(A, badW, C, V, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badW(N,M);
		Kalman testW(A, badW, C, V, psi, P);
		pass = false;
		Test.Check(pass) << what << std::endl;

	}
	catch(invalid_argument& i)
	{
		Test.Check(pass) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected std::exception (" 
			<< u.what()
			<< ")"                                              
			<< std::endl;
	}
	catch (...)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected exception"
			<< std::endl;
	}

	pass = true;
	what = "Dimensions of matrix C are invalid";
	try{
		const Matrix<double> badC(M,N);
		Kalman testC(A, W, badC, V, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badC(N,N);
		Kalman testC(A, W, badC, V, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badC(M,M);
		Kalman testC(A, W, badC, V, psi, P);
		pass = false;
		Test.Check(pass) << what << std::endl;
	}
	catch(invalid_argument& i)
	{
		Test.Check(pass) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected std::exception (" 
			<< u.what()
			<< ")"                                              
			<< std::endl;
	}
	catch (...)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected exception"
			<< std::endl;
	}

	pass = true;
	what = "Dimensions of matrix V are invalid";
	try{
		const Matrix<double> badV(M,M);
		Kalman testV(A, W, C, badV, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badV(N,M);
		Kalman testV(A, W, C, badV, psi, P);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badV(M,N);
		Kalman testV(A, W, C, badV, psi, P);
		pass = false;
		Test.Check(pass) << what << std::endl;
	}
	catch(invalid_argument& i)
	{
		Test.Check(pass) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected std::exception (" 
			<< u.what()
			<< ")"                                              
			<< std::endl;
	}
	catch (...)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected exception"
			<< std::endl;
	}

	pass = true;
	what = "Dimensions of matrix psi are invalid";
	try{
		const Sequence<double> badpsi(N);
		Kalman testPSI(A, W, C, V, badpsi, P);
		pass = false;
		Test.Check(pass) << what << std::endl;
	}
	catch(invalid_argument& i)
	{
		Test.Check(pass) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected std::exception (" 
			<< u.what()
			<< ")"                                              
			<< std::endl;
	}
	catch (...)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected exception"
			<< std::endl;
	}

	pass = true;
	what = "Dimensions of matrix P are invalid";
	try{
		const Matrix<double> badP(N,N);
		Kalman testP(A, W, C, V, psi, badP);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badP(M,N);
		Kalman testP(A, W, C, V, psi, badP);
		pass = false;
	}
	catch(invalid_argument& i)
	{
	}
	try{
		const Matrix<double> badP(N,M);
		Kalman testP(A, W, C, V, psi, badP);
		pass = false;
		Test.Check(pass) << what << std::endl;	
	}
	catch(invalid_argument& i)
	{
		Test.Check(pass) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected std::exception (" 
			<< u.what()
			<< ")"                                              
			<< std::endl;
	}
	catch (...)
	{
		Test.Check(false) 
			<< what 
			<< "caught unexpected exception"
			<< std::endl;
	}

	Sequence<double> a;
	udt* test = &a;
	Kalman testAcc(A, W, C, V, psi, P);
	try{
		what = "Test getA accessor exception";
		testAcc.getA(test);
		Test.Check(false) << what << std::endl;
	}
	catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	try{
                what = "Test getW accessor exception";
                testAcc.getW(test);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	try{
                what = "Test getC accessor exception";
                testAcc.getC(test);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	try{
                what = "Test getV accessor exception";
                testAcc.getV(test);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	try{
                Matrix<double> b;
		    udt* psitest = &b;
		    what = "Test getPSI accessor exception";
                testAcc.getPsi(psitest);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	try{
                what = "Test getP accessor exception";
                testAcc.getP(test);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	what = "Out type does not match in type";
	Kalman testApply(A,W,C,V,psi,P);
	Matrix<double> in(M,M);  
	try{
		Sequence<double> a;
	        udt* pout = &a;
		testApply.apply(pout,in);
		Test.Check(false) << what << std::endl;
	}
	catch(invalid_argument& i)
	{
		Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
	}
	catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	try{
		Matrix<double> a;
		Sequence<double> b;
		udt* out = &b;
	        udt* P = &a;
        	udt* psi = &a;
		testApply.apply(out,in, psi, P);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	what = "output type for psi does not agree with input type";
	try{
		Matrix<double> a;
		Sequence<double> b;
                udt* out = &a;
                udt* P = &a;
                udt* psi = &b;
                testApply.apply(out,in, psi, P);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }

	what = "output type for P does not agree with input type";
        try{
                Matrix<double> a;
		Sequence<double> b;
		udt* out = &a;
                udt* P = &b;
                udt* psi = &a;
                testApply.apply(out,in, psi, P);
		Test.Check(false) << what << std::endl;
        }
        catch(invalid_argument& i)
        {
                Test.Check(true) << what << " (" << i.what() << ")" << std::endl;
        }
        catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
        catch (...)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected exception"
                        << std::endl;
        }
}//Exceptions()

void Accessors()
{
	int M = 2;
	int N = 3;
	
	valarray<double> m(M*M);//{1,2,3,4}
	for(unsigned int i=0; i<m.size(); i++)
		m[i] = i+1;
	valarray<double> n(N*N); // {9,8,7,6,5,4,3,2,1};
	for(unsigned int i=0; i<n.size(); i++)
		n[i] = n.size() -i;
	valarray<double> nm(N*M);// = {0,2,4,6,8,10);
	for(unsigned int i=0; i<nm.size(); i++)
		nm[i] = i*2;
	valarray<double> otherm(M);// = {11,12};
	for(unsigned int i=0; i<otherm.size(); i++)
		otherm[i] = 11+i;

	const Matrix<double> A(m,M,M);
	const Matrix<double> W(m,M,M);
	const Matrix<double> C(nm,N,M);
	const Matrix<double> V(n,N,N);
	const Sequence<double> psi(otherm);
	const Matrix<double> P(m,M,M);

	bool pass=true;
	string what ="Nominal A accessor";
	try{
		Kalman test(A,W,C,V,psi,P);
		valarray<double> garbage(2.5,4);
		Matrix<double> a(garbage,2,2);
		udt* result= &a;
		valarray<double> output (0.,M*M);
		
		test.getA(result);
		a.getData(output);
		for(int i=0; i<M*M; i++)
			pass = (output[i] == m[i]) && pass;
		Test.Check(pass) << what << std::endl;

		what = "Nominal W accessor";
		pass = true;
		test.getW(result);
                a.getData(output);
		for(int i=0; i<M*M;i++)
			pass = (output[i] == m[i])&& pass;
		Test.Check(pass) << what << std::endl;

		what = "Nominal C accessor";
		pass = true;
		test.getC(result);
                a.getData(output);
		for(int i=0; i<M*N;i++)
			pass = (output[i] == nm[i]) && pass;
		Test.Check(pass) << what << std::endl;

		what = "Nominal V accessor";
		pass = true;
		test.getV(result);
                a.getData(output);
		for(int i=0; i<N*N;i++)
			pass = (output[i] == n[i]) && pass;
		Test.Check(pass) << what << std::endl;

		what = "Nominal P accessor";
                pass = true;
                test.getP(result);
                a.getData(output);
                for(int i=0; i<M*M;i++)
                        pass = (output[i] == m[i]) && pass;
                Test.Check(pass) << what << std::endl;
		
		Sequence<double> b;
		udt *result1 = &b;
		what = "Nominal PSI accessor";
		pass = true;
		test.getPsi(result1);
		valarray<double> c = b;
		for(int i=0; i<M;i++)
			pass = (c[i] == otherm[i]) && pass;
		Test.Check(pass) << what << std::endl;
		}//try
		catch ( std::exception& u)
        	{
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        	}

}//Accessors()

void TestState()
{
        const int M = 2;
        const int N = 3;

        valarray<double> m(M*M);//{1,2,3,4}
        for(unsigned int i=0; i<m.size(); i++)
                m[i] = i+1;
        valarray<double> n(N*N); // {9,8,7,6,5,4,3,2,1};
        for(unsigned int i=0; i<n.size(); i++)
                n[i] = n.size() -i;
        valarray<double> nm(N*M);// = {0,2,4,6,8,10);
        for(unsigned int i=0; i<nm.size(); i++)
                nm[i] = i*2;
        valarray<double> otherm(M);// = {11,12};
        for(unsigned int i=0; i<otherm.size(); i++)
                otherm[i] = 11+i;

        const Matrix<double> A(m,M,M);
        const Matrix<double> W(m,M,M);
        const Matrix<double> C(nm,N,M);
        const Matrix<double> V(n,N,N);
        const Sequence<double> psi(otherm);
        const Matrix<double> P(m,M,M);

	string what = "Construct nominal KalmanState";
	try{
		KalmanState state(A,W,C,V,psi,P);
		Test.Check(true) << what << endl;
		what = "Construct Kalman object from a KalmanState";
		Kalman test(state);
		Test.Check(true) << what << endl;
	}
	catch ( std::exception& u)
                {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
                }
	
	what = "Nominal KalmanState copy constructor";
	try{
		KalmanState state(A,W,C,V,psi,P);
		KalmanState state1(state);
	
		Test.Check(true) << what << endl;
		
                Matrix<double> a,b;
		valarray<double> output1,output2;

		bool pass1=true,pass2=true,pass3=true,pass4=true,pass5=true,pass6=true;

		what = "KalmanState getA accessor functions nominally";
		state.getA(a);
		state1.getA(b);
                a.getData(output1);
		b.getData(output2);
                for(int i=0; i<M*M; i++)
			pass1 = (output1[i] == output2[i]) && pass1;
		Test.Check(pass1) << what <<endl;

		what = "KalmanState getW accessor functions nominally";
		state.getW(a);
                state1.getW(b);
                a.getData(output1);     
                b.getData(output2);
                for(int i=0; i<M*M;i++)
                        {
				pass2 = (output1[i] == output2[i]) && pass2;
			}
		Test.Check(pass2) << what << endl;

		what = "KalmanState getC accessor functions nominally";
		state.getC(a);
                state1.getC(b);
                a.getData(output1);     
                b.getData(output2);
                for(int i=0; i<M*N;i++)
                        pass3 = (output1[i] == output2[i]) && pass3;
		Test.Check(pass3) << what << endl;

		what = "KalmanState getV accessor functions nominally";
		state.getV(a);
                state1.getV(b);
                a.getData(output1);     
                b.getData(output2);
                for(int i=0; i<N*N;i++)
                        pass4 = (output1[i] == output2[i]) && pass4;
		Test.Check(pass4) << what << endl;

		what = "KalmanState getP accessor functions nominally";
		state.getP(a);
                state1.getP(b);
                a.getData(output1);     
                b.getData(output2);
                for(int i=0; i<M*M;i++)
                        pass5 = (output1[i] == output2[i]) && pass5;
		Test.Check(pass5) << what << endl;

		what = "KalmanState getPsi accessor functions nominally";
                Sequence<double> c(1),d(1);
                state.getPsi(c);
		state1.getPsi(d);
		output1=c;
		output2=d;
                for(int i=0; i<M;i++)
                        pass6 = (output1[i] == output2[i]) && pass6;
		Test.Check(pass6) << what << endl;

		what = "Copied KalmanState equals orginal KalmanState";
		Test.Check(pass1&&pass2&&pass3&&pass4&&pass5&&pass6) << what << endl;	
	}//try
	catch ( std::exception& u)
        {
                Test.Check(false)
                        << what
                        << "caught unexpected std::exception ("
                        << u.what()
                        << ")"
                        << std::endl;
        }
}//state

int main( int ArgC, char** ArgV)
{

	try{
		string id("$Id");
		Test.Init( ArgC, ArgV);
		if(Test.IsVerbose())
			Test.Message() << id << std::endl;
		
		Exceptions();
		Accessors();
		TestState();
	}
	catch(std::exception& x)
	{
		Test.Check(false) << "Caught std::exception " << x.what() << std::endl;
	}
	catch(...)
	{
		Test.Check(false) << "Caught unknown exception" << std::endl;
	}
      
	Test.Exit();

}

