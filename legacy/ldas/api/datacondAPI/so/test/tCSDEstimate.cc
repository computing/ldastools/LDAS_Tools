/* -*- mode: c++; c-basic-offset: 2; -*- */
//
// tCSDEstimate.cc:
//
// Test driver for CSDEstimate class (mostly for WelchCSDEstimate)
//
//

#include "datacondAPI/config.h"

// system includes
#include <iostream>
#include <complex> 
#include <valarray>
#include <string>
#include <fstream>
#include <stdexcept>

// other includes

#include <general/unittest.h>
#include <filters/LDASConstants.hh>
#include <filters/FilterConstants.hh>
#include <filters/HannWindow.hh>
#include <filters/KaiserWindow.hh>
#include <filters/RectangularWindow.hh>

#include "DataCondAPIConstants.hh"
#include "CSDEstimate.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "WelchCSDEstimate.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"

// the following tests are performed: (for T=float and double)
//
// test error exceptions in constructor 
// test error exceptions in set methods
// test error exceptions in real and complex apply and operator() methods
// test construction of valid welchCSD estimate objects (default and custom)
// test set methods for valid parameters (fft length, overlap length, 
// window, detrend flag)
// test apply and operator() methods on various types of valid data:
// periodogram, non-overlapping segments, overlapping segments, rectangular,
// hann, and kaiser windows, mean and linear detrending, real and complex
// data, random real data (n == 1024)

using namespace datacondAPI;
using std::endl;  

General::UnitTest Test;

#define WELCHCSDESTIMATE_APPLY_FUNC(V, TOut, TXIn, TYIn) \
void  (WelchCSDEstimate::* V)(datacondAPI::WelchCSDSpectrum<TOut>& out, \
			      const datacondAPI::Sequence<TXIn>& xIn, \
		       	      const datacondAPI::Sequence<TYIn>& yIn)

//------------------------------------------------------------------------
// test error exceptions in constructor
//
bool testConstructorError(const std::string& name,
			  const size_t  fftLength,
			  const size_t  overlapLength)
{

  // default hann window for tests 
  Filters::HannWindow window;

  bool pass = false;

  try
  {
    try
    {
      WelchCSDEstimate welchCSD(fftLength,overlapLength,window);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in set methods
//
bool testSetError(const std::string&              name,
                  WelchCSDEstimate           welchCSD,
		  void  (WelchCSDEstimate::* pset)(const size_t length),
		  const size_t  length)
{

  bool pass = false;

  try
  {
    try
    {
      (welchCSD.*pset)(length);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in apply method
//
template<class TOut, class TXIn, class TYIn>
bool testApplyError(const std::string& name,
		    WelchCSDEstimate welchCSD,
		    void (WelchCSDEstimate::* pmf)
		    (datacondAPI::WelchCSDSpectrum<TOut>& out,
		     const datacondAPI::Sequence<TXIn>& xIn,
		     const datacondAPI::Sequence<TYIn>& yIn),
		    const size_t xLength,
		    const size_t yLength)
{
  bool pass = false;

  // default input sequences for apply method
  Sequence<TXIn> xIn(xLength);
  Sequence<TYIn> yIn(yLength);
  
  WelchCSDSpectrum<TOut> out;

  try
  {
    try
    {
      (welchCSD.*pmf)(out, xIn, yIn);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test all error exceptions (constructor, set methods, apply, operator()) 
//
bool testErrors(void)
{
  bool allPass = true;
  bool pass;

  // default welchCSD estimate object 
  WelchCSDEstimate welchCSDdefault;

  // custom welchCSD estimate object (overlap length = 128)
  Filters::RectangularWindow rw;
  WelchCSDEstimate welchCSDcustom(256,128,rw);

  size_t fftLength;
  size_t overlapLength;

  // constructors

  fftLength = 0;
  overlapLength = 0;
  pass = testConstructorError("constructor: fft length = 0",
			      fftLength,
			      overlapLength);
  allPass = allPass && pass;
  
  fftLength = datacondAPI::MaximumFFTLength + 1;
  overlapLength = 0;
  pass =testConstructorError("constructor: fft length > max fft length",
			     fftLength,
	                     overlapLength);
  allPass = allPass && pass;

  fftLength = Filters::MaximumWindowLength + 1;
  overlapLength = 0;
  pass = testConstructorError("constructor: fft length > max window length",
		              fftLength,
			      overlapLength);
  allPass = allPass && pass;

  fftLength = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  overlapLength = fftLength;
  pass = testConstructorError("constructor: overlap length = fft length",
			      fftLength,
			      overlapLength);
  allPass = allPass && pass;

  fftLength = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  overlapLength = fftLength + 1;
  pass = testConstructorError("constructor: overlap length > fft length",
			      fftLength,
			      overlapLength);
  allPass = allPass && pass;

  // set methods

  fftLength = 0;
  pass = testSetError("set method: fftLength = 0",
		      welchCSDdefault,
		      &WelchCSDEstimate::set_fftLength,
		      fftLength);
  allPass = allPass && pass;

  fftLength = datacondAPI::MaximumFFTLength + 1;
  pass = testSetError("set method: fftLength > max fft length",
		      welchCSDdefault,
		      &WelchCSDEstimate::set_fftLength,
		      fftLength);
  allPass = allPass && pass;

  fftLength = Filters::MaximumWindowLength + 1;
  pass = testSetError("set method: fftLength > max window length",
		      welchCSDdefault,
		      &WelchCSDEstimate::set_fftLength,
		      fftLength);
  allPass = allPass && pass;

  
  fftLength = 128;
  pass = testSetError("set method: fftLength = overlap length",
		      welchCSDcustom,
		      &WelchCSDEstimate::set_fftLength,
		      fftLength);
  allPass = allPass && pass;


  fftLength = 127;
  pass = testSetError("set method: fftLength < overlap length",
		      welchCSDcustom,
		      &WelchCSDEstimate::set_fftLength,
		      fftLength);
  allPass = allPass && pass;


  overlapLength = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  pass = testSetError("set method: overlapLength = fft length",
		      welchCSDdefault,
		      &WelchCSDEstimate::set_overlapLength,
		      overlapLength);
  allPass = allPass && pass;

  overlapLength = datacondAPI::WelchCSDEstimateDefaultFFTLength + 1;
  pass = testSetError("set method: overlapLength > fft length",
		      welchCSDdefault,
		      &WelchCSDEstimate::set_overlapLength,
		      overlapLength);
  allPass = allPass && pass;

  // apply method

  // input data have incompatible lengths
  size_t xLength = 1024;
  size_t yLength = 2048; 
  
  pass = testApplyError<std::complex<float>,
  	                float,
			float>
	 ("apply(complex<float>, float, float): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			float>
	 ("apply(complex<float>, complex<float>, float): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  	                float,
			std::complex<float> >
	 ("apply(complex<float>, float, complex<float>): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			std::complex<float> >
	 ("apply(complex<float>, complex<float>, complex<float>): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
   
  pass = testApplyError<std::complex<double>,
  	                double,
			double>
	 ("apply(complex<double>, double, double): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			double>
	 ("apply(complex<double>, complex<double>, double): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  	                double,
			std::complex<double> >
	 ("apply(complex<double>, double, complex<double>): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			std::complex<double> >
	 ("apply(complex<double>, complex<double>, complex<double>): incompatible input lengths",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
  
  
  // no input data
  xLength = 0;
  yLength = 0;

  pass = testApplyError<std::complex<float>,
  	                float,
			float>
	 ("apply(complex<float>, float, float): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			float>
	 ("apply(complex<float>, complex<float>, float): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  	                float,
			std::complex<float> >
	 ("apply(complex<float>, float, complex<float>): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			std::complex<float> >
	 ("apply(complex<float>, complex<float>, complex<float>): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
   
  pass = testApplyError<std::complex<double>,
  	                double,
			double>
	 ("apply(complex<double>, double, double): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			double>
	 ("apply(complex<double>, complex<double>, double): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  	                double,
			std::complex<double> >
	 ("apply(complex<double>, double, complex<double>): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			std::complex<double> >
	 ("apply(complex<double>, complex<double>, complex<double>): no input data",
           welchCSDdefault,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
  
  
  // custom welchCSD estimate object is applied to data that has a
  // length < fft length.  after fft length is reset to the length 
  // of the data, the welchCSD estimate object is no longer valid 
  // since the fft length is now <= the overlap length 

  xLength = 127;
  yLength = 127;
  
  pass = testApplyError<std::complex<float>,
  	                float,
			float>
	 ("apply(complex<float>, float, float): resized fft length < overlap length", 
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			float>
	 ("apply(complex<float>, complex<float>, float): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  	                float,
			std::complex<float> >
	 ("apply(complex<float>, float, complex<float>): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			std::complex<float> >
	 ("apply(complex<float>, complex<float>, complex<float>): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
   
  pass = testApplyError<std::complex<double>,
  	                double,
			double>
	 ("apply(complex<double>, double, double): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			double>
	 ("apply(complex<double>, complex<double>, double): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  	                double,
			std::complex<double> >
	 ("apply(complex<double>, double, complex<double>): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			std::complex<double> >
	 ("apply(complex<double>, complex<double>, complex<double>): resized fft length < overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
  
  // new data length
  xLength = 128;
  yLength = 128;
  
  pass = testApplyError<std::complex<float>,
  	                float,
			float>
	 ("apply(complex<float>, float, float): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			float>
	 ("apply(complex<float>, complex<float>, float): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  	                float,
			std::complex<float> >
	 ("apply(complex<float>, float, complex<float>): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<float>,
  			std::complex<float>,
			std::complex<float> >
	 ("apply(complex<float>, complex<float>, complex<float>): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
   
  pass = testApplyError<std::complex<double>,
  	                double,
			double>
	 ("apply(complex<double>, double, double): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			double>
	 ("apply(complex<double>, complex<double>, double): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  	                double,
			std::complex<double> >
	 ("apply(complex<double>, double, complex<double>): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;

  pass = testApplyError<std::complex<double>,
  			std::complex<double>,
			std::complex<double> >
	 ("apply(complex<double>, complex<double>, complex<double>): resized fft length = overlap length",
           welchCSDcustom,
	   &WelchCSDEstimate::apply,
	   xLength,
	   yLength);
  allPass = allPass && pass;
  
  return allPass;
}

//------------------------------------------------------------------------
// test valid welchCSD estimate objects
//
bool testValidObject(const std::string&            name,
		     const WelchCSDEstimate&  welchCSD,
		     const size_t             fftLengthExpected,
		     const size_t             overlapLengthExpected,
		     const WindowInfo&        windowTypeExpected,
		     const CSDEstimate::DetrendMethod& detrendMethodExpected)

{
  bool allPass = true;
  bool pass;

  // test estimate type
  pass = true;
  if (welchCSD.what() != "Welch CSD Estimate")
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "what" << std::endl;
  allPass = allPass && pass;

  // test fft length
  pass = true;
  if (welchCSD.fftLength() != fftLengthExpected) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "fft length" << std::endl;
  allPass = allPass && pass;

  // test overlap length
  pass = true;
  if (welchCSD.overlapLength() != overlapLengthExpected) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "overlap length" << std::endl;
  allPass = allPass && pass;

  // test window type
  pass = true;
  if (welchCSD.windowType() != windowTypeExpected)
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "window type" << std::endl;
  allPass = allPass && pass;

  // test detrend flag
  pass = true;
  if (welchCSD.detrendMethod() != detrendMethodExpected)
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "detrend flag" << std::endl;
  allPass = allPass && pass;

  return allPass;
}

template<class T>
bool testParsevalR(const TimeSeries<T>& in)
{
    const double tolerance = 1e-4;

    bool pass = false;
    bool allPass = true;

    std::valarray<T> in2 = in;
    in2 *= in2;
    const double pwr_in = in.GetStepSize()*in2.sum();
    
    WelchCSDEstimate welch(in.size(),
			   0,
			   Filters::RectangularWindow(),
			   CSDEstimate::none);

    WelchCSDSpectrum<std::complex<T> > out;

    welch.apply(out, in, in);

    pass = (out.size() == (in.size()/2 + 1));
    Test.Check(pass) << "real TimeSeries apply method: output length"
		     << std::endl;
    allPass = allPass & pass;

    std::complex<T> sum = out.sum();
    double pwr_out = sum.real();

    pass = (std::abs(pwr_in - pwr_out) < tolerance);
    Test.Check(pass) << "real TimeSeries apply method: Parseval"
		     << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(0) == 0.0);
    Test.Check(pass) << "real TimeSeries start frequency"
		     << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(out.size()-1) == in.GetSampleRate()/2);
    Test.Check(pass) << "real TimeSeries Nyquist frequency"
		     << std::endl;
    allPass = allPass & pass;

    return allPass;
}

template<class T>
bool testParsevalC(const TimeSeries<std::complex<T> >& in)
{
    const double tolerance = 1e-4;

    bool pass = false;
    bool allPass = true;

    double pwr_in = 0.0;
    for (size_t i = 0; i < in.size(); ++i)
    {
	pwr_in += norm(in[i]);
    }
    
    pwr_in *= in.GetStepSize();

    WelchCSDEstimate welch(in.size(),
			   0,
			   Filters::RectangularWindow(),
			   CSDEstimate::none);
    WelchCSDSpectrum<std::complex<T> > out;

    welch.apply(out, in, in);

    pass = (out.size() == in.size());
    Test.Check(pass) << "complex TimeSeries apply method: output length"
		     << std::endl;
    allPass = allPass & pass;

    std::complex<T> sum = out.sum();
    double pwr_out = sum.real();

    pass = (std::abs(pwr_in - pwr_out) < tolerance);
    Test.Check(pass) << "complex TimeSeries apply method: Parseval"
		     << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(0) == -in.GetSampleRate()/2);
    Test.Check(pass) << "complex TimeSeries start frequency"
		     << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(out.size()/2) == 0.0);
    Test.Check(pass) << "complex TimeSeries DC frequency"
		     << std::endl;
    allPass = allPass & pass;

    return allPass;
}

//------------------------------------------------------------------------
// test all types of valid welchCSD estimate objects (default, custom, ...)
//
bool testAllValidObjects()
{
  size_t                     fftLength;
  size_t                     overlapLength;
  WindowInfo                 windowType;
  CSDEstimate::DetrendMethod detrendMethod;

  // different windows
  Filters::RectangularWindow rw;
  Filters::HannWindow        hw;
  Filters::KaiserWindow      kw(3); // beta = 3

  bool allPass = true;
  bool pass;

  // default 
  WelchCSDEstimate welchCSDdefault;

  fftLength            = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  overlapLength        = 0;
  windowType           = WindowInfo(hw.name(), hw.param());
  detrendMethod        = CSDEstimate::none;

  pass = testValidObject("default object",
			 welchCSDdefault,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;

  // custom (rectangular window)
  WelchCSDEstimate welchCSDcustomRW1(256,128,rw); // default detrending

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(rw.name(), rw.param());
  detrendMethod        = CSDEstimate::none;

  pass = testValidObject("custom rectangular window object, default detrending",
			 welchCSDcustomRW1,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // custom (rectangular window)
  WelchCSDEstimate welchCSDcustomRW2(256,128,rw,CSDEstimate::none);

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(rw.name(), rw.param());
  detrendMethod        = CSDEstimate::none;

  pass = testValidObject("custom rectangular window object, no detrending",
			 welchCSDcustomRW2,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // custom (hann window)
  WelchCSDEstimate welchCSDcustomHW(256,128,hw,CSDEstimate::mean);

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(hw.name(), hw.param());
  detrendMethod        = CSDEstimate::mean;
  
  pass = testValidObject("custom hann window object, mean detrending",
			 welchCSDcustomHW,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;

  // custom (kaiser window)
  WelchCSDEstimate welchCSDcustomKW(256,128,kw,CSDEstimate::linear);

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(kw.name(), kw.param());
  detrendMethod        = CSDEstimate::linear;

  pass = testValidObject("custom kaiser window object, linear detrending",
			 welchCSDcustomKW,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;

  return allPass;
}

//------------------------------------------------------------------------
// test all valid set methods for welchCSD estimate objects
//
bool testAllValidSetMethods()
{
  bool allPass = true;
  bool pass;

  // different windows
  Filters::RectangularWindow rw;
  Filters::HannWindow        hw;
  Filters::KaiserWindow      kw(3); // beta = 3

  // default object
  WelchCSDEstimate welchCSD;

  // default parameters
  //
  size_t      fftLength;
  size_t      overlapLength = 0;
  WindowInfo  windowType = WindowInfo(hw.name(), hw.param());
  CSDEstimate::DetrendMethod detrendMethod = CSDEstimate::none; 

  // set fft length
  //
  fftLength = 256;
  welchCSD.set_fftLength(fftLength);

  pass = testValidObject("set fft length",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // set overlap length
  //
  overlapLength = 128;
  welchCSD.set_overlapLength(overlapLength);

  pass = testValidObject("set overlap length",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // set window (rectangular)
  //
  welchCSD.set_window(rw);
  windowType = WindowInfo(rw.name(), rw.param());

  pass = testValidObject("set window (rectangular)",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // set window (kaiser)
  //
  welchCSD.set_window(kw);
  windowType = WindowInfo(kw.name(), kw.param());
 
  pass = testValidObject("set window (kaiser)",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;

  // set window (hann) (original default)
  //
  welchCSD.set_window(hw);
  windowType = WindowInfo(hw.name(), hw.param());
 
  pass = testValidObject("set window (hann)",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;

  
  // set detrend flag (CSDEstimate::mean)
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  pass = testValidObject("set detrend flag (mean)",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // set detrend flag (CSDEstimate::linear)
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  pass = testValidObject("set detrend flag (CSDEstimate::linear)",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  // set detrend flag (CSDEstimate::none) (original default)
  //
  detrendMethod = CSDEstimate::none;
  welchCSD.set_detrendMethod(detrendMethod);

  pass = testValidObject("set detrend flag (CSDEstimate::none)",
			 welchCSD,
			 fftLength,
			 overlapLength,
			 windowType,
			 detrendMethod);
  allPass = allPass && pass;


  return allPass;
}

//-----------------------------------------------------------------------
// test apply and operator() methods on valid real data 
//
template <class TOut, class TXIn, class TYIn>
bool testValidApply(const std::string&                        name,
		    WelchCSDEstimate                     welchCSD,
		    const Sequence<TXIn>&                xIn,
		    const Sequence<TYIn>&                yIn,
		    const std::valarray<std::complex<TOut> >& csdExpected,
		    const std::valarray<TOut>&                freqsExpected)
{
  bool pass;
  bool allPass = true;

  double tolerance = 1e-6;

  udt* p_out((udt*)NULL);

  welchCSD.apply(p_out, xIn, yIn);

  const WelchCSDSpectrum<std::complex<TOut> >* out(dynamic_cast<const WelchCSDSpectrum<std::complex<TOut> >*>(p_out));
  
  // test csd output of apply method
  pass = true;
  for (size_t i = 0; i < out->size(); i++)
  {
    if (std::abs((*out)[i].real() - csdExpected[i].real()) > tolerance)
    {
      pass = pass && false;
    }
    if (std::abs((*out)[i].imag() - csdExpected[i].imag()) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "apply method (csd): " << name << std::endl;
  allPass = allPass && pass;

  // test frequency output of apply method
  pass = true;
  for (size_t i = 0; i < out->size(); i++)
  {
    if (std::abs(out->GetFrequency(i) - freqsExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "apply method (freqs): " << name << std::endl;

  allPass = allPass && pass;

  return allPass;
}

//-----------------------------------------------------------------------
// test apply method on different valid sets of real and complex data
//
template <class T>
bool testAllValidApply()
{
  bool allPass = true;
  bool pass = true;

  // parameters
  size_t dataLength;
  size_t fftLength;
  size_t psdLength;
  size_t overlapLength;
  CSDEstimate::DetrendMethod detrendMethod;

  // windows
  Filters::RectangularWindow rw;
  Filters::HannWindow        hw;
  Filters::KaiserWindow      kw(3);  // beta parameter = 3

  // welchCSD estimate object
  WelchCSDEstimate  welchCSD;  // default


  // real-real data --------------------------------------------------------
  //
  dataLength = 8;
  T dataXInReal[] = {
      -0.7223, 
      -0.7215,
      -0.2012,
      -0.0205, 
      0.2789, 
      1.0583, 
      0.6217,
      -1.7506
  };

  T dataYInReal[] = {
      0.6363,
      1.3101, 
      0.3271,
      -0.6730, 
      -0.1493,
      -2.4490,
      0.4733,
      0.1169
  };

  Sequence<T> xInReal(dataXInReal,dataLength);
  Sequence<T> yInReal(dataYInReal,dataLength);

  // periodogram, no detrending ---------------------------------------------
  //
  fftLength = dataLength;
  psdLength = fftLength/2 + 1;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR1[] = {
      0,
      0.12500000000000,
      0.25000000000000,
      0.37500000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR1Expected(freqsR1,psdLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR1a[] = { 
      std::complex<T>(7.424434000000e-02, 0.000000000000e+00),
	  std::complex<T>(-3.904027656109e+00, 8.423869556113e-01),
	  std::complex<T>(-2.394344650000e-01, -2.910241950000e-01),
	  std::complex<T>(-4.576157988914e-01, -1.310067649389e+00),
	  std::complex<T>(5.261699200000e-01, 0.000000000000e+00)
    };

  std::valarray<std::complex<T> > csdR1aExpected(csdR1a,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: periodogram, no detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR1aExpected,
			         freqsR1Expected);
  allPass = allPass && pass;

  TimeSeries<T> tsR1(xInReal, 2048.0);
  pass = testParsevalR(tsR1);
  allPass = allPass && pass;

  // periodogram, mean detrending ------------------------------------------
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR1b[] = { 
      std::complex<T>(0.000000000000e+00, 0.000000000000e+00),
	  std::complex<T>(-3.904027656109e+00, 8.423869556113e-01),
	  std::complex<T>(-2.394344650000e-01, -2.910241950000e-01),
	  std::complex<T>(-4.576157988914e-01, -1.310067649389e+00),
	  std::complex<T>(5.261699200000e-01, 0.000000000000e+00)
	  };
 
  std::valarray<std::complex<T> > csdR1bExpected(csdR1b,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: periodogram, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR1bExpected,
			         freqsR1Expected);
  allPass = allPass && pass;

  // periodogram, linear detrending -----------------------------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR1c[] = { 
      std::complex<T>(-1.232595164408e-32, 0.000000000000e+00),
	  std::complex<T>(-2.715138142700e+00, -2.321098163834e-01),
	  std::complex<T>(-5.728868095578e-01, -8.322609021429e-01),
	  std::complex<T>(-8.952807924364e-01, -1.481753672812e+00),
	  std::complex<T>(4.428157061224e-01, 0.000000000000e+00)
	  };
 
  std::valarray<std::complex<T> > csdR1cExpected(csdR1c,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: periodogram, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR1cExpected,
			         freqsR1Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, no detrending ------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR2[] = {
      0.0,
      0.25000000000000,
      0.50000000000000
  };
 
  std::valarray<T> freqsR2Expected(freqsR2,psdLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR2a[] = { 
      std::complex<T>(-3.854899975000e-01, 0.000000000000e+00),
	  std::complex<T>(-2.136301612500e+00, -4.529418625000e-01),
	  std::complex<T>(5.214597800000e-01, 0.000000000000e+00)
	  };
 
  std::valarray<std::complex<T> > csdR2aExpected(csdR2a,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: non-overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR2aExpected,
			         freqsR2Expected);
  allPass = allPass && pass;

  // non-overlapping segments, rectangular window, mean detrending ----------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR2b[] = { 
      std::complex<T>(3.851859888774e-33, 0.000000000000e+00),
	  std::complex<T>(-2.136301612500e+00, -4.529418625000e-01),
	  std::complex<T>(5.214597800000e-01, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR2bExpected(csdR2b,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: non-overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR2bExpected,
			         freqsR2Expected);
  allPass = allPass && pass;

  // non-overlapping segments, rectangular window, linear detrending ---------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR2c[] = { 
      std::complex<T>(-1.521484656066e-32, 0.000000000000e+00),
	  std::complex<T>(-7.796827149000e-01, -6.761795935000e-01),
	  std::complex<T>(9.418575040000e-02, 0.000000000000e+00)
	  };
 
  std::valarray<std::complex<T> > csdR2cExpected(csdR2c,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: non-overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR2cExpected,
			         freqsR2Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, no detrending ----------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR3[] = {
      0.0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR3Expected(freqsR3,psdLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR3a[] = { 
      std::complex<T>(-5.306812566667e-01, 0.000000000000e+00),
	  std::complex<T>(-1.781645815000e+00, -2.455083616667e-01),
	  std::complex<T>(8.362835500000e-02, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR3aExpected(csdR3a,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR3aExpected,
			         freqsR3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, mean detrending
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR3b[] = {
      std::complex<T>(4.622231866529e-33, 0.000000000000e+00),
	  std::complex<T>(-1.781645815000e+00, -2.455083616667e-01),
	  std::complex<T>(8.362835500000e-02, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR3bExpected(csdR3b,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR3bExpected,
			         freqsR3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, linear detrending
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR3c[] = { 
      std::complex<T>(-1.502225356622e-32, 0.000000000000e+00),
	  std::complex<T>(-5.898631644667e-01, -4.152343430000e-01),
	  std::complex<T>(4.184858880000e-02, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR3cExpected(csdR3c,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR3cExpected,
			         freqsR3Expected);
  allPass = allPass && pass;

  
  // overlapping segments, hann window, no detrending ----------------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(hw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR4[] = {
      0.0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR4Expected(freqsR4,psdLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR4a[] = { 
      std::complex<T>(-8.403837933333e-01, 0.000000000000e+00),
	  std::complex<T>(-1.112139676667e+00, -7.472621700000e-01),
	  std::complex<T>(-2.717558833333e-01, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR4aExpected(csdR4a,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, hann window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR4aExpected,
			         freqsR4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR4b[] = { 
      std::complex<T>(-3.001626558333e-01, 0.000000000000e+00),
	  std::complex<T>(-5.719185391667e-01, -5.804200191667e-01),
	  std::complex<T>(-2.717558833333e-01, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR4bExpected(csdR4b,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, hann window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR4bExpected,
			         freqsR4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, linear detrending --------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR4c[] = { 
      std::complex<T>(-3.001626558333e-01, 0.000000000000e+00),
	  std::complex<T>(-2.530829934333e-01, -6.228515145000e-01),
	  std::complex<T>(4.707966240000e-02, 0.000000000000e+00)
	  };

  std::valarray<std::complex<T> > csdR4cExpected(csdR4c,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, hann window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR4cExpected,
			         freqsR4Expected);
  allPass = allPass && pass;

  
  // overlapping segments, kaiser window, no detrending ----------------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(kw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR5[] = {
      0.0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR5Expected(freqsR5,psdLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR5a[] = { 
      std::complex<T>(-7.960545645264e-01, 0.000000000000e+00),
	  std::complex<T>(-1.409772601231e+00, -6.746891080500e-01),
	  std::complex<T>(-1.891537280953e-02, 0.000000000000e+00)
    };

  std::valarray<std::complex<T> > csdR5aExpected(csdR5a,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, kaiser window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR5aExpected,
			         freqsR5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR5b[] = { 
      std::complex<T>(-1.663858722854e-01, 0.000000000000e+00),
	  std::complex<T>(-1.016618446113e+00, -5.871621180468e-01),
	  std::complex<T>(-1.891537280953e-02, 0.000000000000e+00)
	  };

  std::valarray<std::complex<T> > csdR5bExpected(csdR5b,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, kaiser window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR5bExpected,
			         freqsR5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, linear detrending ------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdR5c[] = { 
      std::complex<T>(-1.663858722854e-01, 0.000000000000e+00),
	  std::complex<T>(-3.960469831422e-01, -6.718625992987e-01),
	  std::complex<T>(5.188079596972e-02, 0.000000000000e+00)
    };
 
  std::valarray<std::complex<T> > csdR5cExpected(csdR5c,psdLength);

  pass = testValidApply<T, T, T>("REAL-REAL input: overlapping segments, kaiser window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInReal,
			         csdR5cExpected,
			         freqsR5Expected);
  allPass = allPass && pass;

	  
  // complex-complex data -------------------------------------------------
  //
  dataLength = 8;
  std::complex<T> dataXInComp[] = { std::complex<T>(-0.7223,-0.4326),
	  		            std::complex<T>(-0.7215,-1.6656),
			            std::complex<T>(-0.2012, 0.1253),
			            std::complex<T>(-0.0205, 0.2877),
			            std::complex<T>( 0.2789,-1.1465),
			            std::complex<T>( 1.0583, 1.1909),
			            std::complex<T>( 0.6217, 1.11892),
			            std::complex<T>(-1.7506,-0.0376) };

  std::complex<T> dataYInComp[] = { std::complex<T>( 0.6363, 0.3273),
	 	                    std::complex<T>( 1.3101, 0.1746),
		                    std::complex<T>( 0.3271,-0.1867),
		                    std::complex<T>(-0.6730, 0.7258),
		                    std::complex<T>(-0.1493,-0.5883),
		                    std::complex<T>(-2.4490, 2.1832),
		                    std::complex<T>( 0.4733,-0.1364),
		                    std::complex<T>( 0.1169, 0.1139) };

  Sequence<std::complex<T> > xInComp(dataXInComp,dataLength);
  Sequence<std::complex<T> > yInComp(dataYInComp,dataLength);

  
  // periodogram, no detrending ---------------------------------------------
  //
  fftLength = dataLength;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC1[] = {
      -0.50000000000000,
      -0.37500000000000,
      -0.25000000000000,
      -0.12500000000000,
      0,
      0.12500000000000,
      0.25000000000000,
      0.37500000000000,
  };

  std::valarray<T> freqsC1Expected(freqsC1,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC1a[] = { 
   	std::complex<T>( 0.57829927600000,-0.62605639600000),
        std::complex<T>( 1.92629241357619, 2.12758921273538),
        std::complex<T>( 0.07841010550000,-0.15473279500000),
        std::complex<T>(-0.74480710926932, 1.41942349337853),
  	std::complex<T>(-0.10852378900000,-0.50453631600000),
        std::complex<T>(-2.07702916007619, 2.51554049576462),
        std::complex<T>(-0.63675633350000, 0.61451101300000),
        std::complex<T>(-0.14597815123068, 0.96092887612147) };
 
  std::valarray<std::complex<T> > csdC1aExpected(csdC1a,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: periodogram, no detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC1aExpected,
			         freqsC1Expected);
  allPass = allPass && pass;

  TimeSeries<std::complex<T> > tsC1(xInComp, 2048.0);
  pass = testParsevalC(tsC1);
  allPass = allPass && pass;

  // periodogram, mean detrending ------------------------------------------- 
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC1b[] = { 
  	std::complex<T>( 0.57829927600000,-0.62605639600000),
        std::complex<T>( 1.92629241357619, 2.12758921273538),
        std::complex<T>( 0.07841010550000,-0.15473279500000),
        std::complex<T>(-0.74480710926932, 1.41942349337853),
  	std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-2.07702916007619, 2.51554049576462),
        std::complex<T>(-0.63675633350000, 0.61451101300000),
        std::complex<T>(-0.14597815123068, 0.96092887612147) };
  
  std::valarray<std::complex<T> > csdC1bExpected(csdC1b,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: periodogram, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC1bExpected,
			         freqsC1Expected);
  allPass = allPass && pass;


  // periodogram, linear detrending -----------------------------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC1c[] = { 
  	std::complex<T>( 0.09220156650340,-0.93720132575964),
        std::complex<T>( 2.24522594196311, 2.60693063603262),
        std::complex<T>( 0.23894994388775, 0.23309665190930),
        std::complex<T>(-1.22876417844066, 0.76340766810223),
  	std::complex<T>( 0.00000000000000, 0.00000000000000),
        std::complex<T>(-1.23423552434066, 0.54617801658075),
        std::complex<T>(-0.84222944092177, 0.20992189009977),
        std::complex<T>(-0.25996007622261, 1.66016650398733) };
  
  std::valarray<std::complex<T> > csdC1cExpected(csdC1c,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: periodogram, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC1cExpected,
			         freqsC1Expected);
  allPass = allPass && pass;

  
  // non-overlapping segments, rectangular window, no detrending ------------
  //
  fftLength = dataLength/2; // :NOTE: this is odd
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC2[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsC2Expected(freqsC2,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC2a[] = { 
    std::complex<T>(8.658274429999999e-01, -2.360403665000000e-01),
    std::complex<T>(8.479177699999982e-02, 1.358045891500000e+00),
    std::complex<T>(-3.835163815000000e-01, 4.439332940000000e-01),
    std::complex<T>(-1.132149212500000e+00, 1.610394973000000e+00)
  };

  std::valarray<std::complex<T> > csdC2aExpected(csdC2a,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: non-overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC2aExpected,
			         freqsC2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, mean detrending ----------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC2b[] = { 
    std::complex<T>(8.658274429999999e-01, -2.360403664999999e-01),
    std::complex<T>(8.479177699999982e-02, 1.358045891500000e+00),
    std::complex<T>(3.081487911019577e-33, -4.622231866529366e-33),
    std::complex<T>(-1.132149212500000e+00, 1.610394973000000e+00)
  };
 
  std::valarray<std::complex<T> > csdC2bExpected(csdC2b,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: non-overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC2bExpected,
			         freqsC2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, linear detrending --------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC2c[] = { 
    std::complex<T>(1.592433800000002e-01, 2.420691225600001e-01),
    std::complex<T>(8.417999634000000e-01, 1.242267118420000e+00),
    std::complex<T>(3.659266894335748e-33, 2.946672814912471e-32),
    std::complex<T>(-3.183118729000002e-01, 9.761489517199999e-01)
  };
 
  std::valarray<std::complex<T> > csdC2cExpected(csdC2c,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: non-overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC2cExpected,
			         freqsC2Expected);
  allPass = allPass && pass;

  
  // overlapping segments, rectangular window, no detrending ---------------
  //
  fftLength = dataLength/2;      // :NOTE: this is odd
  overlapLength = dataLength/4;  // :NOTE: so is this
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC3[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsC3Expected(freqsC3,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC3a[] = { 
    std::complex<T>(1.080645397000000e+00, 8.247937923333333e-01),
    std::complex<T>(1.597334829999999e-01, 9.510405243333333e-01),
    std::complex<T>(-4.480245459999999e-01, 6.065517026666667e-01),
    std::complex<T>(-9.109034200000000e-01, 1.424192158666666e+00)
  };

  std::valarray<std::complex<T> > csdC3aExpected(csdC3a,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC3aExpected,
			         freqsC3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, mean detrending --------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC3b[] = { 
    std::complex<T>(1.080645397000000e+00, 8.247937923333335e-01),
    std::complex<T>(1.597334829999999e-01, 9.510405243333333e-01),
    std::complex<T>(0, -1.232595164407831e-32),
    std::complex<T>(-9.109034200000000e-01, 1.424192158666666e+00)
  };

  std::valarray<std::complex<T> > csdC3bExpected(csdC3b,fftLength);
  
  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC3bExpected,
			         freqsC3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, linear detrending ------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC3c[] = { 
    std::complex<T>(5.370223690666667e-01, 5.029425737066668e-01),
    std::complex<T>(7.045588873999998e-01, 1.048494261613333e+00),
    std::complex<T>(6.548161810916602e-33, 1.322471895145902e-32),
    std::complex<T>(2.425274186666652e-02, 8.441416831466665e-01)
  };
 
  std::valarray<std::complex<T> > csdC3cExpected(csdC3c,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC3cExpected,
			         freqsC3Expected);
  allPass = allPass && pass;

  
  // overlapping segments, hann window, no detrending -----------------------
  //
  fftLength = dataLength/2;      // :NOTE: this is odd
  overlapLength = dataLength/4;  // :NOTE: so is this
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(hw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC4[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsC4Expected(freqsC4,fftLength);

  // expected output (generated by csd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  std::complex<T> csdC4a[] = { 
    std::complex<T>(-3.765674033333320e-02, 5.255339706666660e-01),
    std::complex<T>(4.970951020000000e-01, 1.182883071666666e+00),
    std::complex<T>(-6.900175900000011e-02, 1.644069660666666e+00),
    std::complex<T>(-6.037536013333331e-01, 9.867205596666661e-01)
  };
 
  std::valarray<std::complex<T> > csdC4aExpected(csdC4a,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, hann window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC4aExpected,
			         freqsC4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, mean detrending ----------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  std::complex<T> csdC4b[] = { 
    std::complex<T>(-3.765674033333320e-02, 5.255339706666665e-01),
    std::complex<T>(6.888315929166667e-01, 7.730684679166663e-01),
    std::complex<T>(2.972780185000001e-01, 8.834501506666662e-01),
    std::complex<T>(-4.292103147499997e-01, 6.359156534166661e-01)
  };
 
  std::valarray<std::complex<T> > csdC4bExpected(csdC4b,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, hann window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC4bExpected,
			         freqsC4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, linear detrending -------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  std::complex<T> csdC4c[] = { 
    std::complex<T>(6.041501652000000e-01, 5.658103954199996e-01),
    std::complex<T>(9.609437009999999e-01, 8.778947068933330e-01),
    std::complex<T>(2.972780185000000e-01, 8.834501506666664e-01),
    std::complex<T>(-5.951551729999993e-02, 5.713658391933328e-01)
  };

  std::valarray<std::complex<T> > csdC4cExpected(csdC4c,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, hann window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC4cExpected,
			         freqsC4Expected);
  allPass = allPass && pass;

  // overlapping segments, kaiser window, no detrending ---------------------
  //
  fftLength = dataLength/2;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(kw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC5[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsC5Expected(freqsC5,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC5a[] = { 
    std::complex<T>(4.930924646896767e-01, 5.937917420080749e-01),
    std::complex<T>(4.617158050502556e-01, 1.151660625157568e+00),
    std::complex<T>(-2.864201296835890e-01, 1.337563679860312e+00),
    std::complex<T>(-8.717726904327543e-01, 1.200367354035298e+00)
  };
  
  std::valarray<std::complex<T> > csdC5aExpected(csdC5a,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, kaiser window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC5aExpected,
			         freqsC5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC5b[] = { 
    std::complex<T>(4.930924646896767e-01, 5.937917420080749e-01),
    std::complex<T>(5.685115440161042e-01, 8.835258773684023e-01),
    std::complex<T>(1.647868629163216e-01, 4.897132307524170e-01),
    std::complex<T>(-7.270374509517292e-01, 8.909802037915753e-01)
  };
 
  std::valarray<std::complex<T> > csdC5bExpected(csdC5b,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, kaiser window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC5bExpected,
			         freqsC5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, linear detrending ------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdC5c[] = { 
    std::complex<T>(6.657607522652175e-01, 6.235111338082597e-01),
    std::complex<T>(1.008390894605431e+00, 1.031616668202284e+00),
    std::complex<T>(1.647868629163217e-01, 4.897132307524171e-01),
    std::complex<T>(-9.236645594177867e-02, 7.009675856864812e-01)
  };

  std::valarray<std::complex<T> > csdC5cExpected(csdC5c,fftLength);

  pass = testValidApply<T, std::complex<T>, std::complex<T> >
	                        ("COMPLEX-COMPLEX input: overlapping segments, kaiser window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInComp,
			         csdC5cExpected,
			         freqsC5Expected);
  allPass = allPass && pass;
  
  
  // complex-real data --------------------------------------------------------
  //
  // NOTE: use xInComp and yInReal as input

  // periodogram, no detrending ---------------------------------------------
  //
  fftLength = dataLength;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsCR1[] = {
      -0.50000000000000,
      -0.37500000000000,
      -0.25000000000000,
      -0.12500000000000,
      0,
      0.12500000000000,
      0.25000000000000,
      0.37500000000000
  };

  std::valarray<T> freqsCR1Expected(freqsCR1,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR1a[] = { 
	std::complex<T>( 0.52616992000000, 0.04111238400000),
        std::complex<T>( 0.36122463077858, 1.77917065584611),
        std::complex<T>( 0.05756758950000,-0.01789314350000),
  	std::complex<T>(-0.93454884483001, 1.02684882504257),
  	std::complex<T>( 0.07424434000000,-0.02850550600000),
        std::complex<T>(-2.96947881127859, 1.86923578065389),
        std::complex<T>(-0.29700205450000,-0.30891733850000),
        std::complex<T>(-0.81884042966999, 0.46910300645743) };
 
  std::valarray<std::complex<T> > csdCR1aExpected(csdCR1a,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: periodogram, no detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR1aExpected,
			         freqsCR1Expected);
  allPass = allPass && pass;

  // periodogram, mean detrending ------------------------------------------
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR1b[] = { 
	std::complex<T>( 0.52616992000000, 0.04111238400000),
        std::complex<T>( 0.36122463077858, 1.77917065584611),
        std::complex<T>( 0.05756758950000,-0.01789314350000),
  	std::complex<T>(-0.93454884483001, 1.02684882504257),
  	std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-2.96947881127859, 1.86923578065389),
        std::complex<T>(-0.29700205450000,-0.30891733850000),
        std::complex<T>(-0.81884042966999, 0.46910300645743) };
 
  std::valarray<std::complex<T> > csdCR1bExpected(csdCR1b,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: periodogram, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR1bExpected,
			         freqsCR1Expected);
  allPass = allPass && pass;


  // periodogram, linear detrending -----------------------------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR1c[] = { 
	std::complex<T>( 0.44281570612245,-0.21496877718821),
        std::complex<T>( 0.31139063042026, 2.37053181253202),
        std::complex<T>( 0.05642256012585, 0.18458487421882),
  	std::complex<T>(-1.10071411228275, 0.39680371798858),
  	std::complex<T>( 0.00000000000000, 0.00000000000000),
        std::complex<T>(-1.61442403041686, 0.16469390160517),
        std::complex<T>(-0.62930936968367,-0.64767602792404),
        std::complex<T>(-1.20667142285670, 0.88877813972004) };
 
  std::valarray<std::complex<T> > csdCR1cExpected(csdCR1c,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: periodogram, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR1cExpected,
			         freqsCR1Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, no detrending ------------
  //
  fftLength = dataLength/2;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsCR2[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000
  };

  std::valarray<T> freqsCR2Expected(freqsCR2,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR2a[] = { 
  	std::complex<T>( 0.52145978000000, 0.34839982350000),
        std::complex<T>(-0.30873903775000, 0.94995212725000),
        std::complex<T>(-0.38548999750000, 0.61971511650000),
        std::complex<T>(-1.82756257475000, 0.49701026475000) };
 
  std::valarray<std::complex<T> > csdCR2aExpected(csdCR2a,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: non-overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR2aExpected,
			         freqsCR2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, mean detrending ----------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR2b[] = { 
  	std::complex<T>( 0.52145978000000, 0.34839982350000),
        std::complex<T>(-0.30873903775000, 0.94995212725000),
        std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-1.82756257475000, 0.49701026475000) };
 
  std::valarray<std::complex<T> > csdCR2bExpected(csdCR2b,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: non-overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR2bExpected,
			         freqsCR2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, linear detrending ---------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR2c[] = { 
  	std::complex<T>( 0.09418575040000, 0.37781215336000),
        std::complex<T>(-0.11950899255000, 0.95565974067000),
        std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-0.66017372235000, 0.27948014717000) };
 
  std::valarray<std::complex<T> > csdCR2cExpected(csdCR2c,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: non-overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR2cExpected,
			         freqsCR2Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, no detrending ----------------
  //
  fftLength = dataLength/2;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsCR3[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000
  };

  std::valarray<T> freqsCR3Expected(freqsCR3,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR3a[] = { 
  	std::complex<T>( 0.08362835500000, 0.91966988566667),
        std::complex<T>(-0.16046495516667, 0.68825811816667),
        std::complex<T>(-0.53068125666667, 0.52536650100000),
        std::complex<T>(-1.62118085983333, 0.44274975650000) };
 
  std::valarray<std::complex<T> > csdCR3aExpected(csdCR3a,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR3aExpected,
			         freqsCR3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, mean detrending
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR3b[] = { 
  	std::complex<T>( 0.08362835500000, 0.91966988566667),
        std::complex<T>(-0.16046495516667, 0.68825811816667),
        std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-1.62118085983333, 0.44274975650000) };
 
  std::valarray<std::complex<T> > csdCR3bExpected(csdCR3b,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR3bExpected,
			         freqsCR3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, linear detrending
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR3c[] = { 
  	std::complex<T>( 0.04184858880000, 0.56302995610667),
        std::complex<T>(-0.13536539963333, 0.77600111584667),
        std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-0.45449776483333, 0.36076677284667) };
 
  std::valarray<std::complex<T> > csdCR3cExpected(csdCR3c,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR3cExpected,
			         freqsCR3Expected);
  allPass = allPass && pass;

  
  // overlapping segments, hann window, no detrending ----------------------
  //
  fftLength = dataLength/2;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(hw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsCR4[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000
  };

  std::valarray<T> freqsCR4Expected(freqsCR4,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR4a[] = { 
  	std::complex<T>(-0.27175588333333, 0.45364873233333),
        std::complex<T>(-0.25934206333333, 1.13204705900000),
        std::complex<T>(-0.84038379333333, 1.06318321566667),
        std::complex<T>(-0.85279761333333, 0.38478488900000) };
  
  std::valarray<std::complex<T> > csdCR4aExpected(csdCR4a,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, hann window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR4aExpected,
			         freqsCR4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR4b[] = { 
  	std::complex<T>(-0.27175588333333, 0.45364873233333),
        std::complex<T>( 0.09608794675000, 0.76603697566667),
        std::complex<T>(-0.30016265583333, 0.49800519983333),
        std::complex<T>(-0.66800648591667, 0.18561695650000) };
  
  std::valarray<std::complex<T> > csdCR4bExpected(csdCR4b,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, hann window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR4bExpected,
			         freqsCR4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, linear detrending --------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR4c[] = { 
  	std::complex<T>( 0.04707966240000, 0.63340870062000),
        std::complex<T>( 0.11280777718333, 0.87713270747667),
        std::complex<T>(-0.30016265583333, 0.49800519983333),
        std::complex<T>(-0.36589077061667, 0.25428119297667) };
  
  std::valarray<std::complex<T> > csdCR4cExpected(csdCR4c,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, hann window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR4cExpected,
			         freqsCR4Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, no detrending ----------------------
  //
  fftLength = dataLength/2;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(kw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsCR5[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000
  };

  std::valarray<T> freqsCR5Expected(freqsCR5,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR5a[] = { 
  	std::complex<T>(-0.01891537280953, 0.63622829487153),
        std::complex<T>(-0.18500325104602, 1.04878082984224),
        std::complex<T>(-0.79605456452638, 0.92660078853423),
        std::complex<T>(-1.22476935018520, 0.37409172179222) };
 
  std::valarray<std::complex<T> > csdCR5aExpected(csdCR5a,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, kaiser window, no detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR5aExpected,
			         freqsCR5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR5b[] = { 
  	std::complex<T>(-0.01891537280953, 0.63622829487153),
        std::complex<T>( 0.03472338471181, 0.79681674610960),
        std::complex<T>(-0.16638587228543, 0.27605375940889),
        std::complex<T>(-1.05134183082506, 0.20965462806277) };
  
  std::valarray<std::complex<T> > csdCR5bExpected(csdCR5b,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, kaiser window, mean detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR5bExpected,
			         freqsCR5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, linear detrending ------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdCR5c[] = { 
  	std::complex<T>( 0.05188079596972, 0.69800304180419),
        std::complex<T>( 0.06015975364714, 0.95081173935745),
        std::complex<T>(-0.16638587228543, 0.27605375940889),
        std::complex<T>(-0.45620673678934, 0.27894914005877) };
  
  std::valarray<std::complex<T> > csdCR5cExpected(csdCR5c,fftLength);

  pass = testValidApply<T, std::complex<T>, T>
	                        ("COMPLEX-REAL input: overlapping segments, kaiser window, linear detrending",
		                 welchCSD,
			         xInComp,
			         yInReal,
			         csdCR5cExpected,
			         freqsCR5Expected);
  allPass = allPass && pass;


  // real-complex data -------------------------------------------------
  //
  // NOTE: use xInReal and yInComp as input

  // periodogram, no detrending ---------------------------------------------
  //
  fftLength = dataLength;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsRC1[] = {
      -0.50000000000000,
      -0.37500000000000,
      -0.25000000000000,
      -0.12500000000000,
      0,
      0.12500000000000,
      0.25000000000000,
      0.37500000000000
  };

  std::valarray<T> freqsRC1Expected(freqsRC1,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC1a[] = { 
  	std::complex<T>( 0.52616992000000,-0.66716878000000),
        std::complex<T>( 0.21729485273345, 1.07515603797100),
        std::complex<T>( 0.06058116500000, 0.53880644750000),
        std::complex<T>(-2.30336778587515, 0.09824621391768),
  	std::complex<T>( 0.07424434000000,-0.47603081000000),
        std::complex<T>(-1.60065987023344, 0.94063316952900),
        std::complex<T>(-0.30001563000000, 0.24778225250000),
        std::complex<T>(-0.67491065162485,-0.23491161141768) };
  
  std::valarray<std::complex<T> > csdRC1aExpected(csdRC1a,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: periodogram, no detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC1aExpected,
			         freqsRC1Expected);
  allPass = allPass && pass;


  // periodogram, mean detrending ------------------------------------------- 
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC1b[] = { 
  	std::complex<T>( 0.52616992000000,-0.66716878000000),
        std::complex<T>( 0.21729485273345, 1.07515603797100),
        std::complex<T>( 0.06058116500000, 0.53880644750000),
        std::complex<T>(-2.30336778587515, 0.09824621391768),
  	std::complex<T>( 0.00000000000000, 0.00000000000000),
        std::complex<T>(-1.60065987023344, 0.94063316952900),
        std::complex<T>(-0.30001563000000, 0.24778225250000),
        std::complex<T>(-0.67491065162485,-0.23491161141768) };
 
  std::valarray<std::complex<T> > csdRC1bExpected(csdRC1b,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: periodogram, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC1bExpected,
			         freqsRC1Expected);
  allPass = allPass && pass;


  // periodogram, linear detrending -----------------------------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC1c[] = { 
  	std::complex<T>( 0.44281570612245,-0.72223254857143),
        std::complex<T>( 0.04592158623615, 1.24477043028994),
        std::complex<T>(-0.08871967727891, 0.86918529892857),
        std::complex<T>(-1.61168835746686, 0.49009894073632),
  	std::complex<T>( 0.00000000000000,-0.00000000000000),
        std::complex<T>(-1.10344978523275, 0.25798912435291),
        std::complex<T>(-0.48416713227891, 0.03692439678571),
        std::complex<T>(-0.94120237867260,-0.23698324252204) };
  
  std::valarray<std::complex<T> > csdRC1cExpected(csdRC1c,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: periodogram, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC1cExpected,
			         freqsRC1Expected);
  allPass = allPass && pass;

  
  // non-overlapping segments, rectangular window, no detrending ------------
  //
  fftLength = dataLength/2; // :NOTE: this is odd
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsRC2[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsRC2Expected(freqsRC2,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC2a[] = { 
    std::complex<T>(5.214597799999998e-01, -5.844401900000000e-01),
    std::complex<T>(-1.219092080000000e+00, 9.872101674999998e-01),
    std::complex<T>(-3.854899975000000e-01, -1.757818224999999e-01),
    std::complex<T>(-9.172095325000000e-01, 5.342683049999999e-01)
  };

  std::valarray<std::complex<T> > csdRC2aExpected(csdRC2a,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: non-overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC2aExpected,
			         freqsRC2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, mean detrending ----------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC2b[] = { 
    std::complex<T>(5.214597799999999e-01, -5.844401899999999e-01),
    std::complex<T>(-1.219092080000000e+00, 9.872101674999998e-01),
    std::complex<T>(3.851859888774472e-33, -7.703719777548943e-34),
    std::complex<T>(-9.172095325000000e-01, 5.342683049999999e-01)
  };

  std::valarray<std::complex<T> > csdRC2bExpected(csdRC2b,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: non-overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC2bExpected,
			         freqsRC2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, linear detrending --------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC2c[] = { 
    std::complex<T>(9.418575039999999e-02, -1.357430308000000e-01),
    std::complex<T>(-8.011780419999992e-02, 8.297278879000001e-01),
    std::complex<T>(-9.437056727497456e-33, 7.125940794232773e-33),
    std::complex<T>(-6.995649107000000e-01, 1.535482943999999e-01)
  };
  
  std::valarray<std::complex<T> > csdRC2cExpected(csdRC2c,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: non-overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC2cExpected,
			         freqsRC2Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, no detrending ---------------
  //
  fftLength = dataLength/2;
  overlapLength = dataLength/4;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(rw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsRC3[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };
  
  std::valarray<T> freqsRC3Expected(freqsRC3,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC3a[] = { 
    std::complex<T>(8.362835499999988e-02, -9.487609333333336e-02),
    std::complex<T>(-1.085862408333333e+00, 7.448665849999998e-01),
    std::complex<T>(-5.306812566666667e-01, 8.118520166666671e-02),
    std::complex<T>(-6.957834066666666e-01, 4.993582233333332e-01)
  };

 
  std::valarray<std::complex<T> > csdRC3aExpected(csdRC3a,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, rectangular window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC3aExpected,
			         freqsRC3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, mean detrending --------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC3b[] = { 
    std::complex<T>(8.362835499999999e-02, -9.487609333333336e-02),
    std::complex<T>(-1.085862408333333e+00, 7.448665849999998e-01),
    std::complex<T>(4.622231866529366e-33, -1.540743955509789e-33),
    std::complex<T>(-6.957834066666666e-01, 4.993582233333332e-01)
  };
  
  std::valarray<std::complex<T> > csdRC3bExpected(csdRC3b,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, rectangular window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC3bExpected,
			         freqsRC3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, linear detrending ------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC3c[] = { 
    std::complex<T>(4.184858879999998e-02, -6.008738239999996e-02),
    std::complex<T>(-1.143446920666666e-01, 5.855511995333333e-01),
    std::complex<T>(-6.291371151664970e-33, 9.629649721936179e-33),
    std::complex<T>(-4.755184724000000e-01, 1.703168565333333e-01)
  };
 
  std::valarray<std::complex<T> > csdRC3cExpected(csdRC3c,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, rectangular window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC3cExpected,
			         freqsRC3Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, no detrending -----------------------
  //
  fftLength = dataLength/2;
  overlapLength = dataLength/4;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(hw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsRC4[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsRC4Expected(freqsRC4,fftLength);

  // expected output (generated by csd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  std::complex<T> csdRC4a[] = { 
    std::complex<T>(-2.717558833333331e-01, 7.188523833333313e-02),
    std::complex<T>(-3.023732616666664e-01, 7.000169266666663e-01),
    std::complex<T>(-8.403837933333330e-01, 5.808864449999996e-01),
    std::complex<T>(-8.097664149999996e-01, -4.724524333333351e-02)
  };
 
  std::valarray<std::complex<T> > csdRC4aExpected(csdRC4a,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, hann window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC4aExpected,
			         freqsRC4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, mean detrending ----------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  std::complex<T> csdRC4b[] = { 
    std::complex<T>(-2.717558833333331e-01, 7.188523833333320e-02),
    std::complex<T>(-1.089855320833332e-01, 5.188751041666665e-01),
    std::complex<T>(-3.001626558333331e-01, 3.854449508333332e-01),
    std::complex<T>(-4.629330070833331e-01, -6.154491500000011e-02)
  };
  
  std::valarray<std::complex<T> > csdRC4bExpected(csdRC4b,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, hann window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC4bExpected,
			         freqsRC4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, linear detrending -------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  std::complex<T> csdRC4c[] = { 
    std::complex<T>(4.707966240000021e-02, -6.759830520000019e-02),
    std::complex<T>(1.443388385333335e-01, 4.703490800666664e-01),
    std::complex<T>(-3.001626558333332e-01, 3.854449508333332e-01),
    std::complex<T>(-3.974218319666666e-01, -1.525024344333334e-01)
  };
  
  std::valarray<std::complex<T> > csdRC4cExpected(csdRC4c,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, hann window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC4cExpected,
			         freqsRC4Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, no detrending ---------------------
  //
  fftLength = dataLength/2;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welchCSD.set_fftLength(fftLength);
  welchCSD.set_overlapLength(overlapLength);
  welchCSD.set_window(kw);
  welchCSD.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsRC5[] = {
      -0.50000000000000,
      -0.25000000000000,
      0.0,
      0.25000000000000,
  };

  std::valarray<T> freqsRC5Expected(freqsRC5,fftLength);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC5a[] = { 
    std::complex<T>(-1.891537280952732e-02, -4.243655286345693e-02),
    std::complex<T>(-5.580251024436973e-01, 8.019222678042145e-01),
    std::complex<T>(-7.960545645263756e-01, 4.109628913260871e-01),
    std::complex<T>(-8.517474987875221e-01, 1.272331597541891e-01)
  };
  
  std::valarray<std::complex<T> > csdRC5aExpected(csdRC5a,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, kaiser window, no detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC5aExpected,
			         freqsRC5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC5b[] = { 
    std::complex<T>(-1.891537280952732e-02, -4.243655286345692e-02),
    std::complex<T>(-4.035673333411450e-01, 6.775984125172205e-01),
    std::complex<T>(-1.663858722854296e-01, 2.136594713435258e-01),
    std::complex<T>(-6.130511127721114e-01, 9.043629447038666e-02)
  };

  std::valarray<std::complex<T> > csdRC5bExpected(csdRC5b,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, kaiser window, mean detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC5bExpected,
			         freqsRC5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, linear detrending ------------------
  //
  detrendMethod = CSDEstimate::linear;
  welchCSD.set_detrendMethod(detrendMethod);

  // expected output (generated by csd function in matlab)
  //
  std::complex<T> csdRC5c[] = { 
    std::complex<T>(5.188079596972439e-02, -7.449190799593242e-02),
    std::complex<T>(9.417193848426464e-02, 5.873429868856118e-01),
    std::complex<T>(-1.663858722854296e-01, 2.136594713435258e-01),
    std::complex<T>(-4.902189216264648e-01, -8.451961241306791e-02)
  };

 
  std::valarray<std::complex<T> > csdRC5cExpected(csdRC5c,fftLength);

  pass = testValidApply<T, T, std::complex<T> >
	                        ("REAL-COMPLEX input: overlapping segments, kaiser window, linear detrending",
		                 welchCSD,
			         xInReal,
			         yInComp,
			         csdRC5cExpected,
			         freqsRC5Expected);
  allPass = allPass && pass;

	  
  return allPass;

}

bool testMetaDataCopy()
{
  TimeSeries<double> x(Sequence<double>(10), 1);
  TimeSeries<double> y(Sequence<double>(10), 1);
  udt* out = 0;

  WelchCSDEstimate csd;

  csd.set_fftLength(10);
  csd.set_overlapLength(0);

  x.SetName("x");
  y.SetName("y");

  csd.apply(out, x, y);

  bool pass = out->name() == "csd(x,y)";
  Test.Check(pass) << out->name() << "=?=csd(x,y)" << std::endl;
  return pass;
}

//------------------------------------------------------------------------
//
int main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);

  try {
    
    Test.Message()
      << "$Id: tCSDEstimate.cc,v 1.24 2006/02/16 16:56:15 emaros Exp $"
      << std::endl << std::endl;
    
    //------------------------------------------------------------------------
    // test all error exceptions
    
    Test.Check(testErrors()) << "(all error exception tests)" 
                             << std::endl;
    
    //  testApplyInSize0<float>();
    
    //------------------------------------------------------------------------
    // test valid welchCSD estimate objects
    
    Test.Check(testAllValidObjects()) << "(all valid welchCSD estimate objects tests)" 
                                      << std::endl;
    
    //------------------------------------------------------------------------
    // test valid set methods for welchCSD estimate objects
    
    
    Test.Check(testAllValidSetMethods()) << "(all valid set methods tests)" 
                                         << std::endl;
    
    //-----------------------------------------------------------------------
    // test apply methods for valid data and valid welchCSD estimate objects
    
    Test.Check(testAllValidApply<float>()) << "(apply methods on valid data test <float>)" 
                                           << std::endl;
    
    Test.Check(testAllValidApply<double>()) << "(apply methods on valid data test <double>)" 
                                            << std::endl;
    //---------------------------------------------------------------------
    
    Test.Check(testMetaDataCopy()) << "(metadata copied from input to output)"
                                   << std::endl;
    
    // all done!!
  }
  catch(const std::exception& e)
  {
    Test.Check(false) << "Caught exception: " << e.what() << endl;
  }

  Test.Exit();

  return 0;
}
