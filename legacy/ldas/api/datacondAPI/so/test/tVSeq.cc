//! author="Lee Samuel Finn"
// Test code for datacondAPI::VectorSequence UDT object

#include "datacondAPI/config.h"

#include <iostream>
#include <stdexcept>
#include <valarray>
#include <memory>
#include <typeinfo>

#include "general/unittest.h"

#include "UDT.hh"
#include "SequenceUDT.hh"
#include "VectorSequenceUDT.hh"

using namespace std;

General::UnitTest	Test;

int main(int ArgC, char** ArgV) {  

  // Tests (to be) performed
  // 
  // Constructors:
  //    VectorSequence(void);
  //    VectorSequence(const int vdim, const int sdim);
  //    VectorSequence(const VectorSequence<DataType_>);
  //    udt* Clone(void) const;

  // Accessors: 
  //   vDim();
  //   sDim();
  //   vec(int);
  //   seq(int);

  // Mutators:
  //   resize(const int, const int); 
  //   udt_slice_array<DataType_>& vec(int);
  //   udt_slice_array<DataType_>& seq(int);
  //   VectorSequence<DataType_>& operator=(const DataType_)


  Test.Init(ArgC, ArgV);
  if (Test.IsVerbose())
  {
    cout << "$Id: tVSeq.cc,v 1.10 2005/12/01 22:55:03 emaros Exp $" 
	 << std::endl << std::flush;
  }

  // catch any unexpected exceptions

  try {

    // datacondAPI::VectorSequence(void) 
    datacondAPI::VectorSequence<float> vs0;

    Test.Check(vs0.vDim() == 0)
      << "vs(void) constructor (vdim), vDim" << std::endl << std::flush; 
    Test.Check(vs0.sDim() == 0)
      << "vs(void) constructor (sdim), sDim" << std::endl << std::flush; 

    // datacondAPI::VectorSequence(vdim,sdim) 
    const int vdim(5);
    const int sdim(3);
    datacondAPI::VectorSequence<float> vs2(vdim,sdim);   

    Test.Check(vs2.vDim() == vdim) 
      << "vs(j,k) constructor (vdim), vDim" << std::endl << std::flush; 
    Test.Check(vs2.sDim() == sdim) 
      << "vs(j,k) constructor (sdim), sDim" << std::endl << std::flush; 

    // assign, retrieve through vec(), retrieve through seq()
    bool tvalue = true;
    {
      std::valarray<float> v(vdim*sdim);
      for (int k = 0; k < vdim*sdim; k++) 
	v[k] = k;
      for (int k = 0; k < sdim; k++)
	{
	  datacondAPI::Sequence<float> t(vdim);
	  t = static_cast< const std::valarray<float> >(v)[slice(k*vdim,vdim,1)];     // assign through vec()
	  vs2.vec(k) = t;
	}
      for (int k = 0; k < sdim; k++)
	{
	  std::valarray<float> t(vdim);
	  t = vs2.vec(k);
	  for (int j = 0; j < vdim; j++)
	    tvalue &= t[j] == v[k*vdim+j]; // verify through vec()
	}
      Test.Check(tvalue)
	<< "assign, retrieve through vec()" 
	<< std::endl << std::flush;
      tvalue = true;
      for (int k = 0; k < vdim; k++)
	{
	  std::valarray<float> t(sdim);
	  t = vs2.seq(k);
	  for (int j = 0; j < sdim; j++)
	    tvalue &= t[j] == v[j*vdim+k];
	}
      Test.Check(tvalue)
	<< "Retrieve through seq()" 
	<< std::endl << std::flush;
    }

    // datacondAPI::VectorSequence<>(datacondAPI::VectorSequence<>)
    datacondAPI::VectorSequence<float> vsvs(vs2);
    Test.Check(vs2.vDim() == vdim) 
      << "vs(vs) constructor (vdim)" << std::endl << std::flush; 
    Test.Check(vs2.sDim() == sdim) 
      << "vs(vs) constructor (sdim)" << std::endl << std::flush; 
    tvalue = true;
    for (int k = 0; k < sdim; k++)
      {
	std::valarray<float> a(vdim);
	std::valarray<float> b(vdim);
	a = vsvs.vec(k);
	b = vs2.vec(k);
	for (int j = 0; j < vdim; j++)
	  tvalue &= (a[k] == b[k]);
      }
    Test.Check(tvalue)
      << "vs(vs) constructor (values), vec()" 
      << std::endl << std::flush;

    // datacondAPI::VectorSequence.resize(vdim,sdim)
    vs0.resize(vdim,sdim);
    Test.Check(vs0.vDim() == vdim) 
      << "VectorSequence.resize(vdim,sdim) (vDim)" 
      << std::endl << std::flush; 
    Test.Check(vs0.sDim() == sdim) 
      << "VectorSequence.resize(vdim,sdim) (sDim)" 
      << std::endl << std::flush;

    // Assignment through datacondAPI::VectorSequence.seq()
      for (int k = 0; k < vdim; k++)
	{
	  std::valarray<float> t(sdim);
	  t = vs2.seq(k);
	  vs0.seq(k) = t;
	}
      tvalue = true;
      for (int k = 0; k < sdim; k++)
	{
	  std::valarray<float> a(vdim);
	  std::valarray<float> b(vdim);
	  a = vs2.vec(k);
	  b = vs0.vec(k);
	  for (int j = 0; j < vdim; j++)
	    tvalue &= a[j] == b[j];
	}
    Test.Check(tvalue)
      << "Assignment through VectorSequence.seq()"
      << std::endl << std::flush;

    // Clone()
    std::auto_ptr<datacondAPI::udt> apUDT(vs0.Clone());
    Test.Check(apUDT.get() != 0)
      << "Clone() memory allocated" << std::endl << std::flush;

    // try to downcast:

    tvalue = typeid(*apUDT) == typeid(datacondAPI::VectorSequence<float>);
    Test.Check(tvalue)
      << "typeid(*apUDT) == typeid(datacondAPI::VectorSequence<float>)"
      << std::endl << std::flush;
    try 
      {
	(void)datacondAPI::udt::Cast< datacondAPI::VectorSequence<float> >(*apUDT);
	Test.Check(true)
	  << "downcast *apUDT"
	  << std::endl << std::flush;
      }
    catch (std::bad_cast& r)
      {
	Test.Check(false)
	  << "downcast *apUDT"
	  << std::endl << std::flush;
      }
	
    // Test for some exceptions

    try {
      datacondAPI::VectorSequence<float> vx(-1,1);
      Test.Check(false) << "VectorSequence<float>(-1,1)" 
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "VectorSequence<float>(-1,1): " 
		       << r.what() << std::endl << std::flush;
    }
      
    try {
      datacondAPI::VectorSequence<float> vx(1,-1);
      Test.Check(false) << "VectorSequence<float>(1,-1)" 
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "VectorSequence<float>(1,-1): " 
		       << r.what() << std::endl << std::flush;
    }
      
    try {
      datacondAPI::VectorSequence<float> vx(1,-1);
      Test.Check(false) << "VectorSequence<float>(1,-1)" 
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "VectorSequence<float>(1,-1): " 
		       << r.what() << std::endl << std::flush;
    }
      
    try {
      datacondAPI::VectorSequence<float> vx;
      vx.resize(0,1);
      Test.Check(false) << "vx.resize(0,1)"
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "vx.resize(0,1): "
		       << r.what() << std::endl << std::flush;
    }
      
    try {
      datacondAPI::VectorSequence<float> vx;
      vx.resize(1,0);
      Test.Check(false) << "vx.resize(1,0)"
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "vx.resize(1,0): "
		       << r.what() << std::endl << std::flush;
    }
      
    try {
      datacondAPI::VectorSequence<float> vx;
      vx.resize(-1,1);
      Test.Check(false) << "vx.resize(-1,1)"
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "vx.resize(-1,1): "
		       << r.what() << std::endl << std::flush;
    }
      
    try {
      datacondAPI::VectorSequence<float> vx;
      vx.resize(1,-1);
      Test.Check(false) << "vx.resize(1,-1)"
			<< std::endl << std::flush;
    } catch (std::invalid_argument& r) {
      Test.Check(true) << "vx.resize(1,-1): "
		       << r.what() << std::endl << std::flush;
    }

  } catch (std::exception& r) {
    Test.Check(false) << "Caught unexpected exception: " 
		      << r.what() << std::endl << std::flush;
  }
  Test.Exit();
}

