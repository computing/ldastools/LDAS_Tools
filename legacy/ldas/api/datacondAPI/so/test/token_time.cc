#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

using namespace std;      
   
General::UnitTest	Test;

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_time.cc,v 1.5 2005/12/01 22:55:03 emaros Exp $" << endl;

  try {

    CallChain				cmds;
    Parameters				args;
    CallChain::Symbol*			result;
    datacondAPI::Sequence<double>*	seq_result;

    cmds.AppendCallFunction("cpu_time", args.set(0), "ct");
    cmds.AppendCallFunction("user_time", args.set(0), "ut");
    cmds.AppendCallFunction("wall_time", args.set(0), "wt");
    cmds.AppendCallFunction("time", args.set(0), "t");

    cmds.AppendIntermediateResult("t", "result", "Final Result", "" );

    cmds.Execute();

    result = cmds.GetSymbol("ct");
    Test.Message() << "cpu_time: "
		   << dynamic_cast<datacondAPI::Scalar<double>& >(*result).GetValue()
		   << endl << flush;

    result = cmds.GetSymbol("ut");
    Test.Message() << "user_time: "
		   << dynamic_cast<datacondAPI::Scalar<double>& >(*result).GetValue()
		   << endl << flush;

    result = cmds.GetSymbol("wt");
    Test.Message() << "wall_time: "
		   << dynamic_cast<datacondAPI::Scalar<double>& >(*result).GetValue()
		   << endl << flush;

    result = cmds.GetSymbol("t");
    seq_result = dynamic_cast<datacondAPI::Sequence<double>*>(result);
    Test.Message() << "time: "
		   << (*seq_result)[0] << "s "
		   << (*seq_result)[1] << "u "
		   << (*seq_result)[2] << "r "
		   << endl << flush;
  }
  CATCH(Test);

  Test.Exit();
}
