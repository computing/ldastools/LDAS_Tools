//! author="Edward Maros"
// Test code for timing functions
// $Id: pr1501.cc,v 1.2 2005/12/01 22:55:02 emaros Exp $

#include "datacondAPI/config.h"

#include <iostream>
#include <memory>

#include "general/unittest.h"

#include "token.hh"

#include "CallChain.hh"

#include <general/util.hh>
#include <genericAPI/registry.hh>

General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
    int main(int argc, char** argv);
#ifdef __cplusplus
}
#endif	/* __cplusplus */

void pr1501()
{
    const ILwd::LdasElement*	element(read_ilwd("/ldas_outgoing/jobs/LDAS-PCTEST_0/LDAS-PCTEST1877/_0818a4a0_LdasContainer_p_input_dump.ilwd", ""));

    CallChain	cmds;
    Parameters	args;
    const char* ch0( "H2\\:LSC-AS_Q::AdcData:693960000:0:Frame" );
    const char* ch1( "H2\\:LSC-AS_I::AdcData:693960000:0:Frame" );
  
    cmds.Reset();
    cmds.SetSingleFrameMode( true );

    cmds.SetReturnTarget("frame");

    cmds.IngestILwd( *element );

    delete element;
  
    cmds.AppendCallFunction("integer", args.set(1, "0"), "i0");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "i1");
    cmds.AppendCallFunction("integer", args.set(1, "64"), "i64");
    cmds.AppendCallFunction("integer", args.set(1, "16384"), "i16384");
  
    // x = slice(ch0,0,16384,1)
    cmds.AppendCallFunction("slice",
			    args.set(4, ch0, "i0", "i64", "i1"), 
			    "x");
    // output(x,_,frame,x,time series 1);
    cmds.AppendIntermediateResult("x",
				  "x",
				  "time series 1",
				  "frame");
    // output(x,_,frame,x,time series 1);
    cmds.AppendIntermediateResult("x",
				  "x",
				  "time series 1",
				  "frame");

#if 0
    // x = slice(ch0,16384,16384,1)
    cmds.AppendCallFunction("slice",
			    args.set(4, ch1, "i16384", "i16384", "i1"), 
			    "x");
    // p = psd(x,16384)
    cmds.AppendCallFunction("psd",
			    args.set(2, "x", "i16384"), 
			    "p");
    // output(x,_,frame,x,time series 1);
    cmds.AppendIntermediateResult("p",
				  "p",
				  "psd",
				  "frame");
#endif /* 0 */
    cmds.Execute();

    ILwd::LdasContainer* results( cmds.GetResults( ) );
    
    results->write( 2, 2, std::cerr, ILwd::ASCII );

    Registry::elementRegistry.destructObject( results );
}

int
main(int argc, char** argv)
{
  try {
    pr1501( );
    Test.Check(true) << endl;
  }
  catch(bad_alloc& e)
  {
    Test.Check(true)  << ": caught bad_alloc" << endl;
  }
  catch(exception& e)
  {
    Test.Check(false) << ": caught exception " << e.what() << endl;
  }
  catch(...)
  {
    Test.Check(false) << ": caught unexpected!" << endl;
  }
  Test.Exit();
  //-----------------------------------------------------------------------
  // Should never get here, but if we do, consider it a failure.
  //-----------------------------------------------------------------------
  return 1;
}
