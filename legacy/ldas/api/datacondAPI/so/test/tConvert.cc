//
// Test suite for ConvertToIlwd, mainly to check for memory leaks
//

#include "datacondAPI/config.h"

#include <map>

#include "general/unittest.h"

#include "CallChain.hh"
#include "Statistics.hh"
#include "StatsUDT.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchCSDEstimate.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

using namespace std;
using namespace datacondAPI;

General::UnitTest Test;

std::map<udt::target_type,std::string> target_map;

void
TestConvertToIlwd(const udt& in, const udt::target_type target)
{
    CallChain cmds;

    ILwd::LdasElement* ilwd = in.ConvertToIlwd(cmds, target);
    ILwd::LdasContainer* c;

    Test.Message() << "Dumping: " << ilwd->getName( 0 )
		   << " target type: " << target_map[target]
		   << endl;
    if ( ( c = dynamic_cast< ILwd::LdasContainer* >(ilwd) ) )
    {
      c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
    }
    else
    {
      ilwd->write(Test.Message(), ILwd::ASCII);
    }
    Test.Message(false) << endl;

    delete ilwd;
}

void
TestScalar()
{
    Scalar<float> x(2.0);
    
    TestConvertToIlwd(x, udt::TARGET_GENERIC);
    // TestConvertToIlwd(x, udt::TARGET_METADATA);
    // TestConvertToIlwd(x, udt::TARGET_WRAPPER);
}

void
TestSequence()
{
    {
	Sequence<float> x;
	
	TestConvertToIlwd(x, udt::TARGET_GENERIC);
	// TestConvertToIlwd(x, udt::TARGET_METADATA);
	// TestConvertToIlwd(x, udt::TARGET_WRAPPER);
    }

    {
	Sequence<float> x(2.0, 8);
	
	TestConvertToIlwd(x, udt::TARGET_GENERIC);
	// TestConvertToIlwd(x, udt::TARGET_METADATA);
	// TestConvertToIlwd(x, udt::TARGET_WRAPPER);
    }
}

void
TestStats()
{
    const Sequence<float> a(2.0, 8);
    Statistics stats;
    Stats x;
    
    stats.all(x, a);
    
    TestConvertToIlwd(x, udt::TARGET_GENERIC);
    TestConvertToIlwd(x, udt::TARGET_METADATA);
}

void
TestTimeSeries()
{
    {
	TimeSeries<float> b;
	
	TestConvertToIlwd(b, udt::TARGET_GENERIC);
	// TestConvertToIlwd(b, udt::TARGET_METADATA);
	TestConvertToIlwd(b, udt::TARGET_WRAPPER);
    }

    {
	TimeSeries<float> b;
	
	b.SetSampleRate(2048.0);
	b.resize(0);
	b = 3.0;
	
	TestConvertToIlwd(b, udt::TARGET_GENERIC);
	// TestConvertToIlwd(b, udt::TARGET_METADATA);
	TestConvertToIlwd(b, udt::TARGET_WRAPPER);
    }
}

void
TestWelchSpectrum()
{
    TimeSeries<float> b;

    b.SetSampleRate(2048.0);
    b.resize(1024);
    b = 3.0;
    
    WelchSpectrum<float> x;
    WelchCSDEstimate w;
    udt* xp = &x;

    w.apply(xp, b);

    TestConvertToIlwd(x, udt::TARGET_GENERIC);
    TestConvertToIlwd(x, udt::TARGET_METADATA);
    TestConvertToIlwd(x, udt::TARGET_WRAPPER);
}

void
TestWelchCSDSpectrum()
{
    // For some reason there is an internal compiler error with this
    // TimeSeries<float> b(a, 2048.0);
    TimeSeries<float> b;
    
    b.SetSampleRate(2048.0);
    b.resize(1024);
    b = 2.0;

    WelchCSDSpectrum<std::complex<float> > x;
    WelchCSDEstimate w;
    udt* xp = &x;

    w.apply(xp, b, b);

    TestConvertToIlwd(x, udt::TARGET_GENERIC);
    TestConvertToIlwd(x, udt::TARGET_METADATA);
    TestConvertToIlwd(x, udt::TARGET_WRAPPER);
}

int
main(int argc, char** argv)
{
  Test.Init(argc, argv);

  Test.Message() << "$Id: tConvert.cc,v 1.11 2005/12/01 22:55:02 emaros Exp $"
                 << endl;

  target_map[udt::TARGET_GENERIC] = "TARGET_GENERIC";
  target_map[udt::TARGET_METADATA] = "TARGET_METADATA";
  target_map[udt::TARGET_WRAPPER] = "TARGET_WRAPPER";

  try {
      Test.Message() << "Scalar" << endl;
      TestScalar();

      Test.Message() << "Sequence" << endl;
      TestSequence();

      Test.Message() << "StatsUDT" << endl;
      TestStats();

      Test.Message() << "TimeSeries" << endl;
      TestTimeSeries();

      Test.Message() << "WelchSpectrum" << endl;
      TestWelchSpectrum();

      Test.Message() << "WelchCSDSpectrum" << endl;
      TestWelchCSDSpectrum();
  }
  catch(std::exception& e)
  {
      Test.Check(false) << "Caught exception <" << e.what() << ">" << endl;
  }
  catch(...)
  {
      Test.Check(false) << "Caught unknown exception" << endl;
  }

  Test.Exit();
}

