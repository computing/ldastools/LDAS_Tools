#include "datacondAPI/config.h"

#include <complex>

#include "general/unittest.h"	// Needed for doing "make check"

#include "filters/LDASConstants.hh"	// Constants

#include "token.hh"		// Common header for token testing programs.
#include "ScalarUDT.hh"	// Needed for converting CallChain results.
#include "TimeSeries.hh"	// Needed for converting CallChain results.


General::UnitTest Test;	// Class supporting testing
using namespace datacondAPI;
using std::complex;
using std::endl;

template<class T>
void testReverse(const size_t n)
{
    TimeSeries<T> x;
    x.resize(n);
    for (size_t k = 0; k < x.size(); ++k)
    {
        x[k] = k;
    }

    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.ResetOrAddSymbol("x", x.Clone());

    // y = reverse(x);
    cmds.AppendCallFunction("reverse", args.set(1, "x"),
                            "y");
    
    cmds.AppendIntermediateResult("y", "result", "Final Result", "");
    
    cmds.Execute();
    
    Test.Check(true) << "CallChain executed" << std::endl;

    const udt* const yUdt = cmds.GetSymbol("y");
    
    const TimeSeries<T>& y
        = dynamic_cast<const TimeSeries<T>&>(*yUdt);

    bool pass = true;
    for (size_t k = 0; k < y.size(); ++k)
    {
        pass = pass && (y[k] == x[x.size() - 1 - k]);
    }

    Test.Check(pass)
        << "Sequence reversed correctly" << std::endl;

}

template<class T>
void testReverseException()
{
    Scalar<T> x(1.0);

    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.ResetOrAddSymbol("x", x.Clone());

    // y = reverse(x);
    cmds.AppendCallFunction("reverse", args.set(1, "x"),
                            "y");
    
    cmds.AppendIntermediateResult("y", "result", "Final Result", "");
    
    try {
        cmds.Execute();
        Test.Check(false) << "No exception thrown" << endl;
    }
    catch(const std::exception& e)
    {
        Test.Check(true) << "Caught exception:" << e.what() << endl;
    }
}

int
main(int ArgC, char** ArgV)
{
  using namespace datacondAPI;

  Test.Init(ArgC, ArgV);

  testReverse<float>(8);
  testReverse<float>(17);

  testReverse<double>(8);
  testReverse<double>(17);

  testReverse<complex<float> >(8);
  testReverse<complex<float> >(17);

  testReverse<complex<double> >(8);
  testReverse<complex<double> >(17);

  testReverseException<float>();
  testReverseException<double>();
  testReverseException<complex<float> >();
  testReverseException<complex<double> >();

  Test.Message() << "reverse: end\n";
  Test.Exit();
}
