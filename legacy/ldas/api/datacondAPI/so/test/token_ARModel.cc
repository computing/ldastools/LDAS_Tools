#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

#include "ARModel.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

using namespace std;   
   
template<typename type>
static ostream& operator<<(ostream& a, const std::valarray<type>& b)
{
    a << "{ ";
    for (unsigned int i = 0; i < b.size(); ++i)
    {
	a << b[i] << ' ';
    }
    a << "}";
    return a;
}

using namespace datacondAPI;

General::UnitTest Test;

void test_specific()
{
    // x = dvalarray(2.0, 10)
    // b = arls(x, 1)
    // x1 = slice(x, 0, 5, 1)
    // b1 = arls(x1, 1, z)
    // x2 = slice(x, 5, 5, 1)
    // b2 = arls(x2, z)

    CallChain commands;
    Parameters arguments;

    commands.Reset();

    commands.AppendCallFunction("integer", arguments.set(1, "0"), "zero");
    commands.AppendCallFunction("integer", arguments.set(1, "1"), "one");
    commands.AppendCallFunction("integer", arguments.set(1, "5"), "five");
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "ten");
    commands.AppendCallFunction("double", arguments.set(1, "2.0"), "two");
    commands.AppendCallFunction("dvalarray", arguments.set(2, "two", "ten"), "x");
    commands.AppendCallFunction("arls", arguments.set(2, "x", "one"), "b");
    commands.AppendCallFunction("slice", arguments.set(4, "x", "zero", "five", "one"), "x1");
    commands.AppendCallFunction("arls", arguments.set(3, "x1", "one", "z"), "b1");
    commands.AppendCallFunction("slice", arguments.set(4, "x", "five", "five", "one"), "x2");
    commands.AppendCallFunction("arls", arguments.set(2, "x2", "z"), "b2");

    commands.AppendIntermediateResult("b2", "result", "Final Result", "" );

    commands.Execute();

    udt* b = commands.GetSymbol("b");
    Test.Check(udt::IsA<Sequence<double> >(*b), "b?");
    Test.Message() << udt::Cast<Sequence<double> >(*b) << '\n';
    udt* b1 = commands.GetSymbol("b1");
    Test.Check(udt::IsA<Sequence<double> >(*b1), "b1?");
    Test.Message() << udt::Cast<Sequence<double> >(*b1) << '\n';
    udt* b2 = commands.GetSymbol("b2");
    Test.Check(udt::IsA<Sequence<double> >(*b2), "b2?");
    Test.Message() << udt::Cast<Sequence<double> >(*b2) << '\n';

}

template<typename type>
void test_generic(const std::string& name)
{
}

int main(int argc, char* argv[])
try
{
    Test.Init(argc, argv);

    test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("std::complex<float>");
    test_generic<std::complex<double> >("std::complex<double>");

    Test.Exit();
}
catch (std::exception& x)
{
    Test.Check(false) << "exception: " << x.what() << '\n';
    Test.Exit();
}

