/* -*- mode: c++; c-basic-offset: 2; -*- */
//
// tPSDEstimate.cc:
//
// Test driver for psd estimation using Welch's method
//

#include "datacondAPI/config.h"

// system includes
#include <iostream>
#include <complex> 
#include <valarray>
#include <string>
#include <fstream>
#include <stdexcept>

// other includes
#include <general/unittest.h>
#include <filters/LDASConstants.hh>
#include <filters/RectangularWindow.hh>
#include <filters/HannWindow.hh>
#include <filters/KaiserWindow.hh>

#include "DataCondAPIConstants.hh"
#include "CSDEstimate.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDEstimate.hh"
#include "TimeSeries.hh"

#include "token.hh"

// the following tests are performed: (for T=float and double)
//
// test error exceptions in constructor 
// test error exceptions in set methods
// test error exceptions in real and complex apply and operator() methods
// test construction of valid welch estimate objects (default and custom)
// test set methods for valid parameters (fft length, overlap length, 
// window, detrend flag)
// test apply and operator() methods on various types of valid data:
// periodogram, non-overlapping segments, overlapping segments, rectangular,
// hann, and kaiser windows, mean and linear detrending, real and complex
// data, random real data (n == 1024)

using namespace datacondAPI;

General::UnitTest Test;

#define	WELCHCSDESTIMATE_APPLY_FUNC(V, T) \
void  (WelchCSDEstimate::* V)(datacondAPI::WelchSpectrum<T>& out, \
				const datacondAPI::Sequence<T>& in)

//-----------------------------------------------------------------------
// read single column input data from a file 
// (returns length of input data)
//
template<class T>
int readData(const char* fileName, 
	     T*          input)
{

  // attach source directory to file name
  std::string path;

  if (getenv("DATADIR"))
  {
    path  = getenv("DATADIR");
    path += "/";
  }
  path += fileName;

  std::ifstream inFile(path.c_str());

  // make sure that file exists
  if (inFile == 0) 
  {
    std::cout << "missing file: " << fileName << std::endl;
    abort();
  }

  int i = 0;
  while (1) 
  {
    inFile >> input[i]; // must try to read in data before generating an
                        // end-of-file flag.  works ok if file is empty.  
    if (inFile.eof()) 
    {
      return i;  
    }
    
    i++;
  }
}

//-----------------------------------------------------------------------
// read two column input data from a file 
// (returns length of input data)
//
template<class T>
int readData(const char* fileName, 
	     T*          x,
	     T*          y)
{

  // attach source directory to file name
  std::string path;

  if (getenv("DATADIR"))
  {
    path  = getenv("DATADIR");
    path += "/";
  }
  path += fileName;

  std::ifstream inFile(path.c_str());

  // make sure that file exists
  if (inFile == 0) 
  {
    std::cout << "missing file: " << fileName << std::endl;
    abort();
  }

  int i = 0;
  while (1) 
  {
    inFile >> x[i] >> y[i]; // must try to read in data before generating an
                            // end-of-file flag.  works ok if file is empty.  
    if (inFile.eof()) 
    {
      return i;  
    }
    
    i++;
  }
}

//------------------------------------------------------------------------
// test error exceptions in constructor
//
template<class T>
bool testConstructorError(const std::string& name,
			  const size_t  fftLength,
			  const size_t  overlapLength)
{

  // default hann window for tests 
  Filters::HannWindow window;

  bool pass = false;

  try
  {
    try
    {
      WelchCSDEstimate welch(fftLength,overlapLength,window);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in set methods
//
template<class T>
bool testSetError(const std::string&              name,
                  WelchCSDEstimate           welch,
		  void  (WelchCSDEstimate::* pset)(const size_t length),
		  const size_t  length)
{

  bool pass = false;

  try
  {
    try
    {
      (welch.*pset)(length);
    }
    catch (const std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  CATCH(Test)

  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in real apply and operator() methods
//
template<class T>
bool testApplyErrorR(const std::string&                  name,
		     WelchCSDEstimate               welch,
		     void  (WelchCSDEstimate::* pmf)
		     (datacondAPI::WelchSpectrum< T >& out,
		      const datacondAPI::Sequence< T >& in),
		     const size_t n)
{
  bool pass = false;

  // default input data for apply and operator() methods (all one's)
  Sequence<T> 		in(1,n);
  WelchSpectrum<T>	out;

  try
  {
    try
    {
      (welch.*pmf)(out, in);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in complex apply and operator() methods
//
template<class T>
bool testApplyErrorC(const std::string&                  name,
		     WelchCSDEstimate               welch,
		     void  (WelchCSDEstimate::* pmf)
		     (datacondAPI::WelchSpectrum<T>& out,
		      const datacondAPI::Sequence<std::complex<T> >& in),
		     const size_t n)
{

  // default input data for apply and operator() methods (all one's)
  Sequence<std::complex<T> > in(1,n);
  WelchSpectrum<T>	out;

  bool pass = false;

  try
  {
    try
    {
      (welch.*pmf)(out, in);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test all error exceptions (constructor, set methods, apply, operator()) 
//
template<class T>
bool testErrors(void)
{
  bool allPass = true;
  bool pass;

  // default welch estimate object 
  WelchCSDEstimate welchdefault;

  // custom welch estimate object (overlap length = 128)
  Filters::RectangularWindow rw;
  WelchCSDEstimate welchcustom(256,128,rw);

  size_t fftLength;
  size_t overlapLength;

  // constructors

  fftLength = 0;
  overlapLength = 0;
  pass = testConstructorError<T>("constructor: fft length = 0",
				 fftLength,
				 overlapLength);
  allPass = allPass && pass;
  
  fftLength = datacondAPI::MaximumFFTLength + 1;
  overlapLength = 0;
  pass =testConstructorError<T>("constructor: fft length > max fft length",
					       fftLength,
					       overlapLength);
  allPass = allPass && pass;

  fftLength = Filters::MaximumWindowLength + 1;
  overlapLength = 0;
  pass = testConstructorError<T>("constructor: fft length > max window length",
				 fftLength,
				 overlapLength);
  allPass = allPass && pass;

  fftLength = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  overlapLength = fftLength;
  pass = testConstructorError<T>("constructor: overlap length = fft length",
				 fftLength,
				 overlapLength);
  allPass = allPass && pass;

  fftLength = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  overlapLength = fftLength + 1;
  pass = testConstructorError<T>("constructor: overlap length > fft length",
				 fftLength,
				 overlapLength);
  allPass = allPass && pass;

  // set methods

  fftLength = 0;
  pass = testSetError<T>("set method: fftLength = 0",
			 welchdefault,
			 &WelchCSDEstimate::set_fftLength,
			 fftLength);
  allPass = allPass && pass;

  fftLength = datacondAPI::MaximumFFTLength + 1;
  pass = testSetError<T>("set method: fftLength > max fft length",
			 welchdefault,
			 &WelchCSDEstimate::set_fftLength,
			 fftLength);
  allPass = allPass && pass;

  fftLength = Filters::MaximumWindowLength + 1;
  pass = testSetError<T>("set method: fftLength > max window length",
			 welchdefault,
			 &WelchCSDEstimate::set_fftLength,
			 fftLength);
  allPass = allPass && pass;

  
  fftLength = 128;
  pass = testSetError<T>("set method: fftLength = overlap length",
			 welchcustom,
			 &WelchCSDEstimate::set_fftLength,
			 fftLength);
  allPass = allPass && pass;


  fftLength = 127;
  pass = testSetError<T>("set method: fftLength < overlap length",
			 welchcustom,
			 &WelchCSDEstimate::set_fftLength,
			 fftLength);
  allPass = allPass && pass;


  overlapLength = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  pass = testSetError<T>("set method: overlapLength = fft length",
			 welchdefault,
			 &WelchCSDEstimate::set_overlapLength,
			 overlapLength);
  allPass = allPass && pass;

  overlapLength = datacondAPI::WelchCSDEstimateDefaultFFTLength + 1;
  pass = testSetError<T>("set method: overlapLength > fft length",
			 welchdefault,
			 &WelchCSDEstimate::set_overlapLength,
			 overlapLength);
  allPass = allPass && pass;


  // apply and operator() methods

  // no input data
  size_t dataLength = 0;

  pass = testApplyErrorR<T>("real apply method: no input data",
			    welchdefault,
			    &WelchCSDEstimate::apply,
			    dataLength);
  allPass = allPass && pass;


  pass = testApplyErrorC<T>("complex apply method: no input data",
			    welchdefault,
			    &WelchCSDEstimate::apply,
			    dataLength);
  allPass = allPass && pass;

  // custom welch estimate object is applied to data that has a
  // length < fft length.  after fft length is reset to the length 
  // of the data, the welch estimate object is no longer valid 
  // since the fft length is now <= the overlap length 

  dataLength = 127;

  pass = testApplyErrorR<T>("real apply method: resized fft length < overlap length",
			    welchcustom,
			    &WelchCSDEstimate::apply,
			    dataLength);
  allPass = allPass && pass;

  pass = testApplyErrorC<T>("complex apply method: resized fft length < overlap length",
			    welchcustom,
			    &WelchCSDEstimate::apply,
			    dataLength);
  allPass = allPass && pass;

  dataLength = 128;

  pass = testApplyErrorR<T>("real apply method: resized fft length = overlap length",
			    welchcustom,
			    &WelchCSDEstimate::apply,
			    dataLength);
  allPass = allPass && pass;

  pass = testApplyErrorC<T>("complex apply method: resized fft length = overlap length",
			    welchcustom,
			    &WelchCSDEstimate::apply,
			    dataLength);
  allPass = allPass && pass;

  return allPass;
}

//------------------------------------------------------------------------
// test valid welch estimate objects
//
template <class T>
bool testValidObject(const std::string&            name,
		     const WelchCSDEstimate&  welch,
		     const size_t             fftLengthExpected,
		     const size_t             overlapLengthExpected,
		     const WindowInfo&        windowTypeExpected,
		     const CSDEstimate::DetrendMethod&       detrendMethodExpected)

{
  bool allPass = true;
  bool pass;

  // test estimate type
  pass = true;
  if (welch.what() != "Welch CSD Estimate")
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "what" << std::endl;
  allPass = allPass && pass;

  // test fft length
  pass = true;
  if (welch.fftLength() != fftLengthExpected) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "fft length" << std::endl;
  allPass = allPass && pass;

  // test overlap length
  pass = true;
  if (welch.overlapLength() != overlapLengthExpected) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "overlap length" << std::endl;
  allPass = allPass && pass;

  // test window type
  pass = true;
  if (welch.windowType() != windowTypeExpected)
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "window type" << std::endl;
  allPass = allPass && pass;

  // test detrend flag
  pass = true;
  if (welch.detrendMethod() != detrendMethodExpected)
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "detrend flag" << std::endl;
  allPass = allPass && pass;

  return allPass;
}

//------------------------------------------------------------------------
// test all types of valid welch estimate objects (default, custom, ...)
//
template <class T>
bool testAllValidObjects()
{
  size_t              fftLength;
  size_t              overlapLength;
  WindowInfo          windowType;
  CSDEstimate::DetrendMethod         detrendMethod;

  // different windows
  Filters::RectangularWindow rw;
  Filters::HannWindow        hw;
  Filters::KaiserWindow      kw(3); // beta = 3

  bool allPass = true;
  bool pass;

  // default 
  WelchCSDEstimate welchdefault;

  fftLength            = datacondAPI::WelchCSDEstimateDefaultFFTLength;
  overlapLength        = 0;
  windowType           = WindowInfo(hw.name(), hw.param());
  detrendMethod                = CSDEstimate::none;

  pass = testValidObject<T>("default object",
			    welchdefault,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;

  // custom (rectangular window)
  WelchCSDEstimate welchcustomRW1(256,128,rw); // default detrending

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(rw.name(), rw.param());
  detrendMethod        = CSDEstimate::none;

  pass = testValidObject<T>("custom rectangular window object, default detrending",
			    welchcustomRW1,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // custom (rectangular window)
  WelchCSDEstimate welchcustomRW2(256,128,rw,CSDEstimate::none);

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(rw.name(), rw.param());
  detrendMethod        = CSDEstimate::none;

  pass = testValidObject<T>("custom rectangular window object, no detrending",
			    welchcustomRW2,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // custom (hann window)
  WelchCSDEstimate welchcustomHW(256,128,hw,CSDEstimate::mean);

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(hw.name(), hw.param());
  detrendMethod        = CSDEstimate::mean;
  
  pass = testValidObject<T>("custom hann window object, mean detrending",
			    welchcustomHW,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;

  // custom (kaiser window)
  WelchCSDEstimate welchcustomKW(256,128,kw,CSDEstimate::linear);

  fftLength            = 256;
  overlapLength        = 128;
  windowType           = WindowInfo(kw.name(), kw.param());
  detrendMethod        = CSDEstimate::linear;

  pass = testValidObject<T>("custom kaiser window object, linear detrending",
			    welchcustomKW,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;

  return allPass;
}

//------------------------------------------------------------------------
// test all valid set methods for welch estimate objects
//
template <class T>
bool testAllValidSetMethods()
{
  bool allPass = true;
  bool pass;

  // different windows
  Filters::RectangularWindow rw;
  Filters::HannWindow        hw;
  Filters::KaiserWindow      kw(3); // beta = 3

  // default object
  WelchCSDEstimate welch;

  // default parameters
  //
  size_t      fftLength;
  size_t      overlapLength = 0;
  WindowInfo  windowType = WindowInfo(hw.name(), hw.param());
  CSDEstimate::DetrendMethod detrendMethod = CSDEstimate::none; 

  // set fft length
  //
  fftLength = 256;
  welch.set_fftLength(fftLength);

  pass = testValidObject<T>("set fft length",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // set overlap length
  //
  overlapLength = 128;
  welch.set_overlapLength(overlapLength);

  pass = testValidObject<T>("set overlap length",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // set window (rectangular)
  //
  welch.set_window(rw);
  windowType = WindowInfo(rw.name(), rw.param());

  pass = testValidObject<T>("set window (rectangular)",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // set window (kaiser)
  //
  welch.set_window(kw);
  windowType = WindowInfo(kw.name(), kw.param());
 
  pass = testValidObject<T>("set window (kaiser)",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;

  // set window (hann) (original default)
  //
  welch.set_window(hw);
  windowType = WindowInfo(hw.name(), hw.param());
 
  pass = testValidObject<T>("set window (hann)",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;

  
  // set detrend flag (CSDEstimate::mean)
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  pass = testValidObject<T>("set detrend flag (mean)",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // set detrend flag (CSDEstimate::linear)
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  pass = testValidObject<T>("set detrend flag (CSDEstimate::linear)",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  // set detrend flag (CSDEstimate::none) (original default)
  //
  detrendMethod = CSDEstimate::none;
  welch.set_detrendMethod(detrendMethod);

  pass = testValidObject<T>("set detrend flag (CSDEstimate::none)",
			    welch,
			    fftLength,
			    overlapLength,
			    windowType,
			    detrendMethod);
  allPass = allPass && pass;


  return allPass;
}

//-----------------------------------------------------------------------
// test apply and operator() methods on valid real data 
//
template <class T>
bool testValidApplyR(const std::string&       name,
		     WelchCSDEstimate    welch,
		     const Sequence<T>&  in,
		     const std::valarray<T>&	 psdExpected,
		     const std::valarray<T>&	 freqsExpected)
{
  bool pass = true;
  bool allPass = true;

  double tolerance = 1e-4;

  udt*			p_out((udt*)NULL);

  welch.apply(p_out, in);

  const WelchSpectrum<T>*	out(dynamic_cast<const WelchSpectrum<T>*>(p_out));

  // Test correct length
  pass = (psdExpected.size() == out->size());
  Test.Check(pass) << "real apply method (psd) - output has correct length: " << name << std::endl;
  allPass = allPass && pass;

  // test psd output of apply method
  for (size_t i = 0; i < psdExpected.size(); i++)
  {
    if (std::abs((*out)[i] - psdExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "real apply method (psd): " << name << std::endl;
  allPass = allPass && pass;

  // Test correct length
  pass = (freqsExpected.size() == out->size());
  Test.Check(pass) << "real apply method (freqs) - output has correct length: " << name << std::endl;
  allPass = allPass && pass;

  // test frequency output of apply method
  pass = true;
  for (size_t i = 0; i < freqsExpected.size(); i++)
  {
    if (std::abs(out->GetFrequency(i) - freqsExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "real apply method (freqs): " << name << std::endl;
  allPass = allPass && pass;

  return allPass;
}

template<class T>
bool testParsevalR(const Sequence<T>& in)
{
    const double tolerance = 1e-4;

    bool pass = false;

    double pwr_in = 0.0;
    for (size_t i = 0; i < in.size(); ++i)
    {
	pwr_in += in[i]*in[i];
    }
    
    WelchCSDEstimate welch(in.size(),
			   0,
			   Filters::RectangularWindow(),
			   CSDEstimate::none);
    WelchSpectrum<T> out;

    welch.apply(out, in);

    const double pwr_out = out.sum();

    pass = (std::abs(pwr_in - pwr_out) < tolerance);
    Test.Check(pass) << "real apply method: Parseval" << std::endl;

    return pass;
}

template<class T>
bool testParsevalC(const Sequence<std::complex<T> >& in)
{
    const double tolerance = 1e-4;

    bool pass = false;

    double pwr_in = 0.0;
    for (size_t i = 0; i < in.size(); ++i)
    {
	pwr_in += norm(in[i]);
    }
    
    WelchCSDEstimate welch(in.size(),
			   0,
			   Filters::RectangularWindow(),
			   CSDEstimate::none);
    WelchSpectrum<T> out;

    welch.apply(out, in);

    const double pwr_out = out.sum();

    pass = (std::abs(pwr_in - pwr_out) < tolerance);
    Test.Check(pass) << "complex apply method: Parseval" << std::endl;

    return pass;
}

template<class T>
bool testParsevalR(const TimeSeries<T>& in)
{
    const double tolerance = 1e-4;

    bool pass = false;
    bool allPass = true;

    std::valarray<T> in2 = in;
    in2 *= in2;
    const double pwr_in = in.GetStepSize()*in2.sum();
    
    WelchCSDEstimate welch(in.size(),
			   0,
			   Filters::RectangularWindow(),
			   CSDEstimate::none);
    WelchSpectrum<T> out;

    welch.apply(out, in);

    const double pwr_out = out.sum();

    pass = (std::abs(pwr_in - pwr_out) < tolerance);
    Test.Check(pass) << "real TimeSeries apply method: Parseval" << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(0) == 0.0);
    Test.Check(pass) << "real TimeSeries start frequency"
		     << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(out.size()-1) == in.GetSampleRate()/2);
    Test.Check(pass) << "real TimeSeries Nyquist frequency"
		     << std::endl;
    allPass = allPass & pass;

    return allPass;
}

template<class T>
bool testParsevalC(const TimeSeries<std::complex<T> >& in)
{
    const double tolerance = 1e-4;

    bool pass = false;
    bool allPass = true;

    double pwr_in = 0.0;
    for (size_t i = 0; i < in.size(); ++i)
    {
	pwr_in += norm(in[i]);
    }
    
    pwr_in *= in.GetStepSize();

    WelchCSDEstimate welch(in.size(),
			   0,
			   Filters::RectangularWindow(),
			   CSDEstimate::none);
    WelchSpectrum<T> out;

    welch.apply(out, in);

    const double pwr_out = out.sum();

    pass = (std::abs(pwr_in - pwr_out) < tolerance);
    Test.Check(pass) << "complex TimeSeries apply method: Parseval" << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(0) == -in.GetSampleRate()/2);
    Test.Check(pass) << "complex TimeSeries start frequency"
		     << std::endl;
    allPass = allPass & pass;

    pass = (out.GetFrequency(out.size()/2) == 0.0);
    Test.Check(pass) << "complex TimeSeries DC frequency"
		     << std::endl;
    allPass = allPass & pass;

    return allPass;
}

//-----------------------------------------------------------------------
// test apply and operator() methods on valid complex data 
//
template <class T>
bool testValidApplyC(const std::string&                 name,
		     WelchCSDEstimate              welch,
		     const Sequence<std::complex<T> >&  in,
		     const std::valarray<T>&            psdExpected,
		     const std::valarray<T>&            freqsExpected)
{
  bool pass;
  bool allPass = true;

  double tolerance = 1e-4;

  udt*			p_out((udt*)NULL);

  welch.apply(p_out, in);

  const WelchSpectrum<T>*	out(dynamic_cast<const WelchSpectrum<T>*>(p_out));

  // test psd output of apply method
  pass = true;
  for (size_t i = 0; i < welch.fftLength(); i++)
  {
    if (std::abs((*out)[i] - psdExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "complex apply method (psd): " << name << std::endl;
  allPass = allPass && pass;

  // test frequency output of apply method
  pass = true;
  for (size_t i = 0; i < welch.fftLength(); i++)
  {
    if (std::abs(out->GetFrequency(i) - freqsExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "complex apply method (freqs): " << name << std::endl;
  allPass = allPass && pass;

  return allPass;
}

//-----------------------------------------------------------------------
// test apply method on different valid sets of real and complex data
//
template <class T>
bool testAllValidApply()
{
  bool allPass = true;
  bool pass;

  // parameters
  size_t      dataLength;
  size_t      fftLength;
  size_t      overlapLength;
  size_t      psdLength;
  CSDEstimate::DetrendMethod detrendMethod;
  
  // windows
  Filters::RectangularWindow rw;
  Filters::HannWindow        hw;
  Filters::KaiserWindow      kw(3);  // beta parameter = 3

  // welch estimate object
  WelchCSDEstimate     welch;  // default


  // real data -------------------------------------------------------------
  //
  dataLength = 8;
  T dataR[] = {
      -0.7223, 
      -0.7215,
      -0.2012,
      -0.0205, 
      0.2789, 
      1.0583, 
      0.6217,
      -1.7506
  };

  Sequence<T> inR(dataR,dataLength);

  // std::valarrays for output psd and frequency values
  std::valarray<T> psdR;
  std::valarray<T> freqsR;

  // periodogram, no detrending ---------------------------------------------
  //
  fftLength = dataLength;
  psdLength = fftLength/2 + 1;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(rw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR1[] = {
      0,
      0.12500000000000,
      0.25000000000000,
      0.37500000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR1Expected(freqsR1, psdLength);

  // expected output (generated by psd function in matlab)
  //
  T psdR1a[] = {
      0.26542898000000,
      3.21701081762853,
      1.29739140500000,
      0.70323913237147,
      0.24900624500000
  };

  std::valarray<T> psdR1aExpected(psdR1a,psdLength);

  pass = testValidApplyR<T>("periodogram, no detrending",
			    welch,
			    inR,
			    psdR1aExpected,
			    freqsR1Expected);
  allPass = allPass && pass;

  pass = testParsevalR(inR);
  allPass = allPass && pass;

  TimeSeries<T> tsR1(inR, 2048.0);
  pass = testParsevalR(tsR1);
  allPass = allPass && pass;

  // periodogram, mean detrending ------------------------------------------
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR1b[] = {
      0,
      3.21701081762853,
      1.29739140500000,
      0.70323913237147,
      0.24900624500000
  };

  std::valarray<T> psdR1bExpected(psdR1b,psdLength);

  pass = testValidApplyR<T>("periodogram, mean detrending",
			    welch,
			    inR,
			    psdR1bExpected,
			    freqsR1Expected);
  allPass = allPass && pass;


  // periodogram, linear detrending -----------------------------------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR1c[] = {
      0.00000000000000,
      2.79151100864836,
      1.41855518826531,
      0.86652929155572,
      0.31308914938776
  };

  std::valarray<T> psdR1cExpected(psdR1c,psdLength);

  pass = testValidApplyR<T>("periodogram, linear detrending",
			    welch,
			    inR,
			    psdR1cExpected,
			    freqsR1Expected);
  allPass = allPass && pass;

 
  // non-overlapping segments, rectangular window, no detrending ------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(rw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR2[] = {
      0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR2Expected(freqsR2, psdLength);

  // expected output (generated by psd function in matlab)
  //
  T psdR2a[] = {
      0.35215989250000,
      2.19259431500000,
      0.32128408250000
  };

  std::valarray<T> psdR2aExpected(psdR2a,psdLength);

  pass = testValidApplyR<T>("non-overlapping segments, rectangular window, no detrending",
			    welch,
			    inR,
			    psdR2aExpected,
			    freqsR2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, mean detrending ----------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR2b[] = {
      0.00000000000000,
      2.19259431500000,
      0.32128408250000
  };

  std::valarray<T> psdR2bExpected(psdR2b,psdLength);

  pass = testValidApplyR<T>("non-overlapping segments, rectangular window, mean detrending",
			    welch,
			    inR,
			    psdR2bExpected,
			    freqsR2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, linear detrending ---------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR2c[] = {
      0.00000000000000,
      1.25197721700000,
      0.02512041800000
  };

  std::valarray<T> psdR2cExpected(psdR2c,psdLength);

  pass = testValidApplyR<T>("non-overlapping segments, rectangular window, linear detrending",
			    welch,
			    inR,
			    psdR2cExpected,
			    freqsR2Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, no detrending ----------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(rw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR3[] = {
      0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR3Expected(freqsR3,psdLength);

  // expected output (generated by psd function in matlab)
  //
  T psdR3a[] = {
      0.33846828250000,
      1.69411378500000,
      0.29100538916667
  };

  std::valarray<T> psdR3aExpected(psdR3a,psdLength);

  pass = testValidApplyR<T>("overlapping segments, rectangular window, no detrending",
			    welch,
			    inR,
			    psdR3aExpected,
			    freqsR3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, mean detrending
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR3b[] = {
      0.00000000000000,
      1.69411378500000,
      0.29100538916667
  };

  std::valarray<T> psdR3bExpected(psdR3b,psdLength);

  pass = testValidApplyR<T>("overlapping segments, rectangular window, mean detrending",
			    welch,
			    inR,
			    psdR3bExpected,
			    freqsR3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, linear detrending
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR3c[] = {
      0.00000000000000,
      0.86495674446667,
      0.01848744786667
  };

  std::valarray<T> psdR3cExpected(psdR3c, psdLength);

  pass = testValidApplyR<T>("overlapping segments, rectangular window, linear detrending",
			    welch,
			    inR,
			    psdR3cExpected,
			    freqsR3Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, no detrending ----------------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(hw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR4[] = {
      0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR4Expected(freqsR4,psdLength);

  // expected output (generated by psd function in matlab)
  //
  T psdR4a[] = {
      0.62342430833333,
      0.71525297666667,
      0.09182866833333
  };

  std::valarray<T> psdR4aExpected(psdR4a,psdLength);

  pass = testValidApplyR<T>("overlapping segments, hann window, no detrending",
			    welch,
			    inR,
			    psdR4aExpected,
			    freqsR4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR4b[] = {
      0.43016744125000,
      0.52199610958333,
      0.09182866833333
  };

  std::valarray<T> psdR4bExpected(psdR4b,psdLength);

  pass = testValidApplyR<T>("overlapping segments, hann window, mean detrending",
			    welch,
			    inR,
			    psdR4bExpected,
			    freqsR4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, linear detrending --------------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR4c[] = {
      0.43016744125000,
      0.45096582010000,
      0.02079837885000
  };

  std::valarray<T> psdR4cExpected(psdR4c,psdLength);

  pass = testValidApplyR<T>("overlapping segments, hann window, linear detrending",
			    welch,
			    inR,
			    psdR4cExpected,
			    freqsR4Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, no detrending ----------------------
  //
  fftLength = dataLength/2;
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(kw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsR5[] = {
      0,
      0.25000000000000,
      0.50000000000000
  };

  std::valarray<T> freqsR5Expected(freqsR5,psdLength);

  // expected output (generated by psd function in matlab)
  //
  T psdR5a[] = {
      0.50461352501861,
      0.99929899964525,
      0.02019563643407
  };

  std::valarray<T> psdR5aExpected(psdR5a, psdLength);

  pass = testValidApplyR<T>("overlapping segments, kaiser window, no detrending",
			    welch,
			    inR,
			    psdR5aExpected,
			    freqsR5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR5b[] = {
      0.23844999885967,
      0.88398434587559,
      0.02019563643407
  };

  std::valarray<T> psdR5bExpected(psdR5b,psdLength);

  pass = testValidApplyR<T>("overlapping segments, kaiser window, mean detrending",
			    welch,
			    inR,
			    psdR5bExpected,
			    freqsR5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, linear detrending ------------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdR5c[] = {
      0.23844999885967,
      0.63862463112876,
      0.02291937525911
  };

  std::valarray<T> psdR5cExpected(psdR5c,psdLength);

  pass = testValidApplyR<T>("overlapping segments, kaiser window, linear detrending",
			    welch,
			    inR,
			    psdR5cExpected,
			    freqsR5Expected);
  allPass = allPass && pass;


  // complex data -----------------------------------------------------------
  //
  dataLength = 8;
  std::complex<T> dataC[] = { std::complex<T>(-0.7223, 0.6363),
			 std::complex<T>(-0.7215, 1.3101),
			 std::complex<T>(-0.2012, 0.3271),
			 std::complex<T>(-0.0205,-0.6730),
			 std::complex<T>( 0.2789,-0.1493),
			 std::complex<T>( 1.0583,-2.4490),
			 std::complex<T>( 0.6217, 0.4733),
			 std::complex<T>(-1.7506, 0.1169) };

  Sequence<std::complex<T> > inC(dataC,dataLength);

  // std::valarrays for output psd and frequency values
  std::valarray<T> psdC;
  std::valarray<T> freqsC;

  // periodogram, no detrending ---------------------------------------------
  //
  fftLength = dataLength;
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(rw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC1[] = {
      -0.50000000000000,
      -0.37500000000000,
      -0.25000000000000,
      -0.12500000000000,
      0,
      0.12500000000000,
      0.25000000000000,
      0.37500000000000
  };

  std::valarray<T> freqsC1Expected(freqsC1,fftLength);

  // expected output (generated by psd function in matlab)
  //
  T psdC1a[] = { 1.36084496500000,
		 0.41070884843421,
		 0.41240593250000,
		 4.93006458778843,
		 0.28619620000000,
		 3.24529067656579,
		 0.99445432250000,
		 3.03084414721157 };

  std::valarray<T> psdC1aExpected(psdC1a,fftLength);

  pass = testValidApplyC<T>("periodogram, no detrending",
			    welch,
			    inC,
			    psdC1aExpected,
			    freqsC1Expected);
  allPass = allPass && pass;


  pass = testParsevalC(inC);
  allPass = allPass && pass;

  TimeSeries<std::complex<T> > tsC1(inC, 16384.0);
  pass = testParsevalC(tsC1);
  allPass = allPass && pass;

  // periodogram, mean detrending ------------------------------------------- 
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC1b[] = { 1.36084496500000,
		 0.41070884843421,
		 0.41240593250000,
		 4.93006458778843,
		 0.00000000000000,
		 3.24529067656579,
		 0.99445432250000,
		 3.03084414721157 };

  std::valarray<T> psdC1bExpected(psdC1b,fftLength);

  pass = testValidApplyC<T>("periodogram, mean detrending",
			    welch,
			    inC,
			    psdC1bExpected,
			    freqsC1Expected);
  allPass = allPass && pass;


  // periodogram, linear detrending -----------------------------------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC1c[] = { 0.93938281038549,
		 0.68089373786731,
		 0.23683967041383,
		 2.49372285364025,
		 0.00000000000000,
		 2.95794248640707,
		 1.90136147469955,
		 3.64440108349126 };

  std::valarray<T> psdC1cExpected(psdC1c,fftLength);

  pass = testValidApplyC<T>("periodogram, linear detrending",
			    welch,
			    inC,
			    psdC1cExpected,
			    freqsC1Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, no detrending ------------
  //
  fftLength = dataLength/2; // :NOTE: this is odd
  overlapLength = 0;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(rw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC2[] = { 
      -0.50000000000000,
      -0.2500000000000,
      0.000000000000,
      0.25
  };

  std::valarray<T> freqsC2Expected(freqsC2,fftLength);

  // expected output (generated by psd function in matlab)
  //
  T psdC2a[] = {
    1.216451445000000e+00,
    2.018325772500000e+00,
    1.176418125000000e+00,
    2.924209497500000e+00
  };
  std::valarray<T> psdC2aExpected(psdC2a,fftLength);

  pass = testValidApplyC<T>("non-overlapping segments, rectangular window, no detrending",
			    welch,
			    inC,
			    psdC2aExpected,
			    freqsC2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, mean detrending ----------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC2b[] = {
    1.216451445000000e+00,
    2.018325772500000e+00,
    3.582229696560259e-32,
    2.924209497500000e+00
  };

  std::valarray<T> psdC2bExpected(psdC2b,fftLength);

  pass = testValidApplyC<T>("non-overlapping segments, rectangular window, mean detrending",
			    welch,
			    inC,
			    psdC2bExpected,
			    freqsC2Expected);
  allPass = allPass && pass;


  // non-overlapping segments, rectangular window, linear detrending --------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC2c[] = {
    1.524130749599999e+00,
    5.483325627000000e-01,
    1.060224434385173e-31,
    1.900691749700000e+00
  };

  std::valarray<T> psdC2cExpected(psdC2c,fftLength);

  pass = testValidApplyC<T>("non-overlapping segments, rectangular window, linear detrending",
			    welch,
			    inC,
			    psdC2cExpected,
			    freqsC2Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, no detrending ---------------
  //
  fftLength = dataLength/2;    
  overlapLength = dataLength/4;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(rw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC3[] = {
      -0.50000000000000,
      -0.2500000000000,
      0.000000000000,
      0.25
  };

  std::valarray<T> freqsC3Expected(freqsC3,fftLength);

  // expected output (generated by psd function in matlab)
  //
  T psdC3a[] = {
    1.795173634166666e+00,
    1.799956595833333e+00,
    1.610333240833333e+00,
    2.290973319166667e+00
  };

  std::valarray<T> psdC3aExpected(psdC3a,fftLength);

  pass = testValidApplyC<T>("overlapping segments, rectangular window, no detrending",
			    welch,
			    inC,
			    psdC3aExpected,
			    freqsC3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, mean detrending --------------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC3b[] = {
    1.795173634166666e+00,
    1.799956595833333e+00,
    4.057292416175777e-32,
    2.290973319166667e+00
  };

  std::valarray<T> psdC3bExpected(psdC3b,fftLength);

  pass = testValidApplyC<T>("overlapping segments, rectangular window, mean detrending",
			    welch,
			    inC,
			    psdC3bExpected,
			    freqsC3Expected);
  allPass = allPass && pass;


  // overlapping segments, rectangular window, linear detrending ------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC3c[] = {
    1.269802973466666e+00,
    5.181299807666666e-01,
    8.071251408602839e-32,
    1.348598666766667e+00
  };

  std::valarray<T> psdC3cExpected(psdC3c,fftLength);

  pass = testValidApplyC<T>("overlapping segments, rectangular window, linear detrending",
			    welch,
			    inC,
			    psdC3cExpected,
			    freqsC3Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, no detrending -----------------------
  //
  fftLength = dataLength/2;      // :NOTE: this is odd
  overlapLength = dataLength/4;  // :NOTE: so is this
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(hw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC4[] = {
      -0.50000000000000,
      -0.2500000000000,
      0.000000000000,
      0.25
  };

  std::valarray<T> freqsC4Expected(freqsC4,fftLength);

  // expected output (generated by psd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  T psdC4a[] = {
    1.721893331666666e+00,
    1.030395951666666e+00,
    1.833422911666666e+00,
    2.524920291666666e+00
  };

  std::valarray<T> psdC4aExpected(psdC4a,fftLength);

  pass = testValidApplyC<T>("overlapping segments, hann window, no detrending",
			    welch,
			    inC,
			    psdC4aExpected,
			    freqsC4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, mean detrending ----------------------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  T psdC4b[] = {
    1.721893331666666e+00,
    6.678461227083328e-01,
    7.746389520833330e-01,
    1.828686161041666e+00
  };

  std::valarray<T> psdC4bExpected(psdC4b,fftLength);

  pass = testValidApplyC<T>("overlapping segments, hann window, mean detrending",
			    welch,
			    inC,
			    psdC4bExpected,
			    freqsC4Expected);
  allPass = allPass && pass;


  // overlapping segments, hann window, linear detrending -------------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //
  T psdC4c[] = {
    1.428528345150000e+00,
    4.787321341166665e-01,
    7.746389520833333e-01,
    1.724435163116667e+00
  };

  std::valarray<T> psdC4cExpected(psdC4c,fftLength);

  pass = testValidApplyC<T>("overlapping segments, hann window, linear detrending",
			    welch,
			    inC,
			    psdC4cExpected,
			    freqsC4Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, no detrending ---------------------
  //
  fftLength = dataLength/2;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::none;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(kw);
  welch.set_detrendMethod(detrendMethod);

  // expected frequency values
  //
  T freqsC5[] = {
      -0.50000000000000,
      -0.2500000000000,
      0.000000000000,
      0.25
  };

  std::valarray<T> freqsC5Expected(freqsC5,fftLength);

  // expected output (generated by psd function in matlab)
  //
  T psdC5a[] = {
    1.602346603818924e+00,
    1.189447872397856e+00,
    1.820447351890117e+00,
     2.538826088497907e+00
  };

  std::valarray<T> psdC5aExpected(psdC5a,fftLength);

  pass = testValidApplyC<T>("overlapping segments, kaiser window, no detrending",
			    welch,
			    inC,
			    psdC5aExpected,
			    freqsC5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, mean detrending -------------------
  //
  detrendMethod = CSDEstimate::mean;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC5b[] = {
    1.602346603818924e+00,
    9.402971830285823e-01,
    4.293971126782098e-01,
    2.114621419122250e+00
  };

  std::valarray<T> psdC5bExpected(psdC5b,fftLength);

  pass = testValidApplyC<T>("overlapping segments, kaiser window, mean detrending",
			    welch,
			    inC,
			    psdC5bExpected,
			    freqsC5Expected);
  allPass = allPass && pass;


  // overlapping segments, kaiser window, linear detrending ------------------
  //
  detrendMethod = CSDEstimate::linear;
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  //
  T psdC5c[] = {
    1.574208136456287e+00,
    4.629592319252137e-01,
    4.293971126782097e-01,
    1.806684430522573e+00
  };

  std::valarray<T> psdC5cExpected(psdC5c,fftLength);

  pass = testValidApplyC<T>("overlapping segments, kaiser window, linear detrending",
			    welch,
			    inC,
			    psdC5cExpected,
			    freqsC5Expected);
  allPass = allPass && pass;


  // (n == 1024, random real input data) ------------------------------------
  
  // read input data from a file
  dataLength = 1024;
  T dataR1024[1024];
  int count;
  count = readData<T>("tPSDEstimateWelchIn.dat", dataR1024);

  if ( count != ( sizeof( dataR1024 ) / sizeof( *dataR1024 ) ) )
  {
    allPass = allPass && false;
  }
  std::valarray<T> inR1024(dataR1024, dataLength);
 
  // overlapping segments, hann window, mean detrending ---------------------
  //
  fftLength = dataLength/4;   
  psdLength = fftLength/2 + 1;
  overlapLength = fftLength/2;
  detrendMethod = CSDEstimate::mean;

  welch.set_fftLength(fftLength);
  welch.set_overlapLength(overlapLength);
  welch.set_window(hw);
  welch.set_detrendMethod(detrendMethod);

  // expected output (generated by psd function in matlab)
  // :NOTE: had to modify the matlab definition of hanning(n) to agree
  // with the oppenheim and schafer definition, p.468
  //

  // read in expected output from a file
  T dataR1024psdExpected[129];
  T dataR1024freqsExpected[129];
  count = readData<T>("tPSDEstimateWelchOut.dat",
		      dataR1024freqsExpected,
		      dataR1024psdExpected);

  if ( count != ( sizeof( dataR1024psdExpected ) / sizeof( *dataR1024psdExpected ) ) )
  {
    allPass = allPass && false;
  }
 
  std::valarray<T> freqsR1024Expected(dataR1024freqsExpected, psdLength);
  std::valarray<T>   psdR1024Expected(dataR1024psdExpected,   psdLength);

  pass = testValidApplyR<T>("n == 1024, overlapping segments, hann window, mean detrending",
			    welch,
			    inR1024,
			    psdR1024Expected,
			    freqsR1024Expected);
  allPass = allPass && pass;


  return allPass;

}

//------------------------------------------------------------------------
//

// template <class T>
// bool testApplyInSize0(void)
// {
//   bool pass(true);
//   int FFTLength(1024);
//   int ovlLength(512);
//   WelchCSDEstimate psdEst(FFTLength,ovlLength);
//   std::string str;
//
//  try {
//    std::valarray<T> psd(0);
//    std::valarray<T> freqs(0);
//    std::valarray<T> in(0);
//    
//    psd.apply(psd,freqs,in);
//    pass = false;
//    str = "caught no exception";
//  } catch (std::invalid_argument& r) {
//    pass = true;
//    str = r.what();
//  } catch (std::exception &r) {
//    pass = false;
//    str = "caught unexpected exception";
//  }
//  Test.Check(pass) 
//    << "zero length input to apply() (" << str << ")" 
//    << std::endl << std::flush;
//
//  return pass;
//}

//------------------------------------------------------------------------
//
int main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);

  try {

#if 0
    // Quick timing test code...
      
    using namespace std;
    using namespace datacondAPI;
    
    const int trials = 100;
    const size_t N = 262144;
    Sequence<float> in(N);
    udt* out = 0;
    
    WelchCSDEstimate w(65536, 32768, Filters::RectangularWindow(), CSDEstimate::mean);
    
    for (int i = 0; i < trials; ++i)
    {
      w.apply(out, in);
    }
    
    Test.Exit();
#endif

    Test.Message()
      << "$Id: tPSDEstimate.cc,v 1.45 2005/12/01 22:55:03 emaros Exp $"
      << std::endl << std::endl;
    
    //------------------------------------------------------------------------
    // test all error exceptions
    
    Test.Check(testErrors<float>()) << "(all error exception tests <float>)" 
                                    << std::endl;
    
    Test.Check(testErrors<double>()) << "(all error exception tests <double>)" 
                                     << std::endl;
    
    //  testApplyInSize0<float>();
    
    //------------------------------------------------------------------------
    // test valid welch estimate objects
    
    Test.Check(testAllValidObjects<float>()) << "(all valid welch estimate objects tests <float>)" 
                                             << std::endl;
    
    Test.Check(testAllValidObjects<double>()) << "(all valid welch estimate objects tests <double>)" 
                                              << std::endl;
    
    //------------------------------------------------------------------------
    // test valid set methods for welch estimate objects
    
    
    Test.Check(testAllValidSetMethods<float>()) << "(all valid set methods tests <float>)" 
                                                << std::endl;
    
    Test.Check(testAllValidSetMethods<double>()) << "(all valid set methods tests <double>)" 
                                                 << std::endl;
    
    //-----------------------------------------------------------------------
    // test apply methods for valid data and valid welch estimate objects
    
    Test.Check(testAllValidApply<float>()) << "(apply methods on valid data test <float>)" 
                                           << std::endl;
    
    Test.Check(testAllValidApply<double>()) << "(apply methods on valid data test <double>)" 
                                            << std::endl;
    //---------------------------------------------------------------------
    // all done!!
    
  }
  catch (const std::exception& e)
  {
    Test.Check(false) << "Caught exception: " << e.what() << std::endl;
  }

  Test.Exit();

  return 0;
}
