#include "datacondAPI/config.h"

#include <cmath>
#include <iomanip>
#include <sstream>
#include <complex>   

#include <general/unittest.h>

#include "Resample.hh"
#include "TimeSeries.hh"
#include "InterpolateState.hh"

#include "token.hh"

using namespace std;   
using namespace General;
using namespace datacondAPI;

UnitTest Test;

// Tests that for alpha << 1, samples with the same time stamp have the
// the same vale
template<class T>
void TestTimeStamps(const size_t order)
{
    bool pass = true;
    const double offset = 1.0e-12;

#if 0
    valarray<T> b(T(0), order+1);
    Filters::designInterpolatingFilter(b, offset);

    for (size_t k = 0; k < b.size(); ++k)
    {
        cerr << "b[" << k << "] = " << b[k] << endl;
    }
#endif

    InterpolateState<T> state(offset, order);

    const size_t n = 50;
    const double fs = 1;
    const GPSTime initialTime(600000, 0);
    const GPSTime finalTime = initialTime + (offset - (order + 1)/2)/fs;
    
    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);
    TimeSeries<T> y(x);

    state.apply(y);

    Test.Check(y.GetStartTime() == finalTime)
        << "output has correct start-time for order = " << order
        << endl;

    for (size_t k = 0; k < (n - (order + 1)/2); ++k)
    {
        const GPSTime tk = x.GetStartTime() + k*x.GetStepSize();
        const int yIdx = int((tk - y.GetStartTime())/y.GetStepSize() + 0.5);

        const double Abs = abs(y[yIdx] - x[k]);

        if (Abs > 1.0e-09)
        {
            cerr << "Bad data at k = " << k << " yIdx = " << yIdx << endl;
            pass = false;
        }

#if 0
        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + yIdx*y.GetStepSize())
            << "  "
            << "y[" << yIdx << "] = " << setw(5) << y[yIdx]
            << "  "
            << "Abs = " << Abs
            << endl;
#endif

    }
    
    Test.Check(pass) << "Time-stamps are correct for order = " << order
                     << endl;
}

//
// This tests that we have a valid method for correcting fractional delays.
// We upsample by 9/8, giving delay = 90/8 = 11.25, and then attempt
// to correct for the fractional delay of 0.25  so that
//
// a) the tickmarks of the resulting time-series are aligned with the
// tick-marks of a reference time-series with sampling rate original*4/3
//
// b) the input data (a low-frequency sine wave) lines up in time with the
// orignal data
//
template<class T>
void TestResampleDelay(const size_t order)
{
    const int p = 9;
    const int q = 8;
    const int N = 10;

    Resample res(p, q, N);

    const double delay = res.getDelay();
    const double offset = (delay - floor(delay));

    const size_t n = 100*q;
    const double fs = double(q)/double(p);
    const GPSTime initialTime(6000000, 0);
    
    const double resFs = (fs*q)/p;
    const GPSTime finalTime = initialTime
        + (offset - order/2 - 1 - delay)/resFs;

    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);
    TimeSeries<T> y;

    const udt& xUdt = x;
    udt* yUdt = &y;

    res.apply(yUdt, xUdt);

    Test.Check(abs(y.GetSampleRate() - ((x.GetSampleRate()*p)/q)) < 1.0e-6)
        << "Resampled data sample rate is correct" << endl;

    Test.Check((y.GetStartTime() + delay*y.GetStepSize()) == x.GetStartTime())
        << "Resampled data start-time is correct" << endl;

    InterpolateState<T> state(offset, order);

    state.apply(y);

#if 0
    Test.Check(y.GetStartTime() == finalTime)
        << "output has correct start-time for order = " << order
        << endl;

    for (size_t k = 0; k < y.size(); ++k)
    {
        const GPSTime tk = x.GetStartTime() + k*x.GetStepSize();
        const int yIdx = int((tk - y.GetStartTime())/y.GetStepSize() + 0.5);

        const double Abs = abs(y[yIdx] - x[k]);

        if (Abs > 1.0e-09)
        {
            pass = false;
        }

        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + yIdx*y.GetStepSize())
            << "  "
            << "y[" << yIdx << "] = " << setw(5) << y[yIdx]
            << endl;

    }
#endif

#if 0
    for (size_t k = 0; k < min(x.size(), y.size()); ++k)
    {
        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + k*y.GetStepSize())
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << endl;
    }
    
    Test.Check(pass) << "Time-stamps are correct for order = " << order
                     << endl;
#endif
}

template<class T>
void TestValues()
{
    const double offset = .1;
    const size_t order = 1;

    InterpolateState<T> state(offset, order);

    const size_t n = 25;
    const double fs = 1;
    const GPSTime initialTime(6000000, 0);
    const GPSTime finalTime = initialTime + offset/fs;
    
    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);
    TimeSeries<T> y(x);

    state.apply(y);

    Test.Check(y.GetStartTime() == finalTime)
        << "output has correct start-time"
        << endl;

    cerr << "y.GetStartTime() = " << y.GetStartTime() << endl;
    cerr << "finalTime = " << finalTime << endl;

    for (size_t k = 0; k < n; ++k)
    {
        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + k*y.GetStepSize())
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << endl;
    }

}

template<class T>
void TestInterpolateState()
{
    const size_t maxOrder = 6;

    for (size_t order = 1; order <= maxOrder; ++order)
    {
        TestTimeStamps<T>(order);
    }

    for (size_t order = 1; order <= 1; ++order)
    {
        TestResampleDelay<T>(order);
    }


}

template<class T>
void TestInterpolateAction(const double alpha, const int order)
{
    const size_t n = 50;
    const double fs = 16384;
    const GPSTime initialTime(6000000, 0);
    const GPSTime finalTime = initialTime + alpha/fs;
    
    const int size = n/2;

    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x0(data, fs, initialTime);

    TimeSeries<T>* x = x0.Clone();
    x->SetName("H1:LSC-AS_Q");

    Scalar<double>* Alpha = new Scalar<double>(alpha);
    Scalar<int>* Order = new Scalar<int>(order);

    Scalar<int>* Zero = new Scalar<int>(0);
    Scalar<int>* One = new Scalar<int>(1);
    Scalar<int>* Size = new Scalar<int>(size);

    CallChain chain;
    Parameters args;

    chain.Reset();
    
    // for interpolator
    chain.AddSymbol("x", x);
    chain.AddSymbol("alpha", Alpha);
    chain.AddSymbol("order", Order);
    
    // for slices
    chain.AddSymbol("zero", Zero);
    chain.AddSymbol("one", One);
    chain.AddSymbol("size", Size);

    // y = interpolate(x, alpha, order);
    chain.AppendCallFunction("interpolate",
                             args.set(3, "x", "alpha", "order"),
                             "y");

    // x0 = slice(x, 0, size, 1);
    chain.AppendCallFunction("slice",
                             args.set(4, "x", "zero", "size", "one"),
                             "x0");

    // x1 = slice(x, size, size, 1);
    chain.AppendCallFunction("slice",
                             args.set(4, "x", "size", "size", "one"),
                             "x1");

    // y0 = interpolate(x0, alpha, order, z);
    chain.AppendCallFunction("interpolate",
                             args.set(4, "x0", "alpha", "order", "z"),
                             "y0");

    // y1 = interpolate(x1, z);
    chain.AppendCallFunction("interpolate",
                             args.set(2, "x1", "z"),
                             "y1");

    // Needed so doesn't throw exception
    chain.AppendIntermediateResult("y", "result", "Final Result", "" );

    chain.Execute();

    {
        const udt* const result = chain.GetSymbol("y");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

        delete ilwd;
    }

    {
        const udt* const result = chain.GetSymbol("y0");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

        delete ilwd;
    }

    {
        const udt* const result = chain.GetSymbol("y1");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

        delete ilwd;
    }

    // Don't need to delete anything else, obj. registry does it
}

template<class T>
void TestInterpolateAction()
{
    TestInterpolateAction<T>(0.1, 2);
}

int main(int argc, char** argv)
{
    Test.Init(argc, argv);
    Test.Message() << "$Id: tInterpolateState.cc,v 1.5 2005/12/01 22:55:02 emaros Exp $" << endl;

    try {
        TestInterpolateState<float>();
        TestInterpolateState<double>();
        TestInterpolateState<complex<float> >();
        TestInterpolateState<complex<double> >();
        
        TestInterpolateAction<float>();
        TestInterpolateAction<double>();
        TestInterpolateAction<complex<float> >();
        TestInterpolateAction<complex<double> >();

        //TestValues<float>();
    }
    catch(exception& e)
    {
        Test.Check(false) << "Exception: " << e.what() << endl;
    }

    Test.Exit();
}
