//!author="Edward Maros"

#include "datacondAPI/config.h"

#include <cstdarg>
#include <fstream>

#include "general/AtExit.hh"

#include "ilwd/ldaselement.hh"
#include "ilwd/util.hh"

#include "CallChain.hh"

#include "token.hh"

using namespace std;   
   
static const ILwd::LdasElement* read_ilwd(char* filename);

static const char* channel_name
  = "channel_0:data:Container(Vect):Frame:example";
int
main(int argc, char** argv)
{
  Parameters	p;

  try {
  for (int x = 1; x < argc; x++)
  {
    CallChain	chain;

    const ILwd::LdasElement* e(read_ilwd(argv[x]));
    if (dynamic_cast<const ILwd::LdasContainer*>(e))
    {
      ILwd::LdasContainer*	r = (ILwd::LdasContainer*)NULL;

      std::string symbols =
	  chain.IngestILwd(*e, CallChain::SYMBOL);

      std::cout << "Ingested channels" << std::endl;
      std::cout << "List of symbols <" << symbols << ">" << std::endl;

      chain.AppendCallFunction("value", p.set(1, channel_name),
			       "");
      chain.AppendIntermediateResult("",
				     "channel_0",
				     "result of value",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("all",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:all",
				     "all stats",
				     "generic");

      chain.AppendCallFunction("statistics",
			     p.set(2, "size", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:size",
				     "size value",
				     "generic");
      chain.AppendCallFunction("size",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:size",
				     "size value (via size)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "min", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:min",
				     "minimum value",
				     "generic");
      chain.AppendCallFunction("min",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:min",
				     "minimum value (via min)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "max", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:max",
				     "maximum value",
				     "generic");
      chain.AppendCallFunction("max",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:max",
				     "maximum value (via max)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "mean", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:mean",
				     "mean value",
				     "generic");
      chain.AppendCallFunction("mean",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:mean",
				     "mean value (via mean)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "rms", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:rms",
				     "rms value",
				     "generic");
      chain.AppendCallFunction("rms",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:rms",
				     "rms value (via rms)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "variance", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:variance",
				     "variance value",
				     "generic");
      chain.AppendCallFunction("variance",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:variance",
				     "variance value (via variance)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "skewness", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:skewness",
				     "skewness value",
				     "generic");
      chain.AppendCallFunction("skewness",
			     p.set(1, channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:skewness",
				     "skewness value (via skewness)",
				     "generic");
      //-----------------------------------------------------------------
      chain.AppendCallFunction("statistics",
			     p.set(2, "kurtosis", channel_name),
			     "");
      chain.AppendIntermediateResult("",
				     "channel_0:kurtosis",
				     "kurtosis value",
				     "generic");
      chain.AppendCallFunction("kurtosis",
			     p.set(1, channel_name),
			     "result");
      chain.AppendIntermediateResult("result",
				     "channel_0:kurtosis",
				     "kurtosis value (via kurtosis)",
				     "generic");

      //-----------------------------------------------------------------


      //-----------------------------------------------------------------

      
      std::cout << "Executing chain..." << std::endl;
      chain.Execute();
      std::cout << "done" << std::endl;

      std::cout << "Results:" << std::endl;
      if ((r = chain.GetResults()))
      {
	  dynamic_cast<ILwd::LdasContainer*>(r)->write(2, 2,
						       std::cout,
						       ILwd::ASCII);
	  std::cout << std::endl;
      }
      chain.Dump();
    }
  }
  }
  catch(CallChain::Exception& e)
  {
      std::cout << "Caught a CallChain::Exception with message <"
	   << e.GetMessage()
	   << ">" << std::endl;
  }
  catch ( std::logic_error& e )
  {
    cerr << "logic error: " << e.what() << std::endl;
  }
  catch ( std::runtime_error& e )
  {
    cerr << "runtime error: " << e.what() << std::endl;
  }
  catch(...)
  {
    std::cout << "Caught an Unknown exception" << std::endl;
  }
  General::AtExit::Cleanup( );
  return 0;
}

static const ILwd::LdasElement*
read_ilwd(char* filename)
{
  std::ifstream	in(filename);

  try {
    try {
      ILwd::Reader r( in );
      ILwd::readHeader(in);

      try {
	r.skipWhiteSpace();
	try {
	  ILwd::LdasElement* e(ILwd::LdasElement::createElement( r ) );
	  return e;
	}
	catch (...)
	{
	  cerr << "FAIL: createElement" << std::endl;
	  throw;
	}
      }
      catch (...)
      {
	cerr << "FAIL: Skipping white spaces" << std::endl;
	throw;
      }
    }
    catch (...)
    {
      cerr << "FAIL: ILwd::Reader" << std::endl;
      throw;
    }
    
  }
  catch(...)
  {
    cerr << "FAIL: read_ilwd(" << filename << ")" << std::endl;
    throw;
  }
}
