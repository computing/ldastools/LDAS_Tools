#include "datacondAPI/config.h"

#include <unistd.h>

#include <valarray>
#include <stdexcept>
#include <complex>

#include "general/unittest.h"

#include "fft.hh"
#include "ifft.hh"

#include "SequenceUDT.hh"
#include "DFTUDT.hh"

#include "fft_check.hh"

using std::complex;
using namespace datacondAPI;

// This may be too small
const float TOLERANCE = 1E-05;

General::UnitTest Test;

void
TestCaseA001()
{
    std::string what("A001: FFT::FFT() constructor");

    FFT fft;

    // Prevent compiler warning
    Test.Check( true ) << "Construction: " << &fft << std::endl;
}

void
TestCaseA002()
{
    std::string what("A002: FFT::apply() - FFT of complex 1-d data, n = 1");

    const size_t n = 1;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000)
    };

    complex<float> fdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseA003()
{
    std::string what("A003: FFT::apply() - FFT of complex 1-d data, n = 2");

    const size_t n = 2;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(-1.0562305898749, 0.4114496766047)
    };

    complex<float> fdata[n] = {
        complex<float>(1.0562305898749, 0.2885503233953),
        complex<float>(-1.0562305898749, 1.1114496766047)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}


void
TestCaseA004()
{
    std::string what("A004: FFT::apply() - FFT of complex 1-d data, n = 3");

    const size_t n = 3;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(-0.3541085699603, 0.0731699242874),
        complex<float>(-0.2729166236434, -0.6847033205137)
    };

    complex<float> fdata[n] = {
        complex<float>(-0.3428248860444, 0.9354524100200),
        complex<float>(-0.6270251936037, 0.0884666037737),
        complex<float>(0.9698500796481, 1.0760809862063)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseA005()
{
    std::string what("A005: FFT::apply() - FFT of complex 1-d data, n = 8");

    const size_t n = 8;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(0.2443153918741, 0.1634117546991),
        complex<float>(1.6811571966645, -0.6237045669319),
        complex<float>(-0.3395397560449, -0.4546136338311),
        complex<float>(-1.0562305898749, 0.4114496766047),
        complex<float>(-0.1847759065023, 0.6467156727579),
        complex<float>(-0.8692396843909, -0.1095041255282),
        complex<float>(0.9471622566848, -0.6978421336132)
    };

    complex<float> fdata[n] = {
        complex<float>(-0.9114750636130, 0.7205693241320),
        complex<float>(-0.5014599806667, -2.3545329099905),
        complex<float>(-3.8307312970498, 1.2965753537967),
        complex<float>(2.9534390729358, 1.7187888533061),
        complex<float>(0.4228489084103, 0.0359126441574),
        complex<float>(1.5855202776091, -2.1691602053298),
        complex<float>(0.0944350927529, 2.3927413843328),
        complex<float>(0.1874229896214, 3.9591055555953)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}


void
TestCaseA006()
{
    std::string what("A006: FFT::apply() - FFT of real 1-d data, odd n = 13");

    const size_t n = 13;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(1.0887334103231, -0.0169146216653),
        complex<float>(0.1699036532317, -0.6991825587826),
        complex<float>(1.6681980661317, 0.0507043601132),
        complex<float>(0.2506643660745, 0.6967321443024),
        complex<float>(-0.1914525314300, -0.0843756761787),
        complex<float>(0.0478768914785, -0.6926544796157),
        complex<float>(-1.7508229275593, 0.1178499289429),
        complex<float>(-0.3396039398585, 0.6869590882976),
        complex<float>(-0.7414866261754, -0.1510489377375),
        complex<float>(-0.3717785376350, -0.6796592721982),
        complex<float>(1.3212190085457, 0.1838951647390),
        complex<float>(0.1791560736203, 0.6707720803764)
    };

    complex<float> fdata[n] = {
        complex<float>(-1.2926857773641, -0.2514348494313),
        complex<float>(0.1579309886901, 4.7992225148101),
        complex<float>(-2.2251174759340, -0.4138299112784),
        complex<float>(1.9268668665462, 2.3315063363747),
        complex<float>(-1.6351024563620, -0.4605643042366),
        complex<float>(4.5850627584341, 3.7806604406799),
        complex<float>(1.3306069067473, 0.7830772205934),
        complex<float>(4.1169205911819, -2.1326745022011),
        complex<float>(-2.9084452907819, 2.5006741665496),
        complex<float>(-4.3594377353468, 3.1842749293733),
        complex<float>(0.3676786314170, -0.6839693024021),
        complex<float>(0.9967929952043, -4.8017315321106),
        complex<float>(-1.0610710024322, 0.4647887932791)
    };
    
    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB001()
{
    std::string what("B001: IFFT::IFFT() constructor");

    IFFT ifft;

    Test.Check(true ) << "Construction: " << &ifft << std::endl;
}

void
TestCaseB002()
{
    std::string what("B002: IFFT::apply() - IFFT of complex 1-d data, n = 1");

    const size_t n = 1;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000)
    };

    complex<float> fdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB003()
{
    std::string what("B003: IFFT::apply() - IFFT of complex 1-d data, n = 2");

    const size_t n = 2;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(-1.0562305898749, 0.4114496766047)
    };

    complex<float> fdata[n] = {
        complex<float>(1.0562305898749, 0.2885503233953),
        complex<float>(-1.0562305898749, 1.1114496766047)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB004()
{
    std::string what("B004: IFFT::apply() - IFFT of complex 1-d data, n = 3");

    const size_t n = 3;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(-0.3541085699603, 0.0731699242874),
        complex<float>(-0.2729166236434, -0.6847033205137)
    };

    complex<float> fdata[n] = {
        complex<float>(-0.3428248860444, 0.9354524100200),
        complex<float>(-0.6270251936037, 0.0884666037737),
        complex<float>(0.9698500796481, 1.0760809862063)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB005()
{
    std::string what("B005: IFFT::apply() - IFFT of complex 1-d data, n = 8");

    const size_t n = 8;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(0.2443153918741, 0.1634117546991),
        complex<float>(1.6811571966645, -0.6237045669319),
        complex<float>(-0.3395397560449, -0.4546136338311),
        complex<float>(-1.0562305898749, 0.4114496766047),
        complex<float>(-0.1847759065023, 0.6467156727579),
        complex<float>(-0.8692396843909, -0.1095041255282),
        complex<float>(0.9471622566848, -0.6978421336132)
    };

    complex<float> fdata[n] = {
        complex<float>(-0.9114750636130, 0.7205693241320),
        complex<float>(-0.5014599806667, -2.3545329099905),
        complex<float>(-3.8307312970498, 1.2965753537967),
        complex<float>(2.9534390729358, 1.7187888533061),
        complex<float>(0.4228489084103, 0.0359126441574),
        complex<float>(1.5855202776091, -2.1691602053298),
        complex<float>(0.0944350927529, 2.3927413843328),
        complex<float>(0.1874229896214, 3.9591055555953)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB006()
{
    std::string what("B006: IFFT::apply() - IFFT of complex 1-d data, n = 13");

    const size_t n = 13;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(1.0887334103231, -0.0169146216653),
        complex<float>(0.1699036532317, -0.6991825587826),
        complex<float>(1.6681980661317, 0.0507043601132),
        complex<float>(0.2506643660745, 0.6967321443024),
        complex<float>(-0.1914525314300, -0.0843756761787),
        complex<float>(0.0478768914785, -0.6926544796157),
        complex<float>(-1.7508229275593, 0.1178499289429),
        complex<float>(-0.3396039398585, 0.6869590882976),
        complex<float>(-0.7414866261754, -0.1510489377375),
        complex<float>(-0.3717785376350, -0.6796592721982),
        complex<float>(1.3212190085457, 0.1838951647390),
        complex<float>(0.1791560736203, 0.6707720803764)
    };

    complex<float> fdata[n] = {
        complex<float>(-1.2926857773641, -0.2514348494313),
        complex<float>(0.1579309886901, 4.7992225148101),
        complex<float>(-2.2251174759340, -0.4138299112784),
        complex<float>(1.9268668665462, 2.3315063363747),
        complex<float>(-1.6351024563620, -0.4605643042366),
        complex<float>(4.5850627584341, 3.7806604406799),
        complex<float>(1.3306069067473, 0.7830772205934),
        complex<float>(4.1169205911819, -2.1326745022011),
        complex<float>(-2.9084452907819, 2.5006741665496),
        complex<float>(-4.3594377353468, 3.1842749293733),
        complex<float>(0.3676786314170, -0.6839693024021),
        complex<float>(0.9967929952043, -4.8017315321106),
        complex<float>(-1.0610710024322, 0.4647887932791)
    };
    
    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseC002()
{
    std::string what("C002: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 1");

    const size_t n = 1;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000)
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}
void
TestCaseC003()
{
    std::string what("C003: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 2");

    const size_t n = 2;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(-1.0562305898749, 0.4114496766047)
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}
void
TestCaseC004()
{
    std::string what("C004: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 3");

    const size_t n = 3;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(-0.3541085699603, 0.0731699242874),
        complex<float>(-0.2729166236434, -0.6847033205137)
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseC005()
{
    std::string what("C005: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 8");

    const size_t n = 8;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 1.0000000000000),
        complex<float>(0.9249743361524, 0.0000000000000),
        complex<float>(1.9989505464822, 0.0000000000000),
        complex<float>(-0.8718239319649, 0.0000000000000),
        complex<float>(-1.6225424859374, 0.0000000000000),
        complex<float>(0.0831024961533, 0.0000000000000),
        complex<float>(-0.1778578459743, 0.0000000000000),
        complex<float>(1.0020836236942, 0.0000000000000)
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseC006()
{
    std::string what("C006: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 13");

    const size_t n = 13;
    const complex<float> tdata[n] = {
        complex<float>(0.0000000000000, 0.7000000000000),
        complex<float>(1.0887334103231, -0.0169146216653),
        complex<float>(0.1699036532317, -0.6991825587826),
        complex<float>(1.6681980661317, 0.0507043601132),
        complex<float>(0.2506643660745, 0.6967321443024),
        complex<float>(-0.1914525314300, -0.0843756761787),
        complex<float>(0.0478768914785, -0.6926544796157),
        complex<float>(-1.7508229275593, 0.1178499289429),
        complex<float>(-0.3396039398585, 0.6869590882976),
        complex<float>(-0.7414866261754, -0.1510489377375),
        complex<float>(-0.3717785376350, -0.6796592721982),
        complex<float>(1.3212190085457, 0.1838951647390),
        complex<float>(0.1791560736203, 0.6707720803764)
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseE001()
{
    std::string what("E001: FFT apply exceptions - size 0 input array");

    FFT				fft;
    Sequence<complex<float> >	fft_in;
    udt*			fft_out((udt*)NULL);

    try
    {
	fft.apply(fft_out, fft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception"
			 << e.what() << std::endl;
	return;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;
}

void
TestCaseE002()
{
    std::string what("E002: FFT apply exceptions - size 0 input array");

    FFT			fft;
    Sequence< float >	fft_in;
    udt*		fft_out = 0;

    try
    {
	fft.apply(fft_out, fft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception"
			 << e.what() << std::endl;
	return;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;
}

void
TestCaseE003()
{
    std::string what("E003: IFFT apply exceptions - size 0 input array");

    IFFT			ifft;
    DFT<complex<float> >	ifft_in;
    udt* 			ifft_out;

    try
    {
	ifft.apply(ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception"
			 << e.what() << std::endl;
	return;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;
}

void
TestCaseE004()
{
    std::string what("E004: IFFT apply exceptions - size 0 input array");

    const size_t		n(9);
    IFFT			ifft;
    DFT<complex<float> >	ifft_in;
    Sequence<complex<float> >	ifft_out(n);
    udt*			p_ifft_out(0);

    try
    {
	ifft.apply(p_ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception"
			 << e.what() << std::endl;
	return;
    }
    Test.Check(false) << "Didn't catch exception" << std::endl;
}

int
TestCaseF001()
{
    int result = 0;

#if 0
    std::string what("F001: purging of used plans");

    PlanDB planDB;

    planDB.purge_plans(planDB.size());

    Test.Check(planDB.size() == 0) << "Plan database is empty" << std::endl;

    planDB.set_max_size(4);

    Test.Check(planDB.max_size() == 4) << "Plan database max size is 4"
				       << std::endl;

    FFT fft;
    
    Sequence<complex<float> > fft_in(8);
    udt* fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    fft_in.resize(8);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 1) << "Plan database has 1 element" << std::endl;

    sleep(1);

    fft_in.resize(7);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 2) << "Plan database has 2 elements" << std::endl;

    sleep(1);

    fft_in.resize(6);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 3) << "Plan database has 3 elements" << std::endl;

    sleep(1);

    // Re-use the 8-plan to update it's use time
    fft_in.resize(8);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 3) << "Plan database has 3 elements" << std::endl;

    sleep(1);

    fft_in.resize(5);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 4) << "Plan database has 4 elements" << std::endl;

    sleep(1);

    fft_in.resize(4);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() <= planDB.max_size())
	<< "Plan db size is smaller than max_size" << std::endl;

    sleep(1);

    fft_in.resize(3);
    fft.apply(fft_out, fft_in);
    
    delete fft_out;

    Test.Check(planDB.size() <= planDB.max_size())
	<< "Plan db size is smaller than max_size" << std::endl;

    // Oldest plans (7 and 6) should have aged out by now
    size_t count = planDB.count(std::valarray<size_t>(7,1),
				planType<SComplexFwdPlanH>(),
				FFTW_ESTIMATE);

    Test.Check(count == 0) << "Plan 7 is gone" << std::endl;

    count = planDB.count(std::valarray<size_t>(6,1),
			 planType<SComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 0) << "Plan 6 is gone" << std::endl;

    count = planDB.count(std::valarray<size_t>(8,1),
			 planType<SComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 8 is left" << std::endl;

    count = planDB.count(std::valarray<size_t>(5,1), planType<SComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 5 is left" << std::endl;

    count = planDB.count(std::valarray<size_t>(4,1), planType<SComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 4 is left" << std::endl;

    count = planDB.count(std::valarray<size_t>(3,1), planType<SComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 3 is left" << std::endl;

    planDB.purge_plans(planDB.size());

    Test.Check(planDB.size() == 0) << "All plans removed" << std::endl;
#endif

    return result;
}

int
main(int argc, char** argv)
{
  Test.Init(argc, argv);

  try
  {
    TestCaseA001();
    TestCaseA002();
    TestCaseA003();
    TestCaseA004();
    TestCaseA005();
    TestCaseA006();
    
    TestCaseB001();
    TestCaseB002();
    TestCaseB003();
    TestCaseB004();
    TestCaseB005();
    TestCaseB006();

    TestCaseC002();
    TestCaseC003();
    TestCaseC004();
    TestCaseC005();
    TestCaseC006();

    TestCaseE001();
    TestCaseE002();
    TestCaseE003();
    TestCaseE004();

    TestCaseF001();
  }
  catch(std::exception& e)
  {
    Test.Check(false) << "Unhandled exception" 
		      << e.what() << std::endl;
  }
  catch(...)
  {
    Test.Check(false) << "Unhandled exception" << std::endl;
  }

  //PlanDB().purge_plans(PlanDB().size());

  Test.Exit();
}
