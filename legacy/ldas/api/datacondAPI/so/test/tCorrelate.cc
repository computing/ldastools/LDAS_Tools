//
// tCorrelate.cc:
//
// Test driver for Correlate class
//

#include "datacondAPI/config.h"

// system includes
#include <iostream>
#include <complex> 
#include <valarray>
#include <string>
#include <fstream>
#include <stdexcept>

// other includes
#include "general/unittest.h"

#include <filters/LDASConstants.hh>
#include "DataCondAPIConstants.hh"
#include "Correlate.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"

// the following tests are performed: (for T=float and double)
//
// test error exceptions in constructor 
// test error exceptions in set methods
// test error exceptions in apply methods
// test construction of valid Correlate objects (default and custom)
// test set methods for valid parametersi
// test apply methods on various types of valid data

using namespace datacondAPI;

General::UnitTest Test;

//------------------------------------------------------------------------
// test error exceptions in constructor
//
bool testConstructorError(const string& name)
{
  bool pass = false;
/*
  try
  {
    try
    {
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name
                       << " ("<< r.what() << ")" 
		               << endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		               << " (no exception thrown)" 
		               << endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		             << " (unexpected exception: "<< r.what() << ")" 
		             << endl;
  }

*/
  pass = true;
  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in set methods
//
bool testSetError(const string& name)
{
  bool pass = false;
/*
  try
  {
    try
    {
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
		               << " ("<< r.what() << ")" 
		               << endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		               << " (no exception thrown)" 
		               << endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		             << " (unexpected exception: "<< r.what() << ")" 
		             << endl;
  }
*/
  pass = true;
  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in apply method
//
template<class T>
bool testApplyError(const string& name)
{

  bool pass = false;
/*
  try
  {
    try
    {
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name 
	        	       << " ("<< r.what() << ")" 
		               << endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name
		               << " (no exception thrown)" 
		               << endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name
		             << " (unexpected exception: "<< r.what() << ")" 
		             << endl;
  }
*/
  pass = true;
  return pass;
}

//------------------------------------------------------------------------
// test all error exceptions (constructor, set methods, apply, operator()) 
//
bool testErrors(void)
{
  bool allPass = true;
/*
  bool pass;
  
  // constructors
  
  pass = testConstructorError("constructor");
  allPass = allPass && pass;

  // set methods

  pass = testSetError("set method");
  allPass = allPass && pass;

  // apply method

  pass = testApplyError<float>("apply method: float");
  allPass = allPass && pass;
*/
  allPass = true;
  return allPass;
}

//------------------------------------------------------------------------
// test valid Correlate objects
//
bool testValidObject(const string& name)
{
  bool allPass = true;
/*
  bool pass;

  pass = true;
  if (true == false)
  {
    pass = false;
  }
  Test.Check(pass) << name << endl;
  allPass = allPass && pass;
*/
  allPass = true;
  return allPass;
}

//------------------------------------------------------------------------
// test all types of valid Correlate objects (default, custom, ...)
//
bool testAllValidObjects()
{

  bool allPass = true;
/*
  bool pass;

  // default 

  pass = testValidObject("default object");
  allPass = allPass && pass;

  // custom 

  pass = testValidObject("custom object");
  allPass = allPass && pass;
*/
  allPass = true;
  return allPass;
}

//------------------------------------------------------------------------
// test all valid set methods for Correlate objects
//
bool testAllValidSetMethods()
{
  bool allPass = true;
/*
  bool pass;

  pass = testValidObject("set method");
  allPass = allPass && pass;
*/
  allPass = true;
  return allPass;
}

//-----------------------------------------------------------------------
// test apply methods on valid data 
//
template <class T>
bool testValidApply(const string& name)
{
  bool allPass = true;
/*
  bool pass;
  
  double tolerance = 1e-6;

  udt* p_out((udt*)NULL);

  pass = true;
  for (size_t i = 0; i < 0; i++)
  {
    if (true == false)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << "apply method" << name << endl;
  allPass = allPass && pass;
*/
  allPass = true;
  return allPass;
}

//-----------------------------------------------------------------------
// test apply method on different valid sets of data
//
template <class T>
bool testAllValidApply()
{
  bool allPass = true;
/*
  bool pass = true;

  pass = testValidApply<T>("type of data...");
  allPass = allPass && pass;
*/
  allPass = true;
  return allPass;
}

bool testMetaDataCopy()
{
/*
  if(NameMetaData* resolved = dynamic_cast<NameMetaData*>(out))
  {
    bool pass = resolved->GetName() == "csd(x,y)";
    Test.Check(pass) << resolved->GetName() << "=?=csd(x,y)" << endl;
    return pass;
  }
  else
  {
    Test.Check(false) << "output has no Name metadata";
    return false;
  }
*/
  return true;
}
//------------------------------------------------------------------------
//
int main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);

  Test.Message()
    << "$Id: tCorrelate.cc,v 1.4 2005/12/01 22:55:02 emaros Exp $"
    << endl << endl;

  //------------------------------------------------------------------------
  // test all error exceptions

  Test.Check(testErrors()) << "(all error exception tests)" 
		                   << endl;

  //------------------------------------------------------------------------
  // test valid Correlate objects

  Test.Check(testAllValidObjects()) << "(all valid Correlate objects tests)" 
				                    << endl;

  //------------------------------------------------------------------------
  // test valid set methods for Correlate objects

  Test.Check(testAllValidSetMethods()) << "(all valid set methods tests)" 
				                       << endl;

  //-----------------------------------------------------------------------
  // test apply methods for valid data and valid Correlate objects

  Test.Check(testAllValidApply<float>()) 
      << "(apply method on valid data test <float>)" 
	  << endl;
  
  //-----------------------------------------------------------------------
  // test that metadata is properly copied
  
  Test.Check(testMetaDataCopy()) << "(metadata copied from input to output)"
                                 << endl;

  // all done!!

  Test.Exit();

  return 0;
}
