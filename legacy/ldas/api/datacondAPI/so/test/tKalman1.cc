#include "datacondAPI/config.h"

#include "LinFilt.hh"
#include "ShiftState.hh"
#include "LinFiltState.hh"
#include "Kalman.hh"
#include "random.hh"
#include "KalmanState.hh"
#include "general/unittest.h"
#include "MixerState.hh"
#include "Mixer.hh"
#include "Resample.hh"
#include <filters/LDASConstants.hh>
#include <math.h>
#include <iostream.h>
#include <stdio.h>

using namespace datacondAPI;

General::UnitTest Test;
double factorial(int number)
{
	double product = 1;

	for ( ; number > 0 ; number--)
		product *= number;

	return product;
}
class BandSelector
    {

    public:

        BandSelector(const double& frequency, const std::size_t factor)
            : m_downmixer(MixerState(0.0, -frequency))
            , m_downsampler(1, factor)
            , m_downshifter(0)
            , m_upsampler(factor, 1)
            , m_upshifter(0)
            , m_upmixer(MixerState(0.0, +frequency))
        {
        }

      /*BandSelector(const BandSelector& bs)
            : m_downmixer(bs.m_downmixer)
            , m_downsampler(bs.m_downsampler)
            , m_downshifter(bs.m_downshifter ? bs.m_downshifter->Clone() : 0)
            , m_upsampler(bs.m_upsampler)
            , m_upshifter(bs.m_upshifter ? bs.m_upshifter->Clone() : 0)
            , m_upmixer(bs.m_upmixer)
        {
        }*/

        ~BandSelector()
        {
            delete m_downshifter;
            delete m_upshifter;
        }
      /*
        BandSelector& operator=(const BandSelector& bs)
        { 
            if (&bs != this)
            {
                m_downmixer = bs.m_downmixer;
                m_downsampler = bs.m_downsampler;
                delete m_downshifter;
                m_downshifter = bs.m_downshifter ? bs.m_downshifter->Clone() : 0;
                m_upsampler = bs.m_upsampler;
                delete m_upshifter;
                m_upshifter = bs.m_upshifter ? bs.m_upshifter->Clone() : 0;
                m_upmixer = bs.m_upmixer;
            }
            return *this;
	    }*/
   
        BandSelector* clone() const
        {
            return new BandSelector(*this);
        }

        template<typename out_t, typename in_t>
            void apply(std::valarray<out_t>& out, const std::valarray<in_t>& in)
        {
            std::valarray<out_t> downmixed;
            m_downmixer.apply(downmixed, in);
            Sequence<out_t> downsampled;
            m_downsampler.apply(downsampled, downmixed);
            if (!m_downshifter) m_downshifter = new ShiftState<out_t>(m_downsampler.getDelay(), 1);
            dynamic_cast<ShiftState<out_t>&>(*m_downshifter).apply(downsampled);
            out.resize(downsampled.size());
            out = downsampled;
        }

        template<typename out_t, typename in_t>
            void ylppa(std::valarray<out_t>& out, const std::valarray<in_t>& in)
        {
            Sequence<in_t> upsampled;
            m_upsampler.apply(upsampled, in);
            if (!m_upshifter) m_upshifter = new ShiftState<in_t>(m_upsampler.getDelay(), 1);
            dynamic_cast<ShiftState<in_t>&>(*m_upshifter).apply(upsampled);
            m_upmixer.apply(out, upsampled);
        }

    private:

        BandSelector();

        Mixer m_downmixer;
        Resample m_downsampler;
        State* m_downshifter;
        Resample m_upsampler;
        State* m_upshifter;
        Mixer m_upmixer;

    };

void Ntest()
{
  //Declare Constants and Such
  int size = 819200;
  double Q = 57000;
  double fs = 16384;
  double Q1 = 50000;
  double f0 = 512;
  //double f1 = 514;
  double w0 = LDAS_TWOPI * f0;
  //double w1 = LDAS_TWOPI * f1;
  double s1 = 2.8;
  double s2 = 2.38;
  int down = int(fs/16);
  udt* temp; //used for outputs of apply methods
  
  //Sequence<double> input(size);
  //for(int i=0; i < size; i++)
    //input[i] = sin(LDAS_TWOPI*i/1000);
  //input[2000] = 3;
  //ResampleState Rstate1(1,down);
  //Resample Rsamp1(Rstate1);
  //temp = new Sequence<double>; 
  //Rsamp1.apply(temp,input);
  //Sequence<double> z1(udt::Cast<Sequence<double> >(*temp));
  //delete temp;

  
  //ResampleState Rstate2(down,1);
  //Resample Rsamp2(Rstate2);
  //temp = new Sequence<double>; 
  //Rsamp2.apply(temp,z1);
  //Sequence<double> output(udt::Cast<Sequence<double> >(*temp));
  //delete temp;
  //double delay1, delay2;
  //Rsamp1.getDelay(delay1);
  //Rsamp2.getDelay(delay2);
  //cout << delay1+delay2 << endl;
  //  for(int i=0; i< output.size(); i++)
  //cout << input[i] << " " << output[i] << endl;*/
   
  //Initialize state of random number generator
  gasdevstate gstate(-145171690);

  //Create measurement noise
  Sequence<double> n(size);
  gasdev(gstate,n,size);
  n*=sqrt(s1);

  //Create process driving force
  Sequence<double> F(size);
  gasdev(gstate,F,size);
  F*=sqrt(s2);

  //Sequence<double> F1(size);
  //gasdev(gstate,F1,size);
  //F1*sqrt(s2);

  //#Convert process driving force to process noise

  //Calcluate coefficients for Linear Filtering (see pgs 8-10 of paper)
  double R = w0/tan(w0/2/fs);
  //double R1 = w1/tan(w1/2/fs);
  double C1 = (R*R + R*w0/Q + w0*w0);
  //double C2 = (R1*R1 + R1*w1/Q1 + w1*w1);

  //B coefficients
  Sequence<double> b(3);
  b[0] = b[2] = 1/C1;
  b[1] = 2/C1;

  //Sequence<double> b1(3);
  //b1[0] = b1[2] = 1/C2;
  //b1[1] = 2/C2;

  //A coefficients
  Sequence<double> a(3);
  a[0] = 1;
  a[1] = (-2*R*R + 2*w0*w0)/C1;
  a[2] = (R*R - R*w0/Q + w0*w0)/C1;

  //Sequence<double> a1(3);
  //a1[0] = 1;
  //a1[1] = (-2*R1*R1 + 2*w1*w1)/C2;
  //a1[2] = (R1*R1 - R1*w1/Q1 + w1*w1)/C2;
  //Run F through filter to create process noise
  LinFiltState Lstate(b,a);
  LinFilt Lfilt(Lstate);
  temp = new Sequence<double>;
  Lfilt.apply(temp,F);
  Sequence<double> r(udt::Cast<Sequence<double > >(*temp));
  delete temp;

  //LinFiltState Lstate1(b1,a1);
  //LinFilt Lfilt1(Lstate1);
  //temp = new Sequence<double>;
  //Lfilt1.apply(temp,F1);
  //Sequence<double> r1(udt::Cast<Sequence<double > >(*temp));
  //delete temp;
  //# Scale r so the line rises above the background

  //Find the mean of R
  double rmean = 0;
  for(int i=0; i<r.size(); i++)
    rmean +=r[i];

  rmean/=r.size();

  //double rmean1 = 0;
  //for(int i=0; i<r.size(); i++)
  //rmean1 +=r1[i];

  //rmean1/=r1.size();
  //Find the variance
  double rvar =0;
  for(int i=0; i<r.size(); i++)
      rvar += pow(r[i] - rmean,2);

  rvar /= (r.size() - 1);

  //double rvar1 =0;
  //for(int i=0; i<r.size(); i++)
  //rvar1 += pow(r1[i] - rmean1,2);

      //rvar1 /= (r1.size() - 1);
  //Scale R so the variance is s2
  double scale = s2/rvar;
  r *= sqrt(scale);
  //double scale1 = s2/rvar1;
  //r1 *= sqrt(scale1);
  //Finalize simulated signal by combining background and process data
  Sequence<double> g(size);
  g = n+r;/*+r1;*/
  
  //Test output to get n||r||g
  //for(int i=0;i<size;i++)
  //cout << g[i] << " ";  
  
  //Mix down the signal by exp(-i*wc*t)
  //MixerState Mstate1(0,2*(f0)/fs);
  //Mixer Mix1(Mstate1);
  //temp = new Sequence<complex<double> >;
  //Mix1.apply(temp,g);
  //Sequence<complex<double> > gprime(udt::Cast<Sequence<complex<double > > >(*temp));
  //delete temp;

  //Downsample gprime to form z
  //ResampleState Rstate1(1,down);
  //Resample Rsamp1(Rstate1);
  //temp = new Sequence<complex<double> >; 
  //Rsamp1.apply(temp,gprime);
  //need to change zprime on following line to zdprime if removing the delay is the next step
  //Sequence<complex<double> > zprime(udt::Cast<Sequence<complex<double > > >(*temp));
  //delete temp;

  //Remove delay caused by downsampling
  //double delay1;
  //Rsamp1.getDelay(delay1);
  //Sequence<complex<double> > zprime(zdprime[slice(delay,zdprime.size()-delay,1)]);
  

  //Trying BandSelector Stuff
  Sequence<std::complex<double> > zprime;
  BandSelector savior(2*(f0-1)/fs, down);
  savior.apply(zprime,g);

  //Convert Sequence<complex<double> to VectorSeries 
  //in preperation for Kalman filtering
  Sequence<double> temp1(zprime.size()*2);
  for(int i=0;i<zprime.size();i++)
    {
      temp1[i*2] = zprime[i].real();
      temp1[i*2+1] = zprime[i].imag();
      //cout << zprime[i].real() << " " << zprime[i].imag() << endl;
    }
  
  VectorSequence<double> z(temp1,2,zprime.size());

  //# Calculate arguments for Kalman filter
  
  //Calculate A (currently taken from Matlab)
  Matrix<double> A(2,2);
  Matrix<double> Mtemp(2,2);
  Mtemp[0][0] = Mtemp[1][1] = 1;
  Mtemp[0][1] = Mtemp[1][0] = 0;
  Matrix<double> Mtemp1(2,2);
  Mtemp1[0][0] = Mtemp1[1][1] = -1*w0/(2*Q*16);
  Mtemp1[0][1] = ((LDAS_TWOPI*(f0-1))-w0*sqrt(1-1/(4*Q*Q)))/16;
  Mtemp1[1][0] = -1*Mtemp1[0][1];
  for(int j=1;j<20;j++)
    {
      Mtemp = Mtemp + 1/(factorial(j))*Mtemp1;
      Mtemp1 = Mtemp1*Mtemp1;
    }
  A[0][0]=Mtemp[0][0];
  A[0][1]=Mtemp[0][1];
  A[1][0]=Mtemp[1][0];
  A[1][1]=Mtemp[1][1];
  //cout << A[0][0] << " " << A[0][1] << endl
  //<< A[1][0] << " " << A[1][1] << endl;
  
  //A[0][0] = A[1][1] = 0.92225152416737;
  //A[0][1] = -.38200908087127 ;
  //A[1][0] = -A[0][1];
  
  //Calculate C (currently identity matrix)
  Matrix<double> C(2,2);
  //Sequence<double> tC(2);
  C[0][0] = 1;
  C[0][1] = 0;
  //C[0][2] = 1;
  //C[0][3] = 0;
  //C.setRow(0,tC);
  C[1][0] = 0;
  C[1][1] = 1;
  //C[1][2] = 0;
  //C[1][3] = 1;
  //C.setRow(1,tC);

  //Calculate V
  Matrix<double> V(2,2);
  //Sequence<double> tV(2);
  V[0][0] = 1.28706861679107e-13;
  V[0][1] = 0;
  //V.setRow(0,tV);
  V[1][1] = V[0][0];
  V[1][0] = 0;
  //V.setRow(1,tV);
  
  //Calculate W
  Matrix<double> W(2,2);
  //Sequence<double> tW(2);
  W[0][0] = 2.159625886877163e-13;//3.115930e-15*.5*(w0*w0+pow(2*Q*LDAS_TWOPI*(f0-1)-w0*sqrt(4*Q*Q-1),2))/(4*Q*Q);
  W[0][1] = 0;
  //W[2][2] = 13052.00*.5*(w1*w1+pow(2*Q1*w1-w1*sqrt(4*Q1*Q1-1),2))/(4*Q1*Q1);
  //W[2][3] = 0;
  //W.setRow(0,tW);
  W[1][1] = W[0][0];
  W[1][0] = 0;
  //W[3][3] = W[2][2];
  //W[3][2] = 0;
  //W.setRow(1,tW);
  
  //Calculate PSI
  Sequence<double> psi(0.,2);
  //# Calculate arguments for Kalman filter
  
  //Calculate A (currently taken from Matlab)
  
  
  //Create and apply the Kalman filter
  KalmanState Kstate(A,W,C,V,psi,W);
  Kalman Kal(Kstate);
  temp = new VectorSequence<double> (z);
  Kal.apply(temp,z);
  VectorSequence<double> zout(udt::Cast<VectorSequence<double> >(*temp));
  delete temp;
    
  //Get results out of VectorSequence back into Sequence
  Sequence<std::complex<double> > Gpp(zout.sDim());
  for(int i=0;i<zout.sDim();i++)
    {
      std::valarray<double> temp2 = zout.vec(i);
      std::complex<double> temp3(temp2[0],temp2[1]);
      Gpp[i] = temp3;
      //cout << temp2[0] << " " << temp2[1] << endl;
    }
  
  //Trying BandSelector
  Sequence<std::complex<double> > Gv;
  savior.ylppa(Gv,Gpp);

  //Upsample data back to original sample rate
  //ResampleState Rstate2(down,1);
  //Resample Rsamp2(Rstate2);
  //temp = new Sequence<complex<double> >;
  //Rsamp2.apply(temp,Gpp);
  //Sequence<complex<double> > Gs(udt::Cast<Sequence<complex<double> > >(*temp));
  //delete temp;
  

  //Take delay caused by upsampling into account
  //double delay2;
  //Rsamp2.getDelay(delay2);
  //double delay = delay1 + 2 * delay2;
  //Sequence<complex<double> > Gp(Gs[slice(int(delay),Gs.size()-int(delay),1)]);

  //Unmix the data
  //MixerState Mstate2(0,-2*f0/fs);
  //Mixer Mix2(Mstate2);
  //temp = new Sequence<complex<double> >;
  //Mix2.apply(temp,Gp);
  //Sequence<complex<double> > Gv(udt::Cast<Sequence<complex<double> > >(*temp));
  //delete temp;

  //Final Prediction is 2*Real(Gv)
  Sequence<double> rout(Gv.size());
  for(unsigned int i=0;i<gpsi.size();i+=2)
    {
      rout[i] = 2*Gv[i].real();
      cout << rout[i] << " ";
    }

  //Subtract Prediction from Data
  Sequence<double> clean(rout.size());
  for(int i=0; i<rout.size(); i++)
    {
      clean[i] = g[i] - rout[i];
      //cout << clean[i] << " ";
    }

  /**********************************************
  TODO: FINISH C++ TEST CODE BY CHECKING THE RATIO
  OF THE VARIANCES OF n AND clean - n, SHOULD BE
  VERY CLOSE TO ZERO!!!
   **********************************************/
}
  

int main( int ArgC, char ** ArgV)
{
  try{
    std::string id("$Id");
    Test.Init(ArgC, ArgV);
    //if(Test.IsVerbose())
    //Test.Message() << is << endl;
    Ntest();
  }
  catch(std::exception &e)
    {
      Test.Check(false) << "Caught unexpected exception: " << e.what() << endl;
    }
  Test.Exit();
}
