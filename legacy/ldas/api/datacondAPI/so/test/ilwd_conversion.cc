#include "datacondAPI/config.h"

#include <string>

#include "general/unittest.h"

#include "CallChain.hh"
#include "TypeInfo.hh"
#include "UDT.hh"
#include "CSDSpectrumUDT.hh"
#include "DFTUDT.hh"
#include "FrequencySequenceUDT.hh"
#include "Matrix.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "StateUDT.hh"
#include "StatsUDT.hh"
#include "VectorSequenceUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "WelchSpectrumUDT.hh"

using std::endl;   
General::UnitTest	Test;

void
convert( const datacondAPI::udt& UDT, datacondAPI::udt::target_type Target,
	 bool Converts,
	 std::string Message )
{
  ILwd::LdasElement*	element((ILwd::LdasElement*)NULL);
  CallChain	cc;

  try {
    element = UDT.ConvertToIlwd(cc, Target);
    Test.Check(Converts) << Message << ": converted to ilwd"
			 << endl;
    delete element;
  }
  catch ( const datacondAPI::udt::BadTargetConversion& e )
  {
    Test.Check(!Converts) << Message << ": in BadTargetConversion"
			  << endl;
    delete element;
  }
  catch ( const std::exception& e )
  {
    Test.Check(false) << Message << ": unexpected exception thrown: "
		      << e.what()
		      << endl;
    delete element;
  }
  catch ( ... )
  {
    Test.Check(false) << Message << ": unknown exception thrown" << endl;
    delete element;
  }
}

void
conversions( const datacondAPI::udt& UDT,
	     bool Generic, bool Database, bool Wrapper )
{
  std::string	ti(datacondAPI::TypeInfoTable.GetName(typeid(UDT)));
  Test.Message() << " -+* Testing: " << ti << " *+-" << endl;
  //---------------------------------------------------------------------
  // G E N E R I C
  //---------------------------------------------------------------------
  convert(UDT, datacondAPI::udt::TARGET_GENERIC, Generic,
	  ti + " - Generic conversion");
  //---------------------------------------------------------------------
  // D A T A B A S E
  //---------------------------------------------------------------------
  convert(UDT, datacondAPI::udt::TARGET_METADATA, Database,
	  ti + " - Database conversion");
  //---------------------------------------------------------------------
  // W R A P P E R
  //---------------------------------------------------------------------
  convert(UDT, datacondAPI::udt::TARGET_WRAPPER, Wrapper,
	  ti + " - Wrapper conversion");
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  std::valarray<double>	moments(datacondAPI::Stats::MOMENTS_MAX);

  conversions(datacondAPI::CSDSpectrum<float>(), true, false, true);
  conversions(datacondAPI::DFT<std::complex<float> >(), true, false, true);
  conversions(datacondAPI::FrequencySequence<float>(), true, false, true);
  conversions(datacondAPI::Matrix<float>(), true, false, false);
  conversions(datacondAPI::Scalar<int>(5), true, false, false);
  conversions(datacondAPI::Sequence<float>(5.0, 3), true, false, true);
  conversions(datacondAPI::Stats(0, 0, 0, 0, moments), true, true, false);
  conversions(datacondAPI::VectorSequence<float>(), false, false, false);
  conversions(datacondAPI::WelchCSDSpectrum<float>(), true, true, true);
  conversions(datacondAPI::WelchSpectrum<float>(), true, true, true);

  Test.Exit();
}

