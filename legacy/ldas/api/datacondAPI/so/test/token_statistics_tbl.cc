#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"        // common header for token testing programs
#include "Statistics.hh"   // needed for computing expected statistics results
#include "ScalarUDT.hh"    // needed for converting CallChain results 
#include "StatsUDT.hh"     // needed for converting CallChain results

using namespace std;   
using namespace datacondAPI;   
   
General::UnitTest  Test;            // class for testing

//-----------------------------------------------------------------------
// This template function is a pattern for testing the functionality
// of the statistics actions.
//-----------------------------------------------------------------------

template<class T>
bool stats_test(const std::string& type)
{

  //---------------------------------------------------------------------
  // Display start message
  //---------------------------------------------------------------------

  Test.Message() << "Statistics test <" << type << ">: start\n";

  std::string	vector_type;
  bool		allPass(true);

  if (type == "int")
  {
    vector_type = "svalarray";
  }
  else if (type == "float")
  {
    vector_type = "svalarray";
  }
  else if (type == "double")
  {
    vector_type = "dvalarray";
  }
  else
  {
    return false;
  }
  
  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  try
  {
    CallChain   cmds;           // Instance of CallChain
    Parameters  args;           // Structure to hold parameters
    
    cmds.Reset();               // Make sure CallChain is ready for action

    Test.Message() << "...appending classes...\n";

    //-------------------------------------------------------------------
    // Establish variables
    //-------------------------------------------------------------------

    // len = integer(10);
    cmds.AppendCallFunction("integer", args.set(1, "10"), "len");

    // val = integer(1);
    cmds.AppendCallFunction("integer", args.set(1, "1"), "val");

    // v = valarray<vector_type>(val, len); 
    cmds.AppendCallFunction(vector_type, args.set(2, "val", "len"), "v");

    //-------------------------------------------------------------------
    // Establish the actions that are to be exercised.
    //-------------------------------------------------------------------

    Test.Message() << "...appending function...\n";

    // v_size = size(v);
    cmds.AppendCallFunction("size", args.set(1, "v"), "v_size");

    // v_min = min(v);
    cmds.AppendCallFunction("min", args.set(1, "v"), "v_min");

    // v_max = max(v)
    cmds.AppendCallFunction("max", args.set(1, "v"), "v_max");

    // v_mean = mean(v)
    cmds.AppendCallFunction("mean", args.set(1, "v"), "v_mean");

    // v_rms = rms(v)
    cmds.AppendCallFunction("rms", args.set(1, "v"), "v_rms");

    // v_variance = variance(v)
    cmds.AppendCallFunction("variance", args.set(1, "v"), "v_variance");

    // v_skewness = skewness(v)
    cmds.AppendCallFunction("skewness", args.set(1, "v"), "v_skewness");

    // v_kurtosis = kurtosis(v)
    cmds.AppendCallFunction("kurtosis", args.set(1, "v"), "v_kurtosis");

    // v_allstats = all(v)
    cmds.AppendCallFunction("all", args.set(1, "v"), "v_allstats");

    cmds.AppendIntermediateResult("v_allstats", "result", "Final Result", "" );

    //-------------------------------------------------------------------
    // Execute the requests
    //-------------------------------------------------------------------

    Test.Message() << "...executing...\n";

    cmds.Execute();

    //-------------------------------------------------------------------
    // Perform the same calculations using the C++ classes 
    //-------------------------------------------------------------------

    Test.Message() << "...generating...\n";

    datacondAPI::Sequence<T> v(1, 10);  // simple input data

    datacondAPI::Stats allstats;

    datacondAPI::Statistics  statsObj;

    int size = statsObj.size(v);
    double min = statsObj.min(v);
    double max = statsObj.max(v);
    double mean = statsObj.mean(v);
    double rms = statsObj.rms(v);
    double variance = statsObj.variance(v);
    double skewness = statsObj.skewness(v);
    double kurtosis = statsObj.kurtosis(v);
     
    statsObj.all(allstats, v);
 
    //-------------------------------------------------------------------
    // Check to see if the results calculated from the CallChain
    // match the results calculated at the C++ class level.
    //-------------------------------------------------------------------

    Test.Message() << "...comparing...\n";

    // individual statistics

    check_value(size, *(cmds.GetSymbol("v_size")), "size method");
    check_value(min,  *(cmds.GetSymbol("v_min")),  "min method");
    check_value(max,  *(cmds.GetSymbol("v_max")),  "max method");
    check_value(mean, *(cmds.GetSymbol("v_mean")), "mean method");
    check_value(rms,  *(cmds.GetSymbol("v_rms")),  "rms method");
    check_value(variance, *(cmds.GetSymbol("v_variance")), "variance method");
    check_value(skewness, *(cmds.GetSymbol("v_skewness")), "skewness method");
    check_value(kurtosis, *(cmds.GetSymbol("v_kurtosis")), "kurtosis method");

    // all method (metadata)
    const udt* const statsUdt = cmds.GetSymbol("v_allstats");
    const Stats& allstats_var = dynamic_cast<const Stats&>(*statsUdt);
    
    ILwd::LdasElement* ilwd(allstats_var.ConvertToIlwd(cmds,
                                                       udt::TARGET_METADATA));

    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                              Test.Message(), ILwd::ASCII);
    Test.Message(false) << endl;

    if(allstats.GetSize() !=
       datacondAPI::udt::Cast<datacondAPI::Stats>
         (*(cmds.GetSymbol("v_allstats"))).GetSize())
    {
      Test.Check(false) << "all method - size\n";
    }
    else
    {
      Test.Check(true) << "all method - size\n";
    }

    if(allstats.GetMin() !=
       datacondAPI::udt::Cast<datacondAPI::Stats>
         (*(cmds.GetSymbol("v_allstats"))).GetMin())
    {
      Test.Check(false) << "all method - min\n";
    }
    else
    {
      Test.Check(true) << "all method - min\n";
    }

    if(allstats.GetMax() !=
       datacondAPI::udt::Cast<datacondAPI::Stats>
         (*(cmds.GetSymbol("v_allstats"))).GetMax())
    {
      Test.Check(false) << "all method - max\n";
    }
    else
    {
      Test.Check(true) << "all method - max\n";
    }

    if(allstats.GetRMS() !=
       datacondAPI::udt::Cast<datacondAPI::Stats>
         (*(cmds.GetSymbol("v_allstats"))).GetRMS())
    {
      Test.Check(false) << "all method - rms\n";
    }
    else
    {
      Test.Check(true) << "all method - rms\n";
    }

    // all method (moments)

    check_udt_valarray(allstats, cmds.GetSymbol("v_allstats"), 
          	       "all method - moments");

    return allPass;
  }
  //---------------------------------------------------------------------
  // Catch standard exceptions and set Test results.
  // This macro is defined in "token.hh"
  //---------------------------------------------------------------------

  CATCH(Test);
  return false;

}

//-----------------------------------------------------------------------
// Main
//-----------------------------------------------------------------------

int
main(int ArgC, char** ArgV)
{
  //---------------------------------------------------------------------
  // Initialize UnitTest class
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  try
  {
    //-------------------------------------------------------------------
    // Perform tests
    //-------------------------------------------------------------------

    Test.Check(stats_test<float>("float"))
      << "(n == 10, statistics test <float>)"
      << endl;
    Test.Check(stats_test<double>("double"))
      << "(n == 10, statistics test <double>)"
      << endl;

  }

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Terminate program with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Exit();
}
