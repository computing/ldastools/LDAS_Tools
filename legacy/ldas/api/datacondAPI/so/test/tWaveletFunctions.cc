//$Id: tWaveletFunctions.cc,v 1.11 2009/05/20 00:14:59 emaros Exp $

/*
 Run this test by 'make check' or

 point to directory where example data files located by setting
 variable DATADIR, for example, in csh: 

 setenv DATADIR ../../../../../api/datacondAPI/so/test/

 run program:

 tWaveletFunctions ; echo exit status = $stat
 and see if 'exit status = 0'

 or for more detailed output:

 tWaveletFunctions --verbose ; echo exit status = $stat
*/

#include "datacondAPI/config.h"

#include <fstream>   
#include <cstdlib>

#include "CallChain.hh"
#include "UDT.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "Statistics.hh"

#include "Wavelet.hh"
#include "Biorthogonal.hh"
#include "Haar.hh"
#include "Daubechies.hh"
#include "Symlet.hh"
#include "WaveletUDT.hh"
#include "WaveletFunctions.hh"

#include "general/unittest.h"

// Values defined below must correspond to the values of parameters
// that was used to create expected output data samples.
#define NN 256          	// number of data samples
#define TEST_LEVEL 4		// number of levels of wavelet decomposition
#define TEST_ORDER 10		// wavelet filter length (except Haar)
#define TEST_TREE  0		// wavelet tree type ("0" means diadic tree)
#define TEST_BORDER B_CYCLE	// data border treatment 
#define TEST_LAYER 2		// the number of layer taken for the test

// the following tests are performed:
/*

   test if forward lifting transform for <float> computed properly
   test if inverse lifting transform for <float> computed properly
   test if GetLayer function for Biorthogonal<float> works properly
   test if forward lifting transform for <double> computed properly
   test if inverse lifting transform for <double> computed properly
   test if GetLayer function for Biorthogonal<double> works properly

   test if forward Daubechies transform for <float> computed properly
   test if inverse Daubechies transform for <float> computed properly
   test if forward Daubechies transform for <double> computed properly
   test if inverse Daubechies transform for <double> computed properly

   test if forward Haar transform for <float> computed properly
   test if inverse Haar transform for <float> computed properly
   test if forward Haar transform for <double> computed properly
   test if inverse Haar transform for <double> computed properly

   test if forward symlet transform for <float> computed properly
   test if inverse symlet transform for <float> computed properly
   test if forward symlet transform for <double> computed properly
   test if inverse symlet transform for <double> computed properly
*/

using namespace datacondAPI;
using namespace wat;
using namespace std;   

std::string
dataPath( const char* Filename )
{
  // attach source directory to file name
  std::string path;

  if (getenv("DATADIR"))
  {
    path  = getenv("DATADIR");
    path += "/";
  }
  path += Filename;

  return path;
}

void ilwd_dump(CallChain &chain)
{
// dump result of CallChain in ASCII (for debug only)

    ILwd::LdasContainer* r=NULL;
    cout << "Results:" << std::endl;
    if ((r = chain.GetResults()))
    {
      cout <<" r=" << r<< std::endl;
      r->write(cout, ILwd::ASCII);
      cout << std::endl;
    }
}

General::UnitTest Test;

template<class T>
bool testForward(Wavelet* w , const char* fname, T er)
{
 bool pass = true;

 CallChain chain;

// list of parameters
 const char* par[4]={"input", "wavelet", "level", NULL};

 Scalar<int> lev(TEST_LEVEL); 	// requested level of wavelet decomposition

//input data sequence
 Sequence<T> in(NN);
// example of output sequence from file
 Sequence<T> ex(NN);

// input data from file
 ifstream infile(dataPath("tWaveletFunctions_ts256.dat").c_str());
 if (! infile )
 {
   Test.Check(false) << "file tWaveletFunctions_ts256.dat was not found"
   << std::endl;
   return false;
 }

 for (int i=0; i<NN && infile ; i++) infile >> in[i];
 infile.close();

// load data for comparision with result
 infile.open(dataPath(fname).c_str());
 if (! infile )
 {
   Test.Check(false) << "file " << fname << " was not found" << std::endl;
   return false;
 }

// Initialize TimeSeries from Sequence
 TimeSeries<T> ts=TimeSeries<T>(in, 2048.);

 for (int i=0; i<NN && infile ; i++) infile >> ex[i];
 infile.close();

 Statistics stat;
 double rms_ex = stat.rms(ex);

 Test.Message() << "testForward: example data RMS=" << rms_ex << std::endl;

 try {

    // Need to create copies of the input data because once AddSymbol
    // has been called on an object it's the CallChains responsibility
    // to delete it
    CallChain::Symbol* input = ts.Clone();
    CallChain::Symbol* wl = w->Clone();
    CallChain::Symbol* level = lev.Clone();

    // set names and references for input parameters
    chain.AddSymbol("input", input);
    chain.AddSymbol("wavelet", wl);
    chain.AddSymbol("level", level);

    // Don't need to do anything for the result to be created, it is
    // created automatically by the LHS of the assignment

    chain.AppendCallFunction("WaveletForward",  par, "output");

    chain.AppendIntermediateResult("output", "result", "Final Result", "" );

    chain.Execute();

// dump CallChain symbol table (for debug)
//    chain.Dump();

// dump output in ASCII (for debug)
//   ilwd_dump(chain);

// get pointer to output
    udt* out=chain.GetSymbol("output");

// another way to access output 
//    udt* out=const_cast<udt*>(chain.Peek());

    if ( udt::IsA<Sequence<T> >(*out) )
    {
       Sequence<T>* s = &(udt::Cast<Sequence<T> >(*out));

// compare output data samples with reference data from file

       ex -= *s;		// take difference between sequences

       double rmsd = stat.rms(ex);
       double maxd = stat.max(ex);
       if ( maxd > er*rms_ex ) pass = false;

       Test.Message() << "testForward: RMS of  (example-output) ="
       << rmsd << std::endl;

       Test.Message() << "testForward: maximum (example-output) ="
       << maxd << std::endl;

    }
    else pass = false;

// check wavelet metadata
    if ( udt::IsA<WaveletUDT<T> >(*out) )
    {  
      WaveletUDT<T>* wu = &(udt::Cast<WaveletUDT<T> >(*out));
      if ( wu->pWavelet )
      {
        if ( wu->pWavelet->m_WaveType != w->m_WaveType ) pass = false;
        if ( wu->pWavelet->m_Level != lev.GetValue() )   pass = false;
        if ( wu->pWavelet->m_TreeType  != TEST_TREE )   pass = false;
        if ( wu->pWavelet->m_WaveType != HAAR )
          if ( wu->pWavelet->m_Border  != TEST_BORDER )   pass = false;
      }
    }
    else Test.Check(false) << "testForward: incorrect wavelet metadata" 
    << std::endl;

  } // end of 'try'

  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "Caught a CallChain exception: " 
    << x.what() << std::endl;
  }

  catch (std::invalid_argument& x)
  {
      Test.Check(false) << x.what() << std::endl;
  }

  catch (std::exception& x)
  {
      Test.Check(false) << x.what() << std::endl;
  }

  catch(...)
  {
    Test.Check(false) << "Caught an Unknown exception." << std::endl;
  }

  return pass;
}

template<class T>
bool testInverse(Wavelet *w, const char* fname, T er)
{
 bool pass = true;

 w->setLevel(TEST_LEVEL);

 CallChain chain;

// list of parameters
 const char* par[2]={"input", NULL};

// input sequence
 Sequence<T> in(NN);
// example of output sequence from file
 Sequence<T> ex(NN);

// input data from file
 ifstream infile(dataPath(fname).c_str());
 if (! infile )
 {
   Test.Check(false) << "file " << fname << " was not found" << std::endl;
   return false;
 }

 for (int i=0; i<NN && infile ; i++) infile >> in[i];
 infile.close();

// initialize WaveletUDT from Sequence
 WaveletUDT<T>* wi = new WaveletUDT<T>(in, *w);

// load data for comparision with result
 infile.open(dataPath("tWaveletFunctions_ts256.dat").c_str());

 if (! infile )
 {
   Test.Check(false) << "file tWaveletFunctions_ts256.dat was not found"
   << std::endl;
   return false;
 }

 for (int i=0; i<NN && infile ; i++) infile >> ex[i];
 infile.close();

 Statistics stat;
 double rms_ex = stat.rms(ex);
 Test.Message() << "testInverse: example data RMS=" << rms_ex << std::endl;

 try {

    // Need to create copies of the input data because once AddSymbol
    // has been called on an object it's the CallChains responsibility
    // to delete it
    CallChain::Symbol* input = (*wi).Clone();

    // set names and references for input parameters
    chain.AddSymbol("input", input);

    // Don't need to do anything for the result to be created, it is
    // created automatically by the LHS of the assignment

    chain.AppendCallFunction("WaveletInverse",  par, "output");

    chain.AppendIntermediateResult("output", "result", "Final Result", "" );

    chain.Execute();

// dump CallChain symbol table (for debug)
//    chain.Dump();

// dump output in ASCII (for debug)
//   ilwd_dump(chain);

// get pointer to output
    udt* out=chain.GetSymbol("output");

// another way to access output
//    udt* out=const_cast<udt*>(chain.Peek());

    if ( udt::IsA<TimeSeries<T> >(*out) )
    {
       TimeSeries<T>* s = &(udt::Cast<TimeSeries<T> >(*out));

// compare output data samples with reference data from file
       ex -= *s;                // take difference between sequences

       double rmsd = stat.rms(ex);
       double maxd = stat.max(ex);
       if ( maxd > er*rms_ex ) pass = false;

       Test.Message() << "testInverse: RMS of  (example-output)="
	<< rmsd << std::endl;

       Test.Message() << "testInverse: maximum (example-output)="
        << maxd << std::endl;
    }
    else pass = false;

  } // end of 'try'

  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "Caught a CallChain exception: "
    << x.what() << std::endl;
  }

  catch (std::invalid_argument& x)
  {
      Test.Check(false) << x.what() << std::endl;
  }

  catch (std::exception& x)
  {
      Test.Check(false) << x.what() << std::endl;
  }

  catch(...)
  {
    Test.Check(false) << "Caught an Unknown exception" << std::endl;
  }

  return pass;
}

template<class T>
bool testGetLayer(Wavelet *w, const char* fname, const char* flayer, T er)
{
 bool pass = true;

 Scalar<int> layer(TEST_LAYER);

 w->setLevel(TEST_LEVEL);

 CallChain chain;

// list of parameters
 const char* par[3]={"input", "nlayer", NULL};

// input sequence
 Sequence<T> in(NN);
// example of output sequence from file
 Sequence<T> ex(NN/(1 << (TEST_LAYER+1) ));

// input data from file
 ifstream infile(dataPath(fname).c_str());             // file with input data
 if (! infile )
 {
   Test.Check(false) << "file " << fname << " was not found" << std::endl;
   return false;
 }

 for (int i=0; i<NN && infile ; i++) infile >> in[i];
 infile.close();

// initialize WaveletUDT from Sequence
 WaveletUDT<T>* wi = new WaveletUDT<T>(in, *w);

// load data for comparision with result
 infile.open(dataPath(flayer).c_str());          // file with reference result
 if (! infile )
 {
   Test.Check(false) << "file " << flayer << " was not found" << std::endl;
   return false;
 }

 for (int i=0; i<NN && infile ; i++) infile >> ex[i];
 infile.close();

 Statistics stat;
 double rms_ex = stat.rms(ex);
 Test.Message() << "testGetLayer: example data RMS=" << rms_ex << std::endl;

 try {

    // Need to create copies of the input data because once AddSymbol
    // has been called on an object it's the CallChains responsibility
    // to delete it
    CallChain::Symbol* input = (*wi).Clone();
    CallChain::Symbol* l = layer.Clone();

    // set names and references for input parameters
    chain.AddSymbol("input", input);
    chain.AddSymbol("nlayer", l);

    // Don't need to do anything for the result to be created, it is
    // created automatically by the LHS of the assignment

    chain.AppendCallFunction("GetLayer",  par, "output");

    chain.AppendIntermediateResult("output", "result", "Final Result", "" );

    chain.Execute();

// dump CallChain symbol table (for debug)
//    chain.Dump();

// dump output in ASCII (for debug)
//   ilwd_dump(chain);

// get pointer to output
    udt* out=chain.GetSymbol("output");

// another way to access output
//    udt* out=const_cast<udt*>(chain.Peek());

    if ( udt::IsA<Sequence<T> >(*out) )
    {
       Sequence<T>* s = &(udt::Cast<Sequence<T> >(*out));

// compare output data samples with reference data from file
       ex -= *s;                // take difference between sequences

       double rmsd = stat.rms(ex);
       double maxd = stat.max(ex);
       if ( maxd > er*rms_ex ) pass = false;

       Test.Message() << "testGetLayer: RMS of  (example-output) ="
       << rmsd << std::endl;

       Test.Message() << "testGetLayer: maximum (example-output) ="
       << maxd << std::endl;

    }
    else pass = false;

  } // end of 'try'

  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "Caught a CallChain exception: "
    << x.what() << std::endl;
  }

  catch (std::invalid_argument& x)
  {
    Test.Check(false) << x.what() << std::endl;
  }

  catch (std::exception& x)
  {
    Test.Check(false) << x.what() << std::endl;
  }

  catch(...)
  {
    Test.Check(false) << "Caught an Unknown exception." << std::endl;
  }

  return pass;
}

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message(false) <<
    "$Id: tWaveletFunctions.cc,v 1.11 2009/05/20 00:14:59 emaros Exp $"
    << std::endl;
   
  Wavelet* w;
  w = new Biorthogonal<float>(TEST_ORDER, TEST_TREE, TEST_BORDER);

  Test.Check(testForward<float>(w, "tWaveletFunctions_wl256_4_10_0.dat", 1.e-6))
  << "(test if forward Biorthogonal transform for <float> computed properly)"
  << std::endl;

  Test.Check(testInverse<float>(w, "tWaveletFunctions_wl256_4_10_0.dat", 1.e-6))
  << "(test if Inverse Biorthogonal transform for <float> computed properly)"
  << std::endl;

  Test.Check(testGetLayer<float>(w,"tWaveletFunctions_wl256_4_10_0.dat",
  "tWaveletFunctions_wla2_4_10_0.dat",1.e-6))
  << "(test if GetLayer function for Biorthogonal<float> works properly)"
  << std::endl;

  delete w;
  w = new Biorthogonal<double>(TEST_ORDER, TEST_TREE, TEST_BORDER);

  Test.Check(testForward<double>(w, "tWaveletFunctions_wl256_4_10_0.dat",1.e-14))
  << "(test if forward Biorthogonal transform for <double> computed properly)"
  << std::endl;

  Test.Check(testInverse<double>(w, "tWaveletFunctions_wl256_4_10_0.dat", 1.e-14))
  << "(test if Inverse Biorthogonal transform for <double> computed properly)"
  << std::endl;

  Test.Check(testGetLayer<double>(w,"tWaveletFunctions_wl256_4_10_0.dat",
  "tWaveletFunctions_wla2_4_10_0.dat",1.e-14))
  << "(test if GetLayer function for Biorthogonal<double> works properly)"
  << std::endl;

  delete w;
  w = new Daubechies<float>(TEST_ORDER, TEST_TREE, TEST_BORDER);

  Test.Check(testForward<float>(w, "tWaveletFunctions_wd256_4_10_0.dat", 1.e-6))
  << "(test if forward Daubechies transform for <float> computed properly)" 
  << std::endl;

  Test.Check(testInverse<float>(w, "tWaveletFunctions_wd256_4_10_0.dat", 1.e-6))
  << "(test if inverse Daubechies transform for <float> computed properly)"
  << std::endl;

  delete w;
  w = new Daubechies<double>(TEST_ORDER, TEST_TREE, TEST_BORDER);

  Test.Check(testForward<double>(w, "tWaveletFunctions_wd256_4_10_0.dat",1.e-14))
  << "(test if forward Daubechies transform for <double> computed properly)" 
  << std::endl;

  Test.Check(testInverse<double>(w, "tWaveletFunctions_wd256_4_10_0.dat",1.e-11))
  << "(test if inverse Daubechies transform for <double> computed properly)"
  << std::endl;

  delete w;
  w = new Haar<float>(TEST_TREE);

  Test.Check(testForward<float>(w, "tWaveletFunctions_wh256_4_0.dat", 1.e-6))
  << "(test if forward Haar transform for <float> computed properly)"
  << std::endl;

  Test.Check(testInverse<float>(w, "tWaveletFunctions_wh256_4_0.dat", 1.e-6))
  << "(test if inverse Haar transform for <float> computed properly)"
  << std::endl;

  delete w;
  w = new Haar<double>(TEST_TREE);

  Test.Check(testForward<double>(w, "tWaveletFunctions_wh256_4_0.dat",1.e-14))
  << "(test if forward Haar transform for <double> computed properly)"
  << std::endl;

  Test.Check(testInverse<double>(w, "tWaveletFunctions_wh256_4_0.dat",1.e-14))
  << "(test if Inverse Haar transform for <double> computed properly)"
  << std::endl;

  delete w;
  w = new Symlet<float>(TEST_ORDER, TEST_TREE, TEST_BORDER);

  Test.Check(testForward<float>(w, "tWaveletFunctions_ws256_4_10_0.dat", 1.e-6))
  << "(test if forward symlet transform for <float> computed properly)"
  << std::endl;

  Test.Check(testInverse<float>(w, "tWaveletFunctions_ws256_4_10_0.dat", 1.e-6))
  << "(test if inverse symlet transform for <float> computed properly)"
  << std::endl;

  delete w;
  w = new Symlet<double>(TEST_ORDER, TEST_TREE, TEST_BORDER);

  Test.Check(testForward<double>(w, "tWaveletFunctions_ws256_4_10_0.dat",1.e-14))
  << "(test if forward symlet transform for <double> computed properly)"
  << std::endl;

  Test.Check(testInverse<double>(w, "tWaveletFunctions_ws256_4_10_0.dat",1.e-12))
  << "(test if inverse symlet transform for <double> computed properly)"
  << std::endl;

// all done!!
  Test.Exit();
}
