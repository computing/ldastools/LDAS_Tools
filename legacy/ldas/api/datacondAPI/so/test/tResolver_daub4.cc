//tResolver_daub4.cc

#include "datacondAPI/config.h"

#include <vector>
#include <valarray>

#include "general/unittest.h"

#include "SequenceUDT.hh"
#include "WaveletPSU.hh"
#include "daub4.hh"
#include "resolver.hh"
#include "result.hh"

using namespace datacondAPI::psu;

void TestOne();
void TestTwo();
void ExceptionTest();

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message() << "Executing TestOne." << endl;
  TestOne();

  Test.Message() << "Executing TestTwo." << endl;
  TestTwo();

  Test.Message() << "Executing ExceptionTest." << endl;
  ExceptionTest();

  Test.Exit();

} //end main


void TestOne()
{
  datacondAPI::Sequence<double> data(0.0, 8);
  data[2] = 1.0;

  Daub4 daub4Wavelet;

  Resolver test(daub4Wavelet, 4);

  //test Resolver function called getLevels
  Test.Message() << "Testing Resolver::getLevels." << endl;
  int levelsSet = test.getLevels();
  Test.Message() << "Levels set to " << levelsSet << endl;

  bool levelCheck = false;
  if (levelsSet == 4)
    levelCheck = true;
  Test.Check(levelCheck) << "Levels set." << endl;

  //test Resolver function called setLevels
  Test.Message() << "Testing Resolver::setLevels." << endl;
  test.setLevels(2);
  levelsSet = test.getLevels();
  Test.Message() << "Levels reset to " << levelsSet << endl;

  levelCheck = false;
  if (levelsSet == 2)
    levelCheck = true;
  Test.Check(levelCheck) << "Levels reset." << endl;

  Result<double> testResult;

  //test Resolver function called applyFilter
  Test.Message() << "Testing Resolver::applyFilter." << endl;
  try{
    test.applyFilter(testResult, data);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //test Result function called getLevels:
  Test.Message() << "Testing Result::getLevels." << endl;
  int resultLevels = testResult.getLevels();
  Test.Message() << "Levels of decomposition = "
		 << resultLevels << endl;
  levelCheck = false;
  if (resultLevels == 2)
    levelCheck = true;
  Test.Check(levelCheck) << "Result has correct number of levels"
			 << " of decomposition." << endl;

  //check filtered signal
  datacondAPI::Sequence<double> first;
  first = testResult.getResult(0);

  datacondAPI::Sequence<double> second;
  second = testResult.getResult(1);

  datacondAPI::Sequence<double> lowpass;
  lowpass = testResult.getResult(2);

  Test.Message() << "First decomposition:" << endl;
  for (int i = 0; i < first.size(); i++)
    Test.Message() << first[i] << endl;

  Test.Message() << "Second decomposition:" << endl;
  for (int j = 0; j < second.size(); j++)
    Test.Message() << second[j] << endl;

  Test.Message() << "Final lowpass result:" << endl;
  for (int k = 0; k < lowpass.size(); k++)
    Test.Message() << lowpass[k] << endl;
  
  //expected output
  datacondAPI::Sequence<double> e_first(0.0, 4);
  e_first[1] = (1 - sqrt(3))/4;
  e_first[2] = (3 + sqrt(3))/4;
  datacondAPI::Sequence<double> e_second(0.0, 2);
  e_second[1] = (6 - 6*sqrt(3))/16;
  datacondAPI::Sequence<double> e_lowpass(0.0, 2);
  e_lowpass[1] = (6 + 6*sqrt(3))/16;

  Test.Message() << "Expected first decomposition:" << endl;
  for (int i = 0; i < e_first.size(); i++)
    Test.Message() << e_first[i] << endl;

  Test.Message() << "Expected second decomposition:" << endl;
  for (int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;

  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_first - first);
  datacondAPI::Sequence<double> second_difference(e_second - second);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - lowpass);

  Test.Message() << "Difference in first decomposition:" << endl;
  for (int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in second decomposition:" << endl;
  for (int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_first(e_first == first);
  std::valarray<bool> check_second(e_second == second);
  std::valarray<bool> check_lowpass(e_lowpass == lowpass);

  bool pass1, pass2, pass3;
  pass1 = pass2 = pass3 = true;

  for (int i = 0; i < check_first.size(); i++)
    pass1 = pass1 && check_first[i];

  for (int i = 0; i < check_second.size(); i++)
    pass2 = pass2 && check_second[i];

  for (int i = 0; i < check_lowpass.size(); i++)
    pass3 = pass3 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true)
    overallpass = true;

  Test.Check(overallpass) << "Filtered output." <<endl;


}//end TestOne.


void TestTwo()
{
  datacondAPI::Sequence<double> data(0.0, 48);
  data[3] = 1.0;

  Daub4 daub4Wavelet;
  Resolver test;
  Result<double> testResult;

  Test.Message() << "Testing Resolver::isReady." << endl;
  bool ready_check = false;
  ready_check = test.isReady();

  if (ready_check == false)
  {
    Test.Message() << "Wavelet and/or levels were not set." << endl;
    test.setWavelet(daub4Wavelet);
    Test.Message() << "setWavelet executed." << endl;
    test.setLevels(4);
    Test.Message() << "setLevels executed." << endl;
    test.getWavelet();
    Test.Message() << "getWavelet executed." << endl;
  }
  ready_check = test.isReady();

  Test.Check(ready_check) << "isReady" << endl;

  Resolver test2(daub4Wavelet, 4);
  Test.Message()<< "Applying filter." << endl;
  try{
    test2.applyFilter(testResult, data);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //test Result function called getLevels:
  Test.Message() << "Testing Result::getLevels." << endl;
  int resultLevels = testResult.getLevels();
  Test.Message() << "Levels of decomposition = "
		 << resultLevels << endl;
  bool levelCheck = false;
  if (resultLevels == 4)
    levelCheck = true;
  Test.Check(levelCheck) << "Result has correct number of levels"
			 << " of decomposition." << endl;

  //check filtered signal
  datacondAPI::Sequence<double> zero = testResult.getResult(0);

  datacondAPI::Sequence<double> one = testResult.getResult(1);

  datacondAPI::Sequence<double> two = testResult.getResult(2);

  datacondAPI::Sequence<double> three = testResult.getResult(3);

  datacondAPI::Sequence<double> lowpass = testResult.getResult(4);

  Test.Message() << "Zeroth decomposition:" << endl;
  for (int i = 0; i < zero.size(); i++)
    Test.Message() << zero[i] << endl;

  Test.Message() << "First decomposition:" << endl;
  for (int j = 0; j < one.size(); j++)
    Test.Message() << one[j] << endl;

  Test.Message() << "Second decomposition:" << endl;
  for (int j = 0; j < two.size(); j++)
    Test.Message() << two[j] << endl;

  Test.Message() << "Third decomposition:" << endl;
  for (int j = 0; j < three.size(); j++)
    Test.Message() << three[j] << endl;

  Test.Message() << "Final lowpass result:" << endl;
  for (int k = 0; k < lowpass.size(); k++)
    Test.Message() << lowpass[k] << endl;
 
  //expected output
  datacondAPI::Sequence<double> e_zero(0.0, 24);
  e_zero[2] = (-3 + sqrt(3))/4;
  e_zero[3] = (-1 - sqrt(3))/4;
  datacondAPI::Sequence<double> e_first(0.0, 12);
  e_first[1] = (-2*sqrt(3))/16;
  e_first[2] = (6 + 10*sqrt(3))/16;
  e_first[3] = +1.0/8.0;
  datacondAPI::Sequence<double> e_second(0.0, 6);
  e_second[1] = (6 - 14*sqrt(3))/64;
  e_second[2] = -24.0/64.0;
  e_second[3] = (2 - 2*sqrt(3))/64;
  datacondAPI::Sequence<double> e_third(0.0, 3);
  e_third[1] = (12 - 19*sqrt(3))/64;
  e_third[2] = (-15 - 6*sqrt(3))/32;
  datacondAPI::Sequence<double> e_lowpass(0.0, 3);
  e_lowpass[1] = (33 + 26*sqrt(3))/64;
  e_lowpass[2] = (12 - 11*sqrt(3))/32;

  Test.Message() << "Expected zeroth decomposition:" << endl;
  for (int i = 0; i < e_zero.size(); i++)
    Test.Message() << e_zero[i] << endl;

  Test.Message() << "Expected first decomposition:" << endl;
  for (int j = 0; j < e_first.size(); j++)
    Test.Message() << e_first[j] << endl;

  Test.Message() << "Expected second decomposition:" << endl;
  for (int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected third decomposition:" << endl;
  for (int j = 0; j < e_third.size(); j++)
    Test.Message() << e_third[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;

  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_zero - zero);
  datacondAPI::Sequence<double> second_difference(e_first - one);
  datacondAPI::Sequence<double> third_difference(e_second - two);
  datacondAPI::Sequence<double> fourth_difference(e_third - three);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - lowpass);

  Test.Message() << "Difference in zeroth decomposition:" << endl;
  for (int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in first decomposition:" << endl;
  for (int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in second decomposition:" << endl;
  for (int j = 0; j < third_difference.size(); j++)
    Test.Message() << third_difference[j] << endl;

  Test.Message() << "Difference in third decomposition:" << endl;
  for (int j = 0; j < fourth_difference.size(); j++)
    Test.Message() << fourth_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_zero(e_zero == zero);
  std::valarray<bool> check_one(e_first == one);
  std::valarray<bool> check_two(e_second == two);
  std::valarray<bool> check_three(e_third == three);
  std::valarray<bool> check_lowpass(e_lowpass == lowpass);

  bool pass1, pass2, pass3, pass4, pass5;
  pass1 = pass2 = pass3 = pass4 = pass5 = true;

  for (int i = 0; i < check_zero.size(); i++)
    pass1 = pass1 && check_zero[i];

  for (int i = 0; i < check_one.size(); i++)
    pass2 = pass2 && check_one[i];

  for (int i = 0; i < check_two.size(); i++)
    pass3 = pass3 && check_two[i];

  for (int i = 0; i < check_three.size(); i++)
    pass4 = pass4 && check_three[i];

  for (int i = 0; i < check_lowpass.size(); i++)
    pass5 = pass5 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true
     && pass4 == true && pass5 == true)
    overallpass = true;

  Test.Check(overallpass) << "Filtered output." <<endl;
 
}//end TestTwo.


void ExceptionTest()
{
  //create a data sequence of odd length
  datacondAPI::Sequence<double> data(0.0, 7);
  data[2] = 1.0;

  Daub4 daub4Wavelet;

  //error strings
  string what = "Levels must be greater than zero";
  string what2 = "Data input must have even number of data points.";

  //give Resolver constructor an invalid argument
  try
    {
      Resolver test(daub4Wavelet, 0);
      Test.Check(false) << what << endl;
    }
  catch(invalid_argument& i)
    {
      Test.Check(true) << what << " (" <<i.what()<< ")" << endl;
    }

  Resolver test;

  //give setLevels an invalid argument
  try
    {
      test.setLevels(-4);
      Test.Check(false) << what << endl;
    }
  catch(invalid_argument& j)
    {
      Test.Check(true) << what << " (" << j.what() << ")" << endl;
    }

  //Test.Message() << "you got here." << endl;

  Result<double> output;

  //give applyFilter an invalid argument
  //Test.Message() << "you got here next." << endl;

  try
    {
      //    Test.Message() << "you got here inside the try." << endl;
      test.applyFilter(output, data);
      //    Test.Message() << "you got here after applyFilter." << endl;
      Test.Check(false) << what2 << endl;
    }
  catch(exception& k)
    {
      Test.Check(true) << what2 << " (" << k.what() << ")" << endl;
    }




} //end ExceptionTest










