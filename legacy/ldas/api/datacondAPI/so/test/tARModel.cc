#include "datacondAPI/config.h"

#include <complex>

#include "general/unittest.h"

#include "ARModel.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

#include "random.hh"

using namespace datacondAPI;

General::UnitTest Test;

void test_specific()
{
    // ARModel()
    {
	ARModel ar;
	Test.Check(ar.getOrder() == 0, "ARModel ar; ar.getOrder() == 0");
	try
	{
	    udt* p = 0;
	    ar.getTheta(p);
	    Test.Check(false, "ARModel ar; ar.getTheta(p); did not throw");
	}
	catch (const std::invalid_argument& x)
	{
	    Test.Check(true, "ARModel ar; ar.getTheta(p); threw");
	}
    }
    // explicit ARModel(const int&)
    {
	ARModel ar(7);
	try
	{
	    udt* p = 0;
	    ar.getTheta(p);
	    Test.Check(false, "ARModel ar(7); ar.getTheta(p); did not throw");
	}
	catch (const std::invalid_argument& x)
	{
	    Test.Check(true, "ARModel ar(7); ar.getTheta(p); threw");
	}
	// int getOrder()
	Test.Check(ar.getOrder() == 7, "ARModel ar(7); ar.getOrder() == 0");
	// void getOrder(udt*&)
	{
	    // null pointer
	    {
		udt* p = 0;
		ar.getOrder(p);
		Test.Check(udt::Cast<Scalar<int> >(*p).GetValue() == 7, "ARModel ar(7); udt* p = 0; ar.getOrder(p); udt::Cast<Scalar<int> >(*p).GetValue() == 7");
	    }
	    // valid type
	    {
		Scalar<int> n(0);
	        udt* p = &n;
		ar.getOrder(p);
		Test.Check(n.GetValue() == 7, "ARModel ar(7); Scalar<int> n(0); udt* p = &n; ar.getOrder(p); n.GetValue() == 7");
	    }
	    // invalid type
	    try
	    {
		Scalar<float> n(0);
		udt* p = &n;
		ar.getOrder(p);
		Test.Check(false, "ARModel ar(7); Scalar<float> n(0); udt* p = &n; ar.getOrder(p) (no throw)");
	    }
	    catch (const std::invalid_argument& x)
	    {
		Test.Check(true, "ARModel ar(7); Scalar<float> n(0); udt* p = &n; ar.getOrder(p) (threw)");
	    }
	}
    }
    // explicit ARModel(const udt&)
    {
	// valid type
	{
	    Scalar<int> n(7);
	    ARModel ar(n);
	    Test.Check(ar.getOrder() == 7, "Scalar<int> n(7); ARModel ar(n); ar.getOrder() == 0");
	    try
	    {
		udt* p = 0;
		ar.getTheta(p);
		Test.Check(false,
			   "Scalar<int> n(7); ARModel ar(n); ar.getTheta(p); did not throw");
	    }
	    catch (const std::invalid_argument& x)
	    {
		Test.Check(true, "Scalar<int> n(7); ARModel ar(n); ar.getTheta(p); threw");
	    }
	}
	// invalid type
	try
        {
	    Scalar<float> n(7.0);
	    ARModel ar(n);
	    Test.Check(false, "Scalar<float> n(7.0); ARModel ar(n); did not throw");
	}
	catch (const std::invalid_argument& x)
        {
	    Test.Check(true, "Scalar<float> n(7.0); ARModel ar(n); threw");
	}
    }
    // ARModel(const udt& x, const udt& n)
    {
	Scalar<float> x(1.0);
	// bad n and x
	try
	{
	    ARModel ar(x, x);
	    Test.Check(false, "Scalar<float> x(1.0); ARModel ar(x, x); did not throw");
	}
	catch (const std::invalid_argument& e)
        {
	    Test.Check(true, "Scalar<float> x(1.0); ARModel ar(x, x); threw");
	}
	// bad x
	try
	{
	    Scalar<int> n(1);
	    ARModel ar(x, n);
	    Test.Check(false, "Scalar<float> x(1.0); Scalar<int> n(1); ARModel ar(x, n); did not throw");
	}
	catch (const std::invalid_argument& e)
        {
	    Test.Check(true, "Scalar<float> x(1.0); Scalar<int> n(1); ARModel ar(x, n); threw");
	}
    }
    // int getOrder() implicit above
    // void getOrder(udt*&) implicit above
    // void getTheta(udt*&) (failure: no state) implicit above
    // void getFilter(udt*&)
    try
    {
	ARModel ar;
	udt* b = 0;
	ar.getFilter(b);
	Test.Check(false, "ARModel ar; udt* b = 0; ar.getFilter(b); (no throw)");
    }
    catch (const std::invalid_argument& e)
    {
	Test.Check(true, "ARModel ar; udt* b = 0; ar.getFilter(b); (threw)");
    }
    // void setOrder(const int&)
    {
	ARModel ar;
	ar.setOrder(7);
	Test.Check(ar.getOrder() == 7, "ARModel ar; ar.setOrder(7); ar.getOrder() == 7");
    }
    // void setOrder(const udt&)
    {
	// valid type
	{
	    ARModel ar;
	    Scalar<int> n(7);
	    ar.setOrder(n);
	    Test.Check(ar.getOrder() == 7, "ARModel ar; Scalar<int> n(1); ar.setOrder(n); ar.getOrder() == 7");
	}
	// invalid type
	try
	{
	    ARModel ar;
	    Scalar<float> n(7.0);
	    ar.setOrder(n);
	    Test.Check(false, "ARModel ar; Scalar<float> n(7.0); ar.setOrder(n) (no throw)");
	}
	catch (const std::invalid_argument& x)
        {
	    Test.Check(true, "ARModel ar; Scalar<float> n(7.0); ar.setOrder(n) (threw)");
	}
    }
    // void setTheta(const udt&)
    try
    {
	ARModel ar;
	Scalar<int> theta(0);
	ar.setTheta(theta);
	Test.Check(false, "ARModel ar; Scalar<int> theta; ar.setTheta(theta); (no throw)");
    }
    catch (const std::invalid_argument& x)
    {
	Test.Check(true, "ARModel ar; Scalar<int> theta; ar.setTheta(theta); (no threw)");
    }
    // void apply(udt*& y, const udt& x)
    try
    {
	ARModel ar;
	udt* y = 0;
	Scalar<int> x(0);
	ar.apply(y, x);
	Test.Check(false, "ARModel ar; udt* y = 0; Scalar<int> x; ar.apply(y, x); (no throw)");
    }
    catch (const std::invalid_argument& x)
    {
	Test.Check(true, "ARModel ar; udt* y = 0; Scalar<int> x; ar.apply(y, x); (threw)");
    }
    // void refine(const udt& x)
    try
    {
	ARModel ar;
	Scalar<int> x(0);
	ar.refine(x);
	Test.Check(false, "ARModel ar; Scalar<int> x; ar.refine(x); (no throw)");
    }
    catch (const std::invalid_argument& e)
    {
	Test.Check(true, "ARModel ar; Scalar<int> x; ar.refine(x); (threw)");
    }
    // merit(const udt& m, const udt& x)
    try
    {
	ARModel ar;
	udt* p = 0;
	Scalar<int> x(0);
	ar.merit(p, x);
	Test.Check(false, "ARModel ar; udt* p = 0; Scalar<int> x; ar.merit(p, x); (no throw)");
    }
    catch (const std::invalid_argument& e)
    {
	Test.Check(true, "ARModel ar; udt* p = 0; Scalar<int> x; ar.merit(p, x); (threw)");
    }
}

template<typename type>
bool equality(const std::valarray<type> a, const std::valarray<type> b)
{
    if (a.size() != b.size())
    {
	return false;
    }
    for (unsigned int i = 0; i < a.size(); ++i)
    {
	if (a[i] != b[i])
        {
	    return false;
	}
    }
    return true;
}

template<typename type>
static std::ostream& operator<<(std::ostream& a, const std::valarray<type>& b)
{
    a << "{ ";
    for (unsigned int i = 0; i < b.size(); ++i)
    {
	a << b[i] << ' ';
    }
    a << "}";
    return a;
}

static gasdevstate seed;

void gasdev(std::valarray<float>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<double>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<std::complex<float> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<float>(gasdev(seed), gasdev(seed));
    }
}

void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}

template<typename type>
void test_generic(const std::string& name)
{
    Test.Message() << name << '\n';

    std::valarray<type> theta(2);
    {
	theta[0] = 2.0;
	theta[1] = -1.0;
    }
    // explict ARModel(const std::valarray<type>& theta)
    {	
	ARModel ar(theta);
	Test.Check(ar.getOrder() == (int) theta.size(),
		   "ARModel ar(theta); ar.getOrder() == (int) theta.size()");
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ARModel ar(theta); ar.getTheta(theta_out); equality(theta, theta_out)");

	// ARModel(const ARModel&)
	ARModel br(ar);
	Test.Check(br.getOrder() == (int) theta.size(),
		   "ARModel ar(theta); ARModel br(ar); br.getOrder() == (int) theta.size()");
	br.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ARModel br(ar); br.getTheta(theta_out); equality(theta, theta_out)");

	// ARModel& operator=(const ARModel&)
	ARModel cr;
	cr = ar;
	Test.Check(cr.getOrder() == (int) theta.size(),
		   "ARModel ar(theta); ARModel cr; cr = ar; cr.getOrder() == (int) theta.size()");
	cr.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ARModel cr; cr = ar; cr.getTheta(theta_out); equality(theta, theta_out)");

	// ARModel* Clone() const
	ARModel *p = ar.Clone();
	Test.Check(p != 0, "ARModel *p = ar.Clone(); p != 0");
	Test.Check(p->getOrder() == (int) theta.size(),
		   "ARModel *p = ar.Clone(); p->getOrder() == (int) theta.size()");
	p->getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ARModel* p = ar.Clone(); p->getTheta(theta_out); equality(theta, theta_out)");	
	delete p;
    }

    // explicit ARModel(const udt&)
    {
	Sequence<type> theta_udt(theta);
	ARModel ar(theta_udt);

	Test.Check(ar.getOrder() == (int) theta.size(),
		   "ARModel ar(theta_udt); ar.getOrder() == (int) theta.size()");
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ARModel ar(theta_udt); ar.getTheta(theta_out); equality(theta, theta_out)");
    }

    std::valarray<type> x(7);
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = i;
    }

    // ARModel(const std::valarray<type>& x, const int& n)
    {
	ARModel ar(x, 2);
	Test.Check(ar.getOrder() == 2, "ARModel(x, 2); ar.getOrder() == 2");
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Message() << theta_out << '\n';
    }
    
    // ARModel(const udt& x, const udt& n)
    {
	Sequence<type> x_udt(x);
	Scalar<int> n_udt(2);
	ARModel ar(x_udt, n_udt);
	Test.Check(ar.getOrder() == 2, "ARModel(x_udt, n_udt); ar.getOrder() == 2");
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Message() << theta_out << '\n';

	// invalid x type
	try
	{
	    ARModel br(n_udt, n_udt);
	    Test.Check(false, "ARModel br(n_udt, n_udt); (no throw)");
	}
	catch (const std::invalid_argument& e)
	{
	    Test.Check(true, "ARModel br(n_udt, n_udt); (threw)");
	}
    }

    // void getTheta(const std::valarray<type>&) const implicit above

    // void getTheta(udt*&) const
    {
	ARModel ar(theta);
	
	// null input
	{
	    udt* p = 0;
	    ar.getTheta(p);
	    Test.Check(p != 0, "ARModel ar(theta); udt* p = 0; ar.getTheta(p); p != 0");
	    Test.Check(equality(udt::Cast<Sequence<type> >(*p), theta), "ARModel ar(theta); udt* p = 0; ar.getTheta(p); equality(udt::Cast<Sequence<type> >(*p), theta)");
	    delete p;
	}
	// valid input
	{
	    Sequence<type> theta_out;
	    udt* p = &theta_out;
	    ar.getTheta(p);
	    Test.Check(equality(theta_out, theta), "ARModel ar(theta); udt* p = &theta_out; ar.getTheta(p); equality(udt::Cast<Sequence<type> >(*p), theta)");	    
	}
	// invalid input
	try
	{
	    Scalar<int> theta_not(0);
	    udt* p = &theta_not;
	    ar.getTheta(p);
	    Test.Check(false, "ARModel ar(theta); Scalar<int> theta_not; udt* p = &theta_not; ar.getTheta(p); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "ARModel ar(theta); Scalar<int> theta_not; udt* p = &theta_not; ar.getTheta(p); (threw)");
	}
    }

    // void getFilter(udt*&) const
    {
	ARModel ar(theta);
	std::valarray<type> filter((int) theta.size() + 1);
	filter[0] = type();
	filter[std::slice(1, (int) theta.size(), 1)] = theta;
	Test.Message() << "filter " << filter << '\n';
	
	// null input
	{
	    udt* p = 0;
	    ar.getFilter(p);
	    Test.Check(p != 0, "ARModel ar(theta); udt* p = 0; ar.getFilter(p); p != 0");
	    Test.Check(equality(udt::Cast<Sequence<type> >(*p), filter), "ARModel ar(theta); udt* p = 0; ar.getFilter(p); equality(udt::Cast<Sequence<type> >(*p), filter)");
	    delete p;
	}
	// valid input
	{
	    Sequence<type> filter_out;
	    udt* p = &filter_out;
	    ar.getFilter(p);
	    Test.Check(equality(filter_out, filter), "ARModel ar(theta); udt* p = &filter_out; ar.getFilter(p); equality(udt::Cast<Sequence<type> >(*p), filter)");	    
	}
	// invalid input
	try
	{
	    Scalar<int> filter_not(0);
	    udt* p = &filter_not;
	    ar.getFilter(p);
	    Test.Check(false, "ARModel ar(theta); Scalar<int> filter_not; udt* p = &filter_not; ar.getFilter(p); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "ARModel ar(theta); Scalar<int> filter_not; udt* p = &filter_not; ar.getFilter(p); (threw)");
	}
    }

    // void setOrder(const int&)
    {
	ARModel ar(theta);
	ar.setOrder(3);
	Test.Check(ar.getOrder() == 3, "ar.setOrder(3); ar.getOrder() == 3");
	try
	{
	    std::valarray<type> theta_out;
	    ar.getTheta(theta_out);
	    Test.Check(false, "ar.setOrder(3); ar.getTheta(theta_out); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "ar.setOrder(3); ar.getTheta(theta_out); (threw)");
	}
    }

    // void setOrder(const udt&)
    {
	ARModel ar(theta);
	Scalar<int> n(3);
	ar.setOrder(n);
	Test.Check(ar.getOrder() == 3, "Scalar<int> n(3); ar.setOrder(n); ar.getOrder() == 3");
	try
	{
	    std::valarray<type> theta_out;
	    ar.getTheta(theta_out);
	    Test.Check(false, "Scalar<int> n(3); ar.setOrder(n); ar.getTheta(theta_out); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "Scalar<int> n(3); ar.setOrder(n); ar.getTheta(theta_out); (threw)");
	}
	try
        {
	    Scalar<float> m(4.0);
	    ar.setTheta(m);
	    Test.Check(false, "Scalar<float> m(4.0); ar.setOrder(m); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "Scalar<float> m(4.0); ar.setOrder(m); (threw)");
	}
    }

    // void setTheta(const std::valarray<type>&)
    {
	ARModel ar;
	ar.setTheta(theta);
	Test.Check(ar.getOrder() == (int) theta.size(), "ar.setTheta(theta); ar.getOrder() == (int) theta.size()");
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ar.setTheta(theta); ar.getTheta(theta_out); equality(theta, theta_out)");
    }

    // void setTheta(const udt&)
    {
	ARModel ar;
	Sequence<type> theta_udt(theta);
	ar.setTheta(theta_udt);
	Test.Check(ar.getOrder() == (int) theta.size(), "ar.setTheta(theta_udt); ar.getOrder() == (int) theta.size()");
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Check(equality(theta, theta_out), "ar.setTheta(theta_udt); ar.getTheta(theta_out); equality(theta, theta_out)");
	try
	{
	    Scalar<int> n(0);
	    ar.setTheta(n);
	    Test.Check(false, "Scalar<int> n(0); ar.setTheta(n); (no throw)");
	}
	catch (std::invalid_argument& e)
	{
	    Test.Check(true, "Scalar<int> n(0); ar.setTheta(n); (threw)");
	}
    }

    // void apply(std::valarray<type>& y, const std::valarray<type>& x) const
    {
	ARModel ar(theta);
	std::valarray<type> y;
	ar.apply(y, x);
	Test.Message() << y << " ~ " << x << '\n';
    }

    // void apply(udt*& y, const udt& x) const
    {
	// null pointer
	ARModel ar(theta);
	Sequence<type> x_udt(x);
	udt* y = 0;
	ar.apply(y, x_udt);
	Test.Message() << dynamic_cast<std::valarray<type>&>(*y) << " ~ " << x << '\n';
	delete y;
    }
    {
	// valid pointer
	ARModel ar(theta);
	Sequence<type> x_udt(x);
	Sequence<type> y_udt;
	udt* y = &y_udt;
	ar.apply(y, x_udt);
	Test.Message() << y_udt << " ~ " << x << '\n';
    }
    try
    {
	// invalid type (x)
	ARModel ar(theta);
	Scalar<type> x_udt(0);
	Sequence<type> y_udt;
	udt* y = &y_udt;
	ar.apply(y, x_udt);
	Test.Check(false, "ARModel ar(theta); Scalar<type> x_udt(0); Sequence<type> y_udt; udt* y = &y_udt; ar.apply(y, x_udt); (no throw)");

    }
    catch (std::exception& e)
    {
	Test.Check(true, "ARModel ar(theta); Scalar<type> x_udt(0); Sequence<type> y_udt; udt* y = &y_udt; ar.apply(y, x_udt); (threw)");
    }
    try
    {
	// invalid type (y)
	ARModel ar(theta);
	Sequence<type> x_udt(x);
	Scalar<int> y_udt(0);
	udt* y = &y_udt;
	ar.apply(y, x_udt);
	Test.Check(false, "ARModel ar(theta); Sequence<type> x_udt(x); Scalar<int> y_udt; udt* y = &y_udt; ar.apply(y, x); (no throw)");
    }
    catch (std::invalid_argument& e)
    {
 	Test.Check(true, "ARModel ar(theta); Sequence<type> x_udt(x); Scalar<int> y_udt; udt* y = &y_udt; ar.apply(y, x); (threw)");
   }

    // void refine(const std::valarray<type>& x)
    {
        const std::valarray<type>&
	  const_x( static_cast< const std::valarray<type>& >(x) );
	ARModel ar(2);
	ar.refine(x);
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Message() << theta_out << '\n';

	ARModel br(2);
	br.refine(std::valarray<type>(const_x[std::slice(0, 5, 1)]));
	br.refine(std::valarray<type>(const_x[std::slice(5, 2, 1)]));
	std::valarray<type> theta_split;
	br.getTheta(theta_split);
	Test.Message() << theta_split << '\n';

	Test.Check(equality(theta_out, theta_split), "<split refinement>");	
    }

    // void refine(const udt& x)
    {
	ARModel ar(2);
	Sequence<type> x_udt(x);
	ar.refine(x_udt);
	std::valarray<type> theta_out;
	ar.getTheta(theta_out);
	Test.Message() << theta_out << '\n';
    }
    try
    {
	ARModel ar(2);
	Scalar<int> x_udt(0);
	ar.refine(x_udt);
	Test.Check(false, "ARModel ar(2); Scalar<int> x_udt(0); ar.refine(x_udt); (no throw)");
    }
    catch(std::invalid_argument& e)
    {
    	Test.Check(true, "ARModel ar(2); Scalar<int> x_udt(0); ar.refine(x_udt); (threw)");
    }

    // type merit (std::valarray<type>&)
    {
	ARModel ar(theta);
	Test.Message() << "merit " << ar.merit(x) << '\n';
    }
    try
    {
	ARModel ar;
	ar.merit(x);
	Test.Check(false, "ARModel ar; ar.merit(x); (no throw)");
    }
    catch(std::invalid_argument& e)
    {
	Test.Check(true, "ARModel ar; ar.merit(x); (threw)");
    }

    // void merit(const udt& x)
    {
	ARModel ar(theta);
	Scalar<type> m_udt(0);
	udt* m = &m_udt;
	Sequence<type> x_udt(x);
	ar.merit(m, x_udt);
	Test.Message() << "merit " << m_udt.GetValue() << '\n';
    }

    // correctness

    {

	// generate linear filter

	std::valarray<type> theta(4);
	gasdev(theta);

	// system output

	std::valarray<type> y(100);

	// generate system output by filtering noise

	std::valarray<type> state(0.0, (int) theta.size());
	for (unsigned int i = 0; i < y.size(); ++i)
	{
	    y[i] = state[0] + (type)gasdev(seed);
	    state = state.shift(1);
	    state += theta * y[i];
	}

	ARModel estimator(y, 4);
	std::valarray<type> theta_out;
	estimator.getTheta(theta_out);

	Test.Message() << "      true theta = " << theta << '\n';
	Test.Message() << " estimated theta = " << theta_out << '\n';
	Test.Message() << "           error = " << std::valarray<type>(theta_out - theta) << '\n';

    }

}


int main(int argc, char** argv)
try
{
    Test.Init(argc, argv);

    test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("complex<float>");
    test_generic<std::complex<double> >("complex<double>");

    Test.Exit();

}
catch (const std::exception& x)
{
    Test.Check(false, "unexpected std::exception");
    Test.Message() << x.what() << '\n';
    Test.Exit();
}
catch (...)
{
    Test.Check(false, "unexpected exception");
    Test.Exit();
}
