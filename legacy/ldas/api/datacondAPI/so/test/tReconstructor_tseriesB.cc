//tReconstructor_tseriesB.cc

#include "datacondAPI/config.h"

#include <iostream>

#include "general/unittest.h"

#include "WaveletPSU.hh"
#include "HaarPSU.hh"
#include "resolver.hh"
#include "result.hh"
#include "reconstructor.hh"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"

using namespace std;   
using namespace datacondAPI::psu;

//void EasyTest();
//void RTest();
void PartReconstruct();

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  //EasyTest();

  //RTest();

  PartReconstruct();

  Test.Check(true) << "End of test harness reached." << endl; 

  Test.Exit();

} //end main

/*
void EasyTest()
{
  Test.Message() << "Executing EasyTest." << endl;


  Test.Message() << "Creating TimeSeries." << endl;
  datacondAPI::Sequence<double> data(0.0, 4);
  data[0] = 1.0;

  Haar haarwavelet;
  Result<double> easyResult; 
  datacondAPI::udt* peasyResult = &easyResult;

  const double srate = 2048.0;
  const General::GPSTime StartTime(10000, 0);

  datacondAPI::TimeSeries<double> tdata(data, 1.0 / 100.0);
  tdata.SetName("tdata");
  tdata.SetStartTime(StartTime);
  tdata.SetSampleRate(srate);

  Resolver easyTest(haarwavelet, 1);

  Test.Message() << "Performing decomposition (EasyTest)" << endl;
  easyTest.apply(peasyResult, tdata);
  
  Reconstructor ercst;
  datacondAPI::TimeSeries<double> reconstruction;
  datacondAPI::udt* preconstruction = &reconstruction;

  Test.Message() << "Performing reconstruction (EasyTest)." << endl;

  string what = "Reconstructor::apply";
  try
    {
      cout << "going to apply" << endl;
      ercst.apply(preconstruction, easyResult);
      cout << "applied!" << endl;
    }
  catch(std::domain_error& e)
    {
      Test.Check(false) << e.what() << endl;
    }
  catch(...)
    {
      Test.Check(false) << "Caught unexpected exception.";
    }

  
  //display sequence from inside reconstructed time series
  cout << "size of timeseries:  " << reconstruction.size()<< endl;
  datacondAPI::Sequence<double> treconstruction(reconstruction);
  cout << "size of sequence inside of reconstructed time series:  " << treconstruction.size() << endl;
  Test.Message() << "Reconstructed signal:" << endl;
  for(unsigned int i = 0; i < reconstruction.size(); i++)
    Test.Message() << reconstruction[i] << endl;

  //check reconstructed signal
  Test.Message() << "Expected reconstructed signal:" << endl;
  for(unsigned int pqr = 0; pqr < tdata.size(); pqr++)
    Test.Message() << tdata[pqr] << endl;
  

  //difference between original and reconstructed signals
  datacondAPI::Sequence<double> t1 (tdata);
  datacondAPI::Sequence<double> t2 (reconstruction);
  
  datacondAPI::Sequence<double> difference(t1 - t2);
  Test.Message() << "Difference between original and reconstructed:" << endl;
  for (unsigned int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(t1 == t2);

  bool pass = true;
  for (unsigned int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Reconstructed signal." << endl;
}

void RTest()
{
  Test.Message() << "Executing RTest." << endl;
  Test.Message() << "DECOMPOSITION for full reconstruction:" << endl;
  
  Test.Message() << "Creating TimeSeries." << endl;
  datacondAPI::Sequence<double> data(0.0, 48);
  data[4] = 1.0;

  const double srate = 2048.0;
  const General::GPSTime StartTime(10000, 0);

  datacondAPI::TimeSeries<double> tdata(data, 1.0 / 100.0);
  tdata.SetName("tdata");
  tdata.SetStartTime(StartTime);
  tdata.SetSampleRate(srate);

  Haar haarWavelet;
  Result<double> testResult;
  datacondAPI::udt* ptestResult = &testResult;
  Resolver test(haarWavelet, 4);

  //  Test.Message()<< "Applying filter." << endl;
  try{
    test.apply(ptestResult, tdata);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //Reconstruct the signal
  Test.Message() << "RECONSTRUCTION:" << endl;

  Reconstructor rcst;
  datacondAPI::TimeSeries<double> reconstruction;
  datacondAPI::udt* preconstruction = &reconstruction;

  //Test.Message() << "Applying synthesis filters for reconstruction." << endl;
  string what = "Reconstructor::apply method";
  try
    {
      rcst.apply(preconstruction, testResult);
    }
  catch(std::domain_error& d)
    {
      Test.Check(false) << what << " ( " << d.what() << " )"<< endl;
    }

  //check reconstructed signal
  datacondAPI::Sequence<double> original(0.0, 48);
  original[4] = 1.0;
  
  datacondAPI::Sequence<double> treconstruction(reconstruction);

  //difference between original and reconstructed signals
  datacondAPI::Sequence<double> difference(original - treconstruction);
  Test.Message() << "Difference between original and reconstructed:" << endl;
  for (unsigned int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(original == treconstruction);

  bool pass = true;
  for (unsigned int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Reconstructed signal.\n";

}
*/

void PartReconstruct()
{
  Test.Message() << "DECOMPOSITION for partial reconstruction:" << endl;
  
  Test.Message() << "Creating TimeSeries." << endl;
  datacondAPI::Sequence<double> original(0.0, 24);
  original[3] = 1.0;

  const double srate = 2048.0;
  const General::GPSTime StartTime(10000, 0);

  datacondAPI::TimeSeries<double> toriginal(original, 1.0/100);
  toriginal.SetName("toriginal");
  toriginal.SetStartTime(StartTime);
  toriginal.SetSampleRate(srate);

  Haar haarWavelet2;
  Result<double> testResult2;
  datacondAPI::udt* ptestResult2 = &testResult2;
  Resolver test2(haarWavelet2, 3);

  //Test.Message()<< "Applying filter." << endl;
  try{
    test2.apply(ptestResult2, toriginal);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  Reconstructor rcst;
  datacondAPI::TimeSeries<double> reconstruction;
  datacondAPI::udt* preconstruction = &reconstruction;
  datacondAPI::Scalar<int> level(3);

  Test.Message() << "PARTIAL RECONSTRUCTION from level " << (int&)(level) << endl;

  string what = "Overloaded Reconstructor::apply method";
  try
    {
      rcst.apply(preconstruction, testResult2, level);
    }
  catch(std::invalid_argument& a)
    {
      Test.Check(false) << what << " (" << a.what() << " )" << endl;
    }
  catch(std::exception &e)
    {
      Test.Check(false) << what << "( " << e.what() << " )" << endl;
    }
  catch(...)
    {
      Test.Check(false) << what << "(Caught unexpected exception.)" << endl;
    }

  
  //display partially reconstructed signal
  //Test.Message() << "Partially reconstructed signal: " << endl;
  //for(unsigned int t = 0; t < reconstruction.size(); t++)
  //  Test.Message() << reconstruction[t] << endl;
  

  //expected signals from different levels of reconstruction:
  
  //
  //check partially reconstructed signal -> level = 0
  //datacondAPI::Sequence<double> expected(0.0, 24);
  //expected[3] = 0.5;
  //expected[4] = -0.5;
  //

  //
  //check partially reconstructed signal -> level = 1
  //datacondAPI::Sequence<double> expected(0.0, 24);
  //expected[1] = expected[2] = -0.25;
  //expected[3] = expected[4] = 0.25;
  //

  //
  //check partially reconstructed signal -> level = 2
  //datacondAPI::Sequence<double> expected(0.0, 24);
  //expected[1] = expected[2] = expected[3] = expected[4] = 0.125;
  //expected[5] = expected[6] = expected[7] = expected[8] = -0.125; 
  // 


  //check partially reconstructed signal -> level = 3
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = expected[3] = expected[4] = 0.125;
  expected[5] = expected[6] = expected[7] = expected[8] = 0.125; 
 
  datacondAPI::Sequence<double> treconstruction(reconstruction);

  //difference between expected and reconstructed signals
  datacondAPI::Sequence<double> difference(expected - treconstruction);
  Test.Message() << "Difference = (expected - reconstructed):" << endl;
  for (unsigned int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
   std::valarray<bool> check(expected == reconstruction);

  bool pass = true;
  for (unsigned int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  //Test.Check(pass) << "Partially reconstructed signal.\n";

}
