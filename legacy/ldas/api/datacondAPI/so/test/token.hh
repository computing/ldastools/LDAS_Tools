#ifndef TOKEN_HH
#define	TOKEN_HH

#include <stdarg.h>
#include <stdexcept>
#include <valarray>

#include "genericAPI/swigexception.hh"

#include "CallChain.hh"

#define CATCH(LM_T) \
  catch ( ... )								\
  {									\
    try									\
    {									\
      throw;								\
    }									\
    catch ( const CallChain::Exception& e )				\
    {									\
      LM_T.Check(false) << "CallChain Exception : "			\
                          << e.GetMessage() << std::endl;		\
    }									\
    catch ( const SwigException& e )					\
    {									\
      LM_T.Check(false) << "SwigException: " << e.getResult()		\
                          << " Info: " << e.getInfo() << std::endl;	\
    }									\
    catch ( const std::invalid_argument& e )				\
    {									\
      LM_T.Check(false) << "invalid argument: " << e.what() << std::endl;\
    }									\
    catch ( const std::logic_error& e )					\
    {									\
      LM_T.Check(false) << "logic error: " << e.what() << std::endl;	\
    }									\
    catch ( const std::runtime_error& e )				\
    {									\
      LM_T.Check(false) << "runtime error: " << e.what() << std::endl;\
    }									\
    catch ( const std::exception& e )					\
    {									\
      LM_T.Check(false) << "std exception: " <<  e.what() << std::endl;\
    }									\
    catch(...)								\
    {									\
      LM_T.Check(false) << "Caught an Unknown exception" << std::endl;	\
    }									\
  }

class Parameters
{
public:
  inline Parameters(void);
  inline const char** set(int Count, ...);

private:
  static const int	m_max = 10;
  const char*		m_buffer[ m_max ];

  int buf_size( ) const;
};

Parameters::
Parameters(void)
{
}

   
inline int Parameters::
buf_size( ) const
{
  return ( sizeof( m_buffer ) / sizeof( *m_buffer) );
}
   
   
const char** Parameters::
set(int Count, ...)
{
  int		c = 0;
  va_list	ap;
  va_start(ap, Count);
  for (c = 0; (c < Count) && (c < buf_size( ) ); c++)
  {
    m_buffer[c] = va_arg(ap, char*);
  }
  va_end(ap);
  m_buffer[c] = (char*)NULL;
  return m_buffer;
}


template<class VClass>
std::ostream& operator<<(std::ostream& vout, const std::valarray<VClass>& ValArray);

std::ostream& operator<<(std::ostream& vout, const std::complex<int>& x);

template<class VClass>
void check_valarray(const std::valarray<VClass>& Input,
		    CallChain::Symbol* Result,
		    std::string Text);

template<class VClass>
void check_valarray_variance(const std::valarray<VClass>& Input,
			     CallChain::Symbol* Result,
			     std::string Text);

void check_udt_valarray(const datacondAPI::udt& Input,
			CallChain::Symbol* Result,
			std::string Text);

template<class DataType_>
void check_value(DataType_ Value, const CallChain::Symbol& Symbol,
		 const std::string& Message);

template<class DataType_>
void check_value_variance(DataType_ Value, const CallChain::Symbol& Symbol,
			  const std::string& Message);

//: Read a ilwd from a file
const ILwd::LdasElement* read_ilwd(const char* Filename,
				   const char* Env = "SRCDIR");

#endif	/* TOKEN_HH */
