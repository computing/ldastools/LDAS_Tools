#include "datacondAPI/config.h"

#include <unistd.h>

#include <valarray>
#include <stdexcept>
#include <complex>

#include "general/unittest.h"

#include "fft.hh"

#include "ifft.hh"

#include "DFTUDT.hh"

using std::complex;
using namespace datacondAPI;

// This may be too small
const double TOLERANCE = 1E-09;

General::UnitTest Test;

//
// Test FFT for a particular length n
//
// tdata is the time-domain data to be FFT'ed
// fdata is the expected frequency domain data
//
int
TestFFT(const complex<double>* tdata,
	const complex<double>* fdata, const size_t n)
{
    int result = 0;
Test.Message() << "Starting...\n";
	// OLD (FVALARRAY)

	/*
    const complex<double>* p = fdata;
	fvalarray<double> fft_result(n, false);
    for (int i = fft_result.start(); i < fft_result.finish(); i++)
    {
	fft_result.set(i, *p);
	++p;
    }
	valarray< complex<double> > fft_in(tdata, n);
	*/

	// NEW (UDT)

	const unsigned int nyquist = (n + 1) / 2;
	const complex<double>* p = fdata;
	Sequence<std::complex<double> > fft_result(n);
	for(unsigned int i = nyquist; i < n; ++i)
	{
		fft_result[i] = *p;
		++p;
	}
	for(unsigned int i = 0; i < nyquist; ++i)
	{
		fft_result[i] = *p;
		++p;
	}
Test.Message() << "Copied...\n";
    Sequence<complex<double> > fft_in(tdata, n);

	// CONTINUES
    
    // The FFT object is static so that we can test that successive FFT's
    // of different lengths work
    static FFT fft;
    
	// OLD (FVALARRAY)
    
	/*
	fvalarray< double > fft_out;
    fft_out.make_asymmetric();
    fft_out.resize(n);
	*/

	// NEW (UDT)

	udt* fft_out = 0;

	// CONTINUES

    fft.apply(fft_out, fft_in);
Test.Message() << "Applied\n";  
	// OLD (FVALARRAY)
    
	/*
    Test.Check(fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
	Test.Check(fft_out.is_symmetric() == false) << "FFT output has correct symmetry" << std::endl;

    for (int i = fft_out.start(); i < fft_out.finish(); i++)
    {
	const double diff = abs(fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
    }

    // zero size and correct symmetry
    fft_out.make_asymmetric();
    fft_out.resize(0);
	*/

	// NEW (UDT)
Test.Message() << "Dereferencing " << fft_out << std::endl;
	DFT<std::complex<double> >& r_fft_out = udt::Cast<DFT<std::complex<double> > >(*fft_out);
Test.Message() << "Referenced\n";
    Test.Check(r_fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
	Test.Check(r_fft_out.IsSymmetric() == false) << "FFT output has correct symmetry" << std::endl;
	for(unsigned int i = 0; i < n; ++i)
	{
	const double diff = abs(r_fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << r_fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
	}

	r_fft_out.resize(0);
Test.Message() << "Continuing...\n";
	// CONTINUES
    
    fft.apply(fft_out, fft_in);

	// OLD (FVALARRAY)

	/*
    Test.Check(fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
    Test.Check(fft_out.is_symmetric() == false) << "FFT output has correct symmetry" << std::endl;

    for (int i = fft_out.start(); i < fft_out.finish(); i++)
    {
	const double diff = abs(fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
    }
	*/

	// NEW (UDT)

    Test.Check(r_fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
	Test.Check(r_fft_out.IsSymmetric() == false) << "FFT output has correct symmetry" << std::endl;
	for(unsigned int i = 0; i < n; ++i)
	{
	const double diff = abs(r_fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << r_fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
	}

	// OLD (FVALARRAY)

	/*
    // zero size incorrect symmetry
    fft_out.make_symmetric();
    fft_out.resize(0);

    fft.apply(fft_out, fft_in);
    
    Test.Check(fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
    Test.Check(fft_out.is_symmetric() == false) << "FFT output has correct symmetry" << std::endl;

    for (int i = fft_out.start(); i < fft_out.finish(); i++)
    {
	const double diff = abs(fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
    }

    // wrong size incorrect symmetry
    fft_out.make_symmetric();
    fft_out.resize(n + 3);
	*/

	// NEW (UDT)

	r_fft_out.resize(n + 3);

	// CONTINUES
    
    fft.apply(fft_out, fft_in);

	// OLD (FVALARRAY)
	
	/*    
    Test.Check(fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
    Test.Check(fft_out.is_symmetric() == false) << "FFT output has correct symmetry" << std::endl;

    for (int i = fft_out.start(); i < fft_out.finish(); i++)
    {
	const double diff = abs(fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
    }
	*/

	// NEW (UDT)

    Test.Check(r_fft_out.size() == (size_t)n) << "FFT output has correct size" << std::endl;
	Test.Check(r_fft_out.IsSymmetric() == false) << "FFT output has correct symmetry" << std::endl;
	for(unsigned int i = 0; i < n; ++i)
	{
	const double diff = abs(r_fft_out[i] - fft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << r_fft_out[i] << "    "
	    << fft_result[i] << "    "
	    << diff << std::endl;
	}

	delete fft_out;

    return result;
}

//
// Test IFFT for a particular length n
//
// tdata is the expected time-domain data
// fdata is the frequency domain data to be IFFT'ed
//
int
TestIFFT(const complex<double>* tdata,
	 const complex<double>* fdata, const size_t n)
{
    int result = 0;

	// OLD (FVALARRAY)

	/*
    valarray< complex<double> > ifft_result(tdata, n);

    // Set input frequency-domain data
    fvalarray<double> ifft_in(complex<double>(0.0, 0.0), n, false);
    const complex<double>* p = fdata;
    for (int i = ifft_in.start(); i < ifft_in.finish(); i++)
    {
	ifft_in.set(i, *p);
	++p;
    }

    // Create empty output array
    valarray< complex<double> > ifft_out;
	*/

	// NEW (UDT)

	Sequence<std::complex<double> > ifft_result(tdata, n);

	const unsigned int nyquist = (n + 1) / 2;
	const complex<double>* p = fdata;
	DFT<std::complex<double> > ifft_in(n);
	for(unsigned int i = nyquist; i < n; ++i)
	{
		ifft_in[i] = *p;
		++p;
	}
	for(unsigned int i = 0; i < nyquist; ++i)
	{
		ifft_in[i] = *p;
		++p;
	}

	udt* ifft_out = 0;

	// CONTINUES
    
    // The FFT object is static so that we can test that successive FFT's
    // of different lengths work
    static IFFT ifft;
     
    // zero size

	// OLD(FVLARRAY)
	/*
    ifft_out.resize(0);
	*/

	// CONTINUES

    ifft.apply(ifft_out, ifft_in);

	// OLD (FVALARRAY)
    /*
    Test.Check(ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;

    for (size_t i = 0; i < ifft_out.size(); i++)
    {
	const double diff = abs(ifft_out[i] - ifft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << ifft_out[i] << "    "
	    << ifft_result[i] << "    "
	    << diff << std::endl;
    }

    // correct size
    ifft_out.resize(n);
	*/
	// NEW (UDT)
	Sequence<std::complex<double> >& r_ifft_out = udt::Cast<Sequence<std::complex<double> > >(*ifft_out);
    Test.Check(r_ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;
	for(unsigned int i = 0; i < n; ++i)
	{
	const double diff = abs(r_ifft_out[i] - ifft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << r_ifft_out[i] << "    "
	    << ifft_result[i] << "    "
	    << diff << std::endl;
	}
    r_ifft_out.resize(n);
	// CONTINUES

    ifft.apply(ifft_out, ifft_in);
    
	// OLD (FVALARRAY)
	/*
    Test.Check(ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;

    for (size_t i = 0; i < ifft_out.size(); i++)
    {
	const double diff = abs(ifft_out[i] - ifft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << ifft_out[i] << "    "
	    << ifft_result[i] << "    "
	    << diff << std::endl;
    }

    // incorrect size - should be resized
    ifft_out.resize(n + 3);
	*/
	// NEW (UDT)
    Test.Check(r_ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;
	for(unsigned int i = 0; i < n; ++i)
	{
	const double diff = abs(r_ifft_out[i] - ifft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << r_ifft_out[i] << "    "
	    << ifft_result[i] << "    "
	    << diff << std::endl;
	}
    r_ifft_out.resize(n + 3);
	// CONTINUES

    ifft.apply(ifft_out, ifft_in);

	// OLD (FVALARRAY)
	/*
    Test.Check(ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;

    for (size_t i = 0; i < ifft_out.size(); i++)
    {
	const double diff = abs(ifft_out[i] - ifft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << ifft_out[i] << "    "
	    << ifft_result[i] << "    "
	    << diff << std::endl;
    }
	*/
	// NEW (UDT)
    Test.Check(r_ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;
	for(unsigned int i = 0; i < n; ++i)
	{
	const double diff = abs(r_ifft_out[i] - ifft_result[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << r_ifft_out[i] << "    "
	    << ifft_result[i] << "    "
	    << diff << std::endl;
	}
	// CONTINUES

	delete ifft_out;

    return result;
}

//
// Test FFT/IFFT reversibility for a particular length n
//
// tdata is the expected time-domain data
// fdata is the frequency domain data to be IFFT'ed
//
int
TestFFTandIFFT(const complex<double>* tdata, const size_t n)
{
    int result = 0;

	// OLD (FVALARRAY)
	/*
    valarray< complex<double> > fft_in(tdata, n);
    fvalarray<double> fft_out(complex<double>(0.0, 0.0), n);
    valarray< complex<double> > ifft_out(complex<double>(0.0, 0.0), n);
	*/
	// NEW (UDT)
    Sequence<complex<double> > fft_in(tdata, n);
    DFT<complex<double> > fft_out(n);
    Sequence<complex<double> > ifft_out(n);
	udt* p_fft_out = &fft_out;
	udt* p_ifft_out = &ifft_out;
	// CONTINUES

    // The FFT object is static so that we can test that successive FFT's
    // of different lengths work
    static FFT fft;
    static IFFT ifft;

	// OLD (FVAARRAY)
	/*
    fft.apply(fft_out, fft_in);
    ifft.apply(ifft_out, fft_out);
	*/
	// NEW (UDT)
	fft.apply(p_fft_out, fft_in);
	ifft.apply(p_ifft_out, fft_out);
	// CONTINUES

    Test.Check(ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;
    Test.Check(fft_out.size() == fft_in.size()) << "IFFT output has same size as FFT input" << std::endl;
	// OLD(FVALARRAY)
	/*
    Test.Check(fft_out.is_symmetric() == false) << "FFT output has correct symmetry" << std::endl;
	*/
	// NEW (UDT)
	Test.Check(fft_out.IsSymmetric() == false) << "FFT output has correct symmetry" << std::endl;

    for (size_t i = 0; i < ifft_out.size(); i++)
    {
	const double diff = abs(ifft_out[i] - fft_in[i]);
	Test.Check(diff < TOLERANCE)
	    << "Error is within tolerance "
	    << i << "    "
	    << ifft_out[i] << "    "
	    << fft_in[i] << "    "
	    << diff << std::endl;
    }

    return result;
}

int
TestCaseA001()
{
    int result = 0;

    std::string what("A001: FFT::FFT() constructor");

    FFT fft;

    // Prevent compiler warning
    Test.Check( true ) << "Construction: " << &fft << std::endl;

    return result;
}

int
TestCaseA002()
{
    int result = 0;

    std::string what("A002: FFT::apply() - FFT of complex 1-d data, n = 1");

    const size_t n = 1;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000)
    };

    complex<double> fdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000)
    };

    result |= TestFFT(tdata, fdata, n);

    return result;
}

int
TestCaseA003()
{
    int result = 0;

    std::string what("A003: FFT::apply() - FFT of complex 1-d data, n = 2");

    const size_t n = 2;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(-1.0562305898749, 0.4114496766047)
    };

    complex<double> fdata[n] = {
        complex<double>(1.0562305898749, 0.2885503233953),
        complex<double>(-1.0562305898749, 1.1114496766047)
    };

    result |= TestFFT(tdata, fdata, n);
    
    return result;
}


int
TestCaseA004()
{
    int result = 0;

    std::string what("A004: FFT::apply() - FFT of complex 1-d data, n = 3");

    const size_t n = 3;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(-0.3541085699603, 0.0731699242874),
        complex<double>(-0.2729166236434, -0.6847033205137)
    };

    complex<double> fdata[n] = {
        complex<double>(-0.3428248860444, 0.9354524100200),
        complex<double>(-0.6270251936037, 0.0884666037737),
        complex<double>(0.9698500796481, 1.0760809862063)
    };

    result |= TestFFT(tdata, fdata, n);
    
    return result;
}

int
TestCaseA005()
{
    int result = 0;

    std::string what("A005: FFT::apply() - FFT of complex 1-d data, n = 8");

    const size_t n = 8;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(0.2443153918741, 0.1634117546991),
        complex<double>(1.6811571966645, -0.6237045669319),
        complex<double>(-0.3395397560449, -0.4546136338311),
        complex<double>(-1.0562305898749, 0.4114496766047),
        complex<double>(-0.1847759065023, 0.6467156727579),
        complex<double>(-0.8692396843909, -0.1095041255282),
        complex<double>(0.9471622566848, -0.6978421336132)
    };

    complex<double> fdata[n] = {
        complex<double>(-0.9114750636130, 0.7205693241320),
        complex<double>(-0.5014599806667, -2.3545329099905),
        complex<double>(-3.8307312970498, 1.2965753537967),
        complex<double>(2.9534390729358, 1.7187888533061),
        complex<double>(0.4228489084103, 0.0359126441574),
        complex<double>(1.5855202776091, -2.1691602053298),
        complex<double>(0.0944350927529, 2.3927413843328),
        complex<double>(0.1874229896214, 3.9591055555953)
    };

    result |= TestFFT(tdata, fdata, n);
    
    return result;
}


int
TestCaseA006()
{
    int result = 0;

    std::string what("A006: FFT::apply() - FFT of real 1-d data, odd n = 13");

    const size_t n = 13;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(1.0887334103231, -0.0169146216653),
        complex<double>(0.1699036532317, -0.6991825587826),
        complex<double>(1.6681980661317, 0.0507043601132),
        complex<double>(0.2506643660745, 0.6967321443024),
        complex<double>(-0.1914525314300, -0.0843756761787),
        complex<double>(0.0478768914785, -0.6926544796157),
        complex<double>(-1.7508229275593, 0.1178499289429),
        complex<double>(-0.3396039398585, 0.6869590882976),
        complex<double>(-0.7414866261754, -0.1510489377375),
        complex<double>(-0.3717785376350, -0.6796592721982),
        complex<double>(1.3212190085457, 0.1838951647390),
        complex<double>(0.1791560736203, 0.6707720803764)
    };

    complex<double> fdata[n] = {
        complex<double>(-1.2926857773641, -0.2514348494313),
        complex<double>(0.1579309886901, 4.7992225148101),
        complex<double>(-2.2251174759340, -0.4138299112784),
        complex<double>(1.9268668665462, 2.3315063363747),
        complex<double>(-1.6351024563620, -0.4605643042366),
        complex<double>(4.5850627584341, 3.7806604406799),
        complex<double>(1.3306069067473, 0.7830772205934),
        complex<double>(4.1169205911819, -2.1326745022011),
        complex<double>(-2.9084452907819, 2.5006741665496),
        complex<double>(-4.3594377353468, 3.1842749293733),
        complex<double>(0.3676786314170, -0.6839693024021),
        complex<double>(0.9967929952043, -4.8017315321106),
        complex<double>(-1.0610710024322, 0.4647887932791)
    };
    
    result |= TestFFT(tdata, fdata, n);

    return result;
}

int
TestCaseB001()
{
    int result = 0;

    std::string what("B001: IFFT::IFFT() constructor");

    IFFT ifft;
    
    Test.Check( true ) << "Construction: " << &ifft << std::endl;

    return result;
}

int
TestCaseB002()
{
    int result = 0;

    std::string what("B002: IFFT::apply() - IFFT of complex 1-d data, n = 1");

    const size_t n = 1;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000)
    };

    complex<double> fdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000)
    };

    result |= TestIFFT(tdata, fdata, n);

    return result;
}

int
TestCaseB003()
{
    int result = 0;

    std::string what("B003: IFFT::apply() - IFFT of complex 1-d data, n = 2");

    const size_t n = 2;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(-1.0562305898749, 0.4114496766047)
    };

    complex<double> fdata[n] = {
        complex<double>(1.0562305898749, 0.2885503233953),
        complex<double>(-1.0562305898749, 1.1114496766047)
    };

    result |= TestIFFT(tdata, fdata, n);

    return result;
}

int
TestCaseB004()
{
    int result = 0;

    std::string what("B004: IFFT::apply() - IFFT of complex 1-d data, n = 3");

    const size_t n = 3;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(-0.3541085699603, 0.0731699242874),
        complex<double>(-0.2729166236434, -0.6847033205137)
    };

    complex<double> fdata[n] = {
        complex<double>(-0.3428248860444, 0.9354524100200),
        complex<double>(-0.6270251936037, 0.0884666037737),
        complex<double>(0.9698500796481, 1.0760809862063)
    };

    result |= TestIFFT(tdata, fdata, n);

    return result;
}

int
TestCaseB005()
{
    int result = 0;

    std::string what("B005: IFFT::apply() - IFFT of complex 1-d data, n = 8");

    const size_t n = 8;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(0.2443153918741, 0.1634117546991),
        complex<double>(1.6811571966645, -0.6237045669319),
        complex<double>(-0.3395397560449, -0.4546136338311),
        complex<double>(-1.0562305898749, 0.4114496766047),
        complex<double>(-0.1847759065023, 0.6467156727579),
        complex<double>(-0.8692396843909, -0.1095041255282),
        complex<double>(0.9471622566848, -0.6978421336132)
    };

    complex<double> fdata[n] = {
        complex<double>(-0.9114750636130, 0.7205693241320),
        complex<double>(-0.5014599806667, -2.3545329099905),
        complex<double>(-3.8307312970498, 1.2965753537967),
        complex<double>(2.9534390729358, 1.7187888533061),
        complex<double>(0.4228489084103, 0.0359126441574),
        complex<double>(1.5855202776091, -2.1691602053298),
        complex<double>(0.0944350927529, 2.3927413843328),
        complex<double>(0.1874229896214, 3.9591055555953)
    };

    result |= TestIFFT(tdata, fdata, n);

    return result;
}

int
TestCaseB006()
{
    int result = 0;

    std::string what("B006: IFFT::apply() - IFFT of complex 1-d data, n = 13");

    const size_t n = 13;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(1.0887334103231, -0.0169146216653),
        complex<double>(0.1699036532317, -0.6991825587826),
        complex<double>(1.6681980661317, 0.0507043601132),
        complex<double>(0.2506643660745, 0.6967321443024),
        complex<double>(-0.1914525314300, -0.0843756761787),
        complex<double>(0.0478768914785, -0.6926544796157),
        complex<double>(-1.7508229275593, 0.1178499289429),
        complex<double>(-0.3396039398585, 0.6869590882976),
        complex<double>(-0.7414866261754, -0.1510489377375),
        complex<double>(-0.3717785376350, -0.6796592721982),
        complex<double>(1.3212190085457, 0.1838951647390),
        complex<double>(0.1791560736203, 0.6707720803764)
    };

    complex<double> fdata[n] = {
        complex<double>(-1.2926857773641, -0.2514348494313),
        complex<double>(0.1579309886901, 4.7992225148101),
        complex<double>(-2.2251174759340, -0.4138299112784),
        complex<double>(1.9268668665462, 2.3315063363747),
        complex<double>(-1.6351024563620, -0.4605643042366),
        complex<double>(4.5850627584341, 3.7806604406799),
        complex<double>(1.3306069067473, 0.7830772205934),
        complex<double>(4.1169205911819, -2.1326745022011),
        complex<double>(-2.9084452907819, 2.5006741665496),
        complex<double>(-4.3594377353468, 3.1842749293733),
        complex<double>(0.3676786314170, -0.6839693024021),
        complex<double>(0.9967929952043, -4.8017315321106),
        complex<double>(-1.0610710024322, 0.4647887932791)
    };
    
    result |= TestIFFT(tdata, fdata, n);

    return result;
}

int
TestCaseC002()
{
    int result = 0;

    std::string what("C002: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 1");

    const size_t n = 1;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000)
    };

    result |= TestFFTandIFFT(tdata, n);

    return result;
}
int
TestCaseC003()
{
    int result = 0;

    std::string what("C003: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 2");

    const size_t n = 2;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(-1.0562305898749, 0.4114496766047)
    };

    result |= TestFFTandIFFT(tdata, n);

    return result;
}
int
TestCaseC004()
{
    int result = 0;

    std::string what("C004: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 3");

    const size_t n = 3;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(-0.3541085699603, 0.0731699242874),
        complex<double>(-0.2729166236434, -0.6847033205137)
    };

    result |= TestFFTandIFFT(tdata, n);

    return result;
}

int
TestCaseC005()
{
    int result = 0;

    std::string what("C005: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 8");

    const size_t n = 8;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 1.0000000000000),
        complex<double>(0.9249743361524, 0.0000000000000),
        complex<double>(1.9989505464822, 0.0000000000000),
        complex<double>(-0.8718239319649, 0.0000000000000),
        complex<double>(-1.6225424859374, 0.0000000000000),
        complex<double>(0.0831024961533, 0.0000000000000),
        complex<double>(-0.1778578459743, 0.0000000000000),
        complex<double>(1.0020836236942, 0.0000000000000)
    };

    result |= TestFFTandIFFT(tdata, n);

    return result;
}

int
TestCaseC006()
{
    int result = 0;

    std::string what("C006: IFFT::apply() - IFFT of FFT of complex 1-d data, n = 13");

    const size_t n = 13;
    const complex<double> tdata[n] = {
        complex<double>(0.0000000000000, 0.7000000000000),
        complex<double>(1.0887334103231, -0.0169146216653),
        complex<double>(0.1699036532317, -0.6991825587826),
        complex<double>(1.6681980661317, 0.0507043601132),
        complex<double>(0.2506643660745, 0.6967321443024),
        complex<double>(-0.1914525314300, -0.0843756761787),
        complex<double>(0.0478768914785, -0.6926544796157),
        complex<double>(-1.7508229275593, 0.1178499289429),
        complex<double>(-0.3396039398585, 0.6869590882976),
        complex<double>(-0.7414866261754, -0.1510489377375),
        complex<double>(-0.3717785376350, -0.6796592721982),
        complex<double>(1.3212190085457, 0.1838951647390),
        complex<double>(0.1791560736203, 0.6707720803764)
    };

    result |= TestFFTandIFFT(tdata, n);

    return result;
}

int
TestCaseE001()
{
    int result = 0;

    std::string what("E001: FFT apply exceptions - size 0 input array");

    FFT fft;
    //valarray< complex<double> > fft_in;
    //fvalarray<double> fft_out;

    Sequence<complex<double> > fft_in;
    udt* fft_out = 0;

    try
    {
	fft.apply(fft_out, fft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception "
			 << e.what() << std::endl;
	return result;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;

    Test.Check(fft_out == 0) << "Output UDT is null" << std::endl;

    return result;
}

int
TestCaseE002()
{
    int result = 0;

    std::string what("E002: FFT apply exceptions - size 0 input array");

    //const size_t n = 8;
    FFT fft;
    //valarray< complex<double> > fft_in;
    //fvalarray<double> fft_out(n);

    Sequence<complex<double> > fft_in;
    udt* fft_out = 0;

    try
    {
	fft.apply(fft_out, fft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception "
			 << e.what() << std::endl;
	return result;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;

    Test.Check(fft_out == 0) << "Output UDT is null" << std::endl;

    return result;
}

int
TestCaseE003()
{
    int result = 0;

    std::string what("E003: IFFT apply exceptions - size 0 input array");

    IFFT ifft;
    //fvalarray<double> ifft_in(0, false);
    //valarray< complex<double> > ifft_out;

    DFT<complex<double> > ifft_in;
    udt* ifft_out;

    try
    {
	ifft.apply(ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception "
			 << e.what() << std::endl;
	return result;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;

    Test.Check(ifft_out == 0) << "Output UDT is null" << std::endl;

    return result;
}

int
TestCaseE004()
{
    int result = 0;

    std::string what("E004: IFFT apply exceptions - size 0 input array");

    const size_t n = 9;
    IFFT ifft;
    //fvalarray<double> ifft_in(0, false);
    //valarray< complex<double> > ifft_out(n);

    DFT<complex<double> > ifft_in;
    Sequence<complex<double> > ifft_out(n);
    udt* p_ifft_out = 0;

    try
    {
	ifft.apply(p_ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception "
			 << e.what() << std::endl;
	return result;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;

    Test.Check(p_ifft_out == 0) << "Output UDT is null" << std::endl;

    return result;
}

#if 0 // Not relevant to UDT

int
TestCaseE005()
{
    int result = 0;

    std::string what("E005: IFFT apply exceptions - symmetric input array");

    const size_t n = 8;
    IFFT ifft;
    fvalarray<double> ifft_in(n, true);
    valarray< complex<double> > ifft_out(n);

    try
    {
	ifft.apply(ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception"
			 << e.what() << std::endl;
	return result;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;

    return result;
}

#endif // 0 Not relevant to UDT

int
TestCaseF001()
{
    int result = 0;

#if 0
    std::string what("F001: purging of used plans");

    //PlanDB planDB;

    //planDB.purge_plans(planDB.size());

    //Test.Check(planDB.size() == 0) << "Plan database is empty" << std::endl;

    planDB.set_max_size(4);

    Test.Check(planDB.max_size() == 4) << "Plan database max size is 4"
				       << std::endl;

    FFT fft;
    
    Sequence<complex<double> > fft_in(8);
    udt* fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    fft_in.resize(8);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 1) << "Plan database has 1 element" << std::endl;

    sleep(1);

    fft_in.resize(7);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 2) << "Plan database has 2 elements" << std::endl;

    sleep(1);

    fft_in.resize(6);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 3) << "Plan database has 3 elements" << std::endl;

    sleep(1);

    // Re-use the 8-plan to update it's use time
    fft_in.resize(8);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 3) << "Plan database has 3 elements" << std::endl;

    sleep(1);

    fft_in.resize(5);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 4) << "Plan database has 4 elements" << std::endl;

    sleep(1);

    fft_in.resize(4);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() <= planDB.max_size())
	<< "Plan db size is smaller than max_size" << std::endl;

    sleep(1);

    fft_in.resize(3);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() <= planDB.max_size())
	<< "Plan db size is smaller than max_size" << std::endl;

    // Oldest plans (7 and 6) should have aged out by now
    size_t count = planDB.count(valarray<size_t>(7,1),
				planType<DComplexFwdPlanH>(),
				FFTW_ESTIMATE);

    Test.Check(count == 0) << "Plan 7 is gone" << std::endl;

    count = planDB.count(valarray<size_t>(6,1),
			 planType<DComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 0) << "Plan 6 is gone" << std::endl;

    count = planDB.count(valarray<size_t>(8,1),
			 planType<DComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 8 is left" << std::endl;

    count = planDB.count(valarray<size_t>(5,1), planType<DComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 5 is left" << std::endl;

    count = planDB.count(valarray<size_t>(4,1), planType<DComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 4 is left" << std::endl;

    count = planDB.count(valarray<size_t>(3,1), planType<DComplexFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 3 is left" << std::endl;

    planDB.purge_plans(planDB.size());

    Test.Check(planDB.size() == 0) << "All plans removed" << std::endl;

    delete fft_out;
#endif
    return result;
}

int
main(int argc, char** argv)
{
    int result = 0;

    Test.Init(argc, argv);

    Test.Message() << "Parsed command line\n";

    try
    {
	result |= TestCaseA001();
Test.Message() << "Passed A0001\n";
        result |= TestCaseA002();
Test.Message() << "A0002\n";	
        result |= TestCaseA003();
Test.Message() << "A0003\n";
	result |= TestCaseA004();
Test.Message() << "A0004\n";
	result |= TestCaseA005();
Test.Message() << "A0005\n";
	result |= TestCaseA006();
Test.Message() << "A0006\n";
	result |= TestCaseB001();
	result |= TestCaseB002();
	result |= TestCaseB003();
	result |= TestCaseB004();
	result |= TestCaseB005();
	result |= TestCaseB006();
Test.Message() << "B\n";
	result |= TestCaseC002();
	result |= TestCaseC003();
	result |= TestCaseC004();
	result |= TestCaseC005();
	result |= TestCaseC006();
Test.Message() << "C\n";
	result |= TestCaseE001();
	result |= TestCaseE002();
	result |= TestCaseE003();
	result |= TestCaseE004();
	//result |= TestCaseE005();
Test.Message() << "E\n";
	result |= TestCaseF001();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Unhandled exception: " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception" << std::endl;
    }

    //PlanDB().purge_plans(PlanDB().size());

    Test.Exit();

    return result;
}

