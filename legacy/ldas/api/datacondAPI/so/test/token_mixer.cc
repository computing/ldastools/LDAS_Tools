#include "datacondAPI/config.h"

#include "general/unittest.h"	// Needed for doing "make check"

#include "token.hh"		// Common header for token testing programs.
#include "Mixer.hh"		// Needed for computing mix expected results.
#include "ScalarUDT.hh"		// Needed for converting CallChain results.
#include "SequenceUDT.hh"	// Needed for converting CallChain results.

General::UnitTest	Test;	// Class supporting testing

//-----------------------------------------------------------------------
// This template functions is a pattern for testing the functionality
//   of the mixer.
//-----------------------------------------------------------------------

template<class Tout, class Tin>
void mixer_test(const std::string& type_out,
                const std::string& type_in,
                const std::string& array_name)
{

  //---------------------------------------------------------------------
  // Display start message
  //---------------------------------------------------------------------

  Test.Message() << "Mixer<" << type_out << ", " << type_in << ">: start\n";

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  try
  {
    CallChain	cmds;		// Instance of CallChain
    Parameters	args;		// Structure to hold parameters
    
    cmds.Reset();		// Make sure CallChain is ready for action

    Test.Message() << "...appending classes...\n";

    //-------------------------------------------------------------------
    // Establish variables
    //-------------------------------------------------------------------

    cmds.AppendCallFunction("integer", args.set(1, "16"), "N");
    cmds.AppendCallFunction("double", args.set(1, "0.1234"), "phase");
    cmds.AppendCallFunction("double", args.set(1, "0.5678"), "carrier");

    //-------------------------------------------------------------------
    // Set up the Sequence according to what was passed.
    //-------------------------------------------------------------------

    if (array_name == "svalarray")
    {
      cmds.AppendCallFunction("double", args.set(1, "1.0"), "sbase");
      cmds.AppendCallFunction(array_name, args.set(2, "sbase", "N"), "x");
    }
    else if (array_name == "dvalarray")
    {
      cmds.AppendCallFunction("double", args.set(1, "1.0"), "dbase");
      cmds.AppendCallFunction(array_name, args.set(2, "dbase", "N"), "x");
    }
    else if (array_name == "cvalarray")
    {
      cmds.AppendCallFunction("scomplex", args.set(2, "1.0", "0.0"), "cbase");
      cmds.AppendCallFunction(array_name, args.set(2, "cbase", "N"), "x");
    }
    else if (array_name == "zvalarray")
    {
      cmds.AppendCallFunction("dcomplex", args.set(2, "1.0", "0.0"), "zbase");
      cmds.AppendCallFunction(array_name, args.set(2, "zbase", "N"), "x");
    }

    //-------------------------------------------------------------------
    // Establish the actions that are to be exercised.
    //-------------------------------------------------------------------

    Test.Message() << "...appending function...\n";

    cmds.AppendCallFunction("mix", args.set(3, "phase", "carrier", "x"), "y");

    cmds.AppendCallFunction("mix", args.set(4, "phase", "carrier", "x", "state"), "y2");
    cmds.AppendCallFunction("mix", args.set(4, "phase", "carrier", "x", "state"), "y3");
    cmds.AppendCallFunction("mix", args.set(2, "x", "state"), "y4");

    cmds.AppendIntermediateResult("y4", "result", "Final Result", "" );

    //-------------------------------------------------------------------
    // Execute the requests
    //-------------------------------------------------------------------

    Test.Message() << "...executing...\n";

    cmds.Execute();

    //-------------------------------------------------------------------
    // Perform the same calculations using the native compiler
    //-------------------------------------------------------------------

    Test.Message() << "...generating...\n";

    double phase = 0.1234;
    double carrier = 0.5678;
    datacondAPI::Sequence<Tin> x(1.0, 16);

    datacondAPI::Mixer mix(datacondAPI::MixerState(phase, carrier));
    
    datacondAPI::Sequence<std::complex<Tout> > y;
    datacondAPI::Sequence<std::complex<Tout> > y2;
    datacondAPI::Sequence<std::complex<Tout> > y3;
    datacondAPI::Sequence<std::complex<Tout> > y4;
    datacondAPI::udt* y_pointer = &y;
    datacondAPI::udt* y2_pointer = &y2;
    datacondAPI::udt* y3_pointer = &y3;
    datacondAPI::udt* y4_pointer = &y4;
    
    mix.apply(y_pointer, x);
    mix.apply(y2_pointer, x);
    mix.apply(y3_pointer, x);

    phase = mix.getState().GetPhase();

    mix.apply(y4_pointer, x);

    //-------------------------------------------------------------------
    // Check to see if the Mixer result calculated from the CallChain
    //   matches the result calculated from the compiler.
    //-------------------------------------------------------------------

    Test.Message() << "...comparing...\n";

    check_valarray(y, cmds.GetSymbol("y"), "mixer output");
    check_valarray(y2, cmds.GetSymbol("y2"), "mixer output2");
    check_valarray(y3, cmds.GetSymbol("y3"), "mixer output3");
    check_value(phase, *(cmds.GetSymbol("phase")), "mixer phase");
    check_valarray(y4, cmds.GetSymbol("y4"), "mixer output4");
  }
  //---------------------------------------------------------------------
  // Catch standard exceptions and set Test results.
  //    This macro is defined in "token.hh"
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  Test.Message() << "Mixer<" << type_out << ", " << type_in << ">: end\n";  

}

//-----------------------------------------------------------------------
// Main
//-----------------------------------------------------------------------

int
main(int ArgC, char** ArgV)
{
  //---------------------------------------------------------------------
  // Initialize UnitTest class
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  try
  {
    //-------------------------------------------------------------------
    // Perform tests
    //-------------------------------------------------------------------
    mixer_test<float, float>("float", "float", "svalarray");
    mixer_test<double, double>("double", "double", "dvalarray");
    mixer_test<float, std::complex<float> >("float", "complex<float>",
					    "cvalarray");
    mixer_test<double, std::complex<double> >("double", "complex<double>",
					    "zvalarray");
  }

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Terminate program with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Exit();
}
