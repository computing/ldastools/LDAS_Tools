//!author: "Lee Samuel Finn"
//: test warning exception  class

#include "datacondAPI/config.h"

#include <iostream>
#include <string>

#include "general/unittest.h"

#include "Warning.hh"

using namespace std;  //introduces namespace std

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{	
    Test.Init ( ArgC , ArgV );
	if (Test.IsVerbose())
	{
		cout << "$Id: tWarning.cc,v 1.4 2009/05/20 00:14:59 emaros Exp $" << endl;
	}
	
	// Throw the exception, catch it, and check its data
	
	const char* s = "Warning error";
	const int v(5);
	try {
		throw warning<int>(s, v);
	} catch (warning<int>& x) {
		Test.Check(true, "caught warning");  
		Test.Check(0 == strcmp(x.what(), s)) 
			<< s << " == " << x.what() << endl;
		int vv;
		x.returns(vv);
		Test.Check(v == vv)
			<< v << " == " << vv << endl;
	} catch (bad_exception &x) {
		Test.Check(false,"caught bad exception");
	}
	Test.Exit(); 
}

