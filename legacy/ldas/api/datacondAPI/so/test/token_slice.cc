#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "DatacondCaller.h"
#include "TransFunc.h"

#include "token.hh"

using namespace std;
   
General::UnitTest	Test;

void slice_exception_marginal(const std::string& array_type,
			      const std::string& array_init)
{
  try
  {
    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "11"), "N");
    cmds.AppendCallFunction("integer", args.set(1, "0"), "zero");
    cmds.AppendCallFunction("integer", args.set(1, "7"), "seven");
    cmds.AppendCallFunction("integer", args.set(1, "2"), "two");
    cmds.AppendCallFunction("double", args.set(1, "1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "d", "d"), "cf");
    cmds.AppendCallFunction("dcomplex", args.set(2, "d", "d"), "cd");
    if (array_type == "svalarray")
    {
       cmds.AppendCallFunction("svalarray", args.set(2, "d", "N"), "v");
    }
    else if (array_type == "dvalarray")
    {
      cmds.AppendCallFunction("dvalarray", args.set(2, "d", "N"), "v");
    }    
    else if (array_type == "cvalarray")
    {
      cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "N"), "v");
    }
    else
    {
      cmds.AppendCallFunction("zvalarray", args.set(2, "cd", "N"), "v");
    }

    cmds.AppendCallFunction("slice", args.set(4, "v", "zero", "seven", "two"), "y");
    cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

    cmds.Execute();

    Test.Check(false) << "bad slice did not throw\n";
  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception <" << x.GetMessage() << ">\n";
  }
  catch(std::logic_error& x)
  {
    Test.Check(true) << x.what() << "\n";
  }
  catch( std::exception& x )
  {
    std::ostringstream msg;
    msg <<"bad slice threw wrong exception: " << x.what( ) << std::endl;
    Test.Check(false) << msg.str( );
  }
  catch(...)
  {
    Test.Check(false) << "bad slice threw wrong exception\n";
  }
}

void slice_exception(const std::string& array_type, const std::string& array_init)
{
  try
  {
    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "10"), "N");
    cmds.AppendCallFunction("integer", args.set(1, "0"), "zero");
    cmds.AppendCallFunction("integer", args.set(1, "2"), "two");
    cmds.AppendCallFunction("double", args.set(1, "1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "d", "d"), "cf");
    cmds.AppendCallFunction("dcomplex", args.set(2, "d", "d"), "cd");
    if (array_type == "svalarray")
    {
       cmds.AppendCallFunction("svalarray", args.set(2, "d", "N"), "v");
    }
    else if (array_type == "dvalarray")
    {
      cmds.AppendCallFunction("dvalarray", args.set(2, "d", "N"), "v");
    }    
    else if (array_type == "cvalarray")
    {
      cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "N"), "v");
    }
    else
    {
      cmds.AppendCallFunction("zvalarray", args.set(2, "cd", "N"), "v");
    }

    cmds.AppendCallFunction("slice", args.set(4, "v", "zero", "N", "two"), "y");
    cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

    cmds.Execute();

    Test.Check(false) << "bad slice did not throw\n";
  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception <" << x.GetMessage() << ">\n";
  }
  catch(std::logic_error& x)
  {
    Test.Check(true) << x.what() << "\n";
  }
  catch( std::exception& x )
  {
    std::ostringstream msg;
    msg <<"bad slice threw wrong exception: " << x.what( ) << std::endl;
    Test.Check(false) << msg.str( );
  }
  catch(...)
  {
    Test.Check(false) << "bad slice threw wrong exception\n";
  }
}

void slice_exception_neg1()
{
  try
  {
    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "-1"), "s");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
    cmds.AppendCallFunction("integer", args.set(1, "10"), "N");
    cmds.AppendCallFunction("double", args.set(1, "1"), "d");
   cmds.AppendCallFunction("scomplex", args.set(2, "d", "d"), "cf");
    cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "N"), "v");

    cmds.AppendCallFunction("slice", args.set(4, "v", "s", "one", "one"), "y");

    cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

    cmds.Execute();

    Test.Check(false) << "bad slice did not throw\n";
  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception <" << x.GetMessage() << ">\n";
  }
  catch(std::logic_error& x)
  {
    Test.Check(true) << x.what() << "\n";
  }
  catch( std::exception& x )
  {
    std::ostringstream msg;
    msg <<"bad slice threw wrong exception: " << x.what( ) << std::endl;
    Test.Check(false) << msg.str( );
  }
  catch(...)
  {
    Test.Check(false) << "bad slice threw wrong exception\n";
  }
}

void slice_exception_neg2()
{
  try
  {
    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "-1"), "s");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
    cmds.AppendCallFunction("integer", args.set(1, "10"), "N");
    cmds.AppendCallFunction("double", args.set(1, "1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "d", "d"), "cf");
    cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "N"), "v");

    cmds.AppendCallFunction("slice", args.set(4, "v", "one", "s", "one"), "y");

    cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

    cmds.Execute();

    Test.Check(false) << "bad slice did not throw\n";
  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception <" << x.GetMessage() << ">\n";
  }
  catch(std::logic_error& x)
  {
    Test.Check(true) << x.what() << "\n";
  }
  catch( std::exception& x )
  {
    std::ostringstream msg;
    msg <<"bad slice threw wrong exception: " << x.what( ) << std::endl;
    Test.Check(false) << msg.str( );
  }
  catch(...)
  {
    Test.Check(false) << "bad slice threw wrong exception\n";
  }
}

void slice_exception_neg3()
{
  try
  {
    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "-1"), "s");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
    cmds.AppendCallFunction("integer", args.set(1, "10"), "N");
    cmds.AppendCallFunction("double", args.set(1, "1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "d", "d"), "cf");
    cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "N"), "v");

    cmds.AppendCallFunction("slice", args.set(4, "v", "one", "s", "one"), "y");

    cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

    cmds.Execute();

    Test.Check(false) << "bad slice did not throw\n";
  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception <" << x.GetMessage() << ">\n";
  }
  catch(std::logic_error& x)
  {
    Test.Check(true) << x.what() << "\n";
  }
  catch( std::exception& x )
  {
    std::ostringstream msg;
    msg <<"bad slice threw wrong exception: " << x.what( ) << std::endl;
    Test.Check(false) << msg.str( );
  }
  catch(...)
  {
    Test.Check(false) << "bad slice threw wrong exception\n";
  }
}

void slice_exception_start()
{
  try
  {
    CallChain cmds;
    Parameters args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "10"), "s");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "zero");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
    cmds.AppendCallFunction("integer", args.set(1, "10"), "N");
    cmds.AppendCallFunction("double", args.set(1, "1"), "d");
    cmds.AppendCallFunction("scomplex", args.set(2, "d", "d"), "cf");
    cmds.AppendCallFunction("cvalarray", args.set(2, "cf", "N"), "v");

    cmds.AppendCallFunction("slice", args.set(4, "v", "s", "zero", "one"), "y");

    cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

    cmds.Execute();

    Test.Check(false) << "bad slice did not throw\n";
  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception <" << x.GetMessage() << ">\n";
  }
  catch(std::logic_error& x)
  {
    Test.Check(true) << x.what() << "\n";
  }
  catch( std::exception& x )
  {
    std::ostringstream msg;
    msg <<"bad slice threw wrong exception: " << x.what( ) << std::endl;
    Test.Check(false) << msg.str( );
  }
  catch(...)
  {
    Test.Check(false) << "bad slice threw wrong exception\n";
  }
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_slice.cc,v 1.12 2007/01/24 17:51:28 emaros Exp $" << endl;

  try {

  CallChain		cmds;
  Parameters		args;
  CallChain::Symbol*	result;

  //---------------------------------------------------------------------
  // Addition - Valarrays
  //---------------------------------------------------------------------
  static float _vs_add_result[] = {6};

  const std::valarray<float> vs_add_result(_vs_add_result,
				      sizeof(_vs_add_result)/
				      sizeof(*_vs_add_result));

  cmds.Reset();
  cmds.AppendCallFunction("integer", args.set(1, "4"), "N");
  cmds.AppendCallFunction("integer", args.set(1, "1"), "Start");
  cmds.AppendCallFunction("integer", args.set(1, "1"), "Size");
  cmds.AppendCallFunction("integer", args.set(1, "1"), "Stride");
  cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
  cmds.AppendCallFunction("integer", args.set(1, "0"), "zero");
  cmds.AppendCallFunction("svalarray", args.set(2, "one", "N"), "in");
  cmds.AppendCallFunction("integer", args.set(1, "5"), "scalar");
  cmds.AppendCallFunction("slice", args.set(4, "in",
					    "Start", "Size", "Stride"), "sin");
  cmds.AppendCallFunction("add", args.set(2, "sin", "scalar"), "result");

  // Check special cases
  cmds.AppendCallFunction("slice", args.set(4, "in",
					    "zero", "one", "one"), "");
  cmds.AppendCallFunction("slice", args.set(4, "in",
					    "one", "zero", "one"), "");
  cmds.AppendCallFunction("slice", args.set(4, "in",
					    "one", "one", "zero"), "y");

  cmds.AppendIntermediateResult("y", "result", "Final Result", "" );

  cmds.Execute();

  result = cmds.GetSymbol("result");
  check_valarray(vs_add_result, result,
		 "slice_math: vector-scalar addition: ");

  //---------------------------------------------------------------------
  // Use standalone DatacondCaller
  //---------------------------------------------------------------------

  static const char* actions =
    "in = svalarray( 1, 4);\n"
    "sin = slice( in, 1, 1, 1 );\n"
    "result = add( sin, 5);\n"
    "slice( in, 0, 1, 1);\n"
    "slice( in, 1, 0, 1);\n"
    "y = slice( in, 1, 1, 0);\n"
    "output( y, _, _, result,Final Result);\n"
    ;

  char* errors;
  int	err;

  datacond_symbol_type iosymbols[] = {
    {
      DATACOND_SYMBOL_OUTPUT,
      TranslateUDT,
      "symbol:result",
      (void*)NULL, /* Aux Data */
      (void*)NULL  /* UserData */
    }
  };

  err = DatacondCaller( actions,
			"",
			iosymbols,
			sizeof( iosymbols ) / sizeof( iosymbols ),
			&errors );
  if ( err != DATACOND_OK )
  {
    Test.Check(false)
      << "DatacondCaller: err: " << err;
    // Relese the memory
    if ( errors )
    {
      Test.Message( false ) << ": " << errors;
      free( errors );
      errors = (char*)NULL;
    }
    Test.Message( false ) << std::endl;
  }
  else
  {
    if ( iosymbols[0].s_user_data )
    {
      check_valarray( vs_add_result,
		      reinterpret_cast< CallChain::Symbol * >
		      ( iosymbols[0].s_user_data ),
		      "slice_math via DatacondCaller: vector-scalar addition: ");
    }
    else
    {
      Test.Check( false )
	<< "Unble to obtain value for result"
	<< std::endl;
    }
  }
  delete reinterpret_cast< CallChain::Symbol *>( iosymbols[0].s_user_data );


  //---------------------------------------------------------------------
  // Test exception cases
  //---------------------------------------------------------------------

  slice_exception_marginal("svalarray", "d");
  slice_exception_marginal("dvalarray", "d");
  slice_exception_marginal("cvalarray", "cf");
  slice_exception_marginal("zvalarray", "cd");

  slice_exception("svalarray", "d");
  slice_exception("dvalarray", "d");
  slice_exception("cvalarray", "cf");
  slice_exception("zvalarray", "cd");

  slice_exception_start();

  slice_exception_neg1();
  slice_exception_neg2();
  slice_exception_neg3();

  }
  catch(CallChain::Exception& x)
  {
    Test.Check(false) << "CallChain::Exception<" << x.GetMessage() << ">\n";
  }
  catch(std::exception& x)
  {
    Test.Check(false) << "std::exception<" << x.what() << ">\n";
  }
  catch(...)
  {
    Test.Check(false) << "slice tests threw an exception\n";
  }

  Test.Exit();
}
