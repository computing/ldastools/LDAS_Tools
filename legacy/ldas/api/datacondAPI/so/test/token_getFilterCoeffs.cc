#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

#include "random.hh"

#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"

using namespace datacondAPI;

General::UnitTest Test;

gasdevstate seed;

void gasdev(std::valarray<float>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<double>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<std::complex<float> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<float>(gasdev(seed), gasdev(seed));
    }
}

void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}

template<typename in> struct ensure_complex // complex traits
{
    typedef std::complex<in> type;
};

template<typename in> struct ensure_complex<std::complex<in> >
{
    typedef std::complex<in> type;
};

void test_specific()
{
    Test.Message() << "specific" << std::endl;
}

template<typename type>
void test_generic(const std::string& name)
{
    Test.Message() << "generic<" << name << ">" << std::endl;

    Test.Message() << "arls" << std::endl;

    {
        CallChain commands;
        Parameters arguments;

        Sequence<type> x(100);
        gasdev(x);
        commands.AddSymbol("x", x.Clone());

        Scalar<int> ten(10);
        commands.AddSymbol("ten", ten.Clone());

        Scalar<int> zero(0);
        commands.AddSymbol("zero", zero.Clone());

        // b1 = arls(x, ten, z);
        commands.AppendCallFunction("arls", arguments.set(3, "x", "ten", "z"), "b1");
        // getFilterCoeffs(z, zero, b2, a2);
        commands.AppendCallFunction("getFilterCoeffs", arguments.set(3, "z", "b2", "a2"), "");
        // output(b2, "result", "Final Result");
        commands.AppendIntermediateResult("b2", "result", "Final Result", "" );

        commands.Execute();

        udt* p = commands.GetSymbol("b2");

        if (udt::IsA<Sequence<type> >(*p))
        {
            Test.Check(true) << "filter is of correct type\n";
            Sequence<type>& b = udt::Cast<Sequence<type> >(*p);
            Test.Check(b.size() == 11) << "filter is correct size\n";
        }
        else
        {
            Test.Check(false) << "filter is of correct type\n";
        }

        (void)commands.GetSymbol("a2");
    }

    Test.Message() << "oelslr\n";

    {
        CallChain commands;
        Parameters arguments;

        TimeSeries<type> y(Sequence<type>(20480), 2048.0);
        gasdev(y);
        commands.AddSymbol("y", y.Clone());

        TimeSeries<type> u(Sequence<type>(20480), 2048.0);
        gasdev(u);
        commands.AddSymbol("u", u.Clone());

        Scalar<double> sixty(0.5);
        commands.AddSymbol("sixty", sixty.Clone());

        Scalar<int> five(5);
        commands.AddSymbol("five", five.Clone());

        Scalar<int> three(3);
        commands.AddSymbol("three", three.Clone());

        Scalar<int> sixteen(6);
        commands.AddSymbol("sixteen", sixteen.Clone());

        Scalar<int> one(1);
        commands.AddSymbol("one", one.Clone());

        // w = oelslr(y, u, sixty, five, three, z);
        commands.AppendCallFunction("oelslr", arguments.set(5, "y", "u", "sixty", "five", "sixteen"),"z");

        // getFilterCoeffs(z, one, b, a);
        commands.AppendCallFunction("getFilterCoeffs", arguments.set(3, "z", "b", "a"), "");

        // output(b, "result", "Final Result");
        commands.AppendIntermediateResult("b", "result", "Final Result", "" );

        commands.Execute();

        udt* p = commands.GetSymbol("b");

        Test.Check(udt::IsA<Sequence<typename ensure_complex<type>::type> >(*p)) << "filter is of correct type\n";

        (void)commands.GetSymbol("a");

    }

    Test.Message() << "linfilt\n";

    {
        CallChain commands;
        Parameters arguments;

        Sequence<double> a(1.0, 7);
        Sequence<double> b(1.0, 9);
        Sequence<type> x(100);

        commands.AddSymbol("a", a.Clone());
        commands.AddSymbol("b", b.Clone());
        commands.AddSymbol("x", x.Clone());
        commands.AddSymbol("zero", new Scalar<int>(0));
        commands.AppendCallFunction("linfilt", arguments.set(4, "b", "a", "x", "z"), "y");
        commands.AppendCallFunction("getFilterCoeffs", arguments.set(3, "z", "got_b", "got_a"), "");
        commands.AppendIntermediateResult("a", "result", "Final Result", "");
   
        commands.Execute();

        udt* got_b = commands.GetSymbol("got_b");
        udt* got_a = commands.GetSymbol("got_a");
        Test.Check(udt::IsA<Sequence<double> >(*got_b)) << "filter is of correct type\n";
        Test.Check(udt::Cast<Sequence<double> >(*got_b).size() == b.size()) << "filter is of correct size\n";
        Test.Check(udt::IsA<Sequence<double> >(*got_a)) << "filter is of correct type\n";
        Test.Check(udt::Cast<Sequence<double> >(*got_a).size() == a.size()) << "filter is of correct size\n";
    }

    Test.Message() << "resample\n";

    {
        CallChain commands;
        Parameters arguments;

        Sequence<type> x(36);

        commands.AddSymbol("x", x.Clone());
        commands.AddSymbol("zero", new Scalar<int>(0));
        commands.AddSymbol("two", new Scalar<int>(2));
        commands.AddSymbol("three", new Scalar<int>(3));
        commands.AppendCallFunction("resample", arguments.set(4, "x", "two", "three", "z"), "y");
        commands.AppendCallFunction("getFilterCoeffs", arguments.set(3, "z", "got_b", "got_a"), "");
        commands.AppendIntermediateResult("x", "result", "Final result", "");    

        commands.Execute();

        udt* got_b = commands.GetSymbol("got_b");
        udt* got_a = commands.GetSymbol("got_a");

        Test.Check(udt::IsA<Sequence<double> >(*got_b)) << "filter is of correct type\n";
        Test.Check(udt::IsA<Sequence<double> >(*got_a)) << "filter is of correct type\n";
        Test.Check(udt::Cast<Sequence<double> >(*got_a).size() == 1) << "filter is of correct size\n";
   }
}

int main(int argc, char* argv[])
try
{
    Test.Init(argc, argv);

    test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("std::complex<float>");
    test_generic<std::complex<double> >("std::complex<double>");

    Test.Exit();
}
catch(const CallChain::BadSymbol& x)
{
    Test.Check(false) << "unexpected BadSymbol: " << x.what() << '\n';
    Test.Exit();
}
catch(const std::bad_cast& x)
{
    Test.Check(false) << "unexpected bad_cast\n";
    Test.Exit();
}
catch(const std::exception& x)
{
    Test.Check(false) << "unexpected exception: " << x.what() << '\n';
    Test.Exit();
}
catch(...)
{
    Test.Check(false) << "unexpected exception\n";
    Test.Exit();
}

