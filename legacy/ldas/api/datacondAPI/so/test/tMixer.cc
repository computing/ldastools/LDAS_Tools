/* -*- mode: c++; c-basic-offset: 2; -*- */
//! author="Lee Samuel Finn"
// Test code for Mixer

#include "datacondAPI/config.h"

#include <iostream>
#include <stdexcept>
#include <valarray>

#include "general/unittest.h"
#include "filters/LDASConstants.hh"

#include "Mixer.hh"
#include "MixerState.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"

#include "CallChain.hh"

using namespace std;
using General::GPSTime;

void replacement_unexpected_handler()
{
  cout << "bad exception" << endl;
  throw bad_exception(); 
}

General::UnitTest	Test;

void sanity()
{
    std::valarray<float> in(1.0, 122880);
    datacondAPI::Mixer mixer(datacondAPI::MixerState(0.0, -0.1));
    std::valarray<std::complex<float> > out;
    mixer.apply(out, in);

    for (std::size_t i = 0; i < in.size(); ++i)
    {
        std::complex<float> expected(cos(-0.1 * 3.1415926535897932384626433832795 * i), sin(-0.1 * 3.1415926535897932384626433832795 * i));
        if (abs(out[i] - expected) > 1e-5)
        {
            Test.Check(false) << "unacceptable error at [" << i << "[\n";
        }
    }
}

template< class T >
bool
within_tolerance( T left, T right );

template< >
inline bool
within_tolerance<float>( float Left, float Right )
{
  const static float	tolerance = 1.0e-6;
  float	diff( abs( Left - Right ) );

  return ( diff < tolerance );
}

template< >
inline bool
within_tolerance<double>( double Left, double Right )
{
  const static double	tolerance = 1.0e-15;
  double	diff( abs( Left - Right ) );

  return ( diff < tolerance );
}

int main(int ArgC, char** ArgV) 
try
{  

  // Tests (to be) performed
  // 
  // Creation of Mixer objects of different types
  // Input, output validity:
  //       Valid input
  //       Nearly invalid input
  //       Nearly valid input
  //       Input that stresses implementation details
  // 
  //       In cases of valid input, check that output is valid,
  //       correct, and that the Mixer final state is legitimate.  In
  //       cases of invalid input, check that we raise the appropriate
  //       exceptions and that filter state is left unchanged. (Need
  //       to document that this is the behavior under invalid input.)

  //!TODO: Test copy constructor

  Test.Init(ArgC, ArgV);
  if (Test.IsVerbose())
  {
    cout << "$Id: tMixer.cc,v 1.21 2005/12/01 22:55:03 emaros Exp $" << endl;
  }

  // catch any unexpected exceptions

  try {

    sanity();

    // Commensurable carrier tests (i.e., carrier that is
    // reciprocal of (small) integer)

    // Create several identical mixers and a legitimate input sequence
    // whose length is >> than but not commensurate with 1/fc. 

    // Let the first mixer operate on the entire sequence.

    // Let the second mixer operate on the sequence broken into two
    // pieces, both > 1/fc and neither commensurate with 1/fc. Verify
    // that there is no difference in the results. This is a partial
    // check of the loop unrolling in the mixer.

    // Let the third mixer operate on the sequence split so that the
    // first part is commensurate with 1/fc. This is a partial check
    // of loop unrolling in the mixer, in the special case where there
    // is no remainder.

    // Let the third mixer operate on the sequence split so that the
    // first part has length < 1/fc. This is a partial check
    // of loop unrolling in the special case where there is no
    // loop unrolling. 

    // Repeat with a carrier frequency that is not commensurate with
    // any integer length

    // Check invertibility of mixer operation

    // Check that the mixer applied to length zero valarray excepts
    // and leaves mixer state unchanged

    // Check that the mixer can be correctly applied to legitimate
    // data after being applied to invalid data

    double fc(1.0/8.0);

    datacondAPI::Mixer mixer0(datacondAPI::MixerState(0.0,fc));
    datacondAPI::Mixer mixer1(datacondAPI::MixerState(0.0,fc));
    datacondAPI::Mixer mixer2(datacondAPI::MixerState(0.0,fc));
    datacondAPI::Mixer mixer3(datacondAPI::MixerState(0.0,fc));
   
    // Create input sequence
    
    int N(12345); 
    int M(6172);
    int L(8192);
    int K(5);

    datacondAPI::Sequence<float> in(1.0,N);
    const datacondAPI::Sequence<float>&
      const_in( static_cast< const datacondAPI::Sequence<float>& >( in ) );

    complex<float> z0(0.0f,0.0f);

    datacondAPI::Sequence<complex<float> > out0(z0,N);
    datacondAPI::Sequence<complex<float> > out1(z0,N);
    datacondAPI::Sequence<complex<float> > out2(z0,N);
    datacondAPI::Sequence<complex<float> > out3(z0,N);

    // slices for output sequences

    slice bslice1(0,M,1);
    slice eslice1(M,N-M,1);

    slice bslice2(0,L,1);
    slice eslice2(L,N-L,1);

    slice bslice3(0,K,1);
    slice eslice3(K,N-K,1);

    // Test for some exceptions

    try {
      datacondAPI::Mixer mixer(datacondAPI::MixerState(LDAS_PI,0.0));
      datacondAPI::Sequence<complex<float> > out(0);
      datacondAPI::Sequence<float> in(0);
      datacondAPI::udt* out_pointer = &out;
      mixer.apply(out_pointer,in);
      Test.Check(false) << "in.size() == 0" << std::endl << std::flush;
    } catch (std::exception& r) {
      Test.Check(true) << "in.size() == 0" << std::endl << std::flush;
    }
      
    try {
      datacondAPI::Mixer mixer(datacondAPI::MixerState(0.0,1.1));
      Test.Check(false) << "fc >= 1" << std::endl << std::flush;
    } catch (std::exception& r) {
      Test.Check(true) << "fc >= 1" << std::endl << std::flush;
    }
      
    // apply filter

    try {

      datacondAPI::Sequence<complex<float> > out;
      datacondAPI::udt* out_pointer = &out;
      mixer3.apply(out_pointer,datacondAPI::Sequence<float>(const_in[bslice3]));
      out3[bslice3] = out;
      mixer3.apply(out_pointer,datacondAPI::Sequence<float>(const_in[eslice3]));
      out3[eslice3] = out;

      mixer2.apply(out_pointer,datacondAPI::Sequence<float>(const_in[bslice2]));
      out2[bslice2] = out;
      mixer2.apply(out_pointer,datacondAPI::Sequence<float>(const_in[eslice2]));
      out2[eslice2] = out;

      mixer1.apply(out_pointer,datacondAPI::Sequence<float>(const_in[bslice1]));
      out1[bslice1] = out;
      mixer1.apply(out_pointer,datacondAPI::Sequence<float>(const_in[eslice1]));
      out1[eslice1] = out;

      datacondAPI::udt* out0_pointer = &out0;
      mixer0.apply(out0_pointer,in);

    } catch (std::exception& r) {
      Test.Check(false) 
	<< "Caught unxexpected exception: " 
	<< r.what() << endl;
    }

    // First inspect representative output 

    {
      float d(0.0);
      for ( int k = 0 ; k < 10 ; k++ ) {
	complex<float> z(cos(LDAS_PI*k*fc), sin(LDAS_PI*k*fc));
	d += abs ( out0[k] - z );
      }
      Test.Check( d < 1e-6 ) 
	<< "Representative output: diff = " 
	<< d
	<< std::endl
	<< std::flush;
    }

    // Check all the outputs against each other

    // Check that we get the same results for all the examples

    datacondAPI::Sequence<float> diff(in.size());
    for (int k = 0; k < N; k++)
      diff[k] = abs(out0[k] -out1[k]);
    Test.Check(diff.max()<1e-7)
      << "broken incommensurate (max diff = "
      << diff.max() << ")" << endl;

    for (int k = 0; k < N; k++)
      diff[k] = abs(out0[k] -out2[k]);
    Test.Check(diff.max()<1e-7)
	 << "broken semi-commensurate (max diff = " 
	 << diff.max() << ")" << endl;

    for (int k = 0; k < N; k++)
      diff[k] = abs(out0[k] -out3[k]);
    Test.Check(diff.max()<1e-7)
	 << "broken short incommensurate (max diff = " 
	 << diff.max() << ")" << endl;

    // invert filter
    
    datacondAPI::Mixer cmixer(datacondAPI::MixerState(0.0,-fc));
    try {
      datacondAPI::udt* out1_pointer = &out1;
      cmixer.apply(out1_pointer,out3); // re-use old output
    } catch (std::exception& r) {
      cout << "Unexpected exception: " << r.what() << endl;
    }

    for (size_t k = 0; k < in.size(); k++)
      diff[k] = abs(out1[k]-in[k]);

    Test.Check(diff.max()<1e-7)
      << "(invert filter: max diff = " << diff.max() << ")" 
      << endl;

    // Check that mixer applied to length 0 data excepts and leaves
    // mixer state unchanged

    datacondAPI::MixerState state(mixer0.getState());
    try {
      datacondAPI::Sequence<float> v0(0);
      datacondAPI::Sequence<complex<float> > v1;
      datacondAPI::udt* v1_pointer = &v1;
      mixer0.apply(v1_pointer,v0);
      Test.Check(false) << "length 0 input valarray" << endl;
    } catch (std::exception& r) {
      Test.Check(true)
	<< "length 0 input valarray (" 
	<< r.what() << ")" << endl;
      
      bool tv(true);
      tv = tv && state.GetFrequency() == mixer0.getState().GetFrequency();
      tv = tv && state.GetPhase() == mixer0.getState().GetPhase();
      Test.Check(tv)
	<< "state unchanged after input data exception" 
	<< endl;
    }

  } catch (std::exception& r) {
    cout << "Unexpected exception: " << r.what() << endl;
    throw;
  }

  // Mixer tests on TimeSeries
  try {
    
    Test.Message() << "Mixer acting on TimeSeries" << endl;

    const double fc1 = 1.0/8.0;
    const double fc2 = 1.0/6.0;

    const double phi1 = LDAS_TWOPI/6.0;
    const double phi2 = LDAS_TWOPI/8.0;
    
    datacondAPI::Mixer mixer0(datacondAPI::MixerState(phi1, fc1));
    datacondAPI::Mixer mixer1(datacondAPI::MixerState(phi2, fc2));
   
    // Create input sequence
    
    const int N = 64;
    const double srate = 2048.0;
    const GPSTime stime(610000000, 0);

    const double fc1_hz = fc1*srate/2.0;
    const double fc2_hz = fc2*srate/2.0;

    datacondAPI::TimeSeries<float> in;
    in.SetName("H2:LSC-AS_Q");
    in.resize(N, 1.0);

    // Set meta-data
    in.SetSampleRate(srate);
    in.SetStartTime(stime);

    Test.Message() << "mixer0(phase = " << phi1
		   << ", freq = " << fc1 << ")"
		   << endl;

    Test.Check(in.GetBaseFrequency() == 0.0)
      << "TimeSeries initial base frequency = 0.0"
      << endl;

    Test.Check(in.GetPhase() == 0.0) << "TimeSeries initial phase = 0.0"
				     << endl;

    datacondAPI::udt* out_p = 0;

    Test.Message() << "Applying mixer0 to TimeSeries with sample rate = "
		   << in.GetSampleRate() << " Hz"
		   << endl;

    Test.Message() << "giving frequency shift of "
		   << in.GetSampleRate() << " * " << fc1 << " = "
		   << fc1_hz << " Hz"
		   << endl;

    mixer0.apply(out_p, in);

    datacondAPI::TimeSeries<std::complex<float> >* ts_out_p
      = dynamic_cast<datacondAPI::TimeSeries<std::complex<float> >*>(out_p);

    Test.Check(ts_out_p != 0) << "Output is a TimeSeries<std::complex<float> >"
			      << endl;

    datacondAPI::TimeSeries<std::complex<float> >& out = *ts_out_p;

    // Check the values
    Test.Check(out.size() == (unsigned int)N)
      << "Output has correct length = " << N << endl;

    {
      float d(0.0);
      for ( int k = 0 ; k < 10 ; k++ ) {
	complex<float> z(cos(LDAS_PI*k*fc1 + phi1), sin(LDAS_PI*k*fc1 + phi1));
	d += abs ( out[k] - z );
      }
      Test.Check( d < 1e-6 ) 
	<< "Output is correct: diff = " 
	<< d
	<< std::endl
	<< std::flush;
    }

    // Check time meta-data
    Test.Check(out.GetStartTime() == in.GetStartTime())
      << "Output has correct start time "
      << out.GetStartTime().GetSeconds() << "s "
      << out.GetStartTime().GetNanoseconds() << "ns"
      << endl;

    Test.Check(out.GetEndTime() == in.GetEndTime())
      << "Output has correct end time "
      << out.GetEndTime().GetSeconds() << "s "
      << out.GetEndTime().GetNanoseconds() << "ns"
      << endl;

    // Check other meta-data
    Test.Check(out.GetSampleRate() == in.GetSampleRate())
      << "Output has correct sample rate = " << out.GetSampleRate()
      << " Hz"
      << endl;

    Test.Check( within_tolerance( out.GetBaseFrequency(), fc1_hz ) )
      << "Output has correct base frequency, "
      << out.GetBaseFrequency() << " Hz"
      << " (" << fc1_hz << " Hz)"
      << endl;

    Test.Check(out.GetPhase() == phi1)
      << "Output has correct phase, "
      << out.GetPhase()
      << endl;


    // Mix again
    Test.Message() << "Mix already mixed TimeSeries" << endl;

    datacondAPI::udt* out2_p = 0;

    Test.Message() << "mixer1(phase = " << phi2
		   << ", freq = " << fc2 << ")"
		   << endl;

    Test.Message() << "Applying mixer1 to TimeSeries with sample rate = "
		   << in.GetSampleRate() << " Hz"
		   << endl;

    Test.Message() << "giving frequency shift of "
		   << in.GetSampleRate() << " * " << fc2 << " = "
		   << fc2_hz << " Hz"
		   << endl;

    mixer1.apply(out2_p, out);

    datacondAPI::TimeSeries<std::complex<float> >* ts_out2_p
      = dynamic_cast<datacondAPI::TimeSeries<std::complex<float> >*>(out2_p);

    Test.Check(ts_out2_p != 0) <<"Output is a TimeSeries<std::complex<float> >"
			       << endl;

    datacondAPI::TimeSeries<std::complex<float> >& out2 = *ts_out2_p;

    // Check the values
    Test.Check(out2.size() == (unsigned int)N)
      << "Output has correct length" << endl;

    {
      float d(0.0);
      const datacondAPI::MixerState state(mixer0.getState());
      for ( int k = 0 ; k < 10 ; k++ ) {
	complex<float> z(cos(LDAS_PI*k*fc2 + phi2), sin(LDAS_PI*k*fc2 + phi2));
	d += abs ( out2[k] - out[k]*z );
      }
      Test.Check( d < 1e-6 ) 
	<< "Output is correct: diff = " 
	<< d
	<< std::endl
	<< std::flush;
    }

    // Check time meta-data
    Test.Check(out2.GetStartTime() == in.GetStartTime())
      << "Output has correct start time "
      << out2.GetStartTime().GetSeconds() << "s "
      << out2.GetStartTime().GetNanoseconds() << "ns"
      << endl;

    Test.Check(out2.GetEndTime() == in.GetEndTime())
      << "Output has correct end time "
      << out2.GetEndTime().GetSeconds() << "s "
      << out2.GetEndTime().GetNanoseconds() << "ns"
      << endl;

    // Check other meta-data
    Test.Check(out2.GetSampleRate() == in.GetSampleRate())
      << "Output has correct sample rate = " << out2.GetSampleRate()
      << " Hz"
      << endl;

    Test.Check( within_tolerance( out2.GetBaseFrequency(), (fc1_hz + fc2_hz) ) )
      << "Output has correct base frequency, "
      << out2.GetBaseFrequency() << " Hz"
      << " (" << ( fc1_hz + fc2_hz ) << " Hz)"
      << endl;

    Test.Check(out2.GetPhase() == (phi1 + phi2))
      << "Output has correct phase, "
      << out2.GetPhase()
      << endl;

    delete out_p;
    out_p = 0;

    // Check ConvertToIlwd
    
    Test.Message(false) << "Expected data in wrapper format" << endl;

    CallChain cmds;
    ILwd::LdasElement* ilwd = out2_p->ConvertToIlwd(cmds,
					    datacondAPI::udt::TARGET_WRAPPER);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                    Test.Message(),
                                                    ILwd::ASCII);
    Test.Message(false) << endl;

    delete ilwd;

    delete out2_p;
    out2_p = 0;

  } catch (std::exception& r) {
    cout << "Unexpected exception: " << r.what() << endl;
    throw;
  }
  
  Test.Exit();
}
catch(std::exception& x)
{
  cout << "Unexpected exception (global): " << x.what() << endl;
  throw;
}
catch(...)
{
  cout << "Unexpected exception (global): (unknown type)" << endl;
  throw;
}

