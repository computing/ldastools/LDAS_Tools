#include "datacondAPI/config.h"

#include <pthread.h>
#include <stdexcept>
#include <complex>
#include <math.h>

#include "general/unittest.h"

#include "fft.hh"
#include "ifft.hh"

#include <filters/LDASConstants.hh>
#include "SequenceUDT.hh"
#include "DFTUDT.hh"

using namespace datacondAPI;

General::UnitTest Test;

#define TOL 1.0E-05

int
TestCaseA001()
{
    int result = 0;

    std::string what("A001: FFT::apply() for n = 65536");
    
    const size_t n = 65536;
    float val = 2.0;

    Sequence<float> tdata(0.0, n);
    Sequence<std::complex<float> >	fdata_data(std::complex<float>
					   (val*val + 2.0*val + LDAS_PI, 0),
					   n);
    DFT< std::complex<float> >	fdata(fdata_data);
    udt*		       	p_fdata(&fdata);

    FFT fft;
    
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 1: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 1: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 2: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 2: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 3: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true) 
      << "FFT output has correct symmetry"
      << ": Stage 3: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 3: " << what
      << std::endl;

    return result;
}

int
TestCaseA002()
{
    int result = 0;

    std::string what("A002: FFT::apply() float for n = 131072");
    
    const size_t n = 131072;
    double val = -LDAS_E;

    Sequence<double> tdata(0.0, n);
    Sequence<std::complex< double > >  fdata_data(val*val + 2.0*val + LDAS_PI, n);
    DFT< std::complex< double > > fdata(fdata_data);
    udt*			  p_fdata(&fdata);

    FFT fft;
    
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 1: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry" 
      << ": Stage 1: " << what
      << std::endl;
    
    bool exceeds_tol = false;
    for (size_t i = 0; i < n; i++)
    {
	exceeds_tol = (std::abs(fdata[i] - val) > TOL ? true : false);
	if (exceeds_tol) 
	{
	    result |= 1;
	    std::cout << fdata[i] << " " << val << std::endl;
	    break;
	}
    }

    Test.Check(!exceeds_tol)
      << "FFT result is correct"
      << ": Stage 1: " << what
      << std::endl;

    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 2: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 2: " << what
      << std::endl;
    
    exceeds_tol = false;
    for (size_t i = 0; i < n; i++)
    {
	exceeds_tol = (std::abs(fdata[i] - val) > TOL ? true : false);
	if (exceeds_tol) 
	{
	    result |= 1;
	    std::cout << fdata[i] << " " << val << std::endl;
	    break;
	}
    }

    Test.Check(!exceeds_tol)
      << "FFT result is correct"
      << ": Stage 2: " << what
      << std::endl;

    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 3: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 3: " << what
      << std::endl;
    
    exceeds_tol = false;
    for (size_t i = 0; i < n; i++)
    {
	exceeds_tol = (std::abs(fdata[i] - val) > TOL ? true : false);
	if (exceeds_tol) 
	{
	    result |= 1;
	    std::cout << fdata[i] << " " << val << std::endl;
	    break;
	}
    }

    Test.Check(!exceeds_tol)
      << "FFT result is correct"
      << ": Stage 3: " << what
      << std::endl;

    return result;
}

int
TestCaseB001()
{
    int result = 0;

    std::string what("B001: FFT::apply() float for n = 65536");
    
    const size_t n = 65536;
    float val = -5.0;

    Sequence<float> tdata(0.0, n);
    Sequence<std::complex< float > > fdata_data(val*val + 2.0*val + LDAS_PI, n);
    DFT< std::complex< float > >     fdata(fdata_data);
    udt*			     p_fdata(&fdata);

    FFT fft;
    
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 1: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 1: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }
    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 1: " << what
      << std::endl;


    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 2: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 2: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }
    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 2: " << what
      << std::endl;


    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 3: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 3: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 3: " << what
      << std::endl;

    return result;
}

int
TestCaseB002()
{
    int result = 0;

    std::string what("B002: FFT::apply() double float for n = 131072");
    
    const size_t n = 131072;
    double val = LDAS_GAMMA;

    Sequence<double> tdata(0.0, n);
    Sequence< std::complex< double > > fdata_data(val*val + 2.0*val + LDAS_PI, n);
    DFT< std::complex< double > >      fdata(fdata_data);
    udt*			       p_fdata(&fdata);

    FFT fft;
    
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 1: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 1: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 2: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 2: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 3: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 3: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 3: " << what
      << std::endl;

    return result;
}

int
TestCaseR001()
{
    int result = 0;

    std::string what("R001: FFT::apply() float for random n");
    
    int i = 0;
    const int iters = 15;
    
    for (i = 0; i < iters; ++i)
    {
	const int p = rand() % 16;
	const size_t n = (int) pow(2.0, p);

	Test.Message() << "Random FFT: real, single precision, n = "
		       << n << std::endl;

	const Sequence<float> tdata(1.0, n);
	Sequence<float> odata(0.0, n);

	udt* p_tdata = &odata;
	udt* p_fdata = 0;

	FFT fft;
	IFFT ifft;

	fft.apply(p_fdata, tdata);

	ifft.apply(p_tdata, *p_fdata);
	
	for (size_t i = 0; i < n; i++)
	{
	    result |= (std::abs(odata[i] - tdata[i]) < TOL ? 0 : 1);
	}
	
	Test.Check(result == 0)
	    << "FFT result is correct"
	    << what
	    << std::endl;

	delete p_fdata;
    }

    return result;
}


int
TestCaseR002()
{
    int result = 0;

    std::string what("R002: FFT::apply() double for random n");
    
    int i = 0;
    const int iters = 15;
    
    for (i = 0; i < iters; ++i)
    {
	const int p = rand() % 16;
	const size_t n = (int) pow(2.0, p);

	Test.Message() << "Random FFT: real, double precision, n = "
		       << n << std::endl;

	const Sequence<double> tdata(1.0, n);
	Sequence<double> odata(0.0, n);

	udt* p_tdata = &odata;
	udt* p_fdata = 0;

	FFT fft;
	IFFT ifft;

	fft.apply(p_fdata, tdata);

	ifft.apply(p_tdata, *p_fdata);
	
	for (size_t i = 0; i < n; i++)
	{
	    result |= (std::abs(odata[i] - tdata[i]) < TOL ? 0 : 1);
	}
	
	Test.Check(result == 0)
	    << "FFT result is correct"
	    << what
	    << std::endl;

	delete p_fdata;
    }

    return result;
}

int
TestCaseR003()
{
    int result = 0;

    std::string what("R003: FFT::apply() std::complex float for random n");
    
    int i = 0;
    const int iters = 15;
    
    for (i = 0; i < iters; ++i)
    {
	const int p = rand() % 16;
	const size_t n = (int) pow(2.0, p);

	Test.Message() << "Random FFT: std::complex, single precision, n = "
		       << n << std::endl;

	const Sequence<std::complex<float> > tdata(std::complex<float>(1.0, -2.0), n);
	Sequence<std::complex<float> > odata(0.0, n);

	udt* p_tdata = &odata;
	udt* p_fdata = 0;

	FFT fft;
	IFFT ifft;

	fft.apply(p_fdata, tdata);

	ifft.apply(p_tdata, *p_fdata);
	
	for (size_t i = 0; i < n; i++)
	{
	    result |= (std::abs(odata[i] - tdata[i]) < TOL ? 0 : 1);
	}
	
	Test.Check(result == 0)
	    << "FFT result is correct"
	    << what
	    << std::endl;

	delete p_fdata;
    }

    return result;
}

int
TestCaseC001()
{
    int result = 0;

    std::string what("C001: FFT::apply() float for n = 1048576");
    
    const size_t n = 1048576;
    float val = -5.0;

    Sequence<float> tdata(0.0, n);
    Sequence<std::complex< float > > fdata_data(val*val + 2.0*val + LDAS_PI, n);
    DFT< std::complex< float > >     fdata(fdata_data);
    udt*			     p_fdata(&fdata);

    FFT fft;
    
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 1: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 1: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }
    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 1: " << what
      << std::endl;


    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 2: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 2: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }
    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 2: " << what
      << std::endl;


    val = val*val + 1.0;
    tdata[0] = val;
    fft.apply(p_fdata, tdata);
    Test.Check(fdata.size() == n)
      << "FFT output is correct size"
      << ": Stage 3: " << what
      << std::endl;
    Test.Check(fdata.IsSymmetric() == true)
      << "FFT output has correct symmetry"
      << ": Stage 3: " << what
      << std::endl;
    
    for (size_t i = 0; i < n; i++)
    {
	result |= (std::abs(fdata[i] - val) < TOL ? 0 : 1);
    }

    Test.Check(result == 0)
      << "FFT result is correct"
      << ": Stage 3: " << what
      << std::endl;

    return result;
}

int
TestCaseC002()
{
    int result = 0;

    std::string what("C002: FFT::apply() double float for n = 0");
    
    Sequence<double> tdata;
    DFT< std::complex< double > >      fdata;
    udt*			       p_fdata(&fdata);

    FFT fft;
    
    fft.apply(p_fdata, tdata);

    return result;
}

void*
TestThread001(void*)
{
    int result = 0;

    try
    {
	result |= TestCaseA001();
	result |= TestCaseA002();

	result |= TestCaseR001();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Thread 1: Unhandled exception " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception " << std::endl;
    }

    return (void*)0;
}

void*
TestThread002(void*)
{
    int result = 0;

    try
    {
	result |= TestCaseB001();
	result |= TestCaseB002();

	result |= TestCaseR002();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Thread 2: Unhandled exception " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception " << std::endl;
    }

    return (void*)0;
}


void*
TestThread003(void*)
{
    int result = 0;

    try
    {
	result |= TestCaseC001();
	
	try {
	    result |= TestCaseC002();
	}
	catch(std::invalid_argument& e)
	{
	  Test.Check(true) << "Caught invalid_argument" << std::endl;
	}

	result |= TestCaseR003();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Thread 3: Unhandled exception " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception " << std::endl;
    }

    return (void*)0;
}

int
main(int argc, char** argv)
{
    int result = 0;
    void* status = 0;

    Test.Init(argc, argv);

    pthread_t thread1;
    pthread_t thread2;
    pthread_t thread3;

    pthread_create(&thread1, 0,
		   TestThread001, NULL);

    pthread_create(&thread2, 0,
		   TestThread002, NULL);

    pthread_create(&thread3, 0,
		   TestThread003, NULL);

    result = pthread_join(thread1, &status);
    Test.Check(result == 0) << "Thread 1 joined" << std::endl;
    Test.Check(status == 0) << "Thread 1 result ok" << std::endl;

    result = pthread_join(thread2, &status);
    Test.Check(result == 0) << "Thread 2 joined" << std::endl;
    Test.Check(status == 0) << "Thread 2 result ok" << std::endl;

    result = pthread_join(thread3, &status);
    Test.Check(result == 0) << "Thread 3 joined" << std::endl;
    Test.Check(status == 0) << "Thread 3 result ok" << std::endl;

    Test.Exit();

    return result;
}
