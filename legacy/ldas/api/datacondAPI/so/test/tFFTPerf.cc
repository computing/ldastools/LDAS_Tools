#include "datacondAPI/config.h"

#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <valarray>
#include <stdexcept>
#include <complex>

#include "general/unittest.h"

#include "DataCondAPIConstants.hh"

#include "fft.hh"
#include "ifft.hh"

#include "DFTUDT.hh"

using std::complex;
using namespace datacondAPI;

// Return processor time in seconds
inline
double
dtime()
{
  return (double)(clock())/((double)CLOCKS_PER_SEC);
}

// The single FFT object to use
FFT fft;

const int timing_iters = 10;

General::UnitTest Test;

// Amount of CPU time in which the calculation must complete
const double TOLERANCE(4.0);

//
// Time double complex FFT for a particular length n
//
void
TestDCFFT(const size_t n)
{
    datacondAPI::Sequence< complex<double> > fft_in(n);
    for (size_t i = 0; i < fft_in.size(); i++)
    {
	fft_in[i] = 2000.0*drand48() - 1000.0;
    }

    datacondAPI::udt*	fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    double start, end, fft_time;

    start = dtime();
    
    for (int i = 0; i < timing_iters; i++)
    {
	fft.apply(fft_out, fft_in);
    }

    end = dtime();

    fft_time = (end - start)/timing_iters;
    
    Test.Check(fft_time < TOLERANCE) << "FFT time for " << n 
			       << "\t" << fft_time << std::endl;
    delete fft_out;
}

//
// Time single complex FFT for a particular length n
//
void
TestSCFFT(const size_t n)
{
    datacondAPI::Sequence< complex<float> > fft_in(n);
    for (size_t i = 0; i < fft_in.size(); i++)
    {
	fft_in[i] = 2000.0*drand48() - 1000.0;
    }

    datacondAPI::udt*	fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    double start, end, fft_time;

    start = dtime();
    
    for (int i = 0; i < timing_iters; i++)
    {
	fft.apply(fft_out, fft_in);
    }

    end = dtime();

    fft_time = (end - start)/timing_iters;
    
    Test.Check(fft_time < TOLERANCE) << "FFT time for " << n 
			       << "\t" << fft_time << std::endl;
    delete fft_out;

}

//
// Time double real FFT for a particular length n
//
void
TestDRFFT(const size_t n)
{
    datacondAPI::Sequence<double> fft_in(n);
    for (size_t i = 0; i < fft_in.size(); i++)
    {
	fft_in[i] = 2000.0*drand48() - 1000.0;
    }

    datacondAPI::udt*	fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    double start, end, fft_time;

    start = dtime();
    
    for (int i = 0; i < timing_iters; i++)
    {
	fft.apply(fft_out, fft_in);
    }

    end = dtime();

    fft_time = (end - start)/timing_iters;
    
    Test.Check(fft_time < TOLERANCE) << "FFT time for " << n 
			       << "\t" << fft_time << std::endl;
    delete fft_out;
}

//
// Time single real FFT for a particular length n
//
void
TestSRFFT(const size_t n)
{
    datacondAPI::Sequence<float> fft_in(n);
    for (size_t i = 0; i < fft_in.size(); i++)
    {
	fft_in[i] = 2000.0*drand48() - 1000.0;
    }

    datacondAPI::udt*	fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    double start, end, fft_time;

    start = dtime();
    
    for (int i = 0; i < timing_iters; i++)
    {
	fft.apply(fft_out, fft_in);
    }

    end = dtime();

    fft_time = (end - start)/timing_iters;
    
    Test.Check(fft_time < TOLERANCE) << "FFT time for " << n 
			       << "\t" << fft_time << std::endl;
    delete fft_out;
}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);

    const size_t maxN = 1048576;

    try
    {
	// Powers of 2
	size_t n = 1;
	try {
	  Test.Message() << "Double precision complex" << std::endl;
	  while (n <= maxN)
	  {
	    TestDCFFT(n);
	    n *= 2;
	  }
	}
	catch(std::bad_alloc& e)
	{
	  Test.Check(true) << "Insufficient memory to do test of size "
			   << n
			   << std::endl;
	}

	try {
	  n = 1;
	  Test.Message() << "Single precision complex" << std::endl;
	  while (n <= maxN)
	  {
	    TestSCFFT(n);
	    n *= 2;
	  }
	}
	catch(std::bad_alloc& e)
	{
	  Test.Check(true) << "Insufficient memory to do test of size "
			   << n
			   << std::endl;
	}

	try {
	  n = 1;
	  Test.Message() << "Double precision real" << std::endl;
	  while (n <= maxN)
	  {
	    TestDRFFT(n);
	    n *= 2;
	  }
	}
	catch(std::bad_alloc& e)
	{
	  Test.Check(true) << "Insufficient memory to do test of size "
			   << n
			   << std::endl;
	}

	try {
	  n = 1;
	  Test.Message() << "Single precision real" << std::endl;
	  while (n <= maxN)
	  {
	    TestSRFFT(n);
	    n *= 2;
	  }
	}
	catch(std::bad_alloc& e)
	{
	  Test.Check(true) << "Insufficient memory to do test of size "
			   << n
			   << std::endl;
	}
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Unhandled exception" 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception" << std::endl;
    }

    Test.Exit();
}
