//
// tWindowUDT.cc:
//
// Test driver for WindowUDT class
//
//

#include "datacondAPI/config.h"

// system includes
#include <iostream>
#include <complex>
#include <string>
#include <fstream>
#include <stdexcept>

// other includes
#include <general/unittest.h>
#include <filters/LDASConstants.hh>

//#include "DataCondAPIConstants.hh"
#include "WindowUDT.hh"
#include "SequenceUDT.hh"

// the following tests are performed:
//
// test error exception in kaiser window constructor
// test error exception in kaiser window set_beta method
// test error exceptions in apply and operator() methods for all windows 
// test kaiser window set method with valid beta
// test mean and rms value for a trivial window (n == 0)
// test window values  (n == 1)
// test window values  (n == 10)
// test window values  (n == 11)
// test window applied to real random data (n == 1024)
// test window applied to complex random data (n == 10)

using namespace datacondAPI;
using namespace std;   

General::UnitTest Test;

//-----------------------------------------------------------------------
// read input data from a file 
// (returns length of input)
//
template<class T>
int readData(const char* fileName, 
	     T*          input)
{

  // attach source directory to file name
  std::string path;

  if (getenv("DATADIR"))
  {
    path  = getenv("DATADIR");
    path += "/";
  }
  path += fileName;

  std::ifstream inFile(path.c_str());

  // make sure that file exists
  if (inFile == 0) 
  {
    std::cout << "missing file: " << fileName << std::endl;
    abort();
  }

  int i = 0;
  while (1) 
  {
    inFile >> input[i]; // must try to read in data before generating an
                        // end-of-file flag.  works ok if file is empty.  
    if (inFile.eof()) 
    {
      return i;  
    }
    
    i++;
  }
}
//------------------------------------------------------------------------
// test error exceptions in kaiser window constructor (invalid beta)
//
template<class T>
bool testKaiserConstructorError(const std::string& name,
				const double  beta)
{
  bool pass = false;

  try
  {
    try
    {
      KaiserWindowUDT kw(beta);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << "Kaiser Window: " 
		       << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << "Kaiser Window: " 
		       << name 
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << "Kaiser Window: " 
		     << name 
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}
//------------------------------------------------------------------------
// test error exceptions in kaiser window set method (invalid beta)
//
template<class T>
bool testKaiserSetBetaError(const std::string& name,
			    const double  beta)
{
  bool pass = false;

  KaiserWindowUDT kw; // kaiser window with default value for beta

  try
  {
    try
    {
      kw.set_beta(beta);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << "Kaiser Window: " 
		       << name 
		       << " ("<< r.what() << ")" 
		       << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << "Kaiser Window: " 
		       << name 
		       << " (no exception thrown)" 
		       << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << "Kaiser Window: " 
		     << name 
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test error exception in real apply or operator() method 
//
template<class T>
bool testApplyErrorR(const std::string& name,
		     WindowUDT&    window,
		     void (WindowUDT::* pmf)(Sequence<T>&       out,
         				     const Sequence<T>& in),
		     const size_t  n)
{

  const Sequence<T> in(1, n);  // all one's
  Sequence<T> out;

  bool pass = false;
  
  try
  {
    (window.*pmf)(out, in);
    pass = false;
    Test.Check(pass) << window.what().name() << ": "
                     << name
                     << " (no exception thrown)" 
                     << std::endl;
  }
  catch (std::length_error& r)
  {
    pass = true;
    Test.Check(pass) << window.what().name() << ": "
                     << name 
                     << " ("<< r.what() << ")" 
                     << std::endl;
  }
  catch(std::exception& r)
  {
    Test.Check(pass) << window.what().name() << ": "
                     << name 
                     << " (unexpected exception: "<< r.what() << ")" 
                     << std::endl;
  }
  catch (...) 
  {
    Test.Check(pass) << window.what().name() << ": "
                     << name 
                     << " (unknown exception)" 
                     << std::endl;
  }

  return pass;
}

//------------------------------------------------------------------------
// test error exception in complex apply or operator() method 
//
template<class T>
bool testApplyErrorC(const std::string& name,
		     WindowUDT& window,
		     void (WindowUDT::* pmf)(Sequence<std::complex<T> >& out,
				       const Sequence<std::complex<T> >& in),
		     const size_t n)
{
  const Sequence<std::complex<T> > in(1, n);  // all one's
  Sequence<std::complex<T> > out;

  bool pass = false;

  try
  {
      (window.*pmf)(out, in);
      Test.Check(false) << window.what().name() << ": "
			<< name
			<< " (no exception thrown)" 
			<< std::endl;
  }
  catch (length_error& r)
  {
    Test.Check(true) << window.what().name() << ": "
		     << name 
		     << " (" << r.what() << ")" 
		     << std::endl;
    pass = true;
  }
  catch (exception& r) 
  {
    pass = false;
    Test.Check(pass) << window.what().name() << ": "
		     << name 
		     << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }
  catch (...) 
  {
    pass = false;
    Test.Check(pass) << window.what().name() << ": "
		     << name 
		     << " (unknown exception:)" 
		     << std::endl;
  }
  
  return pass;
}

//------------------------------------------------------------------------
// test error exceptions in constructors, apply, and operator() methods 
// for all windows
// (kaiser window beta parameter < 0 and n > MaximumWindowLength)
//
template<class T>
bool testErrors(void)
{
  const size_t n    = Filters::MaximumWindowLength + 1;
  const double beta = -2;

  bool allPass = true;

  // kaiser window constructors
  allPass = allPass && testKaiserConstructorError<T>("constructor",
						     beta);

  // kaiser window set_beta method
  allPass = allPass && testKaiserSetBetaError<T>("set method",
						 beta);

  // apply and operator() methods
  RectangularWindowUDT rw;
  allPass = allPass && testApplyErrorR<T>("real apply method",
					   rw,
					   &WindowUDT::apply,
					   n);

  allPass = allPass && testApplyErrorR<T>("real operator() method",
					   rw,
					   &WindowUDT::operator(),
					   n);

  allPass = allPass && testApplyErrorC<T>("complex apply method",
					   rw,
					   &WindowUDT::apply,
					   n);

  allPass = allPass && testApplyErrorC<T>("complex operator() method",
					   rw,
					   &WindowUDT::operator(),
					   n);

  HannWindowUDT hw;
  allPass = allPass && testApplyErrorR<T>("real apply method",
					  hw,
					  &WindowUDT::apply,
					  n);

  allPass = allPass && testApplyErrorR<T>("real operator() method",
					  hw,
					  &WindowUDT::operator(),
					  n);

  allPass = allPass && testApplyErrorC<T>("complex apply method",
					  hw,
					  &WindowUDT::apply,
					  n);
  
  allPass = allPass && testApplyErrorC<T>("complex operator() method",
					  hw,
					  &WindowUDT::operator(),
					  n);

  KaiserWindowUDT kw;
  allPass = allPass && testApplyErrorR<T>("real apply method",
					  kw,
					  &WindowUDT::apply,
					  n);

  allPass = allPass && testApplyErrorR<T>("real operator() method",
					  kw,
					  &WindowUDT::operator(),
					  n);


  allPass = allPass && testApplyErrorC<T>("complex apply method",
					  kw,
					  &WindowUDT::apply,
					  n);

  allPass = allPass && testApplyErrorC<T>("complex operator() method",
					  kw,
					  &WindowUDT::operator(),
					  n);

  return allPass;
}
//------------------------------------------------------------------------
// test kaiser window set method for valid beta
//
template <class T>
bool testValidKaiserSetBeta()
{
  bool pass = true;

  KaiserWindowUDT kw; // kaiser window with default beta parameter
  double beta = 3;    // valid beta parameter
  kw.set_beta(beta);  // change beta

  // check if beta parameter was changed
  if ( kw.beta() != beta )
  {
    pass = false;
  }
  Test.Check(pass) << "Kaiser Window: set method for valid beta" << std::endl;

  return pass;
}
//------------------------------------------------------------------------
// test windows acting on valid real data (n <= MaximumWindowLength)
//
template <class T>
bool testValidR(const std::string&      name,
		const Sequence<T>& in,
		Sequence<T>&       out,
		WindowUDT&         window,
		const WindowInfo   windowInfoExpected,
		const double       meanExpected,
		const double       rmsExpected,
		const Sequence<T>& outExpected)
{
  bool allPass = true;
  bool pass;

  double tolerance = 1e-5;

  // test window info object
  pass = true;
  if (window.what() != windowInfoExpected)
  {
    pass = false;
  }
  Test.Check(pass) << name << window.what().name() << ": " << "what" << std::endl;
  allPass = allPass && pass;

  // test output of apply method
  pass = true;
  window.apply(out,in);

  for (size_t i = 0; i < in.size(); i++)
  {
    // std::cout << "out[" << i << "] = " << out[i] << std::endl;
    if (abs(out[i] - outExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << name << window.what().name() << ": " << "real apply" << std::endl;
  allPass = allPass && pass;

  // test output of apply method
  pass = true;
  window.operator()(out,in);

  for (size_t i = 0; i < in.size(); i++)
  {
    // std::cout << "out[" << i << "] = " << out[i] << std::endl;
    if (abs(out[i] - outExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << name << window.what().name() << ": " << "real operator()" << std::endl;
  allPass = allPass && pass;

  // test window mean
  pass = true;
  if (abs(window.mean() - meanExpected) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << window.what().name() << ": " << "mean" << std::endl;
  allPass = allPass && pass;

  // test window rms
  pass = true;
  if (abs(window.rms() - rmsExpected) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << window.what().name() << ": " << "rms" << std::endl;
  allPass = allPass && pass;

  return allPass;
}
//------------------------------------------------------------------------
// test windows acting on valid complex data (n <= MaximumWindowLength)
//
template <class T>
bool testValidC(const std::string&                name,
		const Sequence<std::complex<T> >& in,
		Sequence<std::complex<T> >&       out,
		WindowUDT&                   window,
		const WindowInfo             windowInfoExpected,
		const double                 meanExpected,
		const double                 rmsExpected,
		const Sequence<std::complex<T> >& outExpected)
{
  bool allPass = true;
  bool pass;

  double tolerance = 1e-5;

  // test window info object
  pass = true;
  if (window.what() != windowInfoExpected)
  {
    pass = false;
  }
  Test.Check(pass) << name << window.what().name() << ": " << "what" << std::endl;
  allPass = allPass && pass;

  // test output of apply method
  pass = true;
  window.apply(out,in);

  for (size_t i = 0; i < in.size(); i++)
  {
    // std::cout << "out[" << i << "] = " << out[i] << std::endl;
    if (abs(out[i] - outExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << name << window.what().name() << ": " << "complex apply" << std::endl;
  allPass = allPass && pass;

  // test output of apply method
  pass = true;
  window.operator()(out,in);

  for (size_t i = 0; i < in.size(); i++)
  {
    // std::cout << "out[" << i << "] = " << out[i] << std::endl;
    if (abs(out[i] - outExpected[i]) > tolerance)
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << name << window.what().name() << ": " << "complex operator()" << std::endl;
  allPass = allPass && pass;

  // test window mean
  pass = true;
  if (abs(window.mean() - meanExpected) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << window.what().name() << ": " << "mean" << std::endl;
  allPass = allPass && pass;

  // test window rms
  pass = true;
  if (abs(window.rms() - rmsExpected) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << window.what().name() << ": " << "rms" << std::endl;
  allPass = allPass && pass;

  return allPass;
}

//------------------------------------------------------------------------
// test windows acting on valid in-place data
//
template <class T>
bool testInplaceValid(const std::string&      name,
		      WindowUDT&            window)
{
  bool allPass = true;
  bool pass = true;

  const Sequence<T> In(5.0, 100);
  Sequence<T> outExpected = In;
  Sequence<T> in = In;

  // Generate the expected output
  window.apply(outExpected, In);

  pass = true;

  // Apply in place
  window.apply(in, in);

  for (size_t i = 0; i < in.size(); i++)
  {
    if (in[i] != outExpected[i])
    {
      pass = pass && false;
    }
  }

  Test.Check(pass) << name << window.what().name() << ": " << "in-place apply" << std::endl;
  allPass = allPass && pass;

  // test output of apply method
  pass = true;

  // Reset the data
  in = In;

  window.operator()(in, in);

  for (size_t i = 0; i < in.size(); i++)
  {
    if (in[i] != outExpected[i])
    {
      pass = pass && false;
    }
  }
  Test.Check(pass) << name << window.what().name() << ": " << "in-place operator()" << std::endl;
  allPass = allPass && pass;

  return allPass;
}

//------------------------------------------------------------------------
// test apply methods on different sets of valid real and complex data
//
template <class T>
bool testAllValid()
{
  bool allPass = true;

  // parameters
  double     meanExpected;
  double     rmsExpected;
  WindowInfo windowInfoExpected;

  // windows
  RectangularWindowUDT rw;
  HannWindowUDT        hw;
  KaiserWindowUDT      kw;

  kw.set_beta(6); // beta = 6

  // real data --------------------------------------------------------------

  // (n == 0) ---------------------------
  Sequence<T> inR0;
  Sequence<T> outR0;
  Sequence<T> outR0Expected;

  meanExpected = 0.0;
  rmsExpected  = 0.0;

  // Rectangular window
  windowInfoExpected = WindowInfo("Rectangular Window");

  allPass = allPass && testValidR("(real data, n == 0): ",
				  inR0,
				  outR0,
				  rw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR0Expected);

  // Hann window
  windowInfoExpected = WindowInfo("Hann Window");

  allPass = allPass && testValidR("(real data, n == 0): ",
				  inR0,
				  outR0,
				  hw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR0Expected);

  // Kaiser window
  windowInfoExpected = WindowInfo("Kaiser Window", 6);

  allPass = allPass && testValidR("(real data, n == 0): ",
				  inR0,
				  outR0,
				  kw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR0Expected);

  // (n == 1) ---------------------------
  Sequence<T> inR1(1,1);
  Sequence<T> outR1;
  Sequence<T> outR1Expected(1,1);

  meanExpected = 1.0;
  rmsExpected  = 1.0;

  // Rectangular window
  windowInfoExpected = WindowInfo("Rectangular Window");

  allPass = allPass && testValidR("(real data, n == 1): ",
				  inR1,
				  outR1,
				  rw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR1Expected);

  // Hann window
  windowInfoExpected = WindowInfo("Hann Window");

  allPass = allPass && testValidR("(real data, n == 1): ",
				  inR1,
				  outR1,
				  hw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR1Expected);

  // Kaiser window
  windowInfoExpected = WindowInfo("Kaiser Window", 6);

  allPass = allPass && testValidR("(real data, n == 1): ",
				  inR1,
				  outR1,
				  kw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR1Expected);


  // (n == 10) ----------------------------
  Sequence<T> inR10(1,10); // all ones
  Sequence<T> outR10;

  // Rectangular window
  //
  windowInfoExpected = WindowInfo("Rectangular Window");

  meanExpected = 1.0;
  rmsExpected  = 1.0;
  Sequence<T> outR10ExpectedRW(1,10); // all ones

  allPass = allPass && testValidR("(real data, n == 10): ",
				  inR10,
				  outR10,
				  rw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR10ExpectedRW);


  // Hann window
  // results from matlab using oppenheim and schafer definition
  // of hann window (see page 468, 2nd edition) 
  // :NOTE: matlab definition of hann window is different from O&P 
  //
  windowInfoExpected = WindowInfo("Hann Window");

  meanExpected = 0.45;
  rmsExpected  = 0.580947501931113;

  T dataR10ExpectedHW[] = { 0,
			    0.116977778440511,
			    0.413175911166535,
			    0.75,
			    0.969846310392954,
			    0.969846310392954,
			    0.75,
			    0.413175911166535,
			    0.116977778440511,
			    0 };

  Sequence<T> outR10ExpectedHW(dataR10ExpectedHW,10);

  allPass = allPass && testValidR("(real data, n == 10): ",
				  inR10,
				  outR10,
				  hw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR10ExpectedHW);


  // Kaiser window
  // results from matlab
  //
  windowInfoExpected = WindowInfo("Kaiser Window", 6);

  meanExpected = 0.450540717710873;
  rmsExpected  = 0.574518317168563;

  T dataR10ExpectedKW[] = { 0.0148733371047632,
			    0.138110273076192,
			    0.401142142059005,
			    0.731895415397821,
			    0.966682420916582,
			    0.966682420916582,
			    0.731895415397821,
			    0.401142142059005,
			    0.138110273076192,
			    0.0148733371047632 };

  Sequence<T> outR10ExpectedKW(dataR10ExpectedKW,10);

  allPass = allPass && testValidR("(real data, n == 10): ",
				  inR10,
				  outR10,
				  kw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR10ExpectedKW);


  // (n == 11) ----------------------------
  Sequence<T> inR11(1,11); // all ones
  Sequence<T> outR11;

  // Rectangular window
  //
  windowInfoExpected = WindowInfo("Rectangular Window");

  meanExpected = 1.0;
  rmsExpected  = 1.0;

  Sequence<T> outR11ExpectedRW(1,11); // all ones

  allPass = allPass && testValidR("(real data, n == 11): ",
				  inR11,
				  outR11,
				  rw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR11ExpectedRW);


  // Hann window
  // results from matlab using oppenheim and schafer definition
  // of hann window (see page 468, 2nd edition) 
  // :NOTE: matlab definition of hann window is different from O&P 
  //
  windowInfoExpected = WindowInfo("Hann Window");

  meanExpected = 0.454545454545455;
  rmsExpected  = 0.583874208121142;

  T dataR11ExpectedHW[] =  { 0,
			     0.0954915028125263,
			     0.345491502812526,
			     0.654508497187474,
			     0.904508497187474,
			     1,
			     0.904508497187474,
			     0.654508497187474,
			     0.345491502812526,
			     0.0954915028125264,
			     0 };

  Sequence<T> outR11ExpectedHW(dataR11ExpectedHW,11);

  allPass = allPass && testValidR("(real data, n == 11): ",
				  inR11,
				  outR11,
				  hw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR11ExpectedHW);


  // Kaiser window
  // results from matlab
  //
  windowInfoExpected = WindowInfo("Kaiser Window", 6);

  meanExpected = 0.455123691551487;
  rmsExpected  = 0.577412699379494;

  T dataR11ExpectedKW[] = { 0.0148733371047632,
			    0.119398458439033,
			    0.339018056648885,
			    0.634490267147639,
			    0.89540018419286,
			    1,
			    0.89540018419286,
			    0.634490267147639,
			    0.339018056648885,
			    0.119398458439033,
			    0.0148733371047632 };

  Sequence<T> outR11ExpectedKW(dataR11ExpectedKW,11);

  allPass = allPass && testValidR("(real data, n == 11): ",
				  inR11,
				  outR11,
				  kw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR11ExpectedKW);


  // (n == 1024, random input data) ----------------------------------------
  // read input data from a file
  T dataR1024[1024];
  int count = readData<T>("tWindowIn.dat",dataR1024);

  if ( count != ( sizeof( dataR1024 ) / sizeof( *dataR1024 ) ) )
  {
    allPass = false;
  }

  Sequence<T> inR1024(dataR1024,1024); 
  Sequence<T> outR1024;

  // Rectangular window
  //
  windowInfoExpected = WindowInfo("Rectangular Window");

  meanExpected = 1.0;
  rmsExpected  = 1.0;

  Sequence<T> outR1024ExpectedRW = inR1024; // ou = in for rectangular window

  allPass = allPass && testValidR("(real random data, n == 1024): ",
				  inR1024,
				  outR1024,
				  rw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR1024ExpectedRW);

  allPass = allPass
      && testInplaceValid<T>("(in-place apply, real data):", rw);

  // Hann window
  // results from matlab using oppenheim and schafer definition
  // of hann window (see page 468, 2nd edition) 
  // :NOTE: matlab definition of hann window is different from O&P 
  //
  windowInfoExpected = WindowInfo("Hann Window");

  meanExpected = 0.49951171875;
  rmsExpected  = 0.612073352681277;

  // expected output needs to be read in from a file
  T dataR1024ExpectedHW[1024];
  count = readData<T>("tWindowOutHW.dat",dataR1024ExpectedHW);

  if ( count != ( sizeof( dataR1024ExpectedHW ) / sizeof( *dataR1024ExpectedHW ) ) )
  {
    allPass = false;
  }

  Sequence<T> outR1024ExpectedHW(dataR1024ExpectedHW,1024);

  allPass = allPass && testValidR("(real random data, n == 1024): ",
				  inR1024,
				  outR1024,
				  hw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR1024ExpectedHW);

  allPass = allPass
      && testInplaceValid<T>("(in-place apply, real data):", hw);

  // Kaiser window
  // results from matlab
  //
  windowInfoExpected = WindowInfo("Kaiser Window", 6);

  meanExpected = 0.499550765589313;
  rmsExpected  = 0.605297408862981;

  // expected output needs to be read in from a file
  T dataR1024ExpectedKW[1024];
  count = readData<T>("tWindowOutKW.dat",dataR1024ExpectedKW);

  if ( count != ( sizeof( dataR1024ExpectedKW ) / sizeof( *dataR1024ExpectedKW ) ) )
  {
    allPass = false;
  }

  Sequence<T> outR1024ExpectedKW(dataR1024ExpectedKW,1024);

  allPass = allPass && testValidR("(real random data, n == 1024): ",
				  inR1024,
				  outR1024,
				  kw,
				  windowInfoExpected,
				  meanExpected,
				  rmsExpected,
				  outR1024ExpectedKW);
  allPass = allPass
      && testInplaceValid<T>("(in-place apply, real data):", kw);


  // complex data ----------------------------------------------------------

  // (n == 10, random data) ---------------------
  std::complex<T> dataC[] = { std::complex<T>(-4.8, 7.6), 
		         std::complex<T>( 5.9,-2.0), 
		         std::complex<T>( 8.2,-9.6), 
		         std::complex<T>(-4.6,-8.3),
		         std::complex<T>(-8.7, 3.0), 
		         std::complex<T>( 0.9,-2.7), 
		         std::complex<T>( 6.4, 3.9),  
		         std::complex<T>(-6.1,-5.0),
		         std::complex<T>( 5.8, 1.8),  
		         std::complex<T>(-0.8, 5.0) };

  Sequence<std::complex<T> > inC(dataC,10);
  Sequence<std::complex<T> > outC;

  // Rectangular window
  //
  windowInfoExpected = WindowInfo("Rectangular Window");

  meanExpected = 1.0;
  rmsExpected  = 1.0;

  Sequence<std::complex<T> > outCExpectedRW = inC; // out = in for rectangular

  allPass = allPass && testValidC("(complex random data, n == 10): ",
                                  inC,
                                  outC,
                                  rw,
                                  windowInfoExpected,
                                  meanExpected,
                                  rmsExpected,
                                  outCExpectedRW);
  allPass = allPass
      && testInplaceValid<std::complex<T> >("(in-place apply, complex data):", rw);


  // Hann window
  // results from matlab using oppenheim and schafer definition
  // of hann window (see page 468, 2nd edition) 
  // :NOTE: matlab definition of hann window is different from O&P 
  //
  windowInfoExpected = WindowInfo("Hann Window");

  meanExpected = 0.45;
  rmsExpected  = 0.580947501931113;

  std::complex<T> dataCExpectedHW[] = 
  { std::complex<T>(0, 0),                         
    std::complex<T>(0.690168892799015, -0.233955556881022),
    std::complex<T>(3.38804247156558, -3.96648874719873),
    std::complex<T>(-3.45, -6.225),
    std::complex<T>(-8.4376629004187, 2.90953893117886),
    std::complex<T>(0.872861679353659, -2.61858503806098),
    std::complex<T>(4.8, 2.925),
    std::complex<T>(-2.52037305811586, -2.06587955583268),
    std::complex<T>(0.678471114954964, 0.21056000119292),
    std::complex<T>(0, 0) };

  Sequence<std::complex<T> > outCExpectedHW(dataCExpectedHW,10);

  allPass = allPass && testValidC("(complex random data, n == 10): ",
                                  inC,
                                  outC,
                                  hw,
                                  windowInfoExpected,
                                  meanExpected,
                                  rmsExpected,
                                  outCExpectedHW);

  allPass = allPass
      && testInplaceValid<std::complex<T> >("(in-place apply, complex data):", hw);

  // Kaiser window
  // results from matlab
  //
  windowInfoExpected = WindowInfo("Kaiser Window", 6);

  meanExpected = 0.450540717710873;
  rmsExpected  = 0.574518317168563;

  std::complex<T> dataCExpectedKW[] = 
  { std::complex<T>(-0.0713920181028634, 0.1130373619962),
    std::complex<T>(0.814850611149534, -0.276220546152385),
    std::complex<T>(3.28936556488384, -3.85096456376645),
    std::complex<T>(-3.36671891082997, -6.07473194780191),
    std::complex<T>(-8.41013706197426, 2.90004726274975),
    std::complex<T>(0.870014178824924, -2.61004253647477),
    std::complex<T>(4.68413065854605, 2.8543921200515),
    std::complex<T>(-2.44696706655993, -2.00571071029503),
    std::complex<T>(0.801039583841915, 0.248598491537146),
    std::complex<T>(-0.0118986696838106, 0.074366685523816) };

  Sequence<std::complex<T> > outCExpectedKW(dataCExpectedKW,10);

  allPass = allPass && testValidC("(complex random data, n == 10): ",
                                  inC,
                                  outC,
                                  kw,
                                  windowInfoExpected,
                                  meanExpected,
                                  rmsExpected,
                                  outCExpectedKW);

  allPass = allPass
      && testInplaceValid<std::complex<T> >("(in-place apply, complex data):", kw);

  return allPass;
}
//------------------------------------------------------------------------
//
int main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);

  if (Test.IsVerbose())
  { 
    std::cout << "$Id: tWindowUDT.cc,v 1.5 2005/12/01 22:55:03 emaros Exp $" << std::endl << std::endl;
  }

  // test all error exceptions
  //
  Test.Check(testErrors<float>()) << "(all error exception tests <float>)" 
				  << std::endl;

  Test.Check(testErrors<double>()) << "(all error exception tests <double>)" 
				   << std::endl;


  // test kaiser window set method for valid beta
  //
  Test.Check(testValidKaiserSetBeta<float>()) << "(kaiser window set method for valid beta <float>)"
					      << std::endl;

  Test.Check(testValidKaiserSetBeta<double>()) << "(kaiser window set method for valid beta <double>)"
					      << std::endl;


  // test apply methods on valid real and complex data
  //
  Test.Check(testAllValid<float>()) << "(apply and operator() methods on valid data <float>)" 
				    << std::endl;

  Test.Check(testAllValid<double>()) << "(apply and operator() methods on valid data <double>)" 
				    << std::endl;

  // all done!!

  Test.Exit();

  return 0;
}
