//! author="Edward Maros"
// Test code for timing functions
// $Id: tkn_timing.cc,v 1.8 2005/12/01 22:55:03 emaros Exp $

#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

#include "CallChain.hh"
#include "ScalarUDT.hh"

General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
  int main(int argc, char** argv);
#ifdef __cplusplus
	  }
#endif	/* __cplusplus */

int
main(int argc, char** argv)
{
  try {
    CallChain	cmds;
    Parameters	args;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "8192"), "xsize");
    cmds.AppendCallFunction("dcomplex", args.set(2, "1.0", "1.0"), "init");
    cmds.AppendCallFunction("double", args.set(1, "1.0"), "binit");
    cmds.AppendCallFunction("integer", args.set(1, "13"), "bsize");
    cmds.AppendCallFunction("zvalarray", args.set(2, "init", "xsize"), "x");
    cmds.AppendCallFunction("dvalarray", args.set(2, "binit", "bsize"), "a");
    cmds.AppendCallFunction("dvalarray", args.set(2, "binit", "bsize"), "b");
    
    cmds.AppendCallFunction("user_time", args.set(0), "");
    for (size_t x = 0; x < 32; x++)
    {
      cmds.AppendCallFunction("linfilt", args.set(3, "b", "a", "x"), "");
    }
    cmds.AppendCallFunction("user_time", args.set(0), "time");

    cmds.AppendIntermediateResult( "time", "result", "Final Result", "" );
    
    cmds.Execute();

    double result(datacondAPI::udt::Cast< datacondAPI::Scalar<double> >
		  (*(cmds.GetSymbol("time"))).GetValue());

    Test.Message() << "user time: " << result << std::endl;
  }
  CATCH(Test);
  Test.Exit();
  return 0;
}
