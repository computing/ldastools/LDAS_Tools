//tResolver_tseries.cc

#include "datacondAPI/config.h"

#include <vector>
#include <valarray>

#include "general/unittest.h"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WaveletPSU.hh"
#include "HaarPSU.hh"
#include "resolver.hh"
#include "result.hh"
#include "general/gpstime.hh"

using namespace std;   
using namespace datacondAPI::psu;

void TestOne();
void TestTwo();

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message() << "Executing TestOne." << endl;
  TestOne();

  Test.Message() << "Executing TestTwo." << endl;
  TestTwo();
  
  Test.Check(true) << "End of test harness reached." << endl;

  Test.Exit();

} //end main


void TestOne()
{
  datacondAPI::Sequence<double> data(0.0, 8);
  data[2] = 1.0;

  const double srate = 2048.0;
  const General::GPSTime StartTime(10000, 0);

  datacondAPI::TimeSeries<double> tdata(data, 1.0 / 100.0);
  tdata.SetName("tdata");
  tdata.SetStartTime(StartTime);
  tdata.SetSampleRate(srate);

  Haar haarWavelet;
  
  Resolver test(haarWavelet, 4);
 
  //test Resolver function called getLevels
  Test.Message() << "Testing Resolver::getLevels." << endl;
  int levelsSet = test.getLevels();
  Test.Message() << "Levels set to " << levelsSet << endl;

  bool levelCheck = false;
  if (levelsSet == 4)
    levelCheck = true;
  Test.Check(levelCheck) << "Levels set." << endl;

  //test Resolver function called setLevels
  Test.Message() << "Testing Resolver::setLevels." << endl;
  test.setLevels(2);
  levelsSet = test.getLevels();
  Test.Message() << "Levels reset to " << levelsSet << endl;

  levelCheck = false;
  if (levelsSet == 2)
    levelCheck = true;
  Test.Check(levelCheck) << "Levels reset." << endl;

  Result<double> testResult;
  datacondAPI::udt* ptestResult = &testResult;

  //test Resolver function called applyFilter
  Test.Message() << "Testing Resolver::apply." << endl;
  try{
    test.apply(ptestResult, tdata);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //test Result function called getLevels:
  Test.Message() << "Testing Result::getLevels." << endl;
  int resultLevels = testResult.getLevels();
  Test.Message() << "Levels of decomposition = "
		 << resultLevels << endl;
  levelCheck = false;
  if (resultLevels == 2)
    levelCheck = true;
  Test.Check(levelCheck) << "Result has correct number of levels"
			 << " of decomposition." << endl;

  //check filtered signal
  datacondAPI::TimeSeries<double> first;
  datacondAPI::udt* pfirst = &first;
  datacondAPI::Scalar<int> level0(0);
  try
    {
      testResult.getResult(pfirst, level0);
    }

  catch(exception &e)
    {
      Test.Check(false) << e.what() << endl;
      exit(1);
    } 

  datacondAPI::TimeSeries<double> second;
  datacondAPI::udt* psecond = &second;
  datacondAPI::Scalar<int> level1(1);
  try
    {
      testResult.getResult(psecond, level1);
    }

  catch(exception &e)
    {
      Test.Check(false) << e.what() << endl;
      exit(1);
    }

  datacondAPI::TimeSeries<double> lowpass;
  datacondAPI::udt* plowpass = &lowpass;
  datacondAPI::Scalar<int> lowpassnum(2);
  try
    {
      testResult.getResult(plowpass, lowpassnum);
    }

  catch(exception &e)
    {
      Test.Check(false) << e.what() << endl;
      exit(1);
    }
  

  Test.Message() << "First decomposition:" << endl;
  datacondAPI::Sequence<double> s_first(first);
  for (unsigned int i = 0; i < s_first.size(); i++)
    Test.Message() << s_first[i] << endl;

  Test.Message() << "Second decomposition:" << endl;
  datacondAPI::Sequence<double> s_second(second);
  for (unsigned int j = 0; j < s_second.size(); j++)
    Test.Message() << s_second[j] << endl;

  Test.Message() << "Final lowpass result:" << endl;
  datacondAPI::Sequence<double> s_lowpass(lowpass);
  for (unsigned int k = 0; k < s_lowpass.size(); k++)
    Test.Message() << s_lowpass[k] << endl;

  //expected output
  datacondAPI::Sequence<double> e_first(0.0, 4);
  e_first[1] = 1.0;
  datacondAPI::Sequence<double> e_second(0.0, 2);
  e_second[1] = -1.0;
  datacondAPI::Sequence<double> e_lowpass(0.0, 2);
  e_lowpass[1] = 1.0;
  /*
  Test.Message() << "Expected first decomposition:" << endl;
  for (unsigned int i = 0; i < e_first.size(); i++)
    Test.Message() << e_first[i] << endl;

  Test.Message() << "Expected second decomposition:" << endl;
  for (unsigned int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (unsigned int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;
  */
  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_first - s_first);
  datacondAPI::Sequence<double> second_difference(e_second - s_second);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - s_lowpass);

  Test.Message() << "Difference in first decomposition:" << endl;
  for (unsigned int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in second decomposition:" << endl;
  for (unsigned int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (unsigned int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_first(e_first == s_first);
  std::valarray<bool> check_second(e_second == s_second);
  std::valarray<bool> check_lowpass(e_lowpass == s_lowpass);

  bool pass1, pass2, pass3;
  pass1 = pass2 = pass3 = true;

  for (unsigned int i = 0; i < check_first.size(); i++)
    pass1 = pass1 && check_first[i];

  for (unsigned int i = 0; i < check_second.size(); i++)
    pass2 = pass2 && check_second[i];

  for (unsigned int i = 0; i < check_lowpass.size(); i++)
    pass3 = pass3 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true)
    overallpass = true;

  Test.Check(overallpass) << "Filtered output." <<endl;

}//end TestOne.


void TestTwo()
{
  datacondAPI::Sequence<double> data(0.0, 48);
  data[3] = 1.0;

  const double srate = 2048.0;
  const General::GPSTime upStartTime(10000, 0);

  datacondAPI::TimeSeries<double> tdata(data, 1.0 / 100.0);
  tdata.SetName("tdata");
  tdata.SetStartTime(upStartTime);
  tdata.SetSampleRate(srate);

  Haar haarWavelet;
  Resolver test;
  Result<double> testResult;
  datacondAPI::udt* ptestResult = &testResult;

  Test.Message() << "Testing Resolver::isReady." << endl;
  bool ready_check = false;
  ready_check = test.isReady();

  if (ready_check == false)
  {
    Test.Message() << "Wavelet and/or levels were not set." << endl;
    test.setWavelet(haarWavelet);
    Test.Message() << "setWavelet executed." << endl;
    test.setLevels(4);
    Test.Message() << "setLevels executed." << endl;
    test.getWavelet();
    Test.Message() << "getWavelet executed." << endl;
  }
  ready_check = test.isReady();

  Test.Check(ready_check) << "isReady" << endl;

  Resolver test2(haarWavelet, 4);
  Test.Message()<< "Applying filter." << endl;
  try
    {
      test2.apply(ptestResult, tdata);
    }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //test Result function called getLevels:
  Test.Message() << "Testing Result::getLevels." << endl;
  int resultLevels = testResult.getLevels();
  Test.Message() << "Levels of decomposition = "
		 << resultLevels << endl;
  bool levelCheck = false;
  if (resultLevels == 4)
    levelCheck = true;
  Test.Check(levelCheck) << "Result has correct number of levels"
			 << " of decomposition." << endl;

  //check filtered signal
  datacondAPI::TimeSeries<double> zero;
  datacondAPI::udt* pzero = &zero;
  datacondAPI::Scalar<int> level0(0);
  testResult.getResult(pzero, level0);

  datacondAPI::TimeSeries<double> one;
  datacondAPI::udt* pone = &one;
  datacondAPI::Scalar<int> level1(1);
  testResult.getResult(pone, level1);

  datacondAPI::TimeSeries<double> two;
  datacondAPI::udt* ptwo = &two;
  datacondAPI::Scalar<int> level2(2);
  testResult.getResult(ptwo, level2);

  datacondAPI::TimeSeries<double> three;
  datacondAPI::udt* pthree = &three;
  datacondAPI::Scalar<int> level3(3);
  testResult.getResult(pthree, level3);

  datacondAPI::TimeSeries<double> lowpass;
  datacondAPI::udt* plowpass = &lowpass;
  datacondAPI::Scalar<int> lowpassnum(4);
  testResult.getResult(plowpass, lowpassnum);

  Test.Message() << "Zeroth decomposition:" << endl;
  datacondAPI::Sequence<double> s_zero(zero);
  for (unsigned int i = 0; i < s_zero.size(); i++)
    Test.Message() << s_zero[i] << endl;

  Test.Message() << "First decomposition:" << endl;
  datacondAPI::Sequence<double> s_one(one);
  for (unsigned int j = 0; j < s_one.size(); j++)
    Test.Message() << s_one[j] << endl;

  Test.Message() << "Second decomposition:" << endl;
  datacondAPI::Sequence<double> s_two(two);
  for (unsigned int j = 0; j < s_two.size(); j++)
    Test.Message() << s_two[j] << endl;

  Test.Message() << "Third decomposition:" << endl;
  datacondAPI::Sequence<double> s_three(three);
  for (unsigned int j = 0; j < s_three.size(); j++)
    Test.Message() << s_three[j] << endl;

  Test.Message() << "Final lowpass result:" << endl;
  datacondAPI::Sequence<double> s_lowpass(lowpass);
  for (unsigned int k = 0; k < s_lowpass.size(); k++)
    Test.Message() << s_lowpass[k] << endl;

  //expected output
  datacondAPI::Sequence<double> e_zero(0.0, 24);
  e_zero[2] = -1.0;
  datacondAPI::Sequence<double> e_first(0.0, 12);
  e_first[1] = 1.0;
  datacondAPI::Sequence<double> e_second(0.0, 6);
  e_second[1] = -1.0;
  datacondAPI::Sequence<double> e_third(0.0, 3);
  e_third[1] = -1.0;
  datacondAPI::Sequence<double> e_lowpass(0.0, 3);
  e_lowpass[1] = 1.0;
  /*
  Test.Message() << "Expected zeroth decomposition:" << endl;
  for (unsigned int i = 0; i < e_zero.size(); i++)
    Test.Message() << e_zero[i] << endl;

  Test.Message() << "Expected first decomposition:" << endl;
  for (unsigned int j = 0; j < e_first.size(); j++)
    Test.Message() << e_first[j] << endl;

  Test.Message() << "Expected second decomposition:" << endl;
  for (unsigned int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected third decomposition:" << endl;
  for (unsigned int j = 0; j < e_third.size(); j++)
    Test.Message() << e_third[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (unsigned int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;
  */
  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_zero - s_zero);
  datacondAPI::Sequence<double> second_difference(e_first - s_one);
  datacondAPI::Sequence<double> third_difference(e_second - s_two);
  datacondAPI::Sequence<double> fourth_difference(e_third - s_three);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - s_lowpass);

  Test.Message() << "Difference in zeroth decomposition:" << endl;
  for (unsigned int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in first decomposition:" << endl;
  for (unsigned int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in second decomposition:" << endl;
  for (unsigned int j = 0; j < third_difference.size(); j++)
    Test.Message() << third_difference[j] << endl;

  Test.Message() << "Difference in third decomposition:" << endl;
  for (unsigned int j = 0; j < fourth_difference.size(); j++)
    Test.Message() << fourth_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (unsigned int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_zero(e_zero == s_zero);
  std::valarray<bool> check_one(e_first == s_one);
  std::valarray<bool> check_two(e_second == s_two);
  std::valarray<bool> check_three(e_third == s_three);
  std::valarray<bool> check_lowpass(e_lowpass == s_lowpass);

  bool pass1, pass2, pass3, pass4, pass5;
  pass1 = pass2 = pass3 = pass4 = pass5 = true;

  for (unsigned int i = 0; i < check_zero.size(); i++)
    pass1 = pass1 && check_zero[i];

  for (unsigned int i = 0; i < check_one.size(); i++)
    pass2 = pass2 && check_one[i];

  for (unsigned int i = 0; i < check_two.size(); i++)
    pass3 = pass3 && check_two[i];

  for (unsigned int i = 0; i < check_three.size(); i++)
    pass4 = pass4 && check_three[i];

  for (unsigned int i = 0; i < check_lowpass.size(); i++)
    pass5 = pass5 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true
     && pass4 == true && pass5 == true)
    overallpass = true;

  Test.Check(overallpass) << "Filtered output." <<endl;

}//end TestTwo.
