#include "datacondAPI/config.h"

#include <ScalarUDT.hh>
#include <SequenceUDT.hh>

#include "general/unittest.h"

#include "token.hh"

#include "LineRemover.hh"
#include "random.hh"
#include "TimeSeries.hh"

using namespace datacondAPI;

General::UnitTest Test;

static gasdevstate seed;

void gasdev(std::valarray<float>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
        x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<double>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

void gasdev(std::valarray<std::complex<float> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
        x[i] = std::complex<float>(gasdev(seed), gasdev(seed));
    }
}

void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
        x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}


double sqn(const std::valarray<double>& a)
{
    return sqrt(std::valarray<double>(a * a).sum() / a.size());
}

void test_specific()
{   /*
    CallChain commands;
    Parameters arguments;

    commands.Reset();

    // establish environment

    s<double> u(2048);
    gasdev(u);
    commands.AddSymbol("u0", );
    commands.AddSymbol("u1", new Sequence<double>(u));
    commands.AddSymbol("u2", 

    std::valarray<double> y(100);
    gasdev(y);
    commands.AddSymbol("y", new Sequence<double>(y));

    commands.AddSymbol("a", new Scalar<double>(0.5));
    commands.AddSymbol("b", new Scalar<double>(0.1));

    commands.AppendCallFunction("oelslr", arguments.set(4, "y", "u", "a", "b"), "w1");

    // commands.AppendCallFunction("oelslr", arguments.set(5, "y", "u", "a", "b", "z"), "w2");

    // commands.AppendCallFunction("oelslr", arguments.set(2, "u", "z"), "w3");

    TimeSeries<double> tsu;
    tsu.resize(16384);
    gasdev(tsu);
    for (unsigned int i = 0; i < tsu.size(); ++i)
    {
	tsu[i] += 10000.0 * sin(i * 3.1415 * 2.0 * 60.0 / 16384); // base
	tsu[i] += 1000.0 * sin(3.0 * i * 3.1415 * 2.0 * 60.0 / 16384); // harmonic
    }
    tsu.SetSampleRate(16384.0);
    commands.AddSymbol("tsu", new TimeSeries<double>(tsu));

    TimeSeries<double> tsy;
    tsy.resize(16384);
    gasdev(tsy);
    for (unsigned int i = 0; i < tsu.size(); ++i)
    {
	tsy[i] += 100.0 * sin(i * 3.1415 * 2.0 * 60.0 / 16384); // base
	tsy[i] += 100.0 * sin(3.0 * i * 3.1415 * 2.0 * 60.0 / 16384); // harmonic
    }
    tsy.SetSampleRate(16384.0);
    commands.AddSymbol("tsy", new TimeSeries<double>(tsy));

    commands.AddSymbol("tsf", new Scalar<double>(60.0));
    commands.AddSymbol("tsd", new Scalar<double>(1.0));
    commands.AddSymbol("tsn", new Scalar<int>(10));

    commands.AppendCallFunction("oelslr", arguments.set(5, "tsy", "tsu", "tsf", "tsd", "tsn"), "tsw");
    
    commands.AppendIntermediateResult("tsw", "result", "Final Result", "" );

    Test.Message() << "commands.Execute();\n";

    commands.Execute();

    Test.Check(true) << "commands.Execute(); (completed) \n";

    // locals

    LineRemover lr;
    lr.addLine(0.5, 0.1);
    lr.refine(y, u);

    std::valarray<double> w1;
    lr.apply(w1, u);

    std::valarray<double> w2;
    lr.apply(w2, u);

    udt* p_w1 = commands.GetSymbol("w1");
    Test.Check(p_w1) << "p_w1 != 0\n";
    Test.Check(udt::IsA<Sequence<double> >(*p_w1)) << "p_w1 IsA Sequence<double>\n";
    *//*
    udt* p_w2 = commands.GetSymbol("w2");
    Test.Check(p_w2) << "p_w2 != 0\n";
    Test.Check(udt::IsA<Sequence<double> >(*p_w2)) << "p_w2 IsA Sequence<double>\n";

    udt* p_w3 = commands.GetSymbol("w3");
    Test.Check(p_w3) << "p_w3 != 0\n";
    Test.Check(udt::IsA<Sequence<double> >(*p_w3)) << "p_w3 IsA Sequence<double>\n";
    *//*
    udt* p_tsw = commands.GetSymbol("tsw");
    Test.Check(p_tsw) << "p_tsw != 0\n";
    Test.Check(udt::IsA<TimeSeries<double> >(*p_tsw)) << "p_tsw IsA TimeSeries<double>\n";
    TimeSeries<double>& tsw(udt::Cast<TimeSeries<double> >(*p_tsw));

    Test.Message() << "rms(tsu) = " << sqn(tsu) << '\n';
    Test.Message() << "rms(tsy) = " << sqn(tsy) << '\n';
    Test.Message() << "rms(tsw) = " << sqn(tsw) << '\n';
    Test.Message() << "rms(tsy - tsw) = " << sqn(tsy - tsw) << '\n';
    Test.Check( sqn(tsy - tsw) < sqn(tsy) ) << "sqn(tsy - tsw) < sqn(tsy)\n";
    */
}

template<typename type>
void test_generic(const std::string& name)
{
    Test.Message() << "name" << std::endl;
    {
        CallChain chain;
        Parameters parameters;

        Sequence<type> u(2048);
        gasdev(u);
        chain.AddSymbol("u", u.Clone());
        chain.AddSymbol("half", Scalar<double>(0.5).Clone());
        chain.AddSymbol("four", Scalar<int>(4).Clone());
        chain.AddSymbol("eight", Scalar<int>(8).Clone());

        chain.AppendCallFunction("oelslr", parameters.set(5, "u", "u", "half", "four", "eight"), "z");
        chain.AppendCallFunction("oelslr", parameters.set(3, "u", "u", "z"), "z");
        chain.AppendCallFunction("oelslr", parameters.set(2, "u", "z"), "y0");
        chain.AppendCallFunction("oelslr", parameters.set(2, "u", "z"), "y1");
        chain.AppendIntermediateResult("u","u","u","");

        chain.Execute();
 
        LineRemover lr(0.5, 4, 8);
        lr.refine(u, u);
        lr.refine(u, u);
        Sequence<type> y0, y1;
        lr.apply(y0, u);
        lr.apply(y1, u);

        Test.Check(y0.size() == dynamic_cast<Sequence<type>&>(*chain.GetSymbol("y0")).size()) << "sanity" << std::endl;
        Test.Check(y1.size() == dynamic_cast<Sequence<type>&>(*chain.GetSymbol("y1")).size()) << "sanity" << std::endl;
       
    }
}

int main(int argc, char* argv[])
try
{
    Test.Init(argc, argv);

    //test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("std::complex<float>");
    test_generic<std::complex<double> >("std::complex<double>");

    Test.Exit();
}
catch(const CallChain::BadSymbol& x)
{
    Test.Check(false) << "unexpected BadSymbol: " << x.what() << '\n';
    Test.Exit();
}
catch(const std::bad_cast& x)
{
    Test.Check(false) << "unexpected bad_cast\n";
    Test.Exit();
}
catch(const std::exception& x)
{
    Test.Check(false) << "unexpected exception: " << x.what() << '\n';
    Test.Exit();
}
catch(...)
{
    Test.Check(false) << "unexpected exception\n";
    Test.Exit();
}

