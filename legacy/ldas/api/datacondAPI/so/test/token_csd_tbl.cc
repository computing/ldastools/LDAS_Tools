#include "datacondAPI/config.h"

#include <sstream>
   
#include "general/unittest.h"

#include "token.hh"

#include "UDT.hh"
#include "SequenceUDT.hh"
#include "WelchCSDEstimate.hh"

using namespace std;
   
General::UnitTest	Test;

template<class CSDType, class BaseType>
void
csd_test_template(const std::string& CSDTypeStr,
		  const std::string& BaseTypeStr, const BaseType Base)
{
  try {
    //-------------------------------------------------------------------
    // Calculate the expected value
    //-------------------------------------------------------------------
    Parameters args;
    size_t     n = 32;  
    size_t     fftLen = 8;
    size_t     overlap = 4;

    datacondAPI::CSDEstimate::DetrendMethod detrendMethod
	= datacondAPI::CSDEstimate::none;

    datacondAPI::Sequence<CSDType> in(Base, n);
    datacondAPI::udt*              expected = 0;

    datacondAPI::WelchCSDEstimate welch;

    welch.set_fftLength(fftLen);
    welch.set_overlapLength(overlap);
    welch.set_detrendMethod(detrendMethod);
   
    welch.apply(expected, in, in);

    //-------------------------------------------------------------------
    // Calculate value using Call Chain.
    //-------------------------------------------------------------------

    CallChain		cmds;
    CallChain::Symbol*	result;
    ostringstream	base;
    ostringstream	size;
    ostringstream	fftlen;
    ostringstream	overlaplen;
    ostringstream	dflag;
    
    base << Base;
    size << in.size();
    fftlen << fftLen;
    overlaplen << overlap;
    dflag << detrendMethod;

    cmds.Reset();

    cmds.AppendCallFunction("integer", args.set(1, size.str().c_str()), "N");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction("integer", args.set(1, fftlen.str().c_str()), "fftLen");
    cmds.AppendCallFunction("integer", args.set(1, overlaplen.str().c_str()),
			    "overlap");
    cmds.AppendCallFunction("integer", args.set(1, dflag.str().c_str()), "detrendMethod");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction(CSDTypeStr, args.set(2, "base", "N"), "array");

    cmds.AppendCallFunction("csd",
			    args.set(6, "array", "array", "fftLen", "",
				     "overlap", "detrendMethod"),
			    "result");


    cmds.AppendIntermediateResult( "result", "result", "Final Result", "" );

    cmds.Execute();

    //-------------------------------------------------------------------
    // Compare the results to the exepected value.
    //-------------------------------------------------------------------
    result = cmds.GetSymbol("result");

    ILwd::LdasElement* ilwd;

    ilwd = result->ConvertToIlwd(cmds, datacondAPI::udt::TARGET_METADATA_FINAL_RESULT);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2, Test.Message(), ILwd::ASCII);
    Test.Message(false) << endl;
    delete ilwd;

    ilwd = result->ConvertToIlwd(cmds, datacondAPI::udt::TARGET_WRAPPER);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2, Test.Message(), ILwd::ASCII);
    Test.Message(false) << endl;

    delete ilwd;

  }
  catch ( const std::logic_error& )
  {
  }
  CATCH(Test);
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_csd_tbl.cc,v 1.10 2005/12/01 22:55:03 emaros Exp $"
		 << endl << flush;

  try {
    csd_test_template<double, double>("dvalarray", "double", 1.0);
    csd_test_template<double, float>("dvalarray", "double", 2.0);
    csd_test_template<float, double>("svalarray", "double", 3.0);
    csd_test_template<float, float>("svalarray", "double", 4.0);
  }
  CATCH(Test);

  Test.Exit();
}
