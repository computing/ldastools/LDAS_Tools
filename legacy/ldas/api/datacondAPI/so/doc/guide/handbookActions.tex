\subsection{Design}
First consider the name and syntax of the action---the interface as it will appear to the user.  An action may be \emph{overloaded} on the type and number of arguments.
\begin{verbatim}
z = verb(x[, y]); // x is-a T, optional y is-a U
\end{verbatim}
\subsection{Implementing a Function}
The first step is to derive a new class from {\tt
CallChain::Function}.
\begin{verbatim}
class VerbFunction : public CallChain::Function
{
public:
    VerbFunction();
    virtual const std::string& GetName() const;
    virtual void Eval(CallChain* Chain,
                      const CallChain::Params Params,
                      const std::string& Ret) const;
};
\end{verbatim}
The default constructor must be of the form
\begin{verbatim}
VerbFunction::VerbFunction() : CallChain::Function(GetName())
{
}
\end{verbatim}
and the \verb+VerbFunction::GetName+ accessor must be of the form
\begin{verbatim}
VerbFunction::GetName()
{
    static std::string name("verb")
    return name;
}
\end{verbatim}
The first call to \verb+VerbFunction::GetName+ creates a \verb+std::string+ containing the name of the action (\emph{verb}), and returns a constant reference to it.  Subsequent calls do not incur the cost of constructing the \verb+std::string+.

The default constructor \verb+VerbFunction::VerbFunction()+ calls
the parent constructor
{\tt CallChain::Function::Function(const std::string\&)} with the const reference to the \verb+std::string+ returned by \verb+VerbFunction::GetName()+, that is, the name of
the action.  \verb+CallChain::Function::Function(const std::string\&)+ takes the name of the action and registers the virtual function \verb+VerbFunction::Eval+ as the handler for all
action calls of that name.

This is achieved with a single global `dummy' instantiation of a \verb+VerbFunction+ object.
\begin{verbatim}
static VerbFunction VerbFunction_;
\end{verbatim}
Once the code to register the \verb+VerbFunction::Eval}+ handler has been put in place, it only remains to implement the handler.
\subsection{Syntax Implementation}
The method recieves three arguments
\begin{verbatim}
void VerbFunction::Eval(CallChain* Chain,
                        const CallChain::Params Params,
                        const std::string& Ret) const
\end{verbatim}
The first argument \verb+Chain+ is a pointer to the chain to the environment the call originated in, that contains the input data, and will recieve any output data.  The second argument is a const reference to a vector of the names of the action's arguments (\verb+CallChain::Params+ is a \verb+typedef+ for a \verb+std::vector<std::string> >+).  The third argument is the name of
the return value; if it is empty (\verb+== ""+) there is no return
value.

The output of the action will reside in a \verb+CallChain::Symbol+
(a \verb+typedef+ for a \verb+datacondAPI::UDT+), and the creation of a null pointer is the first step in writing the body of the method.
\begin{verbatim}
{
    CallChain::Symbol* z = 0;
\end{verbatim}
The next step is to \verb+switch+ on the number of arguments supplied to the action; that is, the \verb+size()+ of \verb+Params+.
\begin{verbatim}
    switch(Params.size())
    {
    case 2:
        // ...
        break;
    case 1:
        // ...
        break;
    default:
        throw CallChain::BadArgumentCount(GetName(),      // action name
                                          2,              // expected count
                                          Params.size()); // received count
    }
\end{verbatim}
The \verb+switch+ statement checks for all valid argument counts,
and throws a \verb+CallChain::BadArgumentCount+ otherwise.  Inside
a valid agument count \verb+case+ we extract the \verb+CallChain::Symbol+s (\verb+datacondAPI::UDT+s) and then operate on them.
\begin{verbatim}
    case 2:
        {
            const CallChain::Symbol& x = *(Chain->GetSymbol(Params[0]));
            const CallChain::Symbol& y = *(Chain->GetSymbol(Params[1]));
            // ...
        }
        break;
\end{verbatim}
Code must then be added to compute output from the \verb+datacondAPI::UDT+s \verb+x+ and \verb+y+, and set \verb+z+ to point at the output \verb+datacondAPI::UDT+.  The exact form of this is highly dependent on what \verb+verb+ does; it may call a \verb+datacondAPI+ class \verb+apply(z, x, y)+ method, or it may \verb+Cast+ and perform the computations locally or even in-line.  The only restriction is that after the \verb+switch+ block, \verb+z+ (and only \verb+z+) must point to a \verb+datacondAPI::UDT+ on the heap containing the output.

The final part of the method is to push the output onto the call chain stack and name it according to \verb+Ret+.
\begin{verbatim}
    Chain->Push(z);
    Chain->ResetOrAddSymbolWithStackData(Ret);
}
\end{verbatim}