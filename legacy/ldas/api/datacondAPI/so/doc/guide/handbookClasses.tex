% Basic Classes

A class in the \verb+datacondAPI+ library usually represents an 
implementation of a particular data processing operation. 
As such, many of the classes have a similar interface in that they have 
functions which act on sequences of numbers (eg. time-series) to produce 
a desired result.
Often the result will be another sequence, such as the output of a linear
filter. 
This section provides a coding standard for data conditioning classes
so that the class interfaces will be reasonably uniform.
The base standard adopted for C++ coding is given in \cite{codingstd}. 
This will be followed except where expanded by this document.

\subsection{Compiler default methods}

The C++ compiler automatically provides the following methods for a
class if they are left undefined:
\begin{itemize}
\item Default constructor (only if no other constructors are provided).
\item Copy constructor \verb+Class(const Class&)+
\item Default destructor
\item Asignment operator \verb+Class& operator=(const Class&) const+
\item Address-of operator \verb+Class* operator&(const Class&) const+
\end{itemize}

For many classes the default methods are not appropriate and should be
defined by the class developer.
When creating a new class, begin with the following template:
\begin{verbatim}
namespace datacondAPI {

    class MyClass {
    public:
    protected:
    private:
        //: Default constructor
        MyClass();

        //: Copy constructor
        MyClass(const MyClass&);

        //: Destructor
        ~MyClass();

        //: Assignment operator
        const MyClass&
        operator=(const MyClass&);
    };

}
\end{verbatim}
Putting these functions in the private section initially prevents them from
being called by mistake.
As each function is defined, it can be moved to the public section or 
removed entirely if the implicit definition is appropriate.

\subsubsection{Constructors}

Constructors (including the default constructor) 
should initialize all member variables to a known state. 
This implies that all classes should either have a default constructor
defined, or else placed in the private section so that it can't be
called inadvertently. 

For efficiency, constructors should use initializer lists rather than 
assignment to initialize member data. If a class is derived from other
classes, all the base classes must also be constructed with the appropriate
constructor.

\subsubsection{Copy constructor}

Providing a copy constructor is strongly recommended. A copy constructor
is usually required when a class contains pointer members, for example.
The default copy constructor would simply copy the value of the pointer,
which is not normally what is wanted. 

Copy constructors require some extra care on the part of the programmer.
They must ensure that all data members are correctly initialised from
the argument of the constructor, and also that any base classes are
correctly constructed, usually by calling their own copy constructor.
When making changes to the class, such as adding new data members,
the copy constructor should usually be updated as well.

If the programmer does not wish to implement a copy constructor, it may be
omitted if the default copy constructor is appropriate. If it is 
{\em not} appropriate, then a declaration (but no definition) of the
copy constructor should be placed in the private section of the class
so that it won't be called accidently.

\subsubsection{Assignment operator}

Providing an assignment operator is recommended. The default assignment
operator simply calls the assignment operator of each data member.
Often this behaviour is inappropriate. For example, if a class contains
a pointer member, the assignment operator would simply copy the value
of the pointer rather than making a copy of the object being pointed to.
Another example is when a class
contains member data for which assignment is not well-defined, as in the
case of the \verb+valarray+ class. Assignment between \verb+valarray+'s
is undefined when the arrays are of different lengths: a correct
assignment operator would first resize the \verb+valarray+ of the left-hand
side to make this operation well-defined.

Assignment operators require considerable care on the part of the programmer.
They must ensure that 
\begin{itemize}
\item all data members are correctly assigned
\item any base classes are correctly assigned
\item self-assignment is handled correctly.
\end{itemize}
This is best illustrated by an example. Suppose we have a class {\tt Derived}
derived from {\tt Base}. The copy constructors and assignment operators
should resemble this:
\begin{verbatim}
class Base {
    ...
};

class Derived : public Base {
public:
    // Copy constructor
    Derived(const Derived& d)
        : Base(d) // construct base class(es)
    {
        ...       // do the rest
    }

    // Assignment
    const Derived& operator=(const Derived& rhs) const
    {
        if (&rhs != this) // guard against self-assignment
        {
            Base::operator=(rhs); // assign base class(es)
            ...                   // do the rest
        }
        return *this;
    }
};
\end{verbatim}
Note that the construction
\begin{verbatim}
    Derived d1 = d2; 
\end{verbatim}
is actually using the copy constructor, not the assignment operator,
in spite of the equals sign.
When making changes to the class, such as adding new data members,
the assignment operator should usually be updated as well.

If the programmer does not wish to implement an assignment operator, it may be
omitted if the default is appropriate. If it is 
{\em not} appropriate, then a declaration (but no definition) of the
assignment operator should be placed in the private section of the class
so that it won't be called accidently.

\subsubsection{Destructors}

Classes which allocate resources that are not automatically freed 
(eg. have a pointer variable) should have a destructor which explicitly 
frees the resources.
Since any class may someday be used as a base class, destructors should 
be declared virtual even if empty.

\subsubsection{Address-of operator}

In most cases the default {\tt operator\&()} is appropriate.

\subsubsection{Standard containers}

Most standard containers (eg. {\tt list}, {\tt vector}) require that
classes to be contained have a
public copy constructor or assignment operator. Some containers
(eg. {\tt hash\_map}) require that contained classes have a default
constructor. Consider whether a class may be used as container element
before forbidding these functions.

\subsection{The class declaration}

\subsubsection{Member functions}

A data conditioning class will usually have many more functions than
the basic member function types outlined in \cite{codingstd}
(eg. constructors, destructor, accessors, mutators). 
In all cases, the parameters to member functions will be listed in the 
order: output-only parameters, input/output parameters, input-only 
parameters.
Generally, input-only parameters should be passed by {\tt const} reference.
In addition, member functions that do not modify the member variables of 
an object should be declared {\tt const}.

A data conditioning class will usually have a distinguished function 
(or functions) which actually perform the operation for which the class 
was designed. 
This function will be called {\tt apply()} and have the return type 
{\tt void}.

\subsubsection{Data members}

Data members should all be declared in the {\tt private} section of
the class. 
All member data variable names will be prefixed with \verb+m_+.
This helps distinguish member variables from local variables in
member functions, and also allows the part of the name following the
\verb+m_+ to be used as a parameter in member functions.

\subsection{Templated classes}

The code for a template class or function should be laid out in the same 
way as an ordinary class, with the template declaration in a header file 
and the template definition in a source file.
However, since GCC does not yet support external definitions of templates,
special handling is required to insure proper instantiation of templated 
objects.

\subsubsection{Instantiation rules}
\label{sss:InstantiationRules}

As specified in \cite{codingstd}, the preferred method of template 
instantiation is to put the template definition in a {\tt .cc} file and 
make explicit instantiation requests for the template types which are 
needed. 
This saves the overhead of recompiling, and also guarantees that only
certain template types are usable.
The disadvantage is that if a new template type is required, an 
instantiation request must be added to the template definition and the 
source file recompiled, otherwise the linker will fail to find symbols 
for the template member functions.

{\bf Example}\\
In {\tt TemplateClass.hh}:
\begin{verbatim}
// A simple template declaration
template<class T>
class TemplateClass {
public:
    T value() const;
private:
    T m_value;
};
\end{verbatim}

In {\tt TemplateClass.cc}:
\begin{verbatim}
#include "TemplateClass.hh"

// Definitions of member functions
template<class T>
T TemplateClass<T>::value() const
{
    return m_value;
}

// Explicit instantiations - only types listed
// here will be usable
template class TemplateClass<int>;
template class TemplateClass<float>;

...

\end{verbatim}

