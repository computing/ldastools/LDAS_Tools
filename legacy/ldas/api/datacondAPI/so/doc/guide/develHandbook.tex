% $Id: develHandbook.tex,v 1.7 2001/04/26 01:43:48 ldas_anu Exp $
\documentclass[12pt]{article}
\voffset -1.5cm
\oddsidemargin 0.3cm
\evensidemargin 0cm
\textheight 22cm
\textwidth 17cm
\footskip 2.2cm
\parindent=0in
\parskip=0.1in

\def\Ztitle{
\Large\bf Data Conditioning API Developers Handbook}

\def\Zauthors{}
\def\Zdate{}
\def\Znumber{T01xx}
\def\Zversion{00}
\def\Zgroup{R}
\def\Zcomment{This is an internal working note \\of the LIGO Project.}
%\def\Zcomment{\LARGE DRAFT}
\def\Zfilename{}

\begin{document}

\input{tpageLIGO.tex}

\newpage
\tableofcontents

\section{Introduction}

This document is intended to provide a set of guidelines for developers
when writing new components for the LDAS data conditioning API.
From an end-user's perspective, the purpose of the API is to provide
functions for signal processing, analysis and conditioning of
time-series data originating from an interferometer.
Functionality provided by the API includes
\begin{itemize}
\item elementary numerical and vector operations
\item statistical analysis
\item resampling
\item heterodyning
\item mixing
\item filtering
\item Fourier transforms
\end{itemize}
Functionality to be provided in the future includes
\begin{itemize}
\item data drop-out correction
\item cross-correlation
\item convolution
\item regression
\item removal of line artifacts (violin modes, mains power)
\end{itemize}

There are two methods for accessing the functions of the API.
At the lowest level, the data processing operations of the API are performed
by a set C++ classes and functions. These functions are compiled into a
library archive which may be linked (statically or dynamically) with user
code in order to perform a particular set of data conditioning operations. 
Usually, a class in the data conditioning library is designed
to perform only a particular data processing operation
(or group of related operations). Such classes are designed to be as general
as possible -- they are not necessarily specific to the analysis of
interferometer data. For example, a class for calculating statistics should 
be applicable to any set of numbers.

The second method to access the API is via the Tcl interface to the
{\em data conditioning command language} (DCCL). The API provides
extensions to Tcl whereby data may be retrieved from a remote source
(such as an archive of frame files) and acted upon by a series of data
processing operations specified in DCCL. DCCL resembles a simple procedural
language such as C in that it supports variables, literal constants, 
functions and assignments in order to carry out data conditioning operations.
Functions in DCCL (hereafter called {\em actions}) may correspond directly
to the action of a particular C++ class (such as the FFT class), although
more often they represent an aggregate operation involving the use of
several data conditioning classes.

{\bf Example}\\
Suppose we wish to calculate the statistics of a segment of data. Here
is an example of a data file {\tt H-662111999.ilwd} containing one 
second of data from a single channel sampled at 2048 Hz.
It is represented in the LIGO lightweight format
but could just as easily be in Frame format:
\begin{verbatim}
<?ilwd?>
<ilwd size='1'>
    <ilwd name='chan_01' size='2'>
        <int_4u name='rate'>2048</int_4u>
        <int_2s name='data' dims='2048'>123 234 345 ... etc</int_2s>
    </ilwd>
</ilwd>
\end{verbatim}
The following user command reads {\tt H-662111999.ilwd} and calculates
some statistics for the data:
\begin{verbatim}
ldasJob {-name user -password **** -email user@ligo.caltech.edu} 
{ conditionData 
    -inputprotocol file:/H-662111999.ilwd
    -inputformat ilwd
    -returnprotocol file:/H-662111999-VAR.ilwd
    -returnformat {ilwd ascii} 
    -resultcomment {List of statistics}
    -resultname {stats} 
    -aliases {idata=chan_01:data} 
    -algorithms {
        mi = min(idata);
        intermediate(,,min,{Min of chan_01:data})
        ma = max(idata);
        intermediate(,,max,{Max of chan_01:data})
        mu = mean(idata);
        intermediate(,,mean,{Mean of chan_01:data})
        r  = rms(idata);
        intermediate(,,rms,{RMS of chan_01:data})
        v  = variance(idata);
        intermediate(,,var,{Variance of chan_01:data})
        s  = skewness(idata);
        intermediate(,,skew,{Skewness of chan_01:data})
        k  = kurtosis(idata);
        intermediate(,,kurt,{Kurtosis of chan_01:data})
    } 
}
\end{verbatim}
The components of the user command have the following meanings:
\begin{itemize}
\item[{\tt ldasJob}] -- tells the LDAS manager API that the following is a
new LDAS job and provides security information in the form of a username,
password and email address.
\item[{\tt conditionData}] -- tells the manager API that this job should be
directed to the data conditioning API. Arguments to {\tt conditionData} are
explained below.
\item[{\tt -inputprotocol}] -- the protocol and location of the input data.
Each component of the input data is stored as a single variable of the
appropriate type (vector, scalar etc) in the data conditioning API, with
a name constructed by concatenating the names of its containing objects.
In the example, after ingesting the data the API would contain two objects,
a scalar called {\tt chan\_01:rate} and a vector called {\tt chan\_01:data}.
\item[{\tt -inputformat}] -- the format of the input data
\item[{\tt -returnprotocol}] -- the protocol and location of the output data
\item[{\tt -returnformat}] -- the format of the output data
\item[{\tt -resultname}] -- the name associated with the ILWD object containing
the result of the data conditioning command
\item[{\tt -resultcomment}] -- a text comment attached to the result
\item[{\tt -aliases}] -- a list of alternate names for the data objects 
contained in the input
\item[{\tt -algorithms}] -- a sequence of data conditioning actions.
Internally, the API maintains an ILWD object containing a final result.
The[{\tt intermediate} action appends the result of the preceding operation
to the final result. After the last command is executed, the final result
ILWD is written to the output in the specified format.
\end{itemize}
The result file produced by this user command would resemble the following:
\begin{verbatim}
<?ilwd?>
<ilwd size='8'>
  <real_8 comment='Min_of_chan_01:data' name='min'>-2171.0</real_8>
  <real_8 comment='Max_of_chan_01:data' name='max'>1735.0</real_8>
  <real_8 comment='Mean_of_chan_01:data' name='mean'>2.3721</real_8>
  <real_8 comment='RMS_of_chan_01:data' name='rms'>56.7676</real_8>
  <real_8 comment='Variance_of_chan_01:data' name='var'>287.345</real_8>
  <real_8 comment='Skewness_of_chan_01:data' name='skew'>1.6273</real_8>
  <real_8 comment='Kurtosis_of_chan_01:data' name='kurt'>0.7351</real_8>
  <real_8 comment='List_of_statistics' name='stats'>0.7351</real_8>
</ilwd>
\end{verbatim}

\subsection{Purpose}

\subsection{Audience}

\section{Readability and Maintainability}

\input handbookMaint.tex

\section{Program and File Organization}

\input handbookOrganization.tex

\section{Basic Classes}
\label{chap:BasicClasses}

\input handbookClasses.tex

\section{UDTs and UMDs}

\input handbookUDTsAndUMDs.tex

\section{Exceptions}

\input handbookExceptions.tex

\section{Action Glue}

\input handbookActions.tex

\section{Test Code}

\input handbookTestCode.tex

\section{Tutorial}

\input handbookTutorial.tex

\begin{thebibliography}{[99]}
\bibitem{codingstd} ``LIGO Data Analysis System Software Specification for
C++'', James Kent Blackburn and Philip Charlton, LIGO-T00xxxx.
\bibitem{stroustrup} {\em The C++ Programming Language}, Bjarne Stroustrup,
third edition.
\bibitem{perceps} The Perceps Home Page,
\verb+http://starship.python.net/~tbryan/PERCEPS+.
\end{thebibliography}

\end{document}
