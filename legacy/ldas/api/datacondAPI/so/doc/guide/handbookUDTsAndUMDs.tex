\subsection{{\tt UDT} (Unified Data Type)}
All data objects manipulated by the Data Conditioning API inherit
from the abstract base class {\tt UDT}.  That is, every data type
\emph{is-a} {\tt UDT}.
\begin{verbatim}
namespace datacondAPI
{

    class UDT
    {
    public:
    
        UDT();
        
        virtual ~UDT();

        virtual UDT* Clone(void) const = 0;
        
        template<class T> static bool IsA(const udt& In);
        template<class T> static       T&  Cast(      UDT& In);
        template<class T> static const T&  Cast(const UDT& In);
        
        // ...

    }; // class UDT

} // namespace datacondAPI
\end{verbatim}
The (simplified) class definition above ensures that the {\tt UDT}
and all derived types have virtual destructors for safe polymorphic
handling.  The abstract virtual functions (those with an {\tt = 0}
suffix) ensure that {\tt UDT} is an abstract base class, that is,
a class that cannot be instantiated, and provide the {\tt Clone}
 method to facilitate safe polymorphic handling.
The {\tt Clone} method is a ``virtual copy constructor''---it
returns a pointer to a copy of the {\tt UDT} it is called on, without requiring the user have any knowledge of the exact derived
type the {\tt UDT} actually is.

Only so much can be achived without fully (or partially) resolving
the derived type of a {\tt UDT}.  For example, if a particular
{\tt UDT} is-a {\tt Sequence}, the subscript {\tt operator[]} is
meaningful.  In general, more precisely resolving the type of a
{\tt UDT} exposes more functionality (more interface methods).
The {\tt static} methods {\tt IsA} and {\tt Cast} facilitate type
resolution without requiring the user to resort to the unwieldy
{\tt dynamic\_cast} keyword.

To check if a {\tt UDT} reference {\tt input} is actually a
reference to a {\tt UDT}-derived type {\tt DerivedClass}, use the {\tt IsA} method.  To extract a {\tt DerivedClass} reference from {\tt input}, use the cast method.
\begin{verbatim}
if(UDT::IsA<DerivedClass>(input))
{
    DerivedClass& resolved = UDT::Cast<DerivedClass>(input)
    // code using resolved to access DerivedClass methods
}
\end{verbatim}
If the {\tt Cast} is invalid (which can only occur it was not
checked for validity by {\tt IsA}) the method throws a {\tt
std::bad\_cast} exception.
\subsection{{\tt UDTMetaData} (UMD)}
The {\tt UDT} metadata class {\tt UDTMetaData} is the base class

for all metadata classes.  A {\tt UDT} derived type with metadata
derives from both {\tt UDT}, and from a specific metadata class
that is itself derived from {\tt UDTMetaData}.
\begin{verbatim}
namespace datacondAPI
{

    class UDTMetaData
    {
    
    public:

       // ...

    protected:

        UDTMetaData();
        UDTMetaData(const UDTMetaData& value);

        virtual ~UDTMetaData();

        UDTMetaData& operator=(const UDTMetaData& value);

    }; // class UDTMetaData

} // namespace datacondAPI
\end{verbatim}
The implicitly defined methods for {\tt UDTMetaData} are {\tt
protected} to discourage the treatement of metadata as an object.
Metadata can only be constructed and destructed by derived types,
that is, the data.
\subsection{Implementing a UDT with Metadata}
\subsubsection{Design}
For this example, we will consider a \emph{noun} {\tt UDT}, {\tt Noun}, that is a {\tt UDT} representing ``a word that is used to name a person, place, thing, quality, or action''.  Logically,
\begin{itemize}
          \item A {\tt Noun} is a {\tt UDT}.
          \item A {\tt Noun} is a {\tt std::string}.
          \item A {\tt Noun} has a \emph{definition}.
\end{itemize}
While every \emph{noun} has a \emph{definition}, not every \emph{definition} is of a \emph{noun}.  This indicates that we might want to re-use a \emph{definition} elsewhere in the {\tt datacondAPI}.  We should thus implement \emph{definition} as
a metadata class {\tt Definition} inheriting from {\tt UDTMetaData}, not as a member of {\tt Noun}.
\subsubsection{Metadata}
We express the design in C++ as
\begin{verbatim}
class Definition : public UDTMetaData
{
public:
    const std::string& GetDefinition() const;
    const std::string& SetDefinition(const std::string&);
protected:
    Definition(const std::string&);
    Definition(const Definition&);
    Definition& operator=(const Definition&);
    virtual ~Definition();
private:
    Definition();
    std::string m_definition;
};
\end{verbatim}
Any class ({\tt public}ly) inheriting from {\tt Definition} inherits the {\tt public} accessor {\tt GetDefinition} and {\tt public} mutator {\tt SetDefinition}.

The non-trivial and copy constructors and assignment operator are {\tt private}, and the default constructor is {\tt private} to prevent a stand-alone instantiation of {\tt Definition}, that is
a {\tt Definition} without something to define.  Only a class inheriting the metadata, that is, the \emph{data}, can construct the metadata.  The virtual destructor is similarly {\tt protected}.  The {\tt private} default constructor prevents construction of an empty definition.

The implementation of these methods is trivial.
\subsubsection{UDT}
The C++ representation of the {\tt UDT} class design is
\begin{verbatim}
class Noun : public UDT, public std::string, public Description
{
public:
   Noun();
   explicit Noun(const std::string& noun);
   Noun(const std::string& noun, const std::string& description);
   Noun(const Noun&);
   
   virtual ~Noun();
   
   Noun& operator=(const Noun&);
   
   virtual Noun* Clone() const;
};
\end{verbatim}
The majority of the interface is inherited from {\tt std::string} and {\tt Description}.

The implementation of the constructors in terms of the constructors of the base objects is instructive
\begin{verbatim}
Noun::Noun()
: UDT(),
  std::string(),
  Description(std::string())
{
}

Noun::Noun(const std::string& noun)
: UDT(),
  std::string(noun),
  Description(noun)
{
}

Noun::Noun(const std::string& noun,
           const std::string description)
: UDT(),
  std::string(noun),
  Description(description)
{
}
\end{verbatim}
As the class has no members, the implementation of the destructor and {\tt Clone} method are trivial.
\begin{verbatim}
Noun::~Noun()
{
}

Noun* Noun::Clone() const
{
    return new Noun(*this);
}
\end{verbatim}
The assignment operator is implemented in terms of the assignment operators of the base classes.  \emph{Note that this is fragile (must be updated) under the addition of new base classes}.
\begin{verbatim}
Noun& Noun::operator=(const Noun& noun)
{
    if (&noun != this)
    {
        UDT::operator=(noun);
        std::string::operator=(noun);
        Description::operator=(noun);
    }
    return *this;
}
\end{verbatim}
We must instantiate {\tt UDT::IsA<Noun>} and {\tt UDT::Cast<Noun>} for compatibility.
\begin{verbatim}
UDT_CLASS_INSTANTIATION(Noun)
\end{verbatim}
To enable human-readable type names a line
\begin{verbatim}
    ADD_ENTRY(Noun, "Noun")
\end{verbatim}
must be added to the body of {\tt void TypeInfoTable\_t::initialize(void)} in {\tt TypeInfo.cc}.

If users are to construct {\tt Noun}s explicitly in a datacondAPI command, an action {\tt noun} should be added to {\tt DataTypeFunctions.*} (see the section on action glue).