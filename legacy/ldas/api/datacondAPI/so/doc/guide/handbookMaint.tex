% $Id: handbookMaint.tex,v 1.2 2001/04/24 17:35:48 Philip.Charlton Exp $

% Readability and maintainability

This section provides a coding standard for data conditioning classes
so that the class interfaces in the data conditioning API
will be reasonably uniform.
The basic style guide adopted for C++ coding is given in
\cite{codingstd}. This will be followed except where expanded by this
document.

\subsection{Standard namespaces}

All classes, functions, static objects and so on specific to the data
conditioning API will be declared in the {\tt datacondAPI} namespace.
This will prevent any name collisions with objects in other libraries.

All classes, functions and objects which are not intended to be accessible
outside of the implementation of a class or function will be defined in
the compilation unit in which they are used (usually in a {\tt .cc}
file) inside an anonymous namespace.

\subsection{Naming conventions}

Naming conventions will largely conform to the requirements given in the style
guide. Classes designed for data conditioning applications
will have certain required
methods (such as {\tt apply()}). See Chapter \ref{chap:BasicClasses}.

\subsection{Exception specifications}

All functions will have an exception specification. This is a part of
the C++ language which tells the compiler what exceptions a function
may throw. Care must be taken to ensure that every possible exception
that can be thrown is included in the exception specification, including
those that might be thrown by other functions called.

If the function throws an exception that is not in the exception
specification, the program calls the standard library routine
{\tt unexpected()}. The default behaviour of this function is to terminate
the program. This is unacceptable in a real-time system such as LDAS,
however C++ allows the default behaviour to be redefined.
LDAS redefines the behaviour of {\tt unexpected()} so that it throws
{\tt unexpected\_exception} instead.
For this reason, all exception specifications
must include {\tt unexpected\_exception}.

{\bf Example}
\begin{verbatim}
// A function that is only meant to throw std::invalid_argument.
// unexpected_exception is also included as a safety net.
void myFunction(int k)
    throw(std::invalid_argument, unexpected_exception);
\end{verbatim}

\subsection{Comments and Web-based documentation}

Source files should be liberally commented, particularly
where code is complicated and there is a danger that other developers
may change things without realising that there is a good reason for
doing something a certain way. Keep in mind that code still needs to be
maintained long after the original author has moved on. All comments
in LDAS will use the {\tt //} form rather than the {\tt /*  */} form.
Furthermore, some comments will use Perceps tags to generate HTML
documentation automatically -- see below.

Comments in the header file of a class should be regarded as a form of
user documentation for the class. The header file should describe the usage
of each function in the class. In particular, it should describe what the
user can expect for given input and output arguments, including any
exceptions thrown and the circumstances under which they are thrown.
The comments should be complete enough that the user need never look at the
source file to determine what a function really does.

Generally, comments in the header file (and sometimes in source files) must
be written using Perceps tags so that HTML documentation can be generated.
For a full description of Perceps, see \cite{perceps}.
The most commonly used tags are summarised in the following table:
\begin{center}
\begin{tabular}{|l|l|} \hline
{\bf Tag} & {\bf Meaning} \\ \hline
{\tt //: <text>} & Brief description of a function or class\\ \hline
{\tt // <text>} & Following {\tt //:}, a long description of a function or
class\\ \hline
{\tt //!param: <param> - <text>} & Description of a function
parameter\\ \hline
{\tt //!return: <text>} & Description of a functions
return value\\ \hline
{\tt //!exc: <exception> - <text>} & Description of an exception
thrown by a function\\ \hline
{\tt //+ <text>} & Continuation of the preceding tag\\ \hline
{\tt //!ignore\_begin:} & Ignore any comments after this line\\ \hline
{\tt //!ignore\_end:} & Stop ignoring comments\\ \hline
\end{tabular}
\end{center}
Text interpreted by Perceps is typeset as if it were HTML. This means that
spacing, including newlines, is ignored. If you want line breaks you
must insert HTML {\tt <p>} or {\tt <br>} tags by hand. Symbols that
are special in HTML are also special in Perceps, thus symbols such as
greater-than and less-than must be written using their HTML character
codes eg. {\tt \&gt;} and {\tt \&lt;}.

{\bf Example}
\begin{verbatim}
namespace datacondAPI {

    //: A class for illustrating Perceps tags
    //
    // This is the long description of MyClass
    //
    class MyClass {
         //: Default constructor
         //
         // Long description
         //
         MyClass();

         //: Brief description of myFunction
         //
         // Long description of myFunction. Can use HTML tags
         // for <b>typesetting</b>. Special symbols such as
         // greater-than must be written in HTML ie. &gt;
         // <p>
         // New paragraphs must start with the <p> tag.
         //
         //!param: in - An input parameter. The description can
         //+ span several lines if it starts with a + symbol
         //
         //!return: The return value
         //
         //!exc: std::invalid_argument - Thrown if the argument is invalid
         //
         int myFunction(const valarray<float>& in);
    };

}
\end{verbatim}

\subsection{Thread safety}

All data-conditioning classes and functions must be thread-safe. This means
that classes and functions should
\begin{itemize}
\item avoid using non-threadsafe functions and classes
\item avoid using static members in classes
\item avoid using static variables in functions
\item avoid using global variables.
\end{itemize}
Generally, the standard C++ library is thread-safe, although there are
exceptions. 
In cases where non-threadsafe functions or static variables must be used,
code can be made thread-safe by synchronising it with a mutex. 
Locking a mutex at the start of a section of code ensures that only one
thread can execute that code at a time. The mutex is unlocked when the
non-threadsafe section of the code is finished with. In the case of a
non-threadsafe function, a mutex can be locked before calling the function
and unlocked immediately after the call. This will prevent any other thread
from calling the function at the same time. Since mutexes require considerable
overhead, they should only be used where absolutely necessary.

In the case of a shared resource such as a static or global variable,
mutexes can be used to restrict access. For example, it
would be disastrous to allow separate threads to add or remove elements
simultaneously from a complex object such as a linked list.
To synchronise access, a single mutex can be used in many places
to ensure that only one thread can read or write to the shared resource at
a time. In this sense, the mutex is associated with the shared resource
rather than any particular lines of code.

One additional complication arises with the use of mutexes in C++ -- mutexes
are not unlocked automatically when an exception is thrown. This can lead
to mutexes remaining locked even though an exception causes the stack to be
unwound to a point where the mutex would normally be unlocked.
However, C++
guarantees that destructors are called as the stack is unwound during
exception handling. LDAS exploits this feature to provide automatic unlocking
for mutexes. Given a mutex, the class {\tt MutexLock} is used in place of the
usual mutex locking functions:

\begin{verbatim}
class MutexLock {
public:
    MutexLock(pthread_mutex_t& mutex)
        : m_mutex_ptr(&mutex)
    {
        pthread_mutex_lock(m_mutex_ptr);
    }

    MutexLock(pthread_mutex_t* const mutex_ptr)
        : m_mutex_ptr(mutex_ptr)
    {
        pthread_mutex_lock(m_mutex_ptr);
    }

    ~MutexLock()
    {
        pthread_mutex_unlock(m_mutex_ptr);
    }

private:
    MutexLock();
    MutexLock(const MutexLock&);
    const MutexLock& operator=(const MutexLock&);

    pthread_mutex_t* m_mutex_ptr;
};
\end{verbatim}

A mutex is locked by constructing an instance of {\tt MutexLock} from it,
and unlocked when the {\tt MutexLock} goes out of scope.
If an exception is thrown while the mutex is locked, the destructor for
{\tt MutexLock} is called as the stack unwinds and the mutex is unlocked
automatically.

{\bf Example}
\begin{verbatim}
#include <list>
#include <string>
#include <general/mutexlock.hh>

// An example of accessing a shared resource in a thread-safe way

namespace {
    // A (shared) list of words
    list<string> theList;

    // A mutex to synchronise access to theList
    pthread_mutex_t theList_mutex =  PTHREAD_MUTEX_INITIALIZER;
}

// Push word onto the front of the list
void pushFront(const string& word)
{
    {
        // Instantiating MutexLock locks the mutex. Other
        // threads will now block on pushFront() or popFront()
        MutexLock(&theList_mutex);
        theList.push_front(word);
    }
    // MutexLock has now gone out of scope - mutex is now
    // automatically unlocked.
}

// Pop word off the front of the list
void popFront()
{
    { 
        MutexLock(&theList_mutex);
        theList.pop_front();
    }
}
\end{verbatim}
