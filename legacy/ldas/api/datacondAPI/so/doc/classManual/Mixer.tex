%% $Id: Mixer.tex,v 1.1 2001/04/02 20:36:59 Philip.Charlton Exp $

\input Head

\section{\tt Mixer}

% A short (preferably one-line) description of {\tt MyClass}.

{\tt Mixer} --- mix a time series with a local oscillator, returning 
in-phase and quadrature phase components as a complex time series

\subsection{Description}

% A longer description of what {\tt MyClass} is used for.

{\tt Mixer\/} takes the product of a time series with a local
oscillator of fixed frequency and given initial phase. Denote
\begin{itemize}
\item the input time series $x[k]$,
\item the sample rate $f_s$,
\item the local oscillator frequency $f_o$,
\item the local oscillator phase at sample-time $k_0$ $\phi_0$. 
\end{itemize}
The output is a complex-valued time series $z[k]$, 
\begin{equation}
  z[k] = x[k]\exp\left[2\pi i (k-k_0)f_o/f_s + \phi_0\right].
\end{equation}

\subsection{Operating Instructions}

\subsection{Usage}
A \texttt{Mixer} object is initialized at construction. It acts on
input through its (overloaded) \texttt{apply(out,in)} methods:
\begin{verbatim}
//: Interpreted apply action (metadata interpreted & set)
//!param: out - points to result of mixer actingh on in. May be null on
//+ entry, in which case the appropriate output object is allocated 
//+ on heap
//!param: in - data on which mixer acts.
//!exc: std::invalid_argument - if in, out not of appropriate types
//!exc: std::bad_alloc - thrown if allocation to out is necessary and fails
//!todo: check exceptions
void apply(udt*& out, const udt& in) 
  throw(std::invalid_argument, std::bad_alloc, std::unimplemented_error);
 
//: Basic mixer action 
//!param: out - points to result of mixer acting on in. 
//!param: in - data on which mixer acts.
//!exc: std::invalid_argument - if in, out not of appropriate types
//!exc: std::bad_alloc - is this thrown?
//!todo: check exceptions
template<typename out_type, typename in_type>
void apply(std::valarray<std::complex<out_type> >& out, 
           const std::valarray<in_type>& in) 
  throw(std::invalid_argument);
\end{verbatim}
The type of the output argument \texttt{out} must be compatible with
the type of the input argument \texttt{in}, as described by table
\ref{tbl:Mixer} 
\begin{table}
\label{tbl:Mixer}
\caption{Type of \texttt{out} argument to apply required by type of
  \texttt{in} argument. U is either an \texttt{std::valarray},
  \texttt{datacondAPI::Sequence}, or
  \texttt{datacondAPI::TimeSeries}.} 
\begin{tabular}{ll}
\texttt{in} type & \texttt{out} type\\
\hline
U\texttt{$<$float$>$} & U\texttt{$<$complex$<$float$>$ $>$}\\
U\texttt{$<$double$>$} & U\texttt{$<$complex$<$double$>$ $>$}\\
U\texttt{$<$complex$<$float$>$ $>$} & U\texttt{$<$complex$<$float$>$ $>$}\\
U\texttt{$<$complex$<$double$>$} & U\texttt{$<$complex$<$double$>$ $>$}
\end{tabular}
\end{table}

\subsubsection{Example}

% A verbatim block of example code showing proper usage of \texttt{MyClass}.
:TODO: \textbf{This example needs to be updated and corrected:}

\begin{verbatim}
typedef float inputType;
typedef double outBaseType;

Sequence<float> in;                // input can be real or complex
Sequence<complex<outBaseType> > z; // result is always complex

double f0(0.8);                    // intermediate oscillator at 0.8 Nyquist
double iphase(0.0);                // oscillator phase at initial sample
Mixer<outBaseType,inputType> mixer ( iphase, f0 );
\end{verbatim}
$\vdots$
\begin{verbatim}
mixer.apply(z,in);                 // on return z is result
use(z);                            // use result in some way
getMore(in);                       // additional input, in sequence
mixer.apply(z,in);                 // on return z is continued result
State* state;
mixer.getState(state);             // state points to current MixerState
Mixer mixer2(*state);              // create new mixer from state
getMore(in);
udt* z2;
mixer.apply(z,in);
// on return z2 points to Sequence<complex<float> > allocated on heap
// Contents of z2 are identical to contents of z
mixer2.apply(z2,in);
\end{verbatim}

\subsubsection{Options/Defaults}

% A list of options or defaults for public methods or member data of
% \texttt{MyClass}. 

%\include MixerOptions

\subsection{Exceptions}

\subsubsection{Exceptions Thrown}

\begin{itemize}
\item \texttt{getState(State$*$\&)}
  \begin{itemize}
  \item \texttt{std::invalid\_argument} if argument incompatible with 
    \texttt{\&MixerState};
  \item \texttt{std::bad\_alloc} if argument is null pointer and
    \texttt{new} of \texttt{MixerState} fails;
  \end{itemize}

  \item \texttt{apply(udt$*$\& out, const udt\& in)}
  \begin{itemize}
  \item \texttt{std::invalid\_argument} if \texttt{out} incompatible with 
    \texttt{in};
  \item \texttt{std::bad\_alloc} if \texttt{out} is null pointer and
    allocation of storage for output fails;
  \item \texttt{std::unimplemented\_error} if method no implemented
    for \texttt{in}
  \end{itemize}

  \item \texttt{apply(std::valarray$<$Tout$>$ out, const
      std::valarray$<$Tin$>$\& in)} 
  \begin{itemize}
  \item \texttt{std::invalid\_argument} if \texttt{out} incompatible with 
    \texttt{in}
  \end{itemize}
\end{itemize}

% A list of exceptions thrown by \texttt{MyClass}.

\subsubsection{Exceptions Handled}

% A list of exceptions handled by \texttt{MyClass}.

None. 

\subsection{Algorithms}

% A discussion of the algorithms used by \texttt{MyClass}.

Input sequence element $x[k]$ is multiplied by $\exp(\Phi[k])$
to form output sequence element $y[k]$. The local oscillator phase is
\begin{equation}
\Phi[k] := (2\pi f k + \phi)\, \mbox{mod}\, 2\pi.
\end{equation}
The phase is reduced modulo $2\pi$ only after every $2/f$ samples. The
phase and frequency are held in a \texttt{MixerState\/} object, which is
reset after every \texttt{apply()\/} operation. 

\subsection{Accuracy}

% A discussion of issues related to the accuracy of numerical routines
% used by \texttt{MyClass}---e.g., approximations,  argument ranges, etc.

% \include MixerAccuracy

Calculations are performed in the base type: i.e., either \texttt{
  complex$<$float$>$} or \texttt{complex$<$double$>$}.

\subsection{Performance}

% A discussion of issues related to the speed (e.g., $O(N\log N)$) of
% computationally demanding routines used by \texttt{MyClass}.

The speed of \texttt{Mixer\/} operations is limited by the \texttt{
  ::cos()\/} and \texttt{::sin()\/} operations.

%\include MixerPerf

\subsection{Testing}

% A description of any test code/test classes that were used to test
% \texttt{MyClass}. 

\subsection{Notes}

% Miscellaneous notes about \texttt{MyClass} that do not appear
% elsewhere, things that remain to be done, etc.  

\subsection{See Also}

% A list of derived classes, base classes, etc.\ closely related to
% \texttt{MyClass}. 

\begin{itemize}
\item \texttt{MixerState}
\end{itemize}

\subsection{References}

% A list of references for any algorithms, etc.\ used by \texttt{
% MyClass}.

\input Tail

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
