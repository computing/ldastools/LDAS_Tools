\input Head

\section{\tt IFFT}

% A short (preferably one-line) description of {\tt MyClass}.

{\tt IFFT} --- class to calculate the inverse discrete Fourier transform of a
series using the Fast Fourier Transform algorithm.

\subsection{Description}

% A longer description of what {\tt MyClass} is used for.

The class {\tt IFFT} provides functions for calculating the
inverse discrete Fourier transform (IDFT)
of a series using the Fast Fourier
Transform algorithm. The IDFT of a series is another series
containing the same number of elements.
If the input is regarded as a series in the frequency domain 
then the output can be regarded as a time-series.

If $\{ X_k \}$, $k = 0, 1,\ldots N-1$ represents a
series where each $X_k$ is a real or complex number, then
the inverse discrete Fourier transform $\{ x_j \}$ of $\{ X_k \}$ 
is given by 
$$
x_j = \frac{1}{N} \sum_{k\, =\, 0}^{N - 1} X_k \exp(i 2 \pi j k/N)
$$
where the index $j$ takes the values $j = 0, 1, \ldots, N - 1$.
With these conventions, the result of the IDFT is a real series
provided that $X_{k} = X_{N-k}^*$ for all $k \in 0, 1, \ldots, N - 1$.
We refer to such sequences as ``Hermitian symmetric''.
The Hermitian symmetry condition implies that the DC component
$X_{0}$ is real, and the Nyquist component $X_{\lfloor N/2 \rfloor}$
is real if $N$ is even.

\subsection{Operating Instructions}

\subsubsection{Usage}
Speed and memory requirements of the IDFT computation are greatly
affected by the type of IDFT to be performed and the numerical
precision required. The IDFT of a real sequence can be performed in half
the time and memory space used to perform a complex IDFT of the same
length. Single precision calculations also use half the time
and memory required by the equivalent double precision IDFT. 
For these
reasons four IDFT \verb+apply()+ functions are provided for performing 
the different types of IDFT (real single precision; real double precision;
complex single precision; complex double precision). They all have
the common form
\begin{verbatim}
  template<class T>
  void apply(      std::valarray< P >& out,
             const DFT<complex< T > >& in);
\end{verbatim}
where
\begin{itemize}
\item \verb+P+ is one of the types \verb+float+, \verb+double+
  (for real IDFT) or \verb+complex<float>+, \verb+complex<double>+
  (for complex IDFT).
  These need not be explicitly specified: since \verb+apply()+ is
  a template function the compiler automatically chooses the appropriate
  function based on the data types given as parameters.
\item \verb+T+ is either \verb+float+ or \verb+double+
\item \verb+in+ is an input parameter containing the Fourier series
  data $\{ X_j \}$ where \verb+in[j]+ $= X_j$ and
  \verb+in.size()+ $= N$. The DFT object {\tt in} is said to be
  Hermitian symmetric if \verb+out[k] = conj(out[N-k])+ for all {\tt k}.
  In that case the
  function \verb+out.IsSymmetric()+ returns {\tt true} and the {\tt out}
  parameter must be real. If {\tt in} is not Hermitian symmetric then
  {\tt out} must be complex.
\item \verb+out+ is an output parameter containing the IDFT of
  $\{ X_j \}$ where \verb+out[j]+ $= x_j$ and
  \verb+out.size()+ $= N$. 

  Note that
  \verb+out+ need not be initialized to any particular size or
  symmetry, but the IDFT will experience a speed penalty if this is not
  the case, since it must resize the output array before performing the
  IDFT.
\end{itemize}

\subsubsection{UDT Usage}

The {\tt IFFT} class also provides the following method for applying the IFFT
to a UDT object:
\begin{verbatim}
  void apply(udt* out, const udt& in);
\end{verbatim}
where
\begin{itemize}
\item \verb+in+ is a UDT derived from \verb+DFT<complex<T> >+
\item \verb+out+ is either the zero pointer or a pointer to
a \verb+Sequence<T>+, with {\tt T} real or complex as appropriate.
If {\tt out} is zero, a UDT of the appropriate
type to hold the result is created on the heap. This UDT may be re-used
in subsequent calls to {\tt apply()}. It is the users responsibility to
delete it when finished.

If {\tt out} is non-zero, it must point to a UDT of a type appropriate to
holding the result of the FFT.
If the input type is a symmetric
\verb+DFT<complex<T> >+, then the output type must be (or be derived from)
\verb+Sequence<T>+.
If the input type is not symmetric,
then the output type must be (or be derived from)
\verb+Sequence<complex<T> >+.

\end{itemize}


\subsubsection{Example Usage}

% A verbatim block of example code showing proper usage of {\tt
% MyClass}.

\begin{verbatim}
#include "fft.hh"

main()
{
    const size_t N = 8;
    DFT<complex<float> > in(N); 
    Sequence<float> real_out(N);
    Sequence<complex<float> > cplx_out(N);

    udt* r_udt_out_nonzero = &real_out;
    udt* c_udt_out_nonzero = &cplx_out;
    udt* udt_out_zero = 0;

    IFFT ifft;

    // Set Fourier series data
    for (size_t j = 0; j < N; j++)
    {
        in[j] = complex<float>(cos(2.0*M_PI*j/N), sin(2.0*M_PI*j/N));
    }

    // Perform IDFT using valarray method
    if (in.IsSymmetric())
    {
        ifft.apply(real_out, in);
    }
    else
    {
        ifft.apply(cplx_out, in);
    }

    // Perform IDFT using UDT method, without creating output
    if (in.IsSymmetric())
    {
        ifft.apply(r_udt_out_nonzero, in);
        for (size_t k = 0; k < real_out.size(); k++)
        {
            cout << "x[" << k << "] = " << real_out[k] << endl;
        }
    }
    else
    {
        ifft.apply(c_udt_out_nonzero, in);
        for (size_t k = 0; k < cplx_out.size(); k++)
        {
            cout << "x[" << k << "] = " << cplx_out[k] << endl;
        }
    }

    // Perform IDFT using UDT method, creating output
    ifft.apply(udt_out_zero, in);

    // Cast udt_out to Sequence<complex<float> >
    Sequence<complex<float> >& x
        = dynamic_cast<Sequence<complex<float> >&>(*udt_out_zero);
  
    for (size_t k = 0; k < x.size(); k++)
    {
        cout << "x[" << k << "] = " << x[k] << endl;
    }

    // Remember to delete the output!
    delete udt_out_zero;
}
\end{verbatim}

\subsection{Algorithms}

% A discussion of the algorithms used by {\tt MyClass}.

{\tt IFFT} uses the Fast Fourier Transform algorithm as implemented by
FFTW (``Fastest Fourier Transform in the West''). It is fastest when
the sequence length $N$ can be expressed in the form $2^k 3^l 5^m$,
in which case it is $O(N \log N)$.
When $N$ is not of this form the $O(N^2)$ discrete Fourier transform is
used.

\subsection{Accuracy}

% A discussion of issues related to the accuracy of numerical routines
% used by {\tt MyClass}---e.g., approximations,  argument ranges, etc.

The precision of the IFFT is determined by the precision of the input
data.
If the input is in single precision, the IFFT is performed in
single precision. A single precision IFFT is guaranteed to agree
with the IFFT as calculated by Matlab (which uses double precision)
to an absolute tolerance of $10^{-5}$.

If the input is in double precision, the IFFT is performed in
double precision. A double precision IFFT is guaranteed to agree
with the IFFT as calculated by Matlab
to an absolute tolerance of $10^{-9}$.

\subsection{Performance}

% A discussion of issues related to the speed (e.g., $O(N\log N)$) of
% computationally demanding routines used by {\tt MyClass}.
When $N$ is of the form $2^k 3^l 5^m$,
the algorithm is of order $O(N \log N)$, otherwise it is
of order $O(N^2)$.

\subsection{Testing}

% A description of any test code/test classes that were used to test
% {\tt MyClass}. 

\begin{itemize}
\item {\tt tSfft.cc} -- checks accuracy of single-precision complex FFT.
\item {\tt tDfft.cc} -- checks accuracy of double-precision complex FFT.
\item {\tt tSrealfft.cc} -- checks accuracy of single-precision real FFT.
\item {\tt tDrealfft.cc} -- checks accuracy of double-precision real FFT.
\item {\tt tFFTPerf.cc} -- checks performance of the FFT.
\item {\tt tFFT\_thread.cc} -- checks running of the FFT in multiple
simultaneous threads.
\end{itemize}

\subsection{Notes}

% Miscellaneous notes about {\tt MyClass} that do not appear
% elsewhere, things that remain to be done, etc.  
None

\subsection{See Also}

% A list of derived classes, base classes, etc.\ closely related to
% {\tt MyClass}. 

\begin{itemize}
\item {\tt DFT}
\item {\tt FFT}
\end{itemize}


% \subsection{References}

% A list of references for any algorithms, etc.\ used by {\tt
% MyClass}. 
\begin{thebibliography}{99}

\bibitem{FFTW:IFFT}
The FFTW Home Page, {\tt www.fftw.org}

\end{thebibliography}

\input Tail


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
