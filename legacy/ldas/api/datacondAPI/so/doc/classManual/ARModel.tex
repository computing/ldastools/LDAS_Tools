\input Head

\section{\tt ARModel}

% A short (preferably one-line) description of {\tt MyClass}.

System identification and prediction using an Auto-Regression (AR) model.

% \subsection{User Interface}

% % The class definition from the header file

\begin{verbatim}

#ifndef ARMODEL_HH
#define ARMODEL_HH

// $Id: ARModel.tex,v 1.1 2001/08/21 23:36:35 ldas_anu Exp $

#include <valarray>
#include <stdexcept>

#include "StateUDT.hh"

namespace datacondAPI
{

    class ARModel : public State
    {

    public:

	// default overrides


	//: Default constructor
	ARModel();

	//: Copy constructor
	//!param: ar - Original
	ARModel(const ARModel& ar);

	//: Destructor
	~ARModel();

	//: Assignment operator
	//!param: ar - Original
	//!return: reference to self
	ARModel& operator=(const ARModel& ar);

	// UDT interface

	//: Deep copy (UDT functionality)
	//!return: pointer to deep copy
	ARModel* Clone() const;

	ILwd::LdasElement*
	    ConvertToIlwd(const CallChain& Chain,
			  udt::target_t Target = udt::TARGET_GENERIC) const;

	// custom constructors

	//: Construct model of a particular order
	//!param: n - model order
	explicit ARModel(const int& n);

	//: Construct a model from its coefficents
	//!param: theta - model coefficients
	template<typename type>
	explicit ARModel(const std::valarray<type>& theta);

	//: Construct a model from a UDT (either order or coefficents)
	//!param: n_or_theta - scalar order or array of coeffcients
	explicit ARModel(const udt& n_or_theta);

	//: Construct a model of a particular order fitted to given system output
	//!param: x - time series of model output
	//!param: n - desired model order
	template<typename type>
	ARModel(const std::valarray<type>& x, const int& n);

	//: Construct a model of a particular order fitted to given system output
	//!param: x - time series of model output
	//!param: n - desired model order
	ARModel(const udt& x, const udt& n);

	// accessors

	//: Get the model order
	//!return: model order
	int getOrder() const;

	//: Get the model order
	//!param: order - pointer to scalar integer (may be null on input)
	void getOrder(udt*& order) const;

	//: Get the model coefficents
	//!param: theta - reference to output array
	template<typename type>
	void getTheta(std::valarray<type>& theta) const;

	//: Get the model coefficents
	//!param: theta - pointer to output array (may be null on input)
	void getTheta(udt*& theta) const;

	//: Get the FIR filter coefficents corresponding to the model
	//!param: filter - pointer to output array (may be null on input)
	void getFilter(udt*& filter) const;

	// mutators

	//: Set the model order (destroys any existing model)
	//!param: n - new model order
	void setOrder(const int& n);
	
	//: Set the model order (destroys any existing model)
	//!param: n - new model order (scalar integer)
	void setOrder(const udt& n);

	//: Set the model coefficents (destroys any existing model)
	//!param: theta - new model coefficents
	template<typename type>
	void setTheta(const std::valarray<type>& theta);

	//: Set the model coefficents (destroys any existing model)
	//!param: theta - new model coefficents (array)
	void setTheta(const udt& theta);

	// functionality

	//: Apply the model to system output, producing a prediction.  A model must be estimated before calling.
	//!param: y - model prediction
	//!param: x - system output (model input)
	template<typename type>
	void apply(std::valarray<type>& y,
		   const std::valarray<type>& x) const;

	//: Apply the model to system output, producing a prediction.  A model must be estimated before calling.
	//!param: y - model prediction
	//!param: x - system output (model input)
	void apply(udt*& y,
		   const udt& x) const;

	//: (Re)fit the model to (additional) system output x.  A model order must be set before calling.
	//!param: x - system output (model input)
	template<typename type>
	void refine(const std::valarray<type>& x);

	//: (Re)fit the model to (additional) system output x.  A model order must be set before calling.
	//!param: x - system output (model input)
	void refine(const udt& x);

	//: Compute model's merit as the least-squares norm of the difference between the system output and prediction for given system output (model input).
	//!param: x - system output (model input)
	//!return: model merit
	template<typename type>
	type merit(const std::valarray<type>& x) const;

	//: Compute model's merit as the least-squares norm of the difference between the system output and prediction for given system output (model input).
	//!param: m - (scalar) model merit (may be null on input)
	//!param: x - system output (model input)
	void merit(udt*& m, const udt& x) const;

    private:

	class Abstraction;
	template<typename type> class Implementation;

	int m_order;
	mutable Abstraction* m_data;

    };

} // namespace datacondAPI

#endif // ARMODEL_HH

\end{verbatim}

\subsection{Description}

% A longer description of what {\tt MyClass} is used for.

The {\tt ARModel} class takes a time series $y_i$ the output of a ``system'') and produces an FIR linear filter.
The linear filter predicts $y_i$ from $y_{i-1} \ldots y_{i-n}$, and is an optimal fit in the sense that the square-difference between the prediction and actual value is minimal.

The prediction can be used to subtract deterministic components from the signal (whitening) or as a step in estimating a PSD.

\subsection{Operating Instructions}

An {\tt ARModel} must be constructed, have its order $n$ set, be fitted to data using {\tt refine} (multiple calls may be made to continue refinement over broken sequences), and then applied to data with {\tt apply} to produce a prediction.  Some of these steps may be integrated into variants of the constructor.  The ``goodness of fit'' of a particlar prediction on a particular piece of data may be measured with the {\tt merit} method (lower is better).  Overloaded UDT and C++ interfaces to all methods are provided.

\subsubsection{Example Usage}

% A verbatim block of example code showing proper usage of {\tt
% MyClass}.

\begin{verbatim}

ARModel ar1, ar2;                                  // construct blank models
ar1.setOrder(5);                                   // set different model orders
ar2.setOrder(10)
ar1.refine(output);                                // estimate different models
ar2.refine(output);
if (ar1.merit(prediction) < ar2.merit(prediction)) // test for the better model
{
    ar1.apply(prediction, output);                 // use the best model to predict
}
else
{
    ar2.apply(prediction, output);
}
output -= prediction;                              // whiten the system output

\end{verbatim}

% \subsubsection{Options/Defaults}

% % A list of options or defaults for public methods or member data of
% % {\tt MyClass}. 

% \subsection{Exceptions}

% \subsubsection{Exceptions Thrown}

% % A list of exceptions thrown by {\tt MyClass}.

UDT methods throw on invalid types.
Numerical methods throw on empty model.
Numerical methods may forward linear algebra exceptions.

% \subsubsection{Exceptions Handled}

% % A list of exceptions handled by {\tt MyClass}.

\subsection{Algorithms}

% A discussion of the algorithms used by {\tt MyClass}.

Uses a simple linear equation solver from CLAPACK...

\subsection{Accuracy}

% A discussion of issues related to the accuracy of numerical routines
% used by {\tt MyClass}---e.g., approximations,  argument ranges, etc.

Could be made more robust.

\subsection{Performance}

% A discussion of issues related to the speed (e.g., $O(N\log N)$) of
% computationally demanding routines used by {\tt MyClass}.

\subsection{Testing}

% A description of any test code/test classes that were used to test
% {\tt MyClass}. 

Test code exists.  In addition to control flow testing, recovery of a linear filter from coloured gaussian noise is tested (?).

\subsection{Notes}

% Miscellaneous notes about {\tt MyClass} that do not appear
% elsewhere, things that remain to be done, etc.  

\subsection{See Also}

% A list of derived classes, base classes, etc.\ closely related to
% {\tt MyClass}. 

{\tt ARModelFunction} wraps an action using ARModel.
{\tt OEModel} is another system identification class.

\subsection{References}

% A list of references for any algorithms, etc.\ used by {\tt
% MyClass}. 

Ljung...

\input Tail

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
