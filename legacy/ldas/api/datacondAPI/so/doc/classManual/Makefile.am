RECURSIVE_TARGETS=$(PROJECT_RECURSIVE_TARGETS)

DVIFILES = \
	datacondAPI.dvi

if HAVE_PS2PDF
PDFFILES = \
	datacondAPI.pdf
endif

EXTRA_DIST = \
	$(datacondAPI_TEXSOURCE)

datacondAPI_TEXSOURCE = \
	AbsFunction.tex \
	AdditionFunction.tex \
	ArgFunction.tex \
	CSDEstimate.tex \
	ClearFunction.tex \
	ConjFunction.tex \
	DivisionFunction.tex \
	DoubleFunction.tex \
	FFT.tex \
	FIRLP.tex \
	HannWindow.tex \
	HannWindowFig1.ps \
	Head.tex \
	IFFT.tex \
	ImagFunction.tex \
	KFIRLP.tex \
	KaiserWindow.tex \
	KaiserWindowFig1.ps \
	LinFilt.tex \
	LinFiltFig1.tex \
	LinFiltFig2.tex \
	LinFiltFig3.tex \
	LinFiltFig4.tex \
	LinFiltFig5.tex \
	LinFiltState.tex \
	Mixer.tex \
	MixerState.tex \
	MultiplicationFunction.tex \
	MyClass.tex \
	MyFunction.tex \
	PSDEstimate.tex \
	RealFunction.tex \
	RectangularWindow.tex \
	Resample.tex \
	State.tex \
	Statistics.tex \
	SubtractionFunction.tex \
	Tail.tex \
	TypeConversion.tex \
	UDT.tex \
	UDTMetaData.tex \
	UnitTest.tex \
	ValueFunction.tex \
	WelchCSDEstimate.tex \
	WelchCSDEstimateFig1.ps \
	WelchEstimate.tex \
	WelchEstimateFig1.ps \
	WelchPSDEstimate.tex \
	WelchPSDEstimateFig1.ps \
	Window.tex \
	WindowInfo.tex \
	datacondAPI.tex \
	sinc.tex \
	tpageLIGO.tex \
	ResampleState.tex \
	unimplemented_error.tex \
	warning.tex \
	ARModel.tex \
	ARModelFunction.tex \
	LineRemover.tex \
	OEModel.tex \
	oelslrFunction.tex \
	oelslrdFunction.tex \
        Wavelet.tex \
        WaveDWT.tex \
        Haar.tex \
        Biorthogonal.tex \
        Daubechies.tex \
        Symlet.tex \
        WaveletUDT.tex \
        HaarFunction.tex \
        BiorthogonalFunction.tex \
        DaubechiesFunction.tex \
        SymletFunction.tex \
        WaveletForward.tex \
        WaveletInverse.tex \
        GetLayer.tex \
        SignumFunction.tex


all-local : $(PDFFILES)

clean-local :
	@rm -f *.dvi *.aux *.toc *.log *.pdf *.ps

# If there's no aux file to begin with, you usually need
# to run LaTeX one additional time
datacondAPI.aux: datacondAPI.dvi
	env TEXINPUTS=$(srcdir): \
	latex $(srcdir)/datacondAPI;
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file"; \
		touch $(srcdir)/datacondAPI.tex; \
		false; \
	fi

# Need to run LaTeX twice to make sure the cross-refs are ok
datacondAPI.dvi: $(datacondAPI_TEXSOURCE)
	env TEXINPUTS=$(srcdir): \
	latex $(srcdir)/datacondAPI; \
	env TEXINPUTS=$(srcdir): \
	latex $(srcdir)/datacondAPI; \
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file"; \
		touch $(srcdir)/datacondAPI.tex; \
		false; \
	fi

datacondAPI.ps : datacondAPI.dvi
	env TEXINPUTS=$(srcdir): \
	dvips datacondAPI -o datacondAPI.ps

if HAVE_PS2PDF
datacondAPI.pdf : datacondAPI.ps
	ps2pdf datacondAPI.ps
endif

include $(ldas_top_srcdir)/build/make-macros/memory-debug.mk
