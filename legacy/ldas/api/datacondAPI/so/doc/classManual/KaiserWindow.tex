\input Head

\section{\tt KaiserWindow}

A class representing a Kaiser window.

\subsection{Description}

{\tt KaiserWindow} is a class representing a Kaiser window, 
which can be used for low-pass filtering and spectral density estimation.
{\tt KaiserWindow} derives from the abstract base class {\tt Window}.

The sample values $w[i]$ of a Kaiser window are defined by:
%
\[
w[i]:=
\left\{
\begin{array}{ll}
{I_0[\beta(1-[2i/(N-1)-1]^2)^{1/2}]\over I_0(\beta)}
& \quad 0\le i< N,\\
0
& \quad {\rm otherwise}\ ,
\end{array}
\right.
\]
%
where $N$ is the length of the window, $I_0(x)$ is the 0th-order 
modified Bessel function of the first kind, and $\beta\ge 0$ is a 
shape parameter related to the amplitude of the sidelobes of the 
Fourier transform of the window. 
(See Oppenheim and Schafer\cite{KWOS} for details.).
If $N=1$, $w[0]$ is defined to be 1.
When $\beta=0$, a Kaiser window reduces to a rectangular window.

To obtain a Kaiser window with a sidelobe height of
$-\alpha$ dB, one should use the following value of $\beta$ \cite{KWOS}:
%
\[
\beta=
\left\{
\begin{array}{ll}
0.1102(\alpha-8.7)
& \quad \alpha>50\ ,\\
0.5842(\alpha-21)^{0.4}+0.07886(\alpha-21)
& \quad 21\le\alpha\le 50\ ,\\
0.0
& \quad \alpha<21\ ,
\end{array}
\right.
\]
Figure~\ref{f:KaiserWindowFig1} shows a plot of two Kaiser windows, both 
having length 1024, but different shape parameters, $\beta =3$ 
and $\beta=6$, respectively.
%
\begin{figure}[htb!]
\begin{center}
{\epsfig{file=KaiserWindowFig1.ps,
angle=-90,width=4in,bbllx=25pt,bblly=50pt,bburx=590pt,bbury=740pt}}
\caption{\label{f:KaiserWindowFig1}
Kaiser windows having length $N=1024$ and shape parameters
$\beta = 3$ and $\beta = 6$, respectively.}
\end{center}
\end{figure}
%

{\tt KaiserWindow} inherits most of its functionality from the 
abstract base class {\tt Window}.
(See {\tt Window} for more details.)
The only public method defined in {\tt KaiserWindow} that is not 
declared in {\tt Window} is {\tt set\_beta}, which lets the user 
set the value of the Kaiser window shape parameter $\beta$.

\subsection{Operating Instructions}

\subsubsection{Example Usage}

{\footnotesize 
\begin{verbatim}

  // calculate the values of a Kaiser window of length 1024 for two 
  // different values of the shape parameter beta (beta = 3, 6).
  // then write the results to output files "KW3.dat" and "KW6.dat"

  Sequence<float> in(1,1024); // input sequence (all 1's)
  Sequence<float> out;        // output sequence

  KaiserWindow kw(3);  // kaiser window (beta = 3)

  // apply the window to the input data
  kw.apply(out, in);

  // write window values to a file
  ofstream outfileKW3("KW3.dat");
  for (int i = 0; i < 1024; i++ ) 
  {
      outfileKW3 << i << "  " << out[i] << endl;
  }

  // change value of beta 
  kw.set_beta(6);         

  // apply the window to the input data
  kw.apply(out, in);

  // write window values to a file
  ofstream outfileKW6("KW6.dat");
  for (int i = 0; i < 1024; i++ ) 
  {
      outfileKW6 << i << "  " << out[i] << endl;
  }

\end{verbatim}}

\subsection{Algorithms}
\label{ss:KWalgorithms}

{\tt KaiserWindow} contains a private method 
{\tt I0(const double\& x)}, which returns the value of the zero-th 
order modified Bessel function of the first kind $I_0(x)$.
This routine calculates $I_0(x)$ using the power series 
expansion \cite{KWAS}:
%
\begin{equation}
I_0(x)=
1
+{{1\over 4}x^2\over (1!)^2}
+{\left({1\over 4}x^2\right)^2\over (2!)^2}
+{\left({1\over 4}x^2\right)^3\over (3!)^2}
+\ldots
\end{equation}
%
Terms are added to the sum until the next higher-order term is 
$\le 2\times 10^{-9}$.

\subsection{Accuracy}

\begin{itemize}
\item
The accuracy of the power series expansion used to calculate the 
value of the Bessel function $I_0(x)$ is $2\times 10^{-9}$.
(See Sec.~\ref{ss:KWalgorithms} for details.)
\item
The precision of the other methods that are common to all windows 
are described in {\tt Window}.
\end{itemize}

\subsection{Performance}

See {\tt Window}.

\subsection{Testing}

{\tt KaiserWindow} is tested (along with all the other windows 
classes) by the code contained in {\tt tWindow.cc}.
The following tests are performed:
%
\begin{itemize}
\item
test that proper exceptions are thrown by the Kaiser window 
constructor, {\tt set\_beta}, {\tt apply}, and {\tt operator()} 
methods.
\item
test that the {\tt set\_beta} method correctly sets the value 
of the Kaiser window shape parameter $\beta$.
\item
test that the mean, rms, and Kaiser window values are correct for 
trivial and non-trivial window lengths and a non-zero value of $\beta$.
\item 
test that the output values are correct when a Kaiser window with 
a non-zero value of $\beta$ is applied to non-trivial real and 
complex input sequences.
\end{itemize}
%
When testing {\tt KaiserWindow}, we compare the output of the
Kaiser window methods with results previously calculated with Matlab.
The output of the Kaiser window methods are said to be in agreement 
with the results produced by Matlab if the difference is 
$\le 10^{-5}$.
{\tt KaiserWindow} passes all of the above tests.

\subsection{Notes}

None.

\subsection{See Also}

\begin{itemize}
\item 
class {\tt Window}
\item 
class {\tt WindowInfo}
\item
class {\tt RectangularWindow}
\item
class {\tt HannWindow}
\end{itemize}

\begin{thebibliography}{99}

\bibitem{KWOS}
A.V.\ Oppenheim and R.W. Schafer,
{\em Discrete-Time Signal Processing\/},
(New Jersey, Prentice-Hall, 1999), p.474.

\bibitem{KWAS}
M.\ Abramowitz and I.A.\ Stegun, eds.\ 
{\em Handbook of Mathematical Functions\/},
(New York, Dover, 1972), 9.6.12.

\bibitem{KWmatlab} 
The MathWorks: {\tt http:\slash\slash www.mathworks.com}.

\end{thebibliography}

\input Tail

