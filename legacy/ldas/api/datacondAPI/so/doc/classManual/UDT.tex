\input Head

\section{{\tt UDT} (Unified Data Type)}

% A short (preferably one-line) description of {\tt MyClass}.

The UDT is the base type of all data types acted on by the DataConditioningAPI.

\subsection{User Interface}

% The class definition from the header file

\begin{verbatim}

namespace datacondAPI {

  //---------------------------------------------------------------------
  //: Universal Data Type (UDT)
  //
  // <p>
  // This class is the base for all datatypes used in the Data
  // Conditioning API.
  //
  // <p>
  // Constructing a new datatype:
  // 1) Create a new header file (.hh) and a new source file (.cc).
  //    The header file should contain only the class declaration and
  //    any inline declaration. The source file will contain the
  // 2) Modify datacondAPI::TypeInfoTable_t::initialize()
  //    (in TypeInfo.cc) to add handling of new type.
  // 3) If an LDAS user should be able to create an instance of the
  //    class, then add appropriate constuctor functions classes
  //    to DataTypeFunctions.hh and the support code to
  //    DataTypeFunctions.cc.
  //---------------------------------------------------------------------
  class udt
  {
  public:
    typedef enum {
      TARGET_GENERIC,	// Default target
      TARGET_METADATA,
      TARGET_WRAPPER
    } target_t;

    // Constructor
    udt(void);
    
    // Destructor
    virtual ~udt();

    // Add to what has happened to this object.
    void AppendHistory(std::string Action, ... );

    // Cast the UDT to another type
    template<class T> static const T&  Cast(const udt& In);

    // Cast the UDT to another type
    template<class T> static T&  Cast(udt& In);

    // Virtual copy constructor
    virtual datacondAPI::udt* Clone(void) const = 0;

    //: Convert the data to an appropriate ilwd type
    //
    //If no conversion is possible, then a null pointer should be returned
    //
    //!param: const CallChain& Chain - CallChain in which UDT was created.
    //!param: target_t Target - Type of conversion to be performed.
    //
    //!return: ILwd::LdasElement* - The converted type or NULL if no
    //+        conversion possible.
    virtual ILwd::LdasElement*
    ConvertToIlwd(const CallChain& Chain,
		  target_t Target = TARGET_GENERIC) const = 0;

    //: Convert the data to an appropriate ilwd type
    //
    //!param: const CallChain& Chain - CallChain in which UDT was created.
    //!param: target_t Target - Type of conversion to be performed.
    //!param: ILwd::LdasContainer* - Container to receive the ilwd
    //
    //!return: none
    //
    void ConvertToIlwd(const CallChain& Chain,
		       target_t Target,
		       ILwd::LdasContainer* Container) const;

    // Test the validity of the UDT object
    //
    //!return: bool - true if valide, false otherwise.
    //
    virtual bool IsValid(void) const = 0;

    // See if the UDT has been derived from some other type
    template<class T>
    static bool IsA(const udt& In);

  private:
    // Keeps track of what has happened to this object.
#if UDT_HISTORY
    ILwd::LdasHistory	m_history;
#endif	/* UDT_HISTORY */

  };	/* udt */

  ///--------------------------------------------------------------------
  /// Inline function definitions
  ///--------------------------------------------------------------------
};	/* namespace - datacondAPI */

\end{verbatim}

\subsection{Description}

% A longer description of what {\tt MyClass} is used for.

The UDT defines the basic interface of all data types in the
DataConditioningAPI, allowing them to be treated polymorphically where their
type is not important, and guaranteeing all data types have certain
functionality, such as an assertion of validity, a History and the ability to bne converted to an ILWD.

\subsection{Operating Instructions}

\subsubsection{Example Usage}

% A verbatim block of example code showing proper usage of {\tt
% MyClass}.

The UDT is an abstract base class, and as such it is utilised by deriving new types, such as {\tt Sequence$\mathtt{<}$T$\mathtt{>}$}.

\begin{verbatim}

namespace datacondAPI
{
  //---------------------------------------------------------------------
  // This is the base class for sequence data descriptors.
  //---------------------------------------------------------------------

  template <class DataType_t>
  class Sequence : public udt, public std::valarray<DataType_t>
  {
  public:
    // Constructor
    Sequence();
    
    // Constructor
    Sequence(size_t Size);

    // Constructor
    Sequence(const DataType_t& Value, size_t Size);
    
    // Constructor
    Sequence(const std::valarray<DataType_t>& Value);
    
    // Copy constructor
    Sequence(const Sequence& Value);
    
    // Constructor
    Sequence(const DataType_t* Data, size_t Size);

    // Constructor
    template<class ArrayData_t>
    Sequence(const ArrayData_t* Data, size_t Size);

    // Destructor
    virtual ~Sequence();
    
    //: Copy assignment - I have no clue if this should be virtual
    //
    // NOTE: Unlike valarray, this copy assignment is safe to use
    // even if the lhs has a different size to the rhs
    virtual Sequence& operator=(const Sequence& rhs);

    // forwarded methods
    virtual valarray<DataType_t>& operator=(const valarray<DataType_t>&);
    virtual valarray<DataType_t>& operator=(const DataType_t&);
    virtual valarray<DataType_t>& operator=(const slice_array<DataType_t>&);
    virtual valarray<DataType_t>& operator=(const gslice_array<DataType_t>&);
    virtual valarray<DataType_t>& operator=(const mask_array<DataType_t>&);
    virtual valarray<DataType_t>& operator=(const indirect_array<DataType_t>&);

    // Virtual copy constructor
    virtual Sequence* Clone() const;

    // Convert the data to an appropriate ilwd typ
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_t Target) const;

    // Ensure the UDT is valid
    virtual bool IsValid() const;

  };	/* Sequence<T> */

};	/* namespace - Sequence<T> */

\end{verbatim}

\subsubsection{Options/Defaults}

% A list of options or defaults for public methods or member data of
% {\tt MyClass}. 

None.

\subsection{Exceptions}

\subsubsection{Exceptions Thrown}

% A list of exceptions thrown by {\tt MyClass}.

{\tt std::bad\_cast} may be thrown by calling {\tt udt::Cast$\mathtt{<}$T$\mathtt{>}$} without first
checking the validity of the cast with {\tt udt::IsA$\mathtt{<}$T$\mathtt{>}$}.

Other exceptions may be thrown by derived types at their discresion.  There are no exception specfifications.

\subsubsection{Exceptions Handled}

% A list of exceptions handled by {\tt MyClass}.

None.

\subsection{Algorithms}

% A discussion of the algorithms used by {\tt MyClass}.

None.

\subsection{Accuracy}

% A discussion of issues related to the accuracy of numerical routines
% used by {\tt MyClass}---e.g., approximations,  argument ranges, etc.

Not relevant.

\subsection{Performance}

% A discussion of issues related to the speed (e.g., $O(N\log N)$) of
% computationally demanding routines used by {\tt MyClass}.

The type resolution methods {\tt IsA} and {\tt Cast} are O(1) but should be
regarded as expensive and used sparingly.

\subsection{Testing}

% A description of any test code/test classes that were used to test
% {\tt MyClass}. 

The UDT is tested indirectly by the test suites of its derived types and the
token test suite.

\subsection{Notes}

% Miscellaneous notes about {\tt MyClass} that do not appear
% elsewhere, things that remain to be done, etc.  

\subsection{See Also}

% A list of derived classes, base classes, etc.\ closely related to
% {\tt MyClass}. 

\subsubsection{Derived classes}

{\tt Scalar}, {\tt Sequence} and {\tt State}.  Many other clases indirectly inherit from {\tt UDT} via these classes.

\subsubsection{Related classes}

{\tt UDTMetaData} (UMD).

\subsection{References}

% A list of references for any algorithms, etc.\ used by {\tt
% MyClass}. 

\input Tail

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
