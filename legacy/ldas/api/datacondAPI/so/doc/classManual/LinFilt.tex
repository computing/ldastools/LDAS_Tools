%% $Id: LinFilt.tex,v 1.2 2002/08/16 01:24:22 Philip.Charlton Exp $

\input Head

\section{\tt LinFilt}

{\tt LinFilt} --- applies a linear filter to a possibly segmented sequence.


\subsection{Description}

{\tt LinFilt} applies an arbitrary linear filter to a sequence. The
linear filter is specified in transfer function form: i.e., by the
coefficients $a$ and $b$ in 
\begin{equation}
\sum_{j\,=\,0}^{N_A} a[j]y[k-j]
= \sum_{j\,=\,0}^{N_B} b[j]x[k-j]  
\end{equation}
where $x$ is the input sequence and $y$ the output sequence. 

\subsection{Operating Instructions}

\subsubsection{Usage}

\texttt{LinFilt} is initialized on construction. The filter is applied
via the overloaded \texttt{apply} method
\begin{verbatim}
//: Basic filter action
//!param: out - result of filter acting on in. Existing contents destroyed.
//!param: in - input data for filter.
template<typename out_type, typename in_type>
void apply(std::valarray<out_type>& out, 
           const std::valarray<in_type>& in);
 //: Interpreted filter action (metadata interpreted & set)
//!param: out - points to result of linear filtering in. May be null on 
//+ entry, in which case the appropriate output object is allocated
//+ on heap. 
//!param: in - data on which linear filter is to act
void apply(udt*& out, const udt& in); 
\end{verbatim}
The type of the object \texttt{out} will always be the same as the
type of the object \texttt{in}. 

\subsubsection{Example}

\begin{quote}
\footnotesize
\begin{verbatim}
// a, b are transfer function coefficients. 
// These must be of type std::valarray<double>
std::valarray<double> a;
std::valarray<double> b;
.
.
.
// input x may be of type of type std::valarray<T>, Sequence<T> or 
// TimeSeries<T>, where T is float, double, or complex<float>,
// complex<double>. The result y is of the same type as the input x. 
datacondAPI::TimeSeries<complex<float> > x;
datacondAPI::TimeSeries<complex<float> > y;
.
.
.
// Create LinFilt object:
datacondAPI::LinFilt lf(b,a);

// Filter x, producing y
lf.apply(y,x);

// lf has an internal state, which it maintains. If additional points
// x become available these can be filtered by the same object, in
// which case the result, when appended to the original output, will be
// the same as if all the input were filtered in one go. 

// We can make and preserve a copy of the internal state, and use it
// to set another LinFilt object:
datacondAPI::LinFiltState lfstate;
lf.getState(lfstate);
datatcondAPI::LinFilt lfdup(lfstate);
// The result of filtering x by lfdup is identical to the result of
// filtering x by lf. 
\end{verbatim}
\end{quote}

\subsubsection{Options/Defaults}

% A list of options or defaults for public methods or member data of
% LinFilt. 

None. 

\subsection{Exceptions}

\subsubsection{Exceptions Thrown}

\begin{itemize}
\item getState(udt\*\& state)
  \begin{itemize}
  \item \texttt{std::invalid\_argument} when state not null or a pointer to 
    \texttt{datacondAPI::LinFiltState}
  \item \texttt{std::bad\_alloc} when state null and new fails
  \end{itemize}
\item apply(udt\*\& out, const udt\& in)
  \begin{itemize}
  \item \texttt{std::bad\_alloc} when out null and new fails
  \item std::unimplemented\_error when in or out not of appropriate
    type (Sequence$<$T$>$ or TimeSeries$<$T$>$ where T either float,
    double, complex$<$float$>$ or complex$<$double$>$)
  \item std::invalid\_argument when type \*udt not same as type in
  \end{itemize}
\item apply(std::valarray<Tout>\& out, const std::valarray<Tin>\& in)
  \begin{itemize}
  \item std::invalid\_argument when in has zero length
  \end{itemize}
\end{itemize}

\subsubsection{Exceptions Handled}

None.

\subsection{Algorithms}

The output sequence $y[k]$  is related to the input sequence $x[k]$
according to 
\begin{equation}
  a[0]y[k] = \sum_{j\,=\,0}^{N_b}b[j]x[k-j] - \sum_{j\,=\,1}^{N_a}a[j]y[k-j]
\end{equation}
The filter is implemented in transposed type II form, following the
recursion relation for each consecutive $k$:
\begin{eqnarray}
y[k] &=& b[0] x[k] + z[0]\nonumber \\
z[0] &=& b[1] x[k] - a[1] y[k] + z[1]\nonumber \\
z[1] &=& b[2] x[k] - a[2] y[k] + z[2]\nonumber \\
&\vdots&\nonumber \\
z[N_z-1] &=& b[N_z] x[k] - a[N_z] y[k] + z[N_z]\nonumber \\
z[N_z] &=& b[N_z+1] x[k] - a[N_z+1] y[k]\nonumber
\end{eqnarray}
where $N_z = \max(N_a, N_b) - 1$ and the coefficient haves been padded
out to length $N_z + 2$ with zeroes.
The \textit{state vector} $z$ is preserved, together with the transfer
function coefficients, in a \texttt{LinFiltState} object.

\subsection{Accuracy}

Calculations are performed in the precision of the input type. 

\subsection{Performance}

${\cal O}(NM)$, where $N$ is $x.size()$ and $M$ is
$\max(b.size(),a.size())$. 

\subsection{Testing}

\texttt{LinFilt} is tested by the program \texttt{tLinfilt}. Checks
include 
\begin{itemize}
\item Exceptions
\item MA filtering
\item AR filtering 
\item ARMA filtering
\end{itemize}
on both real and complex sequences.

\subsection{Notes}

FIR filter implementation could be speeded by a properly tuned
frequency-domain implementation. 

\subsection{See Also}

\begin{itemize}
\item datacondAPI::LinFiltState
\item datacondAPI::Sequence
\item datacondAPI::TimeSeries
\item std::valarray
\end{itemize}

\input Tail

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
