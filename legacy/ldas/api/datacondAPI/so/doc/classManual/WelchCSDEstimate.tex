\input Head

\section{\tt WelchCSDEstimate}

A class for estimating cross-spectral densities(CSDs) using Welch's 
method \cite{WelchP&W,WelchO&S}.

\subsection{Description}
\label{Welchss:description}

{\tt WelchCSDEstimate} is a class for estimating cross-spectral 
densities (CSDs) using Welch's method \cite{WelchP&W,WelchO&S}.
It derives from the abstract base class {\tt CSDEstimate}.
 
In Welch's method, CSDs are estimated by averaging cross-spectra
of (possibly) overlapping segments of the input data.
A Welch CSD estimate is defined by an fft length, overlap length, 
a choice of window, and detrend method:
%
\begin{itemize}
\item{}
The fft length is an integer that must be greater than zero
and less than or equal to the length of the input data.
If the length of the input data is less than the fft length, 
the {\tt apply} method calls the {\tt set\_fftLength} 
method to change fft length to equal the length of the data.
\item{}
The overlap length is an integer that must be greater than or
equal to zero and less than the fft length.
\item{}
One can use a rectangular, Hann, or Kaiser windows to window 
individual data segments before calculating the discrete Fourier 
transforms (DFTs).
Additional windows---e.g., Bartlett, Blackman, and Hamming 
windows---may be added in the future.
(See {\tt Window} and the associated window classes for more 
information.)
\item{}
The available detrend methods are:
{\tt none}, {\tt mean}, or {\tt linear}.
These, respectively, do nothing, remove a mean, or remove 
a linear trend from the pre-windowed segments of data.
(See {\tt CSDEstimate} for information about set and 
accessor methods for the {\tt detrendMethod} variable.)
\end{itemize}

In addition to the functionality defined in the abstract base 
class {\tt CSDEstimate}, {\tt WelchCSDEstimate} defines the 
following public methods:
%
\begin{description}
%
\item[{\tt set\_fftLength}:]
Sets the value of the fft length.
%
\item[{\tt set\_overlapLength}:]
Sets the value of the overlap length.
%
\item[{\tt set\_window}:]
Sets the window function.
%
\item[{\tt what}:]
Returns a string containing the name of tbe CSD estimate:
{\tt "Welch CSD Estimate"}.
%
\item[{\tt fftLength}:]
Returns the value of the current fft length.
%
\item[{\tt overlapLength}:]
Returns the value of the current overlap length.
%
\item[{\tt windowType}:]
Returns a {\tt WindowInfo} object containing information about
the current window---i.e., the name of the window and a parameter 
value (if any) needed to define the window.
(See {\tt WindowInfo} for details.)
%
\item[{\tt apply}:]
Returns the 2-sided CSD estimate.
The units and normalization of the CSD estimate are chosen 
so that Parseval's theorem holds in the following form:
%
\begin{equation}
{1\over{\tt fftLength}}
\sum_{\tt i=0}^{\tt fftLength-1}{\tt csd[i]} =
{1\over{\tt dataLength}}
\sum_{\tt i=0}^{\tt dataLength-1}{\tt conj(x[i])*y[i]}\ ,
\label{Welche:parseval}
\end{equation}
%
where {\tt x[i]} and {\tt y[i]} are the input data arrays.
Note that for {\tt x[i]=y[i]} the rms value of the input data is 
just the square root of the above quantity.

The discrete frequencies at which the CSD estimate is evaluated 
are in units of the Nyquist critical frequency.
They are given by:
%
\begin{equation}
{\tt freqs[i]}=2.0*({\tt i}-\lfloor{\tt fftLength}/2\rfloor)/
{\tt fftLength}\ ,
\label{Welche:freq1}
\end{equation}
%
where
%
\begin{equation}
{\tt i}=0,1,\cdots,{\tt fftLength}-1\ .
\label{Welche:freq2}
\end{equation}
%
Note that {\tt csd[0]} and {\tt freqs[0]} correspond to the most
negative frequency value, not to zero frequency.
The values in {\tt csd[]} are arranged in order of increasing 
frequency, starting with the most negative frequency value and 
increasing in steps of $2.0*{\tt i}/{\tt fftLength}$.
%
\end{description}

\subsection{Operating Instructions}

\subsubsection{Example Usage}
\label{Welchss:usage}

{\footnotesize 
\begin{verbatim}

  // read in colored noise from a file (16384 samples)
  const size_t n = 16384;
  float data[n];
  ifstream inFile("WelchCSDEstimateIn.dat");

  int i = 0;
  while (1) 
  {
      inFile >> data[i]; // must try to read in data before generating an
                         // end-of-file flag.  works ok if file is empty.  
      if (inFile.eof()) 
      {
          break;  
      }
    
      i++;
  }
  
  // Note: x[i]=y[i]
  Sequence<float> xIn(data, n);
  Sequence<float> yIn = xIn;

  // construct a Welch CSD estimate object to estimate the CSD
  // (actually the power spectrum) of the input data.
  // (fft length=1024, no overlap, kaiser window (beta=5), no detrending)
  KaiserWindow  kw(5);
  WelchCSDEstimate welchCSD(1024,0,kw,none);

  // construct pointer to udt to hold output of the apply method 
  udt* p_out((udt*)NULL);

  // calculate the csd estimate
  welchCSD.apply(p_out, xIn, yIn);

  // cast the pointer to udt to a WelchCSDSpectrum object; 
  // a WelchCSDSpectrum contains both the csd estimate and discrete 
  // frequency values
  const WelchCSDSpectrum<std::complex<TOut> >* 
      out(dynamic_cast<const WelchCSDSpectrum<std::complex<TOut> >*>(p_out));
  
  // write the discrete frequency values and magnitude of the CSD 
  // estimate to a file (to be displayed later)
  ofstream outFile("WelchCSDEstimateOut.dat");

  for (int i = 0; i < 1024; i++)
  {
      outFile << out->GetFrequency(i) 
              << "  " 
              << abs( (*out)[i]) 
              << endl;
  }

\end{verbatim}}

The magnitude of the Welch CSD estimate produced by the above code
is plotted in Fig.~\ref{f:WelchCSDEstimateFig1}.
Note that since {\tt x[i]=y[i]}, this is just the power spectrum of 
the input data.
%
\begin{figure}[htb!]
\begin{center}
{\epsfig{file=WelchCSDEstimateFig1.ps,
angle=-90,width = 4in,bbllx=25pt,bblly=50pt,bburx=590pt,bbury=740pt}}
\caption{\label{f:WelchCSDEstimateFig1}
Welch method CSD estimate (in this case a power spectrum) for colored 
noise.
The Welch estimate was defined by an fft length of 1024, no overlap, 
no detrending, and a Kaiser window with shape parameter $\beta = 5$.
The colored noise was produced by passing white noise through a low-pass
finite impulse response filter.
For more details and for comparison, see the Example section of 
{\tt psd} in the Matlab Signal Processing Toolbox \cite{Welchmatlab}.}
\end{center}
\end{figure}
%

\subsection{Algorithms}
\label{ss:Welchalgorithms}

{\tt WelchCSDEstimate} estimates cross-spectral densities (CSDs)
by averaging cross-spectra of (possibly) overlapping segments of 
the input data \cite{WelchP&W,WelchO&S}.
The basic algorithm is as follows:
%
\begin{enumerate}
\item
Apply the chosen window to each successive detrended segment 
of the input data.
(Note: the window automatically resizes itself to the length 
of the data segment, which is equal to the chosen fft length.)
%
\item
Take the DFT of the window data segment.
(See the {\tt FFT} class for details about discrete Fourier
transforms.)
%
\item
Form the product {\tt conj(fft\_x[])*fft\_y[]} for each segment 
of input data, scaling the result with the inverse of the sum of 
squares of the window.
This yields CSD estimates for each segment of input data.
%
\item
Average the cross-spectra calculated in the previous step to get 
the final CSD estimate.
\end{enumerate}
%
The number of sections averaged is
%
\begin{equation}
{\tt numSections} 
= {\tt floor}
\left({{\tt dataLength} - {\tt overlapLength}\over
       {\tt fftLength}  - {\tt overlapLength}}\right)\ .
\end{equation}
%
The normalization of the CSD estimate and corresponding discrete 
frequency values are given by 
Eqs.~(\ref{Welche:parseval})-(\ref{Welche:freq2}).

\subsection{Accuracy}

The precision of the {\tt apply} method is determined by the type 
{\tt T} (real or complex) of the input data:
If {\tt T} is {\tt float}, the precision is single precision.
If {\tt T} is {\tt double}, the precision is double precision.

\subsection{Performance}

The performance of the {\tt apply} method is dominated (for long
sequences) by the the DFTs of the input data.
These are $O(N\,{\rm log}N)$, where $N$ is the length of the DFT.

\subsection{Testing}

{\tt WelchCSDEstimate} is tested by {\tt tCSDEstimate.cc}.
The following tests are performed:
%
\begin{itemize}
\item
test that proper exceptions are thrown by the Welch estimate 
constructor and set methods when the input arguments are invalid.
\item
test that proper exceptions are thrown by the Welch estimate
{\tt apply} method when the input data are invalid.
%
\item
test that one can construct valid Welch estimate objects using
the constructor.
%
\item
test that one can use the set methods to change the parameters 
(i.e., fft length, overlap length, window, and detrend method)
defining a Welch estimate object.
%
\item
test that the {\tt apply} method produces correct results for the 
CSD estimate and discrete frequency values when different Welch 
estimate objects (defined by different values for the fft length, 
overlap length, window, and detrend method) act on non-trivial 
input sequences of different types ({\tt float} or {\tt double}, 
real or complex).
\end{itemize}
%
When testing {\tt WelchCSDEstimate}, we compare the output of the
{\tt apply} method with results previously calculated with Matlab.
The output of the {\tt WelchCSDEstimate} methods is said to be 
in agreement with the results produced by Matlab if the absolute 
value of the difference between output and result was less than
or equal to $10^{-6}$.
{\tt WelchCSDEstimate} passes all of the above tests.

\subsection{Notes}

\begin{itemize}
\item
Since Matlab's definition of a Hann window disagrees with the 
definition given in {\tt HannWindow}, we wrote our own
Matlab code to generate the appropriate Hann window before
performing the tests that used a Hann window.
(See {\tt HannWindow} for details.)
\end{itemize}

\subsection{See Also}

\begin{itemize}
\item 
{\tt class CSDEstimate}
\item
{\tt class WelchCSDSpectrum}
\item 
{\tt class Window}
\item
{\tt class WindowInfo}
\item
{\tt class RectangularWindow}
\item
{\tt class HannWindow}
\item
{\tt class KaiserWindow}
\item
{\tt class FFT}
\end{itemize}

\begin{thebibliography}{99}

\bibitem{WelchP&W}
D.B.\ Percival and A.T.\ Walden,
{\em Spectral Analysis for Physical Applications\/},
(Cambridge, Cambridge, 1993).

\bibitem{WelchO&S}
A.V.\ Oppenheim and R.W. Schafer,
{\em Discrete-Time Signal Processing\/},
(New Jersey, Prentice-Hall, 1999).

\bibitem{Welchmatlab} 
The MathWorks: {\tt http:\slash\slash www.mathworks.com}.

\end{thebibliography}

%\input Tail

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 

