\input Head

\section{\tt HannWindow}

A class representing a Hann window.

\subsection{Description}
\label{ss:HWdescription}

{\tt HannWindow} is a class representing a Hann window, 
which can be used for low-pass filtering and spectral density estimation.
{\tt HannWindow} derives from the abstract base class {\tt Window}.

The sample values $w[i]$ of a Hann window are defined by:
%
\[
w[i]:=
\left\{
\begin{array}{ll}
0.5-0.5\cos\left[2\pi i/(N-1)\right]
& \quad 0\le i< N,\\
0
& \quad {\rm otherwise}\ ,
\end{array}
\right.
\]
%
where $N$ is the length of the window.
(If $N=1$, $w[0]$ is defined to be 1.)
The above definition agrees with that given in 
Oppenheim and Schafer\cite{HWOS};
it disagrees with the definition of the Hann window (called 
{\tt hanning(n)}) given in Matlab\cite{HWmatlab}.
Figure~\ref{f:HannWindowFig1} shows a plot of a Hann window of length
1024.
%
\begin{figure}[htb!]
\begin{center}
{\epsfig{file=HannWindowFig1.ps,
angle=-90,width=4in,bbllx=25pt,bblly=50pt,bburx=590pt,bbury=740pt}}
\caption{\label{f:HannWindowFig1}
Hann window of length $N=1024$.}
\end{center}
\end{figure}
%

{\tt HannWindow} inherits its functionality from the abstract base 
class {\tt Window}.
See {\tt Window} for more details.

\subsection{Operating Instructions}

\subsubsection{Example Usage}

{\footnotesize 
\begin{verbatim}

  // calculate the values of a Hann window of length 1024,
  // then write the results to an output file "HW.dat"
  //
  Sequence<float> in(1,1024); // input sequence (all 1's)
  Sequence<float> out;        // output sequence

  HannWindow<float> hw;       // Hann window

  // apply the window to the input data
  hw.apply(out, in);

  // write window values to a file
  ofstream outfileHW("HW.dat");
  for (int i = 0; i < 1024; i++ ) 
  {
      outfileHW << i << "  " << out[i] << endl;
  }

\end{verbatim}}

\subsection{Algorithms}

Not applicable.

\subsection{Accuracy}

See {\tt Window}.

\subsection{Performance}

See {\tt Window}.

\subsection{Testing}
\label{ss:HWtesting}

{\tt HannWindow} is tested (along with all the other windows)
by the code contained in {\tt tWindow.cc}.
The following tests are performed:
%
\begin{itemize}
\item
test that proper exceptions are thrown by the Hann window 
{\tt apply} and {\tt operator()} methods.
\item
test that the mean, rms, and Hann window values are correct 
for trivial and non-trivial window lengths.
\item 
test that the output values are correct when a Hann window is applied 
to non-trivial real and complex input sequences.
\end{itemize}
%
When testing {\tt HannWindow}, we compare the output of the
Hann window methods with results previously calculated with Matlab.
The output of the Hann window methods are said to be in agreement 
with the results produced by Matlab if the difference is 
less than or equal to $10^{-5}$.
{\tt HannWindow} passes all of the above tests.

\subsection{Notes}

Since Matlab's definition of a Hann window disagrees with the 
definition given in Sec.~\ref{ss:HWdescription}, we wrote our own 
Matlab code to generate the appropriate Hann window before we did 
the testing described in Sec.~\ref{ss:HWtesting}.

\subsection{See Also}

\begin{itemize}
\item
class {\tt Window}
\item
class {\tt WindowInfo}
\item
class {\tt RectangularWindow}
\item
class {\tt KaiserWindow}
\end{itemize}

\begin{thebibliography}{99}

\bibitem{HWOS}
A.V.\ Oppenheim and R.W. Schafer,
{\em Discrete-Time Signal Processing\/},
(New Jersey, Prentice-Hall, 1999), p.468.

\bibitem{HWmatlab} 
The MathWorks: {\tt http:\slash\slash www.mathworks.com}.

\end{thebibliography}

\input Tail

