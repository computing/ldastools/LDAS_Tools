#ifndef _RAN3FUNCTION_HH_
#define _RAN3FUNCTION_HH_

#include "CallChain.hh"

class ran3Function : public CallChain::Function
{
public:
  ran3Function();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_RAN3FUNCTION_HH_
