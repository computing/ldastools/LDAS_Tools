#ifndef CLAPACK_TYPES_HH
#define CLAPACK_TYPES_HH

  //---------------------------------------------------------------------
  // integer
  //---------------------------------------------------------------------
# if SIZEOF_INTEGER == SIZEOF_INT
  typedef int integer;
# elif SIZEOF_INTEGER == SIZEOF_LONG
  typedef long integer;
# else
# error Unable to determine type of integer
# endif
  //---------------------------------------------------------------------
  // real
  //---------------------------------------------------------------------
# if SIZEOF_REAL == SIZEOF_FLOAT
  typedef float real;
# else
# error Unable to determine type of real
# endif
  //---------------------------------------------------------------------
  // doublereal
  //---------------------------------------------------------------------
# if SIZEOF_DOUBLEREAL == SIZEOF_DOUBLE
  typedef double doublereal;
# else
# error Unable to determine type of doublereal
# endif
  //---------------------------------------------------------------------
  // complex
  //---------------------------------------------------------------------
# if SIZEOF_COMPLEX == (2*SIZEOF_FLOAT)
  typedef std::complex<float> scomplex;
# else
# error Unable to determine type of complex
# endif
  //---------------------------------------------------------------------
  // doublecomplex
  //---------------------------------------------------------------------
# if SIZEOF_DOUBLECOMPLEX == (2*SIZEOF_DOUBLE)
  typedef std::complex<double> dcomplex;
# else
# error Unable to determine type of doublecomplex
# endif

#endif /* CLAPACK_TYPES_HH */
