/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef DATACONDAPI__GEOMETRYUMD_HH
#define DATACONDAPI__GEOMETRYUMD_HH

#include <string>
#include <vector>

#include "general/types.hh"
#include "general/RefPtr.hh"
#include "general/unordered_map.hh"   

namespace ILwd
{
  class LdasContainer;
  class TOC;
}

namespace ILwdFCS
{
  class FrameH;
}

namespace datacondAPI
{
  class ILwdMetaDataHelper;

  //: Keep track of detector geometry
  class GeometryMetaData
  {
  public:
    void AddDetector( const ILwd::TOC& ILwdHelper );

    bool AddDetector( const GeometryMetaData& Geometry,
		      const std::string& Prefix );

    void AddUnmappedDetectors( const GeometryMetaData& Geometry );

    //: Return the number of entries
    unsigned int DetectorCount( ) const;

    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrameH& Storage - place to store values
    void Store( ILwd::LdasContainer& Storage ) const;

    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrameH& Storage - place to store values
    void Store( ILwdFCS::FrameH& Storage ) const;

    GeometryMetaData& operator=( const GeometryMetaData& Src );

    bool operator==( const GeometryMetaData& Src ) const;

    bool operator!=( const GeometryMetaData& Src ) const;

  private:

    // Maintain info about the detectors
    class detector_info;
    typedef General::RefPtr<detector_info> detector_info_type;
    typedef General::unordered_map< std::string, detector_info_type > 
       detector_map_type;
    typedef std::vector< detector_info_type > unmapped_detector_type;

    detector_map_type	m_detectors;
    unmapped_detector_type	m_unmapped_detectors;
  };

  inline unsigned int GeometryMetaData::
  DetectorCount( ) const
  {
    return m_detectors.size() + m_unmapped_detectors.size();
  }

  inline bool GeometryMetaData::
  operator!=( const GeometryMetaData& Src ) const
  {
    return ! ( *this == Src );
  }

}

#include "GeometryMetaData.icc"

#endif /* DATACONDAPI__GEOMETRYUMD_HH */
