/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "datacondAPI/config.h"

#include <complex>   
   
#include <filters/Resample.hh>
#include <filters/valarray_utils.hh>

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ResampleState.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

using std::complex;   
   
UDT_CLASS_INSTANTIATION(ResampleState,UDT_DEFAULT_KEY)

namespace datacondAPI {

  ResampleState::ResampleState(int p,
			       int q,
			       int n,
			       double beta)
    : m_first(true), m_b(),
      m_resample(new Filters::Resample<float>(p, q, n, beta))
  {
  }

  ResampleState::ResampleState(const udt& p,
			       const udt& q,
			       const udt& n,
			       const udt& beta)
    : m_first(true), m_b(), m_resample(0)
  {
    int tmpP = 0;
    int tmpQ = 0;
    int tmpN = 0;
    double tmpBeta = 0;

    if (udt::IsA<Scalar<int> >(p))
    {
      tmpP = udt::Cast<Scalar<int> >(p).GetValue();
    }
    else
    {
      std::string what = "bad udt arg to constructor (p not integer)";
      General::toss<std::invalid_argument>("ResampleState",
					   __FILE__, __LINE__, what);
    }

    if (udt::IsA<Scalar<int> >(q))
    {
      tmpQ = udt::Cast<Scalar<int> >(q).GetValue();
    }
    else
    {
      std::string what = "bad udt arg to constructor (q not integer)";
      General::toss<std::invalid_argument>("ResampleState",
					   __FILE__, __LINE__, what);
    }

    if (udt::IsA<Scalar<int> >(n))
    {
      tmpN = udt::Cast<Scalar<int> >(n).GetValue();
    }
    else
    {
      std::string what = "bad udt arg to constructor (n not integer)";
      General::toss<std::invalid_argument>("ResampleState",
					   __FILE__, __LINE__, what);
    }

    if (udt::IsA<Scalar<double> >(beta))
    {
      tmpBeta = udt::Cast<Scalar<double> >(beta).GetValue();
    }
    else if (udt::IsA<Scalar<float> >(beta))
    {
      tmpBeta = udt::Cast<Scalar<float> >(beta).GetValue();
    }
    else if (udt::IsA<Scalar<int> >(beta))
    {
      tmpBeta = udt::Cast<Scalar<int> >(beta).GetValue();
    }
    else
    {
      std::string what = "bad udt arg to constructor "
        "(beta cannot be converted to double)";
      General::toss<std::invalid_argument>("ResampleState",
					   __FILE__, __LINE__, what);
    }

    m_resample.reset(new Filters::Resample<float>(tmpP, tmpQ,
                                                  tmpN, tmpBeta));
  }

  ResampleState::ResampleState(int p, int q, const Sequence<double>& b)
    : m_first(true), m_b(b), m_resample(0)
  {
    m_resample.reset(new Filters::Resample<float>(p, q, m_b));
  }

  ResampleState::ResampleState(const ResampleState& state)
    : State(state),
      m_first(state.m_first), m_b(state.m_b),
      m_resample(state.m_resample->Clone())
  {
  }

  ResampleState::ResampleState(const udt& state)
    : State(), // use default state, then assign it
      m_resample(0)
  {
    if (udt::IsA<ResampleState>(state))
    {
      const ResampleState& ref = udt::Cast<ResampleState>(state);
     
      State::operator=(ref);

      m_first = ref.m_first;
      
      m_b.resize(ref.m_b.size());
      m_b = ref.m_b;

      m_resample.reset(ref.m_resample->Clone());
    }
    else
    {
      std::string what = "udt not a ResampleState";
      General::toss<std::invalid_argument>("ResampleState",
					   __FILE__, __LINE__, what);
    } 
  }
    
  ResampleState::~ResampleState()
  {
  }
    
  ResampleState& ResampleState::operator=(const ResampleState& ref)
  {
    if (&ref != this)
    {
      State::operator=(ref);

      m_first = ref.m_first;

      m_b.resize(ref.m_b.size());
      m_b = ref.m_b;

      m_resample.reset(ref.m_resample->Clone());
    }

    return *this;
  }
    
  int ResampleState::getP() const
  {
    return m_resample->getP();
  }
    
  int ResampleState::getQ() const
  {
    return m_resample->getQ();
  }
    
  int ResampleState::getN() const
  {
    return m_resample->getN();
  }
    
  int ResampleState::getOrder() const
  {
    return m_resample->getNOrder();
  }
    
  double ResampleState::getDelay() const
  {
    return m_resample->getDelay();
  }

  double ResampleState::getBeta() const
  {
    return m_resample->getBeta();
  }

  void ResampleState::getB(Sequence<double>& b) const
  {
    // Since we only have a ResampleBase, need to run through the types
    // to get the filter coeffs. Sigh.

    const Filters::ResampleBase* const res = m_resample.get();
    
    if (const Filters::Resample<float>* const p =
        dynamic_cast<const Filters::Resample<float>*>(res))
    {
      p->getB(b);
    }
    else if (const Filters::Resample<double>* const p =
        dynamic_cast<const Filters::Resample<double>*>(res))
    {
      p->getB(b);
    }
    else if (const Filters::Resample<complex<float> >* const p =
             dynamic_cast<const Filters::Resample<complex<float> >*>(res))
    {
      p->getB(b);
    }
    else if(const Filters::Resample<complex<double> >* const p =
            dynamic_cast<const Filters::Resample<complex<double> >*>(res))
    {
      p->getB(b);
    }
    else
    {
      std::string what("Unknown type for filter coefficients");
      General::toss<std::runtime_error>("getB",
                                        __FILE__, __LINE__, what);
    }

  }

  template<class T>
  void ResampleState::apply(std::valarray<T>& out, const std::valarray<T>& in)
  {
    Filters::Resample<T>* r =
      dynamic_cast<Filters::Resample<T>*>(m_resample.get());

    // Do we have the right type of state?
    if (r == 0)
    {
      // If this is the first call, allow it to be reset, since our initial
      // guess of float may have been wrong
      if (m_first)
      {
	const int p = getP();
	const int q = getQ();
	
	if (m_b.size() == 0)
	{
	  const int n = getN();
	  const double beta = getBeta();
	  r = new Filters::Resample<T>(p, q, n, beta);
	  m_resample.reset( r );
	}
	else
	{
	  r = new Filters::Resample<T>(p, q, m_b);
	  m_resample.reset( r );
	}

	m_first = false;
      }
      else
      {
	throw std::invalid_argument("ResampleState::apply - "
			       "Attempt to resample data where ResampleState "
		               "was initialized with a different data type");
      }
    }

    r->apply(out, in);
  }

  ResampleState* ResampleState::Clone() const
  {
    return new ResampleState(*this);
  }
    
  ILwd::LdasElement* ResampleState::
  ConvertToIlwd( const CallChain& Chain,
                 datacondAPI::udt::target_type Target ) const
  {
    throw General::unimplemented_error("ResampleState::ConvertToIlwd() - "
				       "unimplemented");

#if 0
    // :TODO: Need a way to convert m_resample to ILWD

    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    container->push_back(new ILwd::LdasArray<int>(getP(), "p"), false);
    container->push_back(new ILwd::LdasArray<int>(getQ(), "q"), false);
    container->push_back(new ILwd::LdasArray<int>(getN(), "n"), false);

    for (size_t k = 0; k < m_lfs.size(); ++k)
    {
      ILwd::LdasContainer* subContainer =
        dynamic_cast<ILwd::LdasContainer*>(m_lfs[k].ConvertToIlwd(Chain,
                                                                  Target));
      if (subContainer)
      {
        container->push_back(subContainer, false);
      }
      else
      {
        // thow exception here - bad_cast
      }
    }

    return container;
#endif
    
    return 0;
  }

  template void 
  ResampleState::apply(std::valarray<float>&,
		       const std::valarray<float>&);
  template void 
  ResampleState::apply(std::valarray<double>&,
		       const std::valarray<double>&);

  template void 
  ResampleState::apply(std::valarray<std::complex<float> >&,
		       const std::valarray<std::complex<float> >&);
  template void 
  ResampleState::apply(std::valarray<std::complex<double> >&,
		       const std::valarray<std::complex<double> >&);

} // namespace datacondAPI
