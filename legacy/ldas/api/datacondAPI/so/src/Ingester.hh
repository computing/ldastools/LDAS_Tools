#ifndef DATACONDAPI__INGESTER_HH
#define	DATACONDAPI__INGESTER_HH

#include <string>
#include <vector>

#include "GeometryMetaData.hh"
#include "CallChain.hh"

namespace ILwd
{
  class LdasContainer;
}
namespace datacondAPI
{
  class udt;
  class ILwdMetaDataHelper;

  class Ingester
  {
  public:
    typedef CallChain::ingestion_type	ingestion_type;

    Ingester( CallChain& Chain,
	      ingestion_type Ingestion,
	      bool AllowDuplicates );

    std::string IngestILwd( const ILwd::LdasElement& Element );

  private:
    CallChain&		m_chain;
    ingestion_type		m_ingestion;
    bool		m_allow_duplicates;
    std::string		m_symbols;
    GeometryMetaData	m_detectors;
    
    //: Adds a floating-point ilwd to the symbol table
    template<class DataType>
    void add_ilwd_float_symbol( const std::string& Symbol,
				const ILwd::LdasArray<DataType>* Data );

    //: Adds an integral type (including unsigned) ilwd to the symbol table
    template<class DataType>
    void add_ilwd_int_symbol( const std::string& Symbol,
			      const ILwd::LdasArray<DataType>* Data );

    void add_symbol_name( const std::string& SymbolName );

    //: Ingest an ILwd::LdasContainer
    bool detector( const ILwdMetaDataHelper& ILwdHelper );

    //: Ingest an ILwd::LdasContainer
    bool detector( const ILwd::LdasContainer* ILwdHelper );

    //: Ingest an ILwd::LdasContainer
    void ingest_ilwd_container( const ILwd::LdasContainer* Container,
				const std::string& Prefix = "" );

    //: Ingest an ILwd::LdasElement
    void ingest_ilwd_element( const ILwd::LdasElement* Element,
			      const std::string& Prefix = "");

    //: Wrapper for mapping ILwd data into UDT classes.
    bool ingest_udt( std::string& SymbolName,
		     const ILwd::LdasElement& Element );

    void store_symbol( std::string Name,
		       datacondAPI::udt* UDT,
		       CallChain::IntermediateResult::primary_type Primary =
		       CallChain::IntermediateResult::MAYBE_PRIMARY );

    void store_symbol( std::string Name,
		       const std::vector<datacondAPI::udt*>& UDTS,
		       CallChain::IntermediateResult::primary_type Primary =
		       CallChain::IntermediateResult::MAYBE_PRIMARY );
  };
}

#endif	/* DATACONDAPI__INGESTER_HH */
