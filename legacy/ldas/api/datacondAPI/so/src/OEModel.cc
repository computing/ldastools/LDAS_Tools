/* -*- c-basic-offset: 4 -*- */
#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>

#include "OEModel.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "Matrix.hh"
#include "LinearAlgebra.hh"

namespace datacondAPI
{
    class OEModel::Abstraction
    {

    public:

	// default overrides

	virtual ~Abstraction() = 0;

	// copying

	virtual Abstraction* clone() const = 0;

	// accessors

	virtual void getTheta(udt*&) const = 0;
	virtual void getFilterB(udt*&) const = 0;
	virtual void getFilterF(udt*&) const = 0;

	// methods

	virtual void apply(udt*& w, const udt& u) const = 0;
	virtual void refine(const udt& y, const udt& u) = 0;
	virtual void merit(udt*& m, const udt& y, const udt& u) const = 0;

    protected:

	// default overrides

	Abstraction();
	Abstraction(const Abstraction&);
	Abstraction& operator=(const Abstraction&);
	
    private:

    };

    template<typename type> class OEModel::Implementation : public OEModel::Abstraction
    {

    public:

	// default overrides

	Implementation();
	Implementation(const Implementation&);
	
	~Implementation();

	Implementation& operator=(const Implementation&);

	// custom construction

	explicit Implementation(const int& order_b );
	explicit Implementation(const int& order_b, const int& order_f );
	explicit Implementation(const std::valarray<type>& theta, const int& order_b);
	Implementation(const std::valarray<type>& y,
		       const std::valarray<type>& u,
		       const int& order_b, const int& order_f);

	// copying

	Implementation* clone() const;

	// mutators

	void getTheta(std::valarray<type>&) const;

	void getTheta(udt*&) const;
	void getFilterB(udt*&) const;
	void getFilterF(udt*&) const;

    void getFilterB(std::valarray<type>&) const;
    void getFilterF(std::valarray<type>&) const;

	// methods
	
	void apply(std::valarray<type>& w,
		   const std::valarray<type>& u) const;
	void apply(udt*& w, const udt& u) const;

	void refine(const std::valarray<type>& y,
		    const std::valarray<type>& u);
	void refine(const udt& y, const udt& u);

	type merit(const std::valarray<type>& y, const std::valarray<type>& u) const;
	void merit(udt*& m, const udt& y, const udt& u) const;

    private:
	
	// model state

	std::valarray<type> m_theta;
	mutable std::valarray<type> m_state;
	
	// placeholder estimator state
	// !!! ARX estimator *not* OE estimator !!

	Matrix<type> m_matrix;
	Matrix<type> m_vector;
	std::valarray<type> m_history_y;
	std::valarray<type> m_history_u;
	
    };

    OEModel::OEModel()
	: m_order_b(0),
	  m_order_f(0),
	  m_data(0)
    {
    }

    OEModel::OEModel(const OEModel& oe)
	: m_order_b(oe.m_order_b),
	  m_order_f(oe.m_order_f),
	  m_data(oe.m_data ? oe.m_data->clone() : 0)
    {
    }

    OEModel::~OEModel()
    {
	delete m_data;
    }

    OEModel& OEModel::operator=(const OEModel& oe)
    {
	if (&oe != this)
	{
	    m_order_b = oe.m_order_b;
	    m_order_f = oe.m_order_f;
	    delete m_data;
	    m_data = oe.m_data->clone();
	}
	return *this;
    }

    OEModel* OEModel::Clone() const
    {
	return new OEModel(*this);
    }

    ILwd::LdasElement*
        OEModel::ConvertToIlwd(const CallChain& Chain,
		      udt::target_type Target) const
    {
	throw General::unimplemented_error("OEModel::ConvertToIlwd is unimplemented");
    }

    OEModel::OEModel(const int& order_b)
	: m_order_b(order_b),
	  m_order_f(order_b),
	  m_data(0)
    {
    }

    OEModel::OEModel(const int& order_b, const int& order_f)
	: m_order_b(order_b),
	  m_order_f(order_f),
	  m_data(0)
    {
    }

    template<typename type>
    OEModel::OEModel(const std::valarray<type>& theta, const int& order_b)
	: m_order_b(order_b),
	  m_order_f(theta.size() - order_b),
	  m_data(new Implementation<type>(theta, order_b))
    {
    }

    OEModel::OEModel(const udt& order_b)
	: m_data(0)
    {
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&order_b))
	{
	    m_order_b = p->GetValue();
	    m_order_f = m_order_b;
	}
	else
	{
	    throw std::invalid_argument("OEModel::OEModel(const udt&): requires a scalar integer");
	}
    }

    OEModel::OEModel(const udt& order_b_or_theta, const udt& order_f_or_order_b)
	: m_data(0)
    {
	const Scalar<int>* q = 0;
	if ((q = dynamic_cast<const Scalar<int>*>(&order_f_or_order_b)))
	{
	}
	else
	{
	    throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&): argument (2) must be a scalar integer");
	}
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&order_b_or_theta))
	{
	    m_order_b = p->GetValue();
	    m_order_f = q->GetValue();

	}
	else if (const std::valarray<float>* p
		 = dynamic_cast<const std::valarray<float>*>(&order_b_or_theta))
	{
	    m_order_b = q->GetValue();
	    m_order_f = p->size() - m_order_b;
	    m_data = new Implementation<float>(*p, m_order_b);
	}
	else if (const std::valarray<double>* p
		 = dynamic_cast<const std::valarray<double>*>(&order_b_or_theta))
	{
	    m_order_b = q->GetValue();
	    m_order_f = p->size() - m_order_b;
	    m_data = new Implementation<double>(*p, m_order_b);
	}
	else if (const std::valarray<std::complex<float> >* p
		 = dynamic_cast<const std::valarray<std::complex<float> >*>(&order_b_or_theta))
	{
	    m_order_b = q->GetValue();
	    m_order_f = p->size() - m_order_b;
	    m_data = new Implementation<std::complex<float> >(*p, m_order_b);
	}
	else if (const std::valarray<std::complex<double> >* p
		 = dynamic_cast<const std::valarray<std::complex<double> >*>(&order_b_or_theta))
	{
	    m_order_b = q->GetValue();
	    m_order_f = p->size() - m_order_b;
	    m_data = new Implementation<std::complex<double> >(*p, m_order_b);
	}
	else
	{
	    throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&): requires argument (1) a scalar integer or array");
	}
    }
    
    template<typename type>
    OEModel::OEModel(const std::valarray<type>& y,
		     const std::valarray<type>& u,
		     const int& order_b, const int& order_f)
	: m_order_b(order_b),
	  m_order_f(order_f),
	  m_data(new Implementation<type>(y, u, order_b, order_f))
    {
    }

    OEModel::OEModel(const udt& y, const udt& u, const udt& order_b, const udt& order_f)
	: m_data(0)
    {
	if (const Scalar<int>* p = dynamic_cast<const Scalar<int>*>(&order_b))
	{
	    m_order_b = p->GetValue();
	}
	else
	{
	    throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires argument (3) a scalar integer");
	}
	if (const Scalar<int>* p = dynamic_cast<const Scalar<int>*>(&order_f))
	{
	    m_order_f = p->GetValue();
	}
	else
	{
	    throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires argument (4) a scalar integer");
	}

	if (const std::valarray<float>* p
	    = dynamic_cast<const std::valarray<float>*>(&y))
        {
	    if (const std::valarray<float>* q =
		dynamic_cast<const std::valarray<float>*>(&u))
	    {
		m_data =
		    new Implementation<float>(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires arguments (1) and (2) have the same precision");
	    }
	}
	else if (const std::valarray<double>* p
	    = dynamic_cast<const std::valarray<double>*>(&y))
        {
	    if (const std::valarray<double>* q =
		dynamic_cast<const std::valarray<double>*>(&u))
	    {
		m_data =
		    new Implementation<double>(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires arguments (1) and (2) have the same precision");
	    }
	}
	else if (const std::valarray<std::complex<float> >* p
	    = dynamic_cast<const std::valarray<std::complex<float> >*>(&y))
        {
	    if (const std::valarray<std::complex<float> >* q =
		dynamic_cast<const std::valarray<std::complex<float> >*>(&u))
	    {
		m_data =
		    new Implementation<std::complex<float> >(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires arguments (1) and (2) have the same precision");
	    }
	}
	else if (const std::valarray<std::complex<double> >* p
	    = dynamic_cast<const std::valarray<std::complex<double> >*>(&y))
        {
	    if (const std::valarray<std::complex<double> >* q =
		dynamic_cast<const std::valarray<std::complex<double> >*>(&u))
	    {
		m_data =
		    new Implementation<std::complex<double> >(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires arguments (1) and (2) have the same precision");
	    }
	}
	else
	{
	    throw std::invalid_argument("OEModel::OEModel(const udt&, const udt&, const udt&, const udt&): requires argument (1) an array");	
	}
    }

    int OEModel::getOrderB() const
    {
	return m_order_b;
    }

    int OEModel::getOrderF() const
    {
	return m_order_f;
    }

    void OEModel::getOrderB(udt*& order_b) const
    {
	if (order_b == 0)
	{
	    order_b = new Scalar<int>(m_order_b);
	}
	else if (Scalar<int>* p
		 = dynamic_cast<Scalar<int>*>(order_b))
	{
	    (int&)(*p) = m_order_b;
	}
	else
	{
	    throw std::invalid_argument("OEModel::getOrderB(udt*&): requires a scalar integer");
	}    
    }

    void OEModel::getOrderF(udt*& order_f) const
    {
	if (order_f == 0)
	{
	    order_f = new Scalar<int>(m_order_f);
	}
	else if (Scalar<int>* p
		 = dynamic_cast<Scalar<int>*>(order_f))
	{
	    (int&)(*p) = m_order_f;
	}
	else
	{
	    throw std::invalid_argument("OEModel::getOrderF(udt*&): requires a scalar integer");
	}
    }
    
    template<typename type>
    void OEModel::getTheta(std::valarray<type>& theta) const
    {
	if (Implementation<type>* p
	    = dynamic_cast<Implementation<type>*>(m_data))
	{
	    p->getTheta(theta);
	}
	else
	{
	    throw std::invalid_argument("type mismatch");
	}
	
    }

    void OEModel::getTheta(udt*& theta) const
    {
	if (m_data)
	{
	    m_data->getTheta(theta);
	}
	else
	{
	    throw std::invalid_argument("OEModel::getTheta(udt*&): no model");
	}
    }

    void OEModel::getFilterB(udt*& filter_b) const
    {
	if (m_data)
	{
	    m_data->getFilterB(filter_b);
	}
	else
	{
	    throw std::invalid_argument("OEModel::getFilterB(udt*&): no model");
	}
    }

    void OEModel::getFilterF(udt*& filter_f) const
    {
	if (m_data)
	{
	    m_data->getFilterF(filter_f);
	}
	else
	{
	    throw std::invalid_argument("OEModel::getFilterF(udt*&): no model");
	}
    }

    template<typename type>
    void OEModel::getFilterB(std::valarray<type>& filter_b) const
    {
        if (const Implementation<type>* p = dynamic_cast<const Implementation<type>*>(m_data))
        {
            p->getFilterB(filter_b);
        }
        else
        {
            throw std::invalid_argument("OEModel::getFilterB(std::valarray<type>&): no model or type mismatch");
        }
    }

    template<typename type>
    void OEModel::getFilterF(std::valarray<type>& filter_f) const
    {
        if (const Implementation<type>* p = dynamic_cast<const Implementation<type>*>(m_data))
        {
            p->getFilterF(filter_f);
        }
        else
        {
            throw std::invalid_argument("OEModel::getFilterF(std::valarray<type>&): no model or type mismatch");
        }
    }

    void OEModel::setOrderB(const int& order_b)
    {
	m_order_b = order_b;
	delete m_data;
	m_data = 0;
    }

    void OEModel::setOrderF(const int& order_f)
    {
	m_order_f = order_f;
	delete m_data;
	m_data = 0;
    }

    void OEModel::setOrderB(const udt& order_b)
    {
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&order_b))
        {
	    m_order_b = p->GetValue();
	    delete m_data;
	    m_data = 0;
	}
	else
        {
	    throw std::invalid_argument("OEModel::setOrderB(const udt&): requires a scalar integer");
	}
    }   

    void OEModel::setOrderF(const udt& order_f)
    {
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&order_f))
        {
	    m_order_f = p->GetValue();
	    delete m_data;
	    m_data = 0;
	}
	else
        {
	    throw std::invalid_argument("OEModel::setOrderB(const udt&): requires a scalar integer");
	}
    }   

    template<typename type>
    void OEModel::setTheta(const std::valarray<type>& theta, const int& order_b)
    {
	m_order_b = order_b;
	m_order_f = theta.size() - m_order_b;
	delete m_data;
	m_data = new Implementation<type>(theta, m_order_b);
    }

    void OEModel::setTheta(const udt& theta, const udt& order_b)
    {
	if (const Scalar<int>* p = dynamic_cast<const Scalar<int>*>(&order_b))
	{
	    m_order_b = p->GetValue();
	}
	else
	{
	    throw std::invalid_argument("OEModel::setTheta(const udt&, const udt&): requires argument (2) a scalar integer");
	}
	if (const std::valarray<float>* p = dynamic_cast<const std::valarray<float>*>(&theta))
	{
	    m_order_f = p->size() - m_order_b;
	    delete m_data;
	    m_data = new Implementation<float>(*p, m_order_b);
	}
	else if (const std::valarray<double>* p = dynamic_cast<const std::valarray<double>*>(&theta))
	{
	    m_order_f = p->size() - m_order_b;
	    delete m_data;
	    m_data = new Implementation<double>(*p, m_order_b);
	}
	else if (const std::valarray<std::complex<float> >* p = dynamic_cast<const std::valarray<std::complex<float> >*>(&theta))
	{
	    m_order_f = p->size() - m_order_b;
	    delete m_data;
	    m_data = new Implementation<std::complex<float> >(*p, m_order_b);
	}
	else if (const std::valarray<std::complex<double> >* p = dynamic_cast<const std::valarray<std::complex<double> >*>(&theta))
	{
	    m_order_f = p->size() - m_order_b;
	    delete m_data;
	    m_data = new Implementation<std::complex<double> >(*p, m_order_b);
	}
	else
	{
	    throw std::invalid_argument("OEModel::setTheta(const udt&): requires argument (1) an array");
	}
    }

    template<typename type>
    void OEModel::apply(std::valarray<type>& w,
			const std::valarray<type>& u) const
    {
	if (const Implementation<type>* p
	    = dynamic_cast<const Implementation<type>*>(m_data))
	{
	    p->apply(w,u);
	}
	else
	{
	    throw std::invalid_argument("type mismatch");
	}
    }

    void OEModel::apply(udt*& w, const udt& u) const
    {
	if (m_data != 0)
	{
	    m_data->apply(w, u);
	}
	else
	{
	    throw std::invalid_argument("OEModel::apply(udt*&, const udt&): no model");
	}
    }

    template<typename type>
    void OEModel::refine(const std::valarray<type>& y, const std::valarray<type>& u)
    {
	if (m_data == 0)
	{
	    m_data = new Implementation<type>(y, u, m_order_b, m_order_f);
	}
	else if (Implementation<type>* p
		 = dynamic_cast<Implementation<type>*>(m_data))
	{
	    p->refine(y, u);
	}
	else
	{
	    throw std::invalid_argument("type mismatch");
	}
    }

    void OEModel::refine(const udt& y, const udt& u)
    {
	if (m_data != 0)
	{
	    m_data->refine(y, u);
	}
	else if (const std::valarray<float>* p
		 = dynamic_cast<const std::valarray<float>*>(&y))
	{
	    if (const std::valarray<float>* q
		= dynamic_cast<const std::valarray<float> *>(&u))
	    {
		m_data =
		    new Implementation<float>(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::refine(const udt&, const udt&): requires arguments (1) and (2) arrays of the same precision");
	    }
	}
	else if (const std::valarray<double>* p
		 = dynamic_cast<const std::valarray<double>*>(&y))
	{
	    if (const std::valarray<double>* q
		= dynamic_cast<const std::valarray<double>*>(&u))
	    {
		m_data =
		    new Implementation<double>(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::refine(const udt&, const udt&): requires arguments (1) and (2) arrays of the same precision");
	    }
	}
	else if (const std::valarray<std::complex<float> >* p
		 = dynamic_cast<const std::valarray<std::complex<float> >*>(&y))
	{
	    if (const std::valarray<std::complex<float> >* q
		= dynamic_cast<const std::valarray<std::complex<float> >*>(&u))
	    {
		m_data =
		    new Implementation<std::complex<float> >(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::refine(const udt&, const udt&): requires arguments (1) and (2) arrays of the same precision");
	    }
	}
	else if (const std::valarray<std::complex<double> >* p
		 = dynamic_cast<const std::valarray<std::complex<double> >*>(&y))
	{
	    if (const std::valarray<std::complex<double> >* q
		= dynamic_cast<const std::valarray<std::complex<double> >*>(&u))
	    {
		m_data =
		    new Implementation<std::complex<double> >(*p, *q, m_order_b, m_order_f);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::refine(const udt&, const udt&): requires arguments (1) and (2) arrays of the same precision");
	    }
	}
	else
	{
	    throw std::invalid_argument("OEModel::refine(const udt&, const udt&): requires argument (1) an array");
	}
    }

    template<typename type>
    type OEModel::merit(const std::valarray<type>& y,
			const std::valarray<type>& u) const
    {
	if (m_data != 0)
	{
	    if (const Implementation<type>* p
		= dynamic_cast<const Implementation<type>*>(m_data))
	    {
		return p->merit(y, u);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::merit(const std::valarray<type>&, const std::valarray<type>&): type mismatch");
	    }
	}
	else
	{
	    throw std::invalid_argument("OEModel::merit(const std::valarray<type>&, const std::valarray<type>&): no model");
	}
    }

    void OEModel::merit(udt*& m, const udt& y, const udt& u) const
    {
	if (m_data != 0)
	{
	    m_data->merit(m, y, u);
	}
	else
	{
	    throw std::invalid_argument("OEModel::merit(udt*&, const udt&, const udt&): no model");
	}
    }

    OEModel::Abstraction::~Abstraction()
    {
    }

    OEModel::Abstraction::Abstraction()
    {
    }

    OEModel::Abstraction::Abstraction(const Abstraction&)
    {
    }

    OEModel::Abstraction& OEModel::Abstraction::operator=(const Abstraction& abstraction)
    {
	if (&abstraction != this)
	{
	}
	return *this;
    }

    template<typename type>
    OEModel::Implementation<type>::Implementation()
	: m_theta(),
	  m_state(),
	  m_matrix(),
	  m_vector(),
	  m_history_y(),
	  m_history_u()
    {
    }

    template<typename type>
    OEModel::Implementation<type>::Implementation(const Implementation& implementation)
	: OEModel::Abstraction( implementation ),
	  m_theta(implementation.m_theta),
	  m_state(implementation.m_state),
	  m_matrix(implementation.m_matrix),
	  m_vector(implementation.m_vector),
	  m_history_y(implementation.m_history_y),
	  m_history_u(implementation.m_history_u)
    {
    }

    template<typename type>
    OEModel::Implementation<type>::~Implementation()
    {
    }

    template<typename type>
    OEModel::Implementation<type>& OEModel::Implementation<type>::operator=(const Implementation<type>& implementation)
    {
	if (&implementation != this)
	{
	    m_theta.resize(implementation.m_theta.size());
	    m_theta = implementation.m_theta;
	    m_state.resize(implementation.m_state.size());
	    m_state = implementation.m_state;
	    m_matrix.resize(implementation.m_theta.size(),
			    implementation.m_theta.size());
	    m_matrix = implementation.m_matrix;
	    m_vector.resize(implementation.m_theta.size(),
			    1);
	    m_vector = implementation.m_vector;
	    m_history_y.resize(implementation.m_history_y.size());
	    m_history_y = implementation.m_history_y;
	    m_history_u.resize(implementation.m_history_u.size());
	    m_history_u = implementation.m_history_u;
	}
	return *this;
    }

    template<typename type>
    OEModel::Implementation<type>::Implementation(const int& order_b )
	: m_theta(type(), order_b + order_b),
	  m_state(type(), order_b ),
	  m_matrix(order_b + order_b, order_b + order_b),
	  m_vector(order_b + order_b, 1),
	  m_history_y(type(), order_b),
	  m_history_u(type(), order_b)
    {
    }

    template<typename type>
    OEModel::Implementation<type>::Implementation(const int& order_b, const int& order_f)
	: m_theta(type(), order_b + order_f),
	  m_state(type(), (order_b >= order_f) ? order_b : order_f),
	  m_matrix(order_b + order_f, order_b + order_f),
	  m_vector(order_b + order_f, 1),
	  m_history_y(type(), order_f),
	  m_history_u(type(), order_b)
    {
    }

    template<typename type>
    OEModel::Implementation<type>::Implementation(const std::valarray<type>& theta, const int& order_b)
	: m_theta(theta),
	  m_state(type(), (order_b >= ((const int)theta.size() - order_b))
		  ? order_b : (theta.size() - order_b)),
	  m_matrix(theta.size(), theta.size()),
	  m_vector(theta.size(), 1),
	  m_history_y(type(), theta.size() - order_b),
	  m_history_u(type(), order_b)
    {
    }

    template<typename type>
    OEModel::Implementation<type>::Implementation(const std::valarray<type>& y,
						  const std::valarray<type>& u,
						  const int& order_b,
						  const int& order_f)
	: m_theta(type(), order_b + order_f),
	  m_state(type(), (order_b >= order_f) ? order_b : order_f),
	  m_matrix(order_b + order_f, order_b + order_f),
	  m_vector(order_b + order_f, 1),
	  m_history_y(type(), order_f),
	  m_history_u(type(), order_b)
    {
	refine(y, u);
    }

    template<typename type>
    OEModel::Implementation<type>* OEModel::Implementation<type>::clone() const
    {
	return new Implementation<type>(*this);
    }

    template<typename type>
    void OEModel::Implementation<type>::getTheta(std::valarray<type>& theta) const
    {
	theta.resize(m_theta.size());
	theta = m_theta;
    }

    template<typename type>
    void OEModel::Implementation<type>::getTheta(udt*& theta) const
    {
	if (std::valarray<type>* p = dynamic_cast<std::valarray<type>*>(theta))
	{
	    p->resize(m_theta.size());
	    (*p) = m_theta;
	}
	else if (theta == 0)
	{
	    theta = new Sequence<type>(m_theta);
	}
	else
	{
	    throw std::invalid_argument("OEModel::Implementation::getTheta(udt*& theta): requres null or a pointer to a sequence UDT");
	}       
    }

    template<typename type>
    void OEModel::Implementation<type>::getFilterB(udt*& filter_b) const
    {
	if (filter_b == 0)
	{
	    filter_b = new Sequence<type>;
	}
	if (std::valarray<type>* p = dynamic_cast<std::valarray<type>*>(filter_b))
	{
	    p->resize(m_history_u.size() + 1);
	    (*p)[0] = type();
	    (*p)[std::slice(1, m_history_u.size(), 1)] = m_theta[std::slice(0, m_history_u.size(), 1)];
	}
	else
	{
	    throw std::invalid_argument("OEModel::Implementation::getFilterB(udt*& theta): requres null or a pointer to a sequence UDT");
	}       
    }

    template<typename type>
    void OEModel::Implementation<type>::getFilterF(udt*& filter_f) const
    {
	if (filter_f == 0)
	{
	    filter_f = new Sequence<type>;
	}
	if (std::valarray<type>* p = dynamic_cast<std::valarray<type>*>(filter_f))
	{
	    p->resize(m_history_y.size());
	    (*p) = m_theta[std::slice(m_history_u.size(), m_history_y.size(), 1)];
	}
	else
	{
	    throw std::invalid_argument("OEModel::Implementation::getFilterF(udt*& theta): requres null or a pointer to a sequence UDT");
	}       
    }

    template<typename type>
    void OEModel::Implementation<type>::getFilterB(std::valarray<type>& filter_b) const
    {
        filter_b.resize(m_history_u.size()+1);
        filter_b[0] = type();
        filter_b[std::slice(1, m_history_u.size(), 1)] = m_theta[std::slice(0, m_history_u.size(), 1)];
    }

    template<typename type>
    void OEModel::Implementation<type>::getFilterF(std::valarray<type>& filter_f) const
    {
        filter_f.resize(m_history_y.size());
        filter_f = m_theta[std::slice(m_history_u.size(), m_history_y.size(), 1)];
    }

    template<typename type>
    void OEModel::Implementation<type>::apply(std::valarray<type>& w,
					      const std::valarray<type>& u) const
    {
	w.resize(u.size());

	if (m_theta.size() > 0)
	{
	    for (unsigned int t = 0; t < u.size(); ++t)
	    {
		w[t] = m_state[0];
		m_state = m_state.shift(1);
		m_state[std::slice(0, m_history_u.size(), 1)]
		    += m_theta[std::slice(0, m_history_u.size(), 1)] * u[t];
		m_state[std::slice(0, m_history_y.size(), 1)]
		    += m_theta[std::slice(m_history_u.size(),
				     m_history_y.size(), 1)] * w[t];
	    }
	}
    }

    template<typename type>
    void OEModel::Implementation<type>::apply(udt*& w, const udt& u) const
    {
	if (const std::valarray<type>* p = dynamic_cast<const std::valarray<type>*>(&u))
	{
	    if (w == 0)
	    {
		w = new Sequence<type>;
	    }
	    if (std::valarray<type>* q = dynamic_cast<std::valarray<type>*>(w))
	    {
		apply(*q, *p);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::Implementation<type>::apply(udt*&, const udt&): (output) type mismatch");
	    }
	}
	else
	{
	    throw std::invalid_argument("OEModel::Implementation<type>::apply(udt*&, const udt&): (input) type mismatch");
	}
    }

    static inline std::valarray<float> conj(const std::valarray<float>& x)
    {
	return x;
    }

    static inline std::valarray<double> conj(const std::valarray<double>& x)
    {
	return x;
    }

    static inline std::valarray<std::complex<float> > conj(const std::valarray<std::complex<float> >& x)
    {
	typedef std::complex<float> i_type;
	i_type (*func)( const i_type& ) = std::conj;
	return x.apply(func);
    }

    static inline std::valarray<std::complex<double> > conj(const std::valarray<std::complex<double> >& x)
    {
	typedef std::complex<double> i_type;
	i_type (*func)( const i_type& ) = std::conj;
	return x.apply(func);
    }

    static inline float conj(const float& x)
    {
	return x;
    }

    static inline double conj(const double& x)
    {
	return x;
    }


template<typename type>
static std::ostream& operator<<(std::ostream& a, const std::valarray<type>& b)
{
    a << "{ ";
    if (b.size() <= 10)
    {
	for (unsigned int i = 0; i < b.size(); ++i)
	{
	    a << b[i] << ' ';
	}
    }
    else
    {
	for (unsigned int i = 0; i < 9; ++i)
	{
	    a << b[i] << ' ';
	}
	a << "... " << b[b.size() - 1] << ' ';
    }
    a << "}";
    return a;
}

    template<typename type>
    void OEModel::Implementation<type>::refine(const std::valarray<type>& y,
					       const std::valarray<type>& u)
    {
	// Solve Ax = b for x where
	//   A = \sum_{t=1}^N \psi(t)\psi^T(t)
	//   b = \sum_{t=1}^N \overline{\psi(t)}y(t)
	// and
	//   \psi(t) = [ u(t-1) \ldots u(t-n_b) y(t-1) \ldots y(t - n_f) ]^T

	std::valarray<type> intermediate(m_theta.size());
	std::valarray<type> temporary(m_theta.size());

	for (unsigned int t = 0; t < u.size(); ++t)
	{

	    intermediate[std::slice(0, m_history_u.size(), 1)] = m_history_u;
	    intermediate[std::slice(m_history_u.size(), m_history_y.size(), 1)]
		= m_history_y;

            temporary = conj(intermediate);

	    // accumulate A
	    for (unsigned int i = 0; i < m_theta.size(); ++i)
	    {
		//temporary = conj(intermediate);
		//std::cout << " temporary = intermediate; " << temporary << '\n';
		//temporary *= intermediate[i];
		//std::cout << " temporary *= intermediate[i]; " << temporary << '\n';
		//temporary += m_matrix.getCol(i);
		//std::cout << " temporary += matrix.getCol(i); " << temporary << '\n';
		//m_matrix.setCol(i, temporary);
                m_matrix.column(i) += (temporary * intermediate[i]);
	    }

	    // accumulate b
	    //temporary = conj(intermediate);
	    //std::cout << " temporary = conj(intermediate); " << temporary << '\n';
	    //temporary *= y[t];
	    //std::cout << " temporary *= y[t]; " << temporary << '\n';
	    //temporary += m_vector.getCol(0);
	    //std::cout << " temporary += m_vector.getCol(0); " << temporary << '\n';
	    //m_vector.setCol(0, temporary);
            m_vector.column(0) += (temporary * y[t]);

	    // rotate psi

	    if (m_history_u.size() > 0)
	    {
		m_history_u = m_history_u.shift(-1);
		m_history_u[0] = u[t];
	    }
	    if (m_history_y.size() > 0)
	    {
		m_history_y = m_history_y.shift(-1);
		m_history_y[0] = y[t];
	    }

	    //std::cout << "intermediate" << intermediate << '\n';

	}

	//for (unsigned int i = 0; i < m_matrix.getNRows(); ++i)
	//{
	//    std::cout << "\tA = " << m_matrix.getRow(i) << '\n';
	//}
	//std::cout << "\tb = " << m_vector.getCol(0) << '\n';

	//std::cout << " det A = " << ((m_matrix.getRow(0)[0] * m_matrix.getRow(1)[1]) - (m_matrix.getRow(0)[1] * m_matrix.getRow(1)[0])) << '\n';

	// output buffer

	try
	{

	    Matrix<type> buffer;
	    LinearAlgebra::SV(m_matrix, buffer, m_vector);

	    m_theta = buffer.column(0);
	}
	catch(std::exception& x)
	{
	    throw std::runtime_error(std::string("OEModel::Implementation<type>::refine(y, u): intercepted exception: ") + x.what());
	}

    }

    template<typename type>
    void OEModel::Implementation<type>::refine(const udt& y, const udt& u)
    {
	if (const std::valarray<type>* p
	    = dynamic_cast<const std::valarray<type>*>(&u))
	{
	    if (const std::valarray<type>* q
		= dynamic_cast<const std::valarray<type>*>(&y))
	    {
		refine(*q, *p);
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::Implementation<type>::refine(const udt&, const udt&): (output) type mismatch");
	    }		
	}
	else
	{
	    throw std::invalid_argument("OEModel::Implementation<type>::refine(const udt&, const udt&): (input) type mismatch");
	}
    }

    template<typename type>
    type OEModel::Implementation<type>::merit(const std::valarray<type>& y, const std::valarray<type>& u) const
    {
	//std::cout << "       input " << u << '\n';
	//std::cout << "      output " << y << '\n';
	std::valarray<type> prediction;
	Implementation<type>(m_theta, m_history_u.size()).apply(prediction, u);
	//std::cout << "  prediction " << prediction << '\n';
	prediction -= y;
	//std::cout << "       error " << prediction << '\n';
	prediction *= conj(prediction);
	//std::cout << " square norm " << prediction << '\n';
	//std::cout << "      summed " << prediction.sum() << '\n';
	return ((type)0.5) * prediction.sum() / (type) prediction.size();
    }

    template<typename type>
    void OEModel::Implementation<type>::merit(udt*& m, const udt& y, const udt& u) const
    {
	if (const std::valarray<type>* p
	    = dynamic_cast<const std::valarray<type>*>(&y))
	{
	    if (const std::valarray<type>* r
		= dynamic_cast<const std::valarray<type>*>(&u))
	    {
		if (m == 0)
		{
		    m = new Scalar<type>(merit(*p, *r));
		}
		else if (Scalar<type>* q = dynamic_cast<Scalar<type>*>(m))
		{
		    q->SetValue(merit(*p, *r));
		}
		else
		{
		    throw std::invalid_argument("OEModel::Implementation<type>::merit(udt*&, const udt&, const udt&): (method output) type mismatch");
		}
	    }
	    else
	    {
		throw std::invalid_argument("OEModel::Implementation<type>::merit(udt*&, const udt&, const udt&): (system input) type mismatch");
	    }
	}
	else
	{
	    throw std::invalid_argument("OEModel::Implementation<type>::merit(udt*&, const udt&, const udt&): (system output) type mismatch");
	}
    }

    // instantiations

#define INSTANTIATE(type) \
\
    template OEModel::OEModel(const std::valarray< type >&, \
			      const int&); \
    template OEModel::OEModel(const std::valarray< type >&, \
			      const std::valarray< type >&, \
			      const int&, \
			      const int&); \
    template void OEModel::getTheta(std::valarray< type >&) const; \
    template void OEModel::getFilterF(std::valarray< type >&) const; \
    template void OEModel::getFilterB(std::valarray< type >&) const; \
    template void OEModel::setTheta(const std::valarray< type >&, \
				    const int&); \
    template void OEModel::apply(std::valarray< type >&, \
			         const std::valarray< type >&) const; \
    template void OEModel::refine(const std::valarray< type >&, \
				  const std::valarray< type >&); \
    template type OEModel::merit(const std::valarray< type >&, \
				 const std::valarray< type >&) const; \
    template class OEModel::Implementation< type >;

    INSTANTIATE(float)
    INSTANTIATE(double)
    INSTANTIATE(std::complex<float>)
    INSTANTIATE(std::complex<double>)

#undef INSTANTIATE

} // namespace datacondAPI

UDT_CLASS_INSTANTIATION(OEModel,UDT_DEFAULT_KEY)
