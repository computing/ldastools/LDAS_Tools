#include "datacondAPI/config.h"

#include <sstream>
#include <complex>   

#include <general/unimplemented_error.hh>

#include "InterpolateState.hh"
#include "TimeSeries.hh"
   
using std::complex;   

namespace datacondAPI {

    template<class T>
    InterpolateState<T>::InterpolateState(const double alpha,
                                             const size_t order)
        : State(), Filters::Interpolate<T>(alpha, order) { }

    template<class T>
    InterpolateState<T>* InterpolateState<T>::Clone() const
    {
        return new InterpolateState<T>(*this);
    }

    template<class T>
    void InterpolateState<T>::apply(Sequence<T>& x)
    {
        Filters::Interpolate<T>::apply(x);
        
        std::ostringstream oss;
        
        oss << "interpolate("
            << x.name() << ","
            << this->getAlpha() << ","
            << this->getOrder() << ")";

        x.SetName(oss.str());
    }
    
    template<class T>
    void InterpolateState<T>::apply(TimeSeries<T>& x)
    {
        // Apply as a Sequence<T> - also sets Sequence metaddata
        apply(static_cast<Sequence<T>&>(x));

        const int m = ( this->getOrder() + 1)/2;

        x.SetStartTime(x.GetStartTime() + (this->getAlpha() - m)*x.GetStepSize());
    }
    
    template<class T>
    ILwd::LdasElement*
    InterpolateState<T>::ConvertToIlwd(const CallChain& Chain,
                                       target_type Target) const
    {
        throw General::unimplemented_error(
                      "InterpolateState<T>::ConvertToIlwd() - unimplemented");
    }

    template class InterpolateState<float>;
    template class InterpolateState<double>;

    template class InterpolateState<complex<float> >;
    template class InterpolateState<complex<double> >;
}
