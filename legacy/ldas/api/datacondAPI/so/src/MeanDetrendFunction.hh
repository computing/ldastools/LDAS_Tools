/* -*- mode: c++ -*- */
#ifndef MEANDETRENDFUNCTION_HH
#define	MEANDETRENDFUNCTION_HH

#include "CallChain.hh"

//: Function support for mean detrending
//
// This class implements the meandetrend() action to be used in the 
// algorithms section of a Tcl command. The syntax as it would appear in
// the action sequence is: x = meandetrend(y)
//
class MeanDetrendFunction: CallChain::Function
{
public:
  //: Constructor
  //
  // Construct a new MeanDetrend function
  MeanDetrendFunction();

  //: Evaluate the MeanDetrend Function
  //
  // This performs the MeanDetrend transformation. The list of parameters
  // must contain a single value which is the name of the symbol to
  // use as the input array to the MeanDetrend function.
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& Params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual
  void Eval(      CallChain* Chain,
	    const CallChain::Params& Params,
	    const std::string& Ret) const;

  //: Return the Name of the function
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //!return: of the function
  virtual
  const std::string& GetName() const;

};

#endif	// MEANDETRENDFUNCTION_HH
