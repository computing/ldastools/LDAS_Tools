#ifndef DataConditionUtilHH
#define DataConditionUtilHH

#include "datacondAPI/config.h"

#include <complex>
#include <string>
#include <typeinfo>

#include "general/types.hh"

#include "mime/spectrum.hh"

void string2lower( std::string& s );

namespace datacondAPI
{
  //
  // The unordered_map requires an equality operator and a hash function.
  // These really ought to go in a common spot because they're used
  // elsewhere, like in TypeInfo
  //
  struct LookupEq {
    bool operator()(const std::type_info* p, const std::type_info* q) const
    {
      return (*p == *q);
    }
  };
  
  struct LookupHash {
    MEM_PTR operator()(const std::type_info* p) const
    {
      return (MEM_PTR) p;
    }
  };
    
  static const std::string	DEFAULT_COMPRESSION_METHOD = "gzip";
  static const int		DEFAULT_COMPRESSION_LEVEL = 1;
}

#endif
