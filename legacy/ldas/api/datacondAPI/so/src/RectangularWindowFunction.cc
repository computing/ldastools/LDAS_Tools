// $Id: RectangularWindowFunction.cc,v 1.5 2005/12/01 22:54:58 emaros Exp $
//
// RectangularWindow action
//
// RectangularWindow has no input arguments and one return value. The format is
//
//     window = RectangularWindow();
//
// window is a Window
//

#include "datacondAPI/config.h"

#include "RectangularWindowFunction.hh"
#include "WindowUDT.hh"
#include "ScalarUDT.hh"

static RectangularWindowFunction _RectangularWindowFunction;

RectangularWindowFunction::RectangularWindowFunction()
    : Function( RectangularWindowFunction::GetName() )
{
}

const std::string& RectangularWindowFunction::GetName(void) const
{
    static std::string name("RectangularWindow");

    return name;
}

void RectangularWindowFunction::Eval(CallChain* Chain,
				     const CallChain::Params& Params,
				     const std::string& Ret) const
{
    switch(Params.size())
    {
    case 0:
	// Nothing to do here
        break;

    default:
        throw CallChain::BadArgumentCount("RectangularWindow",
					  0, Params.size());
    }

    CallChain::Symbol* window = new datacondAPI::RectangularWindowUDT();

    Chain->ResetOrAddSymbol( Ret, window );
}
