#ifndef COMPLEX_HH
#define COMPLEX_HH

#include <complex>

//: Functions for promoting a real UDT to it's complexified equivalent in a
//+way which carries meta-data

namespace datacondAPI {

  // Forward declaration
  class udt;
  template<class T> class Scalar;
  template<class T> class Sequence;

  //: Return a complex version of x as a UDT
  //
  // This function returns a complexified version of its input as
  // a UDT. It preserves all other aspects of meta-data and dynamic
  // type while promoting the data elements to complex numbers.
  // For example, if x is a TimeSeries<float> then the return value
  // will be a pointer to a TimeSeries<complex<float> > with identical
  // meta-data to x, and the real part of the elements of the result will be
  // the same as the elements of x.
  //
  // The result UDT is allocated on the heap and it is the callers
  // responsibility to delete it.
  //
  //!param: x - the input UDT.
  //!return: a pointer to a UDT created on the heap.
  //!exc: invalid_argument - Thrown if the given UDT cannot be converted
  //+ to complex form.
  udt* Complex(const udt& x);

  //: Return a complex version of x as a Scalar
  //
  // This function returns a complexified version of any class based on
  // Scalar<T> as a pointer to a Scalar<complex<T> >.
  // It preserves all other aspects of meta-data and dynamic
  // type while promoting the data elements to complex numbers.
  //
  // The result Scalar is allocated on the heap and it is the callers
  // responsibility to delete it.
  //
  //!param: x - the input Scalar
  //!return: a pointer to a complex Scalar created on the heap.
  //!exc: invalid_argument - Thrown if the given Scalar cannot be converted
  //+ to complex form.
  Scalar<std::complex<double> >* Complex(const Scalar<double>& x);

  //: Return a complex version of x as a Sequence
  //
  // This function returns a complexified version of any class based on
  // Sequence<T> as a pointer to a Sequence<complex<T> >.
  // It preserves all other aspects of meta-data and dynamic
  // type while promoting the data elements to complex numbers.
  // For example, if x is a TimeSeries<float> then the return value
  // will be a pointer to a TimeSeries<complex<float> > with identical
  // meta-data to x, and the real part of the elements of the result will be
  // the same as the elements of x.
  //
  // The result Sequence is allocated on the heap and it is the callers
  // responsibility to delete it.
  //
  //!param: x - the input Sequence.
  //!return: a pointer to a complex Sequence created on the heap.
  //!exc: invalid_argument - Thrown if the given Sequence cannot be converted
  //+ to complex form.
  template<class T>
  Sequence<std::complex<T> >* Complex(const Sequence<T>& x);

}

#endif // COMPLEX_HH
