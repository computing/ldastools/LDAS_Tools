#ifndef _RAN2FUNCTION_HH_
#define _RAN2FUNCTION_HH_

#include "CallChain.hh"

class ran2Function : public CallChain::Function
{
public:
  ran2Function();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_RAN2FUNCTION_HH_
