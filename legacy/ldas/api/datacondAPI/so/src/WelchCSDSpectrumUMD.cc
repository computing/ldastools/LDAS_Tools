/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "config.h"

#include <memory>

#include "general/Memory.hh"

#include "WelchCSDSpectrumUMD.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

unsigned int datacondAPI::WelchCSDSpectrumUMD::GetFFTLength() const
{
    return m_FFTLength;
}

void datacondAPI::WelchCSDSpectrumUMD::SetFFTLength(const unsigned int& length)
{
    m_FFTLength = length;
}
                
unsigned int datacondAPI::WelchCSDSpectrumUMD::GetFFTOverlap() const
{
    return m_FFTOverlap;
}

void datacondAPI::WelchCSDSpectrumUMD::SetFFTOverlap(const unsigned int& overlap)
{
    m_FFTOverlap = overlap;
}
                
datacondAPI::WindowInfo datacondAPI::WelchCSDSpectrumUMD::GetWindowInfo() const
{
    return m_WindowInfo;
}

void datacondAPI::WelchCSDSpectrumUMD::SetWindowInfo(const WindowInfo& window)
{
    m_WindowInfo = window;
}

ILwd::LdasElement* datacondAPI::WelchCSDSpectrumUMD::
ConvertToIlwd(const CallChain& Chain, datacondAPI::udt::target_type Target) const
{
    std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);

    switch (Target)
    {
    case datacondAPI::udt::TARGET_GENERIC:
        container->push_back(
            new ILwd::LdasArray<unsigned int>(m_FFTLength, "FFTLength"),
	    ILwd::LdasContainer::NO_ALLOCATE,
	    ILwd::LdasContainer::OWN );
        container->push_back(
            new ILwd::LdasArray<unsigned int>(m_FFTOverlap, "FFTOverlap"),
	    ILwd::LdasContainer::NO_ALLOCATE,
	    ILwd::LdasContainer::OWN );
        container->push_back(
            new ILwd::LdasString(m_WindowInfo.name(), "WindowName"),
	    ILwd::LdasContainer::NO_ALLOCATE,
	    ILwd::LdasContainer::OWN );
        container->push_back(
            new ILwd::LdasArray<double>(m_WindowInfo.parameter(),
                                        "WindowParameter"),
	    ILwd::LdasContainer::NO_ALLOCATE,
	    ILwd::LdasContainer::OWN );
        break;

    default:
        throw datacondAPI::udt::BadTargetConversion(Target,
                                           "datacondAPI::WelchCSDSpectrumUMD");
        break;
    }

    return container.release();
}

datacondAPI::WelchCSDSpectrumUMD::WelchCSDSpectrumUMD()
    : m_channel_1(""), m_channel_2(""), m_FFTLength(0), m_FFTOverlap(0)
{
}

datacondAPI::WelchCSDSpectrumUMD::WelchCSDSpectrumUMD(const WelchCSDSpectrumUMD& value)
    : m_channel_1(value.m_channel_1),
      m_channel_2(value.m_channel_2),
      m_FFTLength(value.m_FFTLength),
      m_FFTOverlap(value.m_FFTOverlap),
      m_WindowInfo(value.m_WindowInfo)
{
}
                
datacondAPI::WelchCSDSpectrumUMD::~WelchCSDSpectrumUMD()
{
}
