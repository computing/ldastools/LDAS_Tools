#include "datacondAPI/config.h"

extern "C" {
#include <strings.h>
} /* extern "C" */

#include <sstream>

#include "ActionDriver.hh"
#include "AliasDriver.hh"
#include "DatacondCallerCC.hh"
#include "DatacondCaller.h"
#include "CallChain.hh"
#include "UDT.hh"

int
DatacondCallerCC( const char* Algorithm,
		  const char* Aliases,
		  datacond_symbol_type* Symbols,
		  int NumberOfSymbols,
		  char** ErrorMessage,
		  CallChain& Executioner )
{
  using namespace datacondAPI;
  int retval = DATACOND_OK;

  //---------------------------------------------------------------------
  // Do basic initialization
  //---------------------------------------------------------------------

  if ( ErrorMessage )
  {
    *ErrorMessage = (char*)NULL;
  }
  Executioner.Reset( );

  //---------------------------------------------------------------------
  // Loop over the Symbols looking for data to insert into the
  //   symbol table
  //---------------------------------------------------------------------
  if ( Symbols )
  {
    udt*	native = (udt*)NULL;

    int	nos = NumberOfSymbols;

    for ( datacond_symbol_type* cur = Symbols;
	  nos > 0;
	  ++cur, --nos )
    {
      if ( cur->s_direction == DATACOND_SYMBOL_INPUT )
      {
	native = (udt*)NULL;
	try
	{
	  if ( cur->s_translator == NULL )
	  {
	    throw std::invalid_argument( "Translator function (s_translator) must be non-NULL" );
	  }
	  if ( cur->s_user_data == NULL )
	  {
	    throw std::invalid_argument( "User data (s_user_data) must be non-NULL" );
	  }
	  // Translate the data
	  void* native_ptr = &native;

	  (*(cur->s_translator))( DATACOND_SYMBOL_INPUT,
				  &(cur->s_user_data),
				  (void**)native_ptr,
				  cur->s_aux_data );
	  if ( native == (udt*)NULL )
	  {
	    throw std::range_error( "Translation failed" );
	  }
	}
	catch( const std::invalid_argument& e )
	{
	  if ( ErrorMessage )
	  {
	    std::ostringstream	msg;
	    
	    msg << "Unable to translate symbol '" 
		<< cur->s_symbol_name
		<< "' for input: " << e.what( )
	      ;
	    *ErrorMessage = strdup( msg.str( ).c_str( ) );
	  }
	  return DATACOND_INGESTION_FAILURE;
	}
	catch( const std::range_error& e )
	{
	  if ( ErrorMessage )
	  {
	    std::ostringstream	msg;
	    
	    msg << "Unable to translate symbol " 
		<< cur->s_symbol_name
		<< " for input: " << e.what( )
	      ;
	    *ErrorMessage = strdup( msg.str( ).c_str( ) );
	  }
	  return DATACOND_INGESTION_FAILURE;
	}
	// Add to symbol table
	Executioner.AddSymbol( cur->s_symbol_name, native, false );
      }
    }
  }
  //---------------------------------------------------------------------
  // Parse the aliases
  //---------------------------------------------------------------------
  std::istringstream alias_stream( ( Aliases == (char*)NULL ) ? "" : Aliases );
  AliasDriver	aliases;
  
  if ( aliases.Parse( alias_stream ) == false )
  {
    return DATACOND_PARSE_ALIASES_FAILURE;
  }
  //---------------------------------------------------------------------
  // Parse the actions
  //---------------------------------------------------------------------
  std::istringstream
    action_stream( ( Algorithm == (char*)NULL ) ? "" : Algorithm );

  ActionDriver	actions( aliases, Executioner );
  if ( actions.Parse( action_stream ) == false )
  {
    if ( ErrorMessage )
    {
      *ErrorMessage = strdup( actions.Messages( ).c_str( ) );
    }
    return DATACOND_PARSE_FAILURE;
  }

  //---------------------------------------------------------------------
  // Execute the algorithm
  //---------------------------------------------------------------------

  try
  {
    Executioner.Execute( );
  }
  catch( const std::exception& e )
  {
    if ( ErrorMessage )
    {
      *ErrorMessage = strdup( e.what( ) );
    }
    return DATACOND_EXEC_FAILURE;
  }

  //---------------------------------------------------------------------
  // Populate the OUTPUT fields with the results
  //---------------------------------------------------------------------
  if ( ( Symbols ) &&
       ( retval == DATACOND_OK ) )
  {
    int nos = NumberOfSymbols;
    std::ostringstream	errors;
    bool first = true;

    for ( datacond_symbol_type* cur = Symbols;
	  nos > 0;
	  ++cur, --nos )
    {
      if ( cur->s_direction == DATACOND_SYMBOL_OUTPUT )
      {
	udt*	symbol = (udt*)NULL;
	//:TODO: Determine if it is a symbol or an output statement
	//:TODO:   Numeric s_symbol_names are output, all others are
	//:TODO:   symbols

	//:TODO: Fetch the symbol from the output statement
	// Fetch the symbol from the symbol table.
	try
	{
	  std::string sym( cur->s_symbol_name );
	  std::string pattern( "symbol:" );

	  if ( sym.compare( 0, pattern.length( ), pattern ) == 0 )
	  {
	    // Look this one up in the symbol table
	    symbol = Executioner.GetSymbol( &cur->s_symbol_name[ pattern.length( ) ] );
	  }
	  else
	  {
	    // Look this one up in the intermediate results
	    symbol = Executioner.GetIntermediateByName( cur->s_symbol_name );
	    // Fix up the name to be what the user requested
	    symbol->SetName( cur->s_symbol_name );
	  }
	}
	catch( const std::exception& e )
	{
	  if ( ErrorMessage )
	  {
	    if ( !first )
	    {
	      errors << std::endl;
	    }
	    else
	    {
	      first = false;
	    }
	    errors << e.what( );
	  }
	  retval = DATACOND_EXEC_FAILURE;
	}


	if ( retval == DATACOND_OK )
	{
	  void* symbol_ptr = &symbol;
	  // Handle translation of symbols
	  (*(cur->s_translator))( DATACOND_SYMBOL_OUTPUT,
				  &(cur->s_user_data),
				  (void**)symbol_ptr,
				  cur->s_aux_data );
	}
	else
	{
	  if ( ErrorMessage )
	  {
	    *ErrorMessage = strdup( errors.str( ).c_str( ) );
	  }
	}
      }
    }
  }
  return retval;
}

int
DatacondCaller( const char* Algorithm,
		const char* Aliases,
		datacond_symbol_type* Symbols,
		int NumberOfSymbols,
		char** ErrorMessage )
{
  CallChain	cc;

  return DatacondCallerCC( Algorithm,
			   Aliases,
			   Symbols,
			   NumberOfSymbols,
			   ErrorMessage,
			   cc );
}
