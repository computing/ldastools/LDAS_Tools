#ifndef DETREND_HH
#define DETREND_HH

namespace datacondAPI {

    // Forward declarations
    class udt;
    template<class T> class Sequence;
    template<class T> class TimeSeries;

    //: Base class for detrending operations on Sequences and TimeSeries
    class Detrend {
    public:

	//: Types of detrending allowed
	enum DetrendMethod {
            mean,
            linear
        };

	//: Constructor
	Detrend(const DetrendMethod method);

	//: Obtain detrending method
	DetrendMethod getMethod() const;

	//: Apply detrending to UDT
	//
	// Applies the selected detrending algorithm to the input UDT,
	// creating the output if 'out' is 0. If 'out' is created,
	// it comes back as the same dynamic type as 'in'
	void apply(udt*& out, const udt& in) const;

	//: Apply detrending to Sequence
	//
	// The selected detrending algorithm is applied to the entire
	// Sequence
	template<class T>
	void apply(Sequence<T>& out, const Sequence<T>& in) const;

	//: Apply detrending to TimeSeries
	//
	// The selected detrending algorithm is applied only to the
	// parts of the TimeSeries which are not gaps. Only data not in
	// a gap contributes to the mean of the data or to a linear
	// fit. Only data not in gaps has the detrending applied to it,
	// data in gaps is completely unchanged.
	template<class T>
	void apply(TimeSeries<T>& out, const TimeSeries<T>& in) const;

    private:
	//: Dispatch to the correct apply method
	template<class T>
	void dispatch(udt*& out, const T& in) const;
	
	//: Mean detrending
	// 
	// Detrend a TimeSeries by taking out the mean.
	// Does not affect the gaps at all.
	//
	template<class T>
	void meanDetrend(TimeSeries<T>& in) const;

	//: Linear detrending
	// 
	// Detrend a TimeSeries by taking out the mean.
	// Does not affect the gaps at all.
	//
	template<class T>
	void linearDetrend(TimeSeries<T>& in) const;

	DetrendMethod m_method;
    };
    
    inline
    Detrend::DetrendMethod Detrend::getMethod() const { return m_method; }

} // namespace datacondAPI

#endif // DETREND_HH
