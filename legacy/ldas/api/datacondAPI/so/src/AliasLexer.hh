#ifndef DATACONDAPI__ALIAS_LEXER_HH
#define DATACONDAPI__ALIAS_LEXER_HH

#ifndef DATACOND_API__ALIAS_LEXER_CC
#define yyFlexLexer AliasFlexLexer
#include <FlexLexer.h>
#undef yyFlexLexer
#endif /* DATACOND_API__ALIAS_LEXER_CC */

namespace datacondAPI
{
  class AliasDriver;
}

#include "AliasParser.hh"

namespace datacondAPI
{
  class AliasLexer
    : public AliasFlexLexer
  {
  public:
    typedef AliasParser::semantic_type              semantic_type;
    typedef AliasParser::location_type              location_type;

    AliasLexer( std::istream* ArgYYIn = 0, std::ostream* ArgYYOut = 0);

    // This function is defined in ActionLexerLL.ll
    virtual int yylex( );

    void SetYYLloc( location_type* YYLloc );
    void SetYYLval( semantic_type* YYLval );

  private:
    semantic_type*      m_yylval;
    location_type*      m_yylloc;
  };

  inline void AliasLexer::
  SetYYLloc( location_type* YYLloc )
  {
    m_yylloc = YYLloc;
  }

  inline void AliasLexer::
  SetYYLval( semantic_type* YYLval )
  {
    m_yylval = YYLval;
  }

} /* namespace - datacondAPI */

#ifdef ALIAS_FLEX_LEXER_DEFINED
#undef yyFlexLexer
#undef ALIAS_FLEX_LEXER_DEFINED
#endif

#endif /* DATACONDAPI__ALIAS_LEXER_HH */
