// Wavelet Analysis Tool
//--------------------------------------------------------------------
// Implementation of 
// the Haar wavelet transform using lifting scheme 
// References:
//   A.Cohen, I.Daubechies, J.Feauveau Bases of compactly supported wavelets
//   Comm. Pure. Appl. Math. 45, 485-560, 1992
//   W. Sweldens - Building your own wavelets at home
//--------------------------------------------------------------------
//$Id: Haar.cc,v 1.5 2005/12/01 22:54:58 emaros Exp $

#define BIORTHOGONAL_CC

#include "datacondAPI/config.h"

#include "Haar.hh"

namespace datacondAPI {
namespace wat {

// constructors

template<class DataType_> Haar<DataType_>::
Haar(const Wavelet &w) : 
WaveDWT<DataType_>(w) 
{ 
   this->m_WaveType = HAAR;
}

template<class DataType_> Haar<DataType_>::
Haar(const Haar<DataType_> &w) : 
WaveDWT<DataType_>(w) 
{ 
   this->m_WaveType = HAAR;
}

template<class DataType_> Haar<DataType_>::
Haar(int tree) :
WaveDWT<DataType_>(1,1,tree,B_CYCLE) 
{
  this->m_WaveType = HAAR;
}

// destructor
template<class DataType_>
Haar<DataType_>::~Haar()
{  }

// clone
template<class DataType_>
Haar<DataType_>* Haar<DataType_>::Clone() const
{
  return new Haar<DataType_>(*this);
}

// decompose function does one step of forward transformation.
// <level> input parameter is the level to be transformed
// <layer> input parameter is the layer to be transformed.
template<class DataType_>
void Haar<DataType_>::forward(int level,int layer)
{
   level++;                           // increment level (next level now)
   register int stride = 1<<level;    // stride parameter

   unsigned int i;
   
   register DataType_ *dataA;
   register DataType_ *dataD; 

   dataA=this->pWWS+
     this->getOffset(level,layer<<1);     // pointer to approximation layer
   dataD=this->pWWS+
     this->getOffset(level,(layer<<1)+1); // pointer to detail layer

// predict
  for(i=0; i<this->nWWS; i+=stride) {
    *(dataD+i) -= *(dataA+i);
  }

// update
  for(i=0; i<this->nWWS; i+=stride) {
    *(dataA+i) += *(dataD+i) * 0.5;
  }
}

// reconstruct function does one step of inverse transformation.
// <level> input parameter is the level to be reconstructed
// <layer> input parameter is the layer to be reconstructed.
template<class DataType_>
void Haar<DataType_>::inverse(int level,int layer)
{
   level++;                             // increment level (next level now)
   register int stride = 1<<level;      // stride parameter

   unsigned int i;
   
   register DataType_ *dataA;
   register DataType_ *dataD; 

   dataA=this->pWWS+
     this->getOffset(level,layer<<1);     // pointer to approximation layer
   dataD=this->pWWS+
     this->getOffset(level,(layer<<1)+1); // pointer to detail layer

// undo update
  for(i=0; i<this->nWWS; i+=stride) {
    *(dataA+i) -= *(dataD+i) * 0.5;
  }

// undo predict
  for(i=0; i<this->nWWS; i+=stride) {
    *(dataD+i) += *(dataA+i);
  }
}

// instantiations

#define CLASS_INSTANTIATION(class_) template class Haar< class_ >;

CLASS_INSTANTIATION(float)
CLASS_INSTANTIATION(double)
//CLASS_INSTANTIATION(std::complex<float>)
//CLASS_INSTANTIATION(std::complex<double>)

#undef CLASS_INSTANTIATION

}  // end namespace wat
}  // end namespace datacondAPI






