#ifndef _GASDEVFUNCTION_HH_
#define _GASDEVFUNCTION_HH_

#include "CallChain.hh"

class gasdevFunction : public CallChain::Function
{
public:
  gasdevFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_GASDEVFUNCTION_HH_
