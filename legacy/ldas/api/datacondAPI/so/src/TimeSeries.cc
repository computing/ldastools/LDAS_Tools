/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: TimeSeries.cc,v 1.59 2006/11/27 21:32:14 emaros Exp $

#include "config.h"

#include <complex>
#include <memory>
#include <stdexcept>

#include "general/Memory.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrDetector.hh"
#include "ilwdfcs/FrHistory.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "CallChain.hh"
#include "TimeSeries.hh"

#if 0
#undef AT
#define AT() cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << endl << flush
#else
#undef AT
#define	AT()
#endif

using General::GPSTime;

template<class DataType_>
datacondAPI::TimeSeries<DataType_>::TimeSeries()
  : Sequence<DataType_>(),
    TimeSeriesMetaData(1.0)
{
  AT();
}

template<class DataType_>
datacondAPI::TimeSeries<DataType_>::
TimeSeries(const Sequence<DataType_>& data, const double& fs,
	   const GPSTime& StartTime) 
  : Sequence<DataType_>(data),
    TimeSeriesMetaData(fs, StartTime)
{
  AT();
}

template<class DataType_>
datacondAPI::TimeSeries<DataType_>::
TimeSeries(const TimeSeries<DataType_>& data) 
  : Sequence<DataType_>(data),
    TimeSeriesMetaData(data)
{
  AT();
}

template<class DataType_>
datacondAPI::TimeSeries<DataType_>::~TimeSeries()
{
  AT();
}

template <class DataType_>
void datacondAPI::TimeSeries<DataType_>::
CopyMetaData( const udt& Metadata )
{
  if ( this != &Metadata )
  {
    AT();
    try {
      // Copy the timeseries metadata
      static_cast<TimeSeriesMetaData&>( *this ) =
	dynamic_cast< const TimeSeriesMetaData& >( Metadata );
    }
    catch( ... )
    {
      // Cannot copy the metadata if the metadata is unknown
      throw std::runtime_error( "Metadata is not appropriate for TimeSeries" );
    }
  }
}

template <class DataType_>
datacondAPI::TimeSeries<DataType_>* datacondAPI::TimeSeries<DataType_>::
Clone(void) const
{
  AT();
  return new TimeSeries<DataType_>(*this);
}

template<class DataType_>
ILwd::LdasElement* datacondAPI::TimeSeries<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  AT();

  std::unique_ptr< ILwd::LdasContainer> container( new ILwd::LdasContainer );

  switch(Target)
  {
  case udt::TARGET_FRAME:
  case udt::TARGET_FRAME_FINAL_RESULT:
    {
      using namespace ILwdFCS;

      TimeSeries<DataType_>& data =
        const_cast<TimeSeries<DataType_>&>(*this);
      const std::string& lname( Chain.GetIntermediateName( *this,
							   this->name( ) ) );
      
      FrProcData proc_data;

      proc_data.SetName( lname );
      proc_data.SetComment( Chain.GetIntermediateComment( *this ) );
      proc_data.SetType( ILwdFCS::FrProcData::TIME_SERIES );

      //:NOTE: undefined for this class
      // proc_data.SetSubType( ILwdFCS::FrProcData::UNKNOWN_SUB_TYPE );
      // proc_data.SetTimeOffset( 0.0 );

      proc_data.SetTRange( GetEndTime() - GetStartTime() );

      //
      // 
      // These tell you the freq (in Hertz for a TimeSeries and phase of
      // a mixer that was applied to the TimeSeries. The frequency of a Mixer
      // is fixed, whereas the phase is adjusted after each call to
      // Mixer::apply(). The operation is
      //
      //     y_k = x_k * e^{ i * (pi*freq*k + phase) }
      //
      // in Nyquist units or
      //
      //     y_k = x_k * e^{ i * (2*pi*freq * t_k + phase) }
      //
      // in Hertz.
      //
      //:NOTE: The exponential has the opposite sign to what is specified
      // in the frame spec, so we need to negate it on output (and input
      // from frames - see CallChainIngestUDT).
      //
      proc_data.SetFShift( -GetBaseFrequency() );
      proc_data.SetPhase( -GetPhase() );

      //:NOTE: undefined for this class (unless you set it to Nyquist?)
      // proc_data.SetFRange( 0.0 );
      // proc_data.SetBW( 0.0 );

      //:NOTE: Add aux params here

      // Add data
      FrVect vect_data;
      vect_data.SetData( &data[0],
                         data.size(),
                         0.0,
                         "seconds",
                         GetStepSize() );
      vect_data.SetName( lname );

      proc_data.AppendData( vect_data );

      proc_data.AppendHistory( this->getHistory(), GPSTime::NowGPSTime() );

      WhenMetaData::Store( proc_data );

      container.reset( const_cast<CallChain&>(Chain).
		       FormatFrame( Target,
				    container.get( ),
				    proc_data ) );

      // Store the metadata in an FrDetector struct
      GeometryMetaData::Store( const_cast<CallChain&>(Chain).
			       GetFrameHeaderTop() );
    }
    break;

  case udt::TARGET_WRAPPER:
    {
      TimeSeries<DataType_>& data
        = const_cast<TimeSeries<DataType_>&>(*this);
      std::string domain( this->GetDomain() );
      domain += ":domain";
	
      container->appendName( this->name() );
      container->appendName("channel");
      container->appendName("sequence");
      container->push_back(new ILwd::LdasString("TIME", domain),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      container->push_back(new ILwd::LdasArray<REAL_8>
			   (GetStepSize(), "time:step_size", "sec"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      WhenMetaData::ConvertToIlwd( Chain, Target, *( container.get( ) ) );
      
      container->push_back(new ILwd::LdasArray<REAL_8>
			   (GetBaseFrequency(), "base_freq", "hz"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      container->push_back(new ILwd::LdasArray<REAL_8>
			   (GetPhase(), "phase", "radians"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );

      // Store the meta data
      GapMetaData<GPSTime>::Store( *container );
      GeometryMetaData::Store( *container );

      // Push the time-series data
      container->push_back(new ILwd::LdasArray<DataType_>
			   (&data[0], data.size(), "data"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
    }
    break;

  case udt::TARGET_GENERIC:
    container->push_back( Sequence<DataType_>::ConvertToIlwd(Chain, Target),
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
    container->push_back( TimeSeriesMetaData::ConvertToIlwd(Chain, Target),
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
    container->push_back( WhenMetaData::ConvertToIlwd(Chain, Target),
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
    container->push_back(new ILwd::LdasString( this->name(), "NameMetaData" ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
    GapMetaData<GPSTime>::Store( *container );
    GeometryMetaData::Store( *container );
    break;

  default:
    throw udt::BadTargetConversion(Target,
                                   "datacondAPI::TimeSeries");
    break;
  }

  AT();
  return container.release();
}

// accessors

// Accessor required by WhenMetaData
template <class DataType_>
GPSTime datacondAPI::TimeSeries<DataType_>::GetEndTime() const
{
  return ( GetStartTime() + this->size()*GetStepSize() );
}

// mutators

template <class DataType_>
datacondAPI::TimeSeries<DataType_>& 
datacondAPI::TimeSeries<DataType_>::
operator=(const DataType_ d) 
{
  AT();
  Sequence<DataType_>::operator=(d);
  return *this;
}

///----------------------------------------------------------------------
/// Private methods
///----------------------------------------------------------------------

// instantiations

#undef CLASS_INSTANTIATION
#define CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::TimeSeries< class_ >; \
UDT_CLASS_INSTANTIATION(TimeSeries< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)
