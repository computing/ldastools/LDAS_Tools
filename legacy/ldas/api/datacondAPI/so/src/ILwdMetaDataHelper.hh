#ifndef DATACONDAPI__ILWD_META_DATA_HELPER_HH
#define DATACONDAPI__ILWD_META_DATA_HELPER_HH

#include "general/unordered_map.hh"   
   
namespace ILwd
{
  class LdasContainer;
  class LdasElement;
}

namespace datacondAPI
{
  class ILwdMetaDataHelper
  {
  public:
    typedef enum {
      ARM_X_AZIMUTH,
      ARM_X_ALTITUDE,
      ARM_Y_AZIMUTH,
      ARM_Y_ALTITUDE,
      DATA,
      DATA_QUALITY,
      DELTA_TIME,
      DETECTOR,
      ELEVATION,
      FREQUENCY_SHIFT,
      HISTORY,
      LATITUDE,
      LATITUDE_DEGREES,
      LATITUDE_MINUTES,
      LATITUDE_SECONDS,
      LOCAL_TIME,
      LONGITUDE,
      LONGITUDE_DEGREES,
      LONGITUDE_MINUTES,
      LONGITUDE_SECONDS,
      PHASE,
      PROC_DATA,
      SAMPLE_RATE,
      START_TIME,
      SUBTYPE,
      TIME_OFFSET,
      TRANGE,
      TYPE
    } meta_data_type;

    ILwdMetaDataHelper( const ILwd::LdasContainer& Container );

    ILwdMetaDataHelper( const ILwd::LdasElement& Element );

    template<class Type_>
    Type_
    Get( meta_data_type Key ) const;

    template<class Type_>
    Type_
    Get( meta_data_type Key, const Type_& Default ) const;

    const ILwd::LdasContainer& GetContainer( ) const;

    const ILwd::LdasElement* GetILwd( meta_data_type Key,
				      bool ThrowException = true ) const;

    template<class Type_>
    const Type_* GetILwd( meta_data_type Key,
			   bool ThrowException = true ) const;

    template<class Type_>
    Type_* GetILwd( meta_data_type Key,
		     bool ThrowException = true );

    static const std::string& ReverseLookup( meta_data_type Type );

  private:
    typedef General::unordered_map<int,const ILwd::LdasElement*> 
       child_map_type;
    //: Container from which the meta data was derived
    const ILwd::LdasContainer&	m_container;

    //: Map of the LdasElements that corrispond to the input data.
    child_map_type			m_children;

    void init();

    template<class Type_>
    const Type_* get_ilwd( meta_data_type Key,
			    bool ThrowException = true ) const;
  };

  inline const ILwd::LdasContainer& ILwdMetaDataHelper::
  GetContainer( ) const
  {
    return m_container;
  }
}

#endif	/*  DATACONDAPI__ILWD_META_DATA_HELPER_HH */
