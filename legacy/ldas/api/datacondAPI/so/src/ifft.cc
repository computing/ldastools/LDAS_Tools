#include "datacondAPI/config.h"

#include <stdexcept>
#include <memory>
#include <complex>

#include "general/Memory.hh"

#include "ifft.hh"
#include "FFTPlan.hh"

#include "DataCondAPIConstants.hh"
#include "SequenceUDT.hh"
#include "DFTUDT.hh"

template<class TOut, class TIn>
void datacondAPI::IFFT::dispatch(udt*& out, const DFT<std::complex<TIn> >& in)
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    std::unique_ptr<Sequence<TOut> > tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
	tmp.reset(new Sequence<TOut>);
	tmp_out = tmp.get();
    }
    else if (!udt::IsA<Sequence<TOut> >(*tmp_out))
    {
	// Stupidly can't delete the output if it's the wrong type,
	// have to throw an exception instead
	throw std::invalid_argument("IFFT::apply() "
				    "output UDT must be a Sequence");
    }

    apply(udt::Cast<Sequence<TOut> >(*tmp_out), in);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
	// transfer ownership from the unique_ptr to 'out' so that the
	// DFT isn't deleted when the unique_ptr goes out of scope
	out = tmp.release();
    }
}

void datacondAPI::IFFT::apply(udt*& out, const udt& in)
{
    // note that memory does not leak if exceptions are thrown because out
    // is in the scope of the caller and the caller is responsible for it
    
    if (udt::IsA<DFT<std::complex<float> > >(in))
    {
        const DFT<std::complex<float> >& in_ref
	    = udt::Cast<DFT<std::complex<float> > >(in);

	if (in_ref.IsSymmetric())
	{
	    dispatch<float, float>(out, in_ref);
	}
	else
	{
	    dispatch<std::complex<float>, float>(out, in_ref);
	}
    }
    else if (udt::IsA<DFT<std::complex<double> > >(in))
    {
        const DFT<std::complex<double> >& in_ref
	    = udt::Cast<DFT<std::complex<double> > >(in);

	if (in_ref.IsSymmetric())
	{
	    dispatch<double, double>(out, in_ref);
	}
	else
	{
	    dispatch<std::complex<double>, double>(out, in_ref);
	}
    }
    else
    {
        // input type is unsupported
        throw std::invalid_argument("IFFT::apply() "
				    "input UDT must be a DFT");
    }
}

void datacondAPI::IFFT::apply(std::valarray<float>& out,
			      const std::valarray<std::complex<float> >& in)
{
    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take IFFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take IFFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
	out.resize(n);
    }

    float* const out_ptr = &out[0];

    const RealFloatFFTPlan plan(n, false);
    
    size_t i = 0;

    for (i = 0; i <= n/2; ++i)
    {
	out_ptr[i] = in[i].real();
    }

    for (i = n/2 + 1; i < n; ++i)
    {
	out_ptr[i] = in[n-i].imag();
    }

    plan.apply(out_ptr);
    
    out /= n;
}

void datacondAPI::IFFT::apply(std::valarray<double>& out,
                           const std::valarray<std::complex<double> >& in)
{
    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take IFFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take IFFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
	out.resize(n);
    }

    double* const out_ptr = &out[0];

    const RealDoubleFFTPlan plan(n, false);
    
    size_t i = 0;

    for (i = 0; i <= n/2; ++i)
    {
	out_ptr[i] = in[i].real();
    }

    for (i = n/2 + 1; i < n; ++i)
    {
	out_ptr[i] = in[n-i].imag();
    }

    plan.apply(out_ptr);
    
    out /= n;
}

void datacondAPI::IFFT::apply(std::valarray<std::complex<float> >& out,
			      const std::valarray<std::complex<float> >& in)
{
    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take IFFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take IFFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
	out.resize(n);
    }

    out = in;

    const ComplexFloatFFTPlan plan(n, false);

    plan.apply(&out[0]);
    
    std::complex< float > complex_n( n );
    out /= complex_n;
}

void datacondAPI::IFFT::apply(std::valarray<std::complex<double> >& out,
			      const std::valarray<std::complex<double> >& in)
{
    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take IFFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take IFFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
	out.resize(n);
    }

    out = in;

    const ComplexDoubleFFTPlan plan(n, false);

    plan.apply(&out[0]);
    
    std::complex< double > complex_n( n );
    out /= complex_n;
}
