/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef CSDSPECTRUMUMD_HH
#define CSDSPECTRUMUMD_HH
// $Id: CSDSpectrumUMD.hh,v 1.6 2006/02/16 16:54:58 emaros Exp $
#include <string>

#include "UDT.hh"
#include "CSDEstimate.hh"

namespace ILwd
{
  class LdasElement;
}

class CallChain;

namespace datacondAPI
{

  //: Metadata associated with cross-spectral densities
  class CSDSpectrumUMD
  {
    
  public:
    
    // manipulators
    
    //: How is data detrended?
    //!todo: inappropriate metadata: eliminate!
    CSDEstimate::DetrendMethod GetDetrendMethod();
    void SetDetrendMethod(const CSDEstimate::DetrendMethod& detrendMethod);
    
    //: How is CSD estimated?
    //!todo: inappropriate metadata: eliminate!
    std::string GetEstimator();
    void SetEstimator(const std::string& what);
    
    // output
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - CallChain *this is part of
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasElement* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasElement* 
    ConvertToIlwd(const CallChain& Chain, 
		  datacondAPI::udt::target_type Target
		  = datacondAPI::udt::TARGET_GENERIC) const;
    
  protected:
    
    // constructors         

    //: Default constructor
    //!todo: why protected?
    CSDSpectrumUMD();

    //: Copy constructor
    //!todo: why protected?
    CSDSpectrumUMD(const CSDSpectrumUMD& value);
    
    // destructor
    //: Destructor
    //!todo: why protected?
    virtual ~CSDSpectrumUMD();
    
  private:
    
    CSDEstimate::DetrendMethod m_detrendMethod;
    std::string m_what;
    
  }; // class CSDSpectrumUMD
  
} // namespace datacondAPI

#endif // CSDSPECTRUMUMD_HH
