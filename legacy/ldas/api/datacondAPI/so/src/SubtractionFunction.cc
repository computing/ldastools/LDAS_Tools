#include "config.h"

#include <stdio.h>

#include "SubtractionFunction.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "WhenUMD.hh"
#include "TypeInfo.hh"

//=============================================================================
// Static Helper functions
//=============================================================================
static datacondAPI::udt* eval(const datacondAPI::udt& Unknown1,
			      const datacondAPI::udt& Unknwon2,
			      bool Reverse);

static datacondAPI::udt* eval2(const double Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const float Known,
			      const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const int Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const datacondAPI::Sequence<float>& Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const datacondAPI::Sequence<double>& Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);

template<class Out, class In1, class In2>
inline datacondAPI::Scalar<Out>* sub(const In1& Value1,
				     const In2& Value2,
				     bool Reverse)
{
  return new datacondAPI::Scalar<Out>
    ((Reverse ? (Value2 - Value1) : (Value1 - Value2)));
}


template<class Out, class In1, class In2>
datacondAPI::Sequence<Out>* sub_sequence(const In1& Value1,
					 const In2& Value2,
					 bool Reverse);

//=============================================================================
// Subtraction Function
//=============================================================================

SubtractionFunction::
SubtractionFunction()
: Function( SubtractionFunction::GetName() )
{
}

void SubtractionFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
const
{
  using namespace datacondAPI;

  if (2 != Params.size())
  {
    throw CallChain::BadArgumentCount( GetName(), 2, Params.size() );
  }

  datacondAPI::udt*	left(Chain->GetSymbol(Params[0]));
  datacondAPI::udt*	right(Chain->GetSymbol(Params[1]));
  datacondAPI::udt*	answer = 0;

  if (!(answer = eval(*left, *right, false)) &&
      !(answer = eval(*right, *left, true)))
  {
    std::string msg("unable to subtract type ");
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*right));
    msg += " from type ";
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*left));
    throw std::runtime_error(msg);
  }

  // Set metadata
  answer->SetName("sub(" + left->name() + "," + right->name() + ")");

  // Set GPS time of result, if present.
  // Set it from the first argument if possible, otherwise try the
  // second. Otherwise, use the default.
  if (WhenMetaData* const answer_wmd = dynamic_cast<WhenMetaData*>(answer))
  {
      if (const WhenMetaData* wmd = dynamic_cast<const WhenMetaData*>(left))
      {
	  *answer_wmd = *wmd;
      }
      else if (const WhenMetaData* wmd = dynamic_cast<const WhenMetaData*>(right))
      {
	  *answer_wmd = *wmd;
      }
  }

  Chain->ResetOrAddSymbol( Ret, answer );
}

const std::string& SubtractionFunction::
GetName(void) const
{
  static std::string name("sub");
  return name;
}


static SubtractionFunction  _SubtractionFunction;

//=======================================================================
// Definitions of static helper functions
//=======================================================================

static datacondAPI::udt*
eval(const datacondAPI::udt& Unknown1, const datacondAPI::udt& Unknown2,
     bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< datacondAPI::Scalar<int> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<int> >(Unknown1), Unknown2, Reverse);
  }
  else if (udt::IsA< datacondAPI::Scalar<float> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<float> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< datacondAPI::Scalar<double> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<double> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< datacondAPI::Sequence<float> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<float> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< datacondAPI::Sequence<double> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<double> >(Unknown1), Unknown2,
		 Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const double Known, const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // double = double + int
    return sub<double>(Known, udt::Cast< Scalar<int> >(Unknown),
		       Reverse);
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // float = double + float
    return sub<float>(Known, udt::Cast< Scalar<float> >(Unknown),
		      Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // double = double + double
    return sub<double>(Known, udt::Cast< Scalar<double> >(Unknown),
		       Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const float Known, const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // float = float - int
    return sub<float>(Known, udt::Cast< Scalar<int> >(Unknown),
		      Reverse);
  }	
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // float = float - float
    return sub<float>(Known, udt::Cast< Scalar<float> >(Unknown),
		      Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const int Known, const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    return sub<int>(Known, udt::Cast< Scalar<int> >(Unknown),
		    Reverse);
  }
  return (datacondAPI::udt*)NULL;
}


static datacondAPI::udt*
eval2(const datacondAPI::Sequence<float>& Known,
      const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  datacondAPI::Sequence<float>* ret((datacondAPI::Sequence<float>*)NULL);
  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<float> += int
    ret = sub_sequence<float>(Known, udt::Cast< Scalar<int> >(Unknown),
			      Reverse);
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<float> += float
    ret = sub_sequence<float>(Known, udt::Cast< Scalar<float> >(Unknown),
			      Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<float> += double
    ret = sub_sequence<float>(Known,
			      udt::Cast< Scalar<double> >(Unknown),
			      Reverse);
  }
  else if (udt::IsA< Sequence<float> >(Unknown))
  {
    // sequence<float> += sequence<float>
    ret = sub_sequence<float>(Known,
			      udt::Cast< Sequence<float> >(Unknown),
			      Reverse);
  }
  return ret;
}

static datacondAPI::udt*
eval2(const datacondAPI::Sequence<double>& Known,
      const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  datacondAPI::Sequence<double>* ret((datacondAPI::Sequence<double>*)NULL);
  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<double> += int
    ret = sub_sequence<double>(Known, udt::Cast< Scalar<int> >(Unknown),
			       Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<double> += double
    ret = sub_sequence<double>(Known, udt::Cast< Scalar<double> >(Unknown),
			       Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<double> += double
    ret = sub_sequence<double>(Known,
			       udt::Cast< Scalar<double> >(Unknown),
			       Reverse);
  }
  else if (udt::IsA< Sequence<double> >(Unknown))
  {
    // sequence<double> += sequence<double>
    ret = sub_sequence<double>(Known,
			       udt::Cast< Sequence<double> >(Unknown),
			       Reverse);
  }
  return ret;
}

// In the above, this function is only ever called with a Sequence
// on the LHS
template<class Out, class In1, class In2>
datacondAPI::Sequence<Out>*
sub_sequence(const In1& Value1, const In2& Value2, bool Reverse)
{
  // A clone of the first argument is made - there is no need
  // to copy any meta-data.

  datacondAPI::Sequence<Out>* ret = Value1.Clone();

  if (Reverse)
  {
    // Value2 - Value1
    (*ret) *= -1;
    (*ret) += Value2;
  }
  else
  {
    // Value1 - Value2
    (*ret) -= Value2;
  }

  return ret;
}
