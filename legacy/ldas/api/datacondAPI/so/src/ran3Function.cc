#include "datacondAPI/config.h"

#include "ran3state.hh"
#include "SequenceUDT.hh"
#include "random.hh"
#include "ran3Function.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"

const std::string& ran3Function::GetName(void) const
{
  static std::string name("ran3");
  
  return name;
}

ran3Function::ran3Function() : Function( ran3Function::GetName() )
{}

static ran3Function _ran3Function;

void ran3Function::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;   
  using namespace datacondAPI;

  int a = 0, b = 0;

  switch (Params.size())
  {
  case 2:
    {
      if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
	a = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();
      else if(udt::IsA<Scalar<float> >( *Chain->GetSymbol(Params[0])))
	a = (int)udt::Cast<Scalar<float> > (*Chain->GetSymbol(Params[0])).GetValue();
      else if(udt::IsA<Scalar<double> >( *Chain->GetSymbol(Params[0])))
	a = (int)udt::Cast<Scalar<double> > (*Chain->GetSymbol(Params[0])).GetValue();
      
      if(a<=0)
	{
	  throw CallChain::BadArgument(GetName(), 0, "value less than one", "size argument must be greater than zero");
	}
      
      if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	b = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
      else if(udt::IsA<Scalar<float> >( *Chain->GetSymbol(Params[1])))
	b = (int)udt::Cast<Scalar<float> > (*Chain->GetSymbol(Params[1])).GetValue();
      else if(udt::IsA<Scalar<double> >( *Chain->GetSymbol(Params[1])))
	b = (int)udt::Cast<Scalar<double> > (*Chain->GetSymbol(Params[1])).GetValue();
      
      if(b>=0)
	{
	  throw CallChain::BadArgument(GetName(), 2, "value >= 0", "seed value must be negative");
	}
      returnedSymbol = new Sequence<float> (a);
      ran3state state(b);
      ran3(state, udt::Cast<Sequence<float> > (*returnedSymbol), a);
      //    for(int i=0; i<a; i++)
      //  udt::Cast<Sequence<double> >(*returnedSymbol)[i] = ran3(b);
      break;
    }
  case 1:
    {
      if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
	a = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();
      else if(udt::IsA<Scalar<float> >( *Chain->GetSymbol(Params[0])))
	a = (int)udt::Cast<Scalar<float> > (*Chain->GetSymbol(Params[0])).GetValue();
      else if(udt::IsA<Scalar<double> >( *Chain->GetSymbol(Params[0])))
	a = (int)udt::Cast<Scalar<double> > (*Chain->GetSymbol(Params[0])).GetValue();
      
      if(a<=0)
	throw CallChain::BadArgument(GetName(), 0, "value less than one", "size argument must be greater than zero");
      

      b = -248673140;
      
      returnedSymbol = new Sequence<float> (a);
      
      ran3state state(b);
      ran3(state, udt::Cast<Sequence<float> >(*returnedSymbol), a);
      //    for(int i=0; i<a; i++)
      //  udt::Cast<Sequence<double> >(*returnedSymbol)[i] = ran3(b);
      
      break;
    }
  default:
    throw CallChain::BadArgumentCount(GetName(),2,Params.size());
    break;
  
   }

  if (!returnedSymbol)
  {
    throw CallChain::NullResult( GetName() );
  }
  
  Chain->ResetOrAddSymbol( Ret, returnedSymbol );  
}

