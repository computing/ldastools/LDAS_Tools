#ifndef _RAN4STATE_HH_
#define _RAN4STATE_HH_

#include <cmath>
#include <valarray>

#include "general/types.hh"
#include "StateUDT.hh"

namespace datacondAPI
{
  class ran4state : public State
  {
  public:
    
    //: default constructor creates state with default seed
    ran4state();

    //: Construct with particular seed
    //!param: INT_4S - seed, must be negative
    //!exc: std::invalid_argument - seed >= 0 
    ran4state(INT_4S);

    //: Copy Constructor
    //:!param: const ranstate& - state to copy from
    ran4state(const ran4state&);
    
    ~ran4state() {};

    //: reset state with new seed
    //!param: INT_4U - seed, must be negative
    //!exc: std::out_of_range - if seed >= 0 
    void seed(INT_4S);

    //: Generate random number
    //!return: returns a random number
    float getran();

    //: Generate a sequence of random numbers
    //!param: std::valarray<float>& - array to be filled with numbers
    //!param: int - size of array (and quantity of generated numbers)
    void getran(std::valarray<float>&, int);

    ran4state* Clone() const;
    
    ILwd::LdasElement* ConvertToIlwd( const CallChain&,
				      udt::target_type Target) const;

  private:

    void psdes(INT_4U& lword, INT_4U& irword);

    INT_4S idum;
    std::valarray<INT_4U> c1;
    std::valarray<INT_4U> c2;
    INT_4S idums;
    INT_4U jflone;
    INT_4U jflmsk;
  };
}

#endif //_RAN4STATE_HH_
