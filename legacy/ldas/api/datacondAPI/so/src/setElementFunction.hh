/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef SETELEMENTFUNCTION_HH
#define SETELEMENTFUNCTION_HH

// $Id: setElementFunction.hh,v 1.2 2002/12/07 01:32:28 mbarnes Exp $

#include "CallChain.hh"

namespace std {

  template<class T> class complex;

}

namespace datacondAPI {

  class udt;

}

//
//: Function support for setElement action
//
// setElement(y, k, x);  ie. y[k] = x
//
class setElementFunction : public CallChain::Function
{

public:

    setElementFunction();

    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;

    virtual const std::string& GetName() const;

private:
  void checkIndex(const size_t ySize, const int k) const;
  void setRealElement(datacondAPI::udt& y, const int k, const double& x) const;
  void setComplexElement(datacondAPI::udt& y,
                         const int k,
                         const std::complex<double>& x) const;
};

#endif // SETELEMENTFUNCTION_HH
