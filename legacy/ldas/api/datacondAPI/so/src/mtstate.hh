#ifndef MTSTATE_HH
#define MTSTATE_HH

#include <stdexcept>
#include <valarray>

#include "StateUDT.hh"
#include "mtstate.hh"

namespace datacondAPI {

  class mtstate : public State
  {
  public:
    friend class mt1997;

    //: default constructor creates state with default seed
    mtstate();

    //: Construct with particular seed
    //!param: unsigned long - seed, must be non-zero
    //!exc: std::invalid_argument - seed == 0
    mtstate(unsigned long);

    //: Copy constructor
    //!param: const mtstate& - state to copy from
    mtstate(const mtstate&);

    ~mtstate();

    //: reset state with new seed
    //!param: unsigned long - seed, must be non-zero
    //!exc: std::out_of_range - if seed == 0
    void seed(unsigned long);

    //: return next proto-rn
    unsigned long getMT();

    //: return a sequence of consecutive proto-rns
    //!param: std::valarray<unsigned long>& - array of consecutive proto-rns
    //!param: int - number of proto-rns to return (must be > 0)
    //!exc: std::out_of_range - if number proto-rns <= 0
    void getMT(std::valarray<unsigned long>&, int);

    mtstate* Clone() const;

    ILwd::LdasElement* ConvertToIlwd( const CallChain&, 
				      udt::target_type Target) const;  

  private: 
    
    //: generate a block of proto-rns
    void kernel();

    // Period parameters
    const int m_N;
    const int m_M;

    // mutable state parameters
    std::valarray<unsigned long> m_mt;
    int m_mti;
  };

}

#endif
