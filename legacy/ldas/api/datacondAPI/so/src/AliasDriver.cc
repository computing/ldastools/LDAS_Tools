#include "datacondAPI/config.h"

#include "AliasDriver.hh"

namespace datacondAPI
{
  datacondAPI::AliasDriver::
  AliasDriver( )
    : m_scanner( new AliasLexer )

  {
  }

  datacondAPI::AliasDriver::
  ~AliasDriver( )
  {
  }

  bool AliasDriver::
  Parse( std::istream& AliasStream )
  {
    AliasParser		parser( *this );
    m_scanner->yyrestart( &AliasStream );
    return( parser.parse( ) == 0 );
  }

  AliasDriver::token_type AliasDriver::
  Lex( AliasParser::semantic_type* YYLval,
       AliasParser::location_type* YYLloc )
  {
    m_scanner->SetYYLval( YYLval );
    m_scanner->SetYYLloc( YYLloc );

    token_type  retval = token_type( m_scanner->yylex( ) );

    return retval;
  }

} // namespace - datacondAPI
