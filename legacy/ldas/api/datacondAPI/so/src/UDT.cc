//=======================================================================
// Universal Data Type (super class model)
//=======================================================================

#include "config.h"

#include <cstdarg>
#include <sstream>   

#include "ilwd/ldascontainer.hh"

#include "UDT.hh"

#include "CallChain.hh"

#define	 MEM_TRACKING 0

using std::va_list;

datacondAPI::udt::BadTargetConversion::
BadTargetConversion(datacondAPI::udt::target_type Target,
		    const std::string& ClassName)
  : runtime_error(form_message(Target,ClassName).c_str())
{
}

std::string datacondAPI::udt::BadTargetConversion::
form_message(datacondAPI::udt::target_type Target, const std::string& ClassName)
{
  std::ostringstream oss;

  oss << "Unable to convert data for target: "
      << ClassName
      << " "
      << GetTargetType(Target);

  return oss.str();
}

// Constructor
datacondAPI::udt::
udt(const std::string& name)
    : m_name(name)
{
#if MEM_TRACKING
  cerr << "Creating UDT: " << (void*)this
       << " history " << (void*)&m_history
       << endl << flush;
#endif	/* MEM_TRACKING */
}

// Constructor
datacondAPI::udt::
udt(const datacondAPI::udt& Source)
  : m_history( Source.m_history ),
    m_name( Source.m_name )
{
#if MEM_TRACKING
  cerr << "Creating UDT: " << (void*)this
       << " history " << (void*)&m_history
       << endl << flush;
#endif	/* MEM_TRACKING */
}

// Destructor
datacondAPI::udt::
~udt()
{
#if MEM_TRACKING
  cerr << "Destroying UDT: " << (void*)this
       << " history " << (void*)&m_history
       << endl << flush;
#endif	/* MEM_TRACKING */
}

// This is a stub for UDT classes that have no metadata.
void datacondAPI::udt::
CopyMetaData( const udt& MetaData )
{
}

//-----------------------------------------------------------------------
// Add a history record to the current list of records.
//-----------------------------------------------------------------------

void datacondAPI::udt::
AppendHistory( const std::string& Description )
{
  m_history.AppendRecord( ILwd::LdasHistoryRecord( Description ) );
}

void datacondAPI::udt::
AppendHistory( const std::string& Description,
	       const datacondAPI::udt* Data, ... )
{
  CallChain				cc;
  va_list				ap;
  std::vector<const ILwd::LdasElement*>	args;

  va_start(ap, Data);
  while( Data )
  {
    args.push_back( Data->ConvertToIlwd( cc ) );
    Data = va_arg(ap, const datacondAPI::udt*);
  }
  va_end(ap);

  // Create History Record
  ILwd::LdasHistoryRecord	record( Description, args ) ;

  m_history.AppendRecord( record );
}

void datacondAPI::udt::
ConvertToIlwd(const CallChain& Chain,
	      datacondAPI::udt::target_type Target,
	      ILwd::LdasContainer* Container) const
{
  if (m_history.GetHistoryAsILwd().size() > 0)
  {
    Container->
      push_back( new ILwd::LdasContainer(m_history.GetHistoryAsILwd()),
		 ILwd::LdasContainer::NO_ALLOCATE,
		 ILwd::LdasContainer::OWN );
  }
}

std::string datacondAPI::udt::
GetTargetType(datacondAPI::udt::target_type Target)
{
  switch(Target)
  {
  case TARGET_FRAME:
    return "frame";
  case TARGET_METADATA:
    return "metadata";
  case TARGET_WRAPPER:
    return "wrapper";
  case TARGET_GENERIC:
    return "generic";
  default:
    return "unknown";
  }
}

datacondAPI::udt::target_type datacondAPI::udt::
GetTargetType(const std::string& Target)
{
  if ( Target == "frame" )
  {
    return TARGET_FRAME;
  }
  else if (Target == "metadata")
  {
    return TARGET_METADATA;
  }
  else if (Target == "wrapper")
  {
    return TARGET_WRAPPER;
  }
  
  // :TODO: probably should have an error here

  return TARGET_GENERIC;
}

bool datacondAPI::udt::
ReturnsMultipleILwds( datacondAPI::udt::target_type Target ) const
{
  return false;
}

#ifndef DATACOND_API__AUTO_INSTANTIATE
#ifndef UDT_PRIVATE
#define UDT_PRIVATE

UDT_CLASS_INSTANTIATION(udt,__non_data__)

#endif /* UDT_PRIVATE */
#endif /* DATACOND_API_AUTO_INSTANTIATE */

