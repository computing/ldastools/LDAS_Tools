/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "datacondAPI/config.h"

#include <cmath>

#include "BasicFunctions.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

#include "Abs.hh"
#include "Arg.hh"
#include "Imag.hh"
#include "Real.hh"

using namespace std;

void sqrt_apply(datacondAPI::udt*& out, const datacondAPI::udt& in)
{
  if (datacondAPI::udt::IsA<datacondAPI::Sequence<float> >(in))
    {
      if (out == 0)
	{
	  out = in.Clone();
	}
      else if (!datacondAPI::udt::IsA<datacondAPI::Sequence<float> >(*out))
	{
	  throw std::invalid_argument("sqrt_apply");
	}
      
      datacondAPI::udt::Cast<datacondAPI::Sequence<float> >(*out) = datacondAPI::udt::Cast<datacondAPI::Sequence<float> >(in).apply(sqrt);            
    }
  else if (datacondAPI::udt::IsA<datacondAPI::Sequence<std::complex<float> > >(in))
    {
      if (out == 0)
	{
	  out = in.Clone();
	}
      else if (!datacondAPI::udt::IsA<datacondAPI::Sequence<std::complex<float> > >(*out))
	{
	  throw std::invalid_argument("sqrt_apply");
	}
      
      datacondAPI::udt::Cast<datacondAPI::Sequence<std::complex<float> > >(*out) = datacondAPI::udt::Cast<datacondAPI::Sequence<std::complex<float> > >(in).apply(std::sqrt);            
    }
  else if (datacondAPI::udt::IsA<datacondAPI::Sequence<double> >(in))
    {
      if (out == 0)
	{
	  out = in.Clone();
	}
      else if (!datacondAPI::udt::IsA<datacondAPI::Sequence<double> >(*out))
	{
	  throw std::invalid_argument("sqrt_apply");
	}
      
      datacondAPI::udt::Cast<datacondAPI::Sequence<double> >(*out) =datacondAPI::udt::Cast<datacondAPI::Sequence<double> >(in).apply(std::sqrt);            
    }
  else if (datacondAPI::udt::IsA<datacondAPI::Sequence<std::complex<double> > >(in))
    {
      if (out == 0)
	{
	  out = in.Clone();
	}
      else if (!datacondAPI::udt::IsA<datacondAPI::Sequence<std::complex<double> > >(*out))
	{
	  throw std::invalid_argument("sqrt_apply");
	}
      
      datacondAPI::udt::Cast<datacondAPI::Sequence<std::complex<double> > >(*out) = datacondAPI::udt::Cast<datacondAPI::Sequence<std::complex<double> > >(in).apply(std::sqrt);
    }
  else
    {
      throw std::invalid_argument("sqrt_apply");
    }
}

//=======================================================================
// Clear
//=======================================================================

ClearFunction::
ClearFunction()
  : Function( ClearFunction::GetName() )
{
}


void ClearFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  switch(Params.size())
  {
  case 1:
    Chain->DestroySymbol(Params[0]);
    break;
  default:
    throw CallChain::BadArgumentCount( GetName(), 1, Params.size() );
    break;
  }
}

const std::string& ClearFunction::
GetName(void) const
{
  static const std::string name("clear");
  return name;
}

static ClearFunction  _ClearFunction;

//=======================================================================
// Value Function
//=======================================================================

ValueFunction::
ValueFunction()
  : Function( ValueFunction::GetName() )
{
}


void ValueFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  switch(Params.size())
  {
  case 1:
    Chain->ResetOrAddSymbol( Ret, Chain->GetSymbol(Params[0])->Clone() );
    break;
  default:
    throw CallChain::BadArgumentCount( GetName(), 1, Params.size() );
    break;
  }
}

const std::string& ValueFunction::
GetName(void) const
{
  static const std::string name("value");
  return name;
}

static ValueFunction  _ValueFunction;

//=======================================================================
// Real Function
//=======================================================================

//-----------------------------------------------------------------------------
// We construct a static, global instance of a RealFunction so that the
// constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

static RealFunction _RealFunction;

//-----------------------------------------------------------------------------
// RealFunction::RealFunction
//-----------------------------------------------------------------------------

RealFunction::
RealFunction()
  : Function( RealFunction::GetName() )
{
}

//-----------------------------------------------------------------------------
// LinfiltFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "linfilt". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//-----------------------------------------------------------------------------

void RealFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  using datacondAPI::udt;
  using datacondAPI::Real;

  if (Params.size() != 1)
  {
    throw(CallChain::BadArgumentCount(GetName(), 1, Params.size()));
  }

  const udt* const in = Chain->GetSymbol(Params[0]);
  
  udt* const result = Real(*in);
  
  Chain->ResetOrAddSymbol(Ret, result);
}

//-----------------------------------------------------------------------------
// RealFunction::GetName
//-----------------------------------------------------------------------------


const std::string& RealFunction::
GetName(void) const
{
  
  //-------------------------------------------------------------------------
  // The GetName function returns the string "linfilt", the name of the
  // action as presented to the user. The name is stored as a static variable
  // and returned as a const reference for performance reasons.
  //-------------------------------------------------------------------------
  
  static const std::string name("real");
  return name;
}

//=======================================================================
// Imag Function
//=======================================================================

static ImagFunction _ImagFunction;

ImagFunction::
ImagFunction()
  : Function( ImagFunction::GetName() )
{
}


void ImagFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  using datacondAPI::udt;
  using datacondAPI::Imag;

  if (Params.size() != 1)
  {
    throw(CallChain::BadArgumentCount(GetName(), 1, Params.size()));
  }

  const udt* const in = Chain->GetSymbol(Params[0]);
  
  udt* const result = Imag(*in);
  
  Chain->ResetOrAddSymbol(Ret, result);
}

const std::string& ImagFunction::
GetName(void) const
{
  static const std::string name("imag");
  return name;
}

//=======================================================================
// Abs Function
//=======================================================================

static AbsFunction  _AbsFunction;

AbsFunction::
AbsFunction()
  : Function( AbsFunction::GetName() )
{
}

void AbsFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  using datacondAPI::udt;
  using datacondAPI::Abs;

  if (Params.size() != 1)
  {
    throw(CallChain::BadArgumentCount(GetName(), 1, Params.size()));
  }

  const udt* const in = Chain->GetSymbol(Params[0]);
  
  udt* const result = Abs(*in);
  
  Chain->ResetOrAddSymbol(Ret, result);
}

const std::string& AbsFunction::
GetName(void) const
{
  static const std::string name("abs");
  return name;
}

//=======================================================================
// Arg Function
//=======================================================================

static ArgFunction  _ArgFunction;

ArgFunction::
ArgFunction()
  : Function( ArgFunction::GetName() )
{
}


void ArgFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  using datacondAPI::udt;
  using datacondAPI::Arg;

  if (Params.size() != 1)
  {
    throw(CallChain::BadArgumentCount(GetName(), 1, Params.size()));
  }

  const udt* const in = Chain->GetSymbol(Params[0]);
  
  udt* const result = Arg(*in);
  
  Chain->ResetOrAddSymbol(Ret, result);
}

const std::string& ArgFunction::
GetName(void) const
{
  static const std::string name("arg");
  return name;
}

//=======================================================================
// Conj Function
//=======================================================================

//
// Other static declarations have used a leading underscore.
// These are deprecated by the style guide and should be replaced
// by something else.
// 
static ConjFunction staticConjFunction;

ConjFunction::
ConjFunction()
  : Function( ConjFunction::GetName() )
{
}

void ConjFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  using namespace datacondAPI;
  using std::complex;

  CallChain::Symbol* result = 0;
    
  switch(Params.size())
  {
  case 1:
    {
      const CallChain::Symbol* const obj = Chain->GetSymbol(Params[0]);
            
      //
      // This bit is slightly tricky. In order to avoid casting
      // twice we declare a pointer of the type we are checking.
      // If the result of the dynamic_cast is non-zero (that is,
      // we have located the correct dynamic type) then the 
      // body of the 'if' statement is entered and we already have
      // a pointer to the object as it's real type.
      //
      // If the result of the dynamic_cast is zero we go to the
      // next case
      //
      if (const Sequence<complex<float> >* const p = 
          dynamic_cast<const Sequence<complex<float> >* >(obj))
      {
	typedef complex<float> i_type;
        Sequence<i_type >* const r = p->Clone();
	i_type (*func)( const i_type& ) = std::conj;
        *r = r->apply(func);

        result = r;
      }
      else if (const Sequence<complex<double> >* const p = 
               dynamic_cast<const Sequence<complex<double> >* >(obj))
      {
	typedef complex<double> i_type;
        Sequence<i_type >* const r = p->Clone();
	i_type (*func)( const i_type& ) = std::conj;
        *r = r->apply(func);

        result = r;
      }
      else if (const Scalar<complex<float> >* const p = 
               dynamic_cast<const Scalar<complex<float> >* >(obj))
      {
        Scalar<complex<float> >* const r = p->Clone();
        r->SetValue(std::conj(r->GetValue()));

        result = r;
      }
      else if (const Scalar<complex<double> >* const p = 
               dynamic_cast<const Scalar<complex<double> >* >(obj))
      {
        Scalar<complex<double> >* const r = p->Clone();
        r->SetValue(std::conj(r->GetValue()));

        result = r;
      }
      else if (   udt::IsA<Sequence<float> >(*obj)
                  || udt::IsA<Sequence<double> >(*obj)
                  || udt::IsA<Scalar<float> >(*obj)
                  || udt::IsA<Scalar<double> >(*obj)
                  || udt::IsA<Scalar<int> >(*obj) )
      {
        // For real objects, we don't need to do anything
        // but return a copy
        result = obj->Clone();
      }
      else
      {
        throw CallChain::BadArgument(GetName(), 1,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                     "object derived from Scalar or Sequence");
      }
    }
    break;
  default:
    throw CallChain::BadArgumentCount("conj", 1, Params.size());
    break;
  }
    
  if (result == 0)
  {
    throw CallChain::NullResult( GetName() );
  }
    
  Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& ConjFunction::
GetName(void) const
{
  static const std::string name("conj");
    
  return name;
}

//=======================================================================
// Reverse Function
//=======================================================================

//
// Other static declarations have used a leading underscore.
// These are deprecated by the style guide and should be replaced
// by something else.
// 
static ReverseFunction staticReverseFunction;

ReverseFunction::
ReverseFunction()
  : Function( ReverseFunction::GetName() )
{
}

void ReverseFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  using namespace datacondAPI;
  using std::complex;

  if (Params.size() != 1)
  {
    throw CallChain::BadArgumentCount(GetName(), 1, Params.size());
  }

  CallChain::Symbol* result = 0;
  const CallChain::Symbol* const obj = Chain->GetSymbol(Params[0]);

  if (const Sequence<float>* const p = 
      dynamic_cast<const Sequence<float>*>(obj))
  {
    Sequence<float>* const r = p->Clone();
    reverse(&(*r)[0], &(*r)[r->size()]);
    result = r;
  }
  else if (const Sequence<double>* const p = 
           dynamic_cast<const Sequence<double>* >(obj))
  {
    Sequence<double>* const r = p->Clone();
    reverse(&(*r)[0], &(*r)[r->size()]);
    result = r;
  }
  else if (const Sequence<complex<float> >* const p = 
           dynamic_cast<const Sequence<complex<float> >* >(obj))
  {
    Sequence<complex<float> >* const r = p->Clone();
    reverse(&(*r)[0], &(*r)[r->size()]);
    result = r;
  }
  else if (const Sequence<complex<double> >* const p = 
           dynamic_cast<const Sequence<complex<double> >* >(obj))
  {
    Sequence<complex<double> >* const r = p->Clone();
    reverse(&(*r)[0], &(*r)[r->size()]);
    result = r;
  }
  else
  {
    throw CallChain::BadArgument(GetName(), 1,
                                 TypeInfoTable.GetName(typeid(*obj)),
                                 "object derived from Sequence");
  }
    
  Chain->ResetOrAddSymbol(Ret, result);
}

const std::string& ReverseFunction::
GetName(void) const
{
  static const std::string name("reverse");
    
  return name;
}



#define MATH_FUNCTION(NAME)\
\
class NAME##Function : public CallChain::Function\
{\
public:\
    NAME##Function();\
    virtual const std::string& GetName() const;\
    virtual void Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret) const;\
};\
\
NAME##Function::NAME##Function() : CallChain::Function( NAME##Function::GetName())\
{\
}\
\
const std::string& NAME##Function::GetName() const\
{\
    static const std::string name( #NAME );\
    return name;\
}\
\
static NAME##Function NAME##Function_;\
\
inline static float convenience_##NAME (const float& in) { return std:: NAME (in); }\
\
void NAME##Function::Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret) const\
{\
    using namespace datacondAPI;\
    if (Params.size() != 1)\
    {\
        throw CallChain::BadArgumentCount(#NAME, 1, Params.size());\
    }\
    const udt* const in = Chain->GetSymbol(Params[0]);\
    udt* out = 0;\
\
    if (const Scalar<float>* resolved = dynamic_cast<const Scalar<float>*>(in))\
    {\
        Scalar<float>* const r = resolved->Clone();\
        r->SetValue(NAME(r->GetValue()));\
\
        out = r;\
    }\
    else if (const Scalar<double>* resolved = dynamic_cast<const Scalar<double>*>(in))\
    {\
        Scalar<double>* const r = resolved->Clone();\
        r->SetValue(NAME(r->GetValue()));\
\
        out = r;\
    }\
    else if (const Scalar<std::complex<float> >* resolved = dynamic_cast<const Scalar<std::complex<float> >*>(in))\
    {\
        Scalar<std::complex<float> >* const r = resolved->Clone();\
        r->SetValue(NAME(r->GetValue()));\
\
        out = r;\
    }\
    else if (const Scalar<std::complex<double> >* resolved = dynamic_cast<const Scalar<std::complex<double> >*>(in))\
    {\
        Scalar<std::complex<double> >* const r = resolved->Clone();\
        r->SetValue(NAME(r->GetValue()));\
\
        out = r;\
    }\
    else if (const Sequence<float>* resolved = dynamic_cast<const Sequence<float>*>(in))\
    {\
        Sequence<float>* const r = resolved->Clone();\
        *r = r->apply(convenience_##NAME);\
\
        out = r;\
    }\
    else if (const Sequence<double>* resolved = dynamic_cast<const Sequence<double>*>(in))\
    {\
        Sequence<double>* const r = resolved->Clone();\
        *r = r->apply(NAME);\
\
        out = r;\
    }\
    else if (const Sequence<std::complex<float> >* resolved = dynamic_cast<const Sequence<std::complex<float> >*>(in))\
    {\
        Sequence<std::complex<float> >* const r = resolved->Clone();\
        *r = r->apply(NAME);\
\
        out = r;\
    }\
    else if (const Sequence<std::complex<double> >* resolved = dynamic_cast<const Sequence<std::complex<double> >*>(in))\
    {\
        Sequence<std::complex<double> >* const r = resolved->Clone();\
        *r = r->apply(NAME);\
\
        out = r;\
    }\
    else\
    {\
        throw CallChain::BadArgument(GetName(), 1,\
                                    TypeInfoTable.GetName(typeid(*in)),\
                                    "object derived from Scalar or Sequence");\
    }\
    Chain->ResetOrAddSymbol( Ret, out );\
}\

MATH_FUNCTION(cos)
MATH_FUNCTION(cosh)
MATH_FUNCTION(exp)
MATH_FUNCTION(log)
MATH_FUNCTION(sin)
MATH_FUNCTION(sinh)
MATH_FUNCTION(sqrt)

#undef MATH_FUNCTION
