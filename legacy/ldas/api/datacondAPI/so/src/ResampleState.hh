/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef RESAMPLESTATE_HH
#define RESAMPLESTATE_HH

// $Id: ResampleState.hh,v 1.23 2006/11/07 22:25:46 emaros Exp $

#include <memory>

#include "general/Memory.hh"
#include <general/unexpected_exception.hh>

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "StateUDT.hh"

namespace Filters {
  class ResampleBase;
}

namespace datacondAPI {

  class ResampleState : public State {
  public:

    //: Specify everything constructor
    //!param: int p - upsample factor
    //!param: int q - downsample factor
    //!param: int n = 10 - anti-aliasing filter order parameter
    //!param: double beta = 5 - Kaiser window parameter
    //!exc: std::invalid_argument - p < 1, q < 1, n < 1, or beta < 0
    ResampleState(int p,
                  int q,
                  int n = 10,
                  double beta = 5.0);

    //: Specify everything UDT constructor
    //!param: udt& p - upsample factor (should be Scalar<int>)
    //!param: udt& q - downsample factor (should be Scalar<int>)
    //!param: udt& n = Scalar<int>(10) - anti-aliasing filter order parameter
    //!param: udt& beta = Scalar<double>(5.0) - Kaiser window parameter
    //!exc: std::invalid_argument - p, q, n not Scalar<int>; 
    //+ beta not Scalar<double>
    ResampleState(const udt& p, 
                  const udt& q, 
                  const udt& n = Scalar<int>(10), 
                  const udt& beta = Scalar<double>(5.0));

    //: Copy constructor (from ResampleState)
    //!param: const ResampleState& - state object to copy from
    ResampleState(const ResampleState&);

    //: Copy constructor (from udt)
    //!param: const udt& - ResampleState
    //!exc: std::invalid_argument - arg not a ResampleState
    ResampleState(const udt&);

    //: constructor that takes a filter
    ResampleState(int p, int q, const Sequence<double>& b);

    //: destructor
    virtual ~ResampleState();
    
    //: assignment
    //!param: const ResampleState& - object to copy from
    ResampleState& operator=(const ResampleState&);
    
    // accessors
    
    //: get upsample factor
    //!return: int - upsample factor
    int getP()const;

    //: get downsample factor
    //!return: int - downsample factor
    int getQ() const;

    //: get anti-aliasing filter order parameter (not filter order)
    //!return: int - filter order parameter
    int getN() const;

    //: get anti-aliasing filter Kaiser window parameter
    //!return: double - Kaiser window parameter
    double getBeta() const;

    //: get anti-aliasing filter order
    //!return: int - actual anti-aliasing filter order
    int getOrder() const;

    //: get resample delay
    //!return: double - delay, in samples, between input impulse and 
    //+ maximum of output impulse
    double getDelay() const;

    //: get FIR filter coeffcients
    //!param: b - the FIR filter coefficents
    void getB(Sequence<double>& b) const;

    //
    //: Apply the resampling action
    //
    // This function exposes the resampling action of the
    // Filters::Resample class. The first time it is called it
    // initialises an internal state which is specific to the input
    // data type T. Attempting to use the same state later on a different
    // type is an error and will result in an exception being thrown.
    //
    //!param: out - the resampled data
    //!param: in - the input data
    //!exc: std::invalid_argument - if in.size() == 0, or if the current
    //+state was set up with a different data type that T
    template<class T>
    void apply(std::valarray<T>& out, const std::valarray<T>& in);

    // UDT methods

    //: Create a copy of *this
    //!return: ResampleState* - copy of *this, allocated on heap
    virtual
    ResampleState* Clone() const;
    
    virtual
    ILwd::LdasElement* ConvertToIlwd(const CallChain& Chain,
                           udt::target_type Target = udt::TARGET_GENERIC) const;

  private:

    //: default constructor prohibited - no canonical defaults for
    //+ResampleState
    ResampleState();

    //: Remembers the first call to apply()
    bool m_first;

    //: Only need to remember this so we can recreate m_resample
    std::valarray<double> m_b;

    std::unique_ptr<Filters::ResampleBase> m_resample;

  }; // class ResampleState

} // namespace datacondAPI

#endif // RESAMPLESTATE_HH
