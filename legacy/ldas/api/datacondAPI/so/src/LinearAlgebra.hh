#ifndef DATACOND_API__LINEAR_ALGEBRA_HH
#define DATACOND_API__LINEAR_ALGEBRA_HH

#include "CLAPACK.hh"

namespace datacondAPI
{
  template< typename T > class Matrix;
  template< typename T > class Sequence;

  namespace LinearAlgebra
  {
    using datacondAPI::Matrix;

    //===================================================================
    // MM routines
    //===================================================================
    //: MM performs the matrix-matrix operation
    //+     C := alpha*A*B + beta*C
    //+ where A, B, C are matrices, alpha and beta are scalars.
    //+ The transposes of A or B may be used.
    // 
    //  This method wraps BLAS SGEMM
    //
    //!param: Matrix<float>& C - output matrix
    //!param: const Matrix<float>& A - input matrix
    //!param: const Matrix<float>& B - input matrix
    //!param: const float& alpha = 1.0 - input scalar
    //!param: const float& alpha = 0.0 - input scalar
    //!param: const bool& transposeA = false - input flag
    //!param: const bool& transposeB = false - input flag
    void MM(Matrix<float>& C,
	    const Matrix<float>& A,
	    const Matrix<float>& B,
	    const float& alpha = 1.0,
	    const float& beta = 0.0,
	    const bool& transposeA = false,
	    const bool& transposeB = false);

    //: MM performs the matrix-matrix operation
    //+     C := alpha*A*B + beta*C
    //+ where A, B, C are matrices, alpha and beta are scalars.
    //+ The transposes of A or B may be used.
    // 
    //  This method wraps BLAS DGEMM
    //
    //!param: Matrix<double>& C - output matrix
    //!param: const Matrix<double>& A - input matrix
    //!param: const Matrix<double>& B - input matrix
    //!param: const double& alpha = 1.0 - input scalar
    //!param: const double& beta = 0.0 - input scalar
    //!param: const bool& transposeA = false - input flag
    //!param: const bool& transposeB = false - input flag
    void MM(Matrix<double>& C,
	    const Matrix<double>& A,
	    const Matrix<double>& B,
	    const double& alpha = 1.0,
	    const double& beta = 0.0,
	    const bool& transposeA = false,
	    const bool& transposeB = false
	    );

    void MM(Matrix<std::complex<float> >& C,
	    const Matrix<std::complex<float> >& A,
	    const Matrix<std::complex<float> >& B,
	    const std::complex<float>& alpha = 1.0,
	    const std::complex<float>& beta = 0.0,
	    const bool& transposeA = false,
	    const bool& transposeB = false
	    );

    void MM(Matrix<std::complex<double> >& C,
	    const Matrix<std::complex<double> >& A,
	    const Matrix<std::complex<double> >& B,
	    const std::complex<double>& alpha = 1.0,
	    const std::complex<double>& beta = 0.0,
	    const bool& transposeA = false,
	    const bool& transposeB = false
	    );

    //===================================================================
    // MV routines
    //===================================================================
    //: MV performs the matrix-vector operation
    //+     y := A*x
    //+ where x and y are vectors and A is an m by n matrix
    //
    //  This method wraps BLAS SGEMV
    //
    //!param: Sequence<float>& y - output vector
    //!param: const Matrix<float>& A - input m by n matrix
    //!param: const Sequence<float>& x - input n vector
    void MV(Sequence<float>& y,
	    const Matrix<float>& A,
	    const Sequence<float>& x);

    //: MV performs the matrix-vector operation
    //+     y := A*x
    //+ where x and y are vectors and A is an m by n matrix
    //
    //  This method wraps BLAS DGEMV
    //
    //!param: Sequence<double>& y - output vector
    //!param: const Matrix<double>& A - input m by n matrix
    //!param: const Sequence<double>& x - input n vector
    void MV(Sequence<double>& y,
	    const Matrix<double>& A,
	    const Sequence<double>& x);

    void MV(Sequence<std::complex<float> >& y,
	    const Matrix<std::complex<float> >& A,
	    const Sequence<std::complex<float> >& x);

    void MV(Sequence<std::complex<double> >& y,
	    const Matrix<std::complex<double> >& A,
	    const Sequence<std::complex<double> >& x);

    //===================================================================
    // SV routines
    //===================================================================
    //-------------------------------------------------------------------
    //: SV computes the solution to a real system of linear equations
    //+    A * X = B,
    //+ where A is an N-by-N matrix and X and B are N-by-NRHS matrices.
    // 
    //  The LU decomposition with partial pivoting and row interchanges is
    //  used to factor A as
    //     A = P * L * U,
    //  where P is a permutation matrix, L is unit lower triangular, and U is
    //  upper triangular.  The factored form of A is then used to solve the
    //  system of equations A * X = B.
    //
    //!param: const Matrix<float>& A - an N by N matrix (N >= 1).
    //!param: Matrix<float>& X - a matrix that will store the N by M
    //+       (M >= 1) solution for X of AX = B
    //!param: const Matrix<float>& B - an N by M matrix
    void SV(const Matrix<float>& A,
            Matrix<float>& X,
            const Matrix<float>& B);

    //: SV computes the solution to a real system of linear equations
    //+    A * X = B,
    //+ where A is an N-by-N matrix and X and B are N-by-NRHS matrices.
    // 
    //  The LU decomposition with partial pivoting and row interchanges is
    //  used to factor A as
    //     A = P * L * U,
    //  where P is a permutation matrix, L is unit lower triangular, and U is
    //  upper triangular.  The factored form of A is then used to solve the
    //  system of equations A * X = B.
    //
    //!param: const Matrix<double>& A - an N by N matrix (N >= 1).
    //!param: Matrix<double>& X - a matrix that will store the N by M
    //+       (M >= 1) solution for X of AX = B
    //!param: const Matrix<double>& B - an N by M matrix
    void SV(const Matrix<double>& A,
            Matrix<double>& X,
            const Matrix<double>& B);

    void SV(const Matrix<std::complex<float> >& A,
            Matrix<std::complex<float> >& X,
            const Matrix<std::complex<float> >& B);

    void SV(const Matrix<std::complex<double> >& A,
            Matrix<std::complex<double> >& X,
            const Matrix<std::complex<double> >& B);
  }
}

namespace datacondAPI
{
  namespace LinearAlgebra
  {
    //===================================================================
    // MM routines
    //===================================================================
    inline void MM( Matrix<float>& C,
		    const Matrix<float>& A,
		    const Matrix<float>& B,
		    const float& alpha,
		    const float& beta,
		    const bool& transposeA,
		    const bool& transposeB )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MM( C, A, B, alpha, beta, transposeA, transposeB );
#else
      throw std::runtime_error( "LinearAlgebra::MM is unimplemented" );
#endif
    }

    inline void MM( Matrix<double>& C,
		    const Matrix<double>& A,
		    const Matrix<double>& B,
		    const double& alpha,
		    const double& beta,
		    const bool& transposeA,
		    const bool& transposeB )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MM( C, A, B, alpha, beta, transposeA, transposeB );
#else
      throw std::runtime_error( "LinearAlgebra::MM is unimplemented" );
#endif
    }

    inline void MM( Matrix<std::complex<float> >& C,
		    const Matrix<std::complex<float> >& A,
		    const Matrix<std::complex<float> >& B,
		    const std::complex<float>& alpha,
		    const std::complex<float>& beta,
		    const bool& transposeA,
		    const bool& transposeB )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MM( C, A, B, alpha, beta, transposeA, transposeB );
#else
      throw std::runtime_error( "LinearAlgebra::MM is unimplemented" );
#endif
    }

    inline void MM( Matrix<std::complex<double> >& C,
		    const Matrix<std::complex<double> >& A,
		    const Matrix<std::complex<double> >& B,
		    const std::complex<double>& alpha,
		    const std::complex<double>& beta,
		    const bool& transposeA,
		    const bool& transposeB )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MM( C, A, B, alpha, beta, transposeA, transposeB );
#else
      throw std::runtime_error( "LinearAlgebra::MM is unimplemented" );
#endif
    }

    //===================================================================
    // MV routines
    //===================================================================
    inline void MV( Sequence<float>& y,
		    const Matrix<float>& A,
		    const Sequence<float>& x )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MV( y, A, x );
#else
      throw std::runtime_error( "LinearAlgebra::MV is unimplemented" );
#endif
    }

    inline void MV( Sequence<double>& y,
		    const Matrix<double>& A,
		    const Sequence<double>& x )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MV( y, A, x );
#else
      throw std::runtime_error( "LinearAlgebra::MV is unimplemented" );
#endif
    }

    inline void MV( Sequence<std::complex<float> >& y,
		    const Matrix<std::complex<float> >& A,
		    const Sequence<std::complex<float> >& x )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MV( y, A, x );
#else
      throw std::runtime_error( "LinearAlgebra::MV is unimplemented" );
#endif
    }

    inline void MV( Sequence<std::complex<double> >& y,
		    const Matrix<std::complex<double> >& A,
		    const Sequence<std::complex<double> >& x )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.MV( y, A, x );
#else
      throw std::runtime_error( "LinearAlgebra::MV is unimplemented" );
#endif
    }

    //===================================================================
    // SV routines
    //===================================================================
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    inline void SV( const Matrix<float>& A,
		    Matrix<float>& X,
		    const Matrix<float>& B )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.SV( A, X, B );
#else
      throw std::runtime_error( "LinearAlgebra::SV is unimplemented" );
#endif
    }

    inline void SV( const Matrix<double>& A,
		    Matrix<double>& X,
		    const Matrix<double>& B )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.SV( A, X, B );
#else
      throw std::runtime_error( "LinearAlgebra::SV is unimplemented" );
#endif
    }

    inline void SV( const Matrix<std::complex<float> >& A,
		    Matrix<std::complex<float> >& X,
		    const Matrix<std::complex<float> >& B )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.SV( A, X, B );
#else
      throw std::runtime_error( "LinearAlgebra::SV is unimplemented" );
#endif
    }

    inline void SV( const Matrix<std::complex<double> >& A,
		    Matrix<std::complex<double> >& X,
		    const Matrix<std::complex<double> >& B )
    {
#if HAVE_LIBCLAPACK
      TheCLAPACKSoHandle.SV( A, X, B );
#else
      throw std::runtime_error( "LinearAlgebra::SV is unimplemented" );
#endif
    }
  }
}

#endif /* DATACOND_API__LINEAR_ALGEBRA_HH */
