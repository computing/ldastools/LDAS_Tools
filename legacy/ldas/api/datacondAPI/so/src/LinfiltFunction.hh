/* -*- mode: c++ -*- */
#ifndef LINFILT_FUNCTION_HH
#define LINFILT_FUNCTION_HH

#include "CallChain.hh"

//-----------------------------------------------------------------------------
// Forward declarations
//-----------------------------------------------------------------------------

namespace datacondAPI {
  template <class T> class Sequence;
}

//-----------------------------------------------------------------------------
//: Function support for LinFilt action
//
// This class implements the "linfilt" function to be used in the action section of
// a TCL command. The syntax, as it would appear in the action sequence is:
//
// [y = ]linfilt(b, a, x[, z])
//
// where y, x are valarray<T>, b, a are valarray<double> and optional z is a
// valarray<T> OR a previously undefined symbol
//
class LinfiltFunction: CallChain::Function
{
    
public:
    
    //-------------------------------------------------------------------------
    //: Constructor
    //
    // Construct a new LinfiltFunction
    LinfiltFunction();
    
    //-------------------------------------------------------------------------
    //: Evaluate the Linfilt Function
    //
    // This performs the mixing. The list of parameters must contain doubles
    // phase and carrier and a valarray of input data.
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    //
    virtual void Eval(CallChain* Chain,
        const CallChain::Params& Params,
        const std::string& Ret) const;
    
    //-------------------------------------------------------------------------
    //: Return the Name of the function ("linfilt")
    //
    //!return: const std::string& ptName - Returns a reference to the name of
    //!return: the function
    virtual const std::string& GetName(void) const;
    
    //-------------------------------------------------------------------------
    //: Validate parameters
    virtual void
    ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
			const CallChain::Params& Params ) const;

private:
    
};

#endif // LINFILT_FUNCTION_HH
