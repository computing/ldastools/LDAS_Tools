#ifndef CALCW_FUNCTION_HH
#define CALCW_FUNCTION_HH

#include "CallChain.hh"

class calcWFunction : public CallChain::Function{
public:
  calcWFunction();
  virtual const std::string& GetName(void) const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //CALCW_FUNCTION_HH
