#ifndef _GETRESULTFUNCTION_HH_
#define _GETRESULTFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI
{

class getResultFunction : public CallChain::Function
{
public:
  getResultFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain, const CallChain::Params& Params,
		    const std::string& Ret) const;

};

}//end namespace datacondAPI

#endif //_GETRESULTFUNCTION_HH_
