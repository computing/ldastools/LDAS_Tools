/* -*- mode: c++ -*- */
%option c++
%option yyclass="datacondAPI::ActionLexer"
%option outfile="ActionLexer.cc"
%option prefix="Action"
%option noyywrap
%option batch
%option never-interactive
%option nostdinit
%option stack

%{
  //---------------------------------------------------------------------
  // List of states
  //---------------------------------------------------------------------
%}

%x state_str

%{
#define DATACOND_API__ACTION_LEXER_CC

#include "datacondAPI/config.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

#include "ActionDriver.hh"
#include "ActionLexer.hh"
#include "ActionParser.hh"

#define yyFlexLexer ActionFlexLexer

  using std::exit;
  using std::free;
  using std::malloc;
  using std::memset;
  using std::realloc;

  using std::string;

  typedef datacondAPI::ActionParser::token token;
%}

whitespace [ \t]
letter [a-zA-Z]
number [0-9.Ee+-]

%%

%{
  //---------------------------------------------------------------------
  // state: state_str
  //---------------------------------------------------------------------
%}
<state_str>{whitespace}+	{
  /* Eat leading white space */;
}
<state_str>[^ \t\n,)][^\n,)]* {
  m_yylval->sval = new string( YYText( ), YYLeng( ) );
  yy_pop_state( );
  return token::TKN_STRING;
}

%{
  //---------------------------------------------------------------------
  // Default Handler
  //---------------------------------------------------------------------
%}
[\n]			++lineno;
{whitespace}+		/* Eat white space */;
[-+]?[0-9.]{number}*	{
  m_yylval->sval = new string( YYText( ), YYLeng( ) );
  return token::TKN_CONSTANT_NUMERIC;
 }
output {
  mode( MODE_OUTPUT );
  m_yylval->sval = new string( YYText( ), YYLeng( ) );
  return token::TKN_OUTPUT;
}
[A-Za-z_][A-Za-z_0-9:\\-]* {
  m_yylval->sval = new string( YYText( ), YYLeng( ) );
  return token::TKN_IDENTIFIER;
}
[(,] {
  switch( *(YYText( )) )
  {
  case '(':
    param_count_reset( );
    break;
  case ',':
    param_count_inc( );
    break;
  }
  if ( next_option_is_string( ) )
  {
    yy_push_state( state_str );
  }
  return *(YYText( ));
}
[)=;] {
  return *(YYText( ));
}

%%
namespace datacondAPI
{
  ActionLexer::
  ActionLexer( std::istream* ArgYYIn, std::ostream* ArgYYOut )
    : yyFlexLexer(ArgYYIn, ArgYYOut),
      lineno( 1 ),
      m_mode( MODE_UNSET ),
      m_param_count( 0 )
  {
    Reset( );
  }

  void ActionLexer::
  Reset( )
  {
    m_mode = MODE_UNSET;
    m_param_count = 0;
  }

  bool ActionLexer::
  next_option_is_string( ) const
  {
    switch( m_mode )
    {
    case MODE_OUTPUT:
      if ( m_param_count == 4 )
      {
	return true;
      }
      break;
    default:
      break;
    }
    return false;
  }
} // namespace - datacondAPI
