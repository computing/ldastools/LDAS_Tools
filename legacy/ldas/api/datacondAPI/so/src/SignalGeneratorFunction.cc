#include "config.h"

#include "CallChain.hh"
#include "filters/LDASConstants.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

using std::cos;
using std::fmod;
using std::sin;

extern "C"
{
  typedef double (*cmath_func)( double );
}

template<typename F> class SignalGeneratorFunction : public CallChain::Function
{
public:
  SignalGeneratorFunction(const F&, const std::string&);
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain*, const CallChain::Params&, const std::string&) const;
private:
  F m_functor;
  std::string m_name;
};

template<typename F> const std::string& SignalGeneratorFunction<F>::GetName() const
{
  return m_name;
}

template<typename F> SignalGeneratorFunction<F>::SignalGeneratorFunction(const F& functor, const std::string& name)
  : CallChain::Function(name)
  , m_functor(functor)
  , m_name(name)
{
}

template<typename F> void SignalGeneratorFunction<F>::Eval(CallChain* chain, const CallChain::Params& parameters, const std::string& result_name) const
{
  int n = 1;
  double f = 0.;
  double phi = 0.;
  double A = 1.;
  double x0 = 0.;

  using namespace datacondAPI;

  switch (parameters.size())
  {
  case 5:
    if (Scalar<double>* p = dynamic_cast<Scalar<double>*>(chain->GetSymbol(parameters[4])))
    {
      x0 = p->GetValue();
    }
    else if (Scalar<float>* p = dynamic_cast<Scalar<float>*>(chain->GetSymbol(parameters[4])))
    {
      x0 = p->GetValue();
    }
    else if (Scalar<int>* p = dynamic_cast<Scalar<int>*>(chain->GetSymbol(parameters[4])))
    {
      x0 = p->GetValue();
    }
    else
    {
      throw CallChain::BadArgument(m_name, 2, "x0 not int, float or double", "x0 must be a real scalar value");
    }
  case 4:
    if (Scalar<double>* p = dynamic_cast<Scalar<double>*>(chain->GetSymbol(parameters[3])))
    {
      A = p->GetValue();
    }
    else if (Scalar<float>* p = dynamic_cast<Scalar<float>*>(chain->GetSymbol(parameters[3])))
    {
      A = p->GetValue();
    }
    else if (Scalar<int>* p = dynamic_cast<Scalar<int>*>(chain->GetSymbol(parameters[3])))
    {
      A = p->GetValue();
    }
    else
    {
      throw CallChain::BadArgument(m_name, 3, "A not int, float or double", "A must be a real scalar value");
    }
  case 3:
    if (Scalar<double>* p = dynamic_cast<Scalar<double>*>(chain->GetSymbol(parameters[2])))
    {
      phi = p->GetValue();
    }
    else if (Scalar<float>* p = dynamic_cast<Scalar<float>*>(chain->GetSymbol(parameters[2])))
    {
      phi = p->GetValue();
    }
    else if (Scalar<int>* p = dynamic_cast<Scalar<int>*>(chain->GetSymbol(parameters[2])))
    {
      phi = p->GetValue();
    }
    else
    {
      throw CallChain::BadArgument(m_name, 2, "phi not int, float or double", "phi must be a real scalar value");
    }
  case 2:
    if (Scalar<double>* p = dynamic_cast<Scalar<double>*>(chain->GetSymbol(parameters[1])))
    {
      f = p->GetValue();
    }
    else if (Scalar<float>* p = dynamic_cast<Scalar<float>*>(chain->GetSymbol(parameters[1])))
    {
      f = p->GetValue();
    }
    else if (Scalar<int>* p = dynamic_cast<Scalar<int>*>(chain->GetSymbol(parameters[1])))
    {
      f = p->GetValue();
    }
    else
    {
      throw CallChain::BadArgument(m_name, 1, "f not int, float or double", "f must be a real scalar value");
    }

    if (Scalar<int>* p = dynamic_cast<Scalar<int>*>(chain->GetSymbol(parameters[0])))
    {
      n = p->GetValue();
    }
    else
    {
      throw CallChain::BadArgument(m_name, 0, "n not an integer", "n must be a natural number");
    }

    if (n <= 0)
    {
      throw CallChain::BadArgument(m_name, 0, "n <= 0", "n must be a natural number");
    }

    break;
  default:
    throw CallChain::BadArgumentCount(m_name, "2-5", parameters.size());
  } // switch(parameters.size())

  Sequence<double>* result = new Sequence<double>(n);

  try
  {
    for (int i = 0; i < n; ++i)
    {
      (*result)[i] = A * m_functor(LDAS_TWOPI * f * i + phi) + x0;
    }
  }
  catch (...)
  {
    delete result;
    throw;
  }

  chain->ResetOrAddSymbol(result_name, result);
}

namespace // anonymous
{
  double square(double x)
  {
    return x >= 0 ? (fmod(x, LDAS_TWOPI) < LDAS_PI ? 1. : -1.) : (fmod(-x, LDAS_TWOPI) <= LDAS_PI ? -1. : 1.);
  }

  double sawtooth(double x)
  {
    return (fmod(x, LDAS_TWOPI) + (x < 0 ? LDAS_TWOPI : 0)) / LDAS_TWOPI;
  }

  double triangle(double x)
  {
    double y = x >= 0 ? fmod(x, LDAS_TWOPI) : fmod(-x, LDAS_TWOPI);
    return y < LDAS_PI ? 2. * y / LDAS_PI - 1. : 3. - 2. * y / LDAS_PI;
  }
  SignalGeneratorFunction< cmath_func > sineFunction(&sin, "sine");
  SignalGeneratorFunction< cmath_func > cosineFunction(&cos, "cosine");
  SignalGeneratorFunction<double (*)(double)> squareFunction(&square, "square");
  SignalGeneratorFunction<double (*)(double)> sawtoothFunction(&sawtooth, "sawtooth");
  SignalGeneratorFunction<double (*)(double)> triangleFunction(&triangle, "triangle");
}
