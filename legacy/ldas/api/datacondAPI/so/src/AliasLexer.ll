/* -*- mode: c++ -*- */
%option c++
%option yyclass="datacondAPI::AliasLexer"
%option outfile="AliasLexer.cc"
%option prefix="Alias"
%option noyywrap
%option batch
%option never-interactive
%option nostdinit

%{
#define DATACOND_API__ALIAS_LEXER_CC

#include "config.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

#include "general/unordered_map.hh"

#include "AliasLexer.hh"
#include "AliasParser.hh"

  using std::exit;
  using std::free;
  using std::malloc;
  using std::memset;
  using std::realloc;

   using std::string;
   typedef datacondAPI::AliasParser::token token;

#define yyFlexLexer AliasFlexLexer

%}

whitespace [ \n\t]
letter [a-zA-Z]
number [0-9.Ee+-]

%%

{whitespace}+		/* Eat white space */;
"="			return '=';
";"			return ';';
[A-Za-z_][A-Za-z_0-9:\\-]*	{ m_yylval->sval = new string( YYText( ), YYLeng( ) ); return token::TKN_IDENTIFIER; }

%%
namespace datacondAPI
{
  AliasLexer::
  AliasLexer( std::istream* ArgYYIn, std::ostream* ArgYYOut )
    : yyFlexLexer(ArgYYIn, ArgYYOut)
  {
  } // AliasLexer::AliasLexer
} // namespace - datacondAPI

