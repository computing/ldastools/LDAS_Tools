//result.cc

#include "config.h"

#include <vector>
#include <general/toss.hh>
#include <iostream>
#include <memory>   

#include "general/Memory.hh"

#include "UDT.hh"
#include "Resample.hh"
#include "result.hh"

using std::unique_ptr;   
using std::cout;
using std::pow;
   
#if __SUNPRO_CC
#pragma ident "$Id: result.cc,v 1.27 2006/11/27 21:32:14 emaros Exp $"
#else
namespace
{
  const char rcsID[] = "@(#) $Id: result.cc,v 1.27 2006/11/27 21:32:14 emaros Exp $:";
}
#endif

namespace datacondAPI {
namespace psu
{
  template<class T>
  Result<T>::Result()
    : m_result_wavelet(NULL), metadata(NULL)
  { 
    
  }

  template<class T>
  Result<T>::~Result()
  {

  }

  template<class T>
  int Result<T>::
  getLevels() const 
  {
    int levels;
    levels = (fSignal.size() - 1);
    return levels;

    //determines the number of levels of
    //decomposition by finding the size of fSignal
  }

  template<class T>
  datacondAPI::Sequence<double> Result<T>::
  returnH() const
  {
    return Hcoeffs;
  }

  template<class T>
  datacondAPI::Sequence<double> Result<T>::
  returnL() const
  {
    return Lcoeffs;
  }

  template<class T>
  datacondAPI::Sequence<T> Result<T>::
  getResult(const int level) const 
  {
    if(level >= 0 && (unsigned) level <= fSignal.size() )
      {
	//return the requested data
	return fSignal[level];
      }
    else
      {
	std::string what = "getResult -- Illegal levels value";
	General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
	return 0;
      }
  }

  
  template<class T>
  void Result<T>::
  getResult(datacondAPI::udt*& Wcoeffs, const datacondAPI::udt& level) const
  { 
    if(datacondAPI::udt::IsA<Scalar<int> >(level))
      {
	int lev = datacondAPI::udt::Cast<Scalar<int> > (level);
	int size = fSignal.size();

	if (lev >= 0 && lev < size)
	  {
	    if(metadata.get() != NULL) //Result has metadata associated with it, therefore Wcoeffs is a timeseries
	      {
		if(datacondAPI::udt::IsA<TimeSeries<T> >(*Wcoeffs))
		  {
		    //get Sequence from fSignal that corresponds to level
		    datacondAPI::Sequence<T> tempSeq = fSignal[lev];
		    
		    //put the above Sequence into a TimeSeries, assign proper metadata
		    datacondAPI::TimeSeries<T> temp;
		    
		    temp.CopyMetaData(*metadata);
		    double samplerate = metadata->GetSampleRate();
		    double newsamplerate;
		    
		    if (lev < (size-1))
		      {
                        double tmp( lev + 1 );
			newsamplerate = samplerate/(pow(2.0,tmp));
		      }
		    else if(lev == (size-1)) 
		      {
                        double tmp( lev );
			newsamplerate = samplerate/(pow(2.0,tmp));
		      }
		    
		    temp.SetSampleRate(newsamplerate);
		    temp.Sequence<T>::operator=(tempSeq);
		    datacondAPI::udt::Cast<TimeSeries<T> >(*Wcoeffs) = temp;
		  }
		else
		  {
		    std::string what = "getResult-- second parameter not TimeSeries of appropriate type";
		    General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
		  }
	      }
	    else //metadata is NULL, therefore *Wcoeffs should be a Sequence
	      {
		if(datacondAPI::udt::IsA<Sequence<T> >(*Wcoeffs))
		  {
		    Sequence<T> temp(fSignal[lev]);
		    datacondAPI::udt::Cast<Sequence<T> >(*Wcoeffs) = temp;
		  }
		else
		  {
		    std::string what = "getResult-- second parameter not Sequence of appropriate type";
		    General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
		  }
	      }
	  }
	else //lev < 0 or lev >= fSignal.size())
	  {
	    std::string what = "getResult -- Illegal levels value";
	    General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
	  }
      }
    
    else //level is not a Scalar<int>
      {
	std::string what = "getResult-- unsupported data type for second parameter";
	General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
      }
  } 
  

  template<class T>
  ILwd::LdasElement*
  Result<T>::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    std::string what = "tried to call ConvertToIlwd";
    General::toss<General::unimplemented_error>("Result", __FILE__, __LINE__, what);
    return 0; 
  }

  template<class T>
  Result<T>* Result<T>::
  Clone() const
  {
    return new Result<T>(*this);
  }

  template<class T>
  Result<T>::
  Result(const Result& rslt)
    : datacondAPI::udt( rslt ),
      fSignal(rslt.fSignal), Hcoeffs(rslt.Hcoeffs),
      Lcoeffs(rslt.Lcoeffs)
  {
    unique_ptr<Wavelet>temp(rslt.m_result_wavelet->Clone());
    m_result_wavelet.reset( temp.release( ) );

    if(rslt.checkMetaData())
      {
	unique_ptr<datacondAPI::TimeSeries<T> > temp2(rslt.metadata->Clone());
	metadata.reset( temp2.release( ) );
      }

  }

  
  template<class T>
  void Result<T>::
  setMetaData(const datacondAPI::TimeSeries<T>& data)
  {
    if(metadata.get() != NULL)
      {
	//throws an exception: TimeSeries MetaData should not be altered
	std::string what = "setMetaData -- metadata already assigned";
	General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
      }

    else 
      {
	unique_ptr<datacondAPI::TimeSeries<T> > ptemp(new TimeSeries<T> );
	metadata.reset( ptemp.release( ) );
	metadata->CopyMetaData(data);

	//double temp = metadata->GetSampleRate();
	//double temp2 = metadata->GetBaseFrequency();
	//double temp3 = metadata->GetPhase();
	//cout << "sample rate = " << temp << endl;
	//cout << "base frequency = " << temp2 << endl;
	//cout << "phase = " << temp3 << endl;
	
      }
  }
  

  template<class T>
  void Result<T>::
  returnMetaData(datacondAPI::TimeSeries<T>& md) const
  {
    if (metadata.get() != 0)
      {
	md.CopyMetaData(*metadata);

	//cout << "sample rate = " << md.GetSampleRate() << endl;
	//cout << "base frequency = " << md.GetBaseFrequency() << endl;
	//cout << "phase = " << md.GetPhase() << endl;
	//cout << "Metadata returned."<< endl;
      }
 
    else //metadata == 0
      {
	std::string what = "returnMetaData -- null metadata; cannot be transferred";
	General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
      }
  }

  template<class T>
  bool Result<T>::
  checkMetaData() const
  {
    if (metadata.get() != NULL)
      return true;
    else
      return false;
  }

  template<class T>
  void Result<T>::
  add(datacondAPI::Sequence<T>& input)
  {
    fSignal.push_back(input);

    //adds the next level of filtered signal to fSignal
    //when the desired number ofcoeff levels of decomposition
    //is reached, the final lowpass filter output will be 
    //stored in element (level + 1); therefore, when full,
    //fSignal will have a size of (level + 1)
  }

  template<class T>
  void Result<T>::
  assignWavelet(unique_ptr<Wavelet>& m_resolver_wavelet)
  {
    if(m_result_wavelet.get() == NULL)
      {
	unique_ptr<Wavelet> temp(m_resolver_wavelet->Clone());
	m_result_wavelet.reset( temp.release( ) );
      }
  }

  template<class T>
  void Result<T>::
  returnWavelet(unique_ptr<Wavelet>& wavelet) const
  {
    if(wavelet.get() == 0)
      {
	unique_ptr<Wavelet> temp(m_result_wavelet->Clone());
	wavelet.reset( temp.release( ) );
      }
    else
      {
	std::string what = "returnWavelet -- wavelet not null";
	General::toss<std::invalid_argument>("Result", __FILE__, __LINE__, what);
      }
	
  }

  template<class T>
  void Result<T>::
  assignHcoeffs(datacondAPI::Sequence<double> highCoeffs)
  {
    Hcoeffs = highCoeffs;
  }

  template<class T>
  void Result<T>::
  assignLcoeffs(datacondAPI::Sequence<double> lowCoeffs)
  {
    Lcoeffs = lowCoeffs;
  }

} //namespace psu
} //namespace datacondAPI



#undef INSTANTIATE
#define INSTANTIATE(TYPE,KEY)\
template class datacondAPI::psu::Result< TYPE >; \
UDT_CLASS_INSTANTIATION(psu::Result< TYPE >,KEY)

INSTANTIATE(float,float)
INSTANTIATE(double,double)
