/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef TIME_BOUNDED_FREQ_SEQUENCE_UMD__HH
#define TIME_BOUNDED_FREQ_SEQUENCE_UMD__HH
// $Id: TimeBoundedFreqSequenceUMD.hh,v 1.4 2006/02/16 16:54:58 emaros Exp $

#include "general/gpstime.hh"

#include "GeometryMetaData.hh"
#include "WhenUMD.hh"

namespace ILwdFCS
{
  class FrameH;
  class FrProcData;
}

class CallChain;

namespace datacondAPI 
{
  //: metadata specifying the properties of a TimeBoundedFreqSequence
  class TimeBoundedFreqSequenceMetaData
    : public ArbitraryWhenMetaData
  {
    
  public:
    //: destructor
    virtual ~TimeBoundedFreqSequenceMetaData();
    
    // output
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - CallChain owning *this
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasElement* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasContainer*
    ConvertToIlwd( const CallChain& Chain, 
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC ) const;
    
    //: Convert this object to an ILWD
    //!param: const CallChain& - call chain owning *this
    //!param: datacondAPI::udt::target_type - 
    //!param: ILwd::LdasContainer& - receives result
    void ConvertToIlwd( const CallChain&,
			datacondAPI::udt::target_type,
			ILwd::LdasContainer& Container ) const;

    //: Assignment from single value
    TimeBoundedFreqSequenceMetaData& operator=( const TimeBoundedFreqSequenceMetaData& d );

  protected:
    
    // constructors
    
    //: Construct with start time and end time
    TimeBoundedFreqSequenceMetaData( const General::GPSTime& StartTime
				     = General::GPSTime(),
				     const General::GPSTime& EndTime
				     = General::GPSTime() );
    
    //: copy constructor
    //!param: data - object to construct copy of
    TimeBoundedFreqSequenceMetaData( const TimeBoundedFreqSequenceMetaData&
				     data );
    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrProcData& Storage - place to store values
    void store( ILwdFCS::FrProcData& Storage ) const;
    
    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrProcData& Storage - place to store values
    void store( ILwd::LdasContainer& Storage,
		datacondAPI::udt::target_type Target ) const;
    
  }; // class TimeSeriesMetaData

} // namespace datacondAPI

#endif // TIME_BOUNDED_FREQ_SEQUENCE_UMD__HH
