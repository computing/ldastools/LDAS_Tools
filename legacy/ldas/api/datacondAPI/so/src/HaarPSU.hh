//HaarPSU.hh

#ifndef HAAR_PSU_HH
#define HAAR_PSU_HH

#include <general/unexpected_exception.hh>
#include "WaveletPSU.hh"

namespace datacondAPI {
namespace psu
{
  //this class inherits from class Wavelet
  class Haar : public Wavelet 
  {
  public:

    //constructors and destructors

    //: Constructor 
    Haar(); 
    
    //: Constructor that allows specification of order
    //!param: int order - order of Haar wavelet
    Haar(int);

    //: Default destructor
    ~Haar();

    //: Duplicate on heap
    Haar* Clone() const;

    
    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;
  private:

    //: filter order
    int m_order;

  }; //class Haar

} //namespace psu
} //namespace datacondAPI

#endif /* HAAR_PSU_HH */
