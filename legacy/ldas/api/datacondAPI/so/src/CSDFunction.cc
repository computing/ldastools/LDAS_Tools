// $Id: CSDFunction.cc,v 1.4 2005/12/01 22:54:58 emaros Exp $
//
// "csd" action
//
// psd has 5 input arguments and one return value. The format is
//
//     csd_out = csd(csd_a, csd_b, [ fftLength ],
//                   [ window ], [ overlap ], [ detrendMethod ])
//
// csd_a, csd_b are Sequences or subtype
// fftLength, overlap and detrendMethod are integers
// window is a Window
//

#include "datacondAPI/config.h"

#include "CSDFunction.hh"
#include "WelchCSDEstimate.hh"
#include "ScalarUDT.hh"

static CSDFunction _CSDFunction;

CSDFunction::CSDFunction()
    : Function( CSDFunction::GetName() )
{
}

const std::string& CSDFunction::GetName(void) const
{
    static std::string name("csd");
    return name;
}

void CSDFunction::Eval(CallChain* Chain,
                       const CallChain::Params& Params,
                       const std::string& Ret) const
{
    using namespace datacondAPI;

    WelchCSDEstimate we;
    
    const size_t num_params = Params.size();

    switch(num_params)
    {

    case 6:
	// Detrend
	if (Params[5] != "")
	{
	    we.set_detrendMethod(*(Chain->GetSymbol(Params[5])));
	}

    case 5:
	// :TRICKY: Setting overlap is deferred to case 2
	// because it must be set *after* fft length
       
    case 4:
	// Set window
	if (Params[3] != "")
	{
	    we.set_window(*(Chain->GetSymbol(Params[3])));
	}

    case 3:
	// FFT length *must* be set first
	if (Params[2] != "")
	{
	    we.set_fftLength(*(Chain->GetSymbol(Params[2])));
	}
	
	// Set overlap length if present
	if ((num_params >= 5) && (Params[4] != ""))
	{
	    we.set_overlapLength(*(Chain->GetSymbol(Params[4])));
	}

    case 2:
    case 1:
	// Nothing to do here, just use all defaults
        break;

    default:
        throw CallChain::BadArgumentCount("psd", 1, Params.size());
    }

    const udt& csd_a = *(Chain->GetSymbol(Params[0]));
    const udt& csd_b = *(Chain->GetSymbol(Params[1]));
    CallChain::Symbol* csd_out = 0;

    we.apply(csd_out, csd_a, csd_b);
    
    Chain->ResetOrAddSymbol( Ret, csd_out );
}
