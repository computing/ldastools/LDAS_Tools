// $Id: RespFiltFunction.cc,v 1.3 2005/12/01 22:54:58 emaros Exp $
//
// "respfilt" action
//
// y = respfilt(x, R, C, alphas, gammas);
//
// x: real TimeSeries
// R: response function as a FrequencySequence<complex<float> >
// C: sensing function as a FrequencySequence<complex<float> >
// alphas: TimeSeries<complex<float> > of measured alpha values
// gammas: TimeSeries<complex<float> > of measured gamma values
//

#include "config.h"

#include "general/Memory.hh"

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "FrequencySequenceUDT.hh"
#include "RespFilt.hh"
#include "RespFiltFunction.hh"

using namespace datacondAPI;

using std::complex;
using std::string;
using std::unique_ptr;

namespace {

    RespFiltFunction static_RespFiltFunction;

}

const string& RespFiltFunction::GetName() const
{
  static const string name("respfilt");

  return name;
}

RespFiltFunction::RespFiltFunction()
    : Function( RespFiltFunction::GetName() )
{
}

void RespFiltFunction::Eval(CallChain* chain,
                            const CallChain::Params& params,
                            const string& ret) const
{
    if ((params.size() < 5) || (params.size() > 6))
    {
        throw CallChain::BadArgumentCount(GetName(), "5-6", params.size());
    }
    
    const udt* const xUdt = chain->GetSymbol(params[0]);
    const udt* const RUdt = chain->GetSymbol(params[1]);
    const udt* const CUdt = chain->GetSymbol(params[2]);
    const udt* const alphasUdt = chain->GetSymbol(params[3]);
    const udt* const gammasUdt = chain->GetSymbol(params[4]);

    // Placeholder for result
    udt* yUdt = 0;

    const FrequencySequence<complex<float> >* R
        = dynamic_cast<const FrequencySequence<complex<float> >*>(RUdt);
    
    if (R == 0)
    {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     TypeInfoTable.GetName(typeid(*RUdt)),
                                     "FrequencySequence<complex<float> >");
    }

    const FrequencySequence<complex<float> >* C
        = dynamic_cast<const FrequencySequence<complex<float> >*>(CUdt);
    
    if (C == 0)
    {
        throw CallChain::BadArgument(GetName(),
                                     2,
                                     TypeInfoTable.GetName(typeid(*CUdt)),
                                     "FrequencySequence<complex<float> >");
    }
    
    const TimeSeries<complex<float> >* alphas
        = dynamic_cast<const TimeSeries<complex<float> >*>(alphasUdt);
    
    if (alphas == 0)
    {
        throw CallChain::BadArgument(GetName(),
                                     3,
                                     TypeInfoTable.GetName(typeid(*alphasUdt)),
                                     "TimeSeries<complex<float> >");
    }

    const TimeSeries<complex<float> >* gammas
        = dynamic_cast<const TimeSeries<complex<float> >*>(gammasUdt);
    
    if (gammas == 0)
    {
        throw CallChain::BadArgument(GetName(),
                                     4,
                                     TypeInfoTable.GetName(typeid(*gammasUdt)),
                                     "TimeSeries<complex<float> >");
    }

    // Default to FORWARD
    RespFiltDirection direction = RESPFILT_FORWARD;
    if (params.size() == 6)
    {
        const udt* const dirUdt = chain->GetSymbol(params[5]);
        if (const Scalar<int>* const p
            = dynamic_cast<const Scalar<int>*>(dirUdt))
        {
            const int dir = p->GetValue();
            
            if (dir == 0)
            {
                direction = RESPFILT_FORWARD;
            }
            else if (dir == 1)
            {
                direction = RESPFILT_BACKWARD;
            }
            else
            {
                throw std::invalid_argument("Illegal Argument: "
                      "action respfilt(); argument #5 must be either 0 or 1");
            }
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         5,
                                       TypeInfoTable.GetName(typeid(*dirUdt)),
                                         "Scalar<int>");
        }
    }

    if (const TimeSeries<float>* x
        = dynamic_cast<const TimeSeries<float>*>(xUdt))
    {
        unique_ptr<TimeSeries<float> > y(x->Clone());

        respFilt(*y, *R, *C, *alphas, *gammas, direction);
        
        yUdt = y.release();
    }
    else if (const TimeSeries<double>* x
             = dynamic_cast<const TimeSeries<double>*>(xUdt))
    {
        unique_ptr<TimeSeries<double> > y(x->Clone());

        respFilt(*y, *R, *C, *alphas, *gammas, direction);
        
        yUdt = y.release();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                    0,
                                    TypeInfoTable.GetName(typeid(*xUdt)),
                                    "TimeSeries<float> or TimeSeries<double>");
    }
    
    chain->ResetOrAddSymbol(ret, yUdt);
}
