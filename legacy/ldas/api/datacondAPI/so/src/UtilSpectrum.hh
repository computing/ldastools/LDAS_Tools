#ifndef DATACOND_API__UTIL_SPECTRUM_HH
#define DATACOND_API__UTIL_SPECTRUM_HH

namespace datacondAPI
{
  //---------------------------------------------------------------------
  // Routine to return the mime type for the spectrum data
  //---------------------------------------------------------------------
  template<class T> std::string pGetMimeType( void );
}


#endif /* DATACOND_API__UTIL_SPECTRUM_HH */

