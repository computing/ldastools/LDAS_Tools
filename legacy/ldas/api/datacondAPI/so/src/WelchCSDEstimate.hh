#ifndef WELCHCSDESTIMATE_HH
#define WELCHCSDESTIMATE_HH

//!ppp: {eval `cat $ENV{SRCDIR}/cc_rules.pl`}

#include <memory>

#include "general/Memory.hh"

#include "DataCondAPIConstants.hh"
#include "WindowUDT.hh"
#include "CSDEstimate.hh"

namespace datacondAPI {

    // Forward declarations
    template<class T> class WelchCSDSpectrum;
    template<class T> class WelchSpectrum;


    //
    //: A class for spectral density estimation using Welch's method
    //
    // WelchCSDEstimate estimates spectral densities (CSDs and PSDs) by 
    // averaging modified periodograms of (possibly) overlapping segments 
    // of the input data.
    // A Welch spectral density estimate is defined by an fft length, 
    // overlap length, a choice of window, and a detrend option.  
    // (See "Spectral Analysis for Physical Applications" by Percival and 
    // Walden or "Discrete-Time Signal Processing" by Oppenheim and Schafer 
    // for more details.)
    //
    class WelchCSDEstimate : public CSDEstimate {

    public:
	    
	//
	//: Constructor with arguments
	//
	//!param: fftLength - fft length 
	//!param: overlapLength - overlap length
	//!param: window - window
	//!param: detrendMethod - detrend method
	//
	//!exc: std::invalid_argument - thrown if fftLength or 
	//+ overlapLength are invalid 
	//+ (see set_fftLength and set_overlapLength for details)
	//
	WelchCSDEstimate(const size_t fftLength
		                      = WelchCSDEstimateDefaultFFTLength,
	                 const size_t overlapLength = 0,
	                 const Filters::Window& window = Filters::HannWindow(),
	                 const DetrendMethod detrendMethod = none);

	//
	//: Copy constructor
	//
	//!param: r - Welch estimate object that is copied
	//
	WelchCSDEstimate(const WelchCSDEstimate& r);

	//
	//: Destructor
	//
	virtual 
	~WelchCSDEstimate();

	//
	//: Copy assignment
	//
	//!param: r - Welch estimate object that is copied
	//
	//!return: const WelchCSDEstimate&
	//
	const WelchCSDEstimate& 
	operator=(const WelchCSDEstimate& r);

        //
	//: Set length of FFT
	//
	//!param: fftLength - desired fft length
	//
	//!exc: std::invalid_argument - thrown if 
	//+ <ul>
	//+ <li> fftLength == 0, 
	//+ <li> fftLength >  datacondAPI::MaximumFFTLength, 
	//+ <li> fftLength >  Filters::MaximumWindowLength, or 
	//+ <li> fftLength <= m_overlapLength
	//+ </ul>
	//
	void 
	set_fftLength(const size_t fftLength);

        //
	//: Set length of FFT (udt input)
	//
	//!param: fftLength - desired fft length
	//
	//!exc: std::invalid_argument - thrown if the udt input is not 
	//+ a Scalar&lt int&gt
	//
	void 
	set_fftLength(const udt& fftLength);

	//
	//: Set length of overlapped segments
	//
	//!param: overlapLength - desired overlap length
	//
	//!exc: std::invalid_argument - thrown if 
	//+ overlapLength >= m_fftLength 
	//
	void 
	set_overlapLength(const size_t overlapLength);

	//
	//: Set length of overlapped segments (udt input)
	//
	//!param: overlapLength - desired overlap length
	//
	//!exc: std::invalid_argument - thrown if the udt input is not 
	//+ a Scalar&lt int&gt
	//
	void 
	set_overlapLength(const udt& overlapLength);

	//
	//: Set window function
	//
	//!param: window - desired window
	//
	void 
	set_window(const Filters::Window& window);

	//
	//: Set window function (udt input)
	//
	//!param: window - desired window
	//
	//!exc: std::invalid_argument - thrown if the udt input is not
	//+ a Window
	void 
	set_window(const udt& window);

	//
	//: Return string containing the name of the spectral density 
	//+ estimate
	//
	//!return: std::string - "Welch Estimate"
	//
	std::string 
	what() const;

	//
	//: Return fft length
	//
	//!return: size_t - fft length
	//
	size_t 
	fftLength() const;

	//
	//: Return overlap length
	//
	//!return: size_t - overlap length
	//
	size_t 
	overlapLength() const;

	//
	//: Return information about the window being used
	//
	//!return: WindowInfo - window information (name of window 
	//+ and parameter value (if any) needed to define the window)
	//
	WindowInfo 
	windowType() const;

	//
	//: Apply method for calculating the cross-spectral density
	//+ estimate S_xy (udt input and output)
	// 
	// This method checks for valid udt inputs, calls a private 
	// dispatch method to check (or construct) the proper udt output, 
	// and then a private templated apply method to do the actual 
	// calculation.
	// 
	//!param: out - pointer to udt output for CSD estimate: S_xy
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	//!exc: std::invalid_argument - thrown if the input udts are
	//+ not Sequences with the same precision 
	//+ (e.g., xIn = Sequence&lt float&gt and 
	//+ yIn = Sequence&lt std::complex&lt double&gt&gt would generate 
	//+ an exception since float and double don't match).
	//
	void 
	apply(udt*& out, 
	      const udt& xIn, 
	      const udt& yIn);

	//
	//: Apply method for calculating the power spectral density
	//+ estimate S_xx (udt input and output)
	// 
	// This method checks for valid udt input, calls a private 
	// dispatch method to check (or construct) the proper udt output, 
	// and then a private templated apply method to do the actual 
	// calculation.
	// 
	//!param: out - pointer to udt output for PSD estimate: S_xx
	//!param: in - input data: x
	//
	//!exc: std::invalid_argument - thrown if the input udt is not
	//+ a Sequence
	//
	void 
	apply(udt*& out, 
	      const udt& in);

	//
	//: Private apply method for calculating the cross-spectral 
	//+ density estimate S_xy
	//
	// <p> 
	// Returns the 2-sided cross-spectral density (CSD) estimate.
	// The units and  normalization of the CSD estimate are 
	// chosen so that Parseval's theorem holds in the following 
	// form:
	//
	// <p>
	// df * sum(csd[k]) = dt * sum(conj(x[k]) * y[k])
	//
	// <p>
	// where x[] and y[] are the input data arrays and
        // df, dt are obtained from csd.GetFrequencyDelta() and
	// x.GetStepSize() respectively. The step size is just the inverse
	// of the sampling rate.
	//
	// Note that for x[]=y[], the rms value of the input data 
	// is just the square root of the above quantity.
	//
	// <p>
	// The discrete frequency values at which the CSD estimate
	// is evaluated are in units of 2 times the Nyquist critical frequency.
	// They are given by:
	//
	// <p>
	// freqs[k] = (-0.5 + k/fftLength) * (2*Nyquist)	
	//
	// <p>
	// where k = 0, 1,..., fftLength - 1.
	//
	// <p>
	// Note that csd[0] and freqs[0] correspond to the most negative 
	// frequency value, not to zero frequency.  
	// The values in csd[] are arranged in order of increasing 
	// frequency, starting with the most negative frequency value and 
	// increasing in steps of df = (2*Nyq)/fftLength. A Sequence is treated
	// as though it has a sampling rate of 1 so it's frequencies range
	// from -0.5 to +0.5.
	//
	// <p>
	// WelchCSDEstimate estimates cross-spectral densities by 
	// averaging modified cross-spectra of (possibly) overlapping 
	// segments of the input data.  
	// The basic algorithm is as follows (see Percival and Walden or 
	// Oppenheim and Schafer for more details):
	//
	// <p>
	// <ul>
	// <li> 
	// Apply the chosen window to each successive detrended 
	// segment of the input data. 
	// (Note: the window automatically resizes itself to the length 
	// of the data segment, which is equal to the chosen fft length.) 
	// </li>
	// <li> 
	// Take the DFT of the window data segments.
	// </li> 
	// <li> 
	// Form the product conj(fft_x[])*fft_y[] for each segment of
	// input data, scaling the result with the inverse of the sum of 
	// squares of the window.
	// This yields CSD estimates for each segment of input data.
	// </li>
	// <li> 
	// Average the cross-spectra calculated in the previous step 
	// to get the final CSD estimate.
	// </li>
	// </ul>
	//
	// <p>
	// The number of sections averaged is given by
	//
	// <p>
	// numSections=(dataLength - overlapLength)/(fftLength - overlapLength)
	//
	// <p>
	// The apply method throws a std::invalid_argument error if:
	//
	// <p>
	// <ul>
	// <li> x.size() != y.size()
	// <li> x.size()  = 0</li>
	// <li> x.size() < fftLength && x.size() <= overlapLength</li>
	// </ul> 
	//
	// <p>
	// The last condition leads to an error exception because the 
	// fftlength should be reduced to the length of input data, but 
	// this new fft length is less than or equal to the overlap length.
	//
	//!param: out - output CSD estimate: S_xy
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	//!exc: std::invalid_argument - thrown if 
	//+ <ul>
	//+ <li> the input data xIn and yIn have incompatible lengths,
	//+ <li> there is no input data, or
	//+ <li> the fft length is greater than the length of the input 
	//+ data and the resized fft length is <= the overlap length
	//+ </ul>
	// 
	template<class TOut, class TXIn, class TYIn>
	void 
	apply(WelchCSDSpectrum<std::complex<TOut> >& out, 
       	      const Sequence<TXIn>& xIn, 
	      const Sequence<TYIn>& yIn);

	//
	//: Private apply method for calculating the power spectral 
	//+ density estimate S_xx
	//
	// <p> 
	// Returns the 2-sided power spectral density (PSD) estimate.
	// (See the description given in the private apply method for
	// cross-spectral density estimation with x=y for details.)
	// 
	template<class TOut, class TIn>
	void 
	apply(WelchSpectrum<TOut>& out, 
	      const Sequence<TIn>& in);

	template<class TOut, class TIn>
	void 
	apply(WelchSpectrum<TOut>& out, 
	      const Sequence<std::complex<TIn> >& in);

    private:
	//
	//: Dispatch method for cross-spectral density estimation
	//
	// Private method for checking (or constructing) the proper udt
	// output type for the CSD estimation apply method.
	//
	//!param: out - pointer to udt output for CSD estimate: S_xy
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	//!exc: std::invalid_argument - thrown if the output udt is
	//+ not a WelchSpectrum&lt std::complex&lt T &gt&gt object 
	//+ where T is the precision (float or double) of the real or 
	//+ complex input sequences
	//
	template<class TOut, class TXIn, class TYIn>
	void
	dispatch(udt*& out, 
	         const Sequence<TXIn>& xIn, 
		 const Sequence<TYIn>& yIn);
	
	//
	//: Dispatch method for power spectral density estimation 
	//
	// Private method for checking (or constructing) the proper udt
	// output type for the PSD estimation apply method.
	//
	//!param: out - pointer to udt output for PSD estimate: S_xx
	//!param: in - input data: x
	//
	//!exc: std::invalid_argument - thrown if the output udt is
	//+ not a WelchSpectrum&lt T&gt object 
	//+ where T is the precision (float or double) of the real or 
	//+ complex input sequence
	//
	template<class TOut, class TIn>
	void
	dispatch(udt*& out, 
		 const Sequence<TIn>& in);
	
	//: fft length    
	size_t           m_fftLength;
	
	//: overlap length
	size_t           m_overlapLength;
	
	//: pointer to window
	std::unique_ptr<Filters::Window> m_pwindow;
    };

} // end of datacondAPI namespace 

#endif // WELCHCSDESTIMATE
