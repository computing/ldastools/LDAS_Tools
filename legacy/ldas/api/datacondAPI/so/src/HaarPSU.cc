//haar.cc

#include "datacondAPI/config.h"

#include <valarray>
#include <general/toss.hh>

#include "HaarPSU.hh"

namespace
{
  const char rcsID[] = "@(#) $Id: HaarPSU.cc,v 1.1 2009/01/13 23:25:50 emaros Exp $:";
}

namespace datacondAPI{
namespace psu
{

  Haar::Haar()
  {
    m_order = 1;


    m_H.resize(2);
    m_H[0] = 1.0;
    m_H[1] = -1.0;
    
    m_L.resize(2);
    m_L[0] = 1.0;
    m_L[1] = 1.0;


    m_sH.resize(2);
    m_sH[0] = -0.5;
    m_sH[1] = 0.5;

    m_sL.resize(2);
    m_sL[0] = 0.5;
    m_sL[1] = 0.5;

  }

  //:TODO: Need to fix to set order
  Haar::Haar(int order)
  {

    m_order = 1;

    m_H.resize(2);
    m_H[0] = 1.0;
    m_H[1] = -1.0;
    
    m_L.resize(2);
    m_L[0] = 1.0;
    m_L[1] = 1.0;


    m_sH.resize(2);
    m_sH[0] = -0.5;
    m_sH[1] = 0.5;

    m_sL.resize(2);
    m_sL[0] = 0.5;
    m_sL[1] = 0.5;
  }

  Haar::~Haar()
  {
  }

  Haar* Haar::Clone() const
  {
    return new Haar(m_order);
  }
  
  
  ILwd::LdasElement*
  Haar::ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    std::string what = "tried to call ConvertToIlwd";
    General::toss<std::domain_error>("Haar", __FILE__, __LINE__, what);
    return 0;
  }
  
}//namespace psu
 }//namespace datacondAPI
