#! /usr/bin/env perl
# ------------------------------------------------------------------------
# Standard set of transformations that need to be done one swig input
#  files.
# ------------------------------------------------------------------------
$filename = $oldinput . ".swig_rules.log";
open LOG, "> $filename";
print LOG "Hello world\n";
# ------------------------------------------------------------------------
# Make function declarations look like function definitions
# ------------------------------------------------------------------------
# $block=~s/\);/\)\n\{\}/og;
# ------------------------------------------------------------------------
# Remove lines that are not to be documented
# ------------------------------------------------------------------------
# $block =~ s/\/\/\s*BeginPercepsExclude\n.*\/\/\s*EndPercepsExclude\n//mg;
# ------------------------------------------------------------------------
# Remove %addmethods
# ------------------------------------------------------------------------
# $block=~s/\n{\s*//}?\s*%addmethods\s*\{$//g;
# ------------------------------------------------------------------------
# Debugging info
# ------------------------------------------------------------------------
print LOG "final block:\n$block\n";
close LOG;
