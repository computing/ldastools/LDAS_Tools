/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "datacondAPI/config.h"

#include "CSDSpectrumUMD.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"
                
datacondAPI::CSDSpectrumUMD::CSDSpectrumUMD()
    : m_detrendMethod(CSDEstimate::none)
{
}

datacondAPI::CSDSpectrumUMD::CSDSpectrumUMD(const CSDSpectrumUMD& value)
    : m_detrendMethod(value.m_detrendMethod), m_what(value.m_what)
{
}
                
datacondAPI::CSDSpectrumUMD::~CSDSpectrumUMD()
{
}

datacondAPI::CSDEstimate::DetrendMethod
datacondAPI::CSDSpectrumUMD::GetDetrendMethod()
{
    return m_detrendMethod;
}

void datacondAPI::CSDSpectrumUMD::
SetDetrendMethod(const CSDEstimate::DetrendMethod& detrendMethod)
{
    m_detrendMethod = detrendMethod;
}

std::string datacondAPI::CSDSpectrumUMD::GetEstimator()
{
    return m_what;
}

void datacondAPI::CSDSpectrumUMD::SetEstimator(const std::string& what)
{
    m_what = what;
}

ILwd::LdasElement* datacondAPI::CSDSpectrumUMD::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    try {
	switch ( Target )
	{
	case datacondAPI::udt::TARGET_GENERIC:
	    container->push_back( new ILwd::LdasArray<unsigned int>
				  (m_detrendMethod, "DetrendMethod"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    container->push_back( new ILwd::LdasString(m_what, "Estimator"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    break;
	default:
	    throw datacondAPI::udt::
		BadTargetConversion(Target,
				    "datacondAPI::CSDSpectrumUMD");
	}
    }
    catch ( ... )
    {
	delete container;
	throw;
    }
	    
    return container;
}

