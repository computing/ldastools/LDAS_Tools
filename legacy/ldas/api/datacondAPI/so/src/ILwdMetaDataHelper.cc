#include "datacondAPI/config.h"

#include "general/unordered_map.hh"
#include "general/toss.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"

#include "ILwdMetaDataHelper.hh"

using namespace datacondAPI;
using General::unordered_map;   

typedef unordered_map< std::string,
		       ILwdMetaDataHelper::meta_data_type > keyword_map_type;

typedef std::map< ILwdMetaDataHelper::meta_data_type,
		  std::string> reverse_lookup_map_type;
//-----------------------------------------------------------------------
// Forward declaration of static functions
//-----------------------------------------------------------------------

static const keyword_map_type& init_keyword_list( );
static const reverse_lookup_map_type& init_reverse_list( );

//-----------------------------------------------------------------------
// Local exceptions
//-----------------------------------------------------------------------

namespace
{
  class bad_key: public std::range_error
  {
  public:
    bad_key( const std::string& Key )
      : range_error( format(Key) )
    {
    }

    bad_key( const ILwdMetaDataHelper::meta_data_type& Key )
      : range_error( format( ILwdMetaDataHelper::ReverseLookup( Key ) ) )
    {
    }

  private:
    static std::string format( const std::string& Key )
    {
      std::ostringstream	ss;
      ss << "Requested Key("
	 << Key
	 << ") does not exist in ILwdMetaDataHelper";
      return ss.str();
    }
  };
}

//-----------------------------------------------------------------------
// Formal definition of class ILwdMetaDataHelper
//-----------------------------------------------------------------------

ILwdMetaDataHelper::
ILwdMetaDataHelper( const ILwd::LdasContainer& Container )
  : m_container( Container )
{
  init();
}


ILwdMetaDataHelper::
ILwdMetaDataHelper( const ILwd::LdasElement& Element )
  : m_container( dynamic_cast<const ILwd::LdasContainer&>(Element) )
{
  init();
}

template<class Type_>
Type_ ILwdMetaDataHelper::
Get( meta_data_type Key ) const
{
  child_map_type::const_iterator	pos = m_children.find( Key );

  if ( pos == m_children.end() )
  {
    throw bad_key( Key );
  }
  const ILwd::LdasArray< Type_ >* e
    ( dynamic_cast<const ILwd::LdasArray< Type_ >*>( (*pos).second ) );
  if ( e )
  {
    return e->getData()[0];
  }
  else
  {
    throw std::domain_error( "Cannot cast to requested type" );
  }
}

template<class Type_> Type_ ILwdMetaDataHelper::
Get( meta_data_type Key, const Type_& Default ) const
{
  child_map_type::const_iterator	pos = m_children.find( Key );

  if ( pos == m_children.end() )
  {
    return Default;
  }
  const ILwd::LdasArray< Type_ >* e
    ( dynamic_cast<const ILwd::LdasArray< Type_ >*>( (*pos).second ) );
  if ( e )
  {
    return e->getData()[0];
  }
  else
  {
    throw std::domain_error( "Cannot cast to requested type" );
  }
}

template <class Type_>
const Type_* ILwdMetaDataHelper::
GetILwd( meta_data_type Key, bool ThrowException ) const
{
  return get_ilwd<Type_>( Key, ThrowException );
}

template <class Type_>
Type_* ILwdMetaDataHelper::
GetILwd( meta_data_type Key, bool ThrowException )
{
  return const_cast< Type_*>( get_ilwd<Type_>( Key, ThrowException ) );
}

const ILwd::LdasElement* ILwdMetaDataHelper::
GetILwd( meta_data_type Key, bool ThrowException ) const
{
  child_map_type::const_iterator	pos = m_children.find( Key );

  if ( pos == m_children.end() )
  {
    if ( ThrowException )
    {
      throw bad_key( Key );
    }
    else
    {
      return (const ILwd::LdasElement*)NULL;
    }
  }
  return (*pos).second;
}

const std::string& ILwdMetaDataHelper::
ReverseLookup( ILwdMetaDataHelper::meta_data_type Type )
{
  const reverse_lookup_map_type&	reverse_lookup( init_reverse_list( ) );

  reverse_lookup_map_type::const_iterator	pos( reverse_lookup.find( Type ) );
  if ( pos == reverse_lookup.end() )
  {
    General::toss<std::range_error>( "ILwdMetaDataHelper::ReverseLookup",
				     __FILE__,
				     __LINE__,
				     "Type is out of range" );
  }
  return (*pos).second;
}

void ILwdMetaDataHelper::
init( )
{
  using namespace ILwd;
  const keyword_map_type&		kw( init_keyword_list( ) );
  keyword_map_type::const_iterator	kw_end( kw.end() );

  for ( LdasContainer::const_iterator e = m_container.begin();
	e != m_container.end();
	e++ )
  {
    keyword_map_type::const_iterator	kwi( kw.find( (*e)->getNameString() ) );

    if ( kwi != kw_end )
    {
      m_children[ (*kwi).second ] = *e;
    }
  }
}

template <class Type_>
const Type_* ILwdMetaDataHelper::
get_ilwd( meta_data_type Key, bool ThrowException ) const
{
  child_map_type::const_iterator	pos = m_children.find( Key );

  if ( pos == m_children.end() )
  {
    if ( ThrowException )
    {
      throw bad_key( Key );
    }
    else
    {
      return (const Type_*)NULL;
    }
  }
  return dynamic_cast<const Type_*>( (*pos).second );
}

#define	INSTANTIATE(Type_) \
template Type_ \
ILwdMetaDataHelper::Get< Type_ >( ILwdMetaDataHelper::meta_data_type ) const; \
template Type_ \
ILwdMetaDataHelper::Get< Type_ >( ILwdMetaDataHelper::meta_data_type, \
				  const Type_& )  const

INSTANTIATE(INT_2S);
INSTANTIATE(INT_2U);
INSTANTIATE(INT_4S);
INSTANTIATE(INT_4U);
INSTANTIATE(REAL_4);
INSTANTIATE(REAL_8);

#undef INSTANTIATE

#define	INSTANTIATE(LM_TYPE) \
template const LM_TYPE* \
ILwdMetaDataHelper::GetILwd< LM_TYPE >( ILwdMetaDataHelper::meta_data_type, \
				         bool ThrowException ) const; \
template LM_TYPE* \
ILwdMetaDataHelper::GetILwd< LM_TYPE >( ILwdMetaDataHelper::meta_data_type, \
				         bool ThrowException ); \
template const LM_TYPE* \
ILwdMetaDataHelper::get_ilwd< LM_TYPE >( ILwdMetaDataHelper::meta_data_type, \
				         bool ThrowException ) const
INSTANTIATE(ILwd::LdasContainer);
INSTANTIATE(ILwd::LdasArrayBase);
INSTANTIATE(ILwd::LdasArray<INT_2U>);
INSTANTIATE(ILwd::LdasArray<INT_2S>);
INSTANTIATE(ILwd::LdasArray<INT_4U>);
INSTANTIATE(ILwd::LdasArray<REAL_4>);
INSTANTIATE(ILwd::LdasArray<REAL_8>);

#undef INSTANTIATE

//-----------------------------------------------------------------------
// Declaration of static functions
//-----------------------------------------------------------------------

static const keyword_map_type&
init_keyword_list( )
{
  static keyword_map_type	keywords;

  //:TODO: Make thread safe with double loop and mutex locking
  if ( keywords.size() == 0 )
  {
    keywords[ "data" ] = ILwdMetaDataHelper::DATA;
    keywords[ "dt" ] = ILwdMetaDataHelper::DELTA_TIME;
    keywords[ "fShift" ] = ILwdMetaDataHelper::FREQUENCY_SHIFT;
    keywords[ ":detectProc:Container(Detector):Frame" ] =
      ILwdMetaDataHelper::DETECTOR;
    keywords[ ":history:Container(History):Frame" ] =
      ILwdMetaDataHelper::HISTORY;
    keywords[ "sampleRate" ] = ILwdMetaDataHelper::SAMPLE_RATE;
    keywords[ "timeOffset" ] = ILwdMetaDataHelper::TIME_OFFSET;
    keywords[ ":procData:Container(ProcData):Frame" ] =
      ILwdMetaDataHelper::PROC_DATA;
    keywords[ ":data:Container(Vect):Frame" ] = ILwdMetaDataHelper::DATA;
    keywords[ "elevation" ] = ILwdMetaDataHelper::ELEVATION;
    keywords[ "latitude" ] = ILwdMetaDataHelper::LATITUDE;
    keywords[ "latitudeD" ] = ILwdMetaDataHelper::LATITUDE_DEGREES;
    keywords[ "latitudeM" ] = ILwdMetaDataHelper::LATITUDE_MINUTES;
    keywords[ "latitudeS" ] = ILwdMetaDataHelper::LATITUDE_SECONDS;
    keywords[ "longitude" ] = ILwdMetaDataHelper::LONGITUDE;
    keywords[ "longitudeD" ] = ILwdMetaDataHelper::LONGITUDE_DEGREES;
    keywords[ "longitudeM" ] = ILwdMetaDataHelper::LONGITUDE_MINUTES;
    keywords[ "longitudeS" ] = ILwdMetaDataHelper::LONGITUDE_SECONDS;
    keywords[ "armXazimuth" ] = ILwdMetaDataHelper::ARM_X_AZIMUTH;
    keywords[ "armYazimuth" ] = ILwdMetaDataHelper::ARM_Y_AZIMUTH;
    keywords[ "armXaltitude" ] = ILwdMetaDataHelper::ARM_X_ALTITUDE;
    keywords[ "armYaltitude" ] = ILwdMetaDataHelper::ARM_Y_ALTITUDE;
    keywords[ "localTime" ] = ILwdMetaDataHelper::LOCAL_TIME;
    keywords[ "dataQuality" ] = ILwdMetaDataHelper::DATA_QUALITY;
    keywords[ "GTime" ] = ILwdMetaDataHelper::START_TIME;
    keywords[ "phase" ] = ILwdMetaDataHelper::PHASE;
    keywords[ "type" ] = ILwdMetaDataHelper::TYPE;
    keywords[ "subType" ] = ILwdMetaDataHelper::SUBTYPE;
    keywords[ "tRange" ] = ILwdMetaDataHelper::TRANGE;
  }
  return keywords;
}

static const reverse_lookup_map_type&
init_reverse_list( )
{
  static reverse_lookup_map_type	lookup;

  //:TODO: Make thread safe with double loop and mutex locking
  if ( lookup.size() == 0 )
  {
    const keyword_map_type& kwl( init_keyword_list( ) );
    for ( keyword_map_type::const_iterator kw( kwl.begin() );
	  kw != kwl.end();
	  kw++ )
    {
      lookup[ (*kw).second ] = (*kw).first;
    }
  }
  return lookup;
}
