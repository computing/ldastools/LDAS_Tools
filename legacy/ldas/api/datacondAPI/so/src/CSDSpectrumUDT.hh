/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef CSDSPECTRUMUDT_HH
#define CSDSPECTRUMUDT_HH
// $Id: CSDSpectrumUDT.hh,v 1.8 2006/02/16 16:54:58 emaros Exp $

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "TimeBoundedFreqSequenceUDT.hh"
#include "CSDSpectrumUMD.hh"

namespace datacondAPI
{
  //: Representation of a cross-spectral density
  template<class T> 
  class CSDSpectrum : public TimeBoundedFreqSequence<T>, 
		      public CSDSpectrumUMD
  {
    
  public:
    
    // constructors

    //: Default constructor
    CSDSpectrum();

    //: Copy constructor
    //!param: value - object to construct copy of
    CSDSpectrum(const CSDSpectrum& value);
    
    //: Destructor
    virtual ~CSDSpectrum();
    
    // udt
    
    //: Duplicate *this on heap
    //!return: Spectrum<T>* - duplicate of *this, allocated on heap
    virtual CSDSpectrum< T >* Clone() const;

    //: Convert *this to an ILWD
    //!param: Chain - CallChain *this is part of
    //!param: Target - intended destination of ILWD
    //!return: ILwd::LdasContainer* - ILWD allocated on heap or null pointer
    virtual ILwd::LdasContainer* 
    ConvertToIlwd(const CallChain& Chain,
		  datacondAPI::udt::target_type Target 
		  = datacondAPI::udt::TARGET_GENERIC ) const;
			
  }; // class CSDSpectrum

} // namespace datacondAPI

#endif // CSDSPECTRUMUDT_HH
