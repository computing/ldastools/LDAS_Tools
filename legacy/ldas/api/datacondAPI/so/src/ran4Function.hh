#ifndef _RAN4FUNCTION_HH_
#define _RAN4FUNCTION_HH_

#include "CallChain.hh"

class ran4Function : public CallChain::Function
{
public:
  ran4Function();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_RAN4FUNCTION_HH_
