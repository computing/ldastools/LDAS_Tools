#include "config.h"

#include <complex>

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"

#include "TSeriesFunction.hh"

//=======================================================================
// Sequence -> TimeSeries conversion
//=======================================================================

namespace {

    const TSeriesFunction staticTSeriesFunction;

}

TSeriesFunction::TSeriesFunction()
    : Function(TSeriesFunction::GetName())
{
}

void TSeriesFunction::Eval(CallChain* Chain,
                           const CallChain::Params& Params,
			   const std::string& Ret) const
{
    using namespace datacondAPI;
    using General::GPSTime;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int arg = 0;

    double fs = 1.0;
    double frequency = 0.0;
    double phase = 0.0;
    
    unsigned int start_s = 0;
    unsigned int start_ns = 0;

    switch(Params.size())
    {
    case 6:
        // Phase

	arg = 6;
	obj = Chain->GetSymbol(Params[arg - 1]);
	if (const Scalar<double>* const p =
	    dynamic_cast<const Scalar<double>*>(obj))
	{
	    phase = p->GetValue();
	}
	else
	{
            throw CallChain::BadArgument(GetName(),
                                         arg,
                                         TypeInfoTable.GetName(typeid(*obj)),
                                TypeInfoTable.GetName(typeid(Scalar<double>)));
	}

	// Fall through

    case 5:
        // Base frequency

	arg = 5;
	obj = Chain->GetSymbol(Params[arg - 1]);
	if (const Scalar<double>* const p =
	    dynamic_cast<const Scalar<double>*>(obj))
	{
	    frequency = p->GetValue();
	}
	else
	{
            throw CallChain::BadArgument(GetName(),
                                         arg,
                                         TypeInfoTable.GetName(typeid(*obj)),
                                TypeInfoTable.GetName(typeid(Scalar<double>)));
	}

	// Fall through

    case 4:
        // Start nanosecond

	arg = 4;
	obj = Chain->GetSymbol(Params[arg - 1]);
	if (const Scalar<int>* const p =
	    dynamic_cast<const Scalar<int>*>(obj))
	{
	    start_ns = p->GetValue();
	}
	else
	{
            throw CallChain::BadArgument(GetName(),
                                         arg,
                                         TypeInfoTable.GetName(typeid(*obj)),
                                   TypeInfoTable.GetName(typeid(Scalar<int>)));
	}

	// Fall through

    case 3:
        // Start second

	arg = 3;
	obj = Chain->GetSymbol(Params[arg - 1]);
	if (const Scalar<int>* const p =
	    dynamic_cast<const Scalar<int>*>(obj))
	{
	    start_s = p->GetValue();
	}
	else
	{
            throw CallChain::BadArgument(GetName(),
                                         arg,
                                         TypeInfoTable.GetName(typeid(*obj)),
                                   TypeInfoTable.GetName(typeid(Scalar<int>)));
        }

	// Fall through

    case 2:
        // Sampling rate

	arg = 2;
	obj = Chain->GetSymbol(Params[arg - 1]);
	if (const Scalar<double>* const p =
	    dynamic_cast<const Scalar<double>*>(obj))
	{
	    fs = p->GetValue();
	}
	else
	{
            throw CallChain::BadArgument(GetName(),
                                         arg,
                                         TypeInfoTable.GetName(typeid(*obj)),
                                TypeInfoTable.GetName(typeid(Scalar<double>)));
	}

	arg = 1;
	obj = Chain->GetSymbol(Params[arg - 1]);

	if (const Sequence<float>* const p
	    = dynamic_cast<const Sequence<float>*>(obj))
	{
	    TimeSeries<float>* out = new TimeSeries<float>(*p, fs,
                                                   GPSTime(start_s, start_ns));

	    out->SetBaseFrequency(frequency);
	    out->SetPhase(phase);
	    
	    result = out;
	}
	else if (const Sequence<double>* const p
		 = dynamic_cast<const Sequence<double>*>(obj))
	{
	    TimeSeries<double>* out = new TimeSeries<double>(*p, fs,
                                                   GPSTime(start_s, start_ns));

	    out->SetBaseFrequency(frequency);
	    out->SetPhase(phase);
	    
	    result = out;
	}
	else if (const Sequence<std::complex<float> >* const p
		 = dynamic_cast<const Sequence<std::complex<float> >*>(obj))
	{
	    TimeSeries<std::complex<float> >* out 
		= new TimeSeries<std::complex<float> >(*p, fs,
                                                   GPSTime(start_s, start_ns));

	    out->SetBaseFrequency(frequency);
	    out->SetPhase(phase);
	    
	    result = out;
	}
	else if (const Sequence<std::complex<double> >* const p
	    = dynamic_cast<const Sequence<std::complex<double> >*>(obj))
	{
	    TimeSeries<std::complex<double> >* out
		= new TimeSeries<std::complex<double> >(*p, fs,
                                                   GPSTime(start_s, start_ns));

	    out->SetBaseFrequency(frequency);
	    out->SetPhase(phase);
	    
	    result = out;
	}
	else
        {
            throw CallChain::BadArgument(GetName(),
                                         arg,
                                         TypeInfoTable.GetName(typeid(*obj)),
                                         "Object derived from Sequence");
        }

	break;

    default:
      throw CallChain::BadArgumentCount(GetName(), 2, Params.size());
      break;
    }

    if (result == 0)
    {
      throw CallChain::NullResult(GetName());
    }

    Chain->ResetOrAddSymbol(Ret, result);
}

const std::string& TSeriesFunction::
GetName(void) const
{
    static const std::string name("tseries");

    return name;
}
