// $Id:
//
// "cpu_time" action
//
// [y = ]cpu_time()
//
// y: double
//    cpu time (in seconds)
//
// "user_time" action
//
// [y = ]user_time()
//
// y: double
//    user time (in seconds)
//
// "wall_time" action
//
// [y = ]wall_time()
//
// y: double
//    wall time (in seconds)

#include "config.h"

#include <sys/times.h>
#include <unistd.h>

#include "TimingFunctions.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

static double	compute_cpu_time(void);
static double	compute_user_time(void);
static double	compute_wall_time(void);

//=======================================================================
// CpuTimeFunction
//=======================================================================

//-----------------------------------------------------------------------------
// We construct a static, global instance of a CpuTimeFunction so that the
// constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

static CpuTimeFunction	_CpuTimeFunction;

//-----------------------------------------------------------------------------
//
//: CpuTimeFunction::CpuTimeFunction
//
// default constuctor
//
CpuTimeFunction::
CpuTimeFunction()
  : Function( CpuTimeFunction::GetName() )
{
  
  //-------------------------------------------------------------------------
  // The sole task of the CpuTimeFunction constructor is to pass the action
  // name, defined by GetName, to the Function constructor.
  //-------------------------------------------------------------------------

}

//----------------------------------------------------------------------------
//
//: Evaluate cpu time within a call chain
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "cpu_time". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//
void CpuTimeFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  
  //-------------------------------------------------------------------------
  // We use a switch statement to handle the various number of Params we are
  // given. The cpu_time action has no arguments, so we handle cases 0 and
  // throw an exception for any other number of arguments.
  //-------------------------------------------------------------------------
  
  switch(Params.size())
  {
  case 0:
    {
      //-----------------------------------------------------------------
      // We set the symbol name Ret with the calculated result,
      // superceeding any previous value of Ret.
      //-----------------------------------------------------------------
  
      Chain->ResetOrAddSymbol( Ret,
			       new datacondAPI::Scalar<double>
			       (compute_cpu_time()) );
    }
    break;
  default:
    
    //---------------------------------------------------------------------
    // Unsupported number of arguments. We throw an exception
    //---------------------------------------------------------------------
    
    throw CallChain::BadArgumentCount( GetName(), 0, Params.size() );
    break;
  }
}

//=======================================================================
// TimeFunction
//=======================================================================

//-----------------------------------------------------------------------------
// We construct a static, global instance of a TimeFunction so that the
// constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

static TimeFunction	_TimeFunction;

//-----------------------------------------------------------------------------
//
//: TimeFunction::TimeFunction
//
// default constuctor
//
TimeFunction::
TimeFunction()
  : Function( TimeFunction::GetName() )
{
  
  //-------------------------------------------------------------------------
  // The sole task of the TimeFunction constructor is to pass the action
  // name, defined by GetName, to the Function constructor.
  //-------------------------------------------------------------------------

}

//----------------------------------------------------------------------------
//
//: Evaluate cpu time within a call chain
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "cpu_time". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//
void TimeFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  
  //-------------------------------------------------------------------------
  // We use a switch statement to handle the various number of Params we are
  // given. The cpu_time action has no arguments, so we handle cases 0 and
  // throw an exception for any other number of arguments.
  //-------------------------------------------------------------------------
  
  switch(Params.size())
  {
  case 0:
    {

      datacondAPI::Sequence<double>*
	t(new datacondAPI::Sequence<double>(3));

      (*t)[0] = compute_cpu_time();
      (*t)[1] = compute_user_time();
      (*t)[2] = compute_wall_time();

      //-----------------------------------------------------------------
      // Set the symbol name Ret with the calculated result,
      // superceeding any previous value of Ret.
      //-----------------------------------------------------------------
  
      Chain->ResetOrAddSymbol( Ret, t );
    }
    break;
  default:
    
    //---------------------------------------------------------------------
    // Unsupported number of arguments. We throw an exception
    //---------------------------------------------------------------------
    
    throw CallChain::BadArgumentCount( GetName(), 0, Params.size() );
    break;
  }
}

//=======================================================================
// UserTimeFunction
//=======================================================================

static UserTimeFunction	_UserTimeFunction;

UserTimeFunction::
UserTimeFunction()
  : Function( UserTimeFunction::GetName() )
{
}

void UserTimeFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  switch(Params.size())
  {
  case 0:
    {
      //-----------------------------------------------------------------
      // Store the results
      //-----------------------------------------------------------------

      Chain->ResetOrAddSymbol( Ret,
			       new datacondAPI::Scalar<double>
			       (compute_user_time()));
    }
    break;
  default:
    throw CallChain::BadArgumentCount( GetName(), 0, Params.size() );
    break;
  }
}

//=======================================================================
// WallTimeFunction
//=======================================================================

static WallTimeFunction	_WallTimeFunction;

WallTimeFunction::
WallTimeFunction()
  : Function( WallTimeFunction::GetName() )
{
}

void WallTimeFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
  const
{
  switch(Params.size())
  {
  case 0:
    {
      //-----------------------------------------------------------------
      // Store the results
      //-----------------------------------------------------------------

      Chain->ResetOrAddSymbol(Ret,
			      new datacondAPI::Scalar<double>
			      (compute_wall_time()) );
    }
    break;
  default:
    throw CallChain::BadArgumentCount( GetName(), 0, Params.size() );
    break;
  }
}

static double
compute_cpu_time(void)
{
  //-----------------------------------------------------------------
  // Extract the cpu time.
  //-----------------------------------------------------------------
  
  struct tms	now;
  double	val;
  long		clock_ticks(sysconf(_SC_CLK_TCK));
  
  
  if ((clock_t)-1 == times(&now)) {
    throw CallChain::BadSystemCall( "compute_cpu_time()", "times",
				    __FILE__, __LINE__ );
  }
 
  val = now.tms_stime / (double)clock_ticks;
  return val;
}

static double
compute_user_time(void)
{
  struct tms	now;
  double	val;
  long		clock_ticks(sysconf(_SC_CLK_TCK));

  
  if ( (clock_t)-1 == times(&now))
  {
    throw CallChain::BadSystemCall( "compute_user_time()", "times",
				    __FILE__, __LINE__ );
  }
 

  val = now.tms_utime / (double)clock_ticks;
  return val;    
}

static double
compute_wall_time(void)
{
  struct timeval	now;
  double		val;
  
  if (gettimeofday(&now, 0) < 0)
  {
    throw CallChain::BadSystemCall( "compute_wall_time()", "gettimeofday",
				    __FILE__, __LINE__ );
  } 
      
  val = (double)now.tv_sec + now.tv_usec / 1000000.0;
  return val;    
}
