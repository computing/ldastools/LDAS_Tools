/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef SHIFTFUNCTION_HH
#define SHIFTFUNCTION_HH

// $Id: ShiftFunction.hh,v 1.2 2007/01/24 17:51:28 emaros Exp $

#include "CallChain.hh"

//-----------------------------------------------------------------------------
//: Function support for shift action
//
// This class implements the "shift" function to be used in the action
// section of a user command. The syntax, as it would appear in the action
// sequence is one of:
//
//     y = shift(x, offset, order, [z]);
//
// or
//
//     y = shift(x, z);
//
// where x is a time-series, offset is the number of samples (possible
// fractional) we want to shift by (offset >= 0), order is the degree of
// the Lagrange polynomial to be used if interpolation is needed (that is,
// if offset is not a whole number), and z is the (optional) state variable.
//
//
class ShiftFunction : public CallChain::Function {
    
public:
    
  //-------------------------------------------------------------------------
  //: Constructor
  //
  // Construct a new ShiftFunction
  ShiftFunction();
    
  //-------------------------------------------------------------------------
  //: Evaluate the Shift Function
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual void Eval(CallChain* chain,
                    const CallChain::Params& params,
                    const std::string& ret) const;
    
  //-------------------------------------------------------------------------
  //: Return the Name of the function ("shift")
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //!return: the function
  virtual const std::string& GetName() const;
        
  //-------------------------------------------------------------------------
  //: Validate parameters
  virtual void
  ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		      const CallChain::Params& Params ) const;
};

#endif // SHIFTFUNCTION_HH
