//
// :FILENAME:   $Id: Matrix.hh,v 1.9 2009/01/20 17:22:39 emaros Exp $
//
// :PURPOSE:    Defines a Matrix UDT for the datacondAPI
// 
// :REFERENCES:
//
// :NOTES:      Uses a proxy class to allow std::slice_array semantics and
//              optimisations.  The underlying representation is a
//              FORTRAN-packed std::valarray.  In general (as for
//              std::valarray) an attempt to explicitly instantiate the class
//              is an error.  This class is an extended reimplementation of
//              the original datacondAPI::Matrix.
//
#ifndef MATRIX_HH
#define MATRIX_HH

#include <memory>

#include "general/Memory.hh"

// ilwd
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

// datacondAPI
#include "LinearAlgebra.hh"
#include "SequenceUDT.hh"

namespace
{
    const char rcsid[] = "@(#) $Id: Matrix.hh,v 1.9 2009/01/20 17:22:39 emaros Exp $";
}

#define MATRIX_ENABLE_BACKWARD_COMPATIBILITY

namespace datacondAPI {

    // forward declarations to facilitate friendship

    template<typename T> class Matrix;
    template<typename T> bool operator!=(const Matrix<T>&, const Matrix<T>&);
    template<typename T> bool operator==(const Matrix<T>&, const Matrix<T>&);
    template<typename T> Matrix<T> operator*(const T&, const Matrix<T>&);

    //: A UDT Matrix with natural semantics
    // 
    //  Uses a proxy class to allow std::slice_array semantics and
    //  optimisations.  The underlying representation is a FORTRAN-packed
    //  std::valarray.  In general (as for std::valarray) an attempt to
    //  explicitly instantiate the class is an error.  This class is an
    //  extended reimplementation of the original datacondAPI::Matrix.
    //
    template<typename T>
    class Matrix
        : public udt
    {

    public:

        typedef T value_type;

        //: Default constructor
        //
        //  Empty matrix
        //
        Matrix();

        //: Sized constructor
        //
        //  Constructs a T()-initialised matrix with the given dimensions
        //
        //!param: rows    - the number of rows
        //!param: columns - the number of columns
        //!param: scalar  - intial scalar value of the metrix elements (defaults to T())
        //
        //!exc: std::bad_alloc - insufficent memory to construct matrix
        //
        Matrix(std::size_t rows, std::size_t columns, const T& scalar = T());

        //: Copy constructor
        //
        //  Makes an identical copy of the argument
        //
        //!param: right - the original matrix (on the right-hand-side of the
        //+copy)
        //
        //!exc: std::bad_alloc - insufficent memory to construct the matrix
        //
        Matrix(const Matrix& right);

        //: Destructor
        //
        //  Destroys all elements and deallocates memory
        //
        virtual ~Matrix() {}

        //: Virtual copy constructor
        //
        //  UDT interface
        //
        //!return: pointer to a copy of *this
        //
        //!exc: std::bad_alloc - insufficent memory to construct the new matrix
        //
        virtual Matrix* Clone() const { return new Matrix(*this); }

        virtual ILwd::LdasElement* ConvertToIlwd(const CallChain& Chain, datacondAPI::udt::target_type Target = datacondAPI::udt::TARGET_GENERIC) const;

        //: Assignment operator
        //
        //  Sets the matrix elements to a scalar value
        //
        //!param: right - a scalar value
        //
        //!return: a reference to the left-hand-side object
        //
        Matrix& operator=(const T& right);

        //: Assignment operator
        //
        //  Unlike the underlying valarray, matrix assignment resizes the
        //  left-hand-side if required
        //
        //!param: right - the matrix to be assigned from
        //
        //!return: a reference to the left-hand-side object
        //
        //!exc: std::bad_alloc - insufficent memory to resize the
        //+left-hand-side matrix
        //
        Matrix& operator=(const Matrix& right);

        //: The matrix's number of rows
        //
        //!return - the number of rows
        //
        std::size_t rows() const { return m_rows; }

        //: The matrix's number of columns
        //
        //!return - the number of columns
        //
        std::size_t columns() const { return m_columns; }

        //: Encapsulated dimensions of the matrix
        //
        //  The primary purpose of this method is to allow a simple comparison
        //  of the size of two matrices,</P><PRE>a.size() == b.size()</PRE><P>
        //  rather than</P><PRE>a.rows() == b.size() &amp;&amp; a.columns() ==
        //  b.columns()</PRE>
        //
        //!return: - a std::pair containing (.first) the matrix's number of rows and
        //+(.second) the number of columns
        //
        std::pair<std::size_t, std::size_t> size() const;

        //: Change the size of a matrix
        //
        //  The elements of the resized matrix are reinitialized to T()
        //
        //!param: rows    - the new number of rows
        //!param: columns - the new number of columns
        //
        //!exc: std::bad_alloc - insufficent memory to resize the matrix
        //
        void resize(std::size_t rows, std::size_t columns);

        //: Unary plus
        //
        //!return: a matrix with the same dimensions as *this, whose elements
        //+are produced by applying unary plus to the elements of *this;
        //
        //!exc: std::bad_alloc - insufficent memory to construct the returned
        //+matrix
        //
        Matrix operator+() const { return Matrix(m_rows, m_columns, +m_data); }

        //: Unary minus
        //
        //!return: a matrix with the same dimensions as *this, whose elements
        //+are produced by applying unary minus to the elements of *this;
        //
        //!exc: std::bad_alloc - insufficent memory to construct the returned
        //+matrix
        //
        Matrix operator-() const { return Matrix(m_rows, m_columns, -m_data); }

        //: Multiplicative assignment
        //
        //  Multiplies the elements of the matrix by a scalar
        //
        //!param: right - a scalar multiplier
        //
        //!return: a reference to *this
        //
        Matrix& operator*=(const T& right) { m_data *= right; return *this; }

        //: Divisive assignment
        //
        //  Divides the elements of the matrix by a scalar
        //
        //!param: right - a scalar divisior
        //
        //!return: a reference to *this
        //
        Matrix& operator/=(const T& right) { m_data /= right; return *this; }

        //: Additive assignment
        //
        //  Adds a scalar to the elements of the matrix
        //
        //!param: right - an additive scalar
        //
        //!return: a reference to *this
        //
        Matrix& operator+=(const T& right);

        //: Additive assignment
        //
        //  Adds to the elements of the matrix the elements of another matrix
        //
        //!param: right - a matrix with the same size as *this
        //
        //!return: a reference to *this
        //
        //!exc: std::logic_error - matrix sizes are diferent
        //
        Matrix& operator+=(const Matrix& right);

        //: Subtractive assignment
        //
        //  Subtracts a scalar from the elements of the matrix
        //
        //!param: right - a subtractive scalar
        //
        //!return: a reference to *this
        //
        Matrix& operator-=(const T& right);

        //: Subtractive assignment
        //
        //  Subtracts from the elements of the matrix the elements of another matrix
        //
        //!param: right - a matrix with the same size as *this
        //
        //!return: a reference to *this
        //
        //!exc: std::logic_error - matrix sizes are diferent
        //
        Matrix& operator-=(const Matrix& right);

        class proxy_array
        {

        public:

            operator const std::valarray<T>() const
	    {
	      return static_cast<const std::valarray<T> >(m_data)[m_slice];
	    }

            proxy_array& operator=(const T& right);
            proxy_array& operator=(const std::valarray<T>& right);

            T& operator[](std::size_t i);
            const T operator[](std::size_t i) const;

#define MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(OPERATION)\
            proxy_array& operator OPERATION (const T&);\
            proxy_array& operator OPERATION (const std::valarray<T>& right);

MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(+=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(-=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(*=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(/=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(%=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(&=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(|=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(^=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(<<=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(>>=)
#undef MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT

        private:

            proxy_array(); // prohibited

            proxy_array(std::valarray<T>& data, const std::slice& slice);

            proxy_array(const proxy_array& original);

            std::valarray<T>& m_data;
            std::slice m_slice;

            // privacy and frendship for proxy_array may need to be reconsidered to support types derived from Matrix

           friend class datacondAPI::Matrix<T>;

        };

        //: Reference a row of the matrix
        //
        //  Matrix uses a proxy class to reference its rows or columns in a
        //  fashion exactly analagous to std::slice_array.  Rows or columns
        //  can be (compute) assigned to or from valarrays, support implicit
        //  conversion to const std::valarrays (so that they may replace
        //  std::valarrays in expressions) and support subscripting individual
        //  elements of the matrix.
        //
        //!return: a proxy reference to a row of the matrix
        //
        //!exc: std::out_of_range - the requested row does not exist
        //
        proxy_array row(std::size_t i);

        //: Reference a constant row of the matrix
        //
        //!return: a proxy const reference to a row of the matrix
        //
        //!exc: std::out_of_range - the requested row does not exist
        //
        const proxy_array row(std::size_t i) const;

        //: Reference a column of the matrix
        //
        //!return: a proxy reference to a column of the matrix
        //
        //!exc: std::out_of_range - the requested column does not exist
        //
        proxy_array column(std::size_t j);

        //: Reference a constant column of the matrix
        //
        //!return: a proxy const reference to a column of the matrix
        //
        //!exc: std::out_of_range - the requested column does not exist
        //
        const proxy_array column(std::size_t j) const;

        //: Reference a row of the matrix
        //
        //  This operator permits natural-syntax addressing of an element of
        //  the matrix, A[i][j].  If the intent is to return a row reference
        //  A[i] then the syntax A.row(i) should be preferred for clarity.
        //
        //!return: a proxy reference to a row of the matrix
        //
        //!exc: std::out_of_range - the requested row does not exist
        //
        proxy_array operator[](std::size_t i) { return row(i); }

        //: Reference a constant row of the matrix
        //
        //!return: a proxy const reference to a row of the matrix
        //
        //!exc: std::out_of_range - the requested row does not exist
        //
        const proxy_array operator[](std::size_t i) const { return row(i); }

#ifdef MATRIX_ENABLE_BACKWARD_COMPATIBILITY

        // The following member functions are deprecated, should not be
        // used in new code, and may be retired in the future.
        // The functions requiring the user to pack and unpack data manually
        // have been demonstrated to be error-prone and dangerous

        // deprecated, must use manual FORTRAN packing { column, column, column, ... }
        // instead, explicitly construct from logical rows, columns or elements
        Matrix(const std::valarray<T>& data, std::size_t rows, std::size_t columns);

        // replaced by A.rows()
        std::size_t getNRows() const {return rows(); }

        // replaced by A.columns()
        std::size_t getNCols() const { return columns(); }

        // replaced by more efficent A.row(i)
        std::valarray<T> getRow(std::size_t i) const { return row(i); }

        // replaced by more efficent A.col(j)
        std::valarray<T> getCol(std::size_t j) const { return column(j); }

        // deprecated, uses FORTRAN packing { column, column, column, ... }
        // instead, explicitly get logical rows, columns or elements
        void getData(std::valarray<T>& data) const;

        // deprecated, must use manual FORTRAN packing { column, column, column, ... }
        // instead, explicitly set logical rows, columns or elements
        void setData(const std::valarray<T>& data);

        // replaced by A.row(i) = data;
        void setRow(std::size_t i, const std::valarray<T>& data) { row(i) = data; }

        // replaced by A.column(j) = data;
        void setCol(std::size_t j, const std::valarray<T>& data) { column(j) = data; }

#endif

    private:

        // used by friend operations for efficency
        Matrix(std::size_t rows, std::size_t columns, const std::valarray<T>& data);

        // mutable (never const) allows constness of proxy_array to enforce
        // const-correctness, and works around the
        // "const T std::valarray<T>::operator[](std::size_t) const" not
        // "const T& std::valarray<T>::operator[](std::size_t) const" issue
        mutable std::valarray<T> m_data;
        std::size_t m_rows;
        std::size_t m_columns;

#if HAVE_LIBCLAPACK
        friend class CLAPACKSoHandle;
#endif /* HAVELIB_CLAPACK */
        friend bool operator!=<>(const Matrix&, const Matrix&);
        friend bool operator==<>(const Matrix&, const Matrix&);
        friend Matrix operator*<>(const T&, const Matrix&);

    };

    template<typename T>
        inline Matrix<T>::Matrix()
        : m_data()
        , m_rows(0)
        , m_columns(0)
    {
    }

    template<typename T>
        inline Matrix<T>::Matrix(std::size_t rows, std::size_t columns, const T& scalar)
        : m_data(scalar, rows * columns), m_rows(rows), m_columns(columns)
    {
    }

    template<typename T>
        inline Matrix<T>::Matrix(const Matrix& original)
	  : datacondAPI::udt( original ),
	    m_data(original.m_data),
	    m_rows(original.m_rows),
	    m_columns(original.m_columns)
    {
    }

    template<typename T>
        inline ILwd::LdasElement* Matrix<T>::ConvertToIlwd(const CallChain& Chain, datacondAPI::udt::target_type Target) const
    {
        std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);

        switch (Target)
        {
        case datacondAPI::udt::TARGET_GENERIC:
            container->push_back(new ILwd::LdasArray<int>(m_rows, "NRows"),
                                 ILwd::LdasContainer::NO_ALLOCATE,
                                 ILwd::LdasContainer::OWN);
            container->push_back(new ILwd::LdasArray<int>(m_columns, "NCols"),
                                 ILwd::LdasContainer::NO_ALLOCATE,
                                 ILwd::LdasContainer::OWN);
            container->push_back(new ILwd::LdasArray<T>(&(m_data[0]),
                                                        m_data.size()),
                                 ILwd::LdasContainer::NO_ALLOCATE,
                                 ILwd::LdasContainer::OWN);
            break;
        default:
            throw datacondAPI::udt::BadTargetConversion(Target, "datacondAPI::Matrix");

        }

        return container.release();
    }

    template<typename T> inline Matrix<T>& Matrix<T>::operator=(const T& right)
    {
        m_data = right;
        return *this;
    }

    template<typename T> inline Matrix<T>& Matrix<T>::operator=(const Matrix& right)
    {
        if (&right != this)
        {
            if ((m_rows != right.m_rows) || (m_columns != right.m_columns))
            {
                m_rows = right.m_rows;
                m_columns = right.m_columns;
                m_data.resize(m_rows * m_columns);
            }
            m_data = right.m_data;
        }
        return *this;
    }

    template<typename T>
        inline std::pair<std::size_t, std::size_t> Matrix<T>::size() const
    {
        return std::pair<std::size_t, std::size_t>(m_rows, m_columns);
    }

    template<typename T>
        inline void Matrix<T>::resize(std::size_t rows, std::size_t columns)
    {
        m_rows = rows;
        m_columns = columns;
        m_data.resize(rows * columns, T());
    }

    template<typename T>
        inline Matrix<T>& Matrix<T>::operator+=(const T& right)
    {
        m_data += right;
        return *this;
    }

    template<typename T>
        inline Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& right)
    {
        if ((m_rows != right.m_rows) || (m_columns != right.m_columns))
        {
            throw std::logic_error("Matrix<T>& Matrix<T>::operator+=(const Matrix<T>&): Matrix dimensions are different");
        }
        m_data += right.m_data;
        return *this;
    }

    template<typename T>
        inline Matrix<T>& Matrix<T>::operator-=(const T& right)
    {
        m_data -= right;
        return *this;
    }

    template<typename T>
        inline Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& right)
    {
        if ((m_rows != right.m_rows) || (m_columns != right.m_columns))
        {
            throw std::logic_error("Matrix<T>& Matrix<T>::operator-=(const Matrix<T>&): Matrix dimensions are different");
        }
        m_data -= right.m_data;
        return *this;
    }

    template<typename T>
        inline typename Matrix<T>::proxy_array& Matrix<T>::proxy_array::operator=(const T& right)
    {
        m_data[m_slice] = right;
        return *this;
    }

    template<typename T>
        inline typename Matrix<T>::proxy_array& Matrix<T>::proxy_array::operator=(const std::valarray<T>& right)
    {
        if (m_slice.size() != right.size())
        {
            throw std::logic_error("proxy_array& Matrix<T>::proxy_array::operator=(const std::valarray<T>&): array sizes are different");
        }
        m_data[m_slice] = right;
        return *this;
    }

    template<typename T>
        inline T& Matrix<T>::proxy_array::operator[](std::size_t i)
    {
        if (i >= m_slice.size())
        {
            throw std::out_of_range("T& Matrix<T>::proxy_array::operator[](std::size_t): array index beyond valid range");
        }
        return m_data[m_slice.start() + m_slice.stride() * i];
    }

    template<typename T>
        inline const T Matrix<T>::proxy_array::operator[](std::size_t i) const
    {
        if (i >= m_slice.size())
        {
            throw std::out_of_range("const T Matrix<T>::proxy_array::operator[](std::size_t) const: array index beyond valid range");
        }
        return m_data[m_slice.start() + m_slice.stride() * i];
    }

    // note that std::slice_array does not define computed assignment from scalars

#define MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(OPERATION)\
    template<typename T>\
        inline typename Matrix<T>::proxy_array& Matrix<T>::proxy_array::operator OPERATION (const T& right)\
    {\
        for (std::size_t i = m_slice.start(); i < (m_slice.start() + m_slice.size() * m_slice.stride()); i += m_slice.stride())\
        {\
            m_data[i] OPERATION right;\
        }\
        return *this;\
    }\
    \
    template<typename T>\
        inline typename Matrix<T>::proxy_array& Matrix<T>::proxy_array::operator OPERATION (const std::valarray<T>& right)\
    {\
        if (m_slice.size() != right.size())\
        {\
            throw std::logic_error(std::string("proxy_array& Matrix<T>::proxy_array::operator") + #OPERATION + "(const std::valarray<T>&): array sizes are different");\
        }\
        m_data[m_slice] OPERATION right;\
        return *this;\
    }

MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(+=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(-=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(*=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(/=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(%=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(&=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(|=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(^=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(<<=)
MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT(>>=)

#undef MATRIX_PROXY_ARRAY_COMPUTED_ASSIGNMENT

    template<typename T>
        inline Matrix<T>::proxy_array::proxy_array(std::valarray<T>& data, const std::slice& slice)
            : m_data(data), m_slice(slice)
    {
    }

    template<typename T>
        inline Matrix<T>::proxy_array::proxy_array(const proxy_array& original)
            : m_data(original.m_data), m_slice(original.m_slice)
    {
    }

    template<typename T>
        inline typename Matrix<T>::proxy_array Matrix<T>::row(std::size_t i)
    {
        if (i >= m_rows)
        {
            throw std::out_of_range("proxy_array Matrix<T>::row(std::size_t): row index beyond valid range");
        }
        return proxy_array(m_data, std::slice(i, m_columns, m_rows)); // FORTRAN packing
    }

    template<typename T>
        inline const typename Matrix<T>::proxy_array Matrix<T>::row(std::size_t i) const
    {
        if (i >= m_rows)
        {
            throw std::out_of_range("const proxy_array Matrix<T>::row(std::size_t) const: row index beyond valid range");
        }
        return proxy_array(m_data, std::slice(i, m_columns, m_rows)); // FORTRAN packing
    }

    template<typename T>
        inline typename Matrix<T>::proxy_array Matrix<T>::column(std::size_t j)
    {
        if (j >= m_columns)
        {
            throw std::out_of_range("proxy_array Matrix<T>::column(std::size_t): column index beyond valid range");
        }
        return proxy_array(m_data, std::slice(j * m_rows, m_rows, 1)); // FORTRAN packing
    }

    template<typename T>
        inline const typename Matrix<T>::proxy_array Matrix<T>::column(std::size_t j) const
    {
        if (j >= m_columns)
        {
            throw std::out_of_range("const proxy_array Matrix<T>::column(std::size_t) const: column index beyond valid range");
        }
        return proxy_array(m_data, std::slice(j * m_rows, m_rows, 1)); // FORTRAN packing
    }

#ifdef MATRIX_ENABLE_BACKWARD_COMPATIBILITY

    // syntax preserved, performance enhanced

    template<typename T>
        inline Matrix<T>::Matrix(const std::valarray<T>& data, std::size_t rows, std::size_t columns)
        : m_data(data)
        , m_rows(rows)
        , m_columns(columns)
    {
        if (m_data.size() != m_rows * m_columns)
        {
            throw std::logic_error("Matrix::Matrix(const std::valarray<T>&, std::size_t, std::size_t): the given Matrix dimensions are incompatible with the size of the valarray");
        }
    }

    template<typename T>
        inline void Matrix<T>::getData(std::valarray<T>& data) const
    {
        data.resize(m_data.size());
        data = m_data;
    }

    template<typename T>
        inline void Matrix<T>::setData(const std::valarray<T>& data)
    {
        if (data.size() != m_data.size())
        {
            throw std::logic_error("void Matrix<T>::setData(const std::valarray<T>&): the size of the valarray is incompatible with the existing dimensions of the Matrix");
        }
        m_data = data;
    }

#endif

    template<typename T>
        inline Matrix<T>::Matrix(std::size_t rows, std::size_t columns, const std::valarray<T>& data)
        : m_data(data)
        , m_rows(rows)
        , m_columns(columns)
    {
    }

    template<typename T>
        inline bool operator!=(const Matrix<T>& left, const Matrix<T>& right)
    {
        if (left.size() != right.size())
        {
            return true;
        }
        if (std::size_t delta = left.m_rows * left.m_columns)
        {
            return !std::equal(&left.m_data[0], &left.m_data[0] + delta, &right.m_data[0]);
        }
        else
        {
            return false;
        }
    }

    template<typename T>
        inline bool operator==(const Matrix<T>& left, const Matrix<T>& right)
    {
        if (left.size() != right.size())
        {
            return false;
        }
        if (std::size_t delta = left.m_rows * left.m_columns)
        {
            return std::equal(&left.m_data[0], &left.m_data[0] + delta, &right.m_data[0]);
        }
        else
        {
            return true;
        }
    }

    template<typename T>
        inline Matrix<T> operator+(const Matrix<T>& left, const T& right)
    {
        return Matrix<T>(left) += right;
    }

    template<typename T>
        inline Matrix<T> operator+(const T& left, const Matrix<T>& right)
    {
        return Matrix<T>(right) += left; //:TODO: assumes commutative
    }

    template<typename T>
        inline Matrix<T> operator+(const Matrix<T>& left, const Matrix<T>& right)
    {
        if (left.size() != right.size())
        {
            throw std::logic_error("Matrix<T> operator+(const Matrix<T>&, const Matrix<T>&): Matrix dimensions are different");
        }
        return Matrix<T>(left) += right;
    }

    template<typename T>
        inline Matrix<T> operator-(const Matrix<T>& left, const T& right)
    {
        return Matrix<T>(left) -= right;
    }

    template<typename T>
        inline Matrix<T> operator-(const T& left, const Matrix<T>& right)
    {
        return -(Matrix<T>(right) -= left); //:TODO: inefficent
    }

    template<typename T>
        inline Matrix<T> operator-(const Matrix<T>& left, const Matrix<T>& right)
    {
        if (left.size() != right.size())
        {
            throw std::logic_error("Matrix<T> operator-(const Matrix<T>&, const Matrix<T>&): Matrix dimensions are different");
        }
        return Matrix<T>(left) -= right;
    }

    template<typename T>
        inline Matrix<T> operator*(const Matrix<T>& left, const T& right)
    {
        return Matrix<T>(left) *= right;
    }

    template<typename T>
        inline Matrix<T> operator*(const T& left, const Matrix<T>& right)
    {
        return Matrix<T>(right.m_rows, right.m_columns, left * right.m_data);
    }

    template<typename T>
        inline Matrix<T> operator/(const Matrix<T>& left, const T& right)
    {
        return Matrix<T>(left) /= right;
    }

} // namespace datacondAPI

namespace datacondAPI
{

    template<typename T>
        inline Matrix<T> operator*(const Matrix<T>& left, const Matrix<T>& right)
    {
        Matrix<T> result;
	LinearAlgebra::MM(result, left, right);
        return result;
    }

    template<typename T>
        inline std::valarray<T> operator*(const Matrix<T>& left, const std::valarray<T>& right)
    {
        Sequence<T> result;
	LinearAlgebra::MV(result, left, Sequence<T>(right));
        return result;
    }

} // namespace datacondAPI

#endif // MATRIX_HH
