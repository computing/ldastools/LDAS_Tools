/* -*- mode: c++ -*- */
#ifndef LINEARDETRENDFUNCTION_HH
#define	LINEARDETRENDFUNCTION_HH

#include "CallChain.hh"

//: Function support for linear detrending
//
// This class implements the lineardetrend() action to be used in the 
// algorithms section of a Tcl command. The syntax as it would appear in
// the action sequence is: x = lineardetrend(y)
//
class LinearDetrendFunction: CallChain::Function
{
public:
  //: Constructor
  //
  // Construct a new LinearDetrend function
  LinearDetrendFunction();

  //: Evaluate the LinearDetrend Function
  //
  // This performs the LinearDetrend transformation. The list of parameters
  // must contain a single value which is the name of the symbol to
  // use as the input array to the LinearDetrend function.
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& Params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual
  void Eval(      CallChain* Chain,
	    const CallChain::Params& Params,
	    const std::string& Ret) const;

  //: Return the Name of the function
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //!return: of the function
  virtual
  const std::string& GetName() const;

};

#endif	// LINEARDETRENDFUNCTION_HH
