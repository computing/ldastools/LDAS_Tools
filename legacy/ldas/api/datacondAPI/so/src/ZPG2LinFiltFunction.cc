// $Id: ZPG2LinFiltFunction.cc,v 1.2 2005/12/01 22:54:58 emaros Exp $
//
// "zpg2linfilt" action
//
// z= zpg2linfilt(zeroes, poles, gain);
//
// zeroes: Sequence<complex<double> >
// poles:  Sequence<complex<double> >
// gain:   Scalar<double>
// z:      LinFiltState
#include "config.h"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "LinFilt.hh"
#include "ZPG2LinFiltFunction.hh"

using namespace datacondAPI;
using namespace std;

namespace {

    ZPG2LinFiltFunction static_ZPG2LinFiltFunction;

}

const string& ZPG2LinFiltFunction::GetName() const
{
  static const string name("zpg2linfilt");

  return name;
}

ZPG2LinFiltFunction::ZPG2LinFiltFunction()
    : Function( ZPG2LinFiltFunction::GetName() )
{
}

void ZPG2LinFiltFunction::Eval(CallChain* chain,
                               const CallChain::Params& params,
                               const string& ret) const
{
    if (params.size() != 3)
    {
        throw CallChain::BadArgumentCount(GetName(), "3", params.size());
    }
    
    const udt* const zeroesUdt = chain->GetSymbol(params[0]);
    const udt* const polesUdt = chain->GetSymbol(params[1]);
    const udt* const gainUdt = chain->GetSymbol(params[2]);
    
    const Sequence<complex<double> >* zeroes
        = dynamic_cast<const Sequence<complex<double> >*>(zeroesUdt);
    
    if (zeroes == 0)
    {
        throw CallChain::BadArgument(GetName(),
                                     0,
                                     TypeInfoTable.GetName(typeid(*zeroesUdt)),
                                     "Sequence<complex<double> >");
    }
    
    const Sequence<complex<double> >* poles
        = dynamic_cast<const Sequence<complex<double> >*>(polesUdt);
    
    if (poles == 0)
    {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     TypeInfoTable.GetName(typeid(*polesUdt)),
                                     "Sequence<complex<double> >");
    }
    
    double gain = 0;
    
    if (const Scalar<double>* const p
        = dynamic_cast<const Scalar<double>*>(gainUdt))
    {
        gain = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     2,
                                     TypeInfoTable.GetName(typeid(*gainUdt)),
                                     "Scalar<double>");
    }
    
    LinFiltState* const z = new LinFiltState(*zeroes, *poles, gain);

    chain->ResetOrAddSymbol(ret, z);
}
