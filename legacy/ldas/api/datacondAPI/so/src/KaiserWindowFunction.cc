// $Id: KaiserWindowFunction.cc,v 1.6 2005/12/01 22:54:58 emaros Exp $
//
// KaiserWindow action
//
// KaiserWindow has one input argument and one return value. The format is
//
//     window = KaiserWindow( [ beta ] );
//
// window is a KaiserWindowUDT
//
#include "datacondAPI/config.h"

#include <stdexcept>
   
#include "KaiserWindowFunction.hh"
#include "WindowUDT.hh"
#include "ScalarUDT.hh"

using namespace std;   
   
namespace {
    
    KaiserWindowFunction staticKaiserWindowFunction;

}

KaiserWindowFunction::KaiserWindowFunction()
    : Function( KaiserWindowFunction::GetName() )
{
}

const std::string& KaiserWindowFunction::GetName(void) const
{
    static std::string name("KaiserWindow");

    return name;
}

void KaiserWindowFunction::Eval(CallChain* Chain,
                                const CallChain::Params& Params,
                                const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* window = 0;

    if (Params.size() == 1)
    {
        const CallChain::Symbol* const betaUDT
            = Chain->GetSymbol(Params[0]);
        
        if (const Scalar<int>* const p
            = dynamic_cast<const Scalar<int>*>(betaUDT))
        {
            window = new KaiserWindowUDT(double(p->GetValue()));
        }
        else if (const Scalar<double>* const p
                 = dynamic_cast<const Scalar<double>*>(betaUDT))
        {
            window = new KaiserWindowUDT(p->GetValue());
        }
        else
        {
            throw invalid_argument("Illegal Argument: action KaiserWindow(); "
                        "argument #1 must be a Scalar<int> or Scalar<double>");
            
        }
    }
    else if (Params.size() == 0)
    {
        window = new datacondAPI::KaiserWindowUDT();
    }
    else
    {
        throw CallChain::BadArgumentCount("KaiserWindow", 0, Params.size());
    }

    Chain->ResetOrAddSymbol(Ret, window);
}
