/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include <memory>

#include "general/Memory.hh"
#include "general/types.hh"

#include "GapFill.hh"
#include "TimeSeries.hh"
#include "Statistics.hh"

using namespace datacondAPI;
using General::GPSTime;
using std::complex;   

namespace {

    // Fill the gaps with a constant value
    template<class T>
    void fill(TimeSeries<T>& data,
              const T& value,
              const GapMetaData<GPSTime>::Transitions_type& gaps)
    {
        GapMetaData<GPSTime>::Transitions_type::const_iterator i = gaps.begin();
        while (i != gaps.end())
        {
            // recall, (*i).stop is not in the gap, it's the next point after
            // the end of the gap
            const std::slice s((*i).start, (*i).stop - (*i).start, 1);
            data[s] = value;
            ++i;
        }
    }

    // Fill gaps with the mean of the non-gap data
    template<class T>
    void fillMean(TimeSeries<T>& data, 
                  const GapMetaData<GPSTime>::Transitions_type& gaps)
    {
        // Total number of samples in gaps
        size_t gap_total = 0;
        
        GapMetaData<GPSTime>::Transitions_type::const_iterator i = gaps.begin();
        while (i != gaps.end())
        {
            gap_total += ((*i).stop - (*i).start);
            ++i;
        }

        // Calculate the mean of the non-gap data
        // First, fill the gaps with zero
        fill(data, T(0), gaps);
        
        Statistics stats;
        const size_t size = stats.size(data);

        // The first product is the sum of the non-gap data, since
        // the gaps have been set to zero.
        // Dividing by the total number of samples minus the
        // number of gaps gives the mean
        const T m = size*stats.mean(data)/(size - gap_total);

        fill(data, m, gaps);
    }

    // Fill gaps with a straight line fit between the start of the gap and
    // the end of the gap
    template<class T>
    void fillLinear(TimeSeries<T>& data, 
                    const GapMetaData<GPSTime>::Transitions_type& gaps)
    {
        GapMetaData<GPSTime>::Transitions_type::const_iterator i = gaps.begin();
        while (i != gaps.end())
        {
            const size_t start = (*i).start;
            const size_t stop = (*i).stop;
            const T width = stop - start + 1;

            // The values of the data just before and just after the gap.
            // If the gap includes the first sample, use the next
            // non-gap value for the whole gap, that is, fill with a
            // constant value equal to the first non-gap value.
            // Otherwise, use the last value before the gap.
            const T startVal = ((start <= 0) ? data[stop] : data[start - 1]);

            // If the gap includes the last sample, use the last
            // non-gap value for the whole gap, that is, fill with a
            // constant value equal to the last non-gap value.
            // Otherwise, use the next value after the gap.
            const T stopVal = ((stop >= data.size()) ? data[start - 1] :
                               data[stop]);

            for (size_t k = start; k < stop; ++k)
            {
                const T lambda = (k - start + 1)/width;
                data[k] = (1.0 - lambda)*startVal + lambda*stopVal;
            }

            ++i;
        }

    }

  //---------------------------------------------------------------------
  // Template instantiation
  //---------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE( T ) \
    template			   \
    void fill(TimeSeries< T >& data, \
              const T& value, \
              const GapMetaData<GPSTime>::Transitions_type& gaps)

    INSTANTIATE( complex< float > );
    INSTANTIATE( complex< double > );
  
#undef INSTANTIATE

} // namespace

datacondAPI::GapFill::GapFill(const FillMethod_type fill)
    : m_fill(fill)
{
}

void datacondAPI::GapFill::apply(udt*& out, const udt& in) const
{
    // :TODO: Implement gap-filling for complex TimeSeries
    // The problem is that the Statistics class only works
    // on real Sequences.

#define APPLY(lm_type) \
    if (const TimeSeries< lm_type >* const p =             \
        dynamic_cast<const TimeSeries< lm_type >* >(&in))  \
    {                                                     \
        dispatch(out, *p);                                \
    }

#define ELSE_APPLY(lm_type) \
    else APPLY(lm_type)

    APPLY(float)
    ELSE_APPLY(double)
    ELSE_APPLY(complex<float>)
    ELSE_APPLY(complex<double>)
    else
    {
        throw std::invalid_argument("GapFill::apply() "
                                    "input UDT must be a TimeSeries");
    }

#undef ELSE_APPLY
#undef APPLY
}

template<class T>
void datacondAPI::GapFill::dispatch(udt*& out, const T& in) const
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    std::unique_ptr<T> tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
        tmp.reset(new T());
        tmp_out = tmp.get();
    }
    else if (!udt::IsA<T>(*tmp_out))
    {
        throw std::invalid_argument("GapFill::apply() "
                                    "invalid output UDT");
    }

    apply(udt::Cast<T>(*tmp_out), in);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
        // transfer ownership from the unique_ptr to 'out' so that the
        // output isn't deleted when the unique_ptr goes out of scope
        out = tmp.release();
    }
}

template<class T>
void datacondAPI::GapFill::apply(TimeSeries<T>& out,
                                 const TimeSeries<T>& in) const
{
    out = in;

    GapMetaData<GPSTime>::Transitions_type gaps;
        
    out.CalcTransitions(gaps);

    // Don't bother unless there are gaps
    if (gaps.size() != 0)
    {
        switch(m_fill)
        {
        case ZERO:
            fill(out, T(0), gaps);
            break;
        case AVERAGE:
            fillMean(out, gaps);
            break;
        case LINEAR:
            fillLinear(out, gaps);
            break;
        default:
            throw std::runtime_error("GapFill::apply() "
                                     "unimplemented fill method");
            break;
        }
    }

    out.GapSetFillMethod(m_fill);
}

namespace datacondAPI
{
  template<>
  void GapFill::apply(TimeSeries<complex<float> >& out,
		      const TimeSeries<complex<float> >& in) const
  {
    out = in;

    GapMetaData<GPSTime>::Transitions_type gaps;
        
    out.CalcTransitions(gaps);

    // Don't bother unless there are gaps
    if (gaps.size() != 0)
    {
      switch(m_fill)
      {
      case ZERO:
	fill(out, complex<float>(0), gaps);
	break;
      case AVERAGE:
      case LINEAR:
      default:
	throw std::runtime_error("GapFill::apply() "
				 "unimplemented fill method for TimeSeries<float>");
	break;
      }
    }

    out.GapSetFillMethod(m_fill);
  }

  template<>
  void GapFill::apply(TimeSeries<complex<double> >& out,
		      const TimeSeries<complex<double> >& in) const
  {
    out = in;

    GapMetaData<GPSTime>::Transitions_type gaps;
        
    out.CalcTransitions(gaps);

    // Don't bother unless there are gaps
    if (gaps.size() != 0)
    {
      switch(m_fill)
      {
      case ZERO:
	fill(out, complex<double>(0), gaps);
	break;
      case AVERAGE:
      case LINEAR:
      default:
	throw std::runtime_error("GapFill::apply() "
				 "unimplemented fill method for TimeSeries<double>");
	break;
      }
    }

    out.GapSetFillMethod(m_fill);
  }
} // namespace - datacondAPI
