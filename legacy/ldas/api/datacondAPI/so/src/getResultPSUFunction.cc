#include "datacondAPI/config.h"

#include "getResultPSUFunction.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "UDT.hh"
#include "result.hh"

using namespace datacondAPI::psu;  

const std::string& getResultPSUFunction::GetName(void) const
{
  static std::string name("getResultPSU");

  return name;
}

getResultPSUFunction::getResultPSUFunction() : Function( getResultPSUFunction::GetName())
{}

static getResultPSUFunction _getResultPSUFunction;

void getResultPSUFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			   const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;

  switch(Params.size())
    {
    case 2:
      {
	if(datacondAPI::udt::IsA<datacondAPI::psu::Result<double> >( *Chain->GetSymbol(Params[0])))
	  {
	    datacondAPI::psu::Result<double> in(datacondAPI::udt::Cast<datacondAPI::psu::Result<double> >(*Chain->GetSymbol(Params[0])));
	    if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	      {
		datacondAPI::Scalar<int> level(datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue());
		if(in.checkMetaData())
		  {
		    datacondAPI::TimeSeries<double> temp;
		    datacondAPI::udt* p_temp = &temp;
		    in.getResult(p_temp, level);
		    returnedSymbol = new datacondAPI::TimeSeries<double> (temp);
		  }
		else
		  {
		    datacondAPI::Sequence<double> temp;
		    datacondAPI::udt* p_temp = &temp;
		    in.getResult(p_temp, level);
		    returnedSymbol = new datacondAPI::Sequence<double> (temp);
		  }
	      }
	    else //params[1] is not a scalar<int>
	      {
		throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type, second argument", "Usage: getResultPSUFunction(in, level)");
	      }
	  }

	else if (datacondAPI::udt::IsA<datacondAPI::psu::Result<float> >( *Chain->GetSymbol(Params[0])))
	  {
	    datacondAPI::psu::Result<float> in( datacondAPI::udt::Cast<datacondAPI::psu::Result<float> >(*Chain->GetSymbol(Params[0])));
	    if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	      {
		datacondAPI::Scalar<int> level(datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue());
		if(in.checkMetaData())
		  {
		    datacondAPI::TimeSeries<float> temp;
		    datacondAPI::udt* p_temp = &temp;
		    in.getResult(p_temp, level);
		    returnedSymbol = new datacondAPI::TimeSeries<float> (temp);
		  }

		else
		  {
		    datacondAPI::Sequence<float> temp;
		    datacondAPI::udt* p_temp = &temp;
		    in.getResult(p_temp, level);
		    returnedSymbol = new datacondAPI::Sequence<float> (temp);
		  }
	      }
	    else //params[1] is not a scalar<int>
	      {
		throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type, second argument", "Usage: getResultPSUFunction(in, level)");
	      }
	  }
	else //params[0] is not a result of type double or float
	  {
	    throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type, first argument", "Usage: getResultPSUFunction(in, level)");
	  }
	break;

      }//end case 2

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: getResultPSUFunction(in, level)");
	break;
      }//end default

   }//end switch

  if(!returnedSymbol)
  { 
    throw CallChain::NullResult( GetName() );
  }

  Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval

