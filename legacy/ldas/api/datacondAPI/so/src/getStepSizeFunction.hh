/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef GETSTEPSIZEFUNCTION_HH
#define GETSTEPSIZEFUNCTION_HH

// $Id: getStepSizeFunction.hh,v 1.1 2002/11/30 01:14:05 Philip.Charlton Exp $

#include "CallChain.hh"

//
//: Function support for getStepSize action
//
// stepsize = getStepSize(x);
//
class getStepSizeFunction : public CallChain::Function
{

public:

    getStepSizeFunction();

    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;

    virtual const std::string& GetName() const;

};

#endif // GETSTEPSIZEFUNCTION_HH
