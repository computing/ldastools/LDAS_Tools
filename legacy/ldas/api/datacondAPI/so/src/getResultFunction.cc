#include "datacondAPI/config.h"

#include "getResultFunction.hh"
#include "SequenceUDT.hh"
#include "UDT.hh"
#include "result.hh"

namespace datacondAPI
{

const std::string& getResultFunction::GetName(void) const
{
  static std::string name("getResult");

  return name;
}

getResultFunction::getResultFunction() : Function( getResultFunction::GetName())
{}

static getResultFunction _getResultFunction;

void getResultFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			   const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;
  using namespace datacondAPI;

  switch(Params.size())
    {
    case 2:
      {
	if(udt::IsA<Result<double> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<double> in(udt::Cast<Result<double> >(*Chain->GetSymbol(Params[0])));
	    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	      {
		int level = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
		returnedSymbol = new Sequence<double> (in.getResult(level));
	      }
	  }

	else if (udt::IsA<Result<float> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<float> in( udt::Cast<Result<float> >(*Chain->GetSymbol(Params[0])));
	    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	      {
		int level = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
		returnedSymbol = new Sequence<float> (in.getResult(level));
	      }
	  }
	break;
      }//end case 2

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: getResultFunction(in, level)");
	break;
      }//end default

   }//end switch

  if(!returnedSymbol)
  { 
    throw CallChain::NullResult( GetName() );
  }

  Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval

}//end namespace datacondAPI
