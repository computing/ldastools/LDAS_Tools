#include "datacondAPI/config.h"

#include "Coherence.hh"

namespace datacondAPI
{
  Coherence::Coherence(const WelchCSDEstimate& csd)
    : m_csd(csd)
  {
  }

  Coherence::Coherence(const Coherence& right)
    : m_csd(right.m_csd)
  {
  }

  Coherence::~Coherence()
  {
  }

  Coherence& Coherence::operator=(const Coherence& right)
  {
    if (this != &right)
      {
	m_csd = right.m_csd;
      }

    return *this;
  }

  using namespace std;

  void Coherence::apply(datacondAPI::udt*& out, const datacondAPI::udt& xIn, const datacondAPI::udt& yIn)
  {
    try { 
      if (const datacondAPI::Sequence<float>* p = dynamic_cast<const datacondAPI::Sequence<float>*>(&xIn))
	{
	  const datacondAPI::Sequence<float>& q = dynamic_cast<const datacondAPI::Sequence<float>&>(yIn);  
          if (!out) out = new Sequence<std::complex<float> >;
	  datacondAPI::Sequence<std::complex <float> >& r(dynamic_cast<datacondAPI::Sequence<std::complex<float> >&>(*out));
	  apply(r, *p, q);
	}
      else if (const datacondAPI::Sequence<complex<float> >* p = dynamic_cast<const datacondAPI::Sequence<complex<float> >*>(&xIn))
	{
	  const datacondAPI::Sequence<complex<float> >& q = dynamic_cast<const datacondAPI::Sequence<complex<float> >&>(yIn);  
          if (!out) out = new Sequence<std::complex<float> >;
	  datacondAPI::Sequence<std::complex <float> >& r(dynamic_cast<datacondAPI::Sequence<std::complex<float> >&>(*out));
	  apply(r, *p, q);
	}
      else if (const datacondAPI::Sequence<double>* p = dynamic_cast<const datacondAPI::Sequence<double>*>(&xIn))
	{
	  const datacondAPI::Sequence<double>& q = dynamic_cast<const datacondAPI::Sequence<double>&>(yIn);
          if (!out) out = new Sequence<std::complex<double> >; 
	  datacondAPI::Sequence<complex <double> >& r(dynamic_cast<datacondAPI::Sequence<complex<double> >&>(*out));
	  apply(r, *p, q);
	}
      else 
        {
          const datacondAPI::Sequence<complex<double> >& dp = dynamic_cast<const datacondAPI::Sequence<complex<double> >&>(xIn);
	  const datacondAPI::Sequence<complex<double> >& q = dynamic_cast<const datacondAPI::Sequence<complex<double> >&>(yIn);
          if (!out) out = new Sequence<std::complex<double> >;  
	  datacondAPI::Sequence<complex <double> >& r(dynamic_cast<datacondAPI::Sequence<complex<double> >&>(*out));
	  apply(r, dp, q);
	}
    }
    catch (const std::bad_cast&) {
      throw std::invalid_argument("bad argument types in Coherence::apply");
    }
  }

}
#include <iostream>
namespace datacondAPI
{
  template <typename T>
  void Coherence::apply(std::valarray<typename traits<T>::type>& out,
  const std::valarray<T>& in1, const std::valarray<T>& in2)
  {
    
    Sequence<T> x(in1);
    Sequence<T> y(in2);

    WelchCSDSpectrum<typename traits<T>::type> csdxy;

    m_csd.apply(csdxy, x, y);

    WelchSpectrum<typename traits<T>::real_type> psdx;
    WelchSpectrum<typename traits<T>::real_type> psdy;


    m_csd.apply(psdx, x);
    m_csd.apply(psdy, y);
    
    
    out.resize(csdxy.size());

    for (std::size_t i = 0; i < csdxy.size(); ++i)
      {
	out[i] = csdxy[i] / sqrt(psdx[i] * psdy[i]);
      }
          
  }
}
