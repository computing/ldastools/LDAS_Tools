/* -*- mode: c++; c-basic-offset: 4; -*- */
//
// $Id: getFilterCoeffsFunction.cc,v 1.10 2007/01/24 17:51:28 emaros Exp $
//
// "getFilterCoeffs" action
//
// getFilterCoeffs(z, b[, a])
//
// z: state (input)
//    an action state containing one or more filters (such as a ResampleState,
//    ARModel, or LinearFilter)
// b: (output)
//    the fir coefficents of the filter
// a: (optional output)
//    the iir coefficents of the filter

#include "datacondAPI/config.h"

#include <complex>
#include <valarray>

#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "ARModel.hh"
#include "LineRemover.hh"
#include "LinFiltState.hh"
#include "ResampleState.hh"

#include "getFilterCoeffsFunction.hh"

using namespace datacondAPI;

//=============================================================================
// getFilterCoeffsFunction
//=============================================================================

//-----------------------------------------------------------------------------
// getFilterCoeffsFunction::GetName
//-----------------------------------------------------------------------------

const std::string& getFilterCoeffsFunction::GetName(void) const
{

  //-------------------------------------------------------------------------
  // The GetName function returns the string "getFilterCoeffs", the name of
  // the action as presented to the user. The name is stored as a static
  // variable and returned as a const reference for performance reasons.
  //-------------------------------------------------------------------------

  static std::string name("getFilterCoeffs");

  return name;

}

//-----------------------------------------------------------------------------
// getFilterCoeffsFunction::getFilterCoeffsFunction
//-----------------------------------------------------------------------------

getFilterCoeffsFunction::getFilterCoeffsFunction() : Function( getFilterCoeffsFunction::GetName() )
{

  //-------------------------------------------------------------------------
  // The sole task of the getFilterCoeffsFunction constructor is to pass the action name,
  // defined by GetName, to the Function constructor.
  //-------------------------------------------------------------------------

}

//-----------------------------------------------------------------------------
// We construct a static, global instance of a getFilterCoeffsFunction so that
// the constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

static getFilterCoeffsFunction _getFilterCoeffsFunction;

//-----------------------------------------------------------------------------
// getFilterCoeffsFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "getFilterCoeffs". The Eval function recieves a pointer to the
// calling CallChain, and a list of symbol names (Params) and a return value
// symbol name (Ret).
//-----------------------------------------------------------------------------

void getFilterCoeffsFunction::Eval(CallChain* Chain,
                                   const CallChain::Params& Params,
                                   const std::string& Ret) const
{

    switch(Params.size())
    {
    case 3:
    case 2:
        {
            udt* state = Chain->GetSymbol(Params[0]);
            if (udt::IsA<ARModel>(*state))
            {
                const ARModel& ar = udt::Cast<ARModel>(*state);
                udt* b = 0;
                ar.getFilter(b);
                Chain->AddSymbol(Params[1], b);
                if (Params.size() == 3)
                {
                    Sequence<double>* a = new Sequence<double>(1.0, 1);
                    Chain->AddSymbol(Params[2], a);
                }
            }
            else if (udt::IsA<LineRemover>(*state))
            {
                const LineRemover& lr = udt::Cast<LineRemover>(*state);
                udt* b = 0;
                lr.getFilter(b);
                Chain->AddSymbol(Params[1], b);
                if (Params.size() == 3)
                {
                    Sequence<double>* a = new Sequence<double>(1.0, 1);
                    Chain->AddSymbol(Params[2], a);
                }
            }
            else if (udt::IsA<LinFiltState>(*state))
            {
                const LinFiltState& lf = udt::Cast<LinFiltState>(*state);
                Sequence<double>* b = new Sequence<double>;
                lf.getB(*b);
                Chain->AddSymbol(Params[1], b);
                if (Params.size() == 3)
                {
                    Sequence<double>* a = new Sequence<double>;
                    lf.getA(*a);
                    Chain->AddSymbol(Params[2], a);
                }
            }
            else if (udt::IsA<ResampleState>(*state))
            {
                const ResampleState& rs = udt::Cast<ResampleState>(*state);
                Sequence<double>* b = new Sequence<double>;
                rs.getB(*b);
                Chain->AddSymbol(Params[1], b);
                if (Params.size() == 3)
                {
                    // Resample has no getA method, use trivial filter
                    Sequence<double>* a = new Sequence<double>(1.0, 1);
                    Chain->AddSymbol(Params[2], a);
                }
            }
            else
            {
                throw CallChain::BadArgument(GetName(), 0, Params[0],"<State>");
            }
        }
        break;
    default:
        throw CallChain::BadArgumentCount(GetName(), "2-3", Params.size());
    }
}

//-----------------------------------------------------------------------
// This method validates accessability of the parameters
//-----------------------------------------------------------------------
void getFilterCoeffsFunction::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
    switch( Params.size( ) )
    {
    case 3:
	validateParameter( SymbolTable, Params[ 2 ], PARAM_WRITE );
	// :TRICKY: Fall through
    case 2:
	validateParameter( SymbolTable, Params[ 1 ], PARAM_WRITE );
	validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
	break;
    default:
	break;
    }
}
