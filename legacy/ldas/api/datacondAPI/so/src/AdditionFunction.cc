#include "datacondAPI/config.h"

#include <stdio.h>

#include <memory>
#include <valarray>

#include "general/Memory.hh"

#include "AdditionFunction.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

using std::complex;
using namespace datacondAPI;

//=============================================================================
// Static Helper functions
//=============================================================================

static datacondAPI::udt* eval( const datacondAPI::udt& Unknown1,
			       const datacondAPI::udt& Unknwon2 );

static datacondAPI::udt* eval2( const Sequence< complex< float> >& Known,
				const datacondAPI::udt& Unknown );
static datacondAPI::udt* eval2( const Sequence< complex< double> >& Known,
				const datacondAPI::udt& Unknown );
static datacondAPI::udt* eval2( const Sequence<float>& Known,
				const datacondAPI::udt& Unknown );
static datacondAPI::udt* eval2( const Sequence<double>& Known,
				const datacondAPI::udt& Unknown );

static udt* eval2( const Scalar< complex< double > >& Known,
				const udt& Unknown );
static udt* eval2( const Scalar< complex< float > >& Known,
				const udt& Unknown );

static datacondAPI::udt* eval2( const Scalar< double >& Known,
				const datacondAPI::udt& Unknown );
static datacondAPI::udt* eval2( const Scalar< float >& Known,
				const datacondAPI::udt& Unknown );
static datacondAPI::udt* eval2( const Scalar< int >& Known,
				const datacondAPI::udt& Unknown );

//=============================================================================
// Perform arithmatic on scalars
//=============================================================================

template< class Result_, class Parm1_ >
static inline
void op( Scalar< Result_ >& Result, const Scalar< Parm1_ >& Parm1 )
{
  Result.SetValue( Result.GetValue() + Parm1.GetValue() );
}

//-----------------------------------------------------------------------------
// Handle demotion rules
//-----------------------------------------------------------------------------

template<>
static inline
void op< complex< float >, complex< double > >
( Scalar< complex< float > >& Result,
  const Scalar< complex< double > >& Unknown )
{
    // Handle demotion case
    Result.SetValue( Result.GetValue() +
		     complex< float >( Unknown.GetValue().real(),
				       Unknown.GetValue().imag() ) );
}

//=============================================================================
// Perform arithmatic on sequences with Scalars
//=============================================================================

template< class Result_, class Parm1_ >
static inline
void op( Sequence< Result_ >& Result, const Parm1_& Parm1 )
{
  Result += Parm1;
}

//=============================================================================
// Perform arithmatic on sequences with sequences
//=============================================================================

template< class Result_, class Parm1_ >
static inline
void op( Sequence< Result_ >& Result, const Sequence<Parm1_>& Parm1 )
{
  if ( Result.size() != Parm1.size() )
  {
    throw CallChain::LogicError( "Sequences differ in size" );
  }
  for ( size_t i = 0; i < Result.size(); i++ )
  {
    Result[i] += (float)( Parm1[i] );
  }
}

//-----------------------------------------------------------------------------
// Handle demotion rules
//-----------------------------------------------------------------------------

template<>
static inline
void op< float, double >
( Sequence< float >& Result,
  const Sequence< double >& Parm1 )
{
  if ( Result.size() != Parm1.size() )
  {
    throw CallChain::LogicError( "Sequences differ in size" );
  }
  // Handle demotion case
  for ( size_t i = 0; i < Result.size(); i++ )
  {
    Result[i] += (float)( Parm1[i] );
  }
}

template<>
static inline
void op< complex< float >, double >
( Sequence< complex< float > >& Result,
  const Sequence< double >& Parm1 )
{
  if ( Result.size() != Parm1.size() )
  {
    throw CallChain::LogicError( "Sequences differ in size" );
  }
  // Handle demotion case
  for ( size_t i = 0; i < Result.size(); i++ )
  {
    Result[i] += (float)( Parm1[i] );
  }
}

template<>
static inline
void op< complex< float >, complex< double > >
( Sequence< complex< float > >& Result,
  const Sequence< complex< double > >& Parm1 )
{
  if ( Result.size() != Parm1.size() )
  {
    throw CallChain::LogicError( "Sequences differ in size" );
  }
  // Handle demotion case
  for ( size_t i = 0; i < Result.size(); i++ )
  {
    Result[i] += complex<float>( (float)Parm1[i].real(),
				 (float)Parm1[i].imag() );
  }
}

//=============================================================================
// Addition Function
//=============================================================================

AdditionFunction::
AdditionFunction()
  : Function( AdditionFunction::GetName() )
{
  //---------------------------------------------------------------------------
  // The sole purpose of the constructor is to register the action name, from
  // GetName(), using the Function constructor.
  //---------------------------------------------------------------------------
}

void AdditionFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
const
{
  if (2 != Params.size())
  {
    throw CallChain::BadArgumentCount( GetName(),
				       2,
				       Params.size() );
  }

  datacondAPI::udt*	left(Chain->GetSymbol(Params[0]));
  datacondAPI::udt*	right(Chain->GetSymbol(Params[1]));
  datacondAPI::udt*	answer;

  if (!(answer = eval(*left, *right)) &&
      !(answer = eval(*right, *left)))
  {
    std::string msg("unable to add type ");
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*left));
    msg += " to type ";
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*right));
    throw std::runtime_error(msg);
  }

  Chain->ResetOrAddSymbol( Ret, answer );
}

const std::string& AdditionFunction::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("add");

  return name;
}

//-----------------------------------------------------------------------------
// We construct a static instance of the AdditionFunction class. The
// constructor will register the name "add" and enable AdditionFunction::Eval
// to be called whenever the name is encountered.
//-----------------------------------------------------------------------------

static AdditionFunction _AdditionFunction;

//=======================================================================
// Definitions of static functions
//=======================================================================

static datacondAPI::udt*
eval(const datacondAPI::udt& Unknown1, const datacondAPI::udt& Unknown2)
{
  if (udt::IsA< Sequence< complex< float> > >(Unknown1))
  {
    return eval2(udt::Cast< Sequence< complex< float > > >(Unknown1),
		 Unknown2);
  }
  else if (udt::IsA< Sequence< complex< double > > >(Unknown1))
  {
    return eval2(udt::Cast< Sequence< complex< double > > >(Unknown1),
		 Unknown2);
  }
  else if (udt::IsA< Sequence<float> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<float> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Sequence<double> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<double> >(Unknown1), Unknown2);
  }
  else if ( udt::IsA< Scalar< complex< double > > >( Unknown1 ) )
  {
    return eval2( udt::Cast< Scalar< complex< double > > >(Unknown1),
		  Unknown2);
  }
  else if ( udt::IsA< Scalar< complex< float > > >( Unknown1 ) )
  {
    return eval2( udt::Cast< Scalar< complex< float > > >(Unknown1),
		  Unknown2);
  }
  else if (udt::IsA< Scalar<int> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<int> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Scalar<float> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<float> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Scalar<double> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<double> >(Unknown1), Unknown2);
  }
  return (udt*)NULL;
}

static datacondAPI::udt*
eval2(const Scalar< double >& Known, const datacondAPI::udt& Unknown)
{
  std::unique_ptr< Scalar< double > >	ret( Known.Clone() );

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // float = double + int
    op( *ret, udt::Cast< Scalar<int> >(Unknown) );
  }
  else if (udt::IsA< Scalar<float> >(Unknown) )
  {
    // float = double + float
    op( *ret, udt::Cast< Scalar<float> >(Unknown) );
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // double = double + double
    op( *ret, udt::Cast< Scalar<double> >(Unknown) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static datacondAPI::udt*
eval2(const Scalar< float >& Known, const datacondAPI::udt& Unknown)
{
  std::unique_ptr< Scalar< float > >	ret( Known.Clone() );

  if (udt::IsA< Scalar<float> >(Unknown))
  {
    // float = float + float
    op( *ret, udt::Cast< Scalar<float> >(Unknown) );
  }
  else if (udt::IsA< Scalar<int> >(Unknown))
  {
    // float = float + int
    op( *ret, udt::Cast< Scalar<int> >(Unknown) );
  }	
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2(const Scalar< complex< double > >& Known, const udt& Unknown)
{
  std::unique_ptr< Scalar< complex< double > > >	ret( Known.Clone() );

  if ( udt::IsA< Scalar< complex< double > > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< complex< double > > >( Unknown ) ); 
  }
  else if ( udt::IsA< Scalar< double > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< double > >( Unknown ) );
  }
  else if ( udt::IsA< Scalar< float > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< float > >( Unknown ) );
  }
  else if ( udt::IsA< Scalar< int > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< int > >( Unknown ) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }

  return ret.release();
}

static udt*
eval2(const Scalar< complex< float > >& Known, const udt& Unknown)
{
  std::unique_ptr< Scalar< complex< float > > >	ret( Known.Clone() );

  if ( udt::IsA< Scalar< complex< double > > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< complex< float > > >( Unknown ) );
  }
  else if ( udt::IsA< Scalar< complex< float > > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< complex< float > > >( Unknown ) );
  }
  else if ( udt::IsA< Scalar< double > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< double > >( Unknown ) ); 
		   
  }
  else if ( udt::IsA< Scalar< float > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< float > >( Unknown ) );
  }
  else if ( udt::IsA< Scalar< int > >( Unknown ) )
  {
    op( *ret, udt::Cast< Scalar< int > >( Unknown ) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }

  return ret.release();
}

static datacondAPI::udt*
eval2(const Scalar< int >& Known, const datacondAPI::udt& Unknown)
{
  std::unique_ptr< Scalar< int > >	ret( Known.Clone() );

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // int = int + int
    op( *ret, udt::Cast< Scalar<int> >(Unknown) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static datacondAPI::udt*
eval2( const Sequence<float>& Known, const datacondAPI::udt& Unknown )
{
  std::unique_ptr< Sequence< float > >	ret( Known.Clone() );

  if (udt::IsA< Sequence<double> >(Unknown))
  {
    // sequence<float> += sequence<float>
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else if (udt::IsA< Sequence<float> >(Unknown))
  {
    // sequence<float> += sequence<float>
    op( *ret, udt::Cast< Sequence<float> >( Unknown ) );
  }
  else if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<float> += int
    op( *ret, (float)(udt::Cast< Scalar<int> >( Unknown ) ) );
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<float> += float
    op( *ret, (udt::Cast< Scalar<float> >( Unknown ) ) );
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<float> += double
    op( *ret, (float)(udt::Cast< Scalar<double> >( Unknown ) ) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static datacondAPI::udt*
eval2( const Sequence<complex< float> >& Known,
       const datacondAPI::udt& Unknown )
{
  std::unique_ptr< Sequence< complex< float > > >	ret( Known.Clone() );

  if (udt::IsA< Sequence<complex< double > > >(Unknown))
  {
    // sequence<float> += sequence< complex< double > >
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else if (udt::IsA< Sequence<complex< float > > >(Unknown))
  {
    // sequence<float> += sequence<complex< float> )
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else if (udt::IsA< Sequence<double> >(Unknown))
  {
    // sequence<float> += sequence<double>
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else if (udt::IsA< Sequence<float> >(Unknown))
  {
    // sequence<float> += sequence<float>
    op( *ret, udt::Cast< Sequence<float> >( Unknown ) );
  }
  else if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<float> += int
    op( *ret, (float)(udt::Cast< Scalar<int> >( Unknown ) ) );
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<float> += float
    op( *ret, (float)( (udt::Cast< Scalar<float> >( Unknown ) ) ) );
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<float> += double
    op( *ret, (float)(udt::Cast< Scalar<double> >( Unknown ) ) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static datacondAPI::udt*
eval2( const Sequence<complex< double > >& Known,
       const datacondAPI::udt& Unknown )
{
  std::unique_ptr< Sequence< complex< double > > >	ret( Known.Clone() );

  if (udt::IsA< Sequence<complex< double > > >(Unknown))
  {
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else if (udt::IsA< Sequence<double> >(Unknown))
  {
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else if (udt::IsA< Sequence<float> >(Unknown))
  {
    op( *ret, udt::Cast< Sequence<float> >( Unknown ) );
  }
  else if (udt::IsA< Scalar<int> >(Unknown))
  {
    op( *ret, (double)(udt::Cast< Scalar<int> >( Unknown ) ) );
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    op( *ret, (double)( (udt::Cast< Scalar<float> >( Unknown ) ) ) );
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    op( *ret, (double)(udt::Cast< Scalar<double> >( Unknown ) ) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static datacondAPI::udt*
eval2(const Sequence<double>& Known, const datacondAPI::udt& Unknown)
{
  std::unique_ptr< Sequence< double > >	ret( Known.Clone() );

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<double> += int
    op( *ret, (double)(udt::Cast< Scalar<int> >( Unknown ) ) );
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<double> += double
    op( *ret, (udt::Cast< Scalar<double> >( Unknown ) ) );
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<double> += float
    op( *ret, (double)(udt::Cast< Scalar<float> >( Unknown ) ) );
  }
  else if (udt::IsA< Sequence<double> >(Unknown))
  {
    // sequence<double> += sequence<double>
    op( *ret, udt::Cast< Sequence<double> >( Unknown ) );
  }
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}
