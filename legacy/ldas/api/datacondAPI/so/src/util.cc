#include "config.h"

// System Header Files
#include <cctype>

// Local Header Files
#include "util.hh"

void string2lower( std::string& s )
{
    std::string::iterator iter = s.begin();
    for ( std::string::size_type size = s.size(); size != 0; --size )
    {
        *iter = tolower( *iter );
        ++iter;
    }
}
