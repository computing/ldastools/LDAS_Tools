/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "config.h"

#include "WelchSpectrumUMD.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

double datacondAPI::WelchSpectrumUMD::GetBaseFrequency() const
{
    return m_frequency;
}

void datacondAPI::WelchSpectrumUMD::SetBaseFrequency(const double& frequency)
{
    m_frequency = frequency;
}

unsigned int datacondAPI::WelchSpectrumUMD::GetFFTLength() const
{
    return m_FFTLength;
}

void datacondAPI::WelchSpectrumUMD::SetFFTLength(const unsigned int& length)
{
    m_FFTLength = length;
}
                
unsigned int datacondAPI::WelchSpectrumUMD::GetFFTOverlap() const
{
    return m_FFTOverlap;
}

void datacondAPI::WelchSpectrumUMD::SetFFTOverlap(const unsigned int& overlap)
{
    m_FFTOverlap = overlap;
}
                
datacondAPI::WindowInfo datacondAPI::WelchSpectrumUMD::GetWindowInfo() const
{
    return m_WindowInfo;
}

void datacondAPI::WelchSpectrumUMD::SetWindowInfo(const WindowInfo& window)
{
    m_WindowInfo = window;
}

ILwd::LdasElement* datacondAPI::WelchSpectrumUMD::
ConvertToIlwd(  const CallChain& Chain,
		datacondAPI::udt::target_type Target ) const
{
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    try
    {
	switch ( Target )
	{
	case datacondAPI::udt::TARGET_GENERIC:
	    container->push_back( new ILwd::LdasArray<double>
				  (m_frequency, "BaseFrequency"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    container->push_back( new ILwd::LdasArray<unsigned int>
				  (m_FFTLength, "FFTLength"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    container->push_back( new ILwd::LdasArray<unsigned int>
				  (m_FFTOverlap, "FFTOverlap"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    container->push_back( new ILwd::LdasString(m_WindowInfo.name(),
						       "WindowName"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN  );
	    container->push_back( new ILwd::LdasArray<double>
				  (m_WindowInfo.parameter(), "WindowParameter"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    break;
	default:
	    throw datacondAPI::udt::
		BadTargetConversion(Target,
				    "datacondAPI::WelchSpectrumUMD");
	    break;
	}
    }
    catch ( ... )
    {
	delete container;
	throw;
    }

    return container;
}

datacondAPI::WelchSpectrumUMD::WelchSpectrumUMD()
    : m_channel(""), m_frequency(0.0), m_FFTLength(0), m_FFTOverlap(0)
{
}

datacondAPI::WelchSpectrumUMD::WelchSpectrumUMD(const WelchSpectrumUMD& value)
    : m_channel(value.m_channel), 
    m_frequency(value.m_frequency), 
    m_FFTLength(value.m_FFTLength),
    m_FFTOverlap(value.m_FFTOverlap),
    m_WindowInfo(value.m_WindowInfo)
{
}
                
datacondAPI::WelchSpectrumUMD::~WelchSpectrumUMD()
{
}
