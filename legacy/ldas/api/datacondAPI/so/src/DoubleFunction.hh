#ifndef DOUBLEFUNCTION_HH
#define DOUBLEFUNCTION_HH

#include "CallChain.hh"

class DoubleFunction : public CallChain::Function {
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  DoubleFunction();

  //: Get the name of the handled action
  //!return: The name of the handled action
  const std::string& GetName() const;

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  void Eval(CallChain* Chain,
            const CallChain::Params& Params,
            const std::string& Ret) const;
};

#endif // DOUBLEFUNCTION_HH
