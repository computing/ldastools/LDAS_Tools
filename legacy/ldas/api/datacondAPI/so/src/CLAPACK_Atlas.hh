#ifndef CLAPACK_ATLAS_HH
#define CLAPACK_ATLAS_HH

#ifdef ATLAS_ORDER

#define MATRIX_ORDER CblasColMajor

#if SIZEOF_INTEGER == 0
#undef SIZEOF_INTEGER
#define SIZEOF_INTEGER SIZEOF_INT
#else
#ifndef SIZEOF_INTEGER
#define SIZEOF_INTEGER SIZEOF_INT
#endif /* SIZEOF_INTEGER */
#endif /* SIZEOF_ITEGER == 0 */

#if SIZEOF_REAL == 0
#undef SIZEOF_REAL
#define SIZEOF_REAL SIZEOF_FLOAT
#else
#ifndef SIZEOF_REAL
#define SIZEOF_REAL SIZEOF_FLOAT
#endif /* SIZEOF_REAL */
#endif /* SIZEOF_REAL == 0 */

#if SIZEOF_DOUBLEREAL == 0
#undef SIZEOF_DOUBLEREAL
#define SIZEOF_DOUBLEREAL SIZEOF_DOUBLE
#else
#ifndef SIZEOF_DOUBLEREAL
#define SIZEOF_DOUBLEREAL SIZEOF_DOUBLE
#endif /* SIZEOF_DOUBLEREAL */
#endif /* SIZEOF_DOUBLEREAL == 0 */

#if SIZEOF_COMPLEX == 0
#undef SIZEOF_COMPLEX
#define SIZEOF_COMPLEX (2*SIZEOF_FLOAT)
#else
#ifndef SIZEOF_COMPLEX
#define SIZEOF_COMPLEX (2*SIZEOF_FLOAT)
#endif /* SIZEOF_COMPLEX */
#endif /* SIZEOF_COMPLEX == 0 */

#if SIZEOF_DOUBLECOMPLEX == 0
#undef SIZEOF_DOUBLECOMPLEX
#define SIZEOF_DOUBLECOMPLEX (2*SIZEOF_DOUBLE)
#else
#ifndef SIZEOF_DOUBLECOMPLEX
#define SIZEOF_DOUBLECOMPLEX (2*SIZEOF_DOUBLE)
#endif /* SIZEOF_DOUBLECOMPLEX */
#endif /* SIZEOF_DOUBLECOMPLEX == 0 */

#include "CLAPACK_Types.hh"

/*=======================================================================
                   F u n c t i o n   p r o t o t y p e s
  =======================================================================*/

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                           C B L A S
 *:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

#define GEMM_PROTOTYPE(TYPE_PTR,TYPE)					\
  const enum CBLAS_ORDER Order,						\
    const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, \
    integer M, integer N, integer K,					\
    TYPE Alpha, TYPE_PTR* A, integer lda,					\
    TYPE_PTR* B, integer ldb,						\
    TYPE Beta, TYPE_PTR* C, integer ldc

#define GEMM_CPLX_PROTOTYPE(TYPE_PTR,TYPE)					\
  const enum CBLAS_ORDER Order,						\
    const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, \
    integer M, integer N, integer K,					\
    TYPE Alpha, TYPE_PTR* A, integer lda,					\
    TYPE_PTR* B, integer ldb,						\
    TYPE Beta, TYPE_PTR* C, integer ldc

#define GEMM_(FUNC,TRANS_A,TRANS_B,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)	\
    (*FUNC)( MATRIX_ORDER, trans(TRANS_A), trans(TRANS_B), M, N, K,	\
	     ALPHA, A, LDA,						\
	     B, LDB,							\
	     BETA, C, LDC )

#define GEMM_CPLX_(FUNC,TRANS_A,TRANS_B,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)	\
    (*FUNC)( MATRIX_ORDER, trans(TRANS_A), trans(TRANS_B), M, N, K,	\
	     ALPHA, A, LDA,						\
	     B, LDB,							\
	     BETA, C, LDC )

/*-----------------------------------------------------------------------*/

#define GEMV_PROTOTYPE(TYPE_PTR,TYPE)				   \
  const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA, \
    integer M, integer N,					   \
    TYPE Alpha, TYPE_PTR* A, const integer lda,			   \
    TYPE_PTR* X, integer incX,					   \
    TYPE Beta, TYPE_PTR* Y, integer incY

#define GEMV_CPLX_PROTOTYPE(TYPE_PTR,TYPE)				   \
  const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA, \
    integer M, integer N,					   \
    TYPE_PTR* Alpha, TYPE_PTR* A, const integer lda,			   \
    TYPE_PTR* X, integer incX,					   \
    TYPE_PTR* Beta, TYPE_PTR* Y, integer incY

#define GEMV_(FUNC,TRANS_A,M,N,ALPHA,A,LDA,X,INC_X,BETA,Y,INC_Y)	\
  (*FUNC)( MATRIX_ORDER, trans( TRANS_A ), M, N,			\
	   ALPHA, A, LDA, X, INC_X,					\
	   BETA, Y, INC_Y )

#define GEMV_CPLX_(FUNC,TRANS_A,M,N,ALPHA,A,LDA,X,INC_X,BETA,Y,INC_Y)	\
  (*FUNC)( MATRIX_ORDER, trans( TRANS_A ), M, N,			\
	   ALPHA, A, LDA, X, INC_X,					\
	   BETA, Y, INC_Y )

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                           C L A P A C K
 *:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

/*-----------------------------------------------------------------------*
 *                             G E S V
 *-----------------------------------------------------------------------*/

#define GESV_PROTOTYPE(TYPE) \
  const enum CBLAS_ORDER Order, integer n, integer nrhs,	\
    TYPE* A, integer lda, integer *ipiv,			\
    TYPE* B, integer ldb

#define GESV_CPLX_PROTOTYPE(TYPE) \
  const enum CBLAS_ORDER Order, integer n, integer nrhs,	\
    TYPE* A, integer lda, integer *ipiv,			\
    TYPE* B, integer ldb

#define GESV_(FUNC,N,NRHS,A,LDA,IPIV,B,LDB,INFO)	\
  INFO = (*FUNC)( MATRIX_ORDER, N, NRHS,		\
		  A, LDA, IPIV,				\
		  B, LDB )

#define GESV_CPLX_(FUNC,N,NRHS,A,LDA,IPIV,B,LDB,INFO)	\
  INFO = (*FUNC)( MATRIX_ORDER, N, NRHS,		\
		  A, LDA, IPIV,				\
		  B, LDB )

#endif /* ATLAS_ORDER */

#endif /* CLAPACK_ATLAS_HH */
