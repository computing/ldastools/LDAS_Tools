/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef TIME_BOUNDED_FREQ_SEQUENCE_UDT__HH
#define TIME_BOUNDED_FREQ_SEQUENCE_UDT__HH

// $Id: TimeBoundedFreqSequenceUDT.hh,v 1.3 2003/02/28 00:57:19 emaros Exp $

#if HAVE_LDAS_PACKAGE_ILWD
#include <ilwd/ldascontainer.hh>
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "FrequencySequenceUDT.hh"
#include "TimeBoundedFreqSequenceUMD.hh"

namespace datacondAPI {

  template<class T>
  class TimeBoundedFreqSequence: public FrequencySequence<T>,
                                 public TimeBoundedFreqSequenceMetaData {
  public:
    //: Default Constructor
    TimeBoundedFreqSequence();

    //: Copy Constructor
    TimeBoundedFreqSequence( const TimeBoundedFreqSequence< T >& Source );

    //: Construct allowing specification of data, frequencies
    //!param: Sequence<DataType_> - elements of series
    //!param: f0 - frequency of first element in series
    //!param: df - increment in frequency between consequtive elements
    //!param: Start - start of time interval
    //!param: End - end of time interval
    TimeBoundedFreqSequence( const Sequence< T >& DataPoints, 
			     const double& f0, 
			     const double& df,
			     const General::GPSTime& Start,
			     const General::GPSTime& End);

    //: Create duplicate of *this on heap
    //!return: TimeBoundedFreqSequence<T>* - duplicate of *this, allocated on
    //+heap
    virtual TimeBoundedFreqSequence< T >* Clone() const;
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - 
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasContainer* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasContainer*
    ConvertToIlwd(const CallChain& Chain,
		  udt::target_type Target = udt::TARGET_GENERIC ) const;
  };

} // namespace datacondAPI

#endif /* TIME_BOUNDED_FREQ_SEQUENCE_UDT__HH */
