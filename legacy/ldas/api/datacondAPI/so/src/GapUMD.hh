#ifndef GAP_META_DATA_HH
#define	GAP_META_DATA_HH

#include <vector>

#include "general/types.hh"
#include "general/gpstime.hh"

#include "GapFill.hh"

namespace ILwd
{
  class LdasContainer;
}

namespace ILwdFCS
{
  class FrHistory;
}

namespace datacondAPI
{
  template < class Unit_type >
  class GapMetaData
  {
  public:
    //: Structure to hold gap information.
    //
    // Each Gap_type contains an exclusive pair of data points to represent
    // the gap. One element contains the last good piece of data set and
    // the second contains the first good piece of daa of the next data
    // set. The gap is then represented by (first, second).
    // <p>
    // The start time must be calculated in one of two ways depending on
    // where the gap resides in relationship to the data.
    // If the gap is before any data, then the start time is the start
    // time of the sample. Otherwise, the start time is end time of the
    // preceeding data plus the delta time.
    // <p>
    // The stop time must be calculated in one of two ways depending on
    // where the gap resides in relationship to the data.
    // If the gap extends to the end of the sample, then the stop time is the
    // stop time of the sample.
    // Otherwise, the stop time is start time of the
    // preceeding data minus the delta time.
    struct Gap_type {
      //: Start of gap
      Unit_type  	start;
      //: End of gap
      Unit_type	end;

      //: Default constructor
      Gap_type( );
      //: Copy Constructor
      Gap_type( const Gap_type& Source );
      //: Constructor
      //!param: const Unit_type& Start - Start of Gap
      //!param: const Unit_type& End - End of Gap
      Gap_type( const Unit_type& Start, const Unit_type& End );
    };

    // A pair of indices used to represent the start and stop of a gap.
    // An example of use would be:
    // <verbatim>
    // std::valarray<int>	a(20);
    // GapInterval_type		i(5,10);
    // for( unsigned int x = i.start; x < stop; x++ )
    // {
    //   a[x] = 0;
    // }
    // </verbatim>
    struct GapInterval_type {
      INT_4U	start;
      INT_4U	stop;

      GapInterval_type( );
      GapInterval_type( const GapInterval_type& Source );
      GapInterval_type( const INT_4U Start, const INT_4U Stop);
    };

    typedef std::vector<GapInterval_type> Transitions_type;

    //: Specify type for const_iterator
    typedef typename std::vector<Gap_type>::const_iterator const_iterator;

    virtual ~GapMetaData();

    //: Set the filling method
    void GapSetFillMethod(const GapFill::FillMethod_type fill_method);

    //: Get the filling method
    GapFill::FillMethod_type GapGetFillMethod() const;

    //: Append information about the data.
    //
    //!param: const Unit_type& Start - start of the data.
    //!param: const Unit_type& Stop - stop of the data.
    void GapAppendDataRange( const Unit_type& Start, const Unit_type& Stop );

    //: Append information about a gap.
    //
    //!param: const Unit_type& Start - start of the gap.
    //!param: const Unit_type& Stop - stop of the gap.
    void GapAppendGapRange( const Unit_type& Start, const Unit_type& Stop );

    //: Calculate where the gaps exist
    void CalcTransitions( Transitions_type& Gaps ) const;

    //: Calculate where the data exist
    void CalcTransitionsOfData( Transitions_type& Data ) const;

    //: Reset list of gaps
    //!param: bool AllGap - true if the range of data being represented
    //+		by the class is to be set as all gap, false otherwise.
    void GapReset( bool AllGap = false );

    //: Store the metadata in an ilwd
    void Store( ILwd::LdasContainer& Container ) const;

    //: Store the metadata in a FrHistory structure
    void Store( ILwdFCS::FrHistory& History ) const;

    virtual Unit_type GapGetDataBegin() const = 0;
    virtual Unit_type GapGetDataEnd() const = 0;
    virtual unsigned int GapGetDataSize() const = 0;
    virtual double GetStepSize() const = 0;

  protected:
    //: Specify type for iterator
    typedef typename std::vector<Gap_type>::iterator	  iterator;
    typedef typename std::vector<Gap_type>::reverse_iterator reverse_iterator;

    GapMetaData();
    
  private:
    std::vector<Gap_type>		m_gaps;
    GapFill::FillMethod_type	m_fill_method;

    //:TODO: May have to keep the sample rate of when gaps were calculated
    //:TODO:	so as to do the right thing when down sampling.
    
  }; // class: GapMetaData
} // namespace: datacondAPI

#endif /* GAP_META_DATA_HH */
