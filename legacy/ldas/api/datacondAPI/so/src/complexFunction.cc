/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "datacondAPI/config.h"

#include "Complex.hh"
#include "TimeSeries.hh"
#include "ScalarUDT.hh"

#include "complexFunction.hh"

//
// Usage:
//
//   z = complex(x [, y])
//
// where
//
//   x: derived from real Sequence or Scalar
//   y: derived from real Sequence or Scalar
//   z: derived from complex Sequence or Scalar
//

using namespace datacondAPI;
using std::complex;

namespace {

    complexFunction static_complexFunction;


    Scalar<complex<double> >*
    constructComplex(const Scalar<double>& realPart,
                     const Scalar<double>& imagPart)
    {
        // Make a complex "clone" of realPart, to set meta-data
        Scalar<complex<double> >* const ret = Complex(realPart);
        
        const complex<double> val(realPart.GetValue(), imagPart.GetValue());

        ret->SetValue(val);

        return ret;
    }

    template<class T> inline
    Sequence<complex<T> >*
    constructComplex(const Scalar<double>& realPart,
                     const Sequence<T>& imagPart)
    {
        // Make a complex "clone" of imagPart, to set meta-data
        Sequence<complex<T> >* const ret = Complex(imagPart);
        
        // Set ret to i*imagPart (vector operation)
        *ret *= complex<T>(0, 1);

        // Add on real (Scalar) part
	// downcast for type mismatch: complex< float > += double
        *ret += complex<T>(static_cast< T >( realPart.GetValue() ) );

        return ret;
    }

    template<class T>
    inline Sequence<complex<T> >*
    constructComplex(const Sequence<T>& realPart,
                     const Scalar<double>& imagPart)
    {
        // Make a complex "clone" of realPart, to set meta-data
        Sequence<complex<T> >* const ret = Complex(realPart);
        
        // Add on imaginary (Scalar) part
        *ret += complex<T>(0, 1)*T(imagPart.GetValue());

        return ret;
    }

    template<class T>
    inline Sequence<complex<T> >*
    constructComplex(const Sequence<T>& realPart,
                     const Sequence<T>& imagPart)
    {
        if (realPart.size() != imagPart.size())
        {
            throw CallChain::BadArgument("complex", 0, "size mismatch",
                               "real and imaginary parts must have same size");
        }

        // Make a complex "clone" of realPart, to set meta-data
        Sequence<complex<T> >* const ret = Complex(realPart);
        
        const complex<T> i = complex<T>(0, 1);

        for (size_t k = 0; k < ret->size(); ++k)
        {
            (*ret)[k] += i*imagPart[k];
        }

        return ret;
    }

    udt*
    dispatchImagPart( const Scalar<double>& realPart,
		      const udt& imagPart )
    {
        udt* ret = 0;

        if (const Scalar<double>* const p
            = dynamic_cast<const Scalar<double>*>(&imagPart))
        {
            ret = constructComplex(realPart, *p);
        }
        else if (const Sequence<float>* const p
                 = dynamic_cast<const Sequence<float>*>(&imagPart))
        {
            ret = constructComplex(realPart, *p);
        }
        else if (const Sequence<double>* const p
                 = dynamic_cast<const Sequence<double>*>(&imagPart))
        {
            ret = constructComplex(realPart, *p);
        }
        else
        {
            throw CallChain::BadArgument("complex",
                                       1,
                                       TypeInfoTable.GetName(typeid(imagPart)),
                                       "class derived from Scalar<double>, "
                                       "Sequence<float> or Sequence<double>");
        }

        return ret;
    }

    template<class T>
    inline udt*
    dispatchImagPart( const Sequence<T>& realPart,
		      const udt& imagPart )
    {
        udt* ret = 0;

        if (const Scalar<double>* const p
            = dynamic_cast<const Scalar<double>*>(&imagPart))
        {
            ret = constructComplex(realPart, *p);
        }
        else if (const Sequence<T>* const p
                 = dynamic_cast<const Sequence<T>*>(&imagPart))
        {
            ret = constructComplex(realPart, *p);
        }
        else
        {
            throw CallChain::BadArgument("complex",
                                   1,
                                   TypeInfoTable.GetName(typeid(imagPart)),
                                   "class derived from Scalar<double> or " + 
                                   TypeInfoTable.GetName(typeid(Sequence<T>)));
        }

        return ret;
    }

    udt* dispatchRealPart(const udt& realPart,
                          const udt& imagPart)
    {
        udt* ret = 0;

        if (const Scalar<double>* const p
            = dynamic_cast<const Scalar<double>*>(&realPart))
        {
            ret = dispatchImagPart(*p, imagPart);
        }
        else if (const Sequence<float>* const p
                 = dynamic_cast<const Sequence<float>*>(&realPart))
        {
            ret = dispatchImagPart(*p, imagPart);
        }
        else if (const Sequence<double>* const p
                 = dynamic_cast<const Sequence<double>*>(&realPart))
        {
            ret = dispatchImagPart(*p, imagPart);
        }
        else
        {
            throw CallChain::BadArgument("complex",
                                       0,
                                       TypeInfoTable.GetName(typeid(realPart)),
                                       "class derived from Scalar<double>, "
                                       "Sequence<float> or Sequence<double>");
        }

        return ret;
    }


}

const std::string& complexFunction::GetName(void) const
{
    static const std::string name("complex");

    return name;
}

complexFunction::complexFunction()
    : Function(complexFunction::GetName())
{}

void complexFunction::Eval(CallChain* chain,
			   const CallChain::Params& params,
			   const std::string& ret) const
{
    udt* result = 0;

    if (params.size() == 1)
    {
        const udt* const realPart = chain->GetSymbol(params[0]);

        result = Complex(*realPart);

        result->SetName("complex(" + realPart->name() + ")");
    }
    else if (params.size() == 2)
    {
        const udt* const realPart = chain->GetSymbol(params[0]);
        const udt* const imagPart = chain->GetSymbol(params[1]);
        
        result = dispatchRealPart(*realPart, *imagPart);

        result->SetName("complex(" + realPart->name() + ","
                        + imagPart->name() + ")");
    }
    else
    {
	throw CallChain::BadArgumentCount(GetName(), "1-2", params.size());
    }

    chain->ResetOrAddSymbol(ret, result);  
}

