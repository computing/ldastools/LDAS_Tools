/* -*- mode: c++; c-basic-offset: 4; -*- */

// $Id: StatisticsFunction.cc,v 1.29 2006/11/27 21:32:14 emaros Exp $
//
// statistics actions

#include "config.h"

#include "StatisticsFunction.hh"

#include "Statistics.hh"
#include "StatsUDT.hh"
#include "ScalarUDT.hh"

using namespace datacondAPI;
using std::complex;

namespace {

    // A collection of "function objects", one for each of the
    // Statistics actions

    class Size {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            datacondAPI::Statistics stats;
            return new Scalar<int>(stats.size(x));
        }
    };

    class Sum {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.sum(x));
        }
    };

    class Min {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.min(x));
        }
    };

    class Max {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.max(x));
        }
    };

    class Mean {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.mean(x));
        }
    };

    class Rms {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.rms(x));
        }
    };

    class Variance {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.variance(x));
        }
    };

    class Skewness {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.skewness(x));
        }
    };

    class Kurtosis {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            return new Scalar<TOut>(stats.kurtosis(x));
        }
    };

    class All {
    public:
        template<class TIn>
        udt* operator()(const Sequence<TIn>& x) const
        {
            typedef typename StatisticsTraits<TIn>::TOut TOut;
            datacondAPI::Statistics stats;
            std::unique_ptr<Stats> allStats(new Stats());
            stats.all(*allStats, x);
            return allStats.release();
        }
    };

    // Static instances of the functions
    const SizeFunction staticSizeFunction;
    const SumFunction staticSumFunction;
    const MinFunction staticMinFunction;
    const MaxFunction staticMaxFunction;
    const MeanFunction staticMeanFunction;
    const RmsFunction staticRmsFunction;
    const VarianceFunction staticVarianceFunction;
    const SkewnessFunction staticSkewnessFunction;
    const KurtosisFunction staticKurtosisFunction;
    const AllFunction staticAllFunction;
}

template<class Op>
void StatsFunctionBase::evaluate(CallChain* const chain,
                                 const CallChain::Params& params,
                                 const std::string& ret,
                                 const Op& op) const
{
    udt* result = 0;

    if (params.size() != 1)
    {
        throw CallChain::BadArgumentCount(GetName(), 1, params.size());
    }

    const udt* const xUdt = chain->GetSymbol(params[0]);

    if (const Sequence<float>* const p
        = dynamic_cast<const Sequence<float>*>(xUdt))
    {
        result = op(*p);
    }
    else if (const Sequence<double>* const p
             = dynamic_cast<const Sequence<double>*>(xUdt))
    {
        result = op(*p);
    }
    else if (const Sequence<complex<float> >* const p
             = dynamic_cast<const Sequence<complex<float> >*>(xUdt))
    {
        result = op(*p);
    }
    else if (const Sequence<complex<double> >* const p
             = dynamic_cast<const Sequence<complex<double> >*>(xUdt))
    {
        result = op(*p);
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     TypeInfoTable.GetName(typeid(*xUdt)),
                                     "object derived from Sequence");
    }

    chain->ResetOrAddSymbol(ret, result);
}

//----------------------------------------------------------------------------
//
// size() function
//
SizeFunction::SizeFunction()
    : StatsFunctionBase(SizeFunction::GetName()) { }

const std::string& SizeFunction::GetName() const
{
    static const std::string name("size");
    return name;
}

void SizeFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Size());
}

//----------------------------------------------------------------------------
//
// sum() function
//
SumFunction::SumFunction()
    : StatsFunctionBase(SumFunction::GetName()) { }

const std::string& SumFunction::GetName() const
{
    static const std::string name("sum");
    return name;
}

void SumFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Sum());
}

//----------------------------------------------------------------------------
//
// min() function
//
MinFunction::MinFunction()
    : StatsFunctionBase(MinFunction::GetName()) { }

const std::string& MinFunction::GetName() const
{
    static const std::string name("min");
    return name;
}

void MinFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Min());
}

//----------------------------------------------------------------------------
//
// max() function
//
MaxFunction::MaxFunction()
    : StatsFunctionBase(MaxFunction::GetName()) { }

const std::string& MaxFunction::GetName() const
{
    static const std::string name("max");
    return name;
}

void MaxFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Max());
}

//----------------------------------------------------------------------------
//
// mean() function
//
MeanFunction::MeanFunction()
    : StatsFunctionBase(MeanFunction::GetName()) { }

const std::string& MeanFunction::GetName() const
{
    static const std::string name("mean");
    return name;
}

void MeanFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Mean());
}

//----------------------------------------------------------------------------
//
// rms() function
//
RmsFunction::RmsFunction()
    : StatsFunctionBase(RmsFunction::GetName()) { }

const std::string& RmsFunction::GetName() const
{
    static const std::string name("rms");
    return name;
}

void RmsFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Rms());
}

//----------------------------------------------------------------------------
//
// variance() function
//
VarianceFunction::VarianceFunction()
    : StatsFunctionBase(VarianceFunction::GetName()) { }

const std::string& VarianceFunction::GetName() const
{
    static const std::string name("variance");
    return name;
}

void VarianceFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Variance());
}

//----------------------------------------------------------------------------
//
// skewness() function
//
SkewnessFunction::SkewnessFunction()
    : StatsFunctionBase(SkewnessFunction::GetName()) { }

const std::string& SkewnessFunction::GetName() const
{
    static const std::string name("skewness");
    return name;
}

void SkewnessFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Skewness());
}

//----------------------------------------------------------------------------
//
// kurtosis() function
//
KurtosisFunction::KurtosisFunction()
    : StatsFunctionBase(KurtosisFunction::GetName()) { }

const std::string& KurtosisFunction::GetName() const
{
    static const std::string name("kurtosis");
    return name;
}

void KurtosisFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, Kurtosis());
}

//----------------------------------------------------------------------------
//
// All() function
//
AllFunction::AllFunction()
    : StatsFunctionBase(AllFunction::GetName()) { }

const std::string& AllFunction::GetName() const
{
    static const std::string name("all");
    return name;
}

void AllFunction::Eval(CallChain* chain,
                        const CallChain::Params& params,
                        const std::string& ret) const
{
    evaluate(chain, params, ret, All());
}

#undef INSTANTIATE
#define INSTANTIATE(Op) \
template \
void StatsFunctionBase::evaluate< Op >( CallChain* const chain, \
					const CallChain::Params& params, \
					const std::string& ret, \
					const Op& op ) const

INSTANTIATE( Size );
INSTANTIATE( Sum );
INSTANTIATE( Min );
INSTANTIATE( Max );
INSTANTIATE( Mean );
INSTANTIATE( Rms );
INSTANTIATE( Variance );
INSTANTIATE( Skewness );
INSTANTIATE( Kurtosis );
INSTANTIATE( All );

#undef INSTANTIATE

