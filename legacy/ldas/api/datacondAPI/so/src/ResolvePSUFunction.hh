#ifndef _RESOLVEPSUFUNCTION_HH_
#define _RESOLVEPSUFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI {
namespace psu
{

class ResolvePSUFunction : public CallChain::Function
{
public:
  ResolvePSUFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain, const CallChain::Params& Params,
		    const std::string& Ret) const;

};

}//end namespace psu
}//end namespace datacondAPI

#endif //_RESOLVEPSUFUNCTION_HH_
