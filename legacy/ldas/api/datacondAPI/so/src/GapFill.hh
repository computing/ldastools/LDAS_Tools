#ifndef GAPFILL_HH
#define GAPFILL_HH

/// This file contains a set prototypes for Gap filling methods

namespace datacondAPI
{
    class udt;
    template<class T> class TimeSeries;

    class GapFill
    {
    public:
        //: Enumerated type specifying how the gaps have been filled
        typedef enum {
            ZERO,               // Gap has been zero filled
            AVERAGE,            // Gap has been filled with the average
            LINEAR              // Gap has been filled with a linear fit
        } FillMethod_type;

        GapFill(const FillMethod_type fill = ZERO);
      
        void apply(udt*& out, const udt& in) const;

        template<class T>
        void apply(TimeSeries<T>& out, const TimeSeries<T>& in) const;

    private:
        template<class T>
        void dispatch(udt*& out, const T& in) const;

        FillMethod_type m_fill;
    };

} // namespace datacondAPI

#endif  // GAPFILL_HH
