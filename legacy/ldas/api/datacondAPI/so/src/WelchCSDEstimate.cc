#include "config.h"

#include "general/Memory.hh"

#include <filters/FilterConstants.hh>

#include "fft.hh"
#include "DFTUDT.hh"
#include "ScalarUDT.hh"

#include "WelchCSDSpectrumUDT.hh"
#include "WelchSpectrumUDT.hh"

#include "WelchCSDEstimate.hh"
#include "TimeSeries.hh"

namespace {
    
    template<class In, class Out>
    Out addNorm(In first, In last, Out res)
    {
	while (first != last)
	{
	    *res++ += norm(*first++);
	}

	return res;
    }

}

datacondAPI::WelchCSDEstimate::
WelchCSDEstimate(const size_t           fftLength,
            	 const size_t           overlapLength,
	         const Filters::Window& window,
	         const DetrendMethod    detrendMethod)
    : CSDEstimate(detrendMethod),
      m_fftLength(0),
      m_overlapLength(0),
      m_pwindow(window.Clone())
{
    // call set functions to make sure parameters are valid
    set_fftLength(fftLength);
    set_overlapLength(overlapLength);
}


datacondAPI::WelchCSDEstimate::
WelchCSDEstimate(const WelchCSDEstimate& r)
    : CSDEstimate(r),
      m_fftLength(r.m_fftLength),
      m_overlapLength(r.m_overlapLength),
      m_pwindow(r.m_pwindow->Clone())
{
}


datacondAPI::WelchCSDEstimate::
~WelchCSDEstimate()
{
}


const datacondAPI::WelchCSDEstimate& 
datacondAPI::WelchCSDEstimate::
operator=(const WelchCSDEstimate& r) 
{
    if (this != &r)
    {
	// take care of base class (see Holub, p.99)
	CSDEstimate::operator=(r);

	// do the copying here
	m_fftLength     = r.m_fftLength;
	m_overlapLength = r.m_overlapLength;

	m_pwindow.reset(r.m_pwindow->Clone());
    }

    return *this;
}


void
datacondAPI::WelchCSDEstimate::
set_fftLength(const size_t fftLength)
{
    if ( (fftLength <= 0) || 
	 (fftLength >  MaximumFFTLength) ||
	 (fftLength >  Filters::MaximumWindowLength) ||
	 (fftLength <= m_overlapLength) )
    {
	throw std::invalid_argument("WelchCSDEstimate::set_fftLength() "
				    "invalid fft length");
    }

    m_fftLength = fftLength;
}


void
datacondAPI::WelchCSDEstimate::
set_fftLength(const udt& fftLength)
{
    if (udt::IsA<Scalar<int> >(fftLength))
    {
        const int len = udt::Cast<Scalar<int> >(fftLength).GetValue();
        if (len <= 0)
        {
            throw std::invalid_argument("WelchCSDEstimate::set_fftLength() "
                                        "argument must be > 0");
            
        }
        else
        {
            set_fftLength(len);
        }
    }
    else
    {
	throw std::invalid_argument("WelchCSDEstimate::set_fftLength() "
				    "argument must be Scalar<int>");
    }
}


void
datacondAPI::WelchCSDEstimate::
set_overlapLength(const size_t overlapLength)
{
    if (overlapLength >= m_fftLength) 
    {
	throw std::invalid_argument("WelchCSDEstimate::set_overlapLength() "
				    "invalid overlap length");
    }

    m_overlapLength = overlapLength;
}


void
datacondAPI::WelchCSDEstimate::
set_overlapLength(const udt& overlapLength)
{
    if (udt::IsA<Scalar<int> >(overlapLength))
    {
        const int overlap = udt::Cast<Scalar<int> >(overlapLength).GetValue();
        if (overlap < 0)
        {
           throw std::invalid_argument("WelchCSDEstimate::set_overlapLength() "
                                       "overlap < 0");
            
        }
        else
        {
            set_overlapLength(overlap);
        }
    }
    else
    {
	throw std::invalid_argument("WelchCSDEstimate::set_overlapLength() "
				    "argument must be Scalar<int>");
    }
}


void
datacondAPI::WelchCSDEstimate::
set_window(const Filters::Window& window)
{
    m_pwindow.reset(window.Clone());
}


void
datacondAPI::WelchCSDEstimate::
set_window(const udt& window)
{
    if (const WindowUDT* const p = dynamic_cast<const WindowUDT*>(&window))
    {
	set_window(p->getWindow());
    }
    else
    {
	throw std::invalid_argument("WelchCSDEstimate::set_window() "
				    "argument must be a WindowUDT");
    }
}


std::string 
datacondAPI::WelchCSDEstimate::
what() const
{
    return std::string("Welch CSD Estimate");
}


size_t 
datacondAPI::WelchCSDEstimate::
fftLength() const
{
    return m_fftLength;
}


size_t
datacondAPI::WelchCSDEstimate::
overlapLength() const
{
    return m_overlapLength;
}


datacondAPI::WindowInfo
datacondAPI::WelchCSDEstimate::
windowType() const
{
    return WindowInfo(m_pwindow->name(), m_pwindow->param());
}


namespace datacondAPI
{
  //
  // CSD of two real sequences is single-sided
  //
  template<>
  void
  WelchCSDEstimate::
  apply(WelchCSDSpectrum<std::complex<float> >& out, 
	const Sequence<float>& xIn,  
	const Sequence<float>& yIn )
  {
	
    // Check that lengths of input data are equal.
    if ( xIn.size() != yIn.size() )
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "input sequences must have "
				  "equal lengths");
    }
    
    // Assign common length of input data
    const size_t dataLength = xIn.size();

    // Check that input data length is > 0
    if (dataLength == 0) 
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "on zero-length Sequence(s)");
    }

    // Check that the data length is at least as long as the FFT length
    if (dataLength < m_fftLength)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Input Sequence length is less than FFT length");
    }
    
    //
    // For the segments to cover the input exactly, we need to have
    //
    //   dataLength = overlapLength + p*(fftLength - overlapLength)
    //
    // where p is an integer. In other words,
    //
    //   (dataLength - overlapLength) mod (fftLength - overlapLength) == 0
    //
    // Check that the overlapping segments will divide up the input
    // sequence evenly
    if ((dataLength - m_overlapLength) % (m_fftLength - m_overlapLength) != 0)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Segments do not divide input evenly");
    }
    
    const size_t csd_size = m_fftLength/2 + 1;

    // resize output arrays to hold csd estimate
    if (out.size() != csd_size)
    {
      out.resize(csd_size, 0.0);
    }

    // calculate number of sections to average
    const int numSections = (dataLength - m_overlapLength)/
      (m_fftLength - m_overlapLength);
                         
    // Sequences to hold detrended and windowed data
    Sequence<float> segment(m_fftLength);

    // Sequences to hold FFTs
    DFT<std::complex<float> > fft_x_segment(m_fftLength);
    DFT<std::complex<float> > fft_y_segment(m_fftLength);

    // need FFT object to calculate the dft
    FFT fft;

    // loop over different sections
    size_t index = 0;

    for (int i = 0; i < numSections; ++i) 
    {
      segment = xIn[std::slice(index, m_fftLength, 1)];
      detrend(segment);
      m_pwindow->apply(segment, segment);
      fft.apply(fft_x_segment, segment);

      segment = yIn[std::slice(index, m_fftLength, 1)];
      detrend(segment);
      m_pwindow->apply(segment, segment);
      fft.apply(fft_y_segment, segment);

      // form product conj(fft_x)*fft_y for each section of data,	
      // then sum up estimates for each section
      for (size_t k = 0; k < csd_size; ++k)
      {
	out[k] += conj(fft_x_segment[k])*fft_y_segment[k];
      }

      // get index ready for next segment
      index += (m_fftLength - m_overlapLength); 
    }

    // FrequencySequence metadata

    // Get the sampling rate and units
    // :KLUDGE: This is a big fat hairy kludge, should be done
    // by applying to TimeSeries then by applying to Sequence
    double sampleRate = 0.0;
    if (const TimeSeries<float>* const p
	= dynamic_cast<const TimeSeries<float>*>(&xIn))
    {
      sampleRate = p->GetSampleRate();
    }
    else if (const TimeSeries<float>* const p
	     = dynamic_cast<const TimeSeries<float>*>(&yIn))
    {
      sampleRate = p->GetSampleRate();
    }
    else
    {
      // it's just a Sequence after all
      sampleRate = 1.0;
    }

    // 
    // Normalize csd - the definition that has been adopted is so that
    //
    //                      2*delta-t
    //    OneSidedPSD[k] =  --------- < DFT(h)[k] * conj( DFT(h)[k] ) >
    //                          N
    //
    // where < > means ensemble average.
    //
    // The normalisation has several factors. The overall scaling is the
    // inverse of the following product:
    //
    // - square of the window's RMS 
    // - number of sections (to average the sum of individual fft*conf(fft)'s)
    // - FFT length
    // - inverse of delta-t, that is, the sampling rate.
    //
    
    // Square of window's RMS
    const double rms = m_pwindow->rms();
    const double factor = 2.0/(sampleRate*m_fftLength*rms*rms*numSections);
   
    // To make compiler happy: create explicit complex<float>:
    std::complex< float > complex_factor( static_cast< float >( factor ) );
    out *= complex_factor;

    // The DC element needs to halved with respect to the other elements
    out[0] /= 2;

    // The Nyquist element needs to halved with respect to the other elements
    // if the length is even
    if ((m_fftLength % 2) == 0)
    {
      out[m_fftLength/2] /= 2;
    }

    const double FrequencyBase = 0.0;

    out.SetFrequencyBase(FrequencyBase);
    out.SetFrequencyDelta(sampleRate/m_fftLength);

    // CSDSpectrum metadata

    out.SetDetrendMethod(detrendMethod());
    out.SetEstimator(what());

    // WelchCSDEstimate metadata

    out.SetFFTLength(fftLength());
    out.SetFFTOverlap(overlapLength());
    out.SetWindowInfo(windowType());

    out.SetNameOfChannel1(xIn.name());
    out.SetNameOfChannel2(yIn.name());
    out.SetName("csd(" + xIn.name() + "," + yIn.name() + ")");

    if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&xIn) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
    else if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&yIn) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
  }


  //
  // CSD of two real sequences is single-sided
  //
  template<>
  void
  WelchCSDEstimate::
  apply(WelchCSDSpectrum<std::complex<double> >& out, 
	const Sequence<double>& xIn,  
	const Sequence<double>& yIn )
  {
	
    // Check that lengths of input data are equal.
    if ( xIn.size() != yIn.size() )
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "input sequences must have "
				  "equal lengths");
    }
    
    // Assign common length of input data
    const size_t dataLength = xIn.size();

    // Check that input data length is > 0
    if (dataLength == 0) 
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "on zero-length Sequence(s)");
    }

    // Check that the data length is at least as long as the FFT length
    if (dataLength < m_fftLength)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Input Sequence length is less than FFT length");
    }
    
    // Check that the overlapping segments will divide up the input
    // sequence evenly
    if ((dataLength - m_overlapLength) % (m_fftLength - m_overlapLength) != 0)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Segments do not divide input evenly");
    }

    const size_t csd_size = m_fftLength/2 + 1;

    // resize output arrays to hold csd estimate
    if (out.size() != csd_size)
    {
      out.resize(csd_size, 0.0);
    }

    // calculate number of sections to average
    const int numSections = (dataLength - m_overlapLength)/
      (m_fftLength - m_overlapLength);
                         
    // Sequences to hold detrended, windowed, and ffted data
    Sequence<double> segment(m_fftLength);

    DFT<std::complex<double> > fft_x_segment(m_fftLength);
    DFT<std::complex<double> > fft_y_segment(m_fftLength);

    // need FFT object to calculate the dft
    FFT fft;

    // loop over different sections
    size_t index = 0;

    for (int i = 0; i < numSections; i++) 
    {
      segment = xIn[std::slice(index, m_fftLength, 1)];
      detrend(segment);
      m_pwindow->apply(segment, segment);
      fft.apply(fft_x_segment, segment);

      segment = yIn[std::slice(index, m_fftLength, 1)];
      detrend(segment);
      m_pwindow->apply(segment, segment);
      fft.apply(fft_y_segment, segment);

      // form product conj(fft_x)*fft_y for each section of data,	
      // then sum up estimates for each section
      for (unsigned int k = 0; k < csd_size; ++k)
      {
	out[k] += conj(fft_x_segment[k])*fft_y_segment[k];
      }

      // get index ready for next segment
      index += (m_fftLength - m_overlapLength); 
    }

    // FrequencySequence metadata

    // Get the sampling rate and units
    // :KLUDGE: This is a big fat hairy kludge, should be done
    // by applying to TimeSeries then by applying to Sequence
    double sampleRate = 0.0;
    if (const TimeSeries<double>* const p
	= dynamic_cast<const TimeSeries<double>*>(&xIn))
    {
      sampleRate = p->GetSampleRate();
    }
    else if (const TimeSeries<double>* const p
	     = dynamic_cast<const TimeSeries<double>*>(&yIn))
    {
      sampleRate = p->GetSampleRate();
    }
    else
    {
      // it's just a Sequence after all
      sampleRate = 1.0;
    }

    // 
    // Normalize csd - the definition that has been adopted is so that
    //
    //                      2*delta-t
    //    OneSidedPSD[k] =  --------- < DFT(h)[k] * conj( DFT(h)[k] ) >
    //                          N
    //
    // where < > means ensemble average.
    //
    // The normalisation has several factors. The overall scaling is the
    // inverse of the following product:
    //
    // - square of the window's RMS 
    // - number of sections (to average the sum of individual fft*conf(fft)'s)
    // - FFT length
    // - inverse of delta-t, that is, the sampling rate.
    //
    
    // Square of window's RMS
    const double rms = m_pwindow->rms();
    const double factor = 2.0/(sampleRate*m_fftLength*rms*rms*numSections);

    std::complex< double > complex_factor( factor );
    out *= complex_factor;

    // The DC element needs to halved with respect to the other elements
    out[0] /= 2;

    // The Nyquist element needs to halved with respect to the other elements
    // if the length is even
    if ((m_fftLength % 2) == 0)
    {
      out[m_fftLength/2] /= 2;
    }

    const double FrequencyBase = 0.0;

    out.SetFrequencyBase(FrequencyBase);
    out.SetFrequencyDelta(sampleRate/m_fftLength);

    // CSDSpectrum metadata

    out.SetDetrendMethod(detrendMethod());
    out.SetEstimator(what());

    // WelchCSDEstimate metadata

    out.SetFFTLength(fftLength());
    out.SetFFTOverlap(overlapLength());
    out.SetWindowInfo(windowType());

    out.SetNameOfChannel1(xIn.name());
    out.SetNameOfChannel2(yIn.name());
    out.SetName("csd(" + xIn.name() + "," + yIn.name() + ")");

    if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&xIn) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
    else if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&yIn) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
  }

  //
  // All the other combinations - at least one input is complex and the
  // output is a double-sided CSD
  //
  template<class TOut, class TXIn, class TYIn>
  void
  WelchCSDEstimate::
  apply(WelchCSDSpectrum<std::complex<TOut> >& out, 
	const Sequence<TXIn>& xIn,  
	const Sequence<TYIn>& yIn )
  {
	
    // Check that lengths of input data are equal.
    if ( xIn.size() != yIn.size() )
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "input sequences must have "
				  "equal lengths");
    }
    
    // Assign common length of input data
    const size_t dataLength = xIn.size();

    // Check that input data length is > 0
    if (dataLength == 0) 
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "on zero-length Sequence(s)");
    }

    // Check that the data length is at least as long as the FFT length
    if (dataLength < m_fftLength)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Input Sequence length is less than FFT length");
    }
    
    // Check that the overlapping segments will divide up the input
    // sequence evenly
    if ((dataLength - m_overlapLength) % (m_fftLength - m_overlapLength) != 0)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Segments do not divide input evenly");
    }

    // resize output arrays to hold csd estimate
    if (out.size() != m_fftLength)
    {
      out.resize(m_fftLength, 0.0);
    }

    // calculate number of sections to average
    const int numSections = (dataLength - m_overlapLength)/
      (m_fftLength - m_overlapLength);
                         
    // Sequences to hold detrended, windowed, and ffted data
    Sequence<TXIn> x_segment(m_fftLength);
    Sequence<TYIn> y_segment(m_fftLength);

    DFT<std::complex<TOut> > fft_x_segment(m_fftLength);
    DFT<std::complex<TOut> > fft_y_segment(m_fftLength);

    // need FFT object to calculate the dft
    FFT fft;

    // loop over different sections
    size_t index = 0;

    for (int i = 0; i < numSections; i++) 
    {
      x_segment = xIn[std::slice(index, m_fftLength, 1)];
      y_segment = yIn[std::slice(index, m_fftLength, 1)];

      // detrend segments of input data
      detrend(x_segment);
      detrend(y_segment);

      // apply window to detrended segments
      m_pwindow->apply(x_segment, x_segment);
      m_pwindow->apply(y_segment, y_segment);

      // take dft of windowed data
      fft.apply(fft_x_segment, x_segment);
      fft.apply(fft_y_segment, y_segment);

      // form product conj(fft_x)*fft_y for each section of data,	
      // then sum up estimates for each section
      unsigned int nj = 0;
      for (unsigned int ni = out.size()/2; ni < out.size(); ++ni)
      {
	out[ni] += conj(fft_x_segment[nj])*fft_y_segment[nj];
	nj++;
      }
      for (unsigned int ni = 0; ni < out.size()/2; ++ni)
      {
	out[ni] += conj(fft_x_segment[nj])*fft_y_segment[nj];
	nj++;
      }

      // get index ready for next segment
      index += (m_fftLength - m_overlapLength); 
    }

    // FrequencySequence metadata

    // Get the sampling rate and units
    // :KLUDGE: This is a big fat hairy kludge, should be done
    // by applying to TimeSeries then by applying to Sequence
    double sampleRate = 0.0;
    if (const TimeSeries<TXIn>* const p
	= dynamic_cast<const TimeSeries<TXIn>*>(&xIn))
    {
      sampleRate = p->GetSampleRate();
    }
    else if (const TimeSeries<TYIn>* const p
	     = dynamic_cast<const TimeSeries<TYIn>*>(&yIn))
    {
      sampleRate = p->GetSampleRate();
    }
    else
    {
      // it's just a Sequence after all
      sampleRate = 1.0;
    }

    // 
    // Normalize csd - the definition that has been adopted is so that
    //
    //                      delta-t
    //    TwoSidedPSD[k] =  ------- < DFT(h)[k] * conj( DFT(h)[k] ) >
    //                         N
    //
    // where < > means ensemble average.
    //
    // The normalisation has several factors. The overall scaling is the
    // inverse of the following product:
    //
    // - square of the window's RMS 
    // - number of sections (to average the sum of individual fft*conf(fft)'s)
    // - FFT length
    // - inverse of delta-t, that is, the sampling rate.
    //
    
    // Square of window's RMS
    const double rms = m_pwindow->rms();
    const double factor = 1.0/(sampleRate*m_fftLength*rms*rms*numSections);

    std::complex< TOut > complex_factor( static_cast< TOut >( factor ) );
    out *= complex_factor;

    // Frequency base depends on whether fft length is odd or even
    // :TODO: Base frequency will also depend on baseband for "mixed"
    // TimeSeries or Sequence's.
    
    // For clarity, spread out in an if statement
    double FrequencyBase = 0.0;
    if (m_fftLength % 2 == 0)
    {
      FrequencyBase = -sampleRate/2.0;
    }
    else
    {
      FrequencyBase = (-1.0 + 1.0/m_fftLength)*sampleRate/2.0;
    }

    // :TODO: How to set the FrequencyBase correctly when one or both
    // inputs has been mixed by some frequency??
    out.SetFrequencyBase(FrequencyBase);
    out.SetFrequencyDelta(sampleRate/m_fftLength);

    // CSDSpectrum metadata

    out.SetDetrendMethod(detrendMethod());
    out.SetEstimator(what());

    // WelchCSDEstimate metadata

    out.SetFFTLength(fftLength());
    out.SetFFTOverlap(overlapLength());
    out.SetWindowInfo(windowType());

    out.SetNameOfChannel1(xIn.name());
    out.SetNameOfChannel2(yIn.name());
    out.SetName("csd(" + xIn.name() + "," + yIn.name() + ")");

    if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&xIn) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
    else if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&yIn) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
  }

  //
  // One-sided PSD of real data
  //
  template<class TOut, class TIn>
  void
  WelchCSDEstimate::
  apply(WelchSpectrum<TOut>& out, const Sequence<TIn>& in)
  {
    // Check that input data length is > 0
    const size_t dataLength = in.size();

    if (dataLength == 0) 
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "on zero-length Sequence");
    }

    // Check that the data length is at least as long as the FFT length
    if (dataLength < m_fftLength)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Input Sequence length is less than FFT length");
    }
    
    // Check that the overlapping segments will divide up the input
    // sequence evenly
    if ((dataLength - m_overlapLength) % (m_fftLength - m_overlapLength) != 0)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Segments do not divide input evenly");
    }

    const size_t psd_size = m_fftLength/2 + 1;

    // resize output arrays to hold psd estimate
    if (out.size() != psd_size)
    {
      out.resize(psd_size, 0.0);
    }

    // calculate number of sections to average
    const int numSections = (dataLength - m_overlapLength)/
      (m_fftLength - m_overlapLength);
                         
    // Sequences to hold detrended, windowed, and ffted data
    Sequence<TIn> segment(m_fftLength);
    DFT<std::complex<TIn> > fft_segment(m_fftLength);

    // need FFT object to calculate the dft
    FFT fft;

    // loop over different sections
    size_t index = 0;

    for (int i = 0; i < numSections; ++i) 
    {
      segment = in[std::slice(index, m_fftLength, 1)];

      // detrend segment of input data in-place
      detrend(segment);

      // apply window to detrended segments
      m_pwindow->apply(segment, segment);

      // take dft of windowed data
      fft.apply(fft_segment, segment);

      // calculate absolute square of fft for each section of data,	
      // then sum up estimates for each section
      addNorm(&fft_segment[0], &fft_segment[psd_size], &out[0]);

      // get index ready for next segment
      index += (m_fftLength - m_overlapLength); 
    }

    // FrequencySequence metadata
    
    // Get the sampling rate and base frequency. Note that
    // only TimeSeries have these as part of their data, we choose
    // defaults if only acting on Sequences.
    //
    // :KLUDGE: This is a big fat hairy kludge, should be done
    // by applying to TimeSeries then by applying to Sequence
    double sampleRate = 0.0;
    double BaseFrequency = 0.0; // Stored as Hz in TimeSeries
    if (const TimeSeries<TIn>* const p
	= dynamic_cast<const TimeSeries<TIn>*>(&in))
    {
      sampleRate = p->GetSampleRate();
      BaseFrequency = p->GetBaseFrequency();
    }
    else
    {
      // it's just a Sequence after all
      sampleRate = 1.0;
      BaseFrequency = 0.0;
    }

    //
    // Normalize csd - the definition that has been adopted is so that
    //
    //                      delta-t
    //    OneSidedPSD[0] =  -------   < DFT(h)[0] * conj( DFT(h)[0] ) >
    //                         N
    //
    //
    //                      2*delta-t
    //    OneSidedPSD[k] =  --------- < DFT(h)[k] * conj( DFT(h)[k] ) >
    //                         N
    //
    // where < > means ensemble average.
    //
    // The normalisation has several factors. The overall scaling is the
    // inverse of the following product:
    //
    // - square of the window's RMS 
    // - number of sections (to average the sum of individual fft*conf(fft)'s)
    // - FFT length
    // - inverse of delta-t, that is, the sampling rate.
    //
    
    // Square of window's RMS
    const double rms = m_pwindow->rms();
    const double factor = 2.0/(sampleRate*m_fftLength*rms*rms*numSections);

    out *= factor;

    // The DC element needs to halved with respect to the other elements
    out[0] /= 2;

    // The Nyquist element needs to halved with respect to the other elements
    // if the length is even
    if ((m_fftLength % 2) == 0)
    {
      out[m_fftLength/2] /= 2;
    }

    // One-sided PSD has frequency base 0
    const double FrequencyBase = 0.0;

    // For TimeSeries which have been mixed ie. have non-zero base
    // frequency, the PSD of the unmixed TimeSeries will line up
    // with the PSD of the mixed TimeSeries if the mixed PSD is shifted
    // to the left by BaseFrequency
    // ie. shifted to FrequencyBase - BaseFrequency
    out.SetFrequencyBase(FrequencyBase);
    out.SetFrequencyDelta(sampleRate/m_fftLength);

    // Spectrum metadata

    out.SetDetrendMethod(detrendMethod());
    out.SetEstimator(what());

    // WelchCSDEstimate metadata

    out.SetBaseFrequency(BaseFrequency);
    out.SetFFTLength(fftLength());
    out.SetFFTOverlap(overlapLength());
    out.SetWindowInfo(windowType());

    // Fill in certain pieces of Meta Data

    // Set the meta-data to the source data
    out.SetNameOfChannel(in.name());
    
    // Set the name of this pseudo-channel
    out.SetName("psd(" + in.name() + ")");

    if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&in) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
  }

  //
  // 2-sided PSD of complex data
  //
  template<class TOut, class TIn>
  void
  WelchCSDEstimate::
  apply(WelchSpectrum<TOut>& out, const Sequence<std::complex<TIn> >& in)
  {
    // Check that input data length is > 0
    const size_t dataLength = in.size();

    if (dataLength == 0) 
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "on zero-length Sequence");
    }

    // Check that the data length is at least as long as the FFT length
    if (dataLength < m_fftLength)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Input Sequence length is less than FFT length");
    }
    
    // Check that the overlapping segments will divide up the input
    // sequence evenly
    if ((dataLength - m_overlapLength) % (m_fftLength - m_overlapLength) != 0)
    {
      throw std::invalid_argument("WelchCSDEstimate::apply() "
				  "Segments do not divide input evenly");
    }

    // resize output arrays to hold psd estimate
    if (out.size() != m_fftLength)
    {
      out.resize(m_fftLength, 0.0);
    }

    // calculate number of sections to average
    const int numSections = (dataLength - m_overlapLength)/
      (m_fftLength - m_overlapLength);
                         
    // Sequences to hold detrended, windowed, and ffted data
    Sequence<std::complex<TIn> > segment(m_fftLength);
    DFT<std::complex<TIn> > fft_segment(m_fftLength);

    // need FFT object to calculate the dft
    FFT fft;

    // loop over different sections
    size_t index = 0;

    for (int i = 0; i < numSections; i++) 
    {
      segment = in[std::slice(index, m_fftLength, 1)];

      // detrend segments of input data
      detrend(segment);

      // apply window to detrended segments
      m_pwindow->apply(segment, segment);

      // take dft of windowed data
      fft.apply(fft_segment, segment);

      // calculate absolute square of fft for each section of data,	
      // then sum up estimates for each section
      unsigned int nj = 0;
      for (unsigned int ni = out.size()/2; ni < out.size(); ++ni)
      {
	out[ni] += norm(fft_segment[nj++]);
      }
      for (unsigned int ni = 0; ni < out.size()/2; ++ni)
      {
	out[ni] += norm(fft_segment[nj++]);
      }

      // get index ready for next segment
      index += (m_fftLength - m_overlapLength); 
    }

    // FrequencySequence metadata
    
    // Get the sampling rate and base frequency. Note that
    // only TimeSeries have these as part of their data, we choose
    // defaults if only acting on Sequences.
    //
    // :KLUDGE: This is a big fat hairy kludge, should be done
    // by applying to TimeSeries then by applying to Sequence
    double sampleRate = 0.0;
    double BaseFrequency = 0.0; // Stored as Hz in TimeSeries
    if (const TimeSeries<std::complex<TIn> >* const p
	= dynamic_cast<const TimeSeries<std::complex<TIn> >*>(&in))
    {
      sampleRate = p->GetSampleRate();
      BaseFrequency = p->GetBaseFrequency();
    }
    else
    {
      // it's just a Sequence after all
      sampleRate = 1.0;
      BaseFrequency = 0.0;
    }

    //
    // Normalize csd - the definition that has been adopted is so that
    //
    //                      delta-t
    //    TwoSidedPSD[k] =  ------- < DFT(h)[k] * conj( DFT(h)[k] ) >
    //                         N
    //
    // where < > means ensemble average.
    //
    // The normalisation has several factors. The overall scaling is the
    // inverse of the following product:
    //
    // - square of the window's RMS 
    // - number of sections (to average the sum of individual fft*conf(fft)'s)
    // - FFT length
    // - inverse of delta-t, that is, the sampling rate.
    //
    
    // Square of window's RMS
    const double rms = m_pwindow->rms();
    const double factor = 1.0/(sampleRate*m_fftLength*rms*rms*numSections);

    out *= factor;

    // Frequency base depends on whether fft length is odd or even
    // :TODO: Base frequency will also depend on baseband for "mixed"
    // TimeSeries or Sequence's.
    
    // For clarity, spread out in an if statement
    double FrequencyBase = 0.0;
    if (m_fftLength % 2 == 0)
    {
      FrequencyBase = -sampleRate/2.0;
    }
    else
    {
      FrequencyBase = (-1.0 + 1.0/m_fftLength)*sampleRate/2.0;
    }

    // For TimeSeries which have been mixed ie. have non-zero base
    // frequency, the PSD of the unmixed TimeSeries will line up
    // with the PSD of the mixed TimeSeries if the mixed PSD is shifted
    // to the left by BaseFrequency
    // ie. shifted to FrequencyBase - BaseFrequency
    out.SetFrequencyBase(FrequencyBase);
    out.SetFrequencyDelta(sampleRate/m_fftLength);

    // Spectrum metadata

    out.SetDetrendMethod(detrendMethod());
    out.SetEstimator(what());

    // WelchCSDEstimate metadata

    out.SetBaseFrequency(BaseFrequency);
    out.SetFFTLength(fftLength());
    out.SetFFTOverlap(overlapLength());
    out.SetWindowInfo(windowType());

    // Fill in certain pieces of Meta Data

    // Set the meta-data to the source data
    out.SetNameOfChannel(in.name());
    
    // Set the name of this pseudo-channel
    out.SetName("psd(" + in.name() + ")");

    if (const WhenMetaData* i_when = dynamic_cast<const WhenMetaData*>(&in) )
    {
      out.ArbitraryWhenMetaData::operator=(*i_when);
    }
  }
} // namespace - datacondAPI


void datacondAPI::WelchCSDEstimate::
apply(udt*& out, 
      const udt& xIn,
      const udt& yIn)
{
    if ( (udt::IsA<Sequence<float> >(xIn)) &&
	 (udt::IsA<Sequence<float> >(yIn)) ) 
    {
	dispatch<std::complex<float>, float, float>
 	    (out, 
	     udt::Cast<Sequence<float> >(xIn),
	     udt::Cast<Sequence<float> >(yIn));  
    }
    else if ( (udt::IsA<Sequence<float> >(xIn)) &&
	      (udt::IsA<Sequence<std::complex<float> > >(yIn)) ) 
    {
	dispatch<std::complex<float>, float, std::complex<float> >
	    (out, 
	     udt::Cast<Sequence<float> >(xIn),
	     udt::Cast<Sequence<std::complex<float> > >(yIn));  
    }
    else if ( (udt::IsA<Sequence<std::complex<float> > >(xIn)) &&
	      (udt::IsA<Sequence<float> >(yIn)) ) 
    {
	dispatch<std::complex<float>, std::complex<float>, float>
	    (out, 
	     udt::Cast<Sequence<std::complex<float> > >(xIn),
	     udt::Cast<Sequence<float> >(yIn));  
    }
    else if ( (udt::IsA<Sequence<std::complex<float> > >(xIn)) &&
	      (udt::IsA<Sequence<std::complex<float> > >(yIn)) ) 
    {
	dispatch<std::complex<float>, std::complex<float>, std::complex<float> >
	    (out, 
	     udt::Cast<Sequence<std::complex<float> > >(xIn),
	     udt::Cast<Sequence<std::complex<float> > >(yIn));  
    }
    else if ( (udt::IsA<Sequence<double> >(xIn)) &&
	      (udt::IsA<Sequence<double> >(yIn)) ) 
    {
	dispatch<std::complex<double>, double, double>
 	    (out, 
	     udt::Cast<Sequence<double> >(xIn),
	     udt::Cast<Sequence<double> >(yIn));  
    }
    else if ( (udt::IsA<Sequence<double> >(xIn)) &&
	      (udt::IsA<Sequence<std::complex<double> > >(yIn)) ) 
    {
	dispatch<std::complex<double>, double, std::complex<double> >
	    (out, 
	     udt::Cast<Sequence<double> >(xIn),
	     udt::Cast<Sequence<std::complex<double> > >(yIn));  
    }
    else if ( (udt::IsA<Sequence<std::complex<double> > >(xIn)) &&
	      (udt::IsA<Sequence<double> >(yIn)) ) 
    {
	dispatch<std::complex<double>, std::complex<double>, double>
	    (out, 
	     udt::Cast<Sequence<std::complex<double> > >(xIn),
	     udt::Cast<Sequence<double> >(yIn));  
    }
    else if ( (udt::IsA<Sequence<std::complex<double> > >(xIn)) &&
	      (udt::IsA<Sequence<std::complex<double> > >(yIn)) ) 
    {
	dispatch<std::complex<double>, std::complex<double>, 
	         std::complex<double> >
	    (out, 
	     udt::Cast<Sequence<std::complex<double> > >(xIn),
	     udt::Cast<Sequence<std::complex<double> > >(yIn));  
    }
    else
    {
	throw std::invalid_argument("WelchCSDEstimate::apply() "
				    "input UDT's must be Sequences");
    }
}


void datacondAPI::WelchCSDEstimate::
apply(udt*& out, 
      const udt& in)
{
    if (udt::IsA<Sequence<float> >(in))
    {
	dispatch<float, float>(out, udt::Cast<Sequence<float> >(in));
    }
    else if (udt::IsA<Sequence<std::complex<float> > >(in))
    {
	dispatch<float, std::complex<float> >(out,
	    udt::Cast<Sequence<std::complex<float> > >(in));
    }
    else if (udt::IsA<Sequence<double> >(in))
    {
	dispatch<double, double>(out, udt::Cast<Sequence<double> >(in));
    }
    else if (udt::IsA<Sequence<std::complex<double> > >(in))
    {
	dispatch<double, std::complex<double> >(out,
	    udt::Cast<Sequence<std::complex<double> > >(in));
    }
    else
    {
	throw std::invalid_argument("WelchCSDEstimate::apply() "
				    "input UDT must be a Sequence");
    }
}


template<class TOut, class TXIn, class TYIn>
void 
datacondAPI::WelchCSDEstimate::
dispatch(udt*& out, 
         const Sequence<TXIn>& xIn,
         const Sequence<TYIn>& yIn)
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    std::unique_ptr<WelchCSDSpectrum<TOut> > tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
	tmp.reset(new WelchCSDSpectrum<TOut>);
	tmp_out = tmp.get();
    }
    else if (!udt::IsA<WelchCSDSpectrum<TOut> >(*tmp_out))
    {
	// Stupidly can't delete the output if it's the wrong type,
	// have to throw an exception instead
	throw std::invalid_argument("WelchCSDEstimate::apply() "
				    "output UDT must be a WelchCSDSpectrum");
    }

    apply(udt::Cast<WelchCSDSpectrum<TOut> >(*tmp_out), xIn, yIn);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
	// transfer ownership from the unique_ptr to 'out' so that the
	// WelchCSDSpectrum isn't deleted when the unique_ptr goes out of scope
	out = tmp.release();
    }
}


template<class TOut, class TIn>
void 
datacondAPI::WelchCSDEstimate::
dispatch(udt*& out, 
         const Sequence<TIn>& in)
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    std::unique_ptr<WelchSpectrum<TOut> > tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
	tmp.reset(new WelchSpectrum<TOut>);
	tmp_out = tmp.get();
    }
    else if (!udt::IsA<WelchSpectrum<TOut> >(*tmp_out))
    {
	// Stupidly can't delete the output if it's the wrong type,
	// have to throw an exception instead
	throw std::invalid_argument("WelchCSDEstimate::apply() "
				    "output UDT must be a WelchCSDSpectrum");
    }

    apply(udt::Cast<WelchSpectrum<TOut> >(*tmp_out), in);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
	// transfer ownership from the unique_ptr to 'out' so that the
	// WelchSpectrum isn't deleted when the unique_ptr goes out of scope
	out = tmp.release();
    }
}


#undef INSTANTIATE
#define INSTANTIATE(DATATYPE) \
template void datacondAPI::WelchCSDEstimate::apply( \
datacondAPI::WelchCSDSpectrum< std::complex<DATATYPE> >&, \
const datacondAPI::Sequence<std::complex< DATATYPE > >&, \
const datacondAPI::Sequence< DATATYPE >&); \
template void datacondAPI::WelchCSDEstimate::apply( \
datacondAPI::WelchCSDSpectrum< std::complex<DATATYPE> >&, \
const datacondAPI::Sequence< DATATYPE >&, \
const datacondAPI::Sequence<std::complex< DATATYPE > >&); \
template void datacondAPI::WelchCSDEstimate::apply( \
datacondAPI::WelchCSDSpectrum< std::complex<DATATYPE> >&, \
const datacondAPI::Sequence<std::complex< DATATYPE > >&, \
const datacondAPI::Sequence<std::complex< DATATYPE > >&); \
template void datacondAPI::WelchCSDEstimate::apply( \
datacondAPI::WelchSpectrum< DATATYPE >&, \
const datacondAPI::Sequence< DATATYPE >&); \
template void datacondAPI::WelchCSDEstimate::apply( \
datacondAPI::WelchSpectrum< DATATYPE >&, \
const datacondAPI::Sequence<std::complex< DATATYPE > >&); \
  template DATATYPE* addNorm< std::complex< DATATYPE >*, DATATYPE*>(std::complex< DATATYPE >* first, std::complex< DATATYPE >* last, DATATYPE* res)

INSTANTIATE(float);
INSTANTIATE(double);

#undef INSTANTIATE


