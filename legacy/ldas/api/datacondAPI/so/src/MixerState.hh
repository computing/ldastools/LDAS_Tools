/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef MIXERSTATE_HH
#define MIXERSTATE_HH

#include <complex>
#include <general/unexpected_exception.hh>

#include "StateUDT.hh"
#include "SequenceUDT.hh"

namespace datacondAPI
{

  class MixerState : public State
  {

    friend class Mixer;

  public:

    // constructors

    //: construct from phase, frequency
    //!param: phase - phase of Mixer at first (current) sample
    //!param: freq - local oscillator frequency Nyquist frequency units
    //!exc: std::domain_error - condition?
    //!todo: check exceptions
    MixerState(const double& phase, const double& freq);

    //: construct from phase, frequency as udts
    //!param: phase - phase of Mixer at first (current) sample
    //!param: freq - local oscillator frequency Nyquist frequency units
    //!exc: std::domain_error - condition?
    //!exc: std::invalid_argument - condition?
    //!todo: check exceptions
    MixerState(const udt& phase, const udt& freq);

    //: Copy constructor
    //!param: state - MixerState to construct from
    MixerState(const MixerState& state);

    //: Construct from MixerState as udt
    //!param: state - MixerState to construct from 
    //!exc: std::invalid_argument - state not of type MixerState
    MixerState(const udt& state);
    
    // destructor
    //: Destructor
    virtual ~MixerState();
    
    // assignment

    //: assignment operator
    //!param: state - MixerState to assign from
    MixerState& operator=(const MixerState& state);
    
    // accessors
    
    //: phase at current sample
    //!return: double - phase at current sample
    double GetPhase() const;

    //: frequency of local oscillator
    //!return: double - frequency of local oscillator as fraction of Nyquist
    //+ sampling frequency
    double GetFrequency() const;

    // UDT methods
    //: create duplicate object
    datacondAPI::MixerState* Clone() const;

    //: convert *this to an ILWD
    //!param: Chain -
    //!param: Target -
    //!return: ILwd::LdasElement* -
    ILwd::LdasElement* ConvertToIlwd( const CallChain& Chain, datacondAPI::udt::target_type Target = datacondAPI::udt::TARGET_GENERIC ) const;

  private:

    //: default constructor - prohibited (no meaningful default)
    MixerState();

    //: set state phase
    //!param: phase - new state phase
    //!exc: std::invalid_argument - ????
    void SetPhase(const double& phase);

    //: set state phase
    //!param: phase - new state phase
    //!exc: std::domain_error - phase not a Scalar<double>
    //!exc: std::invalid_argument - ????
    void SetPhase(const udt& phase);

    //: set local oscillator frequency
    //!param: phase - new local oscillator frequency in units of Nyquist freq
    //!exc: std::domain_error - |freq| > 1
    void SetFrequency(const double& freq);

    //: set local oscillator frequency
    //!param: freq - new local oscillator frequency in units of Nyquist freq
    //!exc: std::domain_error - |freq| > 1
    //!exc: std::invalid_argument - freq is not a Scalar<>
    void SetFrequency(const udt& freq);

    // member data

    double m_phase;
    double m_frequency;

  }; // class MixerState

} // namespace datacondAPI

#endif // MIXERSTATE_HH
