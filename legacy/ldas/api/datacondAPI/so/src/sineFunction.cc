#include "config.h"

#include <filters/LDASConstants.hh>
   
#include "SequenceUDT.hh"
#include "sineFunction.hh"
#include "ScalarUDT.hh"
#include "TypeInfo.hh"
#include "UDT.hh"

using std::sin;

const std::string& sineFunction::GetName(void) const
{
  static std::string name("sine");
  
  return name;
}

sineFunction::sineFunction() : Function( sineFunction::GetName() )
{}

//static sineFunction _sineFunction;

void sineFunction::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;   
  int n = 1;
  double f = 0;
  double phi = 0;
  
  using namespace datacondAPI;

  switch(Params.size())
  {
  case 2:
    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
      n = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();
    else
      throw CallChain::BadArgument(GetName(), 0, "n not an integer", "n must be a positive integer");

    if(n <= 0)
      throw CallChain::BadArgument(GetName(), 0, "n <= 0", "n must be a positive integer");

    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
      f = (double)udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
    else if(udt::IsA<Scalar<float> >( *Chain->GetSymbol(Params[1])))
      f = (double)udt::Cast<Scalar<float> > (*Chain->GetSymbol(Params[1])).GetValue();
    else if(udt::IsA<Scalar<double> >( *Chain->GetSymbol(Params[1])))
      f = (double)udt::Cast<Scalar<double> > (*Chain->GetSymbol(Params[1])).GetValue();
    else
      throw CallChain::BadArgument( GetName(), 1, "int, float, or double",
				    TypeInfoTable.GetName( typeid( *Chain->GetSymbol( Params[1] ) ) ) );

    if(std::abs(f) > 0.5)
      throw CallChain::BadArgument(GetName(), 0, "|f| > 0.5", "-0.5 <= f <= 0.5");

    break;

  case 3:
    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
      n = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();
    else
      throw CallChain::BadArgument(GetName(), 0, "n not an integer", "n must be a positive integer");

    if(n <= 0)
      throw CallChain::BadArgument(GetName(), 0, "n <= 0", "n must be a positive integer");

    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
      f = (double)udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
    else if(udt::IsA<Scalar<float> >( *Chain->GetSymbol(Params[1])))
      f = (double)udt::Cast<Scalar<float> > (*Chain->GetSymbol(Params[1])).GetValue();
    else if(udt::IsA<Scalar<double> >( *Chain->GetSymbol(Params[1])))
      f = (double)udt::Cast<Scalar<double> > (*Chain->GetSymbol(Params[1])).GetValue();
    else
      throw CallChain::BadArgument( GetName(), 1, "int, float, or double",
				    TypeInfoTable.GetName( typeid( *Chain->GetSymbol( Params[1] ) ) ) );

    if(std::abs(f) > 0.5)
      throw CallChain::BadArgument(GetName(), 0, "|f| > 0.5", "-0.5 <= f <= 0.5");

    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[2])))
      phi = (double)udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[2])).GetValue();
    else if(udt::IsA<Scalar<float> >( *Chain->GetSymbol(Params[2])))
      phi = (double)udt::Cast<Scalar<float> > (*Chain->GetSymbol(Params[2])).GetValue();
    else if(udt::IsA<Scalar<double> >( *Chain->GetSymbol(Params[2])))
      phi = (double)udt::Cast<Scalar<double> > (*Chain->GetSymbol(Params[2])).GetValue();
    else
      throw CallChain::BadArgument( GetName(), 1, "int, float, or double",
				    TypeInfoTable.GetName( typeid( *Chain->GetSymbol( Params[1] ) ) ) );

    if(std::abs(phi) > LDAS_PI)
      throw CallChain::BadArgument(GetName(), 0, "|phi| > PI", "-PI <= phi <= PI");

    break;
    
  default:
    throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: y = sine(n, f, [phi])");
    break;
   }

  returnedSymbol = new Sequence<double>(n);
  for(int k=0; k<n; k++)
    udt::Cast<Sequence<double> >(*returnedSymbol)[k] = sin(LDAS_TWOPI*f*k+phi);

  if (!returnedSymbol)
    {
      throw CallChain::NullResult( GetName() );
    }
  
  Chain->ResetOrAddSymbol( Ret, returnedSymbol );  
}
