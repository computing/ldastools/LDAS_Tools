/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "datacondAPI/config.h"

#include <sstream>

#include <general/toss.hh>
#include <filters/LDASConstants.hh>

#include "Mixer.hh"

using std::ceil;
using std::cos;
using std::fabs;
using std::fmod;
using std::sin;

namespace 
{
  const char rcsID[] = "@(#) $Id: Mixer.cc,v 1.35 2007/06/18 15:26:42 emaros Exp $:";
}

namespace datacondAPI
{
  Mixer::Mixer(const MixerState& state)
    : m_state(state)
  {
  }
  
  Mixer::Mixer(const Mixer& mixer)
    : m_state(mixer.m_state)
  {
  }
  
  Mixer::~Mixer()
  {
  }
  
  Mixer& Mixer::operator=(const Mixer& mixer)
  {
    if (&mixer != this)
    {
      m_state = mixer.m_state;
    }
    return *this;
  }
  
  MixerState Mixer::getState() const
  {
    return m_state;
  }
  
  void Mixer::getState(MixerState& state) const {
    state = m_state;
  }

  void Mixer::setState(const MixerState& state)
  {
    m_state = state;
  }
  
  void Mixer::getState(State*& pstate) const 
  {
    bool null_out = (pstate == 0);
    try 
    {
      if (null_out)
      {
	pstate = new MixerState();
      }
      if (udt::IsA<MixerState>(*pstate))
      {
	*pstate = m_state;
      }
      else
      {
	std::string what = "Mixer::getState(State*&) arg not pointer to MixerState";
	throw std::invalid_argument(what);
      }
    }
    catch(...)
    {
      if (null_out)
      {
	delete pstate;
	pstate = 0;
      }
      throw;
    }
  }
  
  void Mixer::apply(udt*& out, const udt& in)
  {
    // Get the Mixer state info BEFORE applying it
    const double freq  = m_state.GetFrequency();
    const double phase = m_state.GetPhase();

    bool applied = asTimeSeries(out, in);
    if (!applied)
    {
      applied = asSequence(out, in);
    }
    if (!applied)
    {
      std::string what = "Mixer::apply: apply on unimplemented type";
      throw General::unimplemented_error(what);
    }

    // The name is set here because we want the phase of the Mixer
    // from before it's applied, not after.
    std::ostringstream oss;
    oss << "mix("
	<< phase
	<< ","
	<< freq
	<< ","
	<< in.name()
	<< ")";
    
    out->SetName(oss.str());
  }

  bool Mixer::asTimeSeries(udt*& out, const udt& in)
  {
    // Get the Mixer state info BEFORE applying it
    const double freq  = m_state.GetFrequency();
    const double phase = m_state.GetPhase();

    bool ok = applyAs<TimeSeries<std::complex<float> >,TimeSeries<float> >(out, in);
    if (!ok) 
    {
      ok = applyAs<TimeSeries<std::complex<double> >,
	TimeSeries<double> >(out, in); 
    }
    if (!ok)
    {
      ok = applyAs<TimeSeries<std::complex<float> >,
	TimeSeries<std::complex<float> > >(out, in); 
    }
    if (!ok)
    {
      ok = applyAs<TimeSeries<std::complex<double> >,
	TimeSeries<std::complex<double> > >(out, in); 
    }

    if (ok)
    {
      //
      // Process TimeSeries metadata
      //
      // The immediate base classes of TimeSeries are currently
      // TimeSeriesMetaData, WhenMetaData and Sequence
      //
      // TimeSeriesMetaData will be identical except for base frequency
      // and phase
      //
      TimeSeriesMetaData* const omd = dynamic_cast<TimeSeriesMetaData*>(out);
      const TimeSeriesMetaData* const imd
	= dynamic_cast<const TimeSeriesMetaData*>(&in);
      
      if ((omd == 0) || (imd == 0))
      {
	std::string what("apply unimplemented on this type");
	General::toss<General::unimplemented_error>("Mixer",
						    __FILE__,
						    __LINE__,
						    what);
      }
      else
      {
	//
	// First just copy whatever meta-data already exists, in case
	// more meta-data fields are added in the future
	//
	omd->TimeSeriesMetaData::operator=(*imd);
	
	//
	// Calculate the new frequency (in Hz) of the TimeSeries
	// produced from mixing the input TimeSeries.
	// Mixer frequency is in fraction of Nyquist but TimeSeries
	// base frequency is in Hz - need to convert
	//
	const double new_freq_hz = imd->GetBaseFrequency() 
	  + freq*imd->GetSampleRate()/2.0;
	omd->SetBaseFrequency(new_freq_hz);
	
	//
	// New phase is phase of Mixer as it was BEFORE applying to the
	// TimeSeries plus the phase of the first sample of the input
	// TimeSeries, modulo LDAS_TWOPI
	//
	double new_phase = fmod(imd->GetPhase() + phase, LDAS_TWOPI);
	if (new_phase < 0.0)
	{
	  new_phase += LDAS_TWOPI;
	}
	omd->SetPhase(new_phase);
      }
      
      // WhenMetaData just needs to be copied
      WhenMetaData* const owmd = dynamic_cast<WhenMetaData*>(out);
      const WhenMetaData* const iwmd = dynamic_cast<const WhenMetaData*>(&in);
      
      if ((owmd == 0) || (iwmd == 0))
      {
	std::string what("apply unimplemented on this type");
	General::toss<General::unimplemented_error>("Mixer",
						    __FILE__,
						    __LINE__,
						    what);
      }
      else
      {
	owmd->WhenMetaData::operator=(*iwmd);
      }

    }

    return ok;
  }

  bool Mixer::asSequence(udt*& out, const udt& in)
  {
    bool ok = applyAs<Sequence<std::complex<float> >,Sequence<float> >(out, in);
    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<float> >,
	Sequence<float> >(out, in);
    }
    if (!ok) 
    {
      ok = applyAs<Sequence<std::complex<double> >,
	Sequence<double> >(out, in); 
    }
    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<float> >,
	Sequence<std::complex<float> > >(out, in); 
    }
    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<double> >,
	Sequence<std::complex<double> > >(out, in); 
    }
    if (ok)
    {
      // process Sequence metadata
      // Nothing to be done here at the moment
    }
    return ok;
  }

  template <class Tout, class Tin>
  bool Mixer::applyAs(udt*& out, const udt& in)
  {
    bool ok = false;
    bool null_out = ( out == 0 );
    try 
    {
      if (udt::IsA<Tin>(in))
      {
	if (null_out)
	{
	  out = new Tout;
	}
	else if (!udt::IsA<Tout>(*out))
	{
	  std::string what = "datacondAPI::Mixer::applyAs<>: type out not compatible with type in";
	  throw std::invalid_argument(what);
	}
	apply(udt::Cast<Tout>(*out), udt::Cast<Tin>(in));
	ok = true;
      }
      else
      {
	ok = false;
      }
    }
    catch(...)
    {
      if(null_out)
      {
	delete out;
	out =0;
      }
      throw;	
    }
    return ok;
  }

  template<typename out_type, typename in_type>
  void Mixer::apply(std::valarray<std::complex<out_type> >& out,
		    const std::valarray<in_type>& in)
  {
    if (in.size() <= 0)
    {
      throw std::invalid_argument("datacondAPI::Mixer::apply: "
				  "Input Sequence is empty");
    }

    if (out.size() != in.size())
    {
      out.resize(in.size());
    }
    
    int nsamp = in.size();
    
    // Mix with local oscillator
    // for(;cond;) evaluates cond before the loop body; so, 
    // this is safe against nsamp == 0
    
    // Following commented code doesn't unroll the loops: it is meant
    // to debug/check loop unrolling
    
    // std::valarray<std::complex<Tout> >  z(nsamp); // create output
    //  double phi = phase;
    //  for (int k = 0; k < nsamp; k++) 
    //    {
    //      std::complex<double> osc(cos(phi), sin(phi));
    //      z[k] = in[k]*osc;
    //      phi += dphi;
    //    }
    //  phase = fmod(phi,LDAS_TWOPI);
    
    // Implementation note: I've been careful here to reduce the phase
    // to [0,2*M_PI) every ceil(2/fc) samples, which is how long it
    // takes the phase to increase by 2*M_PI rads. Alternatively, can
    // throw caution to wind and only reduce the phase at the end of
    // every call to apply().
    
    double fc(m_state.GetFrequency());  // carrier
    double phi(m_state.GetPhase());	// phase
    
    // Set parameters for loop unrolling
    
    int ksamp((int) ceil(fabs(2/fc))); // samples between phase reduction
    int lsamp = nsamp%ksamp;           // remainder
    int jsamp = (nsamp-lsamp)/ksamp;   // full iterations
    
    std::valarray<std::complex<out_type> >  z(nsamp); // create output
    
    // Mix:
    
    double dphi = LDAS_PI*fc;	// phase increment per cycle
    for (int j = 0; j < jsamp; j++)
    {
      for (int k = 0; k < ksamp; k++)
      {
	int ndx = k + j*ksamp;
	std::complex<out_type> osc(::cos(phi),::sin(phi));
	out[ndx] = ((std::complex<out_type>)in[ndx])*osc;
	phi += dphi; // increment phase 
      }
      phi = fmod(phi,LDAS_TWOPI); // reduce phase to range [0,LDAS_TWOPI)
    }
    for (int l = 0; l < lsamp; l++) // remainder
    {
      int ndx = l + jsamp*ksamp;
      std::complex<out_type> arg(::cos(phi),::sin(phi));
      out[ndx] = ((std::complex<out_type>)in[ndx])*arg;
      phi += dphi;
    }
    
    m_state.SetPhase((phi >= 0) ? fmod(phi, LDAS_TWOPI) : (fmod(phi, LDAS_TWOPI) + LDAS_TWOPI));	// ready phase for next sample
  }
  
#define INSTANTIATE(OUT_TYPE, IN_TYPE) \
  template void Mixer::apply(std::valarray<std::complex< OUT_TYPE > >&, const std::valarray< IN_TYPE >&);
  
INSTANTIATE(float, int)
INSTANTIATE(float, float)
INSTANTIATE(double, double)
INSTANTIATE(float, std::complex<float>)
INSTANTIATE(double, std::complex<double>)
    
} // namespace datacondAPI
