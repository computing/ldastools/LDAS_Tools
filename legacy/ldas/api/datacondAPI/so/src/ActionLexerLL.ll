/* -*- mode: c++ -*- */
%option c++
%option yyclass="datacondAPI::ActionLexer"
%option noyywrap
%option batch
%option never-interactive
%option nostdinit
%option stack

%{
  //---------------------------------------------------------------------
  // List of states
  //---------------------------------------------------------------------
%}

%x str

%{
#include "config.h"

#define YYSTYPE std::string

#include <cstdio>
#include <iostream>
#include <string>

#include "general/hash_map.hh"

#include "ActionLexer.hh"
#include "ActionParserYY.h"
%}

whitespace [ \t]
letter [a-zA-Z]
number [0-9.Ee+-]

%%

%{
  //---------------------------------------------------------------------
  // str state
  //---------------------------------------------------------------------
%}
<str>{whitespace}+	{
  /* Eat leading white space */;
}
<str>[^ \t\n,)][^\n,)]* {
  yylval = yytext;
  yy_pop_state( );
  return TKN_STRING;
}

%{
  //---------------------------------------------------------------------
  // Default Handler
  //---------------------------------------------------------------------
%}
[\n]			++lineno;
{whitespace}+		/* Eat white space */;
[-+]?[0-9.]{number}*	{
  yylval = yytext;
  return TKN_CONSTANT_NUMERIC;
 }
output {
  mode( MODE_OUTPUT );
  yylval = yytext;
  return TKN_OUTPUT;
}
[A-Za-z_][A-Za-z_0-9:\\-]* {
  yylval = yytext;
  return TKN_IDENTIFIER;
}
[(,] {
  switch( *yytext )
  {
  case '(':
    param_count_reset( );
    break;
  case ',':
    param_count_inc( );
    break;
  }
  if ( next_option_is_string( ) )
  {
    yy_push_state( str );
  }
  return *yytext;
}
[)=;] {
  return *yytext;
}

%%

#include "datacondAPI/config.h"
