/* -*- mode: c++; c-basic-offset: 2; -*- */

%skeleton "lalr1.cc"
%require "2.1a"
%defines
%output="AliasParser.cc"
%file-prefix="AliasParser"
%name-prefix="datacondAPI"
%define "parser_class_name" "AliasParser"

%{
#include "datacondAPI/config.h"

#include <strings.h>

#include <string>
#include <sstream>

   namespace yy
   {
   }

   namespace datacondAPI
   {
     using namespace yy;
     class AliasDriver;
   } // namespace - DatacondAPI
%}


// The parsing context.
%parse-param	{ AliasDriver& Driver }
%lex-param	{ AliasDriver& Driver }
%locations
%debug
%error-verbose

// Symbols.
%union
{
  std::string*	sval;
};
%{
#include "AliasDriver.hh"


#define YY_DECL \
  static datacondAPI::AliasParser::token_type			\
  yylex( datacondAPI::AliasParser::semantic_type* YYLval,	\
	 datacondAPI::AliasParser::location_type* YYLloc,	\
	 datacondAPI::AliasDriver& Driver )

// Declare it for the parser's sake.
YY_DECL;
%}

%token        TKN_END 0
%token <sval> TKN_CONSTANT_NUMERIC
%destructor { delete $$; } TKN_CONSTANT_NUMERIC
%token <sval> TKN_IDENTIFIER
%destructor { delete $$; } TKN_IDENTIFIER

%start aliases
%%

aliases: /* empty */
	| alias_decls
	;

alias_decls: alias
	| alias alias_decls
	;

alias: TKN_IDENTIFIER '=' TKN_IDENTIFIER ';'
	{
	  Driver.AppendAlias( *$1, *$3 );
	}
	;

%%

static datacondAPI::AliasParser::token_type
yylex( datacondAPI::AliasParser::semantic_type* YYLval,
       datacondAPI::AliasParser::location_type* YYLloc,
       datacondAPI::AliasDriver& Driver )
{
  return Driver.Lex( YYLval, YYLloc );
}

namespace datacondAPI
{
  void AliasParser::
  error( const location_type& YYLoc, const std::string& Message )
  {
    std::cerr << "CDBUG: AliasParser::error: Message: " << Message << std::endl;
  }
} // namespace - datacondAPI
