// Wavelet Analysis Tool
// universal data container for wavelet transforms
//
//$Id: WaveletUDT.hh,v 1.3 2006/02/16 16:54:58 emaros Exp $
//$Id: WaveletUDT.hh,v 1.3 2006/02/16 16:54:58 emaros Exp $

#ifndef WAVELET_UDT_HH
#define WAVELET_UDT_HH


#include "TimeSeries.hh"
#include "WaveDWT.hh"

namespace datacondAPI
{
//: UDT Representation of Wavelet
template<class DataType_>
class WaveletUDT : public TimeSeries<DataType_>
{
    
   public:
    
      // constructors
    
      //: Default constructor
      WaveletUDT();
    
      //: Construct WaveletUDT for specific wavelet type 
      //+ default constructor
      explicit WaveletUDT(const wat::Wavelet &w);

      //: Construct from Sequence
      //!param: value - data to initialize the WaveletUDT object
      explicit WaveletUDT(const Sequence<DataType_>& value,
			  const wat::Wavelet &w);
    
      //: Construct from TimeSeries
      //!param: value - data to initialize the WaveletUDT object
      explicit WaveletUDT(const TimeSeries<DataType_>& value,
			  const wat::Wavelet &w);
    
      //: Copy constructor
      //!param: value - object to copy from 
      WaveletUDT(const WaveletUDT<DataType_>& value);

      //: destructor
      virtual ~WaveletUDT();
    
      // UDT required members
    
      //: Duplicate *this on heap
      //!return: WaveletUDT* - duplicate of *this, allocated on heap
      virtual WaveletUDT* Clone() const;

      //: Convert *this to an ILWD
      //!param: Chain - CallChain *this is part of
      //!param: Target - intended destination of ILWD
      //!return: ILwd::LdasElement* - ILWD allocated on heap or null pointer
      virtual ILwd::LdasElement*
      ConvertToIlwd( const CallChain& Chain,
		     datacondAPI::udt::target_type 
		     Target = datacondAPI::udt::TARGET_GENERIC) const;

      // accessors

      //: Get maximum possible level of decompostion
      int getMaxLevel();

      //: Extract wavelet coefficients from specified layer
      //!param: n - layer number
      size_t getLayer(Sequence<DataType_> &, int n);

      //: replace wavelet data for specified layer with data from Sequence
      //!param: n - layer number
      void putLayer(const Sequence<DataType_> &, int n);

      // mutators

      //: initialize wavelet parameters from Wavelet object
      void setWavelet(const wat::Wavelet &w);

      //: Perform n steps of forward wavelet transform
      //!param: n - number of steps (-1 means full decomposition)
      void Forward(int n = -1);

      //: Perform n steps of inverse wavelet transform
      //!param: n - number of steps (-1 means full reconstruction)
      void Inverse(int n = -1);
      
// data members

      //: parameters of wavelet transform
      wat::WaveDWT<DataType_>* pWavelet;

  }; // class WaveletUDT<DataType_>

} // namespace datacondAPI

#endif // WAVELET_UDT_HH
