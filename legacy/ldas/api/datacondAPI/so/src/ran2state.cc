#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ran2state.hh"
#include "random.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

namespace datacondAPI{

  ran2state::ran2state():iv(NTAB)
  {
    idum = -248673140;
    iy = 0;
    idum2 = 123456789;
  }

  ran2state::ran2state(int seed):iv(NTAB)
  {
    idum = seed;
    iy = 0;
    idum2 = 123456789;
  }

  ran2state::ran2state(const ran2state& g)
    : State( g ),
      iv(g.iv)
  {
    idum = g.idum;
    iy = g.iy;
    idum2 = g.idum2;
  }

  //restest state with new seed s
  void ran2state::seed(int s)
  {
    idum = s;
    iy = 0;
    idum2 = 123456789;
    //removes previous values in iv so that state is reset
    iv.resize(NTAB);
  }

  float ran2state::getran()
  {
    	int j;
	long k;
	//the following are now member variables
	//static long idum2=123456789;
	//static long iy=0;
	//static long iv[NTAB];
	float temp;
	
	if(idum<=0)
	{
		if(-(idum)<1)
			idum=1;
		else
			idum= -(idum);
		idum2=idum;
		for(j=NTAB+7;j>=0;j--)
		{
			k=idum/IQ1;
			idum=IA1*(idum-k*IQ1)-k*IR1;
			if(idum<0)
				idum+=IM1;
			if(j<NTAB)
				iv[j]=idum;
		}
		iy=iv[0];
	}
	k=idum/IQ1;
	idum=IA1*(idum-k*IQ1)-k*IR1;
	if(idum<0)
		idum+=IM1;
	k=idum2/IQ2;
	idum2=IA2*(idum2-k*IQ2)-k*IR2;
	if(idum2<0)
		idum2 += IM2;
	j=(int)(iy/NDIV);
	iy=iv[j]-idum2;
	iv[j]=idum;
	if(iy<1)
		iy+=IMM1;
	if((temp=AM1*iy)>RNMX) 
		return RNMX;
	else return temp;
  }

  void ran2state::getran(std::valarray<float>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = getran();
  }

  ran2state* ran2state::Clone() const
  {
    return new ran2state(*this);
  }

  ILwd::LdasElement* ran2state::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");
    return container;
  }

}
