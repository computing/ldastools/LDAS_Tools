#ifndef _RAN3STATE_HH_
#define _RAN3STATE_HH_

#include <cmath>
#include <valarray>

#include "StateUDT.hh"

namespace datacondAPI
{
  class ran3state : public State
  {
  public:
    
    //: default constructor creates state with default seed
    ran3state();

    //: Construct with particular seed
    //!param: int - seed, must be negative
    //!exc: std::invalid_argument - seed >= 0 
    ran3state(int);

    //: Copy Constructor
    //:!param: const ranstate& - state to copy from
    ran3state(const ran3state&);
    
    ~ran3state() {};

    //: reset state with new seed
    //!param: unsigned long - seed, must be negative
    //!exc: std::out_of_range - if seed >= 0 
    void seed(int);

    //: Generate random number
    //!return: returns a random number
    float getran();

    //: Generate a sequence of random numbers
    //!param: std::valarray<float>& - array to be filled with numbers
    //!param: int - size of array (and quantity of generated numbers)
    void getran(std::valarray<float>&, int);

    ran3state* Clone() const;
    
    ILwd::LdasElement* ConvertToIlwd( const CallChain&,
				      udt::target_type Target) const;

  private:

    int idum;
    int inext,inextp;
    std::valarray<long> ma;
    int iff;

  };
}

#endif //_RAN3STATE_HH_
