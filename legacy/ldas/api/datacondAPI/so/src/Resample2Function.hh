#ifndef RESAMPLE2FUNCTION_HH
#define RESAMPLE2FUNCTION_HH

#include "CallChain.hh"

//-----------------------------------------------------------------------------
//: Function support for resample2 action
//
// This class implements the "resample2" function to be used in the action
// section of a Tcl command. The syntax, as it would appear in the action
// sequence in one of the forms:
//
//     [y = ] resample2(x, srate[, z ])
//     [y = ] resample2(x, srate, n[, z ])
//     [y = ] resample2(x, srate, n, beta[, z ])
//
// where y, x are TimeSeries<T>, srate is a a Scalar<double> and
// optional z will be set to a ResampleState on return.
//
// The alternative useage given a pre-existing state variable z is
//
//     [y = ] resample2(x, z)
//
class Resample2Function : public CallChain::Function {
    
public:
    
    //: Constructor
    Resample2Function();
    
    //: Evaluate the resample2 function
    //
    // This performs the resampling. The list of parameters must contain
    // doubles phase and carrier and a valarray of input data.
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;
    
    //: Return the name of the function ("resample2")
    //
    //!return: const std::string& - Returns a reference to the name of
    //+the function
    virtual const std::string& GetName(void) const;

    //-------------------------------------------------------------------------
    //: Validate parameters
    virtual void
    ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
			const CallChain::Params& Params ) const;

private:
};

#endif // RESAMPLE2FUNCTION_HH
