#ifndef EXPANDPOLY_HH
#define EXPANDPOLY_HH

namespace std {

    template<class T> class complex;
    template<class T> class valarray;

}

namespace datacondAPI {

    //
    //: Multiply two polynomials represented by coefficient arrays c
    //+ d, returning result in c
    //
    // The input vectors c and d represent the coefficients of polynomials
    // P and Q of degrees (m - 1) and (n - 1) respectively, 
    // where m = c.size() and n = d.size(), that is,
    // <pre>
    //    P(x) = c[0] + c[1]*x + c[2]*x^2 + ... + c[m-1]*x^(m-1)
    //    Q(x) = d[0] + d[1]*x + d[2]*x^2 + ... + d[n-1]*x^(n-1)
    // </pre>
    // 
    // On output, c contains the coefficients of the degree (m + n - 1)
    // polynomial P*Q
    //
    //!param: c - array of coefficients of a polynomial. On output,
    //+ contains the coefficients of product.
    //!param: d - array of coefficients of a polynomial.
    //
    //!exc: invalid_argument - Thrown if either c or d has zero size.
    template<class T>
    void multiplyPolys(std::valarray<T>& c, const std::valarray<T>& d);

    //
    //: Expand the roots of a real polynomial
    //
    // The input vector r represents the real factors of a polynomial P,
    // which are either linear or quadratic as described below.
    //
    // If r[k] is purely real, then the kth factor of P is (x - r[k]).
    // On the other hand, if r[k] has non-zero imaginary component, then
    // the kth factor is the real quadratic (x^2 - 2Re(r[k])x + |r[k]|^2)
    // or (x - r[k])(x - conj(r[k])).
    //
    // This scheme is chosen so that the reality of P(x) is strictly enforced.
    //
    // The input gain represents an overall (real) factor applied to the
    // polynomial after the roots are expanded - thus, it is the
    // coefficient of the highest-order term. In the case where the
    // the input vector has zero size (ie. the polynomial has no
    // roots) the output is the constant polynomial P(x) = gain, so
    // c.size() == 1 and c[0] = gain.
    //
    //!param: c - result of expanding polynomial with factors described by r.
    //!param: r - factors of a polynomial.
    //!param: gain - overall multiplier of polynomial.
    template<class T>
    void expandPoly(std::valarray<T>& c,
                    const std::valarray<std::complex<T> >& r,
                    const T& gain);

} // namespace datacondAPI

#endif // EXPANDPOLY_HH
