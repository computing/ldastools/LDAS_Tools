#ifndef _HAARFUNCTION_HH_
#define _HAARFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI
{

class HaarFunction : public CallChain::Function
{
public:
  HaarFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

}//end namespace datacondAPI

#endif //_HAARFUNCTION_HH_


