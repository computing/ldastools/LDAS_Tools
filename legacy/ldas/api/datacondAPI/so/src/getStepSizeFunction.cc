// $Id: getStepSizeFunction.cc,v 1.3 2005/12/01 22:54:58 emaros Exp $
//
// "getStepSize" action
//
// stepsize = getStepSize(x);
//
// x: TimeSeries or FrequencySequence
//

#include "datacondAPI/config.h"

#include "TimeSeriesUMD.hh"
#include "FrequencyUMD.hh"
#include "ScalarUDT.hh"

#include "getStepSizeFunction.hh"

using namespace datacondAPI;

namespace {

    getStepSizeFunction static_getStepSizeFunction;

}

const std::string& getStepSizeFunction::GetName() const
{
  static const std::string name("getStepSize");

  return name;
}

getStepSizeFunction::getStepSizeFunction()
    : Function( getStepSizeFunction::GetName() )
{
}

void getStepSizeFunction::Eval(CallChain* chain,
                               const CallChain::Params& params,
                               const std::string& ret) const
{
    double stepSize = 0;

    if (params.size() == 1)
    {
        const udt* const argUdt = chain->GetSymbol(params[0]);

        if (const TimeSeriesMetaData* const p
            = dynamic_cast<const TimeSeriesMetaData*>(argUdt))
        {
            stepSize = p->GetStepSize();
        }
        else if (const FrequencyMetaData* const p
            = dynamic_cast<const FrequencyMetaData*>(argUdt))
        {
            stepSize = p->GetFrequencyDelta();
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         0,
                                      TypeInfoTable.GetName(typeid(*argUdt)),
                  "object derived from TimeSeries<T> or FrequencySequence<T>");
        }
    }
    else
    {
        throw CallChain::BadArgumentCount(GetName(), "1", params.size());
    }
    
    chain->ResetOrAddSymbol( ret, new Scalar<double>(stepSize) );
}
