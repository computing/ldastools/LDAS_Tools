//resolver.cc

#include "datacondAPI/config.h"

#include <complex>
#include <general/toss.hh>
#include <iostream>
#include <memory>   

#include "general/Memory.hh"

#include "resolver.hh"
#include "Resample.hh"
#include "TimeSeries.hh"

using std::unique_ptr;   
   
namespace
{
  const char rcsID[] = "@(#) $Id: resolver.cc,v 1.22 2006/11/27 21:32:14 emaros Exp $:";
}

namespace datacondAPI{
namespace psu
{
  //default constructor
  Resolver::Resolver()
  {
    levels = 0;
  }

  //constructor that takes UDT objects
  Resolver::Resolver(datacondAPI::udt& in, datacondAPI::udt& inLevels)
  {
    if(datacondAPI::udt::IsA<Wavelet>(in))
      {
	unique_ptr<Wavelet>temp((datacondAPI::udt::Cast<Wavelet>(in)).Clone());
	m_resolver_wavelet.reset( temp.release( ) );
      }
    else
      {
	std::string what = "Resolver(udt& in, udt& inLevels) -- first parameter not Wavelet object.";
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }

    if(datacondAPI::udt::IsA<Scalar<int> >(inLevels))
      levels = (datacondAPI::udt::Cast<Scalar<int> >(inLevels)).GetValue();
    else
      {
	std::string what = "Resolver(udt& in, udt& inLevels) -- Second parameter not scalar int object.";
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }

    if(levels <= 0)
      {
	std::string what = "Resolver(udt& in, udt& inLevels) -- Levels must be greater than zero."; 
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }
  }

  //constructor with specifications of type of wavelet and number of levels of decomposition
  Resolver::Resolver(Wavelet& in, int inLevels) 
  {  
    setWavelet(in);

    if (inLevels <= 0)
      {
	std::string what = "Resolver(in inLevels) -- Levels must be greater than zero.";
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }
    else
     levels = inLevels;
  }

  //default destructor
  Resolver::~Resolver()
  {  
  }

  //tells # of levels of decomposition currently set
  int Resolver::
  getLevels() const
  {
      return levels;
  }

  //sets number of levels of decomposition to be used
  void Resolver::
  setLevels(int number)
  {
    if(number <= 0)
      {
	std::string what = "setLevels(int) -- Levels must be greater than zero.";
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }
    else
      levels = number;
  }

  //gets analysis filter coefficients
  void Resolver::
  getWavelet()
  {
    // cout << "inside getWavelet" << endl;
    if (m_resolver_wavelet.get() !=0)
      {
	// cout << "m_resolver_wavelet != 0" << endl;
	m_resolver_wavelet->getL(L);
	m_resolver_wavelet->getH(H);
      }
    else
      {
	std::string what = "getWavelet() -- Wavelet not set.";
	General::toss<std::domain_error>("Resolver", __FILE__, __LINE__, what);	

      }
  }

  //user sets the type of wavelet to be used
  void Resolver::
  setWavelet(Wavelet& in)
  {  
    // cout << "setwavelet called" << endl;
    unique_ptr<Wavelet> temp(in.Clone());
    m_resolver_wavelet.reset( temp.release( ) );
    // cout << "you're at the end of setwavelet" << endl;
  }

  //tells whether wavelet/levels are ready for filtering
  bool Resolver::
  isReady()
  {  
    bool wavelet = false;
    if(L.size() > 0 && H.size() > 0)
      wavelet = true;
 
    bool ready = false;
    if (levels > 0 && wavelet == true)
      {
	ready = true;
      }

    return ready; 
  }

  //applies filter to Sequence data
  template<class T> 
  void Resolver::
  applyFilter(Result<T>& output, const datacondAPI::Sequence<T>& data)
  {
    // cout<< "inside applyfilter" << endl;
   getWavelet();

    //check to see if wavelet and levels are set
    if(isReady() != true)
      {
	std::string what = "applyFilter -- Wavelet and/or levels are not set.";
	General::toss<std::domain_error>("Resolver", __FILE__, __LINE__, what);
      }
    // cout<<"wavelet and levels set"<<endl;

    //check size of input signal (must be a multiple of q)
    int datasize = data.size();
    if(datasize % 2 > 0 || datasize <= 0)
      {
	std::string what = "applyFilter -- Sequence of input data must have an even length greater than zero.";
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }
    //cout<<"input size okay"<<endl;
 
     //create a filter using the coefficients from Wavelet
    Resample Hcoeffs(1, 2, H);
    //cout<<"highpass filter created"<<endl;
    Resample Lcoeffs(1, 2, L);
    //cout<<"lowpass filter created"<<endl;

    datacondAPI::Sequence<T> Hdata; //highpass filtered data
    datacondAPI::udt* pHdata = &Hdata;
    Hcoeffs.apply(pHdata, data); //apply is from Resample
    // cout<<"highpass filter applied to data"<<endl;

    datacondAPI::Sequence<T> Ldata; //lowpass filtered data
    datacondAPI::udt* pLdata = &Ldata;
    Lcoeffs.apply(pLdata, data);
    // cout<<"lowpass filter applied to data"<<endl;

    //store Hdata in Result object
    output.add(Hdata);
    // cout<<"highpass filtered data stored in result"<<endl;
    // cout<<"levels = " << levels << endl;
    levels--;
    // cout << "after the subtraction: levels = " << levels << endl;

    //move down to next level?
    if((levels == 0) || (Ldata.size() < 2))
      {
	// cout << "inside last if, levels = " << levels << endl;
	output.add(Ldata);
	// cout<<"lowpass filtered data stored in result"<<endl;
	output.assignWavelet(m_resolver_wavelet);
	// cout<<"wavelet stored in result"<<endl;
	output.assignHcoeffs(H);
	// cout<<"highpass coeffs stored in result"<<endl;
	output.assignLcoeffs(L);
	// cout<<"lowpass coeffs stored in result"<<endl;
      	return;
      }// end if

    else
      {
	// cout<<"going back for more: level # "<< levels <<endl;
	applyFilter(output, Ldata);
      }

  }//end applyFilter

  //applies filter to data (UDT arguments)
  void Resolver:: 
  apply(datacondAPI::udt*& out, const datacondAPI::udt& in)         
  {  
    // cout << "you're in!" << endl;
    if(out == 0)
      {
	out = new Result<double>;
      }

    //TimeSeries<double>
    if (datacondAPI::udt::IsA<datacondAPI::TimeSeries<double> >(in))
      {
	//cout<<"resolver ==> in is a timeseries double" << endl;
	//filter data as if it is a regular sequence.  
	// cout<< "in is a timeseries double" << endl;
	datacondAPI::Sequence<double> inSeq(datacondAPI::udt::Cast<datacondAPI::TimeSeries<double> >(in));
	
	// cout<<"in = inSeq" << endl;
	Resolver::applyFilter(datacondAPI::udt::Cast<Result<double> >(*out), inSeq);
	// cout << "applyFilter executed."<< endl;

	//copy metadata of incoming TimeSeries to Result object
	(datacondAPI::udt::Cast<Result<double> >(*out)).setMetaData(datacondAPI::udt::Cast<datacondAPI::TimeSeries<double> >(in));
	// cout << "metadata transferred" << endl;
		
      }
    
    //TimeSeries<float>
    else if (datacondAPI::udt::IsA<datacondAPI::TimeSeries<float> >(in))
      {
	//cout<<"resolver ==> in is a timeseries float" << endl;
	//filter data as if it is a regular sequence.  
	datacondAPI::Sequence<float> inSeq(datacondAPI::udt::Cast<datacondAPI::TimeSeries<float> >(in));

	Resolver::applyFilter(datacondAPI::udt::Cast<Result<float> >(*out), inSeq);

	//copy metadata of incoming TimeSeries to Result object
	datacondAPI::udt::Cast<Result<float> >(*out).setMetaData(datacondAPI::udt::Cast<datacondAPI::TimeSeries<float> >(in));
		
      }
    
    //Sequence<double>
    else if(datacondAPI::udt::IsA<datacondAPI::Sequence<double> >(in))
      {
	//cout<<"resolver == in is a sequence double" << endl;
	Resolver::applyFilter(datacondAPI::udt::Cast<Result<double> >(*out), 
			      datacondAPI::udt::Cast<datacondAPI::Sequence<double> >(in));
      }

    //Sequence<float>
    else if(datacondAPI::udt::IsA<datacondAPI::Sequence<float> >(in))
      {
	//cout<<"resolver == in is a sequence float" << endl;
	Resolver::applyFilter(datacondAPI::udt::Cast<Result<float> >(*out), 
			      datacondAPI::udt::Cast<datacondAPI::Sequence<float> >(in));
      }
    else 
      {
	std::string what = "apply(udt*&, udt&) -- Unsupported type cast in Resolver";
	General::toss<std::invalid_argument>("Resolver", __FILE__, __LINE__, what);
      }

  }

  //convert *this to appropriate ilwd type
  ILwd::LdasElement*
  Resolver::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    std::string what = "tried to call ConvertToIlwd";
    General::toss<General::unimplemented_error>("Resolver", __FILE__, __LINE__, what);
    return 0;
  }

  //copy constructor
  Resolver* Resolver::
  Clone() const
  {
    return new Resolver(*this);
  }

} //namespace psu
} //namespace datacondAPI

#undef INSTANTIATE
#define INSTANTIATE(TYPE)\
template void datacondAPI::psu::Resolver::applyFilter (datacondAPI::psu::Result< TYPE >& out, const datacondAPI::Sequence< TYPE >& data);
INSTANTIATE(float)
INSTANTIATE(double)

  /*
#define INSTANTIATE1(TYPE1)\
template void datacondAPI::psu::Resolver::TSapplyFilter (datacondAPI::psu::Result< TYPE1 >& out, const datacondAPI::TimeSeries< TYPE1 >& data);
INSTANTIATE1(float)
INSTANTIATE1(double)
  */
