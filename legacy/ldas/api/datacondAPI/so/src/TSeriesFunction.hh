/* -*- mode: c++ -*- */
#ifndef TSERIESFUNCTION_HH
#define TSERIESFUNCTION_HH

#include "CallChain.hh"

//=======================================================================
// Sequence -> TimeSeries conversion 
//=======================================================================

class TSeriesFunction : public CallChain::Function {
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  TSeriesFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* chain,
                    const CallChain::Params& params,
                    const std::string& ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName() const;
};

#endif // TSERIESFUNCTION_HH

