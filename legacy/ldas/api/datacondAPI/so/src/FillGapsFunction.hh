#ifndef FILLGAPSFUNCTION_HH
#define FILLGAPSFUNCTION_HH

#include "CallChain.hh"

class FillGapsFunction : CallChain::Function
{
public:
    //: Default constructor - construction of dummy instance registers  
    //+ the Eval method as the handler for calls to the action named by 
    //+ the GetName method
    FillGapsFunction();
    
    //: Evaluate an action call
    //!param: Chain - environment of the action call
    //!param: Params - container of parameter names
    //!param: Ret - return value name
    virtual void Eval(CallChain* Chain,
		      const CallChain::Params& Params, 
                      const std::string& Ret) const;

    //: Get the name of the handled action
    //!return: The name of the handled action
    virtual const std::string& GetName() const;
};

#endif // FILLGAPSFUNCTION_HH
