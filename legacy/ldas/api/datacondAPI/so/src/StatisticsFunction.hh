/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef STATISTICSFUNCTION_HH
#define	STATISTICSFUNCTION_HH

#include "CallChain.hh"

class StatsFunctionBase : public CallChain::Function {
public:
    StatsFunctionBase(const std::string& name)
        : CallChain::Function(name) { }

protected:
    template<class Op>
    void evaluate(CallChain* const chain,
                  const CallChain::Params& params,
                  const std::string& ret,
                  const Op& op) const;
};

class SizeFunction : public StatsFunctionBase {
public:
    SizeFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class SumFunction : public StatsFunctionBase {
public:
    SumFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class MinFunction : public StatsFunctionBase {
public:
    MinFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class MaxFunction : public StatsFunctionBase {
public:
    MaxFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class MeanFunction : public StatsFunctionBase {
public:
    MeanFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class RmsFunction : public StatsFunctionBase {
public:
    RmsFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class VarianceFunction : public StatsFunctionBase {
public:
    VarianceFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class SkewnessFunction : public StatsFunctionBase {
public:
    SkewnessFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class KurtosisFunction : public StatsFunctionBase {
public:
    KurtosisFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

class AllFunction : public StatsFunctionBase {
public:
    AllFunction();
    
    virtual void Eval(CallChain* chain,
		      const CallChain::Params& params,
		      const std::string& ret) const;
    
    virtual const std::string& GetName() const;
};

#endif // STATISTICSFUNCTION_HH
