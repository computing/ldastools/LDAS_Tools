#ifndef OEMODEL_HH
#define OEMODEL_HH

#include <valarray>
#include <stdexcept>

#include "StateUDT.hh"

namespace datacondAPI
{

    class OEModel : public State
    {

    public:

	// default overrides

	OEModel();
	OEModel(const OEModel& oe);

	~OEModel();

	OEModel& operator=(const OEModel& oe);

	// UDT interface

	OEModel* Clone() const;

	ILwd::LdasElement*
	    ConvertToIlwd(const CallChain& Chain,
			  udt::target_type Target = udt::TARGET_GENERIC) const;

	// custom constructors

	// from model orders (order_f = order_b if not supplied)

	OEModel(const int& order_b);
	OEModel(const int& order_b, const int& order_f);

	// from model parameters

	template<typename type>
	explicit OEModel(const std::valarray<type>& theta, const int& order_b);

	explicit OEModel(const udt& order_b);

	OEModel(const udt& order_b_or_theta, const udt& order_f_or_order_b);

	// with estimated parameters for model orders

	template<typename type>
	OEModel(const std::valarray<type>& y, const std::valarray<type>& u, const int& order_b);
	template<typename type>
	OEModel(const std::valarray<type>& y, const std::valarray<type>& u, const int& order_b, const int& order_f);

	OEModel(const udt& y, const udt& u, const udt& order_b);
	OEModel(const udt& y, const udt& u, const udt& order_b, const udt& order_f);

	// accessors

	int getOrderB() const;
	int getOrderF() const;

	void getOrderB(udt*& order_b) const;
	void getOrderF(udt*& order_f) const;

	template<typename type>
	void getTheta(std::valarray<type>& theta) const;

	void getTheta(udt*& theta) const;

	void getFilterB(udt*& filter_b) const;
	void getFilterF(udt*& filter_f) const;

    template<typename type>
    void getFilterB(std::valarray<type>&) const;
    template<typename type>
    void getFilterF(std::valarray<type>&) const;

	// mutators

	void setOrderB(const int& order_b);
	void setOrderF(const int& order_f);

	void setOrderB(const udt& order_b);
	void setOrderF(const udt& order_f);

	template<typename type>
	void setTheta(const std::valarray<type>& theta, const int& order_b);

	void setTheta(const udt& theta, const udt& order_b);

	// functionality

	template<typename type>
	void apply(std::valarray<type>& w,
		   const std::valarray<type>& u) const;

	void apply(udt*& w,
		   const udt& u) const;

	template<typename type>
	void refine(const std::valarray<type>& y, const std::valarray<type>& u);

	void refine(const udt& y, const udt& u);

	template<typename type>
	type merit(const std::valarray<type>& y, const std::valarray<type>& u) const;

	void merit(udt*& m, const udt& y, const udt& u) const;

    private:

	class Abstraction;
	template<typename type> class Implementation;

	int m_order_b;
	int m_order_f;
	mutable Abstraction* m_data;

    };

} // namespace datacondAPI

#endif // OEMODEL_HH
