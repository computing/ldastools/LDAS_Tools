/* -*- mode: c++ -*- */
#ifndef WAVELET_FUNCTIONS_HH
#define WAVELET_FUNCTIONS_HH

#include "CallChain.hh"

//=======================================================================
// create a wavelet object of type Haar 
//=======================================================================

class HaarFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  HaarFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};


//=======================================================================
// create a wavelet object of type Biorthogonal 
//=======================================================================

class BiorthogonalFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  BiorthogonalFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};


//=======================================================================
// create a wavelet object of type Daubechies 
//=======================================================================

class DaubechiesFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  DaubechiesFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};


//=======================================================================
// create a wavelet object of type Symlet 
//=======================================================================

class SymletFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  SymletFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};


//=======================================================================
// Wavelet forward transform action: 
// wudt=WaveletForward(TimeSeries x, Wavelet w, int layer) 
//=======================================================================

class WaveletForwardFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  WaveletForwardFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

//=======================================================================
// Wavelet Inverse transform action: 
// TimeSeries y = WaveletInverse(WaveletUDT &x) 
//=======================================================================

class WaveletInverseFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  WaveletInverseFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};


//=======================================================================
// get wavelet layer action: 
// Sequence y = GetLayer(WaveletUDT &x) 
//=======================================================================

class GetLayerFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  GetLayerFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

#endif  /* WAVELET_FUNCTIONS_HH */







