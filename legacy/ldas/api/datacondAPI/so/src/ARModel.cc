#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>

#include "ARModel.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "Matrix.hh"
#include "LinearAlgebra.hh"

namespace datacondAPI
{
    class ARModel::Abstraction
    {

    public:

	// default overrides

	virtual ~Abstraction() = 0;

	// copying

	virtual Abstraction* clone() const = 0;

	// accessors

	virtual void getTheta(udt*&) const = 0;
	virtual void getFilter(udt*&) const = 0;

	// methods

	virtual void apply(udt*& y, const udt& x) const = 0;
	virtual void refine(const udt& x) = 0;
	virtual void merit(udt*& m, const udt& x) const = 0;

    protected:

	// default overrides

	Abstraction();
	Abstraction(const Abstraction&);
	Abstraction& operator=(const Abstraction&);
	
    private:

    };

    template<typename type> class ARModel::Implementation : public ARModel::Abstraction
    {

    public:

	// default overrides

	Implementation();
	Implementation(const Implementation&);
	
	~Implementation();

	Implementation& operator=(const Implementation&);

	// custom construction

	explicit Implementation(const int& n);
	explicit Implementation(const std::valarray<type>& theta);
	Implementation(const std::valarray<type>& x,
		       const int& n);

	// copying

	Implementation* clone() const;

	// mutators

	void getTheta(std::valarray<type>&) const;

	void getTheta(udt*&) const;
	void getFilter(udt*&) const;

	// methods
	
	void apply(std::valarray<type>& y,
		   const std::valarray<type>& x) const;
	void apply(udt*& y, const udt& x) const;

	void refine(const std::valarray<type>& x);
	void refine(const udt& x);
	
	type merit(const std::valarray<type>& x) const;
	void merit(udt*& m, const udt& x) const;

    private:
	
	// model state

	std::valarray<type> m_theta;
	mutable std::valarray<type> m_state;
	
	// estimator state

	Matrix<type> m_matrix;
	Matrix<type> m_vector;
	std::valarray<type> m_history;
	
    };

    ARModel::ARModel()
	: m_order(0),
	  m_data(0)
    {
    }

    ARModel::ARModel(const ARModel& ar)
	: m_order(ar.m_order),
	  m_data(ar.m_data ? ar.m_data->clone() : 0)
    {
    }

    ARModel::~ARModel()
    {
	delete m_data;
    }

    ARModel& ARModel::operator=(const ARModel& ar)
    {
	if (&ar != this)
	{
	    m_order = ar.m_order;
	    delete m_data;
	    m_data = ar.m_data->clone();
	}
	return *this;
    }

    ARModel* ARModel::Clone() const
    {
	return new ARModel(*this);
    }

    ILwd::LdasElement*
        ARModel::ConvertToIlwd(const CallChain& Chain,
		      udt::target_type Target) const
    {
	throw General::unimplemented_error("ARModel::ConvertToIlwd is unimplemented");
    }

    ARModel::ARModel(const int& n)
	: m_order(n),
	  m_data(0)
    {
    }

    template<typename type>
    ARModel::ARModel(const std::valarray<type>& theta)
	: m_order(theta.size()),
	  m_data(new Implementation<type>(theta))
    {
    }

    ARModel::ARModel(const udt& n_or_theta)
	: m_order(0),
	  m_data(0)
    {
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&n_or_theta))
	{
	    m_order = p->GetValue();
	}
	else if (const std::valarray<float>* p
		 = dynamic_cast<const std::valarray<float>*>(&n_or_theta))
	{
	    m_order = p->size();
	    m_data = new Implementation<float>(*p);
	}
	else if (const std::valarray<double>* p
		 = dynamic_cast<const std::valarray<double>*>(&n_or_theta))
	{
	    m_order = p->size();
	    m_data = new Implementation<double>(*p);
	}
	else if (const std::valarray<std::complex<float> >* p
		 = dynamic_cast<const std::valarray<std::complex<float> >*>(&n_or_theta))
	{
	    m_order = p->size();
	    m_data = new Implementation<std::complex<float> >(*p);
	}
	else if (const std::valarray<std::complex<double> >* p
		 = dynamic_cast<const std::valarray<std::complex<double> >*>(&n_or_theta))
	{
	    m_order = p->size();
	    m_data = new Implementation<std::complex<double> >(*p);
	}
	else
	{
	    throw std::invalid_argument("ARModel::ARModel(const udt&): requires scalar integer or valarray");
	}
    }
    
    template<typename type>
    ARModel::ARModel(const std::valarray<type>& x,
		     const int& n)
	: m_order(n),
	  m_data(new Implementation<type>(x, n))
    {
    }
    
    ARModel::ARModel(const udt& x, const udt& n)
    {
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&n))
	{
	    m_order = p->GetValue();

	    if (const std::valarray<float>* q
		= dynamic_cast<const std::valarray<float>*>(&x))
	    {
		m_data = new Implementation<float>(*q, m_order);
	    }
	    else if (const std::valarray<double>* q
		     = dynamic_cast<const std::valarray<double>*>(&x))
	    {
		m_data = new Implementation<double>(*q, m_order);
	    }
	    else if (const std::valarray<std::complex<float> >* q
		     = dynamic_cast<const std::valarray<std::complex<float> >*>(&x))
	    {
		m_data = new Implementation<std::complex<float> >(*q, m_order);
	    }
	    else if (const std::valarray<std::complex<double> >* q
		     = dynamic_cast<const std::valarray<std::complex<double> >*>(&x))
	    {
		m_data = new Implementation<std::complex<double> >(*q, m_order);
	    }
	    else
	    {
		throw std::invalid_argument("ARModel::ARModel(const udt&, const udt&): requres argument 1 a valarray");
	    }
	}
	else
	{
	    throw std::invalid_argument("ARModel::ARModel(const udt&, const udt&): requires argument 2 a scalar integer");
	}
    }

    int ARModel::getOrder() const
    {
	return m_order;
    }

    void ARModel::getOrder(udt*& n) const
    {
	if (n == 0)
	{
	    n = new Scalar<int>(m_order);
	}
	else if (Scalar<int>* p
		 = dynamic_cast<Scalar<int>*>(n))
	{
	    (int&)(*p) = m_order;
	}
	else
	{
	    throw std::invalid_argument("ARModel::getOrder(udt*&): requires a scalar integer");
	}
    }

    template<typename type>
    void ARModel::getTheta(std::valarray<type>& theta) const
    {
	if (Implementation<type>* p
	    = dynamic_cast<Implementation<type>*>(m_data))
	{
	    p->getTheta(theta);
	}
	else
	{
	    throw std::invalid_argument("type mismatch");
	}
	
    }

    void ARModel::getTheta(udt*& theta) const
    {
	if (m_data)
	{
	    m_data->getTheta(theta);
	}
	else
	{
	    throw std::invalid_argument("ARModel::getTheta(udt*&): no model");
	}
    }

    void ARModel::getFilter(udt*& filter) const
    {
	if (m_data)
	{
	    m_data->getFilter(filter);
	}
	else
	{
	    throw std::invalid_argument("ARModel::getFilter(udt*&): no model");
	}
    }

    void ARModel::setOrder(const int& n)
    {
	m_order = n;
	delete m_data;
	m_data = 0;
    }

    void ARModel::setOrder(const udt& n)
    {
	if (const Scalar<int>* p
	    = dynamic_cast<const Scalar<int>*>(&n))
        {
	    m_order = p->GetValue();
	    delete m_data;
	    m_data = 0;
	}
	else
        {
	    throw std::invalid_argument("ARModel::setOrder(const udt&): requires a scalar integer");
	}
    }   

    template<typename type>
    void ARModel::setTheta(const std::valarray<type>& theta)
    {
	m_order = theta.size();
	delete m_data;
	m_data = new Implementation<type>(theta);
    }

    void ARModel::setTheta(const udt& theta)
    {
	if (const std::valarray<float>* p = dynamic_cast<const std::valarray<float>*>(&theta))
	{
	    m_order = p->size();
	    delete m_data;
	    m_data = new Implementation<float>(*p);
	}
	else if (const std::valarray<double>* p = dynamic_cast<const std::valarray<double>*>(&theta))
	{
	    m_order = p->size();
	    delete m_data;
	    m_data = new Implementation<double>(*p);
	}
	else if (const std::valarray<std::complex<float> >* p = dynamic_cast<const std::valarray<std::complex<float> >*>(&theta))
	{
	    m_order = p->size();
	    delete m_data;
	    m_data = new Implementation<std::complex<float> >(*p);
	}
	else if (const std::valarray<std::complex<double> >* p = dynamic_cast<const std::valarray<std::complex<double> >*>(&theta))
	{
	    m_order = p->size();
	    delete m_data;
	    m_data = new Implementation<std::complex<double> >(*p);
	}
	else
	{
	    throw std::invalid_argument("ARModel::setTheta(const udt&): invalid type");
	}
    }

    template<typename type>
    void ARModel::apply(std::valarray<type>& y,
			const std::valarray<type>& x) const
    {
	if (const Implementation<type>* p
	    = dynamic_cast<const Implementation<type>*>(m_data))
	{
	    p->apply(y,x);
	}
	else
	{
	    throw std::invalid_argument("type mismatch");
	}
    }

    void ARModel::apply(udt*& y, const udt& x) const
    {
	if (m_data != 0)
	{
	    m_data->apply(y, x);
	}
	else
	{
	    throw std::invalid_argument("ARModel::apply(udt*&, const udt&): no model");
	}
    }

    template<typename type>
    void ARModel::refine(const std::valarray<type>& x)
    {
	if (m_data == 0)
	{
	    m_data = new Implementation<type>(x, m_order);
	}
	else if (Implementation<type>* p
		 = dynamic_cast<Implementation<type>*>(m_data))
	{
	    p->refine(x);
	}
	else
	{
	    throw std::invalid_argument("type mismatch");
	}
    }

    void ARModel::refine(const udt& x)
    {
	if (m_data != 0)
	{
	    m_data->refine(x);
	}
	else if (const std::valarray<float>* p
		 = dynamic_cast<const std::valarray<float>*>(&x))
	{
	    m_data = new Implementation<float>(*p, m_order);
	}
	else if (const std::valarray<double>* p
		 = dynamic_cast<const std::valarray<double>*>(&x))
	{
	    m_data = new Implementation<double>(*p, m_order);
	}
	else if (const std::valarray<std::complex<float> >* p
		 = dynamic_cast<const std::valarray<std::complex<float> >*>(&x))
	{
	    m_data = new Implementation<std::complex<float> >(*p, m_order);
	}
	else if (const std::valarray<std::complex<double> >* p
		 = dynamic_cast<const std::valarray<std::complex<double> >*>(&x))
	{
	    m_data = new Implementation<std::complex<double> >(*p, m_order);
	}
	else
	{
	    throw std::invalid_argument("ARModel::refine: type mismatch");
	}
    }

    template<typename type>
    type ARModel::merit(const std::valarray<type>& x) const
    {
	if (m_data != 0)
	{
	    if (const Implementation<type>* p
		= dynamic_cast<const Implementation<type>*>(m_data))
	    {
		return p->merit(x);
	    }
	    else
	    {
		throw std::invalid_argument("ARModel::merit(const std::valarray<type>&): type mismatch");
	    }
	}
	else
	{
	    throw std::invalid_argument("ARModel::merit(const std::valarray<type>&): no model");
	}
    }

    void ARModel::merit(udt*& m, const udt& x) const
    {
	if (m_data != 0)
	{
	    m_data->merit(m, x);
	}
	else
	{
	    throw std::invalid_argument("ARModel::merit(udt*&, const udt&): no model");
	}
    }

    ARModel::Abstraction::~Abstraction()
    {
    }

    ARModel::Abstraction::Abstraction()
    {
    }

    ARModel::Abstraction::Abstraction(const Abstraction&)
    {
    }

    ARModel::Abstraction& ARModel::Abstraction::operator=(const Abstraction& abstraction)
    {
	if (&abstraction != this)
	{
	}
	return *this;
    }

    template<typename type>
    ARModel::Implementation<type>::Implementation()
	: m_theta(),
	  m_state(),
	  m_matrix(),
	  m_vector(),
	  m_history()
    {
    }

    template<typename type>
    ARModel::Implementation<type>::Implementation(const Implementation& implementation)
        : Abstraction( implementation ),
	  m_theta(implementation.m_theta),
	  m_state(implementation.m_state),
	  m_matrix(implementation.m_matrix),
	  m_vector(implementation.m_vector),
	  m_history(implementation.m_history)
    {
    }

    template<typename type>
    ARModel::Implementation<type>::~Implementation()
    {
    }

    template<typename type>
    ARModel::Implementation<type>& ARModel::Implementation<type>::operator=(const Implementation<type>& implementation)
    {
	if (&implementation != this)
	{
	    m_theta.resize(implementation.m_theta.size());
	    m_theta = implementation.m_theta;
	    m_state.resize(implementation.m_theta.size());
	    m_state = implementation.m_state;
	    m_matrix.resize(implementation.m_theta.size(),
			    implementation.m_theta.size());
	    m_matrix = implementation.m_matrix;
	    m_vector.resize(implementation.m_theta.size(),
			    1);
	    m_vector = implementation.m_vector;
	    m_history.resize(implementation.m_theta.size());
	    m_history = implementation.m_history;
	}
	return *this;
    }

    template<typename type>
    ARModel::Implementation<type>::Implementation(const int& n)
	: m_theta(type(), n),
	  m_state(type(), n),
	  m_matrix(n,n),
	  m_vector(n, 1),
	  m_history(type(), n)
    {
    }

    template<typename type>
    ARModel::Implementation<type>::Implementation(const std::valarray<type>& theta)
	: m_theta(theta),
	  m_state(type(), theta.size()),
	  m_matrix(theta.size(), theta.size()),
	  m_vector(theta.size(), 1),
	  m_history(type(), theta.size())
    {
    }

    template<typename type>
    ARModel::Implementation<type>::Implementation(const std::valarray<type>& x,
						  const int& n)
	: m_theta(type(), n),
	  m_state(type(), n),
	  m_matrix(n, n),
	  m_vector(n, 1),
	  m_history(type(), n)
    {
	refine(x);
    }

    template<typename type>
    ARModel::Implementation<type>* ARModel::Implementation<type>::clone() const
    {
	return new Implementation<type>(*this);
    }

    template<typename type>
    void ARModel::Implementation<type>::getTheta(std::valarray<type>& theta) const
    {
	theta.resize(m_theta.size());
	theta = m_theta;
    }

    template<typename type>
    void ARModel::Implementation<type>::getTheta(udt*& theta) const
    {
	if (std::valarray<type>* p = dynamic_cast<std::valarray<type>*>(theta))
	{
	    p->resize(m_theta.size());
	    (*p) = m_theta;
	}
	else if (theta == 0)
	{
	    theta = new Sequence<type>(m_theta);
	}
	else
	{
	    throw std::invalid_argument("ARModel::Implementation::getTheta(udt*& theta): requres null or a pointer to a sequence UDT");
	}       
    }

    template<typename type>
    void ARModel::Implementation<type>::getFilter(udt*& filter) const
    {
	if (filter == 0)
	{
	    filter = new Sequence<type>;
	}
	if (std::valarray<type>* p = dynamic_cast<std::valarray<type>*>(filter))
	{
	    p->resize(m_theta.size() + 1);
	    (*p)[0] = type();
	    (*p)[std::slice(1, m_theta.size(), 1)] = m_theta;
	}
	else
	{
	    throw std::invalid_argument("ARModel::Implementation::getTheta(udt*& theta): requres null or a pointer to a sequence UDT");
	}       
    }

    template<typename type>
    void ARModel::Implementation<type>::apply(std::valarray<type>& y,
					      const std::valarray<type>& x) const
    {
	y.resize(x.size());

	if (m_theta.size() > 0)
	{
	    for (unsigned int t = 0; t < x.size(); ++t)
	    {
		y[t] = m_state[0];
		
		m_state = m_state.shift(1);
		m_state += m_theta * x[t];
	    }
	}
    }

    template<typename type>
    void ARModel::Implementation<type>::apply(udt*& y, const udt& x) const
    {
	if (const std::valarray<type>* p = dynamic_cast<const std::valarray<type>*>(&x))
	{
	    if (y == 0)
	    {
		y = new Sequence<type>;
	    }
	    if (std::valarray<type>* q = dynamic_cast<std::valarray<type>*>(y))
	    {
		apply(*q, *p);
	    }
	    else
	    {
		throw std::invalid_argument("ARModel::Implementation<type>::apply(udt*&, const udt&): (output) type mismatch");
	    }
	}
	else
	{
	    throw std::invalid_argument("ARModel::Implementation<type>::apply(udt*&, const udt&): (input) type mismatch");
	}
    }
    static inline std::valarray<float> conj(const std::valarray<float>& x)
    {
	return x;
    }

    static inline std::valarray<double> conj(const std::valarray<double>& x)
    {
	return x;
    }

    static inline std::valarray<std::complex<float> > conj(const std::valarray<std::complex<float> >& x)
    {
      typedef std::complex<float> i_type;
	i_type (*func)( const i_type& ) = std::conj;
	return x.apply(func);
    }

    static inline std::valarray<std::complex<double> > conj(const std::valarray<std::complex<double> >& x)
    {
      typedef std::complex<double> i_type;
	i_type (*func)( const i_type& ) = std::conj;
	return x.apply(func);
    }


    template<typename type>
    void ARModel::Implementation<type>::refine(const std::valarray<type>& x)
    {
	// Solve Ax = b for x where
	//   A = \sum_{t=1}^N \psi(t)\psi^T(t)
	//   b = \sum_{t=1}^N \psi(t)y(t)
	// and
	//   \psi(t) = [ y(t-1) \ldots y(t - n) ]^T

	for (unsigned int t = 0; t < x.size(); ++t)
	{
	    // accumulate A
	    for (unsigned int i = 0; i < m_theta.size(); ++i)
	    {
		m_matrix.setCol(i, m_matrix.getCol(i) + (conj(m_history) * m_history[i]));
	    }

	    // accumulate b
	    m_vector.setCol(0, m_vector.getCol(0) + (conj(m_history) * x[t]));

	    // rotate psi
	    m_history = m_history.shift(-1);
	    m_history[0] = x[t];

	}

	// output buffer

	try
	{
	    Matrix<type> buffer;
	    LinearAlgebra::SV(m_matrix, buffer, m_vector);
	    
	    m_theta = buffer.getCol(0);
	}
	catch(std::exception& x)
	{
	    throw std::runtime_error(std::string("ARModel::Implementation<type>::refine(x): intercepted exception: ") + x.what());
	}

    }

    template<typename type>
    void ARModel::Implementation<type>::refine(const udt& x)
    {
	if (const std::valarray<type>* p = dynamic_cast<const std::valarray<type>*>(&x))
	{
	    refine(*p);
	}
	else
	{
	    throw std::invalid_argument("ARModel::Implementation<type>::refine(const udt&): type mismatch");
	}
    }

    template<typename type>
    type ARModel::Implementation<type>::merit(const std::valarray<type>& x) const
    {
	std::valarray<type> y;
	Implementation<type>(*this).apply(y, x);
	y -= x;
	y *= y;
	return y.sum() / (type) y.size();
    }

    template<typename type>
    void ARModel::Implementation<type>::merit(udt*& m, const udt& x) const
    {
	if (const std::valarray<type>* p = dynamic_cast<const std::valarray<type>*>(&x))
	{
	    if (m == 0)
	    {
		m = new Scalar<type>(merit(*p));
	    }
	    else if (Scalar<type>* q = dynamic_cast<Scalar<type>*>(m))
	    {
		q->SetValue(merit(*p));
	    }
	    else
	    {
		throw std::invalid_argument("ARModel::Implementation<type>::merit(udt*&, const udt&): (output) type mismatch");
	    }
	}
	else
	{
	    throw std::invalid_argument("ARModel::Implementation<type>::merit(udt*&, const udt&): (input) type mismatch");
	}
    }

    // instantiations

#define INSTANTIATE(type) \
\
    template ARModel::ARModel(const std::valarray< type >&); \
    template ARModel::ARModel(const std::valarray< type >&, \
                              const int&); \
    template void ARModel::getTheta(std::valarray< type >&) const; \
    template void ARModel::setTheta(const std::valarray< type >&); \
    template void ARModel::apply(std::valarray< type >&, \
                                 const std::valarray< type >&) const; \
    template void ARModel::refine(const std::valarray< type >&); \
    template type ARModel::merit(const std::valarray< type >&) const; \
    template class ARModel::Implementation< type > ;

    INSTANTIATE(float)
    INSTANTIATE(double)
    INSTANTIATE(std::complex<float>)
    INSTANTIATE(std::complex<double>)

#undef INSTANTIATE

} // namespace datacondAPI

UDT_CLASS_INSTANTIATION_CAST(ARModel)
