//result.hh

#ifndef RESULT_HH
#define RESULT_HH

#include <vector>
#include <memory>

#include <general/unexpected_exception.hh>
#include "general/Memory.hh"

#include "TimeSeries.hh"
#include "WaveletPSU.hh"

namespace datacondAPI{
  namespace psu
{

  template<class T >
  class Result : public datacondAPI::udt

  {

    friend class Resolver;
    //Resolver has "friend class Result"
    
  public:

    //: Default constructor
    Result();

    //: Default destructor
    ~Result();

    //: returns the integer number of levels of decomposition
    //!return: int number of levels of decomposition
    int getLevels() const;

    //: gets wavelet used to get Result
    //!param: Resolver& in
    void returnWavelet(std::unique_ptr<Wavelet>&) const;

    //: returns sequence of highpass (analysis) coefficients used
    datacondAPI::Sequence<double> returnH() const;

    //: returns sequence of lowpass (analysis) coefficients used
    datacondAPI::Sequence<double> returnL() const;

    //: gets detail for specified level of decomposition
    //!param: int level - level of decomposition
    //!return: data (datacondAPI::Sequences) stored in Result object
    datacondAPI::Sequence<T> getResult(const int) const;

    //: gets detail or approximation for specified level of decomposition
    //!param: datacondAPI::udt*& Wcoeffs - TimeSeries or Sequence containing output detail or approximation
    //!param: const datacondAPI::udt& level - level of decomposition
    void getResult(datacondAPI::udt*&, const datacondAPI::udt&) const;

    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;

    Result* Clone() const;

    //: Copy constructor
    //!param: const Result& rslt - instance to be copied
    Result(const Result& rslt);

    //: if original data is TimeSeries, sets member data to metadata from TimeSeries
    //!param: datacondAPI::TimeSeries<T> data - original TimeSeries data
    void setMetaData(const datacondAPI::TimeSeries<T>& data);
    
    //: transferrs metadata from Result to TimeSeries<T> object
    //!param: datacondAPI::TimeSeries<T> md - TimeSeries to which metadata is copied
    void returnMetaData(datacondAPI::TimeSeries<T>& md) const;

    //: checks to see whether or not metadata is null
    //!return: bool - true if metadata is NOT null, false if metadata IS null
    bool checkMetaData() const;

  private:

    //: adds the next set of filtered signal to result object
    //!param: datacondAPI::Sequence<double> input - filtered signal from speicific level of decomposition
    void add(datacondAPI::Sequence<T>&);

    //: assigns to Result member the wavelet used in Resolver
    //!param: autoptr<Wavelet> in - wavelet from Resolver to be assigned to m_result_wavelet
    void assignWavelet(std::unique_ptr<Wavelet>&);

    //: assigns highpass analysis filter coefficients to Hcoeffs
    //!param: datacondAPI::Sequence<double> highCoeffs - highpass analysis filter coefficients
    void assignHcoeffs(datacondAPI::Sequence<double>);

    //: assigns lowpass analysis filter coefficients to Lcoeffs
    //!param: datacondAPI::Sequence<double> lowCoeffs - lowpass analysis filter coefficients
    void assignLcoeffs(datacondAPI::Sequence<double>);

  //MEMBER DATA:

    //: vector to store filtered signal at each level
    std::vector< datacondAPI::Sequence<T> > fSignal;

    //: highpass filter coefficients
    datacondAPI::Sequence<double> Hcoeffs;

    //: lowpass filter coefficients
    datacondAPI::Sequence<double> Lcoeffs;

    //: wavelet used in decomposition
    std::unique_ptr<Wavelet> m_result_wavelet;

    //: metadata from TimeSeries
    std::unique_ptr<datacondAPI::TimeSeries<T> > metadata;

  }; //class Result
  
  } //namespace psu
} //namespace datacondAPI

#endif
