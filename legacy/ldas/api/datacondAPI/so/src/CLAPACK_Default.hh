#ifndef CLAPACK_DEFAULT_HH
#define CLAPACK_DEFAULT_HH

#include "CLAPACK_Types.hh"

/*=======================================================================
                   F u n c t i o n   p r o t o t y p e s
  =======================================================================*/

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                           C B L A S
 *:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

/*-----------------------------------------------------------------------*/

#ifndef GEMM_PROTOTYPE
#define GEMM_PROTOTYPE(TYPE_PTR,TYPE)		 \
  char* TransA, char* TransB,			 \
    integer* M, integer* N, integer* K,		 \
    TYPE* Alpha, TYPE_PTR* A, integer* lda,	 \
    TYPE_PTR* B, integer* ldb,			 \
    TYPE* Beta, TYPE_PTR* C, integer* ldc
#endif /* GEMM_PROTOTYPE */

#ifndef GEMM_CPLX_PROTOTYPE
#define GEMM_CPLX_PROTOTYPE(TYPE_PTR,TYPE)	 \
  char* TransA, char* TransB,			 \
    integer* M, integer* N, integer* K,		 \
    TYPE_PTR* Alpha, TYPE_PTR* A, integer* lda,	 \
    TYPE_PTR* B, integer* ldb,			 \
    TYPE_PTR* Beta, TYPE_PTR* C, integer* ldc
#endif /* GEMM_PROTOTYPE */

#ifndef GEMM_
#define GEMM_(FUNC,TRANS_A,TRANS_B,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC) \
    (*FUNC)( &TRANS_A, &TRANS_B, &M, &N, &K,				\
	     &ALPHA, A, &LDA,						\
	     B, &LDB,							\
	     &BETA, C, &LDC )

#endif /* GEMM_ */

#ifndef GEMM_CPLX_
#define GEMM_CPLX_(FUNC,TRANS_A,TRANS_B,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC) \
    (*FUNC)( &TRANS_A, &TRANS_B, &M, &N, &K,				\
	     ALPHA, A, &LDA,						\
	     B, &LDB,							\
	     BETA, C, &LDC )

#endif /* GEMM_CPLX_ */

typedef int(*func_cblas_cgemm)( GEMM_CPLX_PROTOTYPE(scomplex,scomplex*) );
typedef int(*func_cblas_dgemm)( GEMM_PROTOTYPE(doublereal,doublereal) );
typedef int(*func_cblas_sgemm)( GEMM_PROTOTYPE(real,real) );
typedef int(*func_cblas_zgemm)( GEMM_CPLX_PROTOTYPE(dcomplex,dcomplex*) );

/*-----------------------------------------------------------------------*/

#ifndef GEMV_PROTOTYPE
#define GEMV_PROTOTYPE(TYPE_PTR, TYPE)	      \
  char* TransA, integer* M, integer* N,	      \
    TYPE_PTR* Alpha, TYPE_PTR* A, integer* lda,   \
    TYPE_PTR* X, integer* incX,		      \
    TYPE_PTR* Beta, TYPE_PTR* Y, integer* incY
#endif /* GEMV_PROTOTYPE */

#ifndef GEMV_CPLX_PROTOTYPE
#define GEMV_CPLX_PROTOTYPE(TYPE_PTR, TYPE)	      \
  char* TransA, integer* M, integer* N,	      \
    TYPE_PTR* Alpha, TYPE_PTR* A, integer* lda,   \
    TYPE_PTR* X, integer* incX,		      \
    TYPE_PTR* Beta, TYPE_PTR* Y, integer* incY
#endif /* GEMV_PROTOTYPE */

#ifndef GEMV_
#define GEMV_(FUNC,TRANS_A,M,N,ALPHA,A,LDA,X,INC_X,BETA,Y,INC_Y)	\
  (*FUNC)( &TRANS_A, &M, &N,						\
	   &ALPHA, A, &LDA, X, &INC_X,					\
	   &BETA, Y, &INC_Y )
#endif /* CGEMV_ */

#ifndef GEMV_CPLX_
#define GEMV_CPLX_(FUNC,TRANS_A,M,N,ALPHA,A,LDA,X,INC_X,BETA,Y,INC_Y)	\
  (*FUNC)( &TRANS_A, &M, &N,						\
	   ALPHA, A, &LDA, X, &INC_X,					\
	   BETA, Y, &INC_Y )
#endif /* CGEMV_CPLX_ */
typedef int(*func_cblas_cgemv)( GEMV_CPLX_PROTOTYPE(scomplex,scomplex*) );
typedef int(*func_cblas_dgemv)( GEMV_PROTOTYPE(doublereal,doublereal) );
typedef int(*func_cblas_sgemv)( GEMV_PROTOTYPE(real,real) );
typedef int(*func_cblas_zgemv)( GEMV_CPLX_PROTOTYPE(dcomplex,dcomplex*) );

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                           C L A P A C K
 *:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

/*-----------------------------------------------------------------------*/
#ifndef GESV_PROTOTYPE
#define GESV_PROTOTYPE(TYPE) \
  integer* n, integer* nrhs,					\
    TYPE* A, integer* lda, integer *ipiv,			\
    TYPE* B, integer* ldb, integer* Info
#endif

#ifndef GESV_CPLX_PROTOTYPE
#define GESV_CPLX_PROTOTYPE(TYPE) \
  integer* n, integer* nrhs,					\
    TYPE* A, integer* lda, integer *ipiv,			\
    TYPE* B, integer* ldb, integer* Info
#endif

#ifndef GESV_
#define GESV_(FUNC,N,NRHS,A,LDA,IPIV,B,LDB,INFO)	\
  (*FUNC)( &N, &NRHS,			\
	   A, &LDA, IPIV,				\
	   B, &LDB, &INFO )
#endif /* GESV_ */

#ifndef GESV_CPLX_
#define GESV_CPLX_(FUNC,N,NRHS,A,LDA,IPIV,B,LDB,INFO)	\
  (*FUNC)( &N, &NRHS,			\
	   A, &LDA, IPIV,				\
	   B, &LDB, &INFO )
#endif /* GESV_ */

typedef int(*func_cgesv_)( GESV_CPLX_PROTOTYPE(scomplex) );
typedef int(*func_dgesv_)( GESV_PROTOTYPE(doublereal) );
typedef int(*func_sgesv_)( GESV_PROTOTYPE(real) );
typedef int(*func_zgesv_)( GESV_CPLX_PROTOTYPE(dcomplex) );

#endif /* CLAPACK_DEFAULT_HH */
