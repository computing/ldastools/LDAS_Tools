/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef MIX_FUNCTION_HH
#define MIX_FUNCTION_HH

// $Id: MixFunction.hh,v 1.12 2007/01/24 17:51:28 emaros Exp $

#include "CallChain.hh"

//-----------------------------------------------------------------------------
// Forward declarations
//-----------------------------------------------------------------------------

namespace datacondAPI {
  template <class DataType_> class Sequence;
}	/* namespace - datacondAPI */

//-----------------------------------------------------------------------------
//: Function support for Mixer action
//
// This class implements the "mix" function to be used in the action section of
// a TCL command. The syntax, as it would appear in the action sequence is:
//
// result_valarray = mix(phase_double, carrier_double, input_valarray)
//
class MixFunction: CallChain::Function
{
    
public:
    
    //-------------------------------------------------------------------------
    //: Constructor
    //
    // Construct a new MixFunction
    MixFunction();
    
    //-------------------------------------------------------------------------
    //: Evaluate the Mix Function
    //
    // This performs the mixing. The list of parameters must contain doubles
    // phase and carrier and a valarray of input data.
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    //
    virtual void Eval(CallChain* Chain,
        const CallChain::Params& Params,
        const std::string& Ret) const;
    
    //-------------------------------------------------------------------------
    //: Return the Name of the function ("mix")
    //
    //!return: const std::string& ptName - Returns a reference to the name of
    //!return: the function
    virtual const std::string& GetName(void) const;
        
  //-------------------------------------------------------------------------
  //: Validate parameters
  virtual void
  ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		      const CallChain::Params& Params ) const;
};

#endif // MIX_FUNCTION_HH
