/* -*- mode: c++ -*- */
#ifndef SIGGEN_HH
#define SIGGEN_HH

#include <complex>

#include "CallChain.hh"
#include "UDT.hh"

//=======================================================================
// Signal Generator class definition
//=======================================================================

class SigGenSinFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  SigGenSinFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

#endif  /* SIGGEN_HH */

