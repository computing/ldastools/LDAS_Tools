#ifndef _COMPLEXFUNCTION_HH_
#define _COMPLEXFUNCTION_HH_

#include "CallChain.hh"

class complexFunction : public CallChain::Function {
public:
    complexFunction();
    virtual const std::string& GetName() const;
    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;
};

#endif // _COMPLEXFUNCTION_HH_
