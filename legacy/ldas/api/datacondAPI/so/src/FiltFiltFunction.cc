// $Id: FiltFiltFunction.cc,v 1.2 2005/12/01 22:54:58 emaros Exp $
//
// "filtfilt" action
//
// [y = ]filtfilt(b, x)
// [y = ]filtfilt(b, a, x)
//
// b: Sequence<float or double>
//    FIR transfer function
// a: Sequence<float or double>
//    IIR transfer function
// x: class derived from Sequence<T>
// y: clone of x
//


#include "datacondAPI/config.h"

#include "general/Memory.hh"

#include "FiltFiltFunction.hh"

#include "filters/LinFilt.hh"
#include "SequenceUDT.hh"

using namespace datacondAPI;

using std::valarray;
using std::complex;
using std::reverse;

const std::string& FiltFiltFunction::GetName() const
{
    const static std::string name("filtfilt");
    return name;
}

FiltFiltFunction::FiltFiltFunction()
    : Function(FiltFiltFunction::GetName())
{ }

namespace {
    const FiltFiltFunction staticFiltFiltFunction;

    template<class TCoeffs, class TIn>
    void FiltFiltApply(Filters::LinFilt<TCoeffs, TIn>& lf,
                       Sequence<TIn>& y)
    {
        lf.apply(y);
        reverse(&y[0], &y[y.size()]);
        
        lf.reset();

        lf.apply(y);
        reverse(&y[0], &y[y.size()]);
    }
}

template<class TIn, class TCoeffs>
void FiltFiltFunction::dispatch2(const Sequence<TCoeffs>& b,
                                 const udt* const aUdt,
                                 Sequence<TIn>& y) const
{
    if (aUdt == 0)
    {
        Filters::LinFilt<TCoeffs, TIn> lf(b);
        FiltFiltApply(lf, y);
    }
    else
    {
        if (const Sequence<TCoeffs>* const a
            = dynamic_cast<const Sequence<TCoeffs>*>(aUdt))
        {
            Filters::LinFilt<TCoeffs, TIn> lf(b, *a);
            FiltFiltApply(lf, y);
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                             1,
                             TypeInfoTable.GetName(typeid(*aUdt)),
                             TypeInfoTable.GetName(typeid(Sequence<TCoeffs>)));
        }
    }
}

template<class TIn>
void FiltFiltFunction::dispatch1(const udt* const bUdt,
                                 const udt* const aUdt,
                                 Sequence<TIn>& y) const
{
    if (const Sequence<float>* const b
        = dynamic_cast<const Sequence<float>*>(bUdt))
    {
        dispatch2(*b, aUdt, y);
    }
    else if (const Sequence<double>* const b
             = dynamic_cast<const Sequence<double>*>(bUdt))
    {
        dispatch2(*b, aUdt, y);
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     0,
                                     TypeInfoTable.GetName(typeid(*bUdt)),
                                     "Sequence<float> or Sequence<double>");
    }
}

void FiltFiltFunction::Eval(CallChain* chain,
                            const CallChain::Params& params,
                            const std::string& ret) const
{
    std::unique_ptr<udt> result(0);

    CallChain::Symbol* bUdt = 0;
    CallChain::Symbol* aUdt = 0;
    CallChain::Symbol* xUdt = 0;
    size_t xIndex = 0;

    if (params.size() == 2)
    {
        bUdt = chain->GetSymbol(params[0]);

        xIndex = 1;
        xUdt = chain->GetSymbol(params[xIndex]);
    }
    else if (params.size() == 3)
    {
        bUdt = chain->GetSymbol(params[0]);
        aUdt = chain->GetSymbol(params[1]);

        xIndex = 2;
        xUdt = chain->GetSymbol(params[xIndex]);
    }
    else
    {
        throw CallChain::BadArgumentCount(GetName(), 3, params.size());
    }
    

    if (const Sequence<float>* const x
        = dynamic_cast<const Sequence<float>*>(xUdt))
    {
        Sequence<float>* const y = x->Clone();
        result.reset(y);
        dispatch1(bUdt, aUdt, *y);
    }
    else if (const Sequence<double>* const x
        = dynamic_cast<const Sequence<double>*>(xUdt))
    {
        Sequence<double>* const y = x->Clone();
        result.reset(y);
        dispatch1(bUdt, aUdt, *y);
    }
    else if (const Sequence<complex<float> >* const x
        = dynamic_cast<const Sequence<complex<float> >*>(xUdt))
    {
        Sequence<complex<float> >* const y = x->Clone();
        result.reset(y);
        dispatch1(bUdt, aUdt, *y);
    }
    else if (const Sequence<complex<double> >* const x
        = dynamic_cast<const Sequence<complex<double> >*>(xUdt))
    {
        Sequence<complex<double> >* const y = x->Clone();
        result.reset(y);
        dispatch1(bUdt, aUdt, *y);
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     xIndex,
                                     TypeInfoTable.GetName(typeid(*xUdt)),
                                     "object derived from Sequence");
    }

    chain->ResetOrAddSymbol(ret, result.release());
}

