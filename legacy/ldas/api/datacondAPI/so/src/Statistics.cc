#include "config.h"

#include "Statistics.hh"
#include "StatsUDT.hh"

using std::complex;
using std::sqrt;

namespace datacondAPI {

    template<class T>
    unsigned int
    Statistics::size(const Sequence<T>& data) const
    {
        return data.size();
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::min(const Sequence<T>& data) const
    {
        if (data.size() == 0) 
        {
            throw std::invalid_argument("no data for min");
        }

        return data.min();
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::max(const Sequence<T>& data) const
    {
        if (data.size() == 0) 
        {
            throw std::invalid_argument("no data for max");
        }

        return data.max();
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::sum(const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        if (data.size() == 0) 
        {
            throw std::invalid_argument("no data for sum");
        }

        TOut s(0);
        for (size_t k = 0; k < data.size(); ++k)
        {
            s += data[k];
        }
        
        return s;
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::mean(const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        const size_t n = data.size();

        if (n == 0) 
        {
            throw std::invalid_argument("no data for mean");
        }

        typename StatisticsTraits<T>::TOut const m = Statistics::sum(data);

        return m/TOut(n);
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::rms(const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        const size_t n = data.size();

        if (n == 0) 
        {
            throw std::invalid_argument("no data for rms");
        }

        TOut rms(0);
        for (size_t k = 0; k < n; ++k) 
        {
            const TOut value = data[k];
            rms += value*value;
        }
    
        return sqrt(rms/TOut(n));
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::variance(const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        const size_t n = data.size();

        // check for n < 2 error condition
        if (n <= 1) 
        { 
            throw std::invalid_argument("variance of data with length < 2");
        }

        // two-pass algorithm for calculating variance
        // (follows Numerical Recipes in C, p.613)

        // first calculate the mean
        //
        const TOut m = mean(data);

        // now calculate the variance
        //
        TOut extra = 0.0; // extra term ( = 0 if no round-off error)
        TOut var = 0.0; 

        for (size_t k = 0; k < n; ++k) 
        {
            const TOut dev = TOut(data[k]) - m;
            extra += dev;
            var += dev*dev;
        }

        // normalize by 1/(n-1) to get unbiased estimator

        return (var - extra*extra/TOut(n))/TOut(n - 1);
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::skewness(const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        const size_t n = data.size();

        // check for n < 2 error condition
        if (n <= 1) 
        { 
            throw std::invalid_argument("skewness of data with length < 2");
        }

        // two-pass algorithm for calculating variance
        // (follows Numerical Recipes in C, p.613)

        // first calculate the mean
        //
        const TOut m = mean(data);

        // now calculate the variance and skewness
        //
        TOut var  = Statistics::variance(data);
        TOut skew = 0.0;  // :NOTE: skewness = 0 if variance = 0

        if (var != TOut(0))
        {
            for (size_t k = 0; k < n; ++k) 
            {
                const TOut dev = TOut(data[k]) - m;
                skew += dev*dev*dev;  
            }
            skew /= (TOut(n-1)*var*sqrt(var));
        }

        return skew;
    }

    template<class T>
    typename StatisticsTraits<T>::TOut
    Statistics::kurtosis(const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        const size_t n = data.size();

        // check for n<2 error condition
        if (n <= 1) 
        { 
            throw std::invalid_argument("kurtosis of data with length < 2");
        }

        // two-pass algorithm for calculating variance
        // (follows Numerical Recipes in C, p.613)

        // first calculate the mean
        //
        const TOut m = mean(data);

        // now calculate the variance and kurtosis
        //
        TOut var  = variance(data);
        TOut kurt = 0.0;  // :NOTE: kurtosis = 0 if variance = 0

        if (var != TOut(0))
        {
            for (size_t k = 0; k < n; ++k) 
            {
                TOut dev = TOut(data[k]) - m;
                dev *= dev;
                kurt += dev*dev;  
            }
            kurt = kurt/(TOut(n-1)*var*var) - TOut(3.0);
        }

        return kurt;
    }

    template<class T>
    void
    Statistics::all(Stats& out, const Sequence<T>& data) const
    {
        typedef typename StatisticsTraits<T>::TOut TOut;

        // check for n=0, n=1 error conditions
        if (data.size() == 0)
        {
            throw std::invalid_argument("Statistics::all: no input data");
        }
        else if (data.size() == 1) 
        { 
            throw std::invalid_argument("Statistics::all: higher order moments undefined for n = 1");
        }

        // make sure output sequence has proper size
        const unsigned int numMoments = 4;

        if (out.size() != numMoments)
        {
            out.resize(numMoments);
        }

        // fill-in moments
        out[Stats::MEAN] = Statistics::mean(data);
        out[Stats::VARIANCE] = Statistics::variance(data);
        out[Stats::SKEWNESS] = Statistics::skewness(data);
        out[Stats::KURTOSIS] = Statistics::kurtosis(data);

        // fill-in metadata
        out.SetSize(Statistics::size(data));
        out.SetMin(Statistics::min(data));
        out.SetMax(Statistics::max(data));
        out.SetRMS(Statistics::rms(data));

        // conditionally fill-in metadata if it's present in the source object
        if (const WhenMetaData* const resolved
              = dynamic_cast<const WhenMetaData*>(&data))
        {
            out.ArbitraryWhenMetaData::operator=(*resolved);
        }

        out.SetStatisticsOfName(data.name());
    }

    // specializations for complex types
    template<>
    StatisticsTraits<complex<float> >::TOut
    Statistics::min(const Sequence<complex<float> >& data) const
    {
        throw std::invalid_argument("Statistics::min() - "
                                    "unimplemented for Sequence<complex<float> >");
    }

    template<>
    StatisticsTraits<complex<double> >::TOut
    Statistics::min(const Sequence<complex<double> >& data) const
    {
        throw std::invalid_argument("Statistics::min() - "
                                    "unimplemented for Sequence<complex<double> >");
    }

    template<>
    StatisticsTraits<complex<float> >::TOut
    Statistics::max(const Sequence<complex<float> >& data) const
    {
        throw std::invalid_argument("Statistics::max() - "
                                    "unimplemented for Sequence<complex<float> >");
    }

    template<>
    StatisticsTraits<complex<double> >::TOut
    Statistics::max(const Sequence<complex<double> >& data) const
    {
        throw std::invalid_argument("Statistics::max() - "
                                    "unimplemented for Sequence<complex<double> >");
    }

    template<>
    void
    Statistics::all(Stats& out, const Sequence<complex<float> >& data) const
    {
        throw std::invalid_argument("Statistics::all() - "
                                    "unimplemented for Sequence<complex<float> >");
    }

    template<>
    void
    Statistics::all(Stats& out, const Sequence<complex<double> >& data) const
    {
        throw std::invalid_argument("Statistics::all() - "
                                    "unimplemented for Sequence<complex<double> >");
    }

#if 1
    // Instantiations
#undef INSTANTIATE
#define INSTANTIATE(T) \
    template unsigned int Statistics::size(const Sequence< T >&) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::sum(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::min(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::max(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::mean(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::rms(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::variance(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::skewness(const Sequence< T >& data) const; \
    template StatisticsTraits< T >::TOut \
    Statistics::kurtosis(const Sequence< T >& data) const; \
    template void \
    Statistics::all(Stats& stats, const Sequence< T >& data) const

    INSTANTIATE(float);
    INSTANTIATE(double);
    INSTANTIATE(complex<float>);
    INSTANTIATE(complex<double>);

#undef INSTANTIATE
#endif

}
