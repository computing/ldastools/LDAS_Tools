//reconstructor.cc

#include "datacondAPI/config.h"

#include <valarray>
#include <iostream>
#include <memory>   
#include <exception>   

#include "general/Memory.hh"
#include <general/toss.hh>

#include "UDT.hh"
#include "reconstructor.hh"
#include "result.hh"
#include "WaveletPSU.hh"
#include "Resample.hh"

using std::unique_ptr;
using std::exception;   
using std::invalid_argument;   

   
namespace
{
  const char rcsID[] = "@(#) $Id: reconstructor.cc,v 1.17 2009/01/14 00:27:18 emaros Exp $:";
}

namespace datacondAPI{
namespace psu
{

  Reconstructor::Reconstructor()
  {

  }

  Reconstructor::~Reconstructor()
  {

  }
 
  void Reconstructor::
  apply(datacondAPI::udt*& out, const datacondAPI::udt& in)
  {
    if(out == 0)
      {
	if(datacondAPI::udt::IsA<datacondAPI::psu::Result<double> >(in))
	  {
	    if (datacondAPI::udt::Cast<Result<double> >(in).checkMetaData())
	      out = new datacondAPI::TimeSeries<double>;
	    else
	      out = new datacondAPI::Sequence<double>;
	  }

	else if (datacondAPI::udt::IsA<datacondAPI::psu::Result<float> >(in))
	  {
	    if (datacondAPI::udt::Cast<Result<float> >(in).checkMetaData())
	      out = new datacondAPI::TimeSeries<float>;
	    else
	      out = new datacondAPI::Sequence<double>;
	  }

	else
	  {
	    std::string what = "apply(udt*&, udt&) -- unsupported type cast";
	    General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
	  }
      }

    if(datacondAPI::udt::IsA<datacondAPI::psu::Result<double> >(in))
      {
	if (datacondAPI::udt::Cast<Result<double> >(in).checkMetaData())
	  {
	    datacondAPI::Sequence<double> outSeq = (datacondAPI::udt::Cast<datacondAPI::TimeSeries<double> >(*out));
	    apply(outSeq, datacondAPI::udt::Cast<Result<double> >(in));

	    //temporary time series to hold metadata, then to have sequence added
	    datacondAPI::TimeSeries<double> temp;
	    
	    //copy metadata from Result to reconstructed data
	    (datacondAPI::udt::Cast<Result<double> >(in)).returnMetaData(temp);

	    //put outSeq into TimeSeries temp
	    temp.Sequence<double>::operator=(outSeq);
	   
 	    //let temp equal the outgoing parameter
	    datacondAPI::udt::Cast<TimeSeries<double> >(*out) = temp; 

	  }
	else
	  {
	    apply(datacondAPI::udt::Cast<datacondAPI::Sequence<double> >(*out), datacondAPI::udt::Cast<Result<double> >(in));
	  }
      }

    else if(datacondAPI::udt::IsA<Result<float> >(in))
      {
	if(datacondAPI::udt::Cast<Result<float> >(in).checkMetaData())
	  {

	    datacondAPI::Sequence<float> outSeq = (datacondAPI::udt::Cast<datacondAPI::TimeSeries<float> >(*out));
	    apply(outSeq, datacondAPI::udt::Cast<Result<float> >(in));

	    //temporary time series to hold metadata, then to have sequence added
	    datacondAPI::TimeSeries<float> temp;
	    
	    //copy metadata from Result to reconstructed data
	    (datacondAPI::udt::Cast<Result<float> >(in)).returnMetaData(temp);

	    //put outSeq into TimeSeries temp
	    temp.Sequence<float>::operator=(outSeq);
	   
 	    //let temp equal the outgoing parameter
	    datacondAPI::udt::Cast<TimeSeries<float> >(*out) = temp; 

	  }
	else
	  {
	    apply(datacondAPI::udt::Cast<datacondAPI::Sequence<float> >(*out), datacondAPI::udt::Cast<Result<float> >(in));
	  }
      }

    else
      {
	std::string what = "apply(udt*&, udt&) -- unsupported type cast";
	General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
      }
  }

  template<class T>
  void Reconstructor::
  apply(datacondAPI::Sequence<T>& out, const Result<T>& in)
  {
    //level of reconstruction
    int levelnumber = in.getLevels()-1; 
    
    //gets approximation from Result for first value of r
    if(in.checkMetaData())
      {
	datacondAPI::TimeSeries<T> ts;
	datacondAPI::udt* p_ts = &ts;
	datacondAPI::Scalar<int> lev(in.getLevels());
	in.getResult(p_ts, lev);
	datacondAPI::Sequence<T> r(ts);

	//calls recursive apply
	apply(out, in, r, levelnumber);
      }
    else
      {
	datacondAPI::Sequence<T> s;
	datacondAPI::udt* p_s = &s;
	datacondAPI::Scalar<int> lev(in.getLevels());
	in.getResult(p_s, lev);
	datacondAPI::Sequence<T> r(s);
	
	//calls recursive apply
	apply(out, in, r, levelnumber);
      }
  }

  //private member function-- recursive
  template<class T> 
  void Reconstructor::
  apply (datacondAPI::Sequence<T>& out, const Result<T>& in, datacondAPI::Sequence<T>& r, int levelnumber)
  {
    unique_ptr<Wavelet> reconst_wavelet(NULL);
    
    //get high and lowpass reconstruction filters
    datacondAPI::Sequence<double> sCoeff_low;
    datacondAPI::Sequence<double> sCoeff_high;
    getSynthesis(in, reconst_wavelet, sCoeff_high, sCoeff_low);

    Resample h(2, 1, sCoeff_high);
    Resample l(2, 1, sCoeff_low);

    datacondAPI::Sequence<T> detail(in.getResult(levelnumber));

    datacondAPI::Sequence<T> rh; //highpass reconstruction
    datacondAPI::Sequence<T> rl; //lowpass reconstruction


    //filter detail to yield highpass reconstruction
    h.apply(rh, detail);

    rh /= 2;

    //filter lowpass to yield lowpass reconstruction
    l.apply(rl, r);    

    rl /=2;

    //add rh+rl to get out for this level
    r.resize(rl.size());
    r = rl + rh;

    //correct by adding a delay
    r = r.shift(1);  //shift to the left by 1 space

    levelnumber--;
  
    if(levelnumber >= 0)
      {
        apply(out, in, r, levelnumber);
      }
    
    else
      {
	out.resize(r.size());
        out = r;

      }
  }


  void Reconstructor::
  apply(datacondAPI::udt*& out, const datacondAPI::udt& in, const datacondAPI::udt& level)
  {
    if(out == 0)
      {
	if(datacondAPI::udt::IsA<datacondAPI::psu::Result<double> >(in))
	  {
	    if (datacondAPI::udt::Cast<Result<double> >(in).checkMetaData())
	      out = new datacondAPI::TimeSeries<double>;
	    else
	      out = new datacondAPI::Sequence<double>;
	  }

	else if (datacondAPI::udt::IsA<datacondAPI::psu::Result<float> >(in))
	  {
	    if (datacondAPI::udt::Cast<Result<float> >(in).checkMetaData())
	      out = new datacondAPI::TimeSeries<float>;
	    else
	      out = new datacondAPI::Sequence<double>;
	  }

	else
	  {
	    std::string what = "apply(udt*&, udt&, udt&) -- unsupported type cast";
	    General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
	  }
      }

    if(datacondAPI::udt::IsA<datacondAPI::psu::Result<double> >(in) && datacondAPI::udt::IsA<Scalar<int> >(level))
      {
	if(datacondAPI::udt::Cast<Result<double> >(in).checkMetaData())
 	  {
	    Result<double> inResult(datacondAPI::udt::Cast<Result<double> >(in));
	    int lev = datacondAPI::udt::Cast<Scalar<int> >(level).GetValue();
	    datacondAPI::Sequence<double> outSeq;
	    try{
	      apply(outSeq, inResult, lev);
	    }
	    catch(exception &e)
	      {
		throw invalid_argument("exception in apply()");
	      }
	    
	    //temporary time series to hold metadata, then to have sequence added
	    datacondAPI::TimeSeries<double> temp;
	    try{
	      //copy metadata from Result to reconstructed data
	      inResult.returnMetaData(temp); 
	    }
	    catch(exception &e)
	      {
		throw invalid_argument("exception in returnMetaData()");
	      }

	    try{
	    //put outSeq into TimeSeries temp
	    temp.Sequence<double>::operator=(outSeq);
	    }
	    catch(exception &e)
	      {
		throw invalid_argument("exception in assigning sequence to timeseries");
	      }
	    try{
 	    //let temp equal the outgoing parameter
	    datacondAPI::udt::Cast<TimeSeries<double> >(*out) = temp; 
	    }
	    catch(exception &e)
	      {
		throw invalid_argument("exception in assigning temp Timeseries to *out");
	      }
	  }
	
	else
	  {
	    apply(datacondAPI::udt::Cast<datacondAPI::Sequence<double> >(*out), datacondAPI::udt::Cast<Result<double> >(in), datacondAPI::udt::Cast<Scalar<int> >(level).GetValue()); 
	  }
      }

    else if(datacondAPI::udt::IsA<Result<float> >(in) && datacondAPI::udt::IsA<Scalar<int> >(level))
      {
	if(datacondAPI::udt::Cast<Result<float> >(in).checkMetaData())
	  {
	    Result<float> inResult(datacondAPI::udt::Cast<Result<float> >(in));
	    int lev = datacondAPI::udt::Cast<Scalar<int> >(level).GetValue();
	    datacondAPI::Sequence<float> outSeq(datacondAPI::udt::Cast<datacondAPI::TimeSeries<float> >(*out));
	    apply(outSeq, inResult, lev);

	    //temporary time series to hold metadata, then to have sequence added
	    datacondAPI::TimeSeries<float> temp;
	    
	    //copy metadata from Result to reconstructed data
	    inResult.returnMetaData(temp);

	    //put outSeq into TimeSeries temp
	    temp.Sequence<float>::operator=(outSeq);

 	    //let out = temp
	    datacondAPI::udt::Cast<TimeSeries<float> >(*out) = temp;  
	    
	  }

	else
	  {
	    apply(datacondAPI::udt::Cast<datacondAPI::Sequence<float> >(*out), datacondAPI::udt::Cast<Result<float> >(in), datacondAPI::udt::Cast<Scalar<int> >(level).GetValue());
	  }
	
      }
    
    else
      {
	std::string what = "apply(udt*&, udt&, udt&) -- unsupported type";
	General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
      }
  }

  template<class T>
  void Reconstructor::
  apply (datacondAPI::Sequence<T>& out, const Result<T>& in, const int level)
  {

    //a filter applied to a sequence of zeros yields a sequence of zeros.  

    //checks for legal partial reconstruction level value
    if(level < 0 || level > in.getLevels())
      {
	std::string what = "apply(out, in, level) -- Illegal level value.";	
	General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
      }
   
    unique_ptr<Wavelet> reconst_wavelet(NULL);
    datacondAPI::Sequence<T> r;
    datacondAPI::Sequence<T> approx;
    
    //get high and lowpass reconstruction filters
    datacondAPI::Sequence<double> sCoeff_low;
    datacondAPI::Sequence<double> sCoeff_high;
    try{
      getSynthesis(in, reconst_wavelet, sCoeff_high, sCoeff_low);
       }
    catch(exception &e)
      {
	std::string what = "exception thrown in getSynthesis in apply(seq, result, int)" ;
	General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
      }
    if (sCoeff_low.size() == 0 || sCoeff_high.size() == 0)
      {
	std::string what = "apply(out, in, level) -- getSynthesis not working!!";
	General::toss<std::domain_error>("Reconstructor", __FILE__, __LINE__, what);
      }


    if(level == in.getLevels())
      {
	if(in.checkMetaData())
	  {
	    datacondAPI::TimeSeries<T> ts;
	    datacondAPI::udt* p_ts = &ts;
	    datacondAPI::Scalar<int> lev(level);
	    in.getResult(p_ts, lev);
	    datacondAPI::Sequence<T> temp(ts);
	    approx = temp;
	  }
	else
	  {
	    datacondAPI::udt* p_s = &approx;
	    datacondAPI::Scalar<int> lev(level);
	    in.getResult(p_s, lev);
	  }

	if (sCoeff_low.size() == 0 || sCoeff_high.size() == 0)
	  {
	    std::string what = "apply(out, in, level) -- coefficients null!!";
	    General::toss<std::domain_error>("Reconstructor", __FILE__, __LINE__, what);
	  }

	datacondAPI::Sequence<T> rl;
	
	try{
	  Resample l(2, 1, sCoeff_low);
	  l.apply(rl, approx);
	}
	catch(...)
	  {
	    std::string what = "exception in Resample::apply in apply(seq, result, int)";
	    General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
	  }

	r.resize(rl.size());
	r = rl;
	r /=2;    //for wavelets, p = 2 in reconstruction
	r = r.shift(1);

	
	for(int i = (level-2); i >= 0; i--)
	  {
	    datacondAPI::Sequence<T> tr = r;

	    if (sCoeff_low.size() == 0 || sCoeff_high.size() == 0)
	      {
		std::string what = "apply(out, in, level) -- coefficients null!!";
		General::toss<std::domain_error>("Reconstructor", __FILE__, __LINE__, what);
	      }

	    r.resize(tr.size() * 2);
	    try{
	      Resample l(2, 1, sCoeff_low);
	      l.apply(r, tr);
	    }

	    catch(...)
	      {
		std::string what = "exception in Resample::apply in apply(seq, result, int)";
		General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
	      }

	    r /=2;
	    r = r.shift(1);
	  }//end for
      }//end if(level ==in.getLevels())

    else
      {
	if(in.checkMetaData())
	  {
	    datacondAPI::TimeSeries<T> ts;
	    datacondAPI::udt* p_ts = &ts;
	    datacondAPI::Scalar<int> lev(level);
	    in.getResult(p_ts, lev);
	    datacondAPI::Sequence<T> temp(ts);
	    r.resize(temp.size());
	    r = temp;
	  }
	else
	  {
	    datacondAPI::udt* p_s = &r;
	    datacondAPI::Scalar<int> lev(level);
	    in.getResult(p_s, lev);
	  }

	for(int i = level; i >= 0; i--)
	  {
	    if (r.size() == 0)
	      {
		std::string what = "r is null!!";
		General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
	      }

	    datacondAPI::Sequence<T> tr = r;
	    
	    r.resize(tr.size() * 2);

	    if(i < level)
	      {
		try{
		  Resample l(2, 1, sCoeff_low);
		  l.apply(r, tr);
		}

	    catch(...)
	      {
		std::string what = "exception in Resample::apply in apply(seq, result, int)";
		General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
	      }
		r /= 2;
		r = r.shift(1);
	      }

	    else
	      {
		if (sCoeff_low.size() == 0 || sCoeff_high.size() == 0)
		  {
		    std::string what = "apply(out, in, level) -- coefficients null!!";
		    General::toss<std::domain_error>("Reconstructor", __FILE__, __LINE__, what);
		  }

		try{
		Resample h(2, 1, sCoeff_high);
		h.apply(r, tr);
		}

		catch(...)
		  {
		    std::string what = "exception in Resample::apply in apply(seq, result, int)";
		    General::toss<std::invalid_argument>("Reconstructor", __FILE__, __LINE__, what);
		  }
		r /=2;
		r = r.shift(1);
	      }
	  }//end for
      }//end else
    
    out.resize(r.size());
    out = r;

  }
   
  Reconstructor::
  Reconstructor(const Reconstructor& rcstr)
  {
  }

  template<class T>
  void Reconstructor::
  getSynthesis(const Result<T>& in, unique_ptr<Wavelet>& reconst_wavelet, datacondAPI::Sequence<double>& sCoeff_high, datacondAPI::Sequence<double>& sCoeff_low)
  {
    in.returnWavelet(reconst_wavelet);

    if (reconst_wavelet.get() != 0)
      {
        reconst_wavelet->get_sL(sCoeff_low);
        reconst_wavelet->get_sH(sCoeff_high);
      }

    else
      {
	std::string what = "getSynthesis -- Wavelet not set.";
	General::toss<std::domain_error>("Reconstructor", __FILE__, __LINE__, what);
      }
  }


} //namespace psu
} //namespace datacondAPI

#undef INSTANTIATE0
#define INSTANTIATE0(TYPE0)\
template  void datacondAPI::psu::Reconstructor::apply(datacondAPI::Sequence< TYPE0 >& out, const Result< TYPE0 >& in);
INSTANTIATE0(float)
INSTANTIATE0(double)

#undef INSTANTIATE1
#define INSTANTIATE1(TYPE1)\
template  void datacondAPI::psu::Reconstructor::apply(datacondAPI::Sequence< TYPE1 >& out, const Result< TYPE1 >& in, const int level);
INSTANTIATE1(float)
INSTANTIATE1(double)

#undef INSTANTIATE2
#define INSTANTIATE2(TYPE2)\
template  void datacondAPI::psu::Reconstructor::getSynthesis(const Result< TYPE2 >& in, unique_ptr<Wavelet>& reconst_wavelet, datacondAPI::Sequence<double>& sCoeff_high, datacondAPI::Sequence<double>& sCoeff_low);
INSTANTIATE2(float)
INSTANTIATE2(double)

#undef INSTANTIATE3
#define INSTANTIATE3(TYPE3)\
template  void datacondAPI::psu::Reconstructor::apply(datacondAPI::Sequence< TYPE3 >& out, const Result< TYPE3 >& in, datacondAPI::Sequence< TYPE3 >& r, int levelnumber);
INSTANTIATE3(float)
INSTANTIATE3(double)
