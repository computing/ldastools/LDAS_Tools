/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: TimeSeries.hh,v 1.24 2004/08/11 16:21:32 emaros Exp $
#ifndef TIMESERIES_HH
#define	TIMESERIES_HH

#include "TimeSeriesUMD.hh"
#include "SequenceUDT.hh"

// Forward declaration of ILwd elements
namespace ILwd
{
  class LdasElement;
}

namespace datacondAPI
{

  //
  //: Class for representing a time-series
  //
  // A TimeSeries is a Sequence having a start-time (the time at which
  // the first sample was taken) and a sampling rate.
  //
  template <class DataType_>
  class TimeSeries : public Sequence<DataType_>, 
		     public TimeSeriesMetaData
  {
  public:

    //: Default constructor
    //
    // Creates an empty TimeSeries with a default sampling rate of 1.0,
    // so that Nyquist is 0.5
    //
    TimeSeries();
    
    //: Copy constructor
    TimeSeries(const TimeSeries<DataType_>& data);

    //: Constructor
    //:TODO: Add setting of base frequency and phase if needed
    TimeSeries(const Sequence<DataType_>& data, 
	       const double& fs,
	       const General::GPSTime& StartTime = General::GPSTime());
		   
    //: Destructor
    virtual ~TimeSeries();

    virtual General::GPSTime GapGetDataBegin( ) const;
    virtual General::GPSTime GapGetDataEnd( ) const;
    virtual unsigned int GapGetDataSize( ) const;

    //: Accessor required by WhenMetaData
    //
    // Returns the time-stamp one sample past the end of
    // the TimeSeries ie. if the TimeSeries has length N and
    // step-size dt then the end-time is given by
    //     GetStartTime() + n * dt
    //
    virtual
    General::GPSTime GetEndTime() const;

    //: Assignment from single value
    TimeSeries<DataType_>& operator=(const DataType_ d);

    //: Copy the metadata
    // Copies metadata from another UDT to this UDT.
    //!param: const udt& MetaData - Source meta data.
    virtual void CopyMetaData( const udt& MetaData );

    //: Make a copy of this object
    virtual
    TimeSeries<DataType_>* Clone() const;

    //: Convert to an ILWD object
    virtual
    ILwd::LdasElement* ConvertToIlwd(const CallChain& Chain,
			    udt::target_type Target = udt::TARGET_GENERIC) const;
    virtual void ConvertToIlwd(const CallChain& Chain,
			       udt::target_type Target,
			       ILwd::LdasContainer* Container) const;

  private:
    
  }; // TimeSeries<T>

  template< class T > inline void TimeSeries<T>::
  ConvertToIlwd(const CallChain& Chain,
		udt::target_type Target,
		ILwd::LdasContainer* Container) const
  {
    Sequence<T>::ConvertToIlwd( Chain, Target, Container );
  }

  template<class T>
  inline General::GPSTime TimeSeries<T>::
  GapGetDataBegin( ) const
  {
    return GetStartTime();
  }

  template<class T>
  inline General::GPSTime TimeSeries<T>::
  GapGetDataEnd( ) const
  {
    return GetEndTime();
  }

  template<class T>
  inline unsigned int TimeSeries<T>::
  GapGetDataSize( ) const
  {
    return this->size();
  }

} // namespace datacondAPI

#endif	/* TIMESERIES_HH */
