//resolver.hh

#ifndef RESOLVER_HH
#define RESOLVER_HH

#include <memory>   

#include "general/Memory.hh"
   
#include "WaveletPSU.hh"
#include "result.hh"
#include "SequenceUDT.hh"

namespace datacondAPI{
namespace psu
{
  class Resolver
    : public datacondAPI::udt
  {

    friend class Result<double>;
    friend class Result<float>;
    //Result has "friend class Resolver"

  public:
    //: Default constructor
    Resolver();

    //: constructor that takes UDT objects
    //!param: datacondAPI::udt& in - wavelet used
    //!param: datacondAPI::udt inLevels - number of levels of decomposition
    Resolver(datacondAPI::udt&, datacondAPI::udt&);

    //: constructor with specifications of type of wavelet and number of levels
    //!param: Wavelet& in - type of wavelet used
    //!param: int inLevels - number of levels of decomposition
    Resolver(Wavelet&, int);

    //: Default destructor
    ~Resolver();

    //: tells # of levels of decomposition currently being used
    //!return: integer number of levels of decomposition currently set
    int getLevels() const;

    //: allows user to set # of levels of decomposition to be used
    //!param: int number - number of levels of decomposition
    void setLevels(int); 

    //: gets analysis filter coefficients
    void getWavelet(); 

    //: allows user to set the type of wavelet to be used
    //!param: Wavelet& in - type of wavelet used
    void setWavelet(Wavelet&); 

    //: tells whether conditions are ready for filtering
    //!return: false if the wavelet or number of levels has not been set
    bool isReady(); 

    //: applies filter to data (takes UDTs as arguments)
    //!param: datacondAPI::udt*& out - stores each level of signal decomposition
    //!param: const datacondAPI::udt& in - original signal to be filtered
    void apply (datacondAPI::udt*& out, const datacondAPI::udt& in);


    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;

    Resolver* Clone() const;


  private:

    //: Copy constructor
    Resolver(const Resolver&);

    //: applies filter to Sequence data 
    //!param: Result<T>& output - stores each level of signal decomposition
    //!param: datacondAPI::Sequence<T>& data - original signal to be filtered
    template<class T>
    void applyFilter(Result<T>&, const datacondAPI::Sequence<T>&);

    /*
    //: applies filter to TimeSeries data 
    //!param: Result<T>& output - stores each level of signal decomposition
    //!param: datacondAPI::TimeSeries<T>& data - original signal to be filtered
    template<class T>
    void TSapplyFilter(Result<T>&, const datacondAPI::TimeSeries<T>&);
    */

    //: levels of decomposition
    int levels;

    //: accesses information in the wavelet
    std::unique_ptr<Wavelet> m_resolver_wavelet;
    //: original lowpass coefficients from the wavelet
    datacondAPI::Sequence<double> L;
    //: original highpass coefficients from the wavelet
    datacondAPI::Sequence<double> H;

   }; // class Resolver

   inline Resolver::
   Resolver(const Resolver& Source )
     : datacondAPI::udt( Source )
   {
   }
} //namespace psu
} //namespace datacondAPI

#endif

