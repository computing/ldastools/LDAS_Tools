/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include <memory>
#include <sstream>

#include "general/Memory.hh"
#include "general/unordered_map.hh"

#include "filters/valarray_utils.hh"
   
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"
#include "util.hh"

#include "PrecisionConversion.hh"

using std::complex;
using std::unique_ptr;   
using General::unordered_map;   
using namespace datacondAPI;

namespace {

    //
    //
    // The trivial converter is used when the udt is already at
    // the required precision. It is provided because the maps
    // can only contain non-member functions
    //
    udt* convert_trivial(const udt& udt_in)
    {
        return udt_in.Clone();
    }

    //
    // The following functions are the specific conversion routines
    // which are used once the exact dynamic type of an object is
    // determined. Note that derived classes have their own converters
    // but also use the converters of their ancestors.
    //

    //
    // Convert a Scalar
    //
    template<class TOut, class TIn> inline
    void convert_specific(Scalar<TOut>& out, const Scalar<TIn>& in)
    {
        out.SetValue(TOut(in.GetValue()));
    }

    //
    // Convert the Sequence part of an object
    //
    template<class TOut, class TIn> inline
    void convert_specific(Sequence<TOut>& out, const Sequence<TIn>& in)
    {
        // Do UDT part
        out.udt::operator=(in);

        // Do valarray part
        Filters::valarray_copy(out, in);
    }

    template<class TOut, class TIn> inline
    void convert_specific(TimeSeries<TOut>& out, const TimeSeries<TIn>& in)
    {
        convert_specific(static_cast<Sequence<TOut>&>(out),
                         static_cast<const Sequence<TIn>&>(in));
        
        out.TimeSeriesMetaData::operator=(in);
    }

    template<class TOut, class TIn> inline
    void convert_specific(FrequencySequence<TOut>& out,
                          const FrequencySequence<TIn>& in)
    {
        convert_specific(static_cast<Sequence<TOut>&>(out),
                         static_cast<const Sequence<TIn>&>(in));
        
        out.FrequencyMetaData::operator=(in);
        out.GeometryMetaData::operator=(in);
    }

    template<class TOut, class TIn> inline
    void convert_specific(TimeBoundedFreqSequence<TOut>& out,
                          const TimeBoundedFreqSequence<TIn>& in)
    {
        convert_specific(static_cast<FrequencySequence<TOut>&>(out),
                         static_cast<const FrequencySequence<TIn>&>(in));
        
        out.TimeBoundedFreqSequenceMetaData::operator=(in);
    }

    template<class TOut, class TIn> inline
    void convert_specific(CSDSpectrum<TOut>& out,
                          const CSDSpectrum<TIn>& in)
    {
        convert_specific(static_cast<TimeBoundedFreqSequence<TOut>&>(out),
                         static_cast<const TimeBoundedFreqSequence<TIn>&>(in));
        
        out.CSDSpectrumUMD::operator=(in);
    }

    template<class TOut, class TIn> inline
    void convert_specific(WelchSpectrum<TOut>& out,
                          const WelchSpectrum<TIn>& in)
    {
        convert_specific(static_cast<CSDSpectrum<TOut>&>(out),
                         static_cast<const CSDSpectrum<TIn>&>(in));
        
        out.WelchSpectrumUMD::operator=(in);
    }

    template<class TOut, class TIn> inline
    void convert_specific(WelchCSDSpectrum<TOut>& out,
                          const WelchCSDSpectrum<TIn>& in)
    {
        convert_specific(static_cast<CSDSpectrum<TOut>&>(out),
                         static_cast<const CSDSpectrum<TIn>&>(in));
        
        out.WelchCSDSpectrumUMD::operator=(in);
    }
    
    //
    // This is the top level dispatcher - different versions of
    // this function are instantiated and inserted into a map
    // which takes type_info to an appropriate function for
    // converting TIn to TOut.
    //
    template<class TOut, class TIn>
    udt* convert(const udt& udt_in)
    {
        const TIn& in = dynamic_cast<const TIn&>(udt_in);
        unique_ptr<TOut> out(new TOut());
        
        convert_specific(*out, in);
        
        return out.release();
    }

    //
    // These are the prototypes that all functions contained in the map
    // must adhere to. Note that the function uses UDT arguments because
    // all the functions contained in the map must have the same signature,
    // so a templated function wouldn't work.
    //
    // All conversion functions take a single UDT argument and return a
    // pointer to a UDT at the new precision created on the heap
    //
    typedef udt* (*ConversionFn)(const udt& in);
    
    //
    // A ConversionFnMap is a unordered_map that uses a type_info* as the key and
    // contains ConversionFn's as its elements
    //
    typedef unordered_map<const std::type_info*, ConversionFn,
                     LookupHash, LookupEq> ConversionFnMap;
    
    //
    // A function that we can use to initialise the unordered_map when the
    // library is loaded
    //
    const ConversionFnMap& initFloatFnMap()
    {
        static ConversionFnMap floatFnMap;
        
        // Lets be ultra-paranoid
        if (floatFnMap.size() != 0)
        {
            return floatFnMap;
        }
        
        //
        // Now we insert the function pointers into the map by typeid
        //

        // Trivial conversions for things that are already float - 
        // they are "converted" via cloning

        // Trivial Scalar
        floatFnMap[&typeid(Scalar<float>)] = convert_trivial;
        floatFnMap[&typeid(Scalar<complex<float> >)] = convert_trivial;

        // Trivial Sequence
        floatFnMap[&typeid(Sequence<float>)] = convert_trivial;
        floatFnMap[&typeid(Sequence<complex<float> >)] = convert_trivial;

        // Trivial TimeSeries
        floatFnMap[&typeid(TimeSeries<float>)] = convert_trivial;
        floatFnMap[&typeid(TimeSeries<complex<float> >)] = convert_trivial;

        // Trivial FrequencySequence
        floatFnMap[&typeid(FrequencySequence<float>)] = convert_trivial;
        floatFnMap[&typeid(FrequencySequence<complex<float> >)]
            = convert_trivial;

        // Trivial TimeBoundedFreqSequence
        floatFnMap[&typeid(TimeBoundedFreqSequence<float>)]
            = convert_trivial;
        floatFnMap[&typeid(TimeBoundedFreqSequence<complex<float> >)]
            = convert_trivial;

        // Trivial CSDSpectrum
        floatFnMap[&typeid(CSDSpectrum<float>)] = convert_trivial;
        floatFnMap[&typeid(CSDSpectrum<complex<float> >)] = convert_trivial;

        // Trivial WelchSpectrum (real only)
        floatFnMap[&typeid(WelchSpectrum<float>)] = convert_trivial;

        // Trivial WelchCSDSpectrum
        floatFnMap[&typeid(WelchCSDSpectrum<float>)] = convert_trivial;
        floatFnMap[&typeid(WelchCSDSpectrum<complex<float> >)]
            = convert_trivial;

        // Trivial DFT - complex<float> only
        floatFnMap[&typeid(DFT<complex<float> >)] = convert_trivial;

        // Non-trivial conversions

        // Scalar
        floatFnMap[&typeid(Scalar<int>)]
            = convert<Scalar<float>, Scalar<int> >;

        floatFnMap[&typeid(Scalar<double>)]
            = convert<Scalar<float>, Scalar<double> >;

        floatFnMap[&typeid(Scalar<complex<double> >)]
            = convert<Scalar<complex<float> >, Scalar<complex<double> > >;

        // Sequence
        floatFnMap[&typeid(Sequence<double>)]
            = convert<Sequence<float>, Sequence<double> >;
        
        floatFnMap[&typeid(Sequence<complex<double> >)]
            = convert<Sequence<complex<float> >, Sequence<complex<double> > >;
        
        // TimeSeries
        floatFnMap[&typeid(TimeSeries<double>)]
            = convert<TimeSeries<float>, TimeSeries<double> >;
        
        floatFnMap[&typeid(TimeSeries<complex<double> >)]
         = convert<TimeSeries<complex<float> >, TimeSeries<complex<double> > >;

        // FrequencySequence
        floatFnMap[&typeid(FrequencySequence<double>)]
            = convert<FrequencySequence<float>, FrequencySequence<double> >;
        
        floatFnMap[&typeid(FrequencySequence<complex<double> >)]
            = convert<FrequencySequence<complex<float> >,
                      FrequencySequence<complex<double> > >;

        // TimeBoundedFreqSequence
        floatFnMap[&typeid(TimeBoundedFreqSequence<double>)]
            = convert<TimeBoundedFreqSequence<float>,
                      TimeBoundedFreqSequence<double> >;
        
        floatFnMap[&typeid(TimeBoundedFreqSequence<complex<double> >)]
            = convert<TimeBoundedFreqSequence<complex<float> >,
                      TimeBoundedFreqSequence<complex<double> > >;

        // CSDSpectrum
        floatFnMap[&typeid(CSDSpectrum<double>)]
            = convert<CSDSpectrum<float>, CSDSpectrum<double> >;
        
        floatFnMap[&typeid(CSDSpectrum<complex<double> >)]
            = convert<CSDSpectrum<complex<float> >,
                      CSDSpectrum<complex<double> > >;

        // WelchSpectrum (real only)
        floatFnMap[&typeid(WelchSpectrum<double>)]
            = convert<WelchSpectrum<float>, WelchSpectrum<double> >;
        
        // WelchCSDSpectrum
        floatFnMap[&typeid(WelchCSDSpectrum<double>)]
            = convert<WelchCSDSpectrum<float>, WelchCSDSpectrum<double> >;
        
        floatFnMap[&typeid(WelchCSDSpectrum<complex<double> >)]
            = convert<WelchCSDSpectrum<complex<float> >,
                      WelchCSDSpectrum<complex<double> > >;

        // DFT - complex<float> only, uses Sequence converter
        floatFnMap[&typeid(DFT<complex<double> >)]
            = convert<DFT<complex<float> >, DFT<complex<double> > >;
        
        return floatFnMap;
    }
    
    //
    // A function that we can use to initialise the unordered_map when the
    // library is loaded
    //
    const ConversionFnMap& initDoubleFnMap()
    {
        static ConversionFnMap doubleFnMap;
        
        // Lets be ultra-paranoid
        if (doubleFnMap.size() != 0)
        {
            return doubleFnMap;
        }
        
        //
        // Now we insert the function pointers into the map by typeid
        //

        // Trivial conversions for things that are already double - 
        // they are "converted" via cloning

        // Trivial Scalar
        doubleFnMap[&typeid(Scalar<double>)] = convert_trivial;
        doubleFnMap[&typeid(Scalar<complex<double> >)] = convert_trivial;

        // Trivial Sequence
        doubleFnMap[&typeid(Sequence<double>)] = convert_trivial;
        doubleFnMap[&typeid(Sequence<complex<double> >)] = convert_trivial;

        // Trivial TimeSeries
        doubleFnMap[&typeid(TimeSeries<double>)] = convert_trivial;
        doubleFnMap[&typeid(TimeSeries<complex<double> >)] = convert_trivial;

        // Trivial FrequencySequence
        doubleFnMap[&typeid(FrequencySequence<double>)] = convert_trivial;
        doubleFnMap[&typeid(FrequencySequence<complex<double> >)]
            = convert_trivial;

        // Trivial TimeBoundedFreqSequence
        doubleFnMap[&typeid(TimeBoundedFreqSequence<double>)]
            = convert_trivial;
        doubleFnMap[&typeid(TimeBoundedFreqSequence<complex<double> >)]
            = convert_trivial;

        // Trivial CSDSpectrum
        doubleFnMap[&typeid(CSDSpectrum<double>)] = convert_trivial;
        doubleFnMap[&typeid(CSDSpectrum<complex<double> >)] = convert_trivial;

        // Trivial WelchSpectrum (real only)
        doubleFnMap[&typeid(WelchSpectrum<double>)] = convert_trivial;

        // Trivial WelchCSDSpectrum
        doubleFnMap[&typeid(WelchCSDSpectrum<double>)] = convert_trivial;
        doubleFnMap[&typeid(WelchCSDSpectrum<complex<double> >)]
            = convert_trivial;

        // Trivial DFT - complex<double> only
        doubleFnMap[&typeid(DFT<complex<double> >)] = convert_trivial;

        // Non-trivial conversions

        // Scalar
        doubleFnMap[&typeid(Scalar<int>)]
            = convert<Scalar<double>, Scalar<int> >;

        doubleFnMap[&typeid(Scalar<float>)]
            = convert<Scalar<double>, Scalar<float> >;

        doubleFnMap[&typeid(Scalar<complex<float> >)]
            = convert<Scalar<complex<double> >, Scalar<complex<float> > >;

        // Sequence
        doubleFnMap[&typeid(Sequence<float>)]
            = convert<Sequence<double>, Sequence<float> >;
        
        doubleFnMap[&typeid(Sequence<complex<float> >)]
            = convert<Sequence<complex<double> >, Sequence<complex<float> > >;
        
        // TimeSeries
        doubleFnMap[&typeid(TimeSeries<float>)]
            = convert<TimeSeries<double>, TimeSeries<float> >;
        
        doubleFnMap[&typeid(TimeSeries<complex<float> >)]
         = convert<TimeSeries<complex<double> >, TimeSeries<complex<float> > >;

        // FrequencySequence
        doubleFnMap[&typeid(FrequencySequence<float>)]
            = convert<FrequencySequence<double>, FrequencySequence<float> >;
        
        doubleFnMap[&typeid(FrequencySequence<complex<float> >)]
            = convert<FrequencySequence<complex<double> >,
                      FrequencySequence<complex<float> > >;

        // TimeBoundedFreqSequence
        doubleFnMap[&typeid(TimeBoundedFreqSequence<float>)]
            = convert<TimeBoundedFreqSequence<double>,
                      TimeBoundedFreqSequence<float> >;
        
        doubleFnMap[&typeid(TimeBoundedFreqSequence<complex<float> >)]
            = convert<TimeBoundedFreqSequence<complex<double> >,
                      TimeBoundedFreqSequence<complex<float> > >;

        // CSDSpectrum
        doubleFnMap[&typeid(CSDSpectrum<float>)]
            = convert<CSDSpectrum<double>, CSDSpectrum<float> >;
        
        doubleFnMap[&typeid(CSDSpectrum<complex<float> >)]
            = convert<CSDSpectrum<complex<double> >,
                      CSDSpectrum<complex<float> > >;

        // WelchSpectrum (real only)
        doubleFnMap[&typeid(WelchSpectrum<float>)]
            = convert<WelchSpectrum<double>, WelchSpectrum<float> >;
        
        // WelchCSDSpectrum
        doubleFnMap[&typeid(WelchCSDSpectrum<float>)]
            = convert<WelchCSDSpectrum<double>, WelchCSDSpectrum<float> >;
        
        doubleFnMap[&typeid(WelchCSDSpectrum<complex<float> >)]
            = convert<WelchCSDSpectrum<complex<double> >,
                      WelchCSDSpectrum<complex<float> > >;

        // DFT - complex<float> only, uses Sequence converter
        doubleFnMap[&typeid(DFT<complex<float> >)]
            = convert<DFT<complex<double> >, DFT<complex<float> > >;
        
        return doubleFnMap;
    }

    //
    // Static objects
    //
    
    //
    // These are the objects we use to look up conversion functions. They
    // are const references to static unordered_maps contained inside the
    // initFloat(Double)FnMap() function. It's about as threadsafe as possible
    // because a) it's a reference and so must be initialised or the compiler
    // will complain b) it's initialised when the library is loaded
    // and c) it's const
    //
    static const ConversionFnMap& theFloatFnMap = initFloatFnMap();
    static const ConversionFnMap& theDoubleFnMap = initDoubleFnMap();
    
#undef INSTANTIATE
#define INSTANTIATE(TOut,TIn)					\
    template udt* convert< TOut, TIn >(const udt& udt_in)

    // ---- float -------------------------------------------------------
    INSTANTIATE( Scalar<float>, Scalar<int> );
    INSTANTIATE( Scalar<float>, Scalar<double> );
    INSTANTIATE( Scalar<complex<float> >, Scalar<complex<double> > );

    INSTANTIATE( Sequence<float>, Sequence<double> );
    INSTANTIATE( Sequence<complex<float> >, Sequence<complex<double> > );
        
    INSTANTIATE( TimeSeries<float>, TimeSeries<double> );
    INSTANTIATE( TimeSeries<complex<float> >, TimeSeries<complex<double> > );

    INSTANTIATE( FrequencySequence<float>, FrequencySequence<double> );
    INSTANTIATE( FrequencySequence<complex<float> >,
		 FrequencySequence<complex<double> > );

    INSTANTIATE( TimeBoundedFreqSequence<float>,
		 TimeBoundedFreqSequence<double> );
        
    INSTANTIATE( TimeBoundedFreqSequence<complex<float> >,
		 TimeBoundedFreqSequence<complex<double> > );

    INSTANTIATE( CSDSpectrum<float>, CSDSpectrum<double> );
    INSTANTIATE( CSDSpectrum<complex<float> >,
		 CSDSpectrum<complex<double> > );

    INSTANTIATE( WelchSpectrum<float>, WelchSpectrum<double> );
        
    INSTANTIATE( WelchCSDSpectrum<float>, WelchCSDSpectrum<double> );
    INSTANTIATE( WelchCSDSpectrum<complex<float> >,
		 WelchCSDSpectrum<complex<double> > );

    INSTANTIATE( DFT<complex<float> >, DFT<complex<double> > );

    // ---- double ------------------------------------------------------

    INSTANTIATE( Scalar<double>, Scalar<int> );
    INSTANTIATE( Scalar<double>, Scalar<float> );
    INSTANTIATE( Scalar<complex<double> >, Scalar<complex<float> > );

    INSTANTIATE( Sequence<double>, Sequence<float> );
    INSTANTIATE( Sequence<complex<double> >, Sequence<complex<float> > );
        
    INSTANTIATE( TimeSeries<double>, TimeSeries<float> );
    INSTANTIATE( TimeSeries<complex<double> >, TimeSeries<complex<float> > );

    INSTANTIATE( FrequencySequence<double>, FrequencySequence<float> );
    INSTANTIATE( FrequencySequence<complex<double> >,
		 FrequencySequence<complex<float> > );

    INSTANTIATE( TimeBoundedFreqSequence<double>,
		 TimeBoundedFreqSequence<float> );
    INSTANTIATE( TimeBoundedFreqSequence<complex<double> >,
		 TimeBoundedFreqSequence<complex<float> > );

    INSTANTIATE( CSDSpectrum<double>, CSDSpectrum<float> );
    INSTANTIATE( CSDSpectrum<complex<double> >,
		 CSDSpectrum<complex<float> > );

    INSTANTIATE( WelchSpectrum<double>, WelchSpectrum<float> );
        
    INSTANTIATE( WelchCSDSpectrum<double>, WelchCSDSpectrum<float> );
    INSTANTIATE( WelchCSDSpectrum<complex<double> >,
		 WelchCSDSpectrum<complex<float> > );

    INSTANTIATE( DFT<complex<double> >, DFT<complex<float> > );
#undef INSTANTIATE
} // anonymous namespace

namespace datacondAPI {

    udt* convert_to_float(const udt& in)
    {
        //
        // Look up the corresponding conversion function in the function table
        //
        ConversionFnMap::const_iterator iter
            = theFloatFnMap.find(&typeid(in));

        if (iter == theFloatFnMap.end())
        {
            // There was no function in the table for an
            // object of this type - throw an exception
            std::ostringstream oss;
            oss << "No conversion to float for "
                << TypeInfoTable.GetName(typeid(in));

            throw std::invalid_argument(oss.str());
        }

        // Now call the function, which is the second element
        // of the pair obtained in the map lookup
        return (*iter).second(in);
    }

    udt* convert_to_double(const udt& in)
    {
        //
        // Look up the corresponding conversion function in the function table
        //
        ConversionFnMap::const_iterator iter
            = theDoubleFnMap.find(&typeid(in));

        if (iter == theDoubleFnMap.end())
        {
            // There was no function in the table for an
            // object of this type - throw an exception
            std::ostringstream oss;
            oss << "No conversion to double for "
                << TypeInfoTable.GetName(typeid(in));

            throw std::invalid_argument(oss.str());
        }

        // Now call the function, which is the second element
        // of the pair obtained in the map lookup
        return (*iter).second(in);
    }

} // namespace datacondAPI

