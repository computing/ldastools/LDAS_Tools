#include "datacondAPI/config.h"

#include "CoherenceFunction.hh"
#include "Coherence.hh"

static CoherenceFunction _CoherenceFunction;

using namespace datacondAPI;

CoherenceFunction::
CoherenceFunction():
Function(CoherenceFunction::GetName())
{
}

const std::string& CoherenceFunction::GetName(void) const
{
    static std::string name("coherence");
    return name;
}

void CoherenceFunction::Eval(CallChain* Chain,
                       const CallChain::Params& Params,
                       const std::string& Ret) const
{
    // z = coherence(x, y);
    //
    // z = csd(x, y) / sqrt(psd(x) * psd(y))

    if (Params.size() < 2 || Ret == "")
    {
        throw CallChain::LogicError("too few arguments or no return value for coherence action");
    }

    try
    {

        datacondAPI::WelchCSDEstimate wcsde;
        
        if (Params.size() > 2 && Params[2] != "")
        {
            wcsde.set_fftLength(*(Chain->GetSymbol(Params[2])));        
        }

        if (Params.size() > 3 && Params[3] != "")
        {
            wcsde.set_window(*(Chain->GetSymbol(Params[3])));
        }

        if (Params.size() > 4 && Params[4] != "")
        {
            wcsde.set_overlapLength(*(Chain->GetSymbol(Params[4])));
        }

        if (Params.size() > 5 && Params[5] != "")
        {
            wcsde.set_detrendMethod(*(Chain->GetSymbol(Params[5])));
        }

        datacondAPI::Coherence coherer(wcsde);

        udt* out = 0;

        coherer.apply(out, *(Chain->GetSymbol(Params[0])), *(Chain->GetSymbol(Params[1])));

        Chain->ResetOrAddSymbol(Ret, out);

	}
	catch (std::exception& x)
	{
	    throw CallChain::LogicError(std::string("coherence: intercepted ") + x.what());
	}
	catch (...)
	{
	    throw CallChain::LogicError("coherence: intercepted nonstd exception");
	}
}
