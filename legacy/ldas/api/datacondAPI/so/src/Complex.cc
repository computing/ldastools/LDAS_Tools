/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include <memory>

#include "general/Memory.hh"
#include "general/unordered_map.hh"

#include "filters/valarray_utils.hh"

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"
#include "util.hh"

#include "Complex.hh"

using std::complex;
using namespace datacondAPI;
using std::unique_ptr;
using General::unordered_map;

namespace {

    //
    //
    // The trivial converters are used when the udt is already complex.
    // It is provided because the maps can only contain non-member functions.
    //
    udt* convert_trivial(const udt& udt_in)
    {
        return udt_in.Clone();
    }

    //
    // Convert a Scalar
    //
    template<class TOut, class TIn> inline
    void convert_specific(Scalar<complex<TOut> >& out,
                          const Scalar<TIn>& in)
    {
        out.SetValue(complex<TOut>(in.GetValue()));
    }

    //
    // Convert the Sequence part of an object
    //
    template<class T> inline
    void convert_specific(Sequence<complex<T> >& out,
                          const Sequence<T>& in)
    {
        // Do UDT part
        out.udt::operator=(in);

        // Do valarray part
        Filters::valarray_copy(out, in);
    }

    template<class T> inline
    void convert_specific(TimeSeries<complex<T> >& out,
                          const TimeSeries<T>& in)
    {
        convert_specific(static_cast<Sequence<complex<T> >&>(out),
                         static_cast<const Sequence<T>&>(in));
        
        out.TimeSeriesMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(FrequencySequence<complex<T> >& out,
                          const FrequencySequence<T>& in)
    {
        convert_specific(static_cast<Sequence<complex<T> >&>(out),
                         static_cast<const Sequence<T>&>(in));
        
        out.FrequencyMetaData::operator=(in);
        out.GeometryMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(TimeBoundedFreqSequence<complex<T> >& out,
                          const TimeBoundedFreqSequence<T>& in)
    {
        convert_specific(static_cast<FrequencySequence<complex<T> >&>(out),
                       static_cast<const FrequencySequence<T>&>(in));
        
        out.TimeBoundedFreqSequenceMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(CSDSpectrum<complex<T> >& out,
                          const CSDSpectrum<T>& in)
    {
     convert_specific(static_cast<TimeBoundedFreqSequence<complex<T> >&>(out),
                      static_cast<const TimeBoundedFreqSequence<T>&>(in));
        
        out.CSDSpectrumUMD::operator=(in);
    }

    template<class T> inline
    void convert_specific(WelchSpectrum<complex<T> >& out,
                          const WelchSpectrum<T>& in)
    {
        convert_specific(static_cast<CSDSpectrum<complex<T> >&>(out),
                         static_cast<const CSDSpectrum<T>&>(in));
        
        out.WelchSpectrumUMD::operator=(in);
    }

    template<class T> inline
    void convert_specific(WelchCSDSpectrum<complex<T> >& out,
                          const WelchCSDSpectrum<T>& in)
    {
        convert_specific(static_cast<CSDSpectrum<complex<T> >&>(out),
                         static_cast<const CSDSpectrum<T>&>(in));
        
        out.WelchCSDSpectrumUMD::operator=(in);
    }
    
    //
    // This is the top level dispatcher - different versions of
    // this function are instantiated and inserted into a map
    // which takes type_info to an appropriate function for
    // converting TIn to TOut.
    //
    template<class TOut, class TIn>
    udt* convert(const udt& udt_in)
    {
        const TIn& in = dynamic_cast<const TIn&>(udt_in);
        unique_ptr<TOut> out(new TOut());
        
        convert_specific(*out, in);
        
        return out.release();
    }

    //
    // These are the prototypes that all functions contained in the map
    // must adhere to. Note that the function uses UDT arguments because
    // all the functions contained in the map must have the same signature,
    // so a templated function wouldn't work.
    //
    // All conversion functions take a single UDT argument and return a
    // pointer to a UDT at the new precision created on the heap
    //
    typedef udt* (*ConversionFn)(const udt& in);
    
    //
    // A ConversionFnMap is a unordered_map that uses a type_info*
    // as the key and contains ConversionFn's as its elements
    //
    typedef unordered_map< const std::type_info*, ConversionFn,
			   LookupHash, LookupEq > ConversionFnMap;
    
    //
    // A function that we can use to initialise the unordered_map when the
    // library is loaded
    //
    const ConversionFnMap& initComplexFnMap()
    {
        static ConversionFnMap complexFnMap;
        
        // Lets be ultra-paranoid
        if (complexFnMap.size() != 0)
        {
            return complexFnMap;
        }
        
        //
        // Now we insert the function pointers into the map by typeid
        //

        // Trivial conversions for things that are already complex - 
        // they are "converted" via cloning

        // Trivial Scalar
        complexFnMap[&typeid(Scalar<complex<float> >)] = convert_trivial;
        complexFnMap[&typeid(Scalar<complex<double> >)] = convert_trivial;

        // Trivial Sequence
        complexFnMap[&typeid(Sequence<complex<float> >)] = convert_trivial;
        complexFnMap[&typeid(Sequence<complex<double> >)] = convert_trivial;

        // Trivial TimeSeries
        complexFnMap[&typeid(TimeSeries<complex<float> >)] = convert_trivial;
        complexFnMap[&typeid(TimeSeries<complex<double> >)] = convert_trivial;

        // Trivial FrequencySequence
        complexFnMap[&typeid(FrequencySequence<complex<float> >)]
            = convert_trivial;
        complexFnMap[&typeid(FrequencySequence<complex<double> >)]
            = convert_trivial;

        // Trivial TimeBoundedFreqSequence
        complexFnMap[&typeid(TimeBoundedFreqSequence<complex<float> >)]
            = convert_trivial;
        complexFnMap[&typeid(TimeBoundedFreqSequence<complex<double> >)]
            = convert_trivial;

        // Trivial CSDSpectrum
        complexFnMap[&typeid(CSDSpectrum<complex<float> >)]
            = convert_trivial;
        complexFnMap[&typeid(CSDSpectrum<complex<double> >)]
            = convert_trivial;

        // WelchSpectrum can only be real, so no trivial (ie. already complex)
        // conversion

        // Trivial WelchCSDSpectrum
        complexFnMap[&typeid(WelchCSDSpectrum<complex<float> >)]
            = convert_trivial;
        complexFnMap[&typeid(WelchCSDSpectrum<complex<double> >)]
            = convert_trivial;

        // Trivial DFT
        complexFnMap[&typeid(DFT<complex<float> >)] = convert_trivial;
        complexFnMap[&typeid(DFT<complex<double> >)] = convert_trivial;

        // Non-trivial conversions

        // Scalar
        // (yes, int to double is meant to be that way, since we're trying
        // to expunge single-precision Scalars)
        complexFnMap[&typeid(Scalar<int>)]
            = convert<Scalar<complex<double> >, Scalar<int> >;
        complexFnMap[&typeid(Scalar<double>)]
            = convert<Scalar<complex<double> >, Scalar<double> >;

        // Sequence
        complexFnMap[&typeid(Sequence<float>)]
            = convert<Sequence<complex<float> >, Sequence<float> >;
        complexFnMap[&typeid(Sequence<double>)]
            = convert<Sequence<complex<double> >, Sequence<double> >;

        // TimeSeries
        complexFnMap[&typeid(TimeSeries<float>)]
            = convert<TimeSeries<complex<float> >, TimeSeries<float> >;
        complexFnMap[&typeid(TimeSeries<double>)]
            = convert<TimeSeries<complex<double> >, TimeSeries<double> >;

        // FrequencySequence
        complexFnMap[&typeid(FrequencySequence<float>)]
    = convert<FrequencySequence<complex<float> >, FrequencySequence<float> >;
        complexFnMap[&typeid(FrequencySequence<double>)]
    = convert<FrequencySequence<complex<double> >, FrequencySequence<double> >;

        // TimeBoundedFreqSequence
        complexFnMap[&typeid(TimeBoundedFreqSequence<float>)]
            = convert<TimeBoundedFreqSequence<complex<float> >,
            TimeBoundedFreqSequence<float> >;
        complexFnMap[&typeid(TimeBoundedFreqSequence<double>)]
            = convert<TimeBoundedFreqSequence<complex<double> >,
            TimeBoundedFreqSequence<double> >;

        // CSDSpectrum
        complexFnMap[&typeid(CSDSpectrum<float>)]
            = convert<CSDSpectrum<complex<float> >, CSDSpectrum<float> >;
        complexFnMap[&typeid(CSDSpectrum<double>)]
            = convert<CSDSpectrum<complex<double> >, CSDSpectrum<double> >;

        // WelchSpectrum's are real only - convert to complex as a CSDSpectrum
        complexFnMap[&typeid(WelchSpectrum<float>)]
            = convert<CSDSpectrum<complex<float> >, WelchSpectrum<float> >;
        complexFnMap[&typeid(WelchSpectrum<double>)]
            = convert<CSDSpectrum<complex<double> >, WelchSpectrum<double> >;
        
        // WelchCSDSpectrum
        complexFnMap[&typeid(WelchCSDSpectrum<float>)]
            = convert<WelchCSDSpectrum<complex<float> >,
            WelchCSDSpectrum<float> >;
        complexFnMap[&typeid(WelchCSDSpectrum<double>)]
            = convert<WelchCSDSpectrum<complex<double> >,
            WelchCSDSpectrum<double> >;

        // DFT - they only come in complex, so the trivial one is all we need
        
        return complexFnMap;
    }

    //
    // Static objects
    //
    
    //
    // This is the object we use to look up conversion functions. It is a
    // const reference to a static unordered_maps contained inside the
    // initComplexFnMap() function. It's about as threadsafe as possible
    // because a) it's a reference and so must be initialised or the compiler
    // will complain b) it's initialised when the library is loaded
    // and c) it's const
    //
    static const ConversionFnMap& theComplexFnMap = initComplexFnMap();
    
#if __SUNPRO_CC
    //-------------------------------------------------------------------
    // Need to instantiate the function objects
    //-------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE(A,B) template udt* convert< A, B >(const udt& udt_in)

    INSTANTIATE( Scalar<complex<double> >, Scalar<int> );
    INSTANTIATE( Scalar<complex<double> >, Scalar<double> );

    INSTANTIATE( Sequence<complex<float> >, Sequence<float> );
    INSTANTIATE( Sequence<complex<double> >, Sequence<double> );

    INSTANTIATE( TimeSeries<complex<float> >, TimeSeries<float> );
    INSTANTIATE( TimeSeries<complex<double> >, TimeSeries<double> );

    INSTANTIATE( FrequencySequence<complex<float> >,
		 FrequencySequence<float> );
    INSTANTIATE( FrequencySequence<complex<double> >,
		 FrequencySequence<double> );

    INSTANTIATE( TimeBoundedFreqSequence<complex<float> >,
		 TimeBoundedFreqSequence<float> );
    INSTANTIATE( TimeBoundedFreqSequence<complex<double> >,
		 TimeBoundedFreqSequence<double> );

    INSTANTIATE( CSDSpectrum<complex<float> >, CSDSpectrum<float> );
    INSTANTIATE( CSDSpectrum<complex<double> >, CSDSpectrum<double> );

    INSTANTIATE( CSDSpectrum<complex<float> >, WelchSpectrum<float> );
    INSTANTIATE( CSDSpectrum<complex<double> >, WelchSpectrum<double> );
        
    INSTANTIATE( WelchCSDSpectrum<complex<float> >,
		 WelchCSDSpectrum<float> );
    INSTANTIATE( WelchCSDSpectrum<complex<double> >,
		 WelchCSDSpectrum<double> );

#undef INSTANTIATE
#endif
    
} // anonymous namespace

namespace datacondAPI {

    udt* Complex(const udt& in)
    {
        //
        // Look up the corresponding conversion function in the function table
        //
        ConversionFnMap::const_iterator iter
            = theComplexFnMap.find(&typeid(in));

        if (iter == theComplexFnMap.end())
        {
            // There was no function in the table for an
            // object of this type - throw an exception
            std::ostringstream oss;
            oss << "No Complex() operator for "
                << TypeInfoTable.GetName(typeid(in));

            throw std::invalid_argument(oss.str());
        }

        // Now call the function, which is the second element
        // of the pair obtained in the map lookup
        return (*iter).second(in);
    }

    Scalar<complex<double> >* Complex(const Scalar<double>& x)
    {
        // attempt to convert the Scalar to its complex equivalent
        // Remember, Complex returns a dynamically-allocated object
        // so we must hang on to the pointer
        udt* const p = Complex(static_cast<const udt&>(x));

        // Now check if it really did get the right type - this should
        // always succeed but we should check to make sure
        Scalar<complex<double> >* const r
            = dynamic_cast<Scalar<complex<double> >*>(p);
        
        // Check the result of the cast (should always succeed unless there
        // is a bug)
        if (r == 0)
        {
            delete p;
            throw std::bad_cast();
        }

        // dynamic cast worked, so we can safely return the cast pointer

        return r;
    }

    template<class T>
    Sequence<complex<T> >* Complex(const Sequence<T>& x)
    {
        // attempt to convert the Sequence to its complex equivalent
        // Remember, Complex returns a dynamically-allocated object
        // so we must hang on to the pointer
        udt* const p = Complex(static_cast<const udt&>(x));

        // Now check if it really did get the right type - this should
        // always succeed but we should check to make sure
        Sequence<complex<T> >* const r
            = dynamic_cast<Sequence<complex<T> >*>(p);
        
        // Check the result of the cast (should always succeed unless there
        // is a bug)
        if (r == 0)
        {
            delete p;
            throw std::bad_cast();
        }

        // dynamic cast worked, so we can safely return the cast pointer

        return r;
    }

    template
    Sequence<complex<float> >* Complex<float>(const Sequence<float>&);

    template
    Sequence<complex<double> >* Complex<double>(const Sequence<double>&);
}

    
