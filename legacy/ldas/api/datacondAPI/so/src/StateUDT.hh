/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef STATE_UDT_HH
#define STATE_UDT_HH
// $Id: StateUDT.hh,v 1.5 2002/09/27 01:26:04 ldas_anu Exp $
#include "UDT.hh"

namespace datacondAPI
{
  //: Class from which State objects are derived. 
    class State : public udt
    {
    public: 
        virtual State* Clone() const = 0;
    };

} // namespace datacondAPI

#endif // STATE_UDT_HH
