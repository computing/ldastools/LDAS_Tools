/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: Kalman.cc,v 1.22 2009/01/13 23:25:50 emaros Exp $

#include "datacondAPI/config.h"

#include "Kalman.hh"
#include "LinearAlgebra.hh"

namespace 
{
  const char rcsID[] = "@(#) $Id: Kalman.cc,v 1.22 2009/01/13 23:25:50 emaros Exp $";
}

namespace datacondAPI{
  Kalman::Kalman(const Matrix<double>& A, 
		 const Matrix<double>& W, 
		 const Matrix<double>& C, 
		 const Matrix<double>& V, 
		 const Sequence<double>& psi, 
		 const Matrix<double>& P)
    : m_state(A,W,C,V,psi,P)
  {}
  
  Kalman::
  Kalman(const KalmanState& state) :m_state(state)  
  {}
  
  Kalman::
  Kalman(const Kalman& h) : m_state(h.m_state) 
  {}

  Kalman::
  Kalman() {}

  Kalman::
  Kalman(udt& A,
	 udt& W,
	 udt& C,
	 udt& V,
	 udt& psi,
	 udt& P)
    : m_state(A,W,C,V,psi,P)
  {}

  Kalman::
  ~Kalman() {}
  
  void Kalman::getA(Matrix<double>& A) const {m_state.getA(A);}

  void Kalman::getW(Matrix<double>& W) const {m_state.getW(W);}

  void Kalman::getV(Matrix<double>& V) const {m_state.getV(V);}

  void Kalman::getC(Matrix<double>& C) const {m_state.getC(C);}

  void Kalman::getPsi(Sequence<double>& psi) const {m_state.getPsi(psi);}

  void Kalman::
  getP(Matrix<double>& P) const {m_state.getP(P);}


  void Kalman::getA(udt*& A)
  {
    if(A==NULL)
      {
	Matrix<double> *a = (Matrix<double>*)NULL;
	try{
	  a = new Matrix<double>;
	  m_state.getA(*a);
	  A=a;
	}
	catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else if (udt::IsA<Matrix<double> >(*A))
      {
	Matrix<double>& tmp = udt::Cast<Matrix<double> >(*A);
        m_state.getA(tmp);
      }
    else
      throw std::invalid_argument("Wrong UDT type for Kalman.getA");
  }

  void Kalman::getW(udt*& W)
  {
    if(W==NULL)
      {
	Matrix<double> *a = (Matrix<double>*)NULL;
        try{
	  a = new Matrix<double>;
	  m_state.getW(*a);
	  W=a;
        }
        catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else if (udt::IsA<Matrix<double> >(*W))
      {
        Matrix<double>& tmp = udt::Cast<Matrix<double> >(*W);
        m_state.getW(tmp);
      }
    else
      throw std::invalid_argument("Wrong UDT type for Kalman.getW");
  }

  void Kalman::getV(udt*& V)
  {
    if(V==NULL)
      {
	Matrix<double> *a = (Matrix<double>*)NULL;
        try{
	  a = new Matrix<double>;
	  m_state.getV(*a);
	  V=a;
        }
        catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else if (udt::IsA<Matrix<double> >(*V))
      {
        Matrix<double>& tmp = udt::Cast<Matrix<double> >(*V);
        m_state.getV(tmp);
      }
    else
      throw std::invalid_argument("Wrong UDT type for Kalman.getV");
  }

  void Kalman::getC(udt*& C)
  {
    if(C==NULL)
      {
	Matrix<double> *a = (Matrix<double>*)NULL;
        try{
	  a = new Matrix<double>;
	  m_state.getC(*a);
	  C=a;
        }
        catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else if (udt::IsA<Matrix<double> >(*C))
      {
        Matrix<double>& tmp = udt::Cast<Matrix<double> >(*C);
        m_state.getC(tmp);
      }
    else
      throw std::invalid_argument("Wrong UDT type for Kalman.getC");
  }

  void Kalman::getPsi(udt*& psi)
  {
    if(psi==NULL)
      {
	Sequence<double> *a = (Sequence<double>*)NULL;
        try{
	  a = new Sequence<double>;
	  m_state.getPsi(*a);
	  psi=a;
        }
        catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else if (udt::IsA<Sequence<double> >(*psi))
      {
        Sequence<double>& tmp = udt::Cast<Sequence<double> >(*psi);
        m_state.getPsi(tmp);
      }
    else
      throw std::invalid_argument("Wrong UDT type for Kalman.getPsi");
  }

  void Kalman::getP(udt*& P)
  {
    if(P==NULL)
      {
	Matrix<double> *a = (Matrix<double>*)NULL;
        try{
	  a = new Matrix<double>;
	  m_state.getP(*a);
	  P=a;
        }
        catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else if (udt::IsA<Matrix<double> >(*P))
      {
        Matrix<double>& tmp = udt::Cast<Matrix<double> >(*P);
        m_state.getP(tmp);
      }
    else
      throw std::invalid_argument("Wrong UDT type for Kalman.getP");
  }

  void Kalman::getState(KalmanState*& state)
  {
    if(state==NULL)
      {
	KalmanState* a = (KalmanState*)NULL;
	try{
	  a = new KalmanState;
	  *a = m_state;
	  state=a;
	}
	catch(...)
	  {
	    delete a;
	    throw;
	  }
      }
    else{
      *state = m_state;
    }

  }

  template <class out_type, class in_type>  
  void Kalman::apply(VectorSequence<out_type>& z_out, 
		     const VectorSequence<in_type>& in, VectorSequence<out_type>& psi_out, 
		     Matrix<out_type>& P_out) 
  {
    
    int M=0,N=0;
    m_state.getMN(M,N);
    Matrix<out_type> A(M,M), W(M,M), C(N,M), V(N,N), P(N,N), _P_(M,M),
      K(M,N), hP(M,M), prevP(M,M), temp1(M,M), temp2(N,M), temp3(M,N), IDENT(M,M), IDENT1(N,N);
    Sequence<out_type> z(N), hPsi(M), _Psi(M), _z(N), hz(N), prevPsi(M);

    //Get current state so can break input into smaller units
    m_state.getPsi(prevPsi);
    m_state.getP(prevP);
    m_state.getA(A);
    m_state.getW(W);
    m_state.getV(V);
    m_state.getC(C);

	for(int i=0; i<in.sDim(); i++)
	{
	        //~psi[k] = A*^psi[k-1]
	        LinearAlgebra::MV(_Psi,A,prevPsi);

		//~P[k] = A*^P[k-1]*A' + W
		LinearAlgebra::MM(temp1,A,prevP);
		
		_P_=W;
		LinearAlgebra::MM(_P_,temp1,A,1,1,false,true);
		
		//~z[k] = C*~psi[k]
		LinearAlgebra::MV(_z,C,_Psi);
		
		//K[k] = ~P[k]*C'/(V+C*~P[k]*C')
		LinearAlgebra::MM(temp2,C,_P_);

		P=V;
		LinearAlgebra::MM(P,temp2,C,1,1,false,true);	
		LinearAlgebra::MM(temp3,_P_,C,1,0,false,true);

		std::valarray<out_type> I1(0.,N*N);
		for(int j=0; j<N;j++)
		  {
			I1[j*N+j]=1;
			Matrix<out_type> tmp(I1,N,N);
			IDENT1=tmp;	
		  }
		Matrix<out_type> Pinv(P);
		LinearAlgebra::SV(P,Pinv,IDENT1);
		LinearAlgebra::MM(K,temp3,Pinv);	
		
		//^psi[k] = ~psi[k] + K[k]*(z[k] - ~z[k])
		Sequence<in_type> helper(in.vec(i));
		for(size_t k=0;k<z.size();k++)
			z[k]=helper[k]-_z[k];

		LinearAlgebra::MV(hPsi,K,z);

		hPsi+=_Psi;
		
		//^P[k] = (I - K[k]*C)*~P[k]
		std::valarray<out_type> I(0.,M*M);
		for(int j=0; j<M;j++)
		  {
			I[j*M+j]=1;
			Matrix<out_type> tmp(I,M,M);
			IDENT=tmp;	
		  }
		LinearAlgebra::MM(IDENT,K,C,-1,1);
		LinearAlgebra::MM(hP,IDENT,_P_);

		//^z[k] = C*^psi[k]
		LinearAlgebra::MV(hz,C,hPsi);

		//Store psi and P for use in next iteration
		prevPsi=hPsi;
		prevP=hP;
		
		//put ^z[k] and ^psi[k] in output structures
		z_out.vec(i) = hz;
		psi_out.vec(i) = prevPsi;
	}//for
	//put final P in output structure (should eventually be all P's)
	P_out=prevP;

    //store current state so next call can continue where current one left off
    m_state.setPsi(prevPsi);
    m_state.setP(prevP);
  }//apply

  void Kalman::apply(udt*& out, const udt& in)
  {
    bool allocated = false;
    try{
      if(udt::IsA<VectorSequence<double> >(in) &&
	 udt::IsA<VectorSequence<double> >(*out))
      {
	if(out==NULL)
	{
	    out = new VectorSequence<double> (udt::Cast<VectorSequence<double> > (in).vDim(),udt::Cast<VectorSequence<double> > (in).sDim());
	    allocated = true;
	}
       	int M=0, N=0;
	m_state.getMN(M,N);
	VectorSequence<double> psi(M,udt::Cast<VectorSequence<double> >(in).sDim());
	Matrix<double> P (M,M);
	apply(udt::Cast<VectorSequence<double> >(*out),
	      udt::Cast<VectorSequence<double> >(in), psi, P);
      }
      else if(udt::IsA<VectorSequence<float> >(in) &&
	      udt::IsA<VectorSequence<float> >(*out))
	{
	  if(out==NULL)
	    {
	      out = new VectorSequence<float> (udt::Cast<VectorSequence<float> > (in).vDim(),udt::Cast<VectorSequence<float> > (in).sDim());
	      allocated = true;
	    }
	  VectorSequence<float> psi;
	  Matrix<float> P;
	  apply(udt::Cast<VectorSequence<float> >(*out),
		udt::Cast<VectorSequence<float> >(in), psi, P);
	}
      else
	{
	  throw std::invalid_argument("Invalid types for Kalman filter");
	}
    }
    catch(...)
      {
	if(allocated)
	  {
	    delete out;
	    out = 0;
	  }
	throw;
      }
  }

  void Kalman::
  apply(udt*& out,
	const udt& in,
	udt*& psi,
	udt*& P_out)
  {
    bool bpsi=true,bP=true, allocated=false;
    try{
      if(udt::IsA<VectorSequence<double> >(in) &&
	 udt::IsA<VectorSequence<double> >(*out) &&
	 udt::IsA<VectorSequence<double> >(*psi) &&
	 udt::IsA<Matrix<double> >(*P_out) )
	{
	  if(out==NULL)
	    {
	      allocated = true;
	      out = new VectorSequence<double> (udt::Cast<VectorSequence<double> > (in).vDim(),udt::Cast<VectorSequence<double> > (in).sDim());
	    }
	  if(psi==NULL)
	    {
	      bpsi = false;
	    }
	  if(P_out==NULL)
	    {
	      bP = false;
	    }
	  if(bpsi && bP)
	    {
	      apply(udt::Cast<VectorSequence<double> >(*out),
		    udt::Cast<VectorSequence<double> >(in), 
		    udt::Cast<VectorSequence<double> >(*psi), 
		    udt::Cast<Matrix<double> >(*P_out));
	    }
	  else if(bpsi && !bP)
	    {
	      Matrix<double> tP_out;
	      apply(udt::Cast<VectorSequence<double> >(*out),
		    udt::Cast<VectorSequence<double> >(in), 
		    udt::Cast<VectorSequence<double> >(*psi), 
		    tP_out);
	    }
	  else if(!bpsi && bP)
	    {
	      VectorSequence<double> tpsi;
	      apply(udt::Cast<VectorSequence<double> >(*out),
		    udt::Cast<VectorSequence<double> >(in), 
		    tpsi, 
		    udt::Cast<Matrix<double> >(*P_out));
	    }
	  else if(!bpsi && !bP)
	    {
	      Matrix<double> tP_out;
	      VectorSequence<double> tpsi;
	      apply(udt::Cast<VectorSequence<double> >(*out),
		    udt::Cast<VectorSequence<double> >(in), 
		    tpsi, tP_out);
	    }
	}
      if(udt::IsA<VectorSequence<float> >(in) &&
	 udt::IsA<VectorSequence<float> >(*out) &&
	 udt::IsA<VectorSequence<float> >(*psi) &&
	 udt::IsA<Matrix<float> >(*P_out) )
	{
	  if(out==NULL)
	    {
	      allocated = true;
	      out = new VectorSequence<float> (udt::Cast<VectorSequence<float> > (in).vDim(),udt::Cast<VectorSequence<float> >(in).sDim());
	    }
	  if(psi==NULL)
	    {
	      bpsi = false;
	    }
	  if(P_out==NULL)
	    {
	      bP = false;
	    }
	  if(bpsi && bP)
	    apply(udt::Cast<VectorSequence<float> >(*out),
		  udt::Cast<VectorSequence<float> >(in), 
		  udt::Cast<VectorSequence<float> >(*psi), 
		  udt::Cast<Matrix<float> >(*P_out));
	  else if(bpsi && !bP)
	    {
	      Matrix<float> tP_out;
	      apply(udt::Cast<VectorSequence<float> >(*out),
		    udt::Cast<VectorSequence<float> >(in), 
		    udt::Cast<VectorSequence<float> >(*psi), 
		    tP_out);
	    }
	  else if(!bpsi && bP)
	    {
	      VectorSequence<float> tpsi;
	      apply(udt::Cast<VectorSequence<float> >(*out),
		    udt::Cast<VectorSequence<float> >(in), 
		    tpsi, 
		    udt::Cast<Matrix<float> >(*P_out));
	    }
	  else if(!bpsi && !bP)
	    {
	      Matrix<float> tP_out;
	      VectorSequence<float> tpsi;
	      apply(udt::Cast<VectorSequence<float> >(*out),
		    udt::Cast<VectorSequence<float> >(in), 
		    tpsi, tP_out);
	    }
	}
      else
	throw std::invalid_argument("Invalid types for Kalman filter");
    }
    catch(...)
      {
	if(allocated)
	  {
	    delete out;
	    out = 0;
	  }
	throw;
      }
  }
  
}

template void
datacondAPI::Kalman::apply(VectorSequence<double>&,const VectorSequence<double>&,
			   VectorSequence<double>&, Matrix<double>&);

template void
datacondAPI::Kalman::apply(VectorSequence<float>&,const VectorSequence<float>&,
			   VectorSequence<float>&, Matrix<float>&);

