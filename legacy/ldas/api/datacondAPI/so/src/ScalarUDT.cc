/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "config.h"

#include <stdexcept>
#include <complex>

#include "general/types.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldasarray.hh"

#include "ScalarUDT.hh"

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

template <class DataType_>
datacondAPI::Scalar<DataType_>::
Scalar(DataType_ Value)
  : m_value(Value)
{
}

template <class DataType_>
datacondAPI::Scalar<DataType_>::
~Scalar()
{
}

template<class DataType_>
ILwd::LdasElement* datacondAPI::Scalar<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target) const
{
  switch (Target)
  {
  case datacondAPI::udt::TARGET_GENERIC:
    return new ILwd::LdasArray<DataType_>(GetValue());
  default:
    throw datacondAPI::udt::BadTargetConversion(Target,
						"datacondAPI::Scalar");
  }
}

template <class DataType_>
datacondAPI::Scalar< DataType_ >* datacondAPI::Scalar<DataType_>::
Clone(void) const
{
  return new Scalar<DataType_>(m_value);
}

#undef	CLASS_INSTANTIATION
#define	CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::Scalar< class_ >; \
UDT_CLASS_INSTANTIATION(Scalar< class_ >,key_)

CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(short,short)
CLASS_INSTANTIATION(int,int)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)
