#ifndef LINE_REMOVER_HH
#define LINE_REMOVER_HH

#include <complex>
#include <valarray>
#include <vector>

#include "StateUDT.hh"

namespace datacondAPI
{

    class LineRemover : public State
    {

    public:

	// override defaults

	//: Construct a line remover
	LineRemover(const double& frequency,
            const std::size_t& factor,
            const std::size_t& order);

        //: Constructs a line remover
        LineRemover(const udt& frequency, const udt& factor, const udt& order);

	//: Copy a line remover
	//!param: a - line remover to be copied
	LineRemover(const LineRemover& a);

	//: Destructor
	~LineRemover();

	//: Assignment
	//!param: a - line remover to be copied
	LineRemover& operator=(const LineRemover& a);

	// UDT functionality

	//: Deep copy
	//!return: Pointer to a deep copy of the line remover
	virtual LineRemover* Clone() const;

	virtual ILwd::LdasElement*
	    ConvertToIlwd(const CallChain& Chain,
			  udt::target_type Target = udt::TARGET_GENERIC) const;

        // accessors
        void getFilter(udt*& b) const;

	// fit model

	//: (Re)Fit the line remover to system input and output
	//!param: y - system output
	//!param: u - system input
	template<typename type>
	void refine(const std::valarray<type>& y,
		      const std::valarray<type>& u);

	//: (Re)Fit the line remover to system input and output
	//!param: y - system output
	//!param: u - system input
	void refine(const udt& y,
		      const udt& u);

	// predict

	//: Apply the line remover to system input to produce a prediction
	//!param: w - prediction
	//!param: u - system input
	template<typename type>
	void apply(std::valarray<type>& w,
	           const std::valarray<type>& u);

	//: Apply the line remover to system input to produce a prediction
	//!param: w - prediction
	//!param: u - system input
	void apply(udt*& w,
		   const udt& u);

    private:

        LineRemover();

	class Abstraction;

	template<typename type> // scalar only
	class Implementation;

	double m_frequency;
	std::size_t m_factor;
        std::size_t m_order;

	Abstraction* m_data;

    };

}

#endif // LINE_REMOVER_HH
