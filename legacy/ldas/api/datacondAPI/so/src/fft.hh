#ifndef FFT_HH
#define FFT_HH

// $Id: fft.hh,v 1.27 2006/02/16 16:54:58 emaros Exp $

// Forward declarations
namespace std {
    template<class T> class complex;
    template<class T> class valarray;
}

namespace datacondAPI {

    // Forward declarations
    class udt;

    template<class T> class Sequence;

    //
    //: Discrete Fourier transform (DFT) class.
    //
    //  Class to calculate the discrete Fourier transform of a time series
    //  using the FFT algorithm.
    //
    class FFT {
    public:

        //
        //: Apply DFT to a UDT
        //
        // The DFT of the data given in in is returned
        // in the UDT pointed to by out.
        //
	// The conventions used are that the DFT of a series $x_j$ is
	// given by
	// <pre>
	//$$
	//    X_k = \sum_{j=0}^{N-1} x_j \exp(-i 2 \pi j k/N)
	//$$
	// </pre>
        //
	//
	// The behaviour of the <code>apply()</code> function depends on the
	// value of the output pointer. If the
        // pointer is 0, a new UDT of the appropriate type is created
        // on the heap and populated with the result. In this case,
        // the caller is responsible for deleting the memory
	// owned by the output parameter.
	// <p>
	// If the pointer is non-zero on input, the UDT it points to
	// is used to contain the output.
        // If the type of this UDT is not appropriate for the
	// operation, an invalid_argument exception is thrown.
        // For the FFT class, the pointer must point to a
        // DFT&lt;complex&lt;T&gt; &gt;.
	// <p>
	// The following table shows what type <code>out</code> must be
	// if it is non-zero, or else what type it will point
	// to after the call if it was initially zero.
	// 
	// <table border=1>
        // <tr>
	// <th>Type of <code>out</code></th>
	// <th>Type of <code>in</code></th>
	// </tr>
        // <tr>
	// <td>
	// <code>DFT&lt;complex&lt;float&gt; &gt;</code> with Hermitian symmetry
	// </td>
	// <td>
	// <code>Sequence&lt;float&gt;</code>
	// </td>
	// </tr>
        // <tr>
	// <td>
	// <code>DFT&lt;complex&lt;double&gt; &gt;</code> with Hermitian symmetry
	// </td>
	// <td>
	// <code>Sequence&lt;double&gt;</code>
	// </td>
	// </tr>
        // <tr>
	// <td>
	// <code>DFT&lt;complex&lt;float&gt; &gt;</code> without Hermitian symmetry
	// </td>
	// <td>
	// <code>Sequence&lt;complex&lt;float&gt; &gt;</code>
	// </td>
	// </tr>
        // <tr>
	// <td>
	// <code>DFT&lt;complex&lt;double&gt; &gt;</code> without Hermitian symmetry
	// </td>
	// <td>
	// <code>Sequence&lt;complex&lt;double&gt; &gt;</code>
	// </td>
	// </tr>
        // </table>
        //
	// <p>
        // <b>Examples</b>
        // <p>
        // <pre>
	//    // Ex 1. Dynamic allocation done by apply()
        //
        //    udt* out = 0;
        //    FFT  fft;
        //
        //    fft.apply(out, in);
        //    fft.apply(out, in);
        //    // ... (reuse 'out' as many times as you like)
        //
        //    // do something with out, remember to delete
        //    // when finished 
        //    delete out;
	// </pre>
        // <pre>
	//    // Ex 2. Dynamic or static allocation done by user
        //
        //    DFT&lt;complex&lt;float&gt; &gt; out;
        //    FFT  fft;
        //
        //    fft.apply(&amp;out, in);
        //
        //    // do something with out, don't delete when finished 
	// </pre>
        //!param: in - A reference to the UDT to be acted on,
        //+ which must be a UDT derived from Sequence&lt;T&gt;
        //
        //!param: out - A pointer to a UDT to contain the
        //+ DFT of in, or else a null pointer. If the pointer is null
	//+ on input, a new UDT object of the appropriate type will be created
	//+ on the heap. It is the users responsibility to delete this object
	//
        //!exc: std::invalid_argument - Thrown if the input argument is not a
        //+ Sequence&lt;T&gt;
        //!exc: std::invalid_argument - Thrown if the output argument is 
        //+ non-zero and not a DFT&lt;complex&lt;T&gt; &gt;
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &lt;= 0
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &gt; MaxFFTLength
        //
        void apply(udt*& out, const udt& in);

        //: Apply DFT to a real, single precision valarray
        //
        // The DFT of the data given in in is returned
        // in out.
        //
        //!param: in - A reference to the valarray to be acted on
        //
        //!param: out - A reference to a valarray to contain the
        //+ DFT of in
	//
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &lt;= 0
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &gt; MaxFFTLength
        //
	void apply(std::valarray<std::complex<float> >& out,
                   const std::valarray<float>& in);

        //: Apply DFT to a real, double precision valarray
        //
        // The DFT of the data given in in is returned
        // in out.
        //
        //!param: in - A reference to the valarray to be acted on
        //
        //!param: out - A reference to a valarray to contain the
        //+ DFT of in
	//
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &lt;= 0
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &gt; MaxFFTLength
        //
        void apply(std::valarray<std::complex<double> >& out,
                   const std::valarray<double>&  in);
	
        //: Apply DFT to a complex, single precision valarray
        //
        // The DFT of the data given in in is returned
        // in out.
        //
        //!param: in - A reference to the valarray to be acted on
        //
        //!param: out - A reference to a valarray to contain the
        //+ DFT of in
	//
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &lt;= 0
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &gt; MaxFFTLength
        //
        void apply(std::valarray<std::complex<float> >& out,
                   const std::valarray< std::complex<float> >& in);
	
        //: Apply DFT to a complex, double precision valarray
        //
        // The DFT of the data given in in is returned
        // in out.
        //
        //!param: in - A reference to the valarray to be acted on
        //
        //!param: out - A reference to a valarray to contain the
        //+ DFT of in
	//
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &lt;= 0
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &gt; MaxFFTLength
        //
        void apply(std::valarray<std::complex<double> >& out,
                   const std::valarray< std::complex<double> >& in);
        
    private:
        //: Helper function for implementing double-dispatching based
        //+ on the dynamic types of in and out
        //
        // The DFT of the data given in in is returned
        // in the UDT pointed to by out.
        //
        //!param: in - A reference to the Sequence&lt;T&gt; to be acted on
        //
        //!param: out - A pointer to a UDT to contain the
        //+ DFT of in. The behaviour of the
        //+ function depends on the value of this parameter. If the
        //+ pointer is 0, a new UDT of the appropriate type is created
        //+ on the heap and populated with the result. In this case,
        //+ the caller is responsible for deleting the memory
	//+ owned by the output parameter.<p>
	//+ If the pointer is non-zero on input, the UDT it points to
	//+ is used to contain the output.
        //+ If the type of this UDT is not appropriate for the
	//+ operation, an invalid_argument exception is thrown.
        //+ For the FFT class, the pointer must point to a
        //+ DFT&lt;complex&lt;T&gt; &gt;
	//
        //!exc: std::invalid_argument - Thrown if the output argument is 
        //+ non-zero and not a DFT&lt;complex&lt;T&gt; &gt;
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &lt;= 0
        //!exc: std::invalid_argument - Thrown if the length of the input
	//+ array is &gt; MaxFFTLength
        //
	template<class TOut, class TIn>
	void dispatch(udt*& out, const Sequence<TIn>& in);
    };

} // namespace datacondAPI

#endif // FFT_HH
