//daub4.hh

#ifndef DAUB4_HH
#define DAUB4_HH

#include <general/unexpected_exception.hh>
#include "WaveletPSU.hh"

namespace datacondAPI {
namespace psu
{
  //this class inherits from class Wavelet
  class Daub4 : public Wavelet 
  {
  public:

    //constructors and destructors

    //: Constructor 
    Daub4(); 
    
    //: Default destructor
    ~Daub4();

    //: Duplicate on heap
    Daub4* Clone() const;

    
    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;
    
  }; //class Daub4

} //namespace psu
} //namespace datacondAPI

#endif

