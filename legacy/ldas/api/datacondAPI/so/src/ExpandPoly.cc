#include "datacondAPI/config.h"

#include <complex>
#include <stdexcept>
#include <string>
#include <valarray>

#include <filters/LinFilt.hh>

#include "ExpandPoly.hh"

using std::complex;
using std::valarray;
using std::slice;

namespace datacondAPI {

    template<class T>
    void multiplyPolys(std::valarray<T>& c, const std::valarray<T>& d)
    {
        if (d.size() <= 0)
        {
	    throw std::invalid_argument( "multiplyPolys() - "
					 "second array has size zero" );
        }

        if (c.size() <= 0)
        {
            throw std::invalid_argument("multiplyPolys() - "
                                        "first array has size zero");
        }

        valarray<T> cTmp(c);
        
        const size_t l = c.size() + d.size() - 1;
        
        c.resize(l, T(0));
        c[slice(0, cTmp.size(), 1)] = cTmp;
        
        Filters::LinFilt<T, T> lf(d);
        
        lf.apply(c);
    }

    template<class T>
    void expandPoly(std::valarray<T>& c,
                    const std::valarray<std::complex<T> >& r,
                    const T& gain)
    {
        // Initial polynomial is P(x) = 1
        c.resize(1);
        c[0] = gain;
        
        for (size_t k = 0; k < r.size(); ++k)
        {
            if (r[k].imag() == 0)
            {
                // factor is -r[k].real() + x
                valarray<T> d(2);
                d[0] = -r[k].real();
                d[1] = 1;
                multiplyPolys(c, d);
            }
            else
            {
                // factor is norm(r[k]) - 2 Re(r[k]) x + x^2
                valarray<T> d(3);
                d[0] = norm(r[k]);
                d[1] = -2*r[k].real();
                d[2] = 1;
                multiplyPolys(c, d);
            }
        }
    }

    template void expandPoly<float>(std::valarray<float>&,
                                 const std::valarray<std::complex<float> >&,
                                    const float&);

    template void expandPoly<double>(std::valarray<double>&,
                                 const std::valarray<std::complex<double> >&,
                                     const double&);

} // namespace datacondAPI
