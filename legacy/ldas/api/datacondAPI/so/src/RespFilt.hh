#ifndef RESPFILT_HH
#define RESPFILT_HH

#include <complex>

#include "TimeSeries.hh"
#include "FrequencySequenceUDT.hh"

namespace datacondAPI {

    enum RespFiltDirection {
        RESPFILT_FORWARD,
        RESPFILT_BACKWARD
    };

    //
    //: Convert data between "strain" and "ADC counts" using calibration
    //+information
    //
    // This function uses calibration information from a frame file
    // to construct a transfer function, which is applied to the input
    // time-series ts in-place.
    //
    // The function has an option to specify the "direction" of the
    // transformation. In the RESPFILT_FORWARD direction,
    // it is assumed that the input ts contains data in units of "strain".
    // On return, the time-series will have been convolved with the transfer
    // function so that it is in the form of "ADC counts".
    //
    // In the RESPFILT_BACKWARD direction,
    // it is assumed that the input ts contains data in units of "ADC counts".
    // On return, the time-series will have been convolved with the inverse
    // of the transfer function so that it is in the form of "strain".
    //
    // The input arguments are the response function R(f), the sensing
    // function C(f), and time-series alphas(t) and gammas(t) representing
    // a series of measurements based on calibration lines injected into the
    // interferometer data. The transfer function T is constructed in the
    // following manner. First, the values of alpha and gamma at the
    // start-time of ts are determined (if this start-time falls outside
    // the time-interval spanned by either alphas or gammas, an exception
    // is thrown). Then, the transfer function is given by
    //
    //     T = alpha*C/(1 + gamma*H)   where  H = C*R - 1
    //
    // T is then interpolated to the frequency-spacing of ts, and extended
    // to a 2-sided transfer function. The Fourier transform of ts is
    // then calculated and multiplied by T (or 1/T if the direction is
    // RESPFILT_BACKWARD). The final result is obtained using the inverse
    // Fourier transform.
    //
    //!param: ts - the time-series to be convolved. Must be real.
    //!param: R - the response function
    //!param: C - the sensing function
    //!param: alphas - the time-series of measured values of alpha
    //!param: gammas - the time-series of measured values of gamma
    //!param: direction - the direction of the transformation,
    //+RESPFILT_FORWARD (default) or RESPFILT_BACKWARD
    //
    //!exc: runtime_error: - Thrown if ts.size() == 0
    //!exc: runtime_error: - Thrown if R and C have different meta-data (size,
    //+ step-size or start frequency)
    //!exc: runtime_error: - Thrown if the start-time of ts is not contained
    //+ in the time spanned by the alphas or gammas
    template<class T>
    void respFilt(TimeSeries<T>& ts,
                  const FrequencySequence<std::complex<float> >& R,
                  const FrequencySequence<std::complex<float> >& C,
                  const TimeSeries<std::complex<float> >& alphas,
                  const TimeSeries<std::complex<float> >& gammas,
                  const RespFiltDirection& direction = RESPFILT_FORWARD);

}

#endif // RESPFILT_HH

