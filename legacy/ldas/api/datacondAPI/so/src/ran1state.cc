#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ran1state.hh"
#include "random.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

namespace datacondAPI{

  ran1state::ran1state()
  {
    idum = -248673140;
    iy = 0;
    iv.resize(NTAB);
  }

  ran1state::ran1state(int seed)
  {
    idum = seed;
    iy = 0;
    iv.resize(NTAB);
  }

  ran1state::ran1state(const ran1state& g)
    : State( g ),
      iv(g.iv)
  {
    idum = g.idum;
    iy = g.iy;
  }

  //restest state with new seed s
  void ran1state::seed(int s)
  {
    idum = s;
    iy = 0;
    //removes previous values in iv so that state is reset
    iv.resize(NTAB);
    iv=0;
  }

  float ran1state::getran()
  {
	int j;
	long k;
	//static long iy=0;
	//static long iv[NTAB];
	float temp;
	
	if(idum <= 0 || !iy)
	{
		if(-idum <1)
			idum=1;
		else
			idum=-idum;
		for(j=NTAB+7; j>=0; j--)
		{
			k=idum/IQ;
			idum=IA*(idum-k*IQ)-IR*k;
			if(idum<0)
				idum +=IM;
			if(j<NTAB)
				iv[j] =idum;
		}
		iy = iv[0];
	}
	k=idum/IQ;
	idum=IA*(idum-k*IQ)-IR*k;
        if(idum<0)
		idum +=IM;
	j=(int)(iy/NDIV);
	iy=iv[j];
	iv[j] = idum;
	if((temp=AM*iy)>RNMX)
	  return RNMX;
	else 
	  return temp;
  }

  void ran1state::getran(std::valarray<float>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = getran();
  }

  ran1state* ran1state::Clone() const
  {
    return new ran1state(*this);
  }

  ILwd::LdasElement* ran1state::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");
    return container;
  }

}
