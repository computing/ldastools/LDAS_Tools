/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef DATACONDAPI__DETECTOR_UMD_HH
#define DATACONDAPI__DETECTOR_UMD_HH

#include <string>

#include "general/types.hh"

namespace ILwdFCS
{
  class FrDetector;
}

namespace datacondAPI
{
  //: Metadata specifying the properties of a detector
  class DetectorMetaData
  {
  public:

    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrDetector& Storage - source of values
    void Read( const ILwdFCS::FrDetector& Source );
    
    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrDetector& Storage - place to store values
    void Store( ILwdFCS::FrDetector& Storage ) const;
    
  private:
    std::string m_name;
    INT_2S	m_longitude_d;
    INT_2S	m_longitude_m;
    REAL_4	m_longitude_s;
    INT_2S	m_latitude_d;
    INT_2S	m_latitude_m;
    REAL_4	m_latitude_s;
    REAL_4	m_elevation;
    REAL_4	m_arm_x_azimuth;
    REAL_4	m_arm_y_azimuth;
  };
} // datacondAPI

#endif	/* DATACONDAPI__DETECTOR_UMD_HH */
