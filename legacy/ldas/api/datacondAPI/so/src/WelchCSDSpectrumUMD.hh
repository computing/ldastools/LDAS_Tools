/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef WELCHCSDSPECTRUMUMD_HH
#define WELCHCSDSPECTRUMUMD_HH
// $Id: WelchCSDSpectrumUMD.hh,v 1.9 2006/02/16 16:54:58 emaros Exp $

#include "UDT.hh"
#include "WindowInfo.hh"

namespace ILwd
{
  class LdasElement;
}

class CallChain;

namespace datacondAPI
{
  
  class WelchCSDSpectrumUMD
  {
    
  public:
    
    // manipulators
    
    
    //: name of first channel associated with this cross-spectral density
    //!return: string - channel name
    std::string GetNameOfChannel1() const;

    //: Set the name of the first channel associated with this CSD
    //!param: Channel - name
    void SetNameOfChannel1(const std::string& Channel);
    
    //: name of first channel associated with this cross-spectral density
    //!return: string - channel name
    std::string GetNameOfChannel2() const;

    //: Set the name of the second channel associated with this CSD
    //!param: Channel - name
    void SetNameOfChannel2(const std::string& Channel);
    
    //: Length of FFT used to make this estimate
    //!return: int - length 
    //!todo: This is inappropriate metadata and should be eliminated
    unsigned int GetFFTLength() const;

    //: Length of FFT used to make this estimate
    //!param: length - length of DFT used to make this estimate
    //!todo: This is inappropriate metadata and should be eliminated
    void SetFFTLength(const unsigned int& length);
    
    //: overlap used to generate this psd
    //!return: unsigned int - number of elements overlapped(?)
    //!todo: This is inappropriate metadata and should be eliminated
    unsigned int GetFFTOverlap() const;

    //: set the overlap used to generate this psd
    //!param: overlap - number of elements overlapped(?)
    //!todo: This is inappropriate metadata and should be eliminated
    void SetFFTOverlap(const unsigned int& overlap);
    
    //: The window used on the data from which this psd estimate was generated
    //!return: window - information on window used to generate estimate
    //!todo: This is inappropriate metadata and should be eliminated
    WindowInfo GetWindowInfo() const;

    //: Set info on window used on the data from which this psd 
    //+ estimate was generated
    //!param: window - information on window used to generate estimate
    //!todo: This is inappropriate metadata and should be eliminated
    void SetWindowInfo(const WindowInfo& window);
    
    //: Create ILWD representation of *this
    //!param: Chain - CallChain *this is part of
    //!param: Target - intended destination for ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement* 
    ConvertToIlwd(const CallChain& Chain, 
		  datacondAPI::udt::target_type Target 
		  = datacondAPI::udt::TARGET_GENERIC ) const;
    
  protected:
    
    // constructors         
    
    //: Default constrctor
    WelchCSDSpectrumUMD();

    //: Copy constructor
    //!param: vaule - object to construct copy of
    WelchCSDSpectrumUMD(const WelchCSDSpectrumUMD& value);
    
    //: destructor
    //!todo: why is destructor protected?!
    virtual ~WelchCSDSpectrumUMD();
    
  private:
    
    std::string  m_channel_1;
    std::string  m_channel_2;
    
    unsigned int m_FFTLength;
    unsigned int m_FFTOverlap;
    WindowInfo m_WindowInfo;
    
  }; // class WelchCSDSpectrumUMD
  
  inline std::string WelchCSDSpectrumUMD::
  GetNameOfChannel1() const
  {	
    return m_channel_1;
  }
  
  inline void WelchCSDSpectrumUMD::
  SetNameOfChannel1(const std::string& Channel)
  {
    m_channel_1 = Channel;
  }
  
  inline std::string WelchCSDSpectrumUMD::
  GetNameOfChannel2() const
  {
    return m_channel_2;
  }
  
  inline void WelchCSDSpectrumUMD::
  SetNameOfChannel2(const std::string& Channel)
  {
    m_channel_2 = Channel;
  }
  
  
  
} // namespace datacondAPI

#endif // WELCHCSDSPECTRUMUMD_HH
