/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef MIXER_HH
#define MIXER_HH

// $Id: Mixer.hh,v 1.35 2007/06/18 15:26:42 emaros Exp $

#include <complex>

#include <general/unexpected_exception.hh>
#include <general/unimplemented_error.hh>

#include "UDT.hh"
#include "SequenceUDT.hh"
#include "MixerState.hh"
#include "TimeSeries.hh"

namespace datacondAPI
{

  class Mixer
  {
    
  public:
    
    // constructors
    
    //: Construct from MixerState
    //!param: state - MixerState object to initialize from
    explicit Mixer(const MixerState& state);
    
    //: Copy constructor
    //!param: mixer - Mixer object to copy from
    Mixer(const Mixer& mixer);
    
    //: Destructor
    ~Mixer();
    
    //: assignment operator
    //!param: mixer - Mixer object to assign from
    Mixer& operator=(const Mixer& mixer);
    
    //: Interpreted apply action (metadata interpreted & set)
    //!param: out - points to result of mixer actingh on in. May be null on
    //+ entry, in which case the appropriate output object is allocated 
    //+ on heap
    //!param: in - data on which mixer acts.
    //!exc: std::invalid_argument - if in, out not of appropriate types
    //!exc: std::bad_alloc - if can't allocate out
    //!todo: check exceptions
    void apply(udt*& out, const udt& in);
    
    //: Basic mixer action 
    //!param: out - points to result of mixer acting on in. 
    //!param: in - data on which mixer acts.
    //!exc: std::invalid_argument - if in, out not of appropriate types
    //!exc: std::bad_alloc - if allocation to out is necessary and fails
    //!todo: check exceptions
    template<typename out_type, typename in_type>
    void apply(std::valarray<std::complex<out_type> >& out, 
	       const std::valarray<in_type>& in);
    
    // accessors
    //: get copy of state
    //!return: MixerState - copy of objects state
    MixerState getState() const;

    //: get copy of objects state
    //!param: state - on return holds copy of objects current state
    void getState(MixerState& state) const;

    //: get copy of objects state
    //!param: state - on return points to copy of objects current state
    //!exc: std::invalid_argument - if state not MixerState
    //!exc: std::bad_alloc - if can't allocate MixerState
    void getState(State*& state) const;

    // mutators
    //: set the object state
    //!param: state - the new state
    void setState(const MixerState& state);
    
  private:

    //: Helper class for apply(udt*&, udt&). Attempts to apply filter 
    //+ interpreting data as a TimeSeries<>. Sets metadata as appropriate.
    //!param: in - data on which to apply filter
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!return: bool - true if successful interpreting as TimeSeries<>
    bool asTimeSeries(udt*& out, const udt& in);

    //: Helper class for apply(udt*&, udt&). Attempts to apply filter 
    //+ interpreting data as a Sequence<>. Sets metadata as appropriate.
    //!param: in - data on which to apply filter
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!return: bool - true if successful interpreting as Sequence<>
    bool asSequence(udt*& out, const udt& in);
    
    //: Helper class for apply(udt*&, const udt&). Attempts to apply filter 
    //+ on objects of type T
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated
    //+ on heap.
    //!param: in - data on which to attempt to apply filter
    //!return: bool - true if successful applying filter to in
    template <class Tout, class Tin>
    bool applyAs(udt*& out, const udt& in);
    
    // prohibited default methods
    //: default constructor prohibited (why?)
    //!todo: why is this prohibited?
    Mixer();
    
    // member data
    
    MixerState m_state;
    
  }; // class Mixer
  
} // namespace datacondAPI

#endif // MIXER_HH
