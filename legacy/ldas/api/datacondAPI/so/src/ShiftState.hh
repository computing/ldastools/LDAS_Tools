#ifndef SHIFTSTATE_HH
#define SHIFTSTATE_HH

#include <memory>

#include "general/Memory.hh"
#include <general/gpstime.hh>

#include <filters/Interpolate.hh>

#include "StateUDT.hh"

namespace datacondAPI {

    template<class T> class Sequence;
    template<class T> class TimeSeries;

    template<class T>
    class ShiftState : public State {
    public:
        //: Constructor
        //
        //!param: shift - number of samples to shift by. Shift must be
        //+non-negative but may be fractional.
        //
        //!param: order - order of Lagrange polynomial to use if interpolation
        //+is needed
        //
        //!exc: std::invalid_argument - Thrown if shift < 0
        ShiftState(const double shift, const size_t order);

        //: Copy constructor, needed because of pointer member
        ShiftState(const ShiftState& rhs);

        //: Assignment operator, needed because of pointer member
        const ShiftState& operator=(const ShiftState& rhs);

        //: Return the shift
        double getShift() const;

        //: Return the interpolation parameter
        double getAlpha() const;

        //: Return the order
        size_t getOrder() const;

        //: Virtual constructor
        virtual ShiftState* Clone() const;

        //: Apply to a Sequence in-place
        //
        //!param: x - data to shift
        //
        //!exc: std::invalid_argument - Thrown if x.size() == 0
        void apply(Sequence<T>& x);

        //: Apply to a TimeSeries in-place
        //
        //!param: x - data to shift
        //
        //!exc: std::invalid_argument - Thrown if x.size() == 0
        void apply(TimeSeries<T>& x);

        //: Convert this to an LdasElement
        //
        //!exc: unimplemented_error - thrown if called
        virtual ILwd::LdasElement*
        ConvertToIlwd(const CallChain& Chain,
                      target_type Target = TARGET_GENERIC) const;

    private:
        //: Whole-number part of shift
        size_t m_shift_whole;
        
        //: Order
        size_t m_order;
        
        //: State info relevant to TimeSeries shifting
        General::GPSTime m_expected_time;
        double m_expected_sample_rate;

        //: Flag to remember if apply() has been called yet
        bool m_apply_called;
        
        //: Placeholder for interpolator if needed
        std::unique_ptr<Filters::Interpolate<T> > m_interp;
    };

    template<class T>
    inline
    double ShiftState<T>::getAlpha() const
    {
        return ( (m_interp.get() == 0) ? 0 : m_interp->getAlpha() );
    }

    template<class T>
    inline
    double ShiftState<T>::getShift() const
    {
        return (m_shift_whole + getAlpha());
    }

    template<class T>
    inline
    size_t ShiftState<T>::getOrder() const
    {
        return m_order;
    }

}

#endif // SHIFTSTATE_HH
