/* -*- mode: c++ -*- */
#ifndef FFTFUNCTION_HH
#define	FFTFUNCTION_HH

#include "CallChain.hh"

//-----------------------------------------------------------------------
//: Function support for FFTs
//
// This class implements the FFT function to be used in the action
// section of a TCL command. The syntax as it would appear in the
// action sequence is: result_valarray = fft(in_valarray)
//
class FFTFunction: CallChain::Function
{
public:
  //---------------------------------------------------------------------
  //: Constructor
  //
  // Construct a new FFT function
  FFTFunction();

  //---------------------------------------------------------------------
  //: Evaluate the FFT Function
  //
  // This performs the FFT transformation. The list of parameters
  // must conatain a single value which is the name of the symbol to
  // use as the input array to the FFT function.
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& Params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual
  void Eval(      CallChain* Chain,
	    const CallChain::Params& Params,
	    const std::string& Ret) const;

  //---------------------------------------------------------------------
  //: Return the Name of the function
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //!return: of the function
  virtual
  const std::string& GetName() const;

};

//-----------------------------------------------------------------------
//: Function support for Inverse FFTs
//
// This class implements the Inverse FFT function to be used in the
// action section of a TCL command. The syntax as it would appear in the
// action sequence is: result_array = ifft(input_array)
//
class IFFTFunction: CallChain::Function
{
public:
  //---------------------------------------------------------------------
  //: Constructor
  //
  // Construct a new Inverse FFT function
  IFFTFunction();

  //---------------------------------------------------------------------
  //: Evaluate the Inverse FFT Function
  //
  // This performs the Inverse FFT transformation. The list of parameters
  // must conatain a single value which is the name of the symbol to
  // use as the input array to the IFFT function.
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& Params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual
  void Eval(      CallChain* Chain,
	    const CallChain::Params& Params,
	    const std::string& Ret) const;

  //---------------------------------------------------------------------
  //: Return the Name of the function
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //!return: of the function
  //
  virtual
  const std::string& GetName() const;

};

#endif	/* FFTFUNCTION_HH */
