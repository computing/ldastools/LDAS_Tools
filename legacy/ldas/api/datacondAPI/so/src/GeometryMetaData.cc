#include "datacondAPI/config.h"

#include <cassert>
#include <cmath>
#ifndef M_PI
extern "C"
{
#include "math.h"
}
#endif /* M_PI */

#include <memory>

#include "general/Memory.hh"
#include "general/toss.hh"
#include "general/RefPtr.hh"

#include "ilwd/ilwdtoc.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrDetector.hh"

#include "GeometryMetaData.hh"
#include "ILwdMetaDataHelper.hh"

using namespace datacondAPI;

// GeometryMetaData implementation details
//=======================================================================

void GeometryMetaData::
AddDetector( const ILwd::TOC& ILwdHelper )
{
  //---------------------------------------------------------------------
  // Make sure we cleanup if errors are thrown
  //---------------------------------------------------------------------

  std::unique_ptr<detector_info>	detector( new detector_info );

  //---------------------------------------------------------------------
  // Common characteristics
  //---------------------------------------------------------------------

  detector->SetName( ILwdHelper.GetContainer().getName(0) );
  detector->SetElevation( ILwdHelper.GetData<REAL_4>
			  ( ILwdFCS::FrDetector::ELEVATION ) );
  try
  {
    //-------------------------------------------------------------------
    // New style geometry information
    //-------------------------------------------------------------------

    detector->SetLongitude( ILwdHelper.GetData<REAL_8>
			    ( ILwdFCS::FrDetector::LONGITUDE ) );
    detector->SetLatitude( ILwdHelper.GetData<REAL_8>
			   ( ILwdFCS::FrDetector::LATITUDE ) );
    detector->SetArmXAzimuth( ILwdHelper.GetData<REAL_4>
			      ( ILwdFCS::FrDetector::ARM_X_AZIMUTH ) );
    detector->SetArmYAzimuth( ILwdHelper.GetData<REAL_4>
			      ( ILwdFCS::FrDetector::ARM_Y_AZIMUTH ) );
    detector->SetArmXAltitude( ILwdHelper.GetData<REAL_4>
			       ( ILwdFCS::FrDetector::ARM_X_ALTITUDE ) );
    detector->SetArmYAltitude( ILwdHelper.GetData<REAL_4>
			       ( ILwdFCS::FrDetector::ARM_Y_ALTITUDE ) );
    detector->SetArmXMidPoint( ILwdHelper.GetData<REAL_4>
			       ( ILwdFCS::FrDetector::ARM_X_MIDPOINT ) );
    detector->SetArmYMidPoint( ILwdHelper.GetData<REAL_4>
			       ( ILwdFCS::FrDetector::ARM_Y_MIDPOINT ) );
    detector->SetLocalTime( ILwdHelper.GetData<INT_4S>
			    ( ILwdFCS::FrDetector::LOCAL_TIME ) );
#if 0
    try
    {
      const ILwd::LdasArray<CHAR>&
	prefix( ILwdHelper.GetILwd<CHAR>( ILwdFCS::FrDetector::PREFIX ) );

      detector->SetPrefix(  );
    }
    catch( ... )
    {
      detector->SetPrefix( "  " );
    }
#else
    detector->SetPrefix( "  " );
#endif
  }
  catch( ... )
  {
    //-------------------------------------------------------------------
    // Old style geometry information
    //-------------------------------------------------------------------
    const REAL_8 half_pi( M_PI/2 );
    INT_2S longitude_d( ILwdHelper.GetData<INT_2S>( ILwdFCS::FrDetector::LONGITUDE_DEGREES ) );
    INT_2S latitude_d( ILwdHelper.GetData<INT_2S>( ILwdFCS::FrDetector::LONGITUDE_DEGREES ) );

    if ( ( latitude_d == -119 ) && ( longitude_d == 46 ) )
    {
      // Hanford
      detector->SetLongitude( -119.40765714 );
      detector->SetLatitude( 46.4551467 );
    }
    else if ( ( latitude_d == -90 ) && ( longitude_d == 30 ) )
    {
      // Livingston
      detector->SetLongitude( -90.77424039 );
      detector->SetLatitude( 30.56289433 );
    }
    else
    {
      detector->SetLongitude( longitude_d,
			      ILwdHelper.GetData<INT_2S>
			      ( ILwdFCS::FrDetector::LONGITUDE_MINUTES ),
			      ILwdHelper.GetData<REAL_4>
			      ( ILwdFCS::FrDetector::LONGITUDE_SECONDS ) );
      detector->SetLatitude( latitude_d,
			     ILwdHelper.GetData<INT_2S>
			     ( ILwdFCS::FrDetector::LATITUDE_MINUTES ),
			     ILwdHelper.GetData<REAL_4>
			     ( ILwdFCS::FrDetector::LATITUDE_SECONDS ) );
    }
    detector->SetArmXAzimuth( half_pi -
			      ILwdHelper.GetData<REAL_4>
			      ( ILwdFCS::FrDetector::ARM_X_AZIMUTH ) );
    detector->SetArmYAzimuth( half_pi -
			      ILwdHelper.GetData<REAL_4>
			    ( ILwdFCS::FrDetector::ARM_Y_AZIMUTH ) );
    detector->SetArmXAltitude( ILwdHelper.GetData<REAL_4>
			       ( ILwdFCS::FrDetector::ARM_X_ALTITUDE, 0.0 ) );
    detector->SetArmYAltitude( ILwdHelper.GetData<REAL_4>
			       ( ILwdFCS::FrDetector::ARM_Y_ALTITUDE, 0.0 ) );
    detector->SetLocalTime( ILwdHelper.GetData<INT_4S>
			    ( ILwdFCS::FrDetector::LOCAL_TIME, 0 ) );
  }

  //---------------------------------------------------------------------
  // Put detector into map of detectors
  //---------------------------------------------------------------------

  try
  {
    std::string		prefix( ILwdFCS::FrDetector::
				MapDetectorToPrefix( detector->GetName() ) );
    detector_info_type	detector_ref( detector.release() );
    
    m_detectors[ prefix ] = detector_ref;
  }
  catch( ... )
  {
    bool duplicate( false );
    for ( unmapped_detector_type::const_iterator d =
	    m_unmapped_detectors.begin();
	  d != m_unmapped_detectors.end();
	  d++ )
    {
      if ( *(*d) == *detector )
      {
	duplicate = true;
	break;
      }
    }

    if ( ! duplicate )
    {
      detector_info_type	detector_ref( detector.release() );
      m_unmapped_detectors.push_back( detector_ref );
    }
  }
}

bool GeometryMetaData::
AddDetector( const GeometryMetaData& Source,
	     const std::string& Prefix )
{
  detector_map_type::const_iterator retval( Source.m_detectors.find( Prefix ) );

  if ( retval != Source.m_detectors.end() )
  {
    m_detectors[ Prefix ] = (*retval).second;
    return true;
  }
  return false;
}

void GeometryMetaData::
AddUnmappedDetectors( const GeometryMetaData& Source )
{
  for ( unmapped_detector_type::const_iterator sud =
	  Source.m_unmapped_detectors.begin();
	sud != Source.m_unmapped_detectors.end();
	sud++ )
  {
    unmapped_detector_type::const_iterator ud =
      m_unmapped_detectors.begin();
    while ( ud != m_unmapped_detectors.end() )
    {
      if ( *(*ud) == *(*sud) )
      {
	break;
      }
      ud++;
    }
    if ( ud == m_unmapped_detectors.end() )
    {
      m_unmapped_detectors.push_back( *sud );
    }
  }
}

void GeometryMetaData::
Store( ILwd::LdasContainer& Storage ) const
{
  for( detector_map_type::const_iterator d = m_detectors.begin();
       d != m_detectors.end();
       d++ )
  {
    (*d).second->Store( Storage, true );
  }
  for( unmapped_detector_type::const_iterator d = m_unmapped_detectors.begin();
       d != m_unmapped_detectors.end();
       d++ )
  {
    (*d)->Store( Storage, false );
  }
}

void GeometryMetaData::
Store( ILwdFCS::FrameH& Storage ) const
{
  for( detector_map_type::const_iterator d = m_detectors.begin();
       d != m_detectors.end();
       d++ )
  {
    (*d).second->Store( Storage, true );
  }
  for( unmapped_detector_type::const_iterator d = m_unmapped_detectors.begin();
       d != m_unmapped_detectors.end();
       d++ )
  {
    (*d)->Store( Storage, false );
  }
}

GeometryMetaData& GeometryMetaData::
operator=( const GeometryMetaData& Source )
{
  if ( &Source != this )
  {
    m_detectors = Source.m_detectors;
    m_unmapped_detectors = Source.m_unmapped_detectors;
  }
  return *this;
}

bool GeometryMetaData::
operator==( const GeometryMetaData& Source ) const
{
  if ( &Source != this )
  {
    if ( ( Source.m_detectors.size() != m_detectors.size() ) ||
	 ( Source.m_unmapped_detectors.size() != m_unmapped_detectors.size() ) )
    {
      return false;
    }
    for ( detector_map_type::const_iterator i = m_detectors.begin(),
	    j = Source.m_detectors.begin();
	  i != m_detectors.end();
	  i++, j++ )
    {
      if ( ( (*i).first != (*j).first ) ||
	   ( (*i).second.get( ) != (*j).second.get( ) ) )
      {
	return false;
      }
    }
    for ( unmapped_detector_type::const_iterator i = m_unmapped_detectors.begin(),
	    j = Source.m_unmapped_detectors.begin();
	  i != m_unmapped_detectors.end();
	  i++, j++ )
    {
      if ( i->get( ) != j->get( ) )
      {
	return false;
      }
    }
  }
  return true;
}


//-----------------------------------------------------------------------
// Private section
//-----------------------------------------------------------------------

GeometryMetaData::detector_info::
detector_info( )
  : m_arm_x_altitude( ILwdFCS::FrDetector::DFLT_ARM_X_ALTITUDE ),
    m_arm_y_altitude( ILwdFCS::FrDetector::DFLT_ARM_Y_ALTITUDE ),
    m_arm_x_midpoint( ILwdFCS::FrDetector::DFLT_ARM_X_MIDPOINT ),
    m_arm_y_midpoint( ILwdFCS::FrDetector::DFLT_ARM_Y_MIDPOINT ),
    m_local_time( 0 )
{
}

void GeometryMetaData::detector_info::
Store( ILwd::LdasContainer& Storage, bool Mapped ) const
{
  // Create ILwdMetaDataHelper
  ILwdMetaDataHelper	c( Storage );
  
  ILwd::LdasContainer*	dc( c.GetILwd< ILwd::LdasContainer >
			    ( ILwdMetaDataHelper::DETECTOR, false ) );

  if ( !dc )
  {
    // Create container for Detector info
    ILwd::LdasContainer& con( const_cast<ILwd::LdasContainer&>
			      (c.GetContainer() ) );
    dc = new ILwd::LdasContainer( ILwdMetaDataHelper::ReverseLookup
				  ( ILwdMetaDataHelper::DETECTOR ) );
    con.push_back( dc,
		   ILwd::LdasContainer::NO_ALLOCATE,
		   ILwd::LdasContainer::OWN );
  }
  ILwd::LdasContainer::const_iterator dc_pos( dc->begin() );
  if ( Mapped )
  {
    // Check for duplicate geometry info
    while( dc_pos != dc->end() )
    {
      if ( (*dc_pos)->getName( 0 ) == m_name )
      {
	break;
      }
      dc_pos++;
    }
  }
  else
  {
    dc_pos = dc->end();
  }
  if ( dc_pos == dc->end() )
  {
    using namespace ILwdFCS;

    ILwdFCS::FrDetector
      d( m_name,
	 m_prefix.c_str( ),
	 m_longitude,
	 m_latitude,
	 m_elevation,
	 m_arm_x_azimuth,
	 m_arm_y_azimuth,
	 m_arm_x_altitude,
	 m_arm_y_altitude,
	 m_arm_x_midpoint,
	 m_arm_y_midpoint,
	 m_local_time );

#if WORKING
    s->appendName( "detectProc" );
#endif

    dc->push_back( d.Release(),
		   ILwd::LdasContainer::NO_ALLOCATE,
		   ILwd::LdasContainer::OWN );
  }
#if WORKING // :TODO:
  else
  {
    // Ensure data is consistant
    if ( *this != *(*dc_pos) )
    {
      General::toss<std::runtime_error>
	( "GeometryMetaData::detector_info::Store",
	  __FILE__,
	  __LINE__,
	  "Detector information differs" );
    }
  }
#endif
}

void GeometryMetaData::detector_info::
Store( ILwdFCS::FrameH& Storage, bool Mapped ) const
{
  //:TODO: Check for duplicate geometry info
  ILwdFCS::FrDetector*	detector( Storage.GetDetector( m_name ) );

  if ( detector && Mapped )
  {
    //:TODO: Ensure data is consistant
  }
  else
  {
    ILwdFCS::FrDetector d;

    d.SetPrefix( m_prefix.c_str() );
    d.SetName( m_name );
    d.SetLongitude( m_longitude );	// Version 5
    d.SetLatitude( m_latitude );	// Version 5
    d.SetArmXazimuth( m_arm_x_azimuth );
    d.SetArmYazimuth( m_arm_y_azimuth );
    // Attributes introduced with Version 5 Frame Spec
    d.SetArmXaltitude( m_arm_x_altitude );
    d.SetArmYaltitude( m_arm_y_altitude );
    d.SetArmXmidpoint( m_arm_x_midpoint );
    d.SetArmYmidpoint( m_arm_y_midpoint );
    d.SetLocalTime( m_local_time );

    Storage.AppendDetector( d, !Mapped );
  }
}

GeometryMetaData::detector_info& GeometryMetaData::detector_info::
operator=( const GeometryMetaData::detector_info& Source )
{
  if ( &Source != this )
  {
    m_name = Source.m_name;
    m_longitude = Source.m_longitude;
    m_latitude = Source.m_latitude;
    m_elevation = Source.m_elevation;
    m_arm_x_azimuth = Source.m_arm_x_azimuth;
    m_arm_y_azimuth = Source.m_arm_y_azimuth;
    m_arm_x_altitude = Source.m_arm_x_altitude;
    m_arm_y_altitude = Source.m_arm_y_altitude;
    m_local_time = Source.m_local_time;
  }
  return *this;
}

INT_4S GeometryMetaData::detector_info::
get_degrees( REAL_8 Pos )
{
  return (INT_4S)Pos;
}

INT_4S GeometryMetaData::detector_info::
get_minutes( REAL_8 Pos )
{
  Pos = std::abs( Pos );

  return (INT_4S)( ( Pos - (INT_4S)Pos ) * 60 );
}

REAL_4 GeometryMetaData::detector_info::
get_seconds( REAL_8 Pos )
{
  Pos = std::abs( Pos );

  return (INT_4S)( ( (Pos  * 60 ) - ( (INT_4S)Pos * 60 ) ) * 60 );
}


bool GeometryMetaData::detector_info::
operator==( const detector_info& Comparee ) const
{
  return ( ( m_name == Comparee.m_name ) &&
	   ( m_longitude == Comparee.m_longitude ) &&
	   ( m_latitude == Comparee.m_latitude ) &&
	   ( m_elevation == Comparee.m_elevation ) &&
	   ( m_arm_x_azimuth == Comparee.m_arm_x_azimuth ) &&
	   ( m_arm_y_azimuth == Comparee.m_arm_y_azimuth ) &&
	   ( m_arm_x_altitude == Comparee.m_arm_x_altitude ) &&
	   ( m_arm_y_altitude == Comparee.m_arm_y_altitude ) &&
	   ( m_local_time == Comparee.m_local_time ) );
}

bool GeometryMetaData::detector_info::
operator==( const ILwd::TOC& Comparee ) const
try
{
  assert( 0 );
  if ( ( Comparee.GetData<REAL_8>( ILwdFCS::FrDetector::LONGITUDE )
	 == m_longitude ) &&
       ( Comparee.GetData<REAL_8>( ILwdFCS::FrDetector::LATITUDE )
	 == m_latitude ) )
  {
    return true;
  }
  return false;
}
catch( ... )
{
  return false;
}

//=======================================================================
// Instantiate needed template code
//=======================================================================

template class General::RefPtr<GeometryMetaData::detector_info>;
