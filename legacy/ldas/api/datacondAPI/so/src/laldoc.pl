#!/usr/bin/env perl
#------------------------------------------------------------------------
# Provides support for LaL Doc comment styles.
#------------------------------------------------------------------------

-d laldoc || mkdir laldoc,0700 ;

#========================================================================
# Tags that are currently supported:
#------------------------------------------------------------------------
#
# <lalLaTeX [file=name]></lalLaTeX> - This is used to send out LaTeX
#    command between the tags to the optional file. The lines containing
#    the tags are not copied.
# ex:
# /* <lalLaTeX>  - This text will not show up in the doc
#    \section help
#    </lalLaTeX>  - Neither will this text show up in the doc */
#
# <lalVerbatim [file="name"]></lalVerbatim> - This is used to send the text
#    between the tags to the optional file in LaTeX's verbatim mode.
# ex:
# /* <lalVerbatim> - This text will not show up in the doc */
# This text will be put into the document as is.
# /* </lalVerbatim> - Neither will this text show up in the doc */
#
#------------------------------------------------------------------------
# Attributes:
#------------------------------------------------------------------------
# file - This attribute overrides the default output filename for storing
#        the data. By default the output filename is the concatination of
#        the base name, file extension translated to all caps, and ".tex".
#        The filename specified as the argument will have ".tex" appended.
#
#========================================================================
local($lalstate = 0, $lal_working_file = "", $new_block = "");
open LOG, ">> laldoc/lal_log";
print LOG "Working on input: $oldinput\n";

sub blockOffset
{
    $Line = 0;
    open SOURCE, "< $oldinput";
    while(<SOURCE>)
    {
	$Line++;
	/^\/\/\!ppp\:/ && last;
    }
    close SOURCE;
}

sub lalAttributes
{
    my ($args) = @_;
    $args =~ s/^\s*(.*)\s*$/$1/;	# Strip leading and trailing spaces.
    foreach (split /\s*(\w+=\"[^\"]*\")\s+/, $args)
    {
	print LOG "lalAttributes: Working on '$_'\n";
	# File Attribute
	/^file=\"([^\"]*)\"\s*/ && do { $lal_working_file = $1; next; };
	# Name Attribute
	/^name=\"([^\"]*)\"\s*/ && do { $lal_name = $1; next; };
	# Unknown Attribute
	print LOG "lalAttributes: No match found\n";
	
    }
}

sub open_lal_file
{
    my ($file) = @_;
    ($file .= ".tex") && (! $file =~ /\.tex$/);
    print LOG "open_lal_file: $file\n";
    open LAL_FILE, ">> laldoc/$file";
}

sub lalLaTeX
{
    $lalstate = 1;
    &lalAttributes(@_);
    if ($lal_working_file eq "")
    {
	$lal_working_file = $oldinput;
        $lal_working_file =~ s/^.*\///g;
	if ($lal_working_file =~ /^(.*)\.([^\.]+)$/)
	{
	    my $ext = $2;
	    $ext =~ tr/[a-z]/[A-Z]/;
	    $lal_working_file = $1 . $ext;
	}
    }
    open_lal_file $lal_working_file;
}

sub lalRef
{
    $lal_name = "";
    &lalAttributes(@_);
    &addRef($lal_name);
}

sub addRef
{
    local ($foo) = @_;
    $refName[$#refName + 1] = $foo;
    $refLine[$#refLine + 1] = $Line;
    print LOG "Created ref: $foo $Line\n";
}

sub writeRef
{
    local($cmd_file, $filename);
    if ($#refName > 0)
    {
	$filename = $oldinput;
	$filename =~ s/^.*\///g;
	$cmd_file = $filename;
	if ($cmd_file =~ /^(.*)\.([^\.]+)$/)
	{
	    my $ext = $2;
	    $ext =~ tr/[a-z]/[A-Z]/;
	    $cmd_file = $1 . $ext . ".cmd";
	}
	print LOG "open cmd_file: $cmd_file\n";
	open CMD_FILE, ">> laldoc/$cmd_file";
	for ($i = 1; $i <= $#refName; $i++)
	{
	    print CMD_FILE "\\newcommand{$refName[$i]} \"$filename\" \"$refLine[$i]\"\n";
	}
	close CMD_FILE;
    }
}

sub lalVerbatim
{
    $lalstate = 2;
    &lalAttributes(@_);
    if ($lal_working_file eq "")
    {
	$lal_working_file = $oldinput;
        $lal_working_file =~ s/^.*\///g;
    }
    open_lal_file $lal_working_file;
    print LAL_FILE "\\begin{verbatim}\n"
}

sub endLaL
{
    if ($lalstate == 2)
    {
	print LAL_FILE "\\end{verbatim}\n"
    }
    close LAL_FILE;
    $lal_working_file = "";
    $lalstate = 0;
}

$#refLine = $#refName = 0;
&blockOffset();
foreach (split("\n", $block))
{
    $Line++;
    /^(.*)<lalLaTeX(.*)>(.*)$/ && do { &lalLaTeX($2);
				       $new_block .= $1 . $3 . "\n";
				       next; } ;
    /^(.*)<\/lalLaTeX(.*)>(.*)$/ && do { &endLaL();
				     $new_block .= $1 . $3 . "\n";
				     next; } ;
    /^(.*)<lalVerbatim(.*)>(.*)$/ && do { &lalVerbatim($2);
					  $new_block .= $1 . $3 . "\n";
					  next; } ;
    /^(.*)<\/lalVerbatim(.*)>(.*)$/ && do { &endLaL();
					$new_block .= $1 . $3 . "\n";
					next; } ;
    /^(.*)<lalRef(.*)>(.*)$/ && do { &lalRef($2);
			     $new_block .= $1 . $3 . "\n";
			     next; } ;
    /^\/\/\!ref\: (.*)$/ && do { &addRef($1);
				 $new_block .= "";
				 next; } ;
    ($lalstate == 0) && do { $new_block .= $_ . "\n"; next; };
    ($lalstate != 1) && do { $new_block .= $_ . "\n"; };
    print LAL_FILE "$_\n";
}
$block = $new_block;
&writeRef;
print LOG "---BLOCK--\n$block";
close LAL_FILE;
close LOG;
