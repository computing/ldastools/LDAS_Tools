/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "config.h"

#include <cstdio>
#include <cstdlib>
#include <ios>
#include <stdexcept>
#include <sstream>
#include <time.h>

#include "CallChain.hh"
#include "CallChainPrivate.hh"
#include "DatabaseUDT.hh"
#include "Ingester.hh"

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"

#include "general/Memory.hh"
#include "general/unexpected_exception.hh"

#include "genericAPI/swiglogging.h"
#include "genericAPI/registry.hh"

#include "ldas/ldasconfig.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasstring.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "dbaccess/dbaccess.h"
#include "dbaccess/Transaction.hh"
#include "dbaccess/Table.hh"
#include "dbaccess/Process.hh"
#include "dbaccess/ProcessParams.hh"

#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

//!ignore_begin:
// Do not generate any documentation for these debugging macros
#if 0
#define AT() std::cerr << "DEBUG: AT: " << (void*)this << " " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#define AT_STATIC() std::cerr << "DEBUG: AT: " << " " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#define AT_STATIC()
#endif

namespace
{
  static std::string	IGNORE("__ignore__");
  std::ostream& error( std::ostream& Stream, int LineNumber )
  {
    Stream << "Error: ";
    if ( LineNumber != -1 )
    {
      Stream << "Line: " << LineNumber << ": ";
    }
    return Stream;
  }
} // namespace - anonymous
//!ignore_end:

static void set_metadata_final_result( const std::list<CallChain::intermediate_result_type>& Results );

//-----------------------------------------------------------------------
// Construct and initialize the Exception class
//-----------------------------------------------------------------------
CallChain::Exception::
Exception(void)
{
}

//-----------------------------------------------------------------------
// Construct and initialize the Exception class
//!param: const std::string& Message - Text of error message
//-----------------------------------------------------------------------
CallChain::Exception::
Exception( const std::string& Message )
  : m_msg( Message )
{
}

//-----------------------------------------------------------------------
// Retrieve the message describing the exception that occurred.
//!return: const std::string& - Text of the exception
//-----------------------------------------------------------------------
const std::string& CallChain::Exception::
GetMessage(void) const
{
  return m_msg;
}

//------------------------------------
// Construct and initialize the UndocumentedError class
//!param: std::string FileName - Name of the file where the exception
//+       occurred. It is supplied in the C preprocessor symbol __FILE__.
//!param: int LineNumber - The line number where the exception occurred.
//+       It is supplied in the C preprocessor symbol __LINE__.
//-----------------------------------------------------------------------
CallChain::UndocumentedError::
UndocumentedError(std::string FileName, int LineNumber)
{
  std::ostringstream oss;

  oss << "Undocumented Error: "
      << FileName
      << " " << LineNumber;
  m_msg = oss.str();
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Symbol - Name of requested symbol that caused the
//+       exception to be thrown.
//-----------------------------------------------------------------------
CallChain::BadSymbol::
BadSymbol(std::string Symbol)
{
  m_msg = "Bad Symbol: ";
  m_msg.append(Symbol);
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Class - Name of requested class that caused the
//+       exception to be thrown.
//-----------------------------------------------------------------------
CallChain::BadClass::
BadClass(std::string Class)
{
  m_msg = "Bad Class: ";
  m_msg.append(Class);
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: 
CallChain::BadDefaultArgument::
BadDefaultArgument( INT_4U ArgumentOffset )
{
  std::ostringstream	msg;

  msg << "The argument at offset " << ArgumentOffset << " does not support default values";
  m_msg = msg.str( );
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Symbol - Name of requested function that caused the
//+       exception to be thrown.
//-----------------------------------------------------------------------
CallChain::BadFunction::
BadFunction(std::string Function)
{
  m_msg = "Bad Function: ";
  m_msg.append(Function);
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Class - Name of the class
//!param: std::string Method - Name of method in the class
//-----------------------------------------------------------------------
CallChain::BadMethod::
BadMethod(std::string Class, std::string Method)
{
  m_msg = "Bad Method: ";
  m_msg.append(Class);
  m_msg.append("::");
  m_msg.append(Method);
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Class - Name of the class
//!param: std::string Method - Name of method in the class
//!param: std::string Arg - Name of the argument
//!param: std::string Received - Type of argument received
//!param: std::string Expected - Type of argument expected
//-----------------------------------------------------------------------
CallChain::BadArgument::
BadArgument(std::string Class, std::string Method, std::string Arg,
	    std::string Received, std::string Expected)
{
  m_msg = "Illegal Argument: ";
  m_msg.append(Class);
  m_msg.append("::");
  m_msg.append(Method);
  m_msg.append("() argument #");
  m_msg.append(Arg);
  m_msg.append(" Received Type: ");
  m_msg.append(Received);
  if (Expected != "")
  {
    m_msg.append(" Expected Type: ");
    m_msg.append(Expected);
  }
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Function - Name of the function
//!param: std::string Arg - Name of the argument
//!param: std::string Received - Type of argument received
//!param: std::string Expected - Type of argument expected
//-----------------------------------------------------------------------
CallChain::BadArgument::
BadArgument(std::string Function, unsigned int Arg,
	    std::string Received, std::string Expected)
{
  std::ostringstream oss;
  oss << "Illegal Argument: "
      << "action " << Function << "();"
      << " argument #" << Arg << "."
      << " Received: " << Received;
  if (Expected != "")
  {
    oss << " Expected: " << Expected;
  }
  m_msg = oss.str();
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Function - Name of function called
//!param: unsigned int Expected - The number of arguments expected
//!param: unsigned int Received - The actual number of arguments passed
//-----------------------------------------------------------------------
CallChain::BadArgumentCount::
BadArgumentCount(std::string Function,
		 unsigned int Expected, unsigned int Received)
{
  std::ostringstream oss;
  oss << "Wrong number of arguments passed to "
      << Function << "()."
      << " Expected: " << Expected
      << " Received: " << Received;
  m_msg = oss.str();
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: const std::string& Function - Name of function called
//!param: const std::string& Expected - Descriptive text of the number
//+	of arguments expected.
//!param: unsigned int Received - The actual number of arguments passed
//-----------------------------------------------------------------------
CallChain::BadArgumentCount::
BadArgumentCount( const std::string& Function,
		  const std::string& Expected,
		  unsigned int Received )
{
  std::ostringstream oss;
  oss << "Wrong number of arguments passed to "
      << Function << "()."
      << " Expected: " << Expected
      << " Received: " << Received
      << std::ends;
  m_msg = oss.str();
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: const std::string& Function - Name of function called
//!param: const std::string& SystemCall - Name of system fucntion called
//!param: const std::string& File - The filename containing source code that
//+	 generated the exception.
//!param: const int Line - The line of source code that generated
//+	the exception.
//-----------------------------------------------------------------------
CallChain::BadSystemCall::
BadSystemCall( const std::string& Function,
	       const std::string& SystemCall,
	       const std::string& File,
	       const int Line )
{
  std::ostringstream oss;
  oss << "Failed system call ("
      << SystemCall << ") in function "
      << Function << "()"
      << " [File: " << File << " Line: " << Line << "]."
      << std::ends;
  m_msg = oss.str();
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//-----------------------------------------------------------------------

CallChain::LogicError::
LogicError( )
{
  m_msg = "Logic Error";
}

CallChain::LogicError::
LogicError( std::string Message )
{
  m_msg = Message;
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//-----------------------------------------------------------------------
CallChain::NullFieldInDatabaseTable::
NullFieldInDatabaseTable( )
{
  m_msg = "A field was discovered to be null in a query result set";
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: std::string Function - Name of function called
//-----------------------------------------------------------------------
CallChain::NullResult::
NullResult( std::string Function )
{
  m_msg = "No result was produced by function: ";
  m_msg += Function;
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//-----------------------------------------------------------------------

CallChain::TableHasNonSimpleTypes::
TableHasNonSimpleTypes( )
{
  m_msg = "The result table contains columns that are of non-simple types.";
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//-----------------------------------------------------------------------

CallChain::BadIngestion::
BadIngestion( const std::string& Message )
{
  m_msg = Message;
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//-----------------------------------------------------------------------

CallChain::WrongNumberOfRows::
WrongNumberOfRows( CallChain::db_ingestion_type RowRule )
{
  switch( RowRule )
  {
  case CallChain::EXACTLY_ONE:
    m_msg = "There is not exactly one row resulting from spectrum query" ;
    break;
  case CallChain::MORE_THAN_ZERO:
    m_msg = "There is zero rows resulting from spectrum query";
    break;
  case CallChain::ZERO_OR_MORE:
    // Place holder to prevent compiler warnings.
    m_msg = "Bad call to exception: WrongNumberOfRows";
    break;
  }
}

//-----------------------------------------------------------------------
// Construction and initialization of the class.
//!param: const std::string& Name - Name of the function
//-----------------------------------------------------------------------
CallChain::Function::
Function(const std::string& Name)
{
  m_id = CallChain::AddFunction(Name, this);
}

//-----------------------------------------------------------------------
// Release resources back to the system
//-----------------------------------------------------------------------
CallChain::Function::
~Function(void)
{
}


//-----------------------------------------------------------------------
// The default validation method
//-----------------------------------------------------------------------
void CallChain::Function::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
  //---------------------------------------------------------------------
  // Ensure that all parameters have already been defined
  //---------------------------------------------------------------------
  for ( Params::const_iterator
	  cur = Params.begin(),
	  last = Params.end( );
	cur != last;
	++cur )
  {
    validateParameter( SymbolTable, *cur, PARAM_READ );
  }
}

void  CallChain::Function::
validateParameter(  Step::sudo_symbol_table_type& SymbolTable,
		    const std::string& Parameter,
		    const int Mode ) const
{
  //---------------------------------------------------------------------
  // Check if parameter is a place holder
  //---------------------------------------------------------------------
  if ( Parameter.length( ) <= 0 )
  {
      return;
  }
  //---------------------------------------------------------------------
  // Verify that parameters which need to be read have values in the
  //   symbol table.
  //---------------------------------------------------------------------
  if ( Mode & PARAM_READ )
  {
    {
      //-----------------------------------------------------------------
      // Check for number syntax
      //-----------------------------------------------------------------
      std::istringstream	number( Parameter );
      double			n;

      number >> n;

      size_t	s = number.tellg( );
      if ( s == (size_t)( Parameter.length( ) ) )
      {
	//---------------------------------------------------------------
	// It is a number so continue
	//---------------------------------------------------------------
	return;
      }
    }
    if ( SymbolTable.find( Parameter ) == SymbolTable.end( ) )
    {
      std::ostringstream	msg;

      msg << "no such symbol: '" << Parameter << "'";
      throw std::range_error( msg.str( ) );
    }
  }
  //---------------------------------------------------------------------
  // Add write symbol to the symbol table
  //---------------------------------------------------------------------
  if ( Mode & PARAM_WRITE )
  {
    SymbolTable[ Parameter ] = Parameter;
  }
}

//-----------------------------------------------------------------------

std::string	CallChain::VariableNullRef;

CallChain::
CallChain()
  : m_private( new CallChainPrivate() ),
    m_ensure_primary( false ),
    m_cpu_time(0),
    m_user_time(0),
    m_final_result_target(datacondAPI::udt::TARGET_GENERIC)
{
  SetSingleFrameMode( false );
  s_functions();	// Force initialization of functions
  AT();
}

CallChain::
~CallChain()
{
  AT();
  Reset();
  AT();
  delete m_private;
}

CallChain::Function* CallChain::
AddFunction(const std::string& Name, Function* Function)
{
  AT_STATIC();
  CallChain::Function*	retval = Function;
  std::map<std::string, CallChain::Function*>::const_iterator i;

  if ((i = s_functions().find(Name)) == s_functions().end())
  {
    s_functions()[Name] = Function;
  }
  else 
  {
    retval = (*i).second;
  }
  AT_STATIC();
  return retval;
}

void CallChain::
AddSymbol(const std::string& Name,
	  Symbol* SymbolRef,
	  bool AllowDuplicates )
{
  if ( ( !AllowDuplicates ) &&
       ( m_symbols.find( Name ) != m_symbols.end() ) )
  {
    std::ostringstream oss;
    oss << "Attempted to add a non-unique symbol (" << Name << ")";
    throw CallChain::BadIngestion( oss.str() );
  }
  AT();
  DestroySymbol(Name);
  AT();
  m_symbols[Name] = SymbolRef;
  AT();
}

void CallChain::
AppendStep(Step* Step, const int LineNum )
{
  AT();
  m_steps.push_back(Step);
  m_steps.back( )->LineNumber( LineNum );
  AT();
}

void CallChain::
AppendCallFunction( const std::string& Function,
		    const char** Params,
		    const std::string& Return,
		    const int LineNum )
{
  AT();
  AppendStep( new CallFunction(Function, Params, Return), LineNum );
  AT();
}

void CallChain::
AppendIntermediateResult(const char* SymbolName,
			 const char* Alias,
			 const char* Comment,
			 const char* Target,
			 IntermediateResult::primary_type Primary,
			 const char* CompressionMethod,
			 int CompressionLevel,
			 const int LineNum )
{
  using namespace datacondAPI;

  AT();

  udt::target_type	target( udt::GetTargetType( Target ) );
  if ( ( udt::TARGET_GENERIC == target ) && 
       ( udt::GetTargetType( target ) != Target ) )
  {
    target = m_final_result_target;
  }

  AppendStep(new IntermediateResult(SymbolName, Alias, Comment,
				    target, Primary,
				    CompressionMethod,
				    CompressionLevel), LineNum );
  AT();
}

bool CallChain::
CanBePrimary( const datacondAPI::udt& Object )
{
  using namespace datacondAPI;

#if !defined( old )
  if ( dynamic_cast< const WhenMetaData* >( &Object ) )
  {
    return true;
  }
  return false;
#else /* old */
  return ( udt::IsA< TimeSeries<int> >( Object ) ||
	   udt::IsA< TimeSeries<float> >( Object ) ||
	   udt::IsA< TimeSeries<double> >( Object ) ||
	   udt::IsA< TimeSeries< std::complex<float> > >( Object ) ||
	   udt::IsA< TimeSeries< std::complex<double> > >( Object ) ||
	   udt::IsA< TimeBoundedFreqSequence< std::complex<float> > >
	   ( Object ) ||
	   udt::IsA< TimeBoundedFreqSequence< std::complex<double> > >
	   ( Object ) ||
	   udt::IsA< TimeBoundedFreqSequence<float> >( Object ) ||
	   udt::IsA< TimeBoundedFreqSequence<double> >( Object ) );
#endif /* old */
}

void CallChain::
Dump( std::ostream& Stream ) const
{
  Stream << "Symbol Table:" << std::endl;
  for (std::map<std::string, Symbol*>::const_iterator i = m_symbols.begin();
       i != m_symbols.end();
       i++)
  {
    Stream << "\t" << i->first << "\t" << (void*)(i->second) << std::endl;
  }
}

void CallChain::
DestroySymbol(const std::string& SymbolName)
{
  AT();
  std::map<std::string, Symbol*>::iterator	s = m_symbols.find(SymbolName);
  if (s != m_symbols.end())
  {
    if ((*s).second)
    {
      delete (*s).second;	// Reclaim memory
    }
    m_symbols.erase(s);
  }
  AT();
}

void CallChain::
DryRun( ) const
{
  //---------------------------------------------------------------------
  // Verify the most basic conditions
  //---------------------------------------------------------------------
  if ( ( m_steps.size() > 0 ) &&
       ( ! dynamic_cast<CallChain::IntermediateResult*>( m_steps.back() ) ) )
  {
    throw CallChain::LogicError( "Action sequence does not end with an output statement" );
  }

  //---------------------------------------------------------------------
  // Preload the sudo symbol table
  //---------------------------------------------------------------------
  Step::sudo_symbol_table_type	sudo_symbol_table;

  for (std::map<std::string, Symbol*>::const_iterator
	 cur = m_symbols.begin( ),
	 last = m_symbols.end( );
       cur != last;
       cur++ )
  {
    sudo_symbol_table[ cur->first ] = cur->first;
  }
  //---------------------------------------------------------------------
  // Dry run each step verifying that all symbols needed will be
  //   available.
  //---------------------------------------------------------------------
  for (std::list<Step*>::const_iterator s = m_steps.begin();
       s != m_steps.end();
       s++)
  {
    if (!(*s))
    {
      throw CallChain::LogicError( "Null Step" );
    }
    (*s)->DryRun( sudo_symbol_table );
  }
}

void CallChain::
EnsurePrimary( void )
{
  m_ensure_primary = true;

  bool			have_primary( false );
  IntermediateResult*	first( (IntermediateResult*)NULL );
  IntermediateResult*	step;

  for ( std::list<Step*>::const_iterator s = m_steps.begin();
	s != m_steps.end();
	s++)
  {
    // :TRICKY: Assignment inside of comparison
    if ( (step = dynamic_cast<IntermediateResult*>(*s)) )
    {
      if ( ((IntermediateResult*)NULL == first)
	   && ( step->CanBePrimary() )
	   && ( step->GetTarget() == datacondAPI::udt::TARGET_WRAPPER) )
      {
	first = step;
      }
      if ( step->IsPrimary() )
      {
	if ( have_primary )
	{
	  throw std::logic_error("Too many primaries specified - 1");
	}
	else
	{
	  have_primary = true;
	}
      }
    }
  }
  if ( false == have_primary )
  {
    if ( (IntermediateResult*)NULL != first )
    {
      first->SetAsPrimary();
      have_primary = true;
    }
  }
  if ( have_primary == false )
  {
    throw std::logic_error("no primary specified");
  }
}

bool CallChain::
Execute( bool CleanupSymbolTable )
{
  AT();
  int	lineno = -1;
  try {
    m_command_start_time = General::GPSTime();

    //-------------------------------------------------------------------
    // Do basic validation of the callchain before processing the actual
    //   request.
    //-------------------------------------------------------------------

    DryRun( );

    //-------------------------------------------------------------------
    // No glaring errors, now do the CPU intense call.
    //-------------------------------------------------------------------

    for (std::list<Step*>::const_iterator s = m_steps.begin();
	 s != m_steps.end();
	 s++)
    {
      if (!(*s))
      {
	throw CallChain::LogicError( "Null Step" );
      }
      lineno = (*s)->LineNumber( );
      (*s)->Eval(this);
    }

    AT();
    if ( CleanupSymbolTable )
    {
      AT();
      Reset( false );
    }
  }
  catch( CallChain::Exception& e)
  {
    error( m_exception, lineno )
      << "CallChain Exception: " << e.GetMessage();
    throw CallChain::Exception( m_exception.str( ) );
  }
  catch( std::length_error& e)
  {
    error( m_exception, lineno )
      << "length error: " << e.what();
    throw std::length_error( m_exception.str( ) );
  }
  catch( std::domain_error& e)
  {
    error( m_exception, lineno )
      << "domain error: " << e.what();
    throw std::domain_error( m_exception.str( ) );
  }
  catch( std::out_of_range& e)
  {
    error( m_exception, lineno )
      << "out of range: " << e.what();
    throw std::out_of_range( m_exception.str( ) );
  }
  catch( std::invalid_argument& e)
  {
    error( m_exception, lineno )
      << "invalid argument: " << e.what();
    throw std::invalid_argument( m_exception.str( ) );
  }
  catch( std::logic_error& e)
  {
    error( m_exception, lineno )
      << "logic error: " << e.what();
    throw std::logic_error( m_exception.str( ) );
  }
  catch( std::range_error& e)
  {
    error( m_exception, lineno )
      << "range error: " << e.what();
    throw std::range_error( m_exception.str( ) );
  }
  catch( std::overflow_error& e)
  {
    error( m_exception, lineno )
      << "overflow error: " << e.what();
    throw std::overflow_error( m_exception.str( ) );
  }
  catch( std::underflow_error& e)
  {
    error( m_exception, lineno )
      << "underflow error: " << e.what();
    throw std::underflow_error( m_exception.str( ) );
  }
  catch( std::runtime_error& e)
  {
    error( m_exception, lineno )
      << "runtime error: " << e.what();
    throw std::runtime_error( m_exception.str( ) );
  }
  catch( General::unexpected_exception& e )
  {
    // General::unexpected_exception is derived from bad_exception
    error( m_exception, lineno )
      << "unexpected exception: " << e.what( )
		<< ": " << e.msg( );
    throw;
  }
  catch( std::bad_exception& e )
  {
    error( m_exception, lineno )
      << "bad exception: thrown from CallChain";
    throw;
  }
  catch( std::exception& e)
  {
    error( m_exception, lineno )
      << "standard exception: " << e.what();
    throw std::runtime_error( m_exception.str( ) );
  }
  catch( ... )
  {
    error( m_exception, lineno )
      << "Unknown exception caught: "
      << "Unable to retrieve more information about this exception";
    throw std::runtime_error( m_exception.str( ) );
  }
  AT();
  return true;
}

ILwdFCS::FrameH& CallChain::
GetFrameHeader( )
{
  AT();
  if ( ( m_single_frame ) && ( m_frame_headers.size() == 1 ) )
  {
    AT();
    return *(m_frame_headers[0]);
  }
  AT();
  m_frame_headers.resize( m_frame_headers.size() + 1 );
  AT();
  m_frame_headers.back( ) = new ILwdFCS::FrameH( );
  return *( m_frame_headers.back( ) );
}

ILwdFCS::FrameH& CallChain::
GetFrameHeaderTop( )
{
  if ( m_frame_headers.size() == 0 )
  {
    throw CallChain::LogicError( "Uninitialized FrameHeader stack" );
  }
  return *( m_frame_headers.back() );
}

CallChain::Function* CallChain::
GetFunction(const std::string& Name, bool ThrowError)
{
  CallChain::Function*	result = s_functions()[Name];
  if (!result && ThrowError) throw CallChain::BadFunction(Name);
  return result;
}

const std::string& CallChain::
GetIntermediateComment( const Symbol& Source ) const
{
  for (std::list<intermediate_result_type>::const_iterator
	 i = m_intermediate_results.begin();
       i != m_intermediate_results.end();
       i++)
  {
    if ( (*i).s_object == &Source )
    {
      return (*i).s_comment;
    }
  }
  throw std::range_error( "Symbol not found in list of final results" );
}

const std::string& CallChain::
GetIntermediateName( const Symbol& Source ) const
{
  for (std::list<intermediate_result_type>::const_iterator
	 i = m_intermediate_results.begin();
       i != m_intermediate_results.end();
       i++)
  {
    if ( (*i).s_object == &Source )
    {
      return (*i).s_alias;
    }
  }
  throw std::range_error( "Symbol not found in list of final results" );
}

const std::string& CallChain::
GetIntermediateName( const Symbol& Source, const std::string& Default ) const
{
  for (std::list<intermediate_result_type>::const_iterator
	 i = m_intermediate_results.begin();
       i != m_intermediate_results.end();
       i++)
  {
    if ( (*i).s_object == &Source )
    {
      if ( ( (*i).s_alias.length( ) > 0 ) &&
	   ( (*i).s_alias != "_none" ) )
      {
	return (*i).s_alias;
      }
      else
      {
	return Default;
      }
    }
  }
  throw std::range_error( "Symbol not found in list of final results" );
}

CallChain::Symbol* CallChain::
GetIntermediateByName( const std::string& Source ) const
{
  for (std::list<intermediate_result_type>::const_reverse_iterator
	 i = m_intermediate_results.rbegin();
       i != m_intermediate_results.rend();
       i++)
  {
    if ( Source.compare( (*i).s_alias ) == 0 )
    {
      return (*i).s_object;
    }
  }
  std::ostringstream	msg;
  msg << "Symbol '" << Source
      << "' not found in list of final results";
  throw std::range_error( msg.str( ) );
}

ILwd::LdasContainer* CallChain::
GetResults(void) const
{
  std::unique_ptr< ILwd::LdasContainer >	r( new ILwd::LdasContainer( ) );

  set_metadata_final_result( m_intermediate_results );
  AT();
  for (std::list<intermediate_result_type>::const_iterator
	 i = m_intermediate_results.begin();
       i != m_intermediate_results.end();
       i++)
  {
    AT();
    convert_ir_to_ilwd(*i, *r);
  }
  if (m_exception.str( ).length() > 0)
  {
    AT();
    r->push_back( new ILwd::LdasString( m_exception.str( ),
					"Exception",
					"The LDAS job terminated prematurely due to an exception being thrown. The text of the exception follows:"),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
  }
  switch (m_final_result_target)
  {
  case datacondAPI::udt::TARGET_WRAPPER:
    r->setNameString("ilwdConverter:container");
    break;
  default:
    // Do Nothing. This is here so compiler stays quiet.
    break;
  }
  ILwd::LdasContainer*	result = r.release( );
  Registry::elementRegistry.registerObject( result );
  AT();
  return result;
}

DB::Table* CallChain::
FindOrCreateTable( const std::string& Name )
{
  const std::string		program("datacondAPI");
  
  AT();
  if ( (DB::Table*)NULL == m_transaction.GetTable( DB::Process::TABLE_NAME ) )
  {
    //-------------------------------------------------------------------
    // Establish the process info
    //-------------------------------------------------------------------

    AT();
    struct tm			cvs_date;

    AT();
    cvs_date.tm_isdst = 0;

    // Do not try to process the $ of Date. This will cause cvs actions
    // to replace the Date directive with the time the file was last
    // committed.
    AT();
    sscanf(&(LDAS_CVS_DATE[1]),
	   "Date: %4d/%2d/%2d %2d:%2d:%2d",
	   &cvs_date.tm_year, &cvs_date.tm_mon, &cvs_date.tm_mday,
	   &cvs_date.tm_hour, &cvs_date.tm_min, &cvs_date.tm_sec);

    AT();
    cvs_date.tm_year -= 1900;

  
    AT();
    General::GPSTime cvs_gps_time(mktime(&cvs_date), 0, General::GPSTime::UTC);

    //---------------------------------------------------------------------------
    // Process Table information
    //---------------------------------------------------------------------------

    AT();
    DB::Table* process = DB::Table::CreateTable( DB::Process::TABLE_NAME );
    AT();
    m_transaction.AppendTable(process, true);

    AT();
    process->AppendColumnEntry(DB::Process::PROGRAM, program);
    AT();
    process->AppendColumnEntry<CHAR>(DB::Process::PROGRAM_VERSION,
				     LDAS_CVS_TAG);
    AT();
    process->AppendColumnEntry<CHAR>(DB::Process::REPOSITORY,
				     "ldas/api/datacondAPI/so/src");
    AT();
    process->AppendColumnEntry<INT_4S>(DB::Process::REPOSITORY_ENTRY_TIME,
				       cvs_gps_time.GetSeconds());
    AT();
    process->AppendColumnEntry(DB::Process::COMMENT, "CallChain Transaction");

    AT();
    process->AppendColumnEntry<INT_4S>(DB::Process::IS_ONLINE, DB::IsOnline());
    AT();
    process->AppendColumnEntry(DB::Process::NODE, DB::GetNodeName());
    AT();
    process->AppendColumnEntry(DB::Process::USERNAME, DB::GetOSUser());
    AT();
    process->AppendColumnEntry(DB::Process::OS_PROCESS_ID, DB::GetOSProcessId());
    AT();
    process->AppendColumnEntry(DB::Process::JOB_ID, m_job_id);
    AT();
    process->AppendColumnEntry(DB::Process::DOMAIN_NAME, DB::GetOSDomain() );
    AT();
    process->AppendColumnEntry<INT_4S>(DB::Process::START_TIME,
				       m_command_start_time.GetSeconds());
  }
    
  
  AT();
  if ( (DB::Table*)NULL == m_transaction.GetTable( DB::ProcessParams::TABLE_NAME ) )
  {
    AT();
    DB::Table*	process( m_transaction.GetTable( DB::Process::TABLE_NAME ) );
    AT();
    DB::Table::rowoffset_type	id;
    AT();
    id = process->GetRowCount() - 1;

    //---------------------------------------------------------------------
    // Add the arguments - action that should be taken
    //---------------------------------------------------------------------

    AT();
    DB::Table*	process_params = DB::Table::CreateTable
      (DB::ProcessParams::TABLE_NAME);
    AT();
    m_transaction.AppendTable(process_params, true);
  
    AT();
    DB::Table::rowoffset_type	count(0);
    char				cbuffer[32];

    AT();
    for (std::vector<std::string>::const_iterator a = m_actions.begin();
	 a != m_actions.end();
	 a++)
    {
      AT();
      sprintf(cbuffer, "step-%d", count);
      AT();
      process_params->AppendColumnEntry(DB::ProcessParams::PROGRAM, program);
      AT();
      process_params->AppendColumnEntry<CHAR>(DB::ProcessParams::PARAM, cbuffer);
      AT();
      process_params->AppendColumnEntry<CHAR>(DB::ProcessParams::TYPE, "action");
      AT();
      process_params->AppendColumnEntry(DB::ProcessParams::VALUE, *a);
      AT();
      process_params->Associate(count, *process, id);
      AT();
      count++;
    }
  }
  AT();
  DB::Table*	table( m_transaction.GetTable( Name ) );

  AT();
  if ( (DB::Table*)NULL == table )
  {
    AT();
    table = DB::Table::CreateTable( Name );
    AT();
    m_transaction.AppendTable(table, true);
  }
  AT();
  return table;
}

ILwd::LdasContainer* CallChain::
FormatFrame( datacondAPI::udt::target_type Target,
	     ILwd::LdasContainer* Container,
	     ILwdFCS::FrProcData& Proc )
{
  AT();
  ILwd::LdasContainer*	retval;
  AT();
  ILwdFCS::FrameH&	header( GetFrameHeader() );
  AT();

  header.AppendProcData( Proc );
  AT();
  switch( Target )
  {
  case datacondAPI::udt::TARGET_FRAME:
    AT();
    if ( m_single_frame )
    {
      AT();
      retval = new ILwd::LdasContainer( IGNORE );;
    }
    else
    {
      AT();
      retval = header.ConvertToILwd(m_job_id, "datacondAPI", "LDAS", true );
    }
    break;
  case datacondAPI::udt::TARGET_FRAME_FINAL_RESULT:
    {
      AT();
      retval = header.ConvertToILwd(m_job_id, "datacondAPI", "LDAS", true );
    }
    break;
  default:
    AT();
    throw( std::invalid_argument( "Target type is not TARGET_FRAME or TARGET_FRAME_FINAL_RESULT" ) );
    break;
  }
  return retval;
}

ILwd::LdasContainer* CallChain::
FormatTransaction( datacondAPI::udt::target_type Target,
		   ILwd::LdasContainer* Container ) const
{
  switch( Target )
  {
  case datacondAPI::udt::TARGET_METADATA:
    Container->setNameString( IGNORE );
    break;
  case datacondAPI::udt::TARGET_METADATA_FINAL_RESULT:
    m_transaction.GetILwd( Container );
    break;
  default:
    throw( std::invalid_argument( "Target type is not TARGET_METADATA or TARGET_METADATA_FINAL_RESULT" ) );
    break;
  }
  return Container;
}

CallChain::Symbol* CallChain::
GetSymbol(const std::string& Name, bool ThrowError) const
{
  std::map<std::string, Symbol*>::const_iterator s = m_symbols.find(Name);
  CallChain::Symbol*	retval =
    (s == m_symbols.end())
    ? (CallChain::Symbol*)NULL
    : (*s).second;
  if (!retval && ThrowError) throw(CallChain::BadSymbol(Name));
  return retval;
}

std::string CallChain::
IngestILwd( const ILwd::LdasElement& Element,
	    ingestion_type Ingestion )
{
  m_job_id = Element.getJobId();

  datacondAPI::Ingester	di( *this, Ingestion, false );

  return di.IngestILwd( Element );
}

void CallChain::
Reset( bool Intermediate )
{
  for (std::list<Step*>::const_iterator i = m_steps.begin();
       i != m_steps.end();
       i++)
  {
    if ( (*i) )
    {
      delete *i;
    }
  }
  m_steps.erase(m_steps.begin(), m_steps.end());
  for (std::map<std::string, Symbol*>::const_iterator i = m_symbols.begin();
       i != m_symbols.end();
       i++)
  {
    if ( (*i).second )
    {
      delete (*i).second;
    }
  }
  m_symbols.erase(m_symbols.begin(), m_symbols.end());
  if ( Intermediate )
  {
    for (std::list<intermediate_result_type>::const_iterator
	   i = m_intermediate_results.begin();
	 i != m_intermediate_results.end();
	 i++)
    {
      if ( (*i).s_object )
      {
	delete (*i).s_object;
      }
    }
    m_intermediate_results.erase(m_intermediate_results.begin(),
				 m_intermediate_results.end());
    for ( std::vector<ILwdFCS::FrameH*>::const_iterator
	    i = m_frame_headers.begin();
	  i != m_frame_headers.end();
	  i++ )
    {
      if ( (*i) )
      {
	delete *i;
      }
    }
    m_frame_headers.erase( m_frame_headers.begin(),
			   m_frame_headers.end() );
  }
}

void CallChain::
ResetOrAddSymbol(const std::string& SymbolName, Symbol* SymbolObject )
{
  if (SymbolName.length() <= 0)
  {
    if ( SymbolObject )
    {
      delete SymbolObject;
    }
    return;
  }

  AddSymbol( SymbolName, SymbolObject, true );
}

void CallChain::
SetActionList( const char** Actions )
{
  bool eoa(true);	// End Of Action
  for (int x = 0; Actions[x]; x++)
  {
    if (eoa)
    {
      m_actions.push_back(Actions[x]);
      eoa = false;
    }
    else
    {
      m_actions.back() += Actions[x];
    }
    if (Actions[x][strlen(Actions[x]) -1] == ';')
    {
      eoa = true;
    }
  }
}

void CallChain::
SetReturnTarget(const std::string& Name)
{
  m_final_result_target = datacondAPI::udt::GetTargetType(Name);
}

void CallChain::
StoreIntermediateResult(const Symbol* Object,
			const std::string& Alias,
			const std::string& Comment,
			datacondAPI::udt::target_type Target,
			IntermediateResult::primary_type Primary,
			const std::string& CompressionMethod,
			int CompressionLevel,
			bool DeepCopy)
{
  AT( );
  using namespace datacondAPI;

  if (Object)
  {
    intermediate_result_type	result;

    if (DeepCopy)
    {
      AT( );
      result.s_object = Object->Clone();
    }
    else
    {
      AT( );
      result.s_object = (CallChain::Symbol*)Object;
    }
    AT( );
    result.s_alias = Alias;
    result.s_comment = Comment;
    result.s_target = Target;
    result.s_primary = Primary;
    result.s_compression_method = CompressionMethod;
    result.s_compression_level = CompressionLevel;
    // Put in detector information
    AT( );
    m_private->AddDetectorInfo( *(result.s_object) );
    AT( );
    m_intermediate_results.push_back(result);
    AT( );
  }
  AT( );
}

std::map<std::string, CallChain::Function*>& CallChain::s_functions(void)
{
  AT_STATIC();
  static std::map<std::string, CallChain::Function*>	m_functions;
  AT_STATIC();
  return m_functions;
}

void CallChain::
convert_ir_to_ilwd( const intermediate_result_type& Obj,
		    ILwd::LdasContainer& Container ) const
{
  using namespace datacondAPI;	// Allow the use of shorter names

  const CallChain::Symbol&	o(*(Obj.s_object));

  AT();
  if ( o.ReturnsMultipleILwds( Obj.s_target ) )
  {
    o.ConvertToIlwd( *this, Obj.s_target, &Container );
  }
  else
  {
    ILwd::LdasElement*		r = (ILwd::LdasElement*)NULL;

    // :TRICKY: assignment inside of comparison
    if (!( r = o.ConvertToIlwd(*this, Obj.s_target)))
    {
      AT();
      std::string msg("Unknown conversion: udt type: ");
      msg += datacondAPI::TypeInfoTable.GetName(typeid(o));
      r = new ILwd::LdasString(msg);
    }
    if ( r->getNameString( ) == IGNORE )
    {
      // Do nothing
    }
    else if ( ( Obj.s_target == udt::TARGET_FRAME ) ||
	      ( Obj.s_target == udt::TARGET_FRAME_FINAL_RESULT ) )
    {
      r->setName( 0, "LDAS", true );
      r->setMetadata( "compression_method", Obj.s_compression_method );
      r->setMetadata( "compression_level", Obj.s_compression_level );
    }
    else if ( r->getNameString() == "" )
    {
      AT();
      r->setNameString( Obj.s_alias );
    }
    else if ( ( Obj.s_alias.length( ) > 0 ) &&
	      (Obj.s_alias != "_none" ) )
    {
      AT();
      r->setName( 0, Obj.s_alias, true );
    }
    if (r->getComment() == "")
    {
      AT();
      r->setComment(Obj.s_comment);
    }
    if ( Obj.s_primary == IntermediateResult::PRIMARY )
    {
      AT();
      while( r->getNameSize() < 4 )
      {
	r->appendName( "" );
      }
      r->setName( 3, "primary" );
    }
    Container.push_back( r,
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
  }
  AT();
}

void CallChain::
StoreUDT( std::string Name,
	   datacondAPI::udt* UDT,
	   CallChain::ingestion_type Ingestion,
	   bool AllowSymbolOverwrite,
	   std::string* Symbol,
	   IntermediateResult::primary_type Primary )
{
  AT();
  switch( Ingestion )
  {
  case CallChain::SYMBOL:
    AT();
    if ( Symbol )
    {
      *Symbol += Name;
    }
    AddSymbol( Name, UDT, AllowSymbolOverwrite );
    break;
  case CallChain::INTERMEDIATE:
    AT();
    StoreIntermediateResult( UDT,
			     Name, "Pass Through",
			     m_final_result_target,
			     Primary,
			     datacondAPI::DEFAULT_COMPRESSION_METHOD,
			     datacondAPI::DEFAULT_COMPRESSION_LEVEL,
			     false );
    break;
  }
  AT();
}

void CallChain::
StoreUDT( std::string Name,
	  const std::vector<datacondAPI::udt*>& UDTS,
	  CallChain::CallChain::ingestion_type Ingestion,
	  bool AllowSymbolOverwrite,
	  std::string* Symbol,
	  IntermediateResult::primary_type Primary )
{
  AT();
  if ( UDTS.size() == 1 )
  {
    AT();
    StoreUDT( Name, UDTS[0], Ingestion,
	      AllowSymbolOverwrite,
	      Symbol, Primary );
  }
  else
  {
    AT();
    char	n[512];
    int		offset = 1;
    AT();
    for ( std::vector<datacondAPI::udt*>::const_iterator i = UDTS.begin();
	  i != UDTS.end();
	  i++, offset++ )
    {
      AT();
      if ( ( Symbol ) && ( i != UDTS.begin() ) )
      {
	AT();
	*Symbol += " ";
      }
      AT();
      sprintf( n, "%s_%d", Name.c_str(), offset );
      AT();
      StoreUDT( n, *i, Ingestion, AllowSymbolOverwrite, Symbol, Primary );
    }
    AT();
  }
  AT();
}


void CallChain::
dump_actions( void )
{
  int cnt = 0;
  for (std::vector<std::string>::const_iterator a = m_actions.begin();
       a != m_actions.end();
       a++, cnt++)
  {
    std::cerr << "Step: " << cnt << " " << *a << std::endl;
  }
}

static void
set_metadata_final_result( const std::list<CallChain::intermediate_result_type>& Results )
{
  std::list<CallChain::intermediate_result_type>&	r = const_cast< std::list<CallChain::intermediate_result_type>& >( Results );

  std::list<CallChain::intermediate_result_type>::iterator	last_metadata( r.end() );
  std::list<CallChain::intermediate_result_type>::iterator	last_frame( r.end() );
  for (std::list<CallChain::intermediate_result_type>::iterator
	 i = r.begin();
       i != r.end();
       i++)
  {
    if ( datacondAPI::udt::TARGET_METADATA == (*i).s_target )
    {
      if ( last_metadata != r.end() )
      {
	(*last_metadata).s_target =
	  datacondAPI::udt::TARGET_METADATA;
      }
      last_metadata = i;
      (*last_metadata).s_target =
	datacondAPI::udt::TARGET_METADATA_FINAL_RESULT;
    }
    if ( datacondAPI::udt::TARGET_FRAME == (*i).s_target )
    {
      if ( last_frame != r.end() )
      {
	(*last_frame).s_target =
	  datacondAPI::udt::TARGET_FRAME;
      }
      last_frame = i;
      (*last_frame).s_target =
	datacondAPI::udt::TARGET_FRAME_FINAL_RESULT;
    }
  }
}

//-----------------------------------------------------------------------
//
// Class IntermediateResult definition
//
//-----------------------------------------------------------------------

CallChain::IntermediateResult::
IntermediateResult(const char* Symbol, const char* Alias, const char* Comment,
		   datacondAPI::udt::target_type Target, primary_type Primary,
		   const char* CompressionMethod, int CompressionLevel )
  : m_symbol(Symbol),
    m_alias(Alias),
    m_comment(Comment),
    m_compression_level( CompressionLevel ),
    m_compression_method( CompressionMethod ),
    m_target(Target),
    m_primary(Primary)
{
}

bool CallChain::IntermediateResult::
CanBePrimary( void ) const
{
  return ( m_primary != NEVER_PRIMARY );
}

void CallChain::IntermediateResult::
DryRun( sudo_symbol_table_type& SudoSymbolTable ) const
{
  //:TODO: Implement DryRun
}

void CallChain::IntermediateResult::
Eval(CallChain* Chain)
{
  AT( );
  const Symbol*	obj( (const Symbol*)NULL );

  if (m_symbol.length() > 0)
  {
    obj = Chain->GetSymbol(m_symbol);
    if ( !obj )
    {
      std::string	msg("IntermediateResult::Eval(): No intermediate created due to unknown symbol: ");
      msg += m_symbol;
      msg += " alias: ";
      msg += m_alias;

      throw std::runtime_error(msg);
    }
  }
  else
  {
    throw std::invalid_argument( "IntermediateResult::Eval(): Symbol cannot be empty string" );
  }
  AT( );
  if ( Chain->NeedsPrimary() && IsPrimary() )
  {
    if ( !CallChain::CanBePrimary( *obj ) )
    {
      // This object cannot be a primary since it does not have time info
      //   Try to find another object suitable to be primary.
      m_primary = NEVER_PRIMARY;
      Chain->EnsurePrimary();
      AT( );
    }
  }
  {
    AT( );
    using namespace datacondAPI;

    const Database*	db = dynamic_cast<const Database*>( obj );
    if ( db )
    {
      AT( );
      db->APICheck( m_target );
    }
  }
  AT( );
  Chain->StoreIntermediateResult(obj,
				 m_alias,
				 m_comment,
				 m_target,
				 m_primary,
				 m_compression_method,
				 m_compression_level );
  AT( );
  //---------------------------------------------------------------------
  // It is now history. Make sure that it is never set to primary.
  //---------------------------------------------------------------------
  if ( m_primary == MAYBE_PRIMARY )
  {
    AT( );
    m_primary = NEVER_PRIMARY;
  }
  AT( );
}

//-----------------------------------------------------------------------

datacondAPI::udt::target_type CallChain::IntermediateResult::
GetTarget( void ) const
{
  return m_target;
}

//-----------------------------------------------------------------------

bool CallChain::IntermediateResult::
IsPrimary( void ) const
{
  return ( m_primary == PRIMARY );
}

//-----------------------------------------------------------------------

void CallChain::IntermediateResult::
SetAsPrimary( void )
{
  if ( m_primary != NEVER_PRIMARY )
  {
    m_primary = PRIMARY;
  }
}
