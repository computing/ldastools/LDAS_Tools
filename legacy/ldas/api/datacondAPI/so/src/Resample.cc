/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: Resample.cc,v 1.61 2007/06/18 15:26:42 emaros Exp $

#include "datacondAPI/config.h"

#include <sstream>

#include <filters/LDASConstants.hh>
#include <general/toss.hh>

#include "Resample.hh"
#include "TimeSeries.hh"

using std::fmod;

//--Constructors-----------------------

#if __SUNPRO_CC
#pragma ident "$Id: Resample.cc,v 1.61 2007/06/18 15:26:42 emaros Exp $"
#else
namespace 
{
  const char rcsID[] = "@(#) $Id: Resample.cc,v 1.61 2007/06/18 15:26:42 emaros Exp $:";
}
#endif

namespace datacondAPI {

  Resample::Resample(int p, int q, int n, double beta) 
    : m_state(p, q, n, beta)
  {
  }

  Resample::Resample(const udt& p,
                     const udt& q,
                     const udt& n, 
                     const udt& beta )
    : m_state(p, q, n, beta)
  {
  }

  Resample::Resample(const Resample& rsmpl)
    : m_state(rsmpl.m_state)
  {
  }

  Resample::Resample(int p, int q, const Sequence<double>& b)
    : m_state(p, q, b)
  {
  }

  Resample::Resample(const udt& state)
    : m_state(state)
  {
  }

  Resample::~Resample()
  {
  }

  void Resample::operator() (udt*& out,const udt& in)
  {
    apply(out, in);
  }

  template<class T>
  void Resample::operator()(std::valarray<T>& out, 
                            const std::valarray<T>& in)
  {
    m_state.apply(out, in);
  }

  void Resample::apply(udt*& out, const udt& in)
  {
    bool applied = asTimeSeries(out, in);

    if (!applied)
    {
      applied = asSequence(out, in);
    }

    if (!applied)
    {
      std::string what = "apply unimplemented on type";
      General::toss<General::unimplemented_error>("Resample",
                                     __FILE__,
                                     __LINE__,
                                     what);
    }
  }

  bool Resample::asTimeSeries(udt*& out, const udt& in)
  {
    bool ok = applyAs<TimeSeries<float> >(out, in);

    if (!ok) 
    {
      ok = applyAs<TimeSeries<double> >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<TimeSeries<std::complex<float> > >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<TimeSeries<std::complex<double> > >(out, in);
    }

    if (ok)
    {
      // process TimeSeries metadata
      timeSeriesMetadata(out, in);
    }

    return ok;
  }

  void Resample::timeSeriesMetadata(udt*& out, const udt& in)
  {
    // Storage for the time offset to the first sample, needed
    // by both TimeSeriesMetaData and WhenMetaData.
    // This will be set to delay*oldStepSize*q/p, or equivalently
    // delay*newStepSize, where delay is the delay of the resampler,
    // and StepSize is 1/sampling rate of the original or resampled TimeSeries
    double time_offset = 0.0;

    if (const TimeSeriesMetaData* resolved 
        = dynamic_cast<const TimeSeriesMetaData*>(&in))
    {
      if (TimeSeriesMetaData* pout 
          = dynamic_cast<TimeSeriesMetaData*>(out))
      {
        pout->SetSampleRate(resolved->GetSampleRate() * 
                            m_state.getP() / m_state.getQ());
          
        // Base frequency of resampled TimeSeries is just the
        // same as that of the input TimeSeries
        const double new_fc = resolved->GetBaseFrequency();
        pout->SetBaseFrequency(new_fc);
          
        // Phase of the resampled TimeSeries needs to be adjusted,
        // phi -> phi - 2*pi*fc*delay*newStepSize

        // :NOTE: It's important that the sampling rate be set
        // to the new value before the new phase is calculated

        time_offset =  getDelay() * pout->GetStepSize();

        double new_phase = fmod(resolved->GetPhase()
                                - LDAS_TWOPI*new_fc*time_offset, LDAS_TWOPI);
        if (new_phase < 0.0)
        {
          new_phase += LDAS_TWOPI;
        }

        pout->SetPhase(new_phase);
      }
      else
      {
        std::string what = 
          "apply(udt*& out, const udt& in): "
          "incompatible TimeSeriesMetaData (should be impossible)";
        General::toss<std::invalid_argument>("Resample",
                                    __FILE__,
                                    __LINE__,
                                    what);
      }
    }

    // Setting of WhenMetaData was moved into TimeSeries meta-data function
    // from Sequence meta-data function, since Sequences have no time data
    if (const WhenMetaData* resolved = dynamic_cast<const WhenMetaData*>(&in))
    {
      if (WhenMetaData* pout = dynamic_cast<WhenMetaData*>(out))
      {
        // Uses virtual assignment to set start-time and possibly end-time
        *pout = *resolved;

        // Adjust the start-time for delay

        // :NOTE: It's important that the sampling rate be set
        // to the new value before the new start-time is calculated

        const General::GPSTime newStartTime = pout->GetStartTime() - time_offset;
        pout->SetStartTime(newStartTime);
      }
      else
      {
        std::string what = 
          "apply(udt*& out, const udt& in): "
          "WhenMetaData present in input UDT but not in output";
        General::toss<std::invalid_argument>("Resample",__FILE__,__LINE__,what);
      }
    }

    sequenceMetadata(out, in);
  }

  void Resample::sequenceMetadata(udt*& out, const udt& in)
  {
    if (out != 0)
    {
      std::ostringstream oss;
    
      oss << "resample(" << in.name()
          << "," << m_state.getP()
          << "," << m_state.getQ() << ")";
    
      out->SetName(oss.str());
    }
    else
    {
      std::string what = "apply(udt*& out, const udt& in): "
        "null out pointer";
      General::toss<std::invalid_argument>("Resample", __FILE__, __LINE__, what);
    }

  }

  bool Resample::asSequence(udt*& out, const udt& in)
  {
    bool ok = applyAs<Sequence<float> >(out, in);

    if (!ok) 
    {
      ok = applyAs<Sequence<double> >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<float> > >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<double> > >(out, in);
    }

    if (ok)
    {
      // process Sequence metadata
      sequenceMetadata(out,in);
    }
  
    return ok;
  }

  template <class T>
  bool Resample::applyAs(udt*& out, const udt& in)
  {
    bool ok = false;
    bool null_out = ( out == 0 );

    try 
    {
      if (udt::IsA<T>(in))
      {
        if (null_out)
        {
          out = new T;
        }
        else if (!udt::IsA<T>(*out))
        {
          std::string what = "applyAs<>: type out not compatible with type in";
          General::toss<std::invalid_argument>("Resample", __FILE__, __LINE__, what);
        }
        apply(udt::Cast<T>(*out), udt::Cast<T>(in));
        ok = true;
      }
      else
      {
        ok = false;
      }
    }
    catch(...)
    {
      if(null_out)
      {
        delete out;
        out = 0;
      }
      throw;      
    }

    return ok;
  }

  template<class T>
  void Resample::apply(std::valarray<T>& out, const std::valarray<T>& in)
  {
    m_state.apply(out, in);
  }

  void Resample::getPQ(int& p, int& q)
  {
    p = m_state.getP();
    q = m_state.getQ();
  }

  void Resample::getNBeta(int& n, double& beta)
  {
    n = m_state.getN();
    beta = m_state.getBeta();
  }

  void Resample::getDelay(double& delay)
  {
    //:NOTE:might need to be changed for general resampling
    delay = m_state.getDelay();
  }

  double Resample::getDelay(void)
  {
    //:NOTE:might need to be changed for general resampling
    return m_state.getDelay();
  }

  ResampleState Resample::getState()
  {
    return m_state;
  }

  void Resample::getState(udt*& state) 
  {
    if (state == 0)
    {
      state = m_state.Clone();
    }
    else
    {
      if (udt::IsA<ResampleState>(m_state))
      {
        udt::Cast<ResampleState>(*state) = m_state;
      }
      else
      {
        throw std::invalid_argument("Resample::getState(udt*& state): invalid state type");
      }
    }
  }

  void Resample::getState(ResampleState& state) const
  {
    state = m_state;
  }

  void Resample::setState(const ResampleState& state)
  {
    m_state = state;
  }

  // instantiations

  template void 
  Resample::operator()(std::valarray<float>&,
                       const std::valarray<float>&);

  template void 
  Resample::operator()(std::valarray<double>&,
                       const std::valarray<double>&);

  template void 
  Resample::operator()(std::valarray<std::complex<float> >&,
                       const std::valarray<std::complex<float> >&);

  template void 
  Resample::operator()(std::valarray<std::complex<double> >&,
                       const std::valarray<std::complex<double> >&);

  template void
  Resample::apply(std::valarray<float>&,
                  const std::valarray<float>&);

  template void 
  Resample::apply(std::valarray<double>&,
                  const std::valarray<double>&);

  template void 
  Resample::apply(std::valarray<std::complex<float> >&,
                  const std::valarray<std::complex<float> >&);
  
  template void 
  Resample::apply(std::valarray<std::complex<double> >&,
                  const std::valarray<std::complex<double> >&);

} // namespace datacondAPI
