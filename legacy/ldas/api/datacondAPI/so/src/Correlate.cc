#include "datacondAPI/config.h"

#include <memory>

#include "general/Memory.hh"

#include "SequenceUDT.hh"

#include "Correlate.hh"

datacondAPI::Correlate::
Correlate()
{
}


datacondAPI::Correlate::
Correlate(const Correlate& r)
{
}


datacondAPI::Correlate::
~Correlate()
{
}


const datacondAPI::Correlate& 
datacondAPI::Correlate::
operator=(const Correlate& r) 
{
    if (this != &r)
    {
	// do the copying here
    }

    return *this;
}


template<class T>
void
datacondAPI::Correlate::
apply(Sequence<T>& out, 
      const Sequence<T>& xIn,  
      const Sequence<T>& yIn )
    throw (std::invalid_argument)
{
}


template<class T>
void
datacondAPI::Correlate::
apply(Sequence<T>& out, 
      const Sequence<T>& in)
    throw (std::invalid_argument)
{
}


void datacondAPI::Correlate::
apply(udt*& out, 
      const udt& xIn,
      const udt& yIn)
    throw (std::invalid_argument)
{
    if ( (udt::IsA<Sequence<float> >(xIn)) &&
	 (udt::IsA<Sequence<float> >(yIn)) ) 
    {
	dispatch<float>
 	    (out, 
	     udt::Cast<Sequence<float> >(xIn),
	     udt::Cast<Sequence<float> >(yIn));  
    }
    else if ( (udt::IsA<Sequence<std::complex<float> > >(xIn)) &&
	      (udt::IsA<Sequence<std::complex<float> > >(yIn)) ) 
    {
	dispatch<std::complex<float> >
	    (out, 
	     udt::Cast<Sequence<std::complex<float> > >(xIn),
	     udt::Cast<Sequence<std::complex<float> > >(yIn));  
    }
    else if ( (udt::IsA<Sequence<double> >(xIn)) &&
	      (udt::IsA<Sequence<double> >(yIn)) ) 
    {
	dispatch<double>
 	    (out, 
	     udt::Cast<Sequence<double> >(xIn),
	     udt::Cast<Sequence<double> >(yIn));  
    }
    else if ( (udt::IsA<Sequence<std::complex<double> > >(xIn)) &&
	      (udt::IsA<Sequence<std::complex<double> > >(yIn)) ) 
    {
	dispatch<std::complex<double> >
	    (out, 
	     udt::Cast<Sequence<std::complex<double> > >(xIn),
	     udt::Cast<Sequence<std::complex<double> > >(yIn));  
    }
    else
    {
	throw std::invalid_argument("Correlate::apply() "
				    "input UDT's must be Sequences");
    }
	    
}


void datacondAPI::Correlate::
apply(udt*& out, 
      const udt& in)
    throw (std::invalid_argument)
{
    if (udt::IsA<Sequence<float> >(in))
    {
	dispatch<float>
            (out, 
	     udt::Cast<Sequence<float> >(in));
    }
    else if (udt::IsA<Sequence<std::complex<float> > >(in))
    {
	dispatch<std::complex<float> >
	    (out,
	     udt::Cast<Sequence<std::complex<float> > >(in));
    }
    else if (udt::IsA<Sequence<double> >(in))
    {
	dispatch<double>
	    (out, 
	     udt::Cast<Sequence<double> >(in));
    }
    else if (udt::IsA<Sequence<std::complex<double> > >(in))
    {
	dispatch<std::complex<double> >
	    (out,
	     udt::Cast<Sequence<std::complex<double> > >(in));
    }
    else
    {
	throw std::invalid_argument("Correlate::apply() "
				    "input UDT must be a Sequence");
    }

}


template<class T>
void 
datacondAPI::Correlate::
dispatch(udt*& out, 
         const Sequence<T>& xIn,
         const Sequence<T>& yIn)
    throw (std::invalid_argument)
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    unique_ptr<Sequence<T> > tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
	tmp.reset(new Sequence<T>);
	tmp_out = tmp.get();
    }
    else if (!udt::IsA<Sequence<T> >(*tmp_out))
    {
	// Stupidly can't delete the output if it's the wrong type,
	// have to throw an exception instead
	throw std::invalid_argument("Correlate::apply() "
				    "output UDT must be a Sequence");
    }

    apply(udt::Cast<Sequence<T> >(*tmp_out), xIn, yIn);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
	// transfer ownership from the unique_ptr to 'out' so that the
	// Sequence isn't deleted when the unique_ptr goes out of scope
	out = tmp.release();
    }
}


template<class T>
void 
datacondAPI::Correlate::
dispatch(udt*& out, 
         const Sequence<T>& in)
    throw (std::invalid_argument)
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    unique_ptr<Sequence<T> > tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
	tmp.reset(new Sequence<T>);
	tmp_out = tmp.get();
    }
    else if (!udt::IsA<Sequence<T> >(*tmp_out))
    {
	// Stupidly can't delete the output if it's the wrong type,
	// have to throw an exception instead
	throw std::invalid_argument("Correlate::apply() "
				    "output UDT must be a Sequence");
    }

    apply(udt::Cast<Sequence<T> >(*tmp_out), in);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
	// transfer ownership from the unique_ptr to 'out' so that the
	// Sequence isn't deleted when the unique_ptr goes out of scope
	out = tmp.release();
    }
}


#define INSTANTIATE(DATATYPE) \
template void datacondAPI::Correlate::apply( \
datacondAPI::Sequence<##DATATYPE>&, \
const datacondAPI::Sequence<##DATATYPE>&, \
const datacondAPI::Sequence<##DATATYPE>&); \
template void datacondAPI::Correlate::apply( \
datacondAPI::Sequence<##DATATYPE>&, \
const datacondAPI::Sequence<##DATATYPE>&); 

INSTANTIATE(float)
INSTANTIATE(double)
INSTANTIATE(std::complex<float>)
INSTANTIATE(std::complex<double>)
