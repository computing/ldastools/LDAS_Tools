#ifndef RMVM_STATE_HH
#define RMVM_STATE_HH

#include "StateUDT.hh"
#include "KalmanState.hh"
#include "Kalman.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"

namespace datacondAPI
{
  class rmvmState : public State
  {

    //    friend class Kalman;

  public:
    
    //: constructor
    //!param: Sequence<double> freqs - a list of frequencies at which there are violin modes
    //!param: Sequence<double> Qs - a list of Qs for violin modes corresponding to the list of frequencies
    //!param: double min_f - the frequency at which Kalman filtering is to start
    //!param: double max_f - the frequency at which Kalman filtering is to end
    //!param: Matrix<double> A - the state evolution matrix
    //!param: Matrix<double> C - the state to observable transformation matrix
    //!param: Matrix<double> W - the co-variance matrix of the process noise
    //!param: Matrix<double> V - the co-variance matrix of the measurement noise
    //!param: Sequence<double> psi - intial estimation of the state vector
    //!param: Matrix<double> P - error matrix associated with the initial state estimate
    rmvmState(Sequence<double>& freqs,
	      Sequence<double>& Qs,
	      double& min_f,
	      double& max_f,
	      Matrix<double>& A,
	      Matrix<double>& C,
	      Matrix<double>& W,
	      Matrix<double>& V,
	      Sequence<double>& psi,
	      Matrix<double>& P);

    
    //: udt constructor
    //!param: udt& freqs - a list of frequencies at which there are violin modes
    //!param: udt& Qs - a list of Qs for violin modes corresponding to the list of frequencies
    //!param: udt& min_f - the frequency at which Kalman filtering is to start
    //!param: udt& max_f - the frequency at which Kalman filtering is to end
    //!param: udt& A - the state evolution matrix
    //!param: udt& C - the state to observable transformation matrix
    //!param: udt& W - the co-variance matrix of the process noise
    //!param: udt& V - the co-variance matrix of the measurement noise
    //!param: udt& psi - intial estimation of the state vector
    //!param: udt& P - error matrix associated with the initial state estimate
    //!exc: invalid_argument - if freqs, Qs, or psi are not Sequence<doubles>, or if
    // + min_f or max_f are not Scalar<doubles>, or if A, C, W, V, or P are not Matrix<doubles>
    rmvmState(udt& freqs, udt& Qs, udt& min_f, udt& max_f,
	      udt& A, udt& C, udt& W, udt& V, udt& psi, udt& P);

    //: copy constructor
    //!param: const rmvmState& s - state that is to be copied
    rmvmState(const rmvmState& s);

    Kalman m_Kalman;

    //: accessor for violin mode frequencies
    //!param: Sequence<double>& freqs - Sequence to hold list
    void getFreq(Sequence<double>& freqs);

    //: accessor for violin mode frequencies
    //!param: udt*& freqs - pointer to Sequence which will hold list, if is NULL will allocate
    //!exc: invalid_argument - if freqs is not NULL and not a Sequence<double>
    void getFreq(udt*& freqs);
    
    //: accessor for violin mode qualities
    //!param: Sequence<double>& Qs - Sequence to hold list
    void getQ(Sequence<double>& Qs);

    //: accessor for violin mode qualities
    //!param: udt*& freqs - pointer to Sequence which will hold list, if is NULL will allocate
    //!exc: invalid_argument - if Qs is not NULL and not a Sequence<double>
    void getQ(udt*& Qs);

    //: accessor for initial frequency to be filtered
    //!param: double& min_f - will return with minimum frequency value
    void getMinF(double& min_f);

    //: accessor for initial frequency to be filtered
    //!param: udt*& min_f - pointer to Scalar which will hold value, if is NULL will allocate
    //!exc: invalid_argument - if min_f is not NULL and not a Scalar<double>
    void getMinF(udt*& min_f);

    //: accessor for final frequency to be filtered
    //!param: double& max_f - will return with maximum frequency value
    void getMaxF(double& max_f);

    //: accessor for final frequency to be filtered
    //!param: udt*& max_f - pointer to Scalar which will hold value, if is NULL will allocate
    //!exc: invalid_argument - if max_f is not NULL and not a Scalar<double>
    void getMaxF(udt*& max_f);

  private:

    Sequence<double> m_freqs;
    Sequence<double> m_Qs;
    double m_min_f, m_max_f;
  };
};
#endif //RMVM_STATE_HH
