//daub4.cc

#include "datacondAPI/config.h"

#include <valarray>
#include <general/toss.hh>

#include "daub4.hh"

using std::sqrt;

namespace
{
  const char rcsID[] = "@(#) $Id: daub4.cc,v 1.13 2006/11/07 22:25:46 emaros Exp $:";
}

namespace datacondAPI {
  namespace psu
  {
    
    Daub4::Daub4()
    {
      //ANALYSIS FILTERS
      m_H.resize(4);
      m_H[0] = (1.0-sqrt(3.0))/4.0;
      m_H[1] = -(3.0-sqrt(3.0))/4.0;
      m_H[2] = (3.0+sqrt(3.0))/4.0;
      m_H[3] = -(1.0+sqrt(3.0))/4.0;
      
      m_L.resize(4);
      m_L[0] = (1.0+sqrt(3.0))/4;
      m_L[1] = (3.0+sqrt(3.0))/4;
      m_L[2] = (3.0-sqrt(3.0))/4;
      m_L[3] = (1.0-sqrt(3.0))/4;
      
      //SYNTHESIS FILTERS-- NEED TO CHECK THESE
      m_sH.resize(4);
      m_sH[0] = (1.0+sqrt(3.0))/4.0;
      m_sH[1] = (3.0+sqrt(3.0))/4.0;
      m_sH[2] = (3.0-sqrt(3.0))/4.0;
      m_sH[3] = (1.0-sqrt(3.0))/4.0;
      
      m_sL.resize(4);
      m_sL[0] = (1.0-sqrt(3.0))/4.0;
      m_sL[1] = (-3.0+sqrt(3.0))/4.0;
      m_sL[2] = (3.0+sqrt(3.0))/4.0;
      m_sL[3] = (-1.0-sqrt(3.0))/4.0;
      
    }
    
    Daub4::~Daub4()
    {
    }
    
    Daub4* Daub4::Clone() const
    {
      return new Daub4;
    }
    
    
    ILwd::LdasElement*
    Daub4::ConvertToIlwd( const CallChain& Chain,
			  datacondAPI::udt::target_type Target) const
    {
      std::string what = "tried to call ConvertToIlwd";
      General::toss<std::invalid_argument>("Daub4", __FILE__, __LINE__, what);

      return 0; 
    }
    
  }// namespace psu
}//namespace datacondAPI


