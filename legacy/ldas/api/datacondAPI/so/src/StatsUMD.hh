/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef STATSUMD_HH
#define STATSUMD_HH
// $Id: StatsUMD.hh,v 1.7 2006/02/16 16:54:58 emaros Exp $

#include "UDT.hh"
#include <string>

namespace datacondAPI
{
	
  //: metadata specifying the properties of a statistics object
  class StatsMetaData
  {
    
  public:
    
    //: destructor
    virtual ~StatsMetaData();

    // accessors
    
    //: length of sequence statistics generated from
    //!return: unsigned int - length of sequence statistics generated from
    unsigned int GetSize() const;

    //: minimum value in sequence statistic generated from
    //!return: double - minimum value in sequence statistic generated from
    double       GetMin()  const;
    
    //: maximium value in sequence statistics generated from
    //!return: double - maximium value in sequence statistics generated from
    double       GetMax()  const;
    
    //: rms value of sequence statistics generated from
    //!return: double - rms value of sequence statistics generated from
    double       GetRMS()  const;

    //: name of channel statistic generated from(?)
    //!return: std::string - name of channel statistics generated from(?)
    //!todo: Method poorly named. Rename.
    std::string  GetStatisticsOfName() const;
    
    // mutators
    //: length of sequence statistics generated from
    //!param: size - length of sequence statistics generated from
    void SetSize(const unsigned int& size);

    //: minimum value in sequence statistic generated from
    //!param: min - minimum value in sequence statistic generated from
    void SetMin (const double&        min);

    //: maximium value in sequence statistics generated from
    //!param: max - maximium value in sequence statistics generated from
    void SetMax (const double&        max);

    //: rms value of sequence statistics generated from
    //!param: rms - rms value of sequence statistics generated from
    void SetRMS (const double&        rms);

    //: name of channel statistic generated from(?)
    //!param: std::string - name of channel statistics generated from(?)
    //!todo: Method poorly named. Rename.
    void SetStatisticsOfName(const std::string& staistics_of_name);
    
    // output
    //: Convert *this to an appropriate ilwd
    //!param: Chain - CallChain owning *this
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasElement* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasElement* 
    ConvertToIlwd( const CallChain& Chain, 
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC ) const;
    
  protected:
    
    // constructors
    //: Default constructor
    //!todo: Why is this protected!?
    StatsMetaData();
    
    //: Specify everything but channel name constructor
    //!param: size - length of sequence statistics generated from
    //!param: min - minimum value in sequence statistic generated from
    //!param: max - maximium value in sequence statistics generated from
    //!param: rms - rms value of sequence statistics generated from
    StatsMetaData(const unsigned int& size, 
		  const double&       min,
		  const double&       max,
		  const double&       rms);

    //: copy constructor
    //!todo: Why is this protected?!
    StatsMetaData(const StatsMetaData& in);
    
    /*
    // copy assignment
    const StatsMetaData& operator=(const StatsMetaData& r);
    */

  private:
    
    // data
    unsigned int m_size;
    double       m_min;
    double       m_max;
    double       m_rms;
    std::string  m_statistics_of_name;
    
  }; 
  
} // namespace datacondAPI

#endif // STATSUMD_HH
