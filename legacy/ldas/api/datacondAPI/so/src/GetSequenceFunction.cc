#include "datacondAPI/config.h"

#include <complex>

#include "SequenceUDT.hh"
#include "TypeInfo.hh"
#include "GetSequenceFunction.hh"

namespace {

    const GetSequenceFunction staticGetSequenceFunction;

}

GetSequenceFunction::GetSequenceFunction()
    : Function(GetSequenceFunction::GetName())
{
}

void GetSequenceFunction::Eval(CallChain* chain,
                               const CallChain::Params& params,
                               const std::string& ret) const
{
    using namespace datacondAPI;
    using std::complex;

    CallChain::Symbol* result = 0;

    // Only one parameter allowed
    if (params.size() != 1)
    {
      throw CallChain::BadArgumentCount(GetName(), 1, params.size());
    }

    const CallChain::Symbol* const obj = chain->GetSymbol(params[0]);

    if (const Sequence<float>* const p
        = dynamic_cast<const Sequence<float>*>(obj))
    {
        result = new Sequence<float>(*p);
    }
    else if (const Sequence<double>* const p
             = dynamic_cast<const Sequence<double>*>(obj))
    {
        result = new Sequence<double>(*p);
    }
    else if (const Sequence<complex<float> >* const p
             = dynamic_cast<const Sequence<complex<float> >*>(obj))
    {
        result = new Sequence<complex<float> >(*p);
    }
    else if (const Sequence<complex<double> >* const p
             = dynamic_cast<const Sequence<complex<double> >*>(obj))
    {
        result = new Sequence<complex<double> >(*p);
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                     "Object derived from Sequence");
    }

    chain->ResetOrAddSymbol(ret, result);
}

const std::string& GetSequenceFunction::
GetName(void) const
{
    static const std::string name("getSequence");

    return name;
}
