#ifndef _SYNTHESIZEPSUFUNCTION_HH_
#define _SYNTHESIZEPSUFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI {
namespace psu
{

class SynthesizePSUFunction : public CallChain::Function
{
public:
  SynthesizePSUFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain, const CallChain::Params& Params,
		    const std::string& Ret) const;

};

}//end namespace psu
}//end namespace datacondAPI

#endif //_SYNTHESIZEPSUFUNCTION_HH_
