#include "datacondAPI/config.h"

#include "NPSpectrum.hh"

#include <iostream>
#include <sstream>
#include <string>

#include <general/toss.hh>

#include "general/types.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldasarray.hh"

namespace 
{
    const char rcsID[] = "@(#) $Id: NPSpectrum.cc,v 1.18 2006/02/16 16:54:58 emaros Exp $:";
}

template <class T>
datacondAPI::NPSpectrum<T>::
NPSpectrum() 
  : m_data(0), m_dims(0), m_axesLB(0), m_axesUB(0) {}

template <class T>
datacondAPI::NPSpectrum<T>::
NPSpectrum(const std::valarray<size_t>& dims,
	   const std::valarray<T>& data,
 	   const std::valarray<double>& axesLB,
	   const std::valarray<double>& axesUB) 
  throw (std::length_error,std::out_of_range,General::unexpected_exception)
  : m_data(data), m_dims(dims), m_axesLB(axesLB), m_axesUB(axesUB)
{
  size_t ndims = m_dims.size();
  if ( m_axesLB.size() != size_t(ndims) || m_axesUB.size() != size_t(ndims) )
    {
      General::toss<std::length_error>("NPSpectrum", __FILE__, __LINE__, 
				       "incompatible dimensions (axes)");
    }
  
  if (m_data.size() != checkDims())
    {
      General::toss<std::length_error>("NPSpectrum", __FILE__, __LINE__,
				       "incompatible dimensions (data)");
    }

}

// default copy constructor ok

template <class T>
datacondAPI::NPSpectrum<T>::
~NPSpectrum() {}

// default assignment operator ok

template <class T>
size_t datacondAPI::NPSpectrum<T>::
getNDims(void) const { return m_dims.size(); }

template <class T>
void datacondAPI::NPSpectrum<T>::
getDims(std::valarray<size_t>& dims) const
{
  dims.resize(m_dims.size());
  dims = m_dims;
}

template <class T>
void datacondAPI::NPSpectrum<T>::
getAxis(Scalar<double>& min,
	Scalar<double>& max,
	size_t k) const
  throw (std::length_error,General::unexpected_exception)
{
  if ( k < m_dims.size())
    {
      min = Scalar<double>(m_axesLB[k]);
      max = Scalar<double>(m_axesUB[k]);
    }
  else
    {
      std::ostringstream oss;
      oss << "index k = " << k;
      std::string what = oss.str();
      General::toss<std::length_error>("NPSpectrum", __FILE__, __LINE__, what);
    }
}

template <class T>
void
datacondAPI::NPSpectrum<T>::
getAxis(double& min, double& max, size_t k) const
  throw (std::length_error,General::unexpected_exception)
{
  if ( k < m_dims.size())
    {
      min = m_axesLB[k];
      max = m_axesUB[k];
    }
  else
    {
      std::ostringstream oss;
      oss << "index k = " << k;
      std::string what = oss.str();
      General::toss<std::length_error>("NPSpectrum", __FILE__, __LINE__, what);
    }
}

template <class T>
void datacondAPI::NPSpectrum<T>::
getAxes(std::valarray<double>& min, std::valarray<double>& max) const
{
  min.resize(m_axesLB.size());
  min = m_axesLB;
  max.resize(m_axesUB.size());
  max = m_axesUB;
}


template <class T>
const T datacondAPI::NPSpectrum<T>::
operator[] (const std::valarray<size_t>& ndx) const
  throw (std::out_of_range,General::unexpected_exception)
{
  int k = 0;

  // data stored column-major

  if (ndx[0] < m_dims[0])
    {
      k = ndx[0];
    }
  else
    {
      // out-of-bounds index
      std::ostringstream oss;
      oss << "ndx[" << 0 << "] = " << ndx[0];
      std::string what = oss.str();
      General::toss<std::out_of_range>("NPSpectrum", __FILE__, __LINE__, what);
    }
  
  size_t ndims = m_dims.size();
  for (size_t j = 1; j < ndims; j++)
    {
      if ( ndx[j] < m_dims[j] )
	{
	  k *= m_dims[j];
	  k += ndx[j];
	}
      else
	{
	  // out-of-bounds index
	  std::ostringstream oss;
	  oss << "ndx[" << j << "] = " << ndx[j];
	  std::string what = oss.str();
	  General::toss<std::out_of_range>("NPSpectrum", __FILE__,
					   __LINE__, what);
	}
    }
  
  // return value
  return m_data[k];
}

template <class T>
void datacondAPI::NPSpectrum<T>::
slice(datacondAPI::udt*& out, const std::valarray<int>& mask) const
  throw (std::invalid_argument,
	 std::length_error,
	 std::out_of_range,
	 std::bad_alloc,
	 General::unexpected_exception)
{
  // need a mask value for each dimension
  if (mask.size() != m_dims.size())
    {
      General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
					   "bad mask dimension");
    }
  
  bool null_out = ( out == 0 );
  try
    {
      // validate *out
      if (null_out)
	{
	  // allocate storage for null pointer
	  out = new NPSpectrum<T>();
	}
      else if (!udt::IsA<NPSpectrum<T> >(*out))
	{
	  // 
	  General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
					       "*out wrong type");
	}
      if (!udt::IsA<NPSpectrum>(*out))
	{
	  General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
					       "*out wrong type");
	}

      // reference for convenience
      NPSpectrum<T>& npspec = udt::Cast<NPSpectrum<T> >(*out);

      // Get projection, dimensions

      std::valarray<size_t> lengths(1,1);
      std::valarray<size_t> strides(1,1);
      std::gslice gs(0,lengths,strides);

      std::valarray<size_t> dims(1);

      getGSlice(gs, dims, mask);

      // Get axis values
      const int ndims = dims.size();
      std::valarray<double> axesLB(ndims);
      std::valarray<double> axesUB(ndims);
      int j = 0;
      for (size_t k = 0; k < m_dims.size(); k++)
	{
	  if (mask[k] < 0)
	    {
	      axesUB[j] = m_axesUB[k];
	      axesLB[j] = m_axesLB[k];
	      j++;
	    }
	}
      
      // Set the new spectrum object
      npspec.resize(dims,m_data[gs],axesLB,axesUB);
    }
  catch (...)
    {
      // Don't leak memory
      if (null_out)
	delete out;
      throw;
    }
}

template <class T>
void datacondAPI::NPSpectrum<T>::
getData(std::valarray<T>& data) const
{
  data.resize(m_data.size());
  data = m_data;
}

template <class T>
void datacondAPI::NPSpectrum<T>::
getData(std::valarray<T>& data, 
	const std::valarray<int>& mask) const
  throw (std::invalid_argument,
	 std::length_error,
	 General::unexpected_exception)
{
  if (mask.size() != m_dims.size())
    {
      General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
					   "bad mask dimension");
    }
  
  std::valarray<size_t> dims;
  std::gslice gs(0,std::valarray<size_t>(1,1),std::valarray<size_t>(1,1));
  getGSlice(gs, dims, mask);

  int ndata = 1;
  for (size_t k = 0; k < dims.size(); k++)
    ndata *= dims[k];
  data.resize(ndata);
  data = m_data[gs];

}

template <class T>
void datacondAPI::NPSpectrum<T>::
resize(const std::valarray<size_t>& dims,
       const std::valarray<T>& data,
       const std::valarray<double>& axesLB,
       const std::valarray<double>& axesUB) 
  throw (std::out_of_range,std::invalid_argument,
	 std::length_error,General::unexpected_exception)
{
  size_t ndims = dims.size();
  m_dims.resize(ndims);
  m_dims = dims;
  size_t ndata = checkDims();
  if (data.size() != ndata)
    {
      General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
				       "data.size() not compatible with dims");
    }
  m_data.resize(ndata);
  m_data = data;

  if (axesLB.size() != ndims || axesUB.size() != ndims)
    {
      General::toss<std::length_error>("NPSpectrum", __FILE__, __LINE__, 
				       "incompatible dimensions (axes)");
    }
  m_axesLB.resize(ndims);
  m_axesLB = axesLB;
  m_axesUB.resize(ndims);
  m_axesUB = axesUB;
}
    
template <class T>
void datacondAPI::NPSpectrum<T>::
resize(const std::valarray<size_t>& dims) 
  throw (std::out_of_range,
	 General::unexpected_exception) 
{
  m_dims.resize(dims.size());
  m_dims = dims;
  
  m_axesUB.resize(dims.size());
  m_axesLB.resize(dims.size());
  m_axesUB = 0;
  m_axesLB = 0;

  m_data.resize(checkDims());
  m_data = T(0);
}

template <class T>
void datacondAPI::NPSpectrum<T>::
setAxis(double min, double max, int k)
  throw (std::out_of_range,General::unexpected_exception)
{
  if ( (unsigned int)k < m_dims.size() && k >= 0)
    {
      m_axesUB[k] = max;
      m_axesLB[k] = min;
    }
  else
    {
      std::ostringstream oss;
      oss << "index k = " << k << " out of range";
      std::string what = oss.str();
      General::toss<std::out_of_range>("NPSpectrum", __FILE__, __LINE__, what);
    }
}

template <class T>
void datacondAPI::NPSpectrum<T>::
setAxes(const std::valarray<double>& min,
	const std::valarray<double>& max)
{
  if (min.size() != max.size() || min.size() != m_dims.size())
    {
      General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
				  "min, max, num. dimensions not compatible"); 
    }
  m_axesLB.resize(min.size());
  m_axesLB = min;
  m_axesUB.resize(max.size());
  m_axesUB = max;
}
    
template <class T>
void datacondAPI::NPSpectrum<T>::
slice(const std::valarray<int>& mask)
{
  General::toss<General::unimplemented_error>("NPSpectrum",__FILE__,__LINE__,
				 "slice(const std::valarray<int>& mask)");
}
    
template <class T>
void datacondAPI::NPSpectrum<T>::
setData(const std::valarray<T>& data)
  throw (std::out_of_range,std::invalid_argument,General::unexpected_exception)
{
  size_t ndata = checkDims();
  if (data.size() != ndata)
    {
      std::ostringstream oss;
      oss << "data.size() = " << data.size() 
	  << "; should be " << ndata;
      std::string what = oss.str();
      General::toss<std::invalid_argument>("NPSpectrum", __FILE__,
					   __LINE__, what);
    }
  m_data.resize(data.size());
  m_data = data;
}
    
template <class T>
void datacondAPI::NPSpectrum<T>::
setData(const std::valarray<T>& data, 
	const std::valarray<int>& mask)
  throw (General::unimplemented_error,General::unexpected_exception)
{
  std::string what = "setData(const std::valarray<T>&, const std::valarray<int>&)";
  General::toss<General::unimplemented_error>("NPSpectrum",__FILE__,
					      __LINE__,what);
}
    
template <class T>
size_t datacondAPI::NPSpectrum<T>::
checkDims() const throw (std::out_of_range,General::unexpected_exception)
{
  // if m_dims.size() == 0 then we are not yet set. Should that be an error?
  if (m_dims.size() == 0)
    return 0;

  int n = 1;
  for ( size_t k = 0; k < m_dims.size(); k++)
    {
      if (m_dims[k] > 0)
	{
	  n *= m_dims[k];
	}
      else
	{
	  General::toss<std::out_of_range>("NPSpectrum", __FILE__, __LINE__,
					   "non-positive dimension");
	}
    }
  return n;
}

template <class T>
void datacondAPI::NPSpectrum<T>::
getGSlice(std::gslice& gs,
	std::valarray<size_t>& dims,
	const std::valarray<int>& mask) const
  throw (std::invalid_argument,General::unexpected_exception)
{
  // Get dimensions and storage requirements
  int ndims = 0;
  int ndata = 1;
  for (size_t k = 0; k < m_dims.size(); k++)
    {
      if ( mask[k] < 0 )
	{
	  ndims++;
	  ndata *= m_dims[k];
	}
    }
  
  // insure slice isn't a single element
  if (ndims == 0)
    {
      General::toss<std::invalid_argument>("NPSpectrum",__FILE__,__LINE__,
				       "single element mask: use operator[]");
    }
  
  // Set the dimensions
  dims.resize(ndims);
  int j = 0;
  for (size_t k = 0; k < m_dims.size(); k++)
    {
      if ( mask[k] < 0 )
	{
	  dims[j++] = m_dims[k];
	}
    }
  
  // Projection is a gslice: 
  // gslice(start_offset, lengths, strides)
  // lengths are the dimensions
  // strides are derived from dimensions
  // column-major strides
  int stride = 1;
  int start = 0;
  std::valarray<size_t> lengths(dims.size());
  std::valarray<size_t> strides(dims.size());
  lengths = dims;
  j = dims.size()-1;
  for (int k = m_dims.size()-1; k >= 0; k--)
    {
      if (mask[k] < 0)
	{
	  strides[j] = stride;
	  j--;
	}
      else
	{
	  start += stride*mask[k];
	}
      stride *= m_dims[k];
    }
  gs = std::gslice(start,lengths,strides);
}

template<class T>
datacondAPI::NPSpectrum<T>*
datacondAPI::NPSpectrum<T>::
Clone(void) const
  throw (std::bad_alloc,General::unexpected_exception)
{
  return new NPSpectrum<T>(*this);
}

template <class T>
ILwd::LdasElement*
datacondAPI::NPSpectrum<T>::
ConvertToIlwd(const CallChain& Chain,
	      target_type Target) const
  throw (General::unimplemented_error,General::unexpected_exception)
{
  General::toss<General::unimplemented_error>("NPSpectrum",__FILE__,__LINE__,
					      "ConvertToIlwd");
  // Should never get here, but need this here so compiler does not
  //   generate a warning about control reaching the end of non-void
  //   function.
  return (ILwd::LdasElement*)NULL;
}
    
///----------------------------------------------------------------------
/// Instantiation of the class
///----------------------------------------------------------------------

#define	CLASS_INSTANTIATION(class_,key_) \
  template class datacondAPI::NPSpectrum< class_ >; \
  UDT_CLASS_INSTANTIATION(NPSpectrum< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)

#undef CLASS_INSTANTIATION
