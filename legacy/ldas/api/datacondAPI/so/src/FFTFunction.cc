// $Id:
//
// "fft" action
//
// [y = ]fft(x)
//
// x: valarray<T> where T is float, double, complex<float>, complex<double>
//    input sequence
// y: fvalarray<T> where T is float, double
//    output sequence
//
// "ifft" action
//
// [y = ]ifft(x)
//
// x: fvalarray<T> where T is float, double
//    output sequence
// y: valarray<T> where T is float, double, complex<float>, complex<double>
//    input sequence

#include "datacondAPI/config.h"

#include <memory>

#include "FFTFunction.hh"
#include "fft.hh"
#include "ifft.hh"

namespace {

    //
    // We construct a static, global instance of an FFTFunction so that the
    // constructor is called when the API is loaded. The constructor passes the
    // action name on to the Function constructor, thus registering it as a
    // valid action.
    //

    FFTFunction staticFFTFunction;
}

//-----------------------------------------------------------------------------
// FFTFunction::FFTFunction
//-----------------------------------------------------------------------------

FFTFunction::FFTFunction()
    : Function( FFTFunction::GetName() )
{
    //-------------------------------------------------------------------------
    // The sole task of the LinfiltFunction constructor is to pass the action
    // name, defined by GetName, to the Function constructor.
    //-------------------------------------------------------------------------
}

//-----------------------------------------------------------------------------
// FFTFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "fft". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//-----------------------------------------------------------------------------

void 
FFTFunction::Eval(      CallChain* Chain,
                        const CallChain::Params& Params,
                        const std::string& Ret) const
{
    if (Params.size() != 1)
    {
        throw CallChain::BadArgumentCount("fft", 1, Params.size());
    }

    const CallChain::Symbol& in = *(Chain->GetSymbol(Params[0]));

    CallChain::Symbol* out_ptr = 0;
    datacondAPI::FFT fft;

    fft.apply(out_ptr, in);
    
    Chain->ResetOrAddSymbol( Ret, out_ptr );
}

//-----------------------------------------------------------------------------
// FFTFunction::GetName
//-----------------------------------------------------------------------------

const std::string&
FFTFunction::GetName(void) const
{
    //-------------------------------------------------------------------------
    // The GetName function returns the string "fft", the name of the
    // action as presented to the user. The name is stored as a static variable
    // and returned as a const reference for performance reasons.
    //-------------------------------------------------------------------------
    static const std::string name("fft");
    return name;
}

//=======================================================================
// Inverse FFT Function
//=======================================================================

namespace {
    //
    // We construct a static, global instance of an IFFTFunction so that the
    // constructor is called when the API is loaded. The constructor passes the
    // action name on to the Function constructor, thus registering it as a
    // valid action.
    //
    
    static IFFTFunction staticIFFTFunction;      // Force one creation
}

//-----------------------------------------------------------------------------
// IFFTFunction::IFFTFunction
//-----------------------------------------------------------------------------

IFFTFunction::
IFFTFunction()
    : Function( IFFTFunction::GetName() )
{
    //-------------------------------------------------------------------------
    // The sole task of the IFFTFunction constructor is to pass the action
    // name, defined by GetName, to the Function constructor.
    //-------------------------------------------------------------------------
}

//-----------------------------------------------------------------------------
// IFFTFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "ifft". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//-----------------------------------------------------------------------------

void IFFTFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
    const
{
    if (Params.size() != 1)
    {
        throw CallChain::BadArgumentCount("ifft", 1, Params.size());
    }

    const CallChain::Symbol& in = *(Chain->GetSymbol(Params[0]));

    CallChain::Symbol* out_ptr = 0;
    datacondAPI::IFFT ifft;

    ifft.apply(out_ptr, in);
    
    Chain->ResetOrAddSymbol( Ret, out_ptr );
}

//-----------------------------------------------------------------------------
// IFFTFunction::GetName
//-----------------------------------------------------------------------------

const std::string& IFFTFunction::
GetName(void) const
{
    //-------------------------------------------------------------------------
    // The GetName function returns the string "ifft", the name of the
    // action as presented to the user. The name is stored as a static variable
    // and returned as a const reference for performance reasons.
    //-------------------------------------------------------------------------
  
    static const std::string name("ifft");
    return name;
}
