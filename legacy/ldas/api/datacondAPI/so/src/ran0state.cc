#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ran0state.hh"
#include "random.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

namespace datacondAPI {

  ran0state::ran0state()
  {
    idum = -248673140;
  }

  ran0state::ran0state(int seed)
  {
    idum = seed;
    if(seed >= 0 )
      throw std::invalid_argument("seed value not negative");
  }

  ran0state::ran0state(const ran0state& g)
    : State( g )
  {
    idum = g.idum;
  }

  //restest state with new seed s
  void ran0state::seed(int s)
  {
    idum = s;
    if(s >= 0 )
      throw std::invalid_argument("seed value not negative");
  }

  float ran0state::getran()
  {
	long k;
	float ans;
	//idum ^= MASK;
	k = idum/IQ;
	idum = IA *(idum-k*IQ) - IR*k;
	if (idum<0)
		idum += IM;
	ans = AM * idum;
	//idum ^= MASK;
	return ans;
  }

  void ran0state::getran(std::valarray<float>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = getran();
  }

  ran0state* ran0state::Clone() const
  {
    return new ran0state(*this);
  }

  ILwd::LdasElement* ran0state::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");
    return container;
  }

}
