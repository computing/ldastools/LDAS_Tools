/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef GET_FILTER_COEFFS_FUNCTION_HH
#define GET_FILTER_COEFFS_FUNCTION_HH

// $Id: getFilterCoeffsFunction.hh,v 1.3 2007/01/24 17:51:28 emaros Exp $

#include "CallChain.hh"

//-----------------------------------------------------------------------------
//: Function support for getFilterCoeffs action
//
// This class implements the "getFilterCoeffs" function to be used to extract
// the FIR (and optionally IIR) filter coefficents from a state obejct
// containing one or more time-domain linear filters. The syntax, as it would
// appear in the action sequence is:
//
// getFilterCoeffs(state, fir_coeffs, opt_iir_coeffs);
//
class getFilterCoeffsFunction: CallChain::Function
{

public:

    //-------------------------------------------------------------------------
    //: Constructor
    //
    // Construct a new getFilterCoeffsFunction
    getFilterCoeffsFunction();

    //-------------------------------------------------------------------------
    //: Evaluate the getFilterCoeffs Function
    //
    // This retrieves the coefficents. The list of parameters must contain a
    // state, integer filter index, and output variable(s)
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    //
    virtual void Eval(CallChain* Chain,
        const CallChain::Params& Params,
        const std::string& Ret) const;

    //-------------------------------------------------------------------------
    //: Return the Name of the function ("getFilterCoeffs")
    //
    //!return: const std::string& ptName - Returns a reference to the name of
    //!return: the function
    virtual const std::string& GetName(void) const;

    //-------------------------------------------------------------------------
    //: Validate parameters
    virtual void
    ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
			const CallChain::Params& Params ) const;

};

#endif // GET_FILTER_COEFFS_FUNCTION_HH
