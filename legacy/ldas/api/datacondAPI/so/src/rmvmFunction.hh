#ifndef RMVM_FUNCTION_HH
#define RMVM_FUNCTION_HH

#include "CallChain.hh"
#include "ShiftState.hh"
#include "Mixer.hh"
#include "MixerState.hh"
#include "Resample.hh"

class rmvmFunction : public CallChain::Function
{
public:
  rmvmFunction();
  virtual const std::string& GetName(void) const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

namespace datacondAPI{
class BandSelector
    {

    public:

        BandSelector(const double& frequency, const std::size_t factor)
            : m_downmixer(MixerState(0.0, -frequency))
            , m_downsampler(1, factor)
            , m_downshifter(0)
            , m_upsampler(factor, 1)
            , m_upshifter(0)
            , m_upmixer(MixerState(0.0, +frequency))
        {
        }

        ~BandSelector()
        {
            delete m_downshifter;
            delete m_upshifter;
        }
   
        BandSelector* clone() const
        {
            return new BandSelector(*this);
        }

        template<typename out_t, typename in_t>
            void apply(std::valarray<out_t>& out, const std::valarray<in_t>& in)
        {
            std::valarray<out_t> downmixed;
            m_downmixer.apply(downmixed, in);
            Sequence<out_t> downsampled;
            m_downsampler.apply(downsampled, downmixed);
            if (!m_downshifter) m_downshifter = new ShiftState<out_t>(m_downsampler.getDelay(), 1);
            dynamic_cast<ShiftState<out_t>&>(*m_downshifter).apply(downsampled);
            out.resize(downsampled.size());
            out = downsampled;
        }

        template<typename out_t, typename in_t>
            void ylppa(std::valarray<out_t>& out, const std::valarray<in_t>& in)
        {
            Sequence<in_t> upsampled;
            m_upsampler.apply(upsampled, in);
            if (!m_upshifter) m_upshifter = new ShiftState<in_t>(m_upsampler.getDelay(), 1);
            dynamic_cast<ShiftState<in_t>&>(*m_upshifter).apply(upsampled);
            m_upmixer.apply(out, upsampled);
        }

    private:

        BandSelector();

        Mixer m_downmixer;
        Resample m_downsampler;
        State* m_downshifter;
        Resample m_upsampler;
        State* m_upshifter;
        Mixer m_upmixer;

};
}//namespace
#endif //RMVM_FUNCTION_HH
