#include "config.h"

#include "SynthesizePSUFunction.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "reconstructor.hh"
#include "result.hh"

using namespace datacondAPI::psu;


const std::string& SynthesizePSUFunction::GetName(void) const
{
  static std::string name("SynthesizePSU");
  return name;
}

SynthesizePSUFunction::SynthesizePSUFunction()
  : Function( SynthesizePSUFunction::GetName() )
{}

static SynthesizePSUFunction _SynthesizePSUFunction;

void SynthesizePSUFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			       const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;

  switch(Params.size())
    {      
    case 1:
      //SynthesizePSU(Result)
      {	
			
	if(datacondAPI::udt::IsA<Result<double> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<double> in(datacondAPI::udt::Cast<Result<double> >(*Chain->GetSymbol(Params[0])));
	    if(in.checkMetaData())
	      {
		datacondAPI::TimeSeries<double> out;
		datacondAPI::udt* pout = &out;
		Reconstructor reconstruction;
		reconstruction.apply(pout, in);
		returnedSymbol = new datacondAPI::TimeSeries<double> (out);
		//returnedSymbol = new datacondAPI::Scalar<int> (801);
	      }
	    else
	      {
		datacondAPI::Sequence<double> out;
		datacondAPI::udt* pout = &out;
		Reconstructor reconstruction;
		reconstruction.apply(pout, in);
		returnedSymbol = new datacondAPI::Sequence<double> (out);
		//returnedSymbol = new datacondAPI::Scalar<int> (802);
	      }
	  }

	else if (datacondAPI::udt::IsA<Result<float> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<float> in( datacondAPI::udt::Cast<Result<float> >(*Chain->GetSymbol(Params[0])));

	    if(in.checkMetaData())
	      {
		//returnedSymbol = new datacondAPI::Scalar<int> (401);
		datacondAPI::TimeSeries<float> out;
		datacondAPI::udt* pout = &out;
		Reconstructor reconstruction;
		reconstruction.apply(pout, in);
		returnedSymbol = new datacondAPI::TimeSeries<float> (out);
		
	      }

	    else
	      {
		//returnedSymbol = new datacondAPI::Scalar<int> (402);
		datacondAPI::Sequence<float> out;
		datacondAPI::udt* pout = &out;
		Reconstructor reconstruction;
		reconstruction.apply(pout, in);
		returnedSymbol = new datacondAPI::Sequence<float> (out);
	      }

	  }

	else
	  throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type", "Usage: SynthesizePSUFunction(in)");



	break;
      }//end case 1

    case 2:
      //SynthesizePSU(Result, level)
      {

	if(datacondAPI::udt::IsA<Result<double> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<double> in(datacondAPI::udt::Cast<Result<double> >(*Chain->GetSymbol(Params[0])));
	    if(in.checkMetaData())
	      {
		datacondAPI::TimeSeries<double> out;
		datacondAPI::udt* pout = &out;
		if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
		  {
		    datacondAPI::Scalar<int> level (datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue());
		    Reconstructor reconstruction;
		    reconstruction.apply(pout, in, level);
		    returnedSymbol = new datacondAPI::TimeSeries<double> (out); 
		  }
	      }
	    else
	      {
		datacondAPI::Sequence<double> out;
		datacondAPI::udt* pout = &out;
		if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
		  {
		    datacondAPI::Scalar<int> level (datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue());
		    Reconstructor reconstruction;
		    reconstruction.apply(pout, in, level);
		    returnedSymbol = new datacondAPI::Sequence<double> (out); 
		  }
	      }
	  }

	else if (datacondAPI::udt::IsA<Result<float> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<float> in( datacondAPI::udt::Cast<Result<float> >(*Chain->GetSymbol(Params[0])));
	    if(in.checkMetaData())
	      {
		datacondAPI::TimeSeries<float> out;
		datacondAPI::udt* pout = &out;

		if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
		  {
		    datacondAPI::Scalar<int> level (datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue());
		    Reconstructor reconstruction;
		    reconstruction.apply(pout, in, level);
		    //returnedSymbol = new datacondAPI::Scalar<int> (99999999);
		    returnedSymbol = new datacondAPI::TimeSeries<float> (out);
		  }
	      }
	    else
	      {
		datacondAPI::Sequence<float> out;
		datacondAPI::udt* pout = &out;
		if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
		  {
		    datacondAPI::Scalar<int> level (datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue());
		    Reconstructor reconstruction;
		    reconstruction.apply(pout, in, level);
		    returnedSymbol = new datacondAPI::Sequence<float> (out);
		  }
	      }

	  }
	else
	  throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type", "Usage: SynthesizePSUFunction(in, level)");


	break;
      }//end case 2

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: SynthesizePSU(in, level)");
	break;
      }

    }//end switch

  if(!returnedSymbol)
    {
      throw CallChain::NullResult( GetName() );
    }

  Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval


