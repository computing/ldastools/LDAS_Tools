/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef LINFILT_HH
#define LINFILT_HH

#include <general/unexpected_exception.hh>
#include <general/unimplemented_error.hh>

#include "LinFiltState.hh"

// Forward declarations
namespace std {
  template<class T> class valarray;
}

namespace datacondAPI {

  class LinFilt {
  public:

    //: Construct from State
    //!param: state - existing LinFiltState object
    LinFilt(const LinFiltState& state);

    //: Construct from transfer function
    //!param: b - FIR transfer function
    //!param: a - IIR transfer function 
    LinFilt(const std::valarray<double>& b,
	    const std::valarray<double>& a);

    //: Copy constructor
    //!param: lf - existing LinFilt object
    LinFilt(const LinFilt& lf);

    //: Destructor
    ~LinFilt();

    //: Assignment operator. Required to be assignable (C++ spec 23.1.3, 4)
    //!param: lf - LinFilt object to assign from
    LinFilt& operator=(const LinFilt& lf);

    //: extract copy of LinFilt state
    LinFiltState getState();

    //: extract copy of LinFilt state
    //!param: state - holds copy of state on return
    void getState(LinFiltState& state);

    //: extract copy of LinFilt state
    //!param: state - points to copy of state on return. May be null on
    //+ entry, in which case storage is allocated on heap
    //!exc: std::invalid_argument - state not an LinFiltState
    void getState(udt*& state);

    //: change or set the LinFilt state
    //!param: state - new state, which is copied.
    void setState(const LinFiltState& state);

    // Actions:

    //: Basic in-place filter action
    //!param: x - input data for filter.
    //!exc: std::invalid_argument - x.size() == 0
    template<class T>
    void apply(std::valarray<T>& x);

    //: Basic out-of-place filter action
    //!param: out - result of filter acting on in. Existing contents destroyed.
    //!param: in - input data for filter.
    //!exc: std::invalid_argument - in.size() == 0
    template<class TOut, class TIn>
    void apply(std::valarray<TOut>& out, const std::valarray<TIn>& in);

    //: Interpreted filter action (metadata interpreted & set)
    //!param: out - points to result of linear filtering in. May be null on 
    //+ entry, in which case the appropriate output object is allocated
    //+ on heap. 
    //!param: in - data on which linear filter is to act
    //!exc: unimplemented_error - not implemented on type of in
    //!exc: std::invalid_argument - out incompatible with in
    void apply(udt*& out, const udt& in);

  private:

    //: Helper class for apply(udt*&, const udt&). Attempts to apply filter
    //+ on objects of type T
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!param: in - data on which to attempt to apply filter
    //!return: bool - true if successful applying filter to in
    //!exc: std::invalid_argument - out incompatible with in
    template <class T>
    bool applyAs(udt*& out, const udt& in);

    //: Helper class for apply(udt*&, udt&). Attempts to apply filter
    //+ interpreting data as a Sequence<>. Sets metadata as appropriate.
    //!param: in - data on which to apply filter
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ param: on heap.
    //!return: bool - true if successful interpreting as Sequence<>
    //!exc: std::invalid_argument - out incompatible with in
    bool asSequence(udt*& out, const udt& in);

    //: Helper class for apply(udt*&, udt&). Attempts to apply filter 
    //+ interpreting data as a TimeSeries<>. Sets metadata as appropriate.
    //!param: in - data on which to apply filter
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!return: bool - true if successful interpreting as TimeSeries<>
    //!exc: std::invalid_argument - out incompatible with in
    bool asTimeSeries(udt*& out, const udt& in);

    //: Default constructor private. 
    LinFilt();

    //: Current state of object
    LinFiltState m_state;
  };

} // namespace datacondAPI

#endif // LINFILT_HH
