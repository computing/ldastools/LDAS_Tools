#ifndef DATA_FUNCTIONS_HH
#define	DATA_FUNCTIONS_HH

#include "CallChain.hh"
#include "UDT.hh"

//: Complex numbers
//
// This class creates a complex number based on parameters passed to it
// in its Eval method. Complex numbers can be of type float or double.
template<class DataType_>
class ComplexFunction: CallChain::Function
{
public:
  //: Constructor
  ComplexFunction(void);
  //: Evaluation
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
  //: Name of action to use in call chain.
  //
  //!return: The name of this action in the CallChain.
  //+ <table border>
  //+ <tr><th>C Type<th>Action Name
  //+ <tr><td>double<td>dcomplex
  //+ <tr><td>float<td>scomplex
  //+ </table>
  virtual const std::string& GetName(void) const;
};

//: DFTs
template<class DataType_>
class DFTFunction: CallChain::Function
{
public:
  //: Constructor
  DFTFunction(void);
  //: Evaluation
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
  //: Name of action to use in call chain.
  //
  //!return: The name of this action in the CallChain.
  //+ <table border>
  //+ <tr><th>C Type<th>Action Name
  //+ <tr><td>double<td>dfvalarray
  //+ <tr><td>float<td>sfvalarray
  //+ </table>
  virtual const std::string& GetName(void) const;

private:
  //: Return base
  //!exc: runtime_error("Unable to convert symbol of type ... to ...") - 
  //+ is thrown when the Symbol cannot be converted to a DataType_ type.
  DataType_ get_base(const CallChain::Symbol* Symbol) const;
  //: Return size
  //!exc: runtime_error("Unable to convert symbol of type ... to size_t") - 
  //+ is thrown when the Symbol cannot be converted to a size_t type.
  size_t get_len(const CallChain::Symbol* Symbol) const;
};

//: Integer numbers
//
// This class takes its input value and converts it to an integer. This
// is good for creating integers (ex: integer(5)) and for doing rounding
// (ex: integer(5.5)). When doing rounding, the function acts like the
// floor function common in most implementations of the standard C math
// library.
class IntegerFunction: CallChain::Function
{
public:
  //: Constructor
  IntegerFunction(void);
  //: Evaluation
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
  //: Return name of fuction to use in call chain.
  virtual const std::string& GetName(void) const;
};

//: Sequences of numbers
//
// A sequence of numbers. See GetName() for a list of currently supported
// sequence types.
template<class DataType_>
class SequenceFunction: CallChain::Function
{
public:
  //: Constructor
  SequenceFunction(void);
  //: Evaluation
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
  //: Name of action to use in call chain.
  //
  //!return: The name of this action in the CallChain.
  //+ <table border>
  //+ <tr><th>C Type<th>Action Name
  //+ <tr><td>double<td>dvalarray
  //+ <tr><td>float<td>svalarray
  //+ <tr><td>complex&lt;double&gt;<td>cdvalarray
  //+ <tr><td>complex&lt;float&gt;<td>csvalarray
  //+ </table>
  virtual const std::string& GetName(void) const;

private:
  //: Return base
  // This is a helper function to obtain the initial value for the sequence.
  //
  //!return: DataType_ - The value used to initialize the sequence.
  //
  //!exc: runtime_error("Unable to convert symbol of type ... to ...") - 
  //+ is thrown when the Symbol cannot be converted to a DataType_ type.
  DataType_ get_base(const CallChain::Symbol* Symbol) const;

  //: Return size
  // This is a helper function to retrieve the length of the sequence.
  //
  //!return: size_t - number of elements in the sequence.
  //
  //!exc: runtime_error("Unable to convert symbol of type ... to size_t") - 
  //+ is thrown when the Symbol cannot be converted to a size_t type.
  size_t get_len(const CallChain::Symbol* Symbol) const;
};

#endif	/* DATA_FUNCTIONS_HH */
