/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef GETELEMENTFUNCTION_HH
#define GETELEMENTFUNCTION_HH

// $Id: getElementFunction.hh,v 1.1 2002/11/30 01:14:05 Philip.Charlton Exp $

#include "CallChain.hh"

//
//: Function support for getElement action
//
// y = getElement(x, k);
//
class getElementFunction : public CallChain::Function
{

public:

    getElementFunction();

    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;

    virtual const std::string& GetName() const;

private:
  void checkIndex(const size_t xSize, const int k) const;
};

#endif // GETELEMENTFUNCTION_HH
