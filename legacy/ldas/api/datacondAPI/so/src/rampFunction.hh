#ifndef _RAMPFUNCTION_HH_
#define _RAMPFUNCTION_HH_

#include "CallChain.hh"

class rampFunction : public CallChain::Function {
public:
  rampFunction();

  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif // _RAMPFUNCTION_HH_
