/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef WHEN_UMD_HH
#define WHEN_UMD_HH

// $Id: WhenUMD.hh,v 1.13 2003/02/28 00:57:20 emaros Exp $

#include "general/gpstime.hh"

#include "UDT.hh"

namespace ILwd
{
  class LdasElement;
}

namespace ILwdFCS
{
  class FrameH;
  class FrProcData;
}

class CallChain;

namespace datacondAPI
{
  //
  //: Class for providing start- and end-time information
  //
  // This class allows a class derived from it the facility to store
  // and retrieve GPS times representing a start-time and end-time.
  // The usage is that any class which requires this information should
  // be derived from the combination of WhenMetaData and whatever other
  // base classes are necessary. The WhenMetaData provides methods to
  // set and get the start-time, but only to get the end-time. This is
  // based on the assumption that an object will have an arbitrary
  // start-time but that the end-time depends on the object. For this
  // reason the GetEndTime() function is pure virtual and must be overloaded
  // in the derived class.
  // The interpretation of start-time and end-time is left up to the
  // derived classes. 
  //
  class WhenMetaData
  {

  public:

    //: Default constructor
    WhenMetaData();
    
    //: Construct from a General::GPSTime object
    //
    //!param: start_time - time corresponding to first element of
    //+ associated UDT, or first element of UDT that associated UDT 
    //+ is derived from 
    WhenMetaData(const General::GPSTime& start_time);

    //: Destructor
    virtual ~WhenMetaData();

    //: Virtual assignment operator
    //
    // This sets all *applicable* attributes of the left-hand
    // side from the right-hand side
    //
    //!param: rhs - object to be assigned from
    //!ret: A const reference to the "this"
    virtual
    const WhenMetaData& operator=(const WhenMetaData& rhs);

    //: Returns the start-time associated with the first sample of the 
    //+ associated UDT or its progenitor. This time may be zero if there 
    //+ is no such time (i.e., if the object is a construct not derived 
    //+ from, e.g., channel data).
    //!return: General::GPSTime - the start-time
    General::GPSTime GetStartTime() const;

    //: Returns the end-time associated with the first sample of the 
    //+ associated UDT or its progenitor. This time may be zero if there 
    //+ is no such time (i.e., if the object is a sequence, or if it is
    //+ a construct not derived from, e.g., channel data).
    //!return: General::GPSTime - the end-time 
    virtual
    General::GPSTime GetEndTime() const = 0;

    //: Returns the start-time associated with the first sample of the 
    //+ associated UDT or its progenitor. This time may be zero if there 
    //+ is no such time (i.e., if the object is a construct not derived 
    //+ from, e.g., channel data).
    void SetStartTime(const General::GPSTime& start_time);

    //: Convert this object to an ILWD, returning pointer to ILWD
    //!param: const CallChain& -
    //!param: datacondAPI::udt::target_type - 
    //!return: ILwd::LdasElement* - the object as an ILWD. Storage is 
    //+ allocated on the heap and owned by the pointer that is the 
    //+ destination. 
    //!todo: Finish the documentation of the param list, check correctness
    //+ of return description
    virtual ILwd::LdasElement* 
    ConvertToIlwd(const CallChain&, datacondAPI::udt::target_type) const;

    //: Convert this object to an ILWD
    //!param: const CallChain& - call chain owning *this
    //!param: datacondAPI::udt::target_type - 
    //!param: ILwd::LdasContainer& - receives result
    void ConvertToIlwd(const CallChain&,
		       datacondAPI::udt::target_type,
		       ILwd::LdasContainer& Container ) const;

    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrProcData& Storage - place to store values
    void Store( ILwdFCS::FrProcData& Storage ) const;
    
    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrameH& Storage - place to store values
    void Store( ILwdFCS::FrameH& Storage ) const;

  protected:
    //: Store meta data
    //!param: ILwd::LdasContainer& Storage - place to store values
    //!param: datacondAPI::udt::target_type Target - target type
    void store( ILwd::LdasContainer& Storage,
		datacondAPI::udt::target_type Target ) const;
    
  private:

    General::GPSTime m_start_time;
  }; // class WhenMetaData

  //
  //: Class for providing arbitrary start- and end-time information
  //
  // This class allows a class derived from it the facility to store
  // and retrieve GPS times representing a start-time and end-time.
  // The usage is that any class which requires this information should
  // be derived from the combination of ArbitraryWhenMetaData and whatever
  // other base classes are necessary. The class provides methods to
  // set and get the start-time and end-time. 
  // The interpretation of start-time and end-time is left up to the
  // derived classes. 
  //
  class ArbitraryWhenMetaData : public WhenMetaData {

  public:
    //: Default constructor
    ArbitraryWhenMetaData();
    
    //: Construct from General::GPSTime objects
    //!param: start_time - time corresponding to first element of
    //+ associated UDT, or first element of UDT that associated UDT 
    //+ is derived from 
    //!param: stop_time - time corresponding to final(?) element of
    //+ associated UDT, or final(?) element of UDT that associated UDT 
    //+ is derived from 
    ArbitraryWhenMetaData( const General::GPSTime& start_time,
			   const General::GPSTime& end_time );

    //: Virtual assignment operator
    //
    // This sets all *applicable* attributes of the left-hand
    // side from the right-hand side. Since the rhs has functions
    // for getting both start- and end-time, there is enough information
    // to properly assign the whole class
    //
    //!param: rhs - object to be assigned from
    //!ret: A const reference to the "this"
    virtual
    const ArbitraryWhenMetaData& operator=(const WhenMetaData& rhs);

    //: Returns the end-time associated with the first sample of the 
    //+ associated UDT or its progenitor. This time may be zero if there 
    //+ is no such time (i.e., if the object is a sequence, or if it is
    //+ a construct not derived from, e.g., channel data).
    //!return: General::GPSTime - the end-time 
    virtual
    General::GPSTime GetEndTime() const;

    //: Returns the end-time associated with the first sample of the 
    //+ associated UDT or its progenitor. This time may be zero if there 
    //+ is no such time (i.e., if the object is a sequence, or if it is
    //+ a construct not derived from, e.g., channel data).
    void SetEndTime(const General::GPSTime& end_time);

    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrProcData& Storage - place to store values
    void Store( ILwdFCS::FrProcData& Storage ) const;
    
    //: Store meta data that is needed by frames
    //!param: ILwdFCS::FrameH& Storage - place to store values
    void Store( ILwdFCS::FrameH& Storage ) const;
    
  private:

    General::GPSTime m_end_time;
 };
} // Namespace datacondAPI

#endif // WHEN_UMD_HH
