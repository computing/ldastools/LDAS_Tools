//WaveletPSU.hh

#ifndef WAVELET_PSU_HH
#define WAVELET_PSU_HH

#include <general/unexpected_exception.hh>

#include "SequenceUDT.hh"

namespace datacondAPI{

  namespace psu
{

  class Wavelet : public datacondAPI::udt
  {
  public:

    //: Constructor
    Wavelet();

    //: Destructor
    virtual ~Wavelet();

    //: Accessor to highpass coefficients
    //!param: datacondAPI::Sequence<double> h - holds highpass filter coefficients
    virtual void getH(datacondAPI::Sequence<double>&) const;

    //: Accessor to lowpass coefficients
    //!param: datacondAPI::Sequence<double> l - holds lowpass filter coefficients
    virtual void getL(datacondAPI::Sequence<double>&) const;

    //: Accessor to highpass synthesis coefficients
    //!param: datacondAPI::Sequence<double> sh - holds highpass synthesis filter ccoefficients]
    virtual void get_sH(datacondAPI::Sequence<double>&) const;

    //: Accessor to lowpass synthesis coefficients
    //!param: datacondAPI::Sequence<double> sl - holds lowpass synthesis filter coefficients
    virtual void get_sL(datacondAPI::Sequence<double>&) const;
    
    
    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const; 
   
    //:duplicate *this on heap
    virtual datacondAPI::psu::Wavelet* Clone() const = 0;
    

  protected:
    //: Lowpass filter coefficients
    datacondAPI::Sequence<double> m_L;
    //: Highpass filter coefficients
    datacondAPI::Sequence<double> m_H;
    //: Lowpass synthesis filter coefficients
    datacondAPI::Sequence<double> m_sL;
    //: Highpass synthesis filter coefficients
    datacondAPI::Sequence<double> m_sH;

  }; //class Wavelet

} //namespace psu
} //namespace datacondAPI

#endif /* WAVELET_PSU_HH */
