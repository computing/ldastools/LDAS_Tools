// $Id: getElementFunction.cc,v 1.3 2005/12/01 22:54:58 emaros Exp $
//
// "getElement" action
//
// y = getElement(x, k);
//
// x: Sequence
// k: Scalar<int>

#include "datacondAPI/config.h"

#include <stdexcept>   
#include <memory>   
   
#include "general/Memory.hh"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"

#include "getElementFunction.hh"

using namespace std;   
   
using namespace datacondAPI;

namespace {

    getElementFunction static_getElementFunction;

}

const std::string& getElementFunction::GetName() const
{
  static const std::string name("getElement");

  return name;
}

getElementFunction::getElementFunction()
    : Function( getElementFunction::GetName() )
{
}

void getElementFunction::checkIndex(const size_t xSize, const int k) const
{
    if (k < 0)
    {
        throw invalid_argument("Illegal Argument: "
                               "action getElement(); "
                               "argument #1 index must be non-negative");
    }
    
    // avoids a compiler warning
    if (static_cast<size_t>(k) >= xSize)
    {
        throw invalid_argument("Illegal Argument: "
                               "action getElement(); "
                               "argument #1 index out of range");
    }
}

void getElementFunction::Eval(CallChain* chain,
                              const CallChain::Params& params,
                              const std::string& ret) const
{
    unique_ptr<udt> yUdt(0);

    if (params.size() == 2)
    {
        const udt* const xUdt = chain->GetSymbol(params[0]);
        const udt* const kUdt = chain->GetSymbol(params[1]);

        // Get k
        int k = 0;
        
        if (const Scalar<int>* const p
            = dynamic_cast<const Scalar<int>*>(kUdt))
        {
            k = p->GetValue();
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         0,
                                         TypeInfoTable.GetName(typeid(*kUdt)),
                                         "Scalar<int>");
        }

        // Ok - got k, now get the element

        if (const Sequence<float>* const p
            = dynamic_cast<const Sequence<float>*>(xUdt))
        {
            // :NOTE: float cast to double!
            checkIndex(p->size(), k);
            yUdt.reset(new Scalar<double>((*p)[k]));
        }
        else if (const Sequence<double>* const p
            = dynamic_cast<const Sequence<double>*>(xUdt))
        {
            checkIndex(p->size(), k);
            yUdt.reset(new Scalar<double>((*p)[k]));
        }
        else if (const Sequence<complex<float> >* const p
            = dynamic_cast<const Sequence<complex<float> >*>(xUdt))
        {
            // :NOTE: float cast to double!
            checkIndex(p->size(), k);
            yUdt.reset(new Scalar<complex<double> >((*p)[k]));
        }
        else if (const Sequence<complex<double> >* const p
            = dynamic_cast<const Sequence<complex<double> >*>(xUdt))
        {
            checkIndex(p->size(), k);
            yUdt.reset(new Scalar<complex<double> >((*p)[k]));
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         0,
                                         TypeInfoTable.GetName(typeid(*xUdt)),
                                         "object derived from Sequence<T>");
        }
    }
    else
    {
        throw CallChain::BadArgumentCount(GetName(), "2", params.size());
    }
    
    chain->ResetOrAddSymbol( ret, yUdt.release() );
}
