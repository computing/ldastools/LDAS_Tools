/* -*- mode: c++; c-basic-offset: 2; -*- */

// $Id: LinFilt.cc,v 1.37 2006/11/27 21:32:14 emaros Exp $

#include "datacondAPI/config.h"

#include <complex>

#include "LinFilt.hh"
#include "TimeSeries.hh"

#if __SUNPRO_CC
#pragma ident "$Id: LinFilt.cc,v 1.37 2006/11/27 21:32:14 emaros Exp $:"
#else
namespace 
{
  const char rcsID[] = "@(#) $Id: LinFilt.cc,v 1.37 2006/11/27 21:32:14 emaros Exp $:";
}
#endif

namespace datacondAPI {
  
  LinFilt::LinFilt(const LinFiltState& state)
    : m_state(state)
  {
  }
  
  LinFilt::LinFilt(const LinFilt& linfilt)
    : m_state(linfilt.m_state)
  {
  }

  LinFilt::LinFilt(const std::valarray<double>& b,
                   const std::valarray<double>& a)
    : m_state(b, a)
  {
  }
  
  LinFilt::~LinFilt()
  {
  }
  
  LinFilt& LinFilt::operator=(const LinFilt& linfilt)
  {
    if (this != &linfilt)
    {
      m_state = linfilt.m_state;
    }

    return *this;
  }
  
  LinFiltState LinFilt::getState()
  {
    return m_state;
  }

  void LinFilt::getState(LinFiltState& state)
  {
    state = m_state;
  }

  void LinFilt::getState(udt*& state)
  {
    if (state == 0)
    {
      state = new LinFiltState(m_state);
    }
    else if (udt::IsA<LinFiltState>(*state))
    {
      udt::Cast<LinFiltState>(*state) = m_state;
    }
    else
    {
      std::string what = "LinFilt::getState: arg not null or LinFiltState*";
      throw std::invalid_argument(what);
    }
  }

  void LinFilt::setState(const LinFiltState& state)
  {
    m_state = state;
  }

  void LinFilt::apply(udt*& out, const udt& in)
  {
    bool applied = asTimeSeries(out, in);

    if (!applied)
    {
      applied = asSequence(out, in);
    }

    if (!applied)
    {
      std::string what = "LinFilt::apply: apply unimplemented on type";
      throw General::unimplemented_error(what);
    }
    
    // :KLUDGE: Need a better way to set meta-data
    // Successful apply - set name, what should the name be for
    // a linfilted sequence??
    out->SetName("linfilt(" + in.name() + "," + m_state.name() + ")");
  }

  bool LinFilt::asTimeSeries(udt*& out, const udt& in)
  {
    bool ok = applyAs<TimeSeries<float> >(out, in);

    if (!ok) 
    {
      ok = applyAs<TimeSeries<double> >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<TimeSeries<std::complex<float> > >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<TimeSeries<std::complex<double> > >(out, in);
    }

    if (ok)
    {
      // process TimeSeries metadata
      // No special treatment is needed, just a straight copy from
      // input to output
      TimeSeriesMetaData& md = dynamic_cast<TimeSeriesMetaData&>(*out);
      md = dynamic_cast<const TimeSeriesMetaData&>(in);

      WhenMetaData& wmd = dynamic_cast<WhenMetaData&>(*out);
      wmd = dynamic_cast<const WhenMetaData&>(in);
    }

    return ok;
  }

  bool LinFilt::asSequence(udt*& out, const udt& in)
  {
    bool ok = applyAs<Sequence<float> >(out, in);

    if (!ok) 
    {
      ok = applyAs<Sequence<double> >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<float> > >(out, in);
    }

    if (!ok)
    {
      ok = applyAs<Sequence<std::complex<double> > >(out, in);
    }

    if (ok)
    {
      // process Sequence metadata

    }
    return ok;
  }

  template <class T>
  bool LinFilt::applyAs(udt*& out, const udt& in)
  {
    bool ok = false;
    bool null_out = ( out == 0 );

    try 
    {
      if (udt::IsA<T>(in))
      {
        if (null_out)
        {
          out = new T;
        }
        else if (!udt::IsA<T>(*out))
        {
          std::string what = "datacondAPI::LinFilt::applyAs<>: type out not compatible with type in";
          throw std::invalid_argument(what);
        }

        apply(udt::Cast<T>(*out), udt::Cast<T>(in));
        ok = true;
      }
      else
      {
        ok = false;
      }
    }
    catch(...)
    {
      if (null_out)
      {
        delete out;
        out =0;
      }

      throw;    
    }

    return ok;
  }

  template<class T>
  void LinFilt::apply(std::valarray<T>& x) 
  {
    m_state.apply(x);
  }
  
  template<class TOut, class TIn>
  void LinFilt::apply(std::valarray<TOut>& out, 
                      const std::valarray<TIn>& in) 
  {
    m_state.apply(out, in);
  }

#undef INSTANTIATE
#define INSTANTIATE(OUT_TYPE, IN_TYPE) \
template void datacondAPI::LinFilt::apply(std::valarray< OUT_TYPE >&, \
const std::valarray< IN_TYPE >&);

  INSTANTIATE(float, float)
  INSTANTIATE(double, double)
  INSTANTIATE(std::complex<float>, std::complex<float>)
  INSTANTIATE(std::complex<double>, std::complex<double>)

#undef INSTANTIATE

} // namespace datacondAPI
  
