// $Id: Wavelet.cc,v 1.7 2009/01/20 17:22:39 emaros Exp $

#define WAVELET_CC

#include "config.h"

#include <memory>

#include "general/Memory.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "Wavelet.hh"

namespace datacondAPI {
namespace wat {

// constructors

Wavelet::Wavelet(int mH, int mL, int tree, enum BORDER border) : 
   m_WaveType(HAAR)
{ 
   m_H = mH;
   m_L = mL;
   m_Border = border;
   m_Level = 0;
   m_TreeType = tree;
}

Wavelet::Wavelet(const Wavelet &w) 
  : udt( w )
{ 
   m_H = w.m_H;
   m_L = w.m_L;
   m_Border = w.m_Border;
   m_Level = w.m_Level;
   m_TreeType = w.m_TreeType;
   m_WaveType = w.m_WaveType;
}

// destructor
Wavelet::~Wavelet()
{ }

Wavelet* Wavelet::Clone() const
{
  return new Wavelet(*this);
}

ILwd::LdasElement* Wavelet::
ConvertToIlwd( const CallChain& Chain,
               datacondAPI::udt::target_type Target) const
{
   std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);

   container->push_back(new ILwd::LdasArray<int>
                        (int(m_WaveType), "WaveletType"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);

   container->push_back(new ILwd::LdasArray<int>
                        (m_TreeType, "WaveletTreeType"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (m_Level, "WaveletLevel"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (int(m_Border), "WaveletBorder"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN); 
   
   container->push_back(new ILwd::LdasArray<int>
                        (m_H, "WaveletHPFilterLength"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (m_L, "WaveletLPFilterLength"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   return container.release();
}

//*******************************
//*  wavedata acess functions   *
//*******************************

int Wavelet::getOffset(int level, int layer)
{       
   int n=0;

   for(int i=0; i<level; i++)
      if((layer>>i)&1) n += 1<<(level-1-i);

   return n;
}

int Wavelet::convertL2F(int level, int layer)
{       
   int n = layer;
   int j;
   for(int i=1; i<level; i++) {
      j = (1<<i) & (n);
      if(j) n = ((1<<i)-1) ^ (n);
   }
   
   return n;
}

int Wavelet::convertF2L(int level, int index)
{       
   int n = index;
   int j;
   for(int i=level-1; i>=1; i--) {
      j = ((1<<i) & (n));
      if(j) n = ((1<<i)-1) ^ (n);
   }
   return n;
}

} // namespace wat 
} // namespace datacondAPI
