/* -*- mode: c++ -*- */
#ifndef FILTFILT_FUNCTION_HH
#define FILTFILT_FUNCTION_HH

#include "CallChain.hh"

namespace datacondAPI {
  template <class T> class Sequence;
}

//-----------------------------------------------------------------------------
//: Function support for FiltFilt action
//
// This class implements the "filtfilt" function to be used in the action
// section of a TCL command. The syntax, as it would appear in the action
// sequence is:
//
// [y = ]filtfilt(b, x)
// [y = ]filtfilt(b, a, x)
//
// where y, x are valarray<T>, b, a are valarray<T>
//
class FiltFiltFunction : public CallChain::Function {
    
public:
    
    //-------------------------------------------------------------------------
    //: Constructor
    //
    // Construct a new FiltFiltFunction
    FiltFiltFunction();
    
    //-------------------------------------------------------------------------
    //: Evaluate the FiltFilt Function
    //
    // This performs the mixing. The list of parameters must contain doubles
    // phase and carrier and a valarray of input data.
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    //
    virtual void Eval(CallChain* Chain,
        const CallChain::Params& Params,
        const std::string& Ret) const;
    
    //-------------------------------------------------------------------------
    //: Return the Name of the function ("filtfilt")
    //
    //!return: const std::string& ptName - Returns a reference to the name of
    //!return: the function
    virtual const std::string& GetName() const;
    
private:
    
  template<class TIn>
  void dispatch1(const datacondAPI::udt* const bUdt,
                 const datacondAPI::udt* const aUdt,
                 datacondAPI::Sequence<TIn>& y) const;

  template<class TIn, class TCoeffs>
  void dispatch2(const datacondAPI::Sequence<TCoeffs>& b,
                 const datacondAPI::udt* const aUdt,
                 datacondAPI::Sequence<TIn>& y) const;

};

#endif // FILTFILT_FUNCTION_HH
