/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include "FrequencyUMD.hh"

#include "general/types.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

#include "ilwdfcs/FrProcData.hh"

double datacondAPI::FrequencyMetaData::GetFrequency(unsigned int i) const
{
    if ( i > 0 )
    {
	 return ( m_f0 + (m_df * i) );
    }
    return m_f0;
}

double datacondAPI::FrequencyMetaData::GetFrequencyDelta() const
{
    return m_df;
}

void datacondAPI::FrequencyMetaData::SetFrequencyBase(const double& f0)
{
    m_f0 = f0;
}

void datacondAPI::FrequencyMetaData::SetFrequencyDelta(const double& df)
{
    m_df = df;
}

datacondAPI::FrequencyMetaData::~FrequencyMetaData()
{
}

ILwd::LdasElement* datacondAPI::FrequencyMetaData::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    try {
	switch ( Target )
	{
	case datacondAPI::udt::TARGET_GENERIC:
	    container->push_back( new ILwd::LdasArray<double>(m_f0,
							     "FrequencyBase"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    container->push_back( new ILwd::LdasArray<double>(m_df,
							      "FrequencyDelta"),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    break;
	default:
	    throw datacondAPI::udt::
		BadTargetConversion(Target,
				    "datacondAPI::FrequencyUMD");
	}
    }
    catch ( ... )
    {
	delete container;
	throw;
    }

    return container;
}

void datacondAPI::FrequencyMetaData::
store( ILwd::LdasContainer& Storage,
       datacondAPI::udt::target_type Target,
       unsigned int Size ) const
{
    switch( Target )
    {
    case datacondAPI::udt::TARGET_WRAPPER:
	Storage.push_back( new ILwd::LdasArray<REAL_8>
			   (GetFrequency(0), "start_freq", "hz"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
	Storage.push_back( new ILwd::LdasArray<REAL_8>
			   (GetFrequency( Size - 1 ),
			    "stop_freq", "hz"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
	Storage.push_back( new ILwd::LdasArray<REAL_8>
			   ( GetFrequencyDelta(), "freq:step_size",
			     "hz"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
	break;
    default:
	// Do nothing
	break;
    }
}
    
datacondAPI::FrequencyMetaData::FrequencyMetaData()
    : m_f0(0), m_df(0)
{
}

datacondAPI::FrequencyMetaData::FrequencyMetaData(const double& f0, const double& df)
    : m_f0(f0), m_df(df)
{
}

datacondAPI::FrequencyMetaData::FrequencyMetaData(const FrequencyMetaData& in)
    : m_f0(in.m_f0), m_df(in.m_df)
{
}

