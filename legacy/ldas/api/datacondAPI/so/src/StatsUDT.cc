const char rcsID[] = "$Id: StatsUDT.cc,v 1.24 2009/05/20 00:14:58 emaros Exp $";

#include "config.h"

#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <dbaccess/FieldString.hh>
#include <dbaccess/Table.hh>
#include <dbaccess/Transaction.hh>
#include <dbaccess/Process.hh>
#include <dbaccess/SummStatistics.hh>

#include "StatsUDT.hh"
#include "CallChain.hh"

// destructor
datacondAPI::Stats::~Stats()
{
}

// default constructor
datacondAPI::Stats::Stats()
  : Sequence<double>(Stats::MOMENTS_MAX),
    StatsMetaData(0, 0.0, 0.0, 0.0)
{
}

// constructor with arguments
datacondAPI::Stats::Stats(const unsigned int&          size, 
			  const double&                min, 
			  const double&                max,
			  const double&                rms,
			  const std::valarray<double>& moments)
  : Sequence<double>(moments),
    StatsMetaData(size, min, max, rms)
{
    if (moments.size() != Stats::MOMENTS_MAX)
    {
	throw std::invalid_argument("Stats::Stats() "
				 "size of moments array must be MOMENTS_MAX");
    }
}

// UDT clone method
datacondAPI::Stats*
datacondAPI::Stats::Clone() const
{
  return new Stats(*this);
}

// UDT convert to ilwd method
ILwd::LdasElement* datacondAPI::Stats::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  ILwd::LdasContainer* container = new ILwd::LdasContainer("Statistics");
  try {
    switch( Target )
    {
    case datacondAPI::udt::TARGET_METADATA:
    case datacondAPI::udt::TARGET_METADATA_FINAL_RESULT:
      {
	CallChain&		chain( const_cast<CallChain&>( Chain ) );
	DB::Table*		stats =
	  chain.FindOrCreateTable( DB::SummStatistics::TABLE_NAME );
	DB::Table*		process =
	  chain.FindOrCreateTable( DB::Process::TABLE_NAME );
	stats->Associate(stats->GetRowCount(), *process, 0);
	std::string program(process->GetFieldString
			    (process->HasField(DB::Process::PROGRAM))
			    .operator[](0));

	stats->AppendColumnEntry(DB::SummStatistics::PROGRAM, program);
	stats->AppendColumnEntry(DB::SummStatistics::START_TIME_SECONDS,
				 (INT_4S)(GetStartTime().GetSeconds()));
	stats->AppendColumnEntry(DB::SummStatistics::START_TIME_NANOSECONDS,
				 (INT_4S)(GetStartTime().GetNanoseconds()));
	stats->AppendColumnEntry(DB::SummStatistics::END_TIME_SECONDS,
				 (INT_4S)(GetEndTime().GetSeconds()));
	stats->AppendColumnEntry(DB::SummStatistics::END_TIME_NANOSECONDS,
				 (INT_4S)(GetEndTime().GetNanoseconds()));
	stats->AppendColumnEntry(DB::SummStatistics::SAMPLES,
				 (INT_4S)GetSize());
	stats->AppendColumnEntry(DB::SummStatistics::CHANNEL,
				 GetStatisticsOfName());
	stats->AppendColumnEntry(DB::SummStatistics::MIN,
				 GetMin());
	stats->AppendColumnEntry(DB::SummStatistics::MAX,
				 GetMax());
	stats->AppendColumnEntry(DB::SummStatistics::MEAN,
				 (*this)[Stats::MEAN]);
	stats->AppendColumnEntry(DB::SummStatistics::RMS,
				 GetRMS());
	stats->AppendColumnEntry(DB::SummStatistics::VARIANCE,
				 (*this)[Stats::VARIANCE]);
	stats->AppendColumnEntry(DB::SummStatistics::SKEWNESS,
				 (*this)[Stats::SKEWNESS]);
	stats->AppendColumnEntry(DB::SummStatistics::KURTOSIS,
				 (*this)[Stats::KURTOSIS]);
    
	chain.FormatTransaction( Target, container );
	break;
      }
    case datacondAPI::udt::TARGET_GENERIC:
      container->push_back( Sequence<double>::ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( StatsMetaData::ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( ArbitraryWhenMetaData::ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      break;
    default:
      throw datacondAPI::udt::BadTargetConversion(Target,
						  "datacondAPI::Stats");
    }
  }
  catch ( ... )
  {
    delete container;
    throw;
  }

  return container;
}

#if NEEDED
// UDT class instantiation

#define CLASS_INSTANTIATION \
class datacondAPI::Stats; \
UDT_CLASS_INSTANTIATION(Stats,)

CLASS_INSTANTIATION
#else
UDT_CLASS_INSTANTIATION_CAST(Stats)
#endif /* NEEDED */
