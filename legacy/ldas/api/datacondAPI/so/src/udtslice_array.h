/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef UDTSLICEARRAY_HH
#define UDTSLICEARRAY_HH

// $Id: udtslice_array.h,v 1.7 2005/09/15 23:55:25 emaros Exp $

#include <valarray>

namespace datacondAPI {

  //: provide slice operations on VectorSequence.
  //+ A udt_slice_array holds a reference to a an std::valarray and a copy
  //+ of a slice object
  template<typename Tp>
  class udt_slice_array
  {
  public:
    typedef Tp value_type;

    //: Return a copy of the data indicated by the slice
    operator std::valarray<Tp>() const;

    //: Assign to slice
    //!param: std::valarray<Tp>& - data to assign from
    void operator=   (const std::valarray<Tp>&);

    //: Multiply current slice element by element
    //!param: const std::valarray<Tp>& - data to multiply by
    void operator*=  (const std::valarray<Tp>&) const;

    //: divide current slice element by element
    //!param: const std::valarray<Tp>& - data to divide by
    void operator/=  (const std::valarray<Tp>&) const;

    //: Integer divide current slice element by element
    //!param: const std::valarray<Tp>& - data to divide by. 
    void operator%=  (const std::valarray<Tp>&) const;

    //: Add to current slice element by element
    //!param: const std::valarray<Tp>& - data to add
    void operator+=  (const std::valarray<Tp>&) const;

    //: Subtract from current slice element by element
    //!param: const std::valarray<Tp>& - data to subtract
    void operator-=  (const std::valarray<Tp>&) const;

    //: Exclusive-or with current slice element by element
    //!param: const std::valarray<Tp>& - data to exclusive-or with
    void operator^=  (const std::valarray<Tp>&) const;

    //: And with current slice element by element
    //!param: const std::valarray<Tp>& - data to and with
    void operator&=  (const std::valarray<Tp>&) const;

    //: Or with current slice element by element
    //!param: const std::valarray<Tp>& - data to or by
    void operator|=  (const std::valarray<Tp>&) const;

    //: left-shift data in current slice element by element
    //!param: const std::valarray<Tp>& - amount to shift by
    void operator<<= (const std::valarray<Tp>&) const;

    //: Right-shift data in current slice element by element
    //!param: const std::valarray<Tp>& - amount to shift by
    void operator>>= (const std::valarray<Tp>&) const;

    //: Assign to every element in slice
    //!param: const Tp - data to assign 
    void operator= (const Tp &);
    
  protected:
    // udt and descendants have access to these protected methods 

    //: VectorSequence<Tp> granted friendship
    friend class VectorSequence<Tp>;
    
    //: Protected constructor from valarray<Tp> and slice
    //!param: std::valarray<Tp>& - valarray to construct from
    //!param: std::slice& - slice of std::valarray we are interested in
    udt_slice_array(std::valarray<Tp>&, const std::slice&);

    //: Copy constructor
    //!param: const udt_slice_array& - object to copy from
    udt_slice_array (const udt_slice_array&);
    
    //: Default constrctor not implemented
    udt_slice_array ();
    
    //: Assignment operator not implemented
    udt_slice_array& operator= (const udt_slice_array&);
    
    //: slice of interest
    const std::slice m_slice;

    //: reference to data of interest
    std::valarray<Tp>& m_data;
    
  };

  template<typename Tp>
  inline udt_slice_array<Tp>::
  operator std::valarray<Tp>() const 
  { return static_cast<const std::valarray<Tp> >(m_data)[m_slice]; }
  
  template<typename Tp>
  inline udt_slice_array<Tp>::
  udt_slice_array (std::valarray<Tp>& v, const std::slice& s)
    : m_slice(s), m_data(v) {}
  
  template<typename Tp>
  inline udt_slice_array<Tp>::
  udt_slice_array(const udt_slice_array<Tp>& usa)
    : m_slice(usa.m_slice), m_data(usa.m_data) {}
  
  template<typename Tp>
  inline void
  udt_slice_array<Tp>::operator= (const Tp& v) 
  { m_data[m_slice] = v; }
  
  template<typename Tp>
  inline void
  udt_slice_array<Tp>::operator= (const std::valarray<Tp>& v)
  { m_data[m_slice] = v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator*= (const std::valarray<Tp>& v) const
  { m_data[m_slice] *= v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator/= (const std::valarray<Tp>& v) const
  { m_data[m_slice] /= v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator%= (const std::valarray<Tp>& v) const
  { m_data[m_slice] %= v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator+= (const std::valarray<Tp>& v) const
  { m_data[m_slice] += v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator-= (const std::valarray<Tp>& v) const
  { m_data[m_slice] -= v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator^= (const std::valarray<Tp>& v) const
  { m_data[m_slice] ^= v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator&= (const std::valarray<Tp>& v) const
  { m_data[m_slice] &= v; }
  
  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator|= (const std::valarray<Tp>& v) const
  { m_data[m_slice] |= v; }

  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator<<= (const std::valarray<Tp>& v) const
  { m_data[m_slice] <<= v; }

  template<typename Tp>
  inline void		
  udt_slice_array<Tp>::operator>>= (const std::valarray<Tp>& v) const
  { m_data[m_slice] >>= v; }

}

#endif // UDTSLICEARRAY_HH

// Local Variables:
// mode:c++
// End:
