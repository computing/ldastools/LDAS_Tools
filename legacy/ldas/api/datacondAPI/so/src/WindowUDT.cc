#include "config.h"

#include <complex>

#include <general/unimplemented_error.hh>

#include "WindowUDT.hh"
#include "SequenceUDT.hh"

using std::complex;   
   
namespace datacondAPI {

    //
    // WindowUDT
    //

    WindowUDT::~WindowUDT()
    {
    }

    size_t WindowUDT::size() const
    {
        return getWindow().size();
    }

    double WindowUDT::mean() const
    {
        return getWindow().mean();
    }
        
    double WindowUDT::rms() const
    {
        return getWindow().rms();
    }

    WindowInfo WindowUDT::what() const
    {
        const Filters::Window& win = getWindow();

        return WindowInfo(win.name(), win.param());
    }

    void WindowUDT::resize(const size_t n)
    {
        getWindow().resize(n);
    }

    template<class T>
    void WindowUDT::apply(Sequence<T>& x)
    {
        getWindow().apply(x);
    }

    template<class TOut, class TIn>
    void WindowUDT::apply(Sequence<TOut>& out, const Sequence<TIn>& in)
    {
        getWindow().apply(out, in);
    }

    ILwd::LdasElement*
    WindowUDT::ConvertToIlwd(const CallChain& Chain,
                             udt::target_type Target) const
    {
        throw General::unimplemented_error("Window::ConvertToIlwd() - "
                                           "unimplemented");
        return 0;
    }
    
    template<class TOut, class TIn>
    void WindowUDT::operator()(Sequence<TOut>& out, const Sequence<TIn>& in)
    {
        getWindow().apply(out, in);
    }

    //
    // HannWindowUDT
    //

    HannWindowUDT* HannWindowUDT::Clone() const
    {
        return new HannWindowUDT(*this);
    }


    const Filters::HannWindow& HannWindowUDT::getWindow() const
    {
        return m_window;
    }

    Filters::HannWindow& HannWindowUDT::getWindow()
    {
        return m_window;
    }

    //
    // KaiserWindowUDT
    //

    KaiserWindowUDT::KaiserWindowUDT()
        : m_window()
    {
    }

    KaiserWindowUDT::KaiserWindowUDT(const double beta)
        : m_window(beta)
    {
    }
    
    KaiserWindowUDT* KaiserWindowUDT::Clone() const
    {
        return new KaiserWindowUDT(*this);
    }

    double KaiserWindowUDT::beta() const
    {
        return m_window.beta();
    }

    void KaiserWindowUDT::set_beta(const double& beta)
    {
        return m_window.set_beta(beta);
    }

    const Filters::KaiserWindow& KaiserWindowUDT::getWindow() const
    {
        return m_window;
    }

    Filters::KaiserWindow& KaiserWindowUDT::getWindow()
    {
        return m_window;
    }

    //
    // RectangularWindowUDT
    //

    RectangularWindowUDT* RectangularWindowUDT::Clone() const
    {
        return new RectangularWindowUDT(*this);
    }


    const Filters::RectangularWindow& RectangularWindowUDT::getWindow() const
    {
        return m_window;
    }

    Filters::RectangularWindow& RectangularWindowUDT::getWindow()
    {
        return m_window;
    }

    template void WindowUDT::apply(Sequence<float>& x);
    template void WindowUDT::apply(Sequence<double>& x);
    template void WindowUDT::apply(Sequence<complex<float> >& x);
    template void WindowUDT::apply(Sequence<complex<double> >& x);

    template void WindowUDT::apply(Sequence<float>& out,
                                   const Sequence<float>& in);
    template void WindowUDT::apply(Sequence<double>& out,
                                   const Sequence<double>& in);
    template void WindowUDT::apply(Sequence<complex<float> >& out,
                                   const Sequence<complex<float> >& in);
    template void WindowUDT::apply(Sequence<complex<double> >& out,
                                   const Sequence<complex<double> >& in);

    template void WindowUDT::operator()(Sequence<float>& out,
                                        const Sequence<float>& in);
    template void WindowUDT::operator()(Sequence<double>& out,
                                        const Sequence<double>& in);
    template void WindowUDT::operator()(Sequence<complex<float> >& out,
                                        const Sequence<complex<float> >& in);
    template void WindowUDT::operator()(Sequence<complex<double> >& out,
                                        const Sequence<complex<double> >& in);

} // namespace datacondAPI
