/* -*- mode: c++; c-basic-offset: 2; -*- */
//wavelet.cc

#include "config.h"

#include <general/toss.hh>
#include "WaveletPSU.hh"

namespace
{
  const char rcsID[] = "@(#) $Id: WaveletPSU.cc,v 1.1 2009/01/13 23:25:50 emaros Exp $:";
}

namespace datacondAPI {

namespace psu
{

  Wavelet::Wavelet ()
  {
  }

  Wavelet::~Wavelet ()
  {
  }

  void Wavelet::
  getH(datacondAPI::Sequence<double>& h) const
  {
    h = m_H;
  }

  void Wavelet::
  getL(datacondAPI::Sequence<double>& l) const
  {
    l = m_L;
  }
  void Wavelet::
  get_sH(datacondAPI::Sequence<double>& sh) const
  {
    sh.resize(m_sH.size());
    sh = m_sH;
  }

  void Wavelet::
  get_sL(datacondAPI::Sequence<double>& sl) const
  {
    sl.resize(m_sL.size());
    sl = m_sL;
  }

  ILwd::LdasElement* 
  Wavelet::
  ConvertToIlwd( const CallChain& Chain, 
		 datacondAPI::udt::target_type Target) const
  {
    std::string what = "tried to call ConvertToIlwd";
    General::toss<std::domain_error>("Wavelet", __FILE__, __LINE__, what);
    return 0; 
  }

} //namespace psu
 } //namespace datacondAPI

UDT_CLASS_INSTANTIATION(psu::Wavelet,UDT_DEFAULT_KEY)

