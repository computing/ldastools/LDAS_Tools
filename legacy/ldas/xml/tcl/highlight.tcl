#!/bin/sh
# -*- tcl -*- \
exec wish8.1 "$0" "$@"

# highlight.tcl --
#
#	Reads an XMl document and displays it in a Text widget
#	with simple syntax highlighting.
#
# Copyright (c) 1999 Zveno Pty Ltd
# http://www.zveno.com/
#
# Zveno makes this software available free of charge for any purpose.
# Copies may be made of this software but all of this notice must be included
# on any copy.
#
# The software was developed for research purposes only and Zveno does not
# warrant that it is error free or fit for any purpose.  Zveno disclaims any
# liability for all claims, expenses, losses, damages and costs any user may
# incur as a result of using, copying or modifying this software.
#
# $Id: highlight.tcl,v 1.1 1999/05/17 23:27:44 install Exp $

lappend auto_path [file dirname [file dirname [info script]]]

# We need an XML parser
package require xml

###	These procedures are the real guts of the application.
###	They are the callbacks for the XML parser to notify the
###	application of "document events".

# ElementStart --
#
#	This procedure is called when the parser encounters
#	the start tag of an element.
#
# Arguments:
#	w	Text widget
#	name	tag name
#	attList	attribute list, in name/value pairs
#	args	additional switches, such as whether this is an empty element
#
# Results:
#	Start tag displayed in Text widget with appropriate highlighting

proc ElementStart {w name attList args} {
    array set options {-empty 0}
    array set options $args

    $w insert end < markup $name tagname
    foreach {attName value} $attList {
	$w insert end " "
	$w insert end $attName attName = markup

	# Choose an appropriate delimiter for the attribute value
	if {[regexp \" $value]} {
	    if {[regexp ' $value]} {
		# The attribute value contains both ' and " so we'll
		# have to quote the "s
		regsub -all \" $value {\&quot;} value
		set delimiter \"
	    } else {
		set delimiter '
	    }
	} else {
	    set delimiter \"
	}

	$w insert end $delimiter markup $value attValue $delimiter markup
    }
    if {$options(-empty)} {
	$w insert end / markup
    }
    $w insert end > markup

    return {}
}

# ElementEnd --
#
#	This procedure is called when the parser encounters
#	the end tag of an element.
#
# Arguments:
#	w	Text widget
#	name	tag name
#	args	additional switches, such as whether this is an empty element
#
# Results:
#	End tag displayed in Text widget with appropriate highlighting

proc ElementEnd {w name args} {
    array set options {-empty 0}
    array set options $args

    if {$options(-empty)} {

	# The start tag contains the end marker, so there's no work to do here

	return {}
    }

    $w insert end </ markup $name tagname > markup

    return {}
}

# CharacterData --
#
#	This procedure is called when the parser encounters
#	character data.
#
# Arguments:
#	w	Text widget
#	data	character data
#
# Results:
#	Text is displayed in Text widget

proc CharacterData {w data} {
    $w insert end $data
}

###	These procedures are used to manage the user interface

# Open --
#
#	Opens a file and parses it.
#
# Arguments:
#	w	Text widget for display
#	fname	filename to open
#
# Results:
#	Data is read from file, parsed and displayed

proc Open {w {fname {}}} {
    global currentFile

    if {$fname == ""} {
	set fname [tk_getOpenFile -parent . -title "Open XML Document"]
    }

    # Check if user cancelled open operation
    if {$fname == ""} {
	return {}
    }

    # Get the document data, ensuring that UTF-8 characters are read
    set ch [open $fname]
    fconfigure $ch -encoding utf-8
    set data [read $ch]
    close $ch

    # Setup a parser with the required callbacks
    set parser [::xml::parser]
    $parser configure -elementstartcommand [list ElementStart $w] \
	    -elementendcommand [list ElementEnd $w] \
	    -characterdatacommand [list CharacterData $w] \
	    -reportempty 1

    # Prepare the Text widget for the new document
    $w delete 1.0 end
    wm title [winfo toplevel $w] "$fname - [file tail [info script]]"

    # Now let 'er rip
    # If the document is not well-formed the parser will throw an error
    if {[catch {$parser parse $data} err]} {

	$w delete 1.0 end
	wm title [winfo toplevel $w] [file tail [info script]]
	set currentFile {}
	update idletasks

	tk_messageBox -parent . -type ok -title "Error" \
		-message "Unable to display \"$fname\" due to \"$err\"" \
		-icon error

    }

    set currentFile $fname

    return {}
}

# Highlight --
#
#	Change the highlighting options.
#
# Arguments:
#	w	Text widget for display
#
# Results:
#	Text widget tags may be reconfigured.

proc Highlight {w} {
    catch {destroy .highlight}
    set f [toplevel .highlight]
    wm title $f "Highlight Options"

    foreach {tag description} {
	markup {Markup syntax}
	tagname {Tag names}
	attName {Attribute names}
	attValue {Attribute values}
    } {

	label $f.tag$tag -text $description

	set ::Foreground$tag [$w tag cget $tag -foreground]
	catch {unset options}
	array set options [$w tag cget $tag -font]

	# font
	# size
	# bold
	set ::Bold$tag [info exists options(-weight)]
	checkbutton $f.bold$tag -text Bold -variable Bold$tag
	# italic
	set ::Italic$tag [info exists options(-slant)]
	checkbutton $f.italic$tag -text Italic -variable Italic$tag
	# colour
	label $f.fglabel$tag -text Colour:
	button $f.fg$tag -text Choose -command [list ChooseForeground $f.fg$tag ::Foreground$tag]
	if {[string length [set ::Foreground$tag]]} {
	    $f.fg$tag configure -foreground [set ::Foreground$tag]
	}

	grid $f.tag$tag $f.bold$tag $f.italic$tag $f.fglabel$tag $f.fg$tag
    }

    button $f.ok -text OK -command [list ApplyHighlight $w $f]
    button $f.cancel -text Cancel -command [list destroy $f]
    grid $f.ok $f.cancel

    return {}
}

# ApplyHighlight --
#
#	Apply the highlighting options.
#
# Arguments:
#	w	Text widget for display
#	f	option dialog window
#
# Results:
#	Text widget tags may be reconfigured.

proc ApplyHighlight {w f} {
    global currentFile

    destroy $f

    foreach tag {markup tagname attName attValue} {
	if {[string length [set ::Foreground$tag]]} {
	    $w tag configure $tag -foreground [set ::Foreground$tag]
	}
	set font {}
	if {[set ::Italic$tag]} {
	    append font { -slant italic}
	}
	if {[set ::Bold$tag]} {
	    append font { -weight bold}
	}
	$w tag configure $tag -font [string trim $font]
    }

    return {}
}

# ChooseForeground --
#
#	Choose colour for highlighting.
#
# Arguments:
#	t	Button widget displaying highlight option
#	v	variable for choice
#
# Results:
#	Variable is set to chosen value

proc ChooseForeground {t v} {
    upvar #0 $v val

    set opt {}
    if {[string length $val]} {
	set opt [list -initialcolor $val]
    }
    set newValue [eval [list tk_chooseColor -parent [winfo toplevel $t] -title {Choose Highlight Colour}] $opt]

    if {[string length $newValue]} {
	set val $newValue
	$t configure -foreground $val
    }

    return {}
}

###	This is the main application script

wm title . [file tail [info script]]

set w [text .display \
	-xscrollcommand [list .xscroll set] \
	-yscrollcommand [list .yscroll set]]
scrollbar .yscroll -command [list .display yview] -orient vertical
scrollbar .xscroll -command [list .display xview] -orient horizontal

grid .display -row 0 -column 0 -sticky news
grid .xscroll -row 1 -column 0 -sticky ew
grid .yscroll -row 0 -column 1 -sticky ns
grid rowconfigure . 0 -weight 1
grid columnconfigure . 0 -weight 1

. configure -menu .menu
menu .menu -tearoff 0
.menu add cascade -label File -menu .menu.file
.menu add cascade -label View -menu .menu.view

menu .menu.file -tearoff 0
.menu.file add command -label Open... -command [list Open $w]
.menu.file add separator
.menu.file add command -label Quit -command {destroy .}

menu .menu.view -tearoff 0
.menu.view add command -label {Highlighting...} -command [list Highlight $w]

# Set initial highlight properties
$w tag configure markup -font {-weight bold}
$w tag configure tagname -font {-weight bold} -foreground #008080
$w tag configure attName -font {-weight bold -slant italic} -foreground #804000
$w tag configure attValue -font {-slant italic}

if {$argc > 0} {
    Open $w [lindex $argv 0]
}
