#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#

AC_DEFUN([AX_LDAS_PACKAGE_PYTHON],
[
  AC_ARG_ENABLE([python],
	         AC_HELP_STRING([--enable-python],
                    	        [use python (default is CHECK)]),
	         [ case "${enable_python}" in
	           no)
		     ;;
                   *)
		     ;;
                   esac ],
	         [enable_python="CHECK";] )
  case x${enable_python} in
  xno)
    dnl -----------------------------------------------------------------
    dnl  Setup for AM_CONDITIONAL statements since python was disabled
    dnl -----------------------------------------------------------------
    ldas_have_python_package_ctypes="no"
    ldas_have_python_package_numpy="no"
    PYTHON=":"
    HAVE_PYTHON_H=""
    ;;
  *)
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Look for the basics
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AM_PATH_PYTHON([$1],,[:])
    if test "${PYTHON}" != ":"
    then
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  See if the version of python is recent enough to
      dnl    support python-config
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_PATH_PROG([PYTHON_CONFIG],[python${PYTHON_VERSION}-config python-config])
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Figure out the include directive for python
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for python includes)
      case x$PYTHON_CONFIG in
      x)
	dnl -------------------------------------------------------------
	dnl Get the information the hard way
	dnl -------------------------------------------------------------
        PYTHON_INCLUDES="-I`echo ${PYTHON} | \
	  sed 's,/python$,,g' | \
	  sed 's,/amd64,64,g' | \
	  sed 's,/64,64,g' | \
	  sed 's,/bin\([[^/]]*\),/include\1,g'`/python$PYTHON_VERSION";
	;;
      *)
	dnl -------------------------------------------------------------
	dnl Newer version of python have the python-config script
	dnl -------------------------------------------------------------
    	PYTHON_INCLUDES="`$PYTHON_CONFIG --includes`"
	;;
      esac
      AC_SUBST(PYTHON_INCLUDES)
      AC_MSG_RESULT(${PYTHON_INCLUDES})
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl Checking for Python EXEC_PREFIX
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for python EXEC_PREFIX directory)
      PYTHON_SITE_PACKAGES_DIR="`${PYTHON} -c 'from distutils.sysconfig import EXEC_PREFIX; print(EXEC_PREFIX)'`" 2>/dev/null
      AC_SUBST(PYTHON_SITE_PACKAGES_DIR)
      AC_MSG_RESULT(${PYTHON_SITE_PACKAGES_DIR})
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl Checking for Python module directory
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for python module directory)
      PYTHON_SITE_PACKAGES_DIR="`${PYTHON} -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib(1))'`" 2>/dev/null
      AC_SUBST(PYTHON_SITE_PACKAGES_DIR)
      AC_MSG_RESULT(${PYTHON_SITE_PACKAGES_DIR})
      AC_MSG_CHECKING([if installing python modules into system directory])
      AS_IF([test "${PYTHON_SITE_PACKAGES_DIR}" != "${pyexecdir}"],[
        AX_LIBDIRS='LIBDIRS=${libdir}'
	AX_PYTHONPATH='PYTHONPATH=${LIBDIRS}:'"${pyexecdir}/LDAStools"
	AC_MSG_RESULT([no])
	],[
	AC_MSG_RESULT([yes])])
      AC_SUBST(AX_LIBDIRS)
      AC_SUBST(AX_PYTHONPATH)
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Checking for Python header files
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Python.h
      dnl ---------------------------------------------------------------
      old_CPPFLAGS="${CPPFLAGS}"
      CPPFLAGS="${PYTHON_INCLUDES} ${CPPFLAGS}"
      AC_CHECK_HEADERS([Python.h],[HAVE_PYTHON_H="yes"])
      CPPFLAGS="${old_CPPFLAGS}"
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Checking for Python packages
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Python Package - ctypes
      dnl ---------------------------------------------------------------
      AC_MSG_CHECKING(for python package ctypes)
      if test -z "`${PYTHON} -c 'import ctypes' 2>&1`"
      then
        ldas_have_python_package_ctypes="yes"
      else
        ldas_have_python_package_ctypes="no"
      fi
      AC_MSG_RESULT($ldas_have_python_package_ctypes)
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      dnl  Python Package - numpy
      dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      AC_MSG_CHECKING(for pythong package numpy)
      if test -z "`${PYTHON} -c 'import numpy' 2>&1`"
      then
        ldas_have_python_package_numpy="yes"
        AC_MSG_RESULT($ldas_have_python_package_numpy)
        ldas_cppflags=${CPPFLAGS}
        PYTHON_NUMPY_INCLUDE="`${PYTHON} -c 'import numpy; print numpy.get_include()' 2>/dev/null | sed -e 's/^[[[].\(.*\).[]]]\$/\\1/g'`"
        if test x${PYTHON_NUMPY_INCLUDE} != x
        then
          PYTHON_NUMPY_INCLUDE="-I${PYTHON_NUMPY_INCLUDE}"
        fi
        CPPFLAGS="${PYTHON_NUMPY_INCLUDE} ${PYTHON_INCLUDES} $CPPFLAGS"
        AC_CHECK_HEADERS(numpy/arrayobject.h,[],[],
  	[#include <Python.h>
        ])
        CPPFLAGS=${ldas_cppflags}
        AC_SUBST(PYTHON_NUMPY_INCLUDE)
      else
        ldas_have_python_package_numpy="no"
        AC_MSG_RESULT($ldas_have_python_package_numpy)
      fi
    fi
    ;;
  esac
  dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  dnl  Checking for Python packages - end
  dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  AM_CONDITIONAL([HAVE_PYTHON_PACKAGE_CTYPES],dnl
                 [test "${ldas_have_python_package_ctypes}" = "yes"])
  AM_CONDITIONAL([HAVE_PYTHON_PACKAGE_NUMPY],dnl
                 [test "${ldas_have_python_package_numpy}" = "yes"])
  AM_CONDITIONAL([HAVE_PYTHON],[test "${PYTHON}" != ":" -a "${HAVE_PYTHON_H}" != ""])
])
