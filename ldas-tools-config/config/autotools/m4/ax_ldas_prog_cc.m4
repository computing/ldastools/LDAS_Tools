dnl======================================================================
dnl AX_LDAS_PROG_CC
dnl   Check on how to setup cc
dnl Depends:
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#



AC_DEFUN([AX_LDAS_PROG_CC],
[ AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AX_LDAS_ARG_ENABLE_WARNINGS_AS_ERRORS])
  if test x${ac_cv_prog_cc_g} = xyes
  then
    dnl------------------------------------------------------------------
    dnl PR3112: Put in -g (debug) option if supported by compiler
    dnl------------------------------------------------------------------
    CFLAGS="-g $CFLAGS"
  fi
  if test "${GCC}" = "yes"
  then
    touch ,t.c
    ac_ldas_default_cc_includes="`${CC} -v ,t.c 2>&1 | \
  	sed -n -e '/\#include/,/End of search list\./p' | \
  	egrep '^ '`"
    rm ,t.c
  fi
  #=======================================================================
  # Determine compiler vendor
  #=======================================================================
  AC_LANG_PUSH([C])
  LDAS_CC_FLAVOR="generic"
  ldas_prog_cc_vendor=generic
  if test $ldas_prog_cc_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if GNU flavor
    dnl -----------------------------------------------------------------
    if test "${GCC}" = yes
    then
      ldas_prog_cc_vendor="GNU"
      LDAS_CC_FLAVOR=gnu
    fi
  fi
  if test $ldas_prog_cc_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if SunPRO flavor
    dnl -----------------------------------------------------------------
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   #if __SUNPRO_C
                   exit( 0 );
                   #endif /* __SUNPRO_C */
	           exit( 1 );
                 }
               ],[
                ldas_prog_cc_vendor=Sun
        	LDAS_CC_FLAVOR="SunPRO"
               ])
  fi
  AC_LANG_POP([C])
  AM_CONDITIONAL([LDAS_CC_SUNPRO],[test x$LDAS_CC_FLAVOR = xSunPRO])
  dnl -------------------------------------------------------------------
  dnl  setting up of compiler flags
  dnl -------------------------------------------------------------------
  AC_MSG_CHECKING(C vendor)
  AC_MSG_RESULT($ldas_prog_cc_vendor)
  case x$ldas_prog_cc_vendor in
  xGNU) dnl ######## G N U ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cc_optimization_key="-O"
    ldas_prog_cc_optimization_none="-O0"
    ldas_prog_cc_optimization_exrtreme="-O4"
    ldas_prog_cc_optimization_high="-O3"
    ldas_prog_cc_optimization_medium="-O2"
    ldas_prog_cc_optimization_low="-O1"
    ldas_prog_cc_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    case $ldas_processor in
    sparc)
      ldas_prog_cc_64bit="-m64 -mcpu=v9"
      ldas_prog_ld_64bit="-m64 -mcpu=v9"
      ;;
    i386)
      ldas_prog_cc_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    *)
      ldas_prog_cxx_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    esac
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    AS_IF([test x"$enable_warnings_as_errors" = "xyes"],
          [CFLAGS_FATAL_WARNINGS="-Werror"],
	  [CFLAGS_FATAL_WARNINGS=""])
    CFLAGS_PEDANTIC="-pedantic -Wno-long-long"
    dnl .................................................................
    dnl   C
    dnl .................................................................
    AC_LANG_PUSH([C])
    ldas_saved_cflags=${CFLAGS}
    for warn in \
	-Wall \
	-Wextra \
	-Wno-missing-field-initializers \
	-Wno-unused-parameter \
	-Wunused-but-set-variable \
	-Wc++-compat \
	-Wunused-private-field \
	-Wstrict-prototypes
    do
      AC_MSG_CHECKING([if C compiler supports warning flag: ${warn}])
      ldas_saved_cflags=${CFLAGS}
      CFLAGS="${CFLAGS} -Werror ${ldas_prog_cc_warning} ${warn}"
      AC_COMPILE_IFELSE(
        [ AC_LANG_PROGRAM([[#include <stdio.h>]
                           [const char hw[] = "Hello, World\n";]],
                          [[fputs (hw, stdout );]]) ],
        [ ldas_prog_cc_warning="${ldas_prog_cc_warning} ${warn}"
	  AC_MSG_RESULT([yes])
	],
        [ AC_MSG_RESULT([no]) ] )
      CFLAGS=${ldas_saved_cflags}
    done
    AC_LANG_POP
    ;;
  xSun) dnl ######## S U N   S T U D I O ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cc_optimization_key="-O"
    ldas_prog_cc_optimization_none="-xO0"
    ldas_prog_cc_optimization_exrtreme="-xO5"
    ldas_prog_cc_optimization_high="-xO4"
    ldas_prog_cc_optimization_medium="-xO3"
    ldas_prog_cc_optimization_low="-xO1"
    ldas_prog_cc_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    ldas_prog_cc_64bit="-xarch=generic64"
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    CFLAGS_FATAL_WARNINGS="-errwarn=%all"
    CFLAGS_PEDANTIC=""
    ;;
  *) dnl ###### D E F A U L T ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cc_optimization_key="-O"
    ldas_prog_cc_optimization_none="-O0"
    ldas_prog_cc_optimization_exrtreme="-O4"
    ldas_prog_cc_optimization_high="-O3"
    ldas_prog_cc_optimization_medium="-O2"
    ldas_prog_cc_optimization_low="-O1"
    ldas_prog_cc_optimization_default=""
    ;;
  esac
  dnl -------------------------------------------------------------------
  dnl Setup flags for 64 bit compilation
  dnl -------------------------------------------------------------------
  AC_REQUIRE([AX_LDAS_ARG_ENABLE_64BIT])
  case x${enable_64bit} in
  xyes)
    case "x${ldas_prog_cc_64bit}" in
    x)
      ;;
    *)
      CFLAGS="$CFLAGS $ldas_prog_cc_64bit"
      ;;
    esac
    case "x${ldas_prog_ld_64bit}" in
    x)
      ;;
    *)
      LDFLAGS="$ldas_prog_ld_64bit $LDFLAGS"
      ;;
    esac
    ;;
  esac
  CFLAGS="${CFLAGS} ${ldas_prog_cc_warning}"
  dnl -------------------------------------------------------------------
  dnl  Checking on how to pass rpath info
  dnl -------------------------------------------------------------------
  saved_LDFLAGS="$LDFLAGS"
  LDFLAGS="$LDFLAGS -Wl,--rpath,/usr/lib"
  AC_TRY_RUN([ #include <stdlib.h>

               int main( );

               int
               main( )
               {
                   exit( 0 );
               }
             ],[
	        RPATH="--rpath"
             ])
  LDFLAGS="$saved_LDFLAGS"
  case "$RPATH" in
  "")
    saved_LDFLAGS="$LDFLAGS"
    LDFLAGS="$LDFLAGS -Wl,-R,/usr/lib"
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   exit( 0 );
                 }
               ],[
	        RPATH="-R"
               ])
    LDFLAGS="$saved_LDFLAGS"
    ;;
  esac
  AC_SUBST([CFLAGS_FATAL_WARNINGS])
  AC_SUBST([CFLAGS_PEDANTIC])
])
