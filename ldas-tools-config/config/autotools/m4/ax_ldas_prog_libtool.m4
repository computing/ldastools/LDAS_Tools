dnl======================================================================
dnl AX_LDAS_PROG_LIBTOOL
dnl   Check on how to setup libtool
dnl Depends:
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#



AC_DEFUN([AX_LDAS_PROG_LIBTOOL],
[
  dnl -------------------------------------------------------------------
  dnl Need to ensure the correct usage of the C compiler
  dnl -------------------------------------------------------------------
  AC_REQUIRE([AX_LDAS_PROG_CC])
  AC_REQUIRE([AX_LDAS_PROG_CXX])
  dnl -------------------------------------------------------------------
  dnl This hack is needed until libtool 1.5.22 is out. It will properly
  dnl    configure for 64bit Solaris.
  dnl -------------------------------------------------------------------
  ac_save_CC=$CC
  ac_save_CXX=$CXX
  CC="$CC $CFLAGS"
  CXX="$CXX $CXXFLAGS"
  AC_LANG_PUSH([C])
  AC_PROG_LIBTOOL
  AM_PROG_LIBTOOL
  AC_LANG_POP([C])
  case "$LDAS_CXX_FLAVOR" in
  SunPRO)
	if echo $CXXFLAGS | grep -e -library=stlport4 > /dev/null 2>&1
	then
		dnl -----------------------------------------------------
		dnl libtool currently assumes the default std C++ library.
		dnl   LDAS requires the stlport4 library instead.
		dnl -----------------------------------------------------
		mv libtool libtool.orig
		sed -e 's/-lCstd -lCrun/-library=stlport4/g' libtool.orig > libtool
		chmod +x libtool
	fi
	;;
  esac
  CC=$ac_save_CC
  CXX=$ac_save_CXX
])
