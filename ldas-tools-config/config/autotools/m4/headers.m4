dnl ---------------------------------------------------------------------
dnl  Check for Support of unordered_map headers
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AC_LDAS_CHECK_HEADER_UNORDERED_MAP],[
  dnl -------------------------------------------------------------------
  dnl Check if the header file is available
  dnl -------------------------------------------------------------------
  AC_CHECK_HEADER($1,[dnl
    dnl -----------------------------------------------------------------
    dnl Assume that we have a working implementation
    dnl -----------------------------------------------------------------
    ac_ldas_check_header_unordered_map_working=1
    dnl -----------------------------------------------------------------
    dnl Figure out the namespace
    dnl -----------------------------------------------------------------
    case $1 in
    */*)
      ac_ldas_check_header_unordered_map_namespace="`echo $1 | sed -e 's,/[[^/]]*$,,' -e 's,/,::,g' -e 's,^,std::,'`"
      ;;
    *)
      ac_ldas_check_header_unordered_map_namespace="std"
      ;;
    esac
    export ac_ldas_check_header_unordered_map_namespace
    dnl -----------------------------------------------------------------
    dnl Perform test of the header unsetting status flag if it fails.
    dnl -----------------------------------------------------------------
    dnl -----------------------------------------------------------------
    dnl Do some tests of compiler versions.
    dnl -----------------------------------------------------------------
    case $ac_ldas_check_header_unordered_map_working in
    1)
      AC_MSG_CHECKING([for working find method within unordered_map])
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
        #if defined __GNUC__
	#define GCC_VERSION (__GNUC__ * 10000 \
                               + __GNUC_MINOR__ * 100 \
                               + __GNUC_PATCHLEVEL__) 
	#if GCC_VERSION < 40200
	#error Known to have issues with earlier versions of GCC
	#endif /* GCC_VERSION */
	#endif /* __GNUC__ */
        ],[
        ]) dnl AC_PROG_LANG
      ],[dnl
        dnl -------------------------------------------------------------
        dnl  no known issue with the compiler
        dnl -------------------------------------------------------------
        AC_MSG_RESULT(yes)
      ],[
        dnl -------------------------------------------------------------
        dnl  Flag the failure
        dnl -------------------------------------------------------------
        ac_ldas_check_header_unordered_map_working=0
        AC_MSG_RESULT(no)
      ])
      ;;
    esac
    dnl -----------------------------------------------------------------
    dnl Perform test of the header unsetting status flag if it fails.
    dnl -----------------------------------------------------------------
    case $ac_ldas_check_header_unordered_map_working in
    1)
      AC_MSG_CHECKING([for working find method within unordered_map])
      AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
        #include <string>
        #include <$1>

        using $2::unordered_map;
        ],[
	  typedef unordered_map<std::string,int> m_type;

	  m_type m;

	  m_type::const_iterator mi = m.find( std::string( "hello" ) );
        ]) dnl AC_PROG_LANG
      ],[dnl
        dnl -------------------------------------------------------------
        dnl  Find seems to work
        dnl -------------------------------------------------------------
        AC_MSG_RESULT(yes)
      ],[
        dnl -------------------------------------------------------------
        dnl  Flag the failure
        dnl -------------------------------------------------------------
        ac_ldas_check_header_unordered_map_working=0
        AC_MSG_RESULT(no)
      ])
    esac
    dnl =================================================================
    case $ac_ldas_check_header_unordered_map_working in
    1)
      dnl ---------------------------------------------------------------
      dnl The header file is available and provides a funtional
      dnl    implementation so report the header as available.
      dnl ---------------------------------------------------------------
      AC_DEFINE(AS_TR_CPP(HAVE_$1),1,[Define to 1 if $1 is available])
      AC_DEFINE(AS_TR_CPP(WITH_$1_NAMESPACE),$2,[Define namespace to use for unordered_map])
      ;;
    esac
    dnl -----------------------------------------------------------------
    dnl  Cleanup after ourselves
    dnl -----------------------------------------------------------------
    unset ac_ldas_check_header_unordered_map_namespace
  ])
])
dnl ---------------------------------------------------------------------
dnl  Check for Support of FlexLexer.h headers
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_HEADER_FLEX_LEXER_H],[dnl
  AC_CACHE_CHECK([whether using FlexLexer.h needs special flags],[ldas_cv_flex_lexer_cppflags],dnl
		 [  dnl------------------------------------------------------
	            dnl Find where the FlexLexer.h is located
		    dnl------------------------------------------------------
    		    AC_CHECK_HEADER([FlexLexer.h],dnl
				    [HAVE_FLEXLEXER_H="yes"])
    		    AS_CASE([x${HAVE_FLEXLEXER_H}],
            	            [x],[ save_CPPFLAGS=${CPPFLAGS}
			          base_dir=`AS_DIRNAME([$LEX])`
			          paths="${paths} `echo ${LEX} | ${SED} -e 's/bin/libexec/' -e 's|$|/include|'`"
			          paths="${paths} `echo ${base_dir} | ${SED} -e 's|bin$|include|'`"
			          for dir in ${paths}
                                  do
			            unset ac_cv_header_FlexLexer_h
		  	            CPPFLAGS="-I ${dir} ${CPPFLAGS}"
		  	            AC_CHECK_HEADER([FlexLexer.h],dnl
				                    [HAVE_FLEXLEXER_H="yes"])
		  	            AS_CASE([x${HAVE_FLEXLEXER_H}],
		                            [x],[ unset ac_cv_header_FlexLexer_h],
			                    [ dnl default
                                              ldas_cv_flex_lexer_cppflags="-I ${dir}" ])
                                    AS_IF([test x${HAVE_FLEXLEXER_H} != x],dnl
				          [break])
		                  done
       	         	          CPPFLAGS=${save_CPPFLAGS}
       		 	          unset save_CPPFLAGS])
  ])
  AS_IF([test -n "${ldas_cv_flex_lexer_cppflags}"],dnl
        [LEX_CPPFLAGS="${ldas_cv_flex_lexer_cppflags}"])
  AC_SUBST([LEX_CPPFLAGS])
])
