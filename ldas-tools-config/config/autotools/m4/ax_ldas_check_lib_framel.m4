dnl ---------------------------------------------------------------------
dnl  Check for existence of frameL library
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_CHECK_LIB_FRAMEL],[
  AC_LANG_SAVE
  AC_LANG_C
  ldas_saved_cflags=$CFLAGS
  ldas_saved_libs=$LIBS
  AC_ARG_WITH([framel],
  [ --with-framel=DIR   Directory where libframe exists],,
  [ for dir in /opt/lscsoft/libframe $prefix `echo $LD_LIBRARY_PATH | sed -e 's/:/ /g'`
    do
      dir="`echo $dir | sed -e 's/\/lib$//'`";
      if test -f "$dir/include/FrameL.h"
      then
        with_framel="$dir"
	break
      fi
    done ])

  case x$with_framel in
  x)
    ;;
  *)
    LIBS="$LIBS -L$with_framel/lib"
    ;;
  esac
  AC_CHECK_LIB(Frame,FrVectCompress,[
    AC_DEFINE(HAVE_LIBFRAME,1,Define to 1 if you have the `Frame' library (-lFrame))
    case x$with_framel in
    x)
      ;;
    *)
      LIBFRAME="-L$with_framel/lib "
      CPPFLAGSFRAME="-I$with_framel/include"
      ;;
    esac
    LIBFRAME="${LIBFRAME}-lFrame"
    LIBS="${lt_prog_compiler_static} ${LIBFRAME} -lm"
    CFLAGS="$CPPFLAGSFRAME $CFLAGS"
    AC_RUN_IFELSE([AC_LANG_SOURCE([
	#include "FrameL.h"

	extern int FrSlong;

	int
	main( )
	{
	  FrLibShortIni( );
	  /*
	   * Zero exit (true) if FR_LONG_LONG should be defined
	   * Non-zero (false) if FR_LONG_LONG should not be defined
	   */
	  exit( FrSlong == sizeof( long ) );
	}
    ])],[
      AC_DEFINE([DEFINE_FR_LONG_LONG],1,[Define to 1 if FR_LONG_LONG needs to be defined])
    ],[],[])

  ],[],-lm)
  AC_SUBST(LIBFRAME)
  AC_SUBST(CPPFLAGSFRAME)
  AM_CONDITIONAL(HAVE_LIBFRAME, test "x$LIBFRAME" != "x")
  CFLAGS=$ldas_saved_cflags
  LIBS=$ldas_saved_libs
  AC_LANG_RESTORE
])
