dnl ---------------------------------------------------------------------
dnl  Check for Support of md5sum calculations
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_CHECK_LIB_MD5SUM],
  [ dnl -----------------------------------------------------------------
    dnl  Check for the library first
    dnl -----------------------------------------------------------------
    case `uname` in
    Linux)

      AC_CHECK_LIB(crypto,MD5_Init,
	[ AC_DEFINE(HAVE_MD5_IN_CRYPTO,1,[Defined if have md5 routines in libcrypto])
	  LIBMD5="-lcrypto"
	  AC_SUBST(LIBMD5)
          AC_CHECK_HEADERS([openssl/md5.h])
	],[ AC_CHECK_LIB(md5,MD5Init,
	    [ AC_DEFINE(HAVE_LIBMD5,1,[Defined if have libmd5])
	      LIBMD5="-lmd5"
	      AC_SUBST(LIBMD5)
	      AC_CHECK_HEADERS([md5.h])
	      AC_CHECK_HEADERS([md5global.h])
            ],[AC_MSG_ERROR(Missing md5 support) ])])
      ;;
    *)
      AC_CHECK_HEADERS([CommonCrypto/CommonDigest.h])
      AC_CHECK_LIB(md5,MD5Init,
	[ AC_DEFINE(HAVE_LIBMD5,1,[Defined if have libmd5])
	  LIBMD5="-lmd5"
	  AC_SUBST(LIBMD5)
	  AC_CHECK_HEADERS([md5.h])
	  AC_CHECK_HEADERS([md5global.h])
	],[ AC_CHECK_LIB(crypto,MD5_Init,
	    [ AC_DEFINE(HAVE_MD5_IN_CRYPTO,1,[Defined if have md5 routines in libcrypto])
	      LIBMD5="-lcrypto"
	      AC_SUBST(LIBMD5)
              AC_CHECK_HEADERS([openssl/md5.h])
            ],[AC_MSG_ERROR(Missing md5 support) ])])
      ;;
    esac
  ])
