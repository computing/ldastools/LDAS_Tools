dnl ---------------------------------------------------------------------
dnl  Check for Support of Xerces library
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([LDAS_CHECK_LIB_XERCES],[dnl
  AC_ARG_WITH([xerces],
              [AS_HELP_STRING([--with-xerces],
                              [@<:@default=check@:>@])],
              [],
	      [with_xerces=check])
  dnl -------------------------------------------------------------------
  dnl -------------------------------------------------------------------
  LIBXERCES=
  AS_IF([test "x${with_xerces}" != xno],
        [dnl ------------------------------------------------------------
         dnl  Setup for the C++ version of the library
	 dnl ------------------------------------------------------------
         AC_LANG_PUSH([C++])
	 ldas_xerces_save_cppflags="${CPPFLAGS}"
	 ldas_xerces_save_ldflags="${LDFLAGS}"
	 AS_CASE([${with_xerces}],dnl
	         [check],[],
	         [yes],[],
		 [dnl - default
		  CPPFLAGS="${CPPFLAGS} -I${with_xerces}/include/xml"
		  LDFLAGS="-L${with_xerces}/lib${LIB_64_DIR} ${LDFLAGS}"
                 ])
	 dnl ------------------------------------------------------------
	 dnl  Look for the directory containing the header files
	 dnl ------------------------------------------------------------
     	 AC_CHECK_HEADERS([xercesc/dom/DOMNode.hpp],
			  [AS_CASE([${with_xerces}],
			           [check],[],
			           [yes],[],
				   [XERCES_CPPFLAGS="-I${with_xerces}/include/xml"])
 			   HAVE_XERCESC_DOM_DOMNODE_HPP="1"
                          ]) dnl AC_CHECK_HEADERS
	 dnl ------------------------------------------------------------
	 dnl  Check for the library
	 dnl ------------------------------------------------------------
         AC_CHECK_LIB([xerces-c],[main],
                      [AC_SUBST([LIBXERCES],["-lxerces-c"])
		       AS_CASE([${with_xerces}],
			       [check],[],
			       [yes],[],
				XERCES_LDFLAGS="-L${with_xerces}/lib${LIB_64_DIR}")
		       AC_DEFINE([HAVE_LIBXERCES],[1],
		                 [Define if you have libxerces])
		      ],
		      [if test "x$with_xerces" != xcheck; then
		        AC_MSG_FAILURE(
			  [--with-xerces was given, but test for xerces failed])
		       fi
		      ])
	 dnl ------------------------------------------------------------
	 dnl  Restore to the previous language
	 dnl ------------------------------------------------------------
	 AC_SUBST([XERCES_LDFLAGS])
	 AC_SUBST([XERCES_CPPFLAGS])
	 LDFLAGS="${ldas_xerces_save_ldflags}"
     	 CPPFLAGS="${ldas_xerces_save_cppflags}"
     	 AC_LANG_POP
	]
  )
  dnl -------------------------------------------------------------------
  dnl  Create Makefile variable
  dnl -------------------------------------------------------------------
  AS_IF([test -n "${HAVE_XERCESC_DOM_DOMNODE_HPP}" dnl
              -a -n "${LIBXERCES}"],
	[XERCES_ENABLE_VAL="yes"],
	[XERCES_ENABLE_VAL="no"])
  AM_CONDITIONAL([HAVE_LIBXERCES],
		 [test -n "${HAVE_XERCESC_DOM_DOMNODE_HPP}" dnl
                       -a -n "${LIBXERCES}"])
])

dnl----------------------------------------------------------------------
dnl Check to see if ospace is available -
dnl variable:
dnl	$1 - a value other than "no" causes aborting of the configuration
dnl	     process. A value of "no" indicates that Object Space is
dnl	     an optional library.
dnl----------------------------------------------------------------------

AC_DEFUN([AC_LDAS_CHECK_LIB_OSPACE],
[ AC_CHECK_LIB(socket,connect)
  AC_CHECK_LIB(rt,sched_yield)
  AC_REQUIRE([AC_LDAS_CHECK_LIB_THREAD])
  AC_ARG_WITH([ospace], 
	      [AS_HELP_STRING([--with-ospace[[=DIR]]],
                              [specify ObjectSpace directory])],
	      [],
	      [with_ospace="check"])
  dnl -------------------------------------------------------------------
  dnl  Remember what the system looks like before macro called
  dnl -------------------------------------------------------------------
  HAVE_OSPACE=
  ldas_save_pkg_config_path="$PKG_CONFIG_PATH"
  AS_CASE(["x${with_ospace}"],
	  [xno],[],
	  [xyes],[],
	  [xcheck],[],
	  [ dnl default
	    PKG_CONFIG_PATH="${with_ospace}/lib${LIB_64_DIR}/pkgconfig:${PKG_CONFIG_PATH}"
	  ])
  ldas_save_cppflags="${CPPFLAGS}"
  ldas_save_ldflags="${LDFLAGS}"
  dnl -------------------------------------------------------------------
  dnl  Looking for ObjectSpace
  dnl -------------------------------------------------------------------
  AS_IF([test "x${with_ospace}" != xno],
  	[ dnl -----------------------------------------------------------
	  dnl -----------------------------------------------------------
	  PKG_CHECK_MODULES([OSPACE],[ospace],
	    [ dnl -------------------------------------------------------
	      dnl  Has been found
	      dnl -------------------------------------------------------
	      OSPACE_CXXFLAGS="${OSPACE_CFLAGS}"
	      HAVE_OSPACE="1"
	      OSPACE_LDFLAGS="`${PKG_CONFIG} --libs-only-L ospace`"
	      OSPACE_LIBS="`${PKG_CONFIG} --libs-only-l ospace`"
	      ospace_la="`${PKG_CONFIG} --variable=libdir ospace`/libospace.la"
	    ],
	    [ dnl -------------------------------------------------------
	      dnl Since it could not be found using pkg-config,
	      dnl  do it the old school way
	      dnl -------------------------------------------------------
	      dnl  Setup for the C++ version of the library
	      dnl -------------------------------------------------------
              AS_CASE(["x${with_ospace}"],
	      	[xcheck],[],
	      	[xyes],[],
	      	[ dnl default
		  LDFLAGS="-L${with_ospace}/lib${LIB_64_DIR} ${LDFLAGS}"
		  CPPFLAGS="-I${with_ospace}/include ${CPPFLAGS}"
	      	])
	      AC_LANG_PUSH([C++])
	      AC_CHECK_LIB([ospace],[main],
       	        [AC_SUBST([LIBOSPACE],["-lospace"])
                 AS_CASE(["${with_ospace}"],
	      	   [xcheck],[],
	      	   [xyes],[],
	      	   [ dnl default
		     OSPACE_LDFLAGS="-L${with_ospace}/lib${LIB_64_DIR}"
		     OSPACE_CXXFLAGS="-I${with_ospace}/include"
		     ospace_la="${with_ospace}/lib${LIB_64_DIR}/libospace.la"
	      	   ])
		 HAVE_OSPACE="1"],
		 [],
		 [-lpthread -lnsl -lm])
	      AC_LANG_POP([C++])
	    ]) dnl PKG_CHECK_MODULES
  ])
  dnl -------------------------------------------------------------------
  dnl  Set variables according to discovery
  dnl -------------------------------------------------------------------
  AS_IF([test "x${HAVE_OSPACE}" = "x1"],
        [AC_DEFINE([HAVE_LIBOSPACE],[1],
                   [Define to 1 if you have the `ObjectSpace' library])])
  AC_SUBST([OSPACE_LDFLAGS])
  AC_SUBST([OSPACE_CXXFLAGS])
  AC_SUBST([HAVE_OSPACE])
  AC_SUBST([ospace_la])
  AM_CONDITIONAL([HAVE_OSPACE],[test -n "${HAVE_OSPACE}"])
  dnl -------------------------------------------------------------------
  dnl  Restore the system
  dnl -------------------------------------------------------------------
  PKG_CONFIG_PATH="${ldas_save_pkg_config_path}"
  CPPFLAGS="${ldas_save_cppflags}"
  LDFLAGS="${ldas_save_ldflags}"
])

dnl -------------------------------------------------------------------
dnl -- check for SGI STL library (to use instead of gcc provided one)
dnl -------------------------------------------------------------------
AC_DEFUN([LDAS_LIB_STLPORT],[
  dnl -----------------------------------------------------------------
  dnl --- STLport - Core library
  dnl -----------------------------------------------------------------
  AC_ARG_WITH([stlport],
  [ --with-stlport=DIR   Build with SGI STL library],,
  [ for dir in $prefix `echo $LD_LIBRARY_PATH | sed -e 's/:/ /g'`
    do
      dir="`echo $dir | sed -e 's/\/lib$//'`";
      if test -d "$dir/include/stlport"
      then
        with_stlport="$dir"
	break
      fi
    done ])
  AC_MSG_CHECKING(for stlport installation directory)
  case "x$with_stlport" in
  x|xno)
    AC_MSG_RESULT(none)
    ;;
  *)
    if test -d "$dir"/include/stlport
    then
      with_stlport="$dir"
      AC_MSG_RESULT($with_stlport/include/stlport)
      STLPORT_INCLUDE_DIR="$with_stlport/include/stlport"
      STLPORT_LIB_DIR="$with_stlport/lib"
      if test -e $STLPORT_LIB_DIR/libstlport_gcc.so
      then
        STLPORT_LIB="stlport_gcc"
        STLPORT_DEBUG_LIB="stlport_gcc_stldbg"
      else
        STLPORT_LIB="stlport"
        STLPORT_DEBUG_LIB="stlportg"
      fi
      STLPORT_DEBUG_CXXFLAGS="-D_STLP_DEBUG -D_STLP_DEBUG_ALLOC -D_STLP_DEBUG_UNINITIALIZED"
    else
      AC_MSG_ERROR( invalid stlport directory: $with_stlport )
    fi
    ;;
  esac
  dnl -------------------------------------------------------------------
  dnl -- check for debug STLport library
  dnl -------------------------------------------------------------------
  AC_ARG_ENABLE(stlport-debug,
  [AS_HELP_STRING([--enable-stlport-debug],
	          [Build with debug STL library])],
  [enable_stlport_debug=yes],
  [enable_stlport_debug=no])
  dnl -------------------------------------------------------------------
  dnl -- Establish shell variable to know if using the debug version
  dnl -------------------------------------------------------------------
  case x"${with_stlport}" in
  x|xno)
    dnl -----------------------------------------------------------------
    dnl -- Either the installation directory was invalid or the user
    dnl --   specifically requested not to use STLport
    dnl -----------------------------------------------------------------
    ;;
  *)
    dnl -----------------------------------------------------------------
    dnl -- Want to use STLport
    dnl -----------------------------------------------------------------
    AC_MSG_CHECKING(for stlport debug)
    case x"${enable_stlport_debug}" in
    xyes)
      AC_MSG_RESULT(yes)
      case "${STLPORT_DEBUG_LIB}" in
        stlport_dbg)
        STLDEBUG="_dbg"
        ;;
      stlport_*_stldbg)
        STLDEBUG="_stldbg"
        ;;
      esac
      ;;
    *)
      dnl ---------------------------------------------------------------
      dnl Use non-debug version of stlport
      dnl ---------------------------------------------------------------
      AC_MSG_RESULT(no)
      ;;
    esac # x"${enable_stlport_debug}"
  esac # x"${enable_stlport}"
  dnl '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  dnl Set some compiler specific options
  dnl '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  if test "$ldas_prog_cxx_vendor" = Sun
  then
    if test -z "$STLPORT_LIB"
    then
      CXXFLAGS="$CXXFLAGS -library=stlport4"
    else
      CXXFLAGS="$CXXFLAGS -library=no%Cstd"
    fi
  fi
  AC_SUBST(STLDEBUG)
]) dnl LDAS_LIB_STLPORT

dnl ---------------------------------------------------------------------
dnl   Check for Support of STLport Finish
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_LIB_STLPORT_FINISH],[
  dnl -------------------------------------------------------------------
  dnl -- Establish compiler/linker flags for STLport
  dnl -------------------------------------------------------------------
  case x"${with_stlport}" in
  x|xno)
    dnl -----------------------------------------------------------------
    dnl -- Either the installation directory was invalid or the user
    dnl --   specifically requested not to use STLport
    dnl -----------------------------------------------------------------
    ;;
  *)
    dnl -----------------------------------------------------------------
    dnl -- Want to use STLport
    dnl -----------------------------------------------------------------
    case x"${enable_stlport_debug}" in
    xyes)
      debug_flags="$STLPORT_DEBUG_CXXFLAGS"
      stlport_lib="STLPORT_DEBUG_LIB";
      ;;
    *)
      dnl ---------------------------------------------------------------
      dnl Use non-debug version of stlport
      dnl ---------------------------------------------------------------
      stlport_lib="$STLPORT_LIB";
      ;;
    esac # x"${enable_stlport_debug}"
    case x${STLPORT_CXXFLAGS} in
    x) ;;
    *) AC_MSG_CHECKING(for stlport CXXFLAGS)
       CXXFLAGS="${CXXFLAGS} ${STLPORT_CXXFLAGS}"
       AC_MSG_RESULT(${STLPORT_CXXFLAGS})
       ;;
    esac
    case x${STLPORT_INCLUDE_DIR} in
    x) ;;
    *) AC_MSG_CHECKING(for stlport include flags)
       CXXFLAGS="${CXXFLAGS} -I${STLPORT_INCLUDE_DIR} $debug_flags"
       AC_MSG_RESULT(-I${STLPORT_INCLUDE_DIR} $debug_flags)
       ;;
    esac
    case x${STLPORT_LIB_DIR} in
    x)
       case x${stlport_lib} in
       x)
 	 ;;
       *)
         STLPORT_LDFLAGS="-l${stlport_lib}"
         ;;
       esac
       ;;
    *)
       STLPORT_LDFLAGS="${LDFLAGS} -L${STLPORT_LIB_DIR} -l${stlport_lib}"
       ;;
    esac
    case x$STLPORT_LDFLAGS in
    x) ;;
    *) AC_MSG_CHECKING(for stlport LDFLAGS)
       LDFLAGS="${LDFLAGS} ${STLPORT_LDFLAGS}"
       AC_MSG_RESULT(${STLPORT_LDFLAGS})
       ;;
    esac
    ;;
  esac # x"${enable_stlport}"
])
