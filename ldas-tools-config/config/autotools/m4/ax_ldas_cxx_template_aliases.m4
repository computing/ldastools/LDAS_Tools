dnl ---------------------------------------------------------------------
dnl  AX_LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing (2011 standard)
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_CXX_TEMPLATE_ALIASES],
[
AC_MSG_CHECKING([if the C++ compiler supports template aliasing])
AC_LANG_PUSH([C++])
AC_TRY_COMPILE([
#include <memory>
namespace {
  template< class T > using AutoArray = std::unique_ptr< T[] >;
  template< typename T, typename U > using DynamicPointerCast = typename std::dynamic_pointer_cast< T, U >;
}
],[
 AutoArray< int >	a;
],[
  AC_DEFINE([HAVE_CXX_TEMPLATE_ALIASES],[1],[Defined if C++ supports template aliasing])
  AC_MSG_RESULT([yes])
  HAVE_CXX_TEMPLATE_ALIASES=1
],[
  AC_MSG_RESULT([no])
  HAVE_CXX_TEMPLATE_ALIASES=0
])
AC_SUBST([HAVE_CXX_TEMPLATE_ALIASES])
AC_LANG_POP([C++])
])
