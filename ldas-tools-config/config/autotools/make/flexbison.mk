#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#------------------------------------------------------------------------
# YACC rules
#------------------------------------------------------------------------
%.hh %.cc: %.yy
	$(YACC) $(AM_YFLAGS) $(YFLAGS) $<
	protector="$(NAMESPACE)__$(*F)_HH"; \
	protector=`echo $$protector | tr '[:lower:]' '[:upper:]'`; \
	ccprotector="$(NAMESPACE)__$(*F)_CC"; \
	ccprotector=`echo $$ccprotector | tr '[:lower:]' '[:upper:]'`; \
	echo "DEBUG: protector: $$protector ccprotector: $$ccprotector" ; \
	sed -e 's/PARSER_HEADER_H/'$$protector'/g' \
	    -e '/^#ifdef '$$ccprotector'.*$$/,/^#endif .* '$$ccprotector'.*$$/w $(*F).tmp' \
	    -e '/^#ifdef '$$ccprotector'.*$$/,/^#endif .* '$$ccprotector'.*$$/d' \
	    $(*F).hh > $(*F).hh.tmp; \
	cat $(*F).tmp >> $(*F).hh.tmp; \
	mv $(*F).hh.tmp $(*F).hh; \
	awk '/^#include/ { print "#define '$$ccprotector'"; } {print $0; }' \
	  $(*F).cc > $(*F).cc.tmp ; \
	mv $(*F).cc.tmp $(*F).cc
	rm -f $(*F).tmp

#------------------------------------------------------------------------
# LEX rules
#------------------------------------------------------------------------

%.cc: %.ll
	if test $< = `basename $<`; \
	then $(LEX) $(FLEX_SKL) $(srcdir)/$< ;\
	else $(LEX) $(FLEX_SKL) $< ; \
	fi
	if test -f lex.yy.cc; \
	then \
	  mv lex.yy.cc $@; \
	fi

clean-local: clean-local-bison

clean-local-bison:
	rm -rf location.hh position.hh stack.hh
