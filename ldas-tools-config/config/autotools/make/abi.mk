#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#=========================================================================
# Recursive support
#=========================================================================

abi-check-recursive:

ifneq ($(RECURSIVE_TARGETS),abi-check-recursive)
abi-check-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=abi-check-recursive abi-check-recursive
endif

abi-check: abi-check-recursive abi-check-am

abi-check-am: all-am

.PHONY: verify_abi_old_root

#=========================================================================
# Overrides
#=========================================================================

ABI_HDR_DIR ?= $(NAMESPACE)
ABI_OLD_ROOT ?=

REPORTS=$(addprefix compat_reports/,$(lib_LTLIBRARIES:.la=.abi.rpt))
REPORTS+=$(addprefix compat_reports/,$(pkglib_LTLIBRARIES:.la=.abi.rpt))

abi_old_hdrs=$(realpath $(wildcard $(ABI_OLD_ROOT)/include/$(ABI_HDR_DIR)/*))
abi_new_hdrs=$(realpath $(wildcard $(abs_top_builddir)/include/$(ABI_HDR_DIR)/*))

#=========================================================================
# Suplimental rules
#=========================================================================

ifneq ($(ABI_COMPLIANCE_CHECKER),)
ifneq ($(ABI_DUMPER),)

$(REPORTS):
	$(AM_V_GEN) libbasename="$(subst .abi.rpt,,$(@F))"; \
	lib="$${libbasename}.so"; \
	hdr_old="abi_old_hdrs.txt"; \
	hdr_new="abi_new_hdrs.txt"; \
	if test -f "$(ABI_OLD_ROOT)/lib64/$${lib}"; \
	then \
	  lib_old="$(ABI_OLD_ROOT)/lib64/$${lib}"; \
	elif test -f "$(ABI_OLD_ROOT)/lib/64/$${lib}"; \
	then \
	  lib_old="$(ABI_OLD_ROOT)/lib/64/$${lib}"; \
	else \
	  lib_old="$(ABI_OLD_ROOT)/lib/$${lib}"; \
	fi; \
	lib_new=".libs/$${lib}"; \
	report_old="$${libbasename}-1.dump"; \
	report_new="$${libbasename}-2.dump"; \
	rm -f $${hdr_new} $${report_old} $${report_new}; \
	for x in $(abi_old_hdrs); \
	do \
	  echo $${x} >> $${hdr_old}; \
	done; \
	for x in $(abi_new_hdrs); \
	do \
	  echo $${x} >> $${hdr_new}; \
	done; \
	$(ABI_DUMPER) $${lib_old} -public-headers $${hdr_old} -o $${report_old} -lver 1; \
	$(ABI_DUMPER) $${lib_new} -public-headers $${hdr_new} -o $${report_new} -lver 2; \
	$(ABI_COMPLIANCE_CHECKER) -l $${libbasename} -old $${report_old} -new $${report_new}

abi-check: verify_abi_old_root $(REPORTS)
	@true

verify_abi_old_root:
	@if test -d "$(ABI_OLD_ROOT)"; \
        then \
	  true; \
	else \
	  echo "*** Please provide a valid value for ABI_OLD_ROOT [$(ABI_OLD_ROOT)] ***" ; \
	  false; \
	fi
endif
endif
