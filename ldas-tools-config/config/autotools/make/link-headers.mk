#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#========================================================================
# Target for linking header files so they have the same look as when
#   installed.
#
# HDR_DIR              - Destiination directory
# HDR_SOURCE_DIR_FILES - Files in the source directory to be linked
# HDR_BUILD_DIR_FILES  - Files in the build directory to be linked
#
#========================================================================
LINK_HEADERS_TIME_STAMPS =
LINK_HEADERS_TIME_STAMPS += hdr-time-parallel.stamp
LINK_HEADERS_TIME_STAMPS += hdr-time-stamp

hdr-time-parallel.stamp: Makefile $(HDR_BUILD_DIR_FILES)
	@rm -f hdr-time-parallel.stamp
	@touch hdr-time-parallel.stamp.tmp
	$(AM_V_GEN)
	$(AM_V_at)#------------------------------------------------------
	$(AM_V_at)# Remove files that were create by the source directory
	$(AM_V_at)#  as the list may have changed.
	$(AM_V_at)#------------------------------------------------------
	$(AM_V_at)-SRCDIR=`( cd $(srcdir) ; pwd )`; export SRCDIR; \
	  ( cd $(top_builddir)/include/$(HDR_DIR); \
	    for f in `find . \( -type l -exec ls -l {} \; \) -o \( -type d ! -name . -prune \) | awk '{ print $$NF}'`; \
	    do \
	    if test "`dirname $$f`" == $$SRCDIR; \
	    then \
	     rm -f `basename $$f`; \
	    fi; \
	   done; )
	$(AM_V_at)#------------------------------------------------------
	$(AM_V_at)# Create the directory
	$(AM_V_at)#------------------------------------------------------
	$(AM_V_at)mkdir -p $(top_builddir)/include/$(HDR_DIR)
	$(AM_V_at)#------------------------------------------------------
	$(AM_V_at)# Create the Links
	$(AM_V_at)#------------------------------------------------------
	$(AM_V_at)SRCDIR=`( cd $(srcdir) ; pwd )`; export SRCDIR; \
	  for hdr in $(HDR_SOURCE_DIR_FILES); \
	  do \
	    ( cd $(top_builddir)/include/$(HDR_DIR); \
              rm -f $$hdr; \
	      ln -s $$SRCDIR/$$hdr . ); \
	  done
	$(AM_V_at)CWD=`pwd`; export CWD; \
	  for hdr in $(HDR_BUILD_DIR_FILES); \
	  do \
	    ( cd $(top_builddir)/include/$(HDR_DIR); rm -f $$hdr ; ln -s $$CWD/$$hdr . ); \
	  done
	$(AM_V_at)touch hdr-time-stamp
	@mv hdr-time-parallel.stamp.tmp hdr-time-parallel.stamp

hdr-time-stamp: Makefile $(HDR_BUILD_DIR_FILES)
	@if test -f $@ ; then :; else \
	  trap 'rm -rf hdr-time-stamp.lock hdr-time-parallel.stamp' 1 2 13 15; \
	  if mkdir hdr-time-stamp.lock 2>/dev/null ; then \
	    $(MAKE) $(AM_MAKEFLAGS) hdr-time-parallel.stamp ; \
	    result=$$?; rm -rf hdr-time-stamp.lock ; exit $$result; \
	  else \
	    while test -d hdr-time-stamp.lock; do sleep 1; done; \
	    test -f hdr-time-parallel.stamp ; \
	  fi; \
	fi

CLEAN_LOCAL+=link-headers-clean
link-headers-clean:
	rm -f hdr-time-parallel.stamp
	rm -f hdr-time-stamp
	for hdr in $(HDR_SOURCE_DIR_FILES); \
	do \
	  ( cd $(top_builddir)/include/$(HDR_DIR); rm -f $$hdr; ); \
	done
	for hdr in $(HDR_BUILD_DIR_FILES); \
	do \
	  ( cd $(top_builddir)/include/$(HDR_DIR); rm -f $$hdr ); \
	done
