#!/usr/bin/env perl
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#
$pending_block = "";
sub postprocess()
{
    my($new_pending_block) = "";
    my($stage) = 0;
    foreach ( split '\n', $pending_block )
    {
        s,//\s*:TODO:,/// \todo ,i;
	s,//:,/// \\brief,;
	s,//[+],///,;
	s,//\s*!param:(.*)\s+([A-Za-z_0-9]+)\s*-,/// \\param[in] $2,;
	s,//\s*!return:(.*-),/// \\return,;
	s,//\s*!exc:,/// \\exception,;
	
	if ( $stage == 0 )
	{
	    if ( /^\s*\/\/+\s*$/ )
	    {
		next;
    	    }
	    else
	    {
		$stage = 1;
	    }
	}
	elsif ( $stage == 1 )
	{
	    if ( /^\s*\/\/+[-]+\s*$/ )
	    {
		if ($stage == 1)
		{
		    $stage = 0;
		}
	    }
	}
	$new_pending_block .= "$_\n";
    }
    $pending_block = $new_pending_block;
}

sub pack($)
{
    my($line) = @_;

    if ( $line =~ /^\s*\/\// )
    {
	$pending_block = $pending_block . "$line\n";
    }
    else
    {
	postprocess;
	print "$pending_block$line\n";
	$pending_block = "";
    }
}

$con = 0;
$ignore = 0;
$in_comment = 0;
$pattern1 = '^\s*\**\s*(-|=)+\s*\*/\s*$';
$pattern2 = '/^\s*\/\*\*\s*(-|=)+\s*\**$';
while ( <> )
{
    chop;

    s/\$[(](\w+)[)]/$ENV{$1}/g;

    #--------------------------------------------------------------------
    # Remove lines only intended for visual effects
    #--------------------------------------------------------------------
    s,^\s*///(-|=)+$^\s*,,m;
    #--------------------------------------------------------------------
    # Transform C style comment lines
    #--------------------------------------------------------------------
    if ( $in_comment )
    {
	if ( /${pattern1}/ )
	{
	    $in_comment = 0;
	    s,$pattern1,\*/,m;
	}
	else
	{
	    s,/^\s*\*\s+/,,m;
	}
    }
    elsif ( /$pattern2/ )
    {
	$in_comment = 1;
	s,$pattern2,/\*\*,m;	# just make it /**
    }

    &pack($_);
}
