#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#

AC_DEFUN([AX_LDAS_CHECK_FUNC_SWAP_ALL],[
  AX_LDAS_CHECK_HEADER_FOR_SUBST([byteswap.h],[HAVE_BYTESWAP_H])
  AX_LDAS_CHECK_HEADER_FOR_SUBST([sys/byteorder.h],[HAVE_SYS_BYTEORDER_H])
  AX_LDAS_CHECK_HEADER_FOR_SUBST([machine/bswap.h],[HAVE_MACHINE_BSWAP_H])
  AX_LDAS_CHECK_HEADER_FOR_SUBST([libkern/OSByteOrder.h],[HAVE_LIBKERN_OSBYTEORDER_H])

  AX_LDAS_CHECK_FUNC_SWAP([bswap_16],[HAVE_BSWAP_16],[int16])
  AX_LDAS_CHECK_FUNC_SWAP([bswap_32],[HAVE_BSWAP_32],[int32])
  AX_LDAS_CHECK_FUNC_SWAP([bswap_64],[HAVE_BSWAP_64],[int64])

  AX_LDAS_CHECK_FUNC_SWAP([bswap16],[HAVE_BSWAP16],[int16])
  AX_LDAS_CHECK_FUNC_SWAP([bswap32],[HAVE_BSWAP32],[int32])
  AX_LDAS_CHECK_FUNC_SWAP([bswap64],[HAVE_BSWAP64],[int64])

  AX_LDAS_CHECK_FUNC_SWAP([BSWAP_16],[HAVE_BSWAP_16_MACRO],[int16])
  AX_LDAS_CHECK_FUNC_SWAP([BSWAP_32],[HAVE_BSWAP_32_MACRO],[int32])
  AX_LDAS_CHECK_FUNC_SWAP([BSWAP_64],[HAVE_BSWAP_64_MACRO],[int64])

  AX_LDAS_CHECK_FUNC_SWAP([OSSwapInt16],[HAVE_OSSWAPINT16],[int16])
  AX_LDAS_CHECK_FUNC_SWAP([OSSwapInt32],[HAVE_OSSWAPINT32],[int32])
  AX_LDAS_CHECK_FUNC_SWAP([OSSwapInt64],[HAVE_OSSWAPINT64],[int64])
]) dnl AC_DEFUN - AX_LDAS_CHECK_FUNC_SWAP_ALL
