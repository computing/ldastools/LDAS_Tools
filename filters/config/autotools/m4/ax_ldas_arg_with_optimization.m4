dnl ---------------------------------------------------------------------
dnl  Check for optimization specification
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_ARG_WITH_OPTIMIZATION],[dnl
  AC_ARG_WITH([optimization],
	[AS_HELP_STRING([--with-optimization],
	                [Compiler optimization (extreme|high|medium|low|none)])],
	[],[with_optimization="high"])
	case x$CC in
	x) ;; dnl ignore because it has not been set
	*)
	  case x${with_optimization} in
	  xnone) dnl -- none
	    ldas_prog_cc_optimization=$ldas_prog_cc_optimization_none
            ;;
	  xextreme) dnl -- extreme
            ldas_prog_cc_optimization=$ldas_prog_cc_optimization_extreme
	    ;;
	  xhigh) dnl -- high
	    ldas_prog_cc_optimization=$ldas_prog_cc_optimization_high
   	    ;;
          xlow) dnl -- low
            ldas_prog_cc_optimization=$ldas_prog_cc_optimization_low
            ;;
	  xdefault) dnl -- default
            ldas_prog_cc_optimization=$ldas_prog_cc_optimization_default
            ;;
          *) dnl -- all other cases
	    ldas_prog_cc_optimization=$ldas_prog_cc_optimization_medium
	    ;;
	  esac
	  ;;
	esac
	case x$CXX in
	x) ;; dnl ignore because it has not been set
	*)
	  case x${with_optimization} in
	  xnone) dnl -- none
	    ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_none
            ;;
	  xextreme) dnl -- extreme
            ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_extreme
	    ;;
	  xhigh) dnl -- high
	    ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_high
   	    ;;
          xlow) dnl -- low
            ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_low
            ;;
	  xdefault) dnl -- default
            ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_default
            ;;
          *) dnl -- all other cases
	    ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_medium
	    ;;
	  esac
	  ;;
	esac
  dnl -------------------------------------------------------------------
  CFLAGS="$LDAS_DEFS $CFLAGS"
  echo "$CFLAGS" | egrep -e ${ldas_prog_cc_optimization_key}'[[0-9]]' > /dev/null 2>&1
  AS_IF([test $? -ne 0],
  	[CFLAGS="$CFLAGS ${ldas_prog_cc_optimization}"],
	[CFLAGS="`echo $CFLAGS | sed -e 's/'${ldas_prog_cc_optimization_key}'[[0-9]]/'${ldas_prog_cc_optimization}'/g'`"])
  dnl --------------------------------------------------------------
  echo "$LDAS_DEFS $CXXFLAGS" | egrep -e ${ldas_prog_cxx_optimization_key}'[[0-9]]' > /dev/null 2>&1
  AS_IF([test $? -ne 0],
        [CXXFLAGS="$LDAS_DEFS $CXXFLAGS ${ldas_prog_cxx_optimization}"],
	[CXXFLAGS="`echo $LDAS_DEFS $CXXFLAGS | sed -e 's/'${ldas_prog_cxx_optimization_key}'[[0-9]]/'${ldas_prog_cxx_optimization}'/g'`"])
  dnl --------------------------------------------------------------
  AC_MSG_CHECKING([optimization for $CC])
  AC_MSG_RESULT([${ldas_prog_cc_optimization}])
  AC_MSG_CHECKING([optimization for $CXX])
  AC_MSG_RESULT([${ldas_prog_cxx_optimization}])
])
