#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#------------------------------------------------------------------------
# Compiler configuration
#------------------------------------------------------------------------
#------------------------------------------------------------------------
# Fatal Warnings
#------------------------------------------------------------------------
ifneq "$(FATAL_WARNINGS)" "no"
ifneq "x$(CFLAGS)" "x"
AM_CFLAGS+=$(CFLAGS_FATAL_WARNINGS)
endif
ifneq "x$(CXXFLAGS)" "x"
AM_CXXFLAGS+=$(CXXFLAGS_FATAL_WARNINGS)
endif
endif

#------------------------------------------------------------------------
# Pedantic
#------------------------------------------------------------------------
ifneq "$(PEDANTIC)" "no"
ifneq "x$(CFLAGS)" "x"
AM_CFLAGS+=$(CFLAGS_PEDANTIC)
endif
ifneq "x$(CXXFLAGS)" "x"
AM_CXXFLAGS+=$(CXXFLAGS_PEDANTIC)
endif
endif

#------------------------------------------------------------------------
# Special variables
#------------------------------------------------------------------------
CFLAGS_NON_FATAL_WARNINGS:=$(patsubst $(CFLAGS_FATAL_WARNINGS),,$(AM_CFLAGS))
CXXFLAGS_NON_FATAL_WARNINGS:=$(patsubst $(CXXFLAGS_FATAL_WARNINGS),,$(AM_CXXFLAGS))
