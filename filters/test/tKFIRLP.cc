//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <fstream>

#include "ldastoolsal/unittest.h"
#include "filters/KFIRLP.hh"

LDASTools::Testing::UnitTest Test;

using namespace std;
using namespace Filters;

void
testNominal( )
{
    valarray< double > k( 10 );

    std::string basename( "tKFIRLP1.dat" );
    std::string path;
    if ( getenv( "DATADIR" ) != 0 )
        path = getenv( "DATADIR" );
    else
        path += ".";

    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    ifstream inputK1( path.c_str( ) );

    Test.Check( inputK1.is_open( ) ) << "Found data file" << endl;

    unsigned int i = 0;
    while ( i < k.size( ) )
    {
        inputK1 >> k[ i++ ];
    }

    valarray< double > d;
    try
    {
        KFIRLP ktest( .4, 20, .2 );
        Test.Check( true, "KFIRLP: Initializing Kaiser Filter" );
        ktest.apply( d );
    }
    catch ( std::exception& e )
    {
        Test.Check( false )
            << "KFIRLP: Initializing Kaiser Filter: " << e.what( ) << std::endl;
        d.resize( k.size( ) );
        d = 0.0;
    }
    bool dtrue = true;
    for ( unsigned int j = 0; j < k.size( ); j++ )
    {
        Test.Message( ) << "j: " << j << " k[j]: " << k[ j ]
                        << " d[j]: " << d[ j ] << std::endl;
        dtrue = dtrue && fabs( k[ j ] - d[ j ] ) < 1e-10;
    }
    Test.Check( dtrue, "KFIRLP: Kaiser Window design matches Matlab" );
}

void
testCopy( )
{
    valarray< double > t0;
    t0.resize( 1 );
    KFIRLP test1( .25, 20, .5 );
    test1.apply( t0 );
    {
        valarray< double > t1;
        KFIRLP             kopy1( test1 );
        kopy1.apply( t1 );
        bool tvalue( true );
        tvalue = ( t0.size( ) == t1.size( ) );
        if ( tvalue )
            for ( unsigned int j = 0; j < t0.size( ); j++ )
                tvalue =
                    tvalue && t0[ j ] == t1[ j ]; // exact equality required
        Test.Check( tvalue, "KFIRLP copy constructor" );
    }
    {
        valarray< double > t1;
        KFIRLP             kopy2( .5, 50, .1 );
        kopy2.apply( t1 );
        kopy2 = test1;
        kopy2.apply( t1 );
        bool tvalue( true );
        tvalue = t0.size( ) == t1.size( );
        if ( tvalue )
            for ( unsigned int j = 0; j < t0.size( ); j++ )
                tvalue =
                    tvalue && t0[ j ] == t1[ j ]; // exact equality required
        Test.Check( tvalue, "KFIRLP assignment operator" );
    }
}

void
testMutator( )
{
    valarray< double > temp( 1 );
    KFIRLP             ktest( .25, 20, .5 );
    ktest.apply( temp );
    valarray< double > k2( 24 );

    std::string basename( "tKFIRLP2.dat" );
    std::string path;
    if ( getenv( "DATADIR" ) != 0 )
        path = getenv( "DATADIR" );
    else
        path += ".";

    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    ifstream inputK2( path.c_str( ) );

    Test.Check( inputK2.is_open( ) ) << "Found data file" << endl;

    std::size_t i = 0;
    while ( inputK2 && ( i < k2.size( ) ) )
    {
        inputK2 >> k2[ i ];
        i++;
    }
    std::string what = "KFIRLP: Mutate transition band frequency";
    try
    {
        ktest.setFc( .4 );
        Test.Check( ktest.getFc( ) == .4, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( false ) << what << " (" << d.what( ) << ")" << std::endl;
    }
    what = "KFIRLP: Mutate attenuation";
    try
    {
        ktest.setAlpha( 40 );
        Test.Check( ktest.getAlpha( ) == 40, what );
    }
    catch ( std::domain_error& i )
    {
        Test.Check( false ) << what << " (" << i.what( ) << ")" << std::endl;
    }
    what = "KFIRLP: Mutate transition band width";
    try
    {
        ktest.setDf( .2 );
        Test.Check( ktest.getDf( ) == .2, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( false ) << what << " (" << d.what( ) << ")" << std::endl;
    }
    ktest.apply( temp );
    what = "KFIRLP: Mutated filter matches Matlab generated filter";
    bool check = true;
    for ( unsigned int j = 0; j < temp.size( ); j++ )
        check = check && ( temp[ j ] - k2[ j ] ) < 1e-10;
    Test.Check( check, what );
}

void
tryKFIRLP( const std::string& what, double f, double a, double df )
{
    try
    {
        KFIRLP kfirlp( f, a, df );
        Test.Check( false, what );
    }
    catch ( std::domain_error& e )
    {
        Test.Check( true ) << what << " (" << e.what( ) << ")" << std::endl;
    }
    catch ( std::invalid_argument& e )
    {
        Test.Check( true ) << what << " (" << e.what( ) << ")" << std::endl;
    }
}

void
testCExceptions( )
{
    std::string what = "KFIRLP: non-positive transition frequency";
    tryKFIRLP( what, 0, 10, 0.5 );
    what = "KFIRLP: unity transition frequency";
    tryKFIRLP( what, 1, 10, 0.5 );
    what = "KFIRLP: 0 dB attenuation";
    tryKFIRLP( what, 0.5, 0, 0.5 );
    what = "KFIRLP: non-positive transition band-width";
    tryKFIRLP( what, 0, 10, 0.5 );
    what = "KFIRLP: df == 1";
    tryKFIRLP( what, 1, 10, 0.5 );
}

void
testMExceptions( )
{
    valarray< double > temp( 1 );
    KFIRLP             ktest( .25, 20, .5 );
    ktest.apply( temp );
    std::string what;
    what = "KFIRLP: Set passband edge to 0 frequency";
    try
    {
        ktest.setFc( 0 );
        Test.Check( false, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }
    what = "KFIRLP: Set passband edge to Nyquist frequency";
    try
    {
        ktest.setFc( 1 );
        Test.Check( false, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }
    what = "KFIRLP: Set attenuation to zero";
    try
    {
        ktest.setAlpha( 0 );
        Test.Check( false, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }
    what = "KFIRLP: Set transition bandwith to zero";
    try
    {
        ktest.setDf( 0 );
        Test.Check( false, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }
    what = "KFIRLP: Set transition bandwidth to Nyquist frequency";
    try
    {
        ktest.setDf( 1 );
        Test.Check( false, what );
    }
    catch ( std::domain_error& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );
    if ( Test.IsVerbose( ) )
        std::cout << "$Id: tKFIRLP.cc,v 1.4 2005/11/15 18:35:19 emaros Exp $"
                  << std::endl;
    testNominal( );
    testCopy( );
    testMutator( );
    testCExceptions( );
    testMExceptions( );
    Test.Exit( );
}
