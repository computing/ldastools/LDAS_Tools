//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <stdlib.h>
#include <complex>

#include "ldastoolsal/unittest.h"

#include "filters/Interpolate.hh"
#include "filters/LinFilt.hh"

#define INTERP_DEBUG 0

using namespace std;
using namespace LDASTools::Testing;
using namespace Filters;

UnitTest Test;

class IntSequence
{
public:
    IntSequence( const int start = 0, const int stride = 1 )
        : m_val( start ), m_stride( stride )
    {
    }

    int
    operator( )( )
    {
        const int tmp = m_val;
        m_val += m_stride;
        return tmp;
    }

private:
    int m_val;
    int m_stride;
};

class Randint
{
public:
    Randint( const int max ) : m_max( max )
    {
    }

    int
    operator( )( )
    {
        return rand( ) % m_max;
    }

private:
    int m_max;
};

template < class T >
double
Eps( )
{
    return 1.0e-12;
}

template <>
double
Eps< float >( )
{
    return 1.0e-03;
}

template <>
double
Eps< complex< float > >( )
{
    return 1.0e-04;
}

// Generate values of an order n polynomial at x.
template < class T >
T
poly( const T x, const size_t order )
{
    T val = 0.0;

    for ( size_t k = 0; k <= order; ++k )
    {
        val += pow( -x, static_cast< const T >( k ) ) /
            ( static_cast< const T >( k + 1 ) );
    }

    return val;
}

template < class T >
complex< T >
complex_pow( const complex< T >& x, const size_t y )
{
    if ( y == 0 )
    {
        return 1.0;
    }

    complex< T > result( x );

    for ( size_t index = 1; index < y; ++index )
    {
        result *= x;
    }

    return result;
}

template < class T >
complex< T >
poly( const complex< T > x, const size_t order )
{
    complex< T >       val = 0.0;
    const complex< T > minus_x( -x );

    for ( size_t k = 0; k <= order; ++k )
    {
        val += complex_pow< T >( minus_x, k ) / static_cast< T >( k + 1 );
    }

    return val;
}

// test the filter designer exceptions
template < class T >
void
TestFilterExceptions( )
{
    string what;

    {
        what = "zero-size array exception";

        const double  alpha = 0.5;
        valarray< T > b;

        try
        {
            designInterpolatingFilter( b, alpha );
            Test.Check( false ) << what << endl;
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "array of coefficients must have size > 0";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha < 0 exception";

        const double  alpha = -1;
        valarray< T > b( 1 );

        try
        {
            designInterpolatingFilter( b, alpha );
            Test.Check( false ) << what << endl;
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha == 0 exception";

        const double  alpha = 0;
        valarray< T > b( 10 );

        try
        {
            designInterpolatingFilter( b, alpha );
            Test.Check( false ) << what << endl;
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha == 1 exception";

        const double  alpha = 1;
        valarray< T > b( 100 );

        try
        {
            designInterpolatingFilter( b, alpha );
            Test.Check( false ) << what << endl;
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha > 1 exception";

        const double  alpha = 1.5;
        valarray< T > b( 100 );

        try
        {
            designInterpolatingFilter( b, alpha );
            Test.Check( false ) << what << endl;
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }
}

// test the filter design
template < class T >
void
TestFilterCoeffs( const double alpha, const size_t N )
{
    valarray< T > b( T( 0 ), N );

    designInterpolatingFilter( b, alpha );

    const T bSum = b.sum( );

    Test.Check( abs( bSum - static_cast< T >( 1 ) ) < 1.0e-6 )
        << "Filter coeffs for alpha = " << alpha << " order = " << N
        << " sum to unity: b.sum() = " << bSum << endl;

#if INTERP_DEBUG
    for ( size_t k = 0; k < b.size( ); ++k )
    {
        cerr << "b[" << setw( 2 ) << k << "] = " << b[ k ] << endl;
    }
#endif
}

template < class T >
void
TestInterpolateExceptions( )
{
    string what;

    {
        what = "alpha < 0 exception";

        const double alpha = -1;
        const size_t order = 2;

        try
        {
            Interpolate< T >( alpha, order );
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha == 0 exception";

        const double alpha = 0;
        const size_t order = 10;

        try
        {
            Interpolate< T >( alpha, order );
            Test.Check( false ) << what << endl;
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha == 1 exception";

        const double alpha = 1;
        const size_t order = 100;

        try
        {
            Interpolate< T >( alpha, order );
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "alpha > 1 exception";

        const double alpha = 1.5;
        const size_t order = 100;

        try
        {
            Interpolate< T >( alpha, order );
        }
        catch ( invalid_argument& e )
        {
            const string exp = "Filters::designInterpolatingFilter() - "
                               "alpha must satisfy 0 < alpha < 1";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }

    {
        what = "zero-size array exception";

        const double  alpha = 0.5;
        const size_t  order = 3;
        valarray< T > x;

        try
        {
            Interpolate< T > interp( alpha, order );
            interp.apply( x );
        }
        catch ( invalid_argument& e )
        {
            const string exp = "LinFilt::apply(): zero length input";

            Test.Check( e.what( ) == exp ) << what << endl;
        }
        catch ( exception& e )
        {
            Test.Check( false ) << what << ": " << e.what( ) << endl;
        }
        catch ( ... )
        {
            Test.Check( false ) << what << ": Unknown exception" << endl;
        }
    }
}

template < class T >
void
TestInterpolateMembers( )
{
    string what;

    {
        what = "getAlpha()";

        const double alpha = 0.1;
        const size_t order = 2;

        Interpolate< T > interp( alpha, order );

        Test.Check( interp.getAlpha( ) == alpha ) << what << endl;
    }

    {
        what = "getOrder()";

        const double alpha = 0.6;
        const size_t order = 5;

        Interpolate< T > interp( alpha, order );

        Test.Check( interp.getOrder( ) == order ) << what << endl;
    }

    {
        what = "Copy constructor";

        const double alpha = 0.6;
        const size_t order = 5;

        Interpolate< T > interp1( alpha, order );
        Interpolate< T > interp2( interp1 );

        bool pass = true;
        pass = pass && ( interp1.getAlpha( ) == interp2.getAlpha( ) );
        pass = pass && ( interp1.getOrder( ) == interp2.getOrder( ) );

        Test.Check( pass ) << what << endl;
    }

    {
        what = "Assignment operator";

        const double alpha = 0.6;
        const size_t order = 5;

        Interpolate< T > interp1( alpha, order );
        Interpolate< T > interp2( alpha * 0.5, order + 2 );

        interp2 = interp1;

        bool pass = true;
        pass = pass && ( interp1.getAlpha( ) == interp2.getAlpha( ) );
        pass = pass && ( interp1.getOrder( ) == interp2.getOrder( ) );

        Test.Check( pass ) << what << endl;
    }
}

template < class T >
void
TestSplitInterpolate( const double alpha, const int order, double& maxAbs )
{
    Test.Message( ) << "Order " << order
                    << " split interpolation, alpha = " << alpha << endl;

    const size_t n = 5 * ( order + 1 );

    valarray< T > x( 0.0, n );

    generate( std::begin( x ), std::end( x ), Randint( 1000 ) );

    // Store the result of interpolating the whole dataset
    valarray< T > y0( x );

    {
        Interpolate< T > interp( alpha, order );
        interp.apply( y0 );
    }

    bool pass = true;
    bool sizeCheck = true;

    // Break up the input into 3 parts
    for ( size_t k0 = 1; k0 < n - 2; ++k0 )
    {
        for ( size_t k1 = 1; k1 < n - k0 - 1; ++k1 )
        {
            for ( size_t k2 = 1; k2 < n - k0 - k1; ++k2 )
            {
                const valarray< T >& xconst( x );
                const slice          s0( 0, k0, 1 );
                const slice          s1( k0, k1, 1 );
                const slice          s2( k0 + k1, k2, 1 );

                valarray< T > x0( xconst[ s0 ] );
                valarray< T > x1( xconst[ s1 ] );
                valarray< T > x2( xconst[ s2 ] );

                Interpolate< T > interp( alpha, order );

                interp.apply( x0 );
                interp.apply( x1 );
                interp.apply( x2 );

                const size_t x0size = x0.size( );
                const size_t x1size = x1.size( );
                const size_t x2size = x2.size( );

                sizeCheck = sizeCheck && ( x0size == k0 );
                sizeCheck = sizeCheck && ( x1size == k1 );
                sizeCheck = sizeCheck && ( x2size == k2 );

                valarray< T > y( x0size + x1size + x2size );

                y[ slice( 0, x0.size( ), 1 ) ] = x0;
                y[ slice( x0.size( ), x1.size( ), 1 ) ] = x1;
                y[ slice( x0.size( ) + x1.size( ), x2.size( ), 1 ) ] = x2;

                for ( size_t k = 0; k < y.size( ); k++ )
                {
                    // if (y0[k] != y[k]) pass = false;
                    const double Abs = abs( y0[ k ] - y[ k ] );
                    if ( Abs > Eps< T >( ) )
                    {
                        pass = false;
                    }

                    if ( Abs > maxAbs )
                    {
                        maxAbs = Abs;
                    }

#if INTERP_DEBUG
                    if ( !pass )
                    {
                        cerr << y0[ k ] << "   " << y[ k ] << "   " << Abs
                             << endl;
                        goto escape_hatch;
                    }
#endif
                }
            }
        }
    }

#if INTERP_DEBUG
escape_hatch:
#endif

    Test.Check( sizeCheck ) << "Split sizes are correct" << endl;
    Test.Check( pass ) << "Data correct" << endl;
}

// Order 1 should give identity sequence no matter what
template < class T >
void
TestInterpolateOrderZero( const double alpha )
{
    const int    order = 0;
    const size_t n = 13;

    valarray< T > y( 0.0, n );
    generate( std::begin( y ), std::end( y ), Randint( 1000 ) );

    const valarray< T > x( y );

    Interpolate< T > interp( alpha, order );

    interp.apply( y );

    bool pass = true;
    for ( size_t k = 0; k < y.size( ); ++k )
    {
        pass = pass && ( x[ k ] == y[ k ] );
    }

    Test.Check( pass ) << "Order " << order
                       << " interpolation, alpha = " << alpha << endl;
}

template < class T >
void
TestInterpolateAnyOrder( const double alpha, const int order, double& maxAbs )
{
    const size_t n = 3 * ( order + 1 );
    const T      dx = 0.1;

    // There is a filter offset because the "real" interpolation is
    // acausal but we're using a causal filter
    const int     filtOffset = ( order + 1 ) / 2;
    valarray< T > y( 0.0, n );

    for ( size_t k = 0; k < y.size( ); ++k )
    {
        const T tmp = k;
        y[ k ] = poly<>( tmp * dx, order );
    }

    const valarray< T > x( y );

    valarray< T > exp_array( 0.0, n );

    for ( size_t k = 0; k < exp_array.size( ); ++k )
    {
        const T tmp = k + alpha - filtOffset;
        exp_array[ k ] = poly< T >( tmp * dx, order );
    }

    Interpolate< T > interp( alpha, order );

    interp.apply( y );

    bool pass = true;

    // The first few points of the output will be wrong because the data
    // before the first sample is taken to be zero, so we ignore them

    double eps( Eps< T >( ) );
    for ( size_t k = order; k < y.size( ); ++k )
    {
        const double Abs = abs( y[ k ] - exp_array[ k ] );
        pass = pass && ( Abs < eps );

        if ( Abs > maxAbs )
        {
            maxAbs = Abs;
        }

#if INTERP_DEBUG
        cerr << "x[" << setw( 2 ) << k << "] = " << setw( 5 ) << x[ k ] << "  "
             << "y[" << setw( 2 ) << k << "] = " << setw( 5 ) << y[ k ] << "  "
             << "exp[" << setw( 2 ) << k << "] = " << setw( 5 ) << exp[ k ]
             << "  "
             << "diff = " << abs( y[ k ] - exp[ k ] ) << endl;
#endif
    }

    Test.Check( pass ) << "Order " << order
                       << " interpolation, alpha = " << alpha << endl;
}

template < class T >
void
TestInterpolate( )
{
    double maxAbs = 0.0;

    // Order 0 is a bit special
    TestInterpolateOrderZero< T >( 0.1 );
    TestInterpolateOrderZero< T >( 0.5 );
    TestInterpolateOrderZero< T >( 0.99 );

    TestSplitInterpolate< T >( 0.3, 0, maxAbs );

    // Number of alphas to test
    const int maxA = 5;
    const int maxOrder = 8;

    for ( int a = 1; a < maxA; ++a )
    {
        const double alpha = double( a ) / maxA;
        for ( int order = 1; order <= maxOrder; ++order )
        {
            TestInterpolateAnyOrder< T >( alpha, order, maxAbs );
        }
    }

    for ( int a = 1; a < maxA; ++a )
    {
        const double alpha = double( a ) / maxA;
        for ( int order = 1; order <= maxOrder; ++order )
        {
            TestSplitInterpolate< T >( alpha, order, maxAbs );
        }
    }

    Test.Message( ) << "Max. difference was " << setw( 16 ) << maxAbs << endl;
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

    if ( Test.IsVerbose( ) )
    {
        cout << "$Id: tInterpolate.cc,v 1.9 2005/11/15 18:35:19 emaros Exp $"
             << std::endl
             << std::flush;
    }

    try
    {

        // Tests for the non-member function(s)

        Test.Message( ) << "TestFilterExceptions<T>" << endl;
        TestFilterExceptions< double >( );

        Test.Message( ) << "TestFilterCoeffs<T>" << endl;
        TestFilterCoeffs< double >( 0.9, 1 );
        TestFilterCoeffs< double >( 0.7, 2 );
        TestFilterCoeffs< double >( 0.5, 3 );
        TestFilterCoeffs< double >( 0.3, 4 );
        TestFilterCoeffs< double >( 0.1, 5 );
        TestFilterCoeffs< double >( 0.05, 6 );
        TestFilterCoeffs< double >( 0.5, 10 );

        // Tests for the Interpolate class

        Test.Message( ) << "TestInterpolateExceptions<float>" << endl;
        TestInterpolateExceptions< float >( );

        Test.Message( ) << "TestInterpolateMembers<float>" << endl;
        TestInterpolateMembers< float >( );

        Test.Message( ) << "TestInterpolate<float>" << endl;
        TestInterpolate< float >( );

        Test.Message( ) << "TestInterpolate<double>" << endl;
        TestInterpolate< double >( );

        Test.Message( ) << "TestInterpolate<complex<float> >" << endl;
        TestInterpolate< complex< float > >( );

        Test.Message( ) << "TestInterpolate<complex<double> >" << endl;
        TestInterpolate< complex< double > >( );
    }
    catch ( exception& e )
    {
        Test.Check( false ) << "Unexpected exception: " << e.what( ) << endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << "Unexpected exception: Unknown exception" << endl;
    }

    Test.Exit( );
}
