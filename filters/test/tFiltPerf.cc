//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <valarray>
#include <stdexcept>
#include <complex>

#include "ldastoolsal/unittest.h"
#include "filters/LinFilt.hh"

using namespace std;
using namespace Filters;

// Return processor time in seconds
inline double
dtime( )
{
    return (double)( clock( ) ) / ( (double)CLOCKS_PER_SEC );
}

const int timing_iters = 10;

LDASTools::Testing::UnitTest Test;

// Amount of CPU time in which the calculation must complete
const double TOLERANCE( 4.0 );

//
// Time linear filter for a particular length n
//
template < class TCoeffs, class TIn >
void
TestFilter( const size_t n )
{
    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;
    const valarray< TCoeffs >                               b( 1.0, 50 );

    LinFilt< TCoeffs, TIn > f( b );

    valarray< TIn >  in( n );
    valarray< TOut > out( n );
    for ( size_t i = 0; i < in.size( ); i++ )
    {
        in[ i ] = 2000.0 * drand48( ) - 1000.0;
    }

    double start, end, res_time;

    start = dtime( );

    for ( int i = 0; i < timing_iters; i++ )
    {
        f.apply( out, in );
    }

    end = dtime( );

    res_time = ( end - start ) / timing_iters;

    Test.Check( res_time < TOLERANCE )
        << "Filter time for " << n << "\t" << res_time << std::endl;
}

int
main( int argc, char** argv )
{
    Test.Init( argc, argv );

    const size_t maxN = 1048576;

    try
    {
        // Powers of 2
        size_t n = 1;
        try
        {
            n = 2;
            Test.Message( ) << "Single precision real" << std::endl;
            while ( n <= maxN )
            {
                TestFilter< float, float >( n );
                n *= 2;
            }
        }
        catch ( std::bad_alloc& e )
        {
            Test.Check( true )
                << "Insufficient memory to do test of size " << n << std::endl;
        }
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << "Unhandled exception" << e.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false ) << "Unhandled exception" << std::endl;
    }

    Test.Exit( );
}
