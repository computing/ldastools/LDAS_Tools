//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// $Id: tResample.cc,v 1.27 2006/11/07 22:25:47 emaros Exp $

#include <filters_config.h>

#include <typeinfo>
#include <cmath>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <complex>

#include "ldastoolsal/unittest.h"

#include "filters/Resample.hh"
#include "filters/gcd.hh"
#include "filters/FIRLP.hh"
#include "filters/KaiserWindow.hh"
#include "filters/LDASConstants.hh"

using namespace std;
using namespace Filters;
using namespace LDASTools::Testing;

template < class T >
class Rand
{
public:
    Rand( )
    {
    }

    T
    operator( )( )
    {
        return rand( ) % 2000;
    }

private:
    int m_max;
};

template <>
complex< float >
Rand< complex< float > >::operator( )( )
{
    return complex< float >( rand( ) % 2000, rand( ) % 2000 );
}

template <>
complex< double >
Rand< complex< double > >::operator( )( )
{
    return complex< double >( rand( ) % 2000, rand( ) % 2000 );
}

UnitTest Test;

const size_t gInlen = 50;

const double gIndata[ gInlen ] = {
    -1.128343864320229,  -1.349277543102495,   -0.261101623061621,
    0.9534654455048185,  0.128644430046645,    0.656467513885396,
    -1.167819364726639,  -0.4606051795061504,  -0.2624399528383327,
    -1.213152068493907,  -1.319436998109537,   0.9312175149954361,
    0.01124489638413373, -0.6451458156911702,  0.8057287931123757,
    0.2316260107804365,  -0.9897596716820042,  1.339585700610388,
    0.2895020345384132,  1.478917057681278,    1.138028012858371,
    -0.6841385851363396, -1.291936044965938,   -0.07292627626364673,
    -0.3305988798927643, -0.8436276391547997,  0.4977696641827825,
    1.488490470903483,   -0.5464758947676226,  -0.8467581638830595,
    -0.2463365280848998, 0.6630241458559077,   -0.8541973744689799,
    -1.201314815339041,  -0.1198694280573872,  -0.06529401484158653,
    0.4852955559165439,  -0.5954909026194759,  -0.1496677438244753,
    -0.4347519311525334, -0.07933022302342058, 1.535152266122148,
    -0.6064828592772656, -1.34736267385024,    0.46938311986633,
    -0.9035669426177764, 0.03587963872947693,  -0.6275312199668315,
    0.535397954249106,   0.552883517423822
};

template < class T >
inline void
transdata( const double* Input, std::valarray< T >& Out )
{
  copy( Input, Input + ( Out.size( ) * sizeof( double ) ), &Out[ 0 ] );
}

template <>
inline void
transdata( const double* Input, std::valarray< complex< float > >& Out )
{
    for ( size_t x = 0, end = Out.size( ); x != end; ++x )
    {
        Out[ x ] = std::complex< float >( Input[ x ], 0.0 );
    }
}

void
mkpath( std::string& path )
{
    const char* const datadir = getenv( "DATADIR" );
    if ( datadir != 0 )
        path = datadir;
    else
        path = ".";
}

template < class TIn >
void
SplitInputTest( const valarray< TIn >& x, int p, int q, double eps )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    bool valpass = true;
    bool sizepass = true;

    Test.Message( ) << "Resample(" << p << ", " << q
                    << ") 3-way split input test" << endl;

    Test.Check( x.size( ) % q == 0 )
        << "Input length is a multiple of q" << endl;

    const size_t nSegs = x.size( ) / q;

    // Generate data to check against
    valarray< TOut > y0;

    Resample< TIn >( p, q ).apply( y0, x );

    for ( size_t k0 = 1; k0 < nSegs - 2; ++k0 )
    {
        for ( size_t k1 = 1; k1 < nSegs - k0 - 1; ++k1 )
        {
            for ( size_t k2 = 1; k2 < nSegs - k0 - k1; ++k2 )
            {
                const slice s0( 0, q * k0, 1 );
                const slice s1( q * k0, q * k1, 1 );
                const slice s2( q * ( k0 + k1 ), q * k2, 1 );

                valarray< TIn > x0 = x[ s0 ];
                valarray< TIn > x1 = x[ s1 ];
                valarray< TIn > x2 = x[ s2 ];

                valarray< TOut > ytmp;

                valarray< TOut > y( p * ( k0 + k1 + k2 ) );

                Resample< TIn > r( p, q );

                r.apply( ytmp, x0 );
                sizepass = sizepass && ( ytmp.size( ) == p * x0.size( ) / q );
                y[ slice( 0, p * k0, 1 ) ] = ytmp;

                r.apply( ytmp, x1 );
                sizepass = sizepass && ( ytmp.size( ) == p * x1.size( ) / q );
                y[ slice( p * k0, p * k1, 1 ) ] = ytmp;

                r.apply( ytmp, x2 );
                sizepass = sizepass && ( ytmp.size( ) == p * x2.size( ) / q );
                y[ slice( p * ( k0 + k1 ), p * k2, 1 ) ] = ytmp;

                for ( size_t k = 0; k < y.size( ); ++k )
                {
                    // For some reason, general resampling has a couple of
                    // values that are just slightly off.
                    if ( abs( y0[ k ] - y[ k ] ) > eps )
                    {
                        valpass = false;
                        goto escape;
                    }
                }
            }
        }
    }

escape:

    Test.Check( sizepass ) << "3-way split output size" << std::endl;

    Test.Check( valpass ) << "3-way split output values" << std::endl;
}

template < class TIn >
void
Nominal( )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    string what = "Resample nominal test";

    try
    {
#if 0
    {
      what = "Resample: Initialize trivial no-sample";
      Resample<TIn> down0(1, 1);
      Resample<TIn> down1(3, 3);
      Resample<TIn> down2(100, 100);
      down0.reset();
      down1.reset();
      down2.reset();
      Test.Check(true, what);
    }
#endif /* 0 */

#if 0
    {
      what = "Resample: Initialize downsample";
      Resample<TIn> down2(1, 2);
      Resample<TIn> down3(1, 3, 10);
      Resample<TIn> down4(1, 4, 10, 3);
      Test.Check(true, what);
    }
#endif /* 0 */

#if 0
    {
      what = "Resample: Initialize upsample";
      Resample<TIn> up(3, 1);
      Resample<TIn> up2(4, 1, 20);
      Resample<TIn> up3(5, 1, 20, 3);
      Test.Check(true, what);
    }
#endif /* 0 */

#if 0
    {
      what = "Resample: Initialize general resample";
      Resample<TIn> gen(3, 2);
      Resample<TIn> gen2(5, 3, 15);
      Resample<TIn> gen3(7, 4, 15, 3);

      // Generates p*q divisibility bug
      Resample<TIn> gen4(15, 16, 4);

      Test.Check(true, what);
    }
#endif /* 0 */
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << " caught unexpected exception" << std::endl;
    }

    try
    {
        {
            bool pass = true;
            what = "Resample: copy constructor";
            Resample< TIn > a( 3, 1, 20, 3 );
            Resample< TIn > b( a );

            Test.Check( a == b, what + ": tp: 1" );

            valarray< TIn > in( gInlen );
            transdata( gIndata, in );

            valarray< TOut > outa;
            valarray< TOut > outb;

            a.apply( outa, in );
            transdata( gIndata, in );
            b.apply( outb, in );

            for ( size_t k = 0; pass && ( k < outa.size( ) ); ++k )
            {
                pass = ( outa[ k ] == outb[ k ] );
            }

            Test.Check( pass, what );
        }

        {
            bool pass = true;
            what = "Resample: copy constructor for p == q";
            Resample< TIn > a( 1, 1, 20, 3 );
            Resample< TIn > b( a );

            pass = pass && ( b.getP( ) == 1 );
            pass = pass && ( b.getQ( ) == 1 );
            pass = pass && ( b.getN( ) == 20 );
            pass = pass && ( b.getBeta( ) == 3 );

            valarray< TIn > in( gInlen );
            copy( gIndata, gIndata + gInlen, &in[ 0 ] );

            valarray< TOut > outa;
            valarray< TOut > outb;

            a.apply( outa, in );
            b.apply( outb, in );

            for ( size_t k = 0; k < outa.size( ); ++k )
            {
                pass = pass && ( outa[ k ] == outb[ k ] );
            }

            Test.Check( pass, what );
        }

        {
            bool pass = true;
            what = "Resample: assignment";
            Resample< TIn > a( 3, 1, 20, 3 );
            Resample< TIn > b( 1, 2, 10, 1 );

            b = a;

            pass = pass && ( b.getP( ) == a.getP( ) );
            pass = pass && ( b.getQ( ) == a.getQ( ) );
            pass = pass && ( b.getN( ) == a.getN( ) );
            pass = pass && ( b.getBeta( ) == a.getBeta( ) );
            pass = pass && ( b.getDelay( ) == a.getDelay( ) );

            {
                std::ostringstream w;

                w << what << ": metadaa";
                Test.Check( pass, w.str( ) );
            }

            valarray< TIn > in( gInlen );
            copy( gIndata, gIndata + gInlen, &in[ 0 ] );

            valarray< TOut > outa;
            valarray< TOut > outb;

            a.apply( outa, in );
            b.apply( outb, in );

            for ( size_t k = 0; k < outa.size( ); ++k )
            {
                pass = pass && ( outa[ k ] == outb[ k ] );
            }

            Test.Check( pass, what );
        }

        {
            bool pass = true;
            what = "Resample: assignment for p == q";
            Resample< TIn > a( 1, 1, 20, 3 );
            Resample< TIn > b( 1, 2, 10, 1 );

            b = a;

            pass = pass && ( b.getP( ) == a.getP( ) );
            pass = pass && ( b.getQ( ) == a.getQ( ) );
            pass = pass && ( b.getN( ) == a.getN( ) );
            pass = pass && ( b.getBeta( ) == a.getBeta( ) );
            pass = pass && ( b.getDelay( ) == a.getDelay( ) );

            valarray< TIn > in( gInlen );
            copy( gIndata, gIndata + gInlen, &in[ 0 ] );

            valarray< TOut > outa;
            valarray< TOut > outb;

            a.apply( outa, in );
            b.apply( outb, in );

            for ( size_t k = 0; k < outa.size( ); ++k )
            {
                pass = pass && ( outa[ k ] == outb[ k ] );
            }

            Test.Check( pass, what );
        }
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << " caught unexpected exception" << std::endl;
    }

    std::valarray< TIn > data( 100 );
    for ( size_t i = 0; i < data.size( ); i++ )
    {
        data[ i ] = 0.01 * i;
    }

    valarray< TIn > in( gInlen );
    copy( gIndata, gIndata + gInlen, &in[ 0 ] );

    what = "Resample: Apply nominal values for trivial resampling ";
    Test.Message( ) << "Reached: " << what << std::endl;
    try
    {
        const int        p = 1;
        const int        q = 1;
        valarray< TOut > results( 1 );
        Resample< TIn >  none( p, q, 21, 3 );
        none.apply( results, data );
        what = "Resample: Length of trivially results match expected value ";
        Test.Check( results.size( ) == data.size( ), what );
        what = "Resample: filter delay is zero";
        Test.Check( none.getDelay( ) == 0 ) << what << endl;

        valarray< double > b;
        none.getB( b );
        Test.Check( b.size( ) == 1 )
            << "Filter for trivial resample has correct size" << endl;
        Test.Check( b[ 0 ] == 1 )
            << "Filter for trivial resample has correct values" << endl;

        bool pass = true;
        for ( size_t k = 0; k < data.size( ); ++k )
        {
            if ( results[ k ] != data[ k ] )
            {
                pass = false;
            }
        }
        Test.Check( pass ) << "Trivially resampled output values are correct"
                           << endl;

        // Zero tolerance in this test
        SplitInputTest< TIn >( in, p, q, 0.0 );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Message( ) << "Unexpected exception caught.\n";
    }

    what = "Resample: Apply nominal values for upsampling ";
    Test.Message( ) << "Reached: " << what << std::endl;
    try
    {
        const int        p = 3;
        const int        q = 1;
        valarray< TOut > results( 1 );
        Resample< TIn >  up( p, q, 21, 3 );
        up.apply( results, data );
        what = "Resample: Length of upsampled results match expected value ";
        Test.Check( results.size( ) == p * data.size( ) / q, what );
        what = "Resample: filter delay is location of center tap";
        Test.Check( up.getDelay( ) == 63 )
            << what + "(63 == " << up.getDelay( ) << ")" << std::endl;

        SplitInputTest< TIn >( in, p, q, 1.0e-06 );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Message( ) << "Unexpected exception caught.\n";
    }

    what = "Resample: Apply nominal values for downsampling ";
    Test.Message( ) << "Reached: " << what << std::endl;
    try
    {
        const int    p = 1;
        const int    q = 2;
        const size_t outlen = p * gInlen / q;
        TOut         outdata[ outlen ] = {
            -1.094218403069907,  -0.2627637090311222, 0.6319774553307926,
            -0.4838240901740802, -0.7774780586019776, -0.6345722799518843,
            0.1665873621425175,  0.1311427593910666,  -0.09475058630536674,
            1.087310562537239,   0.6154304955631067,  -0.7718465603710458,
            -0.5618685645044653, 0.5279103330258238,  -0.03812595386848678,
            -0.336518856979053,  -0.3123743459525898, -0.7337082652705581,
            0.4795124144568211,  -0.7966037593081549, 0.6057757901304369,
            -0.2742406925478539, -0.4903496430027017, -0.3286871614132139,
            0.2165056008274293
        };

        const valarray< TOut > out( outdata, outlen );
        valarray< TOut >       results;
        Resample< TIn >        down( p, q, 10, 3 ); // p, q, n, beta
        down.apply( results, in );

        what = "Resample: Length of downsampled results match expected value ";
        Test.Check( results.size( ) == p * in.size( ) / q, what );

        what = "Resample: Delay of downsampled data matches expected value ";
        Test.Check( down.getDelay( ) == 10 )
            << what + "(10 == " << down.getDelay( ) << ")" << std::endl;

#if 0
    what = "Resample: Values of downsampled data agree with Matlab";
    bool pass = true;
    const int delay = (int)down.getDelay();
    const size_t NN = std::min(delay, (int)out.size());

    for (size_t k = NN; k < out.size(); ++k)
    {
      std::cerr << "GDEBUG: results[" << k << "] = " << results[k]
		<< " out[" << k << "] = " << out[k]
		<< " abs: " << ( abs(results[k] - out[k]) )
		<< std::endl;
      pass = pass && (abs(results[k] - out[k]) < 1.0e-6);
    }

    Test.Check(pass) << what << endl;
#endif

        SplitInputTest< TIn >( in, p, q, 1.0e-06 );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Message( ) << "Unexpected exception caught.\n";
    }

    what = "Resample: Apply nominal values for general resampling ";
    Test.Message( ) << "Reached: " << what << std::endl;
    try
    {
        const int        p = 7;
        const int        q = 5;
        const int        n = 29;
        valarray< TOut > results( 1 );
        Resample< TIn >  gen( p, q, n, 3 );
        gen.apply( results, data );
        what = "Resample: Length of general resampled results match expected "
               "value ";
        Test.Check( results.size( ) == data.size( ) * p / q, what );
        what =
            "Resample: Delay of general resampled data matches expected value ";
        double delay = (double)n * std::max( p, q ) / q;
        Test.Check( gen.getDelay( ) == delay )
            << what + "(" << delay << "==" << gen.getDelay( ) << ")"
            << std::endl;

        // For some reason, general resampling has a tiny error
        SplitInputTest< TIn >( in, p, q, 1.0e-6 );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Message( ) << "Unexpected exception caught.\n";
    }

    what = "Resample: Apply nominal values from Resample";
    Test.Message( ) << "Reached: " << what << std::endl;
    try
    {
        const int        p = 2;
        const int        q = 5;
        const int        n = 9;
        valarray< TOut > results( 1 );
        Resample< TIn >  res0( p, q, n, 3 );
        Resample< TIn >  res( res0 );
        res.apply( results, data );
        what = "Resample: Length of general resampled results match expected "
               "value ";
        Test.Check( results.size( ) == data.size( ) * p / q, what );
        what =
            "Resample: Delay of general resampled data matches expected value ";
        double delay = (double)n * std::max( p, q ) / q;
        Test.Check( res.getDelay( ) == delay )
            << what + "(" << delay << "==" << res.getDelay( ) << ")"
            << std::endl;
        // For some reason, general resampling has a tiny error
        SplitInputTest< TIn >( in, p, q, 1.0e-6 );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Message( ) << "Unexpected exception caught.\n";
    }
}

template < class TIn >
void
Exceptions( )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    std::string what = " P < 1 test";
    try
    {
        Resample< TIn > test( 0, 2, 20, 3 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& u )
    {
        Test.Check( true ) << what << "(" << u.what( ) << ")" << std::endl;
    }
    catch ( std::exception& u )
    {
        Test.Check( false ) << what << "caught unexpected std::exception ("
                            << u.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }

    what = "Resample: Q cannot be less than 1 ";
    try
    {
        Resample< TIn > test( 2, 0, 20, 3 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& u )
    {
        Test.Check( true ) << what << "(" << u.what( ) << ")" << std::endl;
    }
    catch ( std::exception& u )
    {
        Test.Check( false ) << what << "caught unexpected std::exception ("
                            << u.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }

    what = "Resample: N cannot be less than 1 ";
    try
    {
        const int       zero = 0;
        Resample< TIn > test( 2, 1, zero, 3 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& u )
    {
        Test.Check( true ) << what << "(" << u.what( ) << ")" << std::endl;
    }
    catch ( std::exception& u )
    {
        Test.Check( false ) << what << "caught unexpected std::exception ("
                            << u.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }

    what = "Resample: Beta cannot be less than 0 ";
    try
    {
        Resample< TIn > test( 2, 1, 20, -3.0 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& u )
    {
        Test.Check( true ) << what << "(" << u.what( ) << ")" << std::endl;
    }
    catch ( std::exception& u )
    {
        Test.Check( false ) << what << "caught unexpected std::exception ("
                            << u.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }

    what = "input divisible by q ";
    try
    {
        valarray< TIn > data( 1.0, 20 );
        Resample< TIn > test( 1, 3, 50, 4 );
        try
        {
            valarray< TOut > results;
            test.apply( results, data );
            Test.Check( false, what );
        }
        catch ( std::logic_error& u )
        {
            Test.Check( true ) << what << "(" << u.what( ) << ")" << std::endl;
        }
    }
    catch ( std::invalid_argument& u )
    {
        Test.Check( false ) << what << "(" << u.what( ) << ")" << std::endl;
    }
    catch ( std::exception& u )
    {
        Test.Check( false ) << what << "caught unexpected std::exception ("
                            << u.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }

    what = "apply method input";
    try
    {
        const valarray< TIn > in;

        valarray< TOut > out;

        Resample< TIn > exper( 1, 3 );
        exper.apply( out, in );
    }
    catch ( std::invalid_argument& ue )
    {
        Test.Check( true ) << what << "(" << ue.what( ) << ")" << std::endl;
    }
    catch ( std::exception& ue )
    {
        Test.Check( false ) << what << "(" << ue.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "Caught unexpected exception." << std::endl;
    }
}

template < class TIn >
void
Accessors( )
{
    Test.Message( ) << "Accessor tests" << std::endl;
    std::string what;

    try
    {
        Resample< TIn > a( 2, 7, 13, 3 );
        int             p = 0, q = 0;
        p = a.getP( );
        q = a.getQ( );
        what = "getP(), getQ()";
        Test.Check( p == 2 ) << what << " p = " << p << std::endl;
        Test.Check( q == 7 ) << what << " q = " << q << std::endl;

        int    n;
        double beta;
        n = a.getN( );
        beta = a.getBeta( );
        what = "getN(), getBeta()";
        Test.Check( n == 13 ) << what << " n = " << n << std::endl;
        Test.Check( beta == 3.0 ) << what << "beta = " << beta << std::endl;
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
        what = "Resample: Get Q of upsample object";
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }
}

template < class TIn >
void
testGetB( int p, int q, const size_t n, const double beta )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    Test.Message( ) << "p = " << p << " "
                    << "q = " << q << " "
                    << "n = " << n << endl;

    const int GCD = Filters::gcd( p, q );

    p /= GCD;
    q /= GCD;

    const double       fc = 1.0 / std::max( p, q );
    const int          nOrder = 2 * n * std::max( p, q );
    valarray< double > b;

    Filters::FIRLP( fc, nOrder, Filters::KaiserWindow( beta ) ).apply( b );

    Resample< TIn > r( p, q, n, beta );

    // Make sure everything is the right type
    valarray< TIn >  in( 10 * p * q );
    valarray< TOut > out;
    ;
    r.apply( out, in );

    // Now retrieve the filter coeffs
    valarray< double > b_out;
    r.getB( b_out );

    // Array may have been padded
    Test.Check( b_out.size( ) >= b.size( ) )
        << "Output filter size is correct" << endl;

    Test.Check( ( b_out.size( ) - 1 ) % ( p * q ) == 0 )
        << "Output filter order is divisible by p and q" << endl;

    bool pass = true;
    for ( size_t k = 0; k < b.size( ); ++k )
    {
        // We allow a very small tolerance, since the complex filter coeffs
        // end up being slightly different
        const double diff = abs( b_out[ k ] - b[ k ] );
        pass = pass && ( diff < 1.0e-12 );
    }

    // Padded values must be zero
    for ( size_t k = b.size( ); k < b_out.size( ); ++k )
    {
        pass = pass && ( b_out[ k ] == 0 );
    }

    Test.Check( pass ) << "Output filter values are correct" << endl;
}

template < class TIn >
void
testGetB( const int p, const int q, const size_t bSize )
{
    Test.Message( ) << "p = " << p << " "
                    << "q = " << q << " "
                    << "bSize = " << bSize << endl;

    valarray< double > b( bSize );

    generate( std::begin( b ), std::end( b ), Rand< double >( ) );

    Resample< TIn > r( p, q, b );

    valarray< double > b_out;
    r.getB( b_out );

    // Array may have been padded
    Test.Check( b_out.size( ) >= b.size( ) )
        << "Output filter size is correct" << endl;

    bool pass = true;
    for ( size_t k = 0; k < b.size( ); ++k )
    {
        pass = pass && ( b[ k ] == b_out[ k ] );
    }

    // Padded values must be zero
    for ( size_t k = b.size( ); k < b_out.size( ); ++k )
    {
        pass = pass && ( b_out[ k ] == 0 );
    }

    Test.Check( pass ) << "Output filter values are correct" << endl;
}

template < class TIn >
void
testGetB( )
{
    for ( int p = 1; p < 5; ++p )
    {
        for ( int q = 1; q < 5; ++q )
        {
            if ( p == q )
            {
                break;
            }

            // Generated coeeffs
            testGetB< TIn >( p, q, 10, 5.0 );
            testGetB< TIn >( p, q, 20, 3.0 );

            // User-supplied coeffs
            for ( size_t m = 1; m < 3; ++m )
            {
                // :NOTE: p and q must both be divisors of n, although this is
                // not documented anywhere!
                const size_t bSize = m * p * q;
                testGetB< TIn >( p, q, bSize );
            }
        }
    }
}

template < class TIn >
void
Copy( )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    std::string what = "Copy object ";

    std::string basename( "tResample100.dat" );
    std::string path;
    mkpath( path );
    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    std::ifstream in( path.c_str( ) );

    if ( !in )
    {
        Test.Check( false )
            << "Couldn't open input file " << path.c_str( ) << endl;
    }

    valarray< TIn > data( 100 );
    int             i = 0;
    while ( in )
    {
        in >> data[ i ];
        i++;
    }

    try
    {
        Resample< TIn > a( 2, 1, 20, 3 );
        Resample< TIn > b( a );

        valarray< TOut > out1, out2;

        a.apply( out1, data );
        b.apply( out2, data );

        Test.Check( out1.size( ) == out2.size( ), what + "result size check" );

        bool test = true;
        for ( size_t i = 0; i < out1.size( ) && test; i++ )
            test = ( out1[ i ] == out2[ i ] );
        Test.Check( test, what + "equality check" );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << what << "(" << e.what( ) << ")" << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << what << "caught unexpected exception" << std::endl;
    }
}

template < class TIn >
void
StateTest( )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    std::string basename( "tResample100.dat" );
    std::string path;
    mkpath( path );
    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    std::ifstream in( path.c_str( ) );

    if ( !in )
    {
        Test.Check( false ) << "Couldn't open input file" << endl;
    }

    valarray< TIn > data( 100 );
    for ( int i = 0; in; i++ )
        in >> data[ i ];

    const double eps = 1e-6;

    std::valarray< TOut > r0, r1, r2, r3, r4;

    {
        Resample< TIn > up( 3, 1, 20, 7 );
        Resample< TIn > up2( up );

        up.apply( r0, data );
        up2.apply( r1,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 0, 50, 1 ) ] );
        up2.apply( r2,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 50, 50, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) ) == r0.size( ),
                    "Upsample split input size check" );

        Test.Check( r0.size( ) == 300, "Upsample output size check" );

        r0[ std::slice( 0, 150, 1 ) ] -= r1;
        r0[ std::slice( 150, 150, 1 ) ] -= r2;

        bool test = true;
        for ( int k = 0; k < 300; k++ )
        {
            test = test && abs( r0[ k ] ) < eps;
        }

        Test.Check( test, "Upsample split input value test" );
    }

    {
        unsigned int    Q = 10;
        Resample< TIn > dn( 1, Q, 20, 7 );
        Resample< TIn > dn2( dn );

        dn.apply( r0, data );
        dn2.apply( r1,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 0, 50, 1 ) ] );
        dn2.apply( r2,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 50, 50, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) ) == r0.size( ),
                    "Downsample split input size check" );

        Test.Check( r0.size( ) == 100 / Q, "Downsample output size check" );

        r0[ std::slice( 0, 50 / Q, 1 ) ] -= r1;
        r0[ std::slice( 50 / Q, 50 / Q, 1 ) ] -= r2;

        bool test = true;
        for ( unsigned int k = 0; k < 100 / Q; k++ )
        {
            //    Test.Message() << r0[k] << std::endl;
            test = test && abs( r0[ k ] ) <= eps;
        }
        Test.Check( test, "Downsample split input value test" );
    }

    {
        unsigned int    Q = 5;
        Resample< TIn > dn( 1, Q, 20, 7 );
        Resample< TIn > dn2( dn );

        dn.apply( r0, data );

        dn2.apply( r1,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 0, 25, 1 ) ] );
        dn2.apply( r2,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 25, 25, 1 ) ] );
        dn2.apply( r3,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 50, 25, 1 ) ] );
        dn2.apply( r4,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 75, 25, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) + r3.size( ) + r4.size( ) ) ==
                        r0.size( ),
                    "Downsample split input size check" );

        Test.Check( r0.size( ) == 100 / Q, "Downsample output size check" );

        r0[ std::slice( 0, 25 / Q, 1 ) ] -= r1;
        r0[ std::slice( 25 / Q, 25 / Q, 1 ) ] -= r2;
        r0[ std::slice( 50 / Q, 25 / Q, 1 ) ] -= r3;
        r0[ std::slice( 75 / Q, 25 / Q, 1 ) ] -= r4;

        bool test = true;
        for ( unsigned int k = 0; k < 100 / Q; k++ )
        {
            //    Test.Message() << r0[k] << std::endl;
            test = test && abs( r0[ k ] ) <= eps;
        }
        Test.Check( test, "Downsample split input value test" );
    }

    {
        unsigned int    Q = 10;
        Resample< TIn > gen( 3, Q, 20, 7 );
        Resample< TIn > gen2( gen );

        gen.apply( r0, data );
        gen2.apply( r1,
                    static_cast< const std::valarray< TIn > >(
                        data )[ std::slice( 0, 50, 1 ) ] );
        gen2.apply( r2,
                    static_cast< const std::valarray< TIn > >(
                        data )[ std::slice( 50, 50, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) ) == r0.size( ),
                    "General resample split input size check" );

        Test.Check( r0.size( ) == 300 / Q,
                    "General resample output size check" );

        r0[ std::slice( 0, 150 / Q, 1 ) ] -= r1;
        r0[ std::slice( 150 / Q, 150 / Q, 1 ) ] -= r2;

        bool test = true;
        for ( unsigned int k = 0; k < 300 / Q; k++ )
        {
            //    Test.Message() << r0[k] << std::endl;
            test = test && abs( r0[ k ] ) <= eps;
        }
        Test.Check( test, "General resample split input value test" );
    }
}

template < class PIn >
void
CXStateTest( )
{
    typedef complex< PIn >                          TIn;
    typedef typename ResampleTraits< TIn >::OutType TOut;

    std::string basename( "tResample100.dat" );
    std::string path;
    mkpath( path );
    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    std::ifstream in( path.c_str( ) );

    if ( !in )
    {
        Test.Check( false ) << "Couldn't open input file" << endl;
    }

    valarray< PIn > data0( 100 );
    for ( int i = 0; in; i++ )
        in >> data0[ i ];
    valarray< TIn > data( 50 );
    for ( int i = 0; i < 50; i++ )
        data[ i ] = TIn( data0[ i ], data0[ i + 50 ] );

    double eps = 1.0e-06;

    std::valarray< TOut > r0, r1, r2;

    {
        Resample< TIn > up( 3, 1, 20, 7 );
        Resample< TIn > up2( up );

        up.apply( r0, data );
        up2.apply( r1,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 0, 25, 1 ) ] );
        up2.apply( r2,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 25, 25, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) ) == r0.size( ),
                    "(complex) Upsample split input size check" );

        Test.Check( r0.size( ) == 150, "(complex) Upsample output size check" );

        r0[ std::slice( 0, 75, 1 ) ] -= r1;
        r0[ std::slice( 75, 75, 1 ) ] -= r2;

        bool test = true;
        for ( int k = 0; k < 150; k++ )
        {
            test = test && abs( r0[ k ] ) < eps;
        }

        Test.Check( test, "(complex) Upsample split input value test" );
    }

    {
        unsigned int    Q = 5;
        Resample< TIn > dn( 1, Q, 20, 7 );
        Resample< TIn > dn2( dn );

        dn.apply( r0, data );
        dn2.apply( r1,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 0, 25, 1 ) ] );
        dn2.apply( r2,
                   static_cast< const std::valarray< TIn > >(
                       data )[ std::slice( 25, 25, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) ) == r0.size( ),
                    "(complex) Downsample split input size check " );

        Test.Check( r0.size( ) == 50 / Q,
                    "(complex) Downsample output size check" );

        r0[ std::slice( 0, 25 / Q, 1 ) ] -= r1;
        r0[ std::slice( 25 / Q, 25 / Q, 1 ) ] -= r2;

        bool test = true;
        for ( unsigned int k = 0; k < 50 / Q; k++ )
        {
            //    Test.Message() << r0[k] << std::endl;
            test = test && abs( r0[ k ] ) <= eps;
        }

        Test.Check( test, "(complex) Downsample split input value test" );
    }

    {
        unsigned int    Q = 5;
        Resample< TIn > gen( 6, Q, 20, 7 );
        Resample< TIn > gen2( gen );

        gen.apply( r0, data );
        gen2.apply( r1,
                    static_cast< const valarray< TIn > >(
                        data )[ std::slice( 0, 25, 1 ) ] );
        gen2.apply( r2,
                    static_cast< const valarray< TIn > >(
                        data )[ std::slice( 25, 25, 1 ) ] );

        Test.Check( ( r1.size( ) + r2.size( ) ) == r0.size( ),
                    "(complex) General resample split input size check " );

        Test.Check( r0.size( ) == 300 / Q,
                    "(complex) General resample output size check" );

        r0[ std::slice( 0, 150 / Q, 1 ) ] -= r1;
        r0[ std::slice( 150 / Q, 150 / Q, 1 ) ] -= r2;

        bool test = true;
        for ( unsigned int k = 0; k < 300 / Q; k++ )
        {
            //    Test.Message() << r0[k] << std::endl;
            test = test && abs( r0[ k ] ) <= eps;
        }

        Test.Check( test, "(complex) General resample split input value test" );
    }
}

template < class TIn >
void
ImpulseResponse( int p, int q, int n, double beta )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    try
    {
        Test.Message( ) << "(" << p << ", " << q << ", " << n << ", " << beta
                        << ")" << std::endl;

        std::valarray< TIn > x( 0.0, 2 * n * p * q );
        x[ 0 ] = 1.0;
        Resample< TIn >       rsmpl( p, q, n, beta );
        std::valarray< TOut > y;
        rsmpl.apply( y, x );
        Test.Message( ) << "Filter delay: " << rsmpl.getDelay( ) << std::endl;
        for ( size_t k = (int)( rsmpl.getDelay( ) - 3 );
              k < rsmpl.getDelay( ) + 4;
              k++ )
        {
            Test.Message( ) << k << "     " << y[ k ] << std::endl;
        }
    }
    catch ( std::exception& r )
    {
        Test.Check( false ) << "ImpulseResponse: " << r.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << "ImpulseResponse: unknown exception" << std::endl;
    }
}

template < class PIn >
void
CXImpulseResponse( int p, int q, int n, double beta )
{
    typedef complex< PIn >                          TIn;
    typedef typename ResampleTraits< TIn >::OutType TOut;

    try
    {
        Test.Message( ) << "(" << p << ", " << q << ", " << n << ", " << beta
                        << ")" << std::endl;

        std::valarray< TIn > x( 0.0, 2 * n * std::max( p, q ) );
        x[ 0 ] = TIn( 1.0, 1.0 );
        x[ 0 ] = x[ 0 ] / abs( x[ 0 ] );
        Test.Message( ) << "x[0] = " << x[ 0 ] << std::endl;
        Resample< TIn >       rsmpl( p, q, n, beta );
        std::valarray< TOut > y;
        rsmpl.apply( y, x );
        Test.Message( ) << "Filter delay: " << rsmpl.getDelay( ) << std::endl;
        for ( size_t k = (int)( rsmpl.getDelay( ) - 3 );
              k < rsmpl.getDelay( ) + 4;
              k++ )
        {
            Test.Message( ) << k << "     " << y[ k ] << std::endl;
        }
    }
    catch ( std::exception& r )
    {
        Test.Check( false )
            << "COMPLEX ImpulseResponse: " << r.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false )
            << "COMPLEX ImpulseResponse: unknown exception" << std::endl;
    }
}

template < class TIn >
void
Results( )
{
    typedef typename ResampleTraits< TIn >::OutType TOut;

    const int       n( 300 ), PQ( 9 ), t( 13 );
    Resample< TIn > up( PQ, 1, 21, 15 );
    Resample< TIn > down( 1, PQ, 21, 15 );

    valarray< TIn > a( n );
    for ( int i = 0; i < n; i++ )
        a[ i ] = sin( LDAS_TWOPI * i / t );

    valarray< TOut > r0, r1;
    up.apply( r0, a );
    down.apply( r1, r0 );

    int delay = (int)( ( up.getDelay( ) ) / PQ + down.getDelay( ) );
    Test.Message( ) << "UpDelay = " << up.getDelay( ) << std::endl;
    Test.Message( ) << "DownDelay = " << down.getDelay( ) << std::endl;
    Test.Message( ) << "Delay = " << delay << std::endl;
    int d = delay % t;

    a = a.cshift( -d );

    bool test = true;
    for ( unsigned int i = 2 * delay; i < r1.size( ); i++ )
    {
        // Test.Message() << a[i] << '\t' << r1[i] << std::endl;
        test = test && ( abs( a[ i ] - r1[ i ] ) < 1e-6 );
    }
    std::string what =
        "Resample: Up then downsampled data matches the original";
    Test.Check( test, what );
}

template < class PIn >
void
CXResults( )
{
    typedef complex< PIn >                          TIn;
    typedef typename ResampleTraits< TIn >::OutType TOut;

    const int       n( 300 ), PQ( 9 ), t( 13 );
    Resample< TIn > up( PQ, 1, 21, 20 );
    Resample< TIn > down( 1, PQ, 21, 20 );

    valarray< TIn > z( n );
    double          rhomax = 0;
    for ( int i = 0; i < n; i++ )
    {
        PIn a = sin( LDAS_TWOPI * i / t );
        PIn b = 2 * sin( 1 + LDAS_TWOPI * i / t );
        z[ i ] = TIn( a, b );
        if ( abs( z[ i ] ) > rhomax )
            rhomax = abs( z[ i ] );
    }

    valarray< TOut > r0, r1;
    up.apply( r0, z );
    down.apply( r1, r0 );

    int delay = (int)( ( up.getDelay( ) ) / PQ + down.getDelay( ) );
    Test.Message( ) << "UpDelay = " << up.getDelay( ) << std::endl;
    Test.Message( ) << "DownDelay = " << down.getDelay( ) << std::endl;
    Test.Message( ) << "Delay = " << delay << std::endl;
    int d = delay % t;

    z = z.cshift( -d );

    bool test = true;
    for ( unsigned int i = 2 * delay; i < r1.size( ); i++ )
    {
        // Test.Message() << z[i] << '\t' << r1[i] << '\t'
        //  << abs(z[i] - r1[i]) << std::endl;
        test = test && ( abs( z[ i ] - r1[ i ] ) < rhomax * 1e-6 );
    }
    std::string what =
        "Resample: Up then downsampled COMPLEX data matches the original";
    Test.Check( test, what );
}

template < class T >
void
FilterTest( )
{

    // tests the initFilter overloaded function through the
    // overloaded ResampleState and Resample constructors

    // create an impulse {0,0,1,0,0,0,0,0}
    valarray< T > impulse( 0.0, 8 );
    impulse[ 2 ] = 1.0;

    // input filter {1/2, 1/2}; Haar highpass
    valarray< double > b( 0.5, 2 );

    // upsample factor
    const int p = 1;

    // downsample factor
    const int q = 2;

    Resample< T > test( p, q, b );

    {
        // test filter recovery
        bool                    pass = true;
        std::valarray< double > got_b;

        test.getB( got_b );

        Test.Check( got_b.size( ) >= b.size( ) )
            << "getB: Recovered filter is a compatible size" << std::endl;

        for ( unsigned i = 0; i < b.size( ); ++i )
        {
            pass &= ( got_b[ i ] == b[ i ] );
        }

        for ( unsigned i = b.size( ); i < got_b.size( ); ++i )
        {
            pass &= ( got_b[ i ] == 0 );
        }

        Test.Check( pass ) << "getB: Recovered filter has identical elements"
                           << std::endl;
    }

    // create an output
    valarray< T > out( 4 );

    // apply the filter to the impulse and downsample
    test.apply( out, impulse );

    // output
    if ( Test.IsVerbose( ) )
    {
        Test.Message( ) << "The impulse is:  ";
        for ( size_t i = 0; i < impulse.size( ); i++ )
        {
            Test.Message( ) << impulse[ i ] << std::endl;
            ;
        }

        Test.Message( ) << "The filter is:  ";
        for ( size_t i = 0; i < b.size( ); i++ )
        {
            Test.Message( ) << b[ i ] << std::endl;
        }

        Test.Message( ) << "The response is:  ";
        for ( size_t i = 0; i < out.size( ); i++ )
        {
            Test.Message( ) << out[ i ] << std::endl;
            ;
        }
    }

    // expected filtered output
    valarray< T > expected( 0.0, 4 );
    expected[ 1 ] = 0.5;

    // find and display difference between expected and actual
    std::valarray< T > difference = expected - out;

    if ( Test.IsVerbose( ) )
    {
        Test.Message( ) << "The difference between expected and actual is:  ";
        for ( size_t i = 0; i < difference.size( ); i++ )
        {
            Test.Message( ) << difference[ i ] << std::endl;
        }
    }

    // test to see if output is correct
    std::valarray< bool > check( expected == out );

    bool pass = true;
    for ( size_t i = 0; i < check.size( ); i++ )
    {
        pass = pass && check[ i ];
    }

    Test.Check( pass ) << "FilterTest filtered output" << std::endl;

} // end FilterTest

template < class T >
void
GeneralDownTest( const int p, const int q )
{
    Test.Message( ) << "GeneralDownTest: p = " << p << ", q = " << q << endl;

    // Sanity checks
    Test.Check( p < q ) << "p < q" << endl;
    Test.Check( p != 1 ) << "p != 1" << endl;

    const int n = 1000 * q;

    //
    // Add some sinusoids and check that they're still present after
    // downsampling.
    //
    // Downsampling by p/q should introduce a cutoff at (p/q)*Nyq, but
    // there is already much attenuation here so check with 85% of
    // the cutoff frequency.
    //
    const double Nyq = n / 2.0;
    const double freq2 = 0.85 * p / q * Nyq;
    const double freq1 = 2 * freq2 / 3;
    const double freq0 = 1 * freq2 / 3;

    valarray< T > a0( 0.0, n );
    for ( size_t i = 0; i < a0.size( ); i++ )
    {
        a0[ i ] = sin( ( LDAS_TWOPI * freq0 * i ) / n );
        a0[ i ] += sin( ( LDAS_TWOPI * freq1 * i ) / n );
        a0[ i ] += sin( ( LDAS_TWOPI * freq2 * i ) / n );
    }

    valarray< T > b;
    Resample< T > gen( p, q, 99, 15 );
    gen.apply( b, a0 );
    double delay = gen.getDelay( );

    Test.Message( ) << "Delay = " << delay << std::endl;
    Test.Check( b.size( ) == ( size_t )( n * p ) / q,
                "Output has correct size" );

    valarray< T > a1( 0.0, ( n * p ) / q );
    for ( size_t i = 0; i < a1.size( ); i++ )
    {
        a1[ i ] = sin( ( LDAS_TWOPI * freq0 * ( i - delay ) * q ) / ( p * n ) );
        a1[ i ] +=
            sin( ( LDAS_TWOPI * freq1 * ( i - delay ) * q ) / ( p * n ) );
        a1[ i ] +=
            sin( ( LDAS_TWOPI * freq2 * ( i - delay ) * q ) / ( p * n ) );
    }

    double maxAbs = 0.0;
    bool   test = true;
    for ( size_t i = 2 * ( ( size_t )( delay + 0.5 ) ); i < b.size( ); i++ )
    {
        const double Abs = abs( b[ i ] - a1[ i ] );
        if ( Abs > maxAbs )
            maxAbs = Abs;

        test = test && ( Abs < 1.0e-05 );
#if 0
    Test.Message() << "i = " << i << "   " 
		   << a1[i] << "    " << b[i] << std::endl;
#endif
    }

    Test.Check( test, "Resampled data matches stretched/compressed original" );
}

template < class T >
void
GeneralUpTest( const int p, const int q )
{
    Test.Message( ) << "GeneralUpTest: p = " << p << ", q = " << q << endl;

    // Sanity checks
    Test.Check( p > q ) << "p > q" << endl;
    Test.Check( q != 1 ) << "q != 1" << endl;

    const int n = 1000 * q;

    //
    // Add some sinusoids and check that they're still present after
    // downsampling.
    //
    // Upsampling by p/q should introduce a cutoff at (p/q)*Nyq, but
    // there should be no frequency content above the original Nyquist anyway.
    //
    const double Nyq = n / 2.0;
    const double freq2 = 0.85 * Nyq;
    const double freq1 = 2 * freq2 / 3;
    const double freq0 = 1 * freq2 / 3;

    valarray< T > a0( 0.0, n );
    for ( size_t i = 0; i < a0.size( ); i++ )
    {
        a0[ i ] = sin( ( LDAS_TWOPI * freq0 * i ) / n );
        a0[ i ] += sin( ( LDAS_TWOPI * freq1 * i ) / n );
        a0[ i ] += sin( ( LDAS_TWOPI * freq2 * i ) / n );
    }

    valarray< T > b;

    // :NOTE: Works only for integer delay - want delay to be >= 99
    // and a multiple of q so that delay is integer
    int    nOrder = 0;
    double delay = 0.0;
    while ( delay < 99 )
    {
        nOrder += q;
        delay = nOrder * max( p, q ) / double( q );
    }

    Resample< T > gen( p, q, nOrder, 15 );
    gen.apply( b, a0 );
    delay = gen.getDelay( );

    Test.Message( ) << "Delay = " << delay << std::endl;
    Test.Check( b.size( ) == ( size_t )( n * p ) / q,
                "Output has correct size" );

    valarray< T > a1( 0.0, ( n * p ) / q );
    for ( size_t i = 0; i < a1.size( ); i++ )
    {
        a1[ i ] = sin( ( LDAS_TWOPI * freq0 * ( i - delay ) * q ) / ( p * n ) );
        a1[ i ] +=
            sin( ( LDAS_TWOPI * freq1 * ( i - delay ) * q ) / ( p * n ) );
        a1[ i ] +=
            sin( ( LDAS_TWOPI * freq2 * ( i - delay ) * q ) / ( p * n ) );
    }

    double maxAbs = 0.0;
    bool   test = true;
    for ( size_t i = 2 * ( ( size_t )( delay + 0.5 ) ); i < b.size( ); i++ )
    {
        const double Abs = abs( b[ i ] - a1[ i ] );
        if ( Abs > maxAbs )
            maxAbs = Abs;

        test = test && ( Abs < 1.0e-05 );
#if 0
    Test.Message() << "i = " << i << "   " 
		   << a1[i] << "    " << b[i] << std::endl;
#endif
    }

    Test.Check( test, "Resampled data matches stretched/compressed original" );
}

//
// Compare general resampling in stages
//
template < class T >
void
GeneralTest2( const int p, const int q )
{
    Test.Message( ) << "GeneralTest2: p = " << p << ", q = " << q << endl;

    // Sanity checks
    Test.Check( p != 1 ) << "p != 1" << endl;
    Test.Check( q != 1 ) << "q != 1" << endl;

    const int    n = 1000 * q;
    const double beta = 15.0;

    //
    // Add some random noise
    //

    valarray< T > a0( 0.0, n );

    generate( std::begin( a0 ), std::end( a0 ), Rand< T >( ) );

    for ( size_t k = 0; k < a0.size( ); ++k )
    {
        a0[ k ] = sin( LDAS_TWOPI * k / n );
    }

    valarray< T > b0; // upsampled result
    valarray< T > b; // up/downsampled result

    valarray< T > b_full; // p/q resampled

    // Set nOrder so that delay is integer and > 99 for upsampling
    int nOrder = 99 / p + p;

    Resample< T > gen1( p, 1, nOrder, beta );

    gen1.apply( b0, a0 );

    nOrder = 99;
    Resample< T > gen2( 1, q, nOrder, beta );

    gen2.apply( b, b0 );

    const int delay2 = (int)rint( gen2.getDelay( ) );
    b.shift( delay2 );
    b[ slice( b.size( ) - delay2, delay2, 1 ) ] = 0;

    Test.Message( ) << "delay1 = " << gen1.getDelay( ) << endl;
    Test.Message( ) << "delay2 = " << gen2.getDelay( ) << endl;

    // Now resample in one step

    // :NOTE: Works only for integer delay - want delay to be >= 99
    // and a multiple of q so that delay is integer
    nOrder = 0;
    double realdelay = 0.0;
    while ( realdelay < 99 )
    {
        nOrder += q;
        realdelay = nOrder * max( p, q ) / double( q );
    }

    Resample< T > gen( p, q, nOrder, beta );

    gen.apply( b_full, a0 );

    // Kill off delayed junk
    const int delay_full = (int)rint( gen.getDelay( ) );

    Test.Message( ) << "delay_full = " << delay_full << endl;

    b_full.shift( delay_full );
    b_full[ slice( b_full.size( ) - delay_full, delay_full, 1 ) ] = 0;

    double maxAbs = 0.0;
    bool   test = true;
    for ( size_t i = 0; i < b_full.size( ); ++i )
    {
        const double Abs = abs( b[ i ] - b_full[ i ] );
        if ( Abs > maxAbs )
            maxAbs = Abs;

        test = test && ( Abs < 1.0e-06 );
#if 0
    Test.Message() << "i = " << i << "   " 
		   << b[i] << "    " << b_full[i] << std::endl;
#endif
    }

    Test.Check( test, "Resampled data matches upsampled/downsampled data" );
}

template < class T >
void
GeneralTest( )
{
    GeneralDownTest< T >( 2, 3 );
    GeneralDownTest< T >( 5, 16 );
    GeneralDownTest< T >( 5, 32 );
    GeneralDownTest< T >( 10, 11 );

    GeneralUpTest< T >( 3, 2 );
    GeneralUpTest< T >( 16, 5 );
    // GeneralUpTest<T>(32, 5);
    GeneralUpTest< T >( 11, 10 );

    // GeneralTest2<T>(5, 16);
}

int
main( int ArgC, char** ArgV )
{
    try
    {
        std::string id(
            "$Id: tResample.cc,v 1.27 2006/11/07 22:25:47 emaros Exp $" );
        Test.Init( ArgC, ArgV );
        if ( Test.IsVerbose( ) )
            Test.Message( ) << id << std::endl;

#if 0
    Test.Message() << "------- Nominal -------" << std::endl;
    Nominal<float>();
    Nominal<double>();
#endif /* 0 */

        Nominal< complex< float > >( );
        // Nominal<complex<double> >();

#if 0
    Test.Message() << "------- Exceptions<float> -------" << std::endl;
    Exceptions<float>();

    Test.Message() << "------- Exceptions<double> -------" << std::endl;
    Exceptions<double>();

    Test.Message() << "------- Exceptions<complex<float> > -------" << std::endl;
    Exceptions<complex<float> >();

    Test.Message() << "------- Exceptions<complex<double> > -------" << std::endl;
    Exceptions<complex<double> >();

    Test.Message() << "------- Accessors -------" << std::endl;
    Accessors<float>();

    Test.Message() << "------- Copy -------" << std::endl;
    Copy<float>();
    Copy<double>();

    Test.Message() << "------- StateTest -------" << std::endl;
    StateTest<float>();
    StateTest<double>();

    Test.Message() << "------- CXStateTest -------" << std::endl;
    CXStateTest<float>();
    CXStateTest<double>();

    Test.Message() << "------- ImpulseResponse<double> -------" << std::endl;
    ImpulseResponse<double>(3,1,10,5);
    Test.Message() << "------- ImpulseResponse<double> -------" << std::endl;
    ImpulseResponse<double>(1,3,10,5);
    Test.Message() << "------- ImpulseResponse<double> -------" << std::endl;
    ImpulseResponse<double>(5,3,31,5);

    Test.Message() << "------- CXImpulseResponse<double> -------" << std::endl;
    CXImpulseResponse<double>(3,1,10,5);
    Test.Message() << "------- CXImpulseResponse<double> -------" << std::endl;
    CXImpulseResponse<double>(1,3,10,5);
    Test.Message() << "------- CXImpulseResponse<double> -------" << std::endl;
    CXImpulseResponse<double>(7,5,30,5);

    Test.Message() << "------- Results<float> -------" << std::endl;
    Results<float>();
    Test.Message() << "------- Results<double> -------" << std::endl;
    Results<double>();  

    Test.Message() << "------- CXResults<float> -------" << std::endl;
    CXResults<float>();
    Test.Message() << "------- CXResults<double> -------" << std::endl;
    CXResults<double>();

    Test.Message() << "------- General<float> -------" << std::endl;
    GeneralTest<float>();

    Test.Message() << "------- getB<float> -------" << std::endl;
    testGetB<float>();

    Test.Message() << "------- getB<double> -------" << std::endl;
    testGetB<double>();

    Test.Message() << "------- getB<complex<float> > -------" << std::endl;
    testGetB<complex<float> >();

    Test.Message() << "------- getB<complex<double> > -------" << std::endl;
    testGetB<complex<double> >();

    Test.Message() << "------- General<double> -------" << std::endl;
    GeneralTest<double>();

    Test.Message() << "------- FilterTest<float> -------" << std::endl;
    FilterTest<float>();

    Test.Message() << "------- FilterTest<double> -------" << std::endl;
    FilterTest<double>();
#endif /* 0 */

        Test.Check( true ) << "reached end of test harness" << std::endl;
    }
    catch ( std::exception& x )
    {
        Test.Check( false )
            << "Caught std::exception " << x.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false ) << "Caught unknown exception" << std::endl;
    }

    Test.Exit( );
}
