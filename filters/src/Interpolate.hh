//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef INTERPOLATE_HH
#define INTERPOLATE_HH

#include "filters/LinFilt.hh"

//
// Functions for shifting and interpolation of a vector
//

namespace Filters
{

    /// \brief Class which implements Lagrange interpolation of a vector
    template < class T >
    class Interpolate
    {
    public:
        /// \brief Constructor
        ///
        /// \param[in] alpha
        ///   interpolation parameter, 0 < alpha < 1
        /// \param[in] order
        ///   order of Lagrange polynomial used for interpolation
        ///
        /// \exception invalid_argument
        ///   Thrown if alpha <= 0 or alpha >= 1
        Interpolate( const double alpha, const size_t order );

        /// \brief Get alpha parameter
        double getAlpha( ) const;

        /// \brief Get order parameter
        size_t getOrder( ) const;

        /// \brief Apply the interpolation
        ///
        /// The data contained in x is interpolated and the result returned
        /// in x.
        ///
        /// \param[in] x
        ///   the data to be interpolated
        ///
        /// \exception invalid_argument
        ///   Thrown if x.size() == 0
        void apply( std::valarray< T >& x );

    private:
        /// \brief alpha parameter is only stored for getAlpha()
        double m_alpha;

        /// \brief the filter used for interpolation.
        LinFilt< double, T > m_filt;
    };

    template < class T >
    inline double
    Interpolate< T >::getAlpha( ) const
    {
        return m_alpha;
    }

    template < class T >
    inline size_t
    Interpolate< T >::getOrder( ) const
    {
        return ( m_filt.getBSize( ) - 1 );
    }

    // Non-member functions

    //
    /// \brief Calculate FIR coefficients for an interpolating filter
    ///
    /// This function designs a FIR filter for interpolating the values in
    /// a vector with a Lagrange polynomial of order b.size() - 1.
    ///
    /// \param[out] b
    ///   the array to fill with Lagrange polynomial coefficients.
    /// \param[in] alpha
    ///   interpolation parameter
    ///
    /// \exception invalid_argument
    ///   Thrown if b.size() == 0
    /// \exception invalid_argument
    ///   Thrown if alpha <= 0 or alpha >= 1
    template < class T >
    void designInterpolatingFilter( std::valarray< T >& b, const double alpha );

} // namespace Filters

#endif // INTERPOLATE_HH
