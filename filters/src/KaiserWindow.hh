//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef KAISERWINDOW_HH
#define KAISERWINDOW_HH

#include "filters/FilterConstants.hh"
#include "filters/Window.hh"

namespace Filters
{

    //
    /// \brief A class representing a Kaiser window
    //
    class KaiserWindow : public Window
    {

    public:
        //
        /// \brief Constructor
        ///
        /// \exception std::invalid_argument
        ///   thrown if beta is less than 0
        //
        explicit KaiserWindow( const double beta = KaiserWindowDefaultBeta );

        //
        /// \brief Get the beta parameter for this window
        ///
        /// \return
        ///   beta parameter
        //
        double beta( ) const;

        //
        /// \brief Set the beta parameter for this window
        ///
        /// \exception std::invalid_argument
        ///   thrown if beta is less than 0
        //
        void set_beta( const double& beta );

        //
        /// \brief Return window name
        ///
        /// \return
        ///   window name
        //
        virtual std::string name( ) const;

        //
        /// \brief Return window parameter
        ///
        /// \return
        ///   window parameter
        //
        virtual double param( ) const;

        //
        /// \brief Clone a window
        ///
        /// \return
        ///   copy of current window
        //
        virtual KaiserWindow* Clone( ) const;

    private:
        //
        /// \brief Get the ith element of the window
        ///
        /// A Kaiser window is defined by
        ///
        ///     w[i] = I0( beta*sqrt(1 - [2*i/(n-1) - 1]^2) )/I0(beta)
        ///
        /// for i= 0,1, ... n-1, where I0(x) is the 0th order, modified Bessel
        /// function of the first kind, and beta (>=0) is a shape parameter
        /// related to the amplitude of the sidelobes of the Fourier transform
        /// of the window. See "Discrete-time Signal Processing" by Oppenheim
        /// and Schafer, p.474 for details.)
        ///
        /// \note
        ///   when beta=0, a Kaiser window reduces to a rectangular window
        ///
        /// \param[in] i
        ///   the element at which the window is evaluated
        ///
        /// \return
        ///   the window element w[i]
        //
        virtual double element( const size_t i ) const;

        /* clang-format off */
        //
        /// \brief Modified Bessel Function of first kind, order 0, evaluated by power series expansion
        //
        /* clang-format on */
        double I0( const double& x ) const;

        /// \brief beta parameter
        double m_beta;

        /// \brief parameter for the modified Bessel function
        double m_I0beta;
    };

    inline double
    KaiserWindow::beta( ) const
    {
        return m_beta;
    }

} // namespace Filters

#endif // KAISERWINDOW_HH
