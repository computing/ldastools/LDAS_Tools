//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef BASIC_ARRAY_HH
#define BASIC_ARRAY_HH

namespace Filters
{

    //
    /// \brief This class implements a trivial exception-safe C-style array
    //
    // This class is a highly restricted implementation of C-style arrays,
    // useful for avoiding memory leaks in code where exception may be thrown.
    //
    // It was created because auto_ptr is unsuitable for array data, since
    // there is no way to specify 'delete [] ptr' rather than 'delete ptr'.
    //
    // Note that copy construction and assignment of basic_array's are
    // forbidden. The reason for this is that the implementation uses a
    // 'size' argument to the constructor, rather than a template argument,
    // as would be usual. If instead the class looked like
    //
    //   template<class T, size_t SIZE>
    //   class basic_array { ... };
    //
    // then several things would happen:
    //
    // 1) operations between (say) basic_array<int, 10> and
    // basic_array<int, 11> would be caught by the compiler as type-mismatches.
    //
    // 2) the template argument would have to be constant ie. known at compile
    // time.
    //
    // Both of these conditions are consistent with the usual C-array
    // semantics, but are unfortunately too restrictive for our purpose.
    // They also require the instantation of different templates for every
    // size array that is used. This is an unacceptably high overhead.
    //
    // A compromise is achieved by banning copy construction and operator=()
    // so that the problem of array-size mismatch does not occur. Note that
    // other aspects of C-style array semantics are adhered to. Elements of
    // the array may be accessed in the standard ways eg.
    //
    //   basic_array<float> v(100);
    //
    //   float v10_1 = v[10];
    //   float v10_2 = *(v + 10);
    //   float* vFirst = &v[0];
    //
    // and no range-checking is performed (as is standard for C-style arrays).
    //
    // The other major difference is the presence of the release() member
    // function. This allows ownership of the data pointed to by the
    // basic_array to be transferred to an external pointer, as an interface
    // between basic_array's and C-style arrays. Once release() is called,
    // the basic_array object is invalidated and further calls to operator[]
    // or dereferencing it will give undefined results. Further calls to
    // release() will yield a null pointer. Destruction of the object is
    // still valid, however.
    //
    template < class T >
    class basic_array
    {
    public:
        /// \brief Construct a basic_array
        /// \param[in] size
        ///   size of array to be constructed
        basic_array( const size_t size ) : m_ptr( new T[ size ] )
        {
        }

        /// \brief Destructor
        ~basic_array( )
        {
            delete[] m_ptr;
        }

        /// \brief Implicit cast to a const T*
        operator const T*( ) const
        {
            return m_ptr;
        }

        /// \brief Implicit cast to a T*
        operator T*( )
        {
            return m_ptr;
        }

        /// \brief Return a const reference to the k'th element.
        // No range checking is performed.
        const T& operator[]( const size_t k ) const
        {
            return m_ptr[ k ];
        }

        /// \brief Return a modifiable reference to the k'th element
        // No range checking is performed.
        T& operator[]( const size_t k )
        {
            return m_ptr[ k ];
        }

        /// \brief Transfer ownership of internal data
        //
        // This function provides a means for turning a basic_array
        // into a C-style array.
        //
        // It returns a pointer to the basic_array's data allocated on the
        // heap. It is the responsibility of the caller to delete this
        // memory.
        //
        // Once release() is called, the original basic_array is "frozen".
        // Further calls to release() will yield a null pointer. Calls to
        // operator[] or dereferencing the object will have undefined
        // results, however destruction of the basic_array is still valid.
        //
        T*
        release( )
        {
            T* const tmp = m_ptr;
            m_ptr = 0;
            return tmp;
        };

    private:
        basic_array( );
        basic_array( const basic_array& );
        basic_array& operator=( const basic_array& );

        T* m_ptr;
    };

} // namespace Filters

#endif // BASIC_ARRAY_HH
