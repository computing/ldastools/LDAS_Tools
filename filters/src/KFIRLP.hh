//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef KFIRLP_HH
#define KFIRLP_HH

#include <stdexcept>

#include "filters/KaiserWindow.hh"

namespace Filters
{

    class KFIRLP
    {
    public:
        /* clang-format off */
        /// \brief Constructor
        /// \param[in] t_fc
        ///   freq. of mid-transition band (Nyquist
        ///   units) param: const double& t_alpha - is stop-band depth in dB
        ///   (positive) param: const double& t_df - is transition band width
        ///   (Nyquist units)
        /// \exception domain_error
        ///   Thrown if t_fc <= 0
        ///   or   t_fc >= 1
        ///   or   the abs(2*df-1) is not less than one
        /// \exception invalid_argument
        ///   Thrown if t_alpha<=0
        /* clang-format on */
        KFIRLP( const double& t_fc, const double& t_alpha, const double& t_df );

        /// \brief set mid-point of transition band
        /// \param[in] double t_fc
        ///   freq. of mid-transition band (Nyquist units)
        /// \exception domain_error
        ///   if t_fc <= 0 or t_fc >= 1
        void setFc( double t_fc );

        /// \brief get mid-point of transition band
        /// \return
        ///   Returns the current mid-point of the transition band
        double getFc( ) const;

        /// \brief set stop-band depth
        /// \param[in] t_alpha
        ///   sets the stop-band attenuation
        /// \exception invalid_argument
        ///   if t_alpha <= 0
        void setAlpha( double t_alpha );

        /// \brief get stop-band depth
        /// \return
        ///   Returns the current stop-band attenuation
        double getAlpha( ) const;

        /// \brief set transition band width
        /// \param[in] t_df
        ///   is transition band width (Nyquist frequency
        ///   units) exc: domain_error - if the abs(2*df-1) is not less than one
        void setDf( const double t_df );

        /// \brief get transition band width
        /// \return
        ///   Returns the current width of the transition band
        double getDf( ) const;

        /// \brief get transfer function
        /// \param[in] b
        ///   filter coefficients in the container
        template < class T >
        void apply( std::valarray< T >& b );

        /// \brief filter order
        /// \return
        ///   order (length-1) of the filter
        int getOrder( ) const;

    private:
        /// \brief Kaiser Window beta parameter
        /// \return
        ///   the beta parameter corresponding to the current
        //+  order, alpha
        double getBeta( ) const;

        // data members

        double       fc;
        double       df; ///< width of the transition zone of the filter
        double       alpha; ///< side lobe height
        KaiserWindow kwin; ///< Kaiser Window
    };

    inline double
    KFIRLP::getFc( ) const
    {
        return fc;
    }

    inline double
    KFIRLP::getAlpha( ) const
    {
        return alpha;
    }

    inline double
    KFIRLP::getDf( ) const
    {
        return df;
    }

} // namespace Filters

#endif // KFIRLP_HH
