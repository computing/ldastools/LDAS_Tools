//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef WINDOW_HH
#define WINDOW_HH

#include <string>
#include <complex>
#include <valarray>

namespace Filters
{

    //
    /// \brief An abstract base class for window functions
    //
    class Window
    {

    public:
        //
        /// \breif Default constructor
        //
        Window( );

        //
        /// \brief Destructor
        //
        virtual ~Window( );

        //
        /// \brief Copy assignment
        ///
        /// \param[in] r
        ///   Window that is copied
        ///
        /// \return
        ///   This object
        //
        const Window& operator=( const Window& r );

        //
        /// \brief Return window length
        ///
        /// \return
        ///   length of window
        //
        size_t size( ) const;

        //
        /// \brief Return mean value of window
        ///
        /// \return
        ///   mean value of window elements
        //
        double mean( ) const;

        //
        /// \brief Return RMS value of window
        ///
        /// \return
        ///   root mean square value of window elements
        //
        double rms( ) const;

        //
        /// \brief Return window name
        ///
        /// \return
        ///   window name
        //
        virtual std::string name( ) const = 0;

        //
        /// \brief Return window parameter
        ///
        /// \return
        ///   window parameter
        //
        virtual double param( ) const = 0;

        //
        /// \brief Change the size of the window
        ///
        /// \param[in] n
        ///   desired length of window
        ///
        /// \exception std::length_error
        ///   thrown if desired window length is
        ///   greater than maximum allowed value
        ///
        /// \exception std::bad_alloc
        ///   thrown if not able to allocate memory for
        ///   the window
        //
        void resize( const size_t n );

        //
        /// \brief Apply a window to the data in-place
        ///
        /// \param[in,out] x
        ///   input/output sequence
        ///
        /// \exception std::length_error
        ///   thrown if desired window length is
        ///   greater than maximum allowed value
        ///
        /// \exception std::bad_alloc
        ///   thrown if not able to allocate memory for
        ///   window
        //
        template < class T >
        void apply( std::valarray< T >& x );

        //
        /// \brief Apply a window to the data out-of-place
        ///
        /// \param[out] out
        ///   windowed sequence
        /// \param[in] in
        ///   input sequence
        ///
        /// \exception std::length_error
        ///   thrown if desired window length is
        ///   greater than maximum allowed value
        ///
        /// \exception std::bad_alloc
        ///   thrown if not able to allocate memory for
        ///   window
        //
        template < class TOut, class TIn >
        void apply( std::valarray< TOut >&      out,
                    const std::valarray< TIn >& in );

        //
        /// \brief Synonym for apply
        ///
        /// \param[out] out
        ///   windowed sequence
        /// \param[in] in
        ///   input sequence
        ///
        /// \exception std::length_error
        ///   thrown if desired window length is
        ///   greater than maximum allowed value
        ///
        /// \exception std::bad_alloc
        ///   thrown if not able to allocate memory for
        ///   window
        //
        template < class TOut, class TIn >
        void operator( )( std::valarray< TOut >&      out,
                          const std::valarray< TIn >& in );

        //
        /// \brief Clone a window
        ///
        /// \return
        ///   copy of current window
        //
        virtual Window* Clone( ) const = 0;

    protected:
        //
        /// \brief Populate the elements of the window
        ///
        /// This function will usually need to be called by a derived
        /// class whenever the the window is changed in any way which
        /// affects the element() function, such as when some window
        /// parameter is altered.
        //
        void populate( );

    private:
        //
        /// \brief Get the ith element of the window
        ///
        /// This function returns the value corresponding to the
        /// ith position in the window.
        /// It is the window function evaluated at x = i/N where N is
        /// the length of the window.
        ///
        /// \param[i]
        ///   element at which the window is evaluated
        ///
        /// \return
        ///   the window element w(i/N)
        //
        virtual double element( const size_t i ) const = 0;

        //
        /// \brief Multiply a sequence of floats by the window
        ///
        /// \param[in,out] out
        ///   input (and output) sequence
        //
        void multiply_by_window( std::valarray< float >& out );

        //
        /// \brief Multiply a sequence of doubles by the window
        ///
        /// \param[in,out] out
        ///   input (and output) sequence
        //
        void multiply_by_window( std::valarray< double >& out );

        //
        /// \brief Multiply a sequence of complex floats by the window
        ///
        /// \param[in,out]
        ///   input (and output) sequence
        //
        void multiply_by_window( std::valarray< std::complex< float > >& out );

        //
        /// \brief Multiply a sequence of complex doubles by the window
        ///
        /// \param[in,out] out
        ///   input (and output) sequence
        //
        void multiply_by_window( std::valarray< std::complex< double > >& out );

        /// \brief a sequence for holding a window of floats
        std::valarray< float > m_f_window;

        /// \brief a sequence for holding a window of doubles
        std::valarray< double > m_d_window;

        /// \brief mean value of window elements
        double m_mean;

        /// \brief rms value of window elements
        double m_rms;

        /// \brief true if window elements are floats
        bool m_is_float;
    };

    inline size_t
    Window::size( ) const
    {
        return ( m_is_float ? m_f_window.size( ) : m_d_window.size( ) );
    }

    inline double
    Window::mean( ) const
    {
        return m_mean;
    }

    inline double
    Window::rms( ) const
    {
        return m_rms;
    }

} // namespace Filters

#endif // WINDOW_HH
