//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FILTERS_RESAMPLE_HH
#define FILTERS_RESAMPLE_HH

// $Id: Resample.hh,v 1.20 2006/10/13 19:32:40 emaros Exp $

#include <vector>
#include <valarray>

namespace Filters
{

    // Forward declarations
    template < class TCoeffs, class TIn >
    class LinFilt;

    class ResampleBase
    {
    public:
        //
        // Since this is purely a base class, there are no constructors
        // available except to derived classes
        //

        /// \brief Destructor is virtual so that RTTI can be used
        virtual ~ResampleBase( );

        /// \brief Gets the resample numerator
        /// \return
        ///   upsample factor
        int getP( ) const;

        /// \brief Gets the resample numerator
        /// \return
        ///   downsample factor
        int getQ( ) const;

        /// \brief Gets anti-aliasing filter parameters
        /// \return
        ///   filter length param
        int getN( ) const;

        /// \brief Gets anti-aliasing filter parameters
        /// \return
        ///   filter order
        int getNOrder( ) const;

        /// \brief Gets anti-aliasing filter parameters
        /// \return
        ///   Kaiser Window beta parameter
        double getBeta( ) const;

        /// \brief Gets the delay due to the filtering action
        /// \return
        ///   returns the group delay of the impulse due to filtering
        double getDelay( ) const;

        /// \brief Pure virtual constructor
        /// \return
        ///   pointer to new ResampleBase object
        virtual ResampleBase* Clone( ) const = 0;

        //
        /// \brief Comparison operator
        ///
        /// \param[in] rhs
        ///   instance to be assigned from
        /// \return
        ///   true if data is the same, false otherwise
        bool operator==( const ResampleBase& rhs );

        /// \brief Reset the internal state to its initial condition
        //
        // This function sets the internal state information back to
        // zero, exactly as it is when the Resample object is first created.
        // This means that when the resampling is next applied, data "before"
        // the start of the series will be treated as if it were zero.
        virtual void reset( ) = 0;

    protected:
        //
        /// \brief General purpose constructor.
        ///
        /// \param[in] p
        ///   upsampling ratio
        /// \param[in] q
        ///   downsampling ratio
        /// \param[in] n
        ///   filter order: 2*n*max(p, q) is the length of the filter
        /// \param[in] beta
        ///   the beta parameter of the Kaiser Window
        ///
        /// \exception invalid_argument
        ///   if p or q or n < 1 or beta < 0.0
        //
        ResampleBase( const int    p,
                      const int    q,
                      const int    n,
                      const double beta );

        //
        /// \brief Special purpose constructor.
        ///
        /// \param[in] p
        ///   upsampling ratio
        /// \param[in] q
        ///   downsampling ratio
        ///
        /// \exception invalid_argument
        ///   if p or q < 1
        //
        ResampleBase( const int p, const int q );

    private:
        /// \brief Private default constructor, left undefined
        ResampleBase( );

        /// \brief upsample factor
        int m_p;

        /// \brief downsample factor
        int m_q;

        /// \brief filter order parameter (not actual filter order)
        int m_n;

        /// \brief anti-aliasing filter order
        int m_nOrder;

        /// \brief Kaiser window beta
        double m_beta;

        /// \brief anti-aliasing filter group delay
        double m_delay;

    }; // end ResampleBase

    //-------------------------------------------------------------------------

    template < class TIn >
    class ResampleTraits
    {
    public:
        typedef TIn OutType;
    };

    template <>
    class ResampleTraits< short int >
    {
    public:
        typedef float OutType;
    };

    template <>
    class ResampleTraits< int >
    {
    public:
        typedef float OutType;
    };

    template < class TIn >
    class Resample : public ResampleBase
    {

    public:
        typedef typename ResampleTraits< TIn >::OutType TOut;

        //
        /// \brief General purpose constructor.
        ///
        /// \param[in] p
        ///   upsampling ratio
        /// \param[in] q
        ///   downsampling ratio
        /// \param[in] n = 10
        ///   filter order: 2*n*max(p, q) is the length of the filter
        /// \param[in] beta = 5
        ///   the beta parameter of the Kaiser Window
        ///
        /// \exception invalid_argument
        ///   if p or q or n < 1 or beta < 0.0
        //
        Resample( const int    p,
                  const int    q,
                  const int    n = 10,
                  const double beta = 5.0L );

        //
        /// \brief Special purpose constructor.
        ///
        /// \param[in] p
        ///   upsampling ratio
        /// \param[in] q
        ///   downsampling ratio
        /// \param[in] b
        ///   filter to use
        ///
        /// \exception invalid_argument
        ///   b.size() == 0
        //
        Resample( const int p, const int q, const std::valarray< double >& b );

        //
        /// \brief Copy constructor
        ///
        /// \param[in] rhs
        ///   instance to be copied
        Resample( const Resample& rhs );

        //
        /// \brief Virtual destructor
        //
        virtual ~Resample( );

        //
        /// \brief Assignment, needed because class has pointer members
        ///
        /// \param[in] rhs
        ///   instance to be assigned from
        /// \return
        ///   reference to *this
        const Resample& operator=( const Resample& rhs );

        //
        /// \brief Comparison operator
        ///
        /// \param[in] rhs
        ///   instance to be assigned from
        /// \return
        ///   true if data is the same, false otherwise
        bool operator==( const Resample& rhs );

        //
        /// \brief Virtual constructor
        ///
        /// \return
        ///   pointer to new Resample object
        virtual Resample* Clone( ) const;

        //
        /// \brief Filter coefficents accessor
        ///
        /// \param[in] b
        ///   FIR filter coefficents array
        void getB( std::valarray< double >& b ) const;

        /// \brief Reset the internal state to its initial condition
        //
        // This function sets the internal state information back to
        // zero, exactly as it is when the Resample object is first created.
        // This means that when the resampling is next applied, data "before"
        // the start of the series will be treated as if it were zero.
        virtual void reset( );

        /* clang-format off */
        //
        /// \brief Resamples "in" by the factor p/q and returns the result in "out".
        ///
        /// The first call to apply initialises the internal state of Resample
        /// so that the input data is treated as if it were preceded by an
        /// infinite number of zeroes. After the first apply(), the Resampler is
        /// in a non-default state and it will assume that the next call to
        /// apply() is on a segment of data that is contiguous with the
        /// preceding input. In this way, a long data set can be broken up into
        /// smaller segments which can be resampled individually. The recombined
        /// data is identical resampling the original long data set.
        ///
        /// This function is not const to remind users that its use modifies
        /// the internal state of Resample.
        ///
        /// \param[out] out
        ///   pointer to result of resampling the "in" data
        /// \param[in] in
        ///   pointer to data to be resampled
        /// \param[in] inlen
        ///   length of in
        ///
        /// \exception invalid_argument
        ///   Input length must be a multiple of q
        /* clang-format on */
        void apply( TOut* const out, const TIn* const in, const size_t inlen );

        /* clang-format off */
       //
        /// \brief Resamples "in" by the factor p/q and returns the result in "out".
        ///
        /// The first call to apply initialises the internal state of Resample
        /// so that the input data is treated as if it were preceded by an
        /// infinite number of zeroes. After the first apply(), the Resampler is
        /// in a non-default state and it will assume that the next call to
        /// apply() is on a segment of data that is contiguous with the
        /// preceding input. In this way, a long data set can be broken up into
        /// smaller segments which can be resampled individually. The recombined
        /// data is identical resampling the original long data set.
        ///
        /// This function is not const to remind users that its use modifies
        /// the internal state of Resample.
        ///
        /// \param[out] out
        ///   result of resampling the "in" data
        /// \param[in] in
        ///   data to be resampled
        ///
        /// \exception invalid_argument
        ///   Input length must be a multiple of q
        /* clang-format on */
        void apply( std::valarray< TOut >&      out,
                    const std::valarray< TIn >& in );

    private:
        typedef LinFilt< double, TOut > lft_type;
        typedef lft_type*               lfd_type;
        typedef std::vector< lfd_type > lf_type;

        /// \brief Initialise the filter bank
        void initFilters( const std::valarray< double >& b );

        /// \brief Obtain the filter at position (p, q)
        LinFilt< double, TOut >& getFilter( const int p, const int q ) const;

        /// \brief Pointer to partial sum saved for split input purposes
        std::valarray< TOut > m_orphan;

        /// \brief Storage for anti-aliasing filters
        lf_type m_lf;

    }; // end Resample

    inline int
    ResampleBase::getP( ) const
    {
        return m_p;
    }

    inline int
    ResampleBase::getQ( ) const
    {
        return m_q;
    }

} // namespace Filters

#endif // FILTERS_RESAMPLE_HH
