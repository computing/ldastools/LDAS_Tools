//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FILTERS_LINFILT_HH
#define FILTERS_LINFILT_HH

#include <valarray>
#include <complex>

namespace Filters
{

    ///
    /// \brief Traits for LinFilt objects
    ///
    /// These guarantee that the intermediate calculations for linear
    /// filtering are always done at double precision, even when the
    /// input/output is single precision. The difficulty is that we need
    /// to support both real and complex intermediates
    ///
    /// Under most circumstances, output type is the same as input type
    template < class TCoeffs, class TIn >
    class LinFiltTraits
    {
    public:
        typedef TIn OutType;
    };

    /// Specializations for complex data - if the input type is real but
    /// the coefficients are complex, the output is complex
    template <>
    class LinFiltTraits< std::complex< float >, float >
    {
    public:
        typedef std::complex< float > OutType;
    };

    template <>
    class LinFiltTraits< std::complex< float >, double >
    {
    public:
        typedef std::complex< double > OutType;
    };

    template <>
    class LinFiltTraits< std::complex< double >, float >
    {
    public:
        typedef std::complex< float > OutType;
    };

    template <>
    class LinFiltTraits< std::complex< double >, double >
    {
    public:
        typedef std::complex< double > OutType;
    };

    /// Traits of the internally stored filter coefficients - they are always
    /// stored as double precision
    template < class TCoeffs >
    class LinFiltCoeffTraits
    {
    public:
        typedef double CoeffType;
    };

    template < class T >
    class LinFiltCoeffTraits< std::complex< T > >
    {
    public:
        typedef std::complex< double > CoeffType;
    };

    //
    /// Elements of the state variable are always complex<double> unless both
    /// the input and coefficients are real, in which case they are double.
    //
    template < class TCoeffs, class TIn >
    class LinFiltStateTraits
    {
    public:
        typedef std::complex< double > StateType;
    };

    template <>
    class LinFiltStateTraits< float, float >
    {
    public:
        typedef double StateType;
    };

    template <>
    class LinFiltStateTraits< float, double >
    {
    public:
        typedef double StateType;
    };

    template <>
    class LinFiltStateTraits< double, float >
    {
    public:
        typedef double StateType;
    };

    template <>
    class LinFiltStateTraits< double, double >
    {
    public:
        typedef double StateType;
    };

    /// \brief Virtual base class for LinFilt - required for RTTI
    class LinFiltBase
    {
    public:
        virtual ~LinFiltBase( )
        {
        }

        virtual LinFiltBase* Clone( ) const = 0;

        /// \brief Reset the internal state to its initial condition
        //
        // This function sets the internal state information back to
        // zero, exactly as it is when the LinFilt object is first created.
        // This means that when the filter is next applied, data "before" the
        // start of the series will be treated as if it were zero.
        virtual void reset( ) = 0;
    };

    /// \brief Class for performing linear filtering on arbitrary data sequences
    ///
    /// TCoeffs is the data type of the elements in the filter coefficient
    /// arrays a and b. Internally, coefficients are stored at double precision.
    ///
    /// TIn is the data type of the elements in the input array.
    ///
    /// The output type TOut is derived from TCoeffs and TIn using trait
    /// classes. The internal state type TState is also derived from TCoeffs and
    /// TIn.
    ///
    /// TCoeffs          TIn    gives     TOut             TState
    /// ------------------------------------------------------------------
    /// float            float            float            double
    /// float            double           double           double
    /// float            complex<float>   complex<float>   complex<double>
    /// float            complex<double>  complex<double>  complex<double>
    ///
    /// double           float            float            double
    /// double           double           double           double
    /// double           complex<float>   complex<float>   complex<double>
    /// double           complex<double>  complex<double>  complex<double>
    ///
    /// complex<float>   float            complex<float>   complex<double>
    /// complex<float>   double           complex<double>  complex<double>
    /// complex<float>   complex<float>   complex<float>   complex<double>
    /// complex<float>   complex<double>  complex<double>  complex<double>
    ///
    /// complex<double>  float            float            complex<double>
    /// complex<double>  double           double           complex<double>
    /// complex<double>  complex<float>   complex<float>   complex<double>
    /// complex<double>  complex<double>  complex<double>  complex<double>
    //
    template < class TCoeffs, class TIn >
    class LinFilt : public LinFiltBase
    {
    public:
        typedef typename LinFiltTraits< TCoeffs, TIn >::OutType        TOut;
        typedef typename LinFiltCoeffTraits< TCoeffs >::CoeffType      TCo;
        typedef typename LinFiltStateTraits< TCoeffs, TIn >::StateType TState;

        /// \brief Construct from transfer function
        /// \param[in] b
        ///   FIR transfer function
        /// \param[in] a
        ///   IIR transfer function (defaults to FIR if a is not provided)
        LinFilt( const std::valarray< TCoeffs >& b,
                 const std::valarray< TCoeffs >& a =
                     std::valarray< TCoeffs >( 1.0, 1 ) );

        /// \brief Destructor
        virtual ~LinFilt( );

        /// \brief Assignment operator.
        /// \param[in] lf
        ///   LinFilt object to assign from
        const LinFilt& operator=( const LinFilt& rhs );

        bool operator==( const LinFilt& rhs );

        virtual LinFilt* Clone( ) const;

        /// \brief Get FIR coefficients
        void getB( std::valarray< TCoeffs >& b ) const;

        /// \brief Get FIR size
        size_t getBSize( ) const;

        /// \brief Get IIR coefficients
        void getA( std::valarray< TCoeffs >& a ) const;

        /// \brief Get IIR size
        size_t getASize( ) const;

        /// \brief Get state vector
        void getZ( std::valarray< TState >& z ) const;

        /// \brief Get state size
        size_t getZSize( ) const;

        /// \brief Reset the internal state to its initial condition
        ///
        /// This function sets the internal state information back to
        /// zero, exactly as it is when the LinFilt object is first created.
        /// This means that when the filter is next applied, data "before" the
        /// start of the series will be treated as if it were zero.
        virtual void reset( );

        /// \brief In-place filter action
        /// \param[in] x
        ///   input data for filter.
        /// \exception std::invalid_argument
        ///   x.size() == 0
        void apply( std::valarray< TOut >& x );

        /// \brief Out-of-place filter action
        /// \param[in] y
        ///   result of filter acting on in. Existing contents
        ///   destroyed. param: x - input data for filter. exc:
        ///   std::invalid_argument - x.size() == 0
        void apply( std::valarray< TOut >& y, const std::valarray< TIn >& x );

    private:
        /// \brief Default constructor private.
        LinFilt( );

        /// \brief FIR coefficients
        std::valarray< TCo > m_b;

        /// \brief IIR coefficients
        std::valarray< TCo > m_a;

        /// \brief State information
        std::valarray< TState > m_z;
    };

    template < class TCoeffs, class TIn >
    inline size_t
    LinFilt< TCoeffs, TIn >::getBSize( ) const
    {
        return m_b.size( );
    }

    template < class TCoeffs, class TIn >
    inline size_t
    LinFilt< TCoeffs, TIn >::getASize( ) const
    {
        return m_a.size( );
    }

    template < class TCoeffs, class TIn >
    inline size_t
    LinFilt< TCoeffs, TIn >::getZSize( ) const
    {
        return m_z.size( );
    }

} // namespace Filters

#endif // FILTERS_LINFILT_HH
