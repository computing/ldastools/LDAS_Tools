# Release 2.6.7 - December 26, 2024
    - Corrections for sanitizers

# Release 2.6.6 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.6.4 - August 14, 2019
  - Corrected Portfile to have proper description (Closes #55)

# Release 2.6.3 - December 6, 2018
  - Use namespace for header includes to prevent pollution

# Release 2.6.2 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Standardize source code format by using clang-format
  - Converted to CMake (fixes #32)

# Release 2.6.1 - June 22, 2018
  - Updated packaging rules to have build time dependency on specific
    versions of LDAS Tools packages

# Release 2.6.0 - June 19, 2018
  - Removed hand rolled smart pointers in favor of boost smart pointers
  - Changed version of filters library to 3:0:0

# Release 2.5.2 - November 6, 2017
  - Made compatible with XCode 9.x and GCC 7.2
  - Added ABI check

# Release 2.5.1 - September 9, 2016
  - Added --disable-warnings-as-errors to allow compilation on systems
    where warning messages have not yet been addressed
  - Added conditional setting of DESTDIR in python.mk to prevent install
    issues.

# Release 2.5.0 - April 7, 2016
  - Official release of splitting LDASTools into separate source packages

# Release 2.4.99.1 - March 11, 2016
  - Corrections to spec files.

# Release 2.4.99.0 - March 3, 2016
  - Separated code into independant source distribution
