//
// LDASTools utilities - A collection of utilities base on LDASTools Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools utilities is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools utilities is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "genericAPI/TCL.hh"
#include "frameAPI/createRDS.hh"

#include "query.hh"

namespace
{
    struct channel_aggregator_type
    {
        typedef std::vector< std::string > channels_container_type;

        channels_container_type& channels;

        inline channel_aggregator_type( channels_container_type& Channels )
            : channels( Channels )
        {
            channels.resize( 0 );
        }

        inline void
        operator( )( std::string const& ChannelDescriptor )
        {
            channels.push_back( ChannelDescriptor );
        }

        inline void
        operator( )( std::string const& OldName,
                     std::string const& Resampling,
                     std::string const& NewName )
        {
            channels.push_back(
                FrameAPI::channel_naming_conventions_type::Canonical(
                    OldName, Resampling, NewName ) );
        }
    };
} // namespace

namespace LDAS
{
    namespace createRDS
    {
        //=======================================================================
        //
        //=======================================================================
        query::query( const std::string Query ) : ext( ".gwf" )
        {
            static const char* caller = "query::query";

            std::list< std::string >   parameters;
            std::vector< std::string > op;
            int                        count = 0;

            GenericAPI::TCL::ParseList( Query, parameters );
            op.resize( std::min( parameters.size( ),
                                 size_t( POSITION_QUERY_LIST_SIZE_TCL ) ) );
            QUEUE_LOG_MESSAGE( "op.size: " << op.size( ),
                               MT_DEBUG,
                               30,
                               caller,
                               "CMD_CREATE_RDS" );
            for ( std::list< std::string >::const_iterator
                      cur = parameters.begin( ),
                      last = parameters.end( );
                  cur != last;
                  ++cur, ++count )
            {
                if ( count > POSITION_CHANNEL_LIST_TCL )
                {
                    op[ POSITION_CHANNEL_LIST_TCL ] += *cur;
                }
                else
                {
                    op[ count ] = *cur;
                }
            }
            QUEUE_LOG_MESSAGE(
                "number of elements in query: " << parameters.size( ),
                MT_DEBUG,
                30,
                caller,
                "CMD_CREATE_RDS" );
            desc = op[ POSITION_DESC_TCL ];
            ifo = op[ POSITION_IFO_TCL ];

            //---------------------------------------------------------------------
            // Parse the time range
            //---------------------------------------------------------------------
            std::string s( op[ POSITION_TIME_RANGE_TCL ] );
            std::string e;
            size_t      offset( s.find( '-' ) );

            if ( offset != std::string::npos )
            {
                e = s.substr( offset + 1 );
                s = s.substr( 0, offset );
            }

            LDASTools::AL::GPSTime::seconds_type sv;
            LDASTools::AL::GPSTime::seconds_type ev;

            {
                std::istringstream ss( s );
                ss >> sv;
            }
            {
                std::istringstream ss( e );
                ss >> ev;
            }

            start = LDASTools::AL::GPSTime( sv, 0 );
            end = LDASTools::AL::GPSTime( ev, 0 );

            //---------------------------------------------------------------------
            // Parse out the channel list
            //---------------------------------------------------------------------
            const std::string& cl( op[ POSITION_CHANNEL_LIST_TCL ] );
            size_t             clb( cl.find( '(' ) );
            size_t             cle( cl.rfind( ')' ) );

            if ( clb != std::string::npos )
            {
                ++clb;
            }
            QUEUE_LOG_MESSAGE(
                "cl: " << cl << " clb: " << clb << " cle: " << cle
                       << " len: " << ( cle - clb )
                       << " substr: " << cl.substr( clb, ( cle - clb ) ),
                MT_DEBUG,
                30,
                caller,
                "CMD_CREATE_RDS" );
            std::string cls( cl.substr( clb, ( cle - clb ) ) );
            //---------------------------------------------------------------------
            // Checking to see if the buffer names a file
            //---------------------------------------------------------------------
            channel_aggregator_type    channel_aggregator( channels );
            std::vector< std::string > channel_tokens;

            boost::algorithm::split( channel_tokens,
                                     cls,
                                     boost::is_any_of( "," ),
                                     boost::token_compress_on );
            for ( auto channel : channel_tokens )
            {
                boost::trim( channel );
                if ( boost::filesystem::exists( channel ) )
                {
                    FrameAPI::channel_name_file_type<
                        channel_aggregator_type >
                        channel_name_file( channel_aggregator );
                    channel_name_file.Read( channel );
                }
                else
                {
                    channel_aggregator( channel );
                }
            }
            QUEUE_LOG_MESSAGE(
                "finished query", MT_DEBUG, 30, caller, "CMD_CREATE_RDS" );
        }
    } // namespace createRDS
} // namespace LDAS
