#! /bin/sh
#
# LDASTools utilities - A collection of utilities base on LDASTools Suite
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools utilities is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools utilities is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
set -x
# -*- sh-basic-offset: 4; sh-indentation: 4; -*-
#
#------------------------------------------------------------------------
# Global variables that can be overwritten by user
#------------------------------------------------------------------------
ROOT_FRAME_DIR=${ROOT_FRAME_DIR:=/devscratch/frames/Samples}
export ROOT_FRAME_DIR
EXTENSIONS=${EXTENSIONS:=.gwf}
export EXTENSIONS
OUTPUT_DIR=${OUTPUT_DIR:=${HOME}/tmp}
export OUTPUT_DIR
OUTPUT_ASCII_FILENAME=${OUTPUT_ASCII_FILENAME:=cache.txt}
export OUTPUT_ASCII_FILENAME
OUTPUT_BINARY_FILENAME=${OUTPUT_BINARY_FILENAME:=cache.bin}
export OUTPUT_BINARY_FILENAME
UNROOTED_MOUNT_PTS=${UNROOTED_MOUNT_PTS:=S6 A5 S5 A4}
#------------------------------------------------------------------------
RDS_DIR_OUTPUT_FRAMES=${RDS_DIR_OUTPUT_FRAMES:=${HOME}/tmp}
export RDS_DIR_OUTPUT_FRAMES
RDS_DIR_OUTPUT_MD5SUM=${RDS_DIR_OUTPUT_MD5SUM:=${RDS_DIR_OUTPUT_FRAMES}}
export RDS_DIR_OUTPUT_MD5SUM
#------------------------------------------------------------------------
#
#------------------------------------------------------------------------
DISKCACHE_HOST=localhost
DISKCACHE_PORT=18300

diskcache_cmd=${LDAS_TOP_BUILDDIR}/api/diskcacheAPI/so/src/diskcache

if test -f ${OUTPUT_DIR}/${OUTPUT_BINARY_FILENAME}
then
    DISKCACHE_SEED=${OUTPUT_DIR}/${OUTPUT_BINARY_FILENAME}
fi
case "x${MOUNT_PTS}" in
x) for md in ${UNROOTED_MOUNT_PTS}
   do
     MOUNT_PTS="${MOUNT_PTS}${MOUNT_PTS:+,}${ROOT_FRAME_DIR}/${md}"
   done
   ;;
*)
    ;;
esac
#------------------------------------------------------------------------
# NOTE:
# 
# Data can be seeded using variations of the following two commands:
#
# ligo_data_find --server=ldr.ligo.caltech.edu -o H \
#     -s `expr 815155200` -e `expr 815155200 + 1024` \
#     -t RDS_R_L3 | grep gsiftp | grep /archive/ | \
#     while read file ; do globus-url-copy -vb $file `basename $file` ; done
#
# AND
#
# gsiscp *.gwf ldas@ldas-dev.ligo.caltech.edu:/devscratch/frames/Samples/S5/L3/LHO/H-RDS_R_L3-8151/.
#
#------------------------------------------------------------------------
case $* in
*--run=S5*)
    #--------------------------------------------------------------------
    # S5 Run values
    #--------------------------------------------------------------------
    l0_dt='32'
    run_beg='815153408'
    run_end="`expr 815153696 + ${l0_dt}`"
    rds_beg='815155200'
    case $* in
    *--rds-lvl-4*)
	rds_type='RDS_R_L4'
	rds_dt='1024'
	rds_end="`expr ${rds_beg} + ${rds_dt}`"
	case $* in
	*--channel-list-file*)
	    rds_chan_set="`pwd`/channel_list.txt"
	    rm -f ${rds_chan_set}
	    for x in \
	        'H1:LSC-DARM_ERR!4' \
		'H1:IFO-SV_STATE_VECTOR' \
		'H1:IFO-SV_SEGNUM'
	    do
		echo $x | sed -e 's/!/ /g' | while read c e
		do
		    case x$e in
		    x) e='1';;
		    esac
		    echo $c $e >> ${rds_chan_set}
		done
	    done
            ;;
	*)
	    for x in \
	        'H1:LSC-DARM_ERR!4' \
		'H1:IFO-SV_STATE_VECTOR' \
		'H1:IFO-SV_SEGNUM'
	    do
		rds_chan_set="${rds_chan_set}${rds_chan_set:+,}$x"
	    done
            ;;
	esac
	;;
    *--rds-lvl-3*)
	rds_type='RDS_R_L3'
	rds_dt='256'
	rds_end="`expr ${rds_beg} + ${rds_dt}`"
	for x in \
	    'H1:IFO-SV_SEGNUM' \
	    'H1:IFO-SV_STATE_VECTOR' \
	    'H1:LSC-DARM_ERR' \
	    'H2:IFO-SV_SEGNUM' \
	    'H2:IFO-SV_STATE_VECTOR' \
	    'H2:LSC-DARM_ERR'
	do
	    rds_chan_set="${rds_chan_set}${rds_chan_set:+,}$x"
	done
        ;;
    *) 
        ;;
    esac
    tcl_query="{{R H {}"
    tcl_query="${tcl_query} ${rds_beg}-${rds_end}"
    tcl_query="${tcl_query} Chan(${rds_chan_set})}}"
    ;;
esac
case $1 in
--server-start)
    server_pid="`cat ,diskcache_server_pid 2>/dev/null`"
    server_pid=${server_pid:=none}
    if kill -0 $server_pid 2>/dev/null 1>/dev/null
    then
        echo Server already running with process id: ${server_pid}
    else
	${diskcache_cmd} ${DISKCACHE_SEED:+--cache-file} ${DISKCACHE_SEED} \
	    --port ${DISKCACHE_PORT} \
	    daemon \
	    --concurrency 4 \
	    --extensions ${EXTENSIONS} \
	    --output-ascii ${OUTPUT_DIR}/${OUTPUT_ASCII_FILENAME} \
	    --output-binary ${OUTPUT_DIR}/${OUTPUT_BINARY_FILENAME} \
	    --mount-points ${MOUNT_PTS} &
	server_pid="$!"
	echo ${server_pid} > ,diskcache_server_pid
        echo Server started with process id: ${server_pid}
    fi
    ;;
--server-stop)
    server_pid="`cat ,diskcache_server_pid 2>/dev/null`"
    server_pid=${server_pid:=none}
    if kill -0 $server_pid 2>/dev/null 1>/dev/null
    then
        kill -TERM ${server_pid}
	echo Killed server with pid: ${server_pid}
	rm -f ,diskcache_server_pid
    else
	echo No server running
	case ${server_pid} in
	none) ;;
	*) rm -f ,diskcache_server_pid;;
	esac
    fi
    ;;
--create-rds*)
    #--------------------------------------------------------------------
    # Take care of language specific variations first
    #--------------------------------------------------------------------
    case $1 in
    --create-rds)
	#----------------------------------------------------------------
	# Testing of the C++ executable
	#----------------------------------------------------------------
	create_rds_cmd="${LDAS_TOP_BUILDDIR}/api/userAPI/src/ldas_create_rds"
	rds_debug="--log-debug-level 30 --log-directory ${HOME}/tmp"
	;;
    --create-rds-python)
	#----------------------------------------------------------------
	PYTHONPATH="${LDAS_TOP_BUILDDIR}/api/diskcacheAPI/so/src/.libs"
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/api/diskcacheAPI/so/src"
	#----------------------------------------------------------------
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/api/frameAPI/so/src/.libs"
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/api/frameAPI/so/src/"
	#----------------------------------------------------------------
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/api/genericAPI/so/src/.libs"
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/api/genericAPI/so/src/"
	#----------------------------------------------------------------
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/lib/framecpp/src/OOInterface/.libs"
	PYTHONPATH="${PYTHONPATH}:${LDAS_TOP_BUILDDIR}/lib/framecpp/src/OOInterface"
	export PYTHONPATH
	#----------------------------------------------------------------
	# Testing of the SWIG wrapped Python comman
	#----------------------------------------------------------------
	create_rds_cmd="${LDAS_TOP_SRCDIR}/api/userAPI/src/ldas_py_create_rds"
	opt_query_tcl="--frame-query-tcl"
	rds_debug="--log-debug-level 30 --log-directory ${HOME}/tmp"
	;;
    esac
    #--------------------------------------------------------------------
    # Cleanup from previous running
    #--------------------------------------------------------------------
    #--------------------------------------------------------------------
    # Generate the RDS frame(s)
    #--------------------------------------------------------------------
    tcl_query="`echo $tcl_query | sed -e 's/!/\\!/g'`"
    ${create_rds_cmd} ${rds_debug} \
	--diskcache-host ${DISKCACHE_HOST} \
	--diskcache-port ${DISKCACHE_PORT} \
	--directory-output-frames ${RDS_DIR_OUTPUT_FRAMES} \
	--directory-output-md5sum ${RDS_DIR_OUTPUT_MD5SUM} \
	--description ${rds_type:=RDS_R_UNKNOWN} \
	${opt_query_tcl:=--frame-query} "${tcl_query}"
    echo Exited with status: $?
    ;;
esac
