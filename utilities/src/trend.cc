//
// LDASTools utilities - A collection of utilities base on LDASTools Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools utilities is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools utilities is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <map>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#include <regex>
#pragma GCC diagnostic pop
#include <set>

#include <boost/make_shared.hpp>
#include <boost/program_options.hpp>
#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrTOC.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrTable.hh"
#include "framecpp/FrVect.hh"
#include "framecpp/GPSTime.hh"
#include "framecpp/IFrameStream.hh"
#include "framecpp/OFrameStream.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

//=======================================================================
//=======================================================================

namespace po = boost::program_options;

typedef boost::program_options::options_description options_type;
typedef boost::program_options::variables_map       user_options_type;

#define OLD_FORMAT 0
#define NEW_FORMAT 1
#define MAX_FORMATS 2

static const int TREND_DATA = 7;
static const int TREND_DATA_SUBTYPE = 1; /* Maybe better as mask of stats */

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

using LDASTools::AL::MemChecker;

using GenericAPI::IsLogging;
using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;

class CommandLine;

static void store_trend_channels_as_frproc( const CommandLine& Options );

static inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

class StopWatch
{
public:
    inline void
    Start( )
    {
        start.Now( );
    }

    inline void
    Stop( )
    {
        stop.Now( );
    }

    inline double
    Elapsed( )
    {
        return ( stop - start );
    }

private:
    FrameCPP::GPSTime start;
    FrameCPP::GPSTime stop;
};

class CommandLine
{
public:
    typedef std::vector< std::string > input_files_type;

    typedef enum
    {
        STORE_TREND_CHANNELS_AS_FRPROC
    } method_type;

    CommandLine( int ArgC, char** ArgV );

    const input_files_type& InputFiles( ) const;

    method_type Method( ) const;

    void Usage( int ExitValue ) const;

    void operator( )( );

    static const int DEFAULT_DEBUG_LEVEL;

private:
    options_type      options_visible;
    options_type      options_hidden;
    user_options_type user_options;
    char* const       program_name;

    method_type method;
};

const int CommandLine::DEFAULT_DEBUG_LEVEL = 30;

CommandLine::CommandLine( int ArgC, char** ArgV )
    : options_visible( "Allowed options" ), options_hidden( "Hidden options" ),
      program_name( ArgV[ 0 ] )
{
    //-------------------------------------------------------------------
    // Options not visible to help
    //-------------------------------------------------------------------
    options_hidden.add_options( )( "input-file",
                                   po::value< std::vector< std::string > >( ),
                                   "input frame" );
    //-------------------------------------------------------------------
    // Options visible to help
    //-------------------------------------------------------------------
    options_visible.add_options( )( "help,h", "Print help message and exit" );
    //-------------------------------------------------------------------
    // Positional options
    //-------------------------------------------------------------------
    po::positional_options_description p;
    p.add( "input-file", -1 );

    options_type options_parse;
    options_parse.add( options_visible ).add( options_hidden );

    po::store( po::command_line_parser( ArgC, ArgV )
                   .options( options_parse )
                   .positional( p )
                   .run( ),
               user_options );
    po::notify( user_options );

    if ( user_options.count( "help" ) )
    {
        Usage( 0 );
    }
}

inline const CommandLine::input_files_type&
CommandLine::InputFiles( ) const
{
    return ( user_options[ "input-file" ].as< input_files_type >( ) );
}

inline CommandLine::method_type
CommandLine::Method( ) const
{
    return method;
}

inline void
CommandLine::Usage( int ExitValue ) const
{
    std::cerr << "Usage: " << program_name
              << " [options] <filename> [<filename>...]" << std::endl
              << options_visible << std::endl;
    depart( ExitValue );
}

inline void
CommandLine::operator( )( )
{
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
class channel_list_type : public FrameCPP::FrTOC::FunctionString
{
public:
    typedef std::map< std::string, std::set< std::string > > channel_container;

    channel_container channels;

    channel_list_type( );

    virtual void operator( )( const std::string& ChannelName );

private:
    typedef std::regex  pattern_type;
    typedef std::smatch match_type;

    match_type          match{};
    static pattern_type pattern;
};

channel_list_type::pattern_type channel_list_type::pattern( "^(.*)[.](.*)$" );

channel_list_type::channel_list_type( )
{
}

void
channel_list_type::operator( )( const std::string& ChannelName )
{
    static const char* caller = "channel_list_type::operator()()";

    std::regex_match( ChannelName, match, pattern );
    QUEUE_LOG_MESSAGE( "Cataloging channel: " << ChannelName,
                       MT_DEBUG,
                       30,
                       caller,
                       "ldas_trend" );
    if ( match.size( ) == 3 )
    {
        std::ssub_match channel_name = match[ 1 ];
        std::ssub_match channel_stat = match[ 2 ];
        QUEUE_LOG_MESSAGE( "Cataloging channel base: "
                               << channel_name << " stat: " << channel_stat,
                           MT_DEBUG,
                           30,
                           caller,
                           "ldas_trend" );
        channels[ channel_name ].insert( channel_stat );
    }
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

int
main( int ArgC, char** ArgV )
{
    static const char*  caller = "main";
    MemChecker::Trigger gc_trigger( true );

    FrameCPP::Initialize( );

    try
    {
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        //-------------------------------------------------------------------
        // Variables for logging
        //-------------------------------------------------------------------
        static char cwd_buffer[ 2048 ];

        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (const char*)NULL )
        {
            exit( 1 );
        }
        std::string cwd( cwd_buffer );
        GenericAPI::LoggingInfo::LogDirectory( cwd );
        LDASTools::AL::Log::stream_file_type fs(
            new LDASTools::AL::Log::StreamFile );
        LDASTools::AL::Log::stream_type s;

        s = fs;
        GenericAPI::setLogTag( "trend" );
        GenericAPI::setLogDebugLevel( CommandLine::DEFAULT_DEBUG_LEVEL );
        GenericAPI::LogFormatter( )->EntriesMax( 500 );
        GenericAPI::LogFormatter( )->Stream( s );

        CommandLine cl( ArgC, ArgV );

        cl( );

        switch ( cl.Method( ) )
        {
        case CommandLine::STORE_TREND_CHANNELS_AS_FRPROC:
            store_trend_channels_as_frproc( cl );
            break;
        }
    }
    catch ( const std::exception& Except )
    {
        QUEUE_LOG_MESSAGE( " Caught a  std::exception: " << Except.what( ),
                           MT_DEBUG,
                           30,
                           caller,
                           "CMD_CREATE_RDS" );
    }
    catch ( ... )
    {
        QUEUE_LOG_MESSAGE( " Caught an unknown exception",
                           MT_DEBUG,
                           30,
                           caller,
                           "CMD_CREATE_RDS" );
    }

    return 0;
}

//-----------------------------------------------------------------------
/// \brief Stores multiple channels in a single FrProc
///
/// This method seeks to compress the meta data of the trend frames
/// by storing related trend channels in an FrProcData 2 dimensional
/// vector.
//-----------------------------------------------------------------------
static void
store_trend_channels_as_frproc( const CommandLine& Options )
{
    static const char* caller = "store_trend_channels_as_frproc";

    FrameCPP::GPSTime read_time_acc[ MAX_FORMATS ];
    FrameCPP::GPSTime sread_time_acc[ MAX_FORMATS ];
    FrameCPP::GPSTime write_time_acc[ MAX_FORMATS ];
    StopWatch         timer;

    try
    {
        std::string infilename;
        std::string outfilename[ MAX_FORMATS ];

        for ( auto infilename : Options.InputFiles( ) )
        {
            using namespace FrameCPP;

            for ( size_t x = 0; x < MAX_FORMATS; ++x )
            {
                read_time_acc[ x ] = FrameCPP::GPSTime( 0, 0 );
                sread_time_acc[ x ] = FrameCPP::GPSTime( 0, 0 );
                write_time_acc[ x ] = FrameCPP::GPSTime( 0, 0 );
            }
            QUEUE_LOG_MESSAGE( "Working on file: " << infilename,
                               MT_DEBUG,
                               10,
                               caller,
                               "ldas_trend" );
            //-----------------------------------------------------------------
            // Open the frame file
            //-----------------------------------------------------------------
            timer.Start( );
            auto ifs =
                boost::make_shared< IFrameFStream >( infilename.c_str( ) );
            timer.Stop( );
            read_time_acc[ OLD_FORMAT ] += timer.Elapsed( );

            boost::shared_ptr< OFrameFStream > ofs[ MAX_FORMATS ];

            timer.Start( );
            outfilename[ OLD_FORMAT ] = infilename;
            outfilename[ OLD_FORMAT ] += ".old";
            ofs[ OLD_FORMAT ].reset(
                new OFrameFStream( outfilename[ OLD_FORMAT ].c_str( ) ) );
            timer.Stop( );
            write_time_acc[ OLD_FORMAT ] += timer.Elapsed( );

            timer.Start( );
            outfilename[ NEW_FORMAT ] = infilename;
            outfilename[ NEW_FORMAT ] += ".new";
            ofs[ NEW_FORMAT ].reset(
                new OFrameFStream( outfilename[ NEW_FORMAT ].c_str( ) ) );
            timer.Stop( );
            write_time_acc[ NEW_FORMAT ] += timer.Elapsed( );

            //-----------------------------------------------------------------
            // Loop over each FrAdcData element to generate a channel list
            //-----------------------------------------------------------------
            IFrameFStream::toc_ret_type toc( ifs->GetTOC( ) );
            channel_list_type           channels;

            toc->ForEach( FrTOC::TOC_CHANNEL_NAMES, channels );
            //-----------------------------------------------------------------
            // Loop over list of unique channel names
            //-----------------------------------------------------------------
            OFrameFStream::frameh_type frameh[ MAX_FORMATS ];
            frameh[ NEW_FORMAT ].reset(
                new OFrameFStream::frameh_type::element_type );
            frameh[ OLD_FORMAT ].reset(
                new OFrameFStream::frameh_type::element_type );

            frameh[ OLD_FORMAT ]->SetRawData(
                FrameH::rawData_type( new FrRawData ) );

            for ( channel_list_type::channel_container::const_iterator
                      cur = channels.channels.begin( ),
                      last = channels.channels.end( );
                  cur != last;
                  ++cur )
            {
                //---------------------------------------------------------------
                // Generate FrProcData structure
                //---------------------------------------------------------------
                QUEUE_LOG_MESSAGE(
                    "Creating FrProcData for channel: " << cur->first,
                    MT_DEBUG,
                    20,
                    caller,
                    "ldas_trend" );
                FrProcData sfrproc( cur->first,
                                    "", /* Comment */
                                    TREND_DATA,
                                    TREND_DATA_SUBTYPE,
                                    0, /* TimeOffset */
                                    FR_PROC_DATA_DEFAULT_TRANGE, /* TRange */
                                    FR_PROC_DATA_DEFAULT_FSHIFT, /* FShift */
                                    FR_PROC_DATA_DEFAULT_PHASE, /* Phase */
                                    FR_PROC_DATA_DEFAULT_FRANGE, /* FRange */
                                    FR_PROC_DATA_DEFAULT_BW /* BW */ );

                frameh[ NEW_FORMAT ]->RefProcData( ).append( sfrproc );
                boost::shared_ptr< FrProcData > frproc =
                    frameh[ NEW_FORMAT ]->RefProcData( ).back( );

                //---------------------------------------------------------------
                // Create table of statistical information
                //---------------------------------------------------------------

                boost::shared_ptr< FrProcData::table_value_type > stats(
                    new FrProcData::table_value_type( cur->first, 0 ) );

                frproc->RefTable( ).append( stats );

                for ( channel_list_type::channel_container::mapped_type::
                          const_iterator s_cur = cur->second.begin( ),
                                         s_last = cur->second.end( );
                      s_cur != s_last;
                      ++s_cur )
                {
                    std::ostringstream c;

                    c << cur->first << "." << *s_cur;
                    timer.Start( );
                    IFrameFStream::fr_adc_data_type adc =
                        ifs->ReadFrAdcData( 0, c.str( ) );
                    timer.Stop( );
                    read_time_acc[ OLD_FORMAT ] += timer.Elapsed( );

                    frameh[ OLD_FORMAT ]->GetRawData( )->RefFirstAdc( ).append(
                        adc );

                    stats->RefColumn( ).append( adc->RefData( ).front( ) );
                    stats->RefColumn( ).back( )->SetName( *s_cur );
                }
            }
            //-----------------------------------------------------------------
            // Add to frame
            //-----------------------------------------------------------------
            timer.Start( );
            ofs[ OLD_FORMAT ]->WriteFrame(
                frameh[ OLD_FORMAT ], FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP, 1 );
            timer.Stop( );
            write_time_acc[ OLD_FORMAT ] += timer.Elapsed( );

            timer.Start( );
            ofs[ NEW_FORMAT ]->WriteFrame(
                frameh[ NEW_FORMAT ], FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP, 1 );
            timer.Stop( );
            write_time_acc[ NEW_FORMAT ] += timer.Elapsed( );

            timer.Start( );
            ofs[ OLD_FORMAT ].reset( );
            timer.Stop( );
            write_time_acc[ OLD_FORMAT ] += timer.Elapsed( );

            timer.Start( );
            ofs[ NEW_FORMAT ].reset( );
            timer.Stop( );
            write_time_acc[ NEW_FORMAT ] += timer.Elapsed( );

            {
                //---------------------------------------------------------------
                // Open the new file
                //---------------------------------------------------------------
                timer.Start( );
                ifs.reset(
                    new IFrameFStream( outfilename[ NEW_FORMAT ].c_str( ) ) );
                timer.Stop( );
                read_time_acc[ NEW_FORMAT ] += timer.Elapsed( );
                //---------------------------------------------------------------
                // Read each FrProcData
                //---------------------------------------------------------------
                IFrameFStream::fr_proc_data_type proc;

                for ( channel_list_type::channel_container::const_iterator
                          cur = channels.channels.begin( ),
                          last = channels.channels.end( );
                      cur != last;
                      ++cur )
                {
                    timer.Start( );
                    try
                    {
                        proc = ifs->ReadFrProcData( 0, cur->first );
                    }
                    catch ( ... )
                    {
                    }
                    timer.Stop( );
                    read_time_acc[ NEW_FORMAT ] += timer.Elapsed( );
                }
                //---------------------------------------------------------------
                // Close
                //---------------------------------------------------------------
                timer.Start( );
                ifs.reset( );
                timer.Stop( );
                read_time_acc[ NEW_FORMAT ] += timer.Elapsed( );
            }
            //=================================================================
            // Single channel read
            //=================================================================
            {
                //---------------------------------------------------------------
                // Open the old file
                //---------------------------------------------------------------
                timer.Start( );
                ifs.reset(
                    new IFrameFStream( outfilename[ OLD_FORMAT ].c_str( ) ) );
                timer.Stop( );
                sread_time_acc[ OLD_FORMAT ] += timer.Elapsed( );
                //---------------------------------------------------------------
                // Read each FrProcData
                //---------------------------------------------------------------
                IFrameFStream::fr_adc_data_type adc;

                timer.Start( );
                try
                {
                    std::string channel( channels.channels.begin( )->first );

                    channel += ".min";
                    adc = ifs->ReadFrAdcData( 0, channel );
                }
                catch ( ... )
                {
                }
                timer.Stop( );
                sread_time_acc[ OLD_FORMAT ] += timer.Elapsed( );
                //---------------------------------------------------------------
                // Close
                //---------------------------------------------------------------
                timer.Start( );
                ifs.reset( );
                timer.Stop( );
                sread_time_acc[ OLD_FORMAT ] += timer.Elapsed( );
            }
            {
                //---------------------------------------------------------------
                // Open the new file
                //---------------------------------------------------------------
                timer.Start( );
                ifs.reset(
                    new IFrameFStream( outfilename[ NEW_FORMAT ].c_str( ) ) );
                timer.Stop( );
                sread_time_acc[ NEW_FORMAT ] += timer.Elapsed( );
                //---------------------------------------------------------------
                // Read each FrProcData
                //---------------------------------------------------------------
                IFrameFStream::fr_proc_data_type proc;

                timer.Start( );
                try
                {
                    proc = ifs->ReadFrProcData(
                        0, channels.channels.begin( )->first );
                }
                catch ( ... )
                {
                    if ( !proc )
                    {
                        QUEUE_LOG_MESSAGE(
                            "Unable to read FrProcData channel: "
                                << channels.channels.begin( )->first
                                << " from: " << outfilename[ NEW_FORMAT ],
                            MT_ERROR,
                            0,
                            caller,
                            "ldas_trend" );
                        ;
                    }
                }
                timer.Stop( );
                sread_time_acc[ NEW_FORMAT ] += timer.Elapsed( );
                //---------------------------------------------------------------
                // Close
                //---------------------------------------------------------------
                timer.Start( );
                ifs.reset( );
                timer.Stop( );
                sread_time_acc[ NEW_FORMAT ] += timer.Elapsed( );
            }
            //=================================================================
            // Report Stats
            //=================================================================
            {
                using FrameCPP::GPSTime;

                struct stat in;
                struct stat out[ MAX_FORMATS ];

                ::stat( infilename.c_str( ), &in );
                ::stat( outfilename[ NEW_FORMAT ].c_str( ),
                        &( out[ NEW_FORMAT ] ) );
                ::stat( outfilename[ OLD_FORMAT ].c_str( ),
                        &( out[ OLD_FORMAT ] ) );

                std::cerr
                    << "Statistics: " << infilename << std::endl
                    << "     Read old frame: " << read_time_acc[ OLD_FORMAT ]
                    << std::endl
                    << "     Read new frame: " << read_time_acc[ NEW_FORMAT ]
                    << std::endl
                    << "     New is "
                    << ( ( read_time_acc[ NEW_FORMAT ] - GPSTime( ) ) /
                         ( read_time_acc[ OLD_FORMAT ] - GPSTime( ) ) * 100.0 )
                    << "% of the original" << std::endl
                    << std::endl
                    << "     Read old channel: " << sread_time_acc[ OLD_FORMAT ]
                    << std::endl
                    << "     Read new channel: " << sread_time_acc[ NEW_FORMAT ]
                    << std::endl
                    << "     New is "
                    << ( ( sread_time_acc[ NEW_FORMAT ] - GPSTime( ) ) /
                         ( sread_time_acc[ OLD_FORMAT ] - GPSTime( ) ) * 100.0 )
                    << "% of the original" << std::endl
                    << std::endl
                    << "     Wrote old frame: " << write_time_acc[ OLD_FORMAT ]
                    << std::endl
                    << "     Wrote new frame: " << write_time_acc[ NEW_FORMAT ]
                    << std::endl
                    << "     New is "
                    << ( ( write_time_acc[ NEW_FORMAT ] - GPSTime( ) ) /
                         ( write_time_acc[ OLD_FORMAT ] - GPSTime( ) ) * 100.0 )
                    << "% of the original" << std::endl
                    << std::endl
                    << " Destination is "
                    << ( double( double( out[ NEW_FORMAT ].st_size ) /
                                 double( in.st_size ) ) *
                         100.0 )
                    << "% of the original size" << std::endl;
            }
        }
    }
    catch ( const std::range_error& Exception )
    {
        //-------------------------------------------------------------------
        // Expected error from Command line processing to indicate there
        // are no more options to be processed.
        //-------------------------------------------------------------------
    }
}
