#========================================================================
# This is the master build script controlling the building of the
#   LDAS Tools Suite
#
#
# TEST_PACKAGER=<rpm|macports|deb>
# IGWN_CMAKE_MODULE_DIRS=<PATH>
# -- MacPort -- IGWN_CMAKE_MODULE_DIRS=/opt/local/share/igwn-cmake/cmake/Modules
#
# ** C O N F I G U R A T I O N **
# > cmake -DCMAKE_PREFIX_PATH=`pwd`/install -DPYTHON3_VERSION=3.4 <SourceDir>
#
# ** B U I L D **
# > cmake --build . -- PKG_CONFIG_PATH=`pwd`/install/lib/pkgconfig:${PKG_CONFIG_PATH}
#
# +++++++++++++++++++++++++++++++++++++++++++
#
# Linux 7.3.x
# cmake3 \
#   -DBoost_NO_SYSTEM_PATHS=True \
#   -DBOOST_INCLUDEDIR="/usr/include/boost169;NO_CMAKE_PATH;NO_CMAKE_ENVIRONMENT_PATH" \
#   -DBOOST_LIBRARYDIR=/usr/lib64/boost169 \
#   -DIGWN_CMAKE_MODULE_DIRS=/usr/share/igwn-cmake/cmake/Modules \
#   ~/mnt/Sources/LDAS/ldas-head && cmake3 --build . -- -j8
#
# NOTE:
#
# If using with sanitizer, then do:
# export CC=/usr/bin/clang
# export CXX=/usr/bin/clang++
# cmake3 ...
#
# +++++++++++++++++++++++++++++++++++++++++++
#
# OSX MacPorts
# cmake \
#   -DIGWN_CMAKE_MODULE_DIRS=/opt/local/share/igwn-cmake/cmake/Modules \
#   ~/Sources/LDAS/ldas-head && cmake --build .
#
# OSX MacPorts - testing of Portfiles
# ( sudo rm -rf * ; \
#   cmake \
#     -DIGWN_CMAKE_MODULE_DIRS=/opt/local/share/igwn-cmake/cmake/Modules \
#     -DIGWN_CMAKE_SCRIPTS_DIR="/opt/local/share/igwn-cmake/cmake/scripts" \
#     -DLDASToolsCMake_DIR=/opt/local/share/ldas-tools/cmake/Modules \
#     -DTEST_PACKAGER=macports ~/Sources/LDAS/ldas-head && \
#     sudo cmake --build . --target macports ) 2>&1 | tee ~/tmp/logs/ldas-head-root.log
#========================================================================

cmake_minimum_required(VERSION 3.0)
project(LDASToolsSuite NONE)
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "RellWithDebInfo" CACHE STRING "Choose the type of build." )
endif()
# Set a default value for ASAN_OPTIONS
set(ASAN_OPTIONS_DEFAULT "detect_odr_violation=0:detect_leaks=0" CACHE STRING "Default ASAN options")
# Add the ASAN_OPTIONS environment variable for test execution
set(ENV{ASAN_OPTIONS} "${ASAN_OPTIONS_DEFAULT}")
include(ExternalProject)

set( built_targets )
set(CMAKE_INSTALL_LIBDIR lib)
if (NOT "${CMAKE_CXX_STANDARD}")
  set(CMAKE_CXX_STANDARD 17)
endif( )

function(get_cmake_value RESULT DIR VAR)
  file(READ ${DIR}/CMakeLists.txt input)
  string(REPLACE "\n" ";" input "${input}")
  foreach( line ${input})
    if ( line MATCHES "set.*${VAR}" )
      string(REPLACE "set" "" line ${line})
      string(REPLACE "\(" "" line ${line})
      string(REPLACE "\)" "" line ${line})
      string(REPLACE "${VAR}" "" line ${line})
      string(STRIP "${line}" line)
      set(${RESULT} ${line})
      break()
    endif( )
  endforeach( )
  set(${RESULT} "${${RESULT}}" CACHE INTERNAL "" FORCE)
endfunction( )

function(get_cmake_values SUBDIR)
  list(LENGTH ARGN size)
  while( size GREATER 1 )
    list(GET ARGN 0 result)
    list(GET ARGN 1 var)
    get_cmake_value( ${result} ${CMAKE_SOURCE_DIR}/${SUBDIR} ${var})
    list(REMOVE_AT ARGN 0 1)
    list(LENGTH ARGN size)
  endwhile( )
endfunction( )

#get_cmake_values(
#  igwn_cmake
#  igwn_cmake_name PROJECT_NAME
#  igwn_cmake_ver  PROJECT_VERSION
#  )
get_cmake_values(
  ldastools_cmake
  ldastools_cmake_name PROJECT_NAME
  ldastools_cmake_ver  PROJECT_VERSION
  )
get_cmake_values(
  diskcache
  diskcache_name LDAS_TOOLS_DISKCACHEAPI_PACKAGE_NAME
  diskcache_ver  LDAS_TOOLS_DISKCACHEAPI_VERSION
  )
get_cmake_values(
  diskcache-swig
  diskcache_swig_name LDAS_TOOLS_DISKCACHEAPI_SWIG_PACKAGE_NAME
  diskcache_swig_ver  LDAS_TOOLS_DISKCACHEAPI_SWIG_VERSION
  )
get_cmake_values(
  filters
  filters_name LDAS_TOOLS_FILTERS_PACKAGE_NAME
  filters_ver  LDAS_TOOLS_FILTERS_VERSION
  )
get_cmake_values(
  framecpp
  framecpp_name LDAS_TOOLS_FRAMECPP_PACKAGE_NAME
  framecpp_ver  LDAS_TOOLS_FRAMECPP_VERSION
  )
get_cmake_values(
  framecpp-swig
  framecpp_swig_name  LDAS_TOOLS_FRAMECPP_SWIG_PACKAGE_NAME
  framecpp_swig_ver   LDAS_TOOLS_FRAMECPP_SWIG_VERSION
  )
get_cmake_values(
  frameutils
  frameutils_name LDAS_TOOLS_FRAMEAPI_PACKAGE_NAME
  frameutils_ver  LDAS_TOOLS_FRAMEAPI_VERSION
  )
get_cmake_values(
  frameutils-swig
  frameutils_swig_name LDAS_TOOLS_FRAMEAPI_SWIG_PACKAGE_NAME
  frameutils_swig_ver  LDAS_TOOLS_FRAMEAPI_SWIG_VERSION
  )
get_cmake_values(
  ldasgen
  ldasgen_name LDAS_TOOLS_LDASGEN_PACKAGE_NAME
  ldasgen_ver  LDAS_TOOLS_LDASGEN_VERSION
  )
get_cmake_values(
  ldasgen-swig
  ldasgen_swig_name LDAS_TOOLS_LDASGEN_SWIG_PACKAGE_NAME
  ldasgen_swig_ver  LDAS_TOOLS_LDASGEN_SWIG_VERSION
  )
get_cmake_values(
  ldastoolsal
  ldastoolsal_name LDAS_TOOLS_AL_PACKAGE_NAME
  ldastoolsal_ver  LDAS_TOOLS_AL_VERSION
  )
get_cmake_values(
  ldastoolsal-swig
  ldastoolsal_swig_name    LDAS_TOOLS_AL_SWIG_PACKAGE_NAME
  ldastoolsal_swig_ver     LDAS_TOOLS_AL_SWIG_VERSION
  ldas_tools_swig_verion LDAS_TOOLS_SWIG_VERSION
  )
get_cmake_values(
  suite
  suite_name LDAS_TOOLS_SUITE_PACKAGE_NAME
  suite_ver  LDAS_TOOLS_SUITE_VERSION
  )
get_cmake_values(
  utilities
  utilities_name LDAS_TOOLS_UTILITIES_PACKAGE_NAME
  utilities_ver  LDAS_TOOLS_UTILITIES_VERSION
  )

set(installDir ${CMAKE_CURRENT_BINARY_DIR}/test_install)
set( ENABLE_DOCUMENTATION_ONLY FALSE CACHE BOOL "True if only documentation is to be generated" )
set( TEST_PACKAGER IGNORE CACHE STRING "Specify the OS native packager (ex: rpm, macports, deb)" )
set( BUILD_FRAMECPP_ONLY FALSE CACHE BOOL "Build only the components needed by the FrameCPP Library" )
set( BUILD_SUBSET "" CACHE STRING "Build only the components needed by the FrameCPP Library" )

set( PKG_CONFIG_PATH_ ${installDir}/${CMAKE_INSTALL_LIBDIR}/pkgconfig:$ENV{PKG_CONFIG_PATH} )
set( ENV_PROGRAM ${CMAKE_COMMAND} -E env )

set( PACKAGE_DIRECTORIES
  diskcache
  diskcache-swig
  filters
  framecpp
  framecpp-swig
  frameutils
  frameutils-swig
  ldasgen
  ldasgen-swig
  ldastoolsal
  ldastoolsal-swig
  utilities
  )

find_program( CMAKE_TEST_PROGRAM NAMES ctest3 ctest )

if ( TEST_PACKAGER )
  add_custom_target( ${TEST_PACKAGER}
    COMMAND ${CMAKE_COMMAND} -E echo "Completed: ${TEST_PACKAGER}" )
endif( )

function( package_info_defines VAR PACKAGE_DIR )
  #if( PACKAGE_DIR MATCHES "/igwn_cmake")
  #  set( ${VAR}
  #    -DCPACK_RPM_PACKAGE_NAME=${igwn_cmake_name}
  #    -DCPACK_RPM_PACKAGE_VERSION=${igwn_cmake_ver}
  #    )
  if( PACKAGE_DIR MATCHES "/ldastools_cmake")
    set( ${VAR}
      -DCPACK_RPM_PACKAGE_NAME=${ldastools_cmake_name}
      -DCPACK_RPM_PACKAGE_VERSION=${ldastools_cmake_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/diskcache-swig")
    set( ${VAR}
      -DPROJECT_NAME=${diskcache_swig_name}
      -DPROJECT_VERSION=${diskcache_swig_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/diskcache")
    set( ${VAR}
      -DPROJECT_NAME=${diskcache_name}
      -DPROJECT_VERSION=${diskcache_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/filters")
    set( ${VAR}
      -DPROJECT_NAME=${filters_name}
      -DPROJECT_VERSION=${filters_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/framecpp-swig")
    set( ${VAR}
      -DPROJECT_NAME=${framecpp_swig_name}
      -DPROJECT_VERSION=${framecpp_swig_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/framecpp")
    set( ${VAR}
      -DPROJECT_NAME=${framecpp_name}
      -DPROJECT_VERSION=${framecpp_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/frameutils-swig")
    set( ${VAR}
      -DPROJECT_NAME=${frameutils_swig_name}
      -DPROJECT_VERSION=${frameutils_swig_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/frameutils")
    set( ${VAR}
      -DPROJECT_NAME=${frameutils_name}
      -DPROJECT_VERSION=${frameutils_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/ldasgen-swig")
    set( ${VAR}
      -DPROJECT_NAME=${ldasgen_swig_name}
      -DPROJECT_VERSION=${ldasgen_swig_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/ldasgen")
    set( ${VAR}
      -DPROJECT_NAME=${ldasgen_name}
      -DPROJECT_VERSION=${ldasgen_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/ldastoolsal-swig")
    set( ${VAR}
      -DPROJECT_NAME=${ldastoolsal_swig_name}
      -DPROJECT_VERSION=${ldastoolsal_swig_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/ldastoolsal")
    set( ${VAR}
      -DPROJECT_NAME=${ldastoolsal_name}
      -DPROJECT_VERSION=${ldastoolsal_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/suite")
    set( ${VAR}
      -DPROJECT_NAME=${suite_name}
      -DPROJECT_VERSION=${suite_ver}
      )
  elseif( PACKAGE_DIR MATCHES "/utilities")
    set( ${VAR}
      -DPROJECT_NAME=${utilities_name}
      -DPROJECT_VERSION=${utilities_ver}
      )
  endif( )
  list(APPEND ${VAR}
    -DCPACK_RPM_PACKAGE_RELEASE=1
    -DPROJECT_SUMMARY="Summary"
    -DPROJECT_DESCRIPTION="Description"
    -DPROJECT_DESCRIPTION_LONG="Long Description"
    -DCPACK_SOURCE_PACKAGE_FILE_EXTENSION=".tar.gz"
    -DPACKAGE_URL="url"
    -DLDAS_TOOLS_AL_VERSION=${ldastoolsal_ver}
    -DLDAS_TOOLS_AL_SWIG_VERSION=${ldastoolsal_swig_ver}
    -DLDAS_TOOLS_CMAKE_VERSION=${ldastools_cmake_ver}
    -DLDAS_TOOLS_DISKCACHEAPI_VERSION=${diskcache_ver}
    -DLDAS_TOOLS_DISKCACHEAPI_SWIG_VERSION=${diskcache_swig_ver}
    -DLDAS_TOOLS_FILTERS_VERSION=${filters_ver}
    -DLDAS_TOOLS_FRAMEAPI_VERSION=${frameutils_ver}
    -DLDAS_TOOLS_FRAMEAPI_SWIG_VERSION=${frameutils_swig_ver}
    -DLDAS_TOOLS_FRAMECPP_VERSION=${framecpp_ver}
    -DLDAS_TOOLS_FRAMECPP_SWIG_VERSION=${framecpp_swig_ver}
    -DLDAS_TOOLS_LDASGEN_VERSION=${ldasgen_ver}
    -DLDAS_TOOLS_LDASGEN_SWIG_VERSION=${ldasgen_swig_ver}
    -DLDAS_TOOLS_SUITE_VERSION=${suite_ver}
    -DLDAS_TOOLS_UTILIITES_VERSION=${utilities_ver}
    -DLDAS_TOOLS_SWIG_VERSION=${ldas_tools_swig_verion}
  )
  if ( "${CMAKE_CXX_STANDARD}" )
    list( APPEND ${VAR}
      -DCMAKE_CXX_STANDARD="${CMAKE_CXX_STANDARD}")
  endif( )
  set(${VAR} ${${VAR}} PARENT_SCOPE)
endfunction( )

function( DebBuildDependencies VAR SPEC_FILE )
  file( READ ${SPEC_FILE} dependencies )
  string( REGEX REPLACE "\n" ";" dependencies "${dependencies}" )
  unset(deps)
  set( stage 1 )
  foreach(line ${dependencies})
    string(REGEX REPLACE "[ \t\n]+$" "" line ${line} )
    string(REGEX REPLACE "^[ \t\n]+" "" line ${line} )
    if ( stage EQUAL 1 )
      if( "${line}" MATCHES "^[Bb]uild-[Dd]epends:" )
        string( REGEX REPLACE "^[Bb]uild-[Dd]epends:" "" line "${line}" )
        string( REGEX REPLACE "," ";" line "${line}" )
        string( REGEX REPLACE "[ \t]+" "" line "${line}" )
        list( APPEND deps ${line} )
        set( stage 2 )
      endif ( )
    elseif( stage EQUAL 2 )
      if ( NOT "${line}" MATCHES ",[ \t]*$" )
        set( stage 1 )
      endif( )
      string( REGEX REPLACE "," ";" line "${line}" )
      string( REGEX REPLACE "[ \t]+" "" line "${line}" )
      list( APPEND deps ${line} )
    endif( )
  endforeach( )
  string( REGEX REPLACE "[(][^)]*[)]" "" deps "${deps}")
  #list( REMOVE_ITEM deps debhelper )
  message( STATUS "DEB dependencies: ${deps}")
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

function( RPMBuildDependencies VAR )
  set( options )
  set( oneValueArgs
    EXCLUDE_PATTERN
    TOP_DIR
    )
  set( multiValueArgs )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_EXCLUDE_PATTERN )
    set( ARG_EXCLUDE_PATTERN "(ldas-tools|cmake3)")
  endif( )

  package_info_defines( defines ${ARG_TOP_DIR} )
  message( WARNING "${ARG_TOP_DIR} defines ${defines}" )

  execute_process(
    OUTPUT_VARIABLE PY3
    COMMAND rpm --eval %python3_pkgversion
    )
  execute_process(
    OUTPUT_VARIABLE ${VAR}
    COMMAND ${CMAKE_COMMAND}
    -DEXCLUDE_PATTERN='${ARG_EXCLUDE_PATTERN}'
    -DPACKAGER=rpm
    -DTOP_DIR=${ARG_TOP_DIR}
    -DPYTHON3_VERSION=${PY3}
    ${defines}
    -P ${CMAKE_SOURCE_DIR}/igwn_cmake/scripts/dependencies.cmake
    )
  string(REPLACE "\n" ";" ${VAR} "${${VAR}}")
  message(WARNING "${VAR} ${${VAR}}")
  set( ${VAR} ${${VAR}} PARENT_SCOPE )
endfunction( )

function( RPMBuildDependencies2 VAR SPEC_FILE )
  file( READ ${SPEC_FILE} dependencies )
  execute_process(
    COMMAND rpm  --eval "${dependencies}"
    OUTPUT_VARIABLE dependencies
    ERROR_VARIABLE dependencies
    )
  string( REGEX REPLACE "\n" ";" dependencies "${dependencies}")
  unset(deps)
  foreach(line ${dependencies})
    if( "${line}" MATCHES "^[Bb]uild[Rr]equires:" )
      string(REGEX REPLACE "^[Bb]uild[Rr]equires:[ \t]*" "" line ${line})
      string(REGEX REPLACE ">=[ \t]*(([0-9]+)[.])*[0-9]+" "" line ${line})
      string(REGEX REPLACE ",[ \t*]" ";" line ${line})
      list(APPEND deps ${line})
    endif( )
  endforeach( )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

function( TestPackaging )
  if ( TEST_PACKAGER )
    set( options )
    set( oneValueArgs PACKAGE_NAME )
    set( multiValueArgs DEPENDS CONFIGURE_OPTIONS )
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    set( pkgr ${TEST_PACKAGER} )
    unset( pkg_depends )
    foreach( d ${ARG_DEPENDS} )
      list( APPEND pkg_depends ${d}-${pkgr} )
    endforeach( )
    unset( pkg_cmd )
    unset( post_pkg_cmd )
    if ( pkgr STREQUAL rpm )
      # -----------------------------------------------------------------
      #   RedHat
      # -----------------------------------------------------------------
      RPMBuildDependencies(
        deps
        TOP_DIR "${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}"
        )
      set( pkg_cmd yum install -y )
      set( post_pkg_cmd /bin/sh -c "yum --nogpgcheck localinstall -y /root/rpmbuild/RPMS/*/*ldas-tools-*.rpm" )
      set( setup ${pkg_cmd} ${deps} )
    elseif ( pkgr STREQUAL deb )
      # -----------------------------------------------------------------
      #   Debian
      # -----------------------------------------------------------------
      file( GLOB_RECURSE SPEC_FILE ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/config/control* )
      if ( NOT SPEC_FILE )
        file( GLOB_RECURSE SPEC_FILE  ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/debian/control )
      endif( )
      DebBuildDependencies( deps ${SPEC_FILE} )
      string( REGEX REPLACE ";[^;]*ldas-tools-[^;]*;" ";" deps "${deps}" )
      string( REGEX REPLACE "^[^;]*ldas-tools-[^;]*;" "" deps "${deps}" )
      string( REGEX REPLACE ";[^;]*ldas-tools-[^;]*$" "" deps "${deps}" )
      set( pkg_cmd_wait_on_lock /bin/bash ${CMAKE_SOURCE_DIR}/apt-get-lock-guard  )
      set( pkg_cmd_update apt-get update)
      set( pkg_cmd_fix_broken apt-get -y --fix-broken install)
      set( pkg_cmd_install apt-get -y install)
      set( post_pkg_cmd
        ${pkg_cmd_wait_on_lock}
        COMMAND /bin/sh -c "dpkg -i `find ${CMAKE_BINARY_DIR}/${ARG_PACKAGE_NAME}-prefix/src/${ARG_PACKAGE_NAME}-build -name \\*.deb` || true"
        COMMAND ${pkg_cmd_wait_on_lock}
        COMMAND ${pkg_cmd_fix_broken} )
      set( setup ${pkg_cmd_wait_on_lock}
        COMMAND ${pkg_cmd_update}
        COMMAND ${pkg_cmd_wait_on_lock}
        COMMAND ${pkg_cmd_install} ${deps}
        COMMAND rm -rf ${installDir}
        )
    elseif ( pkgr STREQUAL macports )
      # -----------------------------------------------------------------
      #   MacPorts
      # -----------------------------------------------------------------
      # file( GLOB_RECURSE SPEC_FILE  ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/*/Portfile.in )
      #if ( NOT SPEC_FILE )
      #  file( GLOB_RECURSE SPEC_FILE  ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/debian/control )
      #endif( )
      #DebBuildDependencies( deps ${SPEC_FILE} )
      set( post_pkg_cmd ${CMAKE_COMMAND} -E echo "Installation done as part of building step" )
      set( setup ${CMAKE_COMMAND} -E echo "No setup for macports" )
    endif ( )
    ExternalProject_Add_Step(
      ${ARG_PACKAGE_NAME} ${pkgr}-setup
      COMMENT "Installing project dependencies"
      COMMAND ${setup}
      ALWAYS 0
      DEPENDEES patch
      DEPENDERS configure
      )
    ExternalProject_Add_Step(
      ${ARG_PACKAGE_NAME} ${pkgr}
      COMMENT "Building project packages"
      COMMAND $(MAKE) ${MAKE_PARALLEL} ${pkgr}
      WORKING_DIRECTORY <BINARY_DIR>
      ALWAYS 0
      DEPENDEES install
      )
    ExternalProject_Add_Step(
      ${ARG_PACKAGE_NAME} ${pkgr}-post
      COMMENT "Installing project packages"
      COMMAND ${post_pkg_cmd}
      WORKING_DIRECTORY <BINARY_DIR>
      ALWAYS 0
      DEPENDEES ${pkgr}
      )
    add_dependencies(
      ${pkgr}
      ${ARG_PACKAGE_NAME}-${pkgr}-setup
      ${ARG_PACKAGE_NAME}-${pkgr}
      ${ARG_PACKAGE_NAME}-${pkgr}-post
      )
    ExternalProject_Add_StepTargets( ${ARG_PACKAGE_NAME} ${pkgr} ${pkgr}-setup ${pkgr}-post )
    if ( pkg_depends )
      add_dependencies( ${ARG_PACKAGE_NAME}-${pkgr} ${pkg_depends} )
    endif( )
  endif( )
endfunction( )

function( BuildAutoToolsPackage )
  set( options )
  set( oneValueArgs PACKAGE_NAME )
  set( multiValueArgs DEPENDS CONFIGURE_OPTIONS )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set( target_cmds_ "" )
  if ( NOT ARG_PACKAGE_DIR )
    set( ARG_PACKAGE_DIR ${ARG_PACKAGE_NAME} )
  endif( )

  set(_PackageName ${ARG_PACKAGE_NAME} )
  set(_PackageDir ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_DIR})

  if ( ARG_DEPENDS )
    list( INSERT ARG_DEPENDS 0 "DEPENDS" )
  endif( )
  set( CONFIGURE_OPTIONS_ --prefix ${installDir} --libdir ${installDir}/${CMAKE_INSTALL_LIBDIR} )
  if ( ARG_CONFIGURE_OPTIONS )
    list(APPEND CONFIGURE_OPTIONS_ ${ARG_CONFIGURE_OPTIONS} )
  endif( )
  set( TP_BUILD_COMMAND )
  set( TP_INSTALL_COMMAND )
  set( TP_TEST_COMMAND TEST_COMMAND $(MAKE) check )
  if ( TEST_PACKAGER )
    set( TP_BUILD_COMMAND BUILD_COMMAND ${CMAKE_COMMAND} -E echo "Skipping as part of testing packaging rules" )
    set( TP_INSTALL_COMMAND INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping as part of testing packaging rules" )
    set( TP_TEST_COMMAND TEST_COMMAND ${CMAKE_COMMAND} -E echo "Skipping as part of testing packaging rules" )
  endif( )
  if ( ENABLE_DOCUMENTATION_ONLY )
    list(APPEND CONFIGURE_OPTIONS_ "--enable-documentation-only" )
    set( target_cmds_
      BUILD_COMMAND ${CMAKE_COMMAND} -E echo "Only generating documentation"
      INSTALL_COMMAND ${CMAKE_COMMAND} --build . --target install-html
      )
  endif( )
  ExternalProject_Add(${_PackageName}
    ${ARG_DEPENDS}
    SOURCE_DIR ${_PackageDir}
    INSTALL_DIR ${installDir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -E chdir ${_PackageDir} autoreconf --install > /dev/null 2>&1 || ${CMAKE_COMMAND} -E chdir ${_PackageDir} autoreconf
    COMMAND ${ENV_PROGRAM} PKG_CONFIG_PATH=${PKG_CONFIG_PATH_} ${_PackageDir}/configure ${CONFIGURE_OPTIONS_}
    ${TP_BUILD_COMMAND}
    ${TP_INSTALL_COMMAND}
    ${TP_TEST_COMMAND}
    ${target_cmds_}
    )
  TestPackaging(
    PACKAGE_NAME ${_PackageName}
    DEPENDS ${ARG_DEPENDS} )
endfunction( )

macro( append_configure_option_ VARIABLE )
  if ( DEFINED ${VARIABLE} )
    if(NOT DEFINED ARGV1)
      set(var_type "PATH")
    else()
      set(var_type "${ARGV1}")
    endif()
    list( APPEND CONFIGURE_OPTIONS_
      -D${VARIABLE}:${var_type}=${${VARIABLE}}
      )
  endif( )
endmacro( )
function( BuildCMakePackage )
  set( options DOCUMENTATION_ONLY_IGNORE )
  set( oneValueArgs PACKAGE_NAME DOCUMENTATION_ONLY_INSTALL_TARGET )
  set( multiValueArgs DEPENDS CONFIGURE_OPTIONS )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PACKAGE_DIR )
    set( ARG_PACKAGE_DIR ${ARG_PACKAGE_NAME} )
  endif( )

  if ( ARG_DOCUMENTATION_ONLY_IGNORE AND ENABLE_DOCUMENTATION_ONLY )
    return( )
  endif( )
  set(_PackageName ${ARG_PACKAGE_NAME} )
  set(_PackageDir ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_DIR})

  set( CONFIGURE_OPTIONS_
    -DCM_MSG_DEBUG_VERBOSE:BOOL=${CM_MSG_DEBUG_VERBOSE}
    -DPKG_CONFIG_PATH=${PKG_CONFIG_PATH_}
    -DCMAKE_INSTALL_PREFIX:PATH=${installDir}
    -DCMAKE_PREFIX_PATH:PATH=${installDir}
    -DMACPORTS_FORCE_INSTALLS:BOOL=${MACPORTS_FORCE_INSTALL}
    -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
    -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
    -DABI_DIR:PATH=${ABI_DIR}
  )
  append_configure_option_(SANITIZERS STRING)
  append_configure_option_(BUILD_TESTING BOOL)
  append_configure_option_(Boost_NO_SYSTEM_PATHS)
  append_configure_option_(BOOST_INCLUDEDIR)
  append_configure_option_(BOOST_LIBRARYDIR)
  append_configure_option_(CMAKE_BUILD_TYPE)
  append_configure_option_(CMAKE_CXX_STANDARD)
  append_configure_option_(IGWN_CMAKE_MODULE_DIRS)
  append_configure_option_(IGWN_CMAKE_SCRIPTS_DIR)
  append_configure_option_(LDASToolsCMake_DIR)
  if ( VERBOSE )
    list( APPEND CONFIGURE_OPTIONS_
      -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
      )
  endif( )
  foreach( pyvar
      ENABLE_SWIG_PYTHON3
      PYTHON3_VERSION
      PYTHON3_EXECUTABLE
      )
    if ( DEFINED ${pyvar} )
      list( APPEND CONFIGURE_OPTIONS_ "-D${pyvar}=${${pyvar}}" )
    endif( )
  endforeach( )
  if ( ENABLE_DOCUMENTATION_ONLY )
    list( APPEND CONFIGURE_OPTIONS_ "-DENABLE_DOCUMENTATION_ONLY=TRUE" )
    set( install_target_ "doc" )
    if ( ARG_DOCUMENTATION_ONLY_INSTALL_TARGET )
      set( install_target_ ${ARG_DOCUMENTATION_ONLY_INSTALL_TARGET} )
    endif( )
    set( _BUILD_COMMAND
      BUILD_COMMAND ${CMAKE_COMMAND} -E echo "Only generating documentation"
      INSTALL_COMMAND ${CMAKE_COMMAND} --build . --target ${install_target_}
      )
  elseif ( NOT TEST_PACKAGER )
    set( _BUILD_COMMAND
      TEST_COMMAND ${CMAKE_CTEST_COMMAND}
      COMMAND ${CMAKE_COMMAND} -E env cat Testing/Temporary/LastTest.log
      )
  endif( )
  if ( ARG_CONFIGURE_OPTIONS )
    list(APPEND CONFIGURE_OPTIONS_ ${ARG_CONFIGURE_OPTIONS} )
  endif( )

  if ( ARG_DEPENDS )
    if ( ENABLE_DOCUMENTATION_ONLY )
      set( new_list )
      foreach( elem ${ARG_DEPENDS} )
        list( FIND built_targets ${elem} found )
        if ( NOT found EQUAL -1 )
          list( APPEND new_list ${elem} )
        endif( )
      endforeach( )
      set( ARG_DEPENDS ${new_list} )
    endif( )
    list( INSERT ARG_DEPENDS 0 "DEPENDS" )
  endif( )
  if ( TEST_PACKAGER )
    set( _BUILD_COMMAND
      BUILD_COMMAND ${CMAKE_COMMAND} -E echo "Build will be done as part of packaging"
      INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Install will be done as part of packaging"
      TEST_COMMAND ${CMAKE_COMMAND} -E echo "Test will be done as part of packaging"
      )
  endif( )
  list( APPEND built_targets ${_PackageName} )
  set( built_targets ${built_targets} PARENT_SCOPE )
  ExternalProject_Add( ${_PackageName}
    ${ARG_DEPENDS}
    SOURCE_DIR ${_PackageDir}
    INSTALL_DIR ${installDir}
    CMAKE_ARGS ${CONFIGURE_OPTIONS_}
    ${_BUILD_COMMAND}
    )
  set( _PackageNameBuildDir "${_PackageName}-prefix/src/${_PackageName}-build" )
  #----------------------------------------------------------------------
  add_custom_target( abi-check-${_PackageName}
    COMMENT "Checking ABI/API compatability"
    COMMAND $(MAKE) ${MAKE_PARALLEL} abi-check
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${_PackageNameBuildDir}
    )
  add_dependencies( abi-check abi-check-${_PackageName})
  #----------------------------------------------------------------------
  add_custom_target( doc-${_PackageName}
    COMMENT "Building documentation"
    COMMAND ${CMAKE_COMMAND} --build .  --target doc
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${_PackageNameBuildDir}
    )
  add_dependencies( doc doc-${_PackageName})
  #----------------------------------------------------------------------
  if ( NOT ARG_DOCUMENTATION_ONLY_INSTALL_TARGET )
    add_custom_target( install-doc-${_PackageName}
      COMMENT "Installing documentation"
      COMMAND ${CMAKE_COMMAND} --build .  --target install
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${_PackageNameBuildDir}/doc
      )
    add_dependencies( install-doc install-doc-${_PackageName})
  endif( )
  #----------------------------------------------------------------------
  TestPackaging(
    PACKAGE_NAME ${_PackageName}
    DEPENDS ${ARG_DEPENDS} )
endfunction( )

set( _BUILD_VARIABLES
  _BUILD_IGWN_CMAKE
  _BUILD_LDASTOOLS_CMAKE
  _BUILD_LDAS_TOOLS_AL
  _BUILD_LDAS_TOOLS_AL_SWIG
  _BUILD_FRAMECPP
  _BUILD_FRAMECPP_SWIG
  _BUILD_FILTERS
  _BUILD_LDAS_GEN
  _BUILD_LDAS_GEN_SWIG
  _BUILD_DISK_CACHE
  _BUILD_DISK_CACHE_SWIG
  _BUILD_FRAME_UTILS
  _BUILD_FRAME_UTILS_SWIG
  _BUILD_UTILITIES
  _BUILD_SUITE
  )
foreach( var ${_BUILD_VARIABLES} )
  set( ${var} True CACHE INTERNAL "" )
endforeach( )

if ( BUILD_FRAMECPP_ONLY )
  foreach( var ${_BUILD_VARIABLES} )
    set( ${var} False CACHE INTERNAL "" FORCE )
  endforeach( )
  #set( _BUILD_IGWN_CMAKE True CACHE INTERNAL "" FORCE )
  set( _BUILD_LDASTOOLS_CMAKE True CACHE INTERNAL "" FORCE )
  set( _BUILD_LDAS_TOOLS_AL True CACHE INTERNAL "" FORCE )
  set( _BUILD_FRAMECPP True CACHE INTERNAL "" FORCE )
endif( )

if ( BUILD_SUBSET )
  foreach( var ${_BUILD_VARIABLES} )
    set( ${var} False CACHE INTERNAL "" FORCE )
  endforeach( )
  if ( BUILD_SUBSET STREQUAL "FRAMECPP" )
    set( _SUBSET
      #_BUILD_IGWN_CMAKE
      _BUILD_LDASTOOLS_CMAKE
      _BUILD_LDAS_TOOLS_AL
      _BUILD_FRAMECPP
      )
  elseif ( BUILD_SUBSET STREQUAL "FRAMECPP_SWIG" )
    set( _SUBSET
      #_BUILD_IGWN_CMAKE
      _BUILD_LDASTOOLS_CMAKE
      _BUILD_LDAS_TOOLS_AL
      _BUILD_LDAS_TOOLS_AL_SWIG
      _BUILD_FRAMECPP
      _BUILD_FRAMECPP_SWIG
      )
  #elseif ( BUILD_SUBSET STREQUAL "IGWN_CMAKE" )
  #  set( _SUBSET
  #    _BUILD_IGWN_CMAKE
  #    )
  elseif ( BUILD_SUBSET STREQUAL "LDASTOOLS_CMAKE" )
    set( _SUBSET
      #_BUILD_IGWN_CMAKE
      _BUILD_LDASTOOLS_CMAKE
      )
  elseif ( BUILD_SUBSET STREQUAL "LDASTOOLSAL" )
    set( _SUBSET
      #_BUILD_IGWN_CMAKE
      _BUILD_LDASTOOLS_CMAKE
      _BUILD_LDAS_TOOLS_AL
      )
  endif( )
  if ( _SUBSET )
    foreach( var ${_SUBSET} )
      set( ${var} True CACHE INTERNAL "" FORCE )
    endforeach( )
  endif( )
endif( )

if( NOT DEFINED CM_MSG_DEBUG_VERBOSE )
  set( CM_MSG_DEBUG_VERBOSE FALSE )
endif( )
if( NOT DEFINED SANITIZER )
  set( SANITIZER address )
endif( )
if( NOT DEFINED VERBOSE )
  set( VERBOSE ${CM_MSG_DEBUG_VERBOSE} )
endif( )

foreach( target abi-check doc install-doc )
  add_custom_target( ${target} )
endforeach( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
#if ( _BUILD_IGWN_CMAKE )
#  BuildCMakePackage( PACKAGE_NAME igwn_cmake
#    DOCUMENTATION_ONLY_INSTALL_TARGET install
#    )
#endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if ( _BUILD_LDASTOOLS_CMAKE )
  BuildCMakePackage( PACKAGE_NAME ldastools_cmake
    DOCUMENTATION_ONLY_INSTALL_TARGET install
    )
endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if ( _BUILD_LDAS_TOOLS_AL )
  BuildCMakePackage( PACKAGE_NAME ldastoolsal
    DEPENDS
    ldastools_cmake
    )
endif( )
if ( _BUILD_SUITE )
  BuildCMakePackage( PACKAGE_NAME suite
    DOCUMENTATION_ONLY_INSTALL_TARGET install
    DEPENDS
    ldastools_cmake
    )
endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if ( _BUILD_FRAMECPP )
  BuildCMakePackage( PACKAGE_NAME framecpp
    DEPENDS
    ldastools_cmake
    ldastoolsal
    )
endif( )
if ( _BUILD_FILTERS )
BuildCMakePackage( PACKAGE_NAME filters
  DEPENDS
    ldastoolsal
    ldastools_cmake
    )
endif( )
if ( _BUILD_LDAS_GEN)
  BuildCmakePackage( PACKAGE_NAME ldasgen
    DEPENDS
    ldastoolsal
    ldastools_cmake
    )
endif( )
if ( _BUILD_LDAS_TOOLS_AL_SWIG)
  BuildCMakePackage( PACKAGE_NAME ldastoolsal-swig
    DOCUMENTATION_ONLY_IGNORE
    DEPENDS
    ldastoolsal
    ldastools_cmake
    )
endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if ( _BUILD_FRAMECPP_SWIG )
  BuildCMakePackage( PACKAGE_NAME framecpp-swig
    DOCUMENTATION_ONLY_IGNORE
    DEPENDS
    framecpp
    ldastoolsal-swig
    ldastoolsal
    ldastools_cmake
    )
endif( )
if ( _BUILD_LDAS_GEN_SWIG )
  BuildCMakePackage( PACKAGE_NAME ldasgen-swig
    DOCUMENTATION_ONLY_IGNORE
    DEPENDS
    ldasgen
    ldastoolsal-swig
    ldastoolsal
    ldastools_cmake
    )
endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if ( _BUILD_DISK_CACHE )
  BuildCMakePackage( PACKAGE_NAME diskcache
    CONFIGURE_OPTIONS -DWITH_SYSTEMDSYSTEMUNITDIR:PATH=${installDir}/${CMAKE_INSTALL_LIBDIR}/systemd/system
    DEPENDS
    ldasgen
    ldastoolsal
    ldastools_cmake
    )
endif( )
if ( _BUILD_FRAME_UTILS )
  BuildCMakePackage( PACKAGE_NAME frameutils
    DEPENDS
    ldasgen
    filters
    framecpp
    ldastoolsal
    )
endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if ( _BUILD_DISK_CACHE_SWIG )
  BuildCMakePackage( PACKAGE_NAME diskcache-swig
    DOCUMENTATION_ONLY_IGNORE
    DEPENDS
    diskcache
    ldasgen-swig
    ldasgen
    ldastoolsal-swig
    ldastoolsal
    ldastools_cmake
    )
endif( )
if ( _BUILD_FRAME_UTILS_SWIG )
  BuildCMakePackage( PACKAGE_NAME frameutils-swig
    DOCUMENTATION_ONLY_IGNORE
    DEPENDS
    frameutils
    ldasgen-swig
    ldasgen
    framecpp-swig
    framecpp
    ldastoolsal-swig
    ldastoolsal
    ldastools_cmake
    )
endif( )

#------------------------------------------------------------------------
#------------------------------------------------------------------------

if ( _BUILD_UTILITIES )
  BuildCMakePackage( PACKAGE_NAME utilities
    DEPENDS
    diskcache
    frameutils
    ldasgen
    filters
    framecpp
    ldastoolsal
    ldastools_cmake
    )
endif( )

# -----------------------------------------------------------------------
#  Rules to format code according to a standard
# -----------------------------------------------------------------------

add_custom_target(format-source)
foreach( dir ${PACKAGE_DIRECTORIES} )
  add_custom_target( format-source-${dir}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${dir}
    COMMAND
    ${CMAKE_COMMAND}
    -DSOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR}/${dir}
    -P ${CMAKE_SOURCE_DIR}/suite/config/format-source.cmake
    )
  add_dependencies(format-source format-source-${dir})
endforeach( )
