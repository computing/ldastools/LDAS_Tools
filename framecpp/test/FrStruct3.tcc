//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__FR_STRUCT_3_TCC
#define FRAME_CPP__TEST__FR_STRUCT_3_TCC

#include "framecpp/Version3/FrameH.hh"
#include "framecpp/Version3/FrAdcData.hh"
#include "framecpp/Version3/FrDetector.hh"
#include "framecpp/Version3/FrHistory.hh"
#include "framecpp/Version3/FrHistory.hh"
#include "framecpp/Version3/FrMsg.hh"
#include "framecpp/Version3/FrProcData.hh"
#include "framecpp/Version3/FrRawData.hh"
#include "framecpp/Version3/FrSerData.hh"
#include "framecpp/Version3/FrSimData.hh"
#include "framecpp/Version3/FrStatData.hh"
#include "framecpp/Version3/FrSummary.hh"
#include "framecpp/Version3/FrTrigData.hh"
#include "framecpp/Version3/FrVect.hh"

#define TEMPLATE_SPEC 3
#define NAMESPACE FrameCPP::Version_3
#define USING( ) using namespace NAMESPACE

//=======================================================================
// Frame Object
//=======================================================================
template <>
mk_frame_object_ret_type
mk_frame_object< 3 >( FrameObjectTypes Type )
{
  USING( );

  using FrameCPP::Common::FrameSpec;

  mk_frame_object_ret_type retval;

  switch( Type )
  {
  case FrameSpec::Info::FSI_FRAME_H:
    retval.reset( new FrameH( "frame_h",
			      1,
			      8,
			      GPSTime( 10, 20 ),
			      1,
			      3600,
			      3.0
			      ) );
    break;
  case FrameSpec::Info::FSI_FR_ADC_DATA:
    retval.reset( new FrAdcData( "fr_adc_data",
				 3, 2,
				 8,
				 1024.,
				 2.0,
				 1.0, std::string( "meters" ),
				 30.,
				 10, 20,
				 4 ) );
    reinterpret_cast< FrAdcData* >( retval.get( ) )->AppendComment(  "test data" );
    break;
  case FrameSpec::Info::FSI_FR_DETECTOR:
    retval.reset( new FrDetector( "fr_detector_name",
				  10, 20, 40.2,
				  15, 18, 35.2,
				  200.3,
				  300.4, 400.5, 4000.0 ) );
    break;
  case FrameSpec::Info::FSI_FR_HISTORY:
    {
      std::unique_ptr< FrHistory >
	fr_history( new FrHistory( "fr_history_name", 10, "fr_hsitory_comment" ) );
      retval.reset( fr_history.release( ) );
    }
    break;
  case FrameSpec::Info::FSI_FR_MSG:
    {
      std::unique_ptr< FrMsg >
	fr_msg( new FrMsg( "alarm", "message", 10 ) );
      retval.reset( fr_msg.release( ) );
    }
    break;
  case FrameSpec::Info::FSI_FR_PROC_DATA:
    //-------------------------------------------------------------------
    // FrProcData
    //-------------------------------------------------------------------
    retval.reset( new FrProcData( "fr_proc_data",
				 "fr_proc_data_comment",
				  1024.,
				  GPSTime( 10, 2048 * 2048 ),
				  2048. ) );
    
    break;
  case FrameSpec::Info::FSI_FR_RAW_DATA:
    retval.reset( new FrRawData( "fr_raw_data"
				  ) );
    break;
  case FrameSpec::Info::FSI_FR_SER_DATA:
    //-------------------------------------------------------------------
    // FrSerData
    //-------------------------------------------------------------------
    retval.reset( new FrSerData( "fr_ser_data",
				 GPSTime( 10, 20 ),
				 1024. ) );
    
    reinterpret_cast< FrSerData* >( retval.get( ) )->SetData(  "test data" );
    break;
  case FrameSpec::Info::FSI_FR_SIM_DATA:
    //-------------------------------------------------------------------
    // FrSimData
    //-------------------------------------------------------------------
    retval.reset( new FrSimData( "fr_sim_data",
				 "fr_sim_data_comment",
				 1024.
				 ) );
    break;
  case FrameSpec::Info::FSI_FR_STAT_DATA:
    //-------------------------------------------------------------------
    // FrStatData
    //-------------------------------------------------------------------
    retval.reset( new FrStatData( "fr_stat_data",
				  "fr_stat_data_comment",
				  1,
				  2,
				  4
				  ) );
    break;
  case FrameSpec::Info::FSI_FR_SIM_EVENT:
    //-------------------------------------------------------------------
    // FrSimEvent
    //-------------------------------------------------------------------
    break;
  case FrameSpec::Info::FSI_FR_SUMMARY:
    //-------------------------------------------------------------------
    // FrSummary
    //-------------------------------------------------------------------
    retval.reset( new FrSummary( "fr_summary",
				 "fr_summary_comment",
				 "fr_summary_test"
				 ) );
    
    break;
  case FrameSpec::Info::FSI_FR_TABLE:
    //-------------------------------------------------------------------
    // FrTable
    //-------------------------------------------------------------------
    break;
  case FrameSpec::Info::FSI_FR_TRIG_DATA:
    //-------------------------------------------------------------------
    // FrTrigData
    //-------------------------------------------------------------------
    retval.reset( new FrTrigData( "fr_trig_data",
				  "fr_trig_data_comment",
				  "fr_trig_data_inputs",
				  GPSTime( 10, 20000000 ),
				  1,
				  16.0,
				  32.0,
				  "fr_trig_data_statistics"
				  ) );
    
    break;
  case FrameSpec::Info::FSI_FR_VECT:
    //-------------------------------------------------------------------
    // FrVect
    //-------------------------------------------------------------------
    {
      typedef REAL_8 vect_data_type;

      static const int SAMPLES = 4;
      static const vect_data_type START = 16.0;
      static const vect_data_type INC = 0.5;

      Dimension dim( SAMPLES );
      vect_data_type data[ SAMPLES ];

      vect_data_type cur_val = START;

      for ( int cur = 0; cur != SAMPLES; ++cur )
      {
	data[ cur ] = cur_val;
	cur_val += INC;
      }
      
      retval.reset( new FrVect( "fr_vect",
				1,
				&dim,
				data,
				"fr_vect_unitY"
				) )
	;
    }
    break;
  default:
    {
      std::ostringstream	msg;

      msg << "mk_frame_obj<3>: Unsupported type: "
	  << Type
	;
      throw
	FrameCPP::Unimplemented( msg.str( ),
				 3, __FILE__, __LINE__ );
    }
    break;
  }
  return retval;
}

//=======================================================================
#undef USING
#undef NAMESPACE
#undef TEMPLATE_SPEC

#endif /* FRAME_CPP__TEST__FR_STRUCT_3_TCC */
