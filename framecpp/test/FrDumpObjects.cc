//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

///=======================================================================
// This program is useful in dumping out the information from a frame
// file.
//
// Currently supported modes are hex and post processed.
//
// :TODO: Currently the Version 6 frames are supported in the post
// :TODO:   process mode.
//
//=======================================================================
#if HAVE_CONFIG_H
#include <framecpp_config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <list>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/shared_array.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/regex.hh"
#include "ldastoolsal/regexmatch.hh"
#include "ldastoolsal/reverse.hh"
#include "ldastoolsal/types.hh"

#include "framecpp/Common/Verify.hh"
#include "framecpp/FrameCPP.hh"

#include "StandardOptions.hh"

using LDASTools::AL::MemChecker;
using LDASTools::AL::unordered_map;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;

typedef INT_2U class_id_type;
typedef INT_2U version_type;

enum
{
    CLASS_ID_COMMON_ELEMENTS = 0,
    CLASS_ID_FrSH = 1,
    CLASS_ID_FrSE = 2
};

enum data_type_type
{
    DATA_TYPE_UNKNOWN,
    DATA_TYPE_CHAR,
    DATA_TYPE_CHAR_U,
    DATA_TYPE_INT_2S,
    DATA_TYPE_INT_2U,
    DATA_TYPE_INT_4S,
    DATA_TYPE_INT_4U,
    DATA_TYPE_INT_8S,
    DATA_TYPE_INT_8U,
    DATA_TYPE_REAL_4,
    DATA_TYPE_REAL_8,
    DATA_TYPE_STRING,
    DATA_TYPE_PTR_STRUCT,
    DATA_TYPE_COMPLEX_8,
    DATA_TYPE_COMPLEX_16
};

class FrameStream;
class FrObject;
class SymbolTable;

static INT_8U ClassSize( const std::string& Class );

class CommandLine;

inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

//-----------------------------------------------------------------------
// Description declaration
//-----------------------------------------------------------------------
class Description
{
public:
    struct info
    {
        std::string s_name;
        std::string s_class;
        std::string s_comment;
    };

    typedef std::list< info >    ses_type;
    typedef std::vector< void* > struct_data_type;

    void AddSE( const std::string& Name,
                const std::string& Class,
                const std::string& Comment );

    inline std::string
    Class( INT_4U Index ) const
    {
        std::string retval;

        if ( ( Index > 0 ) && ( Index < m_se.size( ) ) )
        {
            ses_type::const_iterator cur = m_se.begin( );
            while ( Index-- )
            {
                ++cur;
            }
            retval = cur->s_class;
        }
        return retval;
    }

    inline void
    ClassName( const std::string& ClassName )
    {
        m_class_name = ClassName;
    }

    inline const std::string&
    ClassName( ) const
    {
        return m_class_name;
    }

    void Dump( const struct_data_type& Data, const CommandLine& Options ) const;

    void DumpCommonElements( const struct_data_type& Data,
                             const std::string&      ClassName ) const;

    bool Read( FrameStream& FStream, struct_data_type& Data ) const;

    template < typename T >
    T Value( const struct_data_type& Data, INT_2U Offset ) const;

private:
    std::string m_class_name;
    ses_type    m_se;

    void* read( FrameStream& FStream, const std::string& Class );
};

//=======================================================================
// Pre knowledge - These are the classes that need to be known before
//   anything can be read.
//=======================================================================
class FrHeader
{
public:
    bool ByteSwapping( ) const;

    void Dump( ) const;

    void Read( );

    inline INT_2U
    Version( ) const
    {
        return m_data.core.ver;
    }

private:
    union data
    {
        CHAR_U raw[ 40 ];
        struct
        {
            CHAR_U orig[ 5 ];
            CHAR_U ver;
        } core;
        struct
        {
            CHAR_U orig[ 5 ];
            CHAR_U ver;
            CHAR_U rev;
            CHAR_U sizeof_int_2;
            CHAR_U sizeof_int_4;
            CHAR_U sizeof_int_8;
            CHAR_U sizeof_real_4;
            CHAR_U sizeof_real_8;
            CHAR_U order_2_byte[ 2 ];
            CHAR_U order_4_byte[ 4 ];
            CHAR_U order_8_byte[ 8 ];
            CHAR_U pi_4_byte[ 4 ];
            CHAR_U pi_8_byte[ 8 ];
            CHAR_U az[ 2 ];
        } v3;
        struct
        {
            CHAR_U orig[ 5 ];
            CHAR_U ver;
            CHAR_U rev;
            CHAR_U sizeof_int_2;
            CHAR_U sizeof_int_4;
            CHAR_U sizeof_int_8;
            CHAR_U sizeof_real_4;
            CHAR_U sizeof_real_8;
            CHAR_U order_2_byte[ 2 ];
            CHAR_U order_4_byte[ 4 ];
            CHAR_U order_8_byte[ 8 ];
            CHAR_U pi_4_byte[ 4 ];
            CHAR_U pi_8_byte[ 8 ];
            CHAR_U frame_lib;
            CHAR_U checksum_method;
        } v8;
    };

    data m_data;
};

class FrObject
{
public:
    typedef INT_4U class_id_type;
    typedef INT_8U length_type;

    FrObject( FrameStream& Stream );

    void AppendSE( Description& D ) const;

    void Dump( const CommandLine& Options ) const;

    class_id_type FrSHClassId( ) const;

    std::string FrSHClassName( ) const;

    inline length_type
    Length( ) const
    {
        return m_length;
    }

    inline class_id_type
    StructId( ) const
    {
        return m_struct_id;
    }

private:
    union
    {
        struct
        {
            class_id_type class_id;
            char const*   class_name;
        } FrSH;
        struct
        {
        } FrSE;
    } m_cache;

    std::streampos                m_start_pos;
    Description::struct_data_type m_common_elements;
    Description::struct_data_type m_data;

    length_type   m_length;
    class_id_type m_struct_id;
};

class ARRAY_TYPE
{
public:
    static INT_4U m_DumpNElements;

    ARRAY_TYPE( FrameStream&       Stream,
                const std::string& Class,
                const SymbolTable& Symbols );

    ~ARRAY_TYPE( );

    inline static bool
    Match( const std::string& Class )
    {
        RegexMatch m( 3 );

        if ( m.match( pattern, Class.c_str( ) ) )
        {
            return true;
        }
        return false;
    }

    std::string Value( ) const;

private:
    typedef boost::shared_array< CHAR_U > data_type;

    static Regex   pattern;
    static Regex   m_multidimensional_pattern;
    data_type      m_data;
    data_type_type m_data_type;
    INT_4U         m_size;

    template < typename TYPE >
    void dump( std::ostringstream& Stream ) const;

    template < typename TYPE >
    void read( FrameStream& Stream, INT_4U Size );
};

class STRING
{
public:
    STRING( );

    STRING( FrameStream& Stream );

    static inline bool
    Match( const std::string& ClassName )
    {
        return ( ClassName == "STRING" );
    }

    void Read( FrameStream& Stream );

    inline const CHAR*
    Value( ) const
    {
        if ( m_value.get( ) )
        {
            return m_value.get( );
        }
        return "<NULL>";
    }

private:
    INT_2U                      m_length;
    boost::shared_array< CHAR > m_value;
};

class PTR_STRUCT
{
public:
    PTR_STRUCT( FrameStream& Stream );

    static inline bool
    Match( const std::string& ClassName )
    {
        return ( ClassName.substr( 0, 10 ) == "PTR_STRUCT" );
    }

    inline std::string
    Value( ) const
    {
        std::ostringstream retval;

        retval << m_data_class << "-" << m_data_instance;
        return retval.str( );
    }

private:
    INT_2U m_data_class;
    INT_4U m_data_instance;
};

//=======================================================================
// Dynamic interface
//=======================================================================

class InputStream
{
public:
    typedef INT_8U position_type;
    InputStream( );

    std::istream& Advance( position_type Position );

    std::istream& AdvanceABS( position_type Position );

    inline position_type
    Position( ) const
    {
        return m_position;
    }

    std::istream& Read( void* Buffer, position_type Size );

    std::istream& operator( )( std::istream& Stream );

    inline bool
    good( )
    {
        return ( m_stream && m_stream->good( ) );
    }

private:
    std::istream* m_stream;
    position_type m_position;
};

class FrameStream
{
public:
    typedef INT_2U index_type;
    typedef INT_8U size_type;

    //---------------------------------------------------------------------
    /// \brief Add a description to list of known objects
    //---------------------------------------------------------------------
    void AddDescription( class_id_type Id, const Description& Description );

    //---------------------------------------------------------------------
    /// \brief Append an object's description
    //---------------------------------------------------------------------
    void AppendDescription( const FrObject& Object, bool Pre );

    //---------------------------------------------------------------------
    /// \brief Obtain the class id index within set of common elements
    //---------------------------------------------------------------------
    inline index_type
    ClassIdIndex( ) const
    {
        return m_class_id_index;
    }

    //---------------------------------------------------------------------
    /// \brief Obtain the class id index within set of common elements
    //---------------------------------------------------------------------
    inline size_type
    CommonElementSize( ) const
    {
        return m_common_element_size;
    }

    //---------------------------------------------------------------------
    /// \brief True if byte swapping is required
    //---------------------------------------------------------------------
    inline bool
    ByteSwapping( ) const
    {
        return m_header.ByteSwapping( );
    }

    //---------------------------------------------------------------------
    /// \brief Retrieve the class definition
    ///
    /// \param Id
    ///     The class id to lookup
    ///
    /// \return
    ///     A reference to the description object describing Id.
    //---------------------------------------------------------------------
    const Description& GetDescription( class_id_type Id ) const;

    //---------------------------------------------------------------------
    /// \brief Obtain the length index within set of common elements
    //---------------------------------------------------------------------
    inline index_type
    LengthIndex( ) const
    {
        return m_length_index;
    }

    //---------------------------------------------------------------------
    /// \brief Read an element from the stream
    ///
    /// \param Class
    ///     The name of the data type being read
    /// \param SymbolTable
    ///     Collection of objects already read which may be consulted
    ///     when reading array data.
    ///
    /// \return
    ///     The data read from the stream
    //---------------------------------------------------------------------
    void* Read( const std::string& Class, const SymbolTable& SymbolTable );

    std::string String( const std::string& Class, void* Data ) const;

    //---------------------------------------------------------------------
    /// \brief True if byte swapping is required
    //---------------------------------------------------------------------
    inline INT_2U
    Version( ) const
    {
        return m_header.Version( );
    }

    //---------------------------------------------------------------------
    /// \param Filename
    ///     The name of the input file
    //---------------------------------------------------------------------
    void operator( )( LDASTools::AL::ifstream& Filename );

    template < typename T >
    static T Value( const std::string& DataType, void* Data );

private:
    typedef unordered_map< class_id_type, Description > structs_type;

    bool         m_byte_swapping;
    FrHeader     m_header;
    structs_type m_structs;
    index_type   m_class_id_index;
    index_type   m_length_index;
    size_type    m_common_element_size;

    /// \brief Reset all the data members
    void reset( );
};

//-----------------------------------------------------------------------
// SymbolTable
//-----------------------------------------------------------------------
class SymbolTable
{
public:
    typedef const Description::struct_data_type& data_type;
    typedef const Description::ses_type&         data_desc_type;
    SymbolTable( data_desc_type DataDesc, data_type Values )
        : m_data_desc( DataDesc ), m_data( Values )
    {
    }

    inline INT_4U
    size( ) const
    {
        return m_data_desc.size( );
    }

    inline INT_8U
    LookupInteger( const std::string& Key ) const
    {
        INT_8U                                        retval = 0;
        Description::struct_data_type::const_iterator symbol = m_data.begin( );

        std::istringstream m( Key );
        m >> retval;

        if ( retval == 0 )
        {
            for ( Description::ses_type::const_iterator
                      cur = m_data_desc.begin( ),
                      last = m_data_desc.end( );
                  cur != last;
                  ++cur, ++symbol )
            {
                if ( cur->s_name == Key )
                {
                    if ( cur->s_class == "CHAR" )
                    {
                        retval = *( static_cast< CHAR* >( *symbol ) );
                    }
                    else if ( cur->s_class == "CHAR_U" )
                    {
                        retval = *( static_cast< CHAR_U* >( *symbol ) );
                    }
                    else if ( cur->s_class == "INT_2S" )
                    {
                        retval = *( static_cast< INT_2S* >( *symbol ) );
                    }
                    else if ( cur->s_class == "INT_2U" )
                    {
                        retval = *( static_cast< INT_2U* >( *symbol ) );
                    }
                    else if ( cur->s_class == "INT_4S" )
                    {
                        retval = *( static_cast< INT_4S* >( *symbol ) );
                    }
                    else if ( cur->s_class == "INT_4U" )
                    {
                        retval = *( static_cast< INT_4U* >( *symbol ) );
                    }
                    else if ( cur->s_class == "INT_8S" )
                    {
                        retval = *( static_cast< INT_8S* >( *symbol ) );
                    }
                    else if ( cur->s_class == "INT_8U" )
                    {
                        retval = *( static_cast< INT_8U* >( *symbol ) );
                    }
                    break;
                }
            }
        }
        return retval;
    }

private:
    data_desc_type m_data_desc;
    data_type      m_data;
};

enum
{
    MODE_STRUCT,
    MODE_HEX
};

class Output
{
public:
    Output( std::ostream& Stream );

    template < class T >
    std::ostream& operator<<( const T& Data );

private:
    std::ostream& m_stream;
};

static InputStream IStream;
static FrameStream FStream;
static Output      Log( std::cout );

//-----------------------------------------------------------------------
/// \brief Class to handle command line options for this application
//-----------------------------------------------------------------------
class CommandLine : protected CommandLineOptions
{
public:
    using CommandLineOptions::argc_type;
    using CommandLineOptions::argv_type;

    typedef std::list< std::string > channel_container_type;

    CommandLine( argc_type ArgC, argv_type ArgV );

    inline bool
    BadOption( ) const
    {
        bool retval = false;

        for ( const_iterator cur = begin( ), last = end( ); cur != last; ++cur )
        {
            if ( ( *cur )[ 0 ] == '-' )
            {
                std::cerr << "ABORT: Bad option: " << *cur << std::endl;
                retval = true;
            }
        }
        return retval;
    }

    inline INT_4U
    BlockSize( ) const
    {
        return m_block_size;
    }

    inline INT_4U
    MaximumDataElements( ) const
    {
        return m_maximum_data_elements;
    }

    inline bool
    MemoryMappedIO( ) const
    {
        return m_memory_mapped_io;
    }

    inline bool
    Silent( ) const
    {
        return m_silent;
    }

    inline bool
    SilentData( ) const
    {
        return m_silent_data;
    }

    inline void
    Usage( int ExitValue ) const
    {
        std::cout << "Usage: " << ProgramName( ) << m_options << std::endl;
        depart( ExitValue );
    }

    using CommandLineOptions::empty;
    using CommandLineOptions::Pop;
    using CommandLineOptions::size;

private:
    enum option_types
    {
        OPT_BLOCK_SIZE,
        OPT_HELP,
        OPT_MAXIMUM_DATA_ELEMENTS,
        OPT_MEMORY_MAPPED_IO,
        OPT_SILENT,
        OPT_SILENT_DATA
    };

    OptionSet m_options;
    INT_4U    m_block_size;
    INT_4U    m_maximum_data_elements;
    bool      m_memory_mapped_io;
    bool      m_silent;
    bool      m_silent_data;
};

CommandLine::CommandLine( argc_type ArgC, argv_type ArgV )
    : CommandLineOptions( ArgC, ArgV ), m_block_size( 256 ),
      m_maximum_data_elements( 0 ), m_memory_mapped_io( false ),
      m_silent( false ), m_silent_data( false )
{
    INT_4U multiplier = 1024;

    //---------------------------------------------------------------------
    // Setup the options that will be recognized.
    //---------------------------------------------------------------------
    std::ostringstream desc;

    m_options.Synopsis( "[options] <file> [<file> ...]" );

    m_options.Summary(
        "This command will dump the contents of the selected frame files." );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Specify the size in bytes of the input and output buffer."
            " If the number is followed by the letter 'b', 'k' or 'M',"
            " then the buffer size is multiplied by 1, 1024 or"
            " 1048576 respectively. (Default: "
         << BlockSize( ) << ( ( multiplier == 1024 ) ? "k" : "M" ) << " )";

    m_options.Add( Option( OPT_BLOCK_SIZE,
                           "block-size",
                           Option::ARG_REQUIRED,
                           desc.str( ),
                           "size" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add(
        Option( OPT_HELP, "help", Option::ARG_NONE, "Display this message" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Restricts the number of data elements from the FrVect's data "
            "structure"
            " that are displayed."
            " A value of zero (0) is interpreted as unlimited."
            " (Default: "
         << MaximumDataElements( ) << " )";
    m_options.Add( Option( OPT_MAXIMUM_DATA_ELEMENTS,
                           "maximum-data-elements",
                           Option::ARG_REQUIRED,
                           desc.str( ),
                           "size" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Enables memory mapped i/o for the input data."
            " (Default: "
         << ( ( MemoryMappedIO( ) ) ? "enabled" : "disabled" ) << " )";
    m_options.Add( Option( OPT_MEMORY_MAPPED_IO,
                           "memory-mapped-io",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Supress the output of the data structures."
            " (Default: "
         << ( ( Silent( ) ) ? "silent" : "verbose" ) << " )";
    m_options.Add(
        Option( OPT_SILENT, "silent", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    StandardOption< STANDARD_OPTION_SILENT_DATA >(
        m_options, OPT_SILENT_DATA, false );

    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------

    try
    {
        std::string arg_name;
        std::string arg_value;
        bool        parsing = true;

        while ( parsing )
        {
            const int cmd_id( Parse( m_options, arg_name, arg_value ) );

            switch ( cmd_id )
            {
            case CommandLineOptions::OPT_END_OF_OPTIONS:
                parsing = false;
                break;
            case OPT_HELP:
            {
                Usage( 0 );
            }
            break;
            case OPT_BLOCK_SIZE:
            {
                if ( arg_value.length( ) >= 1 )
                {
                    switch ( arg_value[ arg_value.length( ) - 1 ] )
                    {
                    case 'b':
                        multiplier = 1;
                        break;
                    case 'k':
                        multiplier = 1024;
                        break;
                    case 'M':
                        multiplier = 1024 * 1024;
                        break;
                    default:
                        break;
                    }
                }

                std::istringstream num( arg_value.substr(
                    0,
                    ( multiplier == 1 ) ? std::string::npos
                                        : arg_value.length( ) - 1 ) );
                num >> m_block_size;
            }
            break;
            case OPT_MAXIMUM_DATA_ELEMENTS:
            {
                std::istringstream num( arg_value );

                num >> m_maximum_data_elements;
            }
            break;
            case OPT_MEMORY_MAPPED_IO:
                m_memory_mapped_io = true;
                break;
            case OPT_SILENT:
                m_silent = true;
                break;
            case OPT_SILENT_DATA:
                m_silent_data = true;
                break;
            }
        }
    }
    catch ( ... )
    {
    }

    m_block_size *= multiplier;
}

int
main( int ArgC, const char** ArgV ) try
{
    MemChecker::Trigger         gc_trigger( true );
    CommandLine                 cl( ArgC, ArgV );
    boost::shared_array< CHAR > buffer( new CHAR[ cl.BlockSize( ) ] );
    int                         exit_status( 0 );

    if ( cl.empty( ) || cl.BadOption( ) )
    {
        cl.Usage( 1 );
    }

    FrameCPP::Initialize( );

    while ( cl.empty( ) == false )
    {
        //-------------------------------------------------------------------
        // Open the stream
        //-------------------------------------------------------------------
        std::string             filename( cl.Pop( ) );
        LDASTools::AL::ifstream ifile;

        ifile.rdbuf( )->pubsetbuf( buffer.get( ), cl.BlockSize( ) );
        ifile.rdbuf( )->UseMemoryMappedIO( cl.MemoryMappedIO( ) );
        ifile.open( filename.c_str( ) );

        if ( !ifile.good( ) )
        {
            continue;
        }

        try
        {
            //-----------------------------------------------------------------
            // Read the FrameStream structure and write out the information
            //-----------------------------------------------------------------
            FStream( ifile );

            //-----------------------------------------------------------------
            // Read and then write all of the next elements.
            //-----------------------------------------------------------------
            while ( 1 )
            {
                std::streampos oldpos = ifile.tellg( );

                try
                {
                    FrObject o( FStream );

                    if ( ifile.eof( ) || ( !ifile.good( ) ) ||
                         ( oldpos == ifile.tellg( ) ) )
                    {
                        break;
                    }
                    o.Dump( cl );
                }
                catch ( const std::range_error& Error )
                {
                    break;
                }
            }
        }
        catch ( const std::exception& e )
        {
            //-----------------------------------------------------------------
            // Report failure back to user
            //-----------------------------------------------------------------
            std::cerr << "FAILURE: " << filename << ": " << e.what( )
                      << std::endl;
            exit_status = 1;
        }
        catch ( ... )
        {
            //-----------------------------------------------------------------
            // An unexpected error occured while reading the file.
            // Abandon further reading of the file.
            //-----------------------------------------------------------------
            std::cerr << "FAILURE: unknown exception" << std::endl;
            exit_status = 1;
        }
        //-------------------------------------------------------------------
        // Close the stream
        //-------------------------------------------------------------------
        ifile.close( );
    }
    depart( exit_status );
}
catch ( std::exception& e )
{
    std::cerr << "ABORT: Caught exception: " << e.what( ) << std::endl;
    depart( 1 );
}

//=======================================================================
// Description - Definition
//=======================================================================
//-----------------------------------------------------------------------
/// Add an FrSE element to the definition
///
/// \param Name
///     Name of an element of the structure being described.
/// \param Class
///     Contains the string representation of the data type
/// \param Comment
///     Description of the element and its purpose.
//-----------------------------------------------------------------------
void
Description::AddSE( const std::string& Name,
                    const std::string& Class,
                    const std::string& Comment )
{
    info i;

    i.s_name = Name;
    i.s_class = Class;
    i.s_comment = Comment;

    m_se.push_back( i );
}

//-----------------------------------------------------------------------
/// \brief write out description
//-----------------------------------------------------------------------
void
Description::Dump( const struct_data_type& Data,
                   const CommandLine&      Options ) const
{
    void* const* retval_pos = &( Data[ 0 ] );
    bool         is_fr_vect( ClassName( ).compare( "FrVect" ) == 0 );

    for ( ses_type::const_iterator cur = m_se.begin( ), last = m_se.end( );
          cur != last;
          ++cur, ++retval_pos )
    {
        Log << "  " << cur->s_name << ": ";
        if ( ( is_fr_vect ) && ( cur->s_name.compare( "data" ) == 0 ) )
        {
            if ( Options.SilentData( ) )
            {
                //---------------------------------------------------------------
                /// \todo Need to add option to limit the number of data points
                ///    to print.
                //---------------------------------------------------------------
                Log << "...";
            }
            else
            {
                INT_4U old_max = ARRAY_TYPE::m_DumpNElements;
                ARRAY_TYPE::m_DumpNElements = Options.MaximumDataElements( );
                Log << FStream.String( cur->s_class, *retval_pos );
                ARRAY_TYPE::m_DumpNElements = old_max;
            }
        }
        else
        {
            Log << FStream.String( cur->s_class, *retval_pos );
        }
        Log << "" << std::endl;
    }
}

//-----------------------------------------------------------------------
/// \brief write out description
//-----------------------------------------------------------------------
void
Description::DumpCommonElements( const struct_data_type& Data,
                                 const std::string&      ClassName ) const
{
    void* const* retval_pos = &( Data[ 0 ] );

    for ( ses_type::const_iterator cur = m_se.begin( ), last = m_se.end( );
          cur != last;
          ++cur, ++retval_pos )
    {
        Log << " " << cur->s_name;
        if ( cur->s_name == "class" )
        {
            Log << " (" << ClassName << ")";
        }
        Log << ": " << FStream.String( cur->s_class, *retval_pos );
    }
}

//-----------------------------------------------------------------------
/// Read data from the stream using the given description.
//-----------------------------------------------------------------------
bool
Description::Read( FrameStream& FStream, struct_data_type& Data ) const
{
    Data.resize( m_se.size( ) );
    void** pos = &( ( Data )[ 0 ] );

    SymbolTable symbols( m_se, Data );
    bool        complete( true );

    ses_type::const_iterator cur = m_se.begin( ), last = m_se.end( );
    while ( cur != last )
    {
        try
        {
            *pos = FStream.Read( cur->s_class, symbols );
            ++cur;
            ++pos;
        }
        catch ( ... )
        {
            complete = false;
            break;
        }
    }
    if ( !complete )
    {
        while ( cur != last )
        {
            *pos = (void*)NULL;
            ++cur;
            ++pos;
        }
    }

    return complete;
}

template < typename T >
T
Description::Value( const struct_data_type& Data, INT_2U Offset ) const
{
    ses_type::const_iterator cur = m_se.begin( ), last = m_se.end( );
    INT_2U                   x = Offset;

    while ( ( cur != last ) && x )
    {
        ++cur;
        --x;
    }
    return FrameStream::Value< T >( cur->s_class, Data[ Offset ] );
}

//=======================================================================
// FrHeader
//=======================================================================
bool
FrHeader::ByteSwapping( ) const
{
    //---------------------------------------------------------------------
    // Determinate byte ordering
    //---------------------------------------------------------------------
    INT_4U sx12345678 = 0;

    if ( Version( ) >= 8 )
    {
        ::memcpy( &sx12345678, m_data.v8.order_4_byte, sizeof( sx12345678 ) );
    }
    else if ( Version( ) >= 3 )
    {
        ::memcpy( &sx12345678, m_data.v3.order_4_byte, sizeof( sx12345678 ) );
    }
    else
    {
        std::ostringstream msg;

        msg << "FrHeader::ByteSwapping: not supported for version: "
            << Version( );
        throw std::range_error( msg.str( ) );
    }

    return ( sx12345678 != 0x12345678 );
}

void
FrHeader::Dump( ) const
{
    Log << "Bytes: 0-4:   " << m_data.core.orig << std::endl
        << "Bytes: 5:     " << int( m_data.core.ver ) << std::endl;
    if ( Version( ) >= 8 )
    {
        Log << "Bytes: 6:     " << int( m_data.v8.rev ) << std::endl
            << "Bytes: 7:     " << int( m_data.v8.sizeof_int_2 ) << std::endl
            << "Bytes: 8:     " << int( m_data.v8.sizeof_int_4 ) << std::endl
            << "Bytes: 9:     " << int( m_data.v8.sizeof_int_8 ) << std::endl
            << "Bytes: 10:    " << int( m_data.v8.sizeof_real_4 ) << std::endl
            << "Bytes: 11:    " << int( m_data.v8.sizeof_real_8 ) << std::endl
            << "Bytes: 38:    " << int( m_data.v8.frame_lib ) << std::endl
            << "Bytes: 39:    " << int( m_data.v8.checksum_method )
            << std::endl;
    }
    else if ( Version( ) >= 3 )
    {
        Log << "Bytes: 6:     " << int( m_data.v3.rev ) << std::endl
            << "Bytes: 7:     " << int( m_data.v3.sizeof_int_2 ) << std::endl
            << "Bytes: 8:     " << int( m_data.v3.sizeof_int_4 ) << std::endl
            << "Bytes: 9:     " << int( m_data.v3.sizeof_int_8 ) << std::endl
            << "Bytes: 10:    " << int( m_data.v3.sizeof_real_4 ) << std::endl
            << "Bytes: 11:    " << int( m_data.v3.sizeof_real_8 ) << std::endl
            << "Bytes: 38-39: " << m_data.v3.az[ 0 ] << m_data.v3.az[ 1 ]
            << std::endl;
    }
}

void
FrHeader::Read( )
{
    static const INT_2U core_size( sizeof( m_data.core ) );

    FrameCPP::Common::Verify verifier;

    IStream.Read( &m_data.core, core_size );

    if ( IStream.good( ) == false )
    {
        throw std::runtime_error( "Failed to read FrHeader from the stream" );
    }

    if ( Version( ) < 8 )
    {
        IStream.Read( &m_data.v3.rev, sizeof( m_data.v3 ) - core_size );
        verifier.ExamineMagicNumber< 2 >( m_data.v3.order_2_byte );
        verifier.ExamineMagicNumber< 4 >( m_data.v3.order_4_byte );
        verifier.ExamineMagicNumber< 8 >( m_data.v3.order_8_byte );
    }
    else
    {
        IStream.Read( &m_data.v8.rev, sizeof( m_data.v8 ) - core_size );
        verifier.ExamineMagicNumber< 2 >( m_data.v8.order_2_byte );
        verifier.ExamineMagicNumber< 4 >( m_data.v8.order_4_byte );
        verifier.ExamineMagicNumber< 8 >( m_data.v8.order_8_byte );
    }

    if ( IStream.good( ) == false )
    {
        throw std::runtime_error( "Failed to read FrHeader from the stream" );
    }
}

//=======================================================================
// FrObject class definition
//=======================================================================

FrObject::FrObject( FrameStream& Stream )
{
    //---------------------------------------------------------------------
    // Record where this object started
    //---------------------------------------------------------------------
    m_start_pos = IStream.Position( );
    //---------------------------------------------------------------------
    // Read the common elements
    //---------------------------------------------------------------------
    const Description& ce_desc =
        Stream.GetDescription( CLASS_ID_COMMON_ELEMENTS );

    ce_desc.Read( Stream, m_common_elements );
    if ( !IStream.good( ) )
    {
        throw std::range_error( "EOF encountered" );
    }
    m_struct_id = ce_desc.Value< class_id_type >( m_common_elements,
                                                  FStream.ClassIdIndex( ) );
    m_length = ce_desc.Value< length_type >( m_common_elements,
                                             FStream.LengthIndex( ) );
    //---------------------------------------------------------------------
    // Read the remainder of the frame object
    //---------------------------------------------------------------------
    Stream.AppendDescription( *this, true );
    try
    {
        const Description& obj_desc = Stream.GetDescription( StructId( ) );

        length_type end = ( IStream.Position( ) +
                            ( Length( ) - FStream.CommonElementSize( ) ) );

        bool complete = obj_desc.Read( Stream, m_data );
        if ( !complete )
        {
            // If the object has not been defined, then advance to the start
            //   of the next object
            IStream.AdvanceABS( end );
        }
        if ( ( StructId( ) == CLASS_ID_FrSH ) ||
             ( StructId( ) == CLASS_ID_FrSE ) )
        {
            if ( StructId( ) == CLASS_ID_FrSH )
            {
                //---------------------------------------------------------------
                // Get the class id
                //---------------------------------------------------------------
                const Description& d = Stream.GetDescription( CLASS_ID_FrSH );

                std::istringstream s(
                    Stream.String( d.Class( 1 ), m_data[ 1 ] ) );

                s >> m_cache.FrSH.class_id;
                //---------------------------------------------------------------
                // Get teh class name
                //---------------------------------------------------------------
                m_cache.FrSH.class_name =
                    static_cast< STRING* >( m_data[ 0 ] )->Value( );
            }
            FStream.AppendDescription( *this, false );
        }
    }
    catch ( ... )
    {
        // If the object has not been defined, then advance to the start
        //   of the next object
        INT_8U length = ce_desc.Value< INT_8U >( m_common_elements,
                                                 FStream.LengthIndex( ) );
        IStream.Advance( length - FStream.CommonElementSize( ) );
    }
}

void
FrObject::AppendSE( Description& D ) const
{
    D.AddSE( FStream.String( "STRING", m_data[ 0 ] ),
             FStream.String( "STRING", m_data[ 1 ] ),
             FStream.String( "STRING", m_data[ 2 ] ) );
}

FrObject::class_id_type
FrObject::FrSHClassId( ) const
{
    class_id_type retval = 0;

    if ( StructId( ) == CLASS_ID_FrSH )
    {
        retval = m_cache.FrSH.class_id;
    }

    return retval;
}

std::string
FrObject::FrSHClassName( ) const
{
    std::string retval;

    if ( StructId( ) == CLASS_ID_FrSH )
    {
        retval = m_cache.FrSH.class_name;
    }

    return retval;
}

void
FrObject::Dump( const CommandLine& Options ) const
{
    std::string        classname( "Unknown" );
    const Description& ce_desc =
        FStream.GetDescription( CLASS_ID_COMMON_ELEMENTS );

    const Description* obj_desc = (const Description*)NULL;
    try
    {
        INT_4U             cid = ce_desc.Value< INT_4U >( m_common_elements,
                                              FStream.ClassIdIndex( ) );
        const Description& d = FStream.GetDescription( cid );

        obj_desc = &d;
        classname = d.ClassName( );
    }
    catch ( ... )
    {
    }

    Log << "Position: " << m_start_pos;
    ce_desc.DumpCommonElements( m_common_elements, classname );
    Log << "" << std::endl;
    if ( obj_desc && ( Options.Silent( ) == false ) )
    {
        obj_desc->Dump( m_data, Options );
    }
}

//=======================================================================
// InputStream class definition
//=======================================================================

InputStream::InputStream( )
{
}

std::istream&
InputStream::Advance( position_type Position )
{
    m_stream->seekg( Position, std::ios_base::cur );
    m_position += Position;
    return *m_stream;
}

std::istream&
InputStream::AdvanceABS( position_type Position )
{
    m_stream->seekg( Position, std::ios_base::beg );
    m_position = Position;
    return *m_stream;
}

std::istream&
InputStream::Read( void* Buffer, position_type Size )
{
    m_stream->read( (char*)Buffer, Size );

    m_position += m_stream->gcount( );

    return *m_stream;
}

std::istream&
InputStream::operator( )( std::istream& Stream )
{
    m_stream = &Stream;
    m_position = position_type( 0 );
    return *m_stream;
}

//=======================================================================
// FrameStream class definition
//=======================================================================

void
FrameStream::AddDescription( class_id_type Id, const Description& Description )
{
    m_structs[ Id ] = Description;
}

void
FrameStream::AppendDescription( const FrObject& Object, bool Pre )
{
    static class_id_type                  id = 0;
    static std::unique_ptr< Description > d( (Description*)NULL );

    if ( Pre )
    {
        if ( ( Object.StructId( ) > 2 ) && ( d.get( ) ) )
        {
            AddDescription( id, *d );
            id = 0;
            d.reset( (Description*)NULL );
        }
    }
    else
    {
        if ( Object.StructId( ) == 1 )
        {
            if ( d.get( ) != (Description*)NULL )
            {
                AddDescription( id, *d );
            }
            d.reset( new Description );
            id = Object.FrSHClassId( );
            d->ClassName( Object.FrSHClassName( ) );
        }
        else if ( Object.StructId( ) == 2 )
        {
            if ( d.get( ) != (Description*)NULL )
            {
                Object.AppendSE( *d );
            }
        }
    }
}

const Description&
FrameStream::GetDescription( class_id_type Id ) const
{
    structs_type::const_iterator cur = m_structs.find( Id );

    if ( cur == m_structs.end( ) )
    {
        throw std::range_error( "Object description does not exist" );
    }
    return cur->second;
}

void*
FrameStream::Read( const std::string& Class, const SymbolTable& Symbols )
{
    void* retval = (void*)NULL;

#define READER( T )                                                            \
    ( Class == #T )                                                            \
    {                                                                          \
        retval = new T;                                                        \
        IStream.Read( retval, sizeof( T ) );                                   \
        if ( m_byte_swapping )                                                 \
        {                                                                      \
            reverse< sizeof( T ) >( retval, 1 );                               \
        }                                                                      \
    }

    if
        READER( CHAR )
    else if
        READER( CHAR_U )
    else if
        READER( INT_2U )
    else if
        READER( INT_2S )
    else if
        READER( INT_4U )
    else if
        READER( INT_4S )
    else if
        READER( INT_8U )
    else if
        READER( INT_8S )
    else if
        READER( REAL_4 )
    else if
        READER( REAL_8 )
    else if ( STRING::Match( Class ) )
    {
        retval = new STRING( *this );
    }
    else if ( PTR_STRUCT::Match( Class ) )
    {
        retval = new PTR_STRUCT( *this );
    }
    else if ( ARRAY_TYPE::Match( Class ) )
    {
        retval = new ARRAY_TYPE( *this, Class, Symbols );
    }
    else
    {
        throw std::runtime_error( "Unable to read class" );
    }

#undef READER

    return retval;
}

std::string
FrameStream::String( const std::string& Class, void* Data ) const
{
    std::ostringstream retval( "" );

#define CONVERT( T )                                                           \
    ( Class.compare( #T ) == 0 )                                               \
    {                                                                          \
        T* d = static_cast< T* >( Data );                                      \
        retval << ( *d );                                                      \
    }

    if ( Data == (void*)NULL )
    {
        retval << "NULL";
    }
    else if
        CONVERT( INT_2U )
    else if
        CONVERT( INT_2S )
    else if
        CONVERT( INT_4U )
    else if
        CONVERT( INT_4S )
    else if
        CONVERT( INT_8U )
    else if
        CONVERT( INT_8S )
    else if
        CONVERT( REAL_4 )
    else if
        CONVERT( REAL_8 )
    else if ( STRING::Match( Class ) )
    {
        retval << static_cast< STRING* >( Data )->Value( );
    }
    else if ( PTR_STRUCT::Match( Class ) )
    {
        retval << static_cast< PTR_STRUCT* >( Data )->Value( );
    }
    else if ( ARRAY_TYPE::Match( Class ) )
    {
        retval << static_cast< ARRAY_TYPE* >( Data )->Value( );
    }
    else if ( Class.compare( "CHAR" ) == 0 )
    {
        CHAR* d = static_cast< CHAR* >( Data );
        retval << INT_2S( *d );
    }
    else if ( Class.compare( "CHAR_U" ) == 0 )
    {
        CHAR_U* d = static_cast< CHAR_U* >( Data );
        retval << INT_2U( *d );
    }
    else
    {
        retval << "Unknown class: " << Class;
    }
#undef CONVERT

    return retval.str( );
}

template <>
INT_4U
FrameStream::Value( const std::string& DataType, void* Data )
{
    INT_4U retval;

#define CONVERT( T )                                                           \
    ( DataType == #T )                                                         \
    {                                                                          \
        retval = *( static_cast< T* >( Data ) );                               \
    }

    if
        CONVERT( CHAR )
    else if
        CONVERT( CHAR_U )
    else if
        CONVERT( INT_2S )
    else if
        CONVERT( INT_2U )
    else if
        CONVERT( INT_4S )
    else if
        CONVERT( INT_4U )
    else
    {
        throw std::runtime_error( "Unable to convert value to INT_4U" );
    }
    return retval;
}

template <>
INT_8U
FrameStream::Value( const std::string& DataType, void* Data )
{
    INT_8U retval;

#define CONVERT( T )                                                           \
    ( DataType == #T )                                                         \
    {                                                                          \
        retval = *( static_cast< T* >( Data ) );                               \
    }

    if
        CONVERT( CHAR )
    else if
        CONVERT( CHAR_U )
    else if
        CONVERT( INT_2S )
    else if
        CONVERT( INT_2U )
    else if
        CONVERT( INT_4S )
    else if
        CONVERT( INT_4U )
    else if
        CONVERT( INT_8S )
    else if
        CONVERT( INT_8U )
    else
    {
        throw std::runtime_error( "Unable to convert value to INT_4U" );
    }
    return retval;
}

void
FrameStream::operator( )( LDASTools::AL::ifstream& File )
{
    //---------------------------------------------------------------------
    // Open the file
    //---------------------------------------------------------------------
    IStream( File );
    //---------------------------------------------------------------------
    // Read the header
    //---------------------------------------------------------------------
    m_header.Read( );
    m_header.Dump( );
    m_byte_swapping = m_header.ByteSwapping( );
    //---------------------------------------------------------------------
    // Seed the Descriptions for FrSH and FrSE
    //---------------------------------------------------------------------
    {
        Description ce;
        Description sh;
        Description se;

        if ( m_header.Version( ) < 8 )
        {
            sh.ClassName( "FrSH" );
            sh.AddSE( "name", "STRING", "" );
            sh.AddSE( "class", "INT_2U", "" );
            sh.AddSE( "comment", "STRING", "" );

            se.ClassName( "FrSE" );
            se.AddSE( "name", "STRING", "" );
            se.AddSE( "class", "STRING", "" );
            se.AddSE( "comment", "STRING", "" );
        }
        else
        {
            sh.ClassName( "FrSH" );
            sh.AddSE( "name", "STRING", "" );
            sh.AddSE( "class", "INT_2U", "" );
            sh.AddSE( "comment", "STRING", "" );
            sh.AddSE( "chkSum", "INT_4U", "" );

            se.ClassName( "FrSE" );
            se.AddSE( "name", "STRING", "" );
            se.AddSE( "class", "STRING", "" );
            se.AddSE( "comment", "STRING", "" );
            se.AddSE( "chkSum", "INT_4U", "" );
        }
        //-------------------------------------------------------------------
        // Establish the look of the common elements
        //-------------------------------------------------------------------
        ce.ClassName( "CommonElements" );
        if ( m_header.Version( ) >= 8 )
        {
            ce.AddSE( "length", "INT_8U", "" );
            ce.AddSE( "chkType", "CHAR_U", "" );
            ce.AddSE( "class", "CHAR_U", "" );
            ce.AddSE( "instance", "INT_4U", "" );

            m_length_index = 0;
            m_class_id_index = 2;
            m_common_element_size = sizeof( INT_8U ) + sizeof( CHAR_U ) +
                sizeof( CHAR_U ) + sizeof( INT_4U );
        }
        else if ( m_header.Version( ) >= 5 )
        {
            ce.AddSE( "length", "INT_8U", "" );
            ce.AddSE( "class", "INT_2U", "" );
            ce.AddSE( "instance", "INT_4U", "" );

            m_length_index = 0;
            m_class_id_index = 1;
            m_common_element_size = sizeof( INT_8U ) + sizeof( CHAR_U ) +
                sizeof( CHAR_U ) + sizeof( INT_4U );
        }
        else if ( m_header.Version( ) >= 3 )
        {
            ce.AddSE( "length", "INT_4U", "" );
            ce.AddSE( "class", "INT_2U", "" );
            ce.AddSE( "instance", "INT_2U", "" );

            m_length_index = 0;
            m_class_id_index = 1;
            m_common_element_size =
                sizeof( INT_4U ) + sizeof( INT_2U ) + sizeof( INT_2U );
        }

        AddDescription( 0, ce );
        AddDescription( 1, sh );
        AddDescription( 2, se );
    }
}

//=======================================================================
// Output class definition
//=======================================================================

Output::Output( std::ostream& Stream ) : m_stream( Stream )
{
}

template < class T >
std::ostream&
Output::operator<<( const T& Data )
{
    m_stream << Data;
    return m_stream;
}

//=======================================================================
// PTR_STRUCT - Definition
//=======================================================================
PTR_STRUCT::PTR_STRUCT( FrameStream& Stream )
{
    if ( Stream.Version( ) >= 6 )
    {
        INT_2U c;
        INT_4U i;

        IStream.Read( &c, sizeof( c ) );
        IStream.Read( &i, sizeof( i ) );

        if ( Stream.ByteSwapping( ) )
        {
            reverse< sizeof( c ) >( &c, 1 );
            reverse< sizeof( i ) >( &i, 1 );
        }

        m_data_class = c;
        m_data_instance = i;
    }
    else if ( Stream.Version( ) >= 3 )
    {
        INT_2U c;
        INT_2U i;

        IStream.Read( &c, sizeof( c ) );
        IStream.Read( &i, sizeof( i ) );

        if ( Stream.ByteSwapping( ) )
        {
            reverse< sizeof( c ) >( &c, 1 );
            reverse< sizeof( i ) >( &i, 1 );
        }

        m_data_class = c;
        m_data_instance = i;
    }
    else
    {
        throw std::runtime_error( "Unable to read PTR_STRUCT" );
    }
}
//=======================================================================
// ARRAY_TYPE
//=======================================================================
INT_4U ARRAY_TYPE::m_DumpNElements = 0;

Regex ARRAY_TYPE::pattern( "(CHAR|"
                           "CHAR_U|"
                           "INT_2S|"
                           "INT_2U|"
                           "INT_4S|"
                           "INT_4U|"
                           "INT_8S|"
                           "INT_8U|"
                           "REAL_4|"
                           "REAL_8|"
                           "STRING|"
                           "PTR_STRUCT|"
                           "COMPLEX_8|"
                           "COMPLEX_16)"
                           "[[:space:]]*\\[[[:space:]]*"
                           "([^[:space:]]+)"
                           "[[:space:]]*\\]",
                           REG_EXTENDED );

Regex ARRAY_TYPE::m_multidimensional_pattern(
    "^([[:alpha:][:digit:]_]+)\\]*\\[*(.*)$", REG_EXTENDED );

template < typename TYPE >
void
ARRAY_TYPE::dump( std::ostringstream& Stream ) const
{
    const TYPE* d = reinterpret_cast< const TYPE* >( m_data.get( ) );
    if ( d )
    {
        const INT_4U end =
            ( ( m_DumpNElements == 0 ) ? m_size
                                       : std::min( m_size, m_DumpNElements ) );

        for ( INT_4U x = 0; x < end; ++x, ++d )
        {
            if ( x )
            {
                Stream << ", ";
            }
            Stream << *d;
        }
    }
}

template <>
void
ARRAY_TYPE::dump< STRING >( std::ostringstream& Stream ) const
{
    const STRING* d = reinterpret_cast< const STRING* >( m_data.get( ) );
    const INT_4U  end =
        ( ( m_DumpNElements == 0 ) ? m_size
                                   : std::min( m_size, m_DumpNElements ) );

    for ( INT_4U x = 0; x < end; ++x, ++d )
    {
        if ( x )
        {
            Stream << ", ";
        }
        Stream << d->Value( );
    }
}

template <>
void
ARRAY_TYPE::dump< CHAR >( std::ostringstream& Stream ) const
{
    const CHAR*  d = reinterpret_cast< const CHAR* >( m_data.get( ) );
    const INT_4U end =
        ( ( m_DumpNElements == 0 ) ? m_size
                                   : std::min( m_size, m_DumpNElements ) );

    for ( INT_4U x = 0; x < end; ++x, ++d )
    {
        if ( x )
        {
            Stream << ", ";
        }
        Stream << ( ( isprint( *d ) ) ? *d : INT_2S( *d ) );
    }
}

template <>
void
ARRAY_TYPE::dump< CHAR_U >( std::ostringstream& Stream ) const
{
    const CHAR_U* d = static_cast< const CHAR_U* >( m_data.get( ) );
    const INT_4U  end =
        ( ( m_DumpNElements == 0 ) ? m_size
                                   : std::min( m_size, m_DumpNElements ) );

    for ( INT_4U x = 0; x < end; ++x, ++d )
    {
        if ( x )
        {
            Stream << ", ";
        }
        Stream << ( ( isprint( *d ) ) ? *d : INT_2U( *d ) );
    }
}

template < typename TYPE >
void
ARRAY_TYPE::read( FrameStream& Stream, INT_4U Size )
{
    // boost::shared_array< TYPE >	data( new TYPE[ Size ] );
    data_type data( new data_type::element_type[ Size * sizeof( TYPE ) ] );

    IStream.Read( data.get( ), sizeof( TYPE ) * Size );
    if ( Stream.ByteSwapping( ) )
    {
        reverse< sizeof( TYPE ) >( data.get( ), Size );
    }

    m_data = data;
}

#if WORKING
template <>
void
ARRAY_TYPE::read< STRING >( FrameStream& Stream, INT_4U Size )
{
    boost::shared_array< STRING > data( new STRING[ Size ] );
    STRING*                       data_pos( data.get( ) );

    for ( INT_4U x = 0; x != Size; ++x, ++data_pos )
    {
        data_pos->Read( Stream );
    }
    m_data = data;
}
#endif /* WORKING */

template <>
void
ARRAY_TYPE::read< COMPLEX_8 >( FrameStream& Stream, INT_4U Size )
{
    read< REAL_4 >( Stream, Size * 2 );
}

template <>
void
ARRAY_TYPE::read< COMPLEX_16 >( FrameStream& Stream, INT_4U Size )
{
    read< REAL_8 >( Stream, Size * 2 );
}

ARRAY_TYPE::ARRAY_TYPE( FrameStream&       Stream,
                        const std::string& Class,
                        const SymbolTable& Symbols )
    : m_data_type( DATA_TYPE_UNKNOWN ), m_size( 0 )
{
    RegexMatch m( 3 );

    if ( m.match( pattern, Class.c_str( ) ) )
    {
        const std::string t( m.getSubString( 1 ) );
        std::string       size( m.getSubString( 2 ) );
        INT_4U            value = 0;
        RegexMatch        md( 3 );

        if ( md.match( m_multidimensional_pattern, size.c_str( ) ) )
        {
            value = 1;
            do
            {
                std::string symbol( md.getSubString( 1 ) );
                size = md.getSubString( 2 );
                INT_4U s( Symbols.LookupInteger( symbol ) );
                if ( s == ~INT_4U( 0 ) )
                {
                    s = 0;
                }
                value *= s;
            } while ( md.match( m_multidimensional_pattern, size.c_str( ) ) );
        }

        if ( value )
        {

#undef READ
#define READ( TYPE )                                                           \
    ( t.compare( #TYPE ) == 0 )                                                \
    {                                                                          \
        read< TYPE >( Stream, value );                                         \
        m_data_type = DATA_TYPE_##TYPE;                                        \
        m_size = value;                                                        \
    }

            /* if READ( STRING )
               else */
            if
                READ( CHAR )
            else if
                READ( CHAR_U )
            else if
                READ( INT_2S )
            else if
                READ( INT_2U )
            else if
                READ( INT_4S )
            else if
                READ( INT_4U )
            else if
                READ( INT_8S )
            else if
                READ( INT_8U )
            else if
                READ( REAL_4 )
            else if
                READ( REAL_8 )
            else if
                READ( COMPLEX_8 )
            else if
                READ( COMPLEX_16 )
            else
            {
                INT_4U cs = ClassSize( t );
                if ( cs )
                {
                    IStream.Advance( value * cs );
                }
                else
                {
                    throw std::runtime_error( "Unable to read array_type" );
                }
            }
#undef READ
        }
    }
}

ARRAY_TYPE::~ARRAY_TYPE( )
{
#if 0
  if ( m_data )
  {

#undef DELETE
#define DELETE( TYPE )                                                         \
    case DATA_TYPE_##TYPE:                                                     \
        delete[] static_cast< TYPE* >( m_data );                               \
        break

    //-------------------------------------------------------------------
    // Check the datatype assoicated with the array and deallocate
    // the memory accordingly.
    //
    // NOTE: COMPLEX_8 and COMPLEX_16 were allocated as REAL_4 and
    //    REAL_8 arrays respectively with twice the number of elements
    //    as specified by size. As a result, they can use the
    //    appropriate REAL_ deallocaters.
    //-------------------------------------------------------------------  
    switch( m_data_type )
    {
      DELETE( CHAR );
      DELETE( CHAR_U );
      DELETE( INT_2S );
      DELETE( INT_2U );
      DELETE( INT_4S );
      DELETE( INT_4U );
      DELETE( INT_8S );
    case DATA_TYPE_COMPLEX_8:
      DELETE( REAL_4 );
    case DATA_TYPE_COMPLEX_16:
      DELETE( REAL_8 );
      DELETE( STRING );
      DELETE( PTR_STRUCT );
    default:
      delete[] static_cast< char* >( m_data );
    }

#undef DELETE
  }
#endif /* 0 */
}

std::string
ARRAY_TYPE::Value( ) const
{
    std::ostringstream retval;

#undef DUMP
#define DUMP( TYPE )                                                           \
    ( DATA_TYPE_##TYPE == m_data_type )                                        \
    {                                                                          \
        dump< TYPE >( retval );                                                \
    }

    if ( m_data )
    {
        if
            DUMP( STRING )
        else if
            DUMP( CHAR )
        else if
            DUMP( CHAR_U )
        else if
            DUMP( INT_2S )
        else if
            DUMP( INT_2U )
        else if
            DUMP( INT_4S )
        else if
            DUMP( INT_4U )
        else if
            DUMP( INT_8S )
        else if
            DUMP( INT_8U )
        else if
            DUMP( REAL_4 )
        else if
            DUMP( REAL_8 )
    }

#undef DUMP

    if ( ( ( m_size > m_DumpNElements ) && ( m_DumpNElements > 0 ) ) ||
         ( retval.str( ).length( ) <= 0 ) )
    {
        if ( retval.str( ).length( ) > 0 )
        {
            retval << ", ";
        }
        retval << "...";
    }
    return retval.str( );
}

//=======================================================================
// STRING - Definition
//=======================================================================
STRING::STRING( )
{
}

STRING::STRING( FrameStream& Stream )
{
    Read( Stream );
}

static INT_8U
ClassSize( const std::string& Class )
{
    INT_8U retval = 0;

    if ( Class == "CHAR" )
    {
        retval = sizeof( CHAR );
    }
    else if ( Class == "CHAR_U" )
    {
        retval = sizeof( CHAR_U );
    }
    else if ( Class == "INT_2S" )
    {
        retval = sizeof( INT_2S );
    }
    else if ( Class == "INT_2U" )
    {
        retval = sizeof( INT_2U );
    }
    else if ( Class == "INT_4S" )
    {
        retval = sizeof( INT_4S );
    }
    else if ( Class == "INT_4U" )
    {
        retval = sizeof( INT_4U );
    }
    else if ( Class == "INT_8S" )
    {
        retval = sizeof( INT_8S );
    }
    else if ( Class == "INT_8U" )
    {
        retval = sizeof( INT_8U );
    }
    else if ( Class == "REAL_4" )
    {
        retval = sizeof( REAL_4 );
    }
    else if ( Class == "REAL_8" )
    {
        retval = sizeof( REAL_8 );
    }
    return retval;
}

void
STRING::Read( FrameStream& Stream )
{
    if ( Stream.Version( ) >= 3 )
    {
        INT_2U length;
        IStream.Read( &length, sizeof( length ) );
        if ( Stream.ByteSwapping( ) )
        {
            reverse< sizeof( length ) >( &length, 1 );
        }

        m_length = length;

        if ( m_length > 0 )
        {
            m_value.reset( new CHAR[ m_length ] );
            IStream.Read( m_value.get( ), m_length );
            m_value[ m_length - 1 ] = '\0';
        }
    }
    else
    {
        throw std::runtime_error( "STRING: Unsupported frame specification" );
    }
}
