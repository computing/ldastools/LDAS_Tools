//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//
#ifndef RAMP_HH
#define RAMP_HH

#include <algorithm>

#include "framecpp/Dimension.hh"
#include "framecpp/FrVect.hh"

namespace testing
{
    namespace Ramp
    {
        enum wave_form_type
        {
            NO_WAVE_FORM,
            SAW_TOOTH_FALLING,
            SAW_TOOTH_PEAKS,
            SAW_TOOTH_RISING,
            SQUARE_WAVE,
            MAX_WAVE_FORM
        };

        //---------------------------------------------------------------------
        // Saw Tooth Falling function
        //---------------------------------------------------------------------
        template < class T >
        class GenSawToothFalling
        {
        public:
            GenSawToothFalling( const T Start, const T End, const T Inc )
                : m_start( Start ), m_end( End ), m_inc( Inc ), m_cur( Start )
            {
            }

            inline T
            operator( )( )
            {
                T retval = m_cur;

                if ( m_cur == m_end )
                {
                    m_cur = m_start;
                }
                else
                {
                    m_cur -= m_inc;
                }
                return retval;
            }

        private:
            const T m_start;
            const T m_end;
            const T m_inc;
            T       m_cur;
        };

        //---------------------------------------------------------------------
        // Saw Tooth Peaks function
        //---------------------------------------------------------------------
        template < class T >
        class GenSawToothPeaks
        {
        public:
            GenSawToothPeaks( const T Min, const T Max, const T Inc )
                : m_min( Min ), m_max( Max ), m_inc( Inc ), m_cur( Min ),
                  m_rising( true )
            {
            }

            inline T
            operator( )( )
            {
                T retval = m_cur;

                if ( m_rising )
                {
                    if ( m_cur == m_max )
                    {
                        m_rising = false;
                        m_cur -= m_inc;
                    }
                    else
                    {
                        m_cur += m_inc;
                    }
                }
                else
                {
                    if ( m_cur == m_min )
                    {
                        m_rising = true;
                        m_cur += m_inc;
                    }
                    else
                    {
                        m_cur -= m_inc;
                    }
                }
                return retval;
            }

        private:
            const T m_min;
            const T m_max;
            const T m_inc;
            T       m_cur;
            bool    m_rising;
        };

        //---------------------------------------------------------------------
        // Saw Tooth Rising function
        //---------------------------------------------------------------------
        template < class T >
        class GenSawToothRising
        {
        public:
            GenSawToothRising( const T Start, const T End, const T Inc )
                : m_start( Start ), m_end( End ), m_inc( Inc ), m_cur( Start )
            {
            }

            inline T
            operator( )( )
            {
                T retval = m_cur;

                if ( m_cur == m_end )
                {
                    m_cur = m_start;
                }
                else
                {
                    m_cur += m_inc;
                }
                return retval;
            }

        private:
            const T m_start;
            const T m_end;
            const T m_inc;
            T       m_cur;
        };

        //---------------------------------------------------------------------
        // Square wave function generator
        //---------------------------------------------------------------------
        template < class T >
        class GenSquareWave
        {
        public:
            GenSquareWave( const T Start,
                           const T End,
                           const T Inc,
                           INT_4U  Samples )
                : m_start( Start ), m_end( End ), m_inc( Inc ),
                  m_samples( Samples ), m_stop( Samples / 2 ), m_cur( 0 )
            {
            }

            T
            operator( )( )
            {
                T retval;

                retval = ( m_cur < m_stop ) ? m_start : m_end;
                m_cur++;
                if ( m_cur == m_samples )
                {
                    m_cur = 0;
                }
                return retval;
            }

        private:
            const T      m_start;
            const T      m_end;
            const T      m_inc;
            const INT_4U m_samples;
            const INT_4U m_stop;
            INT_4U       m_cur;
        };

    } // namespace Ramp
} // namespace Testing
#if 0
template < class T >
boost::shared_ptr< FrameCPP::FrVect >
ramp_data( const std::string& Name,
           INT_4U             Size,
           T                  Offset = 0,
           T                  Denominator = 1 )
{
  using namespace FrameCPP;

    T*        data( new T[ Size ] );
    FrameCPP::Dimension dim( Size );

    for ( INT_4U x = 0; x < Size; x++ )
    {
        data[ x ] = ( x / Denominator ) + Offset;
    }

    auto retval = boost::make_shared< FrameCPP::FrVect >(
        Name,
        FrameCPP::FrVect::RAW, /* Compress */
        FrameCPP::FrVect::FR_VECT_8R, /* Type */
        1,
        aux_dims,
        DATA_SET_SIZE, /* NData */
        DATA_SET_SIZE * sizeof( REAL_8 ), /* NBytes */
        data8,
        "" );
    return ( retval );
}
#endif /* 0 */
#endif /* RAMP_HH */
