//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__FR_STRUCT_4_TCC
#define FRAME_CPP__TEST__FR_STRUCT_4_TCC

#include "framecpp/Version4/FrameH.hh"
#include "framecpp/Version4/FrAdcData.hh"
#include "framecpp/Version4/FrDetector.hh"
#include "framecpp/Version4/FrHistory.hh"
#include "framecpp/Version4/FrMsg.hh"
#include "framecpp/Version4/FrProcData.hh"
#include "framecpp/Version4/FrRawData.hh"
#include "framecpp/Version4/FrSerData.hh"
#include "framecpp/Version4/FrSimData.hh"
#include "framecpp/Version4/FrSimEvent.hh"
#include "framecpp/Version4/FrStatData.hh"
#include "framecpp/Version4/FrSummary.hh"
#include "framecpp/Version4/FrTable.hh"
#include "framecpp/Version4/FrTrigData.hh"

#define TEMPLATE_SPEC 4
#define NAMESPACE FrameCPP::Version_4
#define PREVIOUS_TEMPLATE_SPEC 3
#define PREVIOUS_NAMESPACE FrameCPP::Version_3
#define USING( ) using namespace NAMESPACE

#undef CHECK_NUMBER_V1
#define CHECK_NUMBER_V1( P, C, A, N )                                          \
    BOOST_TEST_MESSAGE( Leader << "field: " #N );                              \
    BOOST_CHECK( ( INT_4U )( C->A( ) ) == P->A( ) )

//=======================================================================
// FrTable
//=======================================================================
template <>
mk_frame_object_ret_type
mk_frame_object< TEMPLATE_SPEC >( FrameObjectTypes Type )
{
    USING( );

    using FrameCPP::Common::FrameSpec;

    mk_frame_object_ret_type retval;

    switch ( Type )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        retval.reset(
            new FrameH( "frame_h", 1, 8, GPSTime( 10, 20 ), 1, 3600, 3.0, 2 ) );
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        retval.reset( new FrAdcData( "fr_adc_data",
                                     3,
                                     2,
                                     8,
                                     1024.,
                                     2.0,
                                     1.0,
                                     "meters",
                                     30.,
                                     10,
                                     20,
                                     4 ) );

        reinterpret_cast< FrAdcData* >( retval.get( ) )
            ->AppendComment( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
        retval.reset( new FrDetector( "fr_detector_name",
                                      10,
                                      20,
                                      40.2,
                                      15,
                                      18,
                                      35.2,
                                      200.3,
                                      300.4,
                                      400.5 ) );
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
    {
        std::unique_ptr< FrHistory > fr_history(
            new FrHistory( "fr_history_name", 10, "fr_hsitory_comment" ) );
        retval.reset( fr_history.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_MSG:
    {
        std::unique_ptr< FrMsg > fr_msg( new FrMsg( "alarm", "message", 10 ) );
        retval.reset( fr_msg.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        retval.reset( new FrProcData( "fr_proc_data",
                                      "fr_proc_data_comment",
                                      1024.,
                                      GPSTime( 10, 100000000 ),
                                      2048. ) );

        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        retval.reset( new FrRawData( "fr_raw_data" ) );
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        retval.reset(
            new FrSerData( "fr_ser_data", GPSTime( 10, 20 ), 1024. ) );

        reinterpret_cast< FrSerData* >( retval.get( ) )->SetData( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        retval.reset(
            new FrSimData( "fr_sim_data", "fr_sim_data_comment", 1024. ) );
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        retval.reset( new FrSimEvent( "fr_sim_event",
                                      "fr_sim_event_comment",
                                      "fr_sim_event_inputs",
                                      GPSTime( 10, 20000000 ),
                                      1024.0,
                                      2048.0,
                                      16.0 ) );

        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        retval.reset( new FrStatData( "fr_stat_data",
                                      "fr_stat_data_comment",
                                      "fr_stat_data_representation",
                                      1,
                                      2,
                                      4 ) );
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        retval.reset( new FrSummary(
            "fr_summary", "fr_summary_comment", "fr_summary_test" ) );

        break;
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        std::unique_ptr< FrTable > fr_table( new FrTable( "testTable", 0 ) );
        fr_table->AppendComment( "hello world" );
        retval.reset( fr_table.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_TRIG_DATA:
        //-------------------------------------------------------------------
        // FrTrigData
        //-------------------------------------------------------------------
        retval.reset( new FrTrigData( "fr_trig_data",
                                      "fr_trig_data_comment",
                                      "fr_trig_data_inputs",
                                      GPSTime( 10, 20000000 ),
                                      1024.0,
                                      2048.0,
                                      1,
                                      16.0,
                                      32.0,
                                      "fr_trig_data_statistics" ) );

        break;
    case FrameSpec::Info::FSI_FR_VECT:
    {
        typedef REAL_8 vect_data_type;

        static const int            SAMPLES = 4;
        static const vect_data_type START = 16.0;
        static const vect_data_type INC = 0.5;

        Dimension      dim( SAMPLES );
        vect_data_type data[ SAMPLES ];

        vect_data_type cur_val = START;

        for ( int cur = 0; cur != SAMPLES; ++cur )
        {
            data[ cur ] = cur_val;
            cur_val += INC;
        }

        retval.reset( new FrVect( "fr_vect", 1, &dim, data, "fr_vect_unitY" ) );
    }
    break;
    default:
    {
        std::ostringstream msg;

        msg << "mk_frame_obj<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << Type;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
    return retval;
}

//=======================================================================

template <>
void
verify_downconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                     const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }
    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        //-------------------------------------------------------------------
        // FrameH
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrameH );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, (INT_4U)current, GetRun, run );
                CHECK_NUMBER( previous, current, GetFrame, frame );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetULeapS, ULeapS );
                CHECK_NUMBER( previous, current, GetLocalTime, localTime );
                CHECK_NUMBER( previous, current, GetDt, dt );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        //-------------------------------------------------------------------
        // FrAdcData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrAdcData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER_4(
                    previous, GetCrate, current, GetChannelGroup, crate );
                CHECK_NUMBER_4(
                    previous, GetChannel, current, GetChannelNumber, channel );
                CHECK_NUMBER( previous, current, GetNBits, nBits );
                CHECK_NUMBER( previous, current, GetBias, bias );
                CHECK_NUMBER( previous, current, GetSlope, slope );
                CHECK_STRING( previous, current, GetUnits, units );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_NUMBER_V1(
                    previous, current, GetTimeOffsetS, timeOffsetS );
                CHECK_NUMBER( previous, current, GetTimeOffsetN, timeOffsetN );
                CHECK_NUMBER( previous, current, GetFShift, fShift );
                CHECK_NUMBER_4(
                    previous, GetOverRange, current, GetDataValid, overRange );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
        //-------------------------------------------------------------------
        // FrDetector
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrDetector );

            if ( previous && current )
            {
                BOOST_TEST_MESSAGE( Leader << "field: name" );
                BOOST_CHECK(
                    0 == current->GetName( ).compare( previous->GetName( ) ) );
                BOOST_TEST_MESSAGE( Leader << "field: longitudeD" );
                BOOST_CHECK( current->GetLongitudeD( ) ==
                             previous->GetLongitudeD( ) );
                BOOST_TEST_MESSAGE( Leader << "field: longitudeM" );
                BOOST_CHECK( current->GetLongitudeM( ) ==
                             previous->GetLongitudeM( ) );
                BOOST_TEST_MESSAGE( Leader << "field: longitudeS" );
                BOOST_CHECK( current->GetLongitudeS( ) ==
                             previous->GetLongitudeS( ) );
                BOOST_TEST_MESSAGE( Leader << "field: longitudeD" );
                BOOST_CHECK( current->GetLatitudeD( ) ==
                             previous->GetLatitudeD( ) );
                BOOST_TEST_MESSAGE( Leader << "field: longitudeM" );
                BOOST_CHECK( current->GetLatitudeM( ) ==
                             previous->GetLatitudeM( ) );
                BOOST_TEST_MESSAGE( Leader << "field: longitudeS" );
                BOOST_CHECK( current->GetLatitudeS( ) ==
                             previous->GetLatitudeS( ) );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
        //-------------------------------------------------------------------
        // FrHistory
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_SAME( FrHistory );
        }
        break;
    case FrameSpec::Info::FSI_FR_MSG:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_SAME( FrMsg );
        }
        break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrProcData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, current, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, current, GetFShift, fShift );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        //-------------------------------------------------------------------
        // FrRawData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrRawData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSerData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetTime, time );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_STRING( previous, current, GetData, data );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSimData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_NULL( FrSimEvent );
        }
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrStatData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetTimeStart, timeStart );
                CHECK_NUMBER( previous, current, GetTimeEnd, timeEnd );
                CHECK_NUMBER( previous, current, GetVersion, version );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSummary );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetName, comment );
                CHECK_STRING( previous, current, GetName, test );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_TABLE:
        //-------------------------------------------------------------------
        // FrTable
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_NULL( FrTable );
        }
        break;
    case FrameSpec::Info::FSI_FR_TRIG_DATA:
        //-------------------------------------------------------------------
        // FrTrigData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrTrigData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetInputs, inputs );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER_4(
                    previous, GetBvalue, current, GetTriggerStatus, bvalue );
                CHECK_NUMBER_4(
                    previous, GetRvalue, current, GetAmplitude, rvalue );
                CHECK_NUMBER( previous, current, GetProbability, probability );
                CHECK_STRING( previous, current, GetStatistics, statistics );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_VECT:
        //-------------------------------------------------------------------
        // FrVect
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrVect );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetCompress, compress );
                CHECK_NUMBER( previous, current, GetType, type );
                CHECK_NUMBER( previous, current, GetNData, nData );
                CHECK_NUMBER( previous, current, GetNBytes, nBytes );
                CHECK_NUMBER( previous, current, GetNDim, nDim );
                CHECK_NUMBER( previous, current, GetDim( 0 ).GetNx, nx );
                CHECK_NUMBER( previous, current, GetDim( 0 ).GetDx, dx );
                CHECK_STRING( previous, current, GetDim( 0 ).GetUnitX, unitX );
                CHECK_STRING( previous, current, GetUnitY, unitY );
            }
        }
        break;
    default:
    {
        std::ostringstream msg;

        msg << "verify_downconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
}

template <>
void
verify_upconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                   const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
    {
        PROMOTE_FROM_PREVIOUS( FrameH );

        if ( promoted && previous )
        {
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_NUMBER( (INT_4S)previous, promoted, GetRun, run );
            CHECK_NUMBER( previous, promoted, GetFrame, frame );
            CHECK_NUMBER( previous, promoted, GetGTime, GTime );
            CHECK_NUMBER( previous, promoted, GetULeapS, ULeapS );
            CHECK_NUMBER( previous, promoted, GetLocalTime, localTime );
            CHECK_NUMBER( previous, promoted, GetDt, dt );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrAdcData );

        if ( promoted && previous )
        {
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_NUMBER_4(
                previous, GetCrate, promoted, GetChannelGroup, channelGroup );
            CHECK_NUMBER_4( previous,
                            GetChannel,
                            promoted,
                            GetChannelNumber,
                            channelNumber );
            CHECK_NUMBER( previous, promoted, GetNBits, nBits );
            CHECK_NUMBER( previous, promoted, GetBias, bias );
            CHECK_NUMBER( previous, promoted, GetSlope, slope );
            CHECK_STRING( previous, promoted, GetUnits, units );
            CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
            CHECK_NUMBER_V1( previous, promoted, GetTimeOffsetS, timeOffsetS );
            CHECK_NUMBER( previous, promoted, GetTimeOffsetN, timeOffsetN );
            CHECK_NUMBER( previous, promoted, GetFShift, fShift );
            CHECK_NUMBER_4(
                previous, GetOverRange, promoted, GetDataValid, dataValid );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        PROMOTE_FROM_PREVIOUS( FrDetector );

        if ( promoted && previous )
        {
            BOOST_TEST_MESSAGE( Leader << "field: name" );
            BOOST_CHECK( promoted->GetName( ).compare( previous->GetName( ) ) ==
                         0 );
            BOOST_TEST_MESSAGE( Leader << "field: longitudeD" );
            BOOST_CHECK( promoted->GetLongitudeD( ) ==
                         previous->GetLongitudeD( ) );
            BOOST_TEST_MESSAGE( Leader << "field: longitudeM" );
            BOOST_CHECK( promoted->GetLongitudeM( ) ==
                         previous->GetLongitudeM( ) );
            BOOST_TEST_MESSAGE( Leader << "field: longitudeS" );
            BOOST_CHECK( promoted->GetLongitudeS( ) ==
                         previous->GetLongitudeS( ) );
            BOOST_TEST_MESSAGE( Leader << "field: longitudeD" );
            BOOST_CHECK( promoted->GetLatitudeD( ) ==
                         previous->GetLatitudeD( ) );
            BOOST_TEST_MESSAGE( Leader << "field: longitudeM" );
            BOOST_CHECK( promoted->GetLatitudeM( ) ==
                         previous->GetLatitudeM( ) );
            BOOST_TEST_MESSAGE( Leader << "field: longitudeS" );
            BOOST_CHECK( promoted->GetLatitudeS( ) ==
                         previous->GetLatitudeS( ) );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_HISTORY:
        //-------------------------------------------------------------------
        // FrHistory
        //-------------------------------------------------------------------
        PROMOTE_TO_SAME( FrHistory );
        break;
    case FrameSpec::Info::FSI_FR_MSG:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        PROMOTE_TO_SAME( FrMsg );
        break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrProcData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, promoted, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, promoted, GetFShift, fShift );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        //-------------------------------------------------------------------
        // FrRawData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrRawData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSerData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                BOOST_TEST_MESSAGE( " previous: " << previous->GetName( )
                                                  << " promoted: "
                                                  << promoted->GetName( ) );
                CHECK_NUMBER( previous, promoted, GetTime, time );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
                BOOST_TEST_MESSAGE(
                    " previous: " << previous->GetSampleRate( ) << " promoted: "
                                  << promoted->GetSampleRate( ) );
                CHECK_STRING( previous, promoted, GetData, data );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSimData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
            }
        }
        break;
    case FrameCPP::Common::FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        PROMOTE_FROM_NOTHING( FrSimEvent );
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrStatData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER_CONST(
                    promoted, GetRepresentation( ).size, 0, representation );
                CHECK_NUMBER( previous, promoted, GetTimeStart, timeStart );
                CHECK_NUMBER( previous, promoted, GetTimeEnd, timeEnd );
                CHECK_NUMBER( previous, promoted, GetVersion, version );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSummary );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetTest, test );
            }
        }
        break;
    case FrameCPP::Common::FrameSpec::Info::FSI_FR_TABLE:
        //-------------------------------------------------------------------
        // FrTable
        //-------------------------------------------------------------------
        PROMOTE_FROM_NOTHING( FrTable );
        break;
    case FrameSpec::Info::FSI_FR_TRIG_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrTrigData );

        if ( promoted.get( ) && previous.get( ) )
        {
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_STRING( previous, promoted, GetComment, comment );
            CHECK_STRING( previous, promoted, GetInputs, inputs );
            CHECK_NUMBER( previous, promoted, GetGTime, GTime );
            CHECK_NUMBER_4( previous,
                            GetBvalue,
                            promoted,
                            GetTriggerStatus,
                            triggerStatus );
            CHECK_NUMBER_4(
                previous, GetRvalue, promoted, GetAmplitude, amplitude );
            CHECK_NUMBER( previous, promoted, GetProbability, probability );
            CHECK_STRING( previous, promoted, GetStatistics, statistics );

            CHECK_NUMBER_CONST( promoted, GetTimeBefore, 0.0, timeBefore );
            CHECK_NUMBER_CONST( promoted, GetTimeAfter, 0.0, timeAfter );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_VECT:
        //-------------------------------------------------------------------
        // FrVect
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrVect );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_NUMBER( previous, promoted, GetCompress, compress );
                CHECK_NUMBER( previous, promoted, GetType, type );
                CHECK_NUMBER( previous, promoted, GetNData, nData );
                CHECK_NUMBER( previous, promoted, GetNBytes, nBytes );
                CHECK_NUMBER( previous, promoted, GetNDim, nDim );
                CHECK_NUMBER( previous, promoted, GetDim( 0 ).GetNx, nx );
                CHECK_NUMBER( previous, promoted, GetDim( 0 ).GetDx, dx );
                CHECK_NUMBER_CONST(
                    promoted, GetDim( 0 ).GetStartX, REAL_8( 0.0 ), startX );
                CHECK_STRING( previous, promoted, GetDim( 0 ).GetUnitX, unitX );
                CHECK_STRING( previous, promoted, GetUnitY, unitY );
            }
        }
        break;
    default:
    {
        std::ostringstream msg;

        msg << "verify_upconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
}

//=======================================================================
#undef USING
#undef PREVIOUS_NAMESPACE
#undef PREVIOUS_TEMPLATE_SPEC
#undef NAMESPACE
#undef TEMPLATE_SPEC

#endif /* FRAME_CPP__TEST__FR_STRUCT_4_TCC */
