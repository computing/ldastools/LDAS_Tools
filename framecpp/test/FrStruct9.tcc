// -*- mode: c++; -*-
//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__FR_STRUCT_9_TCC
#define FRAME_CPP__TEST__FR_STRUCT_9_TCC

#include "framecpp/Version9/FrameH.hh"
#include "framecpp/Version9/FrAdcData.hh"
#include "framecpp/Version9/FrDetector.hh"
#include "framecpp/Version9/FrEndOfFile.hh"
#include "framecpp/Version9/FrEndOfFrame.hh"
#include "framecpp/Version9/FrEvent.hh"
#include "framecpp/Version9/FrHistory.hh"
#include "framecpp/Version9/FrMsg.hh"
#include "framecpp/Version9/FrProcData.hh"
#include "framecpp/Version9/FrRawData.hh"
#include "framecpp/Version9/FrSerData.hh"
#include "framecpp/Version9/FrSimData.hh"
#include "framecpp/Version9/FrSimEvent.hh"
#include "framecpp/Version9/FrSummary.hh"
#include "framecpp/Version9/FrTable.hh"
#include "framecpp/Version9/FrVect.hh"
#include "framecpp/Version9/GPSTime.hh"
#include "framecpp/Version9/STRING.hh"

#define TEMPLATE_SPEC 9
#define NAMESPACE FrameCPP::Version_9
#define PREVIOUS_TEMPLATE_SPEC 8
#define PREVIOUS_NAMESPACE FrameCPP::Version_8
#define USING( ) using namespace NAMESPACE

#define INSTANTIATE( OBJECT )                                                  \
    template <>                                                                \
    struct PreviousInfo< NAMESPACE::OBJECT >                                   \
    {                                                                          \
        typedef PREVIOUS_NAMESPACE::OBJECT type;                               \
    }

INSTANTIATE( FrameH );
INSTANTIATE( FrAdcData );
INSTANTIATE( FrDetector );
INSTANTIATE( FrEvent );
INSTANTIATE( FrHistory );
INSTANTIATE( FrMsg );
INSTANTIATE( FrProcData );
INSTANTIATE( FrRawData );
INSTANTIATE( FrSerData );
INSTANTIATE( FrSimData );
INSTANTIATE( FrSimEvent );
INSTANTIATE( FrSummary );
INSTANTIATE( FrTable );
INSTANTIATE( FrVect );

#undef INSTANTIATE

template <
    typename FrameObjCurType,
    typename FrameObjPreType = typename PreviousInfo< FrameObjCurType >::type >
void
DemoteToSame9( frame_object_type FrameObj, const std::string& Leader )
{
    boost::shared_ptr< FrameObjCurType > obj_(
        boost::dynamic_pointer_cast< FrameObjCurType >( FrameObj ) );

    DemoteToSame< FrameObjCurType, FrameObjPreType >(
        obj_, PREVIOUS_TEMPLATE_SPEC, Leader );
}

template <
    typename FrameObjCurType,
    typename FrameObjPreType = typename PreviousInfo< FrameObjCurType >::type >
void
PromoteToSame9( frame_object_type FrameObj, const std::string& Leader )
{
    PromoteToSame< FrameObjCurType, FrameObjPreType >(
        FrameObj, TEMPLATE_SPEC, PREVIOUS_TEMPLATE_SPEC, Leader );
}

template <>
mk_frame_object_ret_type
mk_frame_object< TEMPLATE_SPEC >( FrameObjectTypes Type )
{
    USING( );

    using FrameCPP::Common::FrameSpec;

    mk_frame_object_ret_type retval;

    switch ( Type )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        retval.reset(
            new FrameH( "frame_h", 1, 8, GPSTime( 10, 20 ), 3.0, 2 ) );
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        retval.reset( new FrAdcData( "fr_adc_data",
                                     3,
                                     2,
                                     8,
                                     1024,
                                     2.0,
                                     1.0,
                                     "meters",
                                     30.,
                                     10.1,
                                     4,
                                     40.0 ) );
        static_cast< FrAdcData* >( retval.get( ) )
            ->AppendComment( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        static const char pre[ 2 ] = { 'a', 'b' };
        retval.reset( new FrDetector( "fr_detector_name",
                                      pre,
                                      1.1,
                                      2.2,
                                      200.3,
                                      3.3,
                                      4.4,
                                      5.5,
                                      6.6,
                                      7.7,
                                      8.8 ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            FrEvent::ParamList_type params;

            params.push_back( FrEvent::Param_type( "param1", 64.0 ) );

            retval.reset( new FrEvent( "fr_event",
                                       "fr_event_comment",
                                       "fr_event_inputs",
                                       GPSTime( 10, 20000000 ),
                                       1024.0,
                                       2048.0,
                                       1,
                                       16.0,
                                       32.0,
                                       "fr_event_statistics",
                                       params ) );
        }

        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
    {
        std::unique_ptr< FrHistory > fr_history(
            new FrHistory( "fr_history_name", 10, "fr_hsitory_comment" ) );
        retval.reset( fr_history.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_MSG:
    {
        std::unique_ptr< FrMsg > fr_msg(
            new FrMsg( "alarm", "message", 10, GPSTime::NowGPSTime( ) ) );
        retval.reset( fr_msg.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        retval.reset( new FrProcData( "fr_proc_data",
                                      "fr_proc_data_comment",
                                      1,
                                      0,
                                      16.0,
                                      2048.,
                                      1024.0,
                                      4.0,
                                      4096.0,
                                      0 ) );

        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        retval.reset( new FrRawData( "fr_raw_data" ) );
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        retval.reset(
            new FrSerData( "fr_ser_data", GPSTime( 10, 20 ), 1024. ) );

        static_cast< FrSerData* >( retval.get( ) )->SetData( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        retval.reset( new FrSimData(
            "fr_sim_data", "fr_sim_data_comment", 1024., 2048., 4096. ) );
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            FrSimEvent::ParamList_type params;

            params.push_back( FrSimEvent::Param_type( "param1", 64.0 ) );

            retval.reset( new FrSimEvent( "fr_event",
                                          "fr_event_comment",
                                          "fr_event_inputs",
                                          GPSTime( 10, 20000000 ),
                                          1024.0,
                                          2048.0,
                                          16.0,
                                          params ) );
        }

        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        retval.reset( new FrSummary( "fr_summary",
                                     "fr_summary_comment",
                                     "fr_summary_test",
                                     GPSTime( 10, 20 ) ) );

        break;
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        std::unique_ptr< FrTable > fr_table( new FrTable( "testTable", 0 ) );
        fr_table->AppendComment( "hello world" );
        retval.reset( fr_table.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_VECT:
    {
        typedef INT_4U vect_data_type;

        static const vect_data_type START = 0;
        static const vect_data_type STOP = 512;
        static const vect_data_type INC = 2;
        static const int            SAMPLES = ( ( STOP - START ) / INC );

        testing::Ramp::GenSawToothFalling< vect_data_type > data_generator(
            START, STOP, INC );

        Dimension      dim( SAMPLES );
        vect_data_type data[ SAMPLES ];

        std::generate( &( data[ 0 ] ), &( data[ SAMPLES ] ), data_generator );

        retval.reset( new FrVect( "fr_vect", 1, &dim, data, "fr_vect_unitY" ) );
        auto frvect = boost::dynamic_pointer_cast< FrVect >( retval );
        frvect->CompressData( FrVect::DIFF_GZIP, 3 );

        INT_8U nDataValid( SAMPLES );
        CHAR_U dataValid[ SAMPLES ];
        std::fill( dataValid, &dataValid[ SAMPLES ], 0 );
        frvect->SetDataValid( dataValid, nDataValid );
    }
    break;
    default:
    {
        std::ostringstream msg;

        msg << "mk_frame_obj<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << Type;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
    return retval;
}

//=======================================================================

template <>
void
verify_downconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                     const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
    {
        DEMOTE_TO_PREVIOUS( FrameH );
        if ( previous && current )
        {
            CHECK_STRING( previous, current, GetName, name );
            CHECK_NUMBER( previous, current, GetRun, run );
            CHECK_NUMBER( previous, current, GetFrame, frame );
            CHECK_NUMBER( previous, current, GetGTime, GTime );
            CHECK_NUMBER( previous, current, GetDt, dt );
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_ADC_DATA:
    {
        DEMOTE_TO_PREVIOUS( FrAdcData );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        DEMOTE_TO_PREVIOUS( FrDetector );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_EVENT:
    {
        DEMOTE_TO_PREVIOUS( FrEvent );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_HISTORY:
    {
        DEMOTE_TO_PREVIOUS( FrHistory );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_MSG:
    {
        DEMOTE_TO_PREVIOUS( FrMsg );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_PROC_DATA:
    {
        DEMOTE_TO_PREVIOUS( FrProcData );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_RAW_DATA:
    {
        DEMOTE_TO_PREVIOUS( FrRawData );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SER_DATA:
    {
        DEMOTE_TO_PREVIOUS( FrSerData );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SIM_DATA:
    {
        DEMOTE_TO_PREVIOUS( FrSimData );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
    {
        DEMOTE_TO_PREVIOUS( FrSimEvent );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SUMMARY:
    {
        DEMOTE_TO_PREVIOUS( FrSummary );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        DEMOTE_TO_PREVIOUS( FrTable );
        if ( previous && current )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_VECT:
    {
        DEMOTE_TO_PREVIOUS( FrVect );
        if ( previous && current )
        {
            //-------------------------------------------------------------
            // Compression of data
            //-------------------------------------------------------------
            BOOST_CHECK_MESSAGE(
                previous->GetCompress( ) == Previous::FrVect::DIFF_GZIP,
                "previous->GetCompress[0x"
                    << std::hex << previous->GetCompress( )
                    << "] =?= Previous::FrVect::DIFF_GZIP[0x" << std::hex
                    << Previous::FrVect::DIFF_GZIP << std::dec << "]" );
        }
        break;
    }
    default:
    {
        std::ostringstream msg;

        msg << "verify_downconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
        break;
    }
    } // switch
}

template <>
void
verify_upconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                   const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
    {
        PROMOTE_FROM_PREVIOUS( FrameH );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_ADC_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrAdcData );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        PROMOTE_FROM_PREVIOUS( FrDetector );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_EVENT:
    {
        PROMOTE_FROM_PREVIOUS( FrEvent );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_HISTORY:
    {
        PROMOTE_FROM_PREVIOUS( FrHistory );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_MSG:
    {
        PROMOTE_FROM_PREVIOUS( FrMsg );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_PROC_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrProcData );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_RAW_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrRawData );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SER_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrSerData );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SIM_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrSimData );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
    {
        PROMOTE_FROM_PREVIOUS( FrSimEvent );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_SUMMARY:
    {
        PROMOTE_FROM_PREVIOUS( FrSummary );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        PROMOTE_FROM_PREVIOUS( FrTable );
        if ( promoted && previous )
        {
        }
        break;
    }
    case FrameSpec::Info::FSI_FR_VECT:
    {
        PROMOTE_FROM_PREVIOUS( FrVect );
        if ( promoted && previous )
        {
            //-------------------------------------------------------------
            // Compression of data
            //-------------------------------------------------------------
            BOOST_CHECK_MESSAGE( promoted->GetCompress( ) == FrVect::DIFF_GZIP,
                                 "promoted->GetCompress[0x"
                                     << std::hex << promoted->GetCompress( )
                                     << "] =?= FrVect::DIFF_GZIP[0x" << std::hex
                                     << FrVect::DIFF_GZIP << std::dec << "]" );
        }
        break;
    }
    default:
    {
        std::ostringstream msg;

        msg << "verify_upconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
        break;
    }
    } // - switch
}

//=======================================================================

#undef USING
#undef PREVIOUS_TEMPLATE_SPEC
#undef PREVIOUS_NAMESPACE
#undef NAMESPACE
#undef TEMPLATE_SPEC

#endif /* FRAME_CPP__TEST__FR_STRUCT_9_TCC */
