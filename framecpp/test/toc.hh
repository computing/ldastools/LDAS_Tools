//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__TOC_TCC
#define FRAME_CPP__TEST__TOC_TCC

#include <algorithm>

void dump( const FrameCPP::Common::FrTOC* TOC );

template < typename J >
class printer
{
public:
    typedef void ( *func_type )( std::ostream&, const J& );
    inline printer( std::ostream& Stream, func_type Func )
        : m_out( Stream ), m_func( Func )
    {
    }

    inline virtual ~printer( )
    {
    }

    inline void
    operator( )( const J& Data )
    {
        m_out << " ";
        ( *m_func )( m_out, Data );
    }

protected:
    std::ostream& m_out;
    func_type     m_func;
};

template < typename T >
void
dump_toc_array( const char* Lead, const T& Element )
{
    std::cout << Lead << ":";

    for ( typename T::const_iterator cur = Element.begin( ),
                                     last = Element.end( );
          cur != last;
          ++cur )
    {
        std::cout << " " << *cur;
    }
    std::cout << std::endl;
}

template < typename P, typename C >
void
dump_toc_internal_data( const char* Lead,
                        const C&    Container,
                        void ( *Func )( std::ostream& Stream, const P& Data ) )
{
    std::cout << Lead << ":";
    std::for_each(
        Container.begin( ), Container.end( ), printer< P >( std::cout, Func ) );
    std::cout << std::endl;
}

template < typename TOC_TYPE >
inline bool
dump_ver_toc( const FrameCPP::Common::FrTOC* TOC )
{
    const TOC_TYPE* t = dynamic_cast< const TOC_TYPE* >( TOC );
    if ( t )
    {
        dump( t );
    }
    return ( t != (const TOC_TYPE*)NULL );
}

#include "toc4.tcc"
#include "toc6.tcc"
#include "toc8.tcc"
#include "toc9.tcc"

inline void
dump( const FrameCPP::Common::FrTOC* TOC )
{
    if ( TOC )
    {
#if 0
    //-------------------------------------------------------------------
    // This should work, but it currently fails with gcc 4.1.2 with
    // segfault on dynamic_cast<>.
    //-------------------------------------------------------------------
    if ( dump_ver_toc<FrameCPP::Version_8::FrTOC>( TOC ) ) { return; }
    else if ( dump_ver_toc<FrameCPP::Version_6::FrTOC>( TOC ) ) { return; }
    else if ( dump_ver_toc<FrameCPP::Version_4::FrTOC>( TOC ) ) { return; }
#else
        if ( dynamic_cast< const FrameCPP::Version_9::FrTOC* >( TOC ) )
        {
            dump( dynamic_cast< const FrameCPP::Version_9::FrTOC* >( TOC ) );
        }
        else if ( dynamic_cast< const FrameCPP::Version_8::FrTOC* >( TOC ) )
        {
            dump( dynamic_cast< const FrameCPP::Version_8::FrTOC* >( TOC ) );
        }
        else if ( dynamic_cast< const FrameCPP::Version_6::FrTOC* >( TOC ) )
        {
            dump( dynamic_cast< const FrameCPP::Version_6::FrTOC* >( TOC ) );
        }
        else if ( dynamic_cast< const FrameCPP::Version_4::FrTOC* >( TOC ) )
        {
            dump( dynamic_cast< const FrameCPP::Version_4::FrTOC* >( TOC ) );
        }
#endif /* 0 */
        else
        {
            throw std::range_error( "Unknown TOC version" );
        }
    }
    else
    {
        throw std::invalid_argument( "TOC pointer was null" );
    }
}

#endif /* FRAME_CPP__TEST__TOC_TCC */
