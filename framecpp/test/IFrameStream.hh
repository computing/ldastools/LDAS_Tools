//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_CPP__TEST__IFRAMESTREAM_HH
#define FRAME_CPP__TEST__IFRAMESTREAM_HH

namespace FrameCPP
{
    namespace Version_7
    {
        class IFrameStream;
    }

    namespace Version_6
    {
        class IFrameStream;
    }
    namespace Common
    {
        class IFrameStreamGen;
    }

    namespace Test
    {
        //-------------------------------------------------------------------
        /// \brief Version independent manipulation of frame stream.
        //-------------------------------------------------------------------
        class IFrameStream
        {
        private:
            typedef FrameCPP::Version_7::IFrameStream iframestream7;
            typedef FrameCPP::Version_6::IFrameStream iframestream6;
            typedef FrameCPP::Common::IFrameStreamGen iframestream;

        public:
            //-----------------------------------------------------------------
            /// \brief Constructor
            ///
            /// \param[in] Filename
            ///     The name of the frame file to be queried.
            /// \param[in] FrameSpec
            ///     Version frame specification version to use to create
            ///     the actual frame reader.
            ///
            /// \return
            ///     An instance of the class.
            //-----------------------------------------------------------------
            IFrameStream( const char* const Filename, int FrameSpec );

            INT_2U GetVersion( );

            const char* GetOriginator( );

            INT_2U GetLibraryRevision( );

        private:
            LDASTools::AL::ifstream m_istream;
            int                     m_frame_spec;
            iframestream*           m_gen_frame_stream;
            union
            {
                iframestream7* v7;
                iframestream6* v6;
            } m_frame_stream;
        }; // class - IFrameStream
    } // namespace Test
} // namespace FrameCPP

#endif /* FRAME_CPP__TEST__IFRAMESTREAM_HH */
