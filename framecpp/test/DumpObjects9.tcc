// -*- mode: c++ -*-
//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/FrSE.hh"

#include "framecpp/Version9/FrameH.hh"
#include "framecpp/Version9/FrDetector.hh"
#include "framecpp/Version9/FrEndOfFile.hh"
#include "framecpp/Version9/FrEndOfFrame.hh"
#include "framecpp/Version9/FrTOC.hh"

namespace
{
    namespace Version9
    {
        class DumpFrStruct : public ::DumpFrStruct
        {
        public:
            DumpFrStruct( bool VerboseDataMode, bool ScientificDataMode );

            DumpFrStruct( const ::DumpFrStruct& Source );

            virtual void Dump( const Object& Base );
        };

        DumpFrStruct::DumpFrStruct( bool VerboseDataMode,
                                    bool ScientificDataMode )
            : ::DumpFrStruct( VerboseDataMode,
                              ScientificDataMode,
                              ::DumpFrStruct::OUTPUT_MODE_DUMP )
        {
        }

#if 0
    DumpFrStruct::
    DumpFrStruct( const ::DumpFrStruct& Source )
      : ::DumpFrStruct( Source )
    {
    }
#endif /* 0 */

        void
        DumpFrStruct::Dump( const Object& Base )
        {
            using namespace FrameCPP::Version_9;

            switch ( Base.GetClass( ) )
            {
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_SH:
            {
                const FrSH& o( dynamic_cast< const FrSH& >( Base ) );

                header( "FrSH" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "class", o.FrSHImpl::Data::GetClass( ) );
                l( "comment", o.GetComment( ) );

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_SE:
            {
                const FrSE& o( dynamic_cast< const FrSE& >( Base ) );

                header( "FrSE" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "class", o.FrSEImpl::Data::GetClass( ) );
                l( "comment", o.GetComment( ) );

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_ADC_DATA:
            {
                const FrAdcData& o( dynamic_cast< const FrAdcData& >( Base ) );

                header( "FrAdcData" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "channelGroup", o.GetChannelGroup( ) );
                l( "channelNumber", o.GetChannelNumber( ) );
                l( "nBits", o.GetNBits( ) );
                l( "bias", o.GetBias( ) );
                l( "slope", o.GetSlope( ) );
                l( "units", o.GetUnits( ) );
                l( "sampleRate", o.GetSampleRate( ) );
                l( "timeOffset", o.GetTimeOffset( ) );
                l( "fShift", o.GetFShift( ) );
                l( "phase", o.GetPhase( ) );
                l( "dataValid", o.GetDataValid( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "data", o.RefData( ) );
                    dump_container( "aux", o.RefAux( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_DETECTOR:
            {
                const FrDetector& o(
                    dynamic_cast< const FrDetector& >( Base ) );

                header( "FrDetector" );

                std::string prefix;
                prefix += o.GetPrefix( )[ 0 ];
                prefix += o.GetPrefix( )[ 1 ];

                dump_label l( std::cout, m_pad, 15, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "prefix", prefix );
                l( "longitude", o.GetLongitude( ) );
                l( "latitude", o.GetLatitude( ) );
                l( "elevation", o.GetElevation( ) );
                l( "armXazimuth", o.GetArmXazimuth( ) );
                l( "armYazimuth", o.GetArmYazimuth( ) );
                l( "armXaltitude", o.GetArmXaltitude( ) );
                l( "armYaltitude", o.GetArmYaltitude( ) );
                l( "armXmidpoint", o.GetArmXmidpoint( ) );
                l( "armYmidpoint", o.GetArmYmidpoint( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "aux", o.RefAux( ) );
                    dump_container( "table", o.RefTable( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_END_OF_FILE:
            {
                const FrEndOfFile& o(
                    dynamic_cast< const FrEndOfFile& >( Base ) );

                header( "FrEndOfFile" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );

                l( "nFrames", o.GetNFrames( ) );
                l( "nBytes", o.GetNBytes( ) );
                l( "seekTOC", o.GetSeekTOC( ) );
                l( "chkSumFrHeader", o.GetChkSumFrHeader( ) );
                l( "chkSum", o.GetChkSum( ) );
                l( "chkSumFile", o.GetChkSumFile( ) );

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_END_OF_FRAME:
            {
                const FrEndOfFrame& o(
                    dynamic_cast< const FrEndOfFrame& >( Base ) );

                header( "FrEndOfFrame" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "run", o.GetRun( ) );
                l( "frame", o.GetFrame( ) );
                l( "GTimeS", o.GetGTime( ).GetSeconds( ) );
                l( "GTimeN", o.GetGTime( ).GetNanoseconds( ) );

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_EVENT:
            {
                const FrEvent& o( dynamic_cast< const FrEvent& >( Base ) );
                std::vector< REAL_4 >      parameters;
                std::vector< std::string > parameterNames;

                header( "FrEvent" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "inputs", o.GetInputs( ) );
                l( "GTimeS", o.GetGTime( ).GetSeconds( ) );
                l( "GTimeN", o.GetGTime( ).GetNanoseconds( ) );
                l( "timeBefore", o.GetTimeBefore( ) );
                l( "timeAfter", o.GetTimeAfter( ) );
                l( "eventStatus", o.GetEventStatus( ) );
                l( "amplitude", o.GetAmplitude( ) );
                l( "probability", o.GetProbability( ) );
                l( "statistics", o.GetStatistics( ) );
                l( "nParam", o.GetParam( ).size( ) );
                for ( FrEvent::ParamList_type::const_iterator
                          cur = o.GetParam( ).begin( ),
                          end = o.GetParam( ).end( );
                      cur != end;
                      ++cur )
                {
                    parameterNames.push_back( ( *cur ).first );
                    parameters.push_back( ( *cur ).second );
                }
                l( "parameters", parameters );
                l( "parameterNames", parameterNames );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "data", o.RefData( ) );
                    dump_container( "table", o.RefTable( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FRAME_H:
            {
                const FrameH& o( dynamic_cast< const FrameH& >( Base ) );
                dump_label    l( std::cout, m_pad, 12, m_scientific_data_mode );

                header( "FrameH" );

                l( "name", o.GetName( ) );
                l( "run", o.GetRun( ) );
                l( "frame", o.GetFrame( ) );
                l( "dataQuality", o.GetDataQuality( ) );
                l( "GTimeS", o.GetGTime( ).GetSeconds( ) );
                l( "GTimeN", o.GetGTime( ).GetNanoseconds( ) );
                l( "dt", o.GetDt( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "type", o.RefType( ) );
                    dump_container( "user", o.RefUser( ) );
                    dump_container( "detectSim", o.RefDetectSim( ) );
                    dump_container( "detectProc", o.RefDetectProc( ) );
                    dump_container( "history", o.RefHistory( ) );
                    if ( o.GetRawData( ) )
                    {
                        Dump( *( o.GetRawData( ) ) );
                    }
                    else
                    {
                        std::cout << m_pad << "FrRawData - NULL" << std::endl;
                    }
                    dump_container( "procData", o.RefProcData( ) );
                    dump_container( "simData", o.RefSimData( ) );
                    dump_container( "event", o.RefEvent( ) );
                    dump_container( "simEvent", o.RefSimEvent( ) );
                    dump_container( "summaryData", o.RefSummaryData( ) );
                    dump_container( "auxData", o.RefAuxData( ) );
                    dump_container( "auxTable", o.RefAuxTable( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_HISTORY:
            {
                const FrHistory& o( dynamic_cast< const FrHistory& >( Base ) );

                header( "FrHistory" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );

                l( "name", o.GetName( ) );
                l( "time", o.GetTime( ) );
                l( "comment", o.GetComment( ) );

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_MSG:
            {
                const FrMsg& o( dynamic_cast< const FrMsg& >( Base ) );

                header( "FrMsg" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "alarm", o.GetAlarm( ) );
                l( "message", o.GetMessage( ) );
                l( "severity", o.GetSeverity( ) );
                l( "GTimeS", o.GetGTime( ).GetSeconds( ) );
                l( "GTimeN", o.GetGTime( ).GetNanoseconds( ) );

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_PROC_DATA:
            {
                const FrProcData& o(
                    dynamic_cast< const FrProcData& >( Base ) );
                std::vector< REAL_4 >      parameters;
                std::vector< std::string > parameterNames;

                header( "FrProcData" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "type", o.GetType( ) );
                l( "subType", o.GetSubType( ) );
                l( "timeOffset", o.GetTimeOffset( ) );
                l( "tRange", o.GetTRange( ) );
                l( "fShift", o.GetFShift( ) );
                l( "phase", o.GetPhase( ) );
                l( "fRange", o.GetFRange( ) );
                l( "BW", o.GetBW( ) );
                l( "nAuxParam", o.GetAuxParam( ).size( ) );
                for ( FrProcData::AuxParamList_type::const_iterator
                          cur = o.GetAuxParam( ).begin( ),
                          end = o.GetAuxParam( ).end( );
                      cur != end;
                      ++cur )
                {
                    parameters.push_back( ( *cur ).auxParam );
                    parameterNames.push_back( ( *cur ).auxParamName );
                }
                l( "auxParam", parameters );
                l( "auxParamNames", parameterNames );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "data", o.RefData( ) );
                    dump_container( "aux", o.RefAux( ) );
                    dump_container( "table", o.RefTable( ) );
                    dump_container( "history", o.RefHistory( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_RAW_DATA:
            {
                const FrRawData& o( dynamic_cast< const FrRawData& >( Base ) );

                header( "FrRawData" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "firstSer", o.RefFirstSer( ) );
                    dump_container( "firstAdc", o.RefFirstAdc( ) );
                    dump_container( "firsTable", o.RefFirstTable( ) );
                    dump_container( "logMsg", o.RefLogMsg( ) );
                    dump_container( "more", o.RefMore( ) );
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_SER_DATA:
            {
                const FrSerData& o( dynamic_cast< const FrSerData& >( Base ) );

                header( "FrSerData" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "timeSec", o.GetTime( ).GetSeconds( ) );
                l( "timeNsec", o.GetTime( ).GetNanoseconds( ) );
                l( "sampleRate", o.GetSampleRate( ) );
                l( "data", o.GetData( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "serial", o.RefSerial( ) );
                    dump_container( "table", o.RefTable( ) );
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_SIM_DATA:
            {
                const FrSimData& o( dynamic_cast< const FrSimData& >( Base ) );

                header( "FrSimData" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "sampleRate", o.GetSampleRate( ) );
                l( "timeOffset", o.GetTimeOffset( ) );
                l( "fShift", o.GetFShift( ) );
                l( "phase", o.GetPhase( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "data", o.RefData( ) );
                    dump_container( "input", o.RefInput( ) );
                    dump_container( "table", o.RefTable( ) );
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_SIM_EVENT:
            {
                const FrSimEvent& o(
                    dynamic_cast< const FrSimEvent& >( Base ) );
                std::vector< REAL_8 >      parameters;
                std::vector< std::string > parameterNames;

                header( "FrSimEvent" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "inputs", o.GetInputs( ) );
                l( "GTimeS", o.GetGTime( ).GetSeconds( ) );
                l( "GTimeN", o.GetGTime( ).GetNanoseconds( ) );
                l( "timeBefore", o.GetTimeBefore( ) );
                l( "timeAfter", o.GetTimeAfter( ) );
                l( "amplitude", o.GetAmplitude( ) );
                l( "nParam", o.GetParam( ).size( ) );
                for ( FrSimEvent::ParamList_type::const_iterator
                          cur = o.GetParam( ).begin( ),
                          end = o.GetParam( ).end( );
                      cur != end;
                      ++cur )
                {
                    parameterNames.push_back( ( *cur ).first );
                    parameters.push_back( ( *cur ).second );
                }
                l( "parameters", parameters );
                l( "parameterNames", parameterNames );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "data", o.RefData( ) );
                    dump_container( "table", o.RefTable( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_SUMMARY:
            {
                const FrSummary& o( dynamic_cast< const FrSummary& >( Base ) );

                header( "FrSummary" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "test", o.GetTest( ) );
                l( "GTimeS", o.GetGTime( ).GetSeconds( ) );
                l( "GTimeN", o.GetGTime( ).GetNanoseconds( ) );

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "moments", o.RefMoments( ) );
                    dump_container( "table", o.RefTable( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_TABLE:
            {
                const FrTable& o( dynamic_cast< const FrTable& >( Base ) );

                header( "FrTable" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "comment", o.GetComment( ) );
                l( "nColumn", o.GetNColumn( ) );
                l( "nRow", o.GetNRow( ) );

                //:TODO: dump columnName

                switch ( m_output_mode )
                {
                case OUTPUT_MODE_DUMP:
                    dump_container( "column", o.RefColumn( ) );
                    break;
                case OUTPUT_MODE_DUMP_OBJECTS:
                    break;
                }

                trailer( );
            }
            break;
            case FrameCPP::Common::FrameSpec::Info::FSI_FR_VECT:
            {
                const FrVect& o( dynamic_cast< const FrVect& >( Base ) );
                std::vector< INT_8U > nx;
                std::vector< REAL_8 > dx;
                std::vector< REAL_8 > startX;
                std::vector< STRING > unitX;
                for ( INT_4U x = 0; x < o.GetNDim( ); x++ )
                {
                    const Dimension& d( o.GetDim( x ) );
                    nx.push_back( d.GetNx( ) );
                    dx.push_back( d.GetDx( ) );
                    startX.push_back( d.GetStartX( ) );
                    unitX.push_back( d.GetUnitX( ) );
                }

                header( "FrVect" );

                dump_label l( std::cout, m_pad, 10, m_scientific_data_mode );
                l( "name", o.GetName( ) );
                l( "compress", o.GetCompress( ) );
                l( "type", o.GetType( ) );
                l( "nData", o.GetNData( ) );
                l( "nBytes", o.GetNBytes( ) );
                l( "nDim", o.GetNDim( ) );
                l( "nx", nx );
                l( "dx", dx );
                l( "startX", startX );
                l( "unitX", unitX );
                l( "unitY", o.GetUnitY( ) );

                if ( m_verbose_data_mode == false )
                {
                    l( "data", "..." );
                }
                else
                {
                    FrVect::data_type expanded;

                    o.GetDataUncompressed( expanded );

                    switch ( o.GetType( ) )
                    {
                    case FrVect::FR_VECT_C:
                        l( "data",
                           vect< CHAR >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_2S:
                        l( "data",
                           vect< INT_2S >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_8R:
                        l( "data",
                           vect< REAL_8 >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_4R:
                        l( "data",
                           vect< REAL_4 >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_4S:
                        l( "data",
                           vect< INT_4S >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_8S:
                        l( "data",
                           vect< INT_8S >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_8C:
                        l( "data",
                           vect< COMPLEX_8 >( expanded.get( ),
                                              o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_16C:
                        l( "data",
                           vect< COMPLEX_16 >( expanded.get( ),
                                               o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_2U:
                        l( "data",
                           vect< INT_2U >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_4U:
                        l( "data",
                           vect< INT_4U >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_8U:
                        l( "data",
                           vect< INT_8U >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    case FrVect::FR_VECT_1U:
                        l( "data",
                           vect< CHAR_U >( expanded.get( ), o.GetNData( ) ) );
                        break;
                    }
                }

                trailer( );
            }
            break;
            default:
            {
                std::ostringstream msg;

                msg << "Unable to dump structure of type: " << Base.GetClass( );
                throw std::invalid_argument( msg.str( ) );
            }
            break;
            }
        }
    } // namespace Version9
} // namespace

template <>
void
dump< 9 >( Object* Obj, const CommandLine& Options )
{
    if ( Obj )
    {
        ::Version9::DumpFrStruct dumper( ( Options.SilentData( ) == false ),
                                         Options.ScientificNotation( ) );

        dumper.Dump( *Obj );
    }
}
template <>
void
dump_toc_list< 9 >( const Object* TOC, object_type Type )
{
    using namespace FrameCPP::Version_9;

    const FrTOC* toc = dynamic_cast< const FrTOC* >( TOC );

    if ( toc )
    {
        if ( ( Type == ANY ) || ( Type == ADC ) )
        {
            list_names( toc->GetADC( ), true );
        }
        if ( ( Type == ANY ) || ( Type == EVENT ) )
        {
            list_names( toc->GetEvent( ), true );
        }
        if ( ( Type == ANY ) || ( Type == PROC ) )
        {
            list_names( toc->GetProc( ), true );
        }
        if ( ( Type == ANY ) || ( Type == SER ) )
        {
            list_names( toc->GetSer( ), true );
        }
        if ( ( Type == ANY ) || ( Type == SIM ) )
        {
            list_names( toc->GetSim( ), true );
        }
        if ( ( Type == ANY ) || ( Type == SIM_EVENT ) )
        {
            list_names( toc->GetSimEvent( ), true );
        }
    }
}
