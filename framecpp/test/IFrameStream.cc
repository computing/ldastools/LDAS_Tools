//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "ldastoolsal/fstream.hh"

#include "framecpp/Version7/IFrameStream.hh"

#include "framecpp/Version6/IFrameStream.hh"

#include "framecpp/Common/IFrameStreamGen.hh"

#include "IFrameStream.hh"

namespace FrameCPP
{
    namespace Test
    {
        IFrameStream::IFrameStream( const char* const Filename, int FrameSpec )
            : m_istream( Filename ), m_frame_spec( FrameSpec )
        {
            //-----------------------------------------------------------------
            // Verify the creation of the stream
            //-----------------------------------------------------------------
            if ( !m_istream.good( ) )
            {
                std::ostringstream msg;

                msg << "Unable to open file: " << Filename;
                throw std::runtime_error( msg.str( ) );
            }
            try
            {
                m_gen_frame_stream = (iframestream*)NULL;
                //---------------------------------------------------------------
                // Create the actual frame reader
                //---------------------------------------------------------------
                switch ( FrameSpec )
                {
                case 7:
                    m_frame_stream.v7 = new iframestream7( m_istream );
                    m_gen_frame_stream =
                        dynamic_cast< iframestream* >( m_frame_stream.v7 );
                    break;
                case 6:
                    m_frame_stream.v6 = new iframestream6( m_istream );
                    break;
                default:
                {
                    std::ostringstream msg;
                    msg << "Unable to create IFrameStream for Frame Spec: "
                        << FrameSpec;
                    throw std::runtime_error( msg.str( ) );
                }
                break;
                }
            }
            catch ( ... )
            {
                m_istream.close( );
                throw;
            }
        }

        INT_2U
        IFrameStream::GetVersion( )
        {
            INT_2U retval = 0;
            if ( m_gen_frame_stream )
            {
                retval = m_gen_frame_stream->GetVersion( );
            }
            else if ( m_frame_spec == 6 )
            {
                retval = m_frame_stream.v6->GetVersion( );
            }
            return retval;
        }

        INT_2U
        IFrameStream::GetLibraryRevision( )
        {
            INT_2U retval = 0;
            if ( m_gen_frame_stream )
            {
                retval = m_gen_frame_stream->GetLibraryRevision( );
            }
            else if ( m_frame_spec == 6 )
            {
                retval = m_frame_stream.v6->GetLibraryRevision( );
            }
            return retval;
        }

        const char*
        IFrameStream::GetOriginator( )
        {
            if ( m_gen_frame_stream )
            {
                return m_gen_frame_stream->GetOriginator( );
            }
            else if ( m_frame_spec == 6 )
            {
                return m_frame_stream.v6->GetOriginator( );
            }
            return "";
        }
    } // namespace Test
} // namespace FrameCPP
