//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#if HAVE_CONFIG_H
#include <framecpp_config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <list>
#include <sstream>
#include <stdexcept>
#include <string>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/util.hh"
#include "ldastoolsal/types.hh"

#include "framecpp/FrameCPP.hh"
#include "framecpp/IFrameStream.hh"

#include "StandardOptions.hh"

using std::filebuf;

using LDASTools::AL::cmp_nocase;
using LDASTools::AL::MemChecker;

using FrameCPP::Common::FrameBuffer;
typedef FrameCPP::Common::FrameSpec::Object Object;
typedef LDASTools::AL::CommandLineOptions   CommandLineOptions;
typedef CommandLineOptions::Option          Option;
typedef CommandLineOptions::OptionSet       OptionSet;
typedef ::FrameCPP::Common::FrameSpec::ObjectInterface::object_type
    object_base_type;

typedef FrameCPP::Common::IFrameStreamWrapper< 0,
                                               Object /* FrAdcData */,
                                               Object /* FrameH */,
                                               Object /* FrProcData */,
                                               Object /* FrSerData */,
                                               Object /* FrSimData */,
                                               Object /* FrRawData */,
                                               Object /* FrEvent */,
                                               Object /* FrSimEvent */ >
    IFrameStream;

class CommandLine;

enum object_type
{
    ADC,
    HEADER,
    LIST,
    PROC,
    ANY,
    SIM,
    SER,
    EVENT,
    SIM_EVENT
};

struct request_typearget_type
{
    enum
    {
        NAME,
        INDEX
    } type;
    std::string name;
    INT_4U      index;

    request_typearget_type( const std::string& Target )
        : type( NAME ), name( Target )
    {
        if ( Target.length( ) > 0 )
        {
            std::istringstream iss( Target );
            std::string        junk;
            iss >> index;
            if ( iss.good( ) )
            {
                iss >> junk;
                if ( junk.length( ) == 0 )
                {
                    type = INDEX;
                }
            }
        }
    }
};

object_type convert_to_type( const std::string& Type );

inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

//-----------------------------------------------------------------------
/// \brief Class to handle command line options for this application
//-----------------------------------------------------------------------
class CommandLine : protected CommandLineOptions
{
public:
    typedef std::pair< object_type, request_typearget_type > request_type;
    typedef std::list< request_type >      request_container_type;
    typedef std::pair< INT_4U, INT_4U >    range_info_type;
    typedef std::vector< range_info_type > range_type;

    CommandLine( int ArgC, char** ArgV );

    inline void
    Usage( int ExitValue ) const
    {
        std::cout << "Usage: " << ProgramName( ) << m_options << std::endl;
        depart( ExitValue );
    }

    using CommandLineOptions::empty;
    using CommandLineOptions::Pop;
    using CommandLineOptions::size;

    inline bool
    BadOption( ) const
    {
        bool retval = false;

        for ( const_iterator cur = begin( ), last = end( ); cur != last; ++cur )
        {
            if ( ( *cur )[ 0 ] == '-' )
            {
                std::cerr << "ABORT: Bad option: " << *cur << std::endl;
                retval = true;
            }
        }
        return retval;
    }

    inline void
    Range( INT_4U First, INT_4U Last )
    {
        m_ranges.push_back( range_info_type( First, Last ) );
    }

    inline const range_type&
    Ranges( ) const
    {
        return m_ranges;
    }

    inline void
    RangesPop( )
    {
        m_ranges.pop_back( );
    }

    inline const request_container_type&
    Requests( ) const
    {
        return m_requests;
    }

    inline bool
    ScientificNotation( ) const
    {
        return m_scientific_notation;
    }

    inline void
    ScientificNotation( bool Value )
    {
        m_scientific_notation = Value;
    }

    inline bool
    SilentData( ) const
    {
        return m_silent_data;
    }

    inline void
    SilentData( bool Value )
    {
        m_silent_data = Value;
    }

private:
    enum option_types
    {
        OPT_ADC,
        OPT_DUMP,
        OPT_FRAME_RANGE,
        OPT_HEADER,
        OPT_HELP,
        OPT_LIST,
        OPT_PROC,
        OPT_SCIENTIFIC_NOTATION,
        OPT_SILENT_DATA
    };

    OptionSet m_options;
    bool      m_scientific_notation;
    bool      m_silent_data;

    request_container_type m_requests;
    range_type             m_ranges;
};

CommandLine::CommandLine( int ArgC, char** ArgV )
    : CommandLineOptions( ArgC, ArgV ), m_scientific_notation( false ),
      m_silent_data( false )
{
    //---------------------------------------------------------------------
    // Setup the options that will be recognized.
    //---------------------------------------------------------------------
    std::ostringstream desc;

    m_options.Synopsis( "[options] <file> [<file> ...]" );

    m_options.Summary( "This command will dump the table of contents for each"
                       " file specified on the command line." );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add( Option( OPT_ADC,
                           "adc",
                           Option::ARG_REQUIRED,
                           "Display the specified FrADC channel.",
                           "channel-name" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add(
        Option( OPT_DUMP,
                "dump",
                Option::ARG_REQUIRED,
                "Dump the contents of the specified channel.",
                "[(adc|event|proc|ser|sim|simevent)%]channel-name" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add( Option( OPT_FRAME_RANGE,
                           "frame-range",
                           Option::ARG_REQUIRED,
                           "Specify the range of frames to use in queries.",
                           "first[,last]" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Display the frame header information.";
    m_options.Add(
        Option( OPT_HEADER, "header", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    StandardOption< STANDARD_OPTION_HELP >( m_options, OPT_HELP );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add( Option( OPT_LIST,
                           "list",
                           Option::ARG_REQUIRED,
                           "Display thhe channel names of the specified type.",
                           "adc|event|proc|ser|sim|simevent|any" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add( Option( OPT_PROC,
                           "proc",
                           Option::ARG_REQUIRED,
                           "Display the specified FrProc channel.",
                           "channel-name" ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Display real and complex elements in scientific notation."
            " (Default: "
         << ( ( ScientificNotation( ) ) ? "enabled" : "disabled" ) << " )";
    m_options.Add( Option( OPT_SCIENTIFIC_NOTATION,
                           "scientific-notation",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    StandardOption< STANDARD_OPTION_SILENT_DATA >(
        m_options, OPT_SILENT_DATA, SilentData( ) );

    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------

    try
    {
        std::string arg_name;
        std::string arg_value;
        bool        parsing = true;

        while ( parsing )
        {
            const int cmd_id( Parse( m_options, arg_name, arg_value ) );

            switch ( cmd_id )
            {
            case CommandLineOptions::OPT_END_OF_OPTIONS:
                parsing = false;
                break;
            case OPT_ADC:
                m_requests.push_back(
                    request_type( ADC, request_typearget_type( arg_value ) ) );
                break;
            case OPT_DUMP:
            {
                std::string            name( arg_value );
                std::string            prefix;
                std::string::size_type pos = name.find_first_of( "%" );

                if ( pos != std::string::npos )
                {
                    prefix = name.substr( 0, pos );
                    ++pos;
                    name.replace( 0, pos, "" );
                }
                m_requests.push_back(
                    request_type( convert_to_type( prefix ),
                                  request_typearget_type( name ) ) );
            }
            break;
            case OPT_FRAME_RANGE:
            {
                std::istringstream iss( arg_value );
                INT_4U             first, last;
                char               c;

                iss >> first;
                if ( iss.eof( ) )
                {
                    last = first;
                }
                else
                {
                    iss >> c >> last;
                }

                m_ranges.push_back( range_info_type( first, ++last ) );
            }
            break;
            case OPT_HEADER:
                m_requests.push_back(
                    request_type( HEADER, request_typearget_type( "" ) ) );
                break;
            case OPT_HELP:
            {
                Usage( 0 );
            }
            break;
            case OPT_LIST:
                m_requests.push_back(
                    request_type( LIST, request_typearget_type( arg_value ) ) );
                break;
            case OPT_PROC:
                m_requests.push_back(
                    request_type( PROC, request_typearget_type( arg_value ) ) );
                break;
            case OPT_SCIENTIFIC_NOTATION:
                ScientificNotation( true );
                break;
            case OPT_SILENT_DATA:
                SilentData( true );
                break;
            }
        }
    }
    catch ( ... )
    {
    }
}

namespace
{
    template < class T >
    void
    list_names( const T& DataSet, bool Alphabetical )
    {
        typename T::const_iterator begin( DataSet.begin( ) );
        typename T::const_iterator end( DataSet.end( ) );

        std::list< std::string > names;

        for ( typename T::const_iterator o( begin ); o != end; o++ )
        {
            names.push_back( ( *o ).first );
        }
        if ( Alphabetical )
        {
            names.sort( );
        }
        for ( std::list< std::string >::const_iterator i( names.begin( ) );
              i != names.end( );
              i++ )
        {
            std::cout << ( *i ) << std::endl;
        }
    }

} // namespace

template < int >
void dump( Object* Obj, const CommandLine& Options );
template < int >
void dump_toc_list( const Object* TOC, object_type Type );

#include "FrameQuery.tcc"
#include "DumpObjects6.cc"
#include "DumpObjects7.tcc"
#include "DumpObjects8.tcc"
#include "DumpObjects9.tcc"

namespace
{
    void
    dump_object( INT_4U Version, Object* Obj, const CommandLine& Options )
    {
        switch ( Version )
        {
        case 9:
          dump< 9 >( Obj, Options );
          break;
        case 8:
            dump< 8 >( Obj, Options );
            break;
        case 7:
            dump< 7 >( Obj, Options );
            break;
        case 6:
            dump< 6 >( Obj, Options );
            break;
        default:
        {
            std::ostringstream msg;

            msg << "Version " << Version
                << " of the frame specification is currently unsupported by "
                   "this application";
            throw std::runtime_error( msg.str( ) );
        }
        }
    }

    void
    toc_list( INT_4U Version, const Object* TOC, object_type Type )
    {
        switch ( Version )
        {
        case 8:
            dump_toc_list< 8 >( TOC, Type );
            break;
        case 7:
            dump_toc_list< 7 >( TOC, Type );
            break;
        case 6:
            dump_toc_list< 6 >( TOC, Type );
            break;
        default:
        {
            std::ostringstream msg;

            msg << "Version " << Version
                << " of the frame specification is currently unsupported by "
                   "this application";
            throw std::runtime_error( msg.str( ) );
        }
        }
    }

} // namespace

object_type
convert_to_type( const std::string& Type )
{
    object_type retval = ANY;

    if ( cmp_nocase( Type, "adc" ) == 0 )
    {
        retval = ADC;
    }
    else if ( cmp_nocase( Type, "event" ) == 0 )
    {
        retval = EVENT;
    }
    else if ( cmp_nocase( Type, "proc" ) == 0 )
    {
        retval = PROC;
    }
    else if ( cmp_nocase( Type, "ser" ) == 0 )
    {
        retval = SER;
    }
    else if ( cmp_nocase( Type, "sim" ) == 0 )
    {
        retval = SIM;
    }
    else if ( cmp_nocase( Type, "simevent" ) == 0 )
    {
        retval = SIM_EVENT;
    }
    return retval;
}

int
main( int ArgC, char** ArgV ) try
{
    MemChecker::Trigger gc_trigger( true );
    CommandLine         cl( ArgC, ArgV );

    int exit_status = 0;

    FrameCPP::Initialize( );

    if ( ( cl.empty( ) ) || ( cl.BadOption( ) ) )
    {
        cl.Usage( 1 );
    }
    while ( cl.empty( ) == false )
    {
        try
        {
            //-----------------------------------------------------------------
            // Create new instance
            //-----------------------------------------------------------------
            FrameBuffer< filebuf >* ifb(
                new FrameBuffer< filebuf >( std::ios::in ) );
            ifb->open( cl.Pop( ).c_str( ), std::ios::in | std::ios::binary );
            IFrameStream frame_stream( ifb );

            //-----------------------------------------------------------------
            // Give some basic information
            //-----------------------------------------------------------------
            std::cout << "Frame Version: " << frame_stream.Version( );
            try
            {
                const FrameCPP::Common::FrHeader& hdr(
                    frame_stream.GetFrHeader( ) );

                std::cout << " Frame Library: " << hdr.GetFrameLibraryName( )
                          << " Library Revision: " << hdr.GetLibraryRevision( )
                          << " Originator: " << hdr.GetOriginator( );
            }
            catch ( ... )
            {
                // Some information does not exist
            }
            std::cout << std::endl;
            //-----------------------------------------------------------------
            // Start with providing the user requested information.
            //-----------------------------------------------------------------
            if ( cl.Requests( ).size( ) == 0 )
            {
                try
                {
                    if ( cl.Ranges( ).size( ) == 0 )
                    {
                        //-------------------------------------------------------------
                        // Read
                        //-------------------------------------------------------------
                        object_base_type frame_base(
                            frame_stream.ReadFrame( ) );

                        try
                        {
                            while ( frame_base )
                            {
                                dump_object( frame_stream.Version( ),
                                             frame_base.get( ),
                                             cl );
                                frame_base = frame_stream.ReadFrame( );
                            }
                        }
                        catch ( const std::range_error& Error )
                        {
                            //---------------------------------------------------------
                            // This is how we expect to exit.
                            //---------------------------------------------------------
                        }
                    }
                    else
                    {
                        //-------------------------------------------------------------
                        // Read specific frames from the frame file
                        //-------------------------------------------------------------
                        object_base_type frame_base(
                            frame_stream.ReadFrame( ) );

                        for ( CommandLine::range_type::const_iterator
                                  current_range = cl.Ranges( ).begin( ),
                                  end_range = cl.Ranges( ).end( );
                              current_range != end_range;
                              current_range++ )
                        {
                            for ( INT_4U
                                      current_frame( ( *current_range ).first ),
                                  end_frame( ( *current_range ).second );
                                  current_frame < end_frame;
                                  current_frame++ )
                            {
                                std::cout << "Frame Offset: " << current_frame
                                          << std::endl;

                                frame_base =
                                    frame_stream.ReadFrameN( current_frame );

                                if ( frame_base )
                                {
                                    dump_object( frame_stream.Version( ),
                                                 frame_base.get( ),
                                                 cl );
                                }
                            }
                        }
                    }
                }
                catch ( const std::exception& e )
                {
                    std::cerr << "Caught exception while processing file: "
                              << e.what( ) << std::endl;
                }
            }
            else
            {
                bool patched_range( false );
                if ( cl.Ranges( ).size( ) == 0 )
                {
                    cl.Range( 0, frame_stream.GetNumberOfFrames( ) );
                    patched_range = true;
                }
                object_base_type object;
                int              action;

                for ( CommandLine::range_type::const_iterator
                          current_range = cl.Ranges( ).begin( ),
                          end_range = cl.Ranges( ).end( );
                      current_range != end_range;
                      ++current_range )
                {
                    for ( INT_4U current_frame( ( *current_range ).first ),
                          end_frame( ( *current_range ).second );
                          current_frame < end_frame;
                          ++current_frame )
                    {
                        std::cout << "Frame Offset: " << current_frame
                                  << std::endl;
                        for ( CommandLine::request_container_type::
                                  const_iterator r( cl.Requests( ).begin( ) );
                              r != cl.Requests( ).end( );
                              r++ )
                        {
                            int                                 outputs = 0;
                            CommandLine::request_container_type dump_objects;

                            object.reset( );
                            action = ( *r ).first;
                            if ( action == ANY )
                            {
                                dump_objects.push_back(
                                    CommandLine::request_type(
                                        ADC, ( *r ).second ) );
                                dump_objects.push_back(
                                    CommandLine::request_type(
                                        EVENT, ( *r ).second ) );
                                dump_objects.push_back(
                                    CommandLine::request_type(
                                        PROC, ( *r ).second ) );
                                dump_objects.push_back(
                                    CommandLine::request_type(
                                        SER, ( *r ).second ) );
                                dump_objects.push_back(
                                    CommandLine::request_type(
                                        SIM, ( *r ).second ) );
                                dump_objects.push_back(
                                    CommandLine::request_type(
                                        SIM_EVENT, ( *r ).second ) );
                            }
                            else
                            {
                                dump_objects.push_back( *r );
                            }
                            for ( std::list< CommandLine::request_type >::
                                      const_iterator d( dump_objects.begin( ) );
                                  d != dump_objects.end( );
                                  d++ )
                            {
                                switch ( ( *d ).first )
                                {
#define READ_OBJECT_BY_NAME( FUNC )                                            \
    if ( ( *d ).second.type == request_typearget_type::NAME )                  \
    {                                                                          \
        object = frame_stream.FUNC( current_frame, ( *d ).second.name );       \
    }

#define READ_OBJECT( FUNC )                                                    \
    READ_OBJECT_BY_NAME( FUNC )                                                \
    else if ( ( *d ).second.type == request_typearget_type::INDEX )            \
    {                                                                          \
        object = frame_stream.FUNC( current_frame, ( *d ).second.index );      \
    }

                                case ADC:
                                    READ_OBJECT( ReadFrAdcData );
                                    break;
                                case EVENT:
                                    READ_OBJECT_BY_NAME( ReadFrEvent );
                                    break;
                                case PROC:
                                    READ_OBJECT( ReadFrProcData );
                                    break;
                                case SER:
                                    READ_OBJECT_BY_NAME( ReadFrSerData );
                                    break;
                                case SIM:
                                    READ_OBJECT_BY_NAME( ReadFrSimData );
                                    break;
                                case SIM_EVENT:
                                    READ_OBJECT_BY_NAME( ReadFrSimEvent );
                                    break;

#undef READ_OBJECT
#undef READ_OBJECT_BY_NAME

                                case HEADER:
                                {
                                    using FrameCPP::Common::FrameH;

                                    object = frame_stream.ReadFrameH(
                                        current_frame,
                                        FrameH::DETECT_SIM |
                                            FrameH::DETECT_PROC |
                                            FrameH::HISTORY );
                                }
                                break;
                                case LIST:
                                {
                                    ++outputs;
                                    const Object* toc( frame_stream.GetTOC( ) );
                                    object_type   type =
                                        convert_to_type( ( *d ).second.name );

                                    toc_list(
                                        frame_stream.Version( ), toc, type );
                                }
                                break;
                                default:
                                    break;
                                }
                                if ( object.get( ) )
                                {
                                    dump_object( frame_stream.Version( ),
                                                 object.get( ),
                                                 cl );
                                    ++outputs;
                                }
                            }
                            if ( outputs == 0 )
                            {
                                std::cerr << "WARNING: Unknown object named: "
                                          << ( *r ).second.name << std::endl;
                            }
                        }
                    }
                }
                if ( patched_range )
                {
                    // Remove the element that was added
                    cl.RangesPop( );
                }
            }
        }
        catch ( const std::exception& exc )
        {
            std::cerr << exc.what( ) << std::endl;
            exit_status = 1;
        }
    } // for ( int x = 0; x < ArgC; x++ )

    depart( exit_status );
}
catch ( std::exception& e )
{
    std::cerr << "ABORT: Caught exception: " << e.what( ) << std::endl;
    depart( 1 );
}
catch ( ... )
{
    std::cerr << "ABORT: Caught unknown exception: " << std::endl;
    depart( 1 );
}
