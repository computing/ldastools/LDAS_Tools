#ifndef fr_history_hh
#define fr_history_hh

#include <typeinfo>
#include <type_traits>

#include <boost/test/included/unit_test.hpp>

#include "ldastoolsal/ldas_types.h"

#include "framecpp/FrHistory.hh"

namespace testing
{
    namespace fr_history
    {
        INSTANTIATE_STRUCT_ALL( FrHistory, FR_HISTORY_TYPE );

        template < int T_FRAME_SPEC_VERSION >
        using fr_history_type =
            typename fr_object_impl< T_FRAME_SPEC_VERSION,
                                     FR_HISTORY_TYPE >::type;
        template < int T_FRAME_SPEC_VERSION >
        using fr_history_previous_type =
            typename fr_object_previous_impl< T_FRAME_SPEC_VERSION,
                                              FR_HISTORY_TYPE >::type;

        //======================================
        // F U N C T I O N:  version
        //======================================
        // -----
        /// \brief Track where the transitions take place
        // -----
        typedef base_version_constant< 3 > version_root_type;

        template < int T_FRAME_SPEC_VERSION >
        constexpr bool
        is_root( )
        {
            return ( T_FRAME_SPEC_VERSION == version_root_type::value );
        }

        // ---------------
        // version_changes
        //
        // Track where changes to this data structure appear in the frame
        // specification
        // ---------------
        template < int T_FRAME_SPEC_VERSION >
        constexpr int
        version_changes( )
        {
            return ( std::conditional<
                     T_FRAME_SPEC_VERSION >= 8,
                     base_version_constant< 8 >::type,
                     typename std::enable_if< T_FRAME_SPEC_VERSION >=
                                                  version_root_type::value,
                                              version_root_type::type >
                     // conditional - 8
                     >::type
                     // return value
                     ::type::value );
        }

        template < int T_FRAME_SPEC_VERSION >
        constexpr VERSION
                  version( )
        {
            return ( frame_spec_to_version_mapper<
                     version_changes< T_FRAME_SPEC_VERSION >( ) >( ) );
        }

        template < int T_FRAME_SPEC_VERSION >
        constexpr bool
        is_change_version( )
        {
            return ( testing::is_change_version<
                     T_FRAME_SPEC_VERSION,
                     version_changes< T_FRAME_SPEC_VERSION >( ) >( ) );
        }

        // ============
        // version_info
        //
        // Custumization point on how each change to the spec should be handled
        // ============
        template < int T_FRAME_SPEC_VERSION, VERSION T_VERSION >
        struct version_info;

        template < int T_FRAME_SPEC_VERSION >
        struct version_info< T_FRAME_SPEC_VERSION, VERSION::V3 >
        {
            constexpr static int frame_spec_version{ T_FRAME_SPEC_VERSION };

            typedef std::string name_type;
            typedef std::string comment_type;
            typedef INT_4U      time_type;

            struct expected_type
            {
                // -----------
                // Seed values
                // -----------
                // FrHistory
                name_type    name{ name_type( "name" ) };
                comment_type comment{ comment_type( "comment" ) };
                time_type    time{ 20 };

                bool
                compare( const fr_history_type< T_FRAME_SPEC_VERSION >& Actual )
                {
                    return (
                        // FrHistory
                        ( Actual.GetName( ).compare( name ) == 0 ) &&
                        ( Actual.GetComment( ).compare( comment ) == 0 ) &&
                        ( Actual.GetTime( ) == time )
                        // FrHistory end
                    );
                }
                void
                set_default_constructor_values( )
                {
                    // local overrides for corner case of default constructor
                    name = "";
                    comment = "";
                    time = 0;
                }
            };

            static bool
            validate_constructors( )
            {
                bool retval{ true };

                try
                {
                    // -------------------
                    // Default constructor
                    // -------------------
                    fr_history_type< T_FRAME_SPEC_VERSION > history;

                    expected_type expected;
                    expected.set_default_constructor_values( );

                    std::cerr << "DEBUG: Default constructor "
                              << expected.compare( history ) << std::endl;
                    retval = retval && expected.compare( history );
                }
                catch ( ... )
                {
                    retval = false;
                }
                // -----
                // Explicit constructor
                // -----
                try
                {
                    expected_type                           expected;
                    fr_history_type< T_FRAME_SPEC_VERSION > history(
                        expected.name, expected.time, expected.comment );

                    std::cerr << "DEBUG: Explicit constructor "
                              << expected.compare( history ) << std::endl;
                    retval = retval && expected.compare( history );
                }
                catch ( ... )
                {
                    retval = false;
                }
                // -----
                // Copy constructor
                // -----
                try
                {
                    expected_type                           expected;
                    fr_history_type< T_FRAME_SPEC_VERSION > source_history(
                        expected.name, expected.time, expected.comment );
                    fr_history_type< T_FRAME_SPEC_VERSION > history(
                        source_history );

                    std::cerr << "DEBUG: Copy constructor "
                              << expected.compare( history ) << std::endl;
                    retval = retval && expected.compare( history );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }

            static bool
            validate_data_types( )
            {
                fr_history_type< frame_spec_version > history;
                dimension_type< frame_spec_version >  dims;

                return ( // FrHistory
                    ( check_data_type_string< frame_spec_version >(
                        history.GetName( ) ) ) &&
                    ( check_data_type_string< frame_spec_version >(
                        history.GetComment( ) ) ) &&
                    ( check_data_type< time_type >( history.GetTime( ) ) )
                    // FrHistory - end
                );
            }
        };

        template < int T_FRAME_SPEC_VERSION >
        struct version_info< T_FRAME_SPEC_VERSION, VERSION::V8 >
        {
            constexpr static int frame_spec_version{ T_FRAME_SPEC_VERSION };

            using current_type = fr_history_type< frame_spec_version >;
            using previous_type =
                fr_history_previous_type< frame_spec_version >;

            constexpr static INT_4U current{ frame_spec_version };
            constexpr static auto   previous = previous_version( current );

            typedef std::string            name_type;
            typedef std::string            comment_type;
            typedef INT_4U                 time_type;
            typedef LDASTools::AL::GPSTime gtime_type;

            inline static gtime_type
            DEFAULT_G_TIME( )
            {
                static gtime_type value;
                return ( value );
            }

            struct expected_type
            {
                // -----------
                // Seed values
                // -----------
                // FrHistory
                name_type    name{ name_type( "name" ) };
                comment_type comment{ comment_type( "comment" ) };
                time_type    time{ 20 };
                gtime_type   gtime{ gtime_type( 5, 7 ) };

                bool
                compare( const fr_history_type< T_FRAME_SPEC_VERSION >& Actual )
                {
                    return (
                        // FrHistory
                        ( Actual.GetName( ).compare( name ) == 0 ) &&
                        ( Actual.GetComment( ).compare( comment ) == 0 ) &&
                        ( Actual.GetTime( ) == time )
                        // FrHistory end

                    );
                }
                void
                set_default_constructor_values( )
                {
                    // local overrides for corner case of default constructor
                    name = "";
                    comment = "";
                    time = 0;
                    gtime = DEFAULT_G_TIME( );
                }
            };

            static bool
            validate_constructors( )
            {
                bool retval{ true };

                try
                {
                    // -------------------
                    // Default constructor
                    // -------------------
                    fr_history_type< T_FRAME_SPEC_VERSION > history;

                    expected_type expected;
                    expected.set_default_constructor_values( );

                    std::cerr << "DEBUG: Default constructor "
                              << expected.compare( history ) << std::endl;
                    retval = retval && expected.compare( history );
                }
                catch ( ... )
                {
                    retval = false;
                }
                // -----
                // Explicit constructor
                // -----
                try
                {
                    expected_type                           expected;
                    fr_history_type< T_FRAME_SPEC_VERSION > history(
                        expected.name, expected.time, expected.comment );

                    std::cerr << "DEBUG: Explicit constructor "
                              << expected.compare( history ) << std::endl;
                    retval = retval && expected.compare( history );
                }
                catch ( ... )
                {
                    retval = false;
                }
                // -----
                // Copy constructor
                // -----
                try
                {
                    expected_type                           expected;
                    fr_history_type< T_FRAME_SPEC_VERSION > source_history(
                        expected.name, expected.time, expected.comment );
                    fr_history_type< T_FRAME_SPEC_VERSION > history(
                        source_history );

                    std::cerr << "DEBUG: Copy constructor "
                              << expected.compare( history ) << std::endl;
                    retval = retval && expected.compare( history );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }

            static bool
            validate_data_types( )
            {
                fr_history_type< frame_spec_version > history;
                dimension_type< frame_spec_version >  dims;

                return ( // FrHistory
                    ( check_data_type_string< frame_spec_version >(
                        history.GetName( ) ) ) &&
                    ( check_data_type_string< frame_spec_version >(
                        history.GetComment( ) ) ) &&
                    ( check_data_type< time_type >( history.GetTime( ) ) )
                    // FrHistory - end
                );
            }

            static bool
            compare_down_convert( object_type< frame_spec_version > Source,
                                  object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto source_history =
                        boost::dynamic_pointer_cast< current_type >( Source );
                    auto derived_history =
                        boost::dynamic_pointer_cast< previous_type >( Derived );
                    // -----
                    // Do comparison
                    // -----
                    return (
                        // Must be different addresses
                        ( static_cast< const void* >( source_history.get( ) ) !=
                          static_cast< const void* >(
                              derived_history.get( ) ) ) &&
                        // FrHistory
                        ( source_history->GetName( ).compare(
                              derived_history->GetName( ) ) == 0 ) &&
                        ( source_history->GetComment( ).compare(
                              derived_history->GetComment( ) ) == 0 ) &&
                        ( source_history->GetTime( ) ==
                          derived_history->GetTime( ) )
                        // FrHistory end
                    );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }

            static bool
            compare_up_convert( object_type< frame_spec_version > Source,
                                object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto source_history =
                        boost::dynamic_pointer_cast< previous_type >( Source );
                    auto derived_history =
                        boost::dynamic_pointer_cast< current_type >( Derived );

                    return ( compare_down_convert( Derived, Source ) );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }
        };

        //======================================
        // F U N C T I O N:  mk_fr_history
        //======================================

        template < int T_FRAME_SPEC_VERSION, VERSION T_VERSION >
        struct mk_fr_history_helper;

        template < int T_FRAME_SPEC_VERSION >
        struct mk_fr_history_helper< T_FRAME_SPEC_VERSION, VERSION::V3 >
        {
            using retval_type = fr_history_type< T_FRAME_SPEC_VERSION >;

            static retval_type*
            create( )
            {
                static std::string NAME( "nameing" );
                static std::string COMMENT( "greeting" );
                static INT_4U      TIME{ 20 };

                return ( new retval_type( NAME, TIME, COMMENT ) );
            };
        };

        template < int T_FRAME_SPEC_VERSION >
        struct mk_fr_history_helper< T_FRAME_SPEC_VERSION, VERSION::V8 >
        {
            using retval_type = fr_history_type< T_FRAME_SPEC_VERSION >;

            static retval_type*
            create( )
            {
                static std::string NAME( "nameing" );
                static std::string COMMENT( "greeting" );
                static INT_4U      TIME{ 20 };

                return ( new retval_type( NAME, TIME, COMMENT ) );
            };
        };

        template < int T_FRAME_SPEC_VERSION >
        typename FR_HISTORY_TYPE< T_FRAME_SPEC_VERSION >::type*
        mk_fr_history( )
        {
            return ( mk_fr_history_helper<
                     T_FRAME_SPEC_VERSION,
                     version< T_FRAME_SPEC_VERSION >( ) >::create( ) );
        }

        //======================================
        // F U N C T I O N:  verify_data_types
        //======================================

        template < int T_FRAME_SPEC_VERSION >
        bool
        verify_data_types( )
        {
            return (
                version_info<
                    T_FRAME_SPEC_VERSION,
                    version< version_changes< T_FRAME_SPEC_VERSION >( ) >( ) >::
                    validate_data_types( ) );
        }

        //========================================
        // F U N C T I O N:   verify_constructors
        //========================================

        template < int T_FRAME_SPEC_VERSION >
        bool
        verify_constructors( )
        {
            return ( version_info< T_FRAME_SPEC_VERSION,
                                   version< T_FRAME_SPEC_VERSION >( ) >::
                         validate_constructors( ) );
        }

        //======================================
        // F U N C T I O N:   verify_convert
        //======================================

        // -----
        // -----
        template < int T_FRAME_SPEC_VERSION, CONVERT T_CONVERSION >
        struct verify_convert_helper;

        // -----
        /// Specialization to handle T_FRAME_SPEC_VERSION 3 as this is the root
        /// of the frame specification
        // -----
        template < int T_FRAME_SPEC_VERSION >
        struct verify_convert_helper< T_FRAME_SPEC_VERSION, CONVERT::ROOT >
        {
            static bool
            check( )
            {
                return ( convert_check_root< T_FRAME_SPEC_VERSION >(
                    mk_fr_history< T_FRAME_SPEC_VERSION > ) );
            }
        };

        // -----
        /// Specialization to handle no conversion as the base and derived
        /// versions are the same
        // -----
        template < int T_FRAME_SPEC_VERSION >
        struct verify_convert_helper< T_FRAME_SPEC_VERSION, CONVERT::SAME >
        {
            static bool
            check( )
            {
                return ( convert_check_same< T_FRAME_SPEC_VERSION >(
                    mk_fr_history< T_FRAME_SPEC_VERSION >,
                    mk_fr_history< previous_version(
                        T_FRAME_SPEC_VERSION ) > ) );
            }
        };

        // -----
        /// Specialization to handle no conversion as the base and derived
        /// versions are the same
        // -----
        template < int T_FRAME_SPEC_VERSION, CONVERT T_CONVERSION >
        struct verify_convert_helper
        {
            static bool
            check( )
            {
                constexpr INT_4U current{ T_FRAME_SPEC_VERSION };
                constexpr auto   previous = previous_version( current );

                bool retval{ true };

                // -----
                // Down convert
                // -----
                try
                {
                    // With no conversion, the object returned should be at the
                    // same address
                    object_type< T_FRAME_SPEC_VERSION > source_obj{
                        mk_fr_history< current >( )
                    };
                    auto derived_obj = source_obj->DemoteObject(
                        previous, source_obj, nullptr );
                    retval = retval &&
                        version_info< T_FRAME_SPEC_VERSION,
                                      frame_spec_to_version_mapper<
                                          convert_to_frame_spec_mapper<
                                              T_CONVERSION >( ) >( ) >::
                            compare_down_convert( source_obj, derived_obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                // -----
                // Up convert
                // -----
                try
                {
                    // With no conversion, the object returned should be at the
                    // same address
                    object_type< T_FRAME_SPEC_VERSION > source_obj{
                        mk_fr_history< previous >( )
                    };
                    auto derived_obj = source_obj->PromoteObject(
                        current, previous, source_obj, nullptr );
                    retval = retval &&
                        version_info< T_FRAME_SPEC_VERSION,
                                      frame_spec_to_version_mapper<
                                          convert_to_frame_spec_mapper<
                                              T_CONVERSION >( ) >( ) >::
                            compare_up_convert( source_obj, derived_obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }
        };

        template < int T_FRAME_SPEC_VERSION >
        bool
        verify_convert( )
        {
            return ( verify_convert_helper<
                     T_FRAME_SPEC_VERSION,
                     conversion<
                         T_FRAME_SPEC_VERSION,
                         is_root< T_FRAME_SPEC_VERSION >( ),
                         is_change_version< T_FRAME_SPEC_VERSION >( ) >( ) >::
                         check( ) );
        }

        //======================================
        // F U N C T I O N:   do_standard_tests
        //======================================
        template < int FrameSpecT >
        void
        do_standard_tests( )
        {
            BOOST_TEST_MESSAGE( "Checking basic correctness of frame structure "
                                "FrHistory at version "
                                << FrameSpecT );
            BOOST_CHECK( verify_data_types< FrameSpecT >( ) );
            BOOST_CHECK( verify_constructors< FrameSpecT >( ) );
            BOOST_CHECK( verify_convert< FrameSpecT >( ) );
        }
    } // namespace fr_history
} // namespace testing

void
fr_history( )
{
  testing::fr_history::do_standard_tests< 3 >( );
  testing::fr_history::do_standard_tests< 4 >( );
  testing::fr_history::do_standard_tests< 6 >( );
  testing::fr_history::do_standard_tests< 7 >( );
  testing::fr_history::do_standard_tests< 8 >( );
}

#endif /* fr_history_hh */
