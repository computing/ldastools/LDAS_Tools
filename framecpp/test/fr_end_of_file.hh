#ifndef fr_end_of_file_hh
#define fr_end_of_file_hh

#include <cmath>
#include <typeinfo>
#include <type_traits>

#include <boost/test/included/unit_test.hpp>

#include "ldastoolsal/ldas_types.h"

#include "framecpp/Version3/FrEndOfFile.hh"
#include "framecpp/Version4/FrEndOfFile.hh"
#include "framecpp/Version6/FrEndOfFile.hh"
#include "framecpp/Version8/FrEndOfFile.hh"

namespace testing
{
    namespace fr_end_of_file
    {
        INSTANTIATE_STRUCT_ALL( FrEndOfFile, FR_END_OF_FILE_TYPE );

        template < int T_FRAME_SPEC_VERSION >
        using fr_end_of_file_type =
            typename fr_object_impl< T_FRAME_SPEC_VERSION,
                                     FR_END_OF_FILE_TYPE >::type;
        template < int T_FRAME_SPEC_VERSION >
        using fr_end_of_file_previous_type =
            typename fr_object_previous_impl< T_FRAME_SPEC_VERSION,
                                              FR_END_OF_FILE_TYPE >::type;

        //===============================
        // F U N C T I O N: DMSToRadians
        //===============================
        static inline REAL_8
        DMSToRadians( INT_2S Degrees, INT_2S Minutes, REAL_4 Seconds )
        {
            REAL_8 retval = 0.0;

            if ( Degrees >= 0 )
            {
                retval = (REAL_8)Degrees + (REAL_8)Minutes / 60. +
                    (REAL_8)Seconds / 3600.;
            }
            else
            {
                retval = (REAL_8)Degrees - (REAL_8)Minutes / 60. -
                    (REAL_8)Seconds / 3600.;
            }
            retval = ( retval * M_PI ) / 180.;

            return retval;
        }

        //======================================
        // F U N C T I O N:  version
        //======================================
        // -----
        /// \brief Track where the transitions take place
        // -----
        typedef base_version_constant< 3 > version_root_type;

        template < int T_FRAME_SPEC_VERSION >
        constexpr bool
        is_root( )
        {
            return ( T_FRAME_SPEC_VERSION == version_root_type::value );
        }

        // ---------------
        // version_changes
        //
        // Track where changes to this data structure appear in the frame
        // specification
        // ---------------
        template < int T_FRAME_SPEC_VERSION >
        constexpr int
        version_changes( )
        {
            return ( std::conditional< T_FRAME_SPEC_VERSION >= 8,
                                       base_version_constant< 8 >::type,
                                       typename std::conditional<
                                           T_FRAME_SPEC_VERSION >= 6,
                                           base_version_constant< 6 >::type,
                                           typename std::conditional<
                                               T_FRAME_SPEC_VERSION >= 4,
                                               base_version_constant< 4 >::type,
                                               typename std::enable_if<
                                                   T_FRAME_SPEC_VERSION >=
                                                       version_root_type::value,
                                                   version_root_type::type >
                                               // conditional - 4
                                               >::type
                                           // conditional - 6
                                           >::type
                                       // conditional - 8
                                       >::type
                     // return value
                     ::type::value );
        }

        template < int T_FRAME_SPEC_VERSION >
        constexpr VERSION
                  version( )
        {
            return ( frame_spec_to_version_mapper<
                     version_changes< T_FRAME_SPEC_VERSION >( ) >( ) );
        }

        template < int T_FRAME_SPEC_VERSION >
        constexpr bool
        is_change_version( )
        {
            return ( testing::is_change_version<
                     T_FRAME_SPEC_VERSION,
                     version_changes< T_FRAME_SPEC_VERSION >( ) >( ) );
        }

        // ============
        // version_info
        //
        // Custumization point on how each change to the spec should be handled
        // ============
        template < int T_FRAME_SPEC_VERSION, VERSION T_VERSION >
        struct version_info;

        template < int T_FRAME_SPEC_VERSION >
        struct version_info< T_FRAME_SPEC_VERSION, VERSION::V3 >
        {
            constexpr static int frame_spec_version{ T_FRAME_SPEC_VERSION };

          typedef INT_4U nFrames_type;
          typedef INT_4U nBytes_type;
          typedef INT_4U chkFlag_type;
          typedef INT_4U chkSum_type;

            struct expected_type
            {
                // -----------
                // Seed values
                // -----------
                // FrEndOfFile
              nFrames_type nFrames{ 0 };
              nBytes_type nBytes{ 0 };
              chkFlag_type chkFlag{ 0 };
              chkSum_type chkSum{ 0 };

                bool
                compare(
                    const fr_end_of_file_type< T_FRAME_SPEC_VERSION >& Actual )
                {
                    return (
                        // FrEndOfFile
                        ( Actual.NBytes( ) == nBytes ) &&
                        ( Actual.NFrames( ) == nFrames )
                        // FrEndOfFile end
                    );
                }
                void
                set_default_constructor_values( )
                {
                    // local overrides for corner case of default constructor
                }
            };

            static bool
            validate_constructors( )
            {
                bool retval{ true };

                try
                {
                    // -------------------
                    // Default constructor
                    // -------------------
                    fr_end_of_file_type< T_FRAME_SPEC_VERSION > obj;

                    expected_type expected;
                    expected.set_default_constructor_values( );

                    std::cerr << "DEBUG: Default constructor "
                              << expected.compare( obj ) << std::endl;
                    retval = retval && expected.compare( obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }

            static bool
            validate_data_types( )
            {
                fr_end_of_file_type< frame_spec_version > obj;

                return ( // FrEndOfFile
                    true
                    // FrEndOfFile - end
                );
            }
        };

        template < int T_FRAME_SPEC_VERSION >
        struct version_info< T_FRAME_SPEC_VERSION, VERSION::V4 >
        {
            constexpr static int frame_spec_version{ T_FRAME_SPEC_VERSION };

            using current_type = fr_end_of_file_type< frame_spec_version >;
            using previous_type =
                fr_end_of_file_previous_type< frame_spec_version >;

          typedef INT_4U nFrames_type;
          typedef INT_4U nBytes_type;
          typedef INT_4U chkFlag_type;
          typedef INT_4U chkSum_type;
          typedef INT_4U seekTOC_type;

            struct expected_type
            {
                // -----------
                // Seed values
                // -----------
                // FrEndOfFile
              nFrames_type nFrames{ 0 };
              nBytes_type nBytes{ 0 };
              chkFlag_type chkFlag{ 0 };
              chkSum_type chkSum{ 0 };
              seekTOC_type seekTOC{ 0 };

                bool
                compare(
                    const fr_end_of_file_type< T_FRAME_SPEC_VERSION >& Actual )
                {
                    return (
                        // FrEndOfFile
                            ( Actual.NBytes( ) == nBytes ) &&
                            ( Actual.NFrames( ) == nFrames ) &&
                            ( Actual.GetSeekTOC( ) == seekTOC )
                        // FrEndOfFile end
                    );
                }
                void
                set_default_constructor_values( )
                {
                    // local overrides for corner case of default constructor
                }
            };

            static bool
            validate_constructors( )
            {
                bool retval{ true };

                try
                {
                    // -------------------
                    // Default constructor
                    // -------------------
                    fr_end_of_file_type< T_FRAME_SPEC_VERSION > obj;

                    expected_type expected;
                    expected.set_default_constructor_values( );

                    std::cerr << "DEBUG: Default constructor "
                              << expected.compare( obj ) << std::endl;
                    retval = retval && expected.compare( obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }

            static bool
            validate_data_types( )
            {
                fr_end_of_file_type< frame_spec_version > obj;

                return ( // FrEndOfFile
                    ( check_data_type< seekTOC_type >(
                        obj.GetSeekTOC( ) ) )
                    // FrEndOfFile - end
                );
            }

            static bool
            compare_down_convert( object_type< frame_spec_version > Source,
                                  object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto source_obj =
                        boost::dynamic_pointer_cast< current_type >( Source );
                    auto derived_obj =
                        boost::dynamic_pointer_cast< previous_type >( Derived );
                    // -----
                    // Do comparison
                    // -----

                    std::cerr << "DEBUG: NBytes: " << source_obj->NBytes( ) << " =?= " << derived_obj->NBytes( ) << std::endl
                              << "\tNFrames: " << source_obj->NFrames( ) << " =?= " << derived_obj->NFrames( ) << std::endl;

                    return (
                        // Must be different addresses
                        ( static_cast< const void* >( source_obj.get( ) ) !=
                          static_cast< const void* >( derived_obj.get( ) ) ) /* &&
                        // FrEndOfFile
                        ( source_obj->NBytes( ) == derived_obj->NBytes( ) ) &&
                        ( source_obj->NFrames( ) == derived_obj->NFrames( ) ) */
                        // FrEndOfFile end
                    );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }

            static bool
            compare_up_convert( object_type< frame_spec_version > Source,
                                object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto derived_obj =
                        boost::dynamic_pointer_cast< current_type >( Derived );

                    return (
                            compare_down_convert( Derived, Source ) &&
                        // FrEndOfFile - begin
                        ( derived_obj->GetSeekTOC( ) == 0 )
                        // FrEndOfFile - end
                    );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }
        };

        template < int T_FRAME_SPEC_VERSION >
        struct version_info< T_FRAME_SPEC_VERSION, VERSION::V6 >
        {
            constexpr static int frame_spec_version{ T_FRAME_SPEC_VERSION };

            using current_type = fr_end_of_file_type< frame_spec_version >;
            using previous_type =
                fr_end_of_file_previous_type< frame_spec_version >;

            constexpr static INT_4U current{ frame_spec_version };
            constexpr static auto   previous = previous_version( current );

          typedef INT_4U nFrames_type;
          typedef INT_8U nBytes_type;
          typedef INT_4U chkFlag_type;
          typedef INT_4U chkSum_type;
          typedef INT_8U seekTOC_type;

            struct expected_type
            {
                // -----------
                // Seed values
                // -----------
                // FrEndOfFile
              nFrames_type nFrames{ 0 };
              nBytes_type nBytes{ 0 };
              chkFlag_type chkFlag{ 0 };
              chkSum_type chkSum{ 0 };
              seekTOC_type seekTOC{ 0 };

                bool
                compare(
                    const fr_end_of_file_type< T_FRAME_SPEC_VERSION >& Actual )
                {
                    return (
                        // FrEndOfFile
                            ( Actual.NBytes( ) == nBytes ) &&
                            ( Actual.NFrames( ) == nFrames ) &&
                            ( Actual.GetSeekTOC( ) == seekTOC )
                        // FrEndOfFile end
                    );
                }
                void
                set_default_constructor_values( )
                {
                    // local overrides for corner case of default constructor
                }
            };

            static bool
            validate_constructors( )
            {
                bool retval{ true };

                try
                {
                    // -------------------
                    // Default constructor
                    // -------------------
                    fr_end_of_file_type< T_FRAME_SPEC_VERSION > obj;

                    expected_type expected;
                    expected.set_default_constructor_values( );

                    std::cerr << "DEBUG: Default constructor "
                              << expected.compare( obj ) << std::endl;
                    retval = retval && expected.compare( obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }

            static bool
            validate_data_types( )
            {
                fr_end_of_file_type< frame_spec_version > obj;

                return ( // FrEndOfFile
                        ( check_data_type< seekTOC_type >(
                                                          obj.GetSeekTOC( ) ) )
                    // FrEndOfFile - end
                );
            }

            static bool
            compare_down_convert( object_type< frame_spec_version > Source,
                                  object_type< frame_spec_version > Derived,
                                  REAL_8 Multiplier = 1.0 )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto source_obj =
                        boost::dynamic_pointer_cast< current_type >( Source );
                    auto derived_obj =
                        boost::dynamic_pointer_cast< previous_type >( Derived );

                    return (
                        // Must be different addresses
                        ( static_cast< const void* >( source_obj.get( ) ) !=
                          static_cast< const void* >( derived_obj.get( ) ) )
                        // Address check - end
                        &&
                        // FrEndOfFile
                        ( source_obj->NBytes( ) == derived_obj->NBytes( ) ) &&
                        ( source_obj->NFrames( ) == derived_obj->NFrames( ) ) &&
                        ( source_obj->GetSeekTOC( ) == derived_obj->GetSeekTOC( ) )
                        // FrEndOfFile end
                    );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }

            static bool
            compare_up_convert( object_type< frame_spec_version > Source,
                                object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto derived_obj =
                        boost::dynamic_pointer_cast< current_type >( Derived );

                    return ( compare_down_convert( Derived, Source, -1.0 )
                             // Previous - end
                             // New fields - begin
                             // Nothing new
                             // New fields - end
                    );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }
        };

        template < int T_FRAME_SPEC_VERSION >
        struct version_info< T_FRAME_SPEC_VERSION, VERSION::V8 >
        {
            constexpr static int frame_spec_version{ T_FRAME_SPEC_VERSION };

            using current_type = fr_end_of_file_type< frame_spec_version >;
            using previous_type =
                fr_end_of_file_previous_type< frame_spec_version >;

            constexpr static INT_4U current{ frame_spec_version };
            constexpr static auto   previous = previous_version( current );

          typedef INT_4U nFrames_type;
          typedef INT_8U nBytes_type;
          typedef INT_8U seekTOC_type;
          typedef INT_4U chkSumHeader_type;
          typedef INT_4U chkSum_type;
          typedef INT_4U chkSumFile_type;

            struct expected_type
            {
                // -----------
                // Seed values
                // -----------
                // FrEndOfFile
              nFrames_type nFrames{ 0 };
              nBytes_type nBytes{ 0 };
              seekTOC_type seekTOC{ 0 };
              chkSum_type chkSumHeader{ 0 };
              chkSum_type chkSum{ 0 };
              chkSum_type chkSumFile{ 0 };

                bool
                compare(
                    const fr_end_of_file_type< T_FRAME_SPEC_VERSION >& Actual )
                {
                    return (
                        // FrEndOfFile
                            ( Actual.NBytes( ) == nBytes ) &&
                            ( Actual.NFrames( ) == nFrames ) &&
                            ( Actual.SeekTOC( ) == seekTOC )
                        // FrEndOfFile end
                    );
                }
                void
                set_default_constructor_values( )
                {
                    // local overrides for corner case of default
                    // constructor
                }
            };

            static bool
            validate_constructors( )
            {
                bool retval{ true };

                try
                {
                    // -------------------
                    // Default constructor
                    // -------------------
                    fr_end_of_file_type< T_FRAME_SPEC_VERSION > obj;

                    expected_type expected;
                    expected.set_default_constructor_values( );

                    std::cerr << "DEBUG: Default constructor "
                              << expected.compare( obj ) << std::endl;
                    retval = retval && expected.compare( obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }

            static bool
            validate_data_types( )
            {
                fr_end_of_file_type< frame_spec_version > obj;

                return ( // FrEndOfFile
                        true
                    // FrEndOfFile - end
                );
            }

            static bool
            compare_down_convert( object_type< frame_spec_version > Source,
                                  object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    auto source_obj =
                        boost::dynamic_pointer_cast< current_type >( Source );
                    auto derived_obj =
                        boost::dynamic_pointer_cast< previous_type >( Derived );
                    // -----
                    // Do comparison
                    // -----
                    return (
                        // Must be different addresses
                        ( static_cast< const void* >( source_obj.get( ) ) !=
                          static_cast< const void* >( derived_obj.get( ) ) ) &&
                        // FrEndOfFile
                        ( source_obj->NBytes( ) == derived_obj->NBytes( ) ) &&
                        ( source_obj->NFrames( ) == derived_obj->NFrames( ) ) &&
                        ( source_obj->SeekTOC( ) == derived_obj->SeekTOC( ) )
                        // FrEndOfFile end
                    );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }

            static bool
            compare_up_convert( object_type< frame_spec_version > Source,
                                object_type< frame_spec_version > Derived )
            {
                bool retval{ true };

                try
                {
                    // -----
                    // Cast to concreate type
                    // -----
                    return ( compare_down_convert( Derived, Source ) );
                }
                catch ( ... )
                {
                    retval = false;
                }
                return ( retval );
            }
        };

        //======================================
        // F U N C T I O N:  mk_fr_end_of_file
        //======================================

        template < int T_FRAME_SPEC_VERSION, VERSION T_VERSION >
        struct mk_fr_end_of_file_helper;

        template < int T_FRAME_SPEC_VERSION >
        struct mk_fr_end_of_file_helper< T_FRAME_SPEC_VERSION, VERSION::V3 >
        {
            using retval_type = fr_end_of_file_type< T_FRAME_SPEC_VERSION >;

            static retval_type*
            create( )
            {
              std::cerr << "DEBUG: Creating new instance for version 3G" << std::endl;
              return ( new retval_type );
            };
        };

        template < int T_FRAME_SPEC_VERSION >
        struct mk_fr_end_of_file_helper< T_FRAME_SPEC_VERSION, VERSION::V4 >
        {
            using retval_type = fr_end_of_file_type< T_FRAME_SPEC_VERSION >;

            static retval_type*
            create( )
            {
              return ( new retval_type );
            };
        };

        template < int T_FRAME_SPEC_VERSION >
        struct mk_fr_end_of_file_helper< T_FRAME_SPEC_VERSION, VERSION::V6 >
        {
            using retval_type = fr_end_of_file_type< T_FRAME_SPEC_VERSION >;

            static retval_type*
            create( )
            {
              return ( new retval_type );
            };
        };

        template < int T_FRAME_SPEC_VERSION >
        struct mk_fr_end_of_file_helper< T_FRAME_SPEC_VERSION, VERSION::V8 >
        {
            using retval_type = fr_end_of_file_type< T_FRAME_SPEC_VERSION >;

            static retval_type*
            create( )
            {
              return ( new retval_type );
            };
        };

        template < int T_FRAME_SPEC_VERSION >
        typename FR_END_OF_FILE_TYPE< T_FRAME_SPEC_VERSION >::type*
        mk_fr_end_of_file( )
        {
            return ( mk_fr_end_of_file_helper<
                     T_FRAME_SPEC_VERSION,
                     version< T_FRAME_SPEC_VERSION >( ) >::create( ) );
        }

        //======================================
        // F U N C T I O N:  verify_data_types
        //======================================

        template < int T_FRAME_SPEC_VERSION >
        bool
        verify_data_types( )
        {
            return (
                version_info<
                    T_FRAME_SPEC_VERSION,
                    version< version_changes< T_FRAME_SPEC_VERSION >( ) >( ) >::
                    validate_data_types( ) );
        }

        //========================================
        // F U N C T I O N:   verify_constructors
        //========================================

        template < int T_FRAME_SPEC_VERSION >
        bool
        verify_constructors( )
        {
            return ( version_info< T_FRAME_SPEC_VERSION,
                                   version< T_FRAME_SPEC_VERSION >( ) >::
                         validate_constructors( ) );
        }

        //======================================
        // F U N C T I O N:   verify_convert
        //======================================

        // -----
        // -----
        template < int T_FRAME_SPEC_VERSION, CONVERT T_CONVERSION >
        struct verify_convert_helper;

        // -----
        /// Specialization to handle T_FRAME_SPEC_VERSION 3 as this is the root
        /// of the frame specification
        // -----
        template < int T_FRAME_SPEC_VERSION >
        struct verify_convert_helper< T_FRAME_SPEC_VERSION, CONVERT::ROOT >
        {
            static bool
            check( )
            {
                return ( convert_check_root< T_FRAME_SPEC_VERSION >(
                    mk_fr_end_of_file< T_FRAME_SPEC_VERSION > ) );
            }
        };

        // -----
        /// Specialization to handle no conversion as the base and derived
        /// versions are the same
        // -----
        template < int T_FRAME_SPEC_VERSION >
        struct verify_convert_helper< T_FRAME_SPEC_VERSION, CONVERT::SAME >
        {
            static bool
            check( )
            {
                return ( convert_check_same< T_FRAME_SPEC_VERSION >(
                    mk_fr_end_of_file< T_FRAME_SPEC_VERSION >,
                    mk_fr_end_of_file< previous_version(
                        T_FRAME_SPEC_VERSION ) > ) );
            }
        };

        // -----
        /// Specialization to handle no conversion as the base and derived
        /// versions are the same
        // -----
        template < int T_FRAME_SPEC_VERSION, CONVERT T_CONVERSION >
        struct verify_convert_helper
        {
            static bool
            check( )
            {
                constexpr INT_4U current{ T_FRAME_SPEC_VERSION };
                constexpr auto   previous = previous_version( current );

                bool retval{ true };

                std::cerr << "DEBUG: convert: current: " << current << " previous: " << previous << std::endl;
                // -----
                // Down convert
                // -----
                try
                {
                    // With no conversion, the object returned should be at the
                    // same address
                  std::cerr << "DEBUG: Created FrEndOfFile source object" << std::endl;
                    object_type< T_FRAME_SPEC_VERSION > source_obj{
                        mk_fr_end_of_file< current >( )
                    };
                    auto derived_obj = source_obj->DemoteObject(
                        previous, source_obj, nullptr );
                    retval = retval &&
                        version_info< T_FRAME_SPEC_VERSION,
                                      frame_spec_to_version_mapper<
                                          convert_to_frame_spec_mapper<
                                              T_CONVERSION >( ) >( ) >::
                            compare_down_convert( source_obj, derived_obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                // -----
                // Up convert
                // -----
                try
                {
                    // With no conversion, the object returned should be at the
                    // same address
                    object_type< T_FRAME_SPEC_VERSION > source_obj{
                        mk_fr_end_of_file< previous >( )
                    };
                    auto derived_obj = source_obj->PromoteObject(
                        current, previous, source_obj, nullptr );
                    retval = retval &&
                        version_info< T_FRAME_SPEC_VERSION,
                                      frame_spec_to_version_mapper<
                                          convert_to_frame_spec_mapper<
                                              T_CONVERSION >( ) >( ) >::
                            compare_up_convert( source_obj, derived_obj );
                }
                catch ( ... )
                {
                    retval = false;
                }

                return ( retval );
            }
        };

        template < int T_FRAME_SPEC_VERSION >
        bool
        verify_convert( )
        {
            return ( verify_convert_helper<
                     T_FRAME_SPEC_VERSION,
                     conversion<
                         T_FRAME_SPEC_VERSION,
                         is_root< T_FRAME_SPEC_VERSION >( ),
                         is_change_version< T_FRAME_SPEC_VERSION >( ) >( ) >::
                         check( ) );
        }

        //======================================
        // F U N C T I O N:   do_standard_tests
        //======================================
        template < int FrameSpecT >
        void
        do_standard_tests( )
        {
            BOOST_TEST_MESSAGE( "Checking basic correctness of frame structure "
                                "FrEndOfFile at version "
                                << FrameSpecT );
            BOOST_CHECK( verify_data_types< FrameSpecT >( ) );
            BOOST_CHECK( verify_constructors< FrameSpecT >( ) );
            /* BOOST_CHECK( verify_convert< FrameSpecT >( ) ); */
        }
    } // namespace fr_end_of_file
} // namespace testing

void
fr_end_of_file( )
{
    testing::fr_end_of_file::do_standard_tests< 3 >( );
    testing::fr_end_of_file::do_standard_tests< 4 >( );
    testing::fr_end_of_file::do_standard_tests< 6 >( );
    testing::fr_end_of_file::do_standard_tests< 7 >( );
    testing::fr_end_of_file::do_standard_tests< 8 >( );
}

#endif /* fr_end_of_file_hh */
