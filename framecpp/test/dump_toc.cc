//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#if HAVE_CONFIG_H
#include <framecpp_config.h>
#endif /* HAVE_CONFIG_H */

#include <exception>
#include <string.h>
#include <unistd.h>

#include <iostream>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"

#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameBuffer.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/FrTOC.hh"

#include "framecpp/Version4/FrTOC.hh"
#include "framecpp/Version6/FrTOC.hh"
#include "framecpp/Version7/FrTOC.hh"
#include "framecpp/Version8/FrTOC.hh"
#include "framecpp/Version9/FrTOC.hh"

#include "framecpp/FrameCPP.hh"

#include "toc.hh"

using LDASTools::AL::MemChecker;

using namespace FrameCPP::Common;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;

using std::filebuf;

inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

//-----------------------------------------------------------------------
/// \brief Class to handle command line options for this application
//-----------------------------------------------------------------------
class CommandLine : protected CommandLineOptions
{
public:
    typedef std::list< std::string > channel_container_type;

    CommandLine( int ArgC, char** ArgV );

    inline bool
    BadOption( ) const
    {
        bool retval = false;

        for ( const_iterator cur = begin( ), last = end( ); cur != last; ++cur )
        {
            if ( ( *cur )[ 0 ] == '-' )
            {
                std::cerr << "ABORT: Bad option: " << *cur << std::endl;
                retval = true;
            }
        }
        return retval;
    }

    inline void
    Usage( int ExitValue ) const
    {
        std::cout << "Usage: " << ProgramName( ) << m_options << std::endl;
        depart( ExitValue );
    }

    using CommandLineOptions::empty;
    using CommandLineOptions::Pop;
    using CommandLineOptions::size;

private:
    enum option_types
    {
        OPT_HELP
    };

    OptionSet m_options;
};

CommandLine::CommandLine( int ArgC, char** ArgV )
    : CommandLineOptions( ArgC, ArgV )
{
    m_options.Synopsis( "[options] <file> [<file> ...]" );

    m_options.Summary( "This command will dump the table of contents for each"
                       " file specified on the command line." );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.Add(
        Option( OPT_HELP, "help", Option::ARG_NONE, "Display this message" ) );
    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------

    try
    {
        std::string arg_name;
        std::string arg_value;
        bool        parsing = true;

        while ( parsing )
        {
            const int cmd_id( Parse( m_options, arg_name, arg_value ) );

            switch ( cmd_id )
            {
            case CommandLineOptions::OPT_END_OF_OPTIONS:
                parsing = false;
                break;
            case OPT_HELP:
            {
                Usage( 0 );
            }
            break;
            }
        }
    }
    catch ( ... )
    {
    }
}

int
main( int ArgC, char* ArgV[] )
try
{
    MemChecker::Trigger gc_trigger( true );
    CommandLine         cl( ArgC, ArgV );

    if ( cl.empty( ) || cl.BadOption( ) )
    {
        cl.Usage( 1 );
    }

    //---------------------------------------------------------------------
    // Process files
    //---------------------------------------------------------------------
    FrameCPP::Initialize( );

    while ( cl.empty( ) == false )
    {
        std::string filename( cl.Pop( ) );

        try
        {
            //-------------------------------------------------------------------
            // Creation of the frame structure by reading of frame file
            //-------------------------------------------------------------------
            FrameBuffer< filebuf >* ibuf(
                new FrameBuffer< filebuf >( std::ios::in ) );

            ibuf->open( filename.c_str( ), std::ios::in | std::ios::binary );

            IFrameStream ifs( ibuf, 0 );

            std::cout << "Frame file: " << filename << std::endl
                      << "Frame Specification: " << ifs.Version( ) << std::endl;
            const FrTOC* toc( ifs.GetTOC( ) );

            dump( toc );
        }
        catch ( const std::exception& Error )
        {
            std::cerr << "Error reading: " << filename
                      << " exception: " << Error.what( ) << std::endl;
        }
    }
    //---------------------------------------------------------------------
    // Exit
    //---------------------------------------------------------------------
    depart( 0 );
}
catch ( ... )
{
    depart( 1 );
}
