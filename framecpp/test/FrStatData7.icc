//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAMECPP__TESTS__FR_STAT_DATA_7_ICC
#define FRAMECPP__TESTS__FR_STAT_DATA_7_ICC

#undef V_
#define V_ 7

#undef USING_NS_
#define USING_NS_( ) USING_NS(7)

#include "framecpp/Version7/FrameH.hh"
#include "framecpp/Version7/FrDetector.hh"
#include "framecpp/Version7/FrStatData.hh"

template<> create_frame_ret_type
create_frame< V_ >( std::string& Filename )
{
  USING_NS_();

  boost::shared_ptr< FrameH >
    frame( new FrameH( "FrStatDataSampleFrame",
		       -1,
		       0,
		       GTime,
		       0,
		       16.0 ) );

  {
    boost::shared_ptr< FrDetector >	detect;

    detect.reset( new FrDetector( "d1",	// name
				  "d1",	// prefix
				  0.0,	// longitude
				  0.0,	// latitude
				  0.0,	// elevation
				  0.0,	// armXazimuth
				  0.0,	// armYazimuth
				  0.0,	// armXaltitude
				  0.0,	// armYaltitude
				  0.0,	// armXmidpoint
				  0.0,	// armYmidpoint
				  0 	// localTime
				  ) );

    frame->RefDetectProc( ).append( detect );

    detect.reset( new FrDetector( "d2",	// name
				  "d2",	// prefix
				  2.0,	// longitude
				  2.0,	// latitude
				  2.0,	// elevation
				  2.0,	// armXazimuth
				  2.0,	// armYazimuth
				  2.0,	// armXaltitude
				  2.0,	// armYaltitude
				  2.0,	// armXmidpoint
				  2.0,	// armYmidpoint
				  2 	// localTime
				  ) );

    frame->RefDetectProc( ).append( detect );
  }

  //-------------------------------------------------------------------
  // Create StatData information
  //-------------------------------------------------------------------
  typedef stat_data_container_type< FrStatData >
    stat_data_container_type;

  stat_data_container_type::value_type	sdp;
  stat_data_container_type 		stat_data;

  sdp.reset( new FrStatData( "d1:SimpleStat", 	// name
			     "This is simple",	// comment
			     "No repr.",	// representation
			     GTime.GetSeconds( ),// timeStart
			     GTime.GetSeconds( ) + 999, // timeEnd
			     1 ) );		// version
  sdp->SetDetector( frame->RefDetectProc( )[ 0 ] );
  stat_data.push_back( sdp );

  sdp.reset( new FrStatData( "d2:SimpleStat", 	// name
			     "This is simple",	// comment
			     "No repr.",	// representation
			     GTime.GetSeconds( ),// timeStart
			     GTime.GetSeconds( ) + 999, // timeEnd
			     1 ) );		// version
  sdp->SetDetector( frame->RefDetectProc( )[ 1 ] );
  stat_data.push_back( sdp );

  sdp.reset( new FrStatData( "d1:SimpleStat", 	// name
			     "This is simple",	// comment
			     "No repr.",	// representation
			     GTime.GetSeconds( ) + 1000,// timeStart
			     GTime.GetSeconds( ) + 1999, // timeEnd
			     1 ) );		// version
  sdp->SetDetector( frame->RefDetectProc( )[ 0 ] );
  stat_data.push_back( sdp );

  sdp.reset( new FrStatData( "d2:SimpleStat", 	// name
			     "This is simple",	// comment
			     "No repr.",	// representation
			     GTime.GetSeconds( ) + 1000,// timeStart
			     GTime.GetSeconds( ) + 1999, // timeEnd
			     1 ) );		// version
  sdp->SetDetector( frame->RefDetectProc( )[ 1 ] );
  stat_data.push_back( sdp );

  sdp.reset( new FrStatData( "d2:SimpleStat", 	// name
			     "This is simple",	// comment
			     "No repr.",	// representation
			     GTime.GetSeconds( ) + 1000,// timeStart
			     GTime.GetSeconds( ) + 1999, // timeEnd
			     2 ) );		// version
  sdp->SetDetector( frame->RefDetectProc( )[ 1 ] );
  stat_data.push_back( sdp );

  //-------------------------------------------------------------------
  // Create output file
  //-------------------------------------------------------------------
  std::ostringstream	filename;

  filename << "Z-FrStatDataFrame_" << V_ << "-"
	   << frame->GetGTime( ).GetSeconds( ) 
	   << "-" << frame->GetDt( ) << ".gwf";
  Filename = filename.str( );

  unlink( filename.str( ).c_str( ) );
    
  FrameCPP::Common::FrameBuffer< filebuf >*
    obuf( new FrameCPP::Common::FrameBuffer< filebuf >( std::ios::out ) );
  obuf->open( filename.str( ).c_str( ),
	      std::ios::out | std::ios::binary );

  OFrameStream	ofs( obuf, V_ );
  
  for ( stat_data_container_type::iterator
	  cur = stat_data.begin( ),
	  last = stat_data.end( );
	cur != last;
	++cur )
  {
    ofs.WriteFrStatData( *cur );
  }

  ofs.WriteFrame( frame );

  ofs.Close( );
  obuf->close( );

  std::unique_ptr< filebuf_type >	ibuf( new filebuf_type( std::ios::in ) );

  ibuf->open( Filename.c_str( ),
	      std::ios::in | std::ios::binary );

  create_frame_ret_type
    retval( new IFrameStream( ibuf.release( ), V_ ) );

  return retval;
}

#endif /* FRAMECPP__TESTS__FR_STAT_DATA_7_ICC */
