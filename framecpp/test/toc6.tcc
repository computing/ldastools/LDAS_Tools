//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__TOC_6_TCC
#define FRAME_CPP__TEST__TOC_6_TCC

#include <vector>

#include "framecpp/Version6/FrTOC.hh"

namespace toc_6
{
  namespace toc_previous = toc_4;

  using FrameCPP::Version_6::FrTOC;

  //---------------------------------------------------------------------
  // Helpers for Static data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCStatData::name_type,
		     FrTOC::FrTOCStatData::stat_type > stat_data_type;

  inline void
  stat_name( std::ostream& Stream, const stat_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline void
  stat_detector( std::ostream& Stream, const stat_data_type& Data )
  {
    Stream << Data.second.detector;
  }
  
  inline void
  stat_n_stat_instance( std::ostream& Stream, const stat_data_type& Data )
  {
    Stream << Data.second.stat_instances.size( );
  }
  
  inline void
  stat_t_start( std::ostream& Stream, const FrTOC::FrTOCStatData::stat_instance_type& Data )
  {
    Stream << " " << Data.tStart;
  }
  
  inline void
  stat_t_end(std::ostream& Stream, const FrTOC::FrTOCStatData::stat_instance_type& Data )
  {
    Stream << " " << Data.tEnd;

  }
  
  inline void
  stat_version(  std::ostream& Stream, const FrTOC::FrTOCStatData::stat_instance_type& Data )
  {
    Stream << " " << Data.version;
  }
  
  inline void
  stat_position( std::ostream& Stream, const FrTOC::FrTOCStatData::stat_instance_type& Data )
  {
    Stream << " " << Data.positionStat;
  }
  
  //---------------------------------------------------------------------
  // Helpers for ADC data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCAdcData::name_type,
		     FrTOC::FrTOCAdcData::adc_info_type > adc_data_type;

  inline
  void
  adc_name( std::ostream& Stream, const adc_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline
  void
  adc_channelID( std::ostream& Stream, const adc_data_type& Data )
  {
    Stream << Data.second.m_channelID;
  }

  inline
  void
  adc_groupID( std::ostream& Stream, const adc_data_type& Data )
  {
    Stream << Data.second.m_groupID;
  }
  
  inline
  void adc_position( std::ostream& Stream, const adc_data_type& Data )
  {
    for( std::vector< FrTOC::FrTOCAdcData::position_type >::const_iterator
	   first = Data.second.m_positionADC.begin( ),
	   cur = first,
	   last = Data.second.m_positionADC.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << *cur;
    }
  }
  
  //---------------------------------------------------------------------
  // Helpers for Proc data
  //---------------------------------------------------------------------
  using toc_previous::proc_data_type;
  using toc_previous::proc_name;
  using toc_previous::proc_position;
  
  //---------------------------------------------------------------------
  // Helpers for Sim data
  //---------------------------------------------------------------------
  using toc_previous::sim_data_type;
  using toc_previous::sim_name;
  using toc_previous::sim_position;
  
  //---------------------------------------------------------------------
  // Helpers for Ser data
  //---------------------------------------------------------------------
  using toc_previous::ser_data_type;
  using toc_previous::ser_name;
  using toc_previous::ser_position;

  //---------------------------------------------------------------------
  // Helpers for Summary data
  //---------------------------------------------------------------------
  using toc_previous::summary_data_type;
  using toc_previous::summary_name;
  using toc_previous::summary_position;

  //---------------------------------------------------------------------
  // Helpers for Event data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCEvent::name_type,
		     FrTOC::FrTOCEvent::events_container_type >
  event_data_type;

  inline void
  event_name( std::ostream& Stream, const event_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline void
  event_n_event( std::ostream& Stream, const event_data_type& Data )
  {
    Stream << Data.second.size( );
  }

  inline void
  event_gtime_s( std::ostream& Stream, const event_data_type& Data )
  {
    for( FrTOC::FrTOCEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->GTime.GetSeconds( );
    }
  }

  inline void
  event_gtime_n( std::ostream& Stream, const event_data_type& Data )
  {
    for( FrTOC::FrTOCEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->GTime.GetNanoseconds( );
    }
  }

  inline void
  event_amplitude( std::ostream& Stream, const event_data_type& Data )
  {
    for( FrTOC::FrTOCEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->amplitudeEvent;
    }
  }

  inline void
  event_position( std::ostream& Stream, const event_data_type& Data )
  {
    for( FrTOC::FrTOCEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->positionEvent;
    }
  }

  //---------------------------------------------------------------------
  // Helpers for SimEvent data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCSimEvent::name_type,
		     FrTOC::FrTOCSimEvent::events_container_type >
  sim_event_data_type;

  inline void
  sim_event_name( std::ostream& Stream, const sim_event_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline void
  sim_event_n_event( std::ostream& Stream, const sim_event_data_type& Data )
  {
    Stream << Data.second.size( );
  }

  inline void
  sim_event_gtime_s( std::ostream& Stream, const sim_event_data_type& Data )
  {
    for( FrTOC::FrTOCSimEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->GTime.GetSeconds( );
    }
  }

  inline void
  sim_event_gtime_n( std::ostream& Stream, const sim_event_data_type& Data )
  {
    for( FrTOC::FrTOCSimEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->GTime.GetNanoseconds( );
    }
  }

  inline void
  sim_event_amplitude( std::ostream& Stream, const sim_event_data_type& Data )
  {
    for( FrTOC::FrTOCSimEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->amplitudeSimEvent;
    }
  }

  inline void
  sim_event_position( std::ostream& Stream, const sim_event_data_type& Data )
  {
    for( FrTOC::FrTOCSimEvent::events_container_type::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->positionSimEvent;
    }
  }
}

inline void
dump( const FrameCPP::Version_6::FrTOC* TOC )
{
  //---------------------------------------------------------------------
  // Cut down on typing
  //---------------------------------------------------------------------
  using FrameCPP::Version_6::FrTOC;
  using namespace toc_6;

  //---------------------------------------------------------------------
  // Print the information in a human readable form
  //---------------------------------------------------------------------
  std::cout << "ULeapS: " << TOC->GetULeapS( ) << std::endl
	    << "nFrame: " << TOC->GetNFrame( ) << std::endl
    ;
  dump_toc_array( "dataQuality", TOC->GetDataQuality( ) );
  dump_toc_array( "GTimeS", TOC->GetGTimeS( ) );
  dump_toc_array( "GTimeN", TOC->GetGTimeN( ) );
  dump_toc_array( "dt", TOC->GetDt( ) );
  dump_toc_array( "runs", TOC->GetRuns( ) );
  dump_toc_array( "frame", TOC->GetFrame( ) );
  dump_toc_array( "positionH", TOC->GetPositionH( ) );
  dump_toc_array( "nFirstADC", TOC->GetNFirstADC( ) );
  dump_toc_array( "nFirstSer", TOC->GetNFirstSer( ) );
  dump_toc_array( "nFirstTable", TOC->GetNFirstTable( ) );
  dump_toc_array( "nFirstMsg", TOC->GetNFirstMsg( ) );
  //---------------------------------------------------------------------
  // Sh data
  //---------------------------------------------------------------------
  std::cout << "nSH: " << TOC->GetSHid( ).size( ) << std::endl;
  dump_toc_array( "SHid", TOC->GetSHid( ) );
  dump_toc_array( "SHname", TOC->GetSHname( ) );
  //---------------------------------------------------------------------
  // FrDetector
  //---------------------------------------------------------------------
  std::cout << "nDetector: " << TOC->GetNameDetector( ).size( ) << std::endl;
  dump_toc_array( "nameDetector", TOC->GetNameDetector( ) );
  dump_toc_array( "positionDetector", TOC->GetPositionDetector( ) );
  //---------------------------------------------------------------------
  // FrStatData
  //---------------------------------------------------------------------
  {
    const FrTOC::stat_container_type& stat( TOC->GetStat( ) );
    
    std::cout << "nStatType: " << stat.size( ) << std::endl;
    for ( FrTOC::stat_container_type::const_iterator
	    cur = stat.begin( ),
	    last = stat.end( );
	  cur != last;
	  ++cur )
    {
      std::cout << "nameStat: " << cur->first << std::endl;
      std::cout << "detector: " << cur->second.detector << std::endl;
      std::cout <<  "nStatInstance: " << cur->second.stat_instances.size( ) << std::endl;
      dump_toc_internal_data( "tStart", cur->second.stat_instances, &stat_t_start );
      dump_toc_internal_data( "tEnd", cur->second.stat_instances, &stat_t_end );
      dump_toc_internal_data( "version", cur->second.stat_instances, &stat_version );
      dump_toc_internal_data( "positionStat", cur->second.stat_instances, &stat_position );
    }
  }
  //---------------------------------------------------------------------
  // FrAdcData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapADC_type& adc( TOC->GetADC( ) );

    std::cout << "nADC: " << adc.size( ) << std::endl;
    dump_toc_internal_data( "name", adc, &adc_name );
    dump_toc_internal_data( "channelId", adc, &adc_channelID );
    dump_toc_internal_data( "groupId", adc, &adc_groupID );
    dump_toc_internal_data( "positionADC", adc, &adc_position );
  }
  //---------------------------------------------------------------------
  // FrProcData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapProc_type& data( TOC->GetProc( ) );

    std::cout << "nProc: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameProc", data, &proc_name );
    dump_toc_internal_data( "positionProc", data, &proc_position );
  }
  //---------------------------------------------------------------------
  // FrSimData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSim_type& data( TOC->GetSim( ) );

    std::cout << "nSim: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSim", data, &sim_name );
    dump_toc_internal_data( "positionSim", data, &sim_position );
  }
  //---------------------------------------------------------------------
  // FrSerData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSer_type& data( TOC->GetSer( ) );

    std::cout << "nSer: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSer", data, &ser_name );
    dump_toc_internal_data( "positionSer", data, &ser_position );
  }
  //---------------------------------------------------------------------
  // FrSummary
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSummary_type& data( TOC->GetSummary( ) );

    std::cout << "nSummary: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSum", data, &summary_name );
    dump_toc_internal_data( "positionSum", data, &summary_position );
  }
  //---------------------------------------------------------------------
  // FrEvent
  //---------------------------------------------------------------------
  {
    const FrTOC::nameEvent_container_type& data( TOC->GetEvent( ) );

    std::cout << "nEventType: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameEvent", data, &event_name );
    dump_toc_internal_data( "nEvent", data, &event_n_event );
    dump_toc_internal_data( "GTimeSEvent", data, &event_gtime_s );
    dump_toc_internal_data( "GTimeNEvent", data, &event_gtime_n );
    dump_toc_internal_data( "amplitudeEvent", data, &event_amplitude );
    dump_toc_internal_data( "positionEvent", data, &event_position );
  }
  //---------------------------------------------------------------------
  // FrSimEvent
  //---------------------------------------------------------------------
  {
    const FrTOC::nameSimEvent_container_type& data( TOC->GetSimEvent( ) );

    std::cout << "nSimEventType: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSimEvent", data, &sim_event_name );
    dump_toc_internal_data( "nSimEvent", data, &sim_event_n_event );
    dump_toc_internal_data( "GTimeSSimEvent", data, &sim_event_gtime_s );
    dump_toc_internal_data( "GTimeNSimEvent", data, &sim_event_gtime_n );
    dump_toc_internal_data( "amplitudeSimEvent", data, &sim_event_amplitude );
    dump_toc_internal_data( "positionSimEvent", data, &sim_event_position );
  }
}

#endif /* FRAME_CPP__TEST__TOC_6_TCC */
