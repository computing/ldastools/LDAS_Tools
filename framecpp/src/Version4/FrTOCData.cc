//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version4/FrameSpec.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"
#include "framecpp/Version4/FrTOC.hh"

#include "framecpp/Version4/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::TOCInfo;

namespace FrameCPP
{
    namespace Version_4
    {
        //===================================================================
        // FrTOCData
        //===================================================================
        FrTOCData::FrTOCData( Common::IStream& Stream )
        {
            load( Stream );
        }

        void
        FrTOCData::load( Common::IStream& Stream )
        {
            nframe_type nframe;

            Stream >> m_ULeapS >> m_localTime >> nframe;
            if ( nframe )
            {
                m_GTimeS.resize( nframe );
                m_GTimeN.resize( nframe );
                m_dt.resize( nframe );
                m_runs.resize( nframe );
                m_frame.resize( nframe );
                m_positionH.resize( nframe );
                m_nFirstADC.resize( nframe );
                m_nFirstSer.resize( nframe );
                m_nFirstTable.resize( nframe );
                m_nFirstMsg.resize( nframe );
                Stream >> m_GTimeS >> m_GTimeN >> m_dt >> m_runs >> m_frame >>
                    m_positionH >> m_nFirstADC >> m_nFirstSer >>
                    m_nFirstTable >> m_nFirstMsg;
            }
            nsh_type nsh;
            Stream >> nsh;
            if ( nsh > 0 )
            {
                m_SHid.resize( nsh );
                m_SHname.resize( nsh );
                Stream >> m_SHid >> m_SHname;
            }
        }

        void
        FrTOCData::Description( Common::Description& Desc )
        {
            Desc( FrSE(
                "ULeapS", "INT_2S", "From the first FrameH in the file" ) );
            Desc( FrSE(
                "localTime", "INT_4S", "From the first FrameH in the file" ) );
            //-----------------------------------------------------------------
            Desc( FrSE( "nFrame", "INT_4U", "Number of frames in the file" ) );
            Desc( FrSE( "GTimeS",
                        "*INT_4U",
                        "Array of integer GPS frae times (size of nFrames" ) );
            Desc(
                FrSE( "GTimeN",
                      "*INT_4U",
                      "Array of integer GPS residual nanoseconds for the frame"
                      " (size of nFrame" ) );
            Desc( FrSE(
                "dt",
                "*REAL_8",
                "Array of frame durations in seconds( size of nFrames)" ) );
            Desc( FrSE(
                "runs", "*INT_4S", "Array of run numbers (size of nFrame)" ) );
            Desc( FrSE( "frame",
                        "*INT_4U",
                        "Array of frame numbers (size of nFrame)" ) );
            Desc( FrSE( "positionH",
                        "*INT_8U",
                        "Array of FrameH positions, in bytes, from the"
                        " bginning of file (size of nFrame)" ) );
            Desc( FrSE( "nFirstADC",
                        "*INT_8U",
                        "Array of FrADCData positions, in bytes, from the"
                        " bginning of file (size of nFrame)" ) );
            Desc( FrSE( "nFirstSer",
                        "*INT_8U",
                        "Array of FrSerData positions, in bytes, from the"
                        " bginning of file (size of nFrame)" ) );
            Desc( FrSE( "nFirstTable",
                        "*INT_8U",
                        "Array of FrTable positions, in bytes, from the"
                        " bginning of file (size of nFrame)" ) );
            Desc( FrSE( "nFirstMsg",
                        "*INT_8U",
                        "Array of FrMsg positions, in bytes, from the"
                        " bginning of file (size of nFrame)" ) );
            //-----------------------------------------------------------------
            Desc( FrSE(
                "nSH", "INT_4U", "Number of FrSH structures in the file" ) );
            Desc(
                FrSE( "SHid", "*INT_2U", "Array of FrSH IDs (size of nSH)" ) );
            Desc( FrSE(
                "SHname", "*STRING", "Array of FrSH names (size of nSH)" ) );
        }

        //-------------------------------------------------------------------
        /// This method allows for iterting over each element of information
        /// and allows the caller to gather information about each element.
        //-------------------------------------------------------------------
        void
        FrTOCData::forEach( Common::FrTOC::query_info_type Info,
                            Common::FrTOC::FunctionBase&   Action ) const
        {
            switch ( Info )
            {
            case Common::FrTOC::TOC_FR_STRUCTS:
            {
                try
                {
                    Common::FrTOC::FunctionSI& action(
                        dynamic_cast< Common::FrTOC::FunctionSI& >( Action ) );
                    shid_container_type::const_iterator cur_id =
                                                            GetSHid( ).begin( ),
                                                        last_id =
                                                            GetSHid( ).end( );
                    shname_container_type::const_iterator
                        cur_name = GetSHname( ).begin( ),
                        last_name = GetSHname( ).end( );
                    while ( ( cur_id != last_id ) && ( cur_name != last_name ) )
                    {
                        action( *cur_name, *cur_id );
                        ++cur_name;
                        ++cur_id;
                    }
                }
                catch ( ... )
                {
                }
            }
            break;
            default:
                //---------------------------------------------------------------
                // ignore all other requests
                //---------------------------------------------------------------
                break;
            }
        }

        void
        FrTOCData::write( Common::OStream& Stream ) const
        {
            Stream << m_ULeapS << m_localTime << nframe_type( m_frame.size( ) )
                   << m_GTimeS << m_GTimeN << m_dt << m_runs << m_frame
                   << m_positionH << m_nFirstADC << m_nFirstSer << m_nFirstTable
                   << m_nFirstMsg << nsh_type( m_SHid.size( ) ) << m_SHid
                   << m_SHname;
        }

    } // namespace Version_4
} // namespace FrameCPP
