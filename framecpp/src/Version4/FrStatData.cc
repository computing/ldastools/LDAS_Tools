//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <algorithm>

#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version4/FrStatData.hh"
#include "framecpp/Version4/FrDetector.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"
#include "framecpp/Version4/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_4
    {
        //=======================================================================
        // FrStatData::Query
        //=======================================================================
#if WORKING
        FrStatData::Query::~Query( )
        {
            for ( data_type::iterator cur = m_fr_stat_data.begin( ),
                                      last = m_fr_stat_data.end( );
                  cur != last;
                  ++cur )
            {
                if ( ( *cur ).s_own )
                {
                    delete ( *cur ).s_fr_stat_data;
                    ( *cur ).s_fr_stat_data = (FrStatData*)NULL;
                }
            }
        }

        void
        FrStatData::Query::Add( const std::string&            NamePattern,
                                const std::string&            DetectorPattern,
                                const LDASTools::AL::GPSTime& StartTime,
                                const LDASTools::AL::GPSTime& EndTime,
                                const INT_4U                  Version )
        {
            //---------------------------------------------------------------------
            /// \todo
            ///    Verify mode to be table of contents
            //---------------------------------------------------------------------
            const FrTOC* toc(
                dynamic_cast< const FrTOC* >( m_stream->GetTOC( ) ) );
            //---------------------------------------------------------------------
            // Establish regex patterns
            //---------------------------------------------------------------------
            Regex                          name_regex( NamePattern );
            Regex                          detector_regex( DetectorPattern );
            const FrTOC::MapStatType_type& frStatData( toc->GetStatType( ) );

            RegexMatch name_match;
            RegexMatch detector_match;

            //---------------------------------------------------------------------
            // Loop over all FrStatData structures looking for matches
            //---------------------------------------------------------------------
            for ( FrTOC::MapStatType_type::const_iterator
                      cur = frStatData.begin( ),
                      last = frStatData.end( );
                  cur != last;
                  ++cur )
            {
                if ( // Verify name match
                    name_match.match( name_regex,
                                      ( *cur ).first.s_name.c_str( ) ) &&
                    // Verify detector match
                    detector_match.match( detector_regex,
                                          ( *cur ).first.s_detector.c_str( ) ) )
                {
                    //-----------------------------------------------------------------
                    // Add the FrStatData to the results along with any relevant
                    //   detector information.
                    //-----------------------------------------------------------------
                    add( ( *cur ).first,
                         ( *cur ).second,
                         // Round down
                         StartTime.GetSeconds( ),
                         // Round up
                         ( ( EndTime.GetNanoseconds( ) > 0 )
                               ? ( EndTime.GetSeconds( ) + 1 )
                               : ( EndTime.GetSeconds( ) ) ),
                         Version );
                }
            }
            //---------------------------------------------------------------------
            // Read into memory anything that has been requested
            //---------------------------------------------------------------------
            load( );
        }

        void
        FrStatData::Query::Reset( istream_type& Stream )
        {
            m_fr_stat_data.resize( 0 );
            m_stream = &Stream;
            m_dirty = false;
        }

        FrStatData* FrStatData::Query::operator[]( INT_4U Index )
        {
            if ( Index >= size( ) )
            {
                throw std::range_error(
                    "Index out of range for FrStatData::Query" );
            }
            m_fr_stat_data[ Index ].s_own = false;
            return m_fr_stat_data[ Index ].s_fr_stat_data;
        }

        void
        FrStatData::Query::add( const FrTOC::StatTypeKey&   Key,
                                const FrTOC::StatType_type& Data,
                                INT_4U                      Start,
                                INT_4U                      Stop,
                                INT_4U                      Version )
        {
            for ( INT_4U x = 0, last = Data.tStart.size( ); x < last; ++x )
            {
                if ( // Verify Time range match
                    ( ( ( Start == 0 ) && ( Stop == 0 ) ) ||
                      ( ( Start >= Data.tStart[ x ] ) &&
                        ( Start < Data.tEnd[ x ] ) ) ||
                      ( ( Stop >= Data.tStart[ x ] ) &&
                        ( Stop < Data.tEnd[ x ] ) ) ||
                      ( ( Start < Data.tStart[ x ] ) &&
                        ( Stop >= Data.tStart[ x ] ) ) ) &&
                    // Verify Version
                    ( ( Version == FrStatData::ALL_VERSIONS ) ||
                      ( Version == FrStatData::LATEST_VERSION ) ||
                      ( Version == Data.version[ x ] ) ) )
                {
                    if ( Version == FrStatData::LATEST_VERSION )
                    {
                        bool found = false;
                        for ( data_type::iterator
                                  cur_data = m_fr_stat_data.begin( ),
                                  last_data = m_fr_stat_data.end( );
                              cur_data != last_data;
                              ++cur_data )
                        {
                            if ( ( ( *cur_data ).s_nameStat == Key.s_name ) &&
                                 ( ( *cur_data ).s_detector ==
                                   Key.s_detector ) &&
                                 ( ( *cur_data ).s_tStart ==
                                   Data.tStart[ x ] ) &&
                                 ( ( *cur_data ).s_tEnd == Data.tEnd[ x ] ) )
                            {
                                if ( ( *cur_data ).s_version <
                                     Data.version[ x ] )
                                {
                                    ( *cur_data ).s_fr_stat_pos =
                                        Data.positionStat[ x ];
                                    if ( ( *cur_data ).s_own &&
                                         ( *cur_data ).s_fr_stat_data )
                                    {
                                        delete ( *cur_data ).s_fr_stat_data;
                                        m_fr_stat_data.back( ).s_fr_stat_data =
                                            (FrStatData*)NULL;
                                    }
                                    ( *cur_data ).s_version = Data.version[ x ];
                                    m_dirty = true;
                                }
                                found = true;
                                break;
                            }
                        }
                        if ( found == false )
                        {
                            m_dirty = true;
                            m_fr_stat_data.push_back( query_info_type( ) );
                            m_fr_stat_data.back( ).s_fr_stat_pos =
                                Data.positionStat[ x ];
                            m_fr_stat_data.back( ).s_fr_stat_data =
                                (FrStatData*)NULL;
                            m_fr_stat_data.back( ).s_own = true;
                            m_fr_stat_data.back( ).s_nameStat = Key.s_name;
                            m_fr_stat_data.back( ).s_detector = Key.s_detector;
                            m_fr_stat_data.back( ).s_tStart = Data.tStart[ x ];
                            m_fr_stat_data.back( ).s_tEnd = Data.tEnd[ x ];
                            m_fr_stat_data.back( ).s_version =
                                Data.version[ x ];
                        }
                    }
                    else
                    {
                        m_dirty = true;
                        m_fr_stat_data.push_back( query_info_type( ) );
                        m_fr_stat_data.back( ).s_fr_stat_pos =
                            Data.positionStat[ x ];
                        m_fr_stat_data.back( ).s_fr_stat_data =
                            (FrStatData*)NULL;
                        m_fr_stat_data.back( ).s_own = true;
                        m_fr_stat_data.back( ).s_nameStat = Key.s_name;
                        m_fr_stat_data.back( ).s_detector = Key.s_detector;
                        m_fr_stat_data.back( ).s_tStart = Data.tStart[ x ];
                        m_fr_stat_data.back( ).s_tEnd = Data.tEnd[ x ];
                        m_fr_stat_data.back( ).s_version = Data.version[ x ];
                    }
                }
            }
        }

        void
        FrStatData::Query::load( )
        {
            if ( m_dirty )
            {
                // Sort the list by file position in ascending order
                std::sort( m_fr_stat_data.begin( ),
                           m_fr_stat_data.end( ),
                           CompareFilePosition( ) );

                // Loop over list loading in those that have not yet been read.
                for ( data_type::iterator cur = m_fr_stat_data.begin( ),
                                          last = m_fr_stat_data.end( );
                      cur != last;
                      ++cur )
                {
                    if ( ( *cur ).s_fr_stat_data == (FrStatData*)NULL )
                    {
                        std::unique_ptr< FrStatData > stat_data =
                            m_stream->ReadFrStatData( ( *cur ).s_fr_stat_pos );
                        ( *cur ).s_fr_stat_data = stat_data.release( );
                        ( *cur ).s_own = true;
                    }
                }
                m_dirty = false;
            }
        }
#endif /* WORKING */

        //=======================================================================
        // FrStatData
        //=======================================================================

        const INT_4U FrStatData::ALL_VERSIONS = 0xFFFFFFFF;
        const INT_4U FrStatData::LATEST_VERSION = 0xFFFFFFFE;

        FrStatData::FrStatData( )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrStatData::FrStatData( const FrStatData& Source )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION ),
              FrStatDataNPS( Source ),
              FrStatDataPS( Source ), Common::TOCInfo( Source )
        {
        }

        FrStatData::FrStatData( const std::string& name,
                                const std::string& comment,
                                const std::string& representation,
                                const INT_4U       timeStart,
                                const INT_4U       timeEnd,
                                const INT_4U       version )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION ),
              FrStatDataNPS(
                  name, comment, representation, timeStart, timeEnd, version )
        {
        }

        FrStatData::FrStatData( const Previous::FrStatData& Source,
                                stream_base_type*           Stream )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data(
                static_cast< const Previous::FrStatDataStorage& >( Source ) );

            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-----------------------------------------------------------------
                const INT_2U max_ref = Previous::FrStatData::MAX_REF;
#if WORKING
                istream->ReplacePtr(
                    (FrameSpec::Object**)( AddressOfDetector( ) ),
                    (const FrameSpec::Object**)( Source.AddressOfDetector( ) ),
                    max_ref );
#endif /* WORKING */
                istream->ReplaceRef( RefData( ), Source.RefData( ), max_ref );
            }
        }

        FrStatData::FrStatData( istream_type& Stream )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );
        }

        FrameCPP::cmn_streamsize_type
        FrStatData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( ) + m_refs.Bytes( Stream );
        }

        FrStatData*
        FrStatData::Clone( ) const
        {
            return new FrStatData( *this );
        }

        FrStatData*
        FrStatData::Create( istream_type& Stream ) const
        {
            return new FrStatData( Stream );
        }

        const char*
        FrStatData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrStatData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrStatData::StructName( ),
                           FrStatData::STRUCT_ID,
                           "Static Data Structure" ) );

                FrStatDataNPS::storage_type::Describe< FrSE >( ret );
                ref_type::Describe< FrSE >( ret );
            }

            return &ret;
        }

        void
        FrStatData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
        }

        void
        FrStatData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    case TOCInfo::IC_DETECTOR:
                    {
                        boost::shared_ptr< FrDetector > detector(
                            boost::dynamic_pointer_cast< FrDetector >(
                                GetDetector( ) ) );
                        if ( detector )
                        {
                            *data = detector->GetName( );
                        }
                        else
                        {
                            *data = "";
                        }
                    }
                    break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_START:
                        *data = GetTimeStart( );
                        break;
                    case TOCInfo::IC_END:
                        *data = GetTimeEnd( );
                        break;
                    case TOCInfo::IC_VERSION:
                        *data = GetVersion( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrStatData&
        FrStatData::Merge( const FrStatData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrStatData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrStatData::demote_ret_type
        FrStatData::demote( INT_2U              Target,
                            demote_arg_type     Obj,
                            demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                demote_arg_type retval(
                    new Previous::FrStatData( GetName( ),
                                              GetComment( ),
                                              GetTimeStart( ),
                                              GetTimeEnd( ),
                                              GetVersion( ) ) );

                auto istream = FrameCPP::Common::IsIStream( Stream );

                if ( istream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
#if WORKING
                    /* This one remains unused */
                    istream->ReplacePtr(
                        (FrameSpec::Object**)( retval->AddressOfDetector( ) ),
                        (const FrameSpec::Object**)( AddressOfDetector( ) ),
                        MAX_REF );
#endif /* WORKING */
#if WORKING
                    istream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    istream->RemoveResolver( &( RefTable( ) ), MAX_REF );
#else
                    assert( 0 );
#endif
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrStatData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrStatData::promote_ret_type
        FrStatData::promote( INT_2U               Target,
                             promote_arg_type     Obj,
                             promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_4
} // namespace FrameCPP
