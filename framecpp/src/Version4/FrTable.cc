//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version4/FrameSpec.hh"
#include "framecpp/Version4/FrTable.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"
#include "framecpp/Version4/PTR_STRUCT.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_4
    {
        //=======================================================================
        //=======================================================================

        FrTableNPS::data_type::data_type( const data_type& Source )
            : name( Source.name ), comment( Source.comment )
        {
        }

        void
        FrTableNPS::data_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment;
        }

        void
        FrTableNPS::data_type::operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment;
        }

        //=======================================================================
        //=======================================================================

        FrTableNPS::FrTableNPS( )
        {
        }

        FrTableNPS::FrTableNPS( const FrTableNPS& Source )
            : m_data( Source.m_data )
        {
        }

        //=======================================================================
        //=======================================================================
      FrTable::FrTable( ) : Object( CLASS_ID, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrTable::FrTable( const FrTable& Table )
          : Object( CLASS_ID, StructDescription( ), DATA_FORMAT_VERSION ), FrTableNPS( Table ),
              FrTablePS( Table )
        {
        }

        FrTable::FrTable( const std::string& name, INT_4U nrows )
          : Object( CLASS_ID, StructDescription( ), DATA_FORMAT_VERSION )
        {
            setName( name );
        }

        FrTable::FrTable( const std::string&         Name,
                          const std::string&         Comment,
                          nRow_type                  NRows,
                          column_name_container_type ColumnNames )
          : Object( CLASS_ID, StructDescription( ), DATA_FORMAT_VERSION )
        {
            setName( Name );
            AppendComment( Comment );
        }

        FrTable::FrTable( const FrTableNPS& Source )
          : Object( CLASS_ID, StructDescription( ), DATA_FORMAT_VERSION ), FrTableNPS( Source )
        {
        }

        FrTable::FrTable( istream_type& Stream )
          : Object( CLASS_ID, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrTable::~FrTable( )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrTable::Bytes( const Common::StreamBase& Stream ) const
        {
            cmn_streamsize_type retval = m_data.Bytes( Stream ) +
                m_refs.Bytes( Stream ) + Stream.PtrStructBytes( ) // next
                ;
            return retval;
        }

        FrTable*
        FrTable::Create( istream_type& Stream ) const
        {
            return new FrTable( Stream );
        }

        FrTable&
        FrTable::Merge( const FrTable& RHS )
        {
            throw Unimplemented(
                "FrTable& FrTable::Merge( const FrTable& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        FrTable::promote_ret_type
        FrTable::Promote( INT_2U           Source,
                          promote_arg_type Obj,
                          promote_stream_type*    Stream )
        {
          return Common::PromoteObject(DATA_FORMAT_VERSION ,Source ,Obj);
        }

        const char*
        FrTable::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrTable::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( StructName( ), CLASS_ID, "Table Data Structure" ) );

                data_type::Describe< FrSE >( ret );
                refs_type::Describe< FrSE >( ret );

                ret( FrSE( "next", ptr_struct_type::Desc( StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrTable::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );

            WriteNext( Stream );
        }

        const std::string&
        FrTable::GetNameSlow( ) const
        {
            return GetName( );
        }

        bool
        FrTable::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return compare( *this, Obj );
        }

        FrTable&
        FrTable::operator+=( const FrTable& RHS )
        {
            throw Unimplemented(
                "FrTable& FrTable::operator+=( const FrTable& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrTable::demote_ret_type
        FrTable::demote( INT_2U          Target,
                         demote_arg_type Obj,
                         demote_stream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                //-------------------------------------------------------------------
                // Same version
                //-------------------------------------------------------------------
                return Obj;
            }
            else if ( Target < DATA_FORMAT_VERSION )
            {
                //-------------------------------------------------------------------
                // Object does not exist prier to Version 4
                //-------------------------------------------------------------------
                return demote_ret_type( );
            }
            throw Unimplemented( "Object* FrTable::Demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrTable::promote_ret_type
        FrTable::promote( INT_2U           Source,
                          promote_arg_type Obj,
                          promote_stream_type*    Stream ) const
        {
            if ( Source <= DATA_FORMAT_VERSION )
            {
                //-------------------------------------------------------------------
                // Object does not exist prier to Version 4
                //-------------------------------------------------------------------
                return promote_ret_type( );
            }
            throw Unimplemented(
                "Object* FrTable::Promote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }
    } // namespace Version_4
} // namespace FrameCPP
