//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version4/FrameSpec.hh"
#include "framecpp/Version4/FrTOCTrigData.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"
#include "framecpp/Version4/FrTOC.hh"

#include "framecpp/Version4/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::TOCInfo;

namespace FrameCPP
{
    namespace Version_4
    {
        //===================================================================
        //===================================================================
        FrTOCTrigData::FrTOCTrigData( )
        {
        }

        FrTOCTrigData::FrTOCTrigData( Common::IStream& Stream,
                                      INT_4U           FrameCount )
        {
            nTrig_type nTrig;
            Stream >> nTrig;
            if ( nTrig && ( nTrig != ~nTrig_type( 0 ) ) )
            {
                //---------------------------------------------------------------
                // Read in the information
                //---------------------------------------------------------------
                std::vector< name_type >             names( nTrig );
                std::vector< gtimesTrigData_type >   gtimes( nTrig );
                std::vector< gtimenTrigData_type >   gtimen( nTrig );
                std::vector< positionTrigData_type > position( nTrig );

                Stream >> names >> gtimes >> gtimen >> position;

                for ( nTrig_type cur = nTrig_type( 0 ), last = nTrig;
                      cur != last;
                      ++cur )
                {
                    trig_data_type& td = m_info[ names[ cur ] ];
                    td.GTime = GPSTime( gtimes[ cur ], gtimen[ cur ] );
                    td.positionTrigData = position[ cur ];
                }
            }
        }

        void
        FrTOCTrigData::Description( Common::Description& Desc )
        {
            Desc( FrSE( "nTrigDataType",
                        "INT_4U",
                        "Number of FrTrigData in the file" ) );
            Desc( FrSE(
                "nameTrigData", "*STRING", "Array of FrTrigData names" ) );
            Desc( FrSE(
                "GTimeSTrigData", "*INT_4U", "GPS time in integer seconds" ) );
            Desc( FrSE( "GTimeNTrigData",
                        "*INT_4U",
                        "Residual GPS time in integer nanoseconds" ) );
            Desc( FrSE( "positionTrigData",
                        "*INT_8U",
                        "Array of FrTrigData positions, in bytes,"
                        " from beginning of file" ) );
        }

        void
        FrTOCTrigData::QueryTrigData( const Common::TOCInfo& Info,
                                      INT_4U                 FrameOffset,
                                      INT_8U                 Position )
        {
            STRING name;
            INT_4U sec;
            INT_4U nsec;

            Info.TOCQuery( TOCInfo::IC_NAME,
                           TOCInfo::DataType( name ),
                           &name,
                           TOCInfo::IC_GTIME_S,
                           TOCInfo::DataType( sec ),
                           &sec,
                           TOCInfo::IC_GTIME_N,
                           TOCInfo::DataType( nsec ),
                           &nsec,
                           TOCInfo::IC_EOQ );

            trig_data_type& i( m_info[ name ] );

            i.GTime = GPSTime( sec, nsec );
            i.positionTrigData = Position;
        }

        void
        FrTOCTrigData::write( Common::OStream& Stream ) const
        {
            //-----------------------------------------------------------------
            // Flatten data so it is streamable
            //-----------------------------------------------------------------
            if ( m_info.size( ) > 0 )
            {
                nTrig_type nTrig = nTrig_type( m_info.size( ) );

                std::vector< name_type >             names( nTrig );
                std::vector< gtimesTrigData_type >   gtimes( nTrig );
                std::vector< gtimenTrigData_type >   gtimen( nTrig );
                std::vector< positionTrigData_type > position( nTrig );

                nTrig_type x( 0 );

                for ( MapTrig_type::const_iterator cur = m_info.begin( ),
                                                   last = m_info.end( );
                      cur != last;
                      ++cur, ++x )
                {
                    names[ x ] = cur->first;
                    gtimes[ x ] = cur->second.GTime.GetSeconds( );
                    gtimen[ x ] = cur->second.GTime.GetNanoseconds( );
                    position[ x ] = cur->second.positionTrigData;
                }

                //---------------------------------------------------------------
                // Write information
                //---------------------------------------------------------------
                Stream << nTrig << names << gtimes << gtimen << position;
            }
            else
            {
                Stream << ~nTrig_type( 0 );
            }
        }

    } // namespace Version_4
} // namespace FrameCPP
