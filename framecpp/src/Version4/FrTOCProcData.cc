//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version4/FrameSpec.hh"
#include "framecpp/Version4/FrTOCProcData.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"
#include "framecpp/Version4/FrTOC.hh"

#include "framecpp/Version4/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::TOCInfo;

namespace FrameCPP
{
    namespace Version_4
    {
        //===================================================================
        //===================================================================
        void
        FrTOCProcData::Description( Common::Description& Desc )
        {
            Desc( FrSE( "nProc",
                        "INT_4U",
                        "Number of unique FrProcData names in file." ) );
            Desc( FrSE( "nameProc", "*STRING", "Array of FrProcData names" ) );
            Desc(
                FrSE( "positionProc",
                      "*INT_8U",
                      "Array of lists of FrProcData offset positions, in bytes,"
                      " from beginning of file (size of nFrame*nProc)" ) );
        }

        void
        FrTOCProcData::QueryProc( const Common::TOCInfo& Info,
                                  INT_4U                 FrameOffset,
                                  INT_8U                 Position )
        {
            STRING name;

            Info.TOCQuery( Common::TOCInfo::IC_NAME,
                           Common::TOCInfo::DT_STRING_2,
                           &name,
                           Common::TOCInfo::IC_EOQ );

            proc_info_type& i( m_info[ name ] );
            i.resize( FrameOffset + 1 );
            i[ FrameOffset ] = Position;
        }

        //-------------------------------------------------------------------
        /// This method allows for iterting over each element of information
        /// and allows the caller to gather information about each element.
        //-------------------------------------------------------------------
        void
        FrTOCProcData::forEach( Common::FrTOC::query_info_type Info,
                                Common::FrTOC::FunctionBase&   Action ) const
        {
            switch ( Info )
            {
            case Common::FrTOC::TOC_CHANNEL_NAMES:
            {
                try
                {
                    Common::FrTOC::FunctionString& action(
                        dynamic_cast< Common::FrTOC::FunctionString& >(
                            Action ) );

                    for ( MapProc_type::const_iterator cur = m_info.begin( ),
                                                       last = m_info.end( );
                          cur != last;
                          ++cur )
                    {
                        action( cur->first );
                    }
                }
                catch ( ... )
                {
                    // Does not understand Action
                }
            }
            break;
            default:
                //---------------------------------------------------------------
                // ignore all other requests
                //---------------------------------------------------------------
                break;
            }
        }

    } // namespace Version_4
} // namespace FrameCPP
