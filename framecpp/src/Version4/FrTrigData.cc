//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version4/FrTrigData.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"

#include "framecpp/Version4/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_4;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::IStream;
using FrameCPP::Common::OStream;

#define LM_DEBUG 0

//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_EVENT;

namespace FrameCPP
{
    namespace Version_4
    {
        //=======================================================================
        // FrTrigDataStorage::data_type
        //=======================================================================
        FrameCPP::cmn_streamsize_type
        FrTrigDataStorage::data_type::Bytes( ) const
        {
            FrameCPP::cmn_streamsize_type retval = name.Bytes( ) +
                comment.Bytes( ) + inputs.Bytes( ) + Common::Bytes( GTime ) +
                sizeof( timeBefore ) + sizeof( timeAfter ) +
                sizeof( triggerStatus ) + sizeof( amplitude ) +
                sizeof( probability ) + statistics.Bytes( );

            return retval;
        }

        void
        FrTrigDataStorage::data_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment >> inputs >> GTime >> timeBefore >>
                timeAfter >> triggerStatus >> amplitude >> probability >>
                statistics;
        }

        void
        FrTrigDataStorage::data_type::
        operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment << inputs << GTime << timeBefore
                   << timeAfter << triggerStatus << amplitude << probability
                   << statistics;
        }

        //=======================================================================
        // FrTrigData
        //=======================================================================
        FrTrigData::FrTrigData( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrTrigData::FrTrigData( const FrTrigData& Source )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              FrTrigDataStorage( Source ),
              FrTrigDataRefs( Source ), Common::TOCInfo( Source )
        {
        }

        FrTrigData::FrTrigData( const std::string& name,
                                const std::string& comment,
                                const std::string& inputs,
                                const GPSTime&     time,
                                REAL_4             timeBefore,
                                REAL_4             timeAfter,
                                INT_4U             triggerStatus,
                                REAL_4             amplitude,
                                REAL_4             prob,
                                const std::string& statistics )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = name;
            m_data.comment = comment;
            m_data.inputs = inputs;
            m_data.GTime = time;
            m_data.timeBefore = timeBefore;
            m_data.timeAfter = timeAfter;
            m_data.triggerStatus = triggerStatus;
            m_data.amplitude = amplitude;
            m_data.probability = prob;
            m_data.statistics = statistics;
        }

        FrTrigData::FrTrigData( const Previous::FrTrigData& Source,
                                stream_base_type*           Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              FrTrigDataStorage( Source )
        {
            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( istream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                istream->ReplaceRef( RefData( ),
                                     Source.RefData( ),
                                     Previous::FrTrigData::MAX_REF );
            }
        }

        FrTrigData::FrTrigData( istream_type& Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrTrigData*
        FrTrigData::Create( istream_type& Stream ) const
        {
            return new FrTrigData( Stream );
        }

        const std::string&
        FrTrigData::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrTrigData&
        FrTrigData::Merge( const FrTrigData& RHS )
        {
            throw Unimplemented(
                "FrTrigData& FrTrigData::Merge( const FrTrigData& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        const char*
        FrTrigData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrTrigData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrTrigData::StructName( ),
                           s_object_id,
                           "Event Data Structure" ) );

                ret( FrSE( "name", "STRING", "Name of event." ) );
                ret( FrSE( "comment", "STRING", "Descriptor of event" ) );
                ret( FrSE( "inputs",
                           "STRING",
                           "Input channels and filter parameters to event "
                           "process." ) );
                ret( FrSE( "GTimeS",
                           "INT_4U",
                           "GPS time in seconds corresponding to reference "
                           "vale of event,"
                           " as defined by the search algorigthm." ) );
                ret( FrSE(
                    "GTimeN",
                    "INT_4U",
                    "GPS time in residual nanoseconds relative to GTimeS" ) );
                ret(
                    FrSE( "timeBefore",
                          "REAL_4",
                          "Signal duration before (GTimeS.GTimeN)(seconds)" ) );
                ret( FrSE( "timeAfter",
                           "REAL_4",
                           "Signal duration after (GTimeS.GTimeN)(seconds)" ) );
                ret( FrSE( "triggerStatus",
                           "INT_4U",
                           "Defined bby event search algorithm" ) );
                ret( FrSE( "amplitude",
                           "REAL_4",
                           "Continuouis output amplitude returned by event" ) );
                ret( FrSE( "probability",
                           "REAL_4",
                           "Likelihood estimate of event, if available"
                           " (probability = -1 if cannot be estimated)" ) );
                ret( FrSE( "statistics",
                           "STRING",
                           "Statistical description of event, if relevant or "
                           "available" ) );
                ret( FrSE(
                    "data", PTR_STRUCT::Desc( FrVect::StructName( ) ), "" ) );
                ret( FrSE(
                    "table", PTR_STRUCT::Desc( FrTable::StructName( ) ), "" ) );
                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrTrigData::StructName( ) ),
                           "" ) );
            }

            return &ret;
        }

        void
        FrTrigData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_GTIME_S:
                        *data = GetGTime( ).GetSeconds( );
                        break;
                    case TOCInfo::IC_GTIME_N:
                        *data = GetGTime( ).GetNanoseconds( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_4:
                {
                    REAL_4* data = va_arg( vl, REAL_4* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_8:
                {
                    REAL_8* data = va_arg( vl, REAL_8* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        void
        FrTrigData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        bool
        FrTrigData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrTrigData::demote_ret_type
        FrTrigData::demote( INT_2U              Target,
                            demote_arg_type     Obj,
                            demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                demote_ret_type retval(
                    new Previous::FrTrigData( GetName( ),
                                              GetComment( ),
                                              GetInputs( ),
                                              GetGTime( ),
                                              GetTriggerStatus( ),
                                              GetAmplitude( ),
                                              GetProbability( ),
                                              GetStatistics( ) ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
#if WORKING
                    Stream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    Stream->RemoveResolver( &( RefTable( ) ), MAX_REF );
#else
                    assert( 0 );
#endif
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrTrigData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrTrigData::promote_ret_type
        FrTrigData::promote( INT_2U               Target,
                             promote_arg_type     Obj,
                             promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_4
} // namespace FrameCPP
