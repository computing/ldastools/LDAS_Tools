//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_8_FrTOCSimData_HH
#define FrameCPP_VERSION_8_FrTOCSimData_HH

#if !defined( SWIGIMPORTED )
#include "framecpp/Version7/FrTOCSimData.hh"

#include "framecpp/Version8/impl/FrTOCSimDataData.hh"

#include "framecpp/Version8/STRING.hh"
#endif /* ! defined(SWIGIMPORTED) */

#if !defined( SWIGIMPORTED )

namespace FrameCPP
{
#if !defined( SWIGIMPORTED )
    namespace Common
    {
        class TOCInfo;
    }
#endif /* ! defined(SWIGIMPORTED) */

    namespace Version_8
    {
        //===================================================================
        /// \brief Indexed elements of the FrSimData structure.
        //===================================================================
      class FrTOCSimData:
        public virtual FrTOCImpl::FrTOCSimDataData
        {
        public:
            //-----------------------------------------------------------------
            /// \brief Default constructor.
            //-----------------------------------------------------------------
            FrTOCSimData( ) = default;

            //-----------------------------------------------------------------
            /// \brief Number of bytes needed to write this structure
            ///
            /// \param[in] Stream
            ///     The stream from which to the object is being read or
            ///     written.
            ///
            /// \return
            ///     The number of bytes need to read or write this object.
            //-----------------------------------------------------------------
            Common::FrameSpec::size_type
            Bytes( const Common::StreamBase& Stream ) const;

            //-----------------------------------------------------------------
            /// \brief Gather TOC info for FrSimData being written.
            ///
            /// \param[in] Info
            ///     Information
            ///
            /// \param[in] FrameOffset
            ///     The frame offset of the frame being written.
            ///     The frame offsets start at 0 (zero).
            ///
            /// \param[in] Position
            ///     The byte offset from the start of the file
            ///     where the structure is written.
            //-----------------------------------------------------------------
            void QuerySim( const Common::TOCInfo& Info,
                           INT_4U                 FrameOffset,
                           INT_8U                 Position );

#if !defined( SWIG )
            //-----------------------------------------------------------------
            /// \brief asignment operator
            ///
            /// \param[in] Source
            ///     The source to be copied.
            //-----------------------------------------------------------------
            const FrTOCSimData&
            operator=( const Previous::FrTOCSimData& Source );
#endif /* ! defined(SWIG) */

            //-----------------------------------------------------------------
            /// \brief equality operator
            ///
            /// \param[in] RHS
            ///     The FrTOCAdcData object to be compared.
            ///
            /// \return
            ///     The value true is returned if this object is equivelent
            ///     to the RHS instance; false otherwise.
            //-----------------------------------------------------------------
            bool operator==( const FrTOCSimData& RHS ) const;

            //-----------------------------------------------------------------
            /// \brief The description of structure
            ///
            /// \param[out] Desc
            ///     Storage for the description of the structure.
            ///
            /// \return
            ///     A Description object which describes this structure as
            ///     specified by the frame specification.
            //-----------------------------------------------------------------
            template < typename SE >
            static void Description( Common::Description& Desc );

          //-----------------------------------------------------------------
          /// \brief Read contents from stream
          ///
          /// \param[in] Stream
          ///     The stream from which the object is being read.
          ///
          /// \return
          ///    A new instance of this object.
          //-----------------------------------------------------------------
          void Load( istream_type& Stream, INT_4U FrameCount );

        protected:
            //-----------------------------------------------------------------
            /// \brief Iterate over contents.
            ///
            /// \param[in] Info
            ///     Specifies the type of information to be searched.
            ///
            /// \param[in] Action
            ///     Action to be taken for each piece of information found.
            //-----------------------------------------------------------------
            void forEach( Common::FrTOC::query_info_type Info,
                          Common::FrTOC::FunctionBase&   Action ) const;

            //-----------------------------------------------------------------
            /// \brief Write the structure to the stream
            ///
            /// \param[in] Stream
            ///     The output stream where the object is to be written.
            //-----------------------------------------------------------------
            void write( Common::OStream& Stream ) const;
        };

        inline Common::FrameSpec::size_type
        FrTOCSimData::Bytes( const Common::StreamBase& Stream ) const
        {
            Common::FrameSpec::size_type retval = sizeof( nsim_type );
            if ( m_info.size( ) )
            {
                retval += ( m_info.size( ) *
                            ( sizeof( position_type ) *
                              m_info.begin( )->second.size( ) ) );
            }
            for ( MapSim_type::const_iterator cur = m_info.begin( ),
                                              last = m_info.end( );
                  cur != last;
                  ++cur )
            {
                retval += cur->first.Bytes( );
            }
            return retval;
        }

        template < typename SE >
        void
        FrTOCSimData::Description( Common::Description& Desc )
        {
            Desc( SE( "nSim",
                      "INT_4U",
                      "Number of unique FrSimData names in file." ) );
            Desc( SE( "nameSim", "STRING[nSim]", "Array of FrSimData names" ) );
            Desc( SE( "positionSim",
                      "INT_8U[nSim][nFrame]",
                      "Array of lists of FrSimData offset positions, in bytes,"
                      " from beginning of file (size of nFrame*nSim)" ) );
        }

    } // namespace Version_8
} // namespace FrameCPP

#endif /* ! defined(SWIGIMPORTED) */

#endif /* FrameCPP_VERSION_8_FrTOCSimData_HH */
