//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <algorithm>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrAdcData.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"

#include "framecpp/Version8/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

#if HAVE_TEMPLATE_MOVE
#define MOVE_RET( a ) ( std::move( a ) )
#else /* HAVE_TEMPLATE_MOVE */
#define MOVE_RET( a ) ( a )
#endif /* HAVE_TEMPLATE_MOVE */

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using std::ceil;
using std::floor;

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE( FrAdcDataImpl,
                            FSI_FR_ADC_DATA,
                            "FrAdcData",
                            "ADC Data Structure" )

namespace FrameCPP
{
    namespace Version_8
    {
        //-----------------------------------------------------------------------
        //
        //-----------------------------------------------------------------------

        FrAdcData::FrAdcData( )
        {
        }

        FrAdcData::FrAdcData( const name_type&   Name,
                              channelGroup_type  ChannelGroup,
                              channelNumber_type ChannelNumber,
                              nBits_type         NBits,
                              sampleRate_type    SampleRate,
                              bias_type          Bias,
                              slope_type         Slope,
                              const units_type&  Units,
                              fShift_type        FShift,
                              timeOffset_type    TimeOffset,
                              dataValid_type     DataValid,
                              phase_type         Phase )
        {
            Data::name = Name;
            Data::channelGroup = ChannelGroup;
            Data::channelNumber = ChannelNumber;
            Data::nBits = NBits;
            Data::sampleRate = SampleRate;
            Data::bias = Bias;
            Data::slope = Slope;
            Data::units = Units;
            Data::fShift = FShift;
            Data::timeOffset = TimeOffset;
            Data::dataValid = DataValid;
            Data::phase = Phase;
        }

        FrAdcData::FrAdcData( Previous::FrAdcData& Source,
                              stream_base_type*    Stream )
            : FrAdcDataImpl::ClassicIO< FrAdcData >( Source, Stream )
        {
        }

#if 0
        FrAdcData::FrAdcData( istream_type& Stream )
            : nps_type( Stream ), ps_type( Stream ),
              object_type( s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            Stream.Next( this );
        }

        FrAdcData::~FrAdcData( )
        {
        }
#endif /* 0 */

        const FrAdcData::name_type&
        FrAdcData::GetNameSlow( ) const
        {
            return Data::GetName( );
        }

        FrAdcData&
        FrAdcData::Merge( const FrAdcData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrAdcData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrAdcData::subset_ret_type
        FrAdcData::Subset( REAL_8 Offset, REAL_8 Dt ) const
        {
            //---------------------------------------------------------------------
            // Sanity checks
            //---------------------------------------------------------------------
            // Verify the existance of only a single FrVect component
            //---------------------------------------------------------------------
            if ( data.size( ) != 1 )
            {
                std::ostringstream msg;

                msg << "METHOD: frameAPI::FrAdcData::SubFrAdcData: "
                    << "data vectors of length " << data.size( )
                    << " currently are not supported";
                throw std::logic_error( msg.str( ) );
            }
            INT_4U num_elements = data[ 0 ]->GetDim( 0 ).GetNx( );
            //---------------------------------------------------------------------
            /// \todo
            ///   Ensure the dimension of the FrVect is 1
            //---------------------------------------------------------------------
            //---------------------------------------------------------------------
            /// \todo
            ///   Ensure the requested range is valid
            //---------------------------------------------------------------------
            if ( Offset >
                 ( GetTimeOffset( ) + ( num_elements / GetSampleRate( ) ) ) )
            {
                std::ostringstream msg;
                msg << "METHOD: frameAPI::FrAdcData::SubFrAdcData: "
                    << "The value for the parameter Offset (" << Offset
                    << ") is out of range ([0-"
                    << ( GetTimeOffset( ) +
                         ( num_elements / GetSampleRate( ) ) )
                    << "))";
                throw std::range_error( msg.str( ) );
            }
            //---------------------------------------------------------------------
            // Start moving of data
            //---------------------------------------------------------------------
            subset_ret_type retval( new FrAdcData );

            // Copy in the header information
            retval->name = name;
            retval->comment = comment;
            retval->channelGroup = channelGroup;
            retval->channelNumber = channelNumber;
            retval->nBits = nBits;
            retval->sampleRate = sampleRate;
            retval->bias = bias;
            retval->slope = slope;
            retval->units = units;
            retval->fShift = fShift;
            retval->timeOffset = timeOffset;
            retval->dataValid = dataValid;
            retval->phase = phase;

            /// \todo
            ///   Get the slice of FrVect
            REAL_8 startSampleReal = ( Offset * retval->GetSampleRate( ) ) -
                ( retval->GetTimeOffset( ) * retval->GetSampleRate( ) );

            INT_4U endSample = INT_4U(
                ceil( startSampleReal + ( retval->GetSampleRate( ) * Dt ) ) );
            INT_4U startSample = ( startSampleReal > REAL_8( 0 ) )
                ? INT_4U( floor( startSampleReal ) )
                : INT_4U( 0 );

            {
                boost::shared_ptr< FrVect > subvect(
                    this->data[ 0 ]
                        ->SubFrVect( startSample, endSample )
                        .release( ) );

                retval->data.append( subvect );
            }

            //---------------------------------------------------------------------
            // Shift the offset
            //---------------------------------------------------------------------
            if ( startSampleReal > 0 )
            {
                // There is no gap
                retval->SetTimeOffset( 0 );
            }
            else
            {
                retval->incrementTimeOffset( Offset );
            }
            //---------------------------------------------------------------------
            // Return the new FrAdcData
            //---------------------------------------------------------------------
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
            return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
        }

    } // namespace Version_8
} // namespace FrameCPP
