//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_8_FrRawData_HH
#define FrameCPP_VERSION_8_FrRawData_HH

#include "framecpp/Version8/impl/FrRawDataClassicIO.hh"

#if defined( __cplusplus ) && !defined( SWIG )

namespace FrameCPP
{
    namespace Version_8
    {
        //===================================================================
        /// @brief Simulated Data Structure Definition
        //===================================================================
        class FrRawData : public FrRawDataImpl::ClassicIO< FrRawData >
        {
        public:
            //-----------------------------------------------------------------
            /// @brief Default constructor
            ///
            /// @return
            ///    A new instance of this object.
            //-----------------------------------------------------------------
            FrRawData( ) = default;

            //-----------------------------------------------------------------
            /// @brief Copy Constructor
            ///
            /// @param[in] rawData
            ///     The object from which to copy the information.
            ///
            /// @return
            ///    A new instance of this object.
            //-----------------------------------------------------------------
            FrRawData( const FrRawData& rawData ) = default;

            //-----------------------------------------------------------------
            /// @brief Constructor
            ///
            /// @param[in] name
            ///     Namne of raw data.
            ///
            /// @return
            ///     A new instance of the object.
            //-----------------------------------------------------------------
            FrRawData( const name_type& name );

            //-----------------------------------------------------------------
            /// @brief Promotion Constructor
            ///
            /// @param[in] Source
            ///     An FrRawData structure from a previous frame specification.
            /// @param[in] Stream
            ///     The stream from which the earliest version of the
            ///     FrRawData structure was read.
            ///
            /// @return
            ///     A new instance of an FrRawData object
            //-----------------------------------------------------------------
            FrRawData( Previous::FrRawData& Source, stream_base_type* Stream );

            //-----------------------------------------------------------------
            /// @brief Return the name associate with the FrRawData structure.
            ///
            /// @return
            ///     The name associated with the FrRawData structure
            //-----------------------------------------------------------------
            const name_type& GetName( ) const;

            //-----------------------------------------------------------------
            /// @brief equality operator
            ///
            /// @param[in] RHS
            ///     The FrRawData object to be compared.
            ///
            /// @return
            ///     The value true is returned if this object is equivelent
            ///     to the RHS instance; false otherwise.
            //-----------------------------------------------------------------
            inline bool
            operator==( const FrRawData& RHS ) const
            {
                return ( Data::operator==( RHS ) );
            }

            //-----------------------------------------------------------------
            /// @brief equality operator for abstract data type
            ///
            /// @param[in] RHS
            ///     The object to be compared.
            ///
            /// @return
            ///     The value true is returned if this object is equivelent
            ///     to the RHS instance; false otherwise.
            //-----------------------------------------------------------------
            virtual bool
            operator==( const Common::FrameSpec::Object& RHS ) const;

        }; // class FrRawData

    } // namespace Version_8
} // namespace FrameCPP
#endif /* defined( __cplusplus ) && !defined( SWIG ) */

#endif /* FrameCPP_VERSION_8_FrRawData_HH */
