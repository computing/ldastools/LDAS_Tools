//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <memory>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrameSpec.hh"

#include "framecpp/Version8/FrSE.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::FrameSpec;

using namespace FrameCPP::Version_8;

FR_OBJECT_META_DATA_DEFINE( FrSEImpl,
                            FSI_FR_SE,
                            "FrSE",
                            "Frame Structure Element" )

FrSE::FrSE( )
{
}

FrSE::FrSE( const name_type&    Name,
            const classId_type& ClassId,
            const comment_type& Comment )
{
    name = Name;
    classId = ClassId;
    comment = Comment;
}

FrSE::~FrSE( )
{
}

namespace FrameCPP
{
    namespace Version_8
    {
        namespace FrSEImpl
        {
            template <>
            bool
            ClassicIO< FrSE >::
            operator==( const Common::FrameSpec::Object& Obj ) const
            {
                return Common::Compare( *this, Obj );
            }
        } // namespace FrSEImpl
    } // namespace Version_8
} // namespace FrameCPP
