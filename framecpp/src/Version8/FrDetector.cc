//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version8/FrDetector.hh"

#include "Common/ComparePrivate.hh"

#include "framecpp/Common/FrameSpec.tcc"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE( FrDetectorImpl,
                            FSI_FR_DETECTOR,
                            "FrDetector",
                            "Detector Data Structure" )

namespace FrameCPP
{
    namespace Version_8
    {
        //=======================================================================
        // FrDetector
        //=======================================================================
        FrDetector::FrDetector( )
        {
            prefix[ 0 ] = ' ';
            prefix[ 1 ] = ' ';
            longitude = 0.0;
            latitude = 0.0;
            elevation = 0.0;
            armXazimuth = 0.0;
            armYazimuth = 0.0;
            armXaltitude = 0.0;
            armYaltitude = 0.0;
            armXmidpoint = 0.0;
            armYmidpoint = 0.0;
            localTime = 0;
        }

        FrDetector::FrDetector( const name_type&        Name,
                                const char*             Prefix,
                                const longitude_type    Longitude,
                                const latitude_type     Latitude,
                                const elevation_type    Elevation,
                                const armXazimuth_type  ArmXazimuth,
                                const armYazimuth_type  ArmYazimuth,
                                const armXaltitude_type ArmXaltitude,
                                const armYaltitude_type ArmYaltitude,
                                const armXmidpoint_type ArmXmidpoint,
                                const armYmidpoint_type ArmYmidpoint,
                                const localTime_type    LocalTime )
        {
            name = Name;
            prefix[ 0 ] = Prefix[ 0 ];
            prefix[ 1 ] = Prefix[ 1 ];
            longitude = Longitude;
            latitude = Latitude;
            elevation = Elevation;
            armXazimuth = ArmXazimuth;
            armYazimuth = ArmYazimuth;
            armXaltitude = ArmXaltitude;
            armYaltitude = ArmYaltitude;
            armXmidpoint = ArmXmidpoint;
            armYmidpoint = ArmYmidpoint;
            localTime = LocalTime;
        }

        FrDetector::FrDetector( const FrDetector& detector )
        {
            AT( );
            name = detector.name;
            AT( );
            prefix[ 0 ] = detector.prefix[ 0 ];
            AT( );
            prefix[ 1 ] = detector.prefix[ 1 ];
            AT( );
            longitude = detector.longitude;
            AT( );
            latitude = detector.latitude;
            AT( );
            elevation = detector.elevation;
            AT( );
            armXazimuth = detector.armXazimuth;
            AT( );
            armYazimuth = detector.armYazimuth;
            AT( );
            armXaltitude = detector.armXaltitude;
            AT( );
            armYaltitude = detector.armYaltitude;
            AT( );
            armXmidpoint = detector.armXmidpoint;
            AT( );
            armYmidpoint = detector.armYmidpoint;
            AT( );
            localTime = detector.localTime;
            AT( );
            aux = detector.aux;
            AT( );
            table = detector.table;
            AT( );
        }

        FrDetector::FrDetector( Previous::FrDetector& Source,
                                stream_base_type*     Stream )
        {
            name = Source.GetName( );
            std::copy( Source.GetPrefix( ), &Source.GetPrefix( )[ 2 ], prefix );
            longitude = Source.GetLongitude( );
            latitude = Source.GetLatitude( );
            elevation = Source.GetElevation( );
            armXazimuth = Source.GetArmXazimuth( );
            armYazimuth = Source.GetArmYazimuth( );
            armXaltitude = Source.GetArmXaltitude( );
            armYaltitude = Source.GetArmYaltitude( );
            armXmidpoint = Source.GetArmXmidpoint( );
            armYmidpoint = Source.GetArmYmidpoint( );
            localTime = Source.GetLocalTime( );

            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( istream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                istream->ReplaceRef( RefAux( ), Source.RefAux( ), MAX_REF );
                istream->ReplaceRef( RefTable( ), Source.RefTable( ), MAX_REF );
            }
        }

        const std::string&
        FrDetector::GetName( ) const
        {
            return Data::GetName( );
        }

        FrDetector&
        FrDetector::Merge( const FrDetector& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrDetector::operator==( const Common::FrameSpec::Object& RHS ) const
        {
            return Common::Compare( *this, RHS );
        }

    } // namespace Version_8
} // namespace FrameCPP
