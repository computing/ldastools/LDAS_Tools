//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrSimEvent.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"

#include "framecpp/Version8/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::IStream;
using FrameCPP::Common::OStream;

#define LM_DEBUG 0

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE( FrSimEventImpl,
                            FSI_FR_SIM_EVENT,
                            "FrSimEvent",
                            "Simulated Event Data Structure" )

namespace FrameCPP
{
    namespace Version_8
    {
        //===============================================================
        // FrSimEvent
        //===============================================================
        FrSimEvent::FrSimEvent( )
        {
        }

        FrSimEvent::FrSimEvent( const std::string&    name,
                                const std::string&    comment,
                                const std::string&    inputs,
                                const GPSTime&        time,
                                const REAL_4          timeBefore,
                                const REAL_4          timeAfter,
                                const REAL_4          amplitude,
                                const ParamList_type& parameters )
        {
            Data::name = name;
            Data::comment = comment;
            Data::inputs = inputs;
            Data::GTime = time;
            Data::timeBefore = timeBefore;
            Data::timeAfter = timeAfter;
            Data::amplitude = amplitude;
            Data::Params = parameters;
        }

        FrSimEvent::FrSimEvent( const FrSimEvent& Source )
        {
            name = Source.name;
            comment = Source.comment;
            inputs = Source.inputs;
            GTime = Source.GTime;
            timeBefore = Source.timeBefore;
            timeAfter = Source.timeAfter;
            amplitude = Source.amplitude;
            Params = Source.Params;
        }

        FrSimEvent::FrSimEvent( const Previous::FrSimEvent& Source,
                                stream_base_type*           Stream )
            : FrSimEventImpl::ClassicIO< FrSimEvent >( Source, Stream )
        {
        }

        const std::string&
        FrSimEvent::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrSimEvent&
        FrSimEvent::Merge( const FrSimEvent& RHS )
        {
            throw Unimplemented(
                "FrSimEvent& FrSimEvent::Merge( const FrSimEvent& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        bool
        FrSimEvent::operator==( const Common::FrameSpec::Object& RHS ) const
        {
            return Common::Compare( *this, RHS );
        }

    } // namespace Version_8
} // namespace FrameCPP
