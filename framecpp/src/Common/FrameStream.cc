//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <cassert>

#include <iomanip>
#include <sstream>
#include <stdexcept>

#include <boost/pointer_cast.hpp>

#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/System.hh"

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Dictionary.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/StreamRef.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Common/FrHeader.hh"
#include "framecpp/Common/FrameH.hh"
#include "framecpp/Common/FrDetector.hh"
#include "framecpp/Common/FrEndOfFile.hh"
#include "framecpp/Common/FrSH.hh"
#include "framecpp/Common/FrStatData.hh"
#include "framecpp/Common/FrTOC.hh"
#include "framecpp/Common/FrVect.hh"

#define ERROR_HANDLER( )                                                       \
    try                                                                        \
    {                                                                          \
        throw;                                                                 \
    }                                                                          \
    catch ( const std::exception& Exception )                                  \
    {                                                                          \
        setLastError( Exception.what( ) );                                     \
        throw;                                                                 \
    }                                                                          \
    catch ( ... )                                                              \
    {                                                                          \
        setLastError( "Unknwon exception" );                                   \
        throw;                                                                 \
    } //

using LDASTools::System::ErrnoMessage;

#if 0
template < typename T, typename... Args >
std::unique_ptr< T >
make_unique( Args&&... args )
{
    return std::unique_ptr< T >( new T( std::forward< Args >( args )... ) );
}
#endif /* 0 */

template < typename To, typename From, typename Deleter >
std::unique_ptr< To, Deleter >
dynamic_unique_cast( std::unique_ptr< From, Deleter >&& p )
{
    if ( To* cast = dynamic_cast< To* >( p.get( ) ) )
    {
        std::unique_ptr< To, Deleter > result( cast,
                                               std::move( p.get_deleter( ) ) );
        p.release( );
        return result;
    }
    return std::unique_ptr< To, Deleter >(
        nullptr ); // or throw std::bad_cast() if you prefer
}

namespace FrameCPP
{
    namespace Common
    {
        //===================================================================
        //===================================================================
        FrameStream::FrameStream( version_type Version, bool AllowZeroVersion )
            : m_closed_state( false ), m_memory_version( Version )
        {
            if ( ( m_memory_version == 0 ) && ( !AllowZeroVersion ) )
            {
                std::ostringstream msg;

                msg << "The value of " << m_memory_version
                    << " is an invalid or unsupported frame version";
                throw std::out_of_range( msg.str( ) );
            }

            if ( ( m_memory_version ) || ( !AllowZeroVersion ) )
            {
                frameSpecInfo( FrameSpec::SpecInfo( m_memory_version ) );
            }
        }

        void
        IFrameStream::VerifyHeader( Verify& Verifier )
        {
            if ( m_file_header.get( ) )
            {
                FrameSpec::Object* obj( dynamic_cast< FrameSpec::Object* >(
                    m_file_header.get( ) ) );

                if ( obj )
                {
                    obj->VerifyObject( Verifier, *this );
                }
            }
        }

        void
        FrameStream::frameSpecInfo( const FrameSpec::Info* Spec )
        {
            //-----------------------------------------------------------------
            // Initialize the base class information
            //-----------------------------------------------------------------
            StreamBase::frameSpecInfo( Spec );
            //-----------------------------------------------------------------
            // Initialize local information
            //-----------------------------------------------------------------
            m_dictionary.m_stream_ref = ReferenceStreamRef( );
            m_stream_ref = ReferenceStreamRef( );
        }

        FrameStream::object_type
        FrameStream::morph( object_type Obj )
        {
            version_type stream_version = streamVersion( );
            version_type memory_version = memoryVersion( );

            if ( stream_version == memory_version )
            {
                return Obj;
            }

            object_type retval = Obj;
            if ( stream_version < memory_version )
            {
                //---------------------------------------------------------------
                // Upconvert
                //---------------------------------------------------------------
                auto stream = dynamic_cast< IFrameStream* >( this );
                if ( stream )
                {
                    return Object::PromoteObject(
                        memory_version, stream_version, Obj, stream );
                }
            }
            else
            {
                //---------------------------------------------------------------
                // Downconvert
                //---------------------------------------------------------------
                auto stream = dynamic_cast< IFrameStream* >( this );

                if ( stream )
                {
                    return Object::DemoteObject( memory_version, Obj, stream );
                }
            }
            return retval;
        }

        void
        FrameStream::reset( )
        {
            setLastError( "" );
        }

        void
        IFrameStream::init( )
        {
            //-----------------------------------------------------------------
            // Ensure buffering on the stream
            //-----------------------------------------------------------------
            rdbuf( )->buffer( );
            //-----------------------------------------------------------------
            // Read the first bytes to get a basic understanding of the
            //  frame spec used to write the file.
            //-----------------------------------------------------------------
            seekg( 0 ); // To go the beginning of the file.
            clear( ); // Clear flags
            FrHeader base( *this );

            if ( !base.IsValid( ) )
            {
                throw std::runtime_error(
                    "Not a frame file (Invalid FrHeader)." );
            }
            //-----------------------------------------------------------------
            // Setup to use the appropriate frame specification table
            //-----------------------------------------------------------------
            frameSpecInfo( FrameSpec::SpecInfo( base.GetVersion( ) ) );

            //-----------------------------------------------------------------
            // Read version specific portion of FrHeader
            //-----------------------------------------------------------------
            FrameSpec::ObjectInterface::unique_object_type o(
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_FR_HEADER )
                    ->Create( *this ) );

            FrameSpec::Object* fr_header_object( o.get( ) );
            m_file_header.reset( dynamic_cast< FrHeader* >( o.get( ) ) );
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            if ( m_file_header.get( ) )
            {
                o.release( );
            }
            if ( ( !m_file_header ) ||
                 ( tellg( ) != std::streampos( m_file_header->Bytes( ) ) ) )
            {
                std::ostringstream msg;

                msg << "Failed to properly read the FrHeader structure.";

                throw std::runtime_error( msg.str( ) );
            }
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            m_file_header->operator=( base );
            //-----------------------------------------------------------------
            // Verify the header information
            //-----------------------------------------------------------------
            {
                Verify v;

                fr_header_object->VerifyObject( v, *this );
            }
            byteSwapping( m_file_header->ByteSwapping( ) );
            //-----------------------------------------------------------------
            // Open the file and find out the version
            // This is done by using the earliest possible FrHeader
            //   structure and then promoting it.
            //-----------------------------------------------------------------
            version_stream( base.GetVersion( ) );
            frameLibrary( m_file_header->GetFrameLibrary( ) );
            libraryRevision( m_file_header->GetLibraryRevision( ) );
        }

        //-------------------------------------------------------------------
        /// If possible, this method will load the table of contents
        /// from the stream.
        /// One example of not being able to load the table of contents
        /// is the case where the stream does not have a table of
        /// contents structure.
        //-------------------------------------------------------------------
        void
        IFrameStream::load_toc( )
        {
            if ( m_toc_loaded )
            {
                return;
            }
            //-----------------------------------------------------------------
            // One chance to do this
            //-----------------------------------------------------------------
            m_toc_loaded = true;
            //-----------------------------------------------------------------
            // See if TOC is supported by stream frame spec
            //-----------------------------------------------------------------
            try
            {
                if ( frameSpecInfo( ).FrameObject(
                         FrameSpec::Info::FSI_FR_TOC ) ==
                     (FrameSpec::Object*)NULL )
                {
                    return;
                }
            }
            catch ( const std::range_error& e )
            {
                //---------------------------------------------------------------
                // The frame spec does not support table of contents so just
                // return to the caller.
                //---------------------------------------------------------------
                return;
            }
            //-----------------------------------------------------------------
            // Record the current position within the stream
            //-----------------------------------------------------------------
            const std::streampos here( tellg( ) );

            try
            {
                //---------------------------------------------------------------
                // :TODO: Determine if the stream is rewindable
                //---------------------------------------------------------------
                //---------------------------------------------------------------
                // Get the number of bytes for the FrEndOfFile structure
                //---------------------------------------------------------------
                const std::streampos offset(
                    frameSpecInfo( )
                        .FrameObject( FrameSpec::Info::FSI_FR_END_OF_FILE )
                        ->Bytes( *this ) +
                    frameSpecInfo( )
                        .FrameObject( FrameSpec::Info::FSI_COMMON_ELEMENTS )
                        ->Bytes( *this ) );

                seekg( -offset,
                       std::ios_base::end ); // Seek to the beginning of struct
                //---------------------------------------------------------------
                // Read the common elements.
                //---------------------------------------------------------------
                FrameSpec::ObjectInterface::unique_object_type cmn_obj(
                    frameSpecInfo( )
                        .FrameObject( FrameSpec::Info::FSI_COMMON_ELEMENTS )
                        ->Create( *this ) );
                const StreamRefInterface* sri(
                    dynamic_cast< const StreamRefInterface* >(
                        cmn_obj.get( ) ) );

                if ( ( sri == NULL ) ||
                     ( sri->GetLength( ) !=
                       ( StreamRefInterface::length_type )( offset ) ) )
                {
                    //-------------------------------------------------------------
                    // This file appears to be corrupted.
                    //-------------------------------------------------------------
                    throw std::runtime_error( "Missing FrEndOfFile structure" );
                }
                //---------------------------------------------------------------
                // Create object and place into smart pointer for auto cleanup
                //---------------------------------------------------------------
                FrameSpec::ObjectInterface::unique_object_type eof_obj(
                    frameSpecInfo( )
                        .FrameObject( FrameSpec::Info::FSI_FR_END_OF_FILE )
                        ->Create( *this ) );
                //---------------------------------------------------------------
                // Transform into a form that can be used by generic interface
                //---------------------------------------------------------------
                const FrEndOfFile* eof(
                    dynamic_cast< const FrEndOfFile* >( eof_obj.get( ) ) );
                if ( eof )
                {
                    if ( eof->SeekTOC( ) )
                    {
                        seekg( -( eof->SeekTOC( ) ), std::ios::end );
                        //-----------------------------------------------------------
                        // Read the reference
                        //-----------------------------------------------------------
                        FrameSpec::ObjectInterface::unique_object_type ref_obj(
                            frameSpecInfo( )
                                .FrameObject(
                                    FrameSpec::Info::FSI_COMMON_ELEMENTS )
                                ->Create( *this ) );
                        FrameSpec::ObjectInterface::unique_object_type toc_obj(
                            frameSpecInfo( )
                                .FrameObject( FrameSpec::Info::FSI_FR_TOC )
                                ->Create( *this ) );
                        m_toc.reset( dynamic_cast< FrTOC* >( toc_obj.get( ) ) );
                        if ( m_toc )
                        {
                            //---------------------------------------------------------
                            // Successful with the dynamic cast so m_toc is now
                            // responsible for releasing the memory resouces.
                            //---------------------------------------------------------
                            toc_obj.release( );
                            //---------------------------------------------------------
                            // Extract mapping information
                            //---------------------------------------------------------
                            for ( INT_4U cur = 0, last = m_toc->nSH( );
                                  cur != last;
                                  ++cur )
                            {
                                const INT_2U fsi_id =
                                    GetClassId( m_toc->SHname( cur ) );
                                if ( fsi_id )
                                {
                                    m_stream_id_to_fsi_id[ m_toc->SHid(
                                        cur ) ] = fsi_id;
                                }
                            }
                        }
                    }
                }
            }
            catch ( ... )
            {
                seekg( here );
                throw;
            }
            //-----------------------------------------------------------------
            // Restore file position
            //-----------------------------------------------------------------
            seekg( here );
        }

        void
        IFrameStream::reset( )
        {
            Cleanup( );
            FrameStream::reset( );
        }

        //-------------------------------------------------------------------

        //===================================================================
        //===================================================================
        IFrameStream::IFrameStream( buffer_type* Buffer, INT_2U Version )
            : FrameStream( Version, true /* AllowZeroVersion */ ),
              IStream( Buffer ), m_toc_loaded( false ),
              m_reading_frame_subset( false ),
              m_log_recursive_dependants( true ), m_next_frame_index( 0 )
        {
            try
            {
                init( );
            }
            catch ( ... )
            {
                Cleanup( );
                throw;
            }
        }

        IFrameStream::IFrameStream( bool         AutoDelete,
                                    buffer_type* Buffer,
                                    INT_2U       Version )
            : FrameStream( Version, true /* AllowZeroVersion */ ),
              IStream( Buffer, AutoDelete ), m_toc_loaded( false ),
              m_reading_frame_subset( false ),
              m_log_recursive_dependants( true ), m_next_frame_index( 0 )
        {
            try
            {
                init( );
            }
            catch ( ... )
            {
                Cleanup( );
                throw;
            }
        }

        void
        IFrameStream::Cleanup( )
        {
            IStream::Cleanup( );
        }

        IFrameStream::ptr_struct_base_type
        IFrameStream::Next( object_type::element_type* Obj )
        {
            ptr_struct_base_type next( IStream::Next( Obj ) );

            if ( m_logNextPtr && next )
            {
                addRecursiveDependant( next );
            }
            return next;
        }

        IFrameStream::object_type
        IFrameStream::Read( )
        {
            object_type retval;

            //-----------------------------------------------------------------
            // Establish that the stream is in a good state.
            //-----------------------------------------------------------------
            if ( !good( ) )
            {
                return retval;
            }

            //-----------------------------------------------------------------
            // Establish the start of the object being read
            //-----------------------------------------------------------------
            boost::shared_ptr< StreamRefInterface > streamref;
            while ( 1 )
            {
                streamref = readStreamRef( );
                if ( !streamref )
                {
                    // Reached the end of file.
                    return retval;
                }
                //---------------------------------------------------------------
                /// \todo Check if read goes beyond the end of the file
                //---------------------------------------------------------------
                //---------------------------------------------------------------
                // Transform from stream class to FSI class id
                //---------------------------------------------------------------
                const INT_2U sid = streamref->GetClass( );

                try
                {
                    // Testing for exception
                    GetFSIId( sid );
                }
                catch ( ... )
                {
                    //-------------------------------------------------------------
                    // Seek to the end of this unknown class
                    //-------------------------------------------------------------

                    seekg( streamref->GetLength( ) -
                               ReferenceStreamRef( )->Bytes( *this ),
                           std::ios::cur );
                    //-------------------------------------------------------------
                    // Rethrow the exception
                    //-------------------------------------------------------------
                    throw;
                }
                //---------------------------------------------------------------
                // Add information to the appropriate databases.
                //---------------------------------------------------------------
                retval = readObject( streamref.get( ) );
                if ( retval )
                {
                    if ( ( retval->GetClass( ) ==
                           FrameSpec::Info::FSI_FR_SH ) ||
                         ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SE ) )
                    {
                        continue;
                    }
                    m_dictionary.Ref( streamref, retval );
                }
                break;
            }

            //-----------------------------------------------------------------
            // Report back to the caller
            //-----------------------------------------------------------------
            return retval;
        }

        IFrameStream::object_type
        IFrameStream::ReadDetector( const std::string& Name )
        {
            static const char* method =
                "FrameCPP::Common::IFrameStream::ReadDetector";

            //-----------------------------------------------------------------
            // Ensure the table of contents is a usable.
            //-----------------------------------------------------------------
            const FrTOC* toc = GetTOC( );

            if ( toc == (FrTOC*)NULL )
            {
                std::ostringstream msg;

                msg << "The method " << method
                    << " requires a non-NULL table of contents";

                throw std::runtime_error( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Locate it in the table of contents
            //-----------------------------------------------------------------
            const FrTOC::cmn_position_type p( toc->positionDetector( Name ) );

            if ( p <= 0 )
            {
                std::ostringstream msg;

                msg << "The detector named " << Name
                    << " is not indexed in the table of contents for random "
                       "access";
                throw std::runtime_error( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Read the object off the stream
            //-----------------------------------------------------------------
            seekg( p, std::ios::beg );
            object_type retval( Read( ) );

            return retval;
        }

        IFrameStream::object_type
        IFrameStream::ReadFrameN( INT_4U FrameIndex, bool Decompress )
        try
        {
            //-----------------------------------------------------------------
            // Prep for errors
            //-----------------------------------------------------------------
            reset( );
            //-----------------------------------------------------------------
            // Advance to the requested frame within the stream
            //-----------------------------------------------------------------
            object_type retval;

            retval = advanceToFrame( FrameIndex );
            //-----------------------------------------------------------------
            // Read until all references are resolved or until FrEndOfFrame
            //   is reached
            //-----------------------------------------------------------------
            readRecursive( FrameSpec::Info::FSI_FR_END_OF_FRAME, Decompress );
            //-----------------------------------------------------------------
            // Return the results
            //-----------------------------------------------------------------
            return retval;
        }
        catch ( ... )
        {
            ERROR_HANDLER( );
        }

        IFrameStream::object_type
        IFrameStream::ReadFrStatData( INT_8U Position )
        {
            //-----------------------------------------------------------------
            // Read the FrStatData structure
            //-----------------------------------------------------------------
            seekg( Position, std::ios::beg );

            object_type retval( Read( ) );

            if ( ( !retval ) ||
                 ( retval->GetClass( ) != FrameSpec::Info::FSI_FR_STAT_DATA ) )
            {
                //---------------------------------------------------------------
                // This is the case where no FrStatData structure existed
                //---------------------------------------------------------------
                if ( retval )
                {
                    GetDictionary( ).Remove( retval );
                    retval.reset( );
                }
                return retval;
            }
            //-----------------------------------------------------------------
            /// \todo Need to add support for reading the FrDetector for
            ///       FrStatData structures where the FrStatData is before
            ///       any FrameH structures.
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            // Back up to the beginning of the frame
            //-----------------------------------------------------------------
            INT_4U       cur = 0;
            const FrTOC* toc = GetTOC( );

            for ( INT_4U last = toc->nFrame( );
                  ( cur != last ) && ( Position < toc->positionH( cur ) );
                  ++cur )
                ;
            if ( cur > 0 )
            {
                --cur;
            }
            seekg( toc->positionH( cur ), std::ios::beg );
            //-----------------------------------------------------------------
            // Read the additional pieces needed
            //-----------------------------------------------------------------
            readRecursive( FrameSpec::Info::FSI_FR_NULL );
            //-----------------------------------------------------------------
            // Link the pieces
            //-----------------------------------------------------------------
            Resolve( );
            //-----------------------------------------------------------------
            //  Give the user the the information.
            //-----------------------------------------------------------------
            return retval;
        }

        INT_4U
        IFrameStream::ReadFrStatData( const std::string& NamePattern,
                                      const std::string& DetectorPattern,
                                      const LDASTools::AL::GPSTime& StartTime,
                                      const LDASTools::AL::GPSTime& EndTime,
                                      const INT_4U                  Version,
                                      FrStatData::Query& QueryResults )
        {
            QueryResults.Reset( *this );
            QueryResults.Add( NamePattern, StartTime, EndTime, Version );
            return QueryResults.size( );
        }

        IFrameStream::object_type
        IFrameStream::readFrameHSubset( INT_4U Frame, INT_4U ElementMask )
        try
        {
            //-----------------------------------------------------------------
            // Prep for errors
            //-----------------------------------------------------------------
            reset( );
            //-----------------------------------------------------------------
            // Advance to the requested frame within the stream
            //-----------------------------------------------------------------
            object_type retval;

            retval = advanceToFrame( Frame );
            //-----------------------------------------------------------------
            // Pair down what should be read for the FrameH structure
            //-----------------------------------------------------------------
            {
                boost::shared_ptr< FrameH > frameh(
                    boost::dynamic_pointer_cast< FrameH >( retval ) );
                if ( frameh )
                {
                    frameh->readSubset( *this, ElementMask );
                }
            }
            //-----------------------------------------------------------------
            // Read until all references are resolved or until FrEndOfFrame
            //   is reached
            //-----------------------------------------------------------------
            m_reading_frame_subset = true;
            readRecursive( FrameSpec::Info::FSI_FR_END_OF_FRAME );
            m_reading_frame_subset = false;
            //-----------------------------------------------------------------
            // Return the results
            //-----------------------------------------------------------------
            return retval;
        }
        catch ( ... )
        {
            ERROR_HANDLER( );
        }

        IFrameStream::object_type
        IFrameStream::readFrAdcData( INT_4U Frame, const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_ADC_DATA,
                                   const std::string& >( Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrAdcData( INT_4U Frame, INT_4U Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_ADC_DATA >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrAdcStruct( INT_4U             Frame,
                                       const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_ADC_DATA >(
                Frame, Channel, false );
        }

        std::unique_ptr< FrEndOfFile >
        IFrameStream::readFrEndOfFile( )
        {
            //---------------------------------------------------------------
            // Get the number of bytes for the FrEndOfFile structure
            //---------------------------------------------------------------
            const std::streampos offset(
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_FR_END_OF_FILE )
                    ->Bytes( *this ) +
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_COMMON_ELEMENTS )
                    ->Bytes( *this ) );

            seekg( -offset,
                   std::ios_base::end ); // Seek to the beginning of struct
            //---------------------------------------------------------------
            // Read the common elements.
            //---------------------------------------------------------------
            FrameSpec::ObjectInterface::unique_object_type cmn_obj(
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_COMMON_ELEMENTS )
                    ->Create( *this ) );
            const StreamRefInterface* sri(
                dynamic_cast< const StreamRefInterface* >( cmn_obj.get( ) ) );

            if ( ( sri == NULL ) ||
                 ( sri->GetLength( ) !=
                   ( StreamRefInterface::length_type )( offset ) ) )
            {
                //-------------------------------------------------------------
                // This file appears to be corrupted.
                //-------------------------------------------------------------
                throw std::runtime_error( "Missing FrEndOfFile structure" );
            }
            //---------------------------------------------------------------
            // Create object and place into smart pointer for auto cleanup
            //---------------------------------------------------------------
            FrameSpec::ObjectInterface::unique_object_type eof_obj(
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_FR_END_OF_FILE )
                    ->Create( *this ) );
            std::unique_ptr< FrEndOfFile > retval;
            FrEndOfFile*                   eof_ptr =
                dynamic_cast< FrEndOfFile* >( eof_obj.get( ) );
            if ( eof_ptr )
            {
                retval.reset( eof_ptr );
                eof_obj.release( );
            }
            return retval;
        }

        IFrameStream::object_type
        IFrameStream::readFrAdcStruct( INT_4U Frame, INT_4U Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_ADC_DATA >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::readFrEvent( INT_4U Frame, const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_EVENT >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrEventStruct( INT_4U             Frame,
                                         const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_EVENT >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::readFrEvent( const std::string&   EventType,
                                   fr_event_offset_type Index )
        {
            return read_indexed_fr_struct< FrameSpec::Info::FSI_FR_EVENT >(
                EventType, Index, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrProcData( INT_4U Frame, const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_PROC_DATA >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrProcData( INT_4U Frame, INT_4U Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_PROC_DATA >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrProcStruct( INT_4U             Frame,
                                        const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_PROC_DATA >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::readFrProcStruct( INT_4U Frame, INT_4U Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_PROC_DATA >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::readFrSerData( INT_4U Frame, const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_SER_DATA >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrSerStruct( INT_4U             Frame,
                                       const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_SER_DATA >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::readFrSimData( INT_4U Frame, const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_SIM_DATA >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrSimEvent( INT_4U Frame, const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_SIM_EVENT >(
                Frame, Channel, true );
        }

        IFrameStream::object_type
        IFrameStream::readFrSimEventStruct( INT_4U             Frame,
                                            const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_SIM_EVENT >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::readFrSimEvent( const std::string&   EventType,
                                      fr_event_offset_type Index )
        {
            return read_indexed_fr_struct< FrameSpec::Info::FSI_FR_SIM_EVENT >(
                EventType, Index, true );
        }

        std::unique_ptr< FrTOC >
        IFrameStream::readFrTOCHeader( FrEndOfFile::seekTOC_cmn_type Offset )
        {
            if ( !Offset )
            {
                return ( std::unique_ptr< FrTOC >( ) );
            }
            seekg( -( Offset ), std::ios::end );
            //-----------------------------------------------------------
            // Read the reference
            //-----------------------------------------------------------
            FrameSpec::ObjectInterface::unique_object_type ref_obj(
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_COMMON_ELEMENTS )
                    ->Create( *this ) );
            FrameSpec::ObjectInterface::unique_object_type toc_obj(
                frameSpecInfo( )
                    .FrameObject( FrameSpec::Info::FSI_FR_TOC )
                    ->Create( ) );
            FrTOC* toc_ptr = dynamic_cast< FrTOC* >( toc_obj.get( ) );
            if ( toc_ptr )
            {
                toc_obj.release( );
            }
            auto retval = std::unique_ptr< FrTOC >( toc_ptr );
            retval->loadHeader( *this );
            return retval;
        }

        IFrameStream::object_type
        IFrameStream::readNextFrame( )
        try
        {
            //-----------------------------------------------------------------
            // Prep for errors
            //-----------------------------------------------------------------
            reset( );
            //-----------------------------------------------------------------
            // Advance to the requested frame within the stream
            //-----------------------------------------------------------------
            object_type retval;

            retval = advanceToFrame( m_next_frame_index );
            //-----------------------------------------------------------------
            // Read until all references are resolved or until FrEndOfFrame
            //   is reached
            //-----------------------------------------------------------------
            readRecursive( FrameSpec::Info::FSI_FR_END_OF_FRAME );
            ++m_next_frame_index;
            //-----------------------------------------------------------------
            // Return the results
            //-----------------------------------------------------------------
            return retval;
        }
        catch ( ... )
        {
            ERROR_HANDLER( );
        }

        IFrameStream::object_type
        IFrameStream::readFrSimStruct( INT_4U             Frame,
                                       const std::string& Channel )
        {
            return read_fr_struct< FrameSpec::Info::FSI_FR_SIM_DATA >(
                Frame, Channel, false );
        }

        IFrameStream::object_type
        IFrameStream::advanceToFrame( INT_4U Frame )
        {
            object_type retval;

            const FrTOC* toc = (const FrTOC*)NULL;

            try
            {
                toc = dynamic_cast< const FrTOC* >( GetTOC( ) );
            }
            catch ( const std::runtime_error& Error )
            {
                /* Ignore runtime error of no TOC */
            }

            //-----------------------------------------------------------------
            // Position within the file where the frame starts
            //-----------------------------------------------------------------
            if ( toc )
            {
                //---------------------------------------------------------------
                // Fast seek implementation via table of contents.
                //---------------------------------------------------------------
                const std::streampos offset = toc->positionH( Frame );

                seekg( offset, std::ios::beg );
                retval = Read( );
                while (
                    good( ) && retval &&
                    ( ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SE ) ||
                      ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SH ) ) )
                {
                    //-------------------------------------------------------------
                    // Ignore structure definition components
                    //-------------------------------------------------------------
                    retval = Read( );
                }
                if ( retval->GetClass( ) != FrameSpec::Info::FSI_FRAME_H )
                {
                    std::ostringstream msg;

                    msg << "The object at location " << offset
                        << " is not a FrameH structure";
                    throw std::runtime_error( msg.str( ) );
                }
            }
            else
            {
                if ( Frame > m_next_frame_index )
                {
                    std::ostringstream msg;

                    msg << "Unable to rewind to frame from "
                        << m_next_frame_index << " to " << Frame << std::endl;
                    throw std::range_error( msg.str( ) );
                }

                boost::shared_ptr< StreamRefInterface > streamref;

                while ( m_next_frame_index <= Frame )
                {
                    streamref = readStreamRef( );

                    if ( !streamref )
                    {
                        std::ostringstream msg;

                        msg << "Request for frame " << Frame
                            << " exceeds the range of 0 through "
                            << ( m_next_frame_index - 1 );
                        throw std::out_of_range( msg.str( ) );
                    }
                    else
                    {
                        if ( GetFSIId( streamref->GetClass( ) ) ==
                             FrameSpec::Info::FSI_FRAME_H )
                        {
                            ++m_next_frame_index;
                            retval = readObject( streamref.get( ) );
                            if ( m_next_frame_index > Frame )
                            {
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        //-----------------------------------------------------------
                        // Check for dictionary information
                        //-----------------------------------------------------------
                        if ( ( streamref->GetClass( ) == 1 ) ||
                             ( streamref->GetClass( ) == 2 ) )
                        {
                            //---------------------------------------------------------
                            // This object contains dictionary info so need to
                            // capture for proper decoding of stream
                            //---------------------------------------------------------
                            readObject( streamref.get( ) );
                            continue;
                        }
                        //-----------------------------------------------------------
                        // Skip the object
                        //-----------------------------------------------------------
                        seekg( streamref->GetLength( ) -
                                   ReferenceStreamRef( )->Bytes( *this ),
                               std::ios::cur );
                    }
                }
            }
            //-----------------------------------------------------------------
            // Return the information to the caller
            //-----------------------------------------------------------------
            return retval;
        }

        boost::shared_ptr< StreamRefInterface >
        IFrameStream::readStreamRef( )
        {
            boost::shared_ptr< FrameSpec::Object >  obj;
            boost::shared_ptr< StreamRefInterface > retval;

            //-----------------------------------------------------------------
            // Read the common element block
            //-----------------------------------------------------------------
            try
            {
                obj.reset( streamRef( ).Create( *this ) );
            }
            catch ( ... )
            {
                if ( eof( ) && ( gcount( ) == 0 ) )
                {
                    //-------------------------------------------------------------
                    // Just ran into the end of the file. Nothing to panic about
                    //-------------------------------------------------------------
                    return retval;
                }
                //---------------------------------------------------------------
                // Something bad has happened. Notify the caller
                //---------------------------------------------------------------
                throw; // Rethow the exception
            }
            if ( eof( ) && ( gcount( ) == 0 ) )
            {
                //---------------------------------------------------------------
                // Just ran into the end of the file. Nothing to panic about
                //---------------------------------------------------------------
                return retval;
            }
            retval = boost::dynamic_pointer_cast< StreamRefInterface >( obj );
            if ( !retval )
            {
                throw std::runtime_error(
                    "Failed to read common elements for the data structure" );
            }
            try
            {
                INT_4U fsi_id = GetFSIId( retval->GetClass( ) );
                if ( fsi_id == FrameSpec::Info::FSI_FR_DETECTOR )
                {
                    //---------------------------------------------------------
                    // Log where all detectors exist
                    //---------------------------------------------------------
                    INT_8U pos =
                        ( INT_8U( tellg( ) ) -
                          INT_8U( ReferenceStreamRef( )->Bytes( *this ) ) );

                    m_detector_pos[ pos ].m_class = retval->GetClass( );
                    m_detector_pos[ pos ].m_instance = retval->GetInstance( );
                }
            }
            catch ( std::range_error& E )
            {
                std::ostringstream msg;

                INT_8U pos =
                    ( INT_8U( tellg( ) ) -
                      INT_8U( ReferenceStreamRef( )->Bytes( *this ) ) );

                msg << E.what( ) << " at position: " << pos
                    << " [streamref: class: " << retval->GetClass( )
                    << " instance: " << retval->GetInstance( )
                    << " length: " << retval->GetLength( ) << " ]";

                //-------------------------------------------------------------
                // Seek to the end of this unknown class
                //-------------------------------------------------------------
                seekg( retval->GetLength( ) -
                           ReferenceStreamRef( )->Bytes( *this ),
                       std::ios::cur );

                //-------------------------------------------------------------
                // Rethrow the exception
                //-------------------------------------------------------------
                throw std::range_error( msg.str( ) );
            }
            return retval;
        }

        IFrameStream::object_type
        IFrameStream::readObject( const StreamRefInterface* StreamRef )
        {
            const INT_4U fsi_id = GetFSIId( StreamRef->GetClass( ) );

            object_type obj;

            const std::streampos start =
                ( tellg( ) -
                  std::streampos( ReferenceStreamRef( )->Bytes( *this ) ) );

            try
            {
                object_type::element_type* ptr(
                    frameSpecInfo( )
                        .FrameObject(
                            FrameSpec::Info::frame_object_types( fsi_id ) )
                        ->Create( *this ) );

                obj = objectLookup( ptr );
            }
            catch ( const Common::VerifyException& E )
            {
                std::ostringstream msg;

                msg << E.what( ) << " [ StreamId: " << StreamRef->GetClass( )
                    << "-" << StreamRef->GetInstance( ) << " ]";
                throw Common::VerifyException( E.ErrorCode( ), msg.str( ) );
            }
            catch ( const std::runtime_error& E )
            {
                std::ostringstream msg;

                msg << E.what( ) << " [ StreamId: " << StreamRef->GetClass( )
                    << "-" << StreamRef->GetInstance( ) << " ]";
                throw std::runtime_error( msg.str( ) );
            }
            catch ( const std::exception& E )
            {
                std::ostringstream msg;

                msg << E.what( ) << " [ StreamId: " << StreamRef->GetClass( )
                    << "-" << StreamRef->GetInstance( ) << " ]";
                throw std::runtime_error( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Check if the bytes read differs from the exepected value
            //-----------------------------------------------------------------
            if ( ( tellg( ) - start ) !=
                 std::streampos( StreamRef->GetLength( ) ) )
            {
                std::streampos diff = ( tellg( ) - start ) -
                    std::streampos( StreamRef->GetLength( ) );
                //---------------------------------------------------------------
                /// \note
                /// In version 3 patch level 0 of the framecpp library,
                /// the length attribute for the FrSH and FrSE structures were
                /// not properly calculated.
                /// This reader will ignore this specific error so analysis
                /// of these frame files can continue.
                //---------------------------------------------------------------
                if ( !( ( Version( ) == 3 ) &&
                        ( ( ( StreamRef->GetClass( ) == 1 ) &&
                            ( diff == std::streampos( 4 ) ) ) ||
                          ( ( StreamRef->GetClass( ) == 2 ) &&
                            ( diff == std::streampos( 6 ) ) ) ) ) )
                {
                    std::ostringstream msg;

                    msg << "FATAL: Object: " << StreamRef->GetClass( ) << "-"
                        << StreamRef->GetInstance( ) << ": Read "
                        << ( tellg( ) - start ) << " instead of "
                        << StreamRef->GetLength( ) << " bytes.";
                    throw std::range_error( msg.str( ) );
                }
            }
            //-----------------------------------------------------------------
            // Check if stream continues to be in a good state
            //-----------------------------------------------------------------
            if ( !good( ) )
            {
                std::ostringstream msg;

                msg << "I/O error on stream.";
                throw std::runtime_error( msg.str( ) );
            }

            if ( fsi_id == FrameSpec::Info::FSI_FR_SH )
            {
                //---------------------------------------------------------------
                // Create map entry mapping stream class id to memory class id
                //---------------------------------------------------------------
                boost::shared_ptr< FrSH > frsh(
                    boost::dynamic_pointer_cast< FrSH >( obj ) );
                if ( frsh )
                {
                    const INT_2U id = GetClassId( frsh->name( ) );
                    if ( id )
                    {
                        m_stream_id_to_fsi_id[ frsh->classId( ) ] = id;
                    }
                }
            }
            else if ( fsi_id == FrameSpec::Info::FSI_FR_SE )
            {
                //---------------------------------------------------------------
                // Silently ignore these structures.
                //---------------------------------------------------------------
            }
            else
            {
                //---------------------------------------------------------------
                // Alter the object from the version used by the stream to the
                //   version used in memory.
                //---------------------------------------------------------------
                object_type tmp = morph( obj );
                if ( obj != tmp )
                {
                    nextReplace( obj, tmp );
                    obj = tmp;
                }
            }
            return obj;
        }

        void
        IFrameStream::readRecursive( INT_4U StopOnClassId, bool Decompress )
        {
            //-----------------------------------------------------------------
            // Setup for the recursive read
            //-----------------------------------------------------------------
            m_to_be_resolved.clear( );

            for ( resolver_container_type::const_iterator
                      cur = resolverContainer( ).begin( ),
                      last = resolverContainer( ).end( );
                  cur != last;
                  ++cur )
            {
                addRecursiveDependant( ( *cur )->PtrStruct( ) );
            }
            //-----------------------------------------------------------------
            // Read until reaching a stopping point
            //-----------------------------------------------------------------
            boost::shared_ptr< StreamRefInterface > streamref;
            object_type                             obj;

            while ( m_to_be_resolved.empty( ) == false )
            {
                std::streampos struct_end = tellg( );
                streamref = readStreamRef( );
                if ( !streamref )
                {
                    //-------------------------------------------------------------
                    // Reached end of file
                    //-------------------------------------------------------------
                    break;
                }
                try
                {
                    INT_4U fsi_id( GetFSIId( streamref->GetClass( ) ) );

                    if ( fsi_id == StopOnClassId )
                    {
                        //-----------------------------------------------------------
                        // Stop on this object. Rewind to start of StreamRef
                        //-----------------------------------------------------------
                        seekg( -( ReferenceStreamRef( )->Bytes( *this ) ),
                               std::ios::cur );
                        break;
                    }
                    //-------------------------------------------------------------
                    // See if object is needed
                    //-------------------------------------------------------------
                    if ( ( removeRecursiveDependant( streamref.get( ) ) ==
                           false ) &&
                         ( fsi_id != ( FrameSpec::Info::FSI_FR_SH ) ) )
                    {
                        //-----------------------------------------------------------
                        // Advance to the next object
                        //-----------------------------------------------------------
                        struct_end += streamref->GetLength( );
                        seekg( struct_end, std::ios::beg );
                        continue;
                    }
                    //-------------------------------------------------------------
                    // Object is needed
                    //-------------------------------------------------------------
                    if ( m_reading_frame_subset &&
                         ( fsi_id == FrameSpec::Info::FSI_FR_RAW_DATA ) )
                    {
                        m_log_recursive_dependants = false;
                    }
                    obj = readObject( streamref.get( ) );
                    if ( obj )
                    {
                        if ( obj->GetClass( ) == FrameSpec::Info::FSI_FR_SH )
                        {
                            continue;
                        }
                        m_dictionary.Ref( streamref, obj );
                        if ( ( obj->GetClass( ) ==
                               FrameSpec::Info::FSI_FR_VECT ) &&
                             ( Decompress ) )
                        {
                            boost::shared_ptr< FrVect > vect =
                                boost::dynamic_pointer_cast< FrVect >( obj );

                            vect->CompressData( Compression::MODE_RAW, 0 );
                        }
                    }
                    if ( m_reading_frame_subset &&
                         ( fsi_id == FrameSpec::Info::FSI_FR_RAW_DATA ) )
                    {
                        m_log_recursive_dependants = true;
                    }
                }
                catch ( const Common::VerifyException& E )
                {
                    if ( E.ErrorCode( ) ==
                         Common::VerifyException::CHECKSUM_ERROR )
                    {
                        //-----------------------------------------------------------
                        // Checksum errors need to be reported instead of
                        // quietly
                        //   being ignored.
                        //-----------------------------------------------------------
                        throw;
                    }
                    //-------------------------------------------------------------
                    // Seek to the end of this unknown class
                    //-------------------------------------------------------------
                    struct_end += streamref->GetLength( );
                    seekg( struct_end, std::ios::beg );
                }
                catch ( ... )
                {
                    //-------------------------------------------------------------
                    // Seek to the end of this unknown class
                    //-------------------------------------------------------------
                    struct_end += streamref->GetLength( );
                    seekg( struct_end, std::ios::beg );
                }
            }

            //-----------------------------------------------------------------
            // Glue the pieces together
            //-----------------------------------------------------------------
            Resolve( );
            m_to_be_resolved.clear( );
        }

        void
        IFrameStream::addRecursiveDependant( ptr_struct_base_type Ref )
        {
            if ( Ref && m_log_recursive_dependants )
            {
                INT_8U key( Ref->Instance( ) |
                            ( INT_8U( Ref->Class( ) ) << 32 ) );
                if ( key )
                {
                    m_to_be_resolved.insert( key );
                }
            }
        }

        void
        IFrameStream::addRecursiveDependant(
            const StreamRefInterface* StreamRef )
        {
            if ( StreamRef && m_log_recursive_dependants )
            {
                INT_8U key( StreamRef->GetInstance( ) |
                            ( INT_8U( StreamRef->GetClass( ) ) << 32 ) );

                if ( key )
                {
                    m_to_be_resolved.insert( key );
                }
            }
        }

        bool
        IFrameStream::removeRecursiveDependant(
            const StreamRefInterface* StreamRef )
        {
            bool retval = false;
            if ( StreamRef )
            {
                INT_8U key( StreamRef->GetInstance( ) |
                            ( INT_8U( StreamRef->GetClass( ) ) << 32 ) );

                if ( key && ( m_to_be_resolved.erase( key ) > 0 ) )
                {
                    retval = true;
                }
            }

            return retval;
        }

        void
        IFrameStream::pushResolver( resolver_type Resolver )
        {
            //-----------------------------------------------------------------
            // Let the base class use the information first
            //-----------------------------------------------------------------
            IStream::pushResolver( Resolver );
            //-----------------------------------------------------------------
            // Thing that need to be done locally
            //-----------------------------------------------------------------
            if ( Resolver )
            {
                addRecursiveDependant( Resolver->PtrStruct( ) );
            }
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_ADC_DATA, std::string >(
            const FrTOC* TOC, INT_4U Frame, const std::string& Channel ) const
        {
            return TOC->positionADC( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_ADC_DATA, INT_4U >(
            const FrTOC* TOC, INT_4U Frame, const INT_4U& Channel ) const
        {
            return TOC->positionADC( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_EVENT, std::string >(
            const FrTOC* TOC, INT_4U Frame, const std::string& Channel ) const
        {
            return TOC->positionEvent( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_PROC_DATA,
                                std::string >(
            const FrTOC* TOC, INT_4U Frame, const std::string& Channel ) const
        {
            return TOC->positionProc( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_PROC_DATA, INT_4U >(
            const FrTOC* TOC, INT_4U Frame, const INT_4U& Channel ) const
        {
            return TOC->positionProc( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_SER_DATA, std::string >(
            const FrTOC* TOC, INT_4U Frame, const std::string& Channel ) const
        {
            return TOC->positionSer( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_SIM_DATA, std::string >(
            const FrTOC* TOC, INT_4U Frame, const std::string& Channel ) const
        {
            return TOC->positionSim( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position< FrameSpec::Info::FSI_FR_SIM_EVENT,
                                std::string >(
            const FrTOC* TOC, INT_4U Frame, const std::string& Channel ) const
        {
            return TOC->positionSimEvent( Frame, Channel );
        }

        template <>
        INT_8U
        IFrameStream::position_index< FrameSpec::Info::FSI_FR_EVENT,
                                      std::string >( const FrTOC*       TOC,
                                                     const std::string& Channel,
                                                     INT_4U Index ) const
        {
            return TOC->positionEvent( Channel, Index );
        }

        template <>
        INT_8U
        IFrameStream::position_index< FrameSpec::Info::FSI_FR_SIM_EVENT,
                                      std::string >( const FrTOC*       TOC,
                                                     const std::string& Channel,
                                                     INT_4U Index ) const
        {
            return TOC->positionSimEvent( Channel, Index );
        }

        template < INT_4U ClassType, typename ChannelType >
        IFrameStream::object_type
        IFrameStream::advance_to_fr_struct( INT_4U Frame, ChannelType Channel )
        {
            object_type  retval;
            const FrTOC* toc( dynamic_cast< const FrTOC* >( GetTOC( ) ) );

            //-----------------------------------------------------------------
            // Position within the file where the frame starts
            //-----------------------------------------------------------------
            if ( toc )
            {
                //---------------------------------------------------------------
                // Fast seek implementation via table of contents.
                //---------------------------------------------------------------
                const std::streampos offset =
                    position< ClassType >( toc, Frame, Channel );
                if ( offset == (std::streampos)0 )
                {
                    std::ostringstream msg;

                    msg << "channel: " << Channel
                        << " does not exist in frame: " << Frame;
                    throw std::range_error( msg.str( ) );
                }

                clear( );
                seekg( offset, std::ios::beg );
                retval = Read( );
                while (
                    good( ) && retval.get( ) &&
                    ( ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SE ) ||
                      ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SH ) ) )
                {
                    //-------------------------------------------------------------
                    // Ignore structure definition components
                    //-------------------------------------------------------------
                    retval = Read( );
                }
                if ( !good( ) )
                {
                    std::ostringstream msg;
                    msg << "After reading object of type: "
                        << frameSpecInfo( )
                               .FrameObject(
                                   FrameSpec::Info::frame_object_types(
                                       ClassType ) )
                               ->ObjectStructName( )
                        << " stream is no longer in good state."
                        << " ( Frmae: " << Frame << " ChannelType: " << Channel
                        << " offset: " << offset << ")";
                    throw std::runtime_error( msg.str( ) );
                }
                if ( ( !retval ) || ( retval->GetClass( ) != ClassType ) )
                {
                    std::ostringstream msg;

                    msg << "The object named: " << Channel << " at location "
                        << offset << " is not a "
                        << frameSpecInfo( )
                               .FrameObject(
                                   FrameSpec::Info::frame_object_types(
                                       ClassType ) )
                               ->ObjectStructName( )
                        << " structure.";
                    if ( retval.get( ) )
                    {
                        msg << " It is instead of type: "
                            << retval->ObjectStructName( );
                    }
                    else
                    {
                        msg << " It is instead of type: NULL";
                    }
                    throw std::runtime_error( msg.str( ) );
                }
            }
            else
            {
                //---------------------------------------------------------------
                // :TODO: Slower implementation by reading forward in file
                //---------------------------------------------------------------
                throw Unimplemented(
                    "Common::IFrameStreamWrapper::advanceToFrProcData(non-toc)",
                    streamVersion( ),
                    __FILE__,
                    __LINE__ );
            }

            //-----------------------------------------------------------------
            // Return the information to the caller
            //-----------------------------------------------------------------
            return retval;
        }

        template < INT_4U ClassType, typename ChannelType >
        IFrameStream::object_type
        IFrameStream::advance_to_indexed_fr_struct( ChannelType Channel,
                                                    INT_4U      Index )
        {
            object_type  retval;
            const FrTOC* toc( dynamic_cast< const FrTOC* >( GetTOC( ) ) );

            //-----------------------------------------------------------------
            // Position within the file where the frame starts
            //-----------------------------------------------------------------
            if ( toc )
            {
                //---------------------------------------------------------------
                // Fast seek implementation via table of contents.
                //---------------------------------------------------------------
                const std::streampos offset =
                    position_index< ClassType >( toc, Channel, Index );
                if ( offset == (std::streampos)0 )
                {
                    std::ostringstream msg;

                    msg << "channel: " << Channel
                        << " does not exist at index: " << Index;
                    throw std::range_error( msg.str( ) );
                }

                clear( );
                seekg( offset, std::ios::beg );
                retval = Read( );
                while (
                    good( ) && retval.get( ) &&
                    ( ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SE ) ||
                      ( retval->GetClass( ) == FrameSpec::Info::FSI_FR_SH ) ) )
                {
                    //-------------------------------------------------------------
                    // Ignore structure definition components
                    //-------------------------------------------------------------
                    retval = Read( );
                }
                if ( !good( ) )
                {
                    std::ostringstream msg;
                    msg << "After reading object of type: "
                        << frameSpecInfo( )
                               .FrameObject(
                                   FrameSpec::Info::frame_object_types(
                                       ClassType ) )
                               ->ObjectStructName( )
                        << " stream is no longer in good state."
                        << " ( ChannelType: " << Channel << " Index: " << Index
                        << " file position offset: " << offset << ")";
                    throw std::runtime_error( msg.str( ) );
                }
                if ( ( !retval ) || ( retval->GetClass( ) != ClassType ) )
                {
                    std::ostringstream msg;

                    msg << "The object named: " << Channel << " at location "
                        << offset << " is not a "
                        << frameSpecInfo( )
                               .FrameObject(
                                   FrameSpec::Info::frame_object_types(
                                       ClassType ) )
                               ->ObjectStructName( )
                        << " structure.";
                    if ( retval.get( ) )
                    {
                        msg << " It is instead of type: "
                            << retval->ObjectStructName( );
                    }
                    else
                    {
                        msg << " It is instead of type: NULL";
                    }
                    throw std::runtime_error( msg.str( ) );
                }
            }
            else
            {
                //---------------------------------------------------------------
                // :TODO: Slower implementation by reading forward in file
                //---------------------------------------------------------------
                throw Unimplemented(
                    "Common::IFrameStreamWrapper::advanceToFrProcData(non-toc)",
                    streamVersion( ),
                    __FILE__,
                    __LINE__ );
            }

            //-----------------------------------------------------------------
            // Return the information to the caller
            //-----------------------------------------------------------------
            return retval;
        }

        template < INT_4U ClassType, typename ChannelType >
        IFrameStream::object_type
        IFrameStream::read_fr_struct( INT_4U      Frame,
                                      ChannelType Channel,
                                      bool        Recursively )
        try
        {
            //-----------------------------------------------------------------
            // Prep for errors
            //-----------------------------------------------------------------
            reset( );
            //-----------------------------------------------------------------
            // Advance to the requested frame within the stream
            //-----------------------------------------------------------------
            object_type retval;

            m_logNextPtr = false;
            try
            {
                retval = advance_to_fr_struct< ClassType >( Frame, Channel );
            }
            catch ( ... )
            {
                m_logNextPtr = true;
                throw;
            }
            m_logNextPtr = true;
            if ( Recursively && retval )
            {
                //---------------------------------------------------------------
                // Read until all references are resolved or until FrEndOfFrame
                //   is reached
                //---------------------------------------------------------------
                readRecursive( FrameSpec::Info::FSI_FR_NULL );
            }
            //-----------------------------------------------------------------
            // Return the results
            //-----------------------------------------------------------------
            return retval;
        }
        catch ( ... )
        {
            ERROR_HANDLER( );
        }

        template < INT_4U ClassType, typename ChannelType >
        IFrameStream::object_type
        IFrameStream::read_indexed_fr_struct( ChannelType Channel,
                                              INT_4U      Index,
                                              bool        Recursively )
        try
        {
            //-----------------------------------------------------------------
            // Prep for errors
            //-----------------------------------------------------------------
            reset( );
            //-----------------------------------------------------------------
            // Advance to the requested frame within the stream
            //-----------------------------------------------------------------
            object_type retval;

            m_logNextPtr = false;
            try
            {
                retval =
                    advance_to_indexed_fr_struct< ClassType >( Channel, Index );
            }
            catch ( ... )
            {
                m_logNextPtr = true;
                throw;
            }
            m_logNextPtr = true;
            if ( Recursively && retval )
            {
                //---------------------------------------------------------------
                // Read until all references are resolved or until FrEndOfFrame
                //   is reached
                //---------------------------------------------------------------
                readRecursive( FrameSpec::Info::FSI_FR_NULL );
            }
            //-----------------------------------------------------------------
            // Return the results
            //-----------------------------------------------------------------
            return retval;
        }
        catch ( ... )
        {
            ERROR_HANDLER( );
        }

        //===================================================================
        //===================================================================
        bool
        OFrameStream::detector_cmp::operator( )( fr_detector_type S1,
                                                 fr_detector_type S2 ) const
        {
            return ( S1.get( ) < S2.get( ) );
        }

        //===================================================================
        //
        //===================================================================
        OFrameStream::OFrameStream( buffer_type* Buffer, INT_2U Version )
            : FrameStream( Version, false /* AllowZeroVersion */ ),
              OStream( Buffer ), m_toc_location( 0 ), m_frame_count( 0 ),
              m_has_header( false )
        {
            version_stream( Version );
            init( );
        } // method OFrameStream::OFrameStream

        OFrameStream::OFrameStream( buffer_type* Buffer,
                                    INT_2U       StreamVersion,
                                    INT_2U       MemoryVersion )
            : FrameStream( MemoryVersion, false /* AllowZeroVersion */ ),
              OStream( Buffer ), m_toc_location( 0 ), m_frame_count( 0 ),
              m_has_header( false )
        {
            version_stream( StreamVersion );
            init( );
        } // method OFrameStream::OFrameStream

        OFrameStream::OFrameStream( bool         AutoDelete,
                                    buffer_type* Buffer,
                                    INT_2U       Version )
            : FrameStream( Version, false /* AllowZeroVersion */ ),
              OStream( Buffer, AutoDelete ), m_toc_location( 0 ),
              m_frame_count( 0 ), m_has_header( false )
        {
            version_stream( Version );
            init( );
        } // method OFrameStream::OFrameStream

        OFrameStream::~OFrameStream( )
        {
            Close( );
        } // method - OFrameStream::~OFrameStream

        void
        OFrameStream::Close( )
        {
            if ( !m_closed_state )
            {
                //---------------------------------------------------------------
                // Need to write out the table of contents.
                //---------------------------------------------------------------
                if ( m_toc )
                {
                    Write( m_toc );
                }
                //---------------------------------------------------------------
                // Mark the end of file
                //---------------------------------------------------------------
                object_type fr_end_of_file(
                    frameSpecInfo( )
                        .FrameObject( FrameSpec::Info::FSI_FR_END_OF_FILE )
                        ->Clone( ) );
                Write( fr_end_of_file );
                m_closed_state = true;
                flush( );
            }
        } // method - OFrameStream::Close()

        void
        OFrameStream::Write( object_type Obj )
        {
            if ( m_closed_state || ( !Obj ) )
            {
                return;
            }
#if 0
            if ( streamVersion( ) < Obj->FrameSpecVersion( ) )
            {
                Obj = FrameSpec::Object::DemoteObject(
                    streamVersion( ), // Stream Version
                    Obj, // Object
                    this );
            }
#endif /* 0 */
            if ( ( m_has_header == false ) &&
                 ( Obj->GetClass( ) !=
                   FrameSpec::class_type( FrameSpec::Info::FSI_FR_HEADER ) ) )
            {
                //---------------------------------------------------------------
                // Write the header to the file
                //---------------------------------------------------------------
                FrameSpec::ObjectInterface::unique_object_type frheader(
                    frameSpecInfo( )
                        .FrameObject( FrameSpec::Info::FSI_FR_HEADER )
                        ->Clone( ) );
                FrHeader* base = dynamic_cast< FrHeader* >( frheader.get( ) );
                if ( base )
                {
                    //-------------------------------------------------------------
                    // Need to setup some fields
                    //-------------------------------------------------------------
                    base->SetOriginator( "IGWD" );
                    base->SetVersion( frameSpecInfo( ).Version( ) );
                    base->SetLibraryVersion( frameSpecInfo( ).VersionMinor( ) );
                }
                //---------------------------------------------------------------
                // Write the object to the stream.
                //---------------------------------------------------------------
                frheader->Write( *this );
                setFrHeader( frheader );
                m_has_header = true;
            }

#if 0
      INT_4S instance_id = -1;
#endif /* 0 */

            //-----------------------------------------------------------------
            // Prepare for the case where a new FrVect will be created
            //-----------------------------------------------------------------
            object_type dictionary_obj( Obj );

            //-----------------------------------------------------------------
            // Compress compressable objects only if requested.
            //-----------------------------------------------------------------
            if ( ( Obj->GetClass( ) == FrameSpec::Info::FSI_FR_VECT ) &&
                 ( ( CompressionScheme( ) & 0xFF ) != Compression::MODE_RAW ) )

            {
                //---------------------------------------------------------------
                /// \todo Check that the compression mode is supported by
                ///       by the writer
                //---------------------------------------------------------------
                boost::shared_ptr< FrVect > vect =
                    boost::dynamic_pointer_cast< FrVect >( Obj );

                if ( vect &&
                     ( ( vect->Compression( ) & 0xFF ) ==
                       Compression::MODE_RAW ) )
                {
                    FrameSpec::Object* cv = vect->CloneCompressed(
                        CompressionScheme( ), CompressionLevel( ) );
                    if ( cv && ( cv != Obj.get( ) ) )
                    {
                        Obj.reset( cv ); // Reset the obj to be written
                    }
                }
            }

            //-----------------------------------------------------------------
            // Add object to TOC
            //-----------------------------------------------------------------
            std::streampos      start = tellp( );
            cmn_streamsize_type b = 0;
            /* cmn_streamsize_type	ob = Obj->Bytes( *this ); */

            //-----------------------------------------------------------------
            // Create and register reference for the object
            //-----------------------------------------------------------------
            if ( Obj->GetClass( ) > 0 )
            {
                Dictionary::streamref_type ce;

                ce = m_dictionary.RefCreate( dictionary_obj, *this );
                if ( ce )
                {
#if 0
	  instance_id = ce->GetInstance( );
#endif /* 0 */
                    bool wrote_description = false;

                    if ( ( m_defined.find( Obj->GetClass( ) ) ==
                           m_defined.end( ) ) &&
                         ( Obj->GetDescription( *this ) ) )
                    {
                        Obj->GetDescription( *this )->Write( *this );
                        m_defined.insert( Obj->GetClass( ) );
                        wrote_description = true;
                    }
                    b += ce->Bytes( *this );
                    start = tellp( );
                    b = ce->Bytes( *this ) + Obj->Bytes( *this );
                    ce->SetLength( b );
                    //-------------------------------------------------------------
                    // Check if the object to be written is a frame header
                    // object
                    //   and that a description for the object was written.
                    //-------------------------------------------------------------
                    if ( ( Obj->GetClass( ) == FrameSpec::Info::FSI_FRAME_H ) &&
                         ( wrote_description ) )
                    {
                        CheckSumFilter* sum = GetCheckSumFrame( );
                        if ( sum )
                        {
                            //---------------------------------------------------------
                            // Need to reset the frame checksum since it
                            // currently
                            //   contains the checksum of the description of the
                            //   frame header object.
                            //---------------------------------------------------------
                            SetCheckSumFrame( sum->Type( ) );
                        }
                    }
                    //-------------------------------------------------------------
                    // Starting with version 8, the checksum can be at the level
                    // of the individual objects.
                    //-------------------------------------------------------------
                    if ( ( Version( ) >= 8 ) //
                         && GetCheckSumObject( ) //
                         && GetCheckSumObject( )->GetChecksum( ) )
                    {
                        GetCheckSumObject( )->GetChecksum( )->Reset( );
                    }
                    ce->Write( *this );
                }
            }
            //-----------------------------------------------------------------
            // Write object to the stream
            //-----------------------------------------------------------------
            if ( Obj->GetClass( ) == FrameSpec::Info::FSI_FRAME_H )
            {
                ++m_frame_count;
            }
            else if ( Obj->GetClass( ) == FrameSpec::Info::FSI_FR_TOC )
            {
                m_toc_location = start;
            }
            if ( b == 0 )
            {
                b = Obj->Bytes( *this );
            }
            Obj->Write( *this );
            if ( ( Obj->GetClass( ) == FrameSpec::Info::FSI_FR_END_OF_FRAME ) ||
                 ( Obj->GetClass( ) == FrameSpec::Info::FSI_FR_END_OF_FILE ) )
            {
                m_dictionary.ResetInstanceCounts( );
            }
            //-----------------------------------------------------------------
            // Add object to table of contents
            //-----------------------------------------------------------------
            if ( m_toc )
            {
                m_toc->IndexObject( Obj, start );
            }
            //-----------------------------------------------------------------
            // Check the number of bytes written
            //-----------------------------------------------------------------
            std::streampos end = tellp( );
            if ( end != ( start + std::streampos( b ) ) )
            {
                std::ostringstream msg;

                msg << "Wrote " << ( end - start )
                    << " bytes to the stream instead of " << b
                    << " bytes for class: " << Obj->GetClass( );
                const Description* d( Obj->GetDescription( ) );
                if ( d )
                {
                    msg << " (" << d->GetName( ) << ")";
                }
                msg << " [" << ErrnoMessage( ) << "]";
                throw std::length_error( msg.str( ) );
            }
            if ( Obj->GetClass( ) == FrameSpec::Info::FSI_FR_DETECTOR )
            {
                //---------------------------------------------------------------
                // For detectors, need to check for FrStatData that needs to
                //  be outputted.
                //---------------------------------------------------------------
                fr_detector_type detector = boost::dynamic_pointer_cast<
                    fr_detector_type::element_type >( Obj );
                fr_stat_container_type::iterator fr_stat(
                    m_fr_stats.find( detector ) );
                if ( fr_stat != m_fr_stats.end( ) )
                {
                    //-------------------------------------------------------------
                    // Here we put the FrStatData structures onto the print
                    // stack
                    //   since there is a requirement that the first must appear
                    //   after the detector that it references.
                    // The spec is silent on what to do about the others so just
                    //   put them all after the detector they reference.
                    //-------------------------------------------------------------
                    PushMulti( fr_stat->second.begin( ),
                               fr_stat->second.end( ),
                               fr_stat->second.size( ) );
                    //-------------------------------------------------------------
                    // Remove from the list
                    /// \todo
                    ///  Need to ensure that all FrStatData structures have
                    ///  been written to the stream. This is to verify that
                    ///  that an FrDetector structure has been associated
                    ///  with each FrStatData.
                    //-------------------------------------------------------------
                    m_fr_stats.erase( fr_stat );
                } // if ( fr_stats != m_fr_stats.end( ) )
            } // if ( Obj->GetClass( ) == FrameSpec::Info::FSI_FR_DETECTOR )
        } // method - OFrameStream::Write( )

        void
        OFrameStream::WriteFrame( object_type FrameObject,
                                  chkSum_type FrameChecksumType )
        {
            if ( FrameObject->GetClass( ) != FrameSpec::Info::FSI_FRAME_H )
            {
                std::ostringstream msg;

                msg << "WriteFramme called with an object not of class FrameH";

                throw std::invalid_argument( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Setup for calculating checksum for frame
            //-----------------------------------------------------------------
            this->SetCheckSumFrame( CheckSum::kind_type( FrameChecksumType ) );
            PushSingle( FrameObject );
            write_stream_objects( );
        }

        void
        OFrameStream::WriteFrame( object_type FrameObject,
                                  INT_2U      CompressionScheme,
                                  INT_2U      CompressionLevel,
                                  chkSum_type FrameChecksumType )
        {
            Compression( CompressionScheme, CompressionLevel );
            try
            {
                WriteFrame( FrameObject, FrameChecksumType );
            }
            catch ( ... )
            {
                //---------------------------------------------------------------
                // Reset stream to initial state
                //---------------------------------------------------------------
                Compression( CompressionScheme, CompressionLevel );
                //---------------------------------------------------------------
                // Rethrow exception
                //---------------------------------------------------------------
                throw;
            }
            //-----------------------------------------------------------------
            // Reset stream to initial state
            //-----------------------------------------------------------------
            Compression( CompressionScheme, CompressionLevel );
        }

        void
        OFrameStream::WriteFrStatData( object_type Object )
        {
            //-----------------------------------------------------------------
            // Ensure the object is of the appropriate data type
            //-----------------------------------------------------------------
            fr_stat_data_type fr_stat_data =
                boost::dynamic_pointer_cast< fr_stat_data_type::element_type >(
                    Object );
            if ( !fr_stat_data )
            {
                std::ostringstream msg;

                msg << "";
                throw std::domain_error( msg.str( ) );
            }
            m_fr_stats[ fr_stat_data->GetDetector( ) ].push_back(
                fr_stat_data );
        }

        void
        OFrameStream::init( )
        {
            //-----------------------------------------------------------------
            // Ensure buffering on the stream
            //-----------------------------------------------------------------
            rdbuf( )->buffer( );
            seekp( 0 ); // To go the beginning of the file.

            //-----------------------------------------------------------------
            // Create a table of contents structure
            //-----------------------------------------------------------------
            try
            {
                const FrTOC* toc =
                    dynamic_cast< const FrTOC* >( frameSpecInfo( ).FrameObject(
                        FrameSpec::Info::FSI_FR_TOC ) );
                if ( toc )
                {
                    object_type obj( toc->Create( ) );
                    m_toc = boost::dynamic_pointer_cast< FrTOC >( obj );
                }
            }
            catch ( ... )
            {
                //---------------------------------------------------------------
                // Quietly ignore if TOC is not supported.
                //---------------------------------------------------------------
            }
            //-----------------------------------------------------------------
            // By default, generate the file checksum
            //-----------------------------------------------------------------
            SetCheckSumFile( CheckSum::CRC );
            if ( frameSpecInfo( ).Version( ) >= 8 )
            {
                //---------------------------------------------------------------
                // By default, generate a checksum per object
                //---------------------------------------------------------------
                SetCheckSumObject( CheckSum::CRC );
            }
        }

        void
        OFrameStream::write_stream_objects( )
        {
            object_type obj;

            while ( ( obj = Pop( ) ) )
            {
                Write( obj );
            }
        }

    } // namespace Common
} // namespace FrameCPP
