//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>
#include <stdexcept>

#include "ldastoolsal/reverse.hh"

#include "framecpp/Common/CheckSum.hh"

namespace FrameCPP
{
    namespace Common
    {
        //-------------------------------------------------------------------
        // CheckSum
        //-------------------------------------------------------------------

        CheckSum::CheckSum( const kind_type Type )
            : m_value( 0 ), m_type( Type )
        {
        }

        CheckSum::CheckSum( const CheckSum& Source )
            : m_value( Source.m_value ), m_type( Source.m_type )
        {
        }

        CheckSum::~CheckSum( )
        {
        }

        CheckSum*
        CheckSum::Create( kind_type Type )
        {
            switch ( Type )
            {
            case NONE:
                return (CheckSum*)NULL;
            case CRC:
                return new CheckSumCRC( );
            default:
            {
                std::ostringstream msg;

                msg << "Creation of unknown checksum type (" << Type
                    << ") requested.";
                throw std::range_error( msg.str( ) );
            }
            } // switch
        }

        CheckSum*
        CheckSum::Create( kind_type Type, value_type Value )
        {
            switch ( Type )
            {
            case NONE:
                return (CheckSum*)NULL;
            case CRC:
                return new CheckSumCRC( Value );
            default:
            {
                std::ostringstream msg;

                msg << "Creation of unknown checksum type (" << Type
                    << ") requested.";
                throw std::range_error( msg.str( ) );
            }
            } // switch
        }

        std::string
        CheckSum::FormatError( value_type ExpectedValue,
                               value_type CalculatedValue )
        {
            std::ostringstream msg;

            msg << "expected value: " << ExpectedValue << std::hex
                << std::showbase << "(" << ExpectedValue << ")" << std::dec
                << std::noshowbase << " calculated value: " << CalculatedValue
                << std::hex << std::showbase << "(" << CalculatedValue << ")"
                << std::dec << std::noshowbase;

            return msg.str( );
        }

        void
        CheckSum::Reset( )
        {
            m_value = 0;
        }

        //-------------------------------------------------------------------
        // CheckSumCRC
        //-------------------------------------------------------------------

        CheckSumCRC::CheckSumCRC( )
            : CheckSum( CheckSum::CRC ), m_active( true )
        {
        }

        CheckSumCRC::CheckSumCRC( value_type Value )
            : CheckSum( CheckSum::CRC ), m_active( false )
        {
            m_value = Value;
        }

        CheckSumCRC::CheckSumCRC( const CheckSumCRC& Source )
            : CheckSum( Source ), crc_generator( Source.crc_generator ),
              m_active( Source.m_active )
        {
        }

        CheckSumCRC*
        CheckSumCRC::Clone( ) const
        {
            return new CheckSumCRC( *this );
        }

        void
        CheckSumCRC::Reset( )
        {
            m_active = true;
            CheckSum::Reset( );
            crc_generator.reset( );
        }

        void
        CheckSumCRC::calc( const void* Buffer, size_type NBytes )
        {
            if ( m_active == false )
            {
                return;
            }
            if ( Buffer == (CHAR_U*)NULL )
            {
                throw std::invalid_argument(
                    "CheckSumCRC::calc(): Buffer must not be NULL" );
            }
            crc_generator.process_bytes( Buffer, NBytes );
        }

        void
        CheckSumCRC::finish( )
        {
            if ( m_active )
            {
                m_active = false;
                m_value = crc_generator.checksum( );
            }
        }
    } // namespace Common
} // namespace FrameCPP
