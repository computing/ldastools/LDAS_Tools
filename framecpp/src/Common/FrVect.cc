//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_array.hpp>

#include "framecpp/Common/FrVect.hh"

namespace FrameCPP
{
    namespace Common
    {
        void
        FrVect::compressToBuffer(
            const Compression::compress_type_mapping& CompressionMapping,
            const Compression::compress_type_reverse_mapping&
                                                  CompressionReverseMapping,
            const Compression::data_type_mapping& DataTypeMapping,
            const INT_4U                          DataType,
            const INT_8U                          NData,
            const CHAR_U*                         Source,
            const INT_8U                          SourceNBytes,
            const INT_4U                          SourceCompressionMode,
            boost::shared_array< CHAR_U >&        Dest,
            INT_8U&                               DestNBytes,
            INT_4U&                               DestCompressionMode,
            const INT_4U                          Level ) const
        {
            boost::shared_array< CHAR_U > expanded_buffer;

            const INT_8U nData( NData );
            INT_8U       nBytes( SourceNBytes );

#if WORKING
            //---------------------------------------------------------------------
            // Ensure the buffer is fully expanded
            /// \todo need to ensure that this is working for both endiannesses.
            //---------------------------------------------------------------------
            if ( ( SourceCompressionMode & 0xFF ) != Compression::MODE_RAW )
            {
                expandToBuffer( CompressionMapping,
                                DataTypeMapping,
                                DataType,
                                NData,
                                Source,
                                SourceNBytes,
                                SourceCompressionMode,
                                expanded_buffer,
                                nBytes );
            }
#endif /* WORKING */

            //-----------------------------------------------------------------
            // Now compress the data
            //-----------------------------------------------------------------

            INT_4U compress = ( DestCompressionMode & 0xFF );
            boost::shared_array< CHAR_U > data_out;
            INT_8U                        data_out_len( 0 );

            FrameCPP::Compression::Compress(
                compress,
                Level,
                CompressionMapping,
                CompressionReverseMapping,
                DataType,
                DataTypeMapping,
                ( expanded_buffer.get( ) ) // Have uncompressed buffer?
                    ? expanded_buffer.get( ) // Yes; use it
                    : Source, // No; use data buffer
                nData,
                nBytes,
                data_out,
                data_out_len );
            //-----------------------------------------------------------------
            // Final validation before returning to the caller
            //-----------------------------------------------------------------
            Dest = data_out;
            DestNBytes = data_out_len;

            if ( data_out_len == 0 )
            {
                Dest.reset( (CHAR_U*)NULL );
            }
            else
            {
                DestCompressionMode = compress;
            }
        } // method - FrVect::compressToBuffer

#if 0
    void FrVect::
    compressToBuffer( const Compression::compress_type_mapping&
		      CompressionMapping,
		      const Compression::compress_type_reverse_mapping&
		      CompressionReverseMapping,
		      const Compression::data_type_mapping&
		      DataTypeMapping,
		      const INT_4U DataType,
		      const INT_8U NData,
		      const CHAR_U* Source,
		      const INT_8U SourceNBytes,
		      const INT_4U SourceCompressionMode,
		      boost::shared_array< CHAR_U >& Dest,
		      INT_8U& DestNBytes,
		      INT_4U& DestCompressionMode,
		      const INT_4U Level ) const
    {
      boost::shared_array< CHAR_U > expanded_buffer;

      const INT_8U	nData( NData );
      INT_8U		nBytes( SourceNBytes );

#if WORKING
      //---------------------------------------------------------------------
      // Ensure the buffer is fully expanded
      /// \todo need to ensure that this is working for both endiannesses.
      //---------------------------------------------------------------------
      if ( ( SourceCompressionMode & 0xFF ) != Compression::MODE_RAW )
      {
	expandToBuffer( CompressionMapping,
			DataTypeMapping,
			DataType,
			NData,
			Source,
			SourceNBytes,
			SourceCompressionMode,
			expanded_buffer,
			nBytes );
      }
#endif /* WORKING */

      //-----------------------------------------------------------------
      // Now compress the data
      //-----------------------------------------------------------------

      INT_4U	compress = ( DestCompressionMode & 0xFF );
      CHAR_U*	data_out( (CHAR_U*)NULL );
      INT_8U	data_out_len( 0 );

      FrameCPP::Compression::
	Compress( compress,
		  Level,
		  CompressionMapping,
		  CompressionReverseMapping,
		  DataType,
		  DataTypeMapping,
		  ( expanded_buffer.get( ) )	// Have uncompressed buffer?
		  ? expanded_buffer.get( )	// Yes; use it
		  : Source,			// No; use data buffer
		  nData,
		  nBytes,
		  data_out,
		  data_out_len );
      //-----------------------------------------------------------------
      // Final validation before returning to the caller
      //-----------------------------------------------------------------
      Dest.reset( data_out );
      DestNBytes = data_out_len;
      
      if ( data_out_len == 0 )
      {
	Dest.reset( (CHAR_U*)NULL );
      }
      else
      {
	DestCompressionMode = compress;
      }
    } // method - FrVect::compressToBuffer
#endif /* 0 */

        void
        FrVect::expandToBuffer(
            const Compression::compress_type_mapping& CompressionMapping,
            const Compression::data_type_mapping&     DataTypeMapping,
            const INT_4U                              DataType,
            const INT_8U                              NData,
            const CHAR_U*                             Source,
            const INT_8U                              SourceNBytes,
            const INT_4U                              SourceCompressionMode,
            const INT_4U                              SourceByteOrder,
            boost::shared_array< CHAR_U >&            Dest,
            INT_8U&                                   DestNBytes ) const
        {
            //---------------------------------------------------------------------
            // Uncompress to local buffers, then use those buffers as
            //       input to the compression routines
            //---------------------------------------------------------------------
            boost::shared_array< CHAR_U > data_out;
            INT_8U                        data_out_len( 0 );

            FrameCPP::Compression::Expand(
                ( BYTE_ORDER_HOST == SourceByteOrder ),
                ( SourceCompressionMode & 0xFF ),
                CompressionMapping,
                DataType,
                DataTypeMapping,
                Source,
                NData,
                SourceNBytes,
                data_out,
                data_out_len );
            DestNBytes = data_out_len;
            if ( data_out )
            {
                Dest = data_out;
                if ( DestNBytes == 0 )
                {
                    //-----------------------------------------------------------------
                    // Deallocate the buffer since data_out_length shows a
                    // buffer of size zero
                    //-----------------------------------------------------------------
                    Dest.reset( (CHAR_U*)NULL );
                }
            }
            else
            {
                Dest.reset( (CHAR_U*)NULL );
                DestNBytes = 0;
            }
        } // method - FrVect::expandToBuffer

#if 0
    void FrVect::
    expandToBuffer( const Compression::compress_type_mapping&
		    CompressionMapping,
		    const Compression::data_type_mapping&
		    DataTypeMapping,
		    const INT_4U DataType,
		    const INT_8U NData,
		    const CHAR_U* Source,
		    const INT_8U SourceNBytes,
		    const INT_4U SourceCompressionMode,
		    const INT_4U SourceByteOrder,
		    boost::shared_array< CHAR_U >& Dest,
		    INT_8U& DestNBytes ) const
    {
      //---------------------------------------------------------------------
      // Uncompress to local buffers, then use those buffers as
      //       input to the compression routines
      //---------------------------------------------------------------------
      CHAR_U*	data_out( (CHAR_U*)NULL );
      INT_8U	data_out_len( 0 );

      FrameCPP::Compression::
	Expand( ( BYTE_ORDER_HOST == SourceByteOrder ),
		( SourceCompressionMode & 0xFF ),
		CompressionMapping,
		DataType,
		DataTypeMapping,
		Source,
		NData,
		SourceNBytes,
		data_out,
		data_out_len );
      DestNBytes = data_out_len;
      if ( data_out )
      {
	Dest.reset( data_out );
	if ( DestNBytes == 0 )
	{
	  //-----------------------------------------------------------------
	  // Deallocate the buffer since data_out_length shows a buffer
	  // of size zero
	  //-----------------------------------------------------------------
	  Dest.reset( (CHAR_U*)NULL );
	}
      }
      else
      {
	Dest.reset( (CHAR_U*)NULL );
	DestNBytes = 0;
      }
    } // method - FrVect::expandToBuffer
#endif /* 0 */
    } // namespace Common
} // namespace FrameCPP
