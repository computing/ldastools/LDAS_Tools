//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/FrameBuffer.hh"

#include <cassert>

using LDASTools::System::ErrnoMessage;

namespace FrameCPP
{
    namespace Common
    {
        //-------------------------------------------------------------------
        // Specialization of Open based on std::filebuf
        //-------------------------------------------------------------------
        template <>
        FrameBuffer< std::filebuf >*
        FrameBuffer< std::filebuf >::close( )
        {
            if ( std::filebuf::close( ) != (std::filebuf*)NULL )
            {
                bufferId( "" );
                return this;
            }
            return (buffer_type*)NULL;
        }

        template <>
        FrameBuffer< std::filebuf >*
        FrameBuffer< std::filebuf >::open( const std::string& Filename,
                                           std::ios::openmode Mode )
        {
            std::ostringstream msg;

            element_type::open( Filename.c_str( ), Mode );
            if ( element_type::is_open( ) == false )
            {
                msg << "Unable to open file: " << Filename << " ("
                    << ErrnoMessage( ) << ")";
                std::runtime_error err( msg.str( ).c_str( ) );
                throw err;
            }
            bufferId( Filename );
            return this;
        }

        template <>
        void
        FrameBuffer< std::filebuf >::UseMemoryMappedIO( bool Value )
        {
        }

    } // namespace Common

} // namespace FrameCPP

namespace FrameCPP
{
    namespace Common
    {
        //-------------------------------------------------------------------
        // Specialization of Open based on LDASTools::AL::std::filebuf
        //-------------------------------------------------------------------
        template <>
        void
        FrameBuffer< LDASTools::AL::filebuf >::UseMemoryMappedIO( bool Value )
        {
            LDASTools::AL::filebuf::UseMemoryMappedIO( Value );
        }

        template <>
        FrameBuffer< LDASTools::AL::filebuf >*
        FrameBuffer< LDASTools::AL::filebuf >::close( )
        {
            if ( LDASTools::AL::filebuf::close( ) !=
                 (LDASTools::AL::filebuf*)NULL )
            {
                bufferId( "" );
                return this;
            }
            return (buffer_type*)NULL;
        }

        template <>
        FrameBuffer< LDASTools::AL::filebuf >*
        FrameBuffer< LDASTools::AL::filebuf >::open(
            const std::string& Filename, std::ios::openmode Mode )
        {
            LDASTools::AL::filebuf::open( Filename.c_str( ), Mode );
            if ( LDASTools::AL::filebuf::is_open( ) == false )
            {
                std::ostringstream msg;

                msg << "Unable to open file: " << Filename << " ("
                    << ErrnoMessage( ) << ")";
                throw std::runtime_error( msg.str( ) );
            }
            bufferId( Filename );
            return this;
        }

    } // namespace Common

} // namespace FrameCPP

template class FrameCPP::Common::FrameBuffer< LDASTools::AL::filebuf >;
