//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP__COMMON__FRAME_BUFFER_ICC
#define FrameCPP__COMMON__FRAME_BUFFER_ICC

namespace FrameCPP
{
  namespace Common
  {
    template< typename BT >
    bool FrameBuffer< BT >::
    FilterInternally( ) const
    {
      return false;
    }

    template< typename BT >
    void FrameBuffer< BT >::
    mark( const char_type* Offset )
    {
      if ( FilterInternally( ) )
      {
        const typename BT::char_type* end = Offset;

        if ( Offset == (const char_type*)0 )
        {
          if ( m_mode & std::ios::in )
          {
            end = gptr( );
          }
          else if ( m_mode & std::ios::out )
          {
            end = ( pptr( ) == epptr( ) )
              ? pbase( )
              : pptr( );
          }
        }
#if LM_DEBUG_OUTPUT || LM_DEBUG_INPUT
        if ( filter_on )
        {
          std::cerr << "mark:"
                    << " end: " << (void*)end
                    << std::endl;
      }
#endif /* LM_DEBUG_OUTPUT || LM_DEBUG_INPUT */
        for( filters_type::const_iterator
               cur = m_filters.begin( ),
               last = m_filters.end( );
             cur != last;
             ++cur )
        {
          (*cur)->Offset( end );
        }
      } // if ( FilterInternally( ) )
    } // method - mark

  }

}

#endif /* FrameCPP__COMMON__FRAME_BUFFER_ICC */
