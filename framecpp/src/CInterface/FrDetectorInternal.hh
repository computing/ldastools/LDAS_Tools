//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_CPP_INTERFACE__FR_DETECTOR_INTERNAL_HH
#define FRAME_CPP_INTERFACE__FR_DETECTOR_INTERNAL_HH

#include <boost/shared_ptr.hpp>

#include "framecpp/FrDetector.hh"

namespace FrameC
{
    struct FrDetector : public Handle
    {
        typedef FrameCPP::FrDetector             data_core_t;
        typedef boost::shared_ptr< data_core_t > data_t;

        data_t m_data;

        FrDetector( );

        FrDetector( const char* restrict name,
                    const char* restrict         prefix,
                    fr_detector_latitude_t       latitude,
                    fr_detector_longitude_t      longitude,
                    fr_detector_elevation_t      elevation,
                    fr_detector_arm_x_azimuth_t  azimuth_x,
                    fr_detector_arm_y_azimuth_t  azimuth_y,
                    fr_detector_arm_x_altitude_t altitude_x,
                    fr_detector_arm_y_altitude_t altitude_y,
                    fr_detector_arm_x_midpoint_t midpoint_x,
                    fr_detector_arm_y_midpoint_t midpoint_y );

        FrDetector( const data_core_t& Source );

        virtual ~FrDetector( );

        void Read( fr_file_t* restrict Stream, const char* restrict Name );
    };

    template <>
    inline pointer_type
    PointerType( FrDetector* Value )
    {
        return POINTER_FR_DETECTOR;
    }

} /* namespace FrameC */

#endif /* FRAME_CPP_INTERFACE__FR_DETECTOR_INTERNAL_HH */
