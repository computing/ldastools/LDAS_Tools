//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_CPP_INTERFACE__FR_EVENT_INTERNAL_HH
#define FRAME_CPP_INTERFACE__FR_EVENT_INTERNAL_HH

#include <boost/shared_ptr.hpp>

namespace FrameC
{
    struct FrEvent : public Handle
    {
        typedef FrameCPP::FrEvent                data_core_t;
        typedef boost::shared_ptr< data_core_t > data_t;
#if WORKING
        typedef data_core_t::time_t time_t;
#endif /* WORKING */

        data_t m_data;

        FrEvent( );

        FrEvent( const fr_event_name_t         Name,
                 const fr_event_comment_t      Comment,
                 const fr_event_inputs_t       Inputs,
                 const fr_event_gtime_t        GTime,
                 const fr_event_time_before_t  TimeBefore,
                 const fr_event_time_after_t   TimeAfter,
                 const fr_event_event_status_t EventStatus,
                 const fr_event_amplitude_t    Amplitude,
                 const fr_event_probability_t  Probability,
                 const fr_event_statistics_t   Statistics,
                 const fr_event_n_param_t      NParam,
                 const fr_event_parameters_t*  Parameters );
    };

    template <>
    inline pointer_type
    PointerType( FrEvent* Value )
    {
        return POINTER_FR_EVENT;
    }

} // namespace FrameC

#endif /* FRAME_CPP_INTERFACE__FR_EVENT_INTERNAL_HH */
