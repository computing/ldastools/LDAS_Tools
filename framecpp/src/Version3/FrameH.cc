//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "ldastoolsal/types.hh"

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version3/FrameH.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"
#include "framecpp/Version3/FrEndOfFrame.hh"
#include "framecpp/Version3/PTR_STRUCT.hh"

using namespace FrameCPP::Version_3;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrameH::getStaticName( ) << " "       \
              << (void*)this << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrameH::getStaticName( ) << " "         \
              << (void*)this << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

FrameHNPS::FrameHNPS( )
{
}

FrameHNPS::FrameHNPS( const FrameHNPS& Source )
    : name( Source.name ), run( Source.run ), frame( Source.frame ),
      GTime( Source.GTime ), ULeapS( Source.ULeapS ), dt( Source.dt )
{
}

FrameHNPS::FrameHNPS( const std::string& Name,
                      run_type           Run,
                      frame_type         Frame,
                      const GTime_type&  GTimeValue,
                      ULeapS_type        ULeapSValue,
                      localTime_type     LocalTime,
                      const dt_type      Dt )
    : name( Name ), run( Run ), frame( Frame ), GTime( GTimeValue ),
      ULeapS( ULeapSValue ), localTime( LocalTime ), dt( Dt )
{
}

void
FrameHNPS::assign( assign_stream_type& Stream )
{
    Stream >> name >> run >> frame >> GTime >> ULeapS >> localTime >> dt;
}

//=======================================================================
/// Allocate resources using default values.
//=======================================================================
FrameH::FrameH( ) : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION )
{
    MEM_ALLOCATE( );
}

FrameH::FrameH( const FrameH& frame )
    : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION ),
      FrameHNPS( frame ), FrameHPS( frame )
{
}

FrameH::FrameH( const std::string& name,
                run_type           run,
                frame_type         frame,
                const GTime_type&  time,
                ULeapS_type        uLeapS,
                localTime_type     LocalTime,
                const dt_type      dt )
    : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION ),
      FrameHNPS( name, run, frame, time, uLeapS, LocalTime, dt )
{
    MEM_ALLOCATE( );
}

FrameH::FrameH( const FrameHNPS& Source )
    : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION ),
      FrameHNPS( Source )
{
}

FrameH::FrameH( istream_type& Stream )
    : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION )
{
    FrameHNPS::read( Stream );
    m_refs( Stream );
}

FrameH::~FrameH( )
{
    MEM_DELETE( );
}

FrameCPP::cmn_streamsize_type
FrameH::Bytes( const Common::StreamBase& Stream ) const
{
    return FrameHNPS::bytes( ) + m_refs.Bytes( );
}

FrameH*
FrameH::Clone( ) const
{
    return new FrameH( *this );
}

FrameH*
FrameH::Create( istream_type& Stream ) const
{
    return new FrameH( Stream );
}

const char*
FrameH::ObjectStructName( ) const
{
    return StructName( );
}

const Description*
FrameH::StructDescription( )
{
    static Description ret;

    if ( ret.size( ) == 0 )
    {
        ret( FrSH( FrameH::StructName( ),
                   FrameCPP::Common::FrameH::s_object_id,
                   "Frame Header Structure" ) );
        dataDescription( ret );
        refDescription( ret );
    }

    return &ret;
}

void
FrameH::Write( ostream_type& Stream ) const
{
#if WORKING
    //---------------------------------------------------------------------
    // Check to see if per frame checksums are to be calculated
    //---------------------------------------------------------------------
    if ( Stream.IsCalculatingChecksumFrame( ) )
    {
        //-------------------------------------------------------------------
        // Yes ...
        // 1) Reset the buffer
        // 2) Add in the FrBase info
        //-------------------------------------------------------------------
        Stream.SetChecksumMethodFrame( );

        INT_8U length = GetLength( );
        INT_2U class_id = GetClassId( );
        INT_4U instance = GetInstance( );

        Stream.calcChecksumFrame( &length, sizeof( length ) );
        Stream.calcChecksumFrame( &class_id, sizeof( class_id ) );
        Stream.calcChecksumFrame( &instance, sizeof( instance ) );
    }
#endif /* WORKING */

    FrameHNPS::write( Stream );
    m_refs( Stream );

    ostream_type::object_type obj( new FrEndOfFrame( GetRun( ), GetFrame( ) ) );

    Stream.PushSingle( obj );
}

void
FrameH::VerifyObject( Common::Verify&       Verifier,
                      Common::IFrameStream& Stream ) const
{
    if ( Verifier.ValidateMetadata( ) &&
         ( GetGTime( ).GetLeapSeconds( ) != GetULeapS( ) ) )
    {
        std::ostringstream msg;

        msg << "Bad ULeapS value ( for gpstime: " << GetGTime( )
            << " expected: " << GetGTime( ).GetLeapSeconds( )
            << " read: " << GetULeapS( ) << " )";
        throw Common::VerifyException(
            Common::VerifyException::METADATA_INVALID, msg.str( ) );
    }
}

bool
FrameH::operator==( const Common::FrameSpec::Object& Obj ) const
{
    return compare( *this, Obj );
}

FrameH::demote_ret_type
FrameH::demote( frame_spec_version_type Target,
                demote_arg_type         Obj,
                demote_stream_type*     Stream ) const
{
    if ( Target >= DATA_FORMAT_VERSION )
    {
        return Obj;
    }
    throw Unimplemented( "Object* FrameH::demote( Object* Obj ) const",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
}

FrameH::promote_ret_type
FrameH::promote( frame_spec_version_type Target,
                 promote_arg_type        Obj,
                 promote_stream_type*    Stream ) const
{
    return Promote( Target, Obj, Stream );
}

void
FrameH::readSubset( istream_type& Stream, INT_4U ElementMask )
{
    m_refs.readSubset( Stream, ElementMask );
}

void
FrameH::assign( assign_stream_type& Stream )
{
    FrameHNPS::assign( Stream );
}
