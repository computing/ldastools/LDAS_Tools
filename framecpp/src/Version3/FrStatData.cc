//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <algorithm>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version3/FrStatData.hh"
#include "framecpp/Version3/FrDetector.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"
#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_3
    {
        //===================================================================
        // FrStatData
        //===================================================================

        const INT_4U FrStatData::ALL_VERSIONS = 0xFFFFFFFF;
        const INT_4U FrStatData::LATEST_VERSION = 0xFFFFFFFE;

        FrStatData::FrStatData( )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrStatData::FrStatData( const FrStatData& Source )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION ),
              FrStatDataStorage( Source ), FrStatDataRefs( Source )
        {
        }

        FrStatData::FrStatData( const std::string& name,
                                const std::string& comment,
                                const INT_4U       timeStart,
                                const INT_4U       timeEnd,
                                const INT_4U       version )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION ),
              FrStatDataStorage( name, comment, timeStart, timeEnd, version )
        {
        }

        FrStatData::FrStatData( istream_type& Stream )
            : Common::FrStatData( StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );
        }

        FrameCPP::cmn_streamsize_type
        FrStatData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( ) + m_refs.Bytes( Stream );
        }

        FrStatData*
        FrStatData::Clone( ) const
        {
            return new FrStatData( *this );
        }

        FrStatData*
        FrStatData::Create( istream_type& Stream ) const
        {
            return new FrStatData( Stream );
        }

        const char*
        FrStatData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrStatData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrStatData::StructName( ),
                           FrStatData::STRUCT_ID,
                           "Static Data Structure" ) );

                FrStatDataStorage::data_type::Describe( ret );
                ref_type::Describe( ret );
            }

            return &ret;
        }

        void
        FrStatData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
        }

        FrStatData&
        FrStatData::Merge( const FrStatData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrStatData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrStatData::demote_ret_type
        FrStatData::demote( INT_2U              Target,
                            demote_arg_type     Obj,
                            demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            throw Unimplemented(
                "Object* FrStatData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrStatData::promote_ret_type
        FrStatData::promote( INT_2U               Target,
                             promote_arg_type     Obj,
                             promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

    } // namespace Version_3
} // namespace FrameCPP
