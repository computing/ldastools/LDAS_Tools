//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version3/FrAdcData.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"

#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_3;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using std::ceil;
using std::floor;

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_ADC_DATA;

//=======================================================================
//=======================================================================
const FrAdcDataNPS::bias_type FrAdcDataNPS::DEFAULT_BIAS =
    FR_ADC_DATA_DEFAULT_BIAS;
const FrAdcDataNPS::slope_type FrAdcDataNPS::DEFAULT_SLOPE =
    FR_ADC_DATA_DEFAULT_SLOPE;
const FrAdcDataNPS::fShift_type FrAdcDataNPS::DEFAULT_FSHIFT =
    FR_ADC_DATA_DEFAULT_FSHIFT;
const FrAdcDataNPS::timeOffsetS_type FrAdcDataNPS::DEFAULT_TIME_OFFSET_S =
    FR_ADC_DATA_DEFAULT_TIME_OFFSET_S;
const FrAdcDataNPS::timeOffsetN_type FrAdcDataNPS::DEFAULT_TIME_OFFSET_N =
    FR_ADC_DATA_DEFAULT_TIME_OFFSET_N;
const FrAdcDataNPS::overRange_type FrAdcDataNPS::DEFAULT_OVER_RANGE =
    FR_ADC_DATA_DEFAULT_OVER_RANGE;

FrAdcDataNPS::FrAdcDataNPS( )
    : bias( DEFAULT_BIAS ), slope( DEFAULT_SLOPE ),
      units( FR_ADC_DATA_DEFAULT_UNITS( ) ),
      timeOffsetS( DEFAULT_TIME_OFFSET_S ),
      timeOffsetN( DEFAULT_TIME_OFFSET_N ), fShift( DEFAULT_FSHIFT ),
      overRange( DEFAULT_OVER_RANGE )
{
}

FrAdcDataNPS::FrAdcDataNPS( Common::IStream& Stream )
{
    Stream >> name >> comment >> crate >> channel >> nBits >> bias >> slope >>
        units >> sampleRate >> timeOffsetS >> timeOffsetN >> fShift >>
        overRange;
}

void
FrAdcDataNPS::write( Common::OStream& Stream ) const
{
    Stream << name << comment << crate << channel << nBits << bias << slope
           << units << sampleRate << timeOffsetS << timeOffsetN << fShift
           << overRange;
}

//=======================================================================
//=======================================================================
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
FrAdcData::FrAdcData( )
    : FrameSpec::Object(
          s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
{
}

/// \cond ignore
FrAdcData::FrAdcData( const FrAdcData& Source )
    : nps_type( Source ),
      ps_type( Source ), FrameSpec::Object( s_object_id,
                                            StructDescription( ),
                                            DATA_FORMAT_VERSION )
{
}

FrAdcData::FrAdcData( const std::string& Name,
                      INT_4U             Crate,
                      INT_4U             Channel,
                      INT_4U             NBits,
                      REAL_8             SampleRate,
                      REAL_4             Bias,
                      REAL_4             Slope,
                      const std::string& Units,
                      REAL_8             FShift,
                      INT_4U             TimeOffsetS,
                      INT_4U             TimeOffsetN,
                      INT_2U             OverRange )
    : FrameSpec::Object(
          s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
{
    name = Name;
    crate = Crate;
    channel = Channel;
    nBits = NBits;
    sampleRate = SampleRate;
    bias = Bias;
    slope = Slope;
    units = Units;
    fShift = FShift;
    timeOffsetS = TimeOffsetS;
    timeOffsetN = TimeOffsetN;
    overRange = OverRange;
}

FrAdcData::FrAdcData( istream_type& Stream )
    : nps_type( Stream ),
      ps_type( Stream ), FrameSpec::Object( s_object_id,
                                            StructDescription( ),
                                            DATA_FORMAT_VERSION )
{
    Stream.Next( this );
}

FrAdcData::~FrAdcData( )
{
}

const std::string&
FrAdcData::GetNameSlow( ) const
{
    return GetName( );
}

FrameCPP::cmn_streamsize_type
FrAdcData::Bytes( const Common::StreamBase& Stream ) const
{
    return nps_type::bytes( Stream ) + ps_type::Bytes( Stream ) +
        Stream.PtrStructBytes( ) // next
        ;
}

FrAdcData*
FrAdcData::Create( istream_type& Stream ) const
{
    return new FrAdcData( Stream );
}

FrAdcData&
FrAdcData::Merge( const FrAdcData& RHS )
{
    /// \todo
    ///   Need to implement Merge routine
    std::string msg( "Merge currently not implemented for " );
    msg += StructName( );

    throw std::domain_error( msg );
    return *this;
}

const char*
FrAdcData::ObjectStructName( ) const
{
    return StructName( );
}

const Description*
FrAdcData::StructDescription( )
{
    static Description ret;

    if ( ret.size( ) == 0 )
    {
        ret( FrSH( FrAdcData::StructName( ),
                   s_object_id,
                   "AdcData  Data Structure" ) );

        ret( FrSE( "name", "STRING", "" ) );
        ret( FrSE( "comment", "STRING", "" ) );
        ret( FrSE( "crate", "INT_4U", "" ) );
        ret( FrSE( "channel", "INT_4U", "" ) );
        ret( FrSE( "nBits", "INT_4U", "" ) );
        ret( FrSE( "bias", "REAL_4", "" ) );
        ret( FrSE( "slope", "REAL_4", "" ) );
        ret( FrSE( "units", "STRING", "" ) );
        ret( FrSE( "sampleRate", "REAL_8", "" ) );
        ret( FrSE( "timeOffsetS", "INT_4U", "" ) );
        ret( FrSE( "timeOffsetN", "INT_4U", "" ) );
        ret( FrSE( "fShift", "REAL_8", "" ) );
        ret( FrSE( "overRange", "INT_2U", "" ) );

        ret( FrSE( "data", PTR_STRUCT::Desc( FrVect::StructName( ) ), "" ) );
        ret( FrSE( "aux", PTR_STRUCT::Desc( FrVect::StructName( ) ), "" ) );

        ret( FrSE( "next", PTR_STRUCT::Desc( FrAdcData::StructName( ) ), "" ) );
    }

    return &ret;
}

void
FrAdcData::Write( ostream_type& Stream ) const
{
    try
    {
        nps_type::write( Stream );
        ps_type::write( Stream );
        WriteNext( Stream );
    }
    catch ( const std::exception& Exception )
    {
        std::cerr << "DEBUG: Error Writing FrAdcData: " << GetName( )
                  << " exception: " << Exception.what( ) << std::endl;
        throw; // rethrow
    }
}

bool
FrAdcData::operator==( const Common::FrameSpec::Object& Obj ) const
{
    return FrameCPP::Common::Compare( *this, Obj );
}

FrAdcData::demote_ret_type
FrAdcData::demote( INT_2U              Target,
                   demote_arg_type     Obj,
                   demote_stream_type* Stream ) const
{
    if ( Target >= DATA_FORMAT_VERSION )
    {
        return Obj;
    }
    throw Unimplemented( "Object* FrAdcData::demote( Object* Obj ) const",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
}

FrAdcData::promote_ret_type
FrAdcData::promote( INT_2U               Target,
                    promote_arg_type     Obj,
                    promote_stream_type* Stream ) const
{
    return Promote( Target, Obj, Stream );
}
/// \endcond ignore
