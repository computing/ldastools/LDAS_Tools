//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "ldastoolsal/MemChecker.hh"

#include "framecpp/Version3/FrameSpec.hh"

#include "framecpp/Version3/PTR_STRUCT.hh"
#include "framecpp/Version3/StreamRef.hh"

#include "framecpp/Version3/FrameH.hh"
#include "framecpp/Version3/FrAdcData.hh"
#include "framecpp/Version3/FrDetector.hh"
#include "framecpp/Version3/FrHeader.hh"
#include "framecpp/Version3/FrEndOfFile.hh"
#include "framecpp/Version3/FrEndOfFrame.hh"
#include "framecpp/Version3/FrHistory.hh"
#include "framecpp/Version3/FrMsg.hh"
#include "framecpp/Version3/FrProcData.hh"
#include "framecpp/Version3/FrRawData.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSerData.hh"
#include "framecpp/Version3/FrSH.hh"
#include "framecpp/Version3/FrSimData.hh"
#include "framecpp/Version3/FrStatData.hh"
#include "framecpp/Version3/FrSummary.hh"
#include "framecpp/Version3/FrTrigData.hh"
#include "framecpp/Version3/FrVect.hh"

using LDASTools::AL::MemChecker;

using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_3
    {
        static void cleanup_at_exit( );
        bool        init_frame_spec( );

        static const bool Initialized = init_frame_spec( );

        static void
        cleanup_at_exit( )
        {
            if ( Initialized )
            {
            }
            /* delete spec_info; */

            /* spec_info = (FrameSpec::Info*)NULL; */
        }

        bool
        init_frame_spec( )
        {
            static bool initialized = false;

            if ( !initialized )
            {
                //---------------------------------------------------------------
                // Register cleanup handler
                //---------------------------------------------------------------
                MemChecker::Append(
                    cleanup_at_exit, "FrameCPP::Version3::cleanup_at_exit", 0 );
                //-----------------------------------------------------------------
                // Local storage describing implementation of the frame spec
                //-----------------------------------------------------------------
                std::unique_ptr< FrameSpec::Info > info( new FrameSpec::Info(
                    DATA_FORMAT_VERSION, LIBRARY_MINOR_VERSION ) );

                info->FrameObject( new PTR_STRUCT );
                info->FrameObject( new StreamRef );

                info->FrameObject( new FrameH );
                info->FrameObject( new FrAdcData );
                info->FrameObject( new FrDetector );
                info->FrameObject( new FrEndOfFile );
                info->FrameObject( new FrEndOfFrame );
                info->FrameObject( new FrHeader );
                info->FrameObject( new FrHistory );
                info->FrameObject( new FrMsg );
                info->FrameObject( new FrProcData );
                info->FrameObject( new FrRawData );
                info->FrameObject( new FrSE );
                info->FrameObject( new FrSerData );
                info->FrameObject( new FrSH );
                info->FrameObject( new FrSimData );
                info->FrameObject( new FrStatData );
                info->FrameObject( new FrSummary );
                info->FrameObject( new FrTrigData );
                info->FrameObject( new FrVect );

                //---------------------------------------------------------------
                // Register with the stream manipulator
                //---------------------------------------------------------------
                FrameSpec::SpecInfo(
                    FrameSpec::version_type( DATA_FORMAT_VERSION ),
                    info.release( ) );
                initialized = true;
            }
            return true;
        }

    } // namespace Version_3
} // namespace FrameCPP
