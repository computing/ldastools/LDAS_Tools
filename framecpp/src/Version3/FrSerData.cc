//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version3/FrSerData.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"
#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_SER_DATA;

namespace FrameCPP
{
    namespace Version_3
    {
        //===================================================================
        // FrSerData::fr_ser_data_data_type
        //===================================================================

        bool
        FrSerData::fr_ser_data_data_type::
        operator==( const fr_ser_data_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( time ) && CMP__( sampleRate ) &&
                       CMP__( data ) && CMP__( serial ) && CMP__( more ) ) );
#undef CMP__
        }

        //=======================================================================
        // FrSerData
        //=======================================================================
        FrSerData::FrSerData( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrSerData::FrSerData( const FrSerData& SerData )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = SerData.m_data.name;
            m_data.time = SerData.m_data.time;
            m_data.sampleRate = SerData.m_data.sampleRate;
            m_data.data = SerData.m_data.data;
            m_data.serial = SerData.m_data.serial;
            m_data.more = SerData.m_data.more;
        }

        FrSerData::FrSerData( const std::string& name,
                              const GPSTime&     time,
                              REAL_4             sampleRate )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = name;
            m_data.time = time;
            m_data.sampleRate = sampleRate;
        }

        FrSerData::FrSerData( istream_type& Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            Stream >> m_data.name >> m_data.time >> m_data.sampleRate >>
                m_data.data >> m_data.serial >> m_data.more;
            Stream.Next( this );
        }

        FrameCPP::cmn_streamsize_type
        FrSerData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( INT_4U ) // timeSec
                + sizeof( INT_4U ) // timeNsec
                + sizeof( m_data.sampleRate ) + m_data.data.Bytes( ) +
                Stream.PtrStructBytes( ) // serial
                + Stream.PtrStructBytes( ) // more
                + Stream.PtrStructBytes( ) // next
                ;
        }

        FrSerData*
        FrSerData::Create( istream_type& Stream ) const
        {
            return new FrSerData( Stream );
        }

        const char*
        FrSerData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSerData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSerData::StructName( ),
                           s_object_id,
                           "Serial Data Structure" ) );

                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "timeSec", "INT_4U" ) );
                ret( FrSE( "timeNsec", "INT_4U" ) );
                ret( FrSE( "sampleRate", "REAL_4" ) );
                ret( FrSE( "data", "STRING" ) );

                ret( FrSE( "serial",
                           PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret(
                    FrSE( "more", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSerData::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrSerData::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name << m_data.time << m_data.sampleRate
                   << m_data.data << m_data.serial << m_data.more;
            WriteNext( Stream );
        }

        bool
        FrSerData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        const std::string&
        FrSerData::GetName( ) const
        {
            return m_data.name;
        }

        FrSerData&
        FrSerData::Merge( const FrSerData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        FrSerData::demote_ret_type
        FrSerData::demote( INT_2U              Target,
                           demote_arg_type     Obj,
                           demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            throw Unimplemented(
                "Object* FrSerData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrSerData::promote_ret_type
        FrSerData::promote( INT_2U               Target,
                            promote_arg_type     Obj,
                            promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_3
} // namespace FrameCPP
