//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <sstream>

#include "framecpp/Version6/Container.hh"
#include "framecpp/Version6/SearchContainer.hh"

#include "framecpp/Version6/FrAdcData.hh"
#include "framecpp/Version6/FrDetector.hh"
#include "framecpp/Version6/FrEvent.hh"
#include "framecpp/Version6/FrHistory.hh"
#include "framecpp/Version6/FrMsg.hh"
#include "framecpp/Version6/FrProcData.hh"
#include "framecpp/Version6/FrSerData.hh"
#include "framecpp/Version6/FrSimData.hh"
#include "framecpp/Version6/FrSimEvent.hh"
#include "framecpp/Version6/FrStatData.hh"
#include "framecpp/Version6/FrSummary.hh"
#include "framecpp/Version6/FrTable.hh"
#include "framecpp/Version6/FrVect.hh"

#include "framecpp/Container.hh"

using namespace FrameCPP::Version_6;

#define INSTANTIATE( LM_TYPE, LM_FUNC )                                        \
    template class FrameCPP::Version_6::Container< LM_TYPE >;                  \
    template class FrameCPP::SearchContainer<                                  \
        LM_TYPE,                                                               \
        &LM_TYPE::LM_FUNC,                                                     \
        FrameCPP::Version_6::Container< LM_TYPE > >;                           \
    template class FrameCPP::Version_6::SearchContainer< LM_TYPE,              \
                                                         &LM_TYPE::LM_FUNC >

INSTANTIATE( FrAdcData, GetName );
INSTANTIATE( FrDetector, GetName );
INSTANTIATE( FrEvent, GetName );
INSTANTIATE( FrHistory, GetName );
INSTANTIATE( FrMsg, GetAlarm );
INSTANTIATE( FrProcData, GetName );
INSTANTIATE( FrSerData, GetName );
INSTANTIATE( FrSimData, GetName );
INSTANTIATE( FrSimEvent, GetName );
INSTANTIATE( FrStatData, GetName );
INSTANTIATE( FrSummary, GetName );
INSTANTIATE( FrTable, GetName );
INSTANTIATE( FrVect, GetName );

#undef INSTANTIATE
