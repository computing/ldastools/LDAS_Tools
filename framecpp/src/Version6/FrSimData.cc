//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version6/FrSimData.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_SIM_DATA;

namespace FrameCPP
{
    namespace Version_6
    {
        //=======================================================================
        // FrSimDataStorage::data_type
        //=======================================================================
        void
        FrSimDataStorage::data_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment >> sampleRate >> timeOffset >> fShift >>
                phase;
        }

        void
        FrSimDataStorage::data_type::
        operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment << sampleRate << timeOffset << fShift
                   << phase;
        }

        //=======================================================================
        // FrSimDataStorage
        //=======================================================================
        const FrSimDataStorage::timeoffset_type
            FrSimDataStorage::DEFAULT_TIME_OFFSET =
                FrSimDataStorage::timeoffset_type( 0.0 );
        const FrSimDataStorage::fshift_type FrSimDataStorage::DEFAULT_FSHIFT =
            FrSimDataStorage::fshift_type( 0.0 );
        const FrSimDataStorage::phase_type FrSimDataStorage::DEFAULT_PHASE =
            FrSimDataStorage::phase_type( 0.0 );

        FrSimDataStorage::FrSimDataStorage( )
        {
        }

        FrSimDataStorage::FrSimDataStorage( const FrSimDataStorage& Source )
        {
            m_data.name = Source.m_data.name;
            m_data.comment = Source.m_data.comment;
            m_data.sampleRate = Source.m_data.sampleRate;
            m_data.timeOffset = Source.m_data.timeOffset;
            m_data.fShift = Source.m_data.fShift;
            m_data.phase = Source.m_data.phase;
        }

        FrSimDataStorage::FrSimDataStorage( const std::string&    Name,
                                            const std::string&    Comment,
                                            const REAL_4          SampleRate,
                                            const fshift_type     FShift,
                                            const phase_type      Phase,
                                            const timeoffset_type TimeOffset )
        {
            m_data.name = Name;
            m_data.comment = Comment;
            m_data.sampleRate = SampleRate;
            m_data.timeOffset = TimeOffset;
            m_data.fShift = FShift;
            m_data.phase = Phase;
        }

        //=======================================================================
        // FrSimData
        //=======================================================================
        FrSimData::FrSimData( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrSimData::FrSimData( const FrSimData& Source )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              FrSimDataStorage( Source ),
              FrSimDataRefs( Source ), Common::TOCInfo( Source )
        {
        }

        FrSimData::FrSimData( const std::string&    Name,
                              const std::string&    Comment,
                              const REAL_4          SampleRate,
                              const fshift_type     FShift,
                              const phase_type      Phase,
                              const timeoffset_type TimeOffset )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              FrSimDataStorage(
                  Name, Comment, SampleRate, FShift, Phase, TimeOffset )
        {
        }

        FrSimData::FrSimData( Previous::FrSimData& Source,
                              stream_base_type*    Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = Source.GetName( );
            m_data.comment = Source.GetComment( );
            m_data.sampleRate = Source.GetSampleRate( );

            m_data.timeOffset = DEFAULT_TIME_OFFSET;
            m_data.fShift = DEFAULT_FSHIFT;
            m_data.phase = DEFAULT_PHASE;

            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( istream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                istream->ReplaceRef( RefData( ),
                                     Source.RefData( ),
                                     Previous::FrSimData::MAX_REF );
                istream->ReplaceRef( RefInput( ),
                                     Source.RefInput( ),
                                     Previous::FrSimData::MAX_REF );
                istream->ReplaceRef( RefTable( ),
                                     Source.RefTable( ),
                                     Previous::FrSimData::MAX_REF );
            }
        }

        FrSimData::FrSimData( IStream& Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrameCPP::cmn_streamsize_type
        FrSimData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( Stream ) + m_refs.Bytes( Stream ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrSimData*
        FrSimData::Create( istream_type& Stream ) const
        {
            return new FrSimData( Stream );
        }

        const char*
        FrSimData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSimData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSimData::StructName( ),
                           s_object_id,
                           "Simulated Data Structure" ) );

                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "comment", "STRING" ) );
                ret( FrSE( "sampleRate", "REAL_4" ) );
                ret( FrSE( "timeOffset", "REAL_8" ) );
                ret( FrSE( "fShift", "REAL_8" ) );
                ret( FrSE( "phase", "REAL_4" ) );

                ret(
                    FrSE( "data", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "input",
                           PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "table",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSimData::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrSimData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        const std::string&
        FrSimData::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrSimData&
        FrSimData::Merge( const FrSimData& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        void
        FrSimData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        bool
        FrSimData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrSimData::demote_ret_type
        FrSimData::demote( INT_2U              Target,
                           demote_arg_type     Obj,
                           demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrSimData > retval(
                    new Previous::FrSimData(
                        GetName( ), GetComment( ), GetSampleRate( ) ) );

                auto istream = FrameCPP::Common::IsIStream( Stream );

                if ( istream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    istream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefInput( ), RefInput( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrSimData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrSimData::promote_ret_type
        FrSimData::promote( INT_2U               Target,
                            promote_arg_type     Obj,
                            promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
