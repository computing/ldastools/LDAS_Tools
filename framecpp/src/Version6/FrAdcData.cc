//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <algorithm>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/Profile.hh"

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version6/FrAdcData.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

#if HAVE_TEMPLATE_MOVE
#define MOVE_RET( a ) ( std::move( a ) )
#else /* HAVE_TEMPLATE_MOVE */
#define MOVE_RET( a ) ( a )
#endif /* HAVE_TEMPLATE_MOVE */

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using std::ceil;
using std::floor;

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_ADC_DATA;

namespace FrameCPP
{
    namespace Version_6
    {
        //-----------------------------------------------------------------------
        //
        //-----------------------------------------------------------------------

        const FrAdcDataNPS::bias_type FrAdcDataNPS::DEFAULT_BIAS =
            FR_ADC_DATA_DEFAULT_BIAS;
        const FrAdcDataNPS::slope_type FrAdcDataNPS::DEFAULT_SLOPE =
            FR_ADC_DATA_DEFAULT_SLOPE;
        const FrAdcDataNPS::fShift_type FrAdcDataNPS::DEFAULT_FSHIFT =
            FR_ADC_DATA_DEFAULT_FSHIFT;
        const FrAdcDataNPS::timeOffset_type FrAdcDataNPS::DEFAULT_TIME_OFFSET =
            FR_ADC_DATA_DEFAULT_TIME_OFFSET;
        const FrAdcDataNPS::dataValid_type FrAdcDataNPS::DEFAULT_DATA_VALID =
            FR_ADC_DATA_DEFAULT_DATA_VALID;
        const FrAdcDataNPS::phase_type FrAdcDataNPS::DEFAULT_PHASE =
            FR_ADC_DATA_DEFAULT_PHASE;

        FrAdcDataNPS::FrAdcDataNPS( Common::IStream& Stream )
        {
            Stream >> name >> comment >> channelGroup >> channelNumber >>
                nBits >> bias >> slope >> units >> sampleRate >> timeOffset >>
                fShift >> phase >> dataValid;
        }

        void
        FrAdcDataNPS::write( Common::OStream& Stream ) const
        {
            Stream << name << comment << channelGroup << channelNumber << nBits
                   << bias << slope << units << sampleRate << timeOffset
                   << fShift << phase << dataValid;
        }

        //-----------------------------------------------------------------------
        //-----------------------------------------------------------------------
        FrAdcData::FrAdcData( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrAdcData::FrAdcData( const FrAdcData& Source )
            : nps_type( Source ),
              ps_type( Source ), FrameSpec::Object( s_object_id,
                                                    StructDescription( ),
                                                    DATA_FORMAT_VERSION ),
              Common::TOCInfo( Source )
        {
        }

        FrAdcData::FrAdcData( const std::string& Name,
                              channelGroup_type  Group,
                              channelNumber_type Channel,
                              nBits_type         NBits,
                              sampleRate_type    SampleRate,
                              bias_type          Bias,
                              slope_type         Slope,
                              const std::string& Units,
                              fShift_type        FShift,
                              timeOffset_type    TimeOffset,
                              dataValid_type     DataValid,
                              phase_type         Phase )
            : nps_type( Name,
                        Group,
                        Channel,
                        NBits,
                        SampleRate,
                        Bias,
                        Slope,
                        Units,
                        FShift,
                        TimeOffset,
                        DataValid,
                        Phase ),
              FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrAdcData::FrAdcData( Previous::FrAdcData& Source,
                              stream_base_type*    Stream )
            : nps_type( Source, Stream ), //
              ps_type( Source, Stream ), //
              FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrAdcData::FrAdcData( istream_type& Stream )
            : nps_type( Stream ),
              ps_type( Stream ), FrameSpec::Object( s_object_id,
                                                    StructDescription( ),
                                                    DATA_FORMAT_VERSION )
        {
            Stream.Next( this );
        }

        FrAdcData::~FrAdcData( )
        {
        }

        const std::string&
        FrAdcData::GetName( ) const
        {
            return nps_type::GetName( );
        }

        FrameCPP::cmn_streamsize_type
        FrAdcData::Bytes( const Common::StreamBase& Stream ) const
        {
            return nps_type::bytes( Stream ) + ps_type::Bytes( Stream ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrAdcData*
        FrAdcData::Create( istream_type& Stream ) const
        {
            return new FrAdcData( Stream );
        }

        FrAdcData&
        FrAdcData::Merge( const FrAdcData& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        const char*
        FrAdcData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrAdcData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrAdcData::StructName( ),
                           s_object_id,
                           "AdcData  Data Structure" ) );

                nps_type::structDescription< Description, FrSE >( ret );
                ps_type::structDescription< Description, FrSE >(
                    ret, PTR_STRUCT::Desc( FrVect::StructName( ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrAdcData::StructName( ) ),
                           "" ) );
            }

            return &ret;
        }

        FrAdcData::subfradcdata_type
        FrAdcData::SubFrAdcData( REAL_8 Offset, REAL_8 Dt ) const
        {
            //---------------------------------------------------------------------
            // Sanity checks
            //---------------------------------------------------------------------
            // Verify the existance of only a single FrVect component
            //---------------------------------------------------------------------
            if ( data.size( ) != 1 )
            {
                std::ostringstream msg;

                msg << "METHOD: frameAPI::FrAdcData::SubFrAdcData: "
                    << "data vectors of length " << data.size( )
                    << " currently are not supported";
                throw std::logic_error( msg.str( ) );
            }
            INT_4U num_elements = data[ 0 ]->GetDim( 0 ).GetNx( );
            //---------------------------------------------------------------------
            //:TODO: Ensure the dimension of the FrVect is 1
            //---------------------------------------------------------------------
            //---------------------------------------------------------------------
            //:TODO: Ensure the requested range is valid
            //---------------------------------------------------------------------
            if ( Offset >
                 ( GetTimeOffset( ) + ( num_elements / GetSampleRate( ) ) ) )
            {
                std::ostringstream msg;
                msg << "METHOD: frameAPI::FrAdcData::SubFrAdcData: "
                    << "The value for the parameter Offset (" << Offset
                    << ") is out of range ([0-"
                    << ( GetTimeOffset( ) +
                         ( num_elements / GetSampleRate( ) ) )
                    << "))";
                throw std::range_error( msg.str( ) );
            }
            //---------------------------------------------------------------------
            // Start moving of data
            //---------------------------------------------------------------------
            subfradcdata_type retval( new FrAdcData );

            // Copy in the header information
            retval->copy_core( *this );

            //:TODO: Get the slice of FrVect
            REAL_8 startSampleReal = ( Offset * retval->GetSampleRate( ) ) -
                ( retval->GetTimeOffset( ) * retval->GetSampleRate( ) );

            INT_4U endSample = INT_4U(
                ceil( startSampleReal + ( retval->GetSampleRate( ) * Dt ) ) );
            INT_4U startSample = ( startSampleReal > REAL_8( 0 ) )
                ? INT_4U( floor( startSampleReal ) )
                : INT_4U( 0 );

            {
                boost::shared_ptr< FrVect > subvect(
                    this->RefData( )[ 0 ]
                        ->SubFrVect( startSample, endSample )
                        .release( ) );

                retval->data.append( subvect );
            }
            //---------------------------------------------------------------------
            // Shift the offset
            //---------------------------------------------------------------------
            if ( startSampleReal > 0 )
            {
                // There is no gap
                retval->SetTimeOffset( timeOffset_type( 0 ) );
            }
            else
            {
                retval->incrementTimeOffset( Offset );
            }
            //---------------------------------------------------------------------
            // Return the new FrAdcData
            //---------------------------------------------------------------------
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
            return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
        }

        void
        FrAdcData::Write( ostream_type& Stream ) const
        {
            try
            {
                nps_type::write( Stream );
                ps_type::write( Stream );
                WriteNext( Stream );
            }
            catch ( const std::exception& Exception )
            {
                std::cerr << "DEBUG: Error Writing FrAdcData: " << GetName( )
                          << " exception: " << Exception.what( ) << std::endl;
                throw; // rethrow
            }
        }

        void
        FrAdcData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_2U:
                {
                    INT_2U* data = va_arg( vl, INT_2U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_DATA_VALID:
                        *data = GetDataValid( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_CHANNEL_ID:
                        *data = GetChannelNumber( );
                        break;
                    case TOCInfo::IC_GROUP_ID:
                        *data = GetChannelGroup( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrAdcData::demote_ret_type
        FrAdcData::demote( INT_2U              Target,
                           demote_arg_type     Obj,
                           demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------

                INT_4S Seconds = INT_4S( GetTimeOffset( ) );
                INT_4U Nanoseconds = INT_4U( ( GetTimeOffset( ) - Seconds ) *
                                             NANOSECOND_MULTIPLIER );

                // Do actual down conversion
                boost::shared_ptr< Previous::FrAdcData > retval(
                    new Previous::FrAdcData( GetName( ),
                                             GetChannelGroup( ),
                                             GetChannelNumber( ),
                                             GetNBits( ),
                                             GetSampleRate( ),
                                             GetBias( ),
                                             GetSlope( ),
                                             GetUnits( ),
                                             GetFShift( ),
                                             Seconds,
                                             Nanoseconds,
                                             GetDataValid( ) ) );
                reinterpret_cast< Previous::FrAdcData* >( retval.get( ) )
                    ->AppendComment( GetComment( ) );

                auto istream = FrameCPP::Common::IsIStream( Stream );

                if ( istream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    istream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefAux( ), RefAux( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrAdcData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrAdcData::promote_ret_type
        FrAdcData::promote( INT_2U               Target,
                            promote_arg_type     Obj,
                            promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

        bool
        FrAdcData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

    } // namespace Version_6
} // namespace FrameCPP
