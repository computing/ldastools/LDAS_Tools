//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrTOC.hh"

#include "framecpp/Version6/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

template < typename T >
class sum
{
public:
    inline sum( T i = 0 ) : res( i )
    {
    }

    inline void
    operator( )( T x )
    {
        if ( x != FrameCPP::Version_6::FrTOC::NO_DATA_AVAILABLE )
        {
            res += x;
        }
    }

    inline T
    result( ) const
    {
        return res;
    }

private:
    T res;
};

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        //===================================================================
        FrTOCEvent::FrTOCEvent( )
        {
        }

        FrTOCEvent::FrTOCEvent( Common::IStream& Stream )
        {
            nevent_type nevent;
            Stream >> nevent;
            if ( nevent )
            {
                //---------------------------------------------------------------
                // Read in the information
                //---------------------------------------------------------------
                std::vector< name_type >   names( nevent );
                std::vector< nevent_type > events( nevent );

                Stream >> names >> events;

                sum< nevent_type > nevent_sum;
                nevent_type        offset = 0;

                for ( std::vector< nevent_type >::const_iterator
                          cur = events.begin( ),
                          last = events.end( );
                      cur != last;
                      ++cur )
                {
                    nevent_sum( *cur );
                }

                std::vector< gtimesEvent_type > gtimes( nevent_sum.result( ) );
                std::vector< gtimenEvent_type > gtimen( nevent_sum.result( ) );
                std::vector< amplitudeEvent_type > amplitude(
                    nevent_sum.result( ) );
                std::vector< positionEvent_type > position(
                    nevent_sum.result( ) );

                Stream >> gtimes >> gtimen >> amplitude >> position;
                std::vector< nevent_type >::const_iterator cur_event_counter =
                    events.begin( );

                for ( std::vector< name_type >::const_iterator
                          cur = names.begin( ),
                          last = names.end( );
                      cur != last;
                      ++cur, ++cur_event_counter )
                {
                    events_container_type& cur_events = m_info[ *cur ];

                    if ( ( *cur_event_counter == 0 ) ||
                         ( *cur_event_counter == FrTOC::NO_DATA_AVAILABLE ) )
                    {
                        continue;
                    }
                    cur_events.resize( *cur_event_counter );
                    for ( nevent_type x = 0, x_last = *cur_event_counter;
                          x != x_last;
                          ++x, ++offset )
                    {
                        cur_events[ x ].GTime =
                            GPSTime( gtimes[ offset ], gtimen[ offset ] );
                        cur_events[ x ].amplitudeEvent = amplitude[ offset ];
                        cur_events[ x ].positionEvent = position[ offset ];
                    }
                }
            }
        }

        void
        FrTOCEvent::QueryEvent( const Common::TOCInfo& Info,
                                INT_4U                 FrameOffset,
                                INT_8U                 Position )
        {
            STRING name;
            INT_4U sec;
            INT_4U nsec;
            REAL_4 ampl;

            Info.TOCQuery( Common::TOCInfo::IC_NAME,
                           Common::TOCInfo::DataType( name ),
                           &name,
                           Common::TOCInfo::IC_GTIME_S,
                           Common::TOCInfo::DataType( sec ),
                           &sec,
                           Common::TOCInfo::IC_GTIME_N,
                           Common::TOCInfo::DataType( nsec ),
                           &nsec,
                           Common::TOCInfo::IC_AMPLITUDE,
                           Common::TOCInfo::DataType( ampl ),
                           &ampl,
                           Common::TOCInfo::IC_EOQ );

            events_container_type& i( m_info[ name ] );

            event_type e;
            e.GTime = GPSTime( sec, nsec );
            e.amplitudeEvent = ampl;
            e.positionEvent = Position;

            i.push_back( e );
        }

        FrTOCEvent::positionEvent_type
        FrTOCEvent::positionEvent( INT_4U             FrameIndex,
                                   const std::string& Event ) const
        {
            const nameEvent_container_type::const_iterator event(
                m_info.find( Event ) );
            //-----------------------------------------------------------------
            // Locate the event by name
            //-----------------------------------------------------------------
            if ( event == m_info.end( ) )
            {
                //---------------------------------------------------------------
                // Event name does not exist.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "No FrEvent structures with the name '" << Event << "'";
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Verify that the index exists
            //-----------------------------------------------------------------
            if ( FrameIndex >= event->second.size( ) )
            {
                std::ostringstream msg;

                msg << "Request for frame " << FrameIndex
                    << " exceeds the range of 0 through "
                    << ( event->second.size( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Return position information
            //-----------------------------------------------------------------
            return event->second[ FrameIndex ].positionEvent;
        }

        FrTOCEvent::positionEvent_type
        FrTOCEvent::positionEvent( const std::string& EventType,
                                   INT_4U             Index ) const
        {
            const nameEvent_container_type::const_iterator event(
                m_info.find( EventType ) );
            //-----------------------------------------------------------------
            // Locate the event by name
            //-----------------------------------------------------------------
            if ( event == m_info.end( ) )
            {
                //---------------------------------------------------------------
                // Event name does not exist.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "No FrEvent structures with the name '" << EventType
                    << "'";
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Verify that the index exists
            //-----------------------------------------------------------------
            if ( Index >= event->second.size( ) )
            {
                std::ostringstream msg;

                msg << "Request for index " << Index
                    << " exceeds the range of 0 through "
                    << ( event->second.size( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Return position information
            //-----------------------------------------------------------------
            return event->second[ Index ].positionEvent;
        }

        void
        FrTOCEvent::write( Common::OStream& Stream ) const
        {
            //-----------------------------------------------------------------
            // Flatten data so it is streamable
            //-----------------------------------------------------------------
            if ( m_info.size( ) > 0 )
            {
                std::vector< name_type >           names( m_info.size( ) );
                std::vector< nevent_type >         nevent( m_info.size( ) );
                nevent_type                        offset( 0 );
                nevent_type                        eoffset( 0 );
                std::vector< gtimesEvent_type >    gtimes;
                std::vector< gtimenEvent_type >    gtimen;
                std::vector< amplitudeEvent_type > amplitude;
                std::vector< positionEvent_type >  position;

                for ( nameEvent_container_type::const_iterator
                          cur = m_info.begin( ),
                          last = m_info.end( );
                      cur != last;
                      ++cur, ++offset )
                {
                    names[ offset ] = cur->first;
                    const nevent_type c = ( cur->second.size( ) == 0 )
                        ? ( FrTOC::NO_DATA_AVAILABLE )
                        : ( cur->second.size( ) );
                    nevent[ offset ] = c;
                    if ( c != FrTOC::NO_DATA_AVAILABLE )
                    {
                        const int ns( c + eoffset );

                        gtimes.resize( ns );
                        gtimen.resize( ns );
                        amplitude.resize( ns );
                        position.resize( ns );

                        for ( nevent_type x = 0; x != c; ++x, ++eoffset )
                        {
                            gtimes[ eoffset ] =
                                cur->second[ x ].GTime.GetSeconds( );
                            gtimen[ eoffset ] =
                                cur->second[ x ].GTime.GetNanoseconds( );
                            amplitude[ eoffset ] =
                                cur->second[ x ].amplitudeEvent;
                            position[ eoffset ] =
                                cur->second[ x ].positionEvent;
                        }
                    }
                }
                Stream << nevent_type( m_info.size( ) ) << names << nevent
                       << gtimes << gtimen << amplitude << position;
            }
            else
            {
                Stream << nevent_type( 0 );
            }
        }

    } // namespace Version_6
} // namespace FrameCPP
