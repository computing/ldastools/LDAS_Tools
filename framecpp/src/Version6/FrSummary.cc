//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version6/FrSummary.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        // Static
        //===================================================================

        static const FrameSpec::Info::frame_object_types s_object_id =
            FrameSpec::Info::FSI_FR_SUMMARY;

        //=======================================================================
        // FrSummary::fr_summary_data_type
        //=======================================================================

        bool
        FrSummary::fr_summary_data_type::
        operator==( const fr_summary_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( comment ) && CMP__( test ) &&
                       CMP__( GTime ) && CMP__( moments ) && CMP__( table ) ) );
#undef CMP__
        }

    } // namespace Version_6
} // namespace FrameCPP

//=======================================================================
// FrSummary
//=======================================================================

static const int MAX_REF = 2;

namespace FrameCPP
{
    namespace Version_6
    {

        FrSummary::FrSummary( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrSummary::FrSummary( const FrSummary& Summary )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              Common::TOCInfo( Summary )
        {
            m_data.name = Summary.m_data.name;
            m_data.comment = Summary.m_data.comment;
            m_data.test = Summary.m_data.test;
            m_data.GTime = Summary.m_data.GTime;
            m_data.moments = Summary.m_data.moments;
            m_data.table = Summary.m_data.table;
        }

        FrSummary::FrSummary( const std::string& name,
                              const std::string& comment,
                              const std::string& test,
                              const GPSTime&     gtime )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = name;
            m_data.comment = comment;
            m_data.test = test;
            m_data.GTime = gtime;
        }

        FrSummary::FrSummary( Previous::FrSummary& Source,
                              stream_base_type*    Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = Source.GetName( );
            m_data.comment = Source.GetComment( );
            m_data.test = Source.GetTest( );

            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( istream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                istream->ReplaceRef(
                    RefMoments( ), Source.RefMoments( ), MAX_REF );
                istream->ReplaceRef( RefTable( ), Source.RefTable( ), MAX_REF );
            }
        }

        FrSummary::FrSummary( istream_type& Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            Stream >> m_data.name >> m_data.comment >> m_data.test >>
                m_data.GTime >> m_data.moments >> m_data.table;
            Stream.Next( this );
        }

        FrameCPP::cmn_streamsize_type
        FrSummary::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + m_data.comment.Bytes( ) +
                m_data.test.Bytes( ) + sizeof( INT_4U ) // GTimeS
                + sizeof( INT_4U ) // GTimeN
                + Stream.PtrStructBytes( ) // moments
                + Stream.PtrStructBytes( ) // table
                + Stream.PtrStructBytes( ) // next
                ;
        }

        const std::string&
        FrSummary::GetName( ) const
        {
            return m_data.name;
        }

        FrSummary*
        FrSummary::Create( istream_type& Stream ) const
        {
            return new FrSummary( Stream );
        }

        const char*
        FrSummary::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSummary::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSummary::StructName( ),
                           s_object_id,
                           "Summary Data Structure" ) );

                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "comment", "STRING" ) );
                ret( FrSE( "test", "STRING" ) );
                ret( FrSE( "GTimeS", "INT_4U" ) );
                ret( FrSE( "GTimeN", "INT_4U" ) );

                ret( FrSE( "moments",
                           PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "table",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSummary::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrSummary::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name << m_data.comment << m_data.test
                   << m_data.GTime << m_data.moments << m_data.table;
            WriteNext( Stream );
        }

        void
        FrSummary::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrSummary&
        FrSummary::Merge( const FrSummary& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrSummary::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrSummary::demote_ret_type
        FrSummary::demote( INT_2U              Target,
                           demote_arg_type     Obj,
                           demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrSummary > retval(
                    new Previous::FrSummary(
                        GetName( ), GetComment( ), GetTest( ) ) );

                auto istream = FrameCPP::Common::IsIStream( Stream );

                if ( istream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    istream->ReplaceRef(
                        retval->RefMoments( ), RefMoments( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrSummary::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrSummary::promote_ret_type
        FrSummary::promote( INT_2U               Target,
                            promote_arg_type     Obj,
                            promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
