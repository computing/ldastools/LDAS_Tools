//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version6/FrEvent.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

namespace
{
    using namespace FrameCPP::FR_EVENT_PARAM_NAMESPACE;

    FrameCPP::Common::IStream& operator>>( FrameCPP::Common::IStream& Stream,
                                           FrEvent::ParamList_type&   Data );
    FrameCPP::Common::OStream&
    operator<<( FrameCPP::Common::OStream&     Stream,
                const FrEvent::ParamList_type& Data );
} // namespace
//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_EVENT;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        // FrEventStorage::data_type
        //===================================================================
        cmn_streamsize_type
        FrEventStorage::data_type::Bytes( ) const
        {
            FrameCPP::cmn_streamsize_type retval = name.Bytes( ) +
                comment.Bytes( ) + inputs.Bytes( ) + Common::Bytes( GTime ) +
                sizeof( timeBefore ) + sizeof( timeAfter ) +
                sizeof( eventStatus ) + sizeof( amplitude ) +
                sizeof( probability ) + statistics.Bytes( ) +
                sizeof( INT_2U ); // nParam
            ;
            for ( ParamList_type::const_iterator i( Params.begin( ) );
                  i != ( Params.end( ) );
                  i++ )
            {
                retval += sizeof( Param_type::second_type ) +
                    STRING::Bytes( ( *i ).first );
            }

            return retval;
        }

        void
        FrEventStorage::data_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment >> inputs >> GTime >> timeBefore >>
                timeAfter >> eventStatus >> amplitude >> probability >>
                statistics >> Params;
        }

        void
        FrEventStorage::data_type::operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment << inputs << GTime << timeBefore
                   << timeAfter << eventStatus << amplitude << probability
                   << statistics << Params;
        }

        //===================================================================
        // FrEvent
        //===================================================================

        //-------------------------------------------------------------------
        /// Construct an object using defaults.
        //-------------------------------------------------------------------
        FrEvent::FrEvent( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
        }

        FrEvent::FrEvent( const FrEvent& Source )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              FrEventStorage( Source ),
              FrEventRefs( Source ), Common::TOCInfo( Source )
        {
        }

        FrEvent::FrEvent( const name_type&       name,
                          const comment_type&    comment,
                          const inputs_type&     inputs,
                          const time_type&       time,
                          const timeBefore_type  timeBefore,
                          const timeAfter_type   timeAfter,
                          const eventStatus_type eventStatus,
                          const amplitude_type   amplitude,
                          const probability_type prob,
                          const statistics_type& statistics,
                          const ParamList_type&  parameters )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = name;
            m_data.comment = comment;
            m_data.inputs = inputs;
            m_data.GTime = time;
            m_data.timeBefore = timeBefore;
            m_data.timeAfter = timeAfter;
            m_data.eventStatus = eventStatus;
            m_data.amplitude = amplitude;
            m_data.probability = prob;
            m_data.statistics = statistics;
            m_data.Params = parameters;
        }

        FrEvent::FrEvent( const Previous::FrTrigData& Source,
                          stream_base_type*           Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              FrEventStorage( Source )
        {
            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( istream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                istream->ReplaceRef( RefData( ),
                                     Source.RefData( ),
                                     Previous::FrTrigData::MAX_REF );
                istream->ReplaceRef( RefTable( ),
                                     Source.RefTable( ),
                                     Previous::FrTrigData::MAX_REF );
            }
        }

        FrEvent::FrEvent( istream_type& Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrEvent*
        FrEvent::Create( istream_type& Stream ) const
        {
            return new FrEvent( Stream );
        }

        const std::string&
        FrEvent::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrEvent&
        FrEvent::Merge( const FrEvent& RHS )
        {
            throw Unimplemented(
                "FrEvent& FrEvent::Merge( const FrEvent& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        const char*
        FrEvent::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrEvent::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrEvent::StructName( ),
                           s_object_id,
                           "Event Data Structure" ) );

                ret( FrSE( "name", "STRING", "Name of event." ) );
                ret( FrSE( "comment", "STRING", "Descriptor of event" ) );
                ret( FrSE( "inputs",
                           "STRING",
                           "Input channels and filter parameters to event "
                           "process." ) );
                ret( FrSE( "GTimeS",
                           "INT_4U",
                           "GPS time in seconds corresponding to reference "
                           "vale of event,"
                           " as defined by the search algorigthm." ) );
                ret( FrSE(
                    "GTimeN",
                    "INT_4U",
                    "GPS time in residual nanoseconds relative to GTimeS" ) );
                ret(
                    FrSE( "timeBefore",
                          "REAL_4",
                          "Signal duration before (GTimeS.GTimeN)(seconds)" ) );
                ret( FrSE( "timeAfter",
                           "REAL_4",
                           "Signal duration after (GTimeS.GTimeN)(seconds)" ) );
                ret( FrSE( "eventStatus",
                           "INT_4U",
                           "Defined bby event search algorithm" ) );
                ret( FrSE( "amplitude",
                           "REAL_4",
                           "Continuouis output amplitude returned by event" ) );
                ret( FrSE( "probability",
                           "REAL_4",
                           "Likelihood estimate of event, if available"
                           " (probability = -1 if cannot be estimated)" ) );
                ret( FrSE( "statistics",
                           "STRING",
                           "Statistical description of event, if relevant or "
                           "available" ) );
                ret( FrSE( "nParam",
                           "INT_2U",
                           "Number of additional event parameters" ) );
                ret( FrSE(
                    "parameters",
                    "REAL_4[nParam]",
                    "Array of additional event paraameters(size of nParam)" ) );
                ret( FrSE( "parameterNames",
                           "STRING[nParam]",
                           "Array of parameter names (size of nParam)." ) );
                ret( FrSE(
                    "data", PTR_STRUCT::Desc( FrVect::StructName( ) ), "" ) );
                ret( FrSE(
                    "table", PTR_STRUCT::Desc( FrTable::StructName( ) ), "" ) );
                ret( FrSE(
                    "next", PTR_STRUCT::Desc( FrEvent::StructName( ) ), "" ) );
            }

            return &ret;
        }

        void
        FrEvent::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        void
        FrEvent::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_GTIME_S:
                        *data = GetGTime( ).GetSeconds( );
                        break;
                    case TOCInfo::IC_GTIME_N:
                        *data = GetGTime( ).GetNanoseconds( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_4:
                {
                    REAL_4* data = va_arg( vl, REAL_4* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_8:
                {
                    REAL_8* data = va_arg( vl, REAL_8* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        bool
        FrEvent::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrEvent::demote_ret_type
        FrEvent::demote( INT_2U              Target,
                         demote_arg_type     Obj,
                         demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrTrigData > retval(
                    new Previous::FrTrigData( GetName( ),
                                              GetComment( ),
                                              GetInputs( ),
                                              GetGTime( ),
                                              GetTimeBefore( ),
                                              GetTimeAfter( ),
                                              GetEventStatus( ),
                                              GetAmplitude( ),
                                              GetProbability( ),
                                              GetStatistics( ) ) );

                auto istream = FrameCPP::Common::IsIStream( Stream );

                if ( istream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    istream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented( "Object* FrEvent::demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrEvent::promote_ret_type
        FrEvent::promote( INT_2U               Target,
                          promote_arg_type     Obj,
                          promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP

//=======================================================================
// Local functions
//=======================================================================

namespace
{
    using namespace FrameCPP::FR_EVENT_PARAM_NAMESPACE;

    FrameCPP::Common::IStream&
    operator>>( FrameCPP::Common::IStream& Stream,
                FrEvent::ParamList_type&   Data )
    {
        return FrameCPP::FR_EVENT_PARAM_NAMESPACE ::
            FrEventParamRead< FrEvent, STRING_SHADOW >( Stream, Data );
    }

    FrameCPP::Common::OStream&
    operator<<( FrameCPP::Common::OStream&     Stream,
                const FrEvent::ParamList_type& Data )
    {
        return FrameCPP::FR_EVENT_PARAM_NAMESPACE ::
            FrEventParamWrite< FrEvent, STRING_SHADOW >( Stream, Data );
    }
} // namespace
