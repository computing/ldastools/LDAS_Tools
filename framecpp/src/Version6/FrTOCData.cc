//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrTOC.hh"

#include "framecpp/Version6/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        // FrTOCData
        //===================================================================
        FrTOCData::FrTOCData( Common::IStream& Stream )
        {
            load( Stream );
        }

        void
        FrTOCData::load( Common::IStream& Stream )
        {
            nframe_type nframe;

            Stream >> m_ULeapS >> nframe;
            if ( nframe )
            {
                m_dataQuality.resize( nframe );
                m_GTimeS.resize( nframe );
                m_GTimeN.resize( nframe );
                m_dt.resize( nframe );
                m_runs.resize( nframe );
                m_frame.resize( nframe );
                m_positionH.resize( nframe );
                m_nFirstADC.resize( nframe );
                m_nFirstSer.resize( nframe );
                m_nFirstTable.resize( nframe );
                m_nFirstMsg.resize( nframe );
                Stream >> m_dataQuality >> m_GTimeS >> m_GTimeN >> m_dt >>
                    m_runs >> m_frame >> m_positionH >> m_nFirstADC >>
                    m_nFirstSer >> m_nFirstTable >> m_nFirstMsg;
            }
            nsh_type nsh;
            Stream >> nsh;
            if ( nsh > 0 )
            {
                m_SHid.resize( nsh );
                m_SHname.resize( nsh );
                Stream >> m_SHid >> m_SHname;
            }
            ndetector_type ndetector;
            Stream >> ndetector;
            if ( ndetector > 0 )
            {
                m_nameDetector.resize( ndetector );
                m_positionDetector.resize( ndetector );
                Stream >> m_nameDetector >> m_positionDetector;
            }
        }

        //-------------------------------------------------------------------
        /// This method allows for iterting over each element of information
        /// and allows the caller to gather information about each element.
        //-------------------------------------------------------------------
        void
        FrTOCData::forEach( Common::FrTOC::query_info_type Info,
                            Common::FrTOC::FunctionBase&   Action ) const
        {
            switch ( Info )
            {
            case Common::FrTOC::TOC_DETECTOR:
            {
                try
                {
                    Common::FrTOC::FunctionC& action(
                        dynamic_cast< Common::FrTOC::FunctionC& >( Action ) );

                    for ( namedetector_container_type::const_iterator
                              cur = GetNameDetector( ).begin( ),
                              last = GetNameDetector( ).end( );
                          cur != last;
                          ++cur )
                    {
                        try
                        {
                            const Common::DetectorNames::info_type& info(
                                DetectorNameTable.Detector( *cur ) );

                            action( info.s_prefix[ 0 ] );
                        }
                        catch ( ... )
                        {
                        }
                    }
                }
                catch ( ... )
                {
                    // Does not understand Action
                }
            }
            break;
            case Common::FrTOC::TOC_FR_STRUCTS:
            {
                try
                {
                    Common::FrTOC::FunctionSI& action(
                        dynamic_cast< Common::FrTOC::FunctionSI& >( Action ) );
                    shid_container_type::const_iterator cur_id =
                                                            GetSHid( ).begin( ),
                                                        last_id =
                                                            GetSHid( ).end( );
                    shname_container_type::const_iterator
                        cur_name = GetSHname( ).begin( ),
                        last_name = GetSHname( ).end( );
                    while ( ( cur_id != last_id ) && ( cur_name != last_name ) )
                    {
                        action( *cur_name, *cur_id );
                        ++cur_name;
                        ++cur_id;
                    }
                }
                catch ( ... )
                {
                }
            }
            break;
            default:
                //---------------------------------------------------------------
                // ignore all other requests
                //---------------------------------------------------------------
                break;
            }
        }

        void
        FrTOCData::write( Common::OStream& Stream ) const
        {
            Stream << m_ULeapS << nframe_type( m_dataQuality.size( ) )
                   << m_dataQuality << m_GTimeS << m_GTimeN << m_dt << m_runs
                   << m_frame << m_positionH << m_nFirstADC << m_nFirstSer
                   << m_nFirstTable << m_nFirstMsg << nsh_type( m_SHid.size( ) )
                   << m_SHid << m_SHname
                   << ndetector_type( m_nameDetector.size( ) ) << m_nameDetector
                   << m_positionDetector;
        }

    } // namespace Version_6
} // namespace FrameCPP
