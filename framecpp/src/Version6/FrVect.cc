//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>
#include <algorithm>
#include <stdexcept>

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Compression.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version6/FrVect.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_6;

using namespace FrameCPP::Version_6;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using namespace FrameCPP::Compression;

#define LM_DEBUG 0
#define LM_INFO 0

#if LM_DEBUG
#define AT( ) std::cerr << "INFO: " << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

#if HAVE_TEMPLATE_MOVE
#define MOVE_RET( a ) ( std::move( a ) )
#else /* HAVE_TEMPLATE_MOVE */
#define MOVE_RET( a ) ( a )
#endif /* HAVE_TEMPLATE_MOVE */

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_VECT;

namespace
{
    //=====================================================================
    // Local enumerations
    //=====================================================================
    enum
    {
        MODE_RAW = FrVect::BIGENDIAN_RAW,
        MODE_GZIP = FrVect::BIGENDIAN_GZIP,
        MODE_DIFF_GZIP = FrVect::BIGENDIAN_DIFF_GZIP,
        MODE_ZERO_SUPPRESS_SHORT = FrVect::BIGENDIAN_ZERO_SUPPRESS_SHORT,
        MODE_ZERO_SUPPRESS_INT_FLOAT =
            FrVect::BIGENDIAN_ZERO_SUPPRESS_INT_FLOAT,

        MODE_ZERO_SUPPRESS_SHORT_GZIP_OTHER =
            FrVect::ZERO_SUPPRESS_SHORT_GZIP_OTHER,
        MODE_ZERO_SUPPRESS_OTHERWISE_GZIP = FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP
    };

    enum
    {
        MODE_BIGENDIAN = FrVect::BIGENDIAN_RAW,
        MODE_LITTLEENDIAN = FrVect::LITTLEENDIAN_RAW,
        MODE_HOSTENDIAN = FrVect::RAW
    };

    //=====================================================================
    // Local classes
    //=====================================================================
    class DimBuffer
    {
    public:
        DimBuffer( )
        {
        }

        DimBuffer( const std::vector< Dimension >& Dims );

        void copy( std::vector< Dimension >& Dims );

        static INT_8U GetMinimumBytes( );

    private:
        friend class FrameCPP::Common::IStream;
        friend class FrameCPP::Common::OStream;

        std::vector< INT_8U > m_nx;
        std::vector< REAL_8 > m_dx;
        std::vector< REAL_8 > m_startX;
        std::vector< STRING > m_unitX;
    };

    inline INT_8U
    DimBuffer::GetMinimumBytes( )
    {
        return sizeof( INT_8U ) + sizeof( REAL_8 ) + sizeof( REAL_8 ) +
            STRING::Bytes( "" );
    }

    //=====================================================================
    // Local function prototypes
    //=====================================================================
    FrameCPP::Compression::data_type_mapping&     data_type_map_init( );
    FrameCPP::Compression::compress_type_mapping& compress_type_map_init( );
    FrameCPP::Compression::compress_type_reverse_mapping&
    compress_type_reverse_map_init( );

    //=====================================================================
    // Local variables
    //=====================================================================
    static const FrameCPP::Compression::data_type_mapping& data_type_map =
        data_type_map_init( );

    //=====================================================================
    // Local functions
    //=====================================================================
    INT_8U
    calc_nData( const std::vector< Dimension >& Dimensions )
    {
        INT_8U ret( 0 );
        if ( Dimensions.size( ) > 0 )
        {
            ret = 1;
            for ( std::vector< Dimension >::const_iterator i(
                      Dimensions.begin( ) );
                  i != Dimensions.end( );
                  i++ )
            {
                ret *= ( *i ).GetNx( );
            }
        }
        return ret;
    }

    FrameCPP::Compression::compress_type_mapping&
    compress_type_map_init( )
    {
        static FrameCPP::Compression::compress_type_mapping m;

#define INIT( x, y ) m[ FrVect::BIGENDIAN_##x ] = FrameCPP::Compression::y

        INIT( RAW, MODE_RAW );
        INIT( GZIP, MODE_GZIP );
        INIT( DIFF_GZIP, MODE_DIFF_GZIP );
        INIT( ZERO_SUPPRESS_SHORT, MODE_ZERO_SUPPRESS_SHORT );
        INIT( ZERO_SUPPRESS_INT_FLOAT, MODE_ZERO_SUPPRESS_INT_FLOAT );

#undef INIT
#define INIT( x, y ) m[ FrVect::x ] = FrameCPP::Compression::y

        INIT( ZERO_SUPPRESS_SHORT_GZIP_OTHER,
              MODE_ZERO_SUPPRESS_2_OTHERWISE_GZIP );
        INIT( ZERO_SUPPRESS_OTHERWISE_GZIP,
              MODE_ZERO_SUPPRESS_2_4_OTHERWISE_GZIP );

#undef INIT
        return m;
    }

    FrameCPP::Compression::compress_type_reverse_mapping&
    compress_type_reverse_map_init( )
    {
        static FrameCPP::Compression::compress_type_reverse_mapping m;

#define INIT( x, y ) m[ FrameCPP::Compression::x ] = FrVect::BIGENDIAN_##y

        INIT( MODE_RAW, RAW );
        INIT( MODE_GZIP, GZIP );
        INIT( MODE_DIFF_GZIP, DIFF_GZIP );
        INIT( MODE_ZERO_SUPPRESS_SHORT, ZERO_SUPPRESS_SHORT );
        INIT( MODE_ZERO_SUPPRESS_INT_FLOAT, ZERO_SUPPRESS_INT_FLOAT );

#undef INIT
        return m;
    }

    FrameCPP::Compression::data_type_mapping&
    data_type_map_init( )
    {
        static FrameCPP::Compression::data_type_mapping m;

#define INIT( x ) m[ FrameCPP::Version_6::FrVect::x ] = FrameCPP::Compression::x

        INIT( FR_VECT_C );
        INIT( FR_VECT_2S );
        INIT( FR_VECT_8R );
        INIT( FR_VECT_4R );
        INIT( FR_VECT_4S );
        INIT( FR_VECT_8S );
        INIT( FR_VECT_8C );
        INIT( FR_VECT_16C );
        INIT( FR_VECT_STRING );
        INIT( FR_VECT_2U );
        INIT( FR_VECT_4U );
        INIT( FR_VECT_8U );
        INIT( FR_VECT_1U );

#undef INIT
        return m;
    }

} // namespace

static const FrameCPP::Compression::compress_type_mapping& compress_type_map =
    compress_type_map_init( );

static const FrameCPP::Compression::compress_type_reverse_mapping&
    compress_type_reverse_map = compress_type_reverse_map_init( );

namespace
{
    INT_8U
    Bytes( const std::vector< Dimension >& Dims )
    {
        INT_8U ret( sizeof( INT_4U ) ); // nDim
        for ( std::vector< Dimension >::const_iterator d( Dims.begin( ) );
              d != Dims.end( );
              d++ )
        {
            ret += sizeof( INT_8U ) + // nx
                sizeof( REAL_8 ) + // dx
                sizeof( REAL_8 ) + // startX
                ( *d ).GetUnitX( ).Bytes( );
        }
        return ret;
    } // Bytes

    DimBuffer::DimBuffer( const std::vector< Dimension >& Dims )
    {
        INT_4U nDim( Dims.size( ) );

        m_nx.resize( nDim );
        m_dx.resize( nDim );
        m_startX.resize( nDim );
        m_unitX.resize( nDim );

        for ( INT_4U x = 0; x < nDim; x++ )
        {
            m_nx[ x ] = Dims[ x ].GetNx( );
            m_dx[ x ] = Dims[ x ].GetDx( );
            m_startX[ x ] = Dims[ x ].GetStartX( );
            m_unitX[ x ] = Dims[ x ].GetUnitX( );
        }
    }

    void
    DimBuffer::copy( std::vector< Dimension >& Dims )
    {
        INT_4U nDim( m_nx.size( ) );

        Dims.resize( nDim );
        for ( INT_4U x = 0; x < nDim; x++ )
        {
            Dims[ x ] =
                Dimension( m_nx[ x ], m_dx[ x ], m_unitX[ x ], m_startX[ x ] );
        }
    }

} // namespace

namespace FrameCPP
{
    namespace Common
    {
        /// \cond ignore
        template <>
        OStream&
        OStream::operator<<( const DimBuffer& Dims )
        {
            INT_4U nDim( Dims.m_nx.size( ) );

            *this << nDim << Dims.m_nx << Dims.m_dx << Dims.m_startX
                  << Dims.m_unitX;
            return *this;
        }
        /// \endcond ignore

        /// \cond ignore
        template <>
        IStream&
        IStream::operator>>( DimBuffer& Dims )
        {
            INT_4U nDim( 0 );
            *this >> nDim;

            Dims.m_nx.resize( nDim );
            Dims.m_dx.resize( nDim );
            Dims.m_startX.resize( nDim );
            Dims.m_unitX.resize( nDim );

            *this >> Dims.m_nx >> Dims.m_dx >> Dims.m_startX >> Dims.m_unitX;

            return *this;
        }
        /// \endcond ignore
    } // namespace Common
} // namespace FrameCPP

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrVect::getStaticName( ) << " "       \
              << (void*)this << " Line: " << __LINE__ << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrVect::getStaticName( ) << " "         \
              << (void*)this << " Line: " << __LINE__ << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

//=======================================================================
// CLASS - FrVect::data_definition_type
//=======================================================================

namespace FrameCPP
{
    namespace Version_6
    {
        FrVect::data_definition_type::data_definition_type( )
            : name( "" ), type( FR_VECT_C ), dimension( ), unitY( "" )
        {
        }

        FrVect::data_definition_type::data_definition_type(
            const std::string&    n,
            INT_2U                t,
            const byte_order_type byte_order,
            INT_4U                ndim,
            const Dimension*      dims,
            const std::string&    unit )
            : name( n ), type( t ), dimension( ), unitY( unit )
        {
            compress =
                ( ( byte_order == BYTE_ORDER_BIG_ENDIAN ) ? BIGENDIAN_RAW
                                                          : LITTLEENDIAN_RAW );

            for ( INT_4U x = 0; x < ndim; ++x, ++dims )
            {
                dimension.push_back( *dims );
            }

            nData = calc_nData( dimension );
            nBytes = GetTypeSize( type ) * nData;
        }

        bool
        FrVect::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrVect::demote_ret_type
        FrVect::demote( INT_2U              Target,
                        demote_arg_type     Obj,
                        demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                std::vector< Previous::Dimension > dims;
                for ( std::vector< Dimension >::const_iterator
                          cur = m_data.dimension.begin( ),
                          last = m_data.dimension.end( );
                      cur != last;
                      ++cur )
                {
                    dims.push_back( Previous::Dimension( cur->GetNx( ),
                                                         cur->GetDx( ),
                                                         cur->GetUnitX( ),
                                                         cur->GetStartX( ) ) );
                }
                // Do actual down conversion
                boost::shared_ptr< Previous::FrVect > retval(
                    new Previous::FrVect( GetName( ),
                                          GetCompress( ),
                                          GetType( ),
                                          dims.size( ),
                                          &( dims[ 0 ] ),
                                          GetNData( ),
                                          GetNBytes( ),
                                          GetDataRaw( ),
                                          GetUnitY( ) ) );
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented( "Object* FrVect::demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrVect::promote_ret_type
        FrVect::promote( INT_2U               Target,
                         promote_arg_type     Obj,
                         promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

        void
        FrVect::data_definition_type::copy_core(
            const data_definition_type& Source )
        {
            name = Source.name;
            compress = Source.compress;
            type = Source.type;
            nData = Source.nData;
            nBytes = Source.nBytes;

            for ( std::vector< Dimension >::const_iterator
                      d( Source.dimension.begin( ) ),
                  d_end( Source.dimension.end( ) );
                  d != d_end;
                  ++d )
            {
                dimension.push_back( *d );
            }

            unitY = Source.unitY;
        }

        bool
        FrVect::data_definition_type::
        operator==( const FrVect::data_definition_type& RHS ) const
        {
#if LM_INFO
            if ( name != RHS.name )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( compress != RHS.compress )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( type != RHS.type )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( nData != RHS.nData )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( nBytes != RHS.nBytes )
            {
                std::cerr << "DEBUG: " << __LINE__ << " nBytes: " << nBytes
                          << " RHS.nBytes: " << RHS.nBytes << std::endl;
            }
            if ( dimension != RHS.dimension )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( unitY != RHS.unitY )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( std::equal( data, data + nBytes, RHS.data ) == false )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            std::cerr << "DEBUG: nData: " << nData << std::endl;
#endif
            return ( ( this == &RHS ) ||
                     ( ( name == RHS.name ) && ( compress == RHS.compress ) &&
                       ( type == RHS.type ) && ( nData == RHS.nData ) &&
                       ( nBytes == RHS.nBytes ) &&
                       ( dimension == RHS.dimension ) &&
                       ( unitY == RHS.unitY ) &&
                       ( ( data == RHS.data ) ||
                         ( std::equal( data.get( ),
                                       data.get( ) + nBytes,
                                       RHS.data.get( ) ) ) ) ) );
        }

        //=======================================================================
        // CLASS - FrVect
        //=======================================================================
        const int FrVect::DEFAULT_GZIP_LEVEL( 6 );

        FrVect::FrVect( )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              m_data( )
        {
            MEM_ALLOCATE( );
        }

        FrVect::FrVect( const FrVect& Source )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              Common::FrVect( Source ), m_data( )
        {
            MEM_ALLOCATE( );

            m_data.copy_core( Source.m_data );
            data_copy( Source.m_data.data.get( ), Source.GetNBytes( ) );
        }

        FrVect::FrVect( const std::string&    name,
                        INT_2U                type,
                        INT_4U                nDim,
                        const Dimension*      dims,
                        const byte_order_type byte_order,
                        const void*           data,
                        const std::string&    unitY )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              m_data( name, type, byte_order, nDim, dims, unitY )
        {
            MEM_ALLOCATE( );
            data_copy( reinterpret_cast< data_const_pointer_type >( data ),
                       m_data.nBytes );
        }

        FrVect::FrVect( const std::string&    name,
                        INT_2U                type,
                        INT_4U                nDim,
                        const Dimension*      dims,
                        const byte_order_type byte_order,
                        void*                 data,
                        const std::string&    unitY )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              m_data( name, type, byte_order, nDim, dims, unitY )
        {
            MEM_ALLOCATE( );
            data_copy( static_cast< data_pointer_type >( data ),
                       m_data.nBytes );
        }

        FrVect::FrVect( const Previous::FrVect& Source,
                        stream_base_type*       Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data.name = Source.GetName( );
            m_data.compress = Source.GetCompress( );
            m_data.type = Source.GetType( );
            m_data.nData = Source.GetNData( );
            m_data.nBytes = Source.GetNBytes( );
            m_data.data = Source.GetDataRaw( );
            for ( nDim_type cur = 0, last = Source.GetNDim( ); cur != last;
                  ++cur )
            {
                m_data.dimension.push_back( Dimension( Source.GetDim( cur ) ) );
            }
            m_data.unitY = Source.GetUnitY( );
        }

        /** \cond ignore_no_uniquely */
        FrVect::FrVect( const std::string& name,
                        compress_type      Compress,
                        type_type          type,
                        nDim_type          nDim,
                        const Dimension*   dims,
                        nData_type         NData,
                        nBytes_type        NBytes,
                        void*              data,
                        const std::string& unitY )
            : Common::FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              m_data( name, type, BYTE_ORDER_HOST, nDim, dims, unitY )
        {
            m_data.compress = Compress;
            m_data.nData = NData;
            data_copy( ( data_pointer_type )( data ), NBytes );
        }
        /** \endcond */

        /** \cond ignore_no_uniquely */
        FrVect::FrVect( const std::string& name,
                        compress_type      Compress,
                        type_type          type,
                        nDim_type          nDim,
                        const Dimension*   dims,
                        nData_type         NData,
                        nBytes_type        NBytes,
                        data_type          Data,
                        const std::string& unitY )
            : Common::FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION ),
              m_data( name, type, BYTE_ORDER_HOST, nDim, dims, unitY )
        {
            m_data.compress = Compress;
            m_data.nData = NData;
            m_data.nBytes = NBytes;
            m_data.data = Data;
        }
        /** \endcond */

        FrVect::FrVect( istream_type& Stream )
            : FrameSpec::Object(
                  s_object_id, StructDescription( ), DATA_FORMAT_VERSION )
        {
            //---------------------------------------------------------------------
            // Read information from the stream
            //---------------------------------------------------------------------
            DimBuffer dims;

            Stream >> m_data.name >> m_data.compress >> m_data.type >>
                m_data.nData >> m_data.nBytes;
            m_data.data.reset( new data_type::element_type[ m_data.nBytes ] );
            Stream.read( reinterpret_cast< istream_type::char_type* >(
                             m_data.data.get( ) ),
                         m_data.nBytes );
            Stream >> dims >> m_data.unitY;
            Stream.Next( this );
            //---------------------------------------------------------------------
            // Reorder dimension data
            //---------------------------------------------------------------------
            dims.copy( m_data.dimension );
        }

        FrVect::~FrVect( )
        {
            MEM_DELETE( );
        }

        FrVect*
        FrVect::Create( istream_type& Stream ) const
        {
            return new FrVect( Stream );
        }

        const char*
        FrVect::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrVect::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrVect::StructName( ),
                           s_object_id,
                           "Vector Data Structure" ) );

                ret( FrSE( "name", "STRING", "" ) );
                ret( FrSE( "compress", "INT_2U", "" ) );
                ret( FrSE( "type", "INT_2U", "" ) );
                ret( FrSE( "nData", "INT_8U", "" ) );
                ret( FrSE( "nBytes", "INT_8U", "" ) );
                ret( FrSE( "data", "CHAR[nBytes]", "" ) );
                ret( FrSE( "nDim", "INT_4U", "" ) );
                ret( FrSE( "nx", "INT_8U[nDim]", "" ) );
                ret( FrSE( "dx", "REAL_8[nDim]", "" ) );
                ret( FrSE( "startX", "REAL_8[nDim]", "" ) );
                ret( FrSE( "unitX", "STRING[nDim]", "" ) );
                ret( FrSE( "unitY", "STRING", "" ) );

                ret(
                    FrSE( "next", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
            }

            return &ret;
        }

        const std::string&
        FrVect::GetName( ) const
        {
            return m_data.name;
        }

        void
        FrVect::Compress( compression_scheme_type Scheme, int Level )
        {
            AT( );
            if ( Scheme == GetCompress( ) )
            {
                // Nothing to do. Just return
                AT( );
                return;
            }

            //---------------------------------------------------------------------
            // Uncompress the data first
            //---------------------------------------------------------------------
            Uncompress( );

            //---------------------------------------------------------------------
            // Compress the data
            //---------------------------------------------------------------------

            INT_4U                        compress = ( Scheme & 0xFF );
            const INT_8U                  nData( m_data.nData );
            const INT_8U                  nBytes( m_data.nBytes );
            boost::shared_array< CHAR_U > data_out( (CHAR_U*)NULL );
            INT_8U                        data_out_len( 0 );

            FrameCPP::Compression::Compress( compress,
                                             Level,
                                             compress_type_map,
                                             compress_type_reverse_map,
                                             GetType( ),
                                             data_type_map,
                                             m_data.data.get( ),
                                             nData,
                                             nBytes,
                                             data_out,
                                             data_out_len );

            if ( data_out && data_out_len )
            {
                //-------------------------------------------------------------------
                // Save the compressed data
                //-------------------------------------------------------------------
                m_data.nBytes = nBytes_type( data_out_len );
                m_data.data = data_out;
                //-------------------------------------------------------------------
                // Save the compression mode
                //-------------------------------------------------------------------
                m_data.compress = ( MODE_HOSTENDIAN | ( compress & 0xFF ) );
            }
        }

        void
        FrVect::CompressData( INT_4U Scheme, INT_2U GZipLevel )
        {
            Compress( compression_scheme_type( Scheme ), GZipLevel );
        }

        FrameCPP::Common::FrameSpec::Object*
        FrVect::CloneCompressed( cmn_compression_scheme_type Scheme,
                                 cmn_compression_level_type  Level ) const
        {
            Common::FrameSpec::ObjectInterface::unique_object_type retval;
            boost::shared_array< CHAR_U > compressed_data;
            INT_8U                        nbytes( 0 );

            AT( );
            if ( Scheme == GetCompress( ) )
            {
                // Nothing to do. Just return
                AT( );
                return retval.release( );
            }

            compressToBuffer( compress_type_map,
                              compress_type_reverse_map,
                              data_type_map,
                              GetType( ),
                              GetNData( ),
                              GetDataRaw( ).get( ),
                              GetNBytes( ),
                              GetCompress( ),
                              compressed_data,
                              nbytes,
                              Scheme,
                              Level );

            if ( nbytes && compressed_data.get( ) )
            {
                //-------------------------------------------------------------------
                // Save the compressed data
                //-------------------------------------------------------------------
                retval.reset(
                    new FrVect( GetName( ),
                                ( MODE_HOSTENDIAN | ( Scheme & 0xFF ) ),
                                GetType( ),
                                GetNDim( ),
                                &( m_data.dimension[ 0 ] ),
                                GetNData( ),
                                nbytes,
                                compressed_data,
                                GetUnitY( ) ) );
            }
            //---------------------------------------------------------------------
            // Return the results
            //---------------------------------------------------------------------
            return retval.release( );
        }

        //-----------------------------------------------------------------------------
        //
        //: Expand compressed data.
        //
        // In general, uncompressing follows three steps:
        //
        // 1) Gunzip the data.
        // 2) Fix byte-ordering.
        // 3) Integrate.
        //
        // Depending upon the compression type and byte-ordering differences,
        // not all of these steps are needed.
        //
        void
        FrVect::Uncompress( )
        {
            AT( );
            //---------------------------------------------------------------------
            // Check if there is anything that needs to be done
            //---------------------------------------------------------------------
            if ( GetCompress( ) == RAW )
            {
                // Nothing to do. The buffer is already uncompressed
                AT( );
                return;
            }

            //---------------------------------------------------------------------
            // There is something to be done
            //---------------------------------------------------------------------
            INT_8U                        nBytes( m_data.nBytes );
            boost::shared_array< CHAR_U > expanded_buffer;

            expandToBuffer( expanded_buffer, nBytes );

            //---------------------------------------------------------------------
            // Now record what has been done
            //---------------------------------------------------------------------
            m_data.data = expanded_buffer;
            m_data.nBytes = nBytes_type( nBytes );
            m_data.compress = RAW;
        }

        bool
        FrVect::operator!=( const FrVect& RHS ) const
        {
            return !( *this == RHS );
        }

        bool
        FrVect::operator==( const FrVect& RHS ) const
        {
            return ( this->m_data == RHS.m_data );
        }

        FrVect&
        FrVect::operator+=( const FrVect& RHS )
        {
            //---------------------------------------------------------------------
            // Perform sanity checks
            //---------------------------------------------------------------------
            if ( ( this->GetName( ) != RHS.GetName( ) ) ||
                 ( this->GetType( ) != RHS.GetType( ) ) ||
                 ( this->GetNDim( ) != RHS.GetNDim( ) ) ||
                 ( this->GetUnitY( ) != RHS.GetUnitY( ) ) )
            {
                std::ostringstream msg;
                bool               comma;

                msg << "Unable to concat the FrVect structures because: ";
                comma = false;
                if ( this->GetName( ) != RHS.GetName( ) )
                {
                    msg << "name of objects differ (" << this->GetName( )
                        << " vs. " << RHS.GetName( ) << ")";
                    comma = true;
                }
                if ( this->GetType( ) != RHS.GetType( ) )
                {
                    if ( comma )
                    {
                        msg << ", ";
                    }
                    msg << "type of objects differ (" << this->GetType( )
                        << " vs. " << RHS.GetType( ) << ")";
                    comma = true;
                }
                if ( this->GetNDim( ) != RHS.GetNDim( ) )
                {
                    if ( comma )
                    {
                        msg << ", ";
                    }
                    msg << "number of dimensions differ (" << this->GetNDim( )
                        << " vs. " << RHS.GetNDim( ) << ")";
                    comma = true;
                }
                if ( this->GetUnitY( ) != RHS.GetUnitY( ) )
                {
                    if ( comma )
                    {
                        msg << ", ";
                    }
                    msg << "y units differ (" << this->GetUnitY( ) << " vs. "
                        << RHS.GetUnitY( ) << ")";
                    comma = true;
                }
                throw std::domain_error( msg.str( ) );
            }
            for ( INT_4U cur = 0, last = this->GetNDim( ); cur != last; ++cur )
            {
                const Dimension& ld( this->GetDim( cur ) );
                const Dimension& rd( RHS.GetDim( cur ) );

                if ( cur > 0 )
                {
                    if ( ld != rd )
                    {
                        std::ostringstream msg;

                        msg << "Unable to concat the FrVect structures";
                        //:TODO: Need to have a list of reasons
                        throw std::domain_error( msg.str( ) );
                    }
                }
                else
                {
                    if ( ( ld.GetNx( ) != rd.GetNx( ) ) ||
                         ( ld.GetStartX( ) != rd.GetStartX( ) ) ||
                         ( ld.GetUnitX( ) != rd.GetUnitX( ) ) )
                    {
                        std::ostringstream msg;

                        msg << "Unable to concat the FrVect structures";
                        //:TODO: Need to have a list of reasons
                        throw std::domain_error( msg.str( ) );
                    }
                }
            }
            //---------------------------------------------------------------------
            // Calculate how much space is required
            //---------------------------------------------------------------------
            const INT_2U type_size( GetTypeSize( GetType( ) ) );
            this->GetDim( 0 ).SetNx( this->GetDim( 0 ).GetNx( ) +
                                     RHS.GetDim( 0 ).GetNx( ) );
            const INT_8U s = GetNData( ) + RHS.GetNData( );

            //---------------------------------------------------------------------
            // Allocate the space, fill it with the existing data and then store
            //   it.
            //---------------------------------------------------------------------
            data_pointer_type rhs_data( RHS.GetDataRaw( ).get( ) );
            boost::shared_array< data_element_type > rhs_data_expanded;
            nBytes_type                              rhs_data_nbytes;

            expandToBuffer( rhs_data_expanded, rhs_data_nbytes );
            if ( rhs_data_nbytes )
            {
                rhs_data = rhs_data_expanded.get( );
            }

            {
                const data_pointer_type source( GetDataUncompressed( ).get( ) );
                data_type               data( new CHAR_U[ s ] );

                std::copy(
                    source, source + ( GetNData( ) * type_size ), data.get( ) );
                std::copy( rhs_data,
                           rhs_data + ( RHS.GetNData( ) * type_size ),
                           data.get( ) + ( GetNData( ) * type_size ) );
                m_data.data = data;
            }
            //---------------------------------------------------------------------
            // Return
            //---------------------------------------------------------------------
            return *this;
        }

        FrameCPP::cmn_streamsize_type
        FrVect::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( m_data.compress ) +
                sizeof( m_data.type ) + sizeof( m_data.nData ) +
                sizeof( m_data.nBytes ) + m_data.nBytes // data
                + ::Bytes( m_data.dimension ) + m_data.unitY.Bytes( ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrVect::data_pointer_type
        FrVect::GetDataUncompressed(
            boost::shared_array< CHAR_U >& Expanded ) const
        {
            nBytes_type nBytes( 0 );

            expandToBuffer( Expanded, nBytes );
            if ( nBytes == 0 )
            {
                return m_data.data.get( );
            }

            return Expanded.get( );
        }

        template <>
        INT_2U
        FrVect::GetDataType< CHAR >( )
        {
            return FR_VECT_C;
        }

        template <>
        INT_2U
        FrVect::GetDataType< CHAR_U >( )
        {
            return FR_VECT_1U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_2S >( )
        {
            return FR_VECT_2S;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_2U >( )
        {
            return FR_VECT_2U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_4S >( )
        {
            return FR_VECT_4S;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_4U >( )
        {
            return FR_VECT_4U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_8S >( )
        {
            return FR_VECT_8S;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_8U >( )
        {
            return FR_VECT_8U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< REAL_4 >( )
        {
            return FR_VECT_4R;
        }

        template <>
        INT_2U
        FrVect::GetDataType< REAL_8 >( )
        {
            return FR_VECT_8R;
        }

        template <>
        INT_2U
        FrVect::GetDataType< COMPLEX_8 >( )
        {
            return FR_VECT_8C;
        }

        template <>
        INT_2U
        FrVect::GetDataType< COMPLEX_16 >( )
        {
            return FR_VECT_16C;
        }

        template <>
        INT_2U
        FrVect::GetDataType< std::string >( )
        {
            return FR_VECT_STRING;
        }

        INT_8U
        FrVect::GetNData( ) const
        {
            return m_data.nData;
        }
        //-----------------------------------------------------------------------------
        //
        //: Get size of single data point of particular size.
        //
        //! param: INT_2U type - Data type ID.
        //
        //! return: INT_2U - Data size.
        //
        //! exc: None.
        size_t
        FrVect::GetTypeSize( INT_2U type )
        {
            switch ( type )
            {
            case FR_VECT_C:
                return sizeof( CHAR );
            case FR_VECT_2S:
                return sizeof( INT_2S );
            case FR_VECT_8R:
                return sizeof( REAL_8 );
            case FR_VECT_4R:
                return sizeof( REAL_4 );
            case FR_VECT_4S:
                return sizeof( INT_4S );
            case FR_VECT_8S:
                return sizeof( INT_8S );
            case FR_VECT_8C:
                return ( sizeof( REAL_4 ) * 2 );
            case FR_VECT_16C:
                return ( sizeof( REAL_8 ) * 2 );
                /*
                  ????
                  case FR_VECT_STR:
                  return sizeof( std::std::string );
                  ????
                */
            case FR_VECT_2U:
                return sizeof( INT_2U );
            case FR_VECT_4U:
                return sizeof( INT_4U );
            case FR_VECT_8U:
                return sizeof( INT_8U );
            case FR_VECT_1U:
                return sizeof( CHAR_U );
            }
            // None of above: Perhaps add type for throw?
            return 0;
        }

        FrVect&
        FrVect::Merge( const FrVect& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        FrVect::subfrvect_type
        FrVect::SubFrVect( INT_4U Start, INT_4U Stop ) const
        {
            //---------------------------------------------------------------------
            // Create new FrVect
            //---------------------------------------------------------------------
            subfrvect_type retval( new FrVect );
            //---------------------------------------------------------------------
            // Calculate the block size
            //---------------------------------------------------------------------
            INT_4U       block_size = GetTypeSize( GetType( ) );
            const INT_4U ndim = GetNDim( );

            for ( INT_4U x = 1; x < ndim; ++x )
            {
                block_size *= GetDim( x ).GetNx( );
            }
            //---------------------------------------------------------------------
            // Copy the core of the information
            //---------------------------------------------------------------------
            retval->m_data.copy_core( this->m_data );
            //---------------------------------------------------------------------
            // Set the Dimension information for the sink
            //---------------------------------------------------------------------
            retval->GetDim( 0 ).SetNx( Stop - Start );
            retval->SetNData( Stop - Start );
            //---------------------------------------------------------------------
            // Allocate space in the sink
            //---------------------------------------------------------------------
            retval->data_alloc( block_size * ( Stop - Start ) );
            retval->m_data.compress = RAW;
            //---------------------------------------------------------------------
            // Copy the data
            //---------------------------------------------------------------------
            data_pointer_type             data_source( GetDataRaw( ).get( ) );
            INT_8U                        nBytes( 0 );
            boost::shared_array< CHAR_U > expanded_buffer;

            expandToBuffer( expanded_buffer, nBytes );
            if ( nBytes )
            {
                data_source = expanded_buffer.get( );
            }
            data_pointer_type data_sink( retval->GetDataRaw( ).get( ) );

            for ( INT_8U x = Start; x < Stop; ++x )
            {
                //-------------------------------------------------------------------
                // Copy 1 block's worth of data
                //-------------------------------------------------------------------
                std::copy( data_source + ( x * block_size ),
                           data_source + ( ( x + 1 ) * block_size ),
                           data_sink + ( ( x - Start ) * block_size ) );
            }
            //---------------------------------------------------------------------
            // Return the newly constructed FrVect
            //---------------------------------------------------------------------
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
            return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
        }

        //-----------------------------------------------------------------------
        /// This routine is called to verify the integrety of
        /// this FrVect object.
        //-----------------------------------------------------------------------
        void
        FrVect::VerifyObject( Common::Verify&       Verifier,
                              Common::IFrameStream& Stream ) const
        {
            if ( Verifier.Expandability( ) )
            {
                //-------------------------------------------------------------------
                // Verify that the buffer can be decompressed
                //-------------------------------------------------------------------
                INT_8U                        nBytes( 0 );
                boost::shared_array< CHAR_U > expanded_buffer;

                expandToBuffer( expanded_buffer, nBytes );
            }
        }

        void
        FrVect::Write( ostream_type& Stream ) const
        {
            //---------------------------------------------------------------------
            // Write data to the stream.
            //---------------------------------------------------------------------
            DimBuffer dims( m_data.dimension );
            Stream << m_data.name << m_data.compress << m_data.type
                   << m_data.nData << m_data.nBytes;
            Stream.write( reinterpret_cast< const ostream_type::char_type* >(
                              m_data.data.get( ) ),
                          m_data.nBytes );
            Stream << dims << m_data.unitY;
            WriteNext( Stream );
        }

        //=======================================================================
        // Local functions
        //=======================================================================
        void
        FrVect::data_copy( data_const_pointer_type Data, nBytes_type NBytes )
        {
            if ( ( NBytes == 0 ) || ( Data == (const data_pointer_type)NULL ) )
            {
                m_data.data.reset( );
            }
            else
            {
                if ( ( m_data.nBytes != NBytes ) || ( !m_data.data ) )
                {
                    m_data.data.reset( new CHAR_U[ NBytes ] );
                    m_data.nBytes = NBytes;
                }
                std::copy( Data, Data + NBytes, m_data.data.get( ) );
            }
        }

        void
        FrVect::expandToBuffer( boost::shared_array< CHAR_U >& Dest,
                                INT_8U& DestNBytes ) const
        {
            Common::FrVect::expandToBuffer( compress_type_map,
                                            data_type_map,
                                            GetType( ),
                                            GetNData( ),
                                            GetDataRaw( ).get( ),
                                            GetNBytes( ),
                                            GetCompress( ),
                                            ( ( GetCompress( ) & 0x100 )
                                                  ? BYTE_ORDER_LITTLE_ENDIAN
                                                  : BYTE_ORDER_BIG_ENDIAN ),
                                            Dest,
                                            DestNBytes );
        }
    } // namespace Version_6
} // namespace FrameCPP

#if !defined( __SUNPRO_CC ) || ( __SUNPRO_CC > 0x550 )
#include "framecpp/Version6/FrVect.tcc"

#define INSTANTIATE( LM_TYPE )                                                 \
    template FrVect::FrVect( const std::string& name,                          \
                             INT_4U             nDim,                          \
                             const Dimension*   dims,                          \
                             const LM_TYPE*     data,                          \
                             const std::string& unitY );                       \
                                                                               \
    template FrVect::FrVect( const std::string& name,                          \
                             INT_4U             nDim,                          \
                             const Dimension*   dims,                          \
                             LM_TYPE*           data,                          \
                             const std::string& unitY )

#include "TypeInstantiation.tcc"

#undef INSTANTIATE

#endif /* */
