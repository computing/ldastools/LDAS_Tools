//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#define ACCESS_FRAME_H_PRIVATE_PARTS 1

#include "ldastoolsal/types.hh"

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version4/FrameH.hh"

#include "framecpp/Version6/FrameH.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrEndOfFrame.hh"
#include "framecpp/Version6/PTR_STRUCT.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrameH::getStaticName( ) << " "       \
              << (void*)this << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrameH::getStaticName( ) << " "         \
              << (void*)this << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

namespace FrameCPP
{
    namespace Version_6
    {
        FrameHNPS::FrameHNPS( )
        {
        }

        FrameHNPS::FrameHNPS( const FrameHNPS& Source )
            : Common::TOCInfo( Source ), m_data( Source.m_data )
        {
        }

        FrameHNPS::FrameHNPS( const std::string& name,
                              INT_4S             run,
                              INT_4U             frame,
                              const GPSTime&     time,
                              INT_2U             uLeapS,
                              const REAL_8       dt,
                              INT_4U             dqual )
            : m_data( name, run, frame, time, uLeapS, dt, dqual )
        {
        }

        void
        FrameHNPS::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_INT_2S:
                {
                    INT_2S* data = va_arg( vl, INT_2S* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_ULEAP_S:
                        *data = GetULeapS( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4S:
                {
                    INT_4S* data = va_arg( vl, INT_4S* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_RUN:
                        *data = GetRun( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_DATA_QUALITY:
                        *data = GetDataQuality( );
                        break;
                    case TOCInfo::IC_GTIME_S:
                        *data = GetGTime( ).GetSeconds( );
                        break;
                    case TOCInfo::IC_GTIME_N:
                        *data = GetGTime( ).GetNanoseconds( );
                        break;
                    case TOCInfo::IC_FRAME:
                        *data = GetFrame( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_8:
                {
                    REAL_8* data = va_arg( vl, REAL_8* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_DT:
                        *data = GetDt( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        //=======================================================================
        //
        //=======================================================================
        FrameH::FrameH( )
            : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION )
        {
            MEM_ALLOCATE( );
        }

        FrameH::FrameH( const FrameH& frame )
            : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION ),
              FrameHNPS( frame ), FrameHPS( frame )
        {
        }

        FrameH::FrameH( const std::string& name,
                        INT_4S             run,
                        INT_4U             frame,
                        const GPSTime&     time,
                        INT_2U             uLeapS,
                        const REAL_8       dt,
                        INT_4U             dqual )
            : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION ),
              FrameHNPS( name, run, frame, time, uLeapS, dt, dqual )
        {
            MEM_ALLOCATE( );
        }

        FrameH::FrameH( const FrameHNPS& Source )
            : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION ),
              FrameHNPS( Source )
        {
        }

        FrameH::FrameH( istream_type& Stream )
            : Common::FrameH( StructDescription( ), DATA_FORMAT_VERSION )
        {
            m_data( Stream );
            m_refs( Stream );
        }

        FrameH::~FrameH( )
        {
            MEM_DELETE( );
        }

        FrameCPP::cmn_streamsize_type
        FrameH::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( ) + m_refs.Bytes( );
        }

        FrameH*
        FrameH::Clone( ) const
        {
            return new FrameH( *this );
        }

        FrameH*
        FrameH::Create( istream_type& Stream ) const
        {
            return new FrameH( Stream );
        }

        const char*
        FrameH::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrameH::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrameH::StructName( ),
                           FrameCPP::Common::FrameH::s_object_id,
                           "Frame Header Structure" ) );
                dataDescription< FrSE >( ret );
                refDescription< FrSE >( ret );
            }

            return &ret;
        }

        void
        FrameH::Write( OStream& Stream ) const
        {
#if WORKING
            //---------------------------------------------------------------------
            // Check to see if per frame checksums are to be calculated
            //---------------------------------------------------------------------
            if ( Stream.IsCalculatingChecksumFrame( ) )
            {
                //-------------------------------------------------------------------
                // Yes ...
                // 1) Reset the buffer
                // 2) Add in the FrBase info
                //-------------------------------------------------------------------
                Stream.SetChecksumMethodFrame( );

                INT_8U length = GetLength( );
                INT_2U class_id = GetClassId( );
                INT_4U instance = GetInstance( );

                Stream.calcChecksumFrame( &length, sizeof( length ) );
                Stream.calcChecksumFrame( &class_id, sizeof( class_id ) );
                Stream.calcChecksumFrame( &instance, sizeof( instance ) );
            }
#endif /* WORKING */

            m_data( Stream );
            m_refs( Stream );

            object_type eof( new FrEndOfFrame( GetRun( ), GetFrame( ) ) );
            Stream.PushSingle( eof );
        }

        void
        FrameH::VerifyObject( Common::Verify&       Verifier,
                              Common::IFrameStream& Stream ) const
        {
            if ( Verifier.ValidateMetadata( ) &&
                 ( GetGTime( ).GetLeapSeconds( ) != GetULeapS( ) ) )
            {
                std::ostringstream msg;

                msg << "Bad ULeapS value ( for gpstime: " << GetGTime( )
                    << " expected: " << GetGTime( ).GetLeapSeconds( )
                    << " read: " << GetULeapS( ) << " )";
                throw Common::VerifyException(
                    Common::VerifyException::METADATA_INVALID, msg.str( ) );
            }
        }

        bool
        FrameH::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return compare( *this, Obj );
        }

        FrameH::demote_ret_type
        FrameH::demote( INT_2U              Target,
                        demote_arg_type     Obj,
                        demote_stream_type* Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrameH > retval(
                    new Previous::FrameH( GetName( ),
                                          GetRun( ),
                                          GetFrame( ),
                                          GetGTime( ),
                                          GetULeapS( ),
                                          0, // LocalTime
                                          GetDt( ),
                                          GetDataQuality( ) ) );

                auto istream = FrameCPP::Common::IsIStream( Stream );

                if ( istream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    istream->ReplaceRef(
                        retval->RefType( ), RefType( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefUser( ), RefUser( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefDetectSim( ), RefDetectSim( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefDetectProc( ), RefDetectProc( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefHistory( ), RefHistory( ), MAX_REF );
                    istream->ReplacePtr( retval->AddressOfRawData( ),
                                         AddressOfRawData( ),
                                         MAX_REF );
                    istream->ReplaceRef(
                        retval->RefProcData( ), RefProcData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefSimData( ), RefSimData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefTrigData( ), RefEvent( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefSimEvent( ), RefSimEvent( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefSummaryData( ), RefSummaryData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefAuxData( ), RefAuxData( ), MAX_REF );
                    istream->ReplaceRef(
                        retval->RefAuxTable( ), RefAuxTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented( "Object* FrameH::demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrameH::promote_ret_type
        FrameH::promote( INT_2U               Target,
                         promote_arg_type     Obj,
                         promote_stream_type* Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

        void
        FrameH::readSubset( istream_type& Stream, INT_4U ElementMask )
        {
            m_refs.readSubset( Stream, ElementMask );
        }

        void
        FrameH::assign( assign_stream_type& Stream )
        {
            FrameHNPS::assign( Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
