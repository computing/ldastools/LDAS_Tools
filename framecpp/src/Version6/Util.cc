//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <vector>
#include <cassert>

#include <sstream>

#include "ldastoolsal/AtExit.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Singleton.hh"

#include "framecpp/Version3_4_5/history.hh"

#include "framecpp/Version6/FrameH.hh"
#include "framecpp/Version6/FrAdcData.hh"
#include "framecpp/Version6/FrCommon.hh"
#include "framecpp/Version6/FrDetector.hh"
#include "framecpp/Version6/FrEndOfFile.hh"
#include "framecpp/Version6/FrEndOfFrame.hh"
#include "framecpp/Version6/FrEvent.hh"
#include "framecpp/Version6/FrHistory.hh"
#include "framecpp/Version6/FrMsg.hh"
#include "framecpp/Version6/FrProcData.hh"
#include "framecpp/Version6/FrRawData.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrSerData.hh"
#include "framecpp/Version6/FrSimData.hh"
#include "framecpp/Version6/FrSimEvent.hh"
#include "framecpp/Version6/FrStatData.hh"
#include "framecpp/Version6/FrSummary.hh"
#include "framecpp/Version6/FrTOC.hh"
#include "framecpp/Version6/FrVect.hh"
#include "framecpp/Version6/IFrameStream.hh"
#include "framecpp/Version6/OFrameStream.hh"
#include "framecpp/Version6/PTR_STRUCT.hh"
#include "framecpp/Version6/Util.hh"

using namespace FrameCPP::Version_6;
using LDASTools::AL::AtExit;

namespace
{
    typedef std::vector< int > dummy_type;

    //---------------------------------------------------------------------
    // Singleton of read and write routines for frame i/o.
    //---------------------------------------------------------------------
    class IORoutines
    {
        SINGLETON_TS_DECL( IORoutines );

    public:
        //: Return the list of io calls
        const std::vector< Dictionary::io_calls_type >& GetIO( ) const;

    private:
        //: Destructor
        ~IORoutines( );

        //: Storage for the io calls
        std::vector< Dictionary::io_calls_type > m_io;
    };

    const std::vector< Dictionary::io_calls_type >&
    IORoutines::GetIO( ) const
    {
        return m_io;
    }

} // namespace

namespace FrameCPP
{
    namespace Version_6
    {
        const char* LibraryString = "Version6"; // :TODO: Move to FrCommon.cc

        const std::vector< Dictionary::io_calls_type >&
        GetClassIO( )
        {
            return IORoutines::Instance( ).GetIO( );
        }

        template < class LM_Container >
        void
        ReadContainer( IFrameStream& Stream,
                       LM_Container& Container,
                       bool          Recursive )
        {
            PTR_STRUCT p;
            Stream >> p;
            Container.erase( Container.begin( ), Container.end( ) );
        }

        template <>
        void
        ReadContainer( IFrameStream& Stream,
                       dummy_type&   Container,
                       bool          Recursive )
        {
            PTR_STRUCT p;
            Stream >> p;
            Container.erase( Container.begin( ), Container.end( ) );
        }

        template < class LM_Container >
        void
        WriteContainer( OFrameStream& Stream, const LM_Container& Container )
        {
            if ( Container.size( ) > 0 )
            {
                assert( 0 );
            }
            Stream << NULL_PTR_STRUCT;
        }

        template void WriteContainer( OFrameStream&,
                                      const std::vector< int >& );

        void
        Rethrow( const std::exception& Exception, const std::string& Prefix )
        {
            std::ostringstream oss;

            oss << Prefix;
            if ( Prefix.length( ) > 0 )
            {
                oss << " ";
            }
            oss << Exception.what( );
            if ( dynamic_cast< const std::runtime_error* >( &Exception ) )
            {
                throw std::runtime_error( oss.str( ) );
            }
            else
            {
                throw std::runtime_error( oss.str( ) );
            }
        }

    } // namespace Version_6
} // namespace FrameCPP

namespace
{

    //---------------------------------------------------------------------
    // Functions that are needed to make this a proper singleton
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( IORoutines );

    //---------------------------------------------------------------------
    // Default constructor
    //---------------------------------------------------------------------
    IORoutines::IORoutines( )
    {
        AtExit::Append(
            singleton_suicide,
            "framecpp_version6::Util.cc::IORoutines::singleton_suicide",
            0 );

#define ADD( LM_CLASS, LM_ID )                                                 \
    m_io.push_back( Dictionary::io_calls_type(                                 \
        LM_CLASS::getStaticName( ),                                            \
        CLASS_##LM_ID,                                                         \
        (Dictionary::read_func_type)&LM_CLASS::ReadDynamic,                    \
        (Dictionary::write_func_type)NULL ) )
#define ADD_BASE( LM_CLASS, LM_ID )                                            \
    m_io.push_back( Dictionary::io_calls_type(                                 \
        LM_CLASS::getStaticName( ),                                            \
        FrBase::CLASS_##LM_ID,                                                 \
        (Dictionary::read_func_type)&LM_CLASS::ReadDynamic,                    \
        (Dictionary::write_func_type)NULL ) )

        ADD_BASE( FrSE, FR_SE );
        ADD_BASE( FrSH, FR_SH );
        ADD( FrameH, FRAME_H );
        ADD( FrAdcData, FR_ADC_DATA );
        ADD( FrDetector, FR_DETECTOR );
        ADD( FrEndOfFile, FR_END_OF_FILE );
        ADD( FrEndOfFrame, FR_END_OF_FRAME );
        ADD( FrEvent, FR_EVENT );
        ADD( FrHistory, FR_HISTORY );
        ADD( FrMsg, FR_MSG );
        ADD( FrProcData, FR_PROC_DATA );
        ADD( FrRawData, FR_RAW_DATA );
        ADD( FrSerData, FR_SER_DATA );
        ADD( FrSimData, FR_SIM_DATA );
        ADD( FrSimEvent, FR_SIM_EVENT );
        ADD( FrStatData, FR_STAT_DATA );
        ADD( FrSummary, FR_SUMMARY );
        ADD( FrTable, FR_TABLE );
        ADD( FrTOC, FR_TOC );
        ADD( FrVect, FR_VECT );

#undef ADD_BASE
#undef ADD
    } // IORoutines

    IORoutines::~IORoutines( )
    {
        m_io.erase( m_io.begin( ), m_io.end( ) );
        m_io.reserve( 0 );
    }

} // namespace
