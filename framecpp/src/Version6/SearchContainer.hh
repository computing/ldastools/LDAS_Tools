//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_6_SEARCH_CONTAINER_HH
#define FrameCPP_VERSION_6_SEARCH_CONTAINER_HH

#include <string>

#include "framecpp/SearchContainer.hh"
#include "framecpp/Version6/Container.hh"

namespace FrameCPP
{
    namespace Version_6
    {
        template < class T, const std::string& ( T::*F )( ) const >
        class SearchContainer
            : public FrameCPP::SearchContainer< T, F, Container< T > >
        {
        public:
            typedef typename Container< T >::const_iterator const_iterator;
            typedef typename Container< T >::iterator       iterator;

            SearchContainer( bool AllowDuplicates = true );

            SearchContainer( const SearchContainer< T, F >& SC );

            ~SearchContainer( );

            SearchContainer< T, F >&
            Merge( const SearchContainer< T, F >& LHS );
        };

        template < class T, const std::string& ( T::*F )( ) const >
        inline SearchContainer< T, F >::SearchContainer( bool AllowDuplicates )
            : FrameCPP::SearchContainer< T, F, Container< T > >(
                  AllowDuplicates )
        {
        }

        template < class T, const std::string& ( T::*F )( ) const >
        inline SearchContainer< T, F >::SearchContainer(
            const SearchContainer< T, F >& SC )
            : FrameCPP::SearchContainer< T, F, Container< T > >( SC )
        {
        }

        template < class T, const std::string& ( T::*F )( ) const >
        inline SearchContainer< T, F >::~SearchContainer( )
        {
        }

        template < class T, const std::string& ( T::*F )( ) const >
        SearchContainer< T, F >&
        SearchContainer< T, F >::Merge( const SearchContainer< T, F >& RHS )
        {
            //-----------------------------------------------------------------
            // Merge data between the two
            //-----------------------------------------------------------------
            const const_iterator slast( RHS.end( ) );
            const_iterator       sfind;

            for ( iterator cur = this->begin( ), last = this->end( );
                  cur != last;
                  ++cur )
            {
                sfind = RHS.find( ( ( *cur )->*F )( ) );
                if ( sfind != slast )
                {
                    ( *cur )->Merge( *( *sfind ) );
                }
            }
            //-----------------------------------------------------------------
            // Add to the Primary everything that only exists in the Secondary
            //-----------------------------------------------------------------
            for ( const_iterator cur = RHS.begin( ), last = RHS.end( );
                  cur != last;
                  ++cur )
            {
                if ( find( ( ( *cur )->*F )( ) ) == this->end( ) )
                {
                    append( *cur );
                }
            }
            return *this;
        } // Merge
    } // namespace Version_6
} // namespace FrameCPP

#endif /* FrameCPP_VERSION_6_SEARCH_CONTAINER_HH */
