//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrTOC.hh"

#include "framecpp/Version6/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#include "../Common/FrTOCPrivate.hh"

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        //===================================================================
        FrTOCAdcData::FrTOCAdcData( )
        {
        }

        FrTOCAdcData::FrTOCAdcData( Common::IStream& Stream, INT_4U FrameCount )
        {
            nadc_type nadc;
            Stream >> nadc;
            if ( nadc && ( nadc != FrTOC::NO_DATA_AVAILABLE ) )
            {
                typedef FrameCPP::Common::FrTOCAdcDataInputFunctor<
                    FrTOCAdcData::MapADC_type,
                    FrTOCAdcData::name_type,
                    FrTOCAdcData::channel_id_type,
                    FrTOCAdcData::group_id_type,
                    FrTOCAdcData::position_type >
                    functor;

                //---------------------------------------------------------------
                // Read in the information
                //---------------------------------------------------------------
                std::vector< name_type >::size_type s( nadc );
                std::vector< channel_id_type >      channel_ids( s );
                std::vector< group_id_type >        group_ids( s );

                m_keys.resize( s );
                if ( positions_cache.size( ) != ( s * FrameCount ) )
                {
                    positions_cache.resize( s * FrameCount );
                }

                for ( auto& key : m_keys )
                {
                    STRING::Read( Stream, key );
                }
                Stream >> channel_ids >> group_ids;
                //---------------------------------------------------------------
                // Capture the starting position relative to the end of the
                // file.
                //---------------------------------------------------------------
                positions_cache_offset = Stream.Size( ) - Stream.tellg( );
                Stream >> positions_cache;
                //---------------------------------------------------------------
                // Move into structure.
                //---------------------------------------------------------------
                functor f( m_info,
                           channel_ids.begin( ),
                           group_ids.begin( ),
                           positions_cache.begin( ),
                           FrameCount );

                std::for_each( m_keys.begin( ), m_keys.end( ), f );
            }
        }

        FrTOCAdcData::MapADC_type::const_iterator
        FrTOCAdcData::GetADC( const std::string& Channel ) const
        {
            return GetADC( ).find( Channel );
        }

        FrTOCAdcData::MapADC_type::const_iterator
        FrTOCAdcData::GetADC( INT_4U Channel ) const
        {
            if ( Channel >= GetADC( ).size( ) )
            {
                return GetADC( ).end( );
            }
            return GetADC( ).find( m_keys[ Channel ] );
        }

        void
        FrTOCAdcData::QueryAdc( const Common::TOCInfo& Info,
                                INT_4U                 FrameOffset,
                                INT_8U                 Position )
        {
            STRING name;
            INT_4U channel_id;
            INT_4U group_id;

            Info.TOCQuery( Common::TOCInfo::IC_NAME,
                           Common::TOCInfo::DT_STRING_2,
                           &name,
                           Common::TOCInfo::IC_CHANNEL_ID,
                           Common::TOCInfo::DT_INT_4U,
                           &channel_id,
                           Common::TOCInfo::IC_GROUP_ID,
                           Common::TOCInfo::DT_INT_4U,
                           &group_id,
                           Common::TOCInfo::IC_EOQ );

            adc_info_type& i( m_info[ name ] );
            i.m_channelID = channel_id;
            i.m_groupID = group_id;
            i.m_positionADC.resize( FrameOffset + 1 );
            i.m_positionADC[ FrameOffset ] = Position;
        }

        //-------------------------------------------------------------------
        /// This method allows for iterting over each element of information
        /// and allows the caller to gather information about each element.
        //-------------------------------------------------------------------
        void
        FrTOCAdcData::forEach( Common::FrTOC::query_info_type Info,
                               Common::FrTOC::FunctionBase&   Action ) const
        {
            switch ( Info )
            {
            case Common::FrTOC::TOC_CHANNEL_NAMES:
            {
                try
                {
                    Common::FrTOC::FunctionString& action(
                        dynamic_cast< Common::FrTOC::FunctionString& >(
                            Action ) );

                    for ( MapADC_type::const_iterator cur = m_info.begin( ),
                                                      last = m_info.end( );
                          cur != last;
                          ++cur )
                    {
                        action( cur->first );
                    }
                }
                catch ( ... )
                {
                    // Does not understand Action
                }
            }
            break;
            default:
                //---------------------------------------------------------------
                // ignore all other requests
                //---------------------------------------------------------------
                break;
            }
        }

        void
        FrTOCAdcData::write( Common::OStream& Stream ) const
        {
            //-----------------------------------------------------------------
            // Flatten data so it is streamable
            //-----------------------------------------------------------------
            std::vector< name_type >::size_type s( m_info.size( ) );
            if ( s )
            {
                std::vector< name_type >::size_type fc(
                    m_info.begin( )->second.m_positionADC.size( ) );

                std::vector< name_type >       names( s );
                std::vector< channel_id_type > channel_ids( s );
                std::vector< group_id_type >   group_ids( s );
                std::vector< position_type >   positions( s * fc );
                //---------------------------------------------------------------
                // Copy data for streaming
                //---------------------------------------------------------------
                std::vector< name_type >::iterator cur_name = names.begin( );
                std::vector< channel_id_type >::iterator cur_channel_id =
                    channel_ids.begin( );
                std::vector< group_id_type >::iterator cur_group_id =
                    group_ids.begin( );
                std::vector< position_type >::iterator cur_position =
                    positions.begin( );

                for ( MapADC_type::const_iterator cur = m_info.begin( ),
                                                  last = m_info.end( );
                      cur != last;
                      ++cur,
                                                  ++cur_name,
                                                  ++cur_channel_id,
                                                  ++cur_group_id,
                                                  cur_position += fc )
                {
                    *cur_name = cur->first;
                    *cur_channel_id = cur->second.m_channelID;
                    *cur_group_id = cur->second.m_groupID;
                    std::copy( cur->second.m_positionADC.begin( ),
                               cur->second.m_positionADC.end( ),
                               cur_position );
                }
                //---------------------------------------------------------------
                // Stream out
                //---------------------------------------------------------------
                Stream << nadc_type( s );
                for ( auto& name : names )
                {
                    STRING::Write( Stream, name );
                }
                Stream << channel_ids << group_ids << positions;
            }
            else
            {
                Stream << nadc_type( s );
                // Stream << nadc_type( FrTOC::NO_DATA_AVAILABLE );
            }
        }

    } // namespace Version_6
} // namespace FrameCPP
