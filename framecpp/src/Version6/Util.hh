//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_6_Util_HH
#define FrameCPP_VERSION_6_Util_HH

#include <vector>

#include "framecpp/ContainerBasic.hh"

#include "framecpp/Version6/Dictionary.hh"

namespace FrameCPP
{
    namespace Version_6
    {
        class OFrameStream;

        extern const char* LibraryString;

        void Rethrow( const std::exception& Exception,
                      const std::string&    Prefix );

        const std::vector< Dictionary::io_calls_type >& GetClassIO( );
        template < class LM_Container >
        void ReadContainer( IFrameStream& Stream,
                            LM_Container& Container,
                            bool          Recursive = true );
        template < class LM_Container >
        void WriteContainer( OFrameStream&       Stream,
                             const LM_Container& Container );

    } // namespace Version_6
} // namespace FrameCPP

#endif /* FrameCPP_VERSION_6_Util_HH */
