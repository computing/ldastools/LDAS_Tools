//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version7/FrameSpec.hh"

#include "framecpp/Version7/PTR_STRUCT.hh"
#include "framecpp/Version7/StreamRef.hh"

#include "framecpp/Version7/FrAdcData.hh"
#include "framecpp/Version7/FrameH.hh"
#include "framecpp/Version7/FrDetector.hh"
#include "framecpp/Version7/FrEndOfFile.hh"
#include "framecpp/Version7/FrEndOfFrame.hh"
#include "framecpp/Version7/FrEvent.hh"
#include "framecpp/Version7/FrHeader.hh"
#include "framecpp/Version7/FrHistory.hh"
#include "framecpp/Version7/FrMsg.hh"
#include "framecpp/Version7/FrProcData.hh"
#include "framecpp/Version7/FrRawData.hh"
#include "framecpp/Version7/FrSE.hh"
#include "framecpp/Version7/FrSerData.hh"
#include "framecpp/Version7/FrSH.hh"
#include "framecpp/Version7/FrSimData.hh"
#include "framecpp/Version7/FrSimEvent.hh"
#include "framecpp/Version7/FrStatData.hh"
#include "framecpp/Version7/FrSummary.hh"
#include "framecpp/Version7/FrTable.hh"
#include "framecpp/Version7/FrTOC.hh"
#include "framecpp/Version7/FrVect.hh"

namespace FrameCPP
{
    namespace Version_7
    {
        static const bool Initialized = init_frame_spec( );

        bool
        init_frame_spec( )
        {
            using Common::FrameSpec;

            static bool initialized = false;

            (void)Initialized; // Suppress unused warning message

            if ( !initialized )
            {
                //---------------------------------------------------------------
                // Local storage describing implementation of the frame spec
                //---------------------------------------------------------------
                std::unique_ptr< Common::FrameSpec::Info > info(
                    new Common::FrameSpec::Info( DATA_FORMAT_VERSION,
                                                 LIBRARY_MINOR_VERSION ) );
                info->FrameObject( new PTR_STRUCT );
                info->FrameObject( new StreamRef );

                info->FrameObject( new FrAdcData );
                info->FrameObject( new FrameH );
                info->FrameObject( new FrDetector );
                info->FrameObject( new FrEndOfFile );
                info->FrameObject( new FrEndOfFrame );
                info->FrameObject( new FrEvent );
                info->FrameObject( new FrHeader );
                info->FrameObject( new FrHistory );
                info->FrameObject( new FrMsg );
                info->FrameObject( new FrProcData );
                info->FrameObject( new FrRawData );
                info->FrameObject( new FrSE );
                info->FrameObject( new FrSerData );
                info->FrameObject( new FrSH );
                info->FrameObject( new FrSimData );
                info->FrameObject( new FrSimEvent );
                info->FrameObject( new FrStatData );
                info->FrameObject( new FrSummary );
                info->FrameObject( new FrTable );
                info->FrameObject( new FrTOC );
                info->FrameObject( new FrVect );

                //---------------------------------------------------------------
                // Register with the stream manipulator
                //---------------------------------------------------------------
                FrameSpec::SpecInfo(
                    FrameSpec::version_type( DATA_FORMAT_VERSION ),
                    info.release( ) );
                initialized = true;
            }
            return true;
        }
    } // namespace Version_7
} // namespace FrameCPP
