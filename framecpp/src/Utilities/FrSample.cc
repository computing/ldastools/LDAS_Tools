//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#if HAVE_CONFIG_H
#include <framecpp_config.h>
#endif /* HAVE_CONFIG_H */

#include <algorithm>
#include <iostream>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

// Define BOOST_TEST_NO_MAIN since FrSample.hh includes unit_test.hpp
#define BOOST_TEST_NO_MAIN

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/serialization/strong_typedef.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/types.hh"

#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/CheckSum.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameBuffer.hh"
#include "framecpp/Common/FrameStream.hh"

#include "framecpp/FrameCPP.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrDetector.hh"
#include "framecpp/FrEvent.hh"
#include "framecpp/FrHistory.hh"
#include "framecpp/FrMsg.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrSerData.hh"
#include "framecpp/FrSimData.hh"
#include "framecpp/FrSimEvent.hh"
#include "framecpp/FrSummary.hh"
#include "framecpp/FrTable.hh"
#include "framecpp/FrVect.hh"

#include "framecpp/Dimension.hh"

#include "FrSample.hh"
#include "FrSample3.tcc"
#include "FrSample4.tcc"
#include "FrSample6.tcc"
#include "FrSample7.tcc"
#include "FrSample8.tcc"
#include "FrSample9.tcc"

namespace po = boost::program_options;

using LDASTools::AL::filebuf;
using LDASTools::AL::MemChecker;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;
using FrameCPP::Common::CheckSum;
using FrameCPP::Common::FrameBuffer;
using FrameCPP::Common::OFrameStream;
using FrameCPP::Common::OStream;

using LDASTools::AL::GPSTime;
using std::ostringstream;
using std::string;

using FrameCPP::Dimension;

using FrameCPP::FrAdcData;
using FrameCPP::FrameH;
using FrameCPP::FrProcData;
using FrameCPP::FrRawData;
using FrameCPP::FrSerData;
using FrameCPP::FrSimData;
using FrameCPP::FrVect;

class CommandLine;

// The name of the program executable
typedef std::set< std::string > channel_set_type;

typedef boost::shared_ptr< FrameH >     frame_h_type;
typedef boost::shared_ptr< FrAdcData >  fr_adc_data_type;
typedef boost::shared_ptr< FrRawData >  fr_raw_data_type;
typedef boost::shared_ptr< FrProcData > fr_proc_data_type;
typedef boost::shared_ptr< FrSerData >  fr_ser_data_type;
typedef boost::shared_ptr< FrSimData >  fr_sim_data_type;
typedef boost::shared_ptr< FrVect >     fr_vect_type;
typedef std::string                     detector_type;

BOOST_STRONG_TYPEDEF( INT_4U, version_type );

namespace std
{
    std::istream&
    operator>>( std::istream& InputStream, LDASTools::AL::GPSTime& StartTime )
    {
        INT_4U sec;

        InputStream >> sec;
        StartTime = GPSTime( sec, 0 );
        return InputStream;
    }

    std::istream&
    operator>>( std::istream& InputStream, channel_set_type& ChannelSet )
    {
        channel_set_type::value_type channel_name;

        InputStream >> channel_name;

        boost::to_lower( channel_name );
        ChannelSet.insert( channel_name );

        return InputStream;
    }
} // namespace std

//=======================================================================
// Forward declaration of local functions
//=======================================================================

//-----------------------------------------------------------------------
static void all_type_frame( const string&           Type,
                            const channel_set_type& ChannelSet,
                            const GPSTime&          Start,
                            const INT_4U            DeltaT,
                            const INT_4U            Iterations,
                            const version_type      Version,
                            const CommandLine&      CommandLineArgs );

//-----------------------------------------------------------------------

template < class ChannelType >
static void all_type_iteration( const INT_4U            DeltaT,
                                const channel_set_type& ListOfChannels,
                                frame_h_type            Frame,
                                INT_4U*                 Channel,
                                INT_4U                  Iteration,
                                const CommandLine&      CommandLineArgs );

//-----------------------------------------------------------------------

template < class T >
static std::string
data_type( )
{
    return "";
}

//-----------------------------------------------------------------------

template < class T, class ChannelType >
static boost::shared_ptr< ChannelType >
ramped_channel( INT_4U             DeltaT,
                INT_4U*            Channel,
                INT_4U             Iteration,
                T                  Start,
                T                  Stop,
                T                  Inc,
                INT_4U             SampleRate,
                const CommandLine& CommandLineArgs,
                const char*        IFO = "Z0" );

//-----------------------------------------------------------------------

template < class T >
static boost::shared_array< T >
ramp_data_create( INT_4U Samples, T Start, T Stop, T Inc );

//-----------------------------------------------------------------------

template < class T >
static string ramped_name( INT_4U Iteration, const char* IFO );

//-----------------------------------------------------------------------

template < class T >
static fr_vect_type ramped_vector( INT_4U      DeltaT,
                                   INT_4U      SampleRate,
                                   INT_4U      Iteration,
                                   T           Start,
                                   T           Stop,
                                   T           Inc,
                                   const char* IFO );

//-----------------------------------------------------------------------

void
test_frame( version_type Version )
{
    using FrameCPP::Common::FrameSpec;

    //-------------------------------------------------------------------
    // Create the frame
    //-------------------------------------------------------------------
    stat_data_container_type stat_data;
    make_frame_ret_type      frame;

    switch ( Version )
    {
    case 3:
        frame = makeFrame< 3 >( stat_data );
        break;
    case 4:
        frame = makeFrame< 4 >( stat_data );
        break;
    case 6:
        frame = makeFrame< 6 >( stat_data );
        break;
    case 7:
        frame = makeFrame< 7 >( stat_data );
        break;
    case 8:
        frame = makeFrame< 8 >( stat_data );
        break;
    case 9:
        frame = makeFrame< 9 >( stat_data );
        break;
    default:
    {
        std::ostringstream msg;

        msg << "name makeFrame case for version: " << Version;
        throw std::runtime_error( msg.str( ) );
    }
    break;
    }
    //-------------------------------------------------------------------
    // Calculate the filename
    //-------------------------------------------------------------------
    std::ostringstream filename;

    filename << "Z-R_std_test_frame_ver" << Version << "-" << 600000000 << "-"
             << 1 << ".gwf";

    if ( frame.get( ) == (FrameSpec::Object*)NULL )
    {
        throw std::runtime_error( "Failed to create frame" );
    }
    //-------------------------------------------------------------------
    // Creation of io buffer.
    //-------------------------------------------------------------------
    FrameBuffer< filebuf >* obuf( new FrameBuffer< filebuf >( std::ios::out ) );
    try
    {
        //-----------------------------------------------------------------
        // Open the buffer and the stream
        //-----------------------------------------------------------------
        obuf->open( filename.str( ).c_str( ),
                    std::ios::out | std::ios::binary );

        OFrameStream ofs( obuf, Version );

        ofs.SetCheckSumFile( CheckSum::CRC );

        //-----------------------------------------------------------------
        // Write the FrStatData data to the stream
        //-----------------------------------------------------------------
        for ( stat_data_container_type::iterator cur = stat_data.begin( ),
                                                 last = stat_data.end( );
              cur != last;
              ++cur )
        {
            ofs.WriteFrStatData( *cur );
        }
        //-----------------------------------------------------------------
        // Write the frame to the file
        //-----------------------------------------------------------------
        ofs.WriteFrame( frame, CheckSum::CRC );
        //-----------------------------------------------------------------
        // Close the stream and file
        //-----------------------------------------------------------------
        ofs.Close( );
        obuf->close( );
    }
    catch ( ... )
    {
        obuf->close( );
        // unlink( filename.str( ).c_str( ) );
        throw;
    }

    //-------------------------------------------------------------------
    // Release resources associated with the FrStatData
    //-------------------------------------------------------------------
    stat_data.clear( );
}

//-----------------------------------------------------------------------
// Common tasks to be performed when exiting.
//-----------------------------------------------------------------------
inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

//-----------------------------------------------------------------------
/// \brief Class to handle command line options for this application
//-----------------------------------------------------------------------
class CommandLine
{
public:
    typedef std::string history_msg_type;
    typedef bool        with_fr_event_type;
    typedef bool        with_fr_msg_type;
    typedef bool        with_fr_sim_event_type;
    typedef bool        with_fr_summary_type;
    typedef bool        with_fr_table_type;

    CommandLine( int ArgC, char** ArgV );

    inline const channel_set_type&
    ChannelsOfTypeProc( ) const
    {
        return channel_proc_set;
    }

    inline void
    ChannelsOfTypeProc( const std::string& Value )
    {
        channel_proc_set.insert( Value );
    }

    inline const channel_set_type&
    ChannelsOfTypeSerData( ) const
    {
        return channel_ser_data_set;
    }

    inline const channel_set_type&
    ChannelsOfTypeSimData( ) const
    {
        return channel_sim_data_set;
    }

    inline const channel_set_type&
    ChannelType( ) const
    {
        return m_channel_set;
    }

    inline void
    ChannelType( const std::string& Value )
    {
        std::string channel_name( Value );

        boost::to_lower( channel_name );
        m_channel_set.insert( channel_name );
    }

    inline const std::string&
    Description( ) const
    {
        return m_description;
    }

    inline void
    Description( const std::string& Value )
    {
        m_description = Value;
    }

    inline const detector_type&
    Detector( ) const
    {
        return detector;
    }

    inline INT_4U
    Duration( ) const
    {
        return m_duration;
    }

    inline void
    Duration( INT_4U Value )
    {
        m_duration = Value;
    }

    inline const history_msg_type&
    HistoryMsg( ) const
    {
        return ( history_msg );
    }

    inline INT_4U
    Iterations( ) const
    {
        return m_iterations;
    }

    inline void
    Iterations( INT_4U Value )
    {
        m_iterations = Value;
    }

    inline const std::string&
    ProgramName( ) const
    {
        return m_program_name;
    }

    inline const GPSTime&
    StartTime( ) const
    {
        return m_start_time;
    }

    inline void
    StartTime( const GPSTime& Value )
    {
        m_start_time = Value;
    }

    inline bool
    TestFrameMode( ) const
    {
        return m_test_frame_mode;
    }

    inline void
    TestFrameMode( bool Value )
    {
        m_test_frame_mode = Value;
    }

    inline version_type
    Version( ) const
    {
        return m_version;
    }

    inline void
    Version( version_type Value )
    {
        m_version = Value;
    }

    inline bool
    WithFrEvent( ) const
    {
        return ( with_fr_event );
    }

    inline bool
    WithFrMsg( ) const
    {
        return ( with_fr_msg );
    }

    inline bool
    WithFrSimEvent( ) const
    {
        return ( with_fr_sim_event );
    }

    inline bool
    WithFrSummary( ) const
    {
        return ( with_fr_summary );
    }

    inline bool
    WithFrTable( ) const
    {
        return ( with_fr_table );
    }

    inline void
    Usage( int ExitValue ) const
    {
        std::cerr << "Usage: " << ProgramName( ) << std::endl
                  << m_options << std::endl;
        depart( ExitValue );
    }

private:
    po::options_description m_options;
    channel_set_type        m_channel_set;
    channel_set_type        channel_proc_set;
    channel_set_type        channel_ser_data_set;
    channel_set_type        channel_sim_data_set;
    std::string             m_description;
    detector_type           detector;
    INT_4U                  m_duration;
    history_msg_type        history_msg;
    INT_4U                  m_iterations;
    std::string             m_program_name;
    GPSTime                 m_start_time;
    bool                    m_test_frame_mode;
    version_type            m_version;
    with_fr_event_type      with_fr_event = false;
    with_fr_msg_type        with_fr_msg = false;
    with_fr_sim_event_type  with_fr_sim_event = false;
    with_fr_summary_type    with_fr_summary = false;
    with_fr_table_type      with_fr_table = false;
};

CommandLine::CommandLine( int ArgC, char** ArgV )
    : m_description( "ilwd_test_frame" ), m_duration( 1 ), m_iterations( 1 ),
      m_start_time( GPSTime( 600000000, 0 ) ), m_test_frame_mode( false ),
      m_version( FRAME_SPEC_CURRENT )
{
    po::options_description desc( "Options" );

    m_program_name.assign( ArgV[ 0 ] );
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.add_options( ) //
        ( "help,h", "Print help messages" ) //
        ( "description",
          po::value< std::string >( &m_description )
              ->default_value( "ilwd_test_frame" ),
          "Specify the contents of the discription portion"
          " of the frame filename." ) //
        ( "channel-type",
          po::value< channel_set_type >( &m_channel_set ),
          "Add a channel type to the output frame."
          " This option may appear multiple times on the command line to"
          " specify multiple channel types." ) //
        ( "channel-proc-type",
          po::value< channel_set_type >( &channel_proc_set ),
          "Add a processed channel type to the output frame."
          " This option may appear multiple times on the command line to"
          " specify multiple processed channel types." ) //
        ( "channel-ser-data-type",
          po::value< channel_set_type >( &channel_ser_data_set ),
          "Add a serial data channel type to the output frame."
          " This option may appear multiple times on the command line to"
          " specify multiple processed channel types." ) //
        ( "channel-sim-data-type",
          po::value< channel_set_type >( &channel_sim_data_set ),
          "Add a simulated data channel type to the output frame."
          " This option may appear multiple times on the command line to"
          " specify multiple processed channel types." ) //
        ( "detector",
          po::value< std::string >( &detector )->default_value( "" ),
          "Add a detector structure to the frame." ) //
        ( "duration",
          po::value< INT_4U >( &m_duration )->default_value( 1 ),
          "The length of the frame in seconds." ) //
        ( "iterations",
          po::value< INT_4U >( &m_iterations )->default_value( 1 ),
          "Number of sets of channels to produce."
          " This can be used to inflate the size of a frame." ) //
        ( "history-msg",
          po::value< decltype( history_msg ) >( &history_msg )
              ->default_value( history_msg ),
          "Text of history record." ) //
        ( "start-time",
          po::value< GPSTime >( &m_start_time )
              ->default_value( GPSTime( 600000000, 0 ) ),
          "Number of sets of channels to produce."
          " This can be used to inflate the size of a frame." ) //
        ( "test-frame-mode",
          po::value< bool >( &m_test_frame_mode )
              ->default_value( false )
              ->implicit_value( true ),
          "Generate standard test frame."
          "Enable test frame mode" ) //
        ( "version",
          po::value< version_type >( &m_version )
              ->default_value( version_type( FRAME_SPEC_CURRENT ) ),
          "Verion of the frame specification to use for the output frame." ) //
        ( "with-fr-event",
          po::bool_switch( &with_fr_event ),
          "Add a moch FrEvent structure to output frame." ) //
        ( "with-fr-msg",
          po::bool_switch( &with_fr_msg ),
          "Add a moch FrMsg structure to output frame." ) //
        ( "with-fr-sim-event",
          po::bool_switch( &with_fr_sim_event ),
          "Add a moch FrSimEvent structure to output frame." ) //
        ( "with-fr-summary",
          po::bool_switch( &with_fr_summary ),
          "Add a moch FrSummary structure to output frame." ) //
        ( "with-fr-table",
          po::bool_switch( &with_fr_table ),
          "Add a moch FrTable structure to the FrameH::uxTable of output "
          "frame." ) //
        ;

    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------

    po::variables_map vm;
    po::store(
        po::command_line_parser( ArgC, ArgV ).options( m_options ).run( ), vm );
    po::notify( vm );

    if ( vm.count( "help" ) )
    {
        Usage( 0 );
    }
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
int
main( int ArgC, char* ArgV[] )
try
{
    MemChecker::Trigger gc_trigger( true );
    CommandLine         cl( ArgC, ArgV );

#if 0
    if ( ( cl.empty( ) == false ) || ( cl.BadOption( ) ) )
    {
        cl.Usage( 1 );
    }
#endif /* 0 */

    FrameCPP::Initialize( );

    if ( cl.TestFrameMode( ) )
    {
        test_frame( cl.Version( ) );
    }
    else
    {
        if ( cl.ChannelType( ).size( ) == 0 )
        {
            cl.ChannelType( "int_2u" );
            cl.ChannelType( "int_2s" );
            cl.ChannelType( "int_4u" );
            cl.ChannelType( "int_4s" );
            cl.ChannelType( "int_8u" );
            cl.ChannelType( "int_8s" );
            cl.ChannelType( "real_4" );
            cl.ChannelType( "real_8" );
            cl.ChannelType( "complex_8" );
            cl.ChannelType( "complex_16" );
        }
        all_type_frame( cl.Description( ),
                        cl.ChannelType( ),
                        cl.StartTime( ),
                        cl.Duration( ),
                        cl.Iterations( ),
                        cl.Version( ),
                        cl );
    }

    depart( 0 );
}
catch ( const std::exception& e )
{
    std::cerr << "FATAL: Caught exception: " << e.what( ) << std::endl;
    depart( 1 );
}
catch ( ... )
{
    std::cerr << "FATAL: Caught unknown exception: " << std::endl;
    depart( 1 );
}

//=======================================================================
// Local functions
//=======================================================================

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
static void
all_type_frame( const string&           Type,
                const channel_set_type& ChannelSet,
                const GPSTime&          Start,
                const INT_4U            DeltaT,
                const INT_4U            Iterations,
                const version_type      Version,
                const CommandLine&      CommandLineArgs )
{
    //-------------------------------------------------------------------
    // Create filename
    //-------------------------------------------------------------------
    std::ostringstream filename;

    filename << "Z-" << Type << "-" << Start.GetSeconds( ) << "-" << DeltaT
             << ".gwf";
    //-------------------------------------------------------------------
    // Open the buffer and the stream
    //-------------------------------------------------------------------
    FrameBuffer< filebuf >* obuf( new FrameBuffer< filebuf >( std::ios::out ) );
    obuf->open( filename.str( ).c_str( ), std::ios::out | std::ios::binary );

    // OFrameStream ofs( obuf, Version, FRAME_SPEC_CURRENT );
    OFrameStream ofs( obuf, Version );

    ofs.SetCheckSumFile( CheckSum::CRC );

    //-------------------------------------------------------------------
    // Generation of the frame
    //-------------------------------------------------------------------

    frame_h_type frame( new FrameH(
        Type, -1, 0, Start, Start.GetLeapSeconds( ), REAL_8( DeltaT ) ) );

    {
        FrameH::rawData_type rawData(
            new FrameH::rawData_type::element_type( "RawData" ) );
        frame->SetRawData( rawData );
    }

    //-------------------------------------------------------------------
    // Handle FrDetector
    //-------------------------------------------------------------------
    if ( CommandLineArgs.Detector( ).size( ) > 0 )
    {
        using FrameCPP::FrDetector;

        try
        {
            auto data_quality_offset =
                FrDetector::GetDataQualityOffset( CommandLineArgs.Detector( ) );

            if ( data_quality_offset != FrDetector::DQO_UNSET )
            {
                auto detector = FrDetector::GetDetector( data_quality_offset );

                frame->RefDetectProc( ).append( detector );
            }

            switch ( data_quality_offset )
            {
            case FrDetector::DQO_UNSET:
            default:
                break;
            }
        }
        catch ( ... )
        {
            // Ignore erros
        }
    }

    //-------------------------------------------------------------------
    // Loop over the number of iterations for creation of the data sets
    //-------------------------------------------------------------------

    for ( INT_4U i = 1; i <= Iterations; ++i )
    {
        INT_4U channel = 0;
        all_type_iteration< FrAdcData >(
            DeltaT, ChannelSet, frame, &channel, i, CommandLineArgs );
    }

    if ( CommandLineArgs.ChannelsOfTypeProc( ).size( ) > 0 )
    {
        for ( auto iteration = INT_4U( 1 ); iteration <= Iterations;
              ++iteration )
        {
            INT_4U channel = 0;
            all_type_iteration< FrProcData >(
                DeltaT,
                CommandLineArgs.ChannelsOfTypeProc( ),
                frame,
                &channel,
                iteration,
                CommandLineArgs );
        }
    }

    if ( CommandLineArgs.ChannelsOfTypeSerData( ).size( ) > 0 )
    {
        for ( auto iteration = INT_4U( 1 ); iteration <= Iterations;
              ++iteration )
        {
            INT_4U channel = 0;
            all_type_iteration< FrSerData >(
                DeltaT,
                CommandLineArgs.ChannelsOfTypeSerData( ),
                frame,
                &channel,
                iteration,
                CommandLineArgs );
        }
    }

    if ( CommandLineArgs.ChannelsOfTypeSimData( ).size( ) > 0 )
    {
        for ( auto iteration = INT_4U( 1 ); iteration <= Iterations;
              ++iteration )
        {
            INT_4U channel = 0;
            all_type_iteration< FrSimData >(
                DeltaT,
                CommandLineArgs.ChannelsOfTypeSimData( ),
                frame,
                &channel,
                iteration,
                CommandLineArgs );
        }
    }

    if ( CommandLineArgs.HistoryMsg( ).size( ) > 0 )
    {
        // Add a history record to the frame
        using FrameCPP::FrHistory;

        frame->RefHistory( ).append(
            FrHistory( "ALL_TYPE_FRAME_HISTORY_RECORD",
                       CommandLineArgs.StartTime( ).GetSeconds( ),
                       CommandLineArgs.HistoryMsg( ) ) );
    }

    if ( CommandLineArgs.WithFrEvent( ) )
    {
        using FrameCPP::FrEvent;

        FrEvent::ParamList_type param_list;
        param_list.push_back( FrEvent::Param_type( "MOCH_PARAM_1", 42.0 ) );

        frame->RefEvent( ).append(
            FrEvent( "MOCK_FR_EVENT", // name
                     "This is a moch event for testing the frame "
                     "specification", // comment
                     "Z1:NON_CHANNEL", // inputs
                     CommandLineArgs.StartTime( ), // GTime
                     5.0, // timeeBefore,
                     10.0, // timeAfter
                     12, // eventStatus
                     1.0, // amplitude
                     -1.0, // probability
                     "", // statistics
                     param_list // param
                     ) );
    }

    if ( CommandLineArgs.WithFrMsg( ) )
    {
        using FrameCPP::FrMsg;

        frame->GetRawData( )->RefLogMsg( ).append(
            FrMsg( "MOCK_FR_MSG", // alarm
                   "This is a moch event for testing the frame "
                   "specification", // message
                   5.0, // severity
                   CommandLineArgs.StartTime( ) // GTime
                   ) );
    }

    if ( CommandLineArgs.WithFrSimEvent( ) )
    {
        using FrameCPP::FrSimEvent;

        FrSimEvent::ParamList_type param_list;
        param_list.push_back(
            FrSimEvent::Param_type( "MOCH_SIM_PARAM_1", 32.0 ) );

        frame->RefSimEvent( ).append(
            FrSimEvent( "MOCK_FR_SIM_EVENT", // name
                        "This is a moch simulated event for testing the frame "
                        "specification being simulated", // comment
                        "Z1:NON_SIM_CHANNEL", // inputs
                        CommandLineArgs.StartTime( ), // GTime
                        10.0, // timeeBefore,
                        5.0, // timeAfter
                        10.0, // amplitude
                        param_list // param
                        ) );
    }

    if ( CommandLineArgs.WithFrSummary( ) )
    {
        using FrameCPP::FrSummary;

        frame->RefSummaryData( ).append(
            FrSummary( "MOCK_FR_SUMMARY_DATA", // name
                       "This is a moch simulated summary for testing the frame "
                       "specification being simulated", // comment
                       "MOCH_STATISTICAL_TEST", // test
                       CommandLineArgs.StartTime( ) // GTime
                       ) );
    }

    if ( CommandLineArgs.WithFrTable( ) )
    {
        using FrameCPP::FrTable;

        frame->RefAuxTable( ).append( FrTable( "MOCK_FR_TABLE_DATA", // name
                                               3 // Nrows
                                               ) );
        auto& table = *( frame->RefAuxTable( ).end( ) - 1 );
        table->AppendComment( "This is a moch table for testing the frame "
                              "specification being simulated" );

        {
            REAL_4 data[ 8 ] = { 1.0, 2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7 };
            FrameCPP::Dimension dims[ 1 ] = { FrameCPP::Dimension( 8, 1.0 ) };
            FrameCPP::FrVect table_data( "OpticWeight", 1, dims, data, "kg" );
            table->RefColumn( ).append( table_data );
        }
        {
            INT_4U data[ 8 ] = {
                1988, 1989, 1993, 1997, 1998, 2004, 2007, 2019
            };
            FrameCPP::Dimension dims[ 1 ] = { FrameCPP::Dimension( 8, 1.0 ) };
            FrameCPP::FrVect    table_data(
                "YearOfManufactored", 1, dims, data, "year" );
            table->RefColumn( ).append( table_data );
        }
    }

    //-------------------------------------------------------------------
    // Writing of the frame
    //-------------------------------------------------------------------

    ofs.WriteFrame( frame );

    //-------------------------------------------------------------------
    // Close the frame and file stream
    //-------------------------------------------------------------------
    ofs.Close( );
    obuf->close( );
}

template < class ChannelType >
auto
get_channel_ifo( )
{
    if
        constexpr( std::is_same_v< ChannelType, FrAdcData > )
        {
            return "Z0";
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrProcData > )
        {
            return "Z1";
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrSimData > )
        {
            return "Z2";
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrSerData > )
        {
            return "Z3";
        }
    throw( std::runtime_error( "get_channel_ifo: Unsupported ChannelType" ) );
}

template < class ChannelType >
auto&
get_channel_reference( frame_h_type Frame )
{
    if
        constexpr( std::is_same_v< ChannelType, FrAdcData > )
        {
            fr_raw_data_type rd( Frame->GetRawData( ) );

            if ( !rd )
            {
                throw std::range_error( "No FrRawData" );
            }
            return rd->RefFirstAdc( );
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrProcData > )
        {
            return Frame->RefProcData( );
        }
    if
        constexpr( std::is_same_v< ChannelType, FrSerData > )
        {
            fr_raw_data_type rd( Frame->GetRawData( ) );

            if ( !rd )
            {
                throw std::range_error( "No FrRawData" );
            }
            return rd->RefFirstSer( );
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrSimData > )
        {
            return Frame->RefSimData( );
        }
    else
    {
        static_assert( true, "get_channel_reference: Unsupported ChannelType" );
    }
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class ChannelType >
static void
all_type_iteration( const INT_4U            DeltaT,
                    const channel_set_type& ListOfChannels,
                    frame_h_type            Frame,
                    INT_4U*                 Channel,
                    INT_4U                  Iteration,
                    const CommandLine&      CommandLineArgs )
try
{
    //-------------------------------------------------------------------
    // Obtain the channel list to be appended
    //-------------------------------------------------------------------
    auto& frame_channels = get_channel_reference< ChannelType >( Frame );
    auto  ifo = get_channel_ifo< ChannelType >( );
    //-------------------------------------------------------------------
    // Add the ramped channels
    //-------------------------------------------------------------------
    channel_set_type::const_iterator last_channel( ListOfChannels.end( ) );

    if ( ListOfChannels.find( "int_2u" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< INT_2U, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   0,
                                                   512,
                                                   1,
                                                   512,
                                                   CommandLineArgs,
                                                   ifo ) );
    }
    if ( ListOfChannels.find( "int_2s" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< INT_2S, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   -512,
                                                   512,
                                                   1,
                                                   512,
                                                   CommandLineArgs,
                                                   ifo ) );
    }
    if ( ListOfChannels.find( "int_4u" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< INT_4U, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   0,
                                                   1024,
                                                   1,
                                                   1024,
                                                   CommandLineArgs,
                                                   ifo ) );
    }
    if ( ListOfChannels.find( "int_4s" ) != last_channel )
    {

        frame_channels.append(
            ramped_channel< INT_4S, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   -1024,
                                                   1024,
                                                   1,
                                                   1024,
                                                   CommandLineArgs,
                                                   ifo ) );
    }
    if ( ListOfChannels.find( "int_8u" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< INT_8U, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   0,
                                                   2048,
                                                   1,
                                                   1024,
                                                   CommandLineArgs,
                                                   ifo ) );
    }
    if ( ListOfChannels.find( "int_8s" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< INT_8S, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   -2048,
                                                   2048,
                                                   1,
                                                   1024,
                                                   CommandLineArgs,
                                                   ifo ) );
    }

    if ( ListOfChannels.find( "real_4" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< REAL_4, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   -1024,
                                                   1024,
                                                   0.5,
                                                   1024,
                                                   CommandLineArgs,
                                                   ifo ) );
    }
    if ( ListOfChannels.find( "real_8" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< REAL_8, ChannelType >( DeltaT,
                                                   Channel,
                                                   Iteration,
                                                   -1024,
                                                   1024,
                                                   0.5,
                                                   1024,
                                                   CommandLineArgs,
                                                   ifo ) );
    }

    if ( ListOfChannels.find( "complex_8" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< COMPLEX_8, ChannelType >( DeltaT,
                                                      Channel,
                                                      Iteration,
                                                      COMPLEX_8( -1024, 0 ),
                                                      COMPLEX_8( 1024, 0 ),
                                                      COMPLEX_8( 1, 0 ),
                                                      1024,
                                                      CommandLineArgs,
                                                      ifo ) );
    }
    if ( ListOfChannels.find( "complex_16" ) != last_channel )
    {
        frame_channels.append(
            ramped_channel< COMPLEX_16, ChannelType >( DeltaT,
                                                       Channel,
                                                       Iteration,
                                                       COMPLEX_16( -1024, 0 ),
                                                       COMPLEX_16( 1024, 0 ),
                                                       COMPLEX_16( 1, 0 ),
                                                       1024,
                                                       CommandLineArgs,
                                                       ifo ) );
    }
}
catch ( ... )
{
    return;
}

#define DATA_TYPE( A )                                                         \
    template <>                                                                \
    std::string data_type< A >( )                                              \
    {                                                                          \
        return #A;                                                             \
    }

#if 0
//--------------------------
// Currently not being used
//--------------------------
DATA_TYPE(CHAR_U)
DATA_TYPE(CHAR)
#endif /* 0 */
DATA_TYPE( INT_2U )
DATA_TYPE( INT_2S )
DATA_TYPE( INT_4U )
DATA_TYPE( INT_4S )
DATA_TYPE( INT_8U )
DATA_TYPE( INT_8S )
DATA_TYPE( REAL_4 )
DATA_TYPE( REAL_8 )
DATA_TYPE( COMPLEX_8 )
DATA_TYPE( COMPLEX_16 )

#undef DATA_TYPE

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class ChannelType >
boost::shared_ptr< ChannelType >
create_channel( const std::string& ChannelName,
                INT_4U*            Channel,
                INT_4U             SampleRate,
                const CommandLine& CommandLineArgs )
{
    if
        constexpr( std::is_same_v< ChannelType, FrAdcData > )
        {
            //-------------------------------------------------------------------
            // Create the FrAdcData structure to hold the Adc metadata
            //-------------------------------------------------------------------
            boost::shared_ptr< FrAdcData > channel(
                new FrAdcData( ChannelName,
                               0, /* Group */
                               *Channel, /* Channel Number */
                               32, /* NBits */
                               SampleRate, /* sampleRate */
                               0.0, /* bias */
                               1.0, /* slope */
                               "counts", /* units */
                               0.0, /* fShift */
                               0.0, /* timeOffset */
                               0, /* dataValid */
                               0.0 /* phase */ ) );
            return channel;
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrProcData > )
        {
            //-------------------------------------------------------------------
            // Create the FrProcData structure to hold the ProcData metadata
            //-------------------------------------------------------------------
            boost::shared_ptr< FrProcData > channel(
                new FrProcData( ChannelName,
                                "Moch up of an FrProcData structure",
                                FrProcData::TIME_SERIES, // type
                                FrProcData::UNKNOWN_SUB_TYPE, // subType
                                0.0, // timeOffset
                                1.0, // TRange
                                0.0, // FShift
                                0.0, // Phase
                                0.0, // FRange
                                0.0 // BW
                                ) );
            return channel;
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrSerData > )
        {
            //-------------------------------------------------------------------
            // Create the FrAdcData structure to hold the Adc metadata
            //-------------------------------------------------------------------
            boost::shared_ptr< FrSerData > channel(
                new FrSerData( ChannelName,
                               CommandLineArgs.StartTime( ), /* time */
                               SampleRate /* sampleRate */
                               ) );
            return channel;
        }
    else if
        constexpr( std::is_same_v< ChannelType, FrSimData > )
        {
            //-------------------------------------------------------------------
            // Create the FrAdcData structure to hold the Adc metadata
            //-------------------------------------------------------------------
            boost::shared_ptr< ChannelType > channel(
                new ChannelType( ChannelName,
                                 "Moch up of an FrSimData structure",
                                 SampleRate, /* sampleRate */
                                 0.0, /* fShift */
                                 0.0 /* phase */ ) );
            return channel;
        }
    throw( std::runtime_error( "create_channel: Unhandled type" ) );
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T, class ChannelType >
static boost::shared_ptr< ChannelType >
ramped_channel( INT_4U             DeltaT,
                INT_4U*            Channel,
                INT_4U             Iteration,
                T                  Start,
                T                  Stop,
                T                  Inc,
                INT_4U             SampleRate,
                const CommandLine& CommandLineArgs,
                const char*        IFO )
{
    //-------------------------------------------------------------------
    // Create the channel structure to hold the metadata
    //-------------------------------------------------------------------
    auto channel_name = ramped_name< T >( Iteration, IFO );
    auto channel = create_channel< ChannelType >(
        channel_name, Channel, SampleRate, CommandLineArgs );
    //-------------------------------------------------------------------
    // Link the data to the metadata
    //-------------------------------------------------------------------
    {
        fr_vect_type vec( ramped_vector< T >(
            DeltaT, SampleRate, Iteration, Start, Stop, Inc, IFO ) );

        if
            constexpr( std::is_same_v< ChannelType, FrSerData > )
            {
                channel->RefSerial( ).append( vec );
            }
        else
        {
            channel->RefData( ).append( vec );
        }
    }
    //-------------------------------------------------------------------
    // Incriment the channel number
    //-------------------------------------------------------------------
    ( *Channel )++;
    return channel;
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static boost::shared_array< T >
ramp_data_create( INT_4U Samples, T Start, T Stop, T Inc )
{
    boost::shared_array< T > data( new T[ Samples ] );
    data[ 0 ] = Start;
    for ( INT_4U x = 1; x < Samples; x++ )
    {
        if ( data[ x - 1 ] == Stop )
        {
            data[ x ] = Start;
        }
        else
        {
            data[ x ] = data[ x - 1 ] + Inc;
        }
    }
    return data;
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static string
ramped_name( INT_4U Iteration, const char* Prefix )
{
    ostringstream name;

    name << Prefix << ":RAMPED_" << data_type< T >( ) << "_" << Iteration;
    return name.str( );
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static fr_vect_type
ramped_vector( INT_4U      DeltaT,
               INT_4U      SampleRate,
               INT_4U      Iteration,
               T           Start,
               T           Stop,
               T           Inc,
               const char* IFO )
{
    const INT_4U samples( DeltaT * SampleRate );

    Dimension                dim( samples );
    boost::shared_array< T > data(
        ramp_data_create< T >( samples, Start, Stop, Inc ) );

    fr_vect_type vect( new FrVect(
        ramped_name< T >( Iteration, IFO ), 1, &dim, data.get( ), "" /* unitY */
        ) );
    return vect;
}
