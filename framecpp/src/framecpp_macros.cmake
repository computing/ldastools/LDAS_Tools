#
# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
include( CMakeParseArguments )

function( framecpp_hdr_builder )
  set(options
    )
  set(oneValueArgs
    PREVIOUS_VERSION
    VERSION
    )
  set(multiValueArgs
    HEADERS
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PREVIOUS_VERSION )
    math( EXPR ARG_PREVIOUS_VERSION "${ARG_VERSION} - 1" )
  endif( )
  set( version ${ARG_VERSION} )
  set( previous ${ARG_PREVIOUS_VERSION} )
  set( semicolon ";" )
  foreach( hdr ${ARG_HEADERS} )
    string( REPLACE ".hh" "" class ${hdr} )
    if ( class STREQUAL "FrVect" )
      set( body
        "#ifndef FrameCPP__VERSION_@version@__@class@_HH"
        "#define FrameCPP__VERSION_@version@__@class@_HH"
        " "
        "#include \"framecpp/Version@previous@/@class@.hh\""
        " "
        "#include \"framecpp/Version@version@/FrameSpec.hh\""
        " "
        "#if defined(__cplusplus)"
        "namespace FrameCPP"
        "{"
        "  namespace Version_@version@"
        "  {"
        "    using Previous::Dimension@semicolon@"
        "    using Previous::FrVectDataTypes@semicolon@"
        "    using Previous::@class@@semicolon@"
        "  } /* namespace - Version_@version@ */"
        "} /* namespace - FrameCPP */"
        " "
        "#endif /* FrameCPP__VERSION_@version@__@class@_HH */"
        "#endif /* defined(__cplusplus) */"
        )
    elseif ( class STREQUAL "STRING" )
      set( body
        "#ifndef FrameCPP__VERSION_@version@__@class@_HH"
        "#define FrameCPP__VERSION_@version@__@class@_HH"
        " "
        "#include \"framecpp/Version@version@/FrameSpec.hh\""
        " "
        "#include \"framecpp/Version@previous@/@class@.hh\""
        " "
        "#if defined(__cplusplus)"
        "#if !defined(SWIG)"
        "namespace FrameCPP"
        "{"
        "  namespace Version_@version@"
        "  {"
        "    using Previous::@class@@semicolon@"
        "    using Previous::@class@_SHADOW@semicolon@"
        "  } /* namespace - Version_@version@ */"
        "} /* namespace - FrameCPP */"
        "#endif /* ! defined(SWIG) */"
        "#endif /* defined(__cplusplus) */"
        " "
        "#endif /* FrameCPP__VERSION_@version@__@class@_HH */"
        )
    else( )
      set( body
        "#ifndef FrameCPP__VERSION_@version@__@class@_HH"
        "#define FrameCPP__VERSION_@version@__@class@_HH"
        " "
        "#include \"framecpp/Version@version@/FrameSpec.hh\""
        " "
        "#include \"framecpp/Version@previous@/@class@.hh\""
        " "
        "#if defined(__cplusplus)"
        "#if !defined(SWIG)"
        "namespace FrameCPP"
        "{"
        "  namespace Version_@version@"
        "  {"
        "    using Previous::@class@@semicolon@"
        "  } /* namespace - Version_@version@ */"
        "} /* namespace - FrameCPP */"
        "#endif /* ! defined(SWIG) */"
        "#endif /* defined(__cplusplus) */"
        " "
        "#endif /* FrameCPP__VERSION_@version@__@class@_HH */"
        )
    endif( )
    set( mode WRITE )
    foreach( line ${body} )
      string( CONFIGURE "${line}" line @ONLY )
      file( ${mode} ${CMAKE_CURRENT_BINARY_DIR}/${hdr} "${line}\n" )
      set( mode APPEND )
    endforeach( )
  endforeach( )

endfunction( )

function( framecpp_interface_hdr_builder )
  set(options
    )
  set(oneValueArgs
    VERSION
    PREVIOUS_VERSION
    )
  set(multiValueArgs
    HEADERS
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PREVIOUS_VERSION )
    math( EXPR ARG_PREVIOUS_VERSION "${ARG_VERSION} - 1" )
  endif( )
  set( version ${ARG_VERSION} )
  set( previous ${ARG_PREVIOUS_VERSION} )
  set( semicolon ";" )
  foreach( hdr ${ARG_HEADERS} )
    string( REPLACE ".hh" "" class ${hdr} )
    if ( class STREQUAL "FrameStream" )
      set( body
        "#ifndef FRAME_CPP_INTERFACE__@class@_HH"
        "#define FRAME_CPP_INTERFACE__@class@_HH"
        " "
        "#include \"framecpp/FrameCPP.hh\""
        " "
        "#include \"framecpp/Version@version@/@class@.hh\""
        " "
        "#if defined(__cplusplus)"
        "#if !defined(SWIG)"
        "namespace FrameCPP"
        "{"
        "  /**"
        "   * \\brief @comment@"
        "   */"
        "  typedef Version::I@class@ I@class@@semicolon@"
        "  typedef Common::O@class@ O@class@@semicolon@"
        "}"
        "#endif /* !defined(SWIG) */"
        "#endif /* defined(__cplusplus) */"
        " "
        "#endif /* FRAME_CPP_INTERFACE__@class@_HH */"
        )
    else( )
      set( body
        "#ifndef FRAME_CPP_INTERFACE__@class@_HH"
        "#define FRAME_CPP_INTERFACE__@class@_HH"
        " "
        "#include \"framecpp/FrameCPP.hh\""
        " "
        "#include \"framecpp/Version@version@/@class@.hh\""
        " "
        "#if defined(__cplusplus)"
        "#if !defined(SWIG)"
        "namespace FrameCPP"
        "{"
        "  /**"
        "   * \\brief @comment@"
        "   */"
        "  typedef Version::@class@ @class@@semicolon@"
        "}"
        "#endif /* !defined(SWIG) */"
        "#endif /* defined(__cplusplus) */"
        " "
        "#endif /* FRAME_CPP_INTERFACE__@class@_HH */"
        )
    endif( )
    set( mode WRITE )
    foreach( line ${body} )
      string( CONFIGURE "${line}" line @ONLY )
      file( ${mode} ${CMAKE_CURRENT_BINARY_DIR}/${hdr} "${line}\n" )
      set( mode APPEND )
    endforeach( )
  endforeach( )
endfunction( )

function( framecpp_create_cc_built_files match_string replace_string )
  foreach( filename ${ARGN})
    string( REPLACE ${match_string} ${replace_string} src_filename ${filename} )
    file( WRITE "${CMAKE_CURRENT_BINARY_DIR}/${filename}" "#include \"${src_filename}\"\n" )
  endforeach( )
endfunction( )
