#include "FrMsg.hh"

void
TestFrMsgVersion5( )
{
    using FrameCPP::v5::FrMsgData;

    FrMsgData frmsg;

    //---------------------------------------------------------------------
    // Validate data types
    //---------------------------------------------------------------------
    auto alarm = frmsg.GetAlarm( );
    auto message = frmsg.GetMessage( );
    auto severity = frmsg.GetSeverity( );
    auto gtime = frmsg.GetGTime( );

    EXPECT_EQ( typeid( alarm ), typeid( std::string ) );
    EXPECT_EQ( typeid( message ), typeid( std::string ) );
    EXPECT_EQ( typeid( severity ), typeid( INT_4U ) );
    EXPECT_EQ( typeid( gtime ), typeid( LDASTools::AL::GPSTime ) );

    //---------------------------------------------------------------------
    // Validate data i/o
    //---------------------------------------------------------------------

    static const FrMsgData::alarm_type    alarm_expected( "alarm" );
    static const FrMsgData::message_type  message_expected( "message" );
    static const FrMsgData::severity_type severity_expected( 5 );
    static const FrMsgData::GTime_type    gtime_expected( 2100, 2200 );

    frmsg.SetAlarm( alarm_expected );
    frmsg.SetMessage( message_expected );
    frmsg.SetSeverity( severity_expected );
    frmsg.SetGTime( gtime_expected );

    EXPECT_EQ( frmsg.GetAlarm( ), alarm_expected );
    EXPECT_EQ( frmsg.GetMessage( ), message_expected );
    EXPECT_EQ( frmsg.GetSeverity( ), severity_expected );
    EXPECT_EQ( frmsg.GetGTime( ), gtime_expected );
}
