//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2020 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAMECPPCPP__STORAGE__DATA__V3__FR_HISTORY_HH
#define FRAMECPPCPP__STORAGE__DATA__V3__FR_HISTORY_HH

#if defined( __cplusplus )
#if !defined( SWIG )

#include <string>

#include "ldastoolsal/ldas_types.h"

#endif /* !defined( SWIG ) */
#endif /* !defined( __cplusplus ) */

#undef FR_HISTORY_NAME_TYPE
#define FR_HISTORY_NAME_TYPE std::string

#undef FR_HISTORY_TIME_TYPE
#define FR_HISTORY_TIME_TYPE INT_4U

#undef FR_HISTORY_COMMENT_TYPE
#define FR_HISTORY_COMMENT_TYPE std::string

#if defined( __cplusplus )
#if !defined( SWIG )

namespace FrameCPP
{
    namespace v3
    {
        //-----------------------------------------------------------
        /// @brief Storage space for FrMsg information
        ///
        /// This stores information about an FrMsg element that
        /// needs to persist beyond an I/O operation
        //-----------------------------------------------------------
        class FrMsgData
        {
        public:
            typedef FR_HISTORY_NAME_TYPE    name_type;
            typedef FR_HISTORY_TIME_TYPE    time_type;
            typedef FR_HISTORY_COMMENT_TYPE comment_type;

            inline const name_type&
            GetName( ) const
            {
                return ( name );
            }

            inline const comment_type&
            GetComment( ) const
            {
                return ( comment );
            }

            inline time_type
            GetTime( ) const
            {
                return ( time );
            }

            inline void
            SetName( const name_type& Value )
            {
                name = Value;
            }

            inline void
            SetComment( const comment_type& Value )
            {
                comment = Value;
            }

            inline void
            SetTime( time_type Value )
            {
                time = Value;
            }

        protected:
            //-------------------------------------------------------------
            /// Name of history record
            //-------------------------------------------------------------
            name_type name;
            //-------------------------------------------------------------
            /// Time of post-processing
            //-------------------------------------------------------------
            time_type time;
            //-------------------------------------------------------------
            /// Program name and releant comments needed to define
            /// post-processing
            //-------------------------------------------------------------
            comment_type comment;
        };

    } // namespace v3

} // namespace FrameCPP

#endif /* !defined( SWIG ) */
#endif /* !defined( __cplusplus ) */

#endif /* FRAMECPPCPP__STORAGE__DATA__V3__FR_HISTORY_HH */
