#include "FrMsg.hh"

void
TestFrMsgVersion3( )
{
    using FrameCPP::v3::FrMsgData;

    FrMsgData frmsg;

    //---------------------------------------------------------------------
    // Validate data types
    //---------------------------------------------------------------------
    auto alarm = frmsg.GetAlarm( );
    auto message = frmsg.GetMessage( );
    auto severity = frmsg.GetSeverity( );

    EXPECT_EQ( typeid( alarm ), typeid( std::string ) );
    EXPECT_EQ( typeid( message ), typeid( std::string ) );
    EXPECT_EQ( typeid( severity ), typeid( INT_4U ) );

    //---------------------------------------------------------------------
    // Validate data i/o
    //---------------------------------------------------------------------

    static const FrMsgData::alarm_type    alarm_expected( "alarm" );
    static const FrMsgData::message_type  message_expected( "message" );
    static const FrMsgData::severity_type severity_expected( 5 );

    frmsg.SetAlarm( alarm_expected );
    frmsg.SetMessage( message_expected );
    frmsg.SetSeverity( severity_expected );

    EXPECT_EQ( frmsg.GetAlarm( ), alarm_expected );
    EXPECT_EQ( frmsg.GetMessage( ), message_expected );
    EXPECT_EQ( frmsg.GetSeverity( ), severity_expected );
}
