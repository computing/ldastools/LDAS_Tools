//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version9/FrameSpec.hh"
#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/FrTOC.hh"

#include "framecpp/Version9/STRING.hh"

#include "../Common/FrTOCPrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::TOCInfo;

namespace FrameCPP
{
    namespace Version_8
    {
        //===================================================================
        //===================================================================
        void
        FrTOCSimData::Load( istream_type& Stream, INT_4U FrameCount )
        {
            nsim_type n;

            Stream >> n;
            if ( n && ( n != FrTOC::NO_DATA_AVAILABLE ) )
            {
                //---------------------------------------------------------------
                // Read in the information
                //---------------------------------------------------------------
                std::vector< name_type >::size_type s( n );
                std::vector< position_type >        positions( s * FrameCount );

                m_keys.resize( s );
                Stream >> m_keys >> positions;

                //---------------------------------------------------------------
                // Move into structure.
                //---------------------------------------------------------------
                typedef FrameCPP::Common::FrTOCPositionInputFunctor<
                    MapSim_type,
                    name_type,
                    position_type >
                    functor;

                functor f( m_info, positions.begin( ), FrameCount );

                std::for_each( m_keys.begin( ), m_keys.end( ), f );
            }
        }

        void
        FrTOCSimData::QuerySim( const Common::TOCInfo& Info,
                                INT_4U                 FrameOffset,
                                INT_8U                 Position )
        {
            STRING name;

            Info.TOCQuery( TOCInfo::IC_NAME,
                           TOCInfo::DT_STRING_2,
                           &name,
                           TOCInfo::IC_EOQ );

            sim_info_type& i( m_info[ name ] );
            i.resize( FrameOffset + 1 );
            i[ FrameOffset ] = Position;
        }

        //-------------------------------------------------------------------
        /// This method allows for iterting over each element of information
        /// and allows the caller to gather information about each element.
        //-------------------------------------------------------------------
        void
        FrTOCSimData::forEach( Common::FrTOC::query_info_type Info,
                               Common::FrTOC::FunctionBase&   Action ) const
        {
            switch ( Info )
            {
            case Common::FrTOC::TOC_CHANNEL_NAMES:
            {
                try
                {
                    Common::FrTOC::FunctionString& action(
                        dynamic_cast< Common::FrTOC::FunctionString& >(
                            Action ) );

                    for ( MapSim_type::const_iterator cur = m_info.begin( ),
                                                      last = m_info.end( );
                          cur != last;
                          ++cur )
                    {
                        action( cur->first );
                    }
                }
                catch ( ... )
                {
                    // Does not understand Action
                }
            }
            break;
            default:
                //---------------------------------------------------------------
                // ignore all other requests
                //---------------------------------------------------------------
                break;
            }
        }

        void
        FrTOCSimData::write( Common::OStream& Stream ) const
        {
            //-----------------------------------------------------------------
            // Flatten data so it is streamable
            //-----------------------------------------------------------------
            std::vector< name_type >::size_type s( m_info.size( ) );
            if ( s )
            {
                std::vector< name_type >::size_type fc(
                    m_info.begin( )->second.size( ) );

                std::vector< name_type >     names( s );
                std::vector< position_type > positions( s * fc );
                //---------------------------------------------------------------
                // Copy data for streaming
                //---------------------------------------------------------------
                std::vector< name_type >::iterator cur_name = names.begin( );
                std::vector< position_type >::iterator cur_position =
                    positions.begin( );

                for ( MapSim_type::const_iterator cur = m_info.begin( ),
                                                  last = m_info.end( );
                      cur != last;
                      ++cur, ++cur_name, cur_position += fc )
                {
                    *cur_name = cur->first;
                    std::copy( cur->second.begin( ),
                               cur->second.end( ),
                               cur_position );
                }
                //---------------------------------------------------------------
                // Stream out
                //---------------------------------------------------------------
                Stream << nsim_type( s ) << names << positions;
            }
            else
            {
                Stream << nsim_type( FrTOC::NO_DATA_AVAILABLE );
            }
        }

        const FrTOCSimData&
        FrTOCSimData::operator=( const Previous::FrTOCSimData& Source )
        {
            m_info = Source.GetSim( );

            return *this;
        }

    } // namespace Version_8
} // namespace FrameCPP
