//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <memory>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrMsg.hh"

#include "framecpp/Version9/FrMsg.hh"

#include "framecpp/Version9/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE_9( FrMsgImpl,
                              FSI_FR_MSG,
                              "FrMsg",
                              "Message Data Structure" )

namespace FrameCPP
{
    namespace Version_9
    {
        //=======================================================================
        // FrMsg
        //=======================================================================
        FrMsg::FrMsg( )
        {
            severity = DEFAULT_SEVERITY;
        }

        FrMsg::FrMsg( const FrMsg& Source )
        {
            alarm = Source.GetAlarm( );
            message = Source.GetMessage( );
            severity = Source.GetSeverity( );
            GTime = Source.GetGTime( );
        }

        FrMsg::FrMsg( const std::string& Alarm,
                      const std::string& Message,
                      INT_4U             Severity,
                      const GPSTime&     GTimeValue )
        {
            alarm = Alarm;
            message = Message;
            severity = Severity;
            GTime = GTimeValue;
        }

        FrMsg::FrMsg( Previous::FrMsg& Source, stream_base_type* Stream )
        {
            alarm = Source.GetAlarm( );
            message = Source.GetMessage( );
            severity = Source.GetSeverity( );
            GTime = Source.GetGTime( );
            if ( Stream )
            {
                /// \todo Need to replace the next reference
            }
        }

        FrMsg::~FrMsg( )
        {
        }

        const FrMsg::alarm_type&
        FrMsg::GetAlarm( ) const
        {

            return FrMsgImpl::Data::GetAlarm( );
        }

        FrMsg&
        FrMsg::Merge( const FrMsg& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += FrameSpecName( );

            throw std::domain_error( msg );
            return *this;
        }

        namespace FrMsgImpl
        {
            template <>
            bool
            ClassicIO< FrMsg >::
            operator==( const Common::FrameSpec::Object& Obj ) const
            {
                return Common::Compare( *this, Obj );
            }
        } // namespace FrMsgImpl
    } // namespace Version_9
} // namespace FrameCPP
