//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/make_shared.hpp>
#include <boost/unordered_map.hpp>

#include "framecpp/Version9/FrDetector.hh"

#include "Common/ComparePrivate.hh"

#include "framecpp/Common/FrameSpec.tcc"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE_9( FrDetectorImpl,
                              FSI_FR_DETECTOR,
                              "FrDetector",
                              "Detector Data Structure" )

namespace
{
    using namespace FrameCPP::Version_9;

    typedef boost::unordered_map< FrDetector::dataQualityOffsets,
                                  FrDetector::FrDetectorCache_element >
        FrDetectorCache_type;

    typedef boost::bimap< std::string, FrDetector::dataQualityOffsets >
        FrDetectorPrefixMap_type;

    static FrDetectorCache_type FrDetectorCache{
        { FrDetector::DQO_ACIGA,
          boost::make_shared< FrDetector >(
              "ACIGA", // name
              "U1", // prefix
              0.0, // longitude
              0.0, // latitude
              0.0, // elevation
              0.0, // armXaltitude
              0.0, // armYaltitude
              0.0, // armXazimuth
              0.0, // armYazimuth
              0.0, // armXmidpoint
              0.0, // armYmidpoint
              FrDetector::DQO_ACIGA // dataQualityOffset
              ) },
        { FrDetector::DQO_CIT_40,
          boost::make_shared< FrDetector >(
              "CIT_40", // name
              "C1", // prefix
              0.0, // longitude
              0.0, // latitude
              0.0, // elevation
              0.0, // armXaltitude
              0.0, // armYaltitude
              0.0, // armXazimuth
              0.0, // armYazimuth
              0.0, // armXmidpoint
              0.0, // armYmidpoint
              FrDetector::DQO_CIT_40 // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // GEO -
          // Values taken from lal/src/tools/LALDetectors.h
          //---------------------------------------------------------------------
          FrDetector::DQO_GEO_600,
          boost::make_shared< FrDetector >(
              "GEO_600", // name
              "G1", // prefix
              0.17116780435, // longitude
              0.91184982752, // latitutde
              114.425, // elevation
              1.19360100484, // armXazimuth
              5.83039279401, // armYazimuth
              0.00000000000, // armXaltitude
              0.00000000000, // armYaltitude
              300.00000000000, // armXmidpoint
              300.00000000000, // armYmidpoint
              FrDetector::DQO_GEO_600 // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // KAGRA -
          // Values taken from lal/src/tools/LALDetectors.h
          //---------------------------------------------------------------------
          FrDetector::DQO_KAGRA,
          boost::make_shared< FrDetector >(
              "KAGRA", // name
              "K1", // prefix
              2.396441015, // longitude
              0.6355068497, // latitude
              414.181, // elevation
              1.054113, // armXazimuth
              -0.5166798, // armYazimuth
              0.0031414, // armXaltitude
              -0.0036270, // armYaltitude
              1513.2535, // armXmidpoint
              1511.611, // armYmidpoint
              FrDetector::DQO_KAGRA // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // HANFORD - Derived from:
          // /archive/frames/S5/L3/LHO/H-RDS_R_L3-8766/H-RDS_R_L3-876699904-256.gwf
          //---------------------------------------------------------------------
          FrDetector::DQO_LHO_4K,
          boost::make_shared< FrDetector >(
              "LHO_4k", // name
              "H1", // prefix
              -2.08406, // longitute
              0.810795, // latitude
              142.554, // elevation
              5.65488, // armXazimuth
              4.084808, // armYazimuth
              -0.0006195, // armXaltitude
              0.0000125, // armYaltitude
              1997.54, // armXmidpoint
              1997.52, // armYmidpoint
              FrDetector::DQO_LHO_4K // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // LIGO India
          //---------------------------------------------------------------------
          FrDetector::DQO_LIGO_INDIA,
          boost::make_shared< FrDetector >(
              "LIGO_India",
              "A1",
              0.0, // longitude
              0.0, // latitude
              0.0, // elevation
              0.0, // armXaltitude
              0.0, // armYaltitude
              0.0, // armXazimuth
              0.0, // armYazimuth
              0.0, // armXmidpoint
              0.0, // armYmidpoint
              FrDetector::DQO_LIGO_INDIA // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // LIVINGSTON - Derived from:
          // /archive/frames/S5/L3/LLO/L-RDS_R_L3-8766/L-RDS_R_L3-876699904-256.gwf
          //---------------------------------------------------------------------
          FrDetector::DQO_LLO_4K,
          boost::make_shared< FrDetector >(
              "LLO_4k", // name
              "L1", // prefix
              -1.58431, // longitude
              0.533423, // latitude
              -6.574, // elevation
              4.40318, // armXazimuth
              2.83238, // armYazimuth
              -0.0003121, // armXaltitude
              -0.0006107, // armYaltitude
              1997.57, // armXmidpoint
              1997.57, // armYmidpoint
              FrDetector::DQO_LLO_4K // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // TAMA -
          // Values taken from lal/src/tools/LALDetectors.h
          //---------------------------------------------------------------------
          FrDetector::DQO_TAMA_300,
          boost::make_shared< FrDetector >(
              "TAMA_300", // name
              "T1", // prefix
              2.43536359469, // longitude
              0.62267336022, // latitude
              90.0, // elevation
              4.71238898038, // armXazimuth
              3.14159265359, // armYazimuth
              0.00000000000, // armXaltitude
              0.00000000000, // armYaltitude
              150.00000000000, // armXmidpoint
              150.00000000000, // armYmidpoint
              FrDetector::DQO_TAMA_300 // dataQualityOffset
              ) },
        { //---------------------------------------------------------------------
          // VIRGO - Derived from:
          // /archive/frames/WSR8/HrecV3/Virgo/V-HrecV3-855/V-HrecV3-855302400-2240.gwf
          //---------------------------------------------------------------------
          FrDetector::DQO_VIRGO,
          boost::make_shared< FrDetector >(
              "Virgo",
              "V1", // prefix
              -10.5045, // longitude
              43.6316, // latitude
              51.884, // elevation
              -0.3229, // armXazimuth
              4.3895, // armYazimuth
              0.0, // armXaltitude
              0.0, // armYaltitude
              1500.0, // armXmidpoint
              1500.0, // armYmidpoint
              FrDetector::DQO_VIRGO // dataQualityOffset
              ) }

    };

} // namespace

namespace FrameCPP
{
    namespace Version_9
    {
        //=======================================================================
        // FrDetector
        //=======================================================================
        FrDetector::FrDetector( )
        {
            prefix[ 0 ] = ' ';
            prefix[ 1 ] = ' ';
            longitude = 0.0;
            latitude = 0.0;
            elevation = 0.0;
            armXazimuth = 0.0;
            armYazimuth = 0.0;
            armXaltitude = 0.0;
            armYaltitude = 0.0;
            armXmidpoint = 0.0;
            armYmidpoint = 0.0;
        }

        FrDetector::FrDetector( const name_type&             Name,
                                const char*                  Prefix,
                                const longitude_type         Longitude,
                                const latitude_type          Latitude,
                                const elevation_type         Elevation,
                                const armXazimuth_type       ArmXazimuth,
                                const armYazimuth_type       ArmYazimuth,
                                const armXaltitude_type      ArmXaltitude,
                                const armYaltitude_type      ArmYaltitude,
                                const armXmidpoint_type      ArmXmidpoint,
                                const armYmidpoint_type      ArmYmidpoint,
                                const dataQualityOffset_type DataQualityOffset )
        {
            name = Name;
            prefix[ 0 ] = Prefix[ 0 ];
            prefix[ 1 ] = Prefix[ 1 ];
            longitude = Longitude;
            latitude = Latitude;
            elevation = Elevation;
            armXazimuth = ArmXazimuth;
            armYazimuth = ArmYazimuth;
            armXaltitude = ArmXaltitude;
            armYaltitude = ArmYaltitude;
            armXmidpoint = ArmXmidpoint;
            armYmidpoint = ArmYmidpoint;
            dataQualityOffset = DataQualityOffset;
        }

        FrDetector::FrDetector( const FrDetector& detector )
        {
            AT( );
            name = detector.name;
            AT( );
            prefix[ 0 ] = detector.prefix[ 0 ];
            AT( );
            prefix[ 1 ] = detector.prefix[ 1 ];
            AT( );
            longitude = detector.longitude;
            AT( );
            latitude = detector.latitude;
            AT( );
            elevation = detector.elevation;
            AT( );
            armXazimuth = detector.armXazimuth;
            AT( );
            armYazimuth = detector.armYazimuth;
            AT( );
            armXaltitude = detector.armXaltitude;
            AT( );
            armYaltitude = detector.armYaltitude;
            AT( );
            armXmidpoint = detector.armXmidpoint;
            AT( );
            armYmidpoint = detector.armYmidpoint;
            AT( );
            dataQualityOffset = detector.dataQualityOffset;
            AT( );
            aux = detector.aux;
            AT( );
            table = detector.table;
            AT( );
        }

        FrDetector::FrDetector( Previous::FrDetector& Source,
                                stream_base_type*     Stream )
        {
            name = Source.GetName( );
            std::copy( Source.GetPrefix( ), &Source.GetPrefix( )[ 2 ], prefix );
            longitude = Source.GetLongitude( );
            latitude = Source.GetLatitude( );
            elevation = Source.GetElevation( );
            armXazimuth = Source.GetArmXazimuth( );
            armYazimuth = Source.GetArmYazimuth( );
            armXaltitude = Source.GetArmXaltitude( );
            armYaltitude = Source.GetArmYaltitude( );
            armXmidpoint = Source.GetArmXmidpoint( );
            armYmidpoint = Source.GetArmYmidpoint( );

            auto istream = FrameCPP::Common::IsIStream( Stream );

            if ( istream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                istream->ReplaceRef( RefAux( ), Source.RefAux( ), MAX_REF );
                istream->ReplaceRef( RefTable( ), Source.RefTable( ), MAX_REF );
            }
        }

        FrDetector::FrDetectorCache_element
        FrDetector::GetDetector( dataQualityOffsets Offset )
        {
            auto detector_iter = FrDetectorCache.find( Offset );
            if ( detector_iter != FrDetectorCache.end( ) )
            {
                return ( detector_iter->second );
            }
            throw std::range_error( "Unknown detector requested" );
        }

        const std::string&
        FrDetector::GetName( ) const
        {
            return Data::GetName( );
        }

        FrDetector&
        FrDetector::Merge( const FrDetector& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrDetector::operator==( const Common::FrameSpec::Object& RHS ) const
        {
            return Common::Compare( *this, RHS );
        }

    } // namespace Version_9
} // namespace FrameCPP
