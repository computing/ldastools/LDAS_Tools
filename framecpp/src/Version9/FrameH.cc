//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameSpec.tcc"
#include "Common/ComparePrivate.hh"

#include "framecpp/Version9/FrameH.hh"

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE_9( FrameHImpl,
                              FSI_FRAME_H,
                              "FrameH",
                              "Frame Header Structure" )

namespace FrameCPP
{
    namespace Version_9
    {
        //===================================================================
        //
        //===================================================================
        FrameH::FrameH( )
        {
        }

        FrameH::FrameH( const name_type&  name,
                        run_type          run,
                        frame_type        frame,
                        const GTime_type& time,
                        const dt_type     dt,
                        dataQuality_type  dqual )
        {
            Data::name = name;
            Data::run = run;
            Data::frame = frame;
            Data::GTime = time;
            Data::dt = dt;
            Data::dataQuality = dqual;
        }

        //=======================================================================
        //
        //=======================================================================
        FrameH::FrameH( const FrameH& frame )
        {
            Data::name = frame.GetName( );
            Data::run = frame.GetRun( );
            Data::frame = frame.GetFrame( );
            Data::GTime = frame.GetGTime( );
            Data::dt = frame.GetDt( );
            Data::dataQuality = frame.GetDataQuality( );

            Data::type = frame.type;
            Data::user = frame.user;
            Data::detectSim = frame.detectSim;
            Data::detectProc = frame.detectProc;
            Data::history = frame.history;
            Data::rawData = frame.rawData;
            Data::procData = frame.procData;
            Data::simData = frame.simData;
            Data::event = frame.event;
            Data::simEvent = frame.simEvent;
            Data::summaryData = frame.summaryData;
            Data::auxData = frame.auxData;
            Data::auxTable = frame.auxTable;
        }

        FrameH::FrameH( const Previous::FrameH& Source,
                        stream_base_type*       Stream )
            : FrameHImpl::ClassicIO< FrameH >( Source, Stream )
        {
        }

        bool
        FrameH::operator==( const Common::FrameSpec::Object& RHS ) const
        {
            return Common::Compare( *this, RHS );
        }
    } // namespace Version_9
} // namespace FrameCPP

#if 0
#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#define ACCESS_FRAME_H_PRIVATE_PARTS 1

#include "ldastoolsal/types.hh"

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameSpec.tcc"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version8/FrameH.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"
#include "framecpp/Version8/FrEndOfFrame.hh"
#include "framecpp/Version8/PTR_STRUCT.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrameH::getStaticName( ) << " "       \
              << (void*)this << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrameH::getStaticName( ) << " "         \
              << (void*)this << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

namespace FrameCPP
{
    namespace Version_8
    {

#if 0
    FrameHNPS::
    FrameHNPS( )
    {
    }

    FrameHNPS::
    FrameHNPS( const FrameHNPS& Source )
      :   m_data( Source.m_data )
    {
    }

    FrameHNPS::
    FrameHNPS( const std::string& name,
	       INT_4S run,
	       INT_4U frame,
	       const GPSTime& time,
	       const REAL_8 dt,
	       INT_4U dqual )
      : m_data( name, run, frame, time, dt, dqual )
    {
    }

    void FrameHNPS::
    TOCQuery( int InfoClass, ... ) const
    {
      using Common::TOCInfo;

      va_list	vl;
      va_start( vl, InfoClass );
      while ( InfoClass != TOCInfo::IC_EOQ )
      {
	int data_type = va_arg( vl, int );
	switch( data_type )
	{
	case TOCInfo::DT_INT_2S:
	  {
	    INT_2S*	data = va_arg( vl, INT_2S* );
	    switch( InfoClass )
	    {
	    default:
	      goto cleanup;
	      break;
	    }
	  }
	  break;
	case TOCInfo::DT_INT_4S:
	  {
	    INT_4S*	data = va_arg( vl, INT_4S* );
	    switch( InfoClass )
	    {
	    case TOCInfo::IC_RUN:
	      *data = GetRun( );
	      break;
	    default:
	      goto cleanup;
	      break;
	    }
	  }
	  break;
	case TOCInfo::DT_INT_4U:
	  {
	    INT_4U*	data = va_arg( vl, INT_4U* );
	    switch( InfoClass )
	    {
	    case TOCInfo::IC_DATA_QUALITY:
	      *data = GetDataQuality( );
	      break;
	    case TOCInfo::IC_GTIME_S:
	      *data = GetGTime( ).GetSeconds( );
	      break;
	    case TOCInfo::IC_GTIME_N:
	      *data = GetGTime( ).GetNanoseconds( );
	      break;
	    case TOCInfo::IC_FRAME:
	      *data = GetFrame( );
	      break;
	    default:
	      goto cleanup;
	      break;
	    }
	  }
	  break;
	case TOCInfo::DT_REAL_8:
	  {
	    REAL_8* data = va_arg( vl, REAL_8* );
	    switch( InfoClass )
	    {
	    case TOCInfo::IC_DT:
	      *data = GetDt( );
	      break;
	    default:
	      goto cleanup;
	      break;
	    }
	  }
	  break;
	default:
	  // Stop processing
	  goto cleanup;
	}
	InfoClass = va_arg( vl, int );
      }
    cleanup:
      va_end( vl );
    }
#endif /* 0 */

        FrameH::FrameH( const FrameHNPS& Source )
            : object_type( StructDescription( ) ), FrameHNPS( Source )
        {
        }

        void
        FrameH::VerifyObject( Common::Verify&       Verifier,
                              Common::IFrameStream& Stream ) const
        {
        }

        void
        FrameH::pWrite( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );

            boost::shared_ptr< FrEndOfFrame > eof(
                new FrEndOfFrame( GetRun( ), GetFrame( ) ) );
            Stream.PushSingle( eof );
        }

        void
        FrameH::readSubset( istream_type& Stream, INT_4U ElementMask )
        {
            m_refs.readSubset( Stream, ElementMask );
        }

    } // namespace Version_9
} // namespace FrameCPP
#endif /* 0 */
