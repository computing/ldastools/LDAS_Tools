//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version9/FrRawData.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE_9( FrRawDataImpl,
                              FSI_FR_RAW_DATA,
                              "FrRawData",
                              "Raw Data Structure" )

namespace FrameCPP
{
    namespace Version_9
    {
        //=======================================================================
        // FrRawData
        //=======================================================================
        FrRawData::FrRawData( Previous::FrRawData& Source,
                              stream_base_type*    Stream )
            : FrRawDataImpl::ClassicIO< FrRawData >( Source, Stream )
        {
            Data::name = Source.GetName( );
        }

        FrRawData::FrRawData( const std::string& Name )
        {
            Data::name = Name;
        }

        const FrRawData::name_type&
        FrRawData::GetName( ) const
        {
            return Data::GetName( );
        }
        bool
        FrRawData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

    } // namespace Version_9
} // namespace FrameCPP
