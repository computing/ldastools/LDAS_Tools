//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <cmath>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "ldastoolsal/util.hh"

#include "framecpp/Common/CheckSum.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version9/FrameSpec.hh"

#include "framecpp/Version9/FrHeader.hh"

using FrameCPP::Common::FrameSpec;

namespace
{
#define _PI_CONST_ 3.1415926535897932384

    static const INT_2U x1234( 0x1234 );
    static const INT_4U x12345678( 0x12345678 );
    static const INT_8U x123456789abcdef( 0x123456789abcdefLL );

    static const REAL_4 pi_REAL_4( _PI_CONST_ );
    static const REAL_8 pi_REAL_8( _PI_CONST_ );

    template < class T >
    void
    check_magic_number( const unsigned char* const Source,
                        const T                    Ref,
                        const std::string&         Bytes )
    {
        T buf;

        //-------------------------------------------------------------------
        // Load the buffer
        //-------------------------------------------------------------------
        memcpy( &buf, Source, sizeof( buf ) );
        //-------------------------------------------------------------------
        // Check the bytes
        //-------------------------------------------------------------------
        if ( Ref != buf )
        {
            //-----------------------------------------------------------------
            // Reverse the bytes and check again
            //-----------------------------------------------------------------
            reverse< sizeof( buf ) >( &buf, 1 );
            if ( Ref != buf )
            {
                std::ostringstream err;

                err << "Data corruption detected in FrHeader at byte(s): "
                    << Bytes;
                //---------------------------------------------------------------
                // Pattern does not match either BIGENDIAN or LITTLEENDIAN so
                //   it must not be a frame file.
                //---------------------------------------------------------------
                throw std::runtime_error( err.str( ) );
            }
        }
    }

    template < class T >
    inline void
    check_pi( const T Source, const T Precision, const std::string& Bytes )
    {
        const T pi( _PI_CONST_ );

        //-------------------------------------------------------------------
        // Check the bytes
        //-------------------------------------------------------------------
        if ( std::abs( Source - pi ) > Precision )
        {
            std::ostringstream err;

            err << "Value of PI out of range in FrHeader at Bytes: " << Bytes;
            throw std::runtime_error( err.str( ) );
        }
    }
#undef _PI_CONST_
} // namespace

namespace FrameCPP
{
    namespace Version_9
    {
        const char* FrHeader::ORIGINATOR_IGWD = "IGWD";

        FrHeader::FrHeader( )
            : FrameSpec::Object( s_object_id,
                                 (const Common::Description*)NULL,
                                 DATA_FORMAT_VERSION ),
              m_checksum( 0 ), m_checksum_value( 0 )
        {
            m_data.raw.frameChecksumScheme = Common::CheckSum::NONE;
        }

        FrHeader::FrHeader( istream_type& Stream )
            : FrameSpec::Object( s_object_id,
                                 (const Common::Description*)NULL,
                                 DATA_FORMAT_VERSION ),
              m_checksum( 0 )
        {
            istream_type::char_type* d =
                reinterpret_cast< istream_type::char_type* >( &( m_data.raw ) );
            Stream.read( d, sizeof( m_data.raw ) );
            //---------------------------------------------------------------------
            // Figure out how to order the bytes
            //---------------------------------------------------------------------
            {
                INT_2U x;
                char*  xptr = reinterpret_cast< char* >( &x );
                std::copy( m_data.raw.x1234,
                           &m_data.raw.x1234[ sizeof( m_data.raw.x1234 ) ],
                           xptr );
                m_reorder_bytes = ( x1234 != x );
            }
            if ( m_reorder_bytes )
            {
                if ( FrameCPP::HostByteOrder ==
                     FrameCPP::BYTE_ORDER_BIG_ENDIAN )
                {
                    m_byte_order = FrameCPP::BYTE_ORDER_LITTLE_ENDIAN;
                }
                else
                {
                    m_byte_order = FrameCPP::BYTE_ORDER_BIG_ENDIAN;
                }
            }
            else
            {
                m_byte_order = FrameCPP::HostByteOrder;
            }
            m_frame_library = (frame_library_type)m_data.raw.frameLibrary;
        }

        FrHeader::FrHeader( std::istream& Source )
            : FrameSpec::Object( s_object_id,
                                 (const Common::Description*)NULL,
                                 DATA_FORMAT_VERSION ),
              m_checksum( 0 )
        {
            std::istream::char_type* d =
                reinterpret_cast< std::istream::char_type* >( &( m_data.raw ) );
            Source.read( d, sizeof( m_data.raw ) );
            //---------------------------------------------------------------------
            // Figure out how to order the bytes
            //---------------------------------------------------------------------
            {
                INT_2U x;
                char*  xptr = reinterpret_cast< char* >( &x );
                std::copy( m_data.raw.x1234,
                           &m_data.raw.x1234[ sizeof( m_data.raw.x1234 ) ],
                           xptr );
                m_reorder_bytes = ( x1234 != x );
            }
            if ( m_reorder_bytes )
            {
                if ( FrameCPP::HostByteOrder ==
                     FrameCPP::BYTE_ORDER_BIG_ENDIAN )
                {
                    m_byte_order = FrameCPP::BYTE_ORDER_LITTLE_ENDIAN;
                }
                else
                {
                    m_byte_order = FrameCPP::BYTE_ORDER_BIG_ENDIAN;
                }
            }
            else
            {
                m_byte_order = FrameCPP::HostByteOrder;
            }
            m_frame_library = (frame_library_type)m_data.raw.frameLibrary;
        }

        FrHeader::FrHeader( const FrHeader& Source )
            : FrameSpec::Object( s_object_id,
                                 (const Common::Description*)NULL,
                                 DATA_FORMAT_VERSION ),
              Common::FrHeader( Source ), m_checksum( Source.m_checksum )
        {
            const CHAR_U* s =
                reinterpret_cast< const CHAR_U* >( &( Source.m_data.raw ) );
            CHAR_U* d = reinterpret_cast< CHAR_U* >( &( m_data.raw ) );
            std::copy( s, s + sizeof( Source.m_data.raw ), d );
        }

        FrHeader::~FrHeader( )
        {
        }

        FrameSpec::size_type
        FrHeader::Bytes( ) const
        {
            return Common::FrHeader::Bytes( ) + sizeof( m_data.raw );
        }

        bool
        FrHeader::ByteSwapping( ) const
        {
            return m_reorder_bytes;
        }

        FrameSpec::size_type
        FrHeader::Bytes( const Common::StreamBase& /* Stream */ ) const
        {
            return Bytes( );
        }

        FrHeader*
        FrHeader::Clone( ) const
        {
            return new FrHeader( *this );
        }

        const FrameSpec::Object*
        FrHeader::Description( ) const
        {
            return (const FrameSpec::Object*)NULL;
        }

        FrHeader*
        FrHeader::Create( istream_type& Stream ) const
        {
            return new FrHeader( Stream );
        }

        FrHeader*
        FrHeader::Create( std::istream& Stream ) const
        {
            return new FrHeader( Stream );
        }

        const char*
        FrHeader::ObjectStructName( ) const
        {
            static const char* name = "FrHeader";
            return name;
        }

        void
        FrHeader::VerifyObject( Common::Verify&       Verifier,
                                Common::IFrameStream& Stream ) const
        {
            Verifier.ExamineMagicNumber< 2 >( m_data.raw.x1234 );
            Verifier.ExamineMagicNumber< 4 >( m_data.raw.x12345678 );
            Verifier.ExamineMagicNumber< 8 >( m_data.raw.x123456789abcdef );
            Verifier.ChecksumScheme( m_data.raw.frameChecksumScheme );
            Verifier.ChecksumHeader( m_checksum_value );
#if 0
      if ( Verifier.CheckFileChecksum( ) )
      {
	Stream.SetCheckSumFile( (Common::CheckSum::kind_type)(m_data.raw.frameChecksumScheme) );
      }
#endif /* 0 */
        }

        void
        FrHeader::Write( ostream_type& fs ) const
        {
            FrHeader_type data;

            //-----------------------------------------------------------------
            // Check to see if any type of checksum is being calculated
            // for the file.
            //-----------------------------------------------------------------
            Common::CheckSum::kind_type cstype = Common::CheckSum::NONE;

            if ( fs.GetCheckSumFile( ) )
            {
                //---------------------------------------------------------------
                // The checksum of the FrHeader structure needs to be of the
                // same type as the File checksum.
                //---------------------------------------------------------------
                cstype = fs.GetCheckSumFile( )->GetChecksum( )->GetType( );
            }
            fs.SetCheckSumObject( cstype );

            //-----------------------------------------------------------------
            // Write the information common to all frame specifications
            //-----------------------------------------------------------------
            Common::FrHeader::Write( fs );
            //-----------------------------------------------------------------
            // Write the information that is specific to this frame spec.
            //-----------------------------------------------------------------
            // Supply the system specific info for the header
            data.raw.sizeInt2 = (unsigned char)sizeof( INT_2U );
            data.raw.sizeInt4 = (unsigned char)sizeof( INT_4U );
            data.raw.sizeInt8 = (unsigned char)sizeof( INT_8U );
            data.raw.sizeReal4 = (unsigned char)sizeof( REAL_4 );
            data.raw.sizeReal8 = (unsigned char)sizeof( REAL_8 );
            memcpy( data.raw.x1234, &x1234, sizeof( x1234 ) );
            memcpy( data.raw.x12345678, &x12345678, sizeof( x12345678 ) );
            memcpy( data.raw.x123456789abcdef,
                    &x123456789abcdef,
                    sizeof( x123456789abcdef ) );
            memcpy( data.raw.smallPi, &pi_REAL_4, sizeof( pi_REAL_4 ) );
            memcpy( data.raw.bigPi, &pi_REAL_8, sizeof( pi_REAL_8 ) );
            data.raw.frameLibrary = FRAME_LIBRARY_FRAMECPP;
            data.raw.frameChecksumScheme = cstype;
            // Write the header to the stream
            fs.write(
                reinterpret_cast< Common::OStream::char_type* >( &data.raw ),
                std::streamsize( sizeof( data.raw ) ) );

            //-----------------------------------------------------------------
            // Store the
            //-----------------------------------------------------------------
            if ( fs.GetCheckSumObject( ) )
            {
                m_checksum = fs.GetCheckSumObject( )->Value( );
#if LM_DEBUG_OUTPUT
                std::cerr << "DEBUG: FrHeader:"
                          << " m_checksum: " << m_checksum
                          << " cstype: " << cstype << std::endl;
#endif /* LM_DEBUG_OUTPUT */
            }
        }

        bool
        FrHeader::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            /// \todo
            ///   actually implement comparision operator
            throw std::runtime_error(
                "Comparison operator not supported for FrHeader" );
        }

        void
        FrHeader::complete( )
        {
            try
            {
                Common::CheckSumFilter csf( (Common::CheckSum::kind_type)m_data
                                                .raw.frameChecksumScheme );

                Filter( csf );

                m_checksum_value = csf.Value( );
            }
            catch ( ... )
            {
            }
        }

        FrHeader::demote_ret_type
        FrHeader::demote( INT_2U              Target,
                          demote_arg_type     Obj,
                          demote_stream_type* Stream ) const
        {
            throw Unimplemented( "Object* FrHeader::demote( ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrHeader::promote_ret_type
        FrHeader::promote( INT_2U               Target,
                           promote_arg_type     Obj,
                           promote_stream_type* Stream ) const
        {
            throw Unimplemented( "Object* FrHeader::promote( ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

    } // namespace Version_9
} // namespace FrameCPP
