//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2019 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_9__IMPL__FrTOCDataClassicIO_HH
#define FrameCPP_VERSION_9__IMPL__FrTOCDataClassicIO_HH

#include "framecpp/storage/data/v9/FrTOCHeader.hh"

#if defined( __cplusplus )
#if !defined( SWIG )

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/FrameSpec.tcc"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version8/FrTOC.hh"

#include "framecpp/Version9/FrameSpec.hh"
#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/PTR_STRUCT.hh"
#include "framecpp/Version9/STRING.hh"

namespace FrameCPP
{
    namespace Version_9
    {
        namespace FrTOCImpl
        {
            class FrTOCDataClassicIO : public virtual v9::FrTOCHeader
            {
            public:
                FrTOCDataClassicIO( ) = default;

                Common::FrameSpec::size_type
                Bytes( const Common::StreamBase& Stream ) const
                {
                    Common::FrameSpec::size_type retval = 0;

                    //-----------------------------------------------------------------
                    // main header block
                    //-----------------------------------------------------------------
                    retval += STRING::Bytes( fileBaseName ) +
                        sizeof( nFrame_type ) +
                        ( m_dataQuality.size( ) *
                          ( sizeof( data_quality_type ) +
                            sizeof( gtimes_type ) + sizeof( gtimen_type ) +
                            sizeof( dt_type ) + sizeof( positionh_type ) ) );
                    //-----------------------------------------------------------------
                    // SH elements
                    //-----------------------------------------------------------------
                    retval += sizeof( nsh_type ) +
                        ( sizeof( shid_type ) * m_SHid.size( ) );
                    for ( shname_container_type::const_iterator
                              cur = m_SHname.begin( ),
                              last = m_SHname.end( );
                          cur != last;
                          ++cur )
                    {
                        retval += STRING::Bytes( *cur );
                    }
                    //-----------------------------------------------------------------
                    // Detector information
                    //-----------------------------------------------------------------
                    retval += sizeof( ndetector_type ) +
                        ( sizeof( positiondetector_type ) *
                          m_positionDetector.size( ) );
                    for ( namedetector_container_type::const_iterator
                              cur = m_nameDetector.begin( ),
                              last = m_nameDetector.end( );
                          cur != last;
                          ++cur )
                    {
                        retval += STRING::Bytes( *cur );
                    }
                    //-----------------------------------------------------------------
                    // Return the results
                    //-----------------------------------------------------------------
                    return retval;
                }

#if !defined( SWIG )
                //-----------------------------------------------------------------
                /// \brief asignment operator
                ///
                /// \param[in] Source
                ///     The source to be copied.
                //-----------------------------------------------------------------
                const FrTOCDataClassicIO&
                operator=(
                    const Previous::FrTOCImpl::FrTOCDataClassicIO& Source )
                {
                    m_dataQuality = Source.GetDataQuality( );
                    m_GTimeS = Source.GetGTimeS( );
                    m_GTimeN = Source.GetGTimeN( );
                    m_dt = Source.GetDt( );
                    m_positionH = Source.GetPositionH( );

                    m_SHid = Source.GetSHid( );
                    m_SHname.resize( Source.GetSHname( ).size( ) );
                    std::copy( Source.GetSHname( ).begin( ),
                               Source.GetSHname( ).end( ),
                               m_SHname.begin( ) );

                    m_nameDetector.resize( Source.GetNameDetector( ).size( ) );
                    std::copy( Source.GetNameDetector( ).begin( ),
                               Source.GetNameDetector( ).end( ),
                               m_nameDetector.begin( ) );
                    m_positionDetector = Source.GetPositionDetector( );

                    return *this;
                }
#endif /* ! defined(SWIG) */

                inline FrTOCDataClassicIO&
                operator=( FrTOCDataClassicIO&& TOC )
                {
                    static_cast< FrTOCHeader& >( *this ) =
                        static_cast< FrTOCHeader&& >( TOC );
                    return *this;
                }

                //-----------------------------------------------------------------
                /// \brief The description of structure
                ///
                /// \param[out] Desc
                ///     Storage for the description of the structure.
                ///
                /// \return
                ///     A Description object which describes this structure as
                ///     specified by the frame specification.
                //-----------------------------------------------------------------
                template < typename SE >
                static void
                Description( Common::Description& Desc )
                {
                    //-----------------------------------------------------------------
                    Desc( SE(
                        "fileBaseName",
                        "STRING",
                        "The name of the file at the time of creation"
                        " including its extension but excluding any"
                        " directory components. (ex: /a/b/c.gwf becomes c.gwf)"
                        " If there is no corresponding file with the stream,"
                        " the empty string will be stored." ) );
                    //-----------------------------------------------------------------
                    Desc( SE(
                        "nFrame", "INT_4U", "Number of frames in the file" ) );
                    Desc( SE( "dataQuality",
                              "INT_4U[nFrame]",
                              "Array of integer QA words from each FrameH"
                              " (size of nFrames)" ) );
                    Desc( SE(
                        "GTimeS",
                        "INT_4U[nFrame]",
                        "Array of integer GPS frae times (size of nFrames" ) );
                    Desc( SE( "GTimeN",
                              "INT_4U[nFrame]",
                              "Array of integer GPS residual nanoseconds for "
                              "the frame"
                              " (size of nFrame" ) );
                    Desc( SE( "dt",
                              "REAL_8[nFrame]",
                              "Array of frame durations in seconds( size of "
                              "nFrames)" ) );
                    Desc( SE( "positionH",
                              "INT_8U[nFrame]",
                              "Array of FrameH positions, in bytes, from the"
                              " beginning of file (size of nFrame)" ) );
                    //-----------------------------------------------------------------
                    Desc( SE( "nSH",
                              "INT_4U",
                              "Number of FrSH structures in the file" ) );
                    Desc( SE( "SHid",
                              "INT_2U[nSH]",
                              "Array of FrSH IDs (size of nSH)" ) );
                    Desc( SE( "SHname",
                              "STRING[nSH]",
                              "Array of FrSH names (size of nSH)" ) );
                    //-----------------------------------------------------------------
                    Desc( SE( "nDetector",
                              "INT_4U",
                              "Number of distinct types of FrDetector in the "
                              "file" ) );
                    Desc( SE( "nameDetector",
                              "STRING[nDetector]",
                              "Array of FrDetector names (size of nDetector)."
                              " They appear alphabetically" ) );
                    Desc( SE( "positionDetector",
                              "INT_8U[nDetector]",
                              "Array of FrDetector positions from the beginning"
                              " of file (size of nDetector)." ) );
                }

                //-----------------------------------------------------------------
                /// \brief Read contents from stream
                ///
                /// \param[in] Stream
                ///     The stream from which the object is being read.
                ///
                /// \return
                ///    A new instance of this object.
                //-----------------------------------------------------------------
                void
                Load( istream_type& Stream )
                {
                    nFrame_type nframe;

                    Common::FrTOC::string_stream_type::Read( Stream,
                                                             fileBaseName );
                    Stream >> nframe;
                    if ( nframe )
                    {
                        std::vector< io_shname_type >       io_SHname;
                        std::vector< io_namedetector_type > io_nameDetector;
                        m_dataQuality.resize( nframe );
                        m_GTimeS.resize( nframe );
                        m_GTimeN.resize( nframe );
                        m_dt.resize( nframe );
                        m_positionH.resize( nframe );
                        Stream >> m_dataQuality >> m_GTimeS >> m_GTimeN >>
                            m_dt >> m_positionH;
                    }
                    nsh_type nsh;
                    Stream >> nsh;
                    if ( nsh > 0 )
                    {
                        m_SHid.resize( nsh );
                        std::vector< io_shname_type > io_SHname( nsh );
                        Stream >> m_SHid >> io_SHname;
                        m_SHname.resize( nsh );
                        std::copy( io_SHname.begin( ),
                                   io_SHname.end( ),
                                   m_SHname.begin( ) );
                    }
                    ndetector_type ndetector;
                    Stream >> ndetector;
                    if ( ndetector > 0 )
                    {
                        std::vector< io_namedetector_type > io_nameDetector{
                            ndetector
                        };
                        m_positionDetector.resize( ndetector );
                        Stream >> io_nameDetector >> m_positionDetector;
                        m_nameDetector.resize( ndetector );
                        std::copy( io_nameDetector.begin( ),
                                   io_nameDetector.end( ),
                                   m_nameDetector.begin( ) );
                    }
                }

            protected:
                //-----------------------------------------------------------------
                /// \brief Iterate over contents.
                ///
                /// \param[in] Info
                ///     Specifies the type of information to be searched.
                ///
                /// \param[in] Action
                ///     Action to be taken for each piece of information found.
                //-----------------------------------------------------------------
                void
                forEach( Common::FrTOC::query_info_type Info,
                         Common::FrTOC::FunctionBase&   Action ) const
                {
                    switch ( Info )
                    {
                    case Common::FrTOC::TOC_DETECTOR:
                    {
                        try
                        {
                            Common::FrTOC::FunctionC& action(
                                dynamic_cast< Common::FrTOC::FunctionC& >(
                                    Action ) );

                            for ( namedetector_container_type::const_iterator
                                      cur = GetNameDetector( ).begin( ),
                                      last = GetNameDetector( ).end( );
                                  cur != last;
                                  ++cur )
                            {
                                try
                                {
                                    const Common::DetectorNames::info_type&
                                        info( DetectorNameTable.Detector(
                                            *cur ) );

                                    action( info.s_prefix[ 0 ] );
                                }
                                catch ( ... )
                                {
                                }
                            }
                        }
                        catch ( ... )
                        {
                            // Does not understand Action
                        }
                    }
                    break;
                    case Common::FrTOC::TOC_FR_STRUCTS:
                    {
                        try
                        {
                            Common::FrTOC::FunctionSI& action(
                                dynamic_cast< Common::FrTOC::FunctionSI& >(
                                    Action ) );
                            shid_container_type::const_iterator
                                cur_id = GetSHid( ).begin( ),
                                last_id = GetSHid( ).end( );
                            shname_container_type::const_iterator
                                cur_name = GetSHname( ).begin( ),
                                last_name = GetSHname( ).end( );
                            while ( ( cur_id != last_id ) &&
                                    ( cur_name != last_name ) )
                            {
                                action( *cur_name, *cur_id );
                                ++cur_name;
                                ++cur_id;
                            }
                        }
                        catch ( ... )
                        {
                        }
                    }
                    break;
                    default:
                        //---------------------------------------------------------------
                        // ignore all other requests
                        //---------------------------------------------------------------
                        break;
                    }
                }

                //-----------------------------------------------------------------
                /// \brief Write the structure to the stream
                ///
                /// \param[in] Stream
                ///     The output stream where the object is to be written.
                //-----------------------------------------------------------------
                void
                write( Common::OStream& Stream ) const
                {
                    std::vector< io_shname_type > io_shname( m_SHname.size( ) );
                    std::copy( m_SHname.begin( ),
                               m_SHname.end( ),
                               io_shname.begin( ) );
                    std::vector< io_namedetector_type > io_nameDetector(
                        m_nameDetector.size( ) );
                    std::copy( m_nameDetector.begin( ),
                               m_nameDetector.end( ),
                               io_nameDetector.begin( ) );

                    STRING::Write( Stream, fileBaseName );
                    Stream << nFrame_type( m_dataQuality.size( ) )
                           << m_dataQuality << m_GTimeS << m_GTimeN << m_dt
                           << m_positionH
                           << nsh_type( m_SHid.size( ) )
                           /* FrSH info */
                           << m_SHid
                           << io_shname
                           /* FrDetector info */
                           << ndetector_type( io_nameDetector.size( ) )
                           << io_nameDetector << m_positionDetector;
                }

            private:
                typedef STRING io_namedetector_type;
                typedef STRING io_shname_type;
            };

        }; // namespace FrTOCImpl

    } // namespace Version_9
} // namespace FrameCPP

#endif /* ! defined(SWIG) */
#endif /* defined( __cplusplus ) */

#endif /* FrameCPP_VERSION_9__IMPL__FrTOCDataClassicIO_HH */
