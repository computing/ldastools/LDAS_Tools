#include "framecpp/Version9/impl/FrDetectorData.hh"

template < typename L, typename R >
boost::bimap< L, R >
make_bimap(
    std::initializer_list< typename boost::bimap< L, R >::value_type > list )
{
    return boost::bimap< L, R >( list.begin( ), list.end( ) );
}

namespace FrameCPP
{
    namespace Version_9
    {
        namespace FrDetectorImpl
        {
            Data::name_map_type Data::name_map =
                make_bimap< Data::name_map_type::left_key_type,
                            Data::name_map_type::right_key_type >(
                    { { "ACIGA", DQO_ACIGA },
                      { "CIT_40", DQO_CIT_40 },
                      { "GEO_600", DQO_GEO_600 },
                      { "KAGRA", DQO_KAGRA },
                      { "LHO_4k", DQO_LHO_4K },
                      { "LIGO_India", DQO_LIGO_INDIA },
                      { "LLO_4k", DQO_LLO_4K },
                      { "TAMA_300", DQO_TAMA_300 },
                      { "VIRGO", DQO_VIRGO } } );

            Data::prefix_map_type Data::prefix_map =
                make_bimap< prefix_map_type::left_key_type,
                            prefix_map_type::right_key_type >(
                    { { "A1", DQO_LIGO_INDIA },
                      { "C1", DQO_CIT_40 },
                      { "G1", DQO_GEO_600 },
                      { "H1", DQO_LHO_4K },
                      { "K1", DQO_KAGRA },
                      { "L1", DQO_LLO_4K },
                      { "T1", DQO_TAMA_300 },
                      { "U1", DQO_ACIGA },
                      { "V1", DQO_VIRGO } } );

        } // namespace FrDetectorImpl
    } // namespace Version_9
} // namespace FrameCPP
