//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2019 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_9__IMPL__FrVectData_HH
#define FrameCPP_VERSION_9__IMPL__FrVectData_HH

#if !defined( SWIGIMPORTED )
#include <memory>
#include <vector>
#include <string>

#include <boost/shared_array.hpp>
#endif /* ! defined(SWIGIMPORTED) */

#if !defined( SWIGIMPORTED )
#include "ldastoolsal/types.hh"

#include "framecpp/Common/Compression.hh"
#include "framecpp/Common/FrVect.hh"

#include "framecpp/Version8/FrVect.hh"

#include "framecpp/Version9/impl/FrObjectMetaData.hh"

#include "framecpp/Version9/FrameSpec.hh"
#include "framecpp/Version9/Dimension.hh"
#include "framecpp/Version9/GPSTime.hh"
#endif /* ! defined(SWIGIMPORTED)*/

#undef FR_VECT_COMPRESS_TYPE
#undef FR_VECT_TYPE_TYPE
#undef FR_VECT_NDATA_TYPE
#undef FR_VECT_NBYTES_TYPE
#undef FR_VECT_NDIM_TYPE
#undef FR_VECT_COMPRESSION_LEVEL_TYPE

#define FR_VECT_COMPRESS_TYPE INT_2U
#define FR_VECT_TYPE_TYPE INT_2U
#define FR_VECT_NDATA_TYPE INT_8U
#define FR_VECT_NBYTES_TYPE INT_8U
#define FR_VECT_NDIM_TYPE INT_4U
#define FR_VECT_COMPRESSION_LEVEL_TYPE int

#undef FR_VECT_N_DATA_VALID
#define FR_VECT_N_DATA_VALID INT_8U

#undef FR_VECT_DATA_VALID_COMP_SCHEME
#define FR_VECT_DATA_VALID_COMP_SCHEME INT_2U

#undef FR_VECT_N_DATA_VALID_COMP_BYTES
#define FR_VECT_N_DATA_VALID_COMP_BYTES INT_8U

#if !defined( SWIGIMPORTED )
namespace FrameCPP
{
    namespace Version_9
    {
        using Previous::FrVectDataTypes;

        namespace FrVectImpl
        {
            class Data : public Impl::FrObjectMetaData< Data >,
                         public Common::FrVect,
                         public FrVectDataTypes
            {
            public:
                enum compression_scheme_type
                {
                //---------------------------------------------------------
                //---------------------------------------------------------

#if WORDS_BIGENDIAN
#define NATIVE_DECL( x ) x = BIGENDIAN_##x
#else
#define NATIVE_DECL( x ) x = LITTLEENDIAN_##x
#endif
#define CST_DECL( x, y )                                                       \
    BIGENDIAN_##x = y, LITTLEENDIAN_##x = ( y | 0x8000 ), NATIVE_DECL( x )

                    CST_DECL( RAW, 0x00 ),
                    CST_DECL( ZERO_SUPPRESS, 0x01 ),
                    CST_DECL( GZIP, 0x02 ),
                    CST_DECL( DIFF_GZIP, 0x04 ),
                    CST_DECL( ZSTD, 0x08 ),
                    CST_DECL( DIFF_ZSTD, 0x10 ),

                    //---------------------------------------------------------
                    // Declaration of meta modes
                    //---------------------------------------------------------
                    CST_DECL( ZERO_SUPPRESS_OTHERWISE_GZIP,
                              ( 0x0FFF & ( ZERO_SUPPRESS | GZIP ) ) ),
                    CST_DECL( ZERO_SUPPRESS_OTHERWISE_ZSTD,
                              ( 0x0FFF & ( ZERO_SUPPRESS | ZSTD ) ) ),
                    CST_DECL( BEST_COMPRESSION,
                              ( 0x0FFF &
                                ( ZERO_SUPPRESS | GZIP | DIFF_GZIP | ZSTD |
                                  DIFF_ZSTD ) ) ),
#undef CST_DECL
#undef NATIVE_DECL

#undef GENERIC
                    NONE = RAW
                };

                enum data_types_type
                {
                    FR_VECT_C = 0,
                    FR_VECT_2S = 1,
                    FR_VECT_8R = 2,
                    FR_VECT_4R = 3,
                    FR_VECT_4S = 4,
                    FR_VECT_8S = 5,
                    FR_VECT_8C = 6,
                    FR_VECT_16C = 7,
                    FR_VECT_STRING = 8,
                    FR_VECT_2U = 9,
                    FR_VECT_4U = 10,
                    FR_VECT_8U = 11,
                    FR_VECT_1U = 12
                };

                typedef std::string                    name_type;
                typedef std::string                    unit_y_type;
                typedef FR_VECT_COMPRESS_TYPE          compress_type;
                typedef FR_VECT_COMPRESSION_LEVEL_TYPE compression_level_type;
                typedef FR_VECT_TYPE_TYPE              type_type;
                typedef FR_VECT_NDATA_TYPE             nData_type;
                typedef FR_VECT_NBYTES_TYPE            nBytes_type;
                typedef FR_VECT_NDIM_TYPE              nDim_type;
                typedef boost::shared_array< CHAR_U >  data_type;
                typedef data_type::element_type        data_element_type;
                typedef data_element_type*             data_pointer_type;
                typedef const data_element_type*       data_const_pointer_type;
                typedef std::vector< Dimension >       dimension_container_type;
                typedef FR_VECT_N_DATA_VALID           n_data_valid_type;
                typedef FR_VECT_DATA_VALID_COMP_SCHEME
                    data_valid_comp_scheme_type;
                typedef FR_VECT_N_DATA_VALID_COMP_BYTES
                                                      n_data_valid_comp_bytes_type;
                typedef boost::shared_array< CHAR_U > data_valid_type;

                typedef INT_4U nData_v3_type;
                typedef INT_4U nBytes_v3_type;

                static const constexpr int DEFAULT_GZIP_LEVEL = 6;

                virtual ~Data( )
                {
                }

                virtual Compression::compression_base_type
                Compression( ) const
                {
                    return ( 0x0FF & m_data.compress );
                }

                virtual void
                CompressData( INT_4U Scheme, INT_2U Level )
                {
                    Compress( compression_scheme_type( Scheme ), Level );
                }

                //-----------------------------------------------------------------
                /// \brief Allocate a block of memory suitable for storing data.
                ///
                /// \param[in] Type
                ///     The type of data that is to be stored in the buffer.
                /// \param[in] Dims
                ///     Information concearning each dimension of the array
                ///     of data to be stored in the buffer.
                /// \param[in] NDim
                ///     The number of dimensions of data to be stored in the
                ///     buffer
                ///
                /// \return
                ///     Upon successful allocation, an appropriated sized buffer
                ///     is returned.
                ///     An expection is thrown for all error conditions.
                //-----------------------------------------------------------------
                static data_type
                DataAlloc( type_type        Type,
                           const Dimension* Dims,
                           const nDim_type  NDim = 1 )
                {
                    nData_type dsize = 1;

                    for ( nDim_type x = 0; x < NDim; ++x )
                    {
                        dsize *= Dims[ x ].GetNx( );
                    }
                    return DataAlloc( Type, dsize );
                }

                //-----------------------------------------------------------------
                /// \brief Allocate a block of memory suitable for storing data.
                ///
                /// \param[in] Type
                ///     The type of data that is to be stored in the buffer.
                /// \param[in] Dims
                ///     Information concearning each dimension of the array
                ///     of data to be stored in the buffer.
                /// \param[in] NDim
                ///     The number of dimensions of data to be stored in the
                ///     buffer
                ///
                /// \return
                ///     Upon successful allocation, an appropriated sized buffer
                ///     is returned.
                ///     An expection is thrown for all error conditions.
                //-----------------------------------------------------------------
                static data_type
                DataAlloc( type_type Type, nData_type NData )
                {
                    data_type retval;

                    switch ( Type )
                    {
                    case FR_VECT_C:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new CHAR[ NData ] ) );
                        break;
                    case FR_VECT_1U:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new CHAR_U[ NData ] ) );
                        break;
                    case FR_VECT_2S:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new INT_2S[ NData ] ) );
                        break;
                    case FR_VECT_2U:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new INT_2U[ NData ] ) );
                        break;
                    case FR_VECT_4S:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new INT_4S[ NData ] ) );
                        break;
                    case FR_VECT_4U:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new INT_4U[ NData ] ) );
                        break;
                    case FR_VECT_8S:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new INT_8S[ NData ] ) );
                        break;
                    case FR_VECT_8U:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new INT_8U[ NData ] ) );
                        break;
                    case FR_VECT_4R:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new REAL_4[ NData ] ) );
                        break;
                    case FR_VECT_8R:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new REAL_8[ NData ] ) );
                        break;
                    case FR_VECT_8C:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new REAL_4[ NData * 2 ] ) );
                        break;
                    case FR_VECT_16C:
                        retval.reset( reinterpret_cast< data_pointer_type >(
                            new REAL_8[ NData * 2 ] ) );
                        break;
                        /*
                          ????
                          case FR_VECT_STR:
                          return sizeof( std::std::string );
                          ????
                        */
                    }

                    return retval;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the channel name.
                ///
                /// \return
                ///     The channel name
                //-----------------------------------------------------------------
                const name_type&
                GetName( ) const
                {
                    return m_data.name;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the compression algorithm number.
                ///
                /// \return
                ///     The compression algorithm number.
                //-----------------------------------------------------------------
                compress_type
                GetCompress( ) const
                {
                    return m_data.compress;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the vector class.
                ///
                /// \return
                ///     The vector class.
                //-----------------------------------------------------------------
                type_type
                GetType( ) const
                {
                    return m_data.type;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------
                size_t
                GetTypeSize( ) const
                {
                    return GetTypeSize( m_data.type );
                }

                //-----------------------------------------------------------------
                /// \brief Get size of single data point of particular size.
                ///
                /// \param[in] Type
                ///     Data type ID.
                ///
                /// \return
                ///     Data size.
                //-----------------------------------------------------------------
                static size_t
                GetTypeSize( type_type Type )
                {
                    switch ( Type )
                    {
                    case FR_VECT_C:
                        return sizeof( CHAR );
                    case FR_VECT_2S:
                        return sizeof( INT_2S );
                    case FR_VECT_8R:
                        return sizeof( REAL_8 );
                    case FR_VECT_4R:
                        return sizeof( REAL_4 );
                    case FR_VECT_4S:
                        return sizeof( INT_4S );
                    case FR_VECT_8S:
                        return sizeof( INT_8S );
                    case FR_VECT_8C:
                        return ( sizeof( REAL_4 ) * 2 );
                    case FR_VECT_16C:
                        return ( sizeof( REAL_8 ) * 2 );
                        /*
                          ????
                          case FR_VECT_STR:
                          return sizeof( std::std::string );
                          ????
                        */
                    case FR_VECT_2U:
                        return sizeof( INT_2U );
                    case FR_VECT_4U:
                        return sizeof( INT_4U );
                    case FR_VECT_8U:
                        return sizeof( INT_8U );
                    case FR_VECT_1U:
                        return sizeof( CHAR_U );
                    }
                    // None of above: Perhaps add type for throw?
                    return 0;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the number of bytes in the compressed
                /// vector.
                ///
                /// \return
                ///     The number of bytes in the compressed vector.
                //-----------------------------------------------------------------
                nBytes_type
                GetNBytes( ) const
                {
                    return m_data.nBytes;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the number of sample elements in data
                /// series.
                ///
                /// \return
                ///     The number of sample elements in data series.
                //-----------------------------------------------------------------
                nData_type
                GetNData( ) const
                {
                    return m_data.nData;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the number of sample elements in data valid
                /// series.
                ///
                /// \return
                ///     The number of sample elements in data series.
                //-----------------------------------------------------------------
                nData_type
                GetNDataValid( ) const
                {
                    return m_data.nDataValid;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the number of bytes in the compressed
                /// dataValid vector.
                ///
                /// \return
                ///     The number of bytes in the compressed dataValid vector.
                //-----------------------------------------------------------------
                nBytes_type
                GetNDataValidBytes( ) const
                {
                    return m_data.nDataValidCompBytes;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the pointer to the data.
                ///
                /// Return a pointer to the data being managed by the FrVect
                /// structure.
                ///
                /// \return
                ///     The pointer to the data.
                ///
                /// \note
                ///     This will expand the data if it is currently compressed.
                ///
                /// \see GetDataRaw, GetDataUncompressed
                //-----------------------------------------------------------------
                data_type
                GetData( ) const
                {
                    return GetDataRaw( );
                }

                //-----------------------------------------------------------------
                /// @brief Retrieve the pointer to the data.
                ///
                /// Return a pointer to the data valid being managed by the
                /// FrVect structure.
                ///
                /// @return
                ///     The pointer to the data valid.
                ///
                /// @note
                ///     This will expand the data valid if it is currently
                ///     compressed.
                ///
                /// @see GetDataValidRaw
                //-----------------------------------------------------------------
                data_valid_type
                GetDataValid( )
                {
                    uncompress_block( m_data.dataValidCompScheme,
                                      m_data.nDataValidCompBytes,
                                      m_data.dataValid );
                    return m_data.dataValid;
                }

                //-----------------------------------------------------------------
                /// @brief Retrieve the pointer to the data.
                ///
                /// Return a pointer to the data valid being managed by the
                /// FrVect structure.
                ///
                /// @return
                ///     The pointer to the data valid.
                ///
                /// @see GetDataValid
                //-----------------------------------------------------------------
                data_valid_type
                GetDataValidRaw( )
                {
                    return m_data.dataValid;
                }

                //-----------------------------------------------------------------
                /// @brief Retrieve the pointer to the data.
                ///
                /// Return a pointer to the data valid being managed by the
                /// FrVect structure.
                ///
                /// @return
                ///     The pointer to the data valid.
                ///
                /// @note
                ///     This will expand the data valid if it is currently
                ///     compressed.
                ///
                /// @see GetDataValidRaw
                //-----------------------------------------------------------------
                const CHAR_U*
                GetDataValidUncompressed( data_valid_type& Expanded )
                {
                    n_data_valid_type expanded_size;

                    Common::FrVect::expandToBuffer(
                        compress_type_map( ),
                        data_type_map( ),
                        FR_VECT_1U,
                        GetNDataValid( ),
                        GetDataValidRaw( ).get( ),
                        GetNDataValidBytes( ),
                        GetDataValidCompress( ),
                        ( ( GetDataValidCompress( ) & 0x8000 )
                              ? BYTE_ORDER_LITTLE_ENDIAN
                              : BYTE_ORDER_BIG_ENDIAN ),
                        Expanded,
                        expanded_size );
                    return Expanded.get( );
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the dataValid compression algorithm number.
                ///
                /// \return
                ///     The compression algorithm number.
                //-----------------------------------------------------------------
                compress_type
                GetDataValidCompress( ) const
                {
                    return m_data.dataValidCompScheme;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the pointer to the compressed data.
                ///
                /// \return
                ///     The pointer to the compressed data.
                //-----------------------------------------------------------------
                data_type
                GetDataRaw( ) const
                {
                    return m_data.data;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the pointer to the compressed data.
                ///
                /// \return
                ///     The pointer to the compressed data.
                //-----------------------------------------------------------------
                data_type
                GetDataRaw( )
                {
                    return m_data.data;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the pointer to the uncompressed data.
                ///
                /// \return
                ///     The pointer to the uncompressed data.
                //-----------------------------------------------------------------
                data_type
                GetDataUncompressed( )
                {
                    Uncompress( );

                    return m_data.data;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the pointer to the uncompressed data.
                ///
                /// \param[in] Expanded
                ///     Buffer into which the data is expanded.
                ///
                /// \return
                ///     The pointer to the uncompressed data.
                //-----------------------------------------------------------------
                const CHAR_U*
                GetDataUncompressed( data_type& Expanded ) const
                {
                    nBytes_type nBytes( 0 );

                    expandToBuffer( Expanded, nBytes );
                    if ( nBytes == 0 )
                    {
                        return m_data.data.get( );
                    }

                    return Expanded.get( );
                }

                template < class T >
                static type_type GetDataType( );

                //-----------------------------------------------------------------
                /// \brief Retrieve the number of dimensions
                ///
                /// \return
                ///     The number of dimensions
                //-----------------------------------------------------------------
                nDim_type
                GetNDim( ) const
                {
                    return m_data.dimension.size( );
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the Nth dimension
                ///
                /// \param[in] Offset
                ///     Index for desired dimension
                ///
                /// \return
                ///     The Nth dimension
                //-----------------------------------------------------------------
                const Dimension&
                GetDim( nDim_type Offset ) const
                {
                    /// \todo
                    ///   Throw exception if value out of range
                    return m_data.dimension[ Offset ];
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the Nth dimension
                ///
                /// \param[in] Offset
                ///     Index for desired dimension
                ///
                /// \return
                ///     The Nth dimension
                //-----------------------------------------------------------------
                Dimension&
                GetDim( nDim_type Offset )
                {
                    /// \todo
                    ///   Throw exception if value out of range
                    return m_data.dimension[ Offset ];
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve all the dimension information
                ///
                /// \return
                ///     The colletion of dimension data
                //-----------------------------------------------------------------
                dimension_container_type&
                GetDims( )
                {
                    return m_data.dimension;
                }

                //-----------------------------------------------------------------
                /// \brief Retrieve the description of how to interpret each
                /// element
                ///
                /// \return
                ///     The description of how to interpret each element
                //-----------------------------------------------------------------
                const unit_y_type&
                GetUnitY( ) const
                {
                    return m_data.unitY;
                }

                void
                Compress( compression_scheme_type Scheme, int Level )
                {
                    if ( Scheme == GetCompress( ) )
                    {
                        // Nothing to do. Just return
                        return;
                    }

                    //---------------------------------------------------------------------
                    // Uncompress the data first
                    //---------------------------------------------------------------------
                    Uncompress( );

                    compress_block( GetType( ),
                                    Scheme,
                                    Level,
                                    m_data.nData,
                                    m_data.nBytes,
                                    m_data.data,
                                    m_data.compress );
                }

                //-----------------------------------------------------------------------------
                /// \brief Expand compressed data.
                ///
                /// In general, uncompressing follows three steps:
                ///
                /// 1) Gunzip the data.
                /// 2) Fix byte-ordering.
                /// 3) Integrate.
                ///
                /// Depending upon the compression type and byte-ordering
                /// differences, not all of these steps are needed.
                void
                Uncompress( )
                {
                    uncompress_block(
                        m_data.compress, m_data.nBytes, m_data.data );
                }

                //-----------------------------------------------------------------
                /// \brief Set the current FrVect with the MIME data
                //-----------------------------------------------------------------
                void
                MimeData( const std::string& MimeType,
                          void*              Data,
                          nBytes_type        DataSize )
                {
                    //---------------------------------------------------------------------
                    // Setup dimension information
                    //---------------------------------------------------------------------
                    SetUnitY( MIME_TYPE_KEYWORD );
                    GetDims( ).erase( GetDims( ).begin( ), GetDims( ).end( ) );
                    GetDims( ).push_back( Dimension( MIME_TYPE_NX,
                                                     MIME_TYPE_DX,
                                                     MimeType,
                                                     MIME_TYPE_START_X ) );
                    //---------------------------------------------------------------------
                    // Fill data field with the blob
                    //---------------------------------------------------------------------
                    data_copy( (data_element_type*)Data, DataSize );
                }

                //-----------------------------------------------------------------
                /// \brief Establish the channel name.
                ///
                /// Assign a new value for the channel name.
                ///
                /// \param[in] Name
                ///     The channel name
                //-----------------------------------------------------------------
                void
                SetName( const name_type& Name )
                {
                    m_data.name = Name;
                }

                //-----------------------------------------------------------------
                /// \brief Set the description of how to interpret each element
                ///
                /// Assign a new value for the channel name.
                ///
                /// \param[in] UnitY
                ///     New description
                ///
                /// \return
                ///     The description of how to interpret each element
                //-----------------------------------------------------------------
                void
                SetUnitY( const unit_y_type& UnitY )
                {
                    m_data.unitY = UnitY;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------
                void
                SetNData( nData_type NData )
                {
                    m_data.nData = NData;
                }

                //-----------------------------------------------------------------
                /// \brief Store data valid information
                ///
                /// \param[in]
                //-----------------------------------------------------------------
                void
                SetDataValid( const CHAR_U*               DataValid,
                              n_data_valid_type           NDataValid,
                              data_valid_comp_scheme_type Compression = RAW )
                {
                    m_data.nDataValid = NDataValid;
                    m_data.nDataValidCompBytes = NDataValid;
                    m_data.dataValidCompScheme = RAW;
                    if ( m_data.nDataValid == 0 )
                    {
                        m_data.dataValid.reset( );
                        return;
                    }
                    if ( Compression == RAW )
                    {
                        m_data.dataValid.reset(
                            new CHAR_U[ m_data.nDataValidCompBytes ] );
                        std::copy( DataValid,
                                   DataValid + m_data.nDataValidCompBytes,
                                   m_data.dataValid.get( ) );
                    }
                    else
                    {
                        compress_block( FR_VECT_4U,
                                        Compression,
                                        DEFAULT_GZIP_LEVEL,
                                        m_data.nDataValid,
                                        m_data.nDataValidCompBytes,
                                        m_data.dataValid,
                                        m_data.dataValidCompScheme );
                    }
                }

            protected:
                enum
                {
                    MODE_RAW = BIGENDIAN_RAW,
                    MODE_GZIP = BIGENDIAN_GZIP,
                    MODE_DIFF_GZIP = BIGENDIAN_DIFF_GZIP,
                    MODE_ZERO_SUPPRESS = BIGENDIAN_ZERO_SUPPRESS,
                    MODE_ZSTD = BIGENDIAN_ZSTD,
                    MODE_DIFF_ZSTD = BIGENDIAN_DIFF_GZIP
                };

                enum
                {
                    MODE_BIGENDIAN = BIGENDIAN_RAW,
                    MODE_LITTLEENDIAN = LITTLEENDIAN_RAW,
                    MODE_HOSTENDIAN = RAW
                };

                //-------------------------------------------------------
                /// @brief Compress a block data
                ///
                /// @param[in] Scheme
                ///   Desired compression method
                /// @param[in,out] NData
                ///   Block of data to be compressed.
                ///   The compressed buffer will be stored in this
                ///   variable.
                /// @para[in,out] NBytes
                ///   On input, the number of bytes in NData.
                ///   On output, the number of bytes in the compressed
                ///   NData buffer.
                /// @param[in,out] Block,
                ///   Block of data to be compressed.
                ///   Upon output, the compressed form of the block
                /// @param[out] CompressionScheme
                ///   The compression scheme
                //-------------------------------------------------------
                template < typename NDATA_T,
                           typename NBYTES_T,
                           typename DATA_T >
                void
                compress_block( type_type      BlockType,
                                INT_4U         Scheme,
                                int            Level,
                                NDATA_T&       NData,
                                NBYTES_T&      NBytes,
                                DATA_T&        Block,
                                compress_type& CompressionScheme )
                {
                    //---------------------------------------------------------------------
                    // Compress the data
                    //---------------------------------------------------------------------

                    INT_4U                        compress = ( Scheme & 0xFF );
                    INT_4U                        retry_compress = 0;
                    const INT_8U                  nData( NData );
                    const INT_8U                  nBytes( NBytes );
                    boost::shared_array< CHAR_U > data_out( (CHAR_U*)NULL );
                    INT_8U                        data_out_len( 0 );

                    if ( ( compress & ZERO_SUPPRESS ) &&
                         ( ( BlockType != FR_VECT_C ) &&
                           ( BlockType != FR_VECT_1U ) &&
                           ( BlockType != FR_VECT_STRING ) ) )
                    {
                        //-----------------------------------------------
                        // Determine the retry scheme, if any
                        //-----------------------------------------------
                        if ( compress & GZIP )
                        {
                            retry_compress = ( GZIP & 0xFF );
                        }
                        else if ( compress & ZSTD )
                        {
                            retry_compress = ( ZSTD & 0xFF );
                        }
                        //-----------------------------------------------
                        // compress is set here since the origional
                        //   content is needed to determine the retry
                        //   scheme
                        //-----------------------------------------------
                        compress = ( ZERO_SUPPRESS & 0xFF );
                    }
                    else if ( compress ==
                              ( ZERO_SUPPRESS_OTHERWISE_GZIP & 0xFF ) )
                    {
                        compress = ( GZIP & 0xFF );
                    }
                    else if ( compress ==
                              ( ZERO_SUPPRESS_OTHERWISE_ZSTD & 0xFF ) )
                    {
                        compress = ( ZSTD & 0xFF );
                    }
                compression_retry:
                    FrameCPP::Compression::Compress(
                        compress,
                        Level,
                        compress_type_map( ),
                        compress_type_reverse_map( ),
                        BlockType,
                        data_type_map( ),
                        Block.get( ),
                        nData,
                        nBytes,
                        data_out,
                        data_out_len );

                    if ( data_out && data_out_len )
                    {
                        //-------------------------------------------------------------------
                        // Save the compressed data
                        //-------------------------------------------------------------------
                        NBytes = nBytes_type( data_out_len );
                        Block = data_out;
                        //-------------------------------------------------------------------
                        // Save the compression mode
                        //-------------------------------------------------------------------
                        CompressionScheme =
                            ( MODE_HOSTENDIAN | ( compress & 0xFF ) );
                    }
                    else
                    {
                        if ( retry_compress )
                        {
                            compress = retry_compress;
                            retry_compress = 0x00;
                            goto compression_retry;
                        }
                    }
                }

                template < typename DATA_T, typename NBYTES_T >
                inline void
                uncompress_block( compress_type& CompressionScheme,
                                  NBYTES_T&      NBytes,
                                  DATA_T&        Block )
                {
                    //---------------------------------------------------------------------
                    // Check if there is anything that needs to be done
                    //---------------------------------------------------------------------
                    if ( CompressionScheme == RAW )
                    {
                        // Nothing to do. The buffer is already uncompressed
                        return;
                    }

                    //---------------------------------------------------------------------
                    // There is something to be done
                    //---------------------------------------------------------------------
                    nBytes_type nBytes( NBytes );
                    data_type   expanded_buffer;

                    expandToBuffer( expanded_buffer, nBytes );

                    //---------------------------------------------------------------------
                    // Now record what has been done
                    //---------------------------------------------------------------------
                    if ( expanded_buffer.get( ) )
                    {
                        /* Transfer ownership */
                        Block = expanded_buffer;
                        NBytes = NBYTES_T( nBytes );
                        CompressionScheme = RAW;
                    }
                }

                static FrameCPP::Compression::compress_type_mapping&
                compress_type_map( )
                {
                    static FrameCPP::Compression::compress_type_mapping m;

                    if ( m.size( ) == 0 )
                    {

#define INIT( x, y ) m[ Data::BIGENDIAN_##x ] = FrameCPP::Compression::y

                        INIT( RAW, MODE_RAW );
                        INIT( GZIP, MODE_GZIP );
                        INIT( DIFF_GZIP, MODE_DIFF_GZIP );
                        INIT( ZERO_SUPPRESS, MODE_ZERO_SUPPRESS );
                        INIT( ZERO_SUPPRESS_OTHERWISE_GZIP,
                              MODE_ZERO_SUPPRESS_2_4_8_OTHERWISE_GZIP );
                    }

#undef INIT
                    return m;
                }

                static FrameCPP::Compression::compress_type_reverse_mapping&
                compress_type_reverse_map( )
                {
                    static FrameCPP::Compression::compress_type_reverse_mapping
                        m;

                    if ( m.size( ) == 0 )
                    {

#define INIT( x, y ) m[ FrameCPP::Compression::x ] = Data::BIGENDIAN_##y;

                        INIT( MODE_RAW, RAW );
                        INIT( MODE_GZIP, GZIP );
                        INIT( MODE_DIFF_GZIP, DIFF_GZIP );

                        INIT( MODE_ZERO_SUPPRESS, ZERO_SUPPRESS );
                        INIT( MODE_ZERO_SUPPRESS_SHORT, ZERO_SUPPRESS );
                        INIT( MODE_ZERO_SUPPRESS_INT_FLOAT, ZERO_SUPPRESS );
                        INIT( MODE_ZERO_SUPPRESS_WORD_4, ZERO_SUPPRESS );
                        INIT( MODE_ZERO_SUPPRESS_WORD_8, ZERO_SUPPRESS );

                        INIT( MODE_ZERO_SUPPRESS_2_OTHERWISE_GZIP,
                              ZERO_SUPPRESS_OTHERWISE_GZIP );
                        INIT( MODE_ZERO_SUPPRESS_2_4_OTHERWISE_GZIP,
                              ZERO_SUPPRESS_OTHERWISE_GZIP );
                        INIT( MODE_ZERO_SUPPRESS_2_4_8_OTHERWISE_GZIP,
                              ZERO_SUPPRESS_OTHERWISE_GZIP );

                        INIT( MODE_ZERO_SUPPRESS_2_OTHERWISE_ZSTD,
                              ZERO_SUPPRESS_OTHERWISE_ZSTD );
                        INIT( MODE_ZERO_SUPPRESS_2_4_OTHERWISE_ZSTD,
                              ZERO_SUPPRESS_OTHERWISE_ZSTD );
                        INIT( MODE_ZERO_SUPPRESS_2_4_8_OTHERWISE_ZSTD,
                              ZERO_SUPPRESS_OTHERWISE_ZSTD );
#undef INIT
                    }
                    return m;
                }

                void
                data_alloc( nBytes_type NBytes )
                {
                    m_data.data.reset( new CHAR_U[ NBytes ] );
                    m_data.nBytes = NBytes;
                }

                void
                data_copy( data_const_pointer_type Data, nBytes_type NBytes )
                {
                    if ( ( NBytes == 0 ) ||
                         ( Data == (const data_pointer_type)NULL ) )
                    {
                        m_data.data.reset( );
                    }
                    else
                    {
                        if ( ( m_data.nBytes != NBytes ) || ( !m_data.data ) )
                        {
                            m_data.data.reset( new CHAR_U[ NBytes ] );
                            m_data.nBytes = NBytes;
                        }
                        std::copy( Data, Data + NBytes, m_data.data.get( ) );
                    }
                }

                static FrameCPP::Compression::data_type_mapping&
                data_type_map( )
                {
                    static FrameCPP::Compression::data_type_mapping m;

                    if ( m.size( ) == 0 )
                    {
#define INIT( x ) m[ Data::x ] = FrameCPP::Compression::x

                        INIT( FR_VECT_C );
                        INIT( FR_VECT_2S );
                        INIT( FR_VECT_8R );
                        INIT( FR_VECT_4R );
                        INIT( FR_VECT_4S );
                        INIT( FR_VECT_8S );
                        INIT( FR_VECT_8C );
                        INIT( FR_VECT_16C );
                        INIT( FR_VECT_STRING );
                        INIT( FR_VECT_2U );
                        INIT( FR_VECT_4U );
                        INIT( FR_VECT_8U );
                        INIT( FR_VECT_1U );

#undef INIT
                    }
                    return m;
                }

                void
                expandToBuffer( data_type& Dest, nBytes_type& DestNBytes ) const
                {
                    Common::FrVect::expandToBuffer(
                        compress_type_map( ),
                        data_type_map( ),
                        GetType( ),
                        GetNData( ),
                        GetDataRaw( ).get( ),
                        GetNBytes( ),
                        GetCompress( ),
                        ( ( GetCompress( ) & 0x8000 ) ? BYTE_ORDER_LITTLE_ENDIAN
                                                      : BYTE_ORDER_BIG_ENDIAN ),
                        Dest,
                        DestNBytes );
                }

                struct data_definition_type
                {
                public:
                    //---------------------------------------------------------------
                    /// Channel name.
                    ///
                    /// \note
                    ///     Not required to be unique.
                    //---------------------------------------------------------------
                    name_type name;
                    //---------------------------------------------------------------
                    /// Compression algorithm number
                    //---------------------------------------------------------------
                    compress_type compress;
                    //---------------------------------------------------------------
                    /// Vector class
                    //---------------------------------------------------------------
                    type_type type;
                    //---------------------------------------------------------------
                    /// Number of sample elements in data series.
                    //---------------------------------------------------------------
                    nData_type nData;
                    //---------------------------------------------------------------
                    /// Number of bytes in the compressed vector.
                    //---------------------------------------------------------------
                    nBytes_type nBytes;
                    //---------------------------------------------------------------
                    /// Compressed data series.
                    //---------------------------------------------------------------
                    data_type data;
                    //---------------------------------------------------------------
                    /// Dimension data for data series.
                    //---------------------------------------------------------------
                    dimension_container_type dimension;
                    //---------------------------------------------------------------
                    /// String describing how to interpret the value of each
                    /// element.
                    //---------------------------------------------------------------
                    unit_y_type unitY;
                    //---------------------------------------------------------------
                    /// Number of bytes in the data valid vector.
                    //---------------------------------------------------------------
                    n_data_valid_type nDataValid = 0;
                    //---------------------------------------------------------------
                    /// Compression scheme used by dataValid vector
                    //---------------------------------------------------------------
                    data_valid_comp_scheme_type dataValidCompScheme = RAW;
                    //---------------------------------------------------------------
                    /// Number of bytes in the compressed data valid vector.
                    //---------------------------------------------------------------
                    n_data_valid_comp_bytes_type nDataValidCompBytes = 0;
                    //---------------------------------------------------------------
                    /// This is an array of dataValid value covering the span of
                    /// the FrVect.data. A value of 0 in teh array signifies the
                    /// corresponding block of data is valid. A non-zero value
                    /// in the array signifies data as suspect
                    //---------------------------------------------------------------
                    data_valid_type dataValid;

                    data_definition_type( )
                        : name( "" ), compress( RAW ), type( FR_VECT_C ),
                          nData( 0 ), nBytes( 0 ), dimension( ), unitY( "" )
                    {
                    }

                    data_definition_type( const std::string&    n,
                                          INT_2U                t,
                                          const byte_order_type byte_order,
                                          nDim_type             ndim,
                                          const Dimension*      dims,
                                          const std::string&    unit )
                    {
                        set( n, t, byte_order, ndim, dims, unit );
                    }

                    void
                    copy_core( const data_definition_type& Source )
                    {
                        name = Source.name;
                        compress = Source.compress;
                        type = Source.type;
                        nData = Source.nData;
                        nBytes = Source.nBytes;

                        for ( std::vector< Dimension >::const_iterator
                                  d( Source.dimension.begin( ) ),
                              d_end( Source.dimension.end( ) );
                              d != d_end;
                              ++d )
                        {
                            dimension.push_back( *d );
                        }

                        unitY = Source.unitY;

                        nDataValid = Source.nDataValid;
                        dataValidCompScheme = Source.dataValidCompScheme;
                        nDataValidCompBytes = Source.nDataValidCompBytes;
                        dataValid.reset( new CHAR_U[ nDataValidCompBytes ] );
                        std::copy( Source.dataValid.get( ),
                                   Source.dataValid.get( ) +
                                       nDataValidCompBytes,
                                   dataValid.get( ) );
                    }

                    static FrameCPP::cmn_streamsize_type
                    calc_nData( const std::vector< Dimension >& Dimensions )
                    {
                        FrameCPP::cmn_streamsize_type ret( 0 );

                        if ( Dimensions.size( ) > 0 )
                        {
                            ret = 1;
                            for ( std::vector< Dimension >::const_iterator i(
                                      Dimensions.begin( ) );
                                  i != Dimensions.end( );
                                  i++ )
                            {
                                ret *= ( *i ).GetNx( );
                            }
                        }
                        return ret;
                    }

                    void
                    set( const std::string&    n,
                         INT_2U                t,
                         const byte_order_type byte_order,
                         nDim_type             ndim,
                         const Dimension*      dims,
                         const std::string&    unit )
                    {
                        name = n;
                        type = t;
                        unitY = unit;
                        compress = RAW;

                        for ( nDim_type x = 0; x < ndim; ++x, ++dims )
                        {
                            dimension.push_back( *dims );
                        }

                        nData = calc_nData( dimension );
                        nBytes = GetTypeSize( type ) * nData;
                    }

                    bool
                    operator==( const data_definition_type& RHS ) const
                    {
#if LM_INFO
                        if ( name != RHS.name )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        if ( compress != RHS.compress )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        if ( type != RHS.type )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        if ( nData != RHS.nData )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        if ( nBytes != RHS.nBytes )
                        {
                            std::cerr << "DEBUG: " << __LINE__
                                      << " nBytes: " << nBytes
                                      << " RHS.nBytes: " << RHS.nBytes
                                      << std::endl;
                        }
                        if ( dimension != RHS.dimension )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        if ( unitY != RHS.unitY )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        if ( ( nBytes == RHS.nBytes ) &&
                             ( std::equal( data.get( ),
                                           data.get( ) + nBytes,
                                           RHS.data.get( ) ) == false ) )
                        {
                            std::cerr << "DEBUG: " << __LINE__ << std::endl;
                        }
                        std::cerr << "DEBUG: nData: " << nData << std::endl;
#endif
                        return ( ( this == &RHS ) ||
                                 ( ( name == RHS.name ) &&
                                   ( compress == RHS.compress ) &&
                                   ( type == RHS.type ) &&
                                   ( nData == RHS.nData ) &&
                                   ( nBytes == RHS.nBytes ) &&
                                   ( dimension == RHS.dimension ) &&
                                   ( unitY == RHS.unitY ) &&
                                   ( ( data == RHS.data ) ||
                                     ( std::equal( data.get( ),
                                                   data.get( ) + nBytes,
                                                   RHS.data.get( ) ) ) ) &&
                                   ( nDataValid == RHS.nDataValid ) &&
                                   ( dataValidCompScheme ==
                                     RHS.dataValidCompScheme ) &&
                                   ( nDataValidCompBytes ==
                                     RHS.nDataValidCompBytes ) &&
                                   ( ( dataValid == RHS.dataValid ) ||
                                     ( std::equal(
                                         dataValid.get( ),
                                         dataValid.get( ) + nDataValidCompBytes,
                                         RHS.dataValid.get( ) ) ) ) ) );
                    }
                };

                data_definition_type m_data;

            private:
                static const constexpr char* MIME_TYPE_KEYWORD = "MIMETYPE";
                static const constexpr FrameCPP::Version_9::Dimension::nx_type
                                       MIME_TYPE_NX = 0;
                static const constexpr FrameCPP::Version_9::Dimension::dx_type
                                       MIME_TYPE_DX = 0;
                static const constexpr
                        FrameCPP::Version_9::Dimension::startX_type
                        MIME_TYPE_START_X = 0;
            }; // namespace FrVectImpl
        } // namespace FrVectImpl
    } // namespace Version_9
} // namespace FrameCPP

FR_OBJECT_META_DATA_DECLARE_9( FrVectImpl )

#endif /* ! defined(SWIGIMPORTED) */

#endif /* FrameCPP_VERSION_9__IMPL__FrVectData_HH */
