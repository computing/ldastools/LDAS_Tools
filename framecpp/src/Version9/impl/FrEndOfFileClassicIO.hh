//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2019 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_9__IMPL__FrEndOfFileClassicIO_HH
#define FrameCPP_VERSION_9__IMPL__FrEndOfFileClassicIO_HH

#ifndef SWIGIMPORTED
#include <string>
#include <vector>
#endif /* SWIGIMPORTED */

#if !defined( SWIGIMPORTED )
#include "ldastoolsal/types.hh"

#include "framecpp/Version9/FrameSpec.hh"

#endif /* ! defined(SWIGIMPORTED) */

#if !defined( SWIGIMPORTED )
#include "framecpp/Common/Description.hh"

#include "framecpp/Common/FrEndOfFile.hh"

#include "framecpp/Version4/FrEndOfFile.hh"
#include "framecpp/Version8/FrEndOfFile.hh"
#endif /* !defined( SWIGIMPORTED ) */

#include "framecpp/Version9/impl/FrEndOfFileData.hh"

#if !defined( SWIGIMPORTED )
#include "framecpp/Version9/STRING.hh"

#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/FrSE.hh"
#endif /* ! defined(SWIGIMPORTED) */

#if !defined( SWIGIMPORTED )
namespace FrameCPP
{
    namespace Version_9
    {
        class FrEndOfFile;

        namespace FrEndOfFileImpl
        {
            template < typename T >
            class ClassicIO : public virtual Data, public Common::FrEndOfFile
            {
            public:
                //-------------------------------------------------------
                //
                //-------------------------------------------------------
                ClassicIO( )
                    : Common::FrEndOfFile( StructDescription( ),
                                           DATA_FORMAT_VERSION )
                {
                }

                //-----------------------------------------------------------------
                /// \brief The name structure name of this object.
                ///
                /// \return
                ///     The name of the structure as specified by the frame
                ///     specification.
                //-----------------------------------------------------------------
                static const char*
                StructName( )
                {
                    static const CHAR* class_name( "FrEndOfFile" );
                    return class_name;
                }

                //-----------------------------------------------------------------
                /// \brief Number of bytes needed to write this structure
                ///
                /// \param[in] Stream
                ///     The stream from which to the object is being read or
                ///     written.
                ///
                /// \return
                ///     The number of bytes need to read or write this object.
                //-----------------------------------------------------------------
                virtual Common::FrameSpec::size_type
                Bytes( const Common::StreamBase& Stream ) const
                {
                    auto frame_stream_version = StreamVersion( Stream );

                    return (
                        sizeof( nFrames ) +
                        ( ( frame_stream_version >=
                            5 ) // Added support for files > 4Gb
                              ? sizeof( nBytes )
                              : sizeof( nBytes_v3_type ) ) +
                        ( ( frame_stream_version >= 4 ) // Added TOC
                              ? sizeof( seekTOC )
                              : 0 ) + //
                        ( ( frame_stream_version >= 9 ) // Added in TOC checksum
                              ? sizeof( chkSumTOC )
                              : 0 ) +
                        ( ( frame_stream_version >= 8 ) // Added in Version 8
                              ? sizeof( chkSumFrHeader )
                              : 0 ) + //
                        ( ( frame_stream_version >= 8 ) // Added in Version 8
                              ? sizeof( chkSum )
                              : 0 ) + //
                        sizeof( chkSumFile ) );
                }
                //-----------------------------------------------------------------
                /// \brief The description of structure
                ///
                /// \return
                ///     A Description object which describes this structure as
                ///     specified by the frame specification.
                //-----------------------------------------------------------------
                static const Common::Description*
                StructDescription( )
                {
                    using Common::Description;

                    static Description ret;

                    if ( ret.size( ) == 0 )
                    {
                        ret( FrSH( "FrEndOfFile",
                                   FrEndOfFile::s_object_id,
                                   "End of File Data Structure" ) );
                        ret( FrSE( "nFrames",
                                   "INT_4U",
                                   "Number of frames in this file" ) );
                        ret( FrSE( "nBytes",
                                   "INT_8U",
                                   "Total number of bytes in this file"
                                   " ( 0 if NOT computed )" ) );
                        ret( FrSE(
                            "seekTOC",
                            "INT_8U",
                            "Byes to back up to the beginning of the table of"
                            " contents structure. If seekTOC == 0, then there"
                            " is no TOC for this file." ) );
                        ret( FrSE( "chkSumTOC",
                                   "INT_4U",
                                   "FrTOC uniqueness checksum" ) );
                        ret( FrSE( "chkSumFrHeader",
                                   "INT_4U",
                                   "FrHeader checksum." ) );
                        ret(
                            FrSE( "chkSum", "INT_4U", "Structure checksum." ) );
                        ret( FrSE( "chkSumFile", "INT_4U", "File checksum." ) );
                    }

                    return &ret;
                }

                //-----------------------------------------------------------------
                /// \brief Write the structure to the stream
                ///
                /// \param[in] Stream
                ///     The output stream where the object is to be written.
                //-----------------------------------------------------------------
                virtual void
                Write( ostream_type& Stream ) const
                {
                    //---------------------------------------------------------------------
                    // Get local information
                    //---------------------------------------------------------------------

                    const nFrames_type nFrames( Stream.GetNumberOfFrames( ) );
                    const nBytes_type  nBytes(
                        Stream.tellp( ) + std::streampos( Bytes( Stream ) ) );
                    const seekTOC_type seekTOC( ( Stream.GetTOCOffset( ) > 0 )
                                                    ? nBytes -
                                                        Stream.GetTOCOffset( )
                                                    : 0 );

                    chkSumTOC = Stream.GetTOCChecksum( );
                    chkSumFrHeader_type chkSumFrHeader =
                        getFrHeaderChecksum( Stream );
                    chkSum_type     chkSum = 0;
                    chkSumFile_type chkSumFile = 0;

                    //---------------------------------------------------------------------
                    // Write out to the stream
                    //---------------------------------------------------------------------
                    auto frame_stream_version = StreamVersion( Stream );

                    Stream << nFrames;
                    if ( frame_stream_version >= 5 )
                    {
                        Stream << nBytes;
                    }
                    else
                    {
                        Stream << nBytes_v3_type( nBytes );
                    }
                    if ( frame_stream_version >= 8 )
                    {
                        Stream << seekTOC;
                        if ( frame_stream_version >= 9 )
                        {
                            Stream << chkSumTOC;
                        }
                        Stream << chkSumFrHeader;
                        if ( Stream.GetCheckSumObject( ) &&
                             Stream.GetCheckSumObject( )->GetChecksum( ) )
                        {
                            chkSum = Stream.GetCheckSumObject( )->Value( );
                        }
                        Stream << chkSum;
                        if ( Stream.GetCheckSumFile( ) &&
                             Stream.GetCheckSumFile( )->GetChecksum( ) )
                        {
                            chkSumFile = Stream.GetCheckSumFile( )->Value( );
                        }
                        Stream << chkSumFile;
                    }
                    else // frame_stream_version < 8
                    {
                        chkType_v3_type chkType = CheckSum::NONE;
                        chkSumFile = 0;

                        CheckSumFilter* crc = Stream.GetCheckSumFile( );
                        if ( crc )
                        {
                            chkType = crc->Type( );
                            crc->Filter( &chkType, sizeof( chkType ) );
                            crc->Filter( &chkSum, sizeof( chkSum ) );
                            if ( frame_stream_version >= 4 )
                            {
                                if ( frame_stream_version >= 5 )
                                {
                                    crc->Filter( &seekTOC, sizeof( seekTOC ) );
                                }
                                else
                                {
                                    auto seekTOC_v4 =
                                        seekTOC_v4_type( seekTOC );
                                    crc->Filter( &seekTOC_v4,
                                                 sizeof( seekTOC_v4 ) );
                                }
                            }
                        }
                        if ( Stream.GetCheckSumFile( ) &&
                             Stream.GetCheckSumFile( )->GetChecksum( ) )
                        {
                            chkSumFile = Stream.GetCheckSumFile( )->Value( );
                        }
                        Stream << chkType << chkSumFile;
                        if ( frame_stream_version >= 4 )
                        {
                            if ( frame_stream_version >= 5 )
                            {
                                Stream << seekTOC;
                            }
                            else
                            {
                                Stream << seekTOC_v4_type( seekTOC );
                            }
                        }
                    }
                }

            protected:
                virtual void
                assign( assign_stream_type& Stream )
                {
                    //-----------------------------------------------------------------
                    // Read the data from the stream
                    //-----------------------------------------------------------------
                    Stream >> nFrames >> nBytes >> seekTOC >> chkSumFrHeader >>
                        chkSum >> chkSumFile;
                }

                //-----------------------------------------------------------------
                /// \brief Demotes object to previous version of the frame spec
                ///
                /// \param[in] Target
                ///     The version of the frame specification to demote too.
                /// \param[in] Obj
                ///     The version of the object to demote.
                /// \param[in] Stream
                ///     The input stream from which the original object was
                ///     read.
                ///
                /// \return
                ///     An object of the previous generation.
                //-----------------------------------------------------------------
                virtual demote_ret_type
                demote( INT_2U              Target,
                        demote_arg_type     Obj,
                        demote_stream_type* Stream ) const
                {
                    throw Unimplemented(
                        "Object* FrEndOfFile::demote( Object* Obj ) const",
                        DATA_FORMAT_VERSION,
                        __FILE__,
                        __LINE__ );
                }

                virtual chkSumFrHeader_type
                getFrHeaderChecksum( ostream_type& Stream ) const = 0;

                //-----------------------------------------------------------------
                /// \brief The name structure name of this object.
                ///
                /// \return
                ///     The name of the structure as specified by the frame
                ///     specification.
                //-----------------------------------------------------------------
                virtual const char*
                ObjectStructName( ) const
                {
                    return StructName( );
                }

                virtual const Common::Description*
                GetDescription( ostream_type& Stream ) const
                {
                    auto frame_stream_version = StreamVersion( Stream );

                    if ( frame_stream_version >= 9 )
                    {
                        return StructDescription( );
                    }
                    else if ( frame_stream_version >= 8 )
                    {
                        return Version_8::FrEndOfFile::StructDescription( );
                    }
                    else if ( frame_stream_version >= 5 )
                    {
                        return Version_6::FrEndOfFile::StructDescription( );
                    }
                    else if ( frame_stream_version >= 4 )
                    {
                        return Version_4::FrEndOfFile::StructDescription( );
                    }
                    else if ( frame_stream_version >= 3 )
                    {
                        return Version_3::FrEndOfFile::StructDescription( );
                    }
                    return StructDescription( );
                }

                //-----------------------------------------------------------------
                /// \brief Promotes object to another version of the frame spec
                ///
                /// \param[in] Target
                ///     The version of the promoted frame specification.
                /// \param[in] Obj
                ///     The object to be promoted.
                /// \param[in] Stream
                ///     The input stream from which the original object was
                ///     read.
                ///
                /// \return
                ///     An object promoted to the next generation.
                //-----------------------------------------------------------------
                virtual promote_ret_type
                promote( INT_2U               Target,
                         promote_arg_type     Obj,
                         promote_stream_type* Stream ) const
                {
                    throw Unimplemented(
                        "Object* FrEndOfFile::promote( Object* Obj ) const",
                        DATA_FORMAT_VERSION,
                        __FILE__,
                        __LINE__ );
                }
            };

        } // namespace FrEndOfFileImpl
    } // namespace Version_9
} // namespace FrameCPP
#endif /* ! defined(SWIGIMPORTED) */

#endif /* FrameCPP_VERSION_9__IMPL__FrEndOfFileClassicIO_HH */
