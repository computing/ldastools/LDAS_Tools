#ifndef LOGGER_OPTIONS_HH
#define LOGGER_OPTIONS_HH

#include <boost/program_options.hpp>

#include "SimpleLogger.hh"

/// @brief Class to manage logger options using boost::program_options.
///
/// This class provides an interface to configure logger settings such as debug
/// level, sink type, output directory for file sinks, format type, and minimum
/// severity level using command-line options.
///
/// ### Example Usage
/// @code
/// #include "ldastoolsal/Logger.hh"
/// #include "ldastoolsal/LoggerOptions.hh"
///
/// int main(int argc, char* argv[]) {
///     try {
///         LoggerOptions logger_options;
///
///         // Application options description
///         boost::program_options::options_description app_desc("Application
///         Options"); app_desc.add_options()
///             ("help,h", "Show help message");
///
///         // Extend with logger options
///         logger_options.extend_options(app_desc);
///
///         // Variables map to store parsed options
///         boost::program_options::variables_map vm;
///         boost::program_options::store(boost::program_options::parse_command_line(argc,
///         argv, app_desc), vm); boost::program_options::notify(vm);
///
///         // Show help message if --help is specified
///         if (vm.count("help")) {
///             std::cout << app_desc << std::endl;
///             return 0;
///         }
///
///         // Apply logger options
///         SimpleLogger* logger = default_logger();
///         logger_options.apply_options(vm, logger);
///
///         int variable = 42;
///
///         logger_log_debug(logger, 10, "Hello ", variable, " world");
///         logger_log_warning(logger, "This is a warning message.", variable);
///         logger_log_note(logger, "This is a note message.");
///         logger_log_error_non_fatal(logger, "This is a non-fatal error
///         message."); logger_log_error(logger, "This is a fatal error
///         message."); logger_log_email(logger, "This is an email alert.");
///         logger_log_phone(logger, "This is a phone alert.");
///     } catch (const std::exception& e) {
///         std::cerr << "Error: " << e.what() << std::endl;
///         return 1;
///     }
///
///     return 0;
/// }
/// @endcode
class LoggerOptions
{
public:
    /// @brief Constructor for LoggerOptions.
    LoggerOptions( );

    /// @brief Extend the program options description with logger options.
    /// @param desc Program options description
    void extend_options( boost::program_options::options_description& desc );

    /// @brief Apply the parsed options to the SimpleLogger.
    /// @param vm Variables map containing the parsed options
    /// @param logger Pointer to the SimpleLogger instance
    void apply_options( const boost::program_options::variables_map& vm,
                        SimpleLogger*                                logger );

private:
    boost::program_options::options_description
        desc_; ///< Options description for logger options

    /// @brief Convert severity level string to custom_severity_level.
    /// @param level Severity level string
    /// @return custom_severity_level
    SimpleLogger::custom_severity_level
    severity_from_string( const std::string& level );
};

#endif // LOGGER_OPTIONS_HH
