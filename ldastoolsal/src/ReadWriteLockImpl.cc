//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERAL__READ_WRITE_LOCK_IMPLE_CC
#define GENERAL__READ_WRITE_LOCK_IMPLE_CC

#include <ldastoolsal_config.h>

#include <boost/shared_ptr.hpp>

namespace LDASTools
{
    namespace AL
    {
#if DEPRICATED
        class ReadWriteLock::baton_type::impl
        {
        public:
#if HAVE_PTHREAD_RW_LOCK
            typedef pthread_rwlock_t handle_type;
#else /* HAVE_PTHREAD_RW_LOCK */
            typedef pthread_mutex_t handle_type;
#endif /* HAVE_PTHREAD_RW_LOCK */

            typedef ReadWriteLock::lock_mode  lock_mode;
            typedef ReadWriteLock::lock_state lock_state;

            struct info
            {
                inline info( ) : m_mode( NONE ), m_state( STATE_NONE )
                {
                }

                lock_mode  m_mode;
                lock_state m_state;
            };

            impl( ) : m_verbose_tracking( false )
            {
#if HAVE_PTHREAD_RW_LOCK
                pthread_rwlock_init(
                    &handle, static_cast< pthread_rwlockattr_t* >( NULL ) );
#else /* HAVE_PTHREAD_RW_LOCK */
                pthread_mutex_init(
                    &handle, static_cast< pthread_mutexattr_t* >( NULL ) );
#endif /* HAVE_PTHREAD_RW_LOCK */
            }
#if WORKING

            Sync( bool Verbose = true );
            Sync( lock_type& Source, bool Verbose = false );
            Sync( lock_type* Source, bool Verbose = false );
#endif /* WORKING */

            int Lock( lock_mode Mode, const char* Filename, int Linenum );

            int Lock( lock_mode   Mode,
                      bool        TryLock,
                      const char* Filename,
                      int         Linenum );

            int Lock( lock_mode   Mode,
                      int         Timeout,
                      const char* Filename,
                      int         Linenum );

#if WORKING
            int Modify( lock_mode Mode, const char* Filename, int Linenum );
#endif /* WORKING */

            int Unlock( const char* Filename, int Linenum );

#if WORKING
            //-----------------------------------------------------------------
            /// \brief Check if the current thread already owns the resource.
            //-----------------------------------------------------------------
            bool HasLock( ) const;

            //-----------------------------------------------------------------
            /// \brief Verify that a lock of the appropriate mode.
            ///
            /// \return
            ///     False if no lock is held.
            ///     True if a lock with sufficient priveledges is held.
            /// \note
            ///     An exception is thrown if a write lock is requested
            ///     and the thread currently holds a read lock.
            //-----------------------------------------------------------------
            bool IsLocked( lock_mode Mode ) const;

#endif /* WORKING */
        private:
            //-------------------------------------------------------------------
            /// \brief Copy constructor is strictly prohibited.
            //-------------------------------------------------------------------
            impl( impl const& );
            //-------------------------------------------------------------------
            /// \brief Assignment operator is strictly prohibited.
            //-------------------------------------------------------------------
            impl& operator=( impl const& );

#if WORKING
            struct impl;
            boost::shared_ptr< impl > pimpl_;
#endif /* WORKING */

            struct hash_func
            {
                inline size_t
                operator( )( Thread::thread_type::handle_type* Key ) const
                {
                    return reinterpret_cast< size_t >( Key );
                }
            };

            void conflict( ) const;

#if WORKING
            lock_mode mode( ) const;
#endif /* WORKING */

            void set( lock_mode Mode, lock_state State );

            void unset( );

            typedef LDASTools::AL::unordered_map<
                Thread::thread_type::handle_type*,
                info,
                hash_func >
                queue_type;

            const bool m_verbose_tracking;

            handle_type handle; // Formerly m_lock

            queue_type m_info;

            mutable MutexLock::baton_type m_baton;
        };
#endif /* DEPRICATED */
    } // namespace AL
} // namespace LDASTools

#endif /* GENERAL__READ_WRITE_LOCK_IMPLE_CC */
