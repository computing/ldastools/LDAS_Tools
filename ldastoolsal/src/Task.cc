//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include <cstring>
#include <iostream>

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Task.hh"

#include "MutexLockBaton.cc"

namespace LDASTools
{
    namespace AL
    {
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        struct Task::_p_type
        {
        public:
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            typedef Task::cancel_method cancel_method;
            typedef Task::signal_type   signal_type;

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            inline _p_type( cancel_method CancelMethod,
                            signal_type   CancelSignal )
                : cancel_method_( CancelMethod ), cancel_signal( CancelSignal ),
                  delete_on_completion( false )
            {
            }

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            inline ~_p_type( )
            {
            }

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            cancel_method cancel_method_;
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            signal_type cancel_signal;
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            bool delete_on_completion;
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            MutexLock::baton_type lock;
        }; // struct - _p_type

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Task::Task( const std::string& Name,
                    cancel_method      CancelMethod,
                    signal_type        CancelSignal )
            : _p( new _p_type( CancelMethod, CancelSignal ) )
        {
            name.reset( new char[ Name.length( ) + 1 ],
                        std::default_delete< char[] >( ) );
            ::strcpy( name.get( ), Name.c_str( ) );
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Task::~Task( )
        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        MutexLock::baton_type
        Task::Baton( ) const
        {
            return _p->lock;
        }

        //-------------------------------------------------------------------
        ///
        //-------------------------------------------------------------------
        Task::cancel_method
        Task::CancelMethod( ) const
        {
            return _p->cancel_method_;
        }

        //-------------------------------------------------------------------
        ///
        //-------------------------------------------------------------------
        Task::signal_type
        Task::CancelSignal( ) const
        {
            return _p->cancel_signal;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        bool
        Task::DeleteOnCompletion( ) const
        {
            return _p->delete_on_completion;
        }

        //-------------------------------------------------------------------
        /// This function is called regardless of why the task was
        /// terminated.
        /// It allows for any special cleanup or other action that needs to
        /// happen at the end of a task regardless of why the task
        /// terminated.
        //-------------------------------------------------------------------
        void
        Task::OnCompletion( int TaskThreadState )
        {
        }

        void
        Task::delete_on_completion( bool Value )
        {
            _p->delete_on_completion = Value;
        }
    } // namespace AL
} // namespace LDASTools
