//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include <iostream>
#include <cstring>

#include "regexmatch.hh"

//-----------------------------------------------------------------------------
/// \brief  Constructor
///
/// \param nmatches
///
RegexMatch::RegexMatch( off_type nmatches )
    : mMatches( 0 ), mNMatches( nmatches ), mString( 0 ), found( 0 )
{
    if ( mNMatches > 0 )
    {
        mMatches = new regmatch_t[ mNMatches ];
    }
}

//-----------------------------------------------------------------------------
/// \brief  Destructor
///
RegexMatch::~RegexMatch( )
{
    delete[] mMatches;
    mMatches = 0;
}

//-----------------------------------------------------------------------------
/// \brief  Perform a Regex Match
///
/// \param r Precompiled pattern buffer
/// \param string Text string containing the input text
/// \param eflags flag which cause changes in matching behavior
///+	(see: man regexec).
///
/// \return bool true if match was found, false otherwise
///
bool
RegexMatch::match( const Regex& r, const char* string, flag_type eflags )
{
    bool retval;

    mString = string;

    retval = ( ( r.compiled( ) ) ? ( regexec( &r.getPattern( ),
                                              mString,
                                              mNMatches,
                                              mMatches,
                                              eflags ) == 0 )
                                 : false );

    found = 0;
    if ( retval )
    {
        for ( off_type c = 0;
              ( c < mNMatches ) && ( mMatches[ c ].rm_so != -1 );
              ++c, ++found )
        {
            // Looping until we find the end of patterns
        }
    }
    return retval;
}

//-----------------------------------------------------------------------------
/// \brief  Get Substring
///
/// Return a substring as a C++ string object.
///
/// \param m Index of match starting from zero.
///
/// \return std::string - Value of the match if m is within the number of
///+	 matches, the empty string otherwise.
///
std::string
RegexMatch::getSubString( off_type m )
{
    if ( getSubStart( m ) == 0 )
    {
        return "";
    }

    return std::string( getSubStart( m ), getSubLength( m ) );
}
