#ifndef LDASTOOLSAL__LOGGER_HH
#define LDASTOOLSAL__LOGGER_HH

#include "SimpleLogger.hh"

SimpleLogger*
default_logger( )
{
    static std::unique_ptr< SimpleLogger > simple_logger =
        std::make_unique< SimpleLogger >( );
    return simple_logger.get( );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_debug( LOGGER_TYPE logger, int level, Args&&... args )
{
    logger->log_debug( level, std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_ok( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_ok( std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_warning( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_warning( std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_note( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_note( std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_error_non_fatal( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_error_non_fatal( std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_error( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_error( std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_email( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_email( std::forward< Args >( args )... );
}

template < typename LOGGER_TYPE, typename... Args >
void
logger_log_phone( LOGGER_TYPE logger, Args&&... args )
{
    logger->log_phone( std::forward< Args >( args )... );
}
#endif /* LDASTOOLSAL__LOGGER_HH */
