//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include "streamexception.hh"

//-----------------------------------------------------------------------------
/// \brief  Constructor.
///
/// \param in Input Stream
/// \param library Name of library or api
/// \param info Additional information about th exception
/// \param file Name of file where exception was thrown
/// \param line Line of file where exception was thrown
///
StreamException::StreamException( std::istream&      in,
                                  int                library,
                                  const std::string& info,
                                  const char*        file,
                                  size_t             line )
    : LdasException( )
{
    if ( in.eof( ) )
    {
        addError( library, END_OF_FILE, "end_of_file", info, file, line );
    }
    else if ( in.bad( ) )
    {
        addError( library, BAD_STREAM, "bad_stream", info, file, line );
    }
    else if ( in.fail( ) )
    {
        addError( library, FAIL_STREAM, "fail_stream", info, file, line );
    }
    else
    {
        addError( library, UNKNOWN_ERROR, "unknown_error", info, file, line );
    }
}

//-----------------------------------------------------------------------------
/// \brief  Constructor.
///
/// \param out Output stream
/// \param library Name of library or api
/// \param info Additional information about the exception
/// \param file Name of file where exception was thrown
/// \param line Line of file where exception was thrown
///
StreamException::StreamException( std::ostream&      out,
                                  int                library,
                                  const std::string& info,
                                  const char*        file,
                                  size_t             line )
    : LdasException( )
{
    if ( out.eof( ) )
    {
        addError( library, END_OF_FILE, "end_of_file", info, file, line );
    }
    else if ( out.bad( ) )
    {
        addError( library, BAD_STREAM, "bad_stream", info, file, line );
    }
    else if ( out.fail( ) )
    {
        addError( library, FAIL_STREAM, "fail_stream", info, file, line );
    }
    else
    {
        addError( library, UNKNOWN_ERROR, "unknown_error", info, file, line );
    }
}
