//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include "regex.hh"
#include "errors.hh"

using LDASTools::Errors::getErrorString;

//-----------------------------------------------------------------------------
/// \brief  Constructor
///
/// This constructor compiles the given regular expression.
///
///
Regex::Regex( )
    : mRegex( ), mPattern( ), mFlags( REG_EXTENDED ), mCompiled( false )
{
}

//-----------------------------------------------------------------------------
/// \brief  Constructor
///
/// This constructor compiles the given regular expression.
///
/// \param regex
/// \param cflags
///
/// \exception LdasException
///
Regex::Regex( const std::string& regex, int cflags )
    : mRegex( regex ), mPattern( ), mFlags( cflags ), mCompiled( false )
{
    compile( );
}

//-----------------------------------------------------------------------------
/// \brief  Constructor
///
/// This constructor compiles the given regular expression.
///
/// \param regex
/// \param cflags
///
/// \exception LdasException
///
Regex::Regex( const char* regex, int cflags )
    : mRegex( regex ), mPattern( ), mFlags( cflags ), mCompiled( false )
{
    compile( );
}

//-----------------------------------------------------------------------------
/// \brief  Constructor
///
/// This constructor compiles the given regular expression.
///
/// \param Source
///
/// \exception LdasException
///
Regex::Regex( const Regex& Source )
    : mRegex( Source.mRegex ), mPattern( ), mFlags( Source.mFlags ),
      mCompiled( false )
{
    compile( );
}

//-----------------------------------------------------------------------------
/// \brief  Destructor.
///
/// This just frees the compiled regex pattern.
///
Regex::~Regex( ) throw( )
{
    if ( mCompiled )
    {
        regfree( &mPattern );
    }
}

Regex&
Regex::operator=( const Regex& Source )
{
    mRegex = Source.mRegex;
    mFlags = Source.mFlags;
    mCompiled = false;
    compile( );
    return *this;
}

void
Regex::compile( )
{
    if ( mCompiled )
    {
        mCompiled = false;
        regfree( &mPattern );
    }
    if ( mRegex.length( ) > 0 )
    {
        int error = regcomp( &mPattern, mRegex.c_str( ), mFlags );
        if ( error != 0 )
        {
            size_t msgSize( regerror( error, &mPattern, 0, 0 ) );
            char*  msg = new char[ msgSize ];
            regerror( error, &mPattern, msg, msgSize );

            regfree( &mPattern );

            LdasException e( Library::GENERAL,
                             LDASTools::Errors::INVALID_REGEX,
                             getErrorString( LDASTools::Errors::INVALID_REGEX ),
                             msg,
                             __FILE__,
                             __LINE__ );
            delete[] msg;
            msg = 0;

            throw e;
        }
        mCompiled = true;
    }
}
