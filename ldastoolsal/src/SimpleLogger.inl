// -*- mode: c++ -*-
#ifndef LDASTOOLSAL__SIMPLE_LOGGER_INL
#define LDASTOOLSAL__SIMPLE_LOGGER_INL

#include "SimpleLogger.hh"

// Implementation of the template methods
template < typename... Args >
void
SimpleLogger::log_debug( int level, Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) &&
         level >= debug_level_ )
    {
        BOOST_LOG_SCOPED_THREAD_TAG( "Severity",
                                     custom_severity_level( level ) );
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger, custom_severity_level( level ) ) << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_warning( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger,
                       custom_severity_level( severity_level::warning ) )
            << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_ok( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger, custom_severity_level( severity_level::ok ) )
            << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_note( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger, custom_severity_level( severity_level::note ) )
            << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_error_non_fatal( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV(
            logger, custom_severity_level( severity_level::error_non_fatal ) )
            << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_error( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger, custom_severity_level( severity_level::error ) )
            << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_email( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger, custom_severity_level( severity_level::email ) )
            << ss.str( );
    }
}

template < typename... Args >
void
SimpleLogger::log_phone( Args&&... args )
{
    if ( boost::log::core::get( )->get_logging_enabled( ) )
    {
        std::stringstream ss;
        ( ss << ... << args );
        BOOST_LOG_SEV( logger, custom_severity_level( severity_level::phone ) )
            << ss.str( );
    }
}
#endif /* LDASTOOLSAL__SIMPLE_LOGGER_INL */
