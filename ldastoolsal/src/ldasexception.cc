//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include "ldasexception.hh"

//-----------------------------------------------------------------------------
/// \brief  Default Constructor.
///
/// Creates an exception with no error information.
///
LdasException::LdasException( ) : mErrors( )
{
}

//-----------------------------------------------------------------------------
/// \brief  Constructor.
///
/// \param library
/// \param code
/// \param msg
/// \param info
/// \param file
/// \param line
///
LdasException::LdasException( int                library,
                              int                code,
                              const std::string& msg,
                              const std::string& info,
                              const char*        file,
                              size_t             line )
    : mErrors( )
{
    mErrors.push_back( ErrorInfo( library, code, msg, info, file, line ) );
}

//-----------------------------------------------------------------------------
/// \brief  Constructor
///
/// Creates an exception holding the passed ErrorInfo object.
///
/// \param e
///
LdasException::LdasException( const ErrorInfo& e ) : mErrors( )
{
    mErrors.push_back( e );
}

//-----------------------------------------------------------------------------
/// \brief  Copy Constructor
///
/// \param e
///
LdasException::LdasException( const LdasException& e ) : mErrors( e.mErrors )
{
}

//-----------------------------------------------------------------------------
/// \brief  Destructor
///
LdasException::~LdasException( )
{
}

//-----------------------------------------------------------------------------
/// \brief  Assignment Operator
///
/// \param e
///
/// \return LdasException&
///
LdasException&
LdasException::operator=( const LdasException& e )
{
    if ( this != &e )
    {
        mErrors = e.mErrors;
    }

    return *this;
}

//-----------------------------------------------------------------------------
/// \brief  Get Error Info
///
/// \param i
///
/// \return const ErrorInfo&
///
///!exc: range_error
///
const ErrorInfo&
LdasException::getError( size_t i ) const
{
    if ( i >= mErrors.size( ) )
    {
        throw std::range_error( "index out of range" );
    }

    return mErrors[ i ];
}

//-----------------------------------------------------------------------------
/// \brief  Array Operator
///
/// \param i
///
/// \return const ErrorInfo&
///
///!exc: range_error
///
const ErrorInfo& LdasException::operator[]( size_t i ) const
{
    return getError( i );
}

//-----------------------------------------------------------------------------
/// \brief  Add Error
///
/// Add another error info object to the class.
///
/// \param e
///
void
LdasException::addError( const ErrorInfo& e )
{
    mErrors.push_back( e );
    return;
}

//-----------------------------------------------------------------------------
/// \brief  Add Error
///
/// Add another error info object to the class.
///
/// \param library
/// \param code
/// \param msg
/// \param info
/// \param file
/// \param line
///
void
LdasException::addError( int                library,
                         int                code,
                         const std::string& msg,
                         const std::string& info,
                         const char*        file,
                         size_t             line )
{
    addError( ErrorInfo( library, code, msg, info, file, line ) );
    return;
}
