//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>

#include <algorithm>
#include <fstream>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/Log.hh"
#include "ldastoolsal/System.hh"

using LDASTools::AL::MemChecker;
using LDASTools::System::ErrnoMessage;

static const int OFFLOAD_CPU_FOR_N_SECONDS = 1;
static const int DEFAULT_ENTRIES_MAX = 0;
static const int WAIT_FOR_N_SECONDS = 5;

namespace
{
    using LDASTools::AL::Log;

}
namespace LDASTools
{
    namespace AL
    {
        class LogCB : public Log::StreamCBInterface
        {
        public:
            LogCB( Log* Source ) : source( Source )
            {
            }

            virtual ~LogCB( )
            {
            }

            virtual void
            OnClose( )
            {
                source->onStreamClose( );
            }

            virtual void
            OnOpen( )
            {
                source->onStreamOpen( );
            }

        private:
            Log* source;
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Log::StreamCBInterface::~StreamCBInterface( )
        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Log::StreamInterface::StreamInterface( )
        {
        }

        //-------------------------------------------------------------------
        /// \brief Destructor
        //-------------------------------------------------------------------
        Log::StreamInterface::~StreamInterface( )
        {
        }

        //-------------------------------------------------------------------
        /// \brief Destructor
        //-------------------------------------------------------------------
        void
        Log::StreamInterface::onClose( )
        {
            if ( IsOpen( ) && callback )
            {
                callback->OnClose( );
            }
        }

        //-------------------------------------------------------------------
        /// \brief Destructor
        //-------------------------------------------------------------------
        void
        Log::StreamInterface::onOpen( )
        {
            if ( IsOpen( ) && callback )
            {
                callback->OnOpen( );
            }
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Log::StreamStdErr::StreamStdErr( ) : on_open_called( false )
        {
        }

        //-------------------------------------------------------------------
        /// \brief Destructor
        //-------------------------------------------------------------------
        Log::StreamStdErr::~StreamStdErr( )
        {
        }

        //---------------------------------------------------------------
        /// \brief How to close the stream
        //---------------------------------------------------------------
        void
        Log::StreamStdErr::Close( )
        {
        }

        //---------------------------------------------------------------
        /// \brief Resturn the open state of the stream
        ///
        /// \return
        ///     If the stream is open and writable, then return true.
        ///     Otherwise, false.
        //---------------------------------------------------------------
        bool
        Log::StreamStdErr::IsOpen( ) const
        {
            return true;
        }

        //---------------------------------------------------------------
        /// \brief How to open the stream
        //---------------------------------------------------------------
        void
        Log::StreamStdErr::Open( )
        {
            if ( on_open_called == false )
            {
                on_open_called = true;
                onOpen( );
            }
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        void
        Log::StreamStdErr::WriteMessage( const std::string& Message )
        {
            //-----------------------------------------------------------------
            // Write the information only to an open stream
            //-----------------------------------------------------------------
            if ( on_open_called == false )
            {
                Open( );
            }
            //-----------------------------------------------------------------
            // Output to the stream without any additional formatting.
            // The stream is flushed to ensure that the data gets out
            // in case of a crash.
            //-----------------------------------------------------------------
            this->StreamInterface::operator<<( Message ) << std::flush;
        }

        //---------------------------------------------------------------
        /// \brief Gain access to the stream
        //---------------------------------------------------------------
        std::ostream&
        Log::StreamStdErr::stream( )
        {
            return std::cerr;
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        Log::StreamFile::StreamFile( )
            : rotate_bytes( 0 ), rotate_bytes_trigger( 0 ), rotate_entries( 0 ),
              rotate_entries_trigger( 0 )
        {
        }

        //-------------------------------------------------------------------
        /// \brief Destructor
        //-------------------------------------------------------------------
        Log::StreamFile::~StreamFile( )
        {
        }

        //---------------------------------------------------------------
        /// \brief How to close the stream
        //---------------------------------------------------------------
        void
        Log::StreamFile::Close( )
        {
            onClose( );
            close( );
        }

        //---------------------------------------------------------------
        /// \brief Resturn the open state of the stream
        ///
        /// \return
        ///     If the stream is open and writable, then return true.
        ///     Otherwise, false.
        //---------------------------------------------------------------
        bool
        Log::StreamFile::IsOpen( ) const
        {
            return is_open( );
        }

        //---------------------------------------------------------------
        /// \brief How to open the stream
        //---------------------------------------------------------------
        void
        Log::StreamFile::Open( )
        {
            if ( IsOpen( ) )
            {
                Close( );
            }
            open( filename( ).c_str( ) );
            onOpen( );
        }

        void
        Log::StreamFile::WriteMessage( const std::string& Message )
        {
            //-----------------------------------------------------------------
            // Check if the output stream is open.
            //-----------------------------------------------------------------
            if ( !IsOpen( ) )
            {
                //---------------------------------------------------------------
                // Since it is not open, rotate the logs
                //---------------------------------------------------------------
                rotate( );
            }
            //-----------------------------------------------------------------
            // Output the message.
            //-----------------------------------------------------------------
            this->StreamInterface::operator<<( Message ) << std::flush;
            rotate_bytes += Message.size( );
            ++rotate_entries;
            //-----------------------------------------------------------------
            // Check to see if the log file should be closed.
            //-----------------------------------------------------------------
            if ( ( ( rotate_bytes_trigger ) &&
                   ( rotate_bytes >= rotate_bytes_trigger ) ) ||
                 ( ( entriesMax( ) ) && ( entries( ) >= entriesMax( ) ) ) )
            {
                Close( );
                if ( !IsOpen( ) )
                {
                    rotate_bytes = 0;
                    rotate_entries = 0;
                }
            }
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        std::string
        Log::StreamFile::archive_filename( ) const
        {
            struct stat stat_buf;
            std::string fname( StreamFile::filename( ) );

            //-----------------------------------------------------------------
            // Figure out when the last entry was made to the log file
            //-----------------------------------------------------------------
            if ( ::stat( fname.c_str( ), &stat_buf ) != 0 )
            {
                //---------------------------------------------------------------
                // There is an issue with getting access to the log file.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "Stat( " << fname
                    << ", stat_buf ) failed: " << ErrnoMessage( );
                throw std::runtime_error( msg.str( ) );
            }

            LDASTools::AL::GPSTime log_time(
                stat_buf.st_mtime, 0, LDASTools::AL::GPSTime::UTC );

            //-----------------------------------------------------------------
            // Generate the filename.
            //-----------------------------------------------------------------
            std::ostringstream retval;

            retval << ArchiveDirectory( ) << "/" << FilenamePattern( )
                   << ".log." << log_time.GetSeconds( ) << FilenameExtension( );
            return retval.str( );
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        INT_4U
        Log::StreamFile::entriesMax( ) const
        {
            return rotate_entries_trigger;
        }

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        std::string
        Log::StreamFile::filename( ) const
        {
            std::ostringstream filename;

            //-----------------------------------------------------------------
            // Generate the filename.
            //-----------------------------------------------------------------
            filename << ArchiveDirectory( ) << "/" << FilenamePattern( )
                     << ".log" << FilenameExtension( );
            return filename.str( );
        }
        //---------------------------------------------------------------
        //---------------------------------------------------------------
        void
        Log::StreamFile::rotate( )
        {
            try
            {
                //-------------------------------------------------------------------
                // Check to see if the stream is currently open
                //-------------------------------------------------------------------
                if ( IsOpen( ) )
                {
                    //-----------------------------------------------------------------
                    // Write the footer and close the log
                    //-----------------------------------------------------------------
                    Close( );
                }
                //-------------------------------------------------------------------
                /// \todo
                /// Rotate the logs
                //-------------------------------------------------------------------
                int fd = ::open( filename( ).c_str( ), O_RDONLY );
                if ( fd < 0 )
                {
                    //-----------------------------------------------------------------
                    // There is an issue with getting access to the log file.
                    //-----------------------------------------------------------------
                    std::ostringstream msg;

                    msg << "::open(" << filename( )
                        << ") failed: " << ErrnoMessage( );

#if WORKING
                    //-----------------------------------------------------------------
                    // Report the error using the logging mechanism
                    //-----------------------------------------------------------------
                    Message( MT_ERROR,
                             0,
                             "GenericAPI::Text::rotate",
                             "IDLE",
                             msg.str( ) );
#endif /* WORKING */
                }
                else
                {
                    try
                    {
                        ::close( fd );
                        //-----------------------------------------------------------------
                        // Archive the file
                        //-----------------------------------------------------------------
                        std::string fname( filename( ) );
                        std::string aname( archive_filename( ) );

                        if ( ::rename( fname.c_str( ), aname.c_str( ) ) != 0 )
                        {
                            //---------------------------------------------------------------
                            // There is an issue with renaming the file
                            //---------------------------------------------------------------
                            std::ostringstream msg;

                            msg << "::rename( " << fname << ", " << aname
                                << " ) failed: " << ErrnoMessage( );
                            throw std::runtime_error( msg.str( ) );
                        }
#if WORKING
                        //-----------------------------------------------------------------
                        // Setup for fixing symbolic links
                        //-----------------------------------------------------------------
                        std::ostringstream base;
                        base << archive_directory << "/" << filename_pattern;
                        std::ostringstream file1;
                        std::ostringstream file2;
                        file1 << base.str( ) << ".2" << filename_extension;
                        file2 << base.str( ) << ".1" << filename_extension;
                        //-----------------------------------------------------------------
                        // Roll the logs
                        // 1) Remove oldest log
                        // 2) Move n-2 logs up one position.
                        // 3) Create new symbolic link for the file just
                        // archived.
                        //-----------------------------------------------------------------
                        ::unlink( file1.str( ).c_str( ) );
                        ::rename(
                            file2.str( ).c_str( ),
                            file1.str( ).c_str( ) ); /* b.1.html => b.2.html */
                        if ( ::symlink( archive.str( ).c_str( ),
                                        file2.str( ).c_str( ) ) != 0 )
                        {
                            //---------------------------------------------------------------
                            /// \todo
                            /// Report error if creation of symlink failed
                            //---------------------------------------------------------------
                        }
#endif /* WORKING */
                    }
                    catch ( const std::exception& Error )
                    {
#if WORKING
                        //-----------------------------------------------------------------
                        // Report the error using the logging mechanism
                        //-----------------------------------------------------------------
                        std::string job_info( FormatJobInfo( "IDLE" ) );
                        Message( MT_ERROR,
                                 0,
                                 "GenericAPI::Text::rotate",
                                 job_info,
                                 Error.what( ) );
#endif /* WORKING */
                    }
                }
                //-------------------------------------------------------------------
                // Open the new log
                //-------------------------------------------------------------------
                Open( );
            }
            catch ( ... )
            {
                // Ignore any problems that make it out this far.
            }
            if ( !IsOpen( ) )
            {
                try
                {
                    //-------------------------------------------------------------
                    // If there was a problem try to reopen the existing log
                    // file so the messages at least have a place to go
                    //-------------------------------------------------------------
                    Open( );
                }
                catch ( ... )
                {
                    std::cerr << "WARN: Exception: line: " << __LINE__
                              << std::endl;
                }
            }
        }

        //---------------------------------------------------------------
        /// \brief Gain access to the stream
        //---------------------------------------------------------------
        std::ostream&
        Log::StreamFile::stream( )
        {
            return *this;
        }

        //-------------------------------------------------------------------
        /// Constructor for the Log class.
        /// This will save the base name to use for the log file
        /// and start the writing thread.
        ///
        /// This class works by seperating the request to log a message
        /// and the actual writing of the message.
        /// This seperation allows for multiple threads to log messages
        /// asyncronously while minimizing the serialization caused by
        /// critical resources being locked.
        //-------------------------------------------------------------------
        Log::Log( const std::string& BaseName )
            : stream_delete_on_destruct( true ), m_base_name( BaseName ),
              m_entries_max( DEFAULT_ENTRIES_MAX )
        {
            //-----------------------------------------------------------------
            // Start the thread which is responsible for streaming the messages
            // to the output stream.
            //-----------------------------------------------------------------
            boost::shared_ptr< Log::StreamCBInterface > cb( new LogCB( this ) );

            m_stream.reset( new StreamStdErr( ) );
            m_stream->Callback( cb );
        }

        Log::~Log( )
        {
            //-----------------------------------------------------------------
            // Stop background logging.
            //-----------------------------------------------------------------
            Cancel( );
            Join( );
            //-----------------------------------------------------------------
            // Close the stream to ensure it has been flushed.
            //-----------------------------------------------------------------
            purge( );
            //-----------------------------------------------------------------
            // if the message queue is not empty, flush them now
            //-----------------------------------------------------------------
            while ( m_message_queue.empty( ) == false )
            {
                std::cerr << m_message_queue.front( ).s_message << std::flush;
                m_message_queue.pop( );
            }
        }
        //-------------------------------------------------------------------
        /// This member first determins if the Message should be logged based
        /// on the Level for the Group.
        /// If it should be logged, it will be added to the message queue
        /// in a thread safe manner.
        /// Another thread is responsible for actually outputting the
        /// queue of messages to the log stream.
        //-------------------------------------------------------------------
        void
        Log::Message( const group_type   Group,
                      const level_type   Level,
                      const std::string& Message )
        {
            //-----------------------------------------------------------------
            // Determine if the message is to be put into the message queue.
            //-----------------------------------------------------------------
            if ( VerbosityCheck( Group, Level ) == false )
            {
                return;
            }
            //-----------------------------------------------------------------
            // From the looks of everything, the message should be added to
            // the queue.
            //
            // Lock the queue to prevent others from modifying its contents
            // and potentially corrupt memory.
            //-----------------------------------------------------------------
            MutexLock lock( m_message_baton, __FILE__, __LINE__ );

            m_message_queue.push( message_queue_entry_type( ) );
            m_message_queue.back( ).s_message = Message;
            m_message_queue.back( ).s_timestamp.Now( );
        }

        void
        Log::Stream( stream_type Output )
        {
            MutexLock lock( m_message_baton, __FILE__, __LINE__ );

            m_stream = Output;
            if ( m_stream )
            {
                boost::shared_ptr< LogCB > cb( new LogCB( this ) );

                m_stream->Callback( cb );
            }
        }

        Log::stream_type
        Log::Stream( )
        {
            return m_stream;
        }

        void
        Log::Sync( ) const
        {
            MutexLock lock( m_message_baton, __FILE__, __LINE__ );

            while ( ( m_output_queue.empty( ) == false ) ||
                    ( m_message_queue.empty( ) == false ) )
            {
                //---------------------------------------------------------------
                // Release the lock so others can manipulate the queue
                //---------------------------------------------------------------
                m_message_baton.Unlock( __FILE__, __LINE__ );
                //---------------------------------------------------------------
                // Give time for everything else to get started
                //---------------------------------------------------------------
                struct timespec wakeup;

                wakeup.tv_sec = WAIT_FOR_N_SECONDS;
                wakeup.tv_nsec = 0;

                nanosleep( &wakeup, NULL );
                //---------------------------------------------------------------
                // Reacquire the lock so we have an accurate count
                //---------------------------------------------------------------
                m_message_baton.Lock( __FILE__, __LINE__ );
            }
        }

        //-------------------------------------------------------------------
        /// By default, there is no special action that needs to take place
        /// when the file is closed.
        //-------------------------------------------------------------------
        void
        Log::onStreamClose( )
        {
        }

        //-------------------------------------------------------------------
        /// By default, there is no special action that needs to take place
        /// when the file is opened.
        //-------------------------------------------------------------------
        void
        Log::onStreamOpen( )
        {
        }

        //-------------------------------------------------------------------
        /// Send all output to the open log file
        //-------------------------------------------------------------------
        void
        Log::purge( )
        {
            while ( m_output_queue.empty( ) == false )
            {
                if ( m_stream && m_stream->IsOpen( ) )
                {
                    ( *m_stream )
                        << m_output_queue.front( ).s_message << std::flush;
                }
                else
                {
                    std::cerr << m_output_queue.front( ).s_message
                              << std::flush;
                }
                m_output_queue.pop( ); // Remove from the queue
            } // while ( m_output_queue.empty( ) == false )
            while ( m_message_queue.empty( ) == false )
            {
                if ( m_stream && m_stream->IsOpen( ) )
                {
                    ( *m_stream )
                        << m_message_queue.front( ).s_message << std::flush;
                }
                else
                {
                    std::cerr << m_message_queue.front( ).s_message
                              << std::flush;
                }
                m_message_queue.pop( );
            }
        }

        //-------------------------------------------------------------------
        /// This method is used for updating the output stream.
        /// It is done by a child thread and is started when the object is
        /// created.
        /// The thread is terminated when the object is destroyed.
        //-------------------------------------------------------------------
        void
        Log::action( )
        {
            //-----------------------------------------------------------------
            // Setup the thread.
            // Currently allow asyncronous cancellation since we are only
            // sleeping.
            //-----------------------------------------------------------------
            CancellationType( Thread::CANCEL_ASYNCHRONOUS );
            CancellationEnable( true );

            {
                //---------------------------------------------------------------
                // Give time for everything else to get started
                //---------------------------------------------------------------
                struct timespec wakeup;

                wakeup.tv_sec = WAIT_FOR_N_SECONDS;
                wakeup.tv_nsec = 0;

                nanosleep( &wakeup, NULL );
            }

            //-----------------------------------------------------------------
            // Reset the way to be cancelled to be via exception since there
            // are more things that need to be cleaned up.
            //-----------------------------------------------------------------
            CancellationType( Thread::CANCEL_EXCEPTION );
            try
            {
                //---------------------------------------------------------------
                // main thread execution
                //---------------------------------------------------------------
                while ( MemChecker::IsExiting( ) == false )
                {
                    try
                    {
                        //-----------------------------------------------------------
                        // Be cooperative with cancelation
                        //-----------------------------------------------------------
                        CancellationCheck(
                            "LDASTools::AL::Log:: action", __FILE__, __LINE__ );

                        //-----------------------------------------------------------
                        // Critical section
                        //-------------------------------------------------------------
                        {
                            //-----------------------------------------------------------
                            // Reduce the time the message queue is locked by
                            // moving the messages to a temporary variable.
                            //-----------------------------------------------------------
                            MutexLock lock(
                                m_message_baton, __FILE__, __LINE__ );

                            if ( m_message_queue.empty( ) == false )
                            {
                                std::swap( m_message_queue, m_output_queue );
                            }
                        }
                        //-------------------------------------------------------------
                        // Check if there any work to be done
                        //-------------------------------------------------------------
                        if ( m_output_queue.empty( ) )
                        {
                            //-----------------------------------------------------------
                            // Nothing to do so offload the cpu for a couple of
                            // seconds
                            //-----------------------------------------------------------
                            struct timespec wakeup;

                            wakeup.tv_sec = OFFLOAD_CPU_FOR_N_SECONDS;
                            wakeup.tv_nsec = 0;

                            nanosleep( &wakeup, NULL );
                        }
                        else // if ( mq.empty( ) )
                        {
                            //-----------------------------------------------------------
                            // Output the messages to the ouput stream.
                            //-----------------------------------------------------------
                            flush( );
                        } // if ( mq.empty( ) )
                    }
                    catch ( const Thread::cancellation& Exception )
                    {
                        flush( true );
                        //-----------------------------------------------------------
                        // Rethrow because we want out
                        //-----------------------------------------------------------
                        throw;
                    }
                    catch ( std::exception& Exception )
                    {
                        //-----------------------------------------------------------
                        // Something has happened.
                        // Quietly absorb.
                        //-----------------------------------------------------------
                        //-----------------------------------------------------------
                        // Throw away the rest of the message
                        //-----------------------------------------------------------
                        flush( );
                    }
                    catch ( ... )
                    {
                        //-----------------------------------------------------------
                        // Something has happened.
                        // Quietly absorb.
                        //-----------------------------------------------------------
                        //-----------------------------------------------------------
                        // Throw away the rest of the message
                        //-----------------------------------------------------------
                        flush( );
                    }
                } // while( 1 )
            } // try
            catch ( const Thread::cancellation& Exception )
            {
                //---------------------------------------------------------------
                // Quietly ignore this one since it is an indication of shutting
                // down.
                //---------------------------------------------------------------
            }
            catch ( ... )
            {
            }

        } // Log::writer( )

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline void
        Log::flush( bool Everything )
        {
            while ( m_output_queue.empty( ) == false )
            {
                write_message( m_output_queue.front( ) );
                m_output_queue.pop( ); // Remove from the queue
            } // while ( m_output_queue.empty( ) == false )
            if ( Everything )
            {
                //---------------------------------------------------------------
                // Reduce the time the message queue is locked by moving the
                // messages to a temporary variable.
                //---------------------------------------------------------------
                {
                    MutexLock lock( m_message_baton, __FILE__, __LINE__ );

                    if ( m_message_queue.empty( ) == false )
                    {
                        std::swap( m_message_queue, m_output_queue );
                    }
                }
                while ( m_output_queue.empty( ) == false )
                {
                    write_message( m_output_queue.front( ) );
                    m_output_queue.pop( ); // Remove from the queue
                } // while ( m_output_queue.empty( ) == false )
            }
        }

        //-------------------------------------------------------------------
        /// This method does the actual writing of the message to the log.
        //-------------------------------------------------------------------
        void
        Log::write_message( const message_queue_entry_type& MessageInfo )
        {
            if ( m_stream )
            {
                if ( IsParentThread( ) )
                {
                    std::ostringstream msg;

                    msg << "LDASTools::AL::Log::write_message can only be "
                           "called by the child thread";

                    throw std::runtime_error( msg.str( ) );
                }
                m_stream->WriteMessage( MessageInfo.s_message );
            }
        }
    } // namespace AL
} // namespace LDASTools
