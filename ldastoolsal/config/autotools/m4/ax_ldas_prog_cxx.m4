dnl======================================================================
dnl AX_LDAS_PROG_CXX
dnl   Check on how to setup c++
dnl Depends:
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#



AC_DEFUN([AX_LDAS_PROG_CXX],
[ AC_REQUIRE([AC_LIBTOOL_CXX])
  AC_REQUIRE([AC_PROG_CXX])
  AC_REQUIRE([AX_LDAS_ARG_ENABLE_WARNINGS_AS_ERRORS])
  AC_ARG_ENABLE([cxx11],
    [AS_HELP_STRING([--disable-cxx11],[Disable features of 2011 C++ standard])])
  if test x${ac_cv_prog_cxx_g} = xyes
  then
    dnl------------------------------------------------------------------
    dnl PR3112: Put in -g (debug) option if supported by compiler
    dnl------------------------------------------------------------------
    CXXFLAGS="-g $CXXFLAGS"
  fi
  if test "${GXX}" = "yes"
  then
    touch ,t.cc
    ac_default_cxx_includes="`${CXX} -v ,t.cc 2>&1 | \
  	sed -n -e '/\#include/,/End of search list\./p' | \
  	egrep '^ '`"
    rm ,t.cc
  fi
  #=======================================================================
  # Determine compiler vendor
  #=======================================================================
  AC_LANG_PUSH([C++])
  LDAS_CXX_FLAVOR="generic"
  ldas_prog_cxx_vendor=generic
  if test $ldas_prog_cxx_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if GNU flavor
    dnl -----------------------------------------------------------------
    if test "${GXX}" = yes
    then
      ldas_prog_cxx_vendor="GNU"
      LDAS_CXX_FLAVOR=gnu
    fi
  fi
  if test $ldas_prog_cxx_vendor = generic
  then
    dnl -----------------------------------------------------------------
    dnl  Checking if SunPRO flavor
    dnl -----------------------------------------------------------------
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   #if __SUNPRO_CC
                   exit( 0 );
                   #endif /* __SUNPRO_CC */
	           exit( 1 );
                 }
               ],[
                ldas_prog_cxx_vendor=Sun
        	LDAS_CXX_FLAVOR="SunPRO"
               ])
  fi
  AC_LANG_POP([C++])
  dnl -------------------------------------------------------------------
  dnl  setting up of compiler flags
  dnl -------------------------------------------------------------------
  AC_MSG_CHECKING(C++ vendor)
  AC_MSG_RESULT($ldas_prog_cxx_vendor)
  case x$ldas_prog_cxx_vendor in
  xGNU) dnl ######## G N U ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_optimization_key="-O"
    ldas_prog_cxx_optimization_none="-O0"
    ldas_prog_cxx_optimization_exrtreme="-O4"
    ldas_prog_cxx_optimization_high="-O3"
    ldas_prog_cxx_optimization_medium="-O2"
    ldas_prog_cxx_optimization_low="-O1"
    ldas_prog_cxx_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    case $ldas_processor in
    sparc)
      ldas_prog_cxx_64bit="-m64 -mcpu=v9"
      ldas_prog_ld_64bit="-m64 -mcpu=v9"
      ;;
    i386)
      ldas_prog_cxx_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    *)
      ldas_prog_cxx_64bit="-m64"
      ldas_prog_ld_64bit="-m64"
      ;;
    esac
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    AS_IF([test x"$enable_warnings_as_errors" = "xyes"],
          [CXXFLAGS_FATAL_WARNINGS="-Werror"],
	  [CXXFLAGS_FATAL_WARNINGS=""])
    CXXFLAGS_PEDANTIC="-pedantic -Wno-long-long "
    dnl .................................................................
    dnl   C++
    dnl .................................................................
    AC_LANG_PUSH([C++])
    for warn in \
	-Wall \
	-Wextra \
	-Wdeprecated-register \
	-Wno-missing-field-initializers \
	-Wno-unused-parameter \
	-Wignored-qualifiers \
	-Wunused-but-set-variable \
	-Wc++0x-compat \
	-Wunused-private-field
    do
      AC_MSG_CHECKING([if C++ compiler supports warning flag: ${warn}])
      ldas_saved_cxxflags="${CXXFLAGS}"
      CXXFLAGS="${CXXFLAGS} -Werror ${ldas_prog_cxx_warning} ${warn}"
      AC_COMPILE_IFELSE(
        [ AC_LANG_PROGRAM([[#include <stdio.h>]
                           [const char hw[] = "Hello, World\n";]],
                          [[fputs (hw, stdout );]]) ],
        [ ldas_prog_cxx_warning="${ldas_prog_cxx_warning} ${warn}"
	  AC_MSG_RESULT([yes])
	],
        [ AC_MSG_RESULT([no]) ] )
      CXXFLAGS="${ldas_saved_cxxflags}"
    done
    AS_IF([test x"$enable_cxx11" != "xno"],
          [cxx_conform_flags="$cxx_conform_flags -std=c++11"])

    AS_CASE(["${CXXFLAGS}"],
	[*-std=*],[],
	[*-stdlib=*],[],
	[ dnl -----------------------------------------------------------
	  dnl  Specify the level of C++ conformance.
	  dnl     Accept the highest level only.
    	  dnl -----------------------------------------------------------------
    	  cxx_conform_flags="$cxx_conform_flags -std=c++0x"
    	  for flag in ${cxx_conform_flags}
    	  do
      	    AC_MSG_CHECKING([if C++ compiler supports flag: ${flag}])
      	    ldas_saved_cxxflags="${CXXFLAGS}"
      	    CXXFLAGS="${CXXFLAGS} ${flag}"
      	    AC_COMPILE_IFELSE(
              [ AC_LANG_PROGRAM([[#include <stdio.h>]
                                 [const char hw[] = "Hello, World\n";]],
                          	 [[fputs (hw, stdout );]]) ],
              [ dnl
		CXXSTDFLAGS="${flag}"
	      	AC_MSG_RESULT([yes])
          	break
	      ],
              [ CXXFLAGS="${ldas_saved_cxxflags}"
	        AC_MSG_RESULT([no]) ] )
    	  done
    	  dnl -----------------------------------------------------------
	  dnl  Specify the C++ library to use.
	  dnl     Accept the highest level only.
	  dnl -----------------------------------------------
    	  AS_IF([test x"$enable_cxx11" != "xno"],
          	[cxx_conform_libs="$cxx_conform_lib -stdlib=libc++"])
    	  cxx_conform_libs="$cxx_conform_libs -stdlib=libstdc++"
    	  for flag in ${cxx_conform_libs}
    	  do
      	    AC_MSG_CHECKING([if C++ compiler supports flag: ${flag}])
      	    ldas_saved_cxxflags="${CXXFLAGS}"
      	    CXXFLAGS="${CXXFLAGS} ${flag}"
      	    AC_COMPILE_IFELSE(
              [ AC_LANG_PROGRAM([[#include <stdio.h>]
                                [const char hw[] = "Hello, World\n";]],
                          	[[fputs (hw, stdout );]]) ],
              [ dnl
		CXXSTDLDFLAGS="${flag}"
	        AC_MSG_RESULT([yes])
                break
	      ],
              [ CXXFLAGS="${ldas_saved_cxxflags}"
	        AC_MSG_RESULT([no]) ] )
          done
	])
    AC_LANG_POP
    ;;
  xSun) dnl ######## S U N   S T U D I O ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_optimization_key="-O"
    ldas_prog_cxx_optimization_none="-xO0"
    ldas_prog_cxx_optimization_exrtreme="-xO4"
    ldas_prog_cxx_optimization_high="-xO3"
    ldas_prog_cxx_optimization_medium="-xO2"
    ldas_prog_cxx_optimization_low="-xO1"
    ldas_prog_cxx_optimization_default=""
    dnl -----------------------------------------------------------------
    dnl   64 bit
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_64bit="-m64"
    dnl -----------------------------------------------------------------
    dnl  Handling of warnings
    dnl -----------------------------------------------------------------
    CXXFLAGS_FATAL_WARNINGS="-errwarn=%all"
    CXXFLAGS_PEDANTIC=""
    dnl -----------------------------------------------------------------
    dnl  Setting up of CXXFLAGS
    dnl -----------------------------------------------------------------
    dnl Use STLport, not default
    CXXFLAGS="$CXXFLAGS -library=stlport4"
    dnl AC_CHECK_LIB([sunmath],[coshf])
    dnl -----------------------------------------------------------------
    dnl  Checking Version of Sun Studio compiler
    dnl -----------------------------------------------------------------
    AC_LANG_PUSH([C++])
    AC_TRY_RUN([ #include <stdlib.h>

                 int main( );

                 int
                 main( )
                 {
                   #if __SUNPRO_CC <= 0x0590
                   exit( 0 );
                   #endif /* __SUNPRO_CC */
	           exit( 1 );
                 }
               ],[ldas_inline_kludge="inline"
               ])
    AC_LANG_POP([C++])
    ;;
  *) dnl ###### D E F A U L T ########
    dnl -----------------------------------------------------------------
    dnl  Optimization
    dnl -----------------------------------------------------------------
    ldas_prog_cxx_optimization_key="-O"
    ldas_prog_cxx_optimization_none="-O0"
    ldas_prog_cxx_optimization_exrtreme="-O4"
    ldas_prog_cxx_optimization_high="-O3"
    ldas_prog_cxx_optimization_medium="-O2"
    ldas_prog_cxx_optimization_low="-O1"
    ldas_prog_cxx_optimization_default=""
    ;;
  esac
  CXXFLAGS="${CXXFLAGS} ${ldas_prog_cxx_warning}"
  AM_CONDITIONAL([LDAS_CXX_SUNPRO],[test x$LDAS_CXX_FLAVOR = xSunPRO])
  AC_SUBST([CXXFLAGS_FATAL_WARNINGS])
  AC_SUBST([CXXFLAGS_PEDANTIC])
  AC_SUBST([CXXSTDFLAGS])
  AC_SUBST([CXXSTDLDFLAGS])

  dnl -------------------------------------------------------------------
  dnl Setup flags for 64 bit compilation
  dnl -------------------------------------------------------------------
  AC_REQUIRE([AX_LDAS_ARG_ENABLE_64BIT])
  case x${enable_64bit} in
  xyes)
    CXXFLAGS="$CXXFLAGS $ldas_prog_cxx_64bit"
    ;;
  esac
  AC_DEFINE_UNQUOTED([INLINE_KLUDGE],${ldas_inline_kludge},Set to "inline" if need to inline kludge to prevent multi/undefined variables)
  dnl -------------------------------------------------------------------
  dnl
  dnl -------------------------------------------------------------------
  AC_LANG_PUSH([C++])
  AC_RUN_IFELSE([AC_LANG_SOURCE([
    #include <stdlib.h>
    #include <stdexcept>

    int
    main(int, char**)
    {
      try {
        throw std::runtime_error("Some error");
      }
      catch( ... )
      {
        exit( 0 );
      }
      exit( 1 );
    }
  ])],[],[
    AC_MSG_FAILURE([C++ compiler does not properly catch exceptions])
  ])
  AC_LANG_POP([C++])
  dnl -------------------------------------------------------------------
  dnl  Perform all tests using C++
  dnl -------------------------------------------------------------------
  AC_LANG_CPLUSPLUS
])
