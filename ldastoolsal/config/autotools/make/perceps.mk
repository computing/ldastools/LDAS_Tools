#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


NAMESPACE ?= $(PACKAGE)
htmldir ?= $(docdir)/$(NAMESPACE)/html
indexhtml ?= $(NAMESPACE).html

fn_perceps_cc_toc=$(shell if test -f perceps_cc_toc; \
		          then echo perceps_cc_toc; \
			  elif test -f $(srcdir)/perceps_cc_toc; \
			  then echo $(srcdir)/perceps_cc_toc; \
			  else echo /dev/null; fi)
fn_perceps_tcl_toc=$(shell if test -f perceps_tcl_toc; \
		          then echo perceps_tcl_toc; \
			  elif test -f $(srcdir)/perceps_tcl_toc; \
			  then echo $(srcdir)/perceps_tcl_toc; \
			  else echo /dev/null; fi)

perceps_cc_docs=$(subst yes,$(shell cat $(fn_perceps_cc_toc)), \
			    $(subst no,,$(HAVE_PERCEPS_TRUE)))
perceps_tcl_docs=$(shell sed 's/^/tcl\//g' $(fn_perceps_tcl_toc))

perceps_toc =
dynamic_htmlfiles =
ifeq ($(DOXYGEN),)
htmldir = $(docdir)/$(NAMESPACE)/html
indexhtml ?= index.html
perceps_toc += perceps_cc_toc
dynamic_htmlfiles += $(indexhtml) $(perceps_cc_docs)
sourceindexhtml ?= $(NAMESPACE)_index.html
endif
ifneq ($(TCLSH),)
perceps_toc += perceps_tcl_toc
tclhtmldir = $(docdir)/$(NAMESPACE)/html/tcl
tclhtml_DATA += $(perceps_tcl_docs)
endif

static_htmlfiles =
htmlfiles = $(dynamic_htmlfiles) $(static_htmlfiles)

api_srcdir ?= $(top_srcdir)/so/src
giffiles = 
_xswigfiles = $(api_srcdir)/*.swig
_swigfiles = $(shell echo $(_xswigfiles))
swigfiles = $(filter-out $(_xswigfiles),$(_swigfiles))
_xdocfiles = $(api_srcdir)/*.hh $(api_srcdir)/*.icc $(api_srcdir)/*.cc $(api_srcdir)/*.txt
_docfiles = $(shell echo $(_xdocfiles))
docfiles = $(filter-out $(_xdocfiles),$(_docfiles))
html_DATA := $(htmlfiles) $(giffiles)

EXTRA_DIST=$(static_htmlfiles) $(giffiles)

#------------------------------------------------------------------------
# This file cannot be built in parallel
#------------------------------------------------------------------------
.NOTPARALLEL:

#------------------------------------------------------------------------
# Building of documentation
#------------------------------------------------------------------------

index.html: $(sourceindexhtml)
	cp $(sourceindexhtml) index.html

ifneq ($(PERCEPS),)
#------------------------------------------------------------------------
# Perceps is available. Create info from source
#------------------------------------------------------------------------
perceps_tcl_toc: $(swigfiles)

perceps_cc_toc $(perceps_cc_docs) : $(docfiles) $(giffiles)
	@env SRCDIR=$(top_srcdir)/so/src PERCEPS=$(PERCEPS_ENV) \
		$(PERCEPS) $(PERCEPS_FLAGS) \
		--codeblocks \
		--plgdir $(PERCEPS_ENV)/plugins/cplusplus \
		$(docfiles) > .perceps_cc_out 2>&1; \
	egrep '^Generating' .perceps_cc_out | \
		egrep -v '^[^ ]*[ ][ ]*[^ ][^ ]*[ ]' | \
		$(PERCEPS_STRIP_GENERATING) | sort | uniq > perceps_cc_toc; \
	rm .perceps_cc_out

perceps_tcl_toc $(perceps_tcl_docs) : $(swigfiles) $(giffiles)
	if test ! -d tcl; then  mkdir tcl; fi; \
	env PERCEPS=$(PERCEPS_ENV) $(PERCEPS) $(PERCEPS_FLAGS) \
		-d tcl \
		--plgdir $(PERCEPS_ENV)/plugins/tcl \
		$(swigfiles) > .perceps_tcl_out  2>&1; \
	egrep '^Generating' .perceps_tcl_out | \
		egrep -v '^[^ ]*[ ][ ]*[^ ][^ ]*[ ]' | \
		$(PERCEPS_STRIP_GENERATING) | sort | uniq > perceps_tcl_toc; \
	rm .perceps_tcl_out

else
#------------------------------------------------------------------------
# Percpes is not available. Try to see if the source dir has the toc.
#	If not, then just make an empty toc.
#------------------------------------------------------------------------
perceps_cc_toc:
	if test -f $(srcdir)/perceps_cc_toc;	\
	then					\
		cp $(srcdir)/perceps_cc_toc .;	\
	else					\
		touch perceps_cc_toc;		\
	fi

perceps_tcl_toc:
	if test -f $(srcdir)/perceps_tcl_toc;		\
	then						\
		cp $(srcdir)/perceps_tcl_toc .;	\
	else						\
		touch perceps_tcl_toc;			\
	fi
endif

#------------------------------------------------------------------------
# Overrides
#------------------------------------------------------------------------

#install-htmlDATA: $(html_DATA)
#	@$(NORMAL_INSTALL)
#	$(mkinstalldirs) $(DESTDIR)$(htmldir) $(DESTDIR)$(htmldir)/tcl
#	@list='$(html_DATA)'; for p in $$list; do \
#	if test -f $(srcdir)/$$p; then \
#	  echo " $(INSTALL_DATA) $(srcdir)/$$p $(DESTDIR)$(htmldir)/$$p"; \
#	  $(INSTALL_DATA) $(srcdir)/$$p $(DESTDIR)$(htmldir)/$$p; \
#	else if test -f $$p; then \
#	  echo " $(INSTALL_DATA) $$p $(DESTDIR)$(htmldir)/$$p"; \
#	  $(INSTALL_DATA) $$p $(DESTDIR)$(htmldir)/$$p; \
#	fi; fi; \
#	done


#------------------------------------------------------------------------
# Local hooks for additional actions
#------------------------------------------------------------------------

clean-perceps-mk:
	rm -rf $(dynamic_htmlfiles) .perceps $(perceps_toc) \
		$(NAMESPACE)_index.html \
		class_index.html cross_index.html \
		tcl laldoc

dist-perceps-mk: $(dynamic_htmlfiles) $(perceps_toc)
	if test -f index.html; \
	then \
	  cp index.html $(perceps_cc_docs) $(distdir) \
	fi
	if test -d tcl; \
	then \
	  mkdir $(distdir)/tcl; \
	  cp $(perceps_tcl_docs) $(distdir)/tcl; \
	fi
	cp $(perceps_toc) $(distdir)

junk:
	@echo fn_perceps_tcl_toc: $(fn_perceps_tcl_toc)
	@echo perceps_tcl_docs: $(perceps_tcl_docs)
	@echo html_DATA: $(html_DATA)
	@echo tclhtml_DATA: $(tclhtml_DATA)
	@echo _xswigfiles: $(_xswigfiles)
	@echo _swigfiles: $(_swigfiles)
	@echo swigfiles: $(swigfiles)

#------------------------------------------------------------------------
# Add dependencies so pieces get built at the proper time
#------------------------------------------------------------------------
all: $(perceps_toc) $(dynamic_htmlfiles)
clean: clean-perceps-mk
dist: dist-perceps-mk
