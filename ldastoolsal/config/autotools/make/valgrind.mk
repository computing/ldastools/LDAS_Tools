#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#========================================================================
# Allow for some system testing using valgrin if available
#========================================================================
VALGRIND_TESTS_ENVIRONMENT ?= $(TESTS_ENVIRONMENT)
VALGRIND_TESTS ?= $(filter-out $(VALGRIND_EXCLUDE_TESTS), $(TESTS))
VALGRIND_ALL_OPTS=
VALGRIND_ALL_OPTS+=--verbose
ifdef VALGRIND_GEN_SUPPRESSION
VALGRIND_ALL_OPTS+=--demangle=no
VALGRIND_ALL_OPTS+=--gen-suppressions=all
else
VALGRIND_ALL_OPTS+=--demangle=yes
endif
VALGRIND_ALL_OPTS+=--suppressions=$(top_srcdir)/config/data/ldas.supp
VALGRIND_ALL_OPTS+=--suppressions=$(top_srcdir)/config/data/gcc.supp
VALGRIND_ALL_OPTS+=--num-callers=40
# VALGRIND_ALL_OPTS+=--read-var-info=yes
VALGRIND_ALL_OPTS+=$(VALGRIND_TOOL_OPTS)
VALGRIND_ALL_OPTS+=$(VALGRIND_OPTS)

VALGRIND_TOOL_SUITE=

CALLGRIND_ALL_OPTS=
CALLGRIND_ALL_OPTS+=--verbose
CALLGRIND_ALL_OPTS+=--dump-every-bb=10000000

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# CALLGRIND
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=callgrind
CALLGRIND_TOOL_OPTS="--callgrind-out-file=%q{cmd}.profile.%p"
CALLGRIND_EGREP_PATTERN=''

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# DRD
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=drd
DRD_TOOL_OPTS=""
DRD_EGREP_PATTERN='(ERROR SUMMARY:)'

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# HELGRIND
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=helgrind
HELGRIND_TOOL_OPTS="--track-lockorders=no"
HELGRIND_EGREP_PATTERN='(ERROR SUMMARY:)'

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# MEMCHECK
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=memcheck
MEMCHECK_TOOL_OPTS=--leak-check=yes --show-reachable=yes
MEMCHECK_TOOL_OPTS+=--track-origins=yes
MEMCHECK_EGREP_PATTERN='(definitely lost:|possibly lost:|still reachable:|ERROR)'

#========================================================================
# Rules section
#========================================================================

ifdef TESTS
valgrind-gen: check-TESTS
endif

valgrind-clean:
	@if test -d .libs; \
	 then               \
           ( cd .libs; rm -f $(patsubst %,*.%,$(VALGRIND_TOOL_SUITE) ) ); \
           ( cd .libs; rm -f *.profile.* ); \
	 fi

#------------------------------------------------------------------------
# Hooks to the generic targets
#------------------------------------------------------------------------

dataracecheck: valgrind-helgrind
memcheck: valgrind-memcheck
mtsafe: valgrind-drd
profile: valgrind-callgrind

#------------------------------------------------------------------------

valgrind-memcheck:
	@$(MAKE) \
	  VALGRIND_TOOL="memcheck" \
	  VALGRIND_TOOL_OPTS="$(MEMCHECK_TOOL_OPTS)" \
	  VALGRIND_TOOL_EGREP_PATTERN=$(MEMCHECK_EGREP_PATTERN) \
	  valgrind-gen

valgrind-memcheck-gen-supp:
	@$(MAKE) \
	  VALGRIND_GEN_SUPPRESSION=yes \
	  valgrind-memcheck

#------------------------------------------------------------------------

valgrind-callgrind:
	$(MAKE) \
	  VALGRIND_TOOL="callgrind" \
	  VALGRIND_TOOL_OPTS=$(CALLGRIND_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(CALLGRIND_EGREP_PATTERN) \
	  valgrind-gen

#------------------------------------------------------------------------

valgrind-drd:
	$(MAKE) \
	  VALGRIND_TOOL="drd" \
	  VALGRIND_TOOL_OPTS=$(DRD_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(DRD_EGREP_PATTERN) \
	  valgrind-gen

valgrind-drd-gen-supp:
	$(MAKE) \
	  VALGRIND_GEN_SUPPRESSION=yes \
	  valgrind-drd

#------------------------------------------------------------------------

valgrind-helgrind:
	$(MAKE) \
	  VALGRIND_TOOL="helgrind" \
	  VALGRIND_TOOL_OPTS=$(HELGRIND_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(HELGRIND_EGREP_PATTERN) \
	  valgrind-gen

valgrind-helgrind-gen-supp:
	$(MAKE) \
	  VALGRIND_GEN_SUPPRESSION=yes \
	  valgrind-helgrind

#------------------------------------------------------------------------

valgrind-gen: $(VALGRIND_TESTS)
	@for x in $(VALGRIND_TESTS);				\
	do							\
	  if test -x ".libs/lt-$$x";				\
	  then							\
	    cmd=".libs/lt-$$x";					\
	  elif test -x ".libs/$$x";				\
	  then							\
	    cmd=".libs/$$x";					\
	  else							\
	    cmd="./$$x";					\
	  fi;							\
	  export cmd; 						\
	  $(VALGRIND_TESTS_ENVIRONMENT)				\
	  env LD_LIBRARY_PATH="$(VALGRIND_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH}" \
	    GLIBCXX_FORCE_NEW=yes				\
	    $(VALGRIND) --tool=$(VALGRIND_TOOL)			\
	    $(VALGRIND_ALL_OPTS)				\
            $$cmd $(COMMAND_OPTIONS) > $$cmd.$(VALGRIND_TOOL) 2>&1;		\
	done
	@case 'x$(VALGRIND_TOOL_EGREP_PATTERN)' in		\
	  x)							\
	     ;;							\
	  *) find ./ -name '*.$(VALGRIND_TOOL)'			\
	     -exec egrep '$(VALGRIND_TOOL_EGREP_PATTERN)'	\
	     {} /dev/null \;					\
	     ;;							\
	esac

callgrind: $(VALLGRIND_TESTS)
	echo $(VALGRIND_LD_LIBRARY_PATH)
	for x in $(VALGRIND_TESTS);				\
	do							\
	  if test -x .libs/lt-$$x;				\
	  then							\
	    cmd=".libs/lt-$$x";					\
	  elif test -x .libs/$x;				\
	  then							\
	    cmd=".libs/$$x";					\
	  else							\
	    cmd=".libs/$$x";					\
	  fi;							\
	  $(VALGRIND_TESTS_ENVIRONMENT)				\
	  env LD_LIBRARY_PATH="$(VALGRIND_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH}" \
	  callgrind $(CALLGRIND_ALL_OPTS) --base=$$cmd $$cmd;	\
	done

clean: valgrind-clean
