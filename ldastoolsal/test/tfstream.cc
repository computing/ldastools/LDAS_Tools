//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <unistd.h>

#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/unittest.h"

using LDASTools::AL::fstream;
using LDASTools::AL::ifstream;
using LDASTools::AL::ofstream;

LDASTools::Testing::UnitTest Test;

namespace
{
    void
    read_test( )
    {
        Test.Message( ) << "DEBUG: read_test: " << std::endl;
        static const char* file_name( "reading_test" );

        //-------------------------------------------------------------------
        // Seed the file with data
        //-------------------------------------------------------------------
        std::ofstream out_file( file_name );
        out_file << "Hello World" << std::endl;
        out_file.close( );

        try
        {
            //-----------------------------------------------------------------
            // Open the file
            //-----------------------------------------------------------------
            ifstream stream( file_name );
            char     buffer[ 256 ];

            stream.read( buffer, sizeof( buffer ) );

            //-----------------------------------------------------------------
            // Cleanup
            //-----------------------------------------------------------------
            stream.close( );
            unlink( file_name );
        }
        catch ( ... )
        {
            unlink( file_name );
            throw; // rethrow the error
        }
    }

    void
    write_test( )
    {
        Test.Message( ) << "DEBUG: write_test: " << std::endl;
        static const char* file_name( "writing_test" );

        try
        {
            //-----------------------------------------------------------------
            // Open the file
            //-----------------------------------------------------------------
            ofstream stream( file_name );
            char     buffer[ 256 ];
            std::fill( buffer, &buffer[ sizeof( buffer ) ], 'a' );

            stream.write( buffer, sizeof( buffer ) );

            //-----------------------------------------------------------------
            // Cleanup
            //-----------------------------------------------------------------
            stream.close( );
            unlink( file_name );
        }
        catch ( ... )
        {
            unlink( file_name );
            throw; // rethrow the error
        }
    }
} // namespace

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Initialize the test structure
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );

    //---------------------------------------------------------------------
    // :TODO: Run through a series of tests
    //---------------------------------------------------------------------

    write_test( );
    read_test( );

    //---------------------------------------------------------------------
    // Exit with the appropriate exit status
    //---------------------------------------------------------------------
    Test.Exit( );
    return 1;
}
