#include "ldastoolsal/Logger.hh"

int
main( int argc, char* argv[] )
{
    try
    {
        default_logger( )->set_format_type( SimpleLogger::FormatType::HTML );
        default_logger( )->set_debug_level( 5 );
        default_logger( )->set_min_severity(
            SimpleLogger::custom_severity_level(
                SimpleLogger::severity_level::ok ) );

        int variable = 42;

        logger_log_debug( default_logger( ), 10, "Hello ", variable, " world" );
        logger_log_debug(
            default_logger( ), 4, "This is a debugging level 4 message." );
        logger_log_ok( default_logger( ), "All is well with the world." );
        logger_log_warning(
            default_logger( ), "This is a warning message.", variable );
        logger_log_note( default_logger( ), "This is a note message." );
        logger_log_error_non_fatal( default_logger( ),
                                    "This is a non-fatal error message." );
        logger_log_error( default_logger( ), "This is a fatal error message." );
        logger_log_email( default_logger( ), "This is an email alert." );
        logger_log_phone( default_logger( ), "This is a phone alert." );

        default_logger( )->change_sink( "file", "logs" );

        logger_log_debug( default_logger( ),
                          10,
                          "Hello ",
                          variable,
                          " world after changing to file sink." );
        logger_log_warning(
            default_logger( ),
            "This is a warning message after changing to file sink." );

        default_logger( )->change_sink( "null" );

        logger_log_debug(
            default_logger( ), 10, "This message should be ignored." );
        logger_log_warning( default_logger( ),
                            "This warning should also be ignored." );

        default_logger( )->set_format_type( SimpleLogger::FormatType::HTML );
        default_logger( )->change_sink( "console" );

        logger_log_debug( default_logger( ),
                          10,
                          "Hello ",
                          variable,
                          " world after changing to HTML format." );
        logger_log_warning( default_logger( ),
                            "This is a warning message in HTML format." );
    }
    catch ( const std::exception& e )
    {
        std::cerr << "Error: " << e.what( ) << std::endl;
        return 1;
    }

    return 0;
}
