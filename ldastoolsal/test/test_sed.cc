//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldastoolsal/Sed.hh"
#include "ldastoolsal/unittest.h"

using LDASTools::Cmd::Sed;

LDASTools::Testing::UnitTest Test;

void
test_1( )
{
    std::string modified;
    Sed         s( "s/a/a.b/" );

    modified = s( "aba" );

    Test.Check( modified.compare( "a.bba" ) == 0 )
        << "Simple substitution" << std::endl;
}

void
test_2( )
{
    std::string modified;
    Sed         s( "s/a/a.b/i" );

    modified = s( "Aba" );

    Test.Check( modified.compare( "a.bba" ) == 0 )
        << "Ignore case substitution" << std::endl;
}

void
test_global( )
{
    std::string modified;
    Sed         s( "s/a/a.b/g" );

    modified = s( "aba" );

    Test.Check( modified.compare( "a.bba.b" ) == 0 )
        << "Global substitution" << std::endl;
}

void
test_multi_line( )
{
    std::string        modified;
    Sed                s( "s/a/a.b/g" );
    std::ostringstream buffer;
    std::ostringstream answer;

    buffer << "aba" << std::endl << "bab";

    answer << "a.bba.b" << std::endl << "ba.bb";

    modified = s( buffer.str( ) );

    Test.Check( modified.compare( answer.str( ) ) == 0 )
        << "Multi line substitution" << std::endl;
}

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Initialize the test structure
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );

    //---------------------------------------------------------------------
    // Run through a series of tests
    //---------------------------------------------------------------------

    test_1( );
    test_2( );
    test_global( );
    test_multi_line( );

    //---------------------------------------------------------------------
    // Exit with the appropriate exit status
    //---------------------------------------------------------------------
    Test.Exit( );
    return 1;
}
