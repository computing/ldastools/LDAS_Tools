#
# LDASTools filtes - A library implementing filtering algorithms
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools filtes is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools filtes is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#include $(LDAS_DIRECTORY_MAKE_INCLUDE)/compilerflags.mk
#
#noinst_DATA =
#

if( LINUX )
  execute_process(
    COMMAND uname -m
    OUTPUT_VARIABLE CMAKE_SYSTEM_MACHINE
    ERROR_QUIET
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
endif( )

#AM_CPPFLAGS= -I$(srcdir)/../src -I$(top_builddir)/include $(LDASTOOLSAL_INCLUDES)
include_directories(
  BEFORE
    ${CMAKE_CURRENT_SOURCE_DIR}
    "${CMAKE_BINARY_DIR}/include"
    ${CMAKE_BINARY_DIR}
    "${CMAKE_SOURCE_DIR}/src"
    ${Boost_INCLUDE_DIRS}
  )
#include $(LDAS_DIRECTORY_MAKE_INCLUDE)/compilerflags.mk
#
#noinst_HEADERS = \
#	tThreadMutex.cc \
#	tThreadRWLock.cc
#
#noinst_PROGRAMS = \
#	gpstime_converter \
#	ldas-ps-tail \
#	pthread-info
#
#noinst_LTLIBRARIES=\
#	libexception.la
#
#EXTRA_TESTS = \
#	tTimerThread \
#	dynamic_loading
#
#libexception_la_SOURCES=\
#	exception.cc
#
#EXTRA_PROGRAMS = $(TESTS) $(EXTRA_TESTS)
#
#TESTS_ENVIRONMENT=env SRCDIR=$(srcdir)
#
#AM_CPPFLAGS=-I$(srcdir)/../src -I$(top_builddir)/include $(BOOST_CPPFLAGS)
#
#
set( LDADD ldastoolsal ${Boost_LIBRARIES} )
#dynamic_loading_LDADD=-ldl
#
##------------------------------------------------------------------------
## No install programs
##------------------------------------------------------------------------
set( ldas_ps_tail_SOURCES ldas-ps-tail.cc )

set( pthread_info_SOURCES pthread-info.cc )

#------------------------------------------------------------------------
# Test programs
#------------------------------------------------------------------------
set( CommandLine_SOURCES CommandLine.cc )
cx_target_executable(
  CommandLine
  NOINST
  SOURCES ${CommandLine_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( CommandLine COMMAND CommandLine )

#------------------------------------------------------------------------

set( test_logging_SOURCES test_logging.cc )
cx_target_executable(
  test_logging
  NOINST
  SOURCES ${test_logging_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( test_logging COMMAND test_logging )

#------------------------------------------------------------------------

set( FileType_SOURCES FileType.cc )
cx_target_executable(
  FileType
  NOINST
  SOURCES ${FileType_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( FileType
  COMMAND FileType
  ENVIRONMENT
    "SRCDIR=${CMAKE_CURRENT_BINARY_DIR}"
  )

#------------------------------------------------------------------------

set( gpstime_converter_SOURCES gpstime_converter.cc )
cx_target_executable(
  gpstime_converter
  NOINST
  SOURCES ${gpstime_converter_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( gpstime_converter COMMAND gpstime_converter )

#------------------------------------------------------------------------

set( test_cmp_nocase_SOURCES test_cmp_nocase.cc )
cx_target_executable(
  test_cmp_nocase
  NOINST
  SOURCES ${test_cmp_nocase_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( test_cmp_nocase COMMAND test_cmp_nocase )

#------------------------------------------------------------------------

set( test_mkdir_SOURCES test_mkdir.cc )
cx_target_executable(
  test_mkdir
  NOINST
  SOURCES ${test_mkdir_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( test_mkdir COMMAND test_mkdir )

#------------------------------------------------------------------------

set( test_sed_SOURCES test_sed.cc )
cx_target_executable(
  test_sed
  NOINST
  SOURCES ${test_sed_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( test_sed COMMAND test_sed )

#------------------------------------------------------------------------

set( tfstream_SOURCES tfstream.cc )
cx_target_executable(
  tfstream
  NOINST
  SOURCES ${tfstream_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( tfstream COMMAND tfstream )

#------------------------------------------------------------------------

set( tGPSTime_SOURCES tGPSTime.cc )
cx_target_executable(
  tGPSTime
  NOINST
  SOURCES ${tGPSTime_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( tGPSTime COMMAND tGPSTime )

#------------------------------------------------------------------------

cx_msg_debug_variable( CMAKE_SYSTEM_PROCESSOR )
if ( NOT APPLE AND NOT CMAKE_SYSTEM_MACHINE STREQUAL aarch64 )
  set( tPSInfo_SOURCES tPSInfo.cc )
  cx_target_executable(
    tPSInfo
    NOINST
    SOURCES ${tPSInfo_SOURCES}
    LDADD ${LDADD}
    )
  cx_target_test( tPSInfo COMMAND tPSInfo )
endif( )

#------------------------------------------------------------------------

set( tThread_SOURCES tThread.cc )
cx_target_executable(
  tThread
  NOINST
  SOURCES ${tThread_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( tThread COMMAND tThread )

#------------------------------------------------------------------------

set( tTimeout_SOURCES tTimeout.cc )
cx_target_executable(
  tTimeout
  NOINST
  SOURCES ${tTimeout_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( tTimeout COMMAND tTimeout )

#------------------------------------------------------------------------

if ( EXTRA_TESTS )
  set( tTimerThread_SOURCES tTimerThread.cc )
  cx_target_executable(
    tTimerThread
    NOINST
    SOURCES ${tTimerThread_SOURCES}
    LDADD ${LDADD}
    )
  cx_target_test( tTimerThread COMMAND tTimerThread )
endif( )

#------------------------------------------------------------------------

set( tSignalHandler_SOURCES tSignalHandler.cc )
cx_target_executable(
  tSignalHandler
  NOINST
  SOURCES ${tSignalHandler_SOURCES}
  LDADD ${LDADD}
  )
cx_target_test( tSignalHandler COMMAND tSignalHandler )

#------------------------------------------------------------------------

set( tSingleton_SOURCES tSingleton.cc )

#------------------------------------------------------------------------

cx_scheme_sanitizer_set_environment( )
