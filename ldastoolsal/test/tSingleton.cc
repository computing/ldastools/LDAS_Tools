//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldastoolsal/AtExit.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/UnitTestTS.h"

LDASTools::Testing::UnitTestTS Test;

static void test_1( );

using LDASTools::AL::Singleton;

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Initialize the test structure
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );

    //---------------------------------------------------------------------
    // Test the basic functionality
    //---------------------------------------------------------------------
    test_1( );

    //---------------------------------------------------------------------
    // Exit with the appropriate exit status
    //---------------------------------------------------------------------
    Test.Exit( );
    return 1;
}

class Test1 : public Singleton< Test1 >
{
public:
    Test1( )
    {
    }

    ~Test1( ) throw( )
    {
    }
};

SINGLETON_INSTANCE_DEFINITION( SingletonHolder< Test1 > )

static void
test_1( )
{
    bool passed = true;
    try
    {
        Test1::Instance( );
    }
    catch ( ... )
    {
        passed = false;
    }
    Test.Check( passed ) << "Testing creation of singleton";
}
