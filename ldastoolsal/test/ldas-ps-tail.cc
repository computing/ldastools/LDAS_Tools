//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sys/types.h>

#include <iostream>
#include <sstream>
#include <vector>

#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/PSInfo.hh"

using LDASTools::AL::PSInfo;

typedef std::vector< PSInfo > pids_list;

int
main( int ArgC, char** ArgV )
{
    pids_list pids;

    for ( int x = 1; x < ArgC; x++ )
    {
        std::istringstream pid_string( ArgV[ x ] );
        pid_t              pid;

        pid_string >> pid;

        pids.push_back( PSInfo( pid ) );
    }
    PSInfo::psinfo_type pid_info;
    while ( 1 )
    {
        for ( pids_list::const_iterator cur = pids.begin( ), last = pids.end( );
              cur != last;
              ++cur )
        {
            try
            {
                ( *cur )( pid_info );
                std::cout << LDASTools::AL::GPSTime::NowGPSTime( ) << " "
                          << pid_info.s_pid << " " << pid_info.s_fname << " "
                          << pid_info.s_pcpu << " " << pid_info.s_pmem
                          << std::endl;
            }
            catch ( const std::exception& Exception )
            {
                std::cerr << "ERROR: " << Exception.what( ) << std::endl;
            }
            catch ( ... )
            {
            }
        }
        sleep( 2 );
    }
    return 0;
}
