# Release 2.7.0 - December 16, 2024
    - Corrected memory malloc/free matching to quiet valgrind memcheck
    - Added new logging routines based on boost::logging

# Release 2.6.7 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.6.6 - August 13, 2021
  - Corrected Debian package resolution order for boost libaries (closes #107)
  * Corrected issues revealed by address sanitizer
  * Corrected issues revealed by thread sanitizer


# Release 2.6.5 - October 23, 2019
  - Minor changes for default compilers

# Release 2.6.4 - October 23, 2019
  - Added cx_ldas_prog_cxx() configuration check to obtain C++ standard
    flag for use in package config file.

# Release 2.6.3 - August 14, 2019
  - Converted to CMake (Closes #22)
  - Increased thread stack size to PTHREAD_STACK_MIN + 64k
  - Reformatted comments in SOLoader.hh for Doxygen (Closes #69)
  - Corrected Portfile to have proper description (Closes #55)

# Release2.6.2 - December 6, 2018
  - Addressed packaging issues

# Release2.6.1 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Standardize source code format by using clang-format
  - Removed check for type sizes as this now comes from stdint.h
  - Removed LDASUnexpected as the interface is depricated as of C++ 2011 (fixes #43)
  - Updated library versioning
    - ldastoolsal - 7:0:0

# Release2.6.0 - June 19, 2018
  - Removed hand rolled smart pointers in favor of boost smart pointers
  - Changed version of ldastoolsal library to 6:0:0

# Release2.5.6 - November 14, 2016
  - Corrected allocation of dirent_type (#4739)

# Release2.5.2 - September 9, 2016
  - Added functions Interval and Timeout needed by new diskcache

# Release2.5.1 - September 9, 2016
  - Added --disable-warnings-as-errors to allow compilation on systems
    where warning messages have not yet been addressed
  - Added conditional setting of DESTDIR in python.mk to prevent install
    issues.
  - Modified leap second table for gpstime for 2017 Jan 1 leap second.

# Release2.5.0 - April 7, 2016
  - Official release of splitting LDASTools into separate source packages

# Release2.4.99.1 - March 11, 2016
  - Corrections to spec files.

# Release2.4.99.0 - March 3, 2016
  - Separated code into independant source distribution
  - Enhanced the Dictionary class
