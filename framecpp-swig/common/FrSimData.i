/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FR_SIM_DATA_I
#define FR_SIM_DATA_I

%module(package="LDAStools") frameCPP

%include <attribute.i>
%import <boost_shared_ptr.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrSimData.hh"

using namespace FrameCPP;
%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrSimData.hh"

%import "Container.i"

%shared_ptr(FrSimData)

%feature("autodoc",
"""
FrSimData (Simulated Data) implementation of the frame specification.

  Attributes:

    name           Name of simulated data.
    comment        Comment.
    sampleRate     Data Acquisition rate, samples/s.
    timeOffset     Offset of 1st sample relative to the frame start
                   time (seconds). Must be positive and smaller
                   than the frame length.
    fShift         fShift is the frequency (in Hz) in the origional
                   data that corresponds to 0 Hz in the heterodyned
                   series.
    phase          Phase (in radian) of heterodyning signal at
                   start of dataset.
    data           Data vector.
""" ) FrSimData;

class FrSimData
{
public:
  typedef FR_SIM_DATA_SAMPLERATE_TYPE sampleRate_type;
  typedef FR_SIM_DATA_TIMEOFFSET_TYPE timeOffset_type;
  typedef FR_SIM_DATA_FSHIFT_TYPE fShift_type;
  typedef FR_SIM_DATA_PHASE_TYPE phase_type;

  iterable( FrSimData, data_type );

  FrSimData( );
  FrSimData( const std::string& Name,
	     const std::string& Comment,
	     const sampleRate_type SampleRate, 
	     const fShift_type FShift,
	     const phase_type Phase,
	     const timeOffset_type TimeOffset );

  const std::string& GetName() const; 
  const std::string& GetComment( ) const;
  sampleRate_type GetSampleRate( ) const;
  timeOffset_type GetTimeOffset( ) const;
  fShift_type GetFShift( ) const;
  phase_type GetPhase( ) const;

  void SetSampleRate( sampleRate_type Value );
  void SetTimeOffset( timeOffset_type  Value );

  %extend {
    void
    AppendData( FrVect& Data )
    {
      self->RefData( ).append( Data );
    }

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    size_t RefDataSize( );
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    data_type& RefData( );
  }
};

%attribute( FrSimData, const std::string&, name,GetName );
%attribute( FrSimData, const std::string&, comment, GetComment );
%attribute( FrSimData, sampleRate_type, sampleRate, GetSampleRate, SetSampleRate );
%attribute( FrSimData, timeOffset_type, timeOffset, GetTimeOffset, SetTimeOffset );
%attribute( FrSimData, fShift_type, fShift, GetFShift)
%attribute( FrSimData, phase_type, phase, GetPhase)
%attribute( FrSimData, data_type&, data, RefData)

#if ! SWIGIMPORTED

MAKE_REF( FrSimData, Data, data );

CONTAINER_WRAP(FrSimData,FrSimData)

#endif /* ! SWIGIMPORTED */

#endif /* FR_SIM_DATA_I */
