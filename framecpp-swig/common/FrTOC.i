/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


%module(package="LDAStools") frameCPP

%include <attribute.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrTOC.hh"
#include "framecpp/STRING.hh"

using namespace FrameCPP;

#define proc_key_container_type FrTOCProcData::key_container_type

 typedef FrameCPP::FrTOC::namedetector_container_type
   namedetector_container_type;
%}

%import "framecpp/FrTOC.hh"

%import "framecpp/DataTypes.i"
%import "framecpp/GPSTime.i"

%include "framecpp/FrTOCAdc.i"
%include "framecpp/FrTOCProc.i"
%include "framecpp/FrTOCSim.i"

%shared_ptr(FrTOC)

%feature("autodoc",
"""
FrTOC (Table of Contents Structure) implementation of the frame specification.

  Atributes:

    ULeapS  From the first FrameH in this file.
    nFrame  Number of frames in this file.
    GTimeS  Array of GPS seconds of frame start times.
    GTimeN  Array of GPS nanoseconds of frame start times.
""" ) FrTOC;

class FrTOC
{
public:
  //---------------------------------------------------------------------
  // typedefs
  //---------------------------------------------------------------------
  typedef FR_TOC_NFRAME_TYPE nFrame_type;

  //---------------------------------------------------------------------
  // namedetector_container_type
  //---------------------------------------------------------------------

  typedef std::vector< dt_type > dt_container_type;

  typedef std::vector< gtimes_type > gtimes_container_type;

  typedef std::vector< gtimen_type > gtimen_container_type;

  //---------------------------------------------------------------------
  // GetNameDetector
  //---------------------------------------------------------------------
  nFrame_type GetNFrame( ) const;

  const namedetector_container_type& GetNameDetector( ) const;

  const dt_container_type& GetDt( ) const;

  const gtimes_container_type& GetGTimeS( ) const;

  const gtimen_container_type& GetGTimeN( ) const;

  const MapADC_type& GetADC( ) const;

  const MapProc_type& GetProc( ) const;

  const MapSim_type& GetSim( ) const;

};

%attribute( FrTOC, %arg( nFrame_type ), nFrame, GetNFrame );
%attribute( FrTOC, %arg( const gtimes_container_type& ), GTimeS, GetGTimeS );
%attribute( FrTOC, %arg( const gtimen_container_type& ), GTimeN, GetGTimeN );
%attribute( FrTOC, %arg( const dt_container_type& ), dt, GetDt );

#ifndef HAVE_SWIG_TYPEMAP_NAMEDETECTOR_CONTAINER_TYPE
#error no "out" typemap for namedetector_container_type
#endif /* HAVE_SWIG_TYPEMAP_NAMEDETECTOR_CONTAINER_TYPE */
