/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef DIMENSION_I
#define DIMENSION_I

%module(package="LDAStools") frameCPP

%include <attribute.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/Dimension.hh"

using namespace FrameCPP;
%}

%import "framecpp/Dimension.hh"

%include "boost_shared_ptr.i"

%include "Container.i"

%shared_ptr(Dimension)

%feature("docstring")
  Dimension
  "The Dimension class describes a single dimension information of
   a single or multiple dimensional array."

class Dimension
{
public:
  typedef DIMENSION_NDIM_TYPE	nDim_type;
  typedef DIMENSION_NX_TYPE	nx_type;
  typedef DIMENSION_DX_TYPE	dx_type;
  typedef DIMENSION_STARTX_TYPE	startX_type;

  Dimension( );

  Dimension( nx_type Nx, dx_type Dx, const char* UnitX, startX_type StartX );

  nx_type GetNx( ) const;
   
  dx_type GetDx( ) const;
      
  startX_type GetStartX( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the description of how to interpret each element
  ///
  /// \return
  ///     The description of how to interpret each element
  //---------------------------------------------------------------------
  const std::string& GetUnitX( ) const;

  void SetDx( dx_type Dx );
      
  void SetNx( nx_type Nx );
   
  void SetStartX( startX_type StartX );

};

%attribute( Dimension, nx_type, nx, GetNx)
%attribute( Dimension, dx_type, dx, GetDx)
%attribute( Dimension, startX_type, startX, GetStartX)
%attribute( Dimension, const std::string&, unitX, GetUnitX )

#endif /* DIMENSION_I */
