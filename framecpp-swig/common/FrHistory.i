/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FR_HISTORY_I
#define FR_HISTORY_I

%module(package="LDAStools") frameCPP

%include <attribute.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrHistory.hh"

using namespace FrameCPP;
%}

%include "Container.i"

%import "framecpp/FrameCPP.hh"
%include "framecpp/FrHistory.hh"

%shared_ptr(FrHistory)

%feature("autodoc",
"""
FrHistory (History Data) implementation of the frame specification.

  Attributes:

    name     Name of history record. NOTE: When a FrHistory is linked
             to an FrProcData, its name variable must be the FrProcData
             channel name.
    comment  Program name and relevant comments needed to
             define post-processing.

""" ) FrHistory;

class FrHistory
{
public:
  FrHistory( );
  FrHistory( const std::string& Name,
	     INT_4U Time,
	     const std::string& Comment );
	     
  const std::string& GetName() const; 
  const std::string& GetComment( ) const;

};

%attribute( FrHistory, const std::string&, name, GetName );
%attribute( FrHistory, const std::string&, comment, GetComment );

#if ! SWIGIMPORTED
CONTAINER_WRAP(FrHistory,FrHistory);
#endif /* SWIGIMPORTED */

#endif /* FR_HISTORY_I */
