/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FR_ADC_DATA_I
#define FR_ADC_DATA_I

/** @cond IGNORE_BY_DOXYGEN */
%module(package="LDAStools") frameCPP

%include <attribute.i>
%include <boost_shared_ptr.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrAdcData.hh"

using namespace FrameCPP;
%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrAdcData.hh"

%import "Container.i"

%shared_ptr(FrAdcData)

%feature("autodoc",
"""
FrAdcData (ADC Data) implementation of the frame specification.

  Attributes:

    name           Channel name -- must be unique with the frame
    comment        Comment
    channelNumber  Channel number
    channelGroup   Channel grouping number containing ADC
    nBits          Number of bits in A/D output
    bias           DC bias on channel (Units @ ADC counts = 0)
    slope          ADC calibration: input units/ct
    units          ADC calibration: input units for slope.
    sampleRate     Data Acquisition rate, samples/s.
    timeOffset     Offset of 1st sample relative to the frame start
                   time (seconds). Must be positive and smaller
                   than the frame length.
    fShift         fShift is the frequency (in Hz) in the origional
                   data that corresponds to 0 Hz in the heterodyned
                   series.
    phase          Phase (in radian) of heterodyning signal at
                   start of dataset.
    dataValid      Data valid flag: dataValid = 0 > ADC data valid;
                   dataValid != 0 > ADC data suspect/not valid.
    data           Identifier of rverctor of sampled data.
""" ) FrAdcData;

class FrAdcData
{
public:
  typedef FR_ADC_DATA_CHANNEL_GROUP_TYPE channelGroup_type;
  typedef FR_ADC_DATA_CHANNEL_NUMBER_TYPE channelNumber_type;
  typedef FR_ADC_DATA_N_BITS_TYPE nBits_type;
  typedef FR_ADC_DATA_BIAS_TYPE bias_type;
  typedef FR_ADC_DATA_SLOPE_TYPE slope_type;
  typedef FR_ADC_DATA_UNITS_TYPE units_type;
  typedef FR_ADC_DATA_SAMPLERATE_TYPE sampleRate_type;
  typedef FR_ADC_DATA_TIMEOFFSET_TYPE timeOffset_type;
  typedef FR_ADC_DATA_FSHIFT_TYPE fShift_type;
  typedef FR_ADC_DATA_PHASE_TYPE phase_type;
  typedef FR_ADC_DATA_DATAVALID_TYPE dataValid_type;

  iterable( FrAdcData, data_type );

  FrAdcData( );
  FrAdcData( const std::string& Name,
	     channelGroup_type Group, channelNumber_type Channel,
	     nBits_type NBits,
	     sampleRate_type SampleRate );

  const std::string& GetName() const;
  const std::string& GetComment( ) const;
  channelNumber_type GetChannelNumber( ) const;
  channelGroup_type GetChannelGroup( ) const;
  nBits_type GetNBits( ) const;
  bias_type GetBias( ) const;
  slope_type GetSlope( ) const;
  const units_type& GetUnits( ) const;
  sampleRate_type GetSampleRate( ) const;
  timeOffset_type GetTimeOffset( ) const;
  fShift_type GetFShift( ) const;
  phase_type GetPhase( ) const;
  dataValid_type GetDataValid( ) const;

  void SetComment( const std::string& Value );
  void SetChannelGroup( channelGroup_type Value );
  void SetChannelNumber( channelNumber_type Value );
  void SetDataValid( dataValid_type Value );
  void SetSampleRate( sampleRate_type Value );
  void SetTimeOffset( timeOffset_type Value );

  %extend {

    void
    AppendData( FrVect& Data )
    {
      self->RefData( ).append( Data );
    }

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    size_t RefDataSize( );

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    data_type& RefData( );
  }

};

%attribute( FrAdcData, const std::string&, name, GetName )
%attribute( FrAdcData, const std::string&, comment, GetComment, SetComment )
%attribute( FrAdcData, channelNumber_type, channelNumber, GetChannelNumber, SetChannelNumber )
%attribute( FrAdcData, channelGroup_type, channelGroup, GetChannelGroup, SetChannelGroup)
%attribute( FrAdcData, nBits_type, nBits, GetNBits )
%attribute( FrAdcData, bias_type, bias, GetBias )
%attribute( FrAdcData, slope_type, slope, GetSlope )
%attribute( FrAdcData, const std::string&, units, GetUnits )
%attribute( FrAdcData, sampleRate_type, sampleRate, GetSampleRate, SetSampleRate )
%attribute( FrAdcData, timeOffset_type, timeOffset, GetTimeOffset, SetTimeOffset )
%attribute( FrAdcData, fShift_type, fShift, GetFShift )
%attribute( FrAdcData, phase_type, phase, GetPhase )
%attribute( FrAdcData, dataValid_type, dataValid, GetDataValid,SetDataValid )
%attribute( FrAdcData, data_type&, data, RefData )

#if ! SWIGIMPORTED

MAKE_REF( FrAdcData, Data, data );

CONTAINER_WRAP(FrAdcData,FrAdcData)

#endif /* SWIGIMPORTED */

/** @endcond */ /* IGNORE_BY_DOXYGEN */

#endif /* FR_ADC_DATA_I */
