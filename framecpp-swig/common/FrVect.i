
/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef FR_VECT_I
#define FR_VECT_I

/* Begin Group - SWIGInterface */
/**
 * \addtogroup SWIGInterface
 * @{
 */

%module(package="LDAStools") frameCPP

%feature("autodoc","1");

%include <attribute.i>
%include <boost_shared_ptr.i>
%include <std_string.i>
%include <std_vector.i>

%import "framecpp/DataTypes.i"
%include "framecpp/Container.i"
%import "framecpp/Dimension.i"
%import "framecpp/FrVect.hh"

%shared_ptr(FrVect)

%feature("docstring")
  FrVect
  "The FrVect class implements the FrVect structure as specified in
   the lastest approved frame specification."

#if !defined(SWIGIMPORTED)
%template(DimensionContainer) std::vector< Dimension >;
#endif /* !defined(SWIGIMPORTED) */


/**
 * \brief Vector Data Structure Definition
 */
%feature("autodoc",
"""
FrVect (Vector Data) implementation of the frame specification.

  Attributes:

    name      Channel name -- not required to be unique.
    compress  Compression algorithm number.
    type      Vector class.
    nData     Number of sample elements in data series.
    nBytes    Number of bytes in the compressed vector.
    nDim      Dimensionality of data vector.
    unitY     String describing how to interpret the value of
              each element. If dimensionless, then unitY == <NONE>,
              in CAPITALS (without <...>).
    dims      List of information describing each dimension of
              the data vector.
""" ) FrVect;

class FrVect
{
public:
  typedef FR_VECT_COMPRESS_TYPE	          compress_type;
  typedef FR_VECT_TYPE_TYPE			          type_type;
  typedef FR_VECT_NDATA_TYPE		          nData_type;
  typedef FR_VECT_NBYTES_TYPE		          nBytes_type;
  typedef FR_VECT_NDIM_TYPE			          nDim_type;
  typedef FR_VECT_DATA_TYPE               data_type;

  typedef FR_VECT_COMPRESSION_LEVEL_TYPE	compression_level_type;

  typedef DimensionContainer dimension_container_type;

  enum compression_scheme_type {
    RAW = FrameCPP::FrVect::RAW,
    BIGENDIAN_RAW = FrameCPP::FrVect::BIGENDIAN_RAW,
    LITTLEENDIAN_RAW = FrameCPP::FrVect::LITTLEENDIAN_RAW,

    GZIP = FrameCPP::FrVect::GZIP,
    BIGENDIAN_GZIP = FrameCPP::FrVect::BIGENDIAN_GZIP,
    LITTLEENDIAN_GZIP = FrameCPP::FrVect::LITTLEENDIAN_GZIP,

    DIFF_GZIP = FrameCPP::FrVect::DIFF_GZIP,
    BIGENDIAN_DIFF_GZIP = FrameCPP::FrVect::BIGENDIAN_DIFF_GZIP,
    LITTLEENDIAN_DIFF_GZIP = FrameCPP::FrVect::LITTLEENDIAN_DIFF_GZIP,

    ZERO_SUPPRESS = FrameCPP::FrVect::ZERO_SUPPRES,
    BIGENDIAN_ZERO_SUPPRESS = FrameCPP::FrVect::BIGENDIAN_ZERO_SUPPRESS,
    LITTLEENDIAN_ZERO_SUPPRESS = FrameCPP::FrVect::LITTLEENDIAN_ZERO_SUPPRESS,

    ZERO_SUPPRESS_OTHERWISE_GZIP = FrameCPP::FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP,
  };

  enum {
    FR_VECT_C = FrameCPP::FrVect::FR_VECT_C,
    FR_VECT_1U = FrameC::FrVect::FR_VECT_1U,
    FR_VECT_2S = FrameC::FrVect::FR_VECT_2S,
    FR_VECT_2U = FrameC::FrVect::FR_VECT_2U,
    FR_VECT_4S = FrameC::FrVect::FR_VECT_4S,
    FR_VECT_4U = FrameC::FrVect::FR_VECT_4U,
    FR_VECT_8S = FrameC::FrVect::FR_VECT_8S,
    FR_VECT_8U = FrameC::FrVect::FR_VECT_8U,

    FR_VECT_4R = FrameC::FrVect::FR_VECT_4R,
    FR_VECT_8R = FrameC::FrVect::FR_VECT_8R,

    FR_VECT_8C = FrameC::FrVect::FR_VECT_8C,
    FR_VECT_16C = FrameC::FrVect::FR_VECT_16C,

    FR_VECT_STRING = FrameC::FrVect::FR_VECT_STRING
  };

  //---------------------------------------------------------------------
  /// \brief Constructor.
  ///
  /// \param[in] name
  ///     The name of the data.
  /// \param[in] type
  ///     The data type.
  /// \param[in] nDim
  ///     The number of dimensions.
  /// \param[in] dims
  ///     A pointer to 'nDim' Dimension objects containing
  ///     information about the dimensions for this data.
  /// \param[in] byte_order
  ///     Byte order of the data. Default is BYTE_ORDER_HOST
  /// \param[in] data
  ///     A pointer to the data.  Default: 0
  /// \param[in] unitY
  ///     Units for the data.  Default: ""
  ///
  /// \return
  ///    A new instance of this object.
  //---------------------------------------------------------------------
  FrVect( const std::string& name,
	  type_type type,
	  nDim_type nDim,
	  const Dimension* dims, 
	  const std::string& unitY = "" );

  //---------------------------------------------------------------------
  /// \brief Retrieve the channel name.
  ///
  /// \return
  ///     The channel name
  //---------------------------------------------------------------------
  const std::string& GetName( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the compression algorithm number.
  ///
  /// \return
  ///     The compression algorithm number.
  //---------------------------------------------------------------------
  compress_type GetCompress( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the vector class.
  ///
  /// \return
  ///     The vector class.
  //---------------------------------------------------------------------
  type_type GetType( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the number of sample elements in data series.
  ///
  /// \return
  ///     The number of sample elements in data series.
  //---------------------------------------------------------------------
  nData_type GetNData( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the number of bytes in the compressed vector.
  ///
  /// \return
  ///     The number of bytes in the compressed vector.
  //---------------------------------------------------------------------
  nBytes_type GetNBytes( ) const;

#if 0
  //---------------------------------------------------------------------
  /// \brief Retrieve the pointer to the data.
  ///
  /// \return
  ///     The pointer to the data.
  //---------------------------------------------------------------------
  const CHAR_U* GetData( ) const;

  //-----------------------------------------------------------------
  /// \brief Retrieve the pointer to the compressed data.
  ///
  /// \return
  ///     The pointer to the compressed data.
  //-----------------------------------------------------------------
  const CHAR_U* GetDataRaw( ) const;

  //-----------------------------------------------------------------
  /// \brief Retrieve the pointer to the compressed data.
  ///
  /// \return
  ///     The pointer to the compressed data.
  //-----------------------------------------------------------------
  CHAR_U* GetDataRaw( );
#endif /* 0 */


#if !defined( FR_VECT_GET_DATA_UNCOMPRESSED_OVERRIDE )
  //---------------------------------------------------------------------
  /// \brief Retrieve the pointer to the uncompressed data.
  ///
  /// \return
  ///     The pointer to the uncompressed data.
  //---------------------------------------------------------------------
  data_type GetDataUncompressed( );
#endif /* !defined( FR_VECT_GET_DATA_UNCOMPRESSED_OVERRIDE ) */

#if 0
  //-----------------------------------------------------------------
  /// \brief Retrieve the pointer to the uncompressed data.
  ///
  /// \return
  ///     The pointer to the uncompressed data.
  //-----------------------------------------------------------------
  const CHAR_U* GetDataUncompressed( LDASTools::AL::AutoArray< CHAR_U >&
				     Expanded ) const;

  template< class T > static INT_2U GetDataType();
#endif /* 0 */

  //---------------------------------------------------------------------
  /// \brief Retrieve the number of dimensions
  ///
  /// \return
  ///     The number of dimensions
  //---------------------------------------------------------------------
  nDim_type GetNDim( ) const;

#if 0
  const Dimension& GetDim( INT_4U Offset ) const;
#endif /* 0 */

  //---------------------------------------------------------------------
  /// \brief Retrieve the Nth dimension
  ///
  /// \return
  ///     The Nth dimension
  //---------------------------------------------------------------------
  Dimension& GetDim( nDim_type Offset );

  %extend {
    dimension_container_type& GetDims( );
  }
      

  //---------------------------------------------------------------------
  /// \brief Retrieve the description of how to interpret each element
  ///
  /// \return
  ///     The description of how to interpret each element
  //---------------------------------------------------------------------
  const std::string& GetUnitY( ) const;

#if 0
  CHAR_U* SteelData( bool& Owns ) const;

  void Compress( compression_scheme_type Scheme, int GZipLevel );
  void Uncompress( );

  virtual void CompressData( INT_4U Scheme, INT_2U GZipLevel );

  virtual Compression::compression_base_type
  Compression( ) const;

  virtual Common::FrameSpec::Object*
  CloneCompressed( cmn_compression_scheme_type Scheme,
		   cmn_compression_level_type Level ) const;

  //-----------------------------------------------------------------
  /// \brief Establish the channel name.
  ///
  /// \param[in] Name
  ///     The channel name
  //-----------------------------------------------------------------
  void SetName( const std::string& Name );

  void SetNData( INT_4U NData );

  size_t GetTypeSize( ) const;
  static size_t GetTypeSize( INT_2U type );

  //-----------------------------------------------------------------
  /// \brief Merge with another FrAdcData
  ///
  /// \param[in] RHS
  ///     The source of the information to append to this FrAdcData
  ///     structure.
  ///
  /// \return
  ///     A reference to this object
  //-----------------------------------------------------------------
  FrVect& Merge( const FrVect& RHS );

  //-----------------------------------------------------------------
  /// \brief Request a subset of the data.
  ///
  /// \param[in] Start
  ///     The offset in the data set where to start.
  /// \param[in] Stop
  ///     The offset in the data set where to stop.
  ///
  /// \return
  ///     The subset of data bounded by data[Start, Stop).
  //-----------------------------------------------------------------
  std::unique_ptr< FrVect > SubFrVect( INT_4U Start, INT_4U Stop ) const;
#endif /* 0 */
};

%attribute( FrVect, const std::string&, name, GetName );
%attribute( FrVect, compress_type, compress, GetCompress );
%attribute( FrVect, type_type, type, GetType );
%attribute( FrVect, nData_type, nData, GetNData );
%attribute( FrVect, nBytes_type, nBytes, GetNBytes );
%attribute( FrVect, nDim_type, nDim, GetNDim );
%attribute( FrVect, const std::string&, unitY, GetUnitY );
%attribute( FrVect, dimension_container_type&, dims, GetDims );

#if defined(ADDITIONAL_FR_VECT_ATTRIBUTES)
ADDITIONAL_FR_VECT_ATTRIBUTES( );
#endif

#if ! SWIGIMPORTED
%{
  FrVect::dimension_container_type&
  FrVect_GetDims( FrVect* Self )
  {
    return Self->GetDims( );
  }
%}
#endif /* ! SWIGIMPORTED */

#if !defined(SWIGIMPORTED)
CONTAINER_WRAP(FrVect,FrVect)
#endif /* !defined(SWIGIMPORTED) */

/* End Group - SWIGInterface */
/**
 * @}
 */
#endif /* FR_VECT_I */
