"""! @brief Test script validating the slope value of the FrAdcData structure."""

import unittest
import numpy as np
from LDAStools import frameCPP;

# unittest will test all the methods whose name starts with 'test'
class TestFrProcData( unittest.TestCase ):
    #====================================================================
    # Test values to be used in test methods
    #====================================================================
    @classmethod
    def setUpClass( self ):
        self.NAME = "test"
        self.COMMENT = "comment test"
        self.TYPE = frameCPP.FrProcData.FREQUENCY_SERIES
        self.SUB_TYPE = frameCPP.FrProcData.DFT
        self.TIME_OFFSET = 1.23479789
        self.TIME_RANGE = 298.892453
        self.FSHIFT = 239487.234879
        self.PHASE = 98.97845
        self.FRANGE = 28.576
        self.BW = 4589.978543
        self.AUX_PARAM = [ ("AuxParamPI", 3.1415), ( "AuxParam2PI", 6.283 ) ]

    def validateArray( self, array1, array2 ):
        if ( len(array1) != len(array2) ):
            return False
        for element1, element2 in zip( array1, array2 ):
            if ( element1 != element2 ):
                return False
        return True

    #
    # Routine to verify structure after construction or manipulation
    #
    def validator( self,
                   procdata,
                   name = "",
                   comment = "",
                   tipe = frameCPP.FrProcData.TIME_SERIES,
                   subtype = frameCPP.FrProcData.UNKNOWN_SUB_TYPE,
                   timeoffset = 0.0,
                   trange = 0.0,
                   fshift = 0.0,
                   phase = 0.0,
                   frange = 0.0,
                   bw = 0.0,
                   auxparam = [] ):
        delta = 0.00001
        self.assertEqual( procdata.name, name )
        self.assertEqual( procdata.comment, comment )
        self.assertAlmostEqual( procdata.timeOffset, timeoffset, delta=delta )
        self.assertAlmostEqual( procdata.tRange, trange, delta=delta )
        self.assertAlmostEqual( procdata.fShift, fshift, delta=delta )
        self.assertAlmostEqual( procdata.phase, phase, delta=delta )
        self.assertAlmostEqual( procdata.fRange, frange, delta=delta )
        self.assertAlmostEqual( procdata.BW, bw, delta=delta )
        # self.assertTrue( self.validateArray( procdata.auxParam, auxparam ) )

    #
    # Validation of the attribute types
    #
    def test_attribute_types( self ):
        procdata = frameCPP.FrProcData( )
        self.assertTrue( isinstance( procdata.name, str ) )
        self.assertTrue( isinstance( procdata.comment, str ) )
        self.assertTrue( isinstance( procdata.type, int ) )
        self.assertTrue( isinstance( procdata.subType, int ) )
        self.assertTrue( isinstance( procdata.timeOffset, float ) )
        self.assertTrue( isinstance( procdata.tRange, float ) )
        self.assertTrue( isinstance( procdata.fShift, float ) )
        self.assertTrue( isinstance( procdata.phase, float ) )
        self.assertTrue( isinstance( procdata.fRange, float ) )
        self.assertTrue( isinstance( procdata.BW, float ) )
        # self.assertTrue( isinstance( procdata.auxParam, list ) )

    #
    # Validation of the method return types
    #
    def test_method_return_types( self ):
        procdata = frameCPP.FrProcData( )
        self.assertIsInstance( procdata.GetName( ), str )
        self.assertIsInstance( procdata.GetComment( ), str )
        self.assertIsInstance( procdata.GetType( ), int )
        self.assertIsInstance( procdata.GetSubType( ), int )
        self.assertIsInstance( procdata.GetTimeOffset( ), float )
        self.assertIsInstance( procdata.GetTRange( ), float )
        self.assertIsInstance( procdata.GetFShift( ), float )
        self.assertIsInstance( procdata.GetPhase( ), float )
        self.assertIsInstance( procdata.GetTRange( ), float )
        self.assertIsInstance( procdata.GetBW( ), float )
        # self.assertIsInstance( procdata.GetAuxParam( ), list )

    #
    # Validation of the default constructor
    #
    def test_constructor_default( self ):
        with self.assertRaises( Warning ):
            procdata = frameCPP.FrProcData( )
            raise Warning( "Successfully constructed object" )
        self.validator( procdata )

    #
    # Validation of the explicit constructor
    #
    def test_constructor_explicit( self ):
        with self.assertRaises( Warning ):
            procdata = frameCPP.FrProcData(
                self.NAME,
                self.COMMENT,
                self.TYPE,
                self.SUB_TYPE,
                self.TIME_OFFSET,
                self.TIME_RANGE,
                self.FSHIFT,
                self.PHASE,
                self.FRANGE,
                self.BW )
            raise Warning( "Successfully constructed object" )
        self.validator( procdata,
                        self.NAME,
                        self.COMMENT,
                        self.TYPE,
                        self.SUB_TYPE,
                        self.TIME_OFFSET,
                        self.TIME_RANGE,
                        self.FSHIFT,
                        self.PHASE,
                        self.FRANGE,
                        self.BW )
    #
    # Validation of setting values via functions
    #
    def test_attribute_setting_via_functions( self ):
        with self.assertRaises( Warning ):
            procdata = frameCPP.FrProcData( )
            raise Warning( "Successfully constructed object" )
        procdata.SetComment( self.COMMENT )
        procdata.SetTimeOffset( self.TIME_OFFSET )
        procdata.SetTRange( self.TIME_RANGE )
        for param in self.AUX_PARAM:
            procdata.AppendAuxParam( param[0], param[1] )

        self.validator( procdata,
                        comment = self.COMMENT,
                        timeoffset = self.TIME_OFFSET,
                        trange = self.TIME_RANGE,
                        auxparam = self.AUX_PARAM )

    #
    # Validation of setting values via attributes
    #
    def test_attribute_setting_via_attributess( self ):
        with self.assertRaises( Warning ):
            procdata = frameCPP.FrProcData( )
            raise Warning( "Successfully constructed object" )
        procdata.comment = self.COMMENT
        procdata.timeOffset = self.TIME_OFFSET
        procdata.tRange = self.TIME_RANGE
        # for param in self.AUX_PARAM:
        #    procdata.AppendAuxParam( param[0], param[1] )

        self.validator( procdata,
                        comment = self.COMMENT,
                        timeoffset = self.TIME_OFFSET,
                        trange = self.TIME_RANGE )

    #
    # Validation of auxParam manipulation
    #
    def test_constructor_default( self ):
        procdata = frameCPP.FrProcData( )
        self.validator( procdata )

    def test_auxParams_list(self):
        auxParam = [
            ("param1", 1.0),
            ("param2", 2.0),
        ]
        procdata = frameCPP.FrProcData( )
        procdata.SetAuxParam( auxParam )
        self.assertListEqual(auxParam, procdata.auxParam)
        procdata.SetAuxParam( [] )
        self.assertListEqual([], procdata.auxParam)
        # procdata.auxParam = auxParam
        # self.assertListEqual(auxParam, procdata.auxParam)

    def test_auxParams_dict(self):
        auxParam = {
            "param1": 1.0,
            "param2": 2.0,
        }
        procdata = frameCPP.FrProcData( )
        procdata.SetAuxParam( auxParam )
        self.assertDictEqual(auxParam, dict( procdata.auxParam ) )
        procdata.SetAuxParam( {} )
        self.assertDictEqual({}, dict( procdata.auxParam ) )
        # procdata.auxParam = auxParam
        # self.assertListEqual(auxParam, procdata.auxParam)

    def test_auxParams_tuple(self):
        auxParam = (
            ("param1", 1.0),
            ("param2", 2.0),
        )
        procdata = frameCPP.FrProcData( )
        procdata.SetAuxParam( auxParam )
        self.assertTupleEqual(auxParam, tuple( procdata.auxParam ) )
        procdata.SetAuxParam( () )
        self.assertTupleEqual((), tuple( procdata.auxParam) )
        # procdata.auxParam = auxParam
        # self.assertListEqual(auxParam, procdata.auxParam)

# running of the tests
if __name__ == '__main__':
    unittest.main( )
