# -*- coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug )
include( Autotools/ArchiveX/cx_ctest_add_python_test )

#------------------------------------------------------------------------
# add_python_test_
#
# Parameters:
#   target: name of test without the PYTHON_NAMESPACE prefix.
#   ARGN:   command and parameters of test to be executed
#
# Environment:
#   PYTHON_PREFIX    - Prefix to use for python variables
#   PYTHON_NAMESPACE - namespace to identify python (ex: python3 )
#   TEST_SOURCE_DIR  - Directory containing the python script
#------------------------------------------------------------------------
function( add_python_test_ target )
    set(options
        )
    set(oneValueArgs
        DEPENDS
        )
    set(multiValueArgs
        )
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    list(GET ARG_UNPARSED_ARGUMENTS 0 cmd )
    list(REMOVE_AT ARG_UNPARSED_ARGUMENTS 0)

    cx_ctest_add_python_test(
        TARGET _${PYTHON_NAMESPACE}${target}
        COMMAND "${TEST_SOURCE_DIR}/${cmd}" ${ARG_UNPARSED_ARGUMENTS}
        PYTHON_EXECUTABLE ${${PYTHON_PREFIX}_EXECUTABLE}
        PYTHON_MODULE_DIR ${PYTHON_MODULE_DIR}
        PYTHON_PATH ${CMAKE_SOURCE_DIR}/python/source ${LDASTOOLS_LIBRARY_DIRS}
        DEPENDS ${ARG_DEPENDS}
        )
    cx_msg_debug( "Adding dependencies: ${ARG_DEPENDS}" )

endfunction( )

function( get_target_library_dir_ TARGET VARIABLE )
    if (WIN32)
        set( ${VARIABLE} "" )
        set( ${VARIABLE} $ENV{PATH} )
        set( GENERATOR "Debug" )
        list( INSERT ${VARIABLE} 0 "${CMAKE_BINARY_DIR}/bin/${GENERATOR}" )
        set( TMP_LIBRARY_DIR "" )
        foreach( TMP ${${VARIABLE}} )
            file( TO_NATIVE_PATH ${TMP} TMP )
            string( REPLACE "\\" "\\\\" TMP ${TMP} )
            #list( APPEND TMP_${VARIABLE} ${TMP} )
            string( LENGTH "${TMP_LIBRARY_DIR}" L )
            if( $L LESS 1 )
                set( TMP_LIBRARY_DIR ${TMP} )
            else( $L LESS 1 )
                set( TMP_LIBRARY_DIR "${TMP_LIBRARY_DIR}\\\\;${TMP}" )
            endif( $L LESS 1 )
        endforeach( TMP )
        set( ${VARIABLE} ${TMP_LIBRARY_DIR} )
        string( REPLACE "\\" "\\\\" ${VARIABLE} ${${VARIABLE}} )
        message( STATUS "${VARIABLE}: ${${VARIABLE}}" )
    else (WIN32)
        get_target_property( ${VARIABLE} ${TARGET} LIBRARY_OUTPUT_DIRECTORY )
    endif(WIN32)
    set( ${VARIABLE} ${${VARIABLE}} PARENT_SCOPE )

endfunction( )

#-----------------------------------------------------------------------
#
#-----------------------------------------------------------------------
function( do_python_tests )
    set(options)
    set(oneValueArgs NAMESPACE PREFIX PYTHON_MODULE_DIR TEST_SOURCE_DIR)
    set(multiValueArgs)
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if ( NOT ARG_PREFIX )
        set( ARG_PREFIX PYTHON )
    endif( )

    if ( ARG_NAMESPACE )
        set( PYTHON_NAMESPACE ${ARG_NAMESPACE} )
    else ( )
        set( PYTHON_NAMESPACE "python" )
    endif ( )
    if ( ARG_PYTHON_MODULE_DIR )
        set( PYTHON_MODULE_DIR ${ARG_PYTHON_MODULE_DIR} )
    endif ( )
    if ( ARG_TEST_SOURCE_DIR )
        set( TEST_SOURCE_DIR ${ARG_TEST_SOURCE_DIR} )
    else ( )
        set( TEST_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} )
    endif ( )

    set( PYTHON_PREFIX ${ARG_PREFIX} )

    # get_target_library_dir_( framecpp FRAMECPP_LIBRARY_DIR )

    #====================================================================
    # Dependecies
    #====================================================================

    #====================================================================
    # These tests require a minimum of Python
    #====================================================================
    # Final variable substitution before use
    #--------------------------------------------------------------------
    string(CONFIGURE "${NDS_TESTER_FLAGS}" NDS_TESTER_FLAGS )
    #--------------------------------------------------------------------

    cl_info_framename( LATEST_TEST_FRAME ${FRAME_SPEC_LATEST} FULLPATH )

    # add_python_test(_${PYTHON_NAMESPACE}_is-trend-type_ test_is_trend_type.py)
    add_python_test_( _frameCPP_ test_frameCPP.py ${LATEST_TEST_FRAME} )
    foreach( v ${FRAME_SPEC_SET} )
        cl_info_framename( TEST_FRAME ${v} FULLPATH DESC fr_vect_types )
        add_python_test_( _frameCPP-frvect-types_v${v}_ test_frameCPP_frvect_types.py ${TEST_FRAME} )
    endforeach( )
    add_python_test_( _frameCPP-Detector_   test_Detector.py -v )
    add_python_test_( _frameCPP-FrProcData_ test_frprocdata.py )
    add_python_test_( _frameCPP-FrEvent_    test_frevent.py )
    add_python_test_( _frameCPP-FrSimEvent_ test_frsimevent.py )
    add_python_test_( _frameCPP-ticket-108_ test_ticket_108.py )
    add_python_test_( _frameCPP-ticket-114_ test_ticket_114.py )
    cl_info_framename( TEST_FRAME ${FRAME_SPEC_LATEST} FULLPATH DESC fr_vect_types )
    add_python_test_( _frgetvect_compat_    test_frgetvect_compat.py ${TEST_FRAME} )
    add_python_test_( _uncertainty_         uncertainty.py )
endfunction( )
