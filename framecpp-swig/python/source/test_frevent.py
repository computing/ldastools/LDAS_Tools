"""! @brief Test script validating the slope value of the FrAdcData structure."""

import unittest
from LDAStools import frameCPP;

# unittest will test all the methods whose name starts with 'test'
class TestFrEvent( unittest.TestCase ):

    #
    # Validation of the method return types
    #
    def test_method_return_types( self ):
        event = frameCPP.FrEvent( )
        self.assertIsInstance( event.GetName( ), str )
        self.assertIsInstance( event.GetComment( ), str )
        self.assertIsInstance( event.GetInputs( ), str )
        self.assertIsInstance( event.GetGTime( ), tuple )
        self.assertIsInstance( event.GetTimeBefore( ), float )
        self.assertIsInstance( event.GetTimeAfter( ), float )
        self.assertIsInstance( event.GetEventStatus( ), int )
        self.assertIsInstance( event.GetAmplitude( ), float )
        self.assertIsInstance( event.GetProbability( ), float )
        self.assertIsInstance( event.GetStatistics( ), str )
        self.assertIsInstance( event.GetParam( ), list )

    #
    # Validation of the method return types
    #
    def test_attribute_types( self ):
        event = frameCPP.FrEvent( )
        self.assertIsInstance( event.name, str )
        self.assertIsInstance( event.comment, str )
        self.assertIsInstance( event.inputs, str )
        self.assertIsInstance( event.GTime, tuple )
        self.assertIsInstance( event.timeBefore, float )
        self.assertIsInstance( event.timeAfter, float )
        self.assertIsInstance( event.eventStatus, int )
        self.assertIsInstance( event.amplitude, float )
        self.assertIsInstance( event.probability, float )
        self.assertIsInstance( event.statistics, str )
        self.assertIsInstance( event.params, list )

    def test_constructor_minimal( self ):
        with self.assertRaises( Warning ):
            frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', [] )
            raise Warning( "Successfully constructed object" )

    # https://git.ligo.org/computing/ldastools/LDAS_Tools/-/issues/146
    def test_constructor_params_list(self):
        params = [
            ("param1", 1.0),
            ("param2", 2.0),
        ]
        e = frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', params)
        self.assertListEqual(params, e.params)

    # https://git.ligo.org/computing/ldastools/LDAS_Tools/-/issues/146
    def test_constructor_params_dict(self):
        params = {
            "param1": 1.0,
            "param2": 2.0,
        }
        e = frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', params)
        self.assertDictEqual(params, dict(e.params))

    # https://git.ligo.org/computing/ldastools/LDAS_Tools/-/issues/146
    def test_constructor_params_tuple(self):
        params = (
            ( "param1", 1.0),
            ( "param2", 2.0),
        )
        e = frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', params)
        self.assertTupleEqual(params, tuple( e.params) )


# running of the tests
if __name__ == '__main__':
    unittest.main( )
