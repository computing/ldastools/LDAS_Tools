from LDAStools import frameCPP;

def test_fr_adc_data():
    #
    # FrAdcData constructors
    #

    channelNumber = 0
    channelGroup = 0
    nBits = 16
    sampleRate = 1024.

    print(frameCPP.FrAdcData( ))
    print(frameCPP.FrAdcData('test_fr_adc_data', channelGroup, channelNumber, nBits, sampleRate))

def test_fr_proc_data( ):
    #
    # FrProcData constructors
    #

    tipe = frameCPP.FrProcData.TIME_SERIES
    subType = 0
    timeOffset = 0.
    tRange = 1.
    fShift = 0.
    phase = 0.
    fRange = 0.
    bw = 0.


    print(frameCPP.FrProcData())
    print(frameCPP.FrProcData('test_fr_proc_data', 'comment', tipe, subType, timeOffset, tRange, fShift, phase, fRange, bw))


test_fr_adc_data( )
test_fr_proc_data( )
