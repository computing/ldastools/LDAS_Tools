#
function(BuildRules
    p_python_prefix_
    p_python_default_version_
    p_python_namespace_
    )
  if ( NOT DEFINED ENABLE_SWIG_${p_python_prefix_} OR ENABLE_SWIG_${p_python_prefix_} )
    if ( NOT ${p_python_prefix_}_VERSION )
      set(${p_python_prefix_}_VERSION ${p_python_default_version_})
    endif ()

    #======================================================================
    # Find python and key modules
    #======================================================================

    cx_python(
      VERSION ${${p_python_prefix_}_VERSION}
      PREFIX ${p_python_prefix_} )
    cx_python_module(
      MODULE numpy
      PREFIX ${p_python_prefix_} )

    if (NOT ${p_python_prefix_}LIBS_FOUND OR NOT ${p_python_prefix_}_NUMPY_INCLUDE_PATH)
      set( ENABLE_SWIG_${p_python_prefix_} FALSE )
      set( ENABLE_SWIG_${p_python_prefix_} ${ENABLE_SWIG_${p_python_prefix_}} PARENT_SCOPE )
    endif( )

    if (${p_python_prefix_}LIBS_FOUND AND ${p_python_prefix_}_NUMPY_INCLUDE_PATH)
      #----------------------------------------------------------------------
      # include some additonal functions/macros
      #----------------------------------------------------------------------

      include( ${CMAKE_SOURCE_DIR}/python/source/testsuite.cmake )

      #----------------------------------------------------------------------
      # All pieces needed have been found
      #----------------------------------------------------------------------

      cm_check_headers(
        PREFIX ${p_python_prefix_}
        numpy/arrayobject.h
        )
      set( ENABLE_SWIG_${p_python_prefix_} "yes"
        CACHE INTERNAL "Enable building of SWIG bindings for Python" )
      message(STATUS "Generating Python ${PYTHON_VERSION_STRING} extensions for ${PROJECT_NAME}")

      #----------------------------------------------------------------------
      # Build the python bindings
      #----------------------------------------------------------------------

      set( INCLUDE_PATHS
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${PROJECT_SOURCE_DIR}/python/source
        ${PROJECT_BINARY_DIR}/python/source
        ${PROJECT_SOURCE_DIR}/common
        ${PROJECT_BINARY_DIR}/common
        ${PROJECT_BINARY_DIR}/include
        ${LDASTOOLS_INCLUDE_DIRS}
        ${${p_python_prefix_}_NUMPY_INCLUDE_PATH}
        ${${p_python_prefix_}_INCLUDE_PATH}
        ${CMAKE_INSTALL_FULL_INCLUDEDIR} )
      unset(DASH_I_INCLUDE_PATHS)
      unset( EXTRA_INCLUDE_PATH )
      if ( "${LDASTOOLS_INCLUDE_DIRS}" STREQUAL "" )
        set( EXTRA_INCLUDE_PATH "/usr/include" )
      endif ( )
      foreach( path ${INCLUDE_PATH} ${EXTRA_INCLUDE_PATH})
        list(APPEND DASH_I_INCLUDE_PATHS "-I${path}")
      endforeach( )

      #--------------------------------------------------------------------
      # Need to reverse the list as the BEFORE directive iterates over
      #   the list, resulting in a reverse order.
      #--------------------------------------------------------------------
      list( REVERSE INCLUDE_PATHS )
      include_directories( BEFORE ${INCLUDE_PATHS} )

      # cx_msg_debug_variable( LDASTOOLS_LIBRARIES_FULL_PATH )

      cx_swig_python_module(
        PREFIX         ${p_python_prefix_}
        INTERFACE_FILE ${PROJECT_BINARY_DIR}/include/framecpp/python/frameCPP.i
        MODULE         frameCPP
        MODULE_DIR     LDAStools
        CXX
        EXTEND_SWIG_FLAGS
        SWIG_FLAGS     ${DASH_I_INCLUDE_PATHS}
        LINK_LIBRARIES ${LDASTOOLS_LIBRARIES_FULL_PATH}
        )

      #====================================================================
      # Do unit tests to verify the interface
      #====================================================================
      do_python_tests(
        PREFIX            ${p_python_prefix_}
        NAMESPACE         ${p_python_namespace_}
        PYTHON_MODULE_DIR ${CMAKE_CURRENT_BINARY_DIR}
        TEST_SOURCE_DIR   ${CMAKE_SOURCE_DIR}/python/source
        )

      #====================================================================
      # Installation of executables
      #====================================================================
      configure_file(
        ${CMAKE_SOURCE_DIR}/python/source/pyframecpp-dump.in
        ${CMAKE_CURRENT_BINARY_DIR}/py${${p_python_prefix_}_VERSION_MAJOR}${${p_python_prefix_}_VERSION_MINOR}-framecpp-dump
        @ONLY
        )
      install(
        PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/py${${p_python_prefix_}_VERSION_MAJOR}${${p_python_prefix_}_VERSION_MINOR}-framecpp-dump
        DESTINATION ${CMAKE_INSTALL_BINDIR}
        )
    else( )
      #--------------------------------------------------------------------
      # All pieces needed have NOT been found
      #--------------------------------------------------------------------
      message(STATUS "Not generating Python 3 extensions for ${PROJECT_NAME}")
    endif( )

  endif ( NOT DEFINED ENABLE_SWIG_${p_python_prefix_} OR ENABLE_SWIG_${p_python_prefix_} )
endfunction( )
