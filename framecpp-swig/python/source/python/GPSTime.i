/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef FRAMECPP_PYTHON_GPS_TIME_I
#define FRAMECPP_PYTHON_GPS_TIME_I

//-----------------------------------------------------------------------
// MapADC_type
//---------------------------------------------------------------------
#if defined(SWIGPYTHON)
#define HAVE_SWIG_TYPEMAP_GPS_TIME
%typemap(in) FrameCPP::GPSTime const & (FrameCPP::GPSTime temp) {
  if (PyTuple_Check($input)) {
    if (PyTuple_Size($input) != 2) {
      PyErr_SetString(PyExc_TypeError, "GPS time tuple must have exactly 2 elements( seconds and nanoseconds)");
      return NULL;
    }
    PyObject *seconds_obj = PyTuple_GetItem($input, 0);
    PyObject *nanoseconds_obj = PyTuple_GetItem($input, 1);
    if (!PyNumber_Check(seconds_obj) || !PyNumber_Check(nanoseconds_obj)) {
      PyErr_SetString(PyExc_TypeError, "Elements of tuple must be numbers");
      return NULL;
    }
    temp = FrameCPP::GPSTime(PyLong_AsUnsignedLong(seconds_obj), PyLong_AsUnsignedLong(nanoseconds_obj));
  } else if (SWIG_ConvertPtr($input, (void **) &temp, $descriptor(FrameCPP::GPSTime *), 0) == -1) {
    PyErr_SetString(PyExc_TypeError, "Parameter must be a tuple of size 2 or a FrameCPP::GPSTime object");
    return NULL;
  }
  $1 = &temp;
 }

%typemap(out) FrameCPP::GPSTime {
  $result = Py_BuildValue( "ii",
                           $1.GetSeconds( ),
                           $1.GetNanoseconds( ) );
 }

%typemap(out) const FrameCPP::GPSTime& {
  $result = Py_BuildValue( "ii",
                           $1->GetSeconds( ),
                           $1->GetNanoseconds( ) );
 }

%typemap(out) FrameCPP::GPSTime* {
  $result = Py_BuildValue( "ii",
                           $1->GetSeconds( ),
                           $1->GetNanoseconds( ) );
 }

%typemap(out) const gtimes_container_type& {

  PyObject* tuple = PyTuple_New( $1->size( ) );
  int tuple_index = 0;

  for ( $1_basetype::const_iterator
          cur = $1->begin( ),
          last = $1->end( );
        cur != last;
        ++cur,
          ++tuple_index )
    {
      PyTuple_SET_ITEM( tuple, tuple_index,
                        PyInt_FromLong( *cur ) );
    }
  $result = tuple;
 }
#endif /* SWIGPYTHON */

#endif /* FRAMECPP_PYTHON_GPS_TIME_I */
