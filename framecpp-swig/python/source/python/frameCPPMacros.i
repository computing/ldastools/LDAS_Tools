/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame
 * specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef SWIG__PYTHON__FRAMECPP_MACROS_I
#define SWIG__PYTHON__FRAMECPP_MACROS_I

#if defined( SWIGPYTHON )

#if !SWIGIMPORTED
%define DOCSTRING
"The frameCPP package for Python allows for Python developers to interface with the frameCPP library."
"The specification on which this interface is based can be found at:"
"https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=329"
%enddef
#endif /* SWIGIMPORTED */

%feature( "autodoc", "1" );
%feature( "docstring" );

// =====================================================================
//  Make sure that no exceptions leak out
// =====================================================================

%define ldas_swig_exception_handler( )
%exception
{
    //---------------------------------------------------------------------
    // Ensure that no exceptions leave the C++ layer
    //---------------------------------------------------------------------
    try
    {
        $action;
    }
    //-------------------------------------------------------------------
    // Derived from std::logic_failure
    //-------------------------------------------------------------------
    catch ( const std::invalid_argument& Exception )
    {
      PyErr_SetString( PyExc_ValueError,
                       const_cast< char* >( Exception.what( ) ) );
      goto fail;
    }
    catch ( const std::out_of_range& Exception )
    {
        PyErr_SetString( PyExc_IndexError,
                         const_cast< char* >( Exception.what( ) ) );
        goto fail;
    }
    //-------------------------------------------------------------------
    // Derived from std::runtime_error
    //-------------------------------------------------------------------
    catch ( const std::overflow_error& Exception )
    {
      PyErr_SetString( PyExc_OverflowError,
                       const_cast< char* >( Exception.what( ) ) );
      goto fail;
    }
    catch ( const std::range_error& Exception )
    {
        PyErr_SetString( PyExc_ValueError,
                         const_cast< char* >( Exception.what( ) ) );
        goto fail;
    }
    //-------------------------------------------------------------------
    // Derived from std::exception
    //-------------------------------------------------------------------
    catch ( const std::bad_alloc& Exception )
    {
        PyErr_SetString( PyExc_MemoryError,
                         const_cast< char* >( Exception.what( ) ) );
        goto fail;
    }
    catch ( const std::runtime_error& Exception )
    {
        PyErr_SetString( PyExc_RuntimeError,
                         const_cast< char* >( Exception.what( ) ) );
        goto fail;
    }
    //-------------------------------------------------------------------
    // Base
    //-------------------------------------------------------------------
    catch ( const std::exception& Exception )
    {
        PyErr_SetString( PyExc_Exception,
                         const_cast< char* >( Exception.what( ) ) );
        goto fail;
    }
    catch ( ... )
    {
        PyErr_SetString( PyExc_Exception,
                         const_cast< char* >( "unknown error" ) );
        goto fail;
    }
}
%enddef

%typemap( in ) IFrameFStream::size_type
{
    $1 = PyInt_AsLong( $input );
}

%typemap( out ) IFrameFStream::size_type
{
    $result = PyInt_FromLong( $1 );
}

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// MAKE_REF
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
%define MAKE_REF( KLASS, BASE, base )
%{
  const KLASS## ::##base##_type &KLASS##_Ref##BASE( KLASS * Source )
  {
    return Source->Ref##BASE( );
  }

  size_t KLASS##_Ref##BASE##Size( KLASS* Source )
  {
    return Source->Ref##BASE( ).size( );
  }
%}

%enddef

 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 // TYPEDEF_CONTAINER
 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
%define TYPEDEF_CONTAINER( KLASS, NAME )
typedef std::vector< boost::shared_ptr< KLASS > > NAME
%enddef

 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 // iterable
 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
%define
iterable( KLASS, Type ) typedef Container< FrVect > Type;
%enddef

#endif /* defined(SWIGPYTHON) */

#endif /* SWIG__PYTHON__FRAMECPP_MACROS_I */
