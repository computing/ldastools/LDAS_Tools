/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef FRAMECPP_PYTHON_PARAMETERS_I
#define FRAMECPP_PYTHON_PARAMETERS_I

// clang-format off
%{
#include <Python.h>

#include <sstream>
#include <type_traits>
%}
// clang-format on

%typemap(in) const Parameters_type & (Parameters_type temp){
  std::cerr << "DEBUG: Trying to parse parameter option" << std::endl;
  //---------------------------------------------------------------------
  // Check the type of input data
  //---------------------------------------------------------------------
  if ( PyList_Check( $input ) )
  {
    std::cerr << "DEBUG: Parameter option is a list" << std::endl;
    //-------------------------------------------------------------------
    // Should be easy enough as we have a simple list
    //-------------------------------------------------------------------
    bool	translated = true;

    for ( int cur = 0,
            last = PyList_Size( $input );
          cur != last;
          ++cur )
    {
      PyObject*	parameters_object = PyList_GetItem( $input, cur );
      PyObject* key;
      PyObject* value;

      if ( PyTuple_Check( parameters_object )
           && ( PyTuple_Size( parameters_object ) == 2 )
           && ( PyUnicode_Check( key = PyTuple_GetItem( parameters_object, 0 ) ) )
           && ( PyFloat_Check( value = PyTuple_GetItem( parameters_object, 1 ) ) ) )
      {
        const char*	n = PyUnicode_AsUTF8( key );
        double	v = PyFloat_AsDouble( value );

        temp.push_back( $1_basetype::value_type( n, v ) );
      }
      else
      {
        std::cerr << "FAIL: Unable to parse element " << cur << " of list" << std::endl;
        translated = false;
        break;
      }
    }
    if ( translated )
    {
      $1 = &temp;
    }
  }
  else if ( PyDict_Check( $input ) )
  {
    std::cerr << "DEBUG: Parameter option is a dict" << std::endl;
    //-------------------------------------------------------------------
    // Dictionaries are handled as hash maps with unique keys.
    // There is no guarantee of the order of the data.
    //-------------------------------------------------------------------
    bool	translated = true;
    PyObject	*key;
    PyObject	*value;

    for ( Py_ssize_t cur = 0;
          PyDict_Next( $input, &cur, &key, &value );
          )
    {
      if ( PyUnicode_Check( key )
           && ( PyFloat_Check( value ) ) )
      {
        const char*	n = PyUnicode_AsUTF8( key );
        double	v = PyFloat_AsDouble( value );

        temp.push_back( $1_basetype::value_type( n, v ) );
      }
      else
      {
        translated = false;
        break;
      }
    }
    if ( translated )
    {
      $1 = &temp;
    }
  }
  else if ( PyTuple_Check( $input ) )
  {
    std::cerr << "DEBUG: Parameter option is a tuple" << std::endl;
    //-------------------------------------------------------------------
    // This is a tuple.
    //-------------------------------------------------------------------
    bool	translated = true;

    for ( int cur = 0,
            last = PyTuple_Size( $input );
          cur != last;
          ++cur )
      {
        PyObject*	parameters_object = PyTuple_GetItem( $input, cur );
        PyObject* key;
        PyObject* value;

        if ( PyTuple_Check( parameters_object )
             && ( PyTuple_Size( parameters_object ) == 2 )
             && ( PyUnicode_Check( key = PyTuple_GetItem( parameters_object, 0 ) ) )
             && ( PyFloat_Check( value = PyTuple_GetItem( parameters_object, 1 ) ) ) )
          {
            const char*	n = PyUnicode_AsUTF8( key );
            double	v = PyFloat_AsDouble( value );

            temp.push_back( $1_basetype::value_type( n, v ) );
          }
        else
          {
            translated = false;
            break;
          }
      }
    if ( translated )
      {
        $1 = &temp;
      }
  }
  if ( ! $1 )
  {
    std::cerr << "DEBUG: Unable to translate parameter option" << std::endl;
    std::ostringstream msg;

    msg << "Unable to translate parameter to Parameters_type";
    PyErr_SetString( PyExc_TypeError, msg.str( ).c_str( ) );
    return NULL;
  }
}

%typemap(out) const Parameters_type & {
  std::cerr << "DEBUG: Parameter out as py list" << std::endl;
  PyObject* parameters_list = PyList_New( $1->size( ) );
  Py_ssize_t index = 0;

  for ( auto
	  cur = $1->begin( ),
	  last = $1->end( );
	cur != last;
	++cur )
  {
    PyObject* name = PyUnicode_FromString( cur->first.c_str( ) );
    PyObject* value = PyFloat_FromDouble( cur->second );
    PyObject* parameter = PyTuple_New( 2 );

    PyTuple_SetItem( parameter, 0, name );
    PyTuple_SetItem( parameter, 1, value );

    PyList_SetItem( parameters_list, index++, parameter );
  }
  std::cerr << "DEBUG: result set to parameters_list" << std::endl;
  $result = parameters_list;
}

#endif /* FRAMECPP_PYTHON_PARAMETERS_I */
