
/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef FR_VECT__PYTHON_I
#define FR_VECT__PYTHON_I

#ifndef SWIGIMPORTED

%begin %{
  extern "C"
  {
#define NPY_NO_DEPRECATED_API NPY_API_VERSION

#include "framecpp_swig_config.h"

#include <Python.h>

#if HAVE_NUMPY_ARRAYOBJECT_H
#include <numpy/arrayobject.h>
#endif /* HAVE_NUMPY_ARRAYOBJECT_H */
  } /* extern "C" */
%}

%init %{
#if HAVE_NUMPY_ARRAYOBJECT_H
#include <numpy/arrayobject.h>
#endif /* HAVE_NUMPY_ARRAYOBJECT_H */

  import_array( );
%}

%{
#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"

#include "framecpp/FrVect.hh"

#if HAVE_NUMPY_ARRAYOBJECT_H
#include "numpy/npy_common.h"
#include "numpy/arrayobject.h"
#endif /* HAVE_NUMPY_ARRAYOBJECT_H */

  using namespace FrameCPP;
  using boost::shared_ptr;
  using std::vector;

  extern "C" {
    PyObject* get_buffer( FrVect* Self );
  } /* extern "C" */

  PyObject* get_buffer( FrVect* Self )
  {
    PyObject*		retval = (PyObject*)NULL;
    FrVect::data_type	data = Self->GetDataUncompressed( );

#if HAVE_NUMPY_ARRAYOBJECT_H
    //-------------------------------------------------------------------
    // Interface: NumPy
    //-------------------------------------------------------------------
    {
      int typenum = 0;
      npy_intp*	dims = (npy_intp*)NULL;

      try
      {
	//---------------------------------------------------------------
	// Establish the dimensionality of the array
	//---------------------------------------------------------------
	int	nd( Self->GetNDim( ) );

	dims = PyDimMem_NEW( nd );
	if ( dims )
	{
	  //-------------------------------------------------------------
	  // Populate the dimension array
	  //-------------------------------------------------------------
	  for ( int x = 0; x < nd; ++x )
	  {
	    dims[ x ] = Self->GetDim( x ).GetNx( );
	  }
	  //-------------------------------------------------------------
	  // Map the datatype
	  //-------------------------------------------------------------
	  switch( Self->GetType( ) )
	  {
	    //-----------------------------------------------------------
	    // Integers
	    //-----------------------------------------------------------
	  case FrVect::FR_VECT_C:	// FR_VECT_C
	    typenum = NPY_INT8;
	    break;
	  case FrVect::FR_VECT_1U:	// FR_VECT_1U
	    typenum = NPY_UINT8;
	    break;
	  case FrVect::FR_VECT_2S:	// FR_VECT_2S
	    typenum = NPY_INT16;
	    break;
	  case FrVect::FR_VECT_2U:	// FR_VECT_2U
	    typenum = NPY_UINT16;
	    break;
	  case FrVect::FR_VECT_4S:	// FR_VECT_4S
	    typenum = NPY_INT32;
	    break;
	  case FrVect::FR_VECT_4U:	// FR_VECT_4U
	    typenum = NPY_UINT32;
	    break;
	  case FrVect::FR_VECT_8S:	// FR_VECT_8S
	    typenum = NPY_INT64;
	    break;
	  case FrVect::FR_VECT_8U:	// FR_VECT_8U
	    typenum = NPY_UINT64;
	    break;
	    //-----------------------------------------------------------
	    // Real
	    //-----------------------------------------------------------
	  case FrVect::FR_VECT_4R:	// FR_VECT_4R
	    typenum = NPY_FLOAT32;
	    break;
	  case FrVect::FR_VECT_8R:	// FR_VECT_8R
	    typenum = NPY_FLOAT64;
	    break;
	    //-----------------------------------------------------------
	    // Complex
	    //-----------------------------------------------------------
	  case FrVect::FR_VECT_8C:	// FR_VECT_8C
	    typenum = NPY_COMPLEX64;
	    break;
	  case FrVect::FR_VECT_16C:	// FR_VECT_16C
	    typenum = NPY_COMPLEX128;
	    break;
	  default:
	    //-----------------------------------------------------------
	    // It has not been recognized.
	    //-----------------------------------------------------------
            typenum = 0;
	    break;
	  }
	  retval = PyArray_SimpleNewFromData( nd, dims, typenum, data.get( ) );
	}
      }
      catch( ... )
      {
	if ( dims )
	{
	  PyDimMem_FREE( dims );
	}
	dims = (npy_intp*)NULL;
	throw;
      }
    }
#else /* HAVE_NUMPY_ARRAYOBJECT_H */
    //-------------------------------------------------------------------
    // Interface: Simple buffer interface
    //-------------------------------------------------------------------
#if PY_VERSION_HEX >= 0x03030000
    /* #error New buffer interface not yet supported */
    retval = PyMemoryView_FromMemory( data.get( ), Self->GetNBytes( ),
				      PyBUF_WRITE );
#else /* PY_VERSION_HEX */
    //-------------------------------------------------------------------
    // This is the older buffer interface that is being phased out
    //-------------------------------------------------------------------
    retval = PyBuffer_FromReadWriteMemory( data.get( ),
					   Self->GetNBytes( ) );
#endif /* PY_VERSION_HEX */
#endif /* HAVE_NUMPY_ARRAYOBJECT_H */
    return retval;
  }
%}
#endif /* ! SWIGIMPORTED */

// %ignore FrVect::GetDataUncompressed();
#define FR_VECT_GET_DATA_UNCOMPRESSED_OVERRIDE
#if defined( FR_VECT_GET_DATA_UNCOMPRESSED_OVERRIDE )
%extend FrVect {
  PyObject* GetDataUncompressed( )
  {
    PyObject*	retval = get_buffer( self );

    return retval;
  }

}
#endif /* defined(FR_VECT_GET_DATA_UNCOMPRESSED_OVERRIDE) */

%extend FrVect {

  //-------------------------------------------------------------------
  /// \brief Retrieve the memory associated with FrVect as an Array
  //-------------------------------------------------------------------
  PyObject* GetDataArray( )
  {
    PyObject*	retval = get_buffer( self );

    return retval;
  }

};

#endif /* FR_VECT__PYTHON_I */

