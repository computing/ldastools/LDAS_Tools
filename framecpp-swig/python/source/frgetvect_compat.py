# Copyright (C) 2012  Nickolas Fotopoulos
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from __future__ import division

from LDAStools import frameCPP
import numpy as np

__author__ = "Nickolas Fotopoulos <nickolas.fotopoulos@ligo.org>"

#------------------------------------------------------------------------
# Provide backwards compatability for Python 2 vs 3
#------------------------------------------------------------------------
try:
    xrange
except NameError:
    xrange = range

#
# Functions to navigate the filestream
#
def iter_channel(stream, channame):
    """
    Return an iterator of (offset, vector) for vectors matching a
    given channel name.
    """
    toc = stream.GetTOC()

    # grab vectors from ProcData
    toc_proc = toc.GetProc()
    if channame in toc_proc:
        for frame_num in xrange(toc.GetNFrame()):
            proc = stream.ReadFrProcData(frame_num, channame)
            for v in proc.data:
                yield v

    # grab vectors from AdcData
    toc_adc = toc.GetADC()
    if channame in toc_adc:
        for frame_num in xrange(toc.GetNFrame()):
            adc = stream.ReadFrAdcData(frame_num, channame)
            for v in adc.data:
                yield v

#
# The high-level functions that end-users will call
#
def frgetvect1d(filename, channel, start=None, span=None):
    fs = frameCPP.IFrameFStream(filename)
    toc = fs.GetTOC()
    vects = list(iter_channel(fs, channel))

    if not vects:
        raise ValueError("In file %s, vector %s not found." % (filename, channel))

    # now filter to vectors that overlap the requested region
    offset = toc.GetGTimeS()[0] + toc.GetGTimeN()[0] * 1e-9
    sev_list = []  # each row will be (start, end, vector)
    for vect in vects:
        if vect.nDim != 1:
            raise ValueError("Multiple dimensions are not supported.")
        dim = vect.GetDim(0)
        startX = dim.startX + offset
        vect_end = startX + dim.dx * dim.nx
        if (start is None or vect_end > start) and (span is None or startX < start + span):
            sev_list.append((startX, vect_end, vect))
    if len(sev_list) == 0:
        raise ValueError("In file %s, vector %s not found for start %10.1f and span %0.1f." \
            % (filename, channel, start, span))
    sev_list.sort()  # sorts by time first
    if start is None:
        start = sev_list[0][0]
    if span is None:
        end = sev_list[-1][1]
    else:
        end = start + span

    # sanity check
    if start < sev_list[0][0] or end > sev_list[-1][1]:
        raise ValueError("In file %s, frames do not fully cover %s for start %10.1f and span %0.1f." \
            % (filename, channel, start or -np.inf, span or np.inf))

    # extract useful quantities
    vects = [vect for _, _, vect in sev_list]
    dim = vects[0].GetDim(0)
    dx = dim.dx
    true_start = int(round((start - dim.startX) / dx)) * dx + dim.startX
    true_end = int(round((end - vects[-1].GetDim(0).startX) / dx)) * dx + vects[-1].GetDim(0).startX
    expected_num_samples = int(round((true_end - true_start) / dx))

    print( start, end, true_start, true_end, expected_num_samples )

    # sanity checks
    if not all(vect.GetDim(0).dx == dx for vect in vects):
        raise ValueError("differing sample rates detected")
    for (_, old_end, _), (new_start, _, _) in zip(sev_list, sev_list[1:]):
        if old_end != new_start:
            raise ValueError("The vectors do not form a contiguous block. "\
                "There is a gap between %f and %f." % (old_end, new_start))

    # extract data
    # NB: extract first array separately to get dtype for the output array
    now = start
    offset = 0
    v_start, v_end, vect = sev_list[0]
    first_array = vect.GetDataArray()
    start_ind = int(round((now - v_start) / dx))
    end_ind = int(round((min(v_end, end) - v_start) / dx))
    out_data = np.zeros(expected_num_samples, dtype=first_array.dtype)
    out_data[:end_ind - start_ind] = first_array[start_ind:end_ind]
    del first_array
    now = min(v_end, end)
    offset += end_ind - start_ind
    for v_start, v_end, vect in sev_list[1:]:
        start_ind = int(round((now - v_start) / dx))
        assert start_ind >= 0, "gap detected"
        end_ind = int(round((min(v_end, end) - v_start) / dx))
        out_data[offset:offset + end_ind - start_ind + 1] = vect.GetDataArray()[start_ind:end_ind]
        now = min(v_end, end)
        offset += end_ind - start_ind
        if offset == expected_num_samples:
            break
    assert offset == expected_num_samples

    return (out_data, true_start, 0.0, dim.dx, dim.unitX, vect.unitY)
