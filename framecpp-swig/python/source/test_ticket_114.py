"""! @brief Test script validating the slope value of the FrAdcData structure."""

import unittest
from LDAStools import frameCPP;

exit_status = 0

# unittest will test all the methods whose name starts with 'test'
class Ticket114( unittest.TestCase ):
    EXPECTED_DEFAULT_SLOPE = 1.0

    def test_default_slope_from_member( self ):
        self.assertEqual( frameCPP.FrAdcData( ).slope,
                          self.EXPECTED_DEFAULT_SLOPE )

    def test_default_slope_from_get_method( self ):
        self.assertEqual( frameCPP.FrAdcData( ).GetSlope( ),
                          self.EXPECTED_DEFAULT_SLOPE )

# running of the tests
unittest.main( )
