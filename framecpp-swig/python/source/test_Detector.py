#! /usr/bin/env python

# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import unittest
from LDAStools import frameCPP

class DetectorTest( unittest.TestCase ):
    def test_detector_instantiation( self ):
        self.assertEqual( frameCPP.FrDetector.GetDetector( frameCPP.FrDetector.DQO_GEO_600 ).name,
                          'GEO_600' )
        self.assertEqual( frameCPP.FrDetector.GetDetector( frameCPP.FrDetector.DQO_LHO_4K ).name,
                          'LHO_4k' )
        self.assertEqual( frameCPP.FrDetector.GetDetector( frameCPP.FrDetector.DQO_KAGRA ).name,
                          'KAGRA' )
        self.assertEqual( frameCPP.FrDetector.GetDetector( frameCPP.FrDetector.DQO_LLO_4K ).name,
                          'LLO_4k' )
        self.assertEqual( frameCPP.FrDetector.GetDetector( frameCPP.FrDetector.DQO_TAMA_300 ).name,
                          'TAMA_300' )
        self.assertEqual( frameCPP.FrDetector.GetDetector( frameCPP.FrDetector.DQO_VIRGO ).name,
                          'Virgo' )

    def test_detector_constructor( self ):
        name = "Test"
        prefix = "Z1"
        longitude = 1.2
        latitude = 1.4
        elevation = 1.6
        armXazimuth = 2.2
        armYazimuth = 2.4
        armXaltitude = 3.2
        armYaltitude = 3.4
        armXmidpoint = 4.2
        armYmidpoint = 4.4;

        d = frameCPP.FrDetector( name,
                                 prefix,
                                 longitude, latitude,
                                 elevation,
                                 armXazimuth, armYazimuth,
                                 armXaltitude, armYaltitude,
                                 armXmidpoint, armYmidpoint )

        self.assertEqual( d.name, name )
        self.assertEqual(  d.prefix[0], prefix[0] )
        self.assertEqual(  d.prefix[1], prefix[1] )
        self.assertAlmostEqual( d.longitude, longitude, places=6 )
        self.assertAlmostEqual( d.latitude, latitude, places=6 )
        self.assertAlmostEqual( d.elevation, elevation, places=6 )
        self.assertAlmostEqual( d.armXazimuth, armXazimuth, places=6 )
        self.assertAlmostEqual( d.armYazimuth, armYazimuth, places=6 )
        self.assertAlmostEqual( d.armXaltitude, armXaltitude, places=6 )
        self.assertAlmostEqual( d.armYaltitude, armYaltitude, places=6 )
        self.assertAlmostEqual( d.armXmidpoint, armXmidpoint, places=6 )
        self.assertAlmostEqual( d.armYmidpoint, armYmidpoint, places=6 )

if __name__ == '__main__':
    unittest.main( )
