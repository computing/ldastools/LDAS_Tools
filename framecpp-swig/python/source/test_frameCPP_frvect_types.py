#! /usr/bin/env python

# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

##
#

#------------------------------------------------------------------------
# Test the reading of different types of FrVects from frames
#------------------------------------------------------------------------
from __future__ import division

import sys

from LDAStools import frameCPP
import numpy as np

__author__ = "Edward Maros <ed.maros@ligo.org>"

class frame:
    #--------------------------------------------------------------------
    # Set of channels and their expected numpy data types
    #--------------------------------------------------------------------
    channel_set = ( # [ 'Z0:RAMPED_COMPLEX_16_1', np.complex128 ],
                    # [ 'Z0:RAMPED_COMPLEX_8_1', np.complex64 ],
                    [ 'Z0:RAMPED_INT_2S_1', np.int16 ],
                    [ 'Z0:RAMPED_INT_2U_1', np.uint16 ],
                    [ 'Z0:RAMPED_INT_4S_1', np.int32 ],
                    [ 'Z0:RAMPED_INT_4U_1', np.uint32 ],
                    [ 'Z0:RAMPED_INT_8S_1', np.int64 ],
                    [ 'Z0:RAMPED_INT_8U_1', np.uint64 ],
                    [ 'Z0:RAMPED_REAL_4_1', np.float32 ],
                    [ 'Z0:RAMPED_REAL_8_1', np.float64 ] )

    #--------------------------------------------------------------------
    # Constructor taking the name of the frame file
    #--------------------------------------------------------------------
    def __init__(self, Filename):
        self.fs = frameCPP.IFrameFStream( Filename )

    #--------------------------------------------------------------------
    # Destructor to ensure things are properly cleaned
    #--------------------------------------------------------------------
    def __del__(self):
        self.fs = None

    #--------------------------------------------------------------------
    # Iterate over channels
    #--------------------------------------------------------------------
    def check(self):
        retval = True
        for c in ( self.channel_set ):
            retval = self.verifyChannelType( c[0], c[1] ) and retval
        return retval

    #--------------------------------------------------------------------
    # Read the channel and verify the data type of the numpy array
    #--------------------------------------------------------------------
    def verifyChannelType(self, ChannelName, ChannelType ):
        retval = True
        adc = self.fs.ReadFrAdcData(0, ChannelName)
        print( "Read FrAdcData: %s data size: %d" % (adc.name, len(adc.data)) )
        retval = ( adc.data[ 0 ].GetDataArray( ).dtype.type == ChannelType )
        return retval

if __name__ == "__main__":
    #--------------------------------------------------------------------
    # This is a simple script to verify that the C++ interface for
    #   Python generates the expected data type numpy arrays.
    #--------------------------------------------------------------------
    retval = True
    f = frame( sys.argv[ 1 ] )
    retval = f.check( ) and retval
    if retval:
        exit( 0 )
    else:
        exit( 1 )
