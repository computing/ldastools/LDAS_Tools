#! /usr/bin/env python

# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
from LDAStools import frameCPP
import numpy

__author__ = "Edward Maros <ed.maros@ligo.org>"

epoc = frameCPP.GPSTime( 1234567890, 0 )
samples = 16384;
sampleRate = 1 / samples
dt = 16.0
f0 = 0.0
df = 0.5
f_range = f0 + df * ( samples + 1 )

# generate fake data
#
# FrVect
#
dim = frameCPP.Dimension( samples, sampleRate, "m", 0.0 )
channel_data = frameCPP.FrVect( "UncertaintyFreq", # Name
                                frameCPP.FrVect.FR_VECT_8R, # Data Type
                                1, dim, #
                                "" # UnitY
                                )
#
# data_array
#
data_array = channel_data.GetDataArray( );
data_array = numpy.random.random( samples );
channel = frameCPP.FrProcData( "UncertaintyFreq", # Name
                               "How to create an uncertainty frequency channel", # Comment
                               frameCPP.FrProcData.FREQUENCY_SERIES, # Type
                               frameCPP.FrProcData.TRANSFER_FUNCTION, # SubType
                               0, # TimeOffset
                               dt, # TRange
                               0.0, # FShift
                               0.0, # Phase
                               f_range, # FRange
                               0 # BW
                              )
channel.AppendData( channel_data )

# write to GWF
frame = frameCPP.FrameH( )
frame.SetName( 'LIGO' )
frame.SetRun( 4 )
frame.SetGTime( epoc )
frame.SetDt( dt )
frame.AppendFrProcData( channel )

#--------------------------------------------------------------------
# Create the file
#--------------------------------------------------------------------
filename = 'Z-Frequency_Example-%d-%d.gwf' % ( epoc.GetSeconds( ), dt )
framestream = frameCPP.OFrameFStream( filename )
framestream.WriteFrame( frame,
                        frameCPP.FrVect.RAW,
                        0 )
