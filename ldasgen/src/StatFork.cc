//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#include <iostream>
#include <sstream>

#include "ldastoolsal/Directory.hh"

#include "genericAPI/StatFork.hh"
#include "genericAPI/StatStream.hh"

static const char* class_name = "StatFork";

namespace GenericAPI
{
    StatFork::StatFork( ) : m_cmd( LIBEXECDIR "/ldas/lstat" )
    {
    }

    StatFork*
    StatFork::vnew( ) const
    {
        return new StatFork( );
    }

    std::string
    StatFork::Debug( debug_info DebugInfo ) const
    {
        std::ostringstream msg;

        switch ( DebugInfo )
        {
        case STAT_DEBUG_GENERAL:
        default:
            msg << class_name << ": " << (void*)this
                << " ChildPid: " << ChildPid( );
        }

        return msg.str( );
    }

    void
    StatFork::Init( )
    {
        Spawn( );
    }

    int
    StatFork::LStat( const std::string& Filename, struct stat& Buf ) const
    {
        if ( isParent( ) )
        {
            StatStream stream;

            return stream.Request( getStdOut( ), getStdIn( ), Filename, Buf );
        }
        errno = EFAULT;
        return -1;
    }

    int
    StatFork::LStat( const directory_type& Dir,
                     const std::string&    RelFilename,
                     stat_buf_type&        Buf ) const
    {
        std::string filename( Dir.Name( ) );

        filename += "/" + RelFilename;
        return LStat( filename, Buf );
    }

    int
    StatFork::Stat( const std::string& Filename, struct stat& Buf ) const
    {
        if ( isParent( ) )
        {
            StatStream stream;

            return stream.Request( getStdOut( ), getStdIn( ), Filename, Buf );
        }
        errno = EFAULT;
        return -1;
    }

    int
    StatFork::Stat( const directory_type& Dir,
                    const std::string&    RelFilename,
                    stat_buf_type&        Buf ) const
    {
        std::string filename( Dir.Name( ) );

        filename += "/" + RelFilename;
        return Stat( filename, Buf );
    }

    void
    StatFork::evalChild( )
    {
        //-------------------------------------------------------------------
        // Replace stdin with pipe
        //-------------------------------------------------------------------
        close( 0 ); // Close stdin
        if ( dup( getStdIn( ) ) == -1 )
        {
            _exit( 1 );
        }
        close( getStdIn( ) );

        //-------------------------------------------------------------------
        // Replace stdout with pipe
        //-------------------------------------------------------------------
        close( 1 ); // Close stdout
        if ( dup( getStdOut( ) ) == -1 )
        {
            _exit( 1 );
        }
        close( getStdOut( ) );

        //-------------------------------------------------------------------
        // Need to start the lstat process
        //-------------------------------------------------------------------
        execl( m_cmd.c_str( ), m_cmd.c_str( ), (const char*)NULL );
    }

    void
    StatFork::evalParent( )
    {
        //-------------------------------------------------------------------
        // Nothing to do.
        //-------------------------------------------------------------------
    }
} // namespace GenericAPI
