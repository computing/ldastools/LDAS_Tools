//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__SYMBOLS_HH
#define GENERIC_API__SYMBOLS_HH

#include <string>

#include "ldastoolsal/types.hh"
#include "ldastoolsal/mutexlock.hh"

//---------------------------------------------------------------------------
// THIS IS A TEMPLATE FOR DOCUMENTATION
//---------------------------------------------------------------------------
// \class <class_name>
//
// \brief This provides thread safe methods for the global variable.
//
// This class provides the necessary wrapping of the global variable to
// allow it to be used in a multi-threaded environment in a thread safe
// manor.
//---------------------------------------------------------------------------
// \typedef <class_name>::vtype value_type
//
// \brief The type of the global variable.
//
// This is useful for code portability.
// Functions and mehtods that need access to this global variable,
// should use this typdef when reserving space for the Get method.
//---------------------------------------------------------------------------
// \fn static void <class_name>::Get( value_type& Value )
//
// \brief Retrieve the value of the global variable.
//
// This method retrieve the value of the global variable
// in a thread safe manor.
//
// \param[out] Value
//     Storage reserved by the caller to receive a copy of the global
//     variable.
//---------------------------------------------------------------------------
// \fn static void <class_name>::Set( value_type& Value )
//
// \brief Retrieve the value of the global variable.
//
// This method sets the value of the global variable
// in a thread safe manor.
//
// \param[in] Value
//     The new value for the global variable.
//---------------------------------------------------------------------------
#define SYMBOL_CLASS_DECL( class_name, vtype )                                 \
    class class_name                                                           \
    {                                                                          \
    public:                                                                    \
        typedef vtype value_type;                                              \
                                                                               \
        inline static void                                                     \
        Get( value_type& Value )                                               \
        {                                                                      \
            return m_var_info.Get( Value );                                    \
        }                                                                      \
        inline static void                                                     \
        Set( const value_type& Value )                                         \
        {                                                                      \
            return m_var_info.Set( Value );                                    \
        }                                                                      \
                                                                               \
    private:                                                                   \
        typedef GenericAPI::Symbols::Storage< value_type > storage_type;       \
                                                                               \
        static storage_type m_var_info;                                        \
    }

#define SYMBOL_CLASS_DECL_BY_VALUE( class_name, vtype )                        \
    class class_name                                                           \
    {                                                                          \
    public:                                                                    \
        typedef vtype value_type;                                              \
                                                                               \
        inline static void                                                     \
        Get( value_type& Value )                                               \
        {                                                                      \
            return m_var_info.Get( Value );                                    \
        }                                                                      \
        inline static void                                                     \
        Set( const value_type Value )                                          \
        {                                                                      \
            return m_var_info.Set( Value );                                    \
        }                                                                      \
                                                                               \
    private:                                                                   \
        typedef GenericAPI::Symbols::Storage< value_type > storage_type;       \
                                                                               \
        static storage_type m_var_info;                                        \
    }

#define SYMBOL_CLASS_INIT( class_name, value )                                 \
    class_name::storage_type class_name::m_var_info =                          \
        class_name::value_type( value )

namespace GenericAPI
{
    void APINameGet( std::string& Name );
    void APINameSet( const std::string& Name );

    void EMailNotifyGet( const std::string& Name, std::string& Value );
    void EMailNotifySet( const std::string& Name, const std::string& Value );

    namespace Symbols
    {
        template < typename storage_type >
        class symbol
        {
        public:
            typedef storage_type value_type;

            static void
            Get( value_type& Value )
            {
                LDASTools::AL::MutexLock l( m_baton, __FILE__, __LINE__ );

                Value = m_value;
            }

            static void
            Set( const value_type& Value )
            {
                LDASTools::AL::MutexLock l( m_baton, __FILE__, __LINE__ );

                m_value = Value;
            }

        private:
            static LDASTools::AL::MutexLock::baton_type m_baton;
            static value_type                           m_value;
        };

        template < typename value_type >
        struct Storage
        {
            typedef value_type storage_type;

            inline Storage( storage_type Value )
                : m_baton( __FILE__, __LINE__ ), m_value( Value )
            {
            }

            inline ~Storage( )
            {
            }

            inline const Storage&
            operator=( storage_type Value )
            {
                Set( Value );
                return *this;
            }

            inline void
            Get( storage_type& Value ) const
            {
                LDASTools::AL::MutexLock l( m_baton, __FILE__, __LINE__ );

                Value = m_value;
            }

            inline void
            Set( const storage_type& Value )
            {
                LDASTools::AL::MutexLock l( m_baton, __FILE__, __LINE__ );

                m_value = Value;
            }

            mutable LDASTools::AL::MutexLock::baton_type m_baton;
            storage_type                                 m_value;
        };

        template <>
        struct Storage< INT_4U >
        {
            typedef INT_4U storage_type;

            inline Storage( storage_type Value ) : m_baton( ), m_value( Value )
            {
            }

            inline const Storage&
            operator=( storage_type Value )
            {
                Set( Value );
                return *this;
            }

            inline void
            Get( storage_type& Value ) const
            {
                LDASTools::AL::MutexLock l( m_baton, __FILE__, __LINE__ );

                Value = m_value;
            }

            inline void
            Set( const storage_type Value )
            {
                LDASTools::AL::MutexLock l( m_baton, __FILE__, __LINE__ );

                m_value = Value;
            }

            mutable LDASTools::AL::MutexLock::baton_type m_baton;
            storage_type                                 m_value;
        };

        class HTTPUrl
        {
        public:
            typedef std::string value_type;

        private:
            static Storage< value_type > m_var_info;
        };

        SYMBOL_CLASS_DECL( HTTP_URL, std::string );
        SYMBOL_CLASS_DECL( LDAS_ARCHIVE_DIR, std::string );
        SYMBOL_CLASS_DECL( LDAS_LOG_DIR, std::string );
        SYMBOL_CLASS_DECL( LDAS_MANAGER_HOST, std::string );
        SYMBOL_CLASS_DECL( LDAS_MANAGER_KEY, std::string );
        SYMBOL_CLASS_DECL_BY_VALUE( LDAS_MANAGER_PORT_EMERGENCY, INT_4U );
        SYMBOL_CLASS_DECL( LDAS_SYSTEM, std::string );
        SYMBOL_CLASS_DECL( RUN_CODE, std::string );
    } // namespace Symbols

} // namespace GenericAPI

#endif /* GENERIC_API__SYMBOLS_HH */
