//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string>
#include <sstream>

#include "ldastoolsal/Directory.hh"

#include "genericAPI/MountPointStatus.hh"
#include "genericAPI/StatDirect.hh"

#ifndef AT_SYMLINK_NOFOLLOW
#undef HAVE_FSTATAT
#define HAVE_FSTATAT 0
#endif /* AT_SYMLINK_NOFOLLOW */

static const char* class_name = "StatDirect";
using GenericAPI::MountPointStatus;

namespace GenericAPI
{
    typedef struct stat stat_buf;

    StatDirect::StatDirect( )
    {
    }

    StatDirect::~StatDirect( )
    {
    }

    void
    StatDirect::Init( )
    {
    }

    std::string
    StatDirect::Debug( debug_info DebugInfo ) const
    {
        std::ostringstream msg;

        switch ( DebugInfo )
        {
        case STAT_DEBUG_GENERAL:
        default:
            msg << class_name << ": " << (void*)this;
        }

        return msg.str( );
    }

    int
    StatDirect::LStat( const std::string& Filename, stat_buf_type& Buf ) const
    {
        //-------------------------------------------------------------------
        // Check the availability of the filesystem
        //-------------------------------------------------------------------
        if ( MountPointStatus::Status( Filename ) ==
             MountPointStatus::STATE_OFFLINE )
        {
            errno = EINTR;
            return -1;
        }
        //-------------------------------------------------------------------
        // Actually lstat the file
        //-------------------------------------------------------------------
        return lstat( Filename.c_str( ), &Buf );
    }

    int
    StatDirect::LStat( const directory_type& Dir,
                       const std::string&    RelFilename,
                       stat_buf_type&        Buf ) const
    {
        //-------------------------------------------------------------------
        // Check the availability of the filesystem
        //-------------------------------------------------------------------
        std::string filename( Dir.Name( ) );

        filename += "/" + RelFilename;
        if ( MountPointStatus::Status( filename ) ==
             MountPointStatus::STATE_OFFLINE )
        {
            errno = EINTR;
            return -1;
        }
        //-------------------------------------------------------------------
        // Actually lstat the file
        //-------------------------------------------------------------------
#if HAVE_FSTATAT
        int fd = Dir.Fd( );

        if ( fd < 0 )
        {
            return lstat( filename.c_str( ), &Buf );
        }
        else if ( RelFilename.length( ) <= 0 )
        {
            // Get stat info about the current file descriptor
            return fstat( fd, &Buf );
        }
        else
        {
            return fstatat(
                fd, RelFilename.c_str( ), &Buf, AT_SYMLINK_NOFOLLOW );
        }
#else /* HAVE_FSTATAT */
        return lstat( filename.c_str( ), &Buf );
#endif /* HAVE_FSTATAT */
    }

    int
    StatDirect::Stat( const std::string& Filename, stat_buf_type& Buf ) const
    {
        //-------------------------------------------------------------------
        // Check the availability of the filesystem
        //-------------------------------------------------------------------
        if ( MountPointStatus::Status( Filename ) ==
             MountPointStatus::STATE_OFFLINE )
        {
            errno = EINTR;
            return -1;
        }
        //-------------------------------------------------------------------
        // Actually stat the file
        //-------------------------------------------------------------------
        return stat( Filename.c_str( ), &Buf );
    }

    int
    StatDirect::Stat( const directory_type& Dir,
                      const std::string&    RelFilename,
                      stat_buf_type&        Buf ) const
    {
        //-------------------------------------------------------------------
        // Check the availability of the filesystem
        //-------------------------------------------------------------------
        std::string filename( Dir.Name( ) );

        filename += "/" + RelFilename;
        if ( MountPointStatus::Status( filename ) ==
             MountPointStatus::STATE_OFFLINE )
        {
            errno = EINTR;
            return -1;
        }
        //-------------------------------------------------------------------
        // Actually stat the file
        //-------------------------------------------------------------------
#if HAVE_FSTATAT
        int fd = Dir.Fd( );

        if ( fd < 0 )
        {
            std::string filename( Dir.Name( ) );

            filename += "/" + RelFilename;
            return stat( filename.c_str( ), &Buf );
        }
        else if ( RelFilename.length( ) <= 0 )
        {
            // Get stat info about the current file descriptor
            return fstat( fd, &Buf );
        }
        else
        {
            return fstatat( fd, RelFilename.c_str( ), &Buf, 0 );
        }
#else /* HAVE_FSTATAT */
        return stat( filename.c_str( ), &Buf );
#endif /* HAVE_FSTATAT */
    }

    StatDirect*
    StatDirect::vnew( ) const
    {
        return new StatDirect( );
    }
} // namespace GenericAPI
