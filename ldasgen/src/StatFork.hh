//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__STAT_FORK_HH
#define GENERIC_API__STAT_FORK_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <string>

#include "ldastoolsal/Fork.hh"

#include "genericAPI/StatBase.hh"

namespace GenericAPI
{
    class StatFork : public StatBase, public LDASTools::AL::Fork
    {
    public:
        StatFork( );

        virtual StatFork* vnew( ) const;

        virtual std::string
                     Debug( debug_info DebugInfo = STAT_DEBUG_GENERAL ) const;
        virtual void Init( );

        virtual int LStat( const std::string& FileName,
                           stat_buf_type&     Buf ) const;
        virtual int LStat( const directory_type& Dir,
                           const std::string&    RelFilename,
                           stat_buf_type&        Buf ) const;

        virtual int Stat( const std::string& FileName,
                          stat_buf_type&     Buf ) const;
        virtual int Stat( const directory_type& Dir,
                          const std::string&    RelFilename,
                          stat_buf_type&        Buf ) const;

        void SetCommand( const std::string& Command );

    protected:
        virtual void evalChild( );
        virtual void evalParent( );

    private:
        std::string m_cmd;
    };

    inline void
    StatFork::SetCommand( const std::string& Command )
    {
        m_cmd = Command;
    }
} // namespace GenericAPI

#endif /* GENERIC_API__STAT_FORK_HH */
