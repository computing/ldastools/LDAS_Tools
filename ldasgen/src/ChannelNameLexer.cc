//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include "ChannelNameLexer.hh"

#include <boost/tokenizer.hpp>

#include <iostream>
#include <string>
#include <vector>

GenericAPI::ChannelNameLexer::ChannelNameLexer( const std::string& NameString )
{
    Parse( NameString );
}

void
GenericAPI::ChannelNameLexer::Parse( const std::string& ChannelData )
{
    typedef boost::tokenizer< boost::char_separator< char > > tokenizer_type;

    boost::char_separator< char > channel_name_separator( " \n\t(),;" );

    tokenizer_type tokens( ChannelData, channel_name_separator );

    for ( tokenizer_type::iterator token_iter = tokens.begin( );
          token_iter != tokens.end( ); ++token_iter )
    {
        ++m_channel_names[ *token_iter ];
    }

}

void
GenericAPI::ChannelNameLexer::LexerError( const char* msg )
{
}
