//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__STAT_VFS_HH
#define GENERIC_API__STAT_VFS_HH

namespace GenericAPI
{
    //---------------------------------------------------------------------
    /// \brief Returns information about a mounted file system.
    ///
    /// \param[in] Filename
    ///     The pathname of any file within the mounted filesystem.
    /// \param[in,out] StatVFSBuffer
    ///     The reference to a statvfs structure.
    ///     If statvfs returns successfully, then information
    ///     about the mounted filesystem containing Filename
    ///     will be available in this structure.
    ///
    /// \return
    ///     Upon successful completion, the value zero is returned.
    ///     Upon failure, the value -1 is returned and errno
    ///     is set.
    //---------------------------------------------------------------------
    int StatVFS( const std::string& Filename, struct statvfs& StatVFSBuffer );
} // namespace GenericAPI

#endif /* GENERIC_API__STAT_VFS_HH */
