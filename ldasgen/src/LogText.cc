//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

extern "C" {
#include <assert.h>
#include <netdb.h>
}

#include <cctype>
#include <cstdio>
#include <ctime>

#include <algorithm>
#include <string>
#include <iostream>
#include <iomanip>

#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/System.hh"

#include "genericAPI/LogText.hh"
#include "genericAPI/Logging.hh"
// #include "genericAPI/Stat.hh"
#include "genericAPI/LDASplatform.hh"

using LDASTools::AL::MutexLock;
using LDASTools::AL::unordered_map;
using LDASTools::System::ErrnoMessage;

using LDASTools::AL::GPSTime;

using ::GenericAPI::Log::Text;
using namespace ::GenericAPI;

typedef unordered_map< std::string, const char* > site_container_type;

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
template < typename charT, typename traits = std::char_traits< charT > >
class center_helper
{
    std::basic_string< charT, traits > str_;

public:
    center_helper( std::basic_string< charT, traits > str ) : str_( str )
    {
    }
    template < typename a, typename b >
    friend std::basic_ostream< a, b >&
    operator<<( std::basic_ostream< a, b >& s, const center_helper< a, b >& c );
};

template < typename charT, typename traits = std::char_traits< charT > >
center_helper< charT, traits >
centered( std::basic_string< charT, traits > str )
{
    return center_helper< charT, traits >( str );
}

// redeclare for std::string directly so we can support anything that implicitly
// converts to std::string
center_helper< std::string::value_type, std::string::traits_type >
centered( const std::string& str )
{
    return center_helper< std::string::value_type, std::string::traits_type >(
        str );
}

template < typename charT, typename traits >
std::basic_ostream< charT, traits >&
operator<<( std::basic_ostream< charT, traits >&  s,
            const center_helper< charT, traits >& c )
{
    std::streamsize w = s.width( );
    if ( ( w > std::streamsize( c.str_.length( ) ) ) &&
         ( c.str_.length( ) > 0 ) )
    {
        std::streamsize left = ( w + ( c.str_.length( ) - 1 ) ) / 2;
        s.width( left );
        s << c.str_;
        s.width( w - left );
        s << "";
    }
    else
    {
        s << c.str_;
    }
    return s;
}

//-----------------------------------------------------------------------
/// \brief Format gif image appropriate for log entry
//-----------------------------------------------------------------------
static const std::string gif_html( Text::message_type            MT,
                                   const LDASTools::AL::GPSTime& TimeStamp );
//-----------------------------------------------------------------------
/// \brief Format gif information for legend section of web pages.
//-----------------------------------------------------------------------
static const std::string gif_legend( Text::message_type MT );

namespace GenericAPI
{

    namespace Log
    {
        Text::Text( const std::string& BaseName )
            : ::GenericAPI::Log::LDAS( BaseName )
        {
        }

        Text*
        Text::Clone( const std::string& BaseName ) const
        {
            return new Text( BaseName );
        }

        const char*
        Text::FileExtension( ) const
        {
            static const char* extension = ".txt";

            return extension;
        }

        std::string
        Text::FormatJobInfo( const std::string& JobInfo ) const
        {
            std::ostringstream retval;

            retval << "[[" << JobInfo << "]]";

            return retval.str( );
        }

        void
        Text::Message( message_type       MessageType,
                       level_type         Level,
                       const std::string& Caller,
                       const std::string& JobInfo,
                       const std::string& Message )
        {
            //-----------------------------------------------------------------
            // Verify that the message should be logged
            //-----------------------------------------------------------------
            //    if ( ShouldLog( MessageType, Level ) )
            {
                //---------------------------------------------------------------
                // OK, this message should be logged.
                // Format the message for logging and then get it into the queue
                //---------------------------------------------------------------
                std::ostringstream     msg;
                LDASTools::AL::GPSTime t;

                t.Now( );

                msg
                    //-------------------------------------------------------------
                    // GIF image
                    //-------------------------------------------------------------
                    << gif_html( MessageType, t )
                    //-------------------------------------------------------------
                    // Time stamp
                    //-------------------------------------------------------------
                    << " " << t.GetSeconds( );
                //---------------------------------------------------------------
                // JobInfo has already been html formatted
                //---------------------------------------------------------------
                msg << " " << JobInfo;
                //---------------------------------------------------------------
                msg
                    //-------------------------------------------------------------
                    // Caller
                    //-------------------------------------------------------------
                    << " {" << Caller << "} "
                    //-------------------------------------------------------------
                    // Body of message
                    //-------------------------------------------------------------
                    ;
                if ( Message.length( ) == 0 )
                {
                    msg << "no message passed for logging";
                }
                else
                {
                    //-------------------------------------------------------------
                    // Need to have temporary copy for mutations along the way
                    //-------------------------------------------------------------
                    std::string body( Message );
                    // std::string	tmp_body;
                    //-------------------------------------------------------------
                    // As the final step, cleanup of whitespaces and non
                    // printable characters within body.
                    //-------------------------------------------------------------
                    bool ws = false;

                    for ( std::string::const_iterator cur = body.begin( ),
                                                      last = body.end( );
                          cur != last;
                          ++cur )
                    {
                        if ( isspace( *cur ) && ( ws == false ) )
                        {
                            msg << " ";
                            ws = true;
                            continue;
                        }
                        else if ( !isprint( *cur ) )
                        {
                            msg << ".";
                        }
                        else
                        {
                            msg << *cur;
                        }
                        ws = false;
                    }
                    msg << std::endl;
                }
                //---------------------------------------------------------------
                // Send the message to the queue
                //---------------------------------------------------------------
                Log::Message( MessageType, Level, msg.str( ) );
            }
        }

        void
        Text::header( )
        {
            //-----------------------------------------------------------------
            // Gather information
            //-----------------------------------------------------------------
            const char* host =
                ( getenv( "HOST" ) ) ? getenv( "HOST" ) : "localhost";

            //-------------------------------------------------------------------
            //
            //-------------------------------------------------------------------
            std::string site( siteInfoLookup( LDASplatform::LDASSystem( ) ) );
            std::string special_log_header( "" );

            std::string title;

            title = "The ";
            title += LDASplatform::AppName( );
            title += " API on ";
            title += host;
            title += " at ";
            title += site;

            std::ostringstream preamble;
            preamble << " ****** " << title << " ******" << std::endl
                     << " ------ Legend: ------" << std::endl
                     << gif_legend( Text::MT_GREEN ) << std::endl
                     << gif_legend( Text::MT_YELLOW ) << std::endl
                     << gif_legend( Text::MT_ORANGE ) << std::endl
                     << gif_legend( Text::MT_RED ) << std::endl
                     << gif_legend( Text::MT_BLUE ) << std::endl
                     << gif_legend( Text::MT_PURPLE ) << std::endl
                     << gif_legend( Text::MT_MAIL ) << std::endl
                     << gif_legend( Text::MT_PHONE ) << std::endl
                     << std::endl;
            writeDirect( preamble.str( ) );
        }

        void
        Text::footer( )
        {
        }

    } // namespace Log
} // namespace GenericAPI

//=======================================================================
// Local members
//=======================================================================
namespace
{
    struct gif_info
    {
        enum alt_type
        {
            ALT_TIME,
            ALT_EMAIL
        };

        const char*    s_name;
        const char*    s_alt_name;
        const char*    s_alt_desc;
        const char*    s_description;
        const int      s_width;
        const int      s_height;
        const alt_type s_alt_info;
    };
} // namespace

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
static const gif_info&
gif_lookup( Text::message_type MT )
{
    //---------------------------------------------------------------------
    //
    //---------------------------------------------------------------------
    typedef unordered_map< int, const gif_info* > gif_container_type;
    static gif_container_type                     gifs;

    static bool initialized = false;
    if ( initialized == false )
    {
        static MutexLock::baton_type gif_baton;

        MutexLock l( gif_baton, __FILE__, __LINE__ );
        if ( initialized == false )
        {
            static const gif_info gif_info_table[] = {
                //---------------------------------------------------------------
                // 0 - MT_OK
                //---------------------------------------------------------------
                { "ball_green.gif",
                  "NORMAL",
                  "green ball",
                  "Normal status or debugging message",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 1 - MT_WARN
                //---------------------------------------------------------------
                { "ball_yellow.gif",
                  "WARNING",
                  "NOTABLE",
                  "Notable condition which may be a non-fatal error",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 2 - MT_ERROR
                //---------------------------------------------------------------
                { "ball_red.gif",
                  "FATAL",
                  "ERROR",
                  "Error condition which fatal to job",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 3 - MT_EMAIL
                //---------------------------------------------------------------
                { "mail.gif",
                  "URGENT",
                  "email",
                  "Condition requires email notification of the "
                  "responsible "
                  "administrator of this API",
                  27,
                  16,
                  gif_info::ALT_EMAIL },
                //---------------------------------------------------------------
                // 4 - MT_PHONE
                //---------------------------------------------------------------
                { "telephone.gif",
                  "CRITICAL",
                  "telephone",
                  "Condition requires phone notification of the "
                  "responsible "
                  "administrator of this API",
                  27,
                  27,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 5 - MT_DEBUG
                //---------------------------------------------------------------
                { "ball_blue.gif",
                  "DEBUG",
                  "blue ball",
                  "Debugging message intended for developers",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 6 - MT_NOTE
                //---------------------------------------------------------------
                { "ball_purple.gif",
                  "NOTE",
                  "purple ball",
                  "Notable condition which is not an error",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // MT_ORANGE
                //---------------------------------------------------------------
                { "ball_orange.gif",
                  "NON-FATAL",
                  "orange ball",
                  "Error condition not fatal to job",
                  14,
                  12,
                  gif_info::ALT_TIME }
            };

            gifs[ Text::MT_OK ] = &gif_info_table[ Text::MT_OK ];
            gifs[ Text::MT_WARN ] = &gif_info_table[ Text::MT_WARN ];
            gifs[ Text::MT_ERROR ] = &gif_info_table[ Text::MT_ERROR ];
            gifs[ Text::MT_EMAIL ] = &gif_info_table[ Text::MT_EMAIL ];
            gifs[ Text::MT_PHONE ] = &gif_info_table[ Text::MT_PHONE ];
            gifs[ Text::MT_DEBUG ] = &gif_info_table[ Text::MT_DEBUG ];
            gifs[ Text::MT_NOTE ] = &gif_info_table[ Text::MT_NOTE ];
            gifs[ Text::MT_ORANGE ] = &gif_info_table[ Text::MT_ORANGE ];

            initialized = true;
        }
    }
    gif_container_type::iterator i = gifs.find( MT );
    if ( i == gifs.end( ) )
    {
        throw std::range_error( "No gif info" );
    }
    return *( i->second );
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
static const std::string
gif_legend( Text::message_type MT )
{
    std::ostringstream desc;

    try
    {
        const gif_info& gi = gif_lookup( MT );

        desc << gi.s_alt_name << ": " << gi.s_description;
    }
    catch ( ... )
    {
    }

    return desc.str( );
}

static const std::string
gif_html( Text::message_type MT, const LDASTools::AL::GPSTime& TimeStamp )
{
    std::ostringstream gif;
    try
    {
        const gif_info& gi = gif_lookup( MT );

        std::ostringstream alt;

        switch ( gi.s_alt_info )
        {
        case gif_info::ALT_EMAIL:
        {
            alt << LoggingInfo::EMailNotify( LDASplatform::AppName( ) );
        }
        break;
        case gif_info::ALT_TIME:
            // Fall through to default case
        default:
            alt << Log::LDAS::FormatTime( Log::LDAS::TF_LOCAL, TimeStamp )
                << " &#013;"
                << Log::LDAS::FormatTime( Log::LDAS::TF_GMT, TimeStamp );
            break;
        };

        std::ostringstream alt_name;
        alt_name << ' ' << gi.s_alt_name << ' ';

        gif << std::setfill( '*' ) << std::setw( 16 )
            << centered( alt_name.str( ) );
    }
    catch ( ... )
    {
    }

    return gif.str( );
}
