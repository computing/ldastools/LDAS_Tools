//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__TCL_HH
#define GENERIC_API__TCL_HH

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <string>
#include <sstream>

#include "ldastoolsal/types.hh"

namespace GenericAPI
{
    namespace TCL
    {
        template < typename OUT, typename CONST_ITERATOR >
        void List( OUT& Output, CONST_ITERATOR Cur, CONST_ITERATOR Stop );

        template < typename OUT, typename CONST_ITERATOR >
        void
        ListUnbounded( OUT& Output, CONST_ITERATOR Cur, CONST_ITERATOR Stop );

        template < typename OUT >
        void Parse( const CHAR* Input, OUT& Output );

        template < typename OUT >
        void ParseList( const std::string& Input, OUT& Output );

        template < typename OUT >
        void ParseList( const CHAR* Input, OUT& Output );

        template < typename OUT, typename IN >
        void Translate( OUT& Output, const IN& Input );

    } // namespace TCL

    namespace TCL
    {
        template <>
        inline void
        Parse( const CHAR* Input, std::list< std::string >& Output )
        {
            std::istringstream in( Input );
            Output.erase( Output.begin( ), Output.end( ) );
            std::copy( std::istream_iterator< std::string >( in ),
                       std::istream_iterator< std::string >( ),
                       std::back_inserter( Output ) );
        }

        template < typename OUT >
        inline void
        ParseList( const std::string& Input, OUT& Output )
        {
            return ParseList( Input.c_str( ), Output );
        }

        template <>
        inline void
        ParseList( const CHAR* Input, std::list< std::string >& Output )
        {
            std::istringstream in( Input );
            int                depth = 0;
            const CHAR*        start = (const CHAR*)NULL;

            Output.erase( Output.begin( ), Output.end( ) );
            for ( const CHAR* cur = Input; *cur != '\0'; ++cur )
            {
                if ( *cur == '{' )
                {
                    ++depth;
                    if ( depth < 2 )
                    {
                        ++cur;
                        while ( *cur == ' ' )
                        {
                            ++cur;
                        }
                        start = cur;
                        --cur;
                    }
                }
                else if ( *cur == '}' )
                {
                    --depth;
                    if ( depth < 1 )
                    {
                        std::string s( start, cur - start );

                        if ( s.length( ) > 0 )
                        {
                            Output.push_back( s );
                        }
                    }
                }
                else if ( ( depth == 1 ) && ( *cur == ' ' ) )
                {
                    std::string s( start, cur - start );

                    if ( s.length( ) > 0 )
                    {
                        Output.push_back( s );
                    }
                    while ( *cur == ' ' )
                    {
                        ++cur;
                    }
                    start = cur;
                    --cur;
                }
            }
        }

    } // namespace TCL
} // namespace GenericAPI
#endif /* GENERIC_API__TCL_HH */
