//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERICAPI__CHANNEL_NAME_LEXER_HH
#define GENERICAPI__CHANNEL_NAME_LEXER_HH

#include <string>
#include <map>

namespace GenericAPI
{
    /// \brief Supports parsing of channel names from input streams
    //
    // This class supports the extraction of channel names from stream input.
    class ChannelNameLexer
    {
    public:
        //: Data type to hold results
        // The channel name can be referenced by accessing the "first" member.
        // The number of times the channel name has been seen can be accessed
        //  by the "second" member.
        typedef std::map< std::string, int > data_list_type;

        //: Constructor
        //
        //! param: const std::string& NameString - Input string.
        ChannelNameLexer( const std::string& NameString = "" );

        //: Retrieve list of channel names.
        //
        // Returns a list of channel names that have been seen.
        //
        //! return: List of channel names and occurance counts.
        const data_list_type& GetChannelNames( ) const;

        //: Parse additional string for channel names.
        //
        // This routine will expand the list of channel names with any
        // channel names that exist in the input string.
        //
        // const std::string& ChannelData - Input string.
        void Parse( const std::string& ChannelData );

        void LexerError( const char* msg );

    private:
        //: List of channel names
        data_list_type m_channel_names;

        //: Add channel to list
        //! param: const char* ChannelName - Name of channel to add
        void add_channel( const char* ChannelName );
    };

    inline const ChannelNameLexer::data_list_type&
    ChannelNameLexer::GetChannelNames( ) const
    {
        return m_channel_names;
    }
} // namespace GenericAPI

#endif /* GENERICAPI__CHANNEL_NAME_LEXER_HH */
