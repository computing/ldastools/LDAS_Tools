//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__SYMBOL_MAPPER_TCL_HH
#define GENERIC_API__SYMBOL_MAPPER_TCL_HH

#include <ldasgen_config.h>

#include <tcl.h>

#include "genericAPI/SymbolMapper.hh"

namespace GenericAPI
{
    namespace TCL
    {
        class SymbolMapper : public ::GenericAPI::SymbolMapper
        {
        public:
            SymbolMapper( Tcl_Interp* Interp );

            virtual ~SymbolMapper( );

            static void Create( Tcl_Interp* Interp );

        protected:
            virtual void addSymbol( const std::string& Symbol,
                                    arg_type           Arg,
                                    func_type          Function );

        private:
            Tcl_Interp* m_interp;

            Tcl_Interp* interp( );

            static char* change_callback( ClientData    CD,
                                          Tcl_Interp*   Interp,
                                          CONST84 char* Name1,
                                          CONST84 char* Name2,
                                          int           Flags );
        };

        inline Tcl_Interp*
        SymbolMapper::interp( )
        {
            return m_interp;
        }
    } // namespace TCL
} // namespace GenericAPI

#endif /* GENERIC_API__SYMBOL_MAPPER_TCL_HH */
