//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__LOGGING_HH
#define GENERIC_API__LOGGING_HH

#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/Log.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/unordered_map.hh"

#include "genericAPI/LogLDAS.hh"

namespace GenericAPI
{
    typedef boost::shared_ptr< GenericAPI::Log::LDAS > log_type;

    typedef Log::LDAS LogEntryGroup_type;

    class LoggingInfo : public LDASTools::AL::Singleton< LoggingInfo >
    {
    public:
        LoggingInfo( );

        static std::string ArchiveDirectory( );
        static void        ArchiveDirectory( const std::string& Value );
        static int         DebugLevel( );
        static void        DebugLevel( int Value );
        static std::string EMailNotify( const std::string& Name );
        static void        EMailNotify( const std::string& Name,
                                        const std::string& Value );
        static std::string Format( );
        static void        Format( const std::string& Value );
        static std::string LogDirectory( );
        static void        LogDirectory( const std::string& Value );
        static int         RotationCount( );
        static void        RotationCount( int Value );

    private:
        typedef LDASTools::AL::unordered_map< std::string, std::string >
            email_notify_container_type;

        std::string                 archive_dir_;
        int                         debug_level_ = 0;
        email_notify_container_type email_notify_;
        std::string                 format_;
        std::string                 log_dir_;
        int                         rotation_count_;

        std::string archive_directory( );
        void        archive_directory( const std::string& Value );
        int         debug_level( ) const;
        void        debug_level( int Value );
        std::string email_notify( const std::string& Name );
        void email_notify( const std::string& Name, const std::string& Value );
        std::string format( );
        void        format( const std::string& Value );
        std::string log_directory( );
        void        log_directory( const std::string& Value );
        int         rotation_count( ) const;
        void        rotation_count( int Value );
    };

    namespace Log
    {
        class StreamFile : public LDASTools::AL::Log::StreamFile
        {
        public:
            StreamFile( );

        protected:
            virtual std::string archive_filename( ) const;

            virtual INT_4U entriesMax( ) const;

            virtual std::string filename( ) const;
        };

        typedef boost::shared_ptr< StreamFile > stream_file_type;
    } // namespace Log

    //---------------------------------------------------------------------
    /// \brief Retrieve the default log file
    //---------------------------------------------------------------------
    const std::string& LogFileDefault( );

    //---------------------------------------------------------------------
    /// \brief Close the requested log
    //---------------------------------------------------------------------
    void CloseLog( const std::string& Filename = LogFileDefault( ) );

    //---------------------------------------------------------------------
    /// \brief Check if the message would be logged
    ///
    /// \param[in] Group
    ///     The group classification of the message to be logged.
    /// \param[in] Level
    ///     The level at which the message will be logged.
    /// \param[in] Filename
    ///     The filename of the stream to send the message.
    ///
    /// \return
    ///     True if a message would be logged for the give Group at the
    ///     given Level; false otherwise.
    //---------------------------------------------------------------------
    bool IsLogging( const int          Group,
                    const int          Level,
                    const std::string& Filename = LogFileDefault( ) );

    //---------------------------------------------------------------------
    /// \brief Establish the default output file for logging
    //---------------------------------------------------------------------
    void LogFileDefault( const std::string& LogFilename );

    //---------------------------------------------------------------------
    /// \brief Queue a message to be written to a log file
    ///
    /// \param[in] Message
    ///     The text of the message to be written to the log file.
    /// \param[in] Group
    ///     The group classification of the message to be logged.
    /// \param[in] Level
    ///     The level at which the message will be logged.
    /// \param[in] Caller
    ///     The function or method name of the routine logging the message.
    /// \param[in] JobInfo
    ///     Descriptive text of job logging the message.
    /// \param[in] Time
    ///     The time the message was logged.
    /// \param[in] Filename
    ///     The filename of the stream to send the message.
    //---------------------------------------------------------------------
    void queueLogEntry( const std::string& Message,
                        const std::string& Group,
                        const int          Level,
                        const std::string& Caller,
                        const std::string& JobInfo,
                        const unsigned int Time = 0,
                        const std::string& Filename = LogFileDefault( ) );

    //---------------------------------------------------------------------
    /// \brief Queue a message to be written to a log file
    ///
    /// \param[in] Message
    ///     The text of the message to be written to the log file.
    /// \param[in] Group
    ///     The group classification of the message to be logged.
    /// \param[in] Level
    ///     The level at which the message will be logged.
    /// \param[in] Caller
    ///     The function or method name of the routine logging the message.
    /// \param[in] JobInfo
    ///     Descriptive text of job logging the message.
    /// \param[in] Time
    ///     The time the message was logged.
    /// \param[in] Filename
    ///     The filename of the stream to send the message.
    //---------------------------------------------------------------------
    void queueLogEntry( const std::string& Message,
                        const int          Group,
                        const int          Level,
                        const std::string& Caller,
                        const std::string& JobInfo,
                        const unsigned int Time = 0,
                        const std::string& Filename = LogFileDefault( ) );

    //---------------------------------------------------------------------
    /// \brief Establish level of debugging output
    //---------------------------------------------------------------------
    void setLogDebugLevel( const int          Level,
                           const std::string& Filename = std::string( "" ) );

    //---------------------------------------------------------------------
    /// \brief Establish how to format logging messages
    //---------------------------------------------------------------------
    void SetLogFormatter( const GenericAPI::Log::LDAS* Formatter );

    //---------------------------------------------------------------------
    /// \brief Gain access to the current log formatter.
    //---------------------------------------------------------------------
    log_type LogFormatter( const std::string& Filename = LogFileDefault( ) );

    //---------------------------------------------------------------------
    /// \brief Establish tag used to identify logging set
    //---------------------------------------------------------------------
    void setLogTag( const std::string& Tag );

    //---------------------------------------------------------------------
    /// \brief Wait till all log messages are written.
    ///
    /// \param[in] Filename
    ///     The filename of the stream to send the message.
    ///
    //---------------------------------------------------------------------
    void SyncLog( const std::string& Filename = LogFileDefault( ) );

} // namespace GenericAPI

#define QUEUE_LOG_MESSAGE( MSG, GROUP, LEVEL, CALLER, JOBINFO )                \
    if ( GenericAPI::IsLogging( GenericAPI::LogEntryGroup_type::GROUP,         \
                                LEVEL ) )                                      \
    {                                                                          \
        std::ostringstream msg;                                                \
        msg << MSG;                                                            \
        GenericAPI::queueLogEntry( msg.str( ),                                 \
                                   GenericAPI::LogEntryGroup_type::GROUP,      \
                                   LEVEL,                                      \
                                   CALLER,                                     \
                                   JOBINFO );                                  \
    }

#endif /* GENERIC_API__LOGGING_HH */
