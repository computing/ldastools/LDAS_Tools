//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/unordered_map.hh"

#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Symbols.hh"

using LDASTools::AL::MutexLock;
using LDASTools::AL::unordered_map;

namespace GenericAPI
{
    //---------------------------------------------------------------------
    // The API name
    //---------------------------------------------------------------------
    void
    APINameGet( std::string& Name )
    {
        Name = LDASplatform::AppName( );
    }

    void
    APINameSet( const std::string& Name )
    {
        LDASplatform::AppName( Name );
    }

    //---------------------------------------------------------------------
    // The email notify lists
    //---------------------------------------------------------------------
    typedef unordered_map< std::string, std::string >
        email_notify_container_type;

    static MutexLock::baton_type       EMailNotifyBaton( __FILE__, __LINE__ );
    static email_notify_container_type EMailNotify;

    void
    EMailNotifyGet( const std::string& Name, std::string& Value )
    {
        MutexLock l( EMailNotifyBaton, __FILE__, __LINE__ );

        email_notify_container_type::const_iterator pos =
            EMailNotify.find( Name );
        if ( pos == EMailNotify.end( ) )
        {
            Value = EMailNotify[ "default" ];
        }
        else
        {
            Value = pos->second;
        }
    }

    void
    EMailNotifySet( const std::string& Name, const std::string& Value )
    {
        MutexLock l( EMailNotifyBaton, __FILE__, __LINE__ );

        EMailNotify[ Name ] = Value;
    }

    namespace Symbols
    {
#define INIT( x, y )                                                           \
    MutexLock::baton_type symbol< x::value_type >::m_baton( __FILE__,          \
                                                            __LINE__ );        \
    x::value_type         symbol< x::value_type >::m_value = y

#if 1
#else
        INIT( HTTPUrl, "" );
#endif

        // INIT(RUN_CODE,"StandAlone");

#undef INIT

        SYMBOL_CLASS_INIT( HTTP_URL, "" );
        SYMBOL_CLASS_INIT( LDAS_ARCHIVE_DIR, "" );
        SYMBOL_CLASS_INIT( LDAS_LOG_DIR, "" );
        SYMBOL_CLASS_INIT( LDAS_MANAGER_HOST, "" );
        SYMBOL_CLASS_INIT( LDAS_MANAGER_KEY, "" );
        SYMBOL_CLASS_INIT( LDAS_MANAGER_PORT_EMERGENCY, 0 );
        SYMBOL_CLASS_INIT( LDAS_SYSTEM, "StandAlone" );
        SYMBOL_CLASS_INIT( RUN_CODE, "StandAlone" );
    } // namespace Symbols
} // namespace GenericAPI
