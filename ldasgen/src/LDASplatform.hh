//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__LDAS_PLATFORM_HH
#define GENERIC_API__LDAS_PLATFORM_HH

#include "ldastoolsal/Singleton.hh"

namespace GenericAPI
{
    class LDASplatform : public LDASTools::AL::Singleton< LDASplatform >
    {
    public:
        LDASplatform( );

        static std::string AppName( );
        static void        AppName( const std::string& Value );
        static std::string HTTP_URL( );
        static std::string LDASSystem( );
        static std::string RunCode( );

    private:
        std::string app_name_;
        std::string http_url_;
        std::string ldas_system_;
        std::string run_code_;

        std::string app_name( );
        void        app_name( const std::string& Value );
        std::string http_url( );
        std::string ldas_system( );
        std::string run_code( );
    };

    inline std::string
    LDASplatform::app_name( )
    {
        return app_name_;
    }

    inline void
    LDASplatform::app_name( const std::string& Value )
    {
        app_name_ = Value;
    }

    inline std::string
    LDASplatform::http_url( )
    {
        return http_url_;
    }

    inline std::string
    LDASplatform::ldas_system( )
    {
        return ldas_system_;
    }

    inline std::string
    LDASplatform::run_code( )
    {
        return run_code_;
    }
} // namespace GenericAPI

#endif /* GENERIC_API__LDAS_PLATFORM_HH */
