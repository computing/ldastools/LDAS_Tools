//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>
#include <sstream>

#include "ldastoolsal/ErrorLog.hh"
#include "ldastoolsal/MemChecker.hh"

#include "genericAPI/StatFork.hh"
#include "genericAPI/StatDirect.hh"

#include "genericAPI/StatPool.hh"

using LDASTools::AL::ErrorLog;
using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::StdErrLog;

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< GenericAPI::StatPool > )

namespace
{
    typedef GenericAPI::StatDirect stat_type;
    // typedef	GenericAPI::StatFork stat_type;
    typedef std::list< GenericAPI::StatPool::info_type > pool_type;

    static stat_type lstat_base;

    inline static pool_type&
    available( )
    {
        static pool_type m_available;

        return m_available;
    }

    inline static pool_type&
    in_use( )
    {
        static pool_type m_in_use;

        return m_in_use;
    }

} // namespace

namespace GenericAPI
{
    StatPool::StatPool( ) : m_user_init_cb( NULL )
    {
    }

    StatPool::~StatPool( )
    {
        cleanup( );
    }

    void
    StatPool::Cleanup( )
    {
        Instance( ).cleanup( );
    }

    StatPool::info_type
    StatPool::StatType( )
    {
        return &lstat_base;
    }

    void
    StatPool::Destroy( StatPool::info_type Key )
    {
        Instance( ).destroy( Key );
    }

    void
    StatPool::Release( StatPool::info_type Key )
    {
        Instance( ).release( Key );
    }

    StatPool::info_type
    StatPool::Request( )
    {
        return Instance( ).request( );
    }

    void
    StatPool::UserInitCB( user_init_cb UserInitCB )
    {
        Instance( ).user_init_callback( UserInitCB );
    }

    void
    StatPool::cleanup( )
    {
        static bool has_been_cleaned = false;
        if ( has_been_cleaned == false )
        {
            MutexLock lock( m_pool_lock, __FILE__, __LINE__ );

            if ( has_been_cleaned == false )
            {
                if ( StdErrLog.IsOpen( ) )
                {
                    std::ostringstream msg;

                    msg << "StatPool::~StatPool";
                    StdErrLog(
                        ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
                }
                for ( pool_type::const_iterator cur = available( ).begin( ),
                                                last = available( ).end( );
                      cur != last;
                      ++cur )
                {
                    delete *cur;
                }
                available( ).erase( available( ).begin( ),
                                    available( ).end( ) );
#if 0
	for (  pool_type::const_iterator
		 cur = in_use( ).begin( ),
		 last = in_use( ).end( );
	       cur != last;
	       ++cur )
	{
	  delete *cur;
	}
#endif /* 0 */
                in_use( ).erase( in_use( ).begin( ), in_use( ).end( ) );
                has_been_cleaned = true;
            } // if ( has_been_cleaned == false ) (after Mutex lock)
        } // if ( has_been_cleaned == false )
    }

    void
    StatPool::destroy( StatPool::info_type Key )
    {
        MutexLock lock( m_pool_lock, __FILE__, __LINE__ );

        if ( StdErrLog.IsOpen( ) )
        {
            std::ostringstream msg;

            msg << "StatPool::destroy: Key: " << (void*)Key;
            if ( Key )
            {
                msg << Key->Debug( );
            }
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
        //-------------------------------------------------------------------
        // Take out of m_in_use list
        //-------------------------------------------------------------------
        pool_type::size_type size = in_use( ).size( );

        /* erase the element */
        in_use( ).remove( Key );
        if ( size != in_use( ).size( ) )
        {
            //-----------------------------------------------------------------
            // Destroy the process
            //-----------------------------------------------------------------
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::release: deleting: Key: " << (void*)Key;
                if ( Key )
                {
                    msg << Key->Debug( );
                }
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
            delete Key;
        }
    }

    void
    StatPool::release( StatPool::info_type Key )
    {
        MutexLock lock( m_pool_lock, __FILE__, __LINE__ );

        if ( StdErrLog.IsOpen( ) )
        {
            std::ostringstream msg;

            msg << "StatPool::release: " << Key->Debug( );
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
        //-------------------------------------------------------------------
        // Take out of in_use( ) list
        //-------------------------------------------------------------------
        pool_type::size_type size = in_use( ).size( );

        /* erase the element */
        in_use( ).remove( Key );
        if ( size != in_use( ).size( ) )
        {
            //-----------------------------------------------------------------
            // Put into available( ) queue
            //-----------------------------------------------------------------
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::release: returned to a available pool: "
                    << Key->Debug( );
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
            if ( MemChecker::IsExiting( ) )
            {
                delete Key;
            }
            else
            {
                available( ).push_back( Key );
            }
        }
    }

    StatPool::info_type
    StatPool::request( )
    {
        MutexLock lock( m_pool_lock, __FILE__, __LINE__ );
        info_type retval = NULL;

        if ( available( ).size( ) == 0 )
        {
            //-----------------------------------------------------------------
            // Create new thread
            //-----------------------------------------------------------------
            retval = lstat_base.vnew( );
            if ( m_user_init_cb )
            {
                ( *m_user_init_cb )( retval );
            }
            retval->Init( );
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::request: Created new " << retval->Debug( );
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
        }
        else
        {
            //-----------------------------------------------------------------
            // Take one from the available( ) list
            //-----------------------------------------------------------------
            retval = available( ).front( );
            available( ).pop_front( );
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::request: Reusing " << retval->Debug( );
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
        }
        in_use( ).push_back( retval );
        return retval;
    }

    inline void
    StatPool::user_init_callback( user_init_cb Callback )
    {
        m_user_init_cb = Callback;
    }
} // namespace GenericAPI
