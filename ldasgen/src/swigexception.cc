//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>

#include <string.h>
#include <errno.h>

#include "swigexception.hh"

//-----------------------------------------------------------------------------
//
//: Constructor
//
// Convert an LdasException.
//
//! param: const LdasException& e
//
#if CORE_API
SwigException::SwigException( const LdasException& e ) : mResult( ), mInfo( )
{
    if ( e.getSize( ) == 0 )
    {
        mResult = "unkown_error";
    }
    else
    {
        size_t index = e.getSize( ) - 1;

        mResult = e[ index ].getMessage( );
        if ( e[ index ].getInfo( ).size( ) > 0 )
        {
            mResult += std::string( ": " ) + e[ index ].getInfo( );
        }

        std::ostringstream ss;
        ss << " [" << e[ index ].getFile( ) << ":" << e[ index ].getLine( )
           << "]";

        while ( index != 0 )
        {
            --index;
            ss << "\n" << e[ index ].getMessage( );
            if ( e[ index ].getInfo( ).size( ) > 0 )
            {
                ss << ": ";
            }
            ss << e[ index ].getInfo( ) << " [" << e[ index ].getFile( ) << ":"
               << e[ index ].getLine( ) << "]";
        }

        mInfo = ss.str( );
    }
}
#endif /* CORE_API */

#if CORE_API
std::string
SwigException::location( const char* file, int line )
{
    char buffer[ 10 ];
    sprintf( buffer, "%u", line );
    return ( std::string( " [" ) + file + ":" + buffer + "]" );
}
#endif /* CORE_API */

#if CORE_API
std::string
SwigException::os_error( void )
{
    if ( errno != 0 )
    {
        char buffer[ 16 ];
        sprintf( buffer, "%u", errno );
        return ( std::string( " {os err:" ) + buffer + ":" + strerror( errno ) +
                 "} " );
    }
    return "";
}
#endif /* CORE_API */
