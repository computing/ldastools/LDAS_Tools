//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>

#include <sstream>

#include <boost/pointer_cast.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/MkDir.hh"
#include "ldastoolsal/System.hh"
#include "ldastoolsal/unordered_map.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/LogHTML.hh"
#include "genericAPI/LogText.hh"
#include "genericAPI/LDASplatform.hh"

using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::unordered_map;
using LDASTools::System::ErrnoMessage;

using GenericAPI::log_type;

inline void     close_log( const std::string& Filename );
static log_type find_log( const std::string& Filename );

typedef unordered_map< std::string, log_type::element_type::message_type >
    group_map_type;

static group_map_type& group_map_init( );

static group_map_type& group_map = group_map_init( );

static MutexLock::baton_type logs_baton;

static log_type default_log( new GenericAPI::Log::HTML( "" ) );

static std::string log_default_filename;

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< GenericAPI::LoggingInfo > )

namespace GenericAPI
{
    namespace Log
    {
        StreamFile::StreamFile( )
        {
        }

        std::string
        StreamFile::archive_filename( ) const
        {
            struct stat stat_buf;
            std::string fname( filename( ) );

            if ( ::stat( fname.c_str( ), &stat_buf ) != 0 )
            {
                //---------------------------------------------------------------
                // There is an issue with getting access to the log file.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "Stat( " << fname
                    << ", stat_buf ) failed: " << ErrnoMessage( );
                throw std::runtime_error( msg.str( ) );
            }

            LDASTools::AL::GPSTime log_time(
                stat_buf.st_mtime, 0, LDASTools::AL::GPSTime::UTC );

            std::ostringstream retval;

            retval << GenericAPI::LoggingInfo::ArchiveDirectory( ) << "/"
                   << GenericAPI::LDASplatform::AppName( ) << ".log."
                   << log_time.GetSeconds( ) << FilenameExtension( );
            return retval.str( );
        }

        INT_4U
        StreamFile::entriesMax( ) const
        {
            return GenericAPI::LoggingInfo::RotationCount( );
        }

        std::string
        StreamFile::filename( ) const
        {
            std::ostringstream retval;

            retval << GenericAPI::LoggingInfo::LogDirectory( ) << "/"
                   << GenericAPI::LDASplatform::AppName( ) << ".log"
                   << FilenameExtension( );
            return retval.str( );
        }

    } // namespace Log
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    LoggingInfo::LoggingInfo( ) : rotation_count_( 10000 )
    {
    }

    std::string
    LoggingInfo::ArchiveDirectory( )
    {
        return Instance( ).archive_directory( );
    }

    void
    LoggingInfo::ArchiveDirectory( const std::string& Value )
    {
        Instance( ).archive_directory( Value );
    }

    int
    LoggingInfo::DebugLevel( )
    {
        return Instance( ).debug_level( );
    }

    void
    LoggingInfo::DebugLevel( int Value )
    {
        Instance( ).debug_level( Value );
    }

    std::string
    LoggingInfo::EMailNotify( const std::string& Name )
    {
        return Instance( ).email_notify( Name );
    }

    void
    LoggingInfo::EMailNotify( const std::string& Name,
                              const std::string& Value )
    {
        Instance( ).email_notify( Name, Value );
    }

    std::string
    LoggingInfo::Format( )
    {
        return Instance( ).format( );
    }

    void
    LoggingInfo::Format( const std::string& Value )
    {
        Instance( ).format( Value );
    }

    std::string
    LoggingInfo::LogDirectory( )
    {
        return Instance( ).log_directory( );
    }

    void
    LoggingInfo::LogDirectory( const std::string& Value )
    {
        Instance( ).log_directory( Value );
    }

    int
    LoggingInfo::RotationCount( )
    {
        return Instance( ).rotation_count( );
    }

    void
    LoggingInfo::RotationCount( int Value )
    {
        Instance( ).rotation_count( Value );
    }

    inline std::string
    LoggingInfo::archive_directory( )
    {
        if ( archive_dir_.empty( ) )
        {
            return log_directory( );
        }
        return archive_dir_;
    }

    inline void
    LoggingInfo::archive_directory( const std::string& Value )
    {
        archive_dir_ = Value;
    }

    inline int
    LoggingInfo::debug_level( ) const
    {
        return debug_level_;
    }

    inline void
    LoggingInfo::debug_level( int Value )
    {
        setLogDebugLevel( Value );
        debug_level_ = Value;
    }

    inline std::string
    LoggingInfo::email_notify( const std::string& Name )
    {
        email_notify_container_type::iterator pos;
        pos = email_notify_.find( Name );
        if ( pos == email_notify_.end( ) )
        {
            email_notify_container_type::value_type v( Name, "default" );
            pos = email_notify_.insert( pos, v );
        }
        return pos->second;
    }

    inline void
    LoggingInfo::email_notify( const std::string& Name,
                               const std::string& Value )
    {
        email_notify_[ Name ] = Value;
    }

    inline std::string
    LoggingInfo::format( )
    {
        return format_;
    }

    inline void
    LoggingInfo::format( const std::string& Value )
    {
        //-------------------------------------------------------------------
        // Preserve the stream
        //-------------------------------------------------------------------
        LDASTools::AL::Log::stream_type s(
            GenericAPI::LogFormatter( )->Stream( ) );

        format_ = Value;
        if ( Value.compare( "html" ) == 0 )
        {
            //-----------------------------------------------------------------
            // Generate HTML output
            //-----------------------------------------------------------------
            GenericAPI::SetLogFormatter( new GenericAPI::Log::HTML( "" ) );
        }
        else
        {
            //-----------------------------------------------------------------
            // Default case is to use ASCII text as it is human readable
            //-----------------------------------------------------------------
            GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
            format_ = "text";
        }
        //-------------------------------------------------------------------
        // Restore stream
        //-------------------------------------------------------------------
        GenericAPI::LogFormatter( )->Stream( s );
        //-------------------------------------------------------------------
        // For file streams, make sure the output extension is correct
        //-------------------------------------------------------------------
        GenericAPI::Log::stream_file_type fs(
            boost::dynamic_pointer_cast<
                GenericAPI::Log::stream_file_type::element_type >( s ) );
        if ( fs )
        {
            fs->FilenameExtension(
                GenericAPI::LogFormatter( )->FileExtension( ) );
        }
    }

    inline std::string
    LoggingInfo::log_directory( )
    {
        return log_dir_;
    }

    inline void
    LoggingInfo::log_directory( const std::string& Value )
    {
        log_dir_ = Value;
    }

    inline int
    LoggingInfo::rotation_count( ) const
    {
        return rotation_count_;
    }

    inline void
    LoggingInfo::rotation_count( int Value )
    {
        rotation_count_ = Value;
    }

    //---------------------------------------------------------------------
    bool
    IsLogging( const int Group, const int Level, const std::string& Filename )
    {
        if ( MemChecker::IsExiting( ) == true )
        {
            return false;
        }
        log_type log = find_log( Filename );

        return log->VerbosityCheck( Group, Level );
    }

    void
    CloseLog( const std::string& Filename )
    {
        close_log( Filename );
    }

    const std::string&
    LogFileDefault( )
    {
        return log_default_filename;
    }

    void
    LogFileDefault( const std::string& LogFilename )
    {
        log_default_filename = LogFilename;
    }

    log_type
    LogFormatter( const std::string& Filename )
    {
        log_type log = find_log( std::string( Filename ) );

        return log;
    }

    void
    queueLogEntry( const std::string& Message,
                   const std::string& Group,
                   const int          Level,
                   const std::string& Caller,
                   const std::string& JobInfo,
                   const unsigned int Time,
                   const std::string& Filename )
    {
        log_type::element_type::message_type mt =
            group_map[ std::string( Group ) ];

        queueLogEntry( Message, mt, Level, Caller, JobInfo, Time, Filename );
    }

    void
    queueLogEntry( const std::string& Message,
                   const int          Group,
                   const int          Level,
                   const std::string& Caller,
                   const std::string& JobInfo,
                   const unsigned int Time,
                   const std::string& Filename )
    {
        if ( MemChecker::IsExiting( ) == true )
        {
            return;
        }
        log_type    log = find_log( std::string( Filename ) );
        std::string job_info( log->FormatJobInfo( JobInfo ) );

        //-------------------------------------------------------------------
        // Queue the message.
        //-------------------------------------------------------------------
        {
            MutexLock l( logs_baton, __FILE__, __LINE__ );

            if ( MemChecker::IsExiting( ) == false )
            {
                log->Message( log_type::element_type::message_type( Group ),
                              log_type::element_type::level_type( Level ),
                              Caller,
                              job_info,
                              Message );
            }
        }
    }

    void
    setLogDebugLevel( const int Level, const std::string& Filename )
    {
        log_type log = find_log( std::string( Filename ) );

        {
            MutexLock l( logs_baton, __FILE__, __LINE__ );

            if ( MemChecker::IsExiting( ) == false )
            {
                log->Verbosity( log_type::element_type::MT_DEBUG, Level );
            }
        }
    }

#if 0
  void
  setLogOutputDirectory( const std::string& OutputDirectory ) 
  {
    if ( ! OutputDirectory.empty( ) )
    {
      GenericAPI::LoggingInfo::ArchiveDirectory( OutputDirectory );
      GenericAPI::LoggingInfo::LogDirectory( OutputDirectory );
      try
      {
	LDASTools::Cmd::MkDir
	  m( 0x755, LDASTools::Cmd::MkDir::OPT_MAKE_PARENT_DIRECTORIES );
	  
	m( OutputDirectory );
      }
      catch( ... )
      {
      }
    }
  }
#endif /* 0 */

    void
    setLogTag( const std::string& Tag )
    {
        if ( !Tag.empty( ) )
        {
            LDASplatform::AppName( Tag );
        }
    }

    void
    SetLogFormatter( const GenericAPI::Log::LDAS* Formatter )
    {
        default_log.reset( const_cast< GenericAPI::Log::LDAS* >( Formatter ) );
    }

    void
    SyncLog( const std::string& Filename )
    {
        log_type log = find_log( std::string( Filename ) );

        if ( log )
        {
            log->Sync( );
        }
    }
} // namespace GenericAPI

//-----------------------------------------------------------------------
// Local variables
//-----------------------------------------------------------------------
typedef unordered_map< std::string, log_type > log_container_type;

static log_container_type logs;

inline void
close_log( const std::string& Filename )
{
    log_type log_handle;
    {
        MutexLock l( logs_baton, __FILE__, __LINE__ );

        log_container_type::iterator i = logs.find( Filename );
        if ( i != logs.end( ) )
        {
            log_handle = i->second;
            logs.erase( i );
        }
    }
    if ( log_handle )
    {
        log_handle.reset( );
    }
}

static void
unregister_logs( )
{
    MutexLock l( logs_baton, __FILE__, __LINE__ );

    logs.erase( logs.begin( ), logs.end( ) );
}

static log_type
find_log( const std::string& Filename )
{
    using GenericAPI::Log::LDAS;
    std::ostringstream logname( Filename );

    //---------------------------------------------------------------------
    //
    //---------------------------------------------------------------------
    if ( logname.str( ).length( ) == 0 )
    {
        logname << LDAS::LogFilename( default_log->FileExtension( ), 0 );
    }

    //---------------------------------------------------------------------
    // Mutex lock to prevent multiple creations
    //---------------------------------------------------------------------
    MutexLock l( logs_baton, __FILE__, __LINE__ );

#if 1
    log_type n;

    if ( logs.empty( ) )
    {
        //-------------------------------------------------------------------
        // Since it is, register the MemChecker handler to clean up the memory.
        //-------------------------------------------------------------------
        MemChecker::Append(
            unregister_logs, "unregister_logs@api/genericAPI/Logging.cc", 150 );

        //-------------------------------------------------------------------
        // Does not already exist so create a new instance and then add
        // to the list.
        //-------------------------------------------------------------------
        n.reset( default_log->Clone( logname.str( ) ) );

        if ( n )
        {
            logs[ logname.str( ) ] = n;
            n->Spawn( );
        }
    }
    n = logs.begin( )->second;
#else /* 1 */
#if MULTI_LOG_SUPPORT
    log_container_type::iterator i = logs.find( logname.str( ) );
    if ( i != logs.end( ) )
    {
        //-------------------------------------------------------------------
        // Have a log associated with the file.
        //-------------------------------------------------------------------
        return ( i->second );
    }
    //---------------------------------------------------------------------
    // Check to see if the list is empty
    //---------------------------------------------------------------------
    if ( logs.empty( ) )
    {
        //-------------------------------------------------------------------
        // Since it is, register the MemChecker handler to clean up the memory.
        //-------------------------------------------------------------------
        MemChecker::Append(
            unregister_logs, "unregister_logs@api/genericAPI/Logging.cc", 150 );
    }

    //---------------------------------------------------------------------
    // Does not already exist so create a new instance and then add to the
    //   list.
    //---------------------------------------------------------------------
    log_type n( default_log->Clone( logname.str( ) ) );

    if ( n )
    {
        logs[ logname.str( ) ] = n;
        n->Spawn( );

        return n;
    }
#endif /* MULTI_LOG_SUPPORT */
#endif /* 1 */
    return n;
}

static group_map_type&
group_map_init( )
{
    static group_map_type gm;

    if ( gm.size( ) == 0 )
    {
        gm[ "0" ] = log_type::element_type::MT_OK;
        gm[ "1" ] = log_type::element_type::MT_WARN;
        gm[ "2" ] = log_type::element_type::MT_ERROR;
        gm[ "3" ] = log_type::element_type::MT_EMAIL;
        gm[ "4" ] = log_type::element_type::MT_PHONE;
        gm[ "5" ] = log_type::element_type::MT_DEBUG;
        gm[ "6" ] = log_type::element_type::MT_NOTE;
        gm[ "orange" ] = log_type::element_type::MT_ORANGE;
        gm[ "green" ] = log_type::element_type::MT_GREEN;
        gm[ "yellow" ] = log_type::element_type::MT_YELLOW;
        gm[ "red" ] = log_type::element_type::MT_RED;
        gm[ "blue" ] = log_type::element_type::MT_BLUE;
        gm[ "purple" ] = log_type::element_type::MT_PURPLE;
        gm[ "phone" ] = log_type::element_type::MT_PHONE;
        gm[ "pager" ] = log_type::element_type::MT_PHONE;
        gm[ "mail" ] = log_type::element_type::MT_MAIL;
        gm[ "email" ] = log_type::element_type::MT_EMAIL;
        gm[ "certmail" ] = log_type::element_type::MT_CERTMAIL;
    }
    return gm;
}
