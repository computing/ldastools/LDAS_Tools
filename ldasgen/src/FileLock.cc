//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>

#include "genericAPI/FileLock.hh"

GenericAPI::FileLock::FileLock( const std::string& Filename )
    : m_filename( Filename )
{
    m_fd = open( Filename.c_str( ),
                 O_WRONLY | O_CREAT | O_APPEND,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
}

GenericAPI::FileLock::~FileLock( )
{
    if ( m_buffer.str( ).length( ) > 0 )
    {
        lockf( m_fd, F_LOCK, 0 );
        lseek( m_fd, 0, SEEK_END );
        size_t bytes = ::write(
            m_fd, m_buffer.str( ).c_str( ), m_buffer.str( ).length( ) );
        if ( bytes != m_buffer.str( ).length( ) )
        {
            std::cerr << "WARNING: GenericAPI::FileLock::~FileLock wrote "
                      << bytes << " bytes instead of "
                      << m_buffer.str( ).length( ) << " bytes.\n";
        }
    }
    lseek( m_fd, 0, SEEK_SET ); // Go to the beginning
    lockf( m_fd, F_ULOCK, 0 ); // Release all locks
    close( m_fd );
}
