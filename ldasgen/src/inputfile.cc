//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <libgen.h>
#include <string.h>

#include "ldastoolsal/util.hh"

#include "inputfile.hh"

#define IS_DIR_DELAY 10

//-----------------------------------------------------------------------------
//
//: Constructor
//
// This constructor opens the InputFile to represent the specified file.
//
//! param: const char* filename - The file to open.
//! param: std::ios::openmode m - The mode in which to open the file.
//
//! exc: permission_denied - Permission to access the file or a
//+     directory component was denied.
//! exc: file_not_found - The file or a directory component was not
//+     found.
//! exc: bad_alloc - Insufficient kernel memory to open the file.
//! exc: io_error - An unknown I/O error occurred.
//
InputFile::InputFile( const char* filename, std::ios::openmode m )
{
    open_file( filename, m );
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
// This constructor opens the InputFile to represent the specified file and
// allocates a buffer the size specified by the caller.
//
//! param: const char* filename - The file to open.
//! param: std::ios::openmode m - The mode in which to open the file.
//! param: unsigned int buffer_size - The size of the buffer. Zero for
//+	 unbuffered
//
//! exc: permission_denied - Permission to access the file or a
//+     directory component was denied.
//! exc: file_not_found - The file or a directory component was not
//+     found.
//! exc: bad_alloc - Insufficient kernel memory to open the file.
//! exc: io_error - An unknown I/O error occurred.
//
InputFile::InputFile( const char*        filename,
                      std::ios::openmode m,
                      unsigned int       buffer_size )
{
    open_file( filename, m );
    if ( buffer_size )
    {
        m_buffer.reset( new char[ buffer_size ] );

        // Establish as the buffer for the stream
        mStream.setbuf( m_buffer.get( ), buffer_size );
    }
    else
    {
        mStream.setbuf( (char*)NULL, 0 );
    }
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
// This constructor opens the InputFile to represent the specified file and
// allocates a buffer the size specified by the caller.
//
//! param: const char* filename - The file to open.
//! param: std::ios::openmode m - The mode in which to open the file.
//! param: bool use_memory_mapped_io - True if the memory mapped io facilities
//+		of the base class.
//! param: unsigned int buffer_size - The size of the buffer. Zero for
//+	 default buffer size
//! param: char* buffer - Optionally supplied buffer.
//
//! exc: permission_denied - Permission to access the file or a
//+     directory component was denied.
//! exc: file_not_found - The file or a directory component was not
//+     found.
//! exc: bad_alloc - Insufficient kernel memory to open the file.
//! exc: io_error - An unknown I/O error occurred.
//
InputFile::InputFile( const char*        filename,
                      std::ios::openmode m,
                      bool               use_memory_mapped_io,
                      unsigned int       buffer_size,
                      char*              buffer )
{
    open_file( filename, m );
    if ( buffer_size > 0 )
    {
        //---------------------------------------------------------------
        // Establish buffering
        //---------------------------------------------------------------
        if ( buffer != (char*)NULL )
        {
            //-----------------------------------------------------------
            // Caller responsible for buffer allocation/deallocation
            //-----------------------------------------------------------
            mStream.setbuf( buffer, buffer_size );
        }
        else
        {
            //-----------------------------------------------------------
            // Class responsible for buffer allocation/deallocation
            //-----------------------------------------------------------
            m_buffer.reset( new char[ buffer_size ] );
            mStream.setbuf( m_buffer.get( ), buffer_size );
        }
    }
    mStream.rdbuf( )->UseMemoryMappedIO( use_memory_mapped_io );
}

//-----------------------------------------------------------------------------
//
//: Destructor
//
// The destructor closes the file stream.
//
InputFile::~InputFile( )
{
#ifdef GENERIC_API_FILE_IO_LOCK
    MutexLock lock( LDASTools::AL::IOMutex );
#endif /* GENERIC_API_FILE_IO_LOCK */

    mStream.close( );
#ifdef GENERIC_API_FILE_IO_LOCK
    lock.Release( );
#endif /* GENERIC_API_FILE_IO_LOCK */
}

//-----------------------------------------------------------------------------
//
//: Opens the file
//
// This method opens the InputFile to represent the specified file.
//
//! param: const char* filename - The file to open.
//! param: std::ios::openmode m - The mode in which to open the file.
//
//! exc: permission_denied - Permission to access the file or a
//+     directory component was denied.
//! exc: file_not_found - The file or a directory component was not
//+     found.
//! exc: bad_alloc - Insufficient kernel memory to open the file.
//! exc: io_error - An unknown I/O error occurred.
//
void
InputFile::open_file( const char* filename, std::ios::openmode m )
{
    //-------------------------------------------------------------------
    // Ensure the request is not to open a directory
    //-------------------------------------------------------------------
    struct stat file_status;

    memset( &file_status, 0, sizeof( file_status ) );
    if ( stat( filename, &file_status ) == 0 )
    {
        if ( S_ISDIR( file_status.st_mode ) )
        {
            std::ostringstream msg;

            msg << filename << ": "
                << "permission_denied: the path refers to a directory."
                << "( dev: " << file_status.st_dev
                << " inode: " << file_status.st_ino
                << " inode_size: " << sizeof( file_status.st_ino ) << " )";
            std::cerr << msg.str( ) << std::endl;
            struct stat file_status2;
            memset( &file_status2, 0, sizeof( file_status2 ) );
            if ( stat( filename, &file_status2 ) == 0 )
            {
                if ( S_ISDIR( file_status2.st_mode ) )
                {
                    std::ostringstream msg2;

                    msg2 << filename << ": "
                         << "permission_denied: the path refers to a directory."
                         << "( dev: " << file_status2.st_dev
                         << " inode: " << file_status2.st_ino << ")";
                    std::cerr << msg2.str( ) << " [immediately after]"
                              << std::endl;
                    sleep( IS_DIR_DELAY );
                    struct stat file_status3;
                    memset( &file_status3, 0, sizeof( file_status3 ) );
                    if ( stat( filename, &file_status3 ) == 0 )
                    {
                        if ( S_ISDIR( file_status3.st_mode ) )
                        {
                            std::ostringstream msg3;

                            msg3 << filename << ": "
                                 << "permission_denied: the path refers to a "
                                    "directory."
                                 << "( dev: " << file_status3.st_dev
                                 << " inode: " << file_status3.st_ino << " )";
                            std::cerr << msg3.str( ) << " [" << IS_DIR_DELAY
                                      << " second(s) delay]" << std::endl;
                            throw SWIGEXCEPTION( msg3.str( ) );
                        }
                    }
                }
            }
        }
    }

    //-------------------------------------------------------------------
    // Open the file
    //-------------------------------------------------------------------
#ifdef GENERIC_API_FILE_IO_LOCK
    MutexLock lock( LDASTools::AL::IOMutex );
#endif /* GENERIC_API_FILE_IO_LOCK */

    mStream.open( filename, m );

#ifdef GENERIC_API_FILE_IO_LOCK
    lock.Release( );
#endif /* GENERIC_API_FILE_IO_LOCK */

    //-------------------------------------------------------------------
    // If the file is not open, check the file access.
    //-------------------------------------------------------------------
    if ( !mStream.is_open( ) )
    {
        int error = access( filename, R_OK );
        if ( error != 0 )
        {
            std::ostringstream oss;
            oss << filename << ": ";
            // convert error codes to SWIG exceptions.
            switch ( errno )
            {
            case EACCES:
                oss << "permission_denied: (EACCES) requested access to the "
                       "file or directory was denied.";
                throw SWIGEXCEPTION( oss.str( ) );

            case EROFS:
                oss << "permission_denied: (EROFS) file system is "
                       "read-only.";
                throw SWIGEXCEPTION( oss.str( ) );

            case EFAULT:
                oss << "file_not_found: (EFAULT) pathname points outside the "
                       "accessible address space.";
                throw SWIGEXCEPTION( oss.str( ) );

            case ENOENT:
                oss << "file_not_found: (ENOENT) A directory component does "
                       "not exist.";
                throw SWIGEXCEPTION( oss.str( ) );

            case ENOTDIR:
                oss << "file_not_found: (ENOTDIR) A component used as "
                       "a directory is not a directory.";
                throw SWIGEXCEPTION( oss.str( ) );

            case ENAMETOOLONG:
                oss << "file_not_found: (ENAMETOOLONG) pathname too long.";
                throw SWIGEXCEPTION( oss.str( ) );

            case ENOMEM:
                oss << "bad_alloc: (ENOMEM) Insufficient kernel memory "
                       "available.";
                throw SWIGEXCEPTION( oss.str( ) );

            case ELOOP:
                oss << "file_not_found: (ELOOP) Too many symbolic links "
                       "encountered.";
                throw SWIGEXCEPTION( oss.str( ) );

            case EIO:
                oss << "io_error: (EIO) An I/O error occurred.";
                throw SWIGEXCEPTION( oss.str( ) );
            }
        }

        std::ostringstream msg;

        msg << filename << ": "
            << "io_error: An unknown error occurred.";
        throw SWIGEXCEPTION( msg.str( ) );
    }
}

//-----------------------------------------------------------------------------
//
//: Check if the File is Open.
//
//! return: bool
//
bool
InputFile::is_open( )
{
    return mStream.is_open( );
}
