//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <time.h>

#include <algorithm>
#include <set>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/SignalHandler.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/MountPointStatus.hh"

using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::ReadWriteLock;
using LDASTools::AL::SignalHandler;

using LDASTools::AL::GPSTime;

using LDASTools::AL::Directory;
using LDASTools::AL::Thread;

using GenericAPI::MountPointStatus;
using GenericAPI::queueLogEntry;

#if 1
#define USE_CANCEL_SIGNAL 1
static const SignalHandler::signal_type CANCEL_SIGNAL =
    SignalHandler::SIGNAL_TERMINATE;
#else
#undef USE_CANCEL_SIGNAL
#endif /* 0 */

//=======================================================================
// Debugging macros
//=======================================================================
#if 0
#include "ldastoolsal/IOLock.hh"
#define VERBOSE_DEBUGGING 1

#define AT_BASE( a )                                                           \
    {                                                                          \
        int oldstate;                                                          \
                                                                               \
        pthread_setcancelstate( PTHREAD_CANCEL_DISABLE, &oldstate );           \
        MutexLock lock(                                                        \
            LDASTools::AL::IOLock::GetKey( std::cerr ), __FILE__, __LINE__ );  \
        pthread_setcancelstate( oldstate, &oldstate );                         \
                                                                               \
        std::cerr << a << __FILE__ << " " << __LINE__ << std::endl             \
                  << std::flush;                                               \
    }

#else
#define AT_BASE( a )
#endif

#define AT( a ) AT_BASE( a << std::endl << "\t" )
#define HERE( ) AT_BASE( "" )

static const char*
state_string( GenericAPI::MountPointStatus::state_type State )
{
    using GenericAPI::MountPointStatus;

    switch ( State )
    {
    case MountPointStatus::STATE_OFFLINE:
        return "STATE_OFFLINE";
        break;
    case MountPointStatus::STATE_ONLINE:
        return "STATE_ONLINE";
        break;
    }
    return "unknown";
}

//=======================================================================
//
//=======================================================================

typedef MountPointStatus::mount_point_list_type mount_point_list_type;

struct mount_info_type : public Thread
{
public:
    typedef MountPointStatus::state_type state_type;

    mount_info_type( const std::string& Dir );
    state_type Status( );
    void       RestartScanner( );
    void       Set( state_type State, bool StartScanner = true );

private:
    void state( state_type State );

    state_type state( ) const;

    mount_info_type( const mount_info_type& );
    mount_info_type& operator=( const mount_info_type& );

    void action( );

    static void cancelation_handler( mount_info_type* Ref );
    bool        test( );

    mutable MutexLock::baton_type m_sync_lock;
    mutable state_type            p_state;
    mutable Directory             m_mount_point_dir;
};

inline void
mount_info_type::state( mount_info_type::state_type State )
{
    MutexLock l( m_sync_lock, __FILE__, __LINE__ );

    p_state = State;
}

inline mount_info_type::state_type
mount_info_type::state( ) const
{
    MutexLock l( m_sync_lock, __FILE__, __LINE__ );

    return p_state;
}

class mount_points_type : private std::map< std::string, mount_info_type* >
{
public:
    typedef std::map< std::string, mount_info_type* > container_type;
    typedef container_type::iterator                  iterator;
    typedef container_type::const_iterator            const_iterator;

    using container_type::begin;
    using container_type::end;
    using container_type::find;
    using container_type::operator[];
    using container_type::erase;

    iterator Locate( std::string Path );
};

class compare_to_string
{
public:
    inline int
    operator( )( const std::string&                                      T1,
                 const std::pair< const std::string, mount_info_type* >& T2 )
    {
        return ( T1 < T2.first );
    }

    inline int
    operator( )( const std::pair< const std::string, mount_info_type* >& T1,
                 const std::string&                                      T2 )
    {
        return ( T1.first < T2 );
    }
};

static mount_points_type mount_points;

static void cleanup_at_exit( );
static void fork_parent( );
static void fork_prepare( );

static void add( const mount_point_list_type& MountPoint );
static void remove( const mount_point_list_type& MountPoint );

//=======================================================================
// Local functions
//=======================================================================
namespace
{

    static char path_sep = '/';

    inline void
    normalize_path( std::string& InPath )
    {

        bool                  seen_path_sep = false;
        std::string::iterator cur_insert = InPath.begin( );

        for ( std::string::const_iterator cur = InPath.begin( ),
                                          last = InPath.end( );
              cur != last;
              ++cur )
        {
            if ( ( ( *cur ) == path_sep ) && ( seen_path_sep ) )
            {
                continue;
            }

            *cur_insert++ = *cur;
            if ( ( *cur ) == path_sep )
            {
                seen_path_sep = true;
            } // if
            else
            {
                seen_path_sep = false;
            }
        } // for
        //-------------------------------------------------------------------
        // Remove trailing characters.
        //-------------------------------------------------------------------
        InPath.erase( cur_insert, InPath.end( ) );
        //-------------------------------------------------------------------
        // Remove trailing path_sep if not root directory
        //-------------------------------------------------------------------
        if ( InPath.length( ) > 1 )
        {
            cur_insert = InPath.end( );
            cur_insert--;
            if ( *cur_insert == path_sep )
            {
                InPath.erase( cur_insert, InPath.end( ) );
            }
        }
    } // function - normalize_path

    inline void
    normalize( GenericAPI::MountPointStatus::mount_point_list_type& MP )
    {
        for ( GenericAPI::MountPointStatus::mount_point_list_type::iterator
                  cur = MP.begin( ),
                  last = MP.end( );
              cur != last;
              ++cur )
        {
            normalize_path( *cur );
        }
        MP.sort( );
    }

    //=====================================================================
    // Routine for checking if Path1 is a subdirectory of Path2
    //=====================================================================
    bool
    is_subdirectory( const std::string& Subdir, const std::string& Root )
    {
        bool retval = false;
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
#if 0
    AT( "Root.length: " << Root.length( ) << " Subdir.length: " << Subdir.length( ) << " compare: " << Root.compare( Subdir ) );
    AT( "Subdir.compare( 0, " << Root.length( ) << ", " << Root << " ): " << Subdir.compare( 0, Root.length( ), Root ) );
#endif /* 0 */
        if ( ( Root.length( ) == Subdir.length( ) ) &&
             ( Root.compare( Subdir ) == 0 ) )
        {
            retval = true;
        }
        else if ( ( Root.length( ) < Subdir.length( ) ) &&
                  ( Subdir.compare( 0, Root.length( ), Root ) == 0 ) &&
                  ( Subdir[ Root.length( ) ] == path_sep ) )
        {
            retval = true;
        }
#if 0
    AT( "is_subdirectory: " << retval );
#endif /* 0 */
        return retval;
    } // function - is_subdirectory

} // namespace

inline mount_points_type::iterator
mount_points_type::Locate( std::string Path )
{
    //-------------------------------------------------------------------
    //
    //-------------------------------------------------------------------
    static const char* caller = "MountPointStatus::Locate";
    static const char* error_msg = "unmanaged directory";
    //-------------------------------------------------------------------
    // Nothing in the mount point list
    //-------------------------------------------------------------------
    QUEUE_LOG_MESSAGE( "Number of mount points " << size( ),
                       MT_DEBUG,
                       10,
                       caller,
                       "FILESYSTEM_STATUS" );
    if ( size( ) <= 0 )
    {
        throw std::range_error( error_msg );
    }

    normalize_path( Path );
    QUEUE_LOG_MESSAGE( "normalized path: " << Path,
                       MT_DEBUG,
                       10,
                       caller,
                       "FILESYSTEM_STATUS" );
    //-------------------------------------------------------------------
    // Lookup the mount mount
    //-------------------------------------------------------------------
    iterator i =
        std::lower_bound( begin( ), end( ), Path, compare_to_string( ) );
    if ( ( i != end( ) ) && ( Path.length( ) == i->first.length( ) ) &&
         ( is_subdirectory( Path, i->first ) == true ) )
    {
        //-----------------------------------------------------------------
        // Special case for when the directory is the mount point
        //-----------------------------------------------------------------
        QUEUE_LOG_MESSAGE( "Lookup: found path: " << i->first,
                           MT_DEBUG,
                           10,
                           caller,
                           "FILESYSTEM_STATUS" );
        return i;
    }
    if ( i != begin( ) )
    {
        //-----------------------------------------------------------------
        // Setup for the usual case of going one past
        // This is an optimization so only one string compare is needed
        // for the usual case.
        //-----------------------------------------------------------------
        --i;
        if ( is_subdirectory( Path, i->first ) == true )
        {
            QUEUE_LOG_MESSAGE( "Lookup: found path +1: " << i->first,
                               MT_DEBUG,
                               10,
                               caller,
                               "FILESYSTEM_STATUS" );
            return i;
        }
        i++;
    }
    if ( ( i != end( ) ) && ( is_subdirectory( Path, i->first ) == true ) )
    {
        QUEUE_LOG_MESSAGE( "Lookup: found path: non-mount point: " << i->first,
                           MT_DEBUG,
                           10,
                           caller,
                           "FILESYSTEM_STATUS" );
        return i;
    }
    QUEUE_LOG_MESSAGE( "Lookup: Failed for path: " << Path,
                       MT_DEBUG,
                       10,
                       caller,
                       "FILESYSTEM_STATUS" );
    throw std::range_error( error_msg );
}

using GenericAPI::MountPointStatus;

static ReadWriteLock::baton_type&
mount_points_baton( )
{
    static ReadWriteLock::baton_type baton;

    return baton;
}

static bool is_initialized = MountPointStatus::Initialize( );

namespace GenericAPI
{
    //=====================================================================
    // MountPointsStatus
    //=====================================================================
    // Static variable declaration for class
    //---------------------------------------------------------------------

    //---------------------------------------------------------------------
    // Public class methods
    //---------------------------------------------------------------------
    /// This method adds new mount points to the list managed mount
    /// points.
    /// It is careful to check to see if any of the entries in
    /// \a MountPoints are already being managed and only adds
    /// the entries from \a MountPoints which are not currently
    /// being managed.
    //---------------------------------------------------------------------
    void
    MountPointStatus::Add( const mount_point_list_type& MountPoints )
    {
        //-------------------------------------------------------------------
        // Create sorted list of mount points to add
        //-------------------------------------------------------------------
        mount_point_list_type mp( MountPoints );

        normalize( mp );

        //-------------------------------------------------------------------
        // Modifying access
        //-------------------------------------------------------------------
        ReadWriteLock lock(
            mount_points_baton( ), ReadWriteLock::WRITE, __FILE__, __LINE__ );
        //-------------------------------------------------------------------
        // Determine which of the MountPoints are new
        //-------------------------------------------------------------------
        mount_point_list_type additions;

        std::set_difference( mp.begin( ),
                             mp.end( ),
                             mount_points.begin( ),
                             mount_points.end( ),
                             back_inserter( additions ),
                             compare_to_string( ) );
        add( additions );
    }

    bool
    MountPointStatus::Initialize( )
    {
        MemChecker::Append( cleanup_at_exit,
                            "GenericAPI::MountPointStatus::cleanup_at_exit",
                            50 );
        pthread_atfork( fork_prepare, fork_parent, (void ( * )( ))NULL );
        return true;
    }

    //---------------------------------------------------------------------
    /// Verifies that \a Dir exists as an element of the set of
    /// currently managed directories.
    /// If their is an exact match, then true is returned,
    /// false otherwise.
    //---------------------------------------------------------------------
    bool
    MountPointStatus::IsDirManaged( const std::string& Dir )
    {
        //-------------------------------------------------------------------
        // Checking access
        //-------------------------------------------------------------------
        ReadWriteLock lock(
            mount_points_baton( ), ReadWriteLock::READ, __FILE__, __LINE__ );

        if ( mount_points.find( Dir ) == mount_points.end( ) )
        {
            return false;
        }
        return true;
    }

    void
    MountPointStatus::Remove( const mount_point_list_type& MountPoints )
    {
        //-------------------------------------------------------------------
        // Create sorted list of mount points to add
        //-------------------------------------------------------------------
        mount_point_list_type mp( MountPoints );

        normalize( mp );

        //-------------------------------------------------------------------
        // Modifying access
        //-------------------------------------------------------------------
        ReadWriteLock lock(
            mount_points_baton( ), ReadWriteLock::WRITE, __FILE__, __LINE__ );
        //-------------------------------------------------------------------
        // Determine which of the MountPoints need to be removed
        //-------------------------------------------------------------------
        mount_point_list_type commons;

        std::set_intersection( mp.begin( ),
                               mp.end( ),
                               mount_points.begin( ),
                               mount_points.end( ),
                               std::back_inserter( commons ),
                               compare_to_string( ) );
        remove( commons );
    }

    void
    MountPointStatus::Set( const mount_point_list_type& MountPoints )
    {
        //-------------------------------------------------------------------
        // Create sorted list of mount points to add
        //-------------------------------------------------------------------
        mount_point_list_type mp( MountPoints );

        normalize( mp );

        //-------------------------------------------------------------------
        // Modifying access
        //-------------------------------------------------------------------
        ReadWriteLock lock(
            mount_points_baton( ), ReadWriteLock::WRITE, __FILE__, __LINE__ );

        //-------------------------------------------------------------------
        // Generate a list of mount points
        //-------------------------------------------------------------------
        mount_point_list_type mp_old;

        for ( mount_points_type::const_iterator cur = mount_points.begin( ),
                                                last = mount_points.end( );
              cur != last;
              ++cur )
        {
            mp_old.push_back( cur->first );
        }
        //-------------------------------------------------------------------
        // Determine which of the MountPoints are new
        //-------------------------------------------------------------------
        mount_point_list_type additions;

        std::set_difference( mp.begin( ),
                             mp.end( ),
                             mp_old.begin( ),
                             mp_old.end( ),
                             back_inserter( additions ) );

        //-------------------------------------------------------------------
        // Determine which of the MountPoints need to be removed
        //-------------------------------------------------------------------
        mount_point_list_type commons;

        std::set_difference( mp_old.begin( ),
                             mp_old.end( ),
                             mp.begin( ),
                             mp.end( ),
                             back_inserter( commons ) );

        //-------------------------------------------------------------------
        // Modify the mount point lists
        //-------------------------------------------------------------------
        AT( "About to add mount points" );
        add( additions );
        AT( "Finished adding mount points" );
        AT( "About to remove mount points" );
        remove( commons );
        AT( "Finished removinging mount points" );
    }

    MountPointStatus::state_type
    MountPointStatus::Status( const std::string& Path )
    {
        AT( "MountPointStatus::Status: " << Path );
        //-------------------------------------------------------------------
        // Non-modifying access
        //-------------------------------------------------------------------
        ReadWriteLock lock(
            mount_points_baton( ), ReadWriteLock::READ, __FILE__, __LINE__ );
        try
        {
            mount_points_type::iterator mp( mount_points.Locate( Path ) );
            return mp->second->Status( );
        }
        catch ( ... )
        {
        }
        return STATE_ONLINE;
    }

    void
    MountPointStatus::Offline( const std::string& Path )
    {
        static const char* caller = "GenericAPI::MountPointStatus::Offline";

        std::ostringstream msg;

        msg.str( "" );
        msg << "Path: " << Path;
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "FILESYSTEM_STATUS" );
        //-------------------------------------------------------------------
        // Non-modifying access
        //-------------------------------------------------------------------
        ReadWriteLock lock(
            mount_points_baton( ), ReadWriteLock::WRITE, __FILE__, __LINE__ );
        try
        {
            QUEUE_LOG_MESSAGE( "Attempting to locate path: " << Path,
                               MT_DEBUG,
                               10,
                               caller,
                               "FILESYSTEM_STATUS" );
            mount_points_type::iterator mp( mount_points.Locate( Path ) );
            QUEUE_LOG_MESSAGE( "Attempting to flag as offline path: " << Path,
                               MT_DEBUG,
                               10,
                               caller,
                               "FILESYSTEM_STATUS" );
            mp->second->Set( STATE_OFFLINE );
        }
        catch ( ... )
        {
        }
    }
} // namespace GenericAPI

//=======================================================================
// mount_info_type
//=======================================================================

inline void
mount_info_type::Set( state_type State, bool StartScanner )
{
    static const char* caller = "mount_info_type::Set@MountPointStatus";

    std::ostringstream msg;

    QUEUE_LOG_MESSAGE( m_mount_point_dir.Name( )
                           << ": "
                           << "Checking if scanner needs to change state: "
                           << state_string( State )
                           << " =?= " << state_string( state( ) ),
                       MT_DEBUG,
                       10,
                       caller,
                       "FILESYSTEM_STATUS" );
    if ( State != state( ) )
    {
        state( State );
        msg.str( "" );

        msg << "dir: " << m_mount_point_dir.Name( )
            << " state: " << state_string( State );
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "FILESYSTEM_STATUS" );
    }
    QUEUE_LOG_MESSAGE(
        m_mount_point_dir.Name( )
            << ": "
            << "Checking if scanner needs to be restarted: "
            << state_string( State )
            << " =?= " << state_string( MountPointStatus::STATE_OFFLINE )
            << " StartScanner: " << ( StartScanner ? "true" : "false" )
            << " IsAlive: " << ( IsAlive( ) ? "true" : "false" ),
        MT_DEBUG,
        10,
        caller,
        "FILESYSTEM_STATUS" );
    if ( ( State == MountPointStatus::STATE_OFFLINE ) && StartScanner &&
         ( IsAlive( ) == false ) )
    {
        //-----------------------------------------------------------------
        // Start watching for the device to come online.
        //-----------------------------------------------------------------
        msg.str( "" );

        msg << "restart scanner: dir: " << m_mount_point_dir.Name( );
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "FILESYSTEM_STATUS" );
        Spawn( );
    }
}

mount_info_type::mount_info_type( const std::string& Dir )
    : m_mount_point_dir( Dir, false )
{
    state( MountPointStatus::STATE_ONLINE );
    //-------------------------------------------------------------------
    // Assume the state to be online. This will force a scan of the
    //   directory for its true state.
    //-------------------------------------------------------------------
    Set( MountPointStatus::STATE_ONLINE );
}

void
mount_info_type::RestartScanner( )
{
    if ( ( state( ) == MountPointStatus::STATE_OFFLINE ) &&
         ( IsAlive( ) == false ) )
    {
        //-----------------------------------------------------------------
        // Restart because the device is listed as OFFLINE and there
        //   is no thread currently checking the status
        //-----------------------------------------------------------------
        Spawn( );
    }
}

MountPointStatus::state_type
mount_info_type::Status( )
{
#if WORKING || 1
    //-------------------------------------------------------------------
    // Need to check status
    //-------------------------------------------------------------------
    state_type retval = state( );

    if ( retval == MountPointStatus::STATE_OFFLINE )
    {
        //-----------------------------------------------------------------
        // Ensure someone is monitoring for the device's return
        //-----------------------------------------------------------------
        RestartScanner( );
    }
    //-------------------------------------------------------------------
    // Return status to the caller
    //-------------------------------------------------------------------
    return retval;
#else
    return MountPointStatus::STATE_ONLINE;
#endif /* WORKING */
}

void
mount_info_type::cancelation_handler( mount_info_type* Ref )
{
    HERE( );
    AT( "DEBUG: cancelation_handler" << std::endl );
    //-------------------------------------------------------------------
    // Record the state as being offline and do not restart thread
    //-------------------------------------------------------------------
    Ref->Set( MountPointStatus::STATE_OFFLINE, false );
}

void
mount_info_type::action( )
{
    static const char* caller = "mount_info_type::action@MountPointStatus.cc";

    std::ostringstream msg;

    CancellationEnable( false );

    msg.str( "" );
    msg << "action started for: " << m_mount_point_dir.Name( );
    queueLogEntry( msg.str( ),
                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                   0,
                   caller,
                   "FILESYSTEM_STATUS" );
    try
    {
        //-----------------------------------------------------------------
        // Setup to handle cancelation requests
        //-----------------------------------------------------------------
#if USE_CANCEL_SIGNAL
        CancellationType( CANCEL_BY_SIGNAL, CANCEL_SIGNAL );
#else
        //-------------------------------------------------------------------
        // Wait for a good stopping point
        //-------------------------------------------------------------------
        CancellationType( CANCEL_DEFERRED );
        pthread_cleanup_push(
            reinterpret_cast< void ( * )( void* ) >( cancelCleanup ),
            reinterpret_cast< void* >( this ) );
#endif
        CancellationEnable( true );
        AT( "DEBUG: OFFLINE: " << m_mount_point_dir.Name( ) << " "
                               << GPSTime::NowGPSTime( ) << std::endl );
        //-------------------------------------------------------------------
        // This is the process which checks if the device is ONLINE.
        //   This routine will only change the state if it completes
        //   before the timeout interval.
        //-------------------------------------------------------------------
        bool retcode = test( );
        msg.str( "" );
        msg << "test retcode: " << retcode
            << " for: " << m_mount_point_dir.Name( );
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "FILESYSTEM_STATUS" );
        CancellationEnable( false );
        if ( retcode == true )
        {
            Set( MountPointStatus::STATE_ONLINE ); // Mark status as ONLINE
            AT( "DEBUG: ONLINE: " << m_mount_point_dir.Name( ) << " "
                                  << GPSTime::NowGPSTime( ) << std::endl );
        }
#if USE_CANCEL_SIGNAL
#else
        pthread_cleanup_pop( 0 );
#endif
    }
    catch ( const std::exception& Exception )
    {
        msg.str( "" );
        msg << "had exception: " << Exception.what( )
            << " for: " << m_mount_point_dir.Name( );
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "FILESYSTEM_STATUS" );
#if WORKING
        queueLogEntry( Exception.what( ),
                       GenericAPI::LogEntryGroup_type::MT_ORANGE,
                       0,
                       caller,
                       "CXX_THREAD" );
#endif /* WORKING */
    }
    catch ( ... )
    {
        //-----------------------------------------------------------------
        // Catch any exceptions and discard them as there is no
        //   way to feed it back to the user in a meaningful way.
        //-----------------------------------------------------------------
        AT( "Exception" );
        msg.str( "" );
        msg << "had unknown exception for: " << m_mount_point_dir.Name( );
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "FILESYSTEM_STATUS" );
    }
    CancellationEnable( false );
    msg.str( "" );
    msg << "action completed for: " << m_mount_point_dir.Name( );
    queueLogEntry( msg.str( ),
                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                   0,
                   caller,
                   "FILESYSTEM_STATUS" );
    //-------------------------------------------------------------------
    // No one is waiting to collect this thread. Detach to prevent
    // resource leak.
    //-------------------------------------------------------------------
    Detach( );
    m_mount_point_dir.Mode( Directory::MODE_BLOCKING );
    m_mount_point_dir.Close( );
}

bool
mount_info_type::test( )
{
    const std::string          dir_name( m_mount_point_dir.Name( ) );
    const Directory::mode_type previous_blocking_mode(
        m_mount_point_dir.Mode( ) );
#if VERBOSE_DEBUGGING
    std::ostringstream hdr;

    hdr << "mount_info_type::test: dir: " << m_mount_point_dir.Name( );
#endif /* VERBOSE_DEBUGGING */
    AT( hdr.str( ) );
    try
    {
        m_mount_point_dir.Mode( Directory::MODE_BLOCKING );
        AT( hdr.str( ) );
        errno = 0;
        m_mount_point_dir.Open( );
        errno = 0;
        if ( m_mount_point_dir.Next( ) == false )
        {
            if ( errno != 0 )
            {
                throw std::runtime_error( "" );
            }
        }
        AT( hdr.str( ) );
    }
    catch ( const std::exception& Exception )
    {
        m_mount_point_dir.Mode( previous_blocking_mode );
        if ( errno == EINTR )
        {
            AT( hdr.str( ) << " Interupted system call: " << dir_name << " "
                           << GPSTime::NowGPSTime( ) << std::endl );
            AT( hdr.str( ) );
            return false;
        }
    }
    catch ( ... )
    {
        m_mount_point_dir.Mode( previous_blocking_mode );
        if ( errno == EINTR )
        {
            AT( hdr.str( ) << " Interupted system call: " << dir_name << " "
                           << GPSTime::NowGPSTime( ) << std::endl );
            AT( hdr.str( ) );
            return false;
        }
    }
    m_mount_point_dir.Mode( previous_blocking_mode );
    AT( hdr.str( ) );
    return true;
} // method - test

void
add( const mount_point_list_type& MountPoints )
{
    //---------------------------------------------------------------------
    // Add any new entries into the list
    //---------------------------------------------------------------------
#if VERBOSE_DEBUGGING
    int i = 1;
    int max = MountPoints.size( );
#endif /* VERBOSE_DEBUGGING */
    std::unique_ptr< mount_info_type > mi;

    for ( mount_point_list_type::const_iterator cur = MountPoints.begin( ),
                                                last = MountPoints.end( );
          cur != last;
          ++cur )
    {
        AT( "Adding Mount Point: " << *cur << "( " << i++ << " of " << max
                                   << " )" );
        mi.reset( new mount_info_type( *cur ) );
        if ( mi.get( ) )
        {
            mount_points[ *cur ] = mi.release( );
        }
    }
}

static void
cleanup_at_exit( )
{
    (void)is_initialized;
    //---------------------------------------------------------------------
    // Terminate the threads
    //---------------------------------------------------------------------
    fork_prepare( );
    //---------------------------------------------------------------------
    // Remove entries from the list
    //---------------------------------------------------------------------
    for ( mount_points_type::iterator cur = mount_points.begin( ),
                                      last = mount_points.end( );
          cur != last;
          ++cur )
    {
        //-------------------------------------------------------------------
        // Deallocate memory
        //-------------------------------------------------------------------
        delete ( cur->second );
        //-------------------------------------------------------------------
        // Remove from the list
        //-------------------------------------------------------------------
    }
    mount_points.erase( mount_points.begin( ), mount_points.end( ) );
}

static void
fork_parent( )
{
    static const char* const caller = "fork_parent@MountPointStatus.cc";
    std::ostringstream       msg;

    msg << "begin";
    queueLogEntry( msg.str( ),
                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                   0,
                   caller,
                   "CXX_THREAD" );

    AT( "DEBUG: fork_parent: begin" << std::endl );
    //---------------------------------------------------------------------
    // Have exclusive access to the list of mount points
    //---------------------------------------------------------------------
    ReadWriteLock lock(
        mount_points_baton( ), ReadWriteLock::WRITE, __FILE__, __LINE__ );
    //---------------------------------------------------------------------
    // Restart checking for OFFLINE mount points
    //---------------------------------------------------------------------
    for ( mount_points_type::iterator cur = mount_points.begin( ),
                                      last = mount_points.end( );
          cur != last;
          ++cur )
    {
        cur->second->RestartScanner( );
    }

    msg.str( "" );
    msg << "end";
    queueLogEntry( msg.str( ),
                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                   0,
                   caller,
                   "CXX_THREAD" );
}

static void
fork_prepare( )
{
    //---------------------------------------------------------------------
    // Have exclusive access to the list of mount points
    //---------------------------------------------------------------------
    static const char* const caller = "fork_prepare@MountPointStatus.cc";
    std::ostringstream       msg;

    msg << "begin";
    queueLogEntry( msg.str( ),
                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                   0,
                   caller,
                   "CXX_THREAD" );

    ReadWriteLock lock(
        mount_points_baton( ), ReadWriteLock::WRITE, __FILE__, __LINE__ );
    //---------------------------------------------------------------------
    // Cancel checking for OFFLINE mount points
    //---------------------------------------------------------------------
    for ( mount_points_type::iterator cur = mount_points.begin( ),
                                      last = mount_points.end( );
          cur != last;
          ++cur )
    {
        msg.str( "" );
        msg << "mount_point: " << cur->first;
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       caller,
                       "CXX_THREAD" );

        cur->second->Cancel( );
        cur->second->Join( );
    }

    msg.str( "" );
    msg << "end";
    queueLogEntry( msg.str( ),
                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                   0,
                   caller,
                   "CXX_THREAD" );
}

static void
remove( const mount_point_list_type& MountPoints )
{
    //---------------------------------------------------------------------
    // remove from the mount point list
    //---------------------------------------------------------------------
    for ( mount_point_list_type::const_iterator cur = MountPoints.begin( ),
                                                last = MountPoints.end( );
          cur != last;
          ++cur )
    {
        AT( "Removing Mount Point: " << *cur );
        //-------------------------------------------------------------------
        // Find the element
        //-------------------------------------------------------------------
        mount_points_type::iterator i = mount_points.find( *cur );

        if ( i != mount_points.end( ) )
        {
            //-----------------------------------------------------------------
            // Deallocate memory
            //-----------------------------------------------------------------
            delete ( i->second );
            //-----------------------------------------------------------------
            // Remove from the list
            //-----------------------------------------------------------------
            mount_points.erase( i );
        }
    }
}
