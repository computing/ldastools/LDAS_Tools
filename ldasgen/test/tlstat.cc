//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/stat.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <iomanip>

#include "ldastoolsal/unittest.h"
#include "ldastoolsal/MemChecker.hh"

#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"
#include "genericAPI/StatFork.hh"
#include "genericAPI/StatPool.hh"
#include "genericAPI/Stat.hh"

using namespace GenericAPI;

LDASTools::Testing::UnitTest Test;

namespace
{
    class TestStat : public StatFork
    {
    public:
#if 1 && 0
        TestStat( );
#endif

        static const char* LStatCommand( );

    private:
        static bool initialized;
        static char lstat_command[ 4096 ];
    };

    bool TestStat::initialized = false;
    char TestStat::lstat_command[];

#if 1 && 0
    TestStat::TestStat( )
    {
        SetCommand( LStatCommand( ) );
    }
#endif

    inline const char*
    TestStat::LStatCommand( )
    {
        if ( initialized == false )
        {
            const char* abs_builddir = ::getenv( "ABS_BUILDDIR" );

            if ( !abs_builddir )
            {
                abs_builddir = "";
            }
            sprintf( lstat_command, "%s/%s", abs_builddir, "../src/lstat" );
            initialized = true;
        }
        return lstat_command;
    }

#if 1 && 0
    void
    lstat_pool_cb( StatPool::info_type Key )
    {
        StatFork* flstat = NULL;

        if ( ( flstat = dynamic_cast< StatFork* >( Key ) ) )
        {
            flstat->SetCommand( TestStat::LStatCommand( ) );
        }
    }
#endif /* 1 && 0 */

    bool
    ValidateStatBuf( const std::string& Element, bool Cond )
    {
        Test.Check( Cond ) << Element << std::endl;
        return Cond;
    }

    void
    ValidateStat( const struct stat& Local,
                  const struct stat& Remote,
                  int                LocalRetCode,
                  int                RemoteRetCode,
                  int                LocalErrCode,
                  int                RemoteErrCode )
    {
        //-------------------------------------------------------------------
        // Verify that they have generated the same results
        //-------------------------------------------------------------------
        bool cond = ( LocalRetCode == RemoteRetCode );

        Test.Check( cond ) << " return code equivalence"
                           << " (" << LocalRetCode << " vs. " << RemoteRetCode
                           << ")" << std::endl;
        if ( cond )
        {
            if ( LocalRetCode == 0 )
            {
                //---------------------------------------------------------------
                // Both were successful with getting information.
                // Ensure the correctness of the data retrieved
                //---------------------------------------------------------------
                ValidateStatBuf( "st_dev", Local.st_dev == Remote.st_dev );
                ValidateStatBuf( "st_ino", Local.st_ino == Remote.st_ino );
                ValidateStatBuf( "st_mode", Local.st_mode == Remote.st_mode );
                ValidateStatBuf( "st_nlink",
                                 Local.st_nlink == Remote.st_nlink );
                ValidateStatBuf( "st_uid", Local.st_uid == Remote.st_uid );
                ValidateStatBuf( "st_gid", Local.st_gid == Remote.st_gid );
                ValidateStatBuf( "st_rdev", Local.st_rdev == Remote.st_rdev );
                ValidateStatBuf( "st_size", Local.st_size == Remote.st_size );
                ValidateStatBuf( "st_blksize",
                                 Local.st_blksize == Remote.st_blksize );
                if ( ValidateStatBuf( "st_blocks",
                                      Local.st_blocks == Remote.st_blocks ) ==
                     false )
                {
                    Test.Message( )
                        << "  Expected: " << Local.st_blocks
                        << " Received: " << Remote.st_blocks << std::endl;
                }
                ValidateStatBuf( "st_atime",
                                 Local.st_atime == Remote.st_atime );
                ValidateStatBuf( "st_mtime",
                                 Local.st_mtime == Remote.st_mtime );
                ValidateStatBuf( "st_ctime",
                                 Local.st_ctime == Remote.st_ctime );
            }
            else
            {
                //-------------------------------------------------------------
                // Both were unsuccessful with getting information
                // Ensure the correctness of the error code
                //-------------------------------------------------------------
                Test.Check( LocalErrCode == RemoteErrCode )
                    << " errno equivalence"
                    << " (" << LocalErrCode << " vs. " << RemoteErrCode << ")"
                    << std::endl;
            }
        }
        else
        {
            Test.Message( ) << " Error codes: Local: " << LocalErrCode << ": "
                            << strerror( LocalErrCode ) << std::endl;
            Test.Message( ) << " Error codes: Remote: " << RemoteErrCode << ": "
                            << strerror( RemoteErrCode ) << std::endl;
        }
    }

    void
    Validate( const std::string& Filename )
    {
        errno = 0;
        enum
        {
            LOCAL = 0,
            REMOTE,
            POOL,
            FUNCTION,
            END_OF_LIST
        };

        int         retcode[ END_OF_LIST ];
        struct stat statbuf[ END_OF_LIST ];
        int         errcode[ END_OF_LIST ];

        //-------------------------------------------------------------------
        // Run stat command both locally and remote
        //-------------------------------------------------------------------
        //...................................................................
        // Stat using local lstat
        //...................................................................
        retcode[ LOCAL ] = ::lstat( Filename.c_str( ), &( statbuf[ LOCAL ] ) );
        errcode[ LOCAL ] = errno;
#if 1 && 0
        {
            //.................................................................
            // Stat using process
            //.................................................................
            TestStat tlstat;

            tlstat.Spawn( );
            retcode[ REMOTE ] = tlstat.LStat( Filename, statbuf[ REMOTE ] );
            errcode[ REMOTE ] = errno;

            ValidateStat( statbuf[ LOCAL ],
                          statbuf[ REMOTE ],
                          retcode[ LOCAL ],
                          retcode[ REMOTE ],
                          errcode[ LOCAL ],
                          errcode[ REMOTE ] );
        }
#endif /* 0 */
#if 1 && 0
        {
            //.................................................................
            // Stat using POOL
            //.................................................................
            StatPool::UserInitCB( lstat_pool_cb );

            StatPool::info_type tlstat = StatPool::Request( );
            retcode[ POOL ] = tlstat->LStat( Filename, statbuf[ POOL ] );
            errcode[ POOL ] = errno;
            ValidateStat( statbuf[ LOCAL ],
                          statbuf[ POOL ],
                          retcode[ LOCAL ],
                          retcode[ POOL ],
                          errcode[ LOCAL ],
                          errcode[ POOL ] );
            StatPool::Release( tlstat );
        }
#endif /* 0 */
#if 1 && 1
        {
            //.................................................................
            // Stat using FUNCTION
            //.................................................................
            retcode[ FUNCTION ] =
                GenericAPI::LStat( Filename, statbuf[ FUNCTION ] );
            errcode[ FUNCTION ] = errno;
            ValidateStat( statbuf[ LOCAL ],
                          statbuf[ FUNCTION ],
                          retcode[ LOCAL ],
                          retcode[ FUNCTION ],
                          errcode[ LOCAL ],
                          errcode[ FUNCTION ] );
        }
#endif /* 0 */
    }
} // namespace

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );
    {
        LDASTools::AL::MemChecker::Trigger trigger( true );

        GenericAPI::LoggingInfo::LogDirectory( "-" );
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        GenericAPI::LDASplatform::AppName( "tlstat" );

        char filename[ 2048 ];

        const char* abs_builddir( ::getenv( "ABS_BUILDDIR" ) );

        std::fill( filename, filename + sizeof( filename ), '\0' );
        if ( abs_builddir )
        {
            size_t l( ::strlen( abs_builddir ) );
            std::copy( abs_builddir, abs_builddir + l, filename );
            const char* base = "/tlstat";
            size_t      lb = ::strlen( base );
            std::copy( base, base + lb, filename + l );
        }
        else
        {
            const char* base = "./tlstat";
            size_t      lb = ::strlen( base );
            std::copy( base, base + lb, filename );
        }

        Validate( filename );
        Validate( "./Makefile" );
        Validate( "./NoSuchFile.xyz" );

#if 0
        GenericAPI::SyncLog( );
#endif /* 0 */
    }

    StatPool::Cleanup( );

    Test.Exit( );

    return 1; // Should never get here so exit with error if it happens
}
