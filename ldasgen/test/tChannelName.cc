//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/stat.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>

#include "genericAPI/ChannelNameLexer.hh"

inline void
validate_multiple( const std::string& seperator )
{
    using GenericAPI::ChannelNameLexer;

    std::vector< std::string > channel_names_data;
    channel_names_data.push_back( "Z1:a" );
    channel_names_data.push_back( "Z1:b" );

    ChannelNameLexer channel_names(
        boost::algorithm::join( channel_names_data, seperator ) );

    std::ostringstream header;

    header << "channel_name_lexer_constructor: ";

    BOOST_TEST_MESSAGE( header.str( )
                        << "There are "
                        << channel_names.GetChannelNames( ).size( )
                        << " channels" );

    BOOST_CHECK( channel_names.GetChannelNames( ).size( ) == 2 );

    auto channels = channel_names.GetChannelNames( );
    auto CHANNEL_NOT_FOUND = channels.end( );

    BOOST_CHECK( channels.find( channel_names_data[ 0 ] ) !=
                 CHANNEL_NOT_FOUND );
    BOOST_CHECK( channels.find( channel_names_data[ 1 ] ) !=
                 CHANNEL_NOT_FOUND );
}
BOOST_AUTO_TEST_CASE( channel_name_lexer_constructor_single )
{
    using GenericAPI::ChannelNameLexer;

    ChannelNameLexer channel_names( "Z1:a" );

    std::ostringstream header;

    header << "channel_name_lexer_constructor: ";

    BOOST_TEST_MESSAGE( header.str( )
                        << "There are "
                        << channel_names.GetChannelNames( ).size( )
                        << " channels" );

    BOOST_CHECK( channel_names.GetChannelNames( ).size( ) == 1 );

    auto channels = channel_names.GetChannelNames( );
    auto CHANNEL_NOT_FOUND = channels.end( );

    BOOST_CHECK( channels.find( "Z1:a" ) != CHANNEL_NOT_FOUND );
}

BOOST_AUTO_TEST_CASE( channel_name_lexer_constructor_multiple_spaces )
{
  validate_multiple( " " );
}

BOOST_AUTO_TEST_CASE( channel_name_lexer_constructor_multiple_semicolon )
{
  validate_multiple( ";" );
}

BOOST_AUTO_TEST_CASE( channel_name_lexer_constructor_multiple_coma )
{
  validate_multiple( "," );
}

BOOST_AUTO_TEST_CASE( channel_name_lexer_constructor_multiple_newline )
{
  validate_multiple( "\n" );
}
