//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <list>
#include <string>

#include "genericAPI/TCL.hh"

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>

BOOST_AUTO_TEST_CASE( tcl_parse_list )
{
  std::list< std::string > result;

  GenericAPI::TCL::ParseList( "{ parm1 parm2 parm3 parm4 parm5 }", result );
  const std::list< std::string > result_expected {
    "parm1",
    "parm2",
    "parm3",
    "parm4",
    "parm5",
  };

  BOOST_CHECK_MESSAGE( result.size( ) == 5,
                       "parsed string into " << result.size( ) << " element(s) instead of 1." );
  BOOST_CHECK_MESSAGE( result.front( ).compare( result_expected.front( ) ) == 0,
                       "parsed string into '" << result.front( ) << "'" );
}
