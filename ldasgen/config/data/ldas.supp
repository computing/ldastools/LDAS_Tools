#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#
{
  valgrind/pthread
  Memcheck,Addrcheck:Leak
  fun:malloc
  obj:*/valgrind-*/libpthread.so
  obj:*/valgrind-*/libpthread.so
  fun:pthread_key_create
}

{
  memcopy/overlap
  Memcheck,Addrcheck:Overlap
  fun:memcpy
}

{
  yy_flex_alloc
  Memcheck,Addrcheck:Leak
  fun:malloc
  fun:_Z13yy_flex_allocj
  fun:_ZN11yyFlexLexer13yy_push_stateEi
}

{
  dl - add_dependency
  Memcheck,Addrcheck:Leak
  fun:malloc
  fun:realloc
  fun:add_dependency
  fun:_dl_lookup_symbol_internal
}

{
  fftw_malloc
  Memcheck,Addrcheck:Leak
  fun:malloc
  fun:fftw_malloc
}

{
  fftwf_malloc
  Memcheck,Addrcheck:Leak
  fun:malloc
  fun:fftwf_malloc
}

{
  /lib/ld-2.3.2.so
  Memcheck,Addrcheck:Leak
  fun:calloc
  fun:_dlerror_run
}

{
  GCC Exception Throwing
  Memcheck,Addrcheck:Leak
  fun:malloc
  fun:__cxa_get_globals
  fun:__cxa_throw
}

{
  /lib/ld-2.3.2.so
  Memcheck,Addrcheck:Leak
  fun:*alloc
  obj:*/ld-2.3.2.so*
}

{
  ObjectSpace-1.2.12 - initialization (type 1)
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_Z15os_class_of_aux*
}

{
  ObjectSpace-1.2.12 - initialization (type 2)
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_Z*os_class_db*
  fun:_Z*os_class_db6insert*
  fun:_Z*os_classC*
  fun:_Z15os_class_of_aux*
  fun:_Z21_os_class_reg_func_19v
  fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (type 3)
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_Z*
  fun:_ZN8os_class8add_baseE*
  fun:_ZN8os_class8add_baseE*
  fun:_Z22_os_class_reg_func_*
  fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (type 4)
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_ZNSs4_Rep9_S_createEjjRKSaIcE
  fun:_ZNSs4_Rep8_M_cloneERKSaIcEj
  fun:_ZNSs7reserveEj
  fun:_ZN8os_classC1ERKSt9type_infoPKciPi
  fun:_Z15os_class_of_auxP10os_network
  fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (type 5)
  Memcheck,Addrcheck:Leak
  fun:_ZN8os_classC1ERKSt9type_infoPKciPi
  fun:_Z15os_class_of_auxP10os_network
  fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (id_map)
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_ZN11os_class_db6id_mapEv
  fun:_ZN11os_class_db7with_idEi
  fun:_ZN11os_class_db6insertER8os_classRKSt9type_info
  fun:_ZN8os_classC1ERKSt9type_infoPKciPi
  fun:_Z15os_class_of_auxP10os_network
  fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (tmp 1)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZN8os_class8add_baseERS_PFPvS1_E
   fun:_Z22_os_class_reg_func_278v
   fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (tmp 2)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZNSt8_Rb_treeIP8os_classSt4pairIKS1_P7os_castESt10_Select1stIS6_ESt4lessIS1_ESaIS6_EE9_M_insertEPSt18_Rb_tree_node_baseSE_RKS6_
   fun:_ZNSt8_Rb_treeIP8os_classSt4pairIKS1_P7os_castESt10_Select1stIS6_ESt4lessIS1_ESaIS6_EE13insert_uniqueERKS6_
   fun:_ZNSt8_Rb_treeIP8os_classSt4pairIKS1_P7os_castESt10_Select1stIS6_ESt4lessIS1_ESaIS6_EE13insert_uniqueESt17_Rb_tree_iteratorIS6_ERKS6_
}

{
  ObjectSpace-1.2.12 - initialization (tmp 3)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZNSt8_Rb_treeIPSt9type_infoSt4pairIKS1_P8os_classESt10_Select1stIS6_E17os_less_type_infoSaIS6_EE9_M_insertEPSt18_Rb_tree_node_baseSD_RKS6_
   fun:_ZNSt8_Rb_treeIPSt9type_infoSt4pairIKS1_P8os_classESt10_Select1stIS6_E17os_less_type_infoSaIS6_EE13insert_uniqueERKS6_
   fun:_ZNSt8_Rb_treeIPSt9type_infoSt4pairIKS1_P8os_classESt10_Select1stIS6_E17os_less_type_infoSaIS6_EE13insert_uniqueESt17_Rb_tree_iteratorIS6_ERKS6_
}
{
  ObjectSpace-1.2.12 - initialization (tmp 4)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZNSt8_Rb_treeISsSt4pairIKSsP8os_classESt10_Select1stIS4_ESt4lessISsESaIS4_EE9_M_insertEPSt18_Rb_tree_node_baseSC_RKS4_
   fun:_ZNSt8_Rb_treeISsSt4pairIKSsP8os_classESt10_Select1stIS4_ESt4lessISsESaIS4_EE13insert_uniqueERKS4_
   fun:_ZNSt8_Rb_treeISsSt4pairIKSsP8os_classESt10_Select1stIS4_ESt4lessISsESaIS4_EE13insert_uniqueESt17_Rb_tree_iteratorIS4_ERKS4_
}

{
  ObjectSpace-1.2.12 - initialization (tmp 5)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZNSt8_Rb_treeIiSt4pairIKiP8os_classESt10_Select1stIS4_ESt4lessIiESaIS4_EE9_M_insertEPSt18_Rb_tree_node_baseSC_RKS4_
   fun:_ZNSt8_Rb_treeIiSt4pairIKiP8os_classESt10_Select1stIS4_ESt4lessIiESaIS4_EE13insert_uniqueERKS4_
   fun:_ZNSt8_Rb_treeIiSt4pairIKiP8os_classESt10_Select1stIS4_ESt4lessIiESaIS4_EE13insert_uniqueESt17_Rb_tree_iteratorIS4_ERKS4_
}

{
  ObjectSpace-1.2.12 - initialization (tmp 6)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZN8os_classC1ERKSt9type_infoPKciPi
   fun:_Z15os_class_of_auxP10os_network
   fun:_Z41__static_initialization_and_destruction_0ii
}

{
  ObjectSpace-1.2.12 - initialization (tmp 7)
   Addrcheck:Leak
   fun:_Znwj
   fun:_ZN8os_class8add_base*
   fun:_Z22_os_class_reg_func_*
   fun:_Z41__static_initialization_and_destruction_0ii
}

{
  STLport - pool
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_ZN4_STL12__node_allocILb1ELi0EE14_S_chunk_allocEjRi
}

{
  STLport - pool (string)
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_ZN4_STL12_String_baseIcNS_9allocatorIcEEE17_M_allocate_blockEj
}

{
  STLport - static initialization
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_ZN4_STL20_Stl_create_wfilebufEP8_IO_FILEi
  fun:_ZN4_STL8ios_base13_S_initializeEv
  fun:_ZN4_STL8ios_base4InitC1Ev
  fun:_Z41__static_initialization_and_destruction_0ii
}

{
  STLport - static initialization - 2
  Memcheck,Addrcheck:Leak
  fun:_Znwj
  fun:_ZN4_STL8ios_base13_S_initializeEv
  fun:_ZN4_STL8ios_base4InitC1Ev
  fun:_Z41__static_initialization_and_destruction_0ii
}
##===============================================================================
## lib/general issues
##===============================================================================
# Thread class issues
# Valgrind 3.5.0
#   GCC: 4.1.2
#--------------------------------------------------------------------------------
{
   Thread spawning race condition
   Helgrind:Race
   obj:*/lib/valgrind/vgpreload_helgrind-*.so
   fun:start_thread
   fun:clone
}
#--------------------------------------------------------------------------------
# TaskThread class issues
# Valgrind 3.5.0
#   GCC: 4.1.2
#--------------------------------------------------------------------------------
{
   TaskThread Construction race condition
   Helgrind:Race
   obj:*/lib/valgrind/vgpreload_helgrind-*.so
   fun:pthread_create@*
   fun:_ZN7General6Thread5spawnEPFPvS1_E
   fun:_ZN7General6Thread5SpawnEv
   fun:_ZN7General10TaskThreadC1Ev
   fun:_ZN7General10ThreadPool7AcquireEv
   fun:_ZN7General7TimeoutEPNS_4TaskEib
   ...
}
#--------------------------------------------------------------------------------
# lib/ldastoolsal/test/tLDASUnexpected.cc issues
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# Valgrind 3.5.0
#   GCC: 4.1.2
#--------------------------------------------------------------------------------
{
   LDASUnexpected string leak
   Memcheck:Leak
   fun:_Znwm
   fun:_ZNSs4_Rep9_S_createEmmRKSaIcE
   fun:_ZNSs9_M_mutateEmmm
   fun:_ZNSs15_M_replace_safeEmmPKcm
   fun:_ZN9LDASTools5Error14LDASUnexpected10unexpectedEv
   obj:/usr/lib64/libstdc++.so.6.0.13
   fun:__cxa_call_unexpected
   fun:_Z14TestInvalid001v
   fun:main
}
{
   LDASUnexpected string leak - 2
   Memcheck:Leak
   fun:_Znwm
   fun:_ZNSs4_Rep9_S_createEmmRKSaIcE
   obj:/usr/lib64/libstdc++.so.6.0.13
   fun:_ZNSsC1EPKcRKSaIcE
   fun:_Z14TestInvalid001v
   fun:main
}
{
   tLDASUnexpected exception leak due to abort
   Memcheck:Leak
   fun:malloc
   fun:__cxa_allocate_exception
   fun:_Z14TestInvalid001v
   fun:main
}
{
   tLDASUnexpected exception leak due to abort - 2
   Memcheck,Addrcheck:Leak
   fun:malloc
   fun:__cxa_allocate_exception
   fun:_ZN9LDASTools5Error14LDASUnexpected10unexpectedEv
   obj:/usr/lib64/libstdc++.so.6.0.13
   fun:__cxa_call_unexpected
   fun:_Z14TestInvalid001v
   fun:main
}
#--------------------------------------------------------------------------------
# lib/ldastoolsal/test/tTimeout.cc issues
#--------------------------------------------------------------------------------
# Valgrind 3.5.0
#   GCC: 4.1.2
#--------------------------------------------------------------------------------
{
   tTimeout data race - 1
   Helgrind:Race
   obj:*/lib/valgrind/vgpreload_helgrind-*.so
   fun:pthread_create@*
   fun:_Z12sleeper_testii
   fun:main
}
#--------------------------------------------------------------------------------
# Valgrind 3.5.0
#   GCC: 4.1.2
#--------------------------------------------------------------------------------
{
   tTimeout data race - 2
   Helgrind:Race
   obj:*/lib/valgrind/vgpreload_helgrind-*.so
   fun:pthread_create@*
   fun:_ZN7General6Thread5spawnEPFPvS1_E
   fun:_ZN7General6Thread5SpawnEv
   fun:_ZN7General10TaskThreadC1Ev
   fun:_Z16task_thread_testv
   fun:main
}
##===============================================================================
## api/genericAPI issues
##===============================================================================
# Thread class issues
# Valgrind 3.5.0
#   GCC: 4.1.2
#--------------------------------------------------------------------------------
{
   queueLogEntry logging message for fork_prepare
   Helgrind:Race
   obj:/ldcg/stow_pkgs/valgrind-3.5.0/lib/valgrind/vgpreload_helgrind-x86-linux.so
   fun:pthread_create@*
   fun:_ZN7General6Thread5spawnEPFPvS1_E
   fun:_ZN7General6Thread5SpawnEv
   fun:_Z8find_logRKSs
   fun:_ZN10GenericAPI13queueLogEntryERKSsiiS1_S1_jS1_
   fun:_Z12fork_preparev
   fun:fork
   fun:fork
   fun:_ZN7General4Fork5SpawnEv
   fun:_ZN15_GLOBAL__N_Test8ValidateERKSs
   fun:main
}
