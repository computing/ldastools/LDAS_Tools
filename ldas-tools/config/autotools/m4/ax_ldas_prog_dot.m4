dnl======================================================================
dnl AX_LDAS_PROG_DOT
dnl======================================================================
AC_DEFUN([AX_LDAS_PROG_DOT],
[ AC_CACHE_CHECK([whether dot command works],
	         [ldas_cv_shell_dot],
	         [AC_ARG_WITH([dot],
			      [AS_HELP_STRING([--with-dot],
					      [enable support for the dot command])],
			      [],
			      [with_dot=yes])
		  AS_IF([test "x$with_dot" = xyes],
		  	[AC_PATH_PROGS( ldas_cv_shell_dot, dot )],
			[AS_IF([test "x$with_dot" != xno],
			       [ldas_cv_shell_dot="$with_dot"])])
  	          case x$ldas_cv_shell_dot in
                  x) ldas_cv_shell_dot="no";;
                  *) ( $ldas_cv_shell_dot < /dev/null ) 2>/dev/null || ldas_cv_shell_dot=""
	             ;;
		  esac])
  DOT="$ldas_cv_shell_dot"
  case x$DOT in
  x|xno) unset DOT;;
  *)
    DOT_PATH="`echo $DOT | sed -e 's,/[^/]*$,,'`"
    ;;
  esac
  AC_SUBST([DOT_PATH])
  AM_CONDITIONAL([HAVE_DOT],[test x$DOT != x])
])
