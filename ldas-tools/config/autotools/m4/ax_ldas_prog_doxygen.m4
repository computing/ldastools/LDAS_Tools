dnl======================================================================
dnl AX_LDAS_PROG_DOXYGEN
dnl======================================================================
AC_DEFUN([AX_LDAS_PROG_DOXYGEN],
[ AC_REQUIRE([AX_LDAS_PROG_LATEX])
  AC_REQUIRE([AX_LDAS_PROG_DOT])
  AC_ARG_WITH([doxygen],
	AC_HELP_STRING([--with-doxygen],[Specify the path for doxygen]),
	[],[with_doxygen=doxygen])
  AS_CASE([x"$with_doxygen"],
          [x|xno],[unset DOXYGEN],
	  [xyes],[AC_PATH_PROGS([DOXYGEN],[doxygen])],
	  [AC_PATH_PROGS([DOXYGEN],[${with_doxygen}])])
  AC_MSG_CHECKING([for doxygen])
  AS_CASE([x$DOXYGEN],
  	  [x],[AC_MSG_RESULT(no)],
    	  [AC_MSG_RESULT($DOXYGEN)])
  AM_CONDITIONAL([HAVE_DOXYGEN],[test x$DOXYGEN != x])
])
