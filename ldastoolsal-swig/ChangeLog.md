# Release 2.6.11 - December 16, 2024
    - This fixes #188 (closed) by adding debian/source/format, and enhancing
      debian/control and  debian/rules to properly handle Python modules.

# Release 2.6.10 - June 17, 2022
    - Removed support for Python 2 (closes #139)

# Release 2.6.9 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.6.7 - August 14, 2019
  - Corrected Portfile to have proper description (Closes #55)

# Release 2.6.6 - December 6, 2018
  - Addressed packaging issues

# Release 2.6.5 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Updated to cmake_helper 1.0.4

# Release 2.6.4 - August 27, 2018
  - Removed erronously installed README (closes ticket #17)
  - Enhancements to further support for Pythyon 3

# Release 2.6.3 - July 6, 2018
  - Corrections for Red Hat packaging

# Release 2.6.2 - June 27, 2018
  - Updated macro used for Python 3 package version

# Release 2.6.1 - June 22, 2018
  - Updated MODULE_DIR to LDASTools (closes ticket #6)

# Release 2.6.0 - June 19, 2018
  - Separated SWIG extensions into their own package
