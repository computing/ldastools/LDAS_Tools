#
# LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo Frame
# Files
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools Frame Utilities is free software; you may redistribute it and/or
# modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools Frame Utilities is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#DOXYGEN_TAGFILES = \
#	$(top_builddir)/libraries/ldastoolsal/doc/ldastoolsal.tag=/doc/ldastoolsal/html \
#	$(top_builddir)/libraries/framecpp/doc/framecpp.tag=/doc/framecpp/html \
#	$(top_builddir)/libraries/filters/doc/filters.tag=/doc/filters/html \
#	$(top_builddir)/libraries/ldasgen/doc/ldasgen.tag=/doc/ldasgen/html
#DOXYGEN_GENERATE_LATEX = NO

#set( example_path ${CMAKE_CURRENT_SOURCE_DIR} )
#cx_target_manual(
#  TARGET Developer
#  CONFIGURATION Developer
#  EXAMPLE_PATH ${example_path}
#  GENERATE_HTML
#  GENERATE_LATEX
#  )

find_package( Doxygen REQUIRED dot )

set(
  DEVELOPER_SOURCES
  developer.txt
  BuildOrder.dot
  PackageDependencies.dot
  )

# General Options
set( DOXYGEN_GENERATE_HTML YES )
set( DOXYGEN_GENERATE_LATEX YES )
set( DOXYGEN_DOTFILE_DIRS ${CMAKE_CURRENT_SOURCE_DIR} )
# Developer Options
set( DOXYGEN_EXTRACT_ALL YES )
set( DOXYGEN_EXTRACT_PRIVATE YES )
set( DOXYGEN_EXTRACT_STATIC YES )
set( DOXYGEN_EXTRACT_LOCAL_CLASSES YES )
set( DOXYGEN_EXTRACT_LOCAL_METHODS YES )
set( DOXYGEN_EXTRACT_ANON_NSPACES YES )
set( DOXYGEN_OUTPUT_DIRECTORY Developer )

doxygen_add_docs(
  Developer
  ${DEVELOPER_SOURCES}
  ALL
  USE_STAMP_FILE
  COMMENT "Creating Developer documents"
  )

install(
  DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/Developer/html
  DESTINATION ${CMAKE_INSTALL_DOCDIR}/Developer )

