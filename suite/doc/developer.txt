/* -*- mode: C++ -*- */
/*! @mainpage

Docker Testing build (2hr limit):

@code{.sh}
      gitlab-runner exec docker --timeout 7200 level1-el7:rpm:framecpp
@endcode

Docker Interactive Debugging:

@code{.sh}
      docker run -it --rm -v $(pwd):/project0 ligo/lalsuite-dev:el7 /bin/bash
@endcode

@section DependencyDirectory Directory Dependencies

@dotfile PackageDependencies.dot "Package Dependencies"
@dotfile BuildOrder.dot "Build Order"

@subsection RPMMinimalInstalledPackages Minimal Set of RPM Packages

Below is the list of the minimum packages that need to be installed
to install all packages provided by LDAS Tools Suite,
minus the debugging packages.

  - ldas-tools-utilities
  - python3-ldas-tools-diskcacheAPI
  - python3-ldas-tools-frameAPI
  - ldas-tools-diskcacheAPI-swig
  - ldas-tools-frameAPI-swig
  - ldas-tools-framecpp-c-devel
  - ldas-tools-cmake

@subsection RPMInstalledPackages RPM Installed Packages

Below is a complete list of packages that comprise
the LDAS Tools Suite.

  - ldas-tools-al
  - ldas-tools-al-debugsource
  - ldas-tools-al-devel
  - ldas-tools-al-swig
  - ldas-tools-cmake
  - ldas-tools-diskcacheAPI
  - ldas-tools-diskcacheAPI-debugsource
  - ldas-tools-diskcacheAPI-devel
  - ldas-tools-diskcacheAPI-swig
  - ldas-tools-filters
  - ldas-tools-filters-debugsource
  - ldas-tools-filters-devel
  - ldas-tools-frameAPI
  - ldas-tools-frameAPI-devel
  - ldas-tools-frameAPI-debugsource
  - ldas-tools-frameAPI-swig
  - ldas-tools-framecpp
  - ldas-tools-framecpp-c
  - ldas-tools-framecpp-c-devel
  - ldas-tools-framecpp-debugsource
  - ldas-tools-framecpp-devel
  - ldas-tools-framecpp-doc
  - ldas-tools-framecpp-swig
  - ldas-tools-ldasgen
  - ldas-tools-ldasgen-debugsource
  - ldas-tools-ldasgen-devel
  - ldas-tools-ldasgen-swig
  - ldas-tools-utilities
  - ldas-tools-utilities-debugsource
  - ldas-tools-utilities-doc
  - python3-ldas-tools-al
  - python3-ldas-tools-diskcacheAPI
  - python3-ldas-tools-frameAPI
  - python3-ldas-tools-framecpp
  - python3-ldas-tools-ldasgen

@section ProcedureBuilding Building Procedure

<DL>
  <DT>CM_MSG_DEBUG_VERBOSE</DT>
  <DD>Enable verbose debugging TRUE/FALSE</DD>
  <DT>ABI_DIR</DT>
  <DD>Directory used for source of ABI comparison (ex: /usr)</DD>
  <DT>SANITIZER</DT>
  <DD>Value for Google sanitizer (ex: address)</DD>
  <DT>MAKE_BUILD_TYPE</DT>
  <DD>Type of build (ex: Debug)</DD>
  <DT>PYTHON2_VERSION</DT>
  <DD>Python 2 version (ex: 2.7)</DD>
  <DT>PYTHON3_VERSION</DT>
  <DD>Python 3 version (ex: 3.4)</DD>
</DL>

@subsection ProcedureBuildingAll Building All of Packages
@section ReleaseProcedure Release Procedure

@subsection ReleaseProcedureSource Update source cdoe information
Update the following source information
<UL>
  <LI>ABI/API compatability check</LI>
    <LI>Run \code{.sh}make -k abi-check\endcode</LI>
    <LI>Update library version information</LI>
    <LI>CMakeLists.txt</LI>
    <UL>
      <LI>Update package version number</LI>
      <LI>Update package version number of referenced LDAS Tools packages</LI>
    </UL>
  <LI>ChangeLog</LI>
  <UL>
    <LI>Update package version number</LI>
    <LI>Update date \code{.sh}date '+* %a %b %d %G Edward Maros <ed.maros@ligo.org> - <pkg-version>-1'\endcode</LI>
    <LI>NEWS copy entry from ChangeLog </LI>
  </UL>
</UL>

@subsection ReleaseProcedureRedHat Update RedHat information
Update config/<pkg>.pc.in

@code{.sh}
date '+* %a %b %d %G Edward Maros <ed.maros@ligo.org> - <pkg-version>-1'
@endcode

@subsection ReleaseProcedureDebian Debian information
Update debian/changelog with a new entry.
The following command can be used to format the date string which appears at the
end of the new entry

@code{.sh}
date -R
@endcode

@section ChangeLogs Change Logs

\ref ChangeLog20191023 "Change Log 2019.10.23"
\ref ChangeLog20190926 "Change Log 2019.09.26"
\ref ChangeLog20190814 "Change Log 2019.08.14"

<!-- tag ldas-tools-20190814115453 -->

<!-- tag ldas-tools-20180622215958 -->

<!--
 date '+%B %e, %G' ; \
date '+* %a %b %d %G Edward Maros <ed.maros@ligo.org> - <pkg-version>-1'

#========================================================================
# Verify the following files
#========================================================================

CMakeLists.txt
ChangeLog
NEWS
config/ldas-tools-*.spec
debian/control
debian/changelog

August 14, 2019
Wed, 14 Aug 2019 11:54:53 -0700
* Wed Aug 14 2019 Edward Maros <ed.maros@ligo.org> - -1

#========================================================================

*  ./cmake_helper/NEWS:Release - 1.0.8

*  ./ldastoolsal/ChangeLog:Release - 2.6.3
  ./ldastoolsal-swig/ChangeLog:Release - 2.6.6

*  ./framecpp/ChangeLog:Release - 2.6.6
  ./framecpp-swig/ChangeLog:Release - 2.6.8

*  ./filters/ChangeLog:Release - 2.6.4

  ./ldasgen/ChangeLog:Release - 2.6.3
  ./ldasgen-swig/ChangeLog:Release - 2.6.6

  ./diskcache/ChangeLog:Release - 2.6.3
  ./diskcache-swig/ChangeLog:Release - 2.6.7

  ./frameutils/ChangeLog:Release - 2.6.3
  ./frameutils-swig/ChangeLog:Release - 2.6.7

  ./utilities/ChangeLog:Release - 2.6.4

#========================================================================

  * Corrections for Conda build on OSX for framecpp-swig
 */

/*! @page ChangeHistory Change History
 */
-->
*/
