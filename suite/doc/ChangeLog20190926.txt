/** @page ChangeLog20190926 Change Log 2019.09.26
@verbatim
ldas-tools-cmake
Release 1.1.0 - Septempber 26, 2019
  * Moved the installation of .pc file from /usr/lib to /usr/share

@endverbatim
*/
