//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>
#include <memory>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/make_unique.hpp>
#include <boost/smart_ptr.hpp>

#include "framecpp/OFrameStream.hh"
#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrDetector.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrVect.hh"
#if FRAME_SPEC_CURRENT <= 8
#include "framecpp/Detectors.hh"
#endif /* FRAME_SPEC_CURRENT <= 8 */

#include "frame_generator.hh"

namespace testing
{
    using FrameCPP::Common::FrameBuffer;

    typedef FrameBuffer< std::filebuf > frame_buffer_type;

    template < typename Type >
    void
    data_fill( Type* Start, Type* End, const Type Value )
    {
        std::fill( Start, End, Value );
    }

    template < typename Type >
    void
    data_fill( FrameCPP::FrVect::data_type& Data,
               INT_4U                       Size,
               const Type                   Value )
    {
        Data.reset(
            new FrameCPP::FrVect::data_type::element_type[ Size *
                                                           sizeof( Type ) ] );
        data_fill(
            reinterpret_cast< Type* >( &( Data[ 0 ] ) ),
            reinterpret_cast< Type* >( &( Data[ Size * sizeof( Type ) ] ) ),
            Value );
    }

    struct r8_line_filler : public data_filler
    {
        REAL_8 fill_value = 3.1415;

        virtual FrameCPP::FrVect::type_type
        operator( )( FrameCPP::FrVect::data_type& Data, INT_4U Samples )
        {
            data_fill< REAL_8 >( Data, Samples, fill_value );
            return ( FrameCPP::FrVect::FR_VECT_8R );
        }
    };

    FrameCPP::FrVect::type_type
    analysis_ready_filler::operator( )( FrameCPP::FrVect::data_type& Data,
                                        INT_4U                       Samples )
    {
        const CHAR_U NOT_READY{ 0x00 };

        switch ( *current )
        {
        case AR_READY_ALL:
            // All data is ready for analysis
            data_fill< CHAR_U >( Data, Samples, fill_value );
            break;
        case AR_READY_NONE:
            // None of the data is ready for analysis
            data_fill< CHAR_U >( Data, Samples, NOT_READY );
            break;
        case AR_READY_START:
        {
            // Only the data at the start of the set is reay
            INT_4U midpoint = Samples / 2;

            data_fill< CHAR_U >( Data, Samples, fill_value );
            auto mid_point_ptr =
                reinterpret_cast< CHAR_U* >( &Data[ midpoint ] );
            auto end_point_ptr =
                reinterpret_cast< CHAR_U* >( &Data[ Samples ] );

            data_fill( mid_point_ptr, end_point_ptr, NOT_READY );
            break;
        }
        case AR_READY_END:
        {
            // Only the data at the start of the set is ready
            INT_4U midpoint = Samples / 2;

            data_fill< CHAR_U >( Data, Samples, NOT_READY );

            auto mid_point_ptr =
                reinterpret_cast< CHAR_U* >( &Data[ midpoint ] );
            auto end_point_ptr =
                reinterpret_cast< CHAR_U* >( &Data[ Samples ] );

            data_fill( mid_point_ptr, end_point_ptr, fill_value );

            break;
        }
        case AR_READY_MIDDLE:
        {
            // Only the data in the middle of the set is ready
            INT_4U edge = Samples / 4;

            data_fill< CHAR_U >( Data, Samples, NOT_READY );

            auto leading_edge = reinterpret_cast< CHAR_U* >( &Data[ edge ] );
            auto trailing_edge =
                reinterpret_cast< CHAR_U* >( &Data[ edge * 3 ] );

            data_fill( leading_edge, trailing_edge, fill_value );
            break;
        }
        case AR_READY_ENDS:
        {
            // Only the data on the ends of the set is ready
            INT_4U edge = Samples / 4;

            data_fill< CHAR_U >( Data, Samples, fill_value );

            auto leading_edge = reinterpret_cast< CHAR_U* >( &Data[ edge ] );
            auto trailing_edge =
                reinterpret_cast< CHAR_U* >( &Data[ edge * 3 ] );

            data_fill( leading_edge, trailing_edge, NOT_READY );
            break;
        }
        }
        current++;
        if ( current == analysis_ready_filler_scheme.end( ) )
        {
            --current;
        }
        return ( FrameCPP::FrVect::FR_VECT_1U );
    }

    void
    file_cache_type::cleanup( )
    {
        boost::filesystem::path source_path( source_directory );

        if ( boost::filesystem::exists( source_path ) )
        {
            // boost::filesystem::remove_all( source_path );
        }
    }

    RDSFrame::frame_file_container_type
    file_cache_type::setup( )
    {
        RDSFrame::frame_file_container_type retval;
        auto source_path = boost::filesystem::current_path( );
        boost::filesystem::path relative_path( source_directory );
        source_path /= ( relative_path );

        if ( boost::filesystem::exists( source_path ) )
        {
            boost::system::error_code system_error;
            boost::filesystem::remove_all( source_path, system_error );
        }
        boost::filesystem::create_directories( source_path );
        //---------------------------------------------------------------
        // Channels to populate input frames with
        //---------------------------------------------------------------
        auto data_channels =
            std::vector< boost::shared_ptr< data_generator > >( 3 );
        //---------------------------------------------------------------
        // channel to be copied to RDS frame
        //---------------------------------------------------------------
        r8_line_filler interesting_channel_data_filler;

        data_channels[ 0 ] = boost::make_shared< data_generator >(
            interesting_channel_data_filler );
        data_channels[ 0 ]->name = "H1:DataVector";
        //---------------------------------------------------------------
        // channel which will NOT be part of generated RDS frame
        //---------------------------------------------------------------
        r8_line_filler mundane_channel_data_filler;

        mundane_channel_data_filler.fill_value = 0.0;
        data_channels[ 1 ] =
            boost::make_shared< data_generator >( mundane_channel_data_filler );
        data_channels[ 1 ]->name = "H1:mundane_channel";
        data_channels[ 1 ]->samples_per_second = 1.0;
        //---------------------------------------------------------------
        // analysis ready channel
        //---------------------------------------------------------------
        data_channels[ 2 ] =
            boost::make_shared< data_generator >( analysis_ready_data_filler );
        data_channels[ 2 ]->name = "H1:analysis_ready";
        data_channels[ 2 ]->samples_per_second = 1.0;

        {
            auto start = start_time;
            auto dt = duration;
            while ( dt > 0 )
            {
                retval.push_back( create_frame( source_path,
                                                ifo,
                                                description,
                                                start,
                                                seconds_per_file,
                                                data_channels ) );
                start += seconds_per_file;
                if ( seconds_per_file <= dt )
                {
                    dt -= seconds_per_file;
                }
                else
                {
                    dt = 0;
                }
            }
        }
        return ( retval );
    }

    std::string
    file_cache_type::create_frame(
        boost::filesystem::path const&                      SourcePath,
        std::string const&                                  IFO,
        std::string const&                                  Description,
        FrameCPP::Time const&                               StartTime,
        FrameCPP::FrameH::dt_type                           Duration,
        std::vector< boost::shared_ptr< data_generator > >& DataChannels )
    {
        std::ostringstream file_name;
        file_name << IFO // IFO
                  << "-" << Description // Description
                  << "-" << StartTime.GetSeconds( ) // Start time
                  << "-" << Duration // Length in seconds
                  << ".gwf" // Extension
            ;
        boost::filesystem::path file_path( SourcePath );
        file_path /= file_name.str( );

        //---------------------------------------------------------------
        // Create FrameH
        //---------------------------------------------------------------
        FrameCPP::FrameH::run_type const run{ 2023 };
        FrameCPP::FrameH::frame_type     frame_count{ 0 };
        auto frame = boost::make_shared< FrameCPP::FrameH >( //
            "LIGO",
            run,
            frame_count++,
            StartTime,
#if FRAME_SPEC_CURRENT <= 8
            0, // ULeapS
#endif /* 0 */
            Duration );
        if ( IFO.find( "H" ) != std::string::npos )
        {
            auto detector =
#if FRAME_SPEC_CURRENT >= 9
                FrameCPP::FrDetector::GetDetector(
                    FrameCPP::FrDetector::DQO_LHO_4K );
#else /* FRAME_SPEC_CURRENT <= 8 */
                FrameCPP::GetDetector( FrameCPP::DETECTOR_LOCATION_H1,
                                       FrameCPP::Time( ) );
#endif
            frame->RefDetectProc( ).append( detector );
        }
        frame->SetRawData(
            boost::make_shared< FrameCPP::FrRawData >( "Frame raw data" ) );
        //---------------------------------------------------------------
        // Generate the data
        //---------------------------------------------------------------
        for ( auto generator : DataChannels )
        {
            frame->GetRawData( )->RefFirstAdc( ).append(
                ( *generator )( StartTime, Duration ) );
        }
        //---------------------------------------------------------------
        // Write Frame to stream
        //---------------------------------------------------------------
        FrameCPP::OFrameFStream frame_stream( file_path.c_str( ) );
        frame_stream.WriteFrame( frame );
        return ( file_path.string( ) );
    }

} // namespace testing
