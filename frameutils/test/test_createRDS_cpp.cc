///
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sys/stat.h>
#include <algorithm>
#include <unistd.h>
#include <type_traits>

#include "ldastoolsal/DeadLockDetector.hh"

#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "frameAPI/Catalog.hh"
#include "frameAPI/Channel.hh"
#include "frameAPI/createRDS.hh"
#include "frameAPI/Frame.hh"

//  LDAS_TEST_FRAMES_DIR -

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>

typedef std::list< std::string > base_filename_container_type;
typedef std::list< std::string > filename_container_type;

template < typename Container,
           typename = typename std::enable_if<
               std::is_same< typename Container::value_type,
                             std::string >::value >::type >
auto
concatenate( Container const& words ) -> std::string
{
    std::string result;
    for ( const auto& word : words )
    {
        result += word + " ";
    }
    result.pop_back( ); // remove the last space character
    return result;
}

class Search
{
public:
    Search( );

    void AddPath( const std::string& Path );
    void AddPathVar( const std::string& Var, const std::string& SubDir );

    void Find( const base_filename_container_type& BaseNames,
               filename_container_type&            Files ) const;

    typedef std::list< std::string > path_container_type;

    path_container_type paths;
} SEARCH_DIRS;

class TestSet
{
public:
    typedef INT_4U start_time_type;
    typedef INT_4U dt_type;

    static const INT_4U SET_O3_START = 1268056320;
    static const INT_4U SET_O3_DT = 64;
    static const INT_4U SET_O3_FRAMES = 10;

    static const INT_4U SET_O1_LLO_START = 1133126912;
    static const INT_4U SET_O1_LLO_DT = 64;
    static const INT_4U SET_O1_LLO_FRAMES = 10;

    typedef enum
    {
        SET_O3_LHO,
        SET_O3_LLO,
        SET_O1_LHO,
        SET_O1_LLO,
        SET_S6_LHO,
        SET_S6_LLO,
        SET_S5,
        SET_SIZE
    } set_type;

    static const char* DQ[ SET_SIZE ];

    TestSet( );

    void AddSet( set_type Set );

    void AddSet( const base_filename_container_type& Filename,
                 start_time_type                     Start,
                 dt_type                             Dt );

    void Find( const Search&                          SearchDirs,
               ::RDSFrame::frame_file_container_type& FrameFiles );

    void Find( const Search&                          SearchDirs,
               FrameAPI::Catalog::stream_source_type& FrameFiles );

    void operator( )( const std::string& Filename,
                      start_time_type    Start,
                      dt_type            Dt );

private:
    struct file_set_type
    {
        base_filename_container_type files;
        start_time_type              start;
        dt_type                      dt;
    };
    typedef std::list< file_set_type > file_set_container_type;

    file_set_container_type file_set;

    void mkset( start_time_type    Start,
                dt_type            Dt,
                int                Count,
                const std::string& Path,
                const std::string& SiteType );
};

struct rds_global_fixture
{
    rds_global_fixture( )
    {
        //-------------------------------------------------------------------
        // Setup logging as is needed by query class
        //-------------------------------------------------------------------
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        static char cwd_buffer[ 2048 ];
        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (const char*)NULL )
        {
            exit( 1 );
        }
        std::string                       cwd( cwd_buffer );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "test_createRDS_cpp" );
        GenericAPI::LDASplatform::AppName( "test_createRDS_cpp" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );

#if WORKING
        if ( Test.Verbosity( ) > 0 )
        {
            GenericAPI::setLogDebugLevel( Test.Verbosity( ) );
        }
#endif /* WORKING */
        FrameCPP::Initialize( );

        SEARCH_DIRS.AddPathVar( "LDAS_TEST_FRAMES_DIR", "" );
        SEARCH_DIRS.AddPathVar( "HOME", "tmp" );
        SEARCH_DIRS.AddPathVar( "HOME", "mnt/hdfs" );
        SEARCH_DIRS.AddPath( "/hdfs" );
        SEARCH_DIRS.AddPath( "/archive" );

        BOOST_TEST_MESSAGE(
            "SETUP: Directories that will be searched for frame files: "
            << concatenate( SEARCH_DIRS.paths ) );

        std::cout << "global setup" << std::endl;
    }

    ~rds_global_fixture( )
    {
        std::cout << "global teardown" << std::endl;
    }
};

BOOST_GLOBAL_FIXTURE( rds_global_fixture );

BOOST_AUTO_TEST_CASE( testCreateRDSReduce )
{
    ::RDSFrame::frame_file_container_type frame_files;

    TestSet file_set;

    file_set.AddSet( TestSet::SET_O3_LLO );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        BOOST_WARN_MESSAGE(
            false, "Test skipped because there are no files to process" );
        return;
    }
#if 0
    try
    {
        static char cwd[ 1024 ];

        FrameAPI::Catalog::gps_seconds_type start( TestSet::SET_O3_START +
                                                   2 * TestSet::SET_O3_DT );
        FrameAPI::Catalog::gps_seconds_type stop( start +
                                                  ( TestSet::SET_O3_DT ) );
        const char* dq_channel( TestSet::DQ[ TestSet::SET_O3_LLO ] );
        FrameAPI::Catalog::channel_container_type channels;
        FrameAPI::RDS::FileOptions                options;

        //-------------------------------------------------------------------
        // Setup options
        //-------------------------------------------------------------------
        if ( getcwd( cwd, sizeof( cwd ) ) == NULL )
        {
            throw std::runtime_error(
                "Unable to get current working directory" );
        }

        options.OutputTimeStart( start );
        options.OutputTimeEnd( stop );
        options.DirectoryOutputFrames( cwd );
        options.DirectoryOutputMD5Sum( cwd );
        options.OutputType( "testCreateRDS" );

        //-------------------------------------------------------------------
        // Attempt to read the file
        //-------------------------------------------------------------------
        FrameAPI::Frame               frame;
        FrameAPI::Frame::channel_type as_q;

        channels.names.emplace_back( dq_channel, dq_channel );
        frame = FrameAPI::createRDSFrame( frame_files, start, stop, channels );
        BOOST_TEST_MESSAGE(
            "Read frame files: " << concatenate( frame_files ) );
        as_q = frame.GetChannel( dq_channel );
        BOOST_CHECK( as_q->GetName( ) == dq_channel );
    }
    catch ( const std::exception& E )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an exception: " << E.what( ) );
    }
    catch ( ... )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an unhandled exception" );
    }
#endif /* 0 */
}

BOOST_AUTO_TEST_CASE( testCreateRDSReduceRename )
{
    ::RDSFrame::frame_file_container_type frame_files;

    TestSet file_set;

    file_set.AddSet( TestSet::SET_O3_LLO );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        BOOST_WARN_MESSAGE(
            false, "Test skipped because there are no files to process" );
        return;
    }
#if 0
    try
    {
        static char cwd[ 1024 ];

        FrameAPI::Catalog::gps_seconds_type start( TestSet::SET_O3_START +
                                                   2 * TestSet::SET_O3_DT );
        FrameAPI::Catalog::gps_seconds_type stop( start +
                                                  ( TestSet::SET_O3_DT ) );
        const std::string dq_channel( TestSet::DQ[ TestSet::SET_O3_LLO ] );
        const std::string dq_channel_new( dq_channel + "_AR" );
        FrameAPI::Catalog::channel_container_type channels;
        FrameAPI::RDS::FileOptions                options;

        //-------------------------------------------------------------------
        // Setup options
        //-------------------------------------------------------------------
        if ( getcwd( cwd, sizeof( cwd ) ) == NULL )
        {
            throw std::runtime_error(
                "Unable to get current working directory" );
        }

        options.OutputTimeStart( start );
        options.OutputTimeEnd( stop );
        options.DirectoryOutputFrames( cwd );
        options.DirectoryOutputMD5Sum( cwd );
        options.OutputType( "testCreateRDSReduceRename" );

        //-------------------------------------------------------------------
        // Attempt to read the file
        //-------------------------------------------------------------------
        FrameAPI::Frame frame;

        channels.names.emplace_back( dq_channel, dq_channel_new );
        frame = FrameAPI::createRDSFrame( frame_files, start, stop, channels );
        BOOST_TEST_MESSAGE(
            "Read frame files: " << concatenate( frame_files ) );
        try
        {
            auto as_q = frame.GetChannel( dq_channel_new );
            BOOST_CHECK_MESSAGE( as_q->GetName( ) == dq_channel_new,
                                 as_q->GetName( )
                                     << " =?= " << dq_channel_new );
        }
        catch ( const std::exception& Error )
        {
            BOOST_CHECK_MESSAGE( false, "Exception: " << Error.what( ) );
        }
    }
    catch ( const std::exception& E )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an exception: " << E.what( ) );
    }
    catch ( ... )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an unhandled exception" );
    }
#endif /* 0 */
}

BOOST_AUTO_TEST_CASE( testCreateRDSResample )
{
    ::RDSFrame::frame_file_container_type frame_files;

    TestSet file_set;

    file_set.AddSet( TestSet::SET_O3_LLO );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        BOOST_WARN_MESSAGE( false, "Test skipped because no files to process" );
        return;
    }
#if 0
    try
    {
        static char cwd[ 1024 ];

        FrameAPI::RDS::Options::start_type start( TestSet::SET_O3_START +
                                                  2 * TestSet::SET_O3_DT );
        FrameAPI::RDS::Options::end_type   stop( start +
                                               ( 3 * TestSet::SET_O3_DT ) );
        const char* dq_channel( TestSet::DQ[ TestSet::SET_O3_LLO ] );
        FrameAPI::Catalog::channel_container_type channels;
        FrameAPI::RDS::FileOptions                options;

        //-------------------------------------------------------------------
        // Setup options
        //-------------------------------------------------------------------

        if ( getcwd( cwd, sizeof( cwd ) ) == NULL )
        {
            throw std::runtime_error(
                "Unable to get current working directory" );
        }

        options.OutputTimeStart( start );
        options.OutputTimeEnd( stop );
        options.DirectoryOutputFrames( cwd );
        options.DirectoryOutputMD5Sum( cwd );
        options.OutputType( "testCreateRDSResample" );

        //-------------------------------------------------------------------
        // Attempt to create a set of RDS frames from the input set
        //-------------------------------------------------------------------
        channels.names.emplace_back( dq_channel, dq_channel );
        FrameAPI::createRDSSet( frame_files, channels, options );
    }
    catch ( const SwigException& E )
    {
        BOOST_CHECK_MESSAGE( false,
                             "Caught a SWIG exception: results: "
                                 << E.getResult( )
                                 << " info: " << E.getInfo( ) );
    }
    catch ( const std::exception& E )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an exception: " << E.what( ) );
    }
    catch ( ... )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an unhandled exception" );
    }
#endif /* 0 */
}

BOOST_AUTO_TEST_CASE( testCreateRDSResampleRename )
{
    ::RDSFrame::frame_file_container_type frame_files;

    TestSet file_set;

    file_set.AddSet( TestSet::SET_O3_LLO );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        BOOST_WARN_MESSAGE( false, "Test skipped because no files to process" );
        return;
    }
#if 0
    try
    {
        static char cwd[ 1024 ];

        FrameAPI::RDS::Options::start_type start( TestSet::SET_O3_START +
                                                  2 * TestSet::SET_O3_DT );
        FrameAPI::RDS::Options::end_type   stop( start +
                                               ( 3 * TestSet::SET_O3_DT ) );
        const std::string dq_channel( TestSet::DQ[ TestSet::SET_O3_LLO ] );
        const std::string dq_channel_new( dq_channel + "_AR" );
        FrameAPI::Catalog::channel_container_type channels;
        FrameAPI::RDS::FileOptions                options;

        //-------------------------------------------------------------------
        // Setup options
        //-------------------------------------------------------------------

        if ( getcwd( cwd, sizeof( cwd ) ) == NULL )
        {
            throw std::runtime_error(
                "Unable to get current working directory" );
        }

        options.OutputTimeStart( start );
        options.OutputTimeEnd( stop );
        options.DirectoryOutputFrames( cwd );
        options.DirectoryOutputMD5Sum( cwd );
        options.OutputType( "testCreateRDSResampleRename" );

        //-------------------------------------------------------------------
        // Attempt to create a set of RDS frames from the input set
        //-------------------------------------------------------------------
        channels.names.emplace_back( dq_channel, dq_channel_new );
        FrameAPI::createRDSSet( frame_files, channels, options );
    }
    catch ( const SwigException& E )
    {
        BOOST_CHECK_MESSAGE( false,
                             "Caught a SWIG exception: results: "
                                 << E.getResult( )
                                 << " info: " << E.getInfo( ) );
    }
    catch ( const std::exception& E )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an exception: " << E.what( ) );
    }
    catch ( ... )
    {
        BOOST_CHECK_MESSAGE( false, "Caught an unhandled exception" );
    }
#endif /* 0 */
}

#if 0
void TestCreateRDSChannelRenaming( );
void TestCreateRDSResample( );
void TestExceptionMissingChannel( );
void TestOpenData( );

int
main( int ArgC, char** ArgV )
{
#if DEAD_LOCK_DETECTOR_ENABLED
    {
        LDASTools::AL::DeadLockDetector::SetDebugging( 2003 );
    }
#endif /* DEAD_LOCK_DETECTOR_ENABLED */

    //---------------------------------------------------------------------
    // Initialize the testing framework
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );

    try
    {
        TestCreateRDS( );
        TestCreateRDSChannelRenaming( );
        TestCreateRDSResample( );
        TestOpenData( );

#if 0
        TestExceptionMissingChannel( );
#endif /* 0 */
    }
    catch ( const std::exception& E )
    {
        Test.Check( false )
            << "Caught an exception: " << E.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Check( false ) << "Caught an unknown exception" << std::endl;
    }

    //---------------------------------------------------------------------
    // Return the state of testing
    //---------------------------------------------------------------------
    Test.Exit( );
}

void
TestCreateRDSChannelRenaming( )
{
    static const char* func_name = "TestCreateRDSChannelRenaming";

    ::RDSFrame::frame_file_container_type frame_files;

    TestSet file_set;

    file_set.AddSet( TestSet::SET_O1_LLO );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        return;
    }
    Test.Message( 10 ) << func_name << ": Start" << std::endl;
    Test.Message( 20 ) << func_name << ": files[0]: " << frame_files.front( )
                       << std::endl;
    try
    {
        static char cwd[ 1024 ];

        FrameAPI::Catalog::gps_seconds_type start( TestSet::SET_O1_LLO_START +
                                                   2 * TestSet::SET_O1_LLO_DT );
        FrameAPI::Catalog::gps_seconds_type stop( start +
                                                  ( TestSet::SET_O1_LLO_DT ) );
        const char* dq_channel( TestSet::DQ[ TestSet::SET_O1_LLO ] );
        std::string dq_channel_ar( dq_channel );
        dq_channel_ar += "_AR";
        FrameAPI::Catalog::channel_container_type channels;
        FrameAPI::RDS::FileOptions                options;

        //-------------------------------------------------------------------
        // Setup options
        //-------------------------------------------------------------------
        if ( getcwd( cwd, sizeof( cwd ) ) == NULL )
        {
            throw std::runtime_error(
                "Unable to get current working directory" );
        }

        options.OutputTimeStart( start );
        options.OutputTimeEnd( stop );
        options.DirectoryOutputFrames( cwd );
        options.DirectoryOutputMD5Sum( cwd );
        options.OutputType( func_name );

        //-------------------------------------------------------------------
        // Attempt to read the file
        //-------------------------------------------------------------------
        FrameAPI::Frame               frame;
        FrameAPI::Frame::channel_type as_q;

        channels.names.emplace_back( dq_channel, dq_channel_ar );
        Test.Message( 10 ) << func_name << ": About to create frame"
                           << std::endl;
        frame = FrameAPI::createRDSFrame( frame_files, start, stop, channels );
        Test.Message( 10 ) << func_name << ": Created frame" << std::endl;
        as_q = frame.GetChannel( dq_channel_ar );
        Test.Message( 10 ) << func_name << ": as_q.name: " << as_q->GetName( )
                           << std::endl;
        Test.Message( 10 ) << func_name << ": as_q.unitY: " << as_q->GetUnitY( )
                           << std::endl;
        Test.Message( 10 ) << func_name << ": as_q.data[0].name: "
                           << as_q->RefData( )[ 0 ]->GetName( ) << std::endl;
    }
    catch ( const std::exception& E )
    {
        Test.Message( 10 ) << func_name
                           << ": Caught an exception: " << E.what( )
                           << std::endl;
        Test.Check( false )
            << func_name << ": Caught an exception: " << E.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Message( 10 ) << func_name << ": Caught an unhandled exception"
                           << std::endl;
        Test.Check( false )
            << func_name << ": Caught an unhandled exception" << std::endl;
    }
    Test.Message( 10 ) << func_name << ": Stop" << std::endl;
}

void
TestExceptionMissingChannel( )
{
    static const char* func_name = "TestCreateRDSExceptionMissingChannel";

    ::RDSFrame::frame_file_container_type frame_files;

    TestSet file_set;

    file_set( "H-SIDv1_H1H2_250mHz-821935753-26.gwf", 821935753, 26 );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        return;
    }
    Test.Message( 10 ) << func_name << ": Start" << std::endl;
    try
    {
        FrameAPI::Catalog::gps_seconds_type       start( 821935753 );
        FrameAPI::Catalog::gps_seconds_type       stop( start + 16 );
        FrameAPI::Catalog::channel_container_type channels;

        //-------------------------------------------------------------------
        // Attempt to read the file
        //-------------------------------------------------------------------
        FrameAPI::Frame               frame;
        FrameAPI::Frame::channel_type as_q;

        channels.names.emplace_back( "H1:AdjacentPSD", "H1:AdjacentPSD" );
        channels.names.emplace_back( "Missing Channel", "Missing Channel" );
        Test.Message( 10 ) << func_name << ": About to create frame"
                           << std::endl;
        frame = FrameAPI::createRDSFrame( frame_files, start, stop, channels );
        Test.Message( 10 ) << func_name << ": Created frame" << std::endl;
        as_q = frame.GetChannel( "H1:AdjacentPSD" );
        Test.Message( 10 ) << func_name << ": as_q.name: " << as_q->GetName( )
                           << std::endl;
        Test.Message( 10 ) << func_name << ": as_q.unitY: " << as_q->GetUnitY( )
                           << std::endl;
        Test.Message( 10 ) << func_name << ": as_q.data[0].name: "
                           << as_q->RefData( )[ 0 ]->GetName( ) << std::endl;
    }
    catch ( const FrameAPI::Frame::NoChannelFound& E )
    {
        Test.Check( true ) << func_name
                           << ": Caught expected exception: " << E.what( )
                           << std::endl;
    }
    catch ( const FrameAPI::RDS::MissingChannel& E )
    {
        Test.Check( true ) << func_name
                           << ": Caught expected exception: " << E.what( )
                           << std::endl;
    }
    catch ( const std::exception& E )
    {
        Test.Message( 10 ) << func_name
                           << ": Caught an exception: " << E.what( )
                           << std::endl;
        Test.Check( false )
            << func_name << ": Caught an exception: " << E.what( ) << std::endl;
    }
    catch ( ... )
    {
        Test.Message( 10 ) << func_name << ": Caught an unhandled exception"
                           << std::endl;
        Test.Check( false )
            << func_name << ": Caught an unhandled exception" << std::endl;
    }
    Test.Message( 10 ) << func_name << ": Stop" << std::endl;
}

void
TestOpenData( )
{
    static const char*                    func_name = "TestOpenData";
    FrameAPI::Catalog::stream_source_type frame_files;

    TestSet file_set;

    file_set( "H-SIDv1_H1H2_250mHz-821935753-26.gwf", 821935753, 26 );
    file_set.Find( SEARCH_DIRS, frame_files );

    if ( frame_files.size( ) <= 0 )
    {
        //-------------------------------------------------------------------
        // No files to process
        //-------------------------------------------------------------------
        return;
    }
    try
    {
        FrameAPI::Catalog::gps_seconds_type       start( 821935753 );
        FrameAPI::Catalog::gps_seconds_type       stop( start + 16 );
        FrameAPI::Catalog::channel_container_type channels;

        //-------------------------------------------------------------------
        // Open, scan the TOC, and close a set of files.
        // This gets the meta data located in the TOC and makes it available
        //   to the caller via the catalog interface
        //-------------------------------------------------------------------
        FrameAPI::Catalog catalog( frame_files );
        //-------------------------------------------------------------------
        // Display the data range
        //-------------------------------------------------------------------
        Test.Message( 10 ) << func_name
                           << ": start time: " << catalog.GPSStartTime( )
                           << std::endl;
        Test.Message( 10 ) << func_name
                           << ": end time: " << catalog.GPSEndTime( )
                           << std::endl;
        //-------------------------------------------------------------------
        // Obtain the catalog of channels
        //-------------------------------------------------------------------
        FrameAPI::Catalog::channel_dict_type channelDict = catalog.Channels( );
        for ( FrameAPI::Catalog::channel_dict_type::element_type::const_iterator
                  cur = channelDict->begin( ),
                  last = channelDict->end( );
              cur != last;
              ++cur )
        {
            Test.Message( 10 )
                << func_name << ": Channel: " << cur->first << std::endl;
        }
        //------------------------------------------------------------------
        // Read a collection of channels
        //------------------------------------------------------------------
        // channel_names.push_back( "H1H2:CSD" ); // not available
        channels.names.emplace_back( "H1:AdjacentPSD", "H1:AdjacentPSD" );
        channels.resampling.push_back( 1 );
        FrameAPI::Catalog::data_dict_type dataDict(
            catalog.Fetch( start, stop, channels ) );
        //------------------------------------------------------------------
        // Display some meta data about the channels
        //------------------------------------------------------------------
        FrameAPI::Catalog::DataDictionary::channel_type channel(
            ( *dataDict )[ channels.names[ 0 ].old_channel_name ] );
        Test.Message( 10 ) << func_name
                           << ": Channel: unitY: " << channel->GetUnitY( )
                           << std::endl;
    }
    catch ( const std::exception& E )
    {
        Test.Check( false )
            << func_name << ": Caught an exception: " << E.what( ) << std::endl;
    }
}
#endif /* 0 */

const char* TestSet::DQ[ SET_SIZE ] = {
    /* O3_LHO */ "H1:CAL-DELTAL_EXTERNAL_DQ",
    /* O3_LLO */ "L1:CAL-DELTAL_EXTERNAL_DQ",
    /* O1_LHO */ "H1:CAL-DELTAL_EXTERNAL_DQ",
    /* O1_LLO */ "L1:CAL-DELTAL_EXTERNAL_DQ",
    /* S6_LHO */ "H1::LSC-AS_Q",
    /* S6_LLO */ "L1:LSC-AS_Q",
    /* S5 */ "H1:LSC-AS_Q"
};

TestSet::TestSet( )
{
}

void
TestSet::AddSet( set_type Set )
{
    std::list< std::string > files;

    switch ( Set )
    {
    case SET_O3_LHO:
        mkset( TestSet::SET_O3_START, // Start Time
               TestSet::SET_O3_DT, // Delta T
               TestSet::SET_O3_FRAMES, // Number of Frames
               "frames/O3/raw/H1/H-H1_R-12680/", // Parent directory
               "H-H1_R-" // Prefix
        );
        break;
    case SET_O3_LLO:
        mkset( TestSet::SET_O3_START, // Start Time
               TestSet::SET_O3_DT, // Delta T
               TestSet::SET_O3_FRAMES, // Number of Frames
               "frames/O3/raw/L1/L-L1_R-12680/", // Parent directory
               "L-L1_R-" // Prefix
        );
        break;
    case SET_O1_LHO:
        mkset(
            1129247488, 64, 10, "frames/O1/raw/H1/H-H1_R-11292/", "H-H1_R-" );
        break;
    case SET_O1_LLO:
        mkset( TestSet::SET_O1_LLO_START,
               TestSet::SET_O1_LLO_DT,
               TestSet::SET_O1_LLO_FRAMES,
               "frames/O1/raw/L1/L-L1_R-11331/",
               "L-L1_R-" );
        break;
    case SET_S6_LHO:
        mkset( 971621600, 32, 10, "S6/L0/LHO/H-R-9716/", "H-R-" );
        break;
    case SET_S6_LLO:
        mkset( 971603008, 32, 10, "S6/L0/LLO/L-R-9716/", "L-R-" );
        break;
    case SET_S5:
        break;
    case SET_SIZE:
        // NOTE: no-op to prevent warning message of unhandled enumeration value
        break;
    }
}

void
TestSet::AddSet( const base_filename_container_type& Filenames,
                 start_time_type                     Start,
                 dt_type                             Dt )
{
    file_set_type f;

    f.files = Filenames;
    f.start = Start;
    f.dt = Dt;

    file_set.push_back( f );
}

void
TestSet::Find( const Search&                          SearchDirs,
               ::RDSFrame::frame_file_container_type& FrameFiles )
{
    FrameFiles.clear( );
    for ( auto cur : file_set )
    {
        std::list< std::string > files;

        SearchDirs.Find( cur.files, files );
        if ( files.size( ) > 0 )
        {
            FrameFiles.resize( files.size( ) );
            std::copy( files.begin( ), files.end( ), FrameFiles.begin( ) );
            break;
        }
    }
}

void
TestSet::Find( const Search&                          SearchDirs,
               FrameAPI::Catalog::stream_source_type& FrameFiles )
{
    FrameFiles.clear( );
    for ( auto cur : file_set )
    {
        std::list< std::string > files;

        SearchDirs.Find( cur.files, files );
        if ( files.size( ) > 0 )
        {
            FrameFiles.swap( files );
            break;
        }
    }
}

void
TestSet::
operator( )( const std::string& Filename, start_time_type Start, dt_type Dt )
{
    file_set_type f;

    f.files.push_back( Filename );
    f.start = Start;
    f.dt = Dt;

    file_set.push_back( f );
}

inline void
TestSet::mkset( start_time_type    Start,
                dt_type            Dt,
                int                Count,
                const std::string& Path,
                const std::string& SiteType )
{
    base_filename_container_type files;

    for ( int cur = 0, last = Count; cur != last; ++cur, Start += Dt )
    {
        std::ostringstream filename;
        filename << Path << SiteType << Start << "-" << Dt << ".gwf";
        files.push_back( filename.str( ) );
    }

    AddSet( files, Start, Dt );
}

Search::Search( )
{
}

void
Search::AddPath( const std::string& Path )
{
    struct stat st;

    if ( ( stat( Path.c_str( ), &st ) == 0 ) && ( S_ISDIR( st.st_mode ) ) )
    {
        paths.push_back( Path );
    }
}

void
Search::AddPathVar( const std::string& Var, const std::string& SubDir )
{
    const char* v = ::getenv( Var.c_str( ) );
    if ( v )
    {
        std::string path( v );

        if ( SubDir.size( ) > 0 )
        {
            path += "/";
            path += SubDir;
        }
        AddPath( path );
    }
}

void
Search::Find( const base_filename_container_type& BaseNames,
              std::list< std::string >&           Files ) const
{
    std::list< std::string > results;

    for ( auto cur_path : paths )
    {
        for ( auto cur : BaseNames )
        {
            std::string filename( cur_path );

            filename += "/";
            filename += cur;

            std::ifstream ifile( filename.c_str( ) );
            if ( ifile )
            {
                results.push_back( filename );
            }
        }
        if ( results.size( ) == BaseNames.size( ) )
        {
            Files.swap( results );
            return;
        }
        results.clear( );
    }
}
