//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <list>

#include "ConditionData.hh"

using namespace FrameAPI;

enum ifo_type
{
    IFO_LHO,
    IFO_LLO,
    IFO_MAX
};

enum run_type
{
    RUN_S3,
    RUN_S4
};

struct channel_info_type
{
    std::string s_name;
    int         s_q;

    inline channel_info_type( )
    {
    }

    inline channel_info_type( const std::string& Name, int Q )
        : s_name( Name ), s_q( Q )
    {
    }
};

struct test_data_type
{
    ConditionData::frame_files_type                s_files;
    std::list< ConditionData::channel_input_type > s_channels;
};

typedef std::list< channel_info_type > channel_list;

extern std::list< test_data_type > test_data;

extern void init_tests( );

extern INT_4U GetReduceStartTime( const ifo_type     IFO,
                                  const unsigned int Frame );
extern INT_4U GetReduceEndTime( const ifo_type IFO, const unsigned int Frame );
extern INT_4U GetResampleStartTime( const ifo_type     IFO,
                                    const unsigned int Frame );
extern INT_4U GetResampleEndTime( const ifo_type     IFO,
                                  const unsigned int Frame );
extern std::string GetFrameName( const ifo_type IFO, const unsigned int Frame );
extern void        GetFrameNames( bool                      Resample,
                                  INT_4U                    StartTime,
                                  INT_4U                    EndTime,
                                  const ifo_type            IFO,
                                  const run_type            Run,
                                  int                       Level,
                                  std::list< std::string >& Filenames );

extern void GetChannelList( const ifo_type IFO,
                            const run_type Run,
                            int            Level,
                            channel_list&  Channels );
