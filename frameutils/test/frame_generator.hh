//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_UTILS__TEST__FRAME_GENERATOR_HH
#define FRAME_UTILS__TEST__FRAME_GENERATOR_HH

#include <boost/filesystem.hpp>
#include <boost/serialization/strong_typedef.hpp>

#include "framecpp/Dimension.hh"
#include "framecpp/Time.hh"

#include "frameAPI/createRDS.hh"

namespace testing
{
    BOOST_STRONG_TYPEDEF( std::string, directory_path )

    enum ANALYSIS_READY_FILL_STYLE
    {
        AR_READY_ALL,
        AR_READY_NONE,
        AR_READY_START,
        AR_READY_END,
        AR_READY_MIDDLE,
        AR_READY_ENDS,
        AR_READY_00 = AR_READY_NONE,
        AR_READY_01 = AR_READY_ENDS,
        AR_READY_10 = AR_READY_START,
        AR_READY_11 = AR_READY_ALL,
    };

    typedef std::vector< ANALYSIS_READY_FILL_STYLE >
        analysis_ready_filler_scheme_type;

    struct data_filler
    {
        virtual FrameCPP::FrVect::type_type
        operator( )( FrameCPP::FrVect::data_type& Data, INT_4U Samples ) = 0;
    };

    struct analysis_ready_filler : public data_filler
    {
        testing::analysis_ready_filler_scheme_type analysis_ready_filler_scheme;
        CHAR_U                                     fill_value{ 0x03 };
        testing::analysis_ready_filler_scheme_type::const_iterator current;

        inline analysis_ready_filler( )
        {
            analysis_ready_filler_scheme.emplace_back( testing::AR_READY_ALL );
            current = analysis_ready_filler_scheme.begin( );
        }

        inline void
        resize_analysis_ready_filler_scheme( INT_4U NewSize )
        {
            analysis_ready_filler_scheme.resize( NewSize );
            current = analysis_ready_filler_scheme.begin( );
        }

        virtual FrameCPP::FrVect::type_type
        operator( )( FrameCPP::FrVect::data_type& Data, INT_4U Samples );
    };

    struct data_generator
    {

        FrameCPP::FrAdcData::name_type name = "data";
        FrameCPP::FrAdcData::name_type comment =
            "Data channel for sample frame";
        FrameCPP::FrAdcData::channelGroup_type  channelGroup = 1;
        FrameCPP::FrAdcData::channelNumber_type channelNumber = 1;
        FrameCPP::FrAdcData::nBits_type         nBits = 1;
        FrameCPP::FrAdcData::sampleRate_type    samples_per_second = 16.0;
        data_filler&                            filler;

        data_generator( data_filler& Filler ) : filler( Filler )
        {
        }

        virtual ~data_generator( )
        {
        }

        virtual boost::shared_ptr< FrameCPP::FrAdcData >
        operator( )( FrameCPP::Time const&     Start,
                     FrameCPP::FrameH::dt_type Duration )
        {
            using namespace FrameCPP;

            INT_4U samples = samples_per_second * Duration;

            dims.resize( 1 );
            dims[ 0 ] = Dimension( samples, Duration / samples );

            FrVect::data_type data;
            auto              type_for_data = filler( data, samples );

            auto frvect = boost::make_shared< FrVect >( //
                name,
                type_for_data,
                dims.size( ),
                &dims[ 0 ],
                BYTE_ORDER_HOST,
                data.get( ) );
            auto channel = boost::make_shared< FrAdcData >(
                name, channelGroup, channelNumber, nBits, samples_per_second );
            // frvect->AppendComment( comment );
            channel->RefData( ).append( frvect );
            return ( channel );
        }

    protected:
        std::vector< FrameCPP::Dimension > dims;
    };

    struct file_cache_type
    {
        directory_path        source_directory;
        std::string           description;
        std::string           ifo{ "H" };
        FrameCPP::Time        start_time{ 600000000, 0 };
        INT_4U                duration{ 128 };
        INT_4U                seconds_per_file{ 64 };
        analysis_ready_filler analysis_ready_data_filler;

        file_cache_type( const std::string& Description )
            : source_directory( Description + "_input" ),
              description( Description )
        {
        }

        void cleanup( );

        std::string create_frame(
            boost::filesystem::path const&                      SourcePath,
            std::string const&                                  IFO,
            std::string const&                                  Description,
            FrameCPP::Time const&                               StartTime,
            FrameCPP::FrameH::dt_type                           Duration,
            std::vector< boost::shared_ptr< data_generator > >& DataChannels );

        RDSFrame::frame_file_container_type setup( );
    };

} // namespace testing

#endif /* FRAME_UTILS__TEST__FRAME_GENERATOR_HH */
