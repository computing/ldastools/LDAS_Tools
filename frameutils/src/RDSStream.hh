//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RDS_STREAM_HH
#define RDS_STREAM_HH

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"

#include "framecpp/Common/FrameStream.hh"
#include "framecpp/FrameH.hh"

class RDSStream
{
public:
    typedef INT_2U compression_scheme_type;
    typedef INT_2U compression_level_type;

    typedef boost::shared_ptr< FrameCPP::FrameH >       frame_h_type;
    typedef FrameCPP::Common::OFrameStream::chkSum_type chkSum_type;

    virtual ~RDSStream( );

    virtual void Abandon( ) = 0;

    virtual void Close( bool Final ) = 0;

    void IFOList( const std::string& IFOList );

    const std::string& IFOList( ) const;

    virtual bool Next( const LDASTools::AL::GPSTime& StartTime,
                       REAL_8                        Dt,
                       INT_4U&                       FramesPerStream ) = 0;

    void UserRange( INT_4U Start, INT_4U End );

    virtual void Write( frame_h_type            Frame,
                        compression_scheme_type CompressionScheme,
                        compression_level_type  CompressionLevel,
                        chkSum_type             CheckSum ) = 0;

protected:
    LDASTools::AL::GPSTime m_user_start;
    LDASTools::AL::GPSTime m_user_stop;

    void ensureNoSuchFile( const std::string& Filename ) const;

private:
    std::string m_ifo_list;
};

inline RDSStream::~RDSStream( )
{
}

inline const std::string&
RDSStream::IFOList( ) const
{
    return m_ifo_list;
}

inline void
RDSStream::IFOList( const std::string& IFOList )
{
    m_ifo_list = IFOList;
}

inline void
RDSStream::UserRange( INT_4U Start, INT_4U End )
{
    m_user_start = LDASTools::AL::GPSTime( Start, 0 );
    m_user_stop = LDASTools::AL::GPSTime( End, 0 );
}

#endif /* RDS_STREAM_HH */
