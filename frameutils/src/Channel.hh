//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__CHANNEL_HH
#define FRAME_API__CHANNEL_HH

#if !SWIGIMPORTED
#include <boost/shared_array.hpp>

#include "framecpp/Common/Container.hh"

#include "framecpp/Dimension.hh"
#include "framecpp/FrVect.hh"
#endif /* ! SWIGIMPORTED */

#undef CHANNEL_NAME_TYPE
#define CHANNEL_NAME_TYPE std::string
#undef CHANNEL_UNIT_Y_TYPE
#define CHANNEL_UNIT_Y_TYPE std::string
#undef CHANNEL_DATA_TYPE
#define CHANNEL_DATA_TYPE FrameCPP::Common::Container< FrameCPP::FrVect >

#if !SWIGIMPORTED
namespace FrameAPI
{
    //-----------------------------------------------------------------------------
    /// \brief Generic wrapper for channel information
    //-----------------------------------------------------------------------------
    class Channel
    {
    public:
        typedef CHANNEL_NAME_TYPE                                     name_type;
        typedef CHANNEL_DATA_TYPE                                     data_type;
        typedef boost::shared_array< ::FrameCPP::Dimension::dx_type > dx_type;
        typedef boost::shared_array< ::FrameCPP::Dimension::nx_type > nx_type;
        typedef boost::shared_array< ::FrameCPP::Dimension::startX_type >
            startX_type;
        typedef boost::shared_array< ::FrameCPP::Dimension::unitX_type >
                                    unitX_type;
        typedef CHANNEL_UNIT_Y_TYPE unitY_type;

        Channel( );

        virtual ~Channel( );

        dx_type GetDx( ) const;

        name_type GetName( ) const;

        nx_type GetNx( ) const;

        startX_type GetStartX( ) const;

        unitX_type GetUnitX( ) const;

        unitY_type GetUnitY( ) const;

        virtual data_type& RefData( ) const = 0;

    private:
        mutable dx_type     m_dx;
        mutable name_type   name;
        mutable nx_type     m_nx;
        mutable startX_type m_startX;
        mutable unitX_type  m_unitX;

        data_type& seed( ) const;
    };

    inline Channel::dx_type
    Channel::GetDx( ) const
    {
        if ( !m_dx )
        {
            const data_type& frvect( seed( ) );

            m_dx.reset( new dx_type::element_type[ frvect[ 0 ]->GetNDim( ) ] );
            for ( FrameCPP::FrVect::nDim_type cur = 0,
                                              last = frvect[ 0 ]->GetNDim( );
                  cur != last;
                  ++cur )
            {
                m_dx[ cur ] = frvect[ 0 ]->GetDim( cur ).GetDx( );
            }
        }
        return m_dx;
    }

    inline Channel::name_type
    Channel::GetName( ) const
    {
        name_type retval;

        const data_type& frvect( seed( ) );

        retval = frvect[ 0 ]->GetName( );
        return retval;
    }

    inline Channel::nx_type
    Channel::GetNx( ) const
    {
        if ( !m_nx )
        {
            const data_type frvect( seed( ) );

            m_nx.reset( new nx_type::element_type[ frvect[ 0 ]->GetNDim( ) ] );
            for ( FrameCPP::FrVect::nDim_type cur = 0,
                                              last = frvect[ 0 ]->GetNDim( );
                  cur != last;
                  ++cur )
            {
                m_nx[ cur ] = frvect[ 0 ]->GetDim( cur ).GetNx( );
            }
        }
        return m_nx;
    }

    inline Channel::startX_type
    Channel::GetStartX( ) const
    {
        if ( !m_startX )
        {
            const data_type& frvect( seed( ) );

            m_startX.reset(
                new startX_type::element_type[ frvect[ 0 ]->GetNDim( ) ] );
            for ( FrameCPP::FrVect::nDim_type cur = 0,
                                              last = frvect[ 0 ]->GetNDim( );
                  cur != last;
                  ++cur )
            {
                m_startX[ cur ] = frvect[ 0 ]->GetDim( cur ).GetStartX( );
            }
        }
        return m_startX;
    }

    inline Channel::unitX_type
    Channel::GetUnitX( ) const
    {
        if ( !m_unitX )
        {
            const data_type& frvect( seed( ) );

            m_unitX.reset(
                new unitX_type::element_type[ frvect[ 0 ]->GetNDim( ) ] );
            for ( FrameCPP::FrVect::nDim_type cur = 0,
                                              last = frvect[ 0 ]->GetNDim( );
                  cur != last;
                  ++cur )
            {
                m_unitX[ cur ] = frvect[ 0 ]->GetDim( cur ).GetUnitX( );
            }
        }
        return m_unitX;
    }

    inline Channel::unitY_type
    Channel::GetUnitY( ) const
    {
        unitY_type retval;

        const data_type& frvect( seed( ) );

        retval = frvect[ 0 ]->GetUnitY( );

        return retval;
    }

    inline Channel::data_type&
    Channel::seed( ) const
    {
        return RefData( );
    }
} // namespace FrameAPI

#endif /* ! SWIGIMPORTED */
#endif /* FRAME_API__CHANNEL_HH */
