//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__CALIBRATION_VECTOR_HH
#define FRAME_API__CALIBRATION_VECTOR_HH

namespace FrameAPI
{
    namespace LIGO
    {
        namespace O4
        {
            //-----------------------------------------------------------
            // This information is based on dcc document
            //    LIGO-T1900007-v5
            //    Construction and Definition of the Calibration
            //       State Vector
            //===========================================================
            static constexpr char const* CHANNEL_NAME =
                "GDS-CALIB_STATE_VECTOR";
            enum StateMask
            {
                HOFT_OK = ( 1 << 0 ),
                OBS_INTENT = ( 1 << 1 ),
                OBS_READY = ( 1 << 2 ),
                FILTERS_OK = ( 1 << 3 ),
                NO_GAP = ( 1 << 04 ),
                NO_STOCH_HW_INJ = ( 1 << 5 ),
                NO_CBC_HW_INJ = ( 1 << 6 ),
                NO_BURST_HW_INJ = ( 1 << 7 ),
                NO_DETCHAR_HW_INJ = ( 1 << 8 ),
                UNDISTURBED_OK = ( 1 << 9 ),
                KAPPA_TST_SMOOTH_OK = ( 1 << 10 ),
                KAPPA_PUM_SMOOTH_OK = ( 1 << 11 ),
                KAPPA_UIM_SMOOTH_OK = ( 1 << 12 ),
                KAPPA_C_SMOOTH_OK = ( 1 << 13 ),
                F_CC_SMOOTH_OK = ( 1 << 14 ),
                F_S_SQUARED_SMOOTH_OK = ( 1 << 15 ),
                F_S_OVER_Q_SMOOTH_OK = ( 1 << 16 ),
                LOWFREQ_LINE_SUBTR = ( 1 << 17 ),
                MIDFREQ_LINE_SUBTR = ( 1 << 18 ),
                LINE_SUBTR_GATE = ( 1 << 19 ),
                NONSENS_SUBTR = ( 1 << 20 )
            };
        } // namespace O4
    } // namespace LIGO
} // namespace FrameAPI

#endif // FRAME_API__CALIBRATION_VECTOR_HH
