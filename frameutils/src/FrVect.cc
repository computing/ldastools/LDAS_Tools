//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

#include <memory>

#include <boost/shared_array.hpp>

#include "framecpp/Dimension.hh"

#include "FrVect.hh"
#include "util.hh"

using FrameCPP::Dimension;
using FrameCPP::FrVect;

using namespace std;

//! ignore_begin:

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

#if LM_DEBUG
#define INFO( a )                                                              \
    std::cerr << a << " (" << __FILE__ << " " << __LINE__ << ")" << std::endl;
#else
#define INFO( a )
#endif

using namespace FrameAPI::FrVect;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void
FrameAPI::FrVect::appendStructures( Container& Dest, const Container& Source )
{
    for ( FrameAPI::FrVect::Container::const_iterator i( Source.begin( ) );
          i != Source.end( );
          i++ )
    {
        Dest.append( *i );
    }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void
FrameAPI::FrVect::copy( void* Dest, const Container& Source )
{
    FrameCPP::FrVect::data_type               expanded;
    FrameCPP::FrVect::data_const_pointer_type src;

    char* pos( (char*)Dest );

    for ( FrameAPI::FrVect::Container::const_iterator i( Source.begin( ) );
          i != Source.end( );
          i++ )
    {
        FrameCPP::FrVect::nBytes_type bytes(
            FrameCPP::FrVect::GetTypeSize( ( *i )->GetType( ) ) *
            ( *i )->GetNData( ) );

        src = ( *i )->GetDataUncompressed( expanded );
        memcpy( pos, src, bytes );
        pos += bytes;
    }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
FrameCPP::FrVect*
FrameAPI::FrVect::concat( const FrVectList& List )
{
    FrameCPP::FrVect::nData_type  samples( 0 );
    FrameCPP::FrVect::nDim_type   dim_zero( 0 );
    std::list< INT_4U >           segment_samples;
    boost::shared_array< CHAR_U > expanded;

    const FrameCPP::FrVect& pattern( *( ( List.front( ) )->front( ) ) );

    //---------------------------------------------------------------------
    // Information gathering loop
    //---------------------------------------------------------------------

    AT( );
    for ( FrVectList::const_iterator i( List.begin( ) ); i != List.end( ); i++ )
    {
        AT( );
        segment_samples.push_back(
            getSamples( ( *i )->begin( ), ( *i )->end( ) ) );
        samples += segment_samples.back( );
        for ( FrameAPI::FrVect::Container::const_iterator j( ( *i )->begin( ) );
              j != ( *i )->end( );
              j++ )
        {
            dim_zero += ( *j )->GetDim( 0 ).GetNx( );
        }
    }

    //---------------------------------------------------------------------
    // Create a new vector
    //---------------------------------------------------------------------

    AT( );
    vector< Dimension > d( pattern.GetNDim( ) );
    AT( );
    for ( INT_4U i( pattern.GetNDim( ) ); i-- > 0; )
    {
        AT( );
        d[ i ] = pattern.GetDim( i );
    }
    AT( );

    FrameCPP::Dimension dim(
        samples, d[ 0 ].GetDx( ), d[ 0 ].GetUnitX( ), d[ 0 ].GetStartX( ) );

    INFO( "samples: " << samples );
    // Doesn't look necessary to do
    // std::copy( d, &(d[ pattern.GetNDim( ) ]), d );
    AT( );
    FrameCPP::FrVect::data_type data(
        reinterpret_cast< FrameCPP::FrVect::data_pointer_type >(
            createVector( pattern.GetType( ), samples ) ) );
    std::unique_ptr< FrameCPP::FrVect > ret( new FrameCPP::FrVect(
        pattern.GetName( ),
        FrameCPP::FrVect::RAW,
        pattern.GetType( ),
        pattern.GetNDim( ),
        &dim,
        samples,
        FrameCPP::FrVect::GetTypeSize( pattern.GetType( ) ) * samples,
        data,
        pattern.GetUnitY( ) ) );

    // Adjust the array
    const FrameCPP::Dimension& zdim( ret->GetDim( 0 ) );
    ret->GetDim( 0 ) = FrameCPP::Dimension(
        dim_zero, zdim.GetDx( ), zdim.GetUnitX( ), zdim.GetStartX( ) );

    //---------------------------------------------------------------------
    // Data population loop
    //---------------------------------------------------------------------
    {
        AT( );
        FrameCPP::FrVect::data_pointer_type data_ptr(
            ret->GetDataRaw( ).get( ) );
        std::list< INT_4U >::const_iterator ssi( segment_samples.begin( ) );
        INT_4U t_size( FrameCPP::FrVect::GetTypeSize( ret->GetType( ) ) );
        INFO( "t_size: " << t_size );

        for ( FrVectList::const_iterator i( List.begin( ) ); i != List.end( );
              data_ptr += *ssi * t_size, i++, ssi++ )
        {
            AT( );
            FrameAPI::FrVect::copy( data_ptr, *( *i ) );
        }
    }

    //---------------------------------------------------------------------
    // return the results to the user.
    //---------------------------------------------------------------------
    return ret.release( );
}

//-----------------------------------------------------------------------------

void*
FrameAPI::FrVect::createVector( INT_2U Type, INT_4U Size )
{
    AT( );
    switch ( Type )
    {
    case FrameCPP::FrVect::FR_VECT_C:
        return new char[ Size ];
    case FrameCPP::FrVect::FR_VECT_2S:
        return new INT_2S[ Size ];
    case FrameCPP::FrVect::FR_VECT_8R:
        return new REAL_8[ Size ];
    case FrameCPP::FrVect::FR_VECT_4R:
        return new REAL_4[ Size ];
    case FrameCPP::FrVect::FR_VECT_4S:
        return new INT_4S[ Size ];
    case FrameCPP::FrVect::FR_VECT_8S:
        return new INT_8S[ Size ];
    case FrameCPP::FrVect::FR_VECT_8C:
        return new REAL_4[ Size * 2 ];
    case FrameCPP::FrVect::FR_VECT_16C:
        return new REAL_8[ Size * 2 ];
    case FrameCPP::FrVect::FR_VECT_2U:
        return new INT_2U[ Size ];
    case FrameCPP::FrVect::FR_VECT_4U:
        return new INT_4U[ Size ];
    case FrameCPP::FrVect::FR_VECT_8U:
        return new INT_8U[ Size ];
    case FrameCPP::FrVect::FR_VECT_1U:
        return new CHAR_U[ Size ];
    default:
        AT( );
        throw SWIGEXCEPTION(
            "Unable to create data vector since type not known" );
    }
}

//-----------------------------------------------------------------------------

INT_4U
FrameAPI::FrVect::getSamples( Container::const_iterator Start,
                              Container::const_iterator Stop )
{
    INT_4U retval( 0 );

    for ( FrameAPI::FrVect::Container::const_iterator v( Start ); v != Stop;
          v++ )
    {
        retval += ( *v )->GetNData( );
    }
    return retval;
}
