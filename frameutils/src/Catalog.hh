//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__CATALOG_HH
#define FRAME_API__CATALOG_HH

#include <list>
#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/unordered_map.hh"

#include "framecpp/Common/FrameStream.hh"

#include "frameAPI/createRDS.hh"
#include "frameAPI/Frame.hh"

namespace FrameAPI
{
    //---------------------------------------------------------------------
    /// \brief Maintains a catalog of channels.
    //---------------------------------------------------------------------
    class Catalog
    {
    public:
        typedef std::list< std::string > stream_source_type;

        typedef LDASTools::AL::GPSTime start_type;
        typedef LDASTools::AL::GPSTime end_type;

        typedef INT_4U gps_seconds_type;

        typedef ::FrameAPI::channel_container_type channel_container_type;

        class ChannelMetaData
        {
        public:
            ChannelMetaData( );

            ChannelMetaData( const std::string& Name );

            ChannelMetaData( const ChannelMetaData& Source );

            const std::string& name( ) const;

        protected:
            void name( const std::string& Value );

        private:
            std::string _name;
        };

        class DataDictionary
        {
        public:
            typedef Frame::channel_type channel_type;
            typedef std::string         channel_name_type;

            void Push( channel_type Channel );

            channel_type operator[]( const channel_name_type& Name ) const;

        private:
            typedef LDASTools::AL::unordered_map< std::string, channel_type >
                channels_type;

            channels_type channels;
        };

        typedef boost::shared_ptr<
            LDASTools::AL::unordered_map< std::string, ChannelMetaData > >
            channel_dict_type;

        typedef boost::shared_ptr< DataDictionary > data_dict_type;

        Catalog( );

        Catalog( const stream_source_type& Sources );

        channel_dict_type Channels( ) const;

        data_dict_type Fetch( gps_seconds_type              Start,
                              gps_seconds_type              Stop,
                              const channel_container_type& Channels );

        start_type GPSStartTime( ) const;

        end_type GPSEndTime( ) const;

        void Open( const stream_source_type& Sources );

    private:
        class stream
        {
        public:
            typedef REAL_8                  dt_type;
            typedef INT_4U                  nFrame_type;
            typedef Catalog::start_type     start_type;
            typedef Catalog::end_type       end_type;
            typedef LDASTools::AL::ifstream ibuffer_stream_type;
            typedef boost::shared_ptr< FrameCPP::Common::IFrameStream >
                istream_type;

            stream( );

            stream( const stream& Source );

            stream( const std::string& Source );

            void close( );

            const end_type& end( ) const;

            const start_type& start( ) const;

            const FrameCPP::Common::FrTOC* toc( ) const;

        protected:
            void end( const end_type& Value );

            void start( const start_type& Value );

        private:
            istream_type istream;
            start_type   _start;
            end_type     _end;
            dt_type      dt;
        };

        typedef LDASTools::AL::unordered_map< std::string, stream >
            stream_set_type;

        stream_set_type   streams;
        start_type        start;
        bool              start_set;
        end_type          end;
        bool              end_set;
        channel_dict_type channels;

        void close_streams( );
    };

    inline const std::string&
    Catalog::ChannelMetaData::name( ) const
    {
        return _name;
    }

    inline void
    Catalog::ChannelMetaData::name( const std::string& Value )
    {
        _name = Value;
    }

    inline const Catalog::stream::end_type&
    Catalog::stream::end( ) const
    {
        return _end;
    }

    inline void
    Catalog::stream::end( const Catalog::stream::end_type& Value )
    {
        _end = Value;
    }

    inline const Catalog::stream::start_type&
    Catalog::stream::start( ) const
    {
        return _start;
    }

    inline void
    Catalog::stream::start( const Catalog::stream::start_type& Value )
    {
        _start = Value;
    }

    inline Catalog::channel_dict_type
    Catalog::Channels( ) const
    {
        return channels;
    }

    inline Catalog::start_type
    Catalog::GPSStartTime( ) const
    {
        return start;
    }

    inline Catalog::end_type
    Catalog::GPSEndTime( ) const
    {
        return end;
    }

} // namespace FrameAPI

#endif /* FRAME_API__CATALOG_HH */
