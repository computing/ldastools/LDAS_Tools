//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameApiRawDataHH
#define FrameApiRawDataHH

#include "framecpp/FrRawData.hh"
#include "framecpp/Time.hh"

namespace FrameAPI
{
    namespace FrRawData
    {
        //: merge two FrRawData pointers into one
        FrameCPP::FrRawData* merge( FrameCPP::FrRawData*       Primary,
                                    const FrameCPP::FrRawData* Secondary );
    } // namespace FrRawData
} // namespace FrameAPI

#endif // FrameApiRawDataHH
