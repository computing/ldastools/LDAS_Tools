//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

// System Header Files
#include <cmath>
#include <sstream>

// Local Header Files
#include "FrProcData.hh"
#include "FrVect.hh"
#include "util.hh"

using FrameCPP::FrProcData;
using FrameCPP::FrVect;
using FrameCPP::Time;

using namespace std;

namespace FrameAPI
{
    template <>
    REAL_8
    SampleRate( const FrameCPP::FrProcData& C )
    {
        switch ( C.GetType( ) )
        {
        case FrameCPP::FrProcData::TIME_SERIES:
        {
            if ( C.RefData( )[ 0 ] )
            {
                return REAL_8( 1.0 / C.RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) );
            }
        }
        break;
        default:
            break;
        }
        std::ostringstream oss;

        oss << "Unable to obtain or calculate the sample rate";
        throw SWIGEXCEPTION( oss.str( ) );
    }
} // namespace FrameAPI

enum
{
    FR_NAME,
    FR_COMMENT,
    FR_SAMPLERATE,
    FR_TIMEOFFSETS,
    FR_TIMEOFFSETN,
    FR_FSHIFT,
    FR_PHASE,
    FR_DATA,
    FR_TABLE,
    FR_AUX
};

//! ignore_begin:
QueryHash
initProcDataHash( )
{
    QueryHash h;
    h[ "name" ] = FR_NAME;
    h[ "comment" ] = FR_COMMENT;
    h[ "samplerate" ] = FR_SAMPLERATE;
    h[ "timeoffsets" ] = FR_TIMEOFFSETS;
    h[ "timeoffsetn" ] = FR_TIMEOFFSETN;
    h[ "fshift" ] = FR_FSHIFT;
    h[ "phase" ] = FR_PHASE;
    h[ "data" ] = FR_DATA;
    h[ "table" ] = FR_TABLE;
    h[ "aux" ] = FR_AUX;
    return h;
}

QueryHash procDataHash( initProcDataHash( ) );

using namespace FrameAPI::FrProcData;

FrameCPP::FrProcData*
FrameAPI::FrProcData::cloneHeader( const FrameCPP::FrProcData& Source )
{
    FrameCPP::FrProcData* ret =
        new FrameCPP::FrProcData( Source.GetName( ),
                                  Source.GetComment( ),
                                  Source.GetType( ),
                                  Source.GetSubType( ),
                                  Source.GetTimeOffset( ),
                                  Source.GetTRange( ),
                                  Source.GetFShift( ),
                                  Source.GetPhase( ),
                                  Source.GetFRange( ),
                                  Source.GetBW( ) );
    return ret;
}

FrameCPP::FrProcData*
FrameAPI::FrProcData::concat( const std::list< FrameCPP::Object* >& Segment )
{
    //:TODO: Need to ad support for aux data
    //-------------------------------------------------------------------
    // Loop over the parts getting the statistics
    //-------------------------------------------------------------------

    INT_4U samples( 0 );

    for ( std::list< FrameCPP::Object* >::const_iterator s( Segment.begin( ) );
          s != Segment.end( );
          s++ )
    {
        FrameCPP::FrProcData* src(
            dynamic_cast< FrameCPP::FrProcData* >( *s ) );

        samples += FrameAPI::FrVect::getSamples( src->RefData( ).begin( ),
                                                 src->RefData( ).end( ) );
    }

    //-------------------------------------------------------------------
    // Allocate a single channel to hold the data
    //-------------------------------------------------------------------
    std::unique_ptr< FrameCPP::FrProcData > c(
        FrameAPI::FrProcData::cloneHeader(
            *dynamic_cast< FrameCPP::FrProcData* >( Segment.front( ) ) ) );
    c->SetTRange( 0 );
    //-------------------------------------------------------------------
    // Fill in the data
    //-------------------------------------------------------------------

    FrameAPI::FrVect::FrVectList vector_list;

    for ( std::list< FrameCPP::Object* >::const_iterator s( Segment.begin( ) );
          s != Segment.end( );
          s++ )
    {
        FrameCPP::FrProcData* src(
            dynamic_cast< FrameCPP::FrProcData* >( *s ) );

        // Append all comments
        c->AppendComment( src->GetComment( ) );
        c->SetTRange( src->GetTRange( ) + c->GetTRange( ) );
        if ( s == Segment.begin( ) )
        {
            c->SetFRange( src->GetFRange( ) );
        }
        else
        {
            if ( c->GetFRange( ) != src->GetFRange( ) )
            {
                throw std::logic_error( "Unable to concatinate channels due to "
                                        "varying Frequency Ranges" );
            }
        }
        // Append auxiliary information
        FrameAPI::FrVect::appendStructures( c->RefAux( ), src->RefAux( ) );
        // Build a list of vectors to concat
        vector_list.push_back( &( src->RefData( ) ) );
    }
    //-----------------------------------------------------------------
    // Create the new vector and attach it to the channel
    //-----------------------------------------------------------------
    {
        FrameCPP::FrProcData::data_type::value_type d(
            FrameAPI::FrVect::concat( vector_list ) );

        c->RefData( ).append( d );
    }
    //-----------------------------------------------------------------
    // Return the results
    //-----------------------------------------------------------------

    return c.release( );
}
