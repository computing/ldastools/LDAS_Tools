//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RDS_RESAMPLE_FRAME_HH
#define RDS_RESAMPLE_FRAME_HH

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/unordered_map.hh"

#include "framecpp/Common/MD5Sum.hh"
#include "framecpp/FrameH.hh"
#include "framecpp/FrVect.hh"

#include "frameAPI/filereader.hh"
#include "frameAPI/rdsframe.hh"

namespace Filters
{
    class ResampleBase;
}

//-----------------------------------------------------------------------------
//
//: Functional to create RDS resampled frames.
//
class ResampleRawFrame : private RDSFrame
{
public:
    typedef INT_2U                              resample_type;
    typedef RDSFrame::frame_file_container_type frame_file_container_type;
    typedef RDSFrame::channel_container_type    channel_container_type;
    typedef std::vector< resample_type >        resample_arg_container_type;
    typedef LDASTools::AL::unordered_map<
        std::string,
        std::pair< INT_2U, boost::shared_ptr< Filters::ResampleBase > > >
        resample_container_type;

    //---------------------------------------------------------------------------
    /// \brief Constructor.
    ///
    /// \param[in] frame_files
    ///     A list of frame file names.
    /// \param[in] channels
    ///     A list of channels to extract from  original frames
    ///     (only channel names are allowed).
    /// \param[in] Resampling
    ///     A list of resampling values which corrisponds to the channels.
    /// \param[in] UserOptions
    ///     User specified options.
    ///
    /// \return
    ///     A new instance of this object
    //---------------------------------------------------------------------------
    ResampleRawFrame( const char*              frame_files,
                      const char*              channels,
                      const char*              Resampling,
                      const RDSFrame::Options& UserOptions );

    //---------------------------------------------------------------------------
    /// \brief Constructor.
    ///
    /// \param[in] frame_files
    ///     A list of frame file names.
    /// \param[in] channels
    ///     A list of channels to extract from  original frames
    ///     (only channel names are allowed).
    /// \param[in] Resampling
    ///     A list of resampling values which corrisponds to the channels.
    /// \param[in] UserOptions
    ///     User specified options.
    ///
    /// \return
    ///     A new instance of this object
    //---------------------------------------------------------------------------
    ResampleRawFrame( const frame_file_container_type& frame_files,
                      const channel_container_type&    channels,
                      const resample_container_type&   Resampling,
                      const RDSFrame::Options&         UserOptions );

    virtual ~ResampleRawFrame( );

    static resample_container_type
    Resample( const channel_container_type& Channels,
              const std::vector< INT_2U >&  Resampling );

    // Expose the main hook for processing information
    using RDSFrame::ProcessRequest;

protected:
    virtual void processChannel( fr_adc_data_type Adc );

    virtual void processChannel( fr_proc_data_type Proc );

    virtual stop_request_type stopRequest( ) const;

private:
    //: String representation of all resample factors.
    std::string mResampleRecord;

    /// \brief Information regarding resampling for each channel
    resample_container_type resampling;

    //: Dt of the current frame
    REAL_8 mCurrentDt;

    //: Current new frame
    frame_h_type mCurrentFrame;

    bool m_should_write;

    /// \brief Number of files that remain to be written to the file
    INT_4U m_remaining_to_write_to_file;

    // Create frame
    virtual void createHistory( );

    // Initialization
    void initResampleFactor( );

    // Mutators
    void adjustChannelData( );

    void adjustResultChannel( const std::string&           name,
                              const Filters::ResampleBase* state );

#if 0
   // Open a frame file
   virtual void openFrameFile( const std::string& Filename );
#endif /* 0 */

    virtual void rangeOptimizer( const time_type& UserStart,
                                 const time_type& UserStop,
                                 time_type&       DataStart,
                                 time_type&       DataStop ) const;

    virtual void writeFrameToStream( );
};

namespace FrameAPI
{
    namespace RDS
    {
        typedef ResampleRawFrame::channel_container_type channel_container_type;
        typedef ResampleRawFrame::resample_container_type
            resample_container_type;
        typedef ResampleRawFrame::resample_arg_container_type
            resample_arg_container_type;

        void ExpandChannelList( channel_container_type&      Channels,
                                resample_arg_container_type& ResampleRates,
                                const std::string&           SampleFrame );
    } // namespace RDS
} // namespace FrameAPI

#endif
