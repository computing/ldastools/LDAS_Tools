//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RDS_UTIL_HH
#define RDS_UTIL_HH

#include <vector>
#include <string>

#include "framecpp/Time.hh"

class Regex;

//-----------------------------------------------------------------------
/// \brief List parser class.
///
/// Handles comma separated string list.
///
//-----------------------------------------------------------------------
class ListParser
{
public:
    //---------------------------------------------------------------------
    ///
    /// \brief Constructor
    ///
    /// \param[in] list
    ///     List of comma separated strings.
    ///
    /// \return
    ///     A new instance of this object.
    //---------------------------------------------------------------------
    ListParser( const char* list );

    ~ListParser( );

    bool                             getNextToken( );
    const std::vector< std::string > getTokenList( );

private:
    static const char mSeparator;
    static const char mOpen;
    static const char mClose;

    char*       mBuffer;
    const char* mStart;
    const char* mCurrPos;
    std::string mName;
};

#if 0
class FrameName   
{
public:
   
   FrameName( const FrameCPP::Time& start, const REAL_8 dt )
      : mSec( start.getSec() ), mNan( start.getNSec() ), mDt( dt )
   {}
   const std::string operator()( const std::string& original_name,
      const std::string& dir ) const;

private:
   
   const std::string createName( const std::string& name ) const;

   static const Regex re_name;
   static const Regex re_rds_name;   
   
   const INT_4S mSec;
   const INT_4S mNan;   
   const REAL_8 mDt;   
};
#endif /* 0 */

#endif
