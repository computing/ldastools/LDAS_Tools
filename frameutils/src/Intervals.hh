//
// LDASTools Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__INTERVALS_HH
#define FRAME_API__INTERVALS_HH

namespace FrameAPI
{
    struct interval_type;
    struct interval_container_type;
} // namespace FrameAPI

std::ostream& operator<<( std::ostream&                  Stream,
                          const FrameAPI::interval_type& Object );

std::ostream& operator<<( std::ostream&                            Stream,
                          const FrameAPI::interval_container_type& Objects );

namespace FrameAPI
{
    struct interval_type : public std::pair< INT_4U, INT_4U >
    {
    public:
        typedef std::pair< INT_4U, INT_4U > base_type;

        using base_type::pair;

        base_type::first_type&  start = this->first;
        base_type::second_type& stop = this->second;

        //---------------------------------------------------------------
        // @brief copy constructor
        //---------------------------------------------------------------
        inline interval_type( const interval_type& Source )
            : base_type::pair( static_cast< const base_type& >( Source ) )
        {
            //-----------------------------------------------------------
            // This method is required as the default copy constructor
            // would overwrite the values of
            //   start and stop
            // with the values of Source which eventually leads to
            // invalid reads.
            //-----------------------------------------------------------
        }

    private:
        using base_type::first;
        using base_type::second;
    };

    struct interval_container_type : public std::list< interval_type >
    {
    public:
        typedef std::list< interval_type > base_type;

        using base_type::list;

        inline interval_container_type&
        operator+=( interval_type const& rhs_interval )
        {
            for ( auto interval_iter = begin( ); interval_iter != end( );
                  interval_iter++ )
            {
                if ( interval_iter->stop == rhs_interval.start )
                {
                    //---------------------------------------------------
                    // Simply extend the current segment
                    //---------------------------------------------------
                    interval_iter->stop = rhs_interval.stop;
                    return *this;
                }
                else if ( interval_iter->start == rhs_interval.stop )
                {
                    //---------------------------------------------------
                    // Simply prepend to the current segment
                    //---------------------------------------------------
                    interval_iter->start = rhs_interval.start;
                    return *this;
                }
                else if ( rhs_interval.start > interval_iter->stop )
                {
                    //---------------------------------------------------
                    // Not yet in the correct position. Try next entry
                    //---------------------------------------------------
                    continue;
                }
                insert( interval_iter, rhs_interval );
                return *this;
            }
            push_back( rhs_interval );
            return *this;
        }

        inline interval_container_type&
        operator+=( interval_container_type const& rhs_interval )
        {
            for ( auto const& interval : rhs_interval )
            {
                *this += interval;
            }
            return *this;
        }
    };

} // namespace FrameAPI

std::ostream&
operator<<( std::ostream& Stream, FrameAPI::interval_type const& Object )
{
    Stream << "[ " << Object.start << ", " << Object.stop << " )";
    return Stream;
}

std::ostream&
operator<<( std::ostream&                            Stream,
            FrameAPI::interval_container_type const& Objects )
{
    bool        print_seperator = false;
    static auto constexpr seperator = ", ";
    for ( auto object : Objects )
    {
        if ( print_seperator )
        {
            Stream << seperator;
        }
        else
        {
            print_seperator = true;
        }
        Stream << object;
    }
    return Stream;
}
#endif /* FRAME_API__INTERVALS_HH */
