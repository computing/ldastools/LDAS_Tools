//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAMEAPI__MOUNT_MGR_HH
#define FRAMEAPI__MOUNT_MGR_HH

#include <sys/types.h>

#include <sstream>
#include <set>
#include <string>

#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/ReadWriteLock.hh"

namespace FrameAPI
{
    class MountMgr
    {
    public:
        struct MountPoint
        {
            std::string s_fsname;
            std::string s_dir;
            std::string s_type;
            std::string s_options;
            dev_t       s_device;

            MountPoint( );
            MountPoint( const std::string& FSName,
                        const std::string& Dir,
                        const std::string& Type,
                        const std::string& Options,
                        dev_t              Device );
        };

        typedef LDASTools::AL::unordered_map< dev_t, MountPoint > mnt_info_type;
        typedef std::set< std::string >                  active_fstypes_type;
        typedef LDASTools::AL::ReadWriteLock::baton_type baton_type;

        MountMgr( );
        ~MountMgr( );

        void IsKnownFSType( const std::string& FSType );
#if 0
    const MountPoint Lookup( const std::string& FSType );
#endif /* 0 */
        const MountPoint Lookup( dev_t Device );

    private:
        baton_type          m_update_lock;
        mnt_info_type       m_mnt_info;
        active_fstypes_type m_active_fstypes;
        time_t              m_last_mtime;
        std::ostringstream  m_active_fstypes_msg;

        void sync( );
    };
} // namespace FrameAPI
#endif /* FRAMEAPI__MOUNT_MGR_HH */
