//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RDS_FRAME_HH
#define RDS_FRAME_HH

#include <memory>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/FrameH.hh"

#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrVect.hh"
#include "framecpp/Time.hh"

#include "genericAPI/Logging.hh"

#if 0
#if 0
#define DBGMSG( a ) std::cerr << a << __FILE__ << " " << __LINE__ << std::endl
#else
#define DBGMSG( a )
#endif
#endif

#define AT( a ) DBGMSG( a << " " )
#define HERE( ) DBGMSG( "" )

class RDSStream;

namespace FrameAPI
{
    namespace RDS
    {
        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        class MissingChannel : public std::runtime_error
        {
        public:
            typedef LDASTools::AL::GPSTime gps_time_type;

            gps_time_type const gps_start;
            gps_time_type const gps_stop;

            MissingChannel( const std::string& ChannelName,
                            const std::string  Filename = __FILE__,
                            const int          LineNumber = __LINE__ );

            MissingChannel( std::string const&   ChannelName,
                            gps_time_type const& Start,
                            gps_time_type const& Stop,
                            std::string const    Filename = __FILE__,
                            int const            LineNumber = __LINE__ );

        private:
            static std::string create_error_msg( const std::string& ChannelName,
                                                 const std::string  Filename,
                                                 const int LineNumber );

            static std::string
            create_error_msg( const std::string&            ChannelName,
                              const LDASTools::AL::GPSTime& Start,
                              const LDASTools::AL::GPSTime& Stop,
                              const std::string             Filename,
                              const int                     LineNumber );
        };

        class Options
        {
        public:
            typedef std::string analysis_ready_channel_name_type;
            typedef INT_4U      analysis_ready_mask_type;
            typedef INT_4U      seconds_per_frame_type;
            typedef INT_4U      start_type;
            typedef INT_4U      end_type;
            typedef INT_2U      compression_level_type;
            typedef INT_4U      frames_per_file_type;
            typedef INT_4U      rds_level_type;
            typedef INT_4U      padding_type;
            typedef FrameCPP::FrVect::compression_scheme_type
                compression_method_type;

            Options( );

            Options( const start_type             start,
                     const end_type               stop,
                     const char*                  type,
                     const char*                  compression_method,
                     const compression_level_type level,
                     const bool                   VerifyChecksum,
                     const bool                   VerifyFrameChecksum,
                     const bool                   VerifyTimeRange,
                     const bool                   VerifyDataValid,
                     const frames_per_file_type   FramesPerFile,
                     const seconds_per_frame_type SecondsPerFrame,
                     const bool                   AllowShortFrames,
                     const bool                   GenerateFrameChecksum,
                     const bool                   FillMissingDataValidArray,
                     const bool                   VerifyFilenameMetadata );
            Options( const Options& Source );

            bool AllowShortFrames( ) const;

            void AllowShortFrames( bool Value );

            const analysis_ready_channel_name_type&
            AnalysisReadyChannelName( ) const;

            void AnalysisReadyChannelName(
                const analysis_ready_channel_name_type& ChannelName );

            analysis_ready_mask_type AnalysisReadyMask( ) const;

            void AnalysisReadyMask( analysis_ready_mask_type Mask );

            compression_level_type CompressionLevel( ) const;

            void CompressionLevel( compression_level_type Value );

            compression_method_type CompressionMethod( ) const;

            void CompressionMethod( const std::string& Value );

            bool CreateChecksumPerFrame( ) const;

            void CreateChecksumPerFrame( bool Value );

            bool FillMissingDataValidArray( ) const;

            void FillMissingDataValidArray( bool Value );

            frames_per_file_type FramesPerFile( ) const;

            void FramesPerFile( frames_per_file_type FPF );

            bool GenerateFrameChecksum( ) const;

            void GenerateFrameChecksum( bool Value );

            bool HistoryRecord( ) const;

            void HistoryRecord( bool Value );

            end_type OutputTimeEnd( ) const;

            void OutputTimeEnd( end_type Value );

            start_type OutputTimeStart( ) const;

            void OutputTimeStart( start_type Value );

            const std::string& OutputType( ) const;

            void OutputType( const std::string& Value );

            padding_type Padding( ) const;

            void Padding( padding_type Value );

            rds_level_type RDSLevel( ) const;

            void RDSLevel( rds_level_type Level );

            seconds_per_frame_type SecondsPerFrame( ) const;

            void SecondsPerFrame( seconds_per_frame_type SPF );

            bool VerifyChecksum( ) const;

            bool VerifyChecksumOfFrame( ) const;

            bool VerifyChecksumPerFrame( ) const;

            void VerifyChecksumPerFrame( bool Value );

            bool VerifyChecksumOfStream( ) const;

            void VerifyChecksumOfStream( bool Value );

            bool VerifyDataValid( ) const;

            void VerifyDataValid( bool Value );

            bool VerifyFilenameMetadata( ) const;

            void VerifyFilenameMetadata( bool Value );

            bool VerifyTimeRange( ) const;

            void VerifyTimeRange( bool Value );

        private:
            analysis_ready_channel_name_type analysis_ready_channel_name = "";
            analysis_ready_mask_type         analysis_ready_mask = 0x0;
            bool                             history_record;
            //: Method for compression
            compression_method_type m_compression_method;
            //: Level for compression
            compression_level_type m_compression_level;
            //: Number of frames per file
            frames_per_file_type m_frames_per_file;
            //: If checksum for the input file should be verified
            bool m_input_verify_checksum;
            //: If input frames' checksums should be verified
            bool m_input_verify_checksum_per_frame;
            //: If input ADC channel's dataValid field should be checked
            bool m_input_verify_data_valid;
            //: Verify that the frame's name and internal metadata agree
            bool m_input_verify_filename_metadata;
            //: If input frame's filename should be validated against its time
            // range
            bool m_input_verify_time_range;
            //: Allow generation of short final frames
            bool m_output_allow_short_frames;
            //: If the checksum should be created for each frame on output
            bool m_output_checksum_per_frame;
            //: Allow creation of missing dataValid array
            bool m_output_fill_data_valid_array;
            //: Time when to start writing data to frame file
            INT_4U m_output_start;
            //: Time when to stop writing data to frame file
            INT_4U m_output_stop;
            //: Use type of frame file name
            std::string m_output_type;
            //: Number of seconds of padding
            padding_type padding;
            //: The current rds level
            // Not really a command line option but is calculated from the type
            //  field.
            rds_level_type m_rds_level;
            //: Number of seconds of data for each frame
            seconds_per_frame_type m_seconds_per_frame;

            //: The equal operator is not allowed since all data members are
            // constant
            const Options& operator=( const Options& RHS );

            std::string validate_type( const char* Type );
        }; // Class - Options

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        class FileOptions : public Options
        {
        public:
            FileOptions( );

            FileOptions( const INT_4U                 Start,
                         const INT_4U                 Stop,
                         const char*                  dir,
                         const char*                  type,
                         const char*                  compression_method,
                         const INT_2U                 level,
                         const bool                   VerifyChecksum,
                         const bool                   VerifyFrameChecksum,
                         const bool                   VerifyTimeRange,
                         const bool                   VerifyDataValid,
                         const frames_per_file_type   FramesPerFile,
                         const seconds_per_frame_type SecondsPerFrame,
                         const bool                   AllowShortFrames,
                         const bool                   GenerateFrameChecksum,
                         const bool                   FillMissingDataValidArray,
                         const bool                   VerifyFilenameMetadata,
                         const std::string&           MD5SumOutputDirectory );

            FileOptions( const FileOptions& Source );

            const std::string& DirectoryOutputFrames( ) const;

            const std::string& DirectoryOutputMD5Sum( ) const;

            const std::string& OutputDirectory( ) const;

            const std::string& MD5SumOutputDirectory( ) const;

            void DirectoryOutputFrames( const std::string& Value );

            void DirectoryOutputMD5Sum( const std::string& Value );

        private:
            //: Directory to write result frames to
            std::string m_output_directory;
            //: Where to put the md5sum calculations
            std::string m_md5sum_output_directory;
        };

    } // namespace RDS
} // namespace FrameAPI

//-----------------------------------------------------------------------
///
/// \brief Base functional to create RDS frames.
///
//-----------------------------------------------------------------------
class RDSFrame
{
public:
    /// @brief RDS channel name type
    ///
    /// This structure contains two elements and is intended to handle
    /// channel renaming.
    /// If both elements are the same, no renaming takes place.
    /// <UL>
    ///   <LI>The first element of the pair is the input channel name.</LI>
    ///   <LI>The second element of the pair is the output channel name.</LI>
    /// </UL>
    struct channel_name_type : public std::pair< std::string, std::string >
    {
        typedef std::pair< std::string, std::string > base_type;

        using base_type::pair;

        std::string& old_channel_name = first;
        std::string& new_channel_name = second;

        inline channel_name_type( const channel_name_type& Source )
            : base_type::pair( static_cast< const base_type& >( Source ) )
        {
            //-----------------------------------------------------------
            // This method is required as the default copy constructor
            // would overwrite the values of
            //   old_channel_name and new_channel_name
            // with the values of Source which eventually leads to
            // invalid reads.
            //-----------------------------------------------------------
        }

        template <
          typename ContainerOfNames,
          typename ContainerOfResampling
          /*,
            typename std::enable_if<
                std::is_same< NameType, channel_name_type >::value &&
                std::is_class< NameType >::value >::type* = nullptr */ >
        static void
        ParseChannelName( const std::string&                       Source,
                          ContainerOfNames&            Names,
                          ContainerOfResampling& Resampling )
        {
            std::string                source( Source );
            std::string                rename;
            std::vector< std::string > tokens;

            //---------------------------------------------------------------
            // Handle renaming
            //---------------------------------------------------------------
            boost::split( tokens, source, boost::is_any_of( "=" ) );
            if ( tokens.size( ) <= 0 )
            {
                return;
            }
            if ( tokens.size( ) == 1 )
            {
                // There is no renaming
                source = tokens[ 0 ];
            }
            else if ( tokens.size( ) == 2 )
            {
                source = tokens[ 0 ];
                rename = tokens[ 1 ];
            }
            //---------------------------------------------------------------
            // Handle resampling
            //---------------------------------------------------------------
            tokens.resize( 0 );
            boost::split( tokens, source, boost::is_any_of( "!" ) );
            if ( tokens.size( ) == 2 )
            {
                // Handling of explictly specified resamping rate
                try
                {
                    // These are the valid resampling rates
                    source = tokens[ 0 ];
                    const std::set< int > valid_sample_rates = {
                        1, 2, 4, 8, 16
                    };
                    auto resample = std::stod( tokens[ 1 ] );

                    Resampling.push_back(
                        ( valid_sample_rates.find( resample ) !=
                          valid_sample_rates.end( ) )
                            ? resample
                            : 1 );
                }
                catch ( ... )
                {
                    Resampling.push_back( 1 );
                }
            }
            else
            {
                Resampling.push_back( 1 );
            }
            //---------------------------------------------------------------
            // Add name and re-name values as name has all extra
            //  data removed.
            //---------------------------------------------------------------
            std::string source_filename( source );
            Names.emplace_back(
                source_filename,
                ( rename.empty( ) ? source_filename : rename ) );
        }

        inline bool
        IsRenamed( ) const
        {
            return ( old_channel_name != new_channel_name );
        }

        friend std::ostream&
        operator<<( std::ostream&            OutputStream,
                    channel_name_type const& ChannelName )
        {
            OutputStream << ChannelName.old_channel_name;
            if ( ChannelName.old_channel_name != ChannelName.new_channel_name )
            {
                OutputStream << "[ new: " << ChannelName.new_channel_name
                             << " ]";
            }
            return OutputStream;
        }

    private:
        using std::pair< std::string, std::string >::first;
        using std::pair< std::string, std::string >::second;
    };

    typedef std::vector< std::string > frame_file_container_type;
    /// @brief Ordered collection of channel names
    typedef std::vector< channel_name_type > channel_container_type;
    typedef ::FrameAPI::RDS::Options         Options;

    typedef boost::shared_ptr< RDSStream > stream_type;

    //--------------------------------------------------------------------
    ///
    /// \brief Constructor.
    ///
    /// \param[in] frame_files
    ///     A list of frame file names.
    /// \param[in] channels
    ///     A list of channels to extract from original frames
    ///     (only channel names are allowed).
    /// \param[in] CommandOptions
    ///     Collection of flags used to direct the construction
    ///     of the Reduced Data Set results.
    ///
    //--------------------------------------------------------------------
    RDSFrame( const char*    frame_files,
              const char*    channels,
              const Options& CommandOptions );

    //--------------------------------------------------------------------
    ///
    /// \brief Constructor.
    ///
    /// \param[in] frame_files
    ///     A list of frame file names.
    /// \param[in] channels
    ///     A list of channels to extract from original frames
    ///     (only channel names are allowed).
    /// \param[in] CommandOptions
    ///     Collection of flags used to direct the construction
    ///     of the Reduced Data Set results.
    ///
    //--------------------------------------------------------------------
    RDSFrame( const frame_file_container_type& frame_files,
              const channel_container_type&    channels,
              const Options&                   CommandOptions );

    virtual ~RDSFrame( );

    INT_4U GetNumberOfChannels( ) const;
    INT_4U GetNumberOfFrameGroups( ) const;
    INT_4U GetNumberOfFrameFiles( ) const;

    void ProcessRequest( stream_type Output );

protected:
    // Useful typedefs
    typedef LDASTools::AL::GPSTime                    time_type;
    typedef boost::shared_ptr< FrameCPP::FrameH >     frame_h_type;
    typedef boost::shared_ptr< FrameCPP::FrAdcData >  fr_adc_data_type;
    typedef boost::shared_ptr< FrameCPP::FrProcData > fr_proc_data_type;

    typedef std::vector< std::string >::const_iterator stringv_const_iterator;

    typedef enum
    {
        STOP_DATA,
        STOP_USER
    } stop_request_type;

    Options m_options;

    RDSFrame( const Options& CommandOptions );

    virtual stop_request_type stopRequest( ) const = 0;

    virtual void processChannel( fr_adc_data_type Adc ) = 0;

    virtual void processChannel( fr_proc_data_type Proc ) = 0;

    virtual void rangeOptimizer( const time_type& UserStart,
                                 const time_type& UserStop,
                                 time_type&       DataStart,
                                 time_type&       DataStop ) const = 0;

    void postProcessFrame( LDASTools::AL::GPSTime const& DataStart,
                           REAL_8                        DataDt );

    void processAnalysisReady( stream_type Output );

    void processSimple( stream_type Output );

    // Close the current frame file
    virtual void writeFrameToStream( );

    template < typename Functor >
    void
    foreachChannel( Functor& Func ) const
    {
        for ( INT_4U cur = 0, last = GetNumberOfChannels( ); cur != last;
              ++cur )
        {
            Func( getChannelName( cur ).old_channel_name );
        }
    }

    // Accessors
    static FrameCPP::FrProcData*
    getProcChannel( const std::string&                     name,
                    FrameCPP::FrameH::procData_type* const proc );

    // Accessors
    static const std::string& getHistoryName( );

    // Get some pieces out of the frame
    FrameCPP::FrameH::procData_type* getResultProcData( );

    //: Result frame
    frame_h_type mResultFrame;

    //: Frame writer object.
    //
    // It is necessary to keep this one open while frames are being processed
    //   to minimize memory usage by writing frames as soon as they are
    //   available.
    // std::unique_ptr< FileWriter > mWriter;
    boost::shared_ptr< RDSStream > m_stream;

    virtual void createHistory( );

    const channel_name_type& getChannelName( INT_4U Offset ) const;

    const std::string& getInputChannelName( INT_4U Offset ) const;

    const std::string& getOutputChannelName( INT_4U Offset ) const;

private:
    class private_type;

    //--------------------------------------------------------------------
    // Holder for private information
    //--------------------------------------------------------------------

    std::string m_ifo_list;

    private_type* m_p;

    //: No default constructor
    RDSFrame( );

    //: No copy constructor
    RDSFrame( const RDSFrame& );

    //: No assignment operator
    RDSFrame& operator=( const RDSFrame& );

    void init( const frame_file_container_type& FrameFiles,
               const channel_container_type&    Channels );
};

//=======================================================================
//
//=======================================================================
namespace FrameAPI
{
    namespace RDS
    {
        //=================================================================
        //
        //=================================================================
        inline bool
        Options::AllowShortFrames( ) const
        {
            return m_output_allow_short_frames;
        }

        inline const Options::analysis_ready_channel_name_type&
        Options::AnalysisReadyChannelName( ) const
        {
            return ( analysis_ready_channel_name );
        }

        inline void
        Options::AnalysisReadyChannelName(
            const analysis_ready_channel_name_type& ChannelName )
        {
            analysis_ready_channel_name = ChannelName;
        }

        inline Options::analysis_ready_mask_type
        Options::AnalysisReadyMask( ) const
        {
            return ( analysis_ready_mask );
        }

        inline void
        Options::AnalysisReadyMask( analysis_ready_mask_type Mask )
        {
            analysis_ready_mask = Mask;
        }

        inline Options::compression_level_type
        Options::CompressionLevel( ) const
        {
            return m_compression_level;
        }

        inline Options::compression_method_type
        Options::CompressionMethod( ) const
        {
            return m_compression_method;
        }

        inline bool
        Options::CreateChecksumPerFrame( ) const
        {
            return m_output_checksum_per_frame;
        }

        inline bool
        Options::FillMissingDataValidArray( ) const
        {
            return m_output_fill_data_valid_array;
        }

        inline Options::frames_per_file_type
        Options::FramesPerFile( ) const
        {
            return m_frames_per_file;
        }

        inline void
        Options::FramesPerFile( frames_per_file_type FPF )
        {
            m_frames_per_file = FPF;
        }

        inline bool
        Options::GenerateFrameChecksum( ) const
        {
            return m_output_checksum_per_frame;
        }

        inline bool
        Options::HistoryRecord( ) const
        {
            return history_record;
        }

        inline Options::end_type
        Options::OutputTimeEnd( ) const
        {
            return m_output_stop;
        }

        inline Options::start_type
        Options::OutputTimeStart( ) const
        {
            return m_output_start;
        }

        inline const std::string&
        Options::OutputType( ) const
        {
            return m_output_type;
        }

        inline Options::padding_type
        Options::Padding( ) const
        {
            return padding;
        }

        inline void
        Options::Padding( padding_type Value )
        {
            padding = Value;
        }

        inline Options::rds_level_type
        Options::RDSLevel( ) const
        {
            return m_rds_level;
        }

        inline void
        Options::RDSLevel( rds_level_type Level )
        {
            m_rds_level = Level;
        }

        inline Options::seconds_per_frame_type
        Options::SecondsPerFrame( ) const
        {
            return m_seconds_per_frame;
        }

        inline void
        Options::SecondsPerFrame( seconds_per_frame_type SPF )
        {
            m_seconds_per_frame = SPF;
        }

        inline bool
        Options::VerifyChecksum( ) const
        {
            return VerifyChecksumOfStream( );
        }

        inline bool
        Options::VerifyChecksumOfFrame( ) const
        {
            return m_input_verify_checksum_per_frame;
        }

        inline bool
        Options::VerifyChecksumPerFrame( ) const
        {
            return VerifyChecksumOfFrame( );
        }

        inline bool
        Options::VerifyChecksumOfStream( ) const
        {
            return m_input_verify_checksum;
        }

        inline bool
        Options::VerifyDataValid( ) const
        {
            return m_input_verify_data_valid;
        }

        inline bool
        Options::VerifyFilenameMetadata( ) const
        {
            return m_input_verify_filename_metadata;
        }

        inline bool
        Options::VerifyTimeRange( ) const
        {
            return m_input_verify_time_range;
        }

        //=================================================================
        //=================================================================
        inline void
        Options::AllowShortFrames( bool Value )
        {
        }

        inline void
        Options::CompressionLevel( compression_level_type Value )
        {
            m_compression_level = Value;
        }

        inline void
        Options::CreateChecksumPerFrame( bool Value )
        {
            m_output_checksum_per_frame = Value;
        }

        inline void
        Options::FillMissingDataValidArray( bool Value )
        {
            m_output_fill_data_valid_array = Value;
        }

        inline void
        Options::GenerateFrameChecksum( bool Value )
        {
            m_output_checksum_per_frame = Value;
        }

        inline void
        Options::HistoryRecord( bool Value )
        {
            history_record = Value;
        }

        inline void
        Options::OutputTimeEnd( end_type Value )
        {
            m_output_stop = Value;
        }

        inline void
        Options::OutputTimeStart( start_type Value )
        {
            m_output_start = Value;
        }

        inline void
        Options::OutputType( const std::string& Value )
        {
            m_output_type = Value;
        }

        inline void
        Options::VerifyChecksumOfStream( bool Value )
        {
            m_input_verify_checksum = Value;
        }

        inline void
        Options::VerifyChecksumPerFrame( bool Value )
        {
            m_input_verify_checksum_per_frame = Value;
        }

        inline void
        Options::VerifyDataValid( bool Value )
        {
            m_input_verify_data_valid = Value;
        }

        inline void
        Options::VerifyFilenameMetadata( bool Value )
        {
            m_input_verify_filename_metadata = Value;
        }

        inline void
        Options::VerifyTimeRange( bool Value )
        {
            m_input_verify_time_range = Value;
        }

        //=================================================================
        //
        //=================================================================
        inline const std::string&
        FileOptions::DirectoryOutputMD5Sum( ) const
        {
            return m_md5sum_output_directory;
        }

        inline const std::string&
        FileOptions::DirectoryOutputFrames( ) const
        {
            return m_output_directory;
        }

        inline const std::string&
        FileOptions::MD5SumOutputDirectory( ) const
        {
            return DirectoryOutputMD5Sum( );
        }

        inline const std::string&
        FileOptions::OutputDirectory( ) const
        {
            return DirectoryOutputFrames( );
        }

        inline void
        FileOptions::DirectoryOutputFrames( const std::string& Value )
        {
            m_output_directory = Value;
        }

        inline void
        FileOptions::DirectoryOutputMD5Sum( const std::string& Value )
        {
            m_md5sum_output_directory = Value;
        }

    } // namespace RDS
} // namespace FrameAPI

//=======================================================================
// Inline methods for class RDSFrame
//=======================================================================
inline FrameCPP::FrameH::procData_type*
RDSFrame::getResultProcData( )
{
    return &( mResultFrame->RefProcData( ) );
}
#endif /* RDS_FRAME_HH */
