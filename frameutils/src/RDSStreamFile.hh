//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RDS_STREAM_FILE_HH
#define RDS_STREAM_FILE_HH

#include <boost/shared_ptr.hpp>

#include "filereader.hh"
#include "RDSStream.hh"

class RDSStreamFile : public RDSStream
{
public:
    typedef std::list< std::string > result_type;
    typedef INT_4U                   rds_level_type;
    typedef INT_4U                   frames_per_file_type;

    RDSStreamFile( const std::string&         OutputDirectory,
                   const std::string&         OutputType,
                   const std::string&         MD5SumOutputDirectory,
                   const frames_per_file_type FramesPerFile );

    ~RDSStreamFile( );

    virtual void Abandon( );

    virtual void Close( bool Final );

    virtual bool Next( const LDASTools::AL::GPSTime& FrameStart,
                       const REAL_8                  Dt,
                       INT_4U&                       FramesPerStream );

    const result_type& Results( ) const;

    rds_level_type RDSLevel( ) const;

    virtual void Write( frame_h_type            Frame,
                        compression_scheme_type CompressionScheme,
                        compression_level_type  CompressionLevel,
                        chkSum_type             CheckSum );

protected:
    typedef boost::shared_ptr< FileWriter > file_writer_type;
    typedef REAL_8                          delta_t_type;

    std::string m_md5sum_output_directory;

    virtual void appendOutputFrameFilename( const std::string& Filename );

    virtual void closeFrameFile( const std::string& Filename, bool Final );

    void closeFrameWriter( file_writer_type   Writer,
                           const std::string& Filename );

    virtual void openFrameFile( const std::string& Filename );

    void ensureNoSuchFile( const std::string& Filename ) const;

    file_writer_type fileWriter( );

    void fileWriter( file_writer_type::element_type* Value );

    void fileWriterUnset( );

    std::string generateOutputName( INT_4U FileStart, REAL_8 FileDeltaT ) const;

    std::string generateTmpOutputName( INT_4U FileStart ) const;

    frames_per_file_type frames_per_file;

private:
    std::string    m_output_directory;
    result_type    m_output_frame_names;
    std::string    m_output_type;
    rds_level_type m_rds_level;

    file_writer_type m_writer;
};

inline RDSStreamFile::rds_level_type
RDSStreamFile::RDSLevel( ) const
{
    return m_rds_level;
}

inline const RDSStreamFile::result_type&
RDSStreamFile::Results( ) const
{
    return m_output_frame_names;
}

inline RDSStreamFile::file_writer_type
RDSStreamFile::fileWriter( )
{
    return m_writer;
}

inline void
RDSStreamFile::fileWriter(
    RDSStreamFile::file_writer_type::element_type* Value )
{
    m_writer.reset( Value );
}

inline void
RDSStreamFile::fileWriterUnset( )
{
    fileWriter( (RDSStreamFile::file_writer_type::element_type*)NULL );
}

#endif /* RDS_STREAM_FILE_HH */
