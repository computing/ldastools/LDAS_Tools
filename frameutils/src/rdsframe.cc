//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

#include <unistd.h>
#include <string.h>

#include <cctype>
#include <cerrno>

#include <algorithm>
#include <iterator>
#include <memory>
#include <sstream>
#include <stdexcept>

#include <boost/smart_ptr/make_unique.hpp>
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

// General
#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/regex.hh"
#include "ldastoolsal/regexmatch.hh"
#include "ldastoolsal/unordered_map.hh"

#include "framecpp/Common/CheckSum.hh"
#include "framecpp/Common/FrTOC.hh"
#include "framecpp/Common/MD5Sum.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/FrDetector.hh"
#include "framecpp/FrHistory.hh"
#include "framecpp/FrTOC.hh"
#include "framecpp/FrVect.hh"
#include "framecpp/STRING.hh"

// GenericAPI Header Files
#include "genericAPI/swigexception.hh"
#include "genericAPI/Logging.hh"

// Local Header Files
#include "DeviceIOConfiguration.hh"
#include "rdsframe.hh"
#include "rdsreduce.hh"
#include "rdsutil.hh"
#include "RDSStream.hh"
#include "filereader.hh"

#include "FileCache.icc"
#include "FileCacheEntry.icc"

#include "util.hh"

using namespace std;
using namespace FrameCPP;

using LDASTools::AL::GPSTime;
using LDASTools::AL::MemChecker;

using FrameAPI::DeviceIOConfiguration;
using FrameAPI::LogMD5Sum;
using FrameAPI::StrToCompressionScheme;
using FrameCPP::Common::CheckSum;
using FrameCPP::Common::MD5Sum;

#if 0
#define DBGMSG( a ) std::cerr << a << __FILE__ << " " << __LINE__ << std::endl
#define DBGACTIVE 1
#else
#define DBGMSG( a )
#endif

#define AT( a ) DBGMSG( a << " " )
#define HERE( ) DBGMSG( "" )

typedef RDSFrame::channel_name_type channel_name_type;

namespace
{
    class CollectNames : public FrameCPP::Common::FrTOC::FunctionString,
                         public std::list< channel_name_type >
    {
    public:
        virtual ~CollectNames( );

        virtual void operator( )( const channel_name_type& ChannelName );
    };
} // namespace

namespace FrameAPI
{
    namespace RDSFrameCC
    {
        static const std::string mHistoryName( "createRDS" );

        //----------------------------------------------------------------
        // Note: This routine is only for debugging
        //----------------------------------------------------------------
        std::ostream&
        operator<<( std::ostream& Stream, const FileCache& Source )
        {
            for ( FileCache::entry_type::const_iterator
                      current( Source.begin( ) ),
                  end( Source.end( ) );
                  current != end;
                  current++ )
            {
                Stream << *current;
            }
            return Stream;
        }

        //----------------------------------------------------------------
        //----------------------------------------------------------------
        class ChannelEntry
        {
        public:
            typedef ChannelCacheEntry::fr_adc_data_type  fr_adc_data_type;
            typedef ChannelCacheEntry::fr_proc_data_type fr_proc_data_type;

            ChannelEntry( const channel_name_type& Name );
            ChannelEntry( const ChannelEntry& Source );
            const channel_name_type& GetName( ) const;
            fr_adc_data_type&        FrAdcData( );
            fr_proc_data_type&       FrProcData( );
            void                     Reset( );

        private:
            friend std::ostream& operator<<( std::ostream&,
                                             const ChannelEntry& );
            channel_name_type    m_name;
            fr_adc_data_type     m_adc;
            fr_proc_data_type    m_proc;
        };

        ChannelEntry::fr_adc_data_type&
        ChannelEntry::FrAdcData( )
        {
            return m_adc;
        }

        ChannelEntry::fr_proc_data_type&
        ChannelEntry::FrProcData( )
        {
            return m_proc;
        }

        void
        ChannelEntry::Reset( )
        {
            m_adc.reset( );
            m_proc.reset( );
        }

        inline const channel_name_type&
        ChannelEntry::GetName( ) const
        {
            return m_name;
        }

        std::ostream&
        operator<<( std::ostream& Stream, const ChannelEntry& Source )
        {
            Stream << Source.m_name;
            return Stream;
        }

    } // namespace RDSFrameCC
} // namespace FrameAPI

using namespace FrameAPI::RDSFrameCC;

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
typedef std::vector< ChannelEntry > channel_cache_type;

//=======================================================================
// class - RDSFrame::Options
//=======================================================================
namespace FrameAPI
{
    namespace RDS
    {
        MissingChannel::MissingChannel( const std::string& ChannelName,
                                        const std::string  Filename,
                                        const int          LineNumber )
            : std::runtime_error(
                  create_error_msg( ChannelName, Filename, LineNumber ) )
        {
        }

        MissingChannel::MissingChannel( const std::string& ChannelName,
                                        const LDASTools::AL::GPSTime& Start,
                                        const LDASTools::AL::GPSTime& Stop,
                                        const std::string             Filename,
                                        const int LineNumber )
            : std::runtime_error( create_error_msg(
                  ChannelName, Start, Stop, Filename, LineNumber ) ),
              gps_start( Start ), gps_stop( Stop )
        {
        }

        std::string
        MissingChannel::create_error_msg( const std::string& ChannelName,
                                          const std::string  Filename,
                                          const int          LineNumber )
        {
            std::ostringstream retval;

            retval << "Missing channel: " << ChannelName << "( " << Filename
                   << ": " << LineNumber << " )";
            return retval.str( );
        }

        std::string
        MissingChannel::create_error_msg( const std::string& ChannelName,
                                          const LDASTools::AL::GPSTime& Start,
                                          const LDASTools::AL::GPSTime& Stop,
                                          const std::string Filename,
                                          const int         LineNumber )
        {
            std::ostringstream retval;

            retval << "Unable to locate the channel named: " << ChannelName
                   << " in any of the input frames for the time range: "
                   << Start << " - " << Stop << "( " << Filename << ": "
                   << LineNumber << " )";
            return retval.str( );
        }

        Options::Options( )
            : history_record( true ),
              m_compression_method(
                  StrToCompressionScheme( "zero_suppress_otherwise_gzip" ) ),
              m_compression_level( 1 ), m_frames_per_file( 1 ),
              m_input_verify_checksum( false ),
              m_input_verify_checksum_per_frame( false ),
              m_input_verify_data_valid( false ),
              m_input_verify_filename_metadata( true ),
              m_input_verify_time_range( true ),
              m_output_allow_short_frames( true ),
              m_output_checksum_per_frame( true ),
              m_output_fill_data_valid_array( false ), m_output_start( 0 ),
              m_output_stop( 0 ), m_output_type( "" ), padding( 0 ),
              m_rds_level( 0 ), m_seconds_per_frame( 0 )
        {
        }

        Options::Options( const INT_4U Start,
                          const INT_4U Stop,
                          const char*  type,
                          const char*  compression_method,
                          const INT_2U level,
                          const bool   VerifyChecksum,
                          const bool   VerifyFrameChecksum,
                          const bool   VerifyTimeRange,
                          const bool   VerifyDataValid,
                          const INT_4U FramesPerFile,
                          const INT_4U SecondsPerFrame,
                          const bool   AllowShortFrames,
                          const bool   GenerateFrameChecksum,
                          const bool   FillMissingDataValidArray,
                          const bool   VerifyFilenameMetadata )
            : history_record( true ),
              m_compression_method(
                  StrToCompressionScheme( compression_method ) ),
              m_compression_level( level ), m_frames_per_file( FramesPerFile ),
              m_input_verify_checksum( VerifyChecksum ),
              m_input_verify_checksum_per_frame( VerifyFrameChecksum ),
              m_input_verify_data_valid( VerifyDataValid ),
              m_input_verify_filename_metadata( VerifyFilenameMetadata ),
              m_input_verify_time_range( VerifyTimeRange ),
              m_output_allow_short_frames( AllowShortFrames ),
              m_output_checksum_per_frame( GenerateFrameChecksum ),
              m_output_fill_data_valid_array( FillMissingDataValidArray ),
              m_output_start( Start ), m_output_stop( Stop ),
              m_output_type( type ), padding( 0 ), m_rds_level( 0 ),
              m_seconds_per_frame( SecondsPerFrame )
        {
            //-------------------------------------------------------------------
            // Sanity check on type
            //-------------------------------------------------------------------
            m_output_type = validate_type( m_output_type.c_str( ) );
            //-------------------------------------------------------------------
            // Sanity check for output directory.
            //-------------------------------------------------------------------
            if ( Start > Stop )
            {
                std::ostringstream msg;

                msg << "The start time (" << m_output_start
                    << ") exceeds the stop time (" << m_output_stop << ")";

                throw SWIGEXCEPTION( msg.str( ) );
            }
        }

        Options::Options( const Options& Source )
            : analysis_ready_channel_name( Source.analysis_ready_channel_name ),
              analysis_ready_mask( Source.analysis_ready_mask ),
              history_record( Source.history_record ),
              m_compression_method( Source.m_compression_method ),
              m_compression_level( Source.m_compression_level ),
              m_frames_per_file( Source.m_frames_per_file ),
              m_input_verify_checksum( Source.m_input_verify_checksum ),
              m_input_verify_checksum_per_frame(
                  Source.m_input_verify_checksum_per_frame ),
              m_input_verify_data_valid( Source.m_input_verify_data_valid ),
              m_input_verify_filename_metadata(
                  Source.m_input_verify_filename_metadata ),
              m_input_verify_time_range( Source.m_input_verify_time_range ),
              m_output_allow_short_frames( Source.m_output_allow_short_frames ),
              m_output_checksum_per_frame( Source.m_output_checksum_per_frame ),
              m_output_fill_data_valid_array(
                  Source.m_output_fill_data_valid_array ),
              m_output_start( Source.m_output_start ),
              m_output_stop( Source.m_output_stop ),
              m_output_type( Source.m_output_type ),
              padding( Source.Padding( ) ), m_rds_level( Source.m_rds_level ),
              m_seconds_per_frame( Source.m_seconds_per_frame )
        {
        }

        void
        Options::CompressionMethod( const std::string& Value )
        {
            m_compression_method = StrToCompressionScheme( Value.c_str( ) );
        }

        std::string
        Options::validate_type( const char* Type )
        {
            //-------------------------------------------------------------------
            // Initialization
            //-------------------------------------------------------------------
            std::string retval( Type );
            m_rds_level = 0;

            //-------------------------------------------------------------------
            // Sanity check for type specifier
            //-------------------------------------------------------------------
            if ( retval.length( ) > 0 )
            {
                static const char* good_characters =
                    "abcdefghijklmnopqrstuvwxyz"
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    "0123456789_";

                if ( retval.find_first_not_of( good_characters ) !=
                     std::string::npos )
                {
                    throw SWIGEXCEPTION( "Invalid character in type field" );
                }
                //-----------------------------------------------------------------
                // Search for the version level
                //-----------------------------------------------------------------
                std::string::size_type level_pos =
                    retval.find_last_not_of( "0123456789" );

                if ( level_pos != std::string::npos )
                {
                    std::istringstream lvl( retval.substr( ++level_pos ) );
                    lvl >> m_rds_level; // Translate ASCII to int
                    retval.erase( level_pos ); // Remove level from string
                }
            }
            else
            {
                retval = "RDS_R_L";
            }
            return retval;
        }

        FileOptions::FileOptions( )
            : Options( ), m_output_directory( ), m_md5sum_output_directory( )
        {
        }

        FileOptions::FileOptions( const INT_4U       Start,
                                  const INT_4U       Stop,
                                  const char*        dir,
                                  const char*        type,
                                  const char*        compression_method,
                                  const INT_2U       level,
                                  const bool         VerifyChecksum,
                                  const bool         VerifyFrameChecksum,
                                  const bool         VerifyTimeRange,
                                  const bool         VerifyDataValid,
                                  const INT_4U       FramesPerFile,
                                  const INT_4U       SecondsPerFrame,
                                  const bool         AllowShortFrames,
                                  const bool         GenerateFrameChecksum,
                                  const bool         FillMissingDataValidArray,
                                  const bool         VerifyFilenameMetadata,
                                  const std::string& MD5SumOutputDirectory )
            : Options( Start,
                       Stop,
                       type,
                       compression_method,
                       level,
                       VerifyChecksum,
                       VerifyFrameChecksum,
                       VerifyTimeRange,
                       VerifyDataValid,
                       FramesPerFile,
                       SecondsPerFrame,
                       AllowShortFrames,
                       GenerateFrameChecksum,
                       FillMissingDataValidArray,
                       VerifyFilenameMetadata ),
              m_output_directory( dir ),
              m_md5sum_output_directory( MD5SumOutputDirectory )
        {
            //-------------------------------------------------------------------
            // Sanity check for output directory.
            //-------------------------------------------------------------------
            if ( m_output_directory.empty( ) )
            {
                throw SWIGEXCEPTION( "Output directory must be specified." );
            }
            //-------------------------------------------------------------------
            // Make sure the output directory is writable by user
            // :TODO: Need to create test for this new code
            //-------------------------------------------------------------------
            if ( access( m_output_directory.c_str( ), R_OK | W_OK | X_OK ) !=
                 0 )
            {
                std::ostringstream msg;

                switch ( errno )
                {
                case ENOENT:
                    msg << "The specified output directory ("
                        << m_output_directory.c_str( ) << ") does not exist";
                    break;
                default:
                    msg << "Insufficient permission to create files under the "
                           "directory \""
                        << m_output_directory.c_str( ) << "\"";
                    break;
                }
                throw SWIGEXCEPTION( msg.str( ) );
            }
        }

        FileOptions::FileOptions( const FileOptions& Source )
            : Options( Source ),
              m_output_directory( Source.m_output_directory ),
              m_md5sum_output_directory( Source.m_md5sum_output_directory )
        {
        }

    } // namespace RDS
} // namespace FrameAPI

//=======================================================================
// class RDSFrame::private_type
//=======================================================================
class RDSFrame::private_type
{
public:
    typedef RDSFrame::channel_name_type     channel_name_type;
    typedef boost::shared_ptr< FrameH >     frame_h_type;
    typedef boost::shared_ptr< FrAdcData >  fr_adc_data_type;
    typedef boost::shared_ptr< FrProcData > fr_proc_data_type;

    private_type( );

    void                      Close( );
    frame_h_type              CreateFrame( const LDASTools::AL::GPSTime& Start,
                                           const LDASTools::AL::GPSTime& Stop );
    frame_h_type              CreateFrame( const LDASTools::AL::GPSTime& Start,
                                           REAL_8                        DeltaT );
    const channel_cache_type& GetChannelCache( ) const;
    channel_cache_type&       GetChannelCache( );
    const FileCache&          GetFileCache( ) const;
    //
    char*        GetBuffer( ) const;
    unsigned int GetBufferSize( ) const;
    // Get the starting time of FileCache
    LDASTools::AL::GPSTime GetStart( ) const;
    LDASTools::AL::GPSTime GetStop( ) const;
    void   InitChannelCache( const channel_container_type& Channels );
    INT_4U InitFileCache( const RDSFrame::Options&          Options,
                          const std::vector< std::string >& FileList );

    FrameCPP::Common::MD5Sum& MD5Sum( );

    void PurgeFilesOlderThan( const LDASTools::AL::GPSTime& Start );

    void RetrieveChannel( const channel_name_type&      Channel,
                          frame_h_type&                 Frame,
                          const LDASTools::AL::GPSTime& Start,
                          REAL_8&                       DeltaT,
                          fr_adc_data_type&             Adc,
                          fr_proc_data_type&            Proc,
                          const bool                    VerifyDataValid,
                          const bool FillMissingDataValidArray );
    void RetrieveChannels( frame_h_type&                 Frame,
                           RDSFrame&                     OutputFrame,
                           const LDASTools::AL::GPSTime& Start,
                           const REAL_8                  DeltaT,
                           const RDSFrame::Options&      Options );
    bool
    RetrieveChannelsWhichAreAnalysisReady( frame_h_type& Frame,
                                           RDSFrame&     OutputFrame,
                                           const LDASTools::AL::GPSTime& Start,
                                           REAL_8&                       DeltaT,
                                           const RDSFrame::Options& Options );

private:
    channel_cache_type          m_channel_cache;
    FileCache                   m_file_cache;
    boost::shared_array< char > m_buffer;
    unsigned int                m_buffer_size;
};

inline void
RDSFrame::private_type::Close( )
{
    m_file_cache.Close( );
}

inline const channel_cache_type&
RDSFrame::private_type::GetChannelCache( ) const
{
    HERE( );
    return m_channel_cache;
}

inline channel_cache_type&
RDSFrame::private_type::GetChannelCache( )
{
    HERE( );
    return m_channel_cache;
}

inline char*
RDSFrame::private_type::GetBuffer( ) const
{
    return m_buffer.get( );
}

inline unsigned int
RDSFrame::private_type::GetBufferSize( ) const
{
    return m_buffer_size;
}

inline const FileCache&
RDSFrame::private_type::GetFileCache( ) const
{
    return m_file_cache;
}

inline LDASTools::AL::GPSTime
RDSFrame::private_type::GetStart( ) const
{
    return m_file_cache.GetStart( );
}

inline LDASTools::AL::GPSTime
RDSFrame::private_type::GetStop( ) const
{
    return m_file_cache.GetStop( );
}

void
RDSFrame::private_type::InitChannelCache(
    const channel_container_type& Channels )
{
    HERE( );
    m_channel_cache.reserve( Channels.size( ) );
    for ( auto const& cur : Channels )
    {
        HERE( );
        AT( "Adding channel: " << cur << " to channel cache" );
        m_channel_cache.emplace_back( ChannelEntry( cur ) );
    }
}

inline void
RDSFrame::private_type::PurgeFilesOlderThan(
    const LDASTools::AL::GPSTime& Start )
{
    m_file_cache.PurgeIfOlderThan( Start );
}

void
RDSFrame::private_type::RetrieveChannel( const channel_name_type&      Channel,
                                         frame_h_type&                 Frame,
                                         const LDASTools::AL::GPSTime& Start,
                                         REAL_8&                       DeltaT,
                                         fr_adc_data_type&             Adc,
                                         fr_proc_data_type&            Proc,
                                         const bool VerifyDataValid,
                                         const bool FillMissingDataValidArray )
{
    static const char* caller = "RDSFrame::private_type::RetrieveChannel";

    //--------------------------------------------------------------------
    // Look in the file chache for the given channels
    //--------------------------------------------------------------------
    HERE( );
    AT( "DEBUG: old_channel_name: " << Channel.old_channel_name
                                    << " new_channel_name: "
                                    << Channel.new_channel_name );
    const REAL_8 orig_dt = DeltaT;
    for ( auto current_file_cache_entry( m_file_cache.begin( ) ),
          last_file_cache_entry( m_file_cache.end( ) );
          current_file_cache_entry != last_file_cache_entry;
          current_file_cache_entry++ )
    {
        //-----------------------------------------------------------------
        // Locate a file that should have the requested channel
        //-----------------------------------------------------------------
        HERE( );
        if ( current_file_cache_entry->ContainsIFO(
                 Channel.old_channel_name[ 0 ] ) )
        {
            HERE( );
            if ( current_file_cache_entry->ReadChannel(
                     Channel,
                     Frame,
                     Start,
                     DeltaT,
                     Adc,
                     Proc,
                     VerifyDataValid,
                     FillMissingDataValidArray ) )
            {
                //-----------------------------------------------------------
                // Channel was found and has been read in.
                //-----------------------------------------------------------
                HERE( );
                return;
            }
            HERE( );
        }
        HERE( );
    }
    //--------------------------------------------------------------------
    // No channel was located, time to throw an excpetion
    //--------------------------------------------------------------------
    HERE( );
    QUEUE_LOG_MESSAGE(
        "Throwing exception: MissingChannel: " << Channel.old_channel_name,
        MT_DEBUG,
        30,
        caller,
        "CreateRDS" );
    throw FrameAPI::RDS::MissingChannel( Channel.old_channel_name,
                                         Start,
                                         ( Start + orig_dt ),
                                         __FILE__,
                                         __LINE__ );
}

void
RDSFrame::private_type::RetrieveChannels( frame_h_type& Frame,
                                          RDSFrame&     OutputFrame,
                                          const LDASTools::AL::GPSTime& Start,
                                          const REAL_8                  DeltaT,
                                          const RDSFrame::Options& Options )
{
    static const char* caller = "RDSFrame::private_type::RetrieveChannels";

    QUEUE_LOG_MESSAGE( "Entry:", MT_DEBUG, 50, caller, "CreateRDS" );
    if ( Frame )
    {
        QUEUE_LOG_MESSAGE( //
            "Frame: " << Frame->GetName( ) //
                      << " FrAdcData Entries: "
                      << ( ( Frame->GetRawData( ) )
                               ? Frame->GetRawData( )->RefFirstAdc( ).size( )
                               : 0 )
                      << " FrProcData: " << Frame->RefProcData( ).size( ) //
            ,
            MT_DEBUG,
            40,
            caller,
            "CreateRDS" );
    }
    //--------------------------------------------------------------------
    // Reset the dynamic parts of the channel entry
    //--------------------------------------------------------------------
    HERE( );
    for ( auto& current : GetChannelCache( ) )
    {
        current.Reset( );
    }

    //--------------------------------------------------------------------
    // Loop over all channels in all files creating the output data
    //--------------------------------------------------------------------

    LDASTools::AL::GPSTime end_time( Start + DeltaT );
    LDASTools::AL::GPSTime cur_start_time( Start );
    LDASTools::AL::GPSTime min_start_time( end_time );

    REAL_8 cur_dt = DeltaT;

    HERE( );
    while ( cur_dt > 0 )
    {
        for ( auto& current : GetChannelCache( ) )
        {
            REAL_8 dt = cur_dt;

            HERE( );
            RetrieveChannel( current.GetName( ),
                             Frame,
                             cur_start_time,
                             dt,
                             current.FrAdcData( ),
                             current.FrProcData( ),
                             Options.VerifyDataValid( ),
                             Options.FillMissingDataValidArray( ) );
            //--------------------------------------------------------------
            // Setup for next batch of reads
            //--------------------------------------------------------------
            if ( ( cur_start_time + dt ) < min_start_time )
            {
                min_start_time = ( cur_start_time + dt );
            }
        }
        if ( cur_start_time == min_start_time )
        {
            std::ostringstream msg;

            msg << "data missing at GPS time: " << cur_start_time.GetSeconds( );
            throw std::runtime_error( msg.str( ) );
        }
        cur_start_time = min_start_time;
        cur_dt = DeltaT - ( cur_start_time - Start );
        min_start_time = cur_start_time + cur_dt;
        PurgeFilesOlderThan( cur_start_time );
    }
    QUEUE_LOG_MESSAGE(
        "Frame: " << Frame->GetName( ) << " FrAdcData Entries: "
                  << ( ( Frame->GetRawData( ) )
                           ? Frame->GetRawData( )->RefFirstAdc( ).size( )
                           : 0 )
                  << " FrProcData: " << Frame->RefProcData( ).size( ),
        MT_DEBUG,
        40,
        caller,
        "CreateRDS" );
    QUEUE_LOG_MESSAGE( "Exit:", MT_DEBUG, 50, caller, "CreateRDS" );
}

bool
RDSFrame::private_type::RetrieveChannelsWhichAreAnalysisReady(
    frame_h_type&                 Frame,
    RDSFrame&                     OutputFrame,
    const LDASTools::AL::GPSTime& Start,
    REAL_8&                       DeltaT,
    const RDSFrame::Options&      Options )
{
    static const char* caller =
        "RDSFrame::private_type::RetrieveChannelsWhichAreAnalysisReady";

    QUEUE_LOG_MESSAGE( "Entry:", MT_DEBUG, 40, caller, "CreateRDS" );
    HERE( );
    bool is_short = false;
    if ( Frame )
    {
        HERE( );
        QUEUE_LOG_MESSAGE(
            "Frame: " << Frame->GetName( ) << " FrAdcData Entries: "
                      << ( ( Frame->GetRawData( ) )
                               ? Frame->GetRawData( )->RefFirstAdc( ).size( )
                               : 0 )
                      << " FrProcData: " << Frame->RefProcData( ).size( ),
            MT_DEBUG,
            40,
            caller,
            "CreateRDS" );
    }
    //--------------------------------------------------------------------
    // Reset the dynamic parts of the channel entry
    //--------------------------------------------------------------------
    for ( auto& current : GetChannelCache( ) )
    {
        HERE( );
        current.Reset( );
    }

    //--------------------------------------------------------------------
    // Loop over all channels in all files creating the output data
    //--------------------------------------------------------------------

    LDASTools::AL::GPSTime end_time( Start + DeltaT );
    LDASTools::AL::GPSTime cur_start_time( Start );
    LDASTools::AL::GPSTime min_start_time( end_time );

    REAL_8 cur_dt = DeltaT;
    bool   data_pending = false;

    HERE( );
    while ( ( cur_dt > 0 ) && ( !is_short ) )
    {
        try
        {
            for ( auto& current : GetChannelCache( ) )
            {
                HERE( );
                REAL_8 dt = cur_dt;

                try
                {
                    HERE( );
                    RetrieveChannel( current.GetName( ),
                                     Frame,
                                     cur_start_time,
                                     dt,
                                     current.FrAdcData( ),
                                     current.FrProcData( ),
                                     Options.VerifyDataValid( ),
                                     Options.FillMissingDataValidArray( ) );
                    data_pending = true;
                    HERE( );
                    QUEUE_LOG_MESSAGE( //
                        "Read channel:" //
                            << " data_pending: " << data_pending //
                            << " is_short: " << is_short //
                            << " cur_start_time: " << cur_start_time //
                            << " dt: " << dt //
                            << " cur_dt: " << cur_dt //
                        ,
                        MT_DEBUG,
                        30,
                        caller,
                        "CreateRDS" );
                }
                catch ( const FrameAPI::DataNotAnalysisReady& Exception )
                {
                    HERE( );
                    DeltaT = cur_dt = ( Exception.gps_start - Start );
                    QUEUE_LOG_MESSAGE( //
                        "Exception DataNotAnalysisReady:" //
                            << " data_pending: " << data_pending //
                            << " is_short: " << is_short //
                            << " gap start: " << Exception.gps_start //
                            << " gap dt: " << Exception.dt //
                            << " cur_start_time: " << cur_start_time //
                            << " cur_dt: " << cur_dt //
                            << " dt: " << dt //
                        ,
                        MT_DEBUG,
                        30,
                        caller,
                        "CreateRDS" );
                    HERE( );
                    throw;
                }
                //-----------------------------------------------------------
                // Setup for next batch of reads
                //-----------------------------------------------------------
                if ( ( cur_start_time + dt ) < min_start_time )
                {
                    HERE( );
                    min_start_time = ( cur_start_time + dt );
                }
                HERE( );
            }
            HERE( );
            if ( ( cur_start_time == min_start_time ) && ( DeltaT != 0 ) )
            {
                // Translate missing data into DataNotAnalysisReady exception
                FrameAPI::DataNotAnalysisReady missing( cur_start_time, 1 );
                DeltaT = 0;
                throw missing;
            }
            HERE( );
            cur_start_time = min_start_time;
            cur_dt = DeltaT - ( cur_start_time - Start );
            if ( cur_dt < 0 )
            {
                cur_dt = 0;
            }
            min_start_time = cur_start_time + cur_dt;
            QUEUE_LOG_MESSAGE( //
                "Setup for next next iteration:"
                    << " cur_start_time: " << cur_start_time //
                    << " cur_dt: " << cur_dt //
                    << " min_start_time: " << min_start_time //
                ,
                MT_DEBUG,
                40,
                caller,
                "CreateRDS" );
            HERE( );
            PurgeFilesOlderThan( cur_start_time );
        }
        catch ( ... )
        {
            HERE( );
            PurgeFilesOlderThan( cur_start_time );
            throw;
        }
    }
    QUEUE_LOG_MESSAGE(
        "Frame: " << Frame->GetName( ) << " FrAdcData Entries: "
                  << ( ( Frame->GetRawData( ) )
                           ? Frame->GetRawData( )->RefFirstAdc( ).size( )
                           : 0 )
                  << " FrProcData: " << Frame->RefProcData( ).size( ),
        MT_DEBUG,
        40,
        caller,
        "CreateRDS" );
    QUEUE_LOG_MESSAGE( "Exit:"
                           << " is_short: " << is_short,
                       MT_DEBUG,
                       40,
                       caller,
                       "CreateRDS" );
    return ( is_short );
}

inline INT_4U
RDSFrame::private_type::InitFileCache(
    const RDSFrame::Options&          Options,
    const std::vector< std::string >& FileList )
{
    m_file_cache.Init( Options, FileList );
    return m_file_cache.GetRDSLevel( );
}

std::ostream&
operator<<( std::ostream& Stream, const channel_cache_type& Source )
{
    Stream << "{ ";
    for ( channel_cache_type::const_iterator begin( Source.begin( ) ),
          current( Source.begin( ) ),
          end( Source.end( ) );
          current != end;
          current++ )
    {
        if ( current != begin )
        {
            Stream << ", ";
        }
        Stream << *current;
    }
    Stream << " }";
    return Stream;
}

RDSFrame::RDSFrame( const char*    frame_files,
                    const char*    channels,
                    const Options& CommandOptions )
    : // protected
      m_options( CommandOptions ),
      // private
      m_p( (private_type*)NULL )
{
    ListParser             frame_files_parser( frame_files );
    ListParser             channel_parser( channels );
    channel_container_type channel_names;

    HERE( );
    AT( "channels: " << channels );
    channel_names.reserve( channel_parser.getTokenList( ).size( ) );
    for ( const auto& channel_string : channel_parser.getTokenList( ) )
    {
        HERE( );
        auto pos = channel_string.find( '=' );
        if ( pos == std::string::npos )
        {
            // Case where no renaming is taking place
            HERE( );
            channel_names.emplace_back( channel_string, channel_string );
        }
        else
        {
            // Renaming takes place
            HERE( );
            auto old_channel_name = channel_string.substr( 0, pos );
            auto new_channel_name = channel_string.substr( pos + 1 );
            channel_names.emplace_back( old_channel_name, new_channel_name );
        }
    }

    HERE( );
    init( frame_files_parser.getTokenList( ), channel_names );
}

RDSFrame::RDSFrame( const frame_file_container_type& frame_files,
                    const channel_container_type&    channels,
                    const Options&                   CommandOptions )
    : // protected
      m_options( CommandOptions ),
      // private
      m_p( (private_type*)NULL )
{
#if DBGACTIVE
    AT( "channels.size( ): " << channels.size( ) );
    for ( auto const& channel : channels )
    {
        AT( "  channel: " << channel );
    }
#endif /* DBGACTIVE */
    init( frame_files, channels );
}

RDSFrame::RDSFrame( const Options& CommandOptions )
    : // protected
      m_options( CommandOptions ),
      // private
      m_p( (private_type*)NULL )
{
    HERE( );
}

//-----------------------------------------------------------------------------
//
//: Destructor.
//
RDSFrame::~RDSFrame( )
{
    delete m_p;
    m_p = (private_type*)NULL;
}

INT_4U
RDSFrame::GetNumberOfChannels( ) const
{
    return m_p->GetChannelCache( ).size( );
}

INT_4U
RDSFrame::GetNumberOfFrameGroups( ) const
{
    return m_p->GetFileCache( ).size( );
}

INT_4U
RDSFrame::GetNumberOfFrameFiles( ) const
{
    if ( m_p->GetFileCache( ).size( ) > 0 )
    {
        return m_p->GetFileCache( ).front( ).size( );
    }
    return 0;
}

//-----------------------------------------------------------------------------
//
//: Main control loop
//
// This method loops through all files and frames within the files.
//
void
RDSFrame::ProcessRequest( stream_type Output )
{
    static const char* caller = "RDSFrame::ProcessRequest";

    HERE( );
    if ( !Output )
    {
        HERE( );
        throw std::runtime_error( "Invalid Output stream" );
    }
    m_stream = Output;
    m_stream->IFOList( m_ifo_list );

    //--------------------------------------------------------------------
    // Sanity checks
    //--------------------------------------------------------------------
    HERE( );
    if ( m_options.FramesPerFile( ) == 0 )
    {
        HERE( );
        throw std::range_error(
            "The number of frames per file must be greater than 0" );
    }

    HERE( );
    if ( ( !m_options.AnalysisReadyChannelName( ).empty( ) ) &&
         ( m_options.AnalysisReadyMask( ) != 0x0 ) )
    {
        HERE( );
        QUEUE_LOG_MESSAGE( "Processing using Analysis Ready algorithm",
                           MT_DEBUG,
                           30,
                           caller,
                           "CreateRDS" );
        processAnalysisReady( Output );
    }
    else
    {
        HERE( );
        HERE( );
        QUEUE_LOG_MESSAGE( "Processing using Simple algorithm",
                           MT_DEBUG,
                           30,
                           caller,
                           "CreateRDS" );
        processSimple( Output );
    }
}

const RDSFrame::channel_name_type&
RDSFrame::getChannelName( INT_4U offset ) const
{
    HERE( );
    return m_p->GetChannelCache( ).operator[]( offset ).GetName( );
}

const std::string&
RDSFrame::getInputChannelName( INT_4U offset ) const
{
    HERE( );
    return m_p->GetChannelCache( )
        .
        operator[]( offset )
        .GetName( )
        .old_channel_name;
}

const std::string&
RDSFrame::getOutputChannelName( INT_4U offset ) const
{
    HERE( );
    return m_p->GetChannelCache( )
        .
        operator[]( offset )
        .GetName( )
        .new_channel_name;
}

void
RDSFrame::postProcessFrame( LDASTools::AL::GPSTime const& DataStart,
                            REAL_8                        DataDt )
{
    static const char* caller = "RDSFrame::postProcessFrame";

    //--------------------------------------------------------------
    // Post process the concatinated channel data
    //--------------------------------------------------------------
    QUEUE_LOG_MESSAGE(
        "Frame: " << mResultFrame->GetName( ) << " FrAdcData Entries: "
                  << ( ( mResultFrame->GetRawData( ) )
                           ? mResultFrame->GetRawData( )->RefFirstAdc( ).size( )
                           : 0 )
                  << " FrProcData: " << mResultFrame->RefProcData( ).size( )
                  << " DataStart: " << DataStart << " DataDt: " << DataDt,
        MT_DEBUG,
        30,
        caller,
        "CreateRDS" );
    AT( "m_p->GetChannelCache( ).size( ): "
        << m_p->GetChannelCache( ).size( ) );
    for ( auto& current : m_p->GetChannelCache( ) )
    {
        if ( MemChecker::IsExiting( ) )
        {
            std::ostringstream msg;

            msg << "System is exiting";

            HERE( );
            throw std::runtime_error( msg.str( ) );
        }
        if ( current.FrAdcData( ) )
        {
            //--------------------------------------------------------
            // Processing FrAdcData
            //--------------------------------------------------------
            HERE( );
            QUEUE_LOG_MESSAGE(
                "Processing FrAdcData", MT_DEBUG, 40, caller, "CreateRDS" );
            processChannel( current.FrAdcData( ) );
        }
        else if ( current.FrProcData( ) )
        {
            //--------------------------------------------------------
            // Processing FrProcData
            //--------------------------------------------------------
            HERE( );
            QUEUE_LOG_MESSAGE(
                "Processing FrProcData", MT_DEBUG, 40, caller, "CreateRDS" );
            processChannel( current.FrProcData( ) );
        }
        else
        {
            std::ostringstream msg;

            msg << "Have neither FrAdcData or FrProcData for channel: "
                << current.GetName( );
            AT( msg.str( ) );
            throw SWIGEXCEPTION( msg.str( ) );
        }
    }
    HERE( );
    QUEUE_LOG_MESSAGE(
        "Frame(after): "
            << mResultFrame->GetName( ) << " FrAdcData Entries: "
            << ( ( mResultFrame->GetRawData( ) )
                     ? mResultFrame->GetRawData( )->RefFirstAdc( ).size( )
                     : 0 )
            << " FrProcData: " << mResultFrame->RefProcData( ).size( ),
        MT_DEBUG,
        30,
        caller,
        "CreateRDS" );
    //-------------------------------------------------------------------
    // FrameH corrections
    //-------------------------------------------------------------------
    if ( mResultFrame.get( ) )
    {
        //----------------------------------------------------------
        // Correct some information
        //----------------------------------------------------------
        mResultFrame->SetGTime( DataStart );
        mResultFrame->SetDt( DataDt );
        QUEUE_LOG_MESSAGE( "Setting result frame attributes:"
                               << " DataStart: " << DataStart
                               << " DataDt: " << DataDt,
                           MT_DEBUG,
                           30,
                           caller,
                           "RDSFrame" );
        //----------------------------------------------------------
        // Check to see if any data is associated with the FrRawData
        //   structure
        //----------------------------------------------------------
        {
            boost::shared_ptr< FrameCPP::FrRawData > rd(
                mResultFrame->GetRawData( ) );

            if ( ( rd ) && ( rd->RefFirstAdc( ).size( ) == 0 ) &&
                 ( rd->RefFirstSer( ).size( ) == 0 ) &&
                 ( rd->RefFirstTable( ).size( ) == 0 ) &&
                 ( rd->RefLogMsg( ).size( ) == 0 ) &&
                 ( rd->RefMore( ).size( ) == 0 ) )
            {
                rd.reset( );
                mResultFrame->SetRawData( rd );
            }
        }
        //----------------------------------------------------------
        // Add our history information
        //----------------------------------------------------------
        createHistory( );
        //----------------------------------------------------------
        // Write frame to file
        //----------------------------------------------------------
        writeFrameToStream( );
    } // if ( mResultFrame.get( ) )
}

void
RDSFrame::processAnalysisReady( stream_type Output )
{
    static const char* caller = "RDSFrame::processAnalysisReady";

    const LDASTools::AL::GPSTime user_start( m_options.OutputTimeStart( ), 0 );
    const LDASTools::AL::GPSTime user_stop( m_options.OutputTimeEnd( ), 0 );

    LDASTools::AL::GPSTime data_start;
    LDASTools::AL::GPSTime data_stop;
    REAL_8                 data_dt;
    INT_4U                 frames_to_write( 0 );

    ChannelCacheEntry::fr_adc_data_type  adc;
    ChannelCacheEntry::fr_proc_data_type proc;

    //--------------------------------------------------------------------
    // Sanity checks
    //--------------------------------------------------------------------
    HERE( );
#if 0
    //--------------------------------------------------------------------
    // Because the RDSFrame is privately inherited by ReduceRawFrame,
    //   the dynamic_cast always returns nullptr
    //--------------------------------------------------------------------
    if ( dynamic_cast< ReduceRawFrame* >( this ) == nullptr )
      {
        std::ostringstream msg;

        msg << "Analysis ready selection criteria is only valid for "
          "ReduceRawFrame model";

        throw std::runtime_error( msg.str( ) );
      }
#endif /* 0 */
    //--------------------------------------------------------------------
    // Start processing the requests
    //--------------------------------------------------------------------
    try
    {
        //-----------------------------------------------------------------
        // Initialize
        //-----------------------------------------------------------------
        HERE( );
        data_start = m_p->GetStart( );
        data_stop = m_p->GetStop( );

        rangeOptimizer( user_start, user_stop, data_start, data_stop );

        m_stream->UserRange( user_start.GetSeconds( ),
                             user_stop.GetSeconds( ) );

        QUEUE_LOG_MESSAGE( "data_start: " << data_start
                                          << " data_stop: " << data_stop,
                           MT_DEBUG,
                           30,
                           caller,
                           "RDSFrame" );

        if ( data_start != GPSTime( m_options.OutputTimeStart( ), 0 ) )
        {
            data_dt = GPSTime( m_options.OutputTimeStart( ), 0 ) - data_start;
        }
        else
        {
            data_dt = m_options.SecondsPerFrame( );
        }

        //-----------------------------------------------------------------
        // Create each frame requested by user
        //-----------------------------------------------------------------
        const GPSTime stop( ( stopRequest( ) == STOP_DATA ) ? data_stop
                                                            : user_stop );
        while ( data_start < stop )
        {
            try
            {
                QUEUE_LOG_MESSAGE( //
                    "Top of try block:" //
                        << " data_start: " << data_start //
                        << " user_start: " << user_start //
                        << " user_stop: " << user_stop //
                        << " data_stop: " << data_stop //
                        << " stop: " << stop //
                        << " data_dt: " << data_dt //
                        << " frames_to_write: " << frames_to_write //
                    ,
                    MT_DEBUG,
                    30,
                    caller,
                    "RDSFrame" );
                if ( data_start == GPSTime( m_options.OutputTimeStart( ), 0 ) )
                {
                    // This is the case where the dt needs to be reset.
                    data_dt = m_options.SecondsPerFrame( );
                }
                if ( data_start < user_stop )
                {
                    //----------------------------------------------------------
                    // Check to see if the data_dt needs to be adjusted
                    //----------------------------------------------------------
                    if ( ( data_start + data_dt ) > user_stop )
                    {
                        data_dt = user_stop - data_start;
                    }
                }
                else
                {
                    //-----------------------------------------------------------
                    // For the case where the data stops beyond the user's
                    //   request, squeeze everything into a final frame.
                    //-----------------------------------------------------------
                    data_dt = ( data_stop - data_start );
                }
                if ( frames_to_write == 0 )
                {
                    frames_to_write = m_options.FramesPerFile( );
                    QUEUE_LOG_MESSAGE( "data_start: " << data_start
                                                      << " data_dt: " << data_dt
                                                      << " frames_to_write: "
                                                      << frames_to_write,
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "RDSFrame" );
                    m_stream->Next( data_start, data_dt, frames_to_write );
                    QUEUE_LOG_MESSAGE( "frames_to_write: " << frames_to_write,
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "RDSFrame" );
                    HERE( );
                    if ( frames_to_write == 0 )
                    {
                        QUEUE_LOG_MESSAGE(
                            "break", MT_DEBUG, 30, caller, "RDSFrame" );
                        break;
                    }
                    HERE( );
                    QUEUE_LOG_MESSAGE( "end of conditional",
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "RDSFrame" );
                }

                //--------------------------------------------------------------
                // Make room for the new frame
                //--------------------------------------------------------------
                HERE( );
                mResultFrame.reset( );
                try
                {
                    //--------------------------------------------------------------
                    // Add in channel information
                    //--------------------------------------------------------------
                    HERE( );
                    (void)m_p->RetrieveChannelsWhichAreAnalysisReady(
                        mResultFrame, *this, data_start, data_dt, m_options );
                    //--------------------------------------------------------------
                    // Post process the concatinated data
                    //--------------------------------------------------------------
                    HERE( );
                    postProcessFrame( data_start, data_dt );
                    //--------------------------------------------------------------
                    // Advance to the next frame and set the length of the
                    //   dt appropriately
                    //--------------------------------------------------------------
                    data_start += data_dt;
                    QUEUE_LOG_MESSAGE( //
                        "Completed postProcessFrame" //
                        ,
                        MT_DEBUG,
                        30,
                        caller,
                        "RDSFrame" );
                }
                catch ( FrameAPI::DataNotAnalysisReady const& Exception )
                {
                    //---------------------------------------------------
                    // Reposition data_start to the end of the analysis
                    //    ready gap
                    //---------------------------------------------------
                    QUEUE_LOG_MESSAGE( //
                        "DataNotAnalysisReady exception: " //
                            << Exception.what( ) //
                            << " data_start: " << data_start //
                            << " data_dt: " << data_dt //
                        ,
                        MT_DEBUG,
                        30,
                        caller,
                        "RDSFrame" );
                    if ( data_dt > 0 )
                    {
                        //-----------------------------------------------
                        // Some data was read so it needs to be processed
                        //-----------------------------------------------
                        postProcessFrame( data_start, data_dt );
                    }
                    m_stream->Close( false );
                    //---------------------------------------------------
                    // Start of next segment needs to be after data gap
                    //---------------------------------------------------
                    data_start = Exception.gps_start + Exception.dt;
                }
                if ( ( data_start >= user_start ) &&
                     ( data_start < user_stop ) )
                {
                    // Set data_dt to the size requested by the user
                    data_dt = m_options.SecondsPerFrame( );
                    QUEUE_LOG_MESSAGE( "Reset"
                                           << " data_dt: " << data_dt
                                           << std::endl
                                           << "\t[ Line: " << __LINE__
                                           << " File: " << __FILE__ << "]",
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "CreateRDS" );
                    continue;
                }
                else if ( data_start == user_stop )
                {
                    //---------------------------------------------------
                    // Set data_dt to the remainder of data
                    //---------------------------------------------------
                    data_dt = data_stop - data_start;
                    QUEUE_LOG_MESSAGE( "Reset"
                                           << " data_dt: " << data_dt
                                           << std::endl
                                           << "\t[ Line: " << __LINE__
                                           << " File: " << __FILE__ << "]",
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "CreateRDS" );
                }
                else if ( ( data_start < user_stop ) &&
                          ( ( data_start + data_dt ) > user_stop ) )
                {
                    // Set data_dt to the remainder of user data
                    data_dt = user_stop - data_start;
                    QUEUE_LOG_MESSAGE( "Reset"
                                           << " data_dt: " << data_dt
                                           << std::endl
                                           << "\t[ Line: " << __LINE__
                                           << " File: " << __FILE__ << "]",
                                       MT_DEBUG,
                                       30,
                                       caller,
                                       "CreateRDS" );
                }
            }
            catch ( FrameAPI::RDS::MissingChannel const& Exception )
            {
                data_start = Exception.gps_stop;
                data_dt = m_options.SecondsPerFrame( );
                QUEUE_LOG_MESSAGE( " MissingChannel Exception: "
                                       << Exception.what( ) //
                                       << " user_start: " << user_start //
                                       << " next data_start: " << data_start //
                                       << " next data_dt: " << data_dt //
                                       << " next frames_to_write: "
                                       << frames_to_write,
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "RDSFrame" );
            } // try/catch
        } // while

        //-----------------------------------------------------------------
        // Finish up
        //-----------------------------------------------------------------
        //----------------------------------------------------------------
        // Close all open input files
        //----------------------------------------------------------------
        m_p->Close( );
        //----------------------------------------------------------------
        // close out any pending requests
        //----------------------------------------------------------------
        m_stream->Close( true );
    }
    catch ( const SwigException& E )
    {
        //-----------------------------------------------------------------
        // Cleanup any temporary file
        //-----------------------------------------------------------------
        m_stream->Abandon( );
        //-----------------------------------------------------------------
        // Rethrow the exception
        //-----------------------------------------------------------------
        throw;
    }
    catch ( const std::exception& E )
    {
        //-----------------------------------------------------------------
        // Cleanup any temporary file
        //-----------------------------------------------------------------
        m_stream->Abandon( );
        //-----------------------------------------------------------------
        // Rethrow the exception
        //-----------------------------------------------------------------
        throw;
    }
    catch ( ... )
    {
        //-----------------------------------------------------------------
        // Cleanup any temporary file
        //-----------------------------------------------------------------
        m_stream->Abandon( );
        //-----------------------------------------------------------------
        // Rethrow the exception
        //-----------------------------------------------------------------
        throw;
    }
}

void
RDSFrame::processSimple( stream_type Output )
{
    static const char* caller = "RDSFrame::processSimple";

    const LDASTools::AL::GPSTime user_start( m_options.OutputTimeStart( ), 0 );
    const LDASTools::AL::GPSTime user_stop( m_options.OutputTimeEnd( ), 0 );

    LDASTools::AL::GPSTime data_start;
    LDASTools::AL::GPSTime data_stop;
    REAL_8                 data_dt;
    INT_4U                 frames_to_write( 0 );

    ChannelCacheEntry::fr_adc_data_type  adc;
    ChannelCacheEntry::fr_proc_data_type proc;

    //--------------------------------------------------------------------
    // Start processing the requests
    //--------------------------------------------------------------------
    try
    {
        //-----------------------------------------------------------------
        // Initialize
        //-----------------------------------------------------------------
        data_start = m_p->GetStart( );
        data_stop = m_p->GetStop( );

        rangeOptimizer( user_start, user_stop, data_start, data_stop );

        m_stream->UserRange( user_start.GetSeconds( ),
                             user_stop.GetSeconds( ) );

        QUEUE_LOG_MESSAGE( "data_start: " << data_start
                                          << " data_stop: " << data_stop,
                           MT_DEBUG,
                           30,
                           caller,
                           "RDSFrame" );

        if ( data_start != GPSTime( m_options.OutputTimeStart( ), 0 ) )
        {
            data_dt = GPSTime( m_options.OutputTimeStart( ), 0 ) - data_start;
        }
        else
        {
            data_dt = m_options.SecondsPerFrame( );
        }

        //-----------------------------------------------------------------
        // Create each frame requested by user
        //-----------------------------------------------------------------
        const GPSTime stop( ( stopRequest( ) == STOP_DATA ) ? data_stop
                                                            : user_stop );
        while ( data_start < stop )
        {
            QUEUE_LOG_MESSAGE(
                "data_start: " << data_start << " user_start: " << user_start
                               << " user_stop: " << user_stop
                               << " data_stop: " << data_stop
                               << " stop: " << stop << " data_dt: " << data_dt
                               << " frames_to_write: " << frames_to_write,
                MT_DEBUG,
                30,
                caller,
                "RDSFrame" );
            if ( data_start == GPSTime( m_options.OutputTimeStart( ), 0 ) )
            {
                // This is the case where the dt needs to be reset.
                data_dt = m_options.SecondsPerFrame( );
            }
            if ( data_start < user_stop )
            {
                //----------------------------------------------------------
                // Check to see if the data_dt needs to be adjusted
                //----------------------------------------------------------
                if ( ( data_start + data_dt ) > user_stop )
                {
                    data_dt = user_stop - data_start;
                }
            }
            else
            {
                //-----------------------------------------------------------
                // For the case where the data stops beyond the user's
                //   request, squeeze everything into a final frame.
                //-----------------------------------------------------------
                data_dt = ( data_stop - data_start );
            }
            if ( frames_to_write == 0 )
            {
                frames_to_write = m_options.FramesPerFile( );
                QUEUE_LOG_MESSAGE(
                    "data_start: " << data_start << " data_dt: " << data_dt
                                   << " frames_to_write: " << frames_to_write,
                    MT_DEBUG,
                    30,
                    caller,
                    "RDSFrame" );
                m_stream->Next( data_start, data_dt, frames_to_write );
                QUEUE_LOG_MESSAGE( "frames_to_write: " << frames_to_write,
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "RDSFrame" );
                if ( frames_to_write == 0 )
                {
                    QUEUE_LOG_MESSAGE(
                        "exiting loop", MT_DEBUG, 30, caller, "RDSFrame" );
                    break;
                }
            }

            //--------------------------------------------------------------
            // Make room for the new frame
            //--------------------------------------------------------------
            mResultFrame.reset( );
            //--------------------------------------------------------------
            // Add in channel information
            //--------------------------------------------------------------
            m_p->RetrieveChannels(
                mResultFrame, *this, data_start, data_dt, m_options );
            //--------------------------------------------------------------
            // Post process the concatinated data
            //--------------------------------------------------------------
            postProcessFrame( data_start, data_dt );
            //--------------------------------------------------------------
            // Advance to the next frame and set the length of the
            //   dt appropriately
            //--------------------------------------------------------------
            data_start += data_dt;
            if ( ( data_start >= user_start ) && ( data_start < user_stop ) )
            {
                // Set data_dt to the size requested by the user
                data_dt = m_options.SecondsPerFrame( );
                QUEUE_LOG_MESSAGE( "Reset"
                                       << " data_dt: " << data_dt << std::endl
                                       << "\t[ Line: " << __LINE__
                                       << " File: " << __FILE__ << "]",
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CreateRDS" );
                continue;
            }
            else if ( data_start == user_stop )
            {
                // Set data_dt to the remainder of data
                data_dt = data_stop - data_start;
                QUEUE_LOG_MESSAGE( "Reset"
                                       << " data_dt: " << data_dt << std::endl
                                       << "\t[ Line: " << __LINE__
                                       << " File: " << __FILE__ << "]",
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CreateRDS" );
            }
            else if ( ( data_start < user_stop ) &&
                      ( ( data_start + data_dt ) > user_stop ) )
            {
                // Set data_dt to the remainder of user data
                data_dt = user_stop - data_start;
                QUEUE_LOG_MESSAGE( "Reset"
                                       << " data_dt: " << data_dt << std::endl
                                       << "\t[ Line: " << __LINE__
                                       << " File: " << __FILE__ << "]",
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CreateRDS" );
            }
        }

        //-----------------------------------------------------------------
        // Finish up
        //-----------------------------------------------------------------
        //----------------------------------------------------------------
        // Close all open input files
        //----------------------------------------------------------------
        m_p->Close( );
        //----------------------------------------------------------------
        // close out any pending requests
        //----------------------------------------------------------------
        m_stream->Close( true );
    }
    catch ( const SwigException& E )
    {
        //-----------------------------------------------------------------
        // Cleanup any temporary file
        //-----------------------------------------------------------------
        m_stream->Abandon( );
        //-----------------------------------------------------------------
        // Rethrow the exception
        //-----------------------------------------------------------------
        throw;
    }
    catch ( const std::exception& E )
    {
        //-----------------------------------------------------------------
        // Cleanup any temporary file
        //-----------------------------------------------------------------
        m_stream->Abandon( );
        //-----------------------------------------------------------------
        // Rethrow the exception
        //-----------------------------------------------------------------
        throw;
    }
    catch ( ... )
    {
        //-----------------------------------------------------------------
        // Cleanup any temporary file
        //-----------------------------------------------------------------
        m_stream->Abandon( );
        //-----------------------------------------------------------------
        // Rethrow the exception
        //-----------------------------------------------------------------
        throw;
    }
}

void
RDSFrame::writeFrameToStream( )
{
    static const char* caller = "RDSFrame::writeFrameToStream";

    QUEUE_LOG_MESSAGE(
        "Writing frame"
            << " Start Time: " << mResultFrame->GetGTime( )
            << " Dt: " << mResultFrame->GetDt( ) << " with: "
            << ( ( mResultFrame->GetRawData( ) )
                     ? mResultFrame->GetRawData( )->RefFirstAdc( ).size( )
                     : 0 )
            << " FrAdcData structures"
            << " with: " << mResultFrame->RefProcData( ).size( )
            << " FrProcData structures"
            << " allow duplicate FrProcData names: "
            << mResultFrame->RefProcData( ).AllowDuplicates( ),
        MT_DEBUG,
        30,
        caller,
        "RDSFrame" );

    if ( ( mResultFrame->GetDt( ) > 0.0 ) &&
         ( mResultFrame->GetGTime( ).GetSeconds( ) > 0 ) )
    {
        m_stream->Write( mResultFrame,
                         m_options.CompressionMethod( ),
                         m_options.CompressionLevel( ),
                         ( ( m_options.CreateChecksumPerFrame( ) )
                               ? CheckSum::CRC
                               : CheckSum::NONE ) );
    }
}

//-----------------------------------------------------------------------------
//
//: Get a pointer to the specific FrProc channel.
//
// Caller must call 'rehash' on passed ProcData container to
// guarantee existence of channel map for the frame.
//
//! param: const std::string& name - Channel name.
//! param: const FrameCPP::FrameH::procData_type* proc - A pointer to the
//+       ProcData container for the frame.
//
//! return: FrameCPP::FrProcData* - A pointer to the channel object.
//
//! exc: Channel not found: name. - Specified channel is not found.
//
FrameCPP::FrProcData*
RDSFrame::getProcChannel( const std::string&                     name,
                          FrameCPP::FrameH::procData_type* const proc )
{
    if ( proc->size( ) == 0 )
    {
        ostringstream msg;
        msg << "Channel not found: " << name;

        throw SWIGEXCEPTION( msg.str( ) );
    }

    pair< FrameH::const_procData_hash_iterator,
          FrameH::const_procData_hash_iterator >
        p( proc->hashFind( name ) );

    if ( p.first == p.second )
    {
        ostringstream msg;
        msg << "Channel not found: " << name;

        throw SWIGEXCEPTION( msg.str( ) );
    }

    return p.first->second.get( );
}

//-----------------------------------------------------------------------------
//
//: Create new history record.
//
void
RDSFrame::createHistory( )
{
    if ( mResultFrame.get( ) == (FrameH*)NULL )
    {
        return;
    }
    if ( m_options.HistoryRecord( ) )
    {
        GPSTime gps_time;
        gps_time.Now( );

        string ldas_msg( "LDAS, Version: " );
        ldas_msg += PACKAGE_VERSION;

        // Insert record
        FrameCPP::FrameH::history_type::value_type h( new FrHistory(
            getHistoryName( ), gps_time.GetSeconds( ), ldas_msg ) );
        mResultFrame->RefHistory( ).append( h );

        //-----------------------------------------------------------------
        // Add record(s) relating to list of channels
        //-----------------------------------------------------------------
        string record;

        record.reserve( FrameCPP::Version::STRING::MAX_STRING_LENGTH );
        record.resize( 0 );
        record.append( "frameAPI: RDS (channels:" );

        HERE( );
        for ( auto& current : m_p->GetChannelCache( ) )
        {
            HERE( );
            if ( ( record.length( ) +
                   current.GetName( ).old_channel_name.length( ) + 2 ) >=
                 FrameCPP::Version::STRING::MAX_STRING_LENGTH )
            {
                //-----------------------------------------------------------
                // Need to output the record since it is full
                //-----------------------------------------------------------
                HERE( );
                record.append( ")" );

                FrameCPP::FrameH::history_type::value_type h( new FrHistory(
                    mHistoryName, gps_time.GetSeconds( ), record ) );
                mResultFrame->RefHistory( ).append( h );
                record.resize( 0 );
                record.append( "frameAPI: RDS (channels (cont):" );
            }
            HERE( );
            record.append( " " );
            record.append( current.GetName( ).old_channel_name );
        }
        //-----------------------------------------------------------------
        // Write the final record of channel name information
        //-----------------------------------------------------------------
        HERE( );
        record.append( ")" );
        {
            HERE( );
            FrameCPP::FrameH::history_type::value_type h(
                new FrHistory( mHistoryName, gps_time.GetSeconds( ), record ) );
            mResultFrame->RefHistory( ).append( h );
        }
    }
    //--------------------------------------------------------------------
    HERE( );
    return;
}

//-----------------------------------------------------------------------------
//
//: Get history name for new records.
//
const std::string&
RDSFrame::getHistoryName( )
{
    return mHistoryName;
}

void
RDSFrame::init( const frame_file_container_type& FrameFiles,
                const channel_container_type&    Channels )
{
    HERE( );
    auto local_data = boost::make_unique< private_type >( );
    //-------------------------------------------------------------------
    // Organize the frame files into groups and arange in assending
    //   time order
    //-------------------------------------------------------------------
    {
        //----------------------------------------------------------------
        // Have local scoping for parser.
        //----------------------------------------------------------------
        INT_4U rds_level;

        HERE( );
        rds_level = local_data->InitFileCache( m_options, FrameFiles );
        if ( m_options.RDSLevel( ) > 0 )
        {
            HERE( );
            if ( rds_level >= m_options.RDSLevel( ) )
            {
                HERE( );
                std::ostringstream msg;
                msg << "For the given input, the user specified RDS level "
                       "of "
                    << m_options.RDSLevel( )
                    << " does not exceed the minimum level of " << rds_level
                    << ".";
                throw SWIGEXCEPTION( msg.str( ) );
            }
        }
        else
        {
            HERE( );
            m_options.RDSLevel( ++rds_level );
        }

        if ( local_data->GetFileCache( ).size( ) <= 0 )
        {
            HERE( );
            throw SWIGEXCEPTION( "List of frame files is empty." );
        }
    }

    //--------------------------------------------------------------------
    // Check if either the number of frames per file or the seconds per
    //   frame are zero.
    //--------------------------------------------------------------------
    HERE( );
    if ( m_options.FramesPerFile( ) == 0 )
    {
        HERE( );
        m_options.FramesPerFile(
            local_data->GetFileCache( ).GetFramesPerFile( ) );
    }
    HERE( );
    if ( m_options.SecondsPerFrame( ) == 0 )
    {
        HERE( );
        m_options.SecondsPerFrame(
            local_data->GetFileCache( ).GetSecondsPerFrame( ) );
    }
    HERE( );
    if ( ( m_options.AllowShortFrames( ) == false ) &&
         ( ( ( m_options.OutputTimeEnd( ) - m_options.OutputTimeStart( ) ) %
             ( m_options.SecondsPerFrame( ) * m_options.FramesPerFile( ) ) ) !=
           0 ) )
    {
        HERE( );
        std::ostringstream msg;
        msg << "The choice of " << m_options.FramesPerFile( )
            << " frame(s) per file"
            << " each " << m_options.SecondsPerFrame( )
            << " second(s) in length"
            << " will yield a final frame file which is shorter in length "
               "than "
               "the rest";
        throw SWIGEXCEPTION( msg.str( ) );
    }
    //--------------------------------------------------------------------
    // Generate a list of channels
    //--------------------------------------------------------------------
    HERE( );
    {
        //----------------------------------------------------------------
        // Have local scoping for parser.
        //----------------------------------------------------------------
        HERE( );
        local_data->InitChannelCache( Channels );

        if ( local_data->GetChannelCache( ).empty( ) )
        {
            HERE( );
            throw SWIGEXCEPTION( "List of channels is empty." );
        }

        //-----------------------------------------------------------------
        // Create a list of IFOs using the first character of each channel
        //   name
        //-----------------------------------------------------------------
        HERE( );
        for ( auto& current : local_data->GetChannelCache( ) )
        {
            char ifo( ( current.GetName( ) ).old_channel_name[ 0 ] );
            bool added( false );

            HERE( );
            for ( std::string::iterator current_ifo( m_ifo_list.begin( ) ),
                  end_ifo( m_ifo_list.end( ) );
                  current_ifo != end_ifo;
                  current_ifo++ )
            {
                if ( *current_ifo == ifo )
                {
                    HERE( );
                    added = true;
                    break;
                }
                else if ( ifo < *current_ifo )
                {
                    HERE( );
                    m_ifo_list.insert( current_ifo, ifo );
                    added = true;
                    break;
                }
            }
            if ( added == false )
            {
                HERE( );
                m_ifo_list += ifo;
            }
        }
    }
    HERE( );
    m_p = local_data.release( );
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
RDSFrame::private_type::private_type( )
    : m_buffer_size( FrameAPI::StreamBufferSize )
{
    if ( m_buffer_size ==
         FrameCPP::Common::FrameBufferInterface::M_BUFFER_SIZE_SYSTEM )
    {
        // Do Nothing
    }
    else if ( m_buffer_size > 0 )
    {
        m_buffer.reset( new char[ m_buffer_size ] );
    }
}

namespace FrameAPI
{
    namespace RDSFrameCC
    {
        //=================================================================
        //=================================================================
        ChannelEntry::ChannelEntry( const channel_name_type& Name )
            : m_name( Name )
        {
        }
        //=================================================================
        //=================================================================
        ChannelEntry::ChannelEntry( const ChannelEntry& Source )
            : m_name( Source.m_name ), m_adc( Source.m_adc ),
              m_proc( Source.m_proc )
        {
        }
    } // namespace RDSFrameCC
} // namespace FrameAPI

namespace
{
    CollectNames::~CollectNames( )
    {
    }

    void
    CollectNames::operator( )( const channel_name_type& ChannelName )
    {
        push_back( ChannelName );
    }
} // namespace
