//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "frameutils_config.h"

#include <memory>
#include <stdexcept>

#include "framecpp/Common/FrameBuffer.hh"

#include "framecpp/FrTOC.hh"

#include "frameAPI/Catalog.hh"
#include "frameAPI/Channel.hh"
#include "frameAPI/createRDS.hh"
#include "frameAPI/Frame.hh"

namespace FrameAPI
{
    Catalog::ChannelMetaData::ChannelMetaData( )
    {
    }

    Catalog::ChannelMetaData::ChannelMetaData( const std::string& Name )
        : _name( Name )
    {
    }

    Catalog::ChannelMetaData::ChannelMetaData( const ChannelMetaData& Source )
        : _name( Source._name )
    {
    }

    void
    Catalog::DataDictionary::Push( channel_type Channel )
    {
        channels[ Channel->GetName( ) ] = Channel;
    }

    Catalog::DataDictionary::channel_type Catalog::DataDictionary::
                                          operator[]( const channel_name_type& Channel ) const
    {
        channels_type::const_iterator cur( channels.find( Channel ) );
        if ( cur != channels.end( ) )
        {
            return cur->second;
        }
        throw std::range_error( "Unable to find requested channel" );
    }

    Catalog::stream::stream( )
    {
    }

    Catalog::stream::stream( const stream& Source )
    {
    }

    Catalog::stream::stream( const std::string& Source )
    {
        //-------------------------------------------------------------------
        // Allocate space the buffer
        //-------------------------------------------------------------------
        typedef FrameCPP::Common::FrameBuffer< LDASTools::AL::filebuf >
                                       buffer_type;
        std::unique_ptr< buffer_type > buffer(
            new buffer_type( std::ios::in ) );
        buffer->open( Source, std::ios::in );

        istream.reset( new istream_type::element_type( buffer.release( ) ) );

        //-------------------------------------------------------------------
        // Calculate the start and duration of the frames in the stream
        //-------------------------------------------------------------------
        const FrameCPP::Common::FrTOC* toc( istream->GetTOC( ) );
        const nFrame_type              nframes( toc->nFrame( ) );

        start( start_type( toc->GTimeS( )[ 0 ], toc->GTimeN( )[ 0 ] ) );
        end( start_type( toc->GTimeS( )[ nframes - 1 ],
                         toc->GTimeN( )[ nframes - 1 ] ) +
             toc->dt( )[ nframes - 1 ] );
        dt = end( ) - start( );
    }

    void
    Catalog::stream::close( )
    {
        /// \todo Need to close the stream to prevent resource leak
        /* istream->Close( ); */
    }

    const FrameCPP::Common::FrTOC*
    Catalog::stream::toc( ) const
    {
        return istream->GetTOC( );
    }

    Catalog::Catalog( )
        : start_set( false ), end_set( false ),
          channels( new channel_dict_type::element_type )
    {
    }

    Catalog::Catalog( const stream_source_type& Sources )
        : start_set( false ), end_set( false ),
          channels( new channel_dict_type::element_type )
    {
        Open( Sources );
    }

    Catalog::data_dict_type
    Catalog::Fetch( gps_seconds_type              Start,
                    gps_seconds_type              Stop,
                    const channel_container_type& Channels )
    {
        data_dict_type retval( new data_dict_type::element_type( ) );

        //-------------------------------------------------------------------
        // Reclaim resouces before requiring more
        //-------------------------------------------------------------------
        close_streams( );
        //-------------------------------------------------------------------
        // Get the channels
        //-------------------------------------------------------------------
        frame_file_container_type files;

        for ( stream_set_type::const_iterator cur = streams.begin( ),
                                              last = streams.end( );
              cur != last;
              ++cur )
        {
            files.push_back( cur->first );
        }

        Frame output_frame( createRDSFrame( files, Start, Stop, Channels ) );

        for ( channel_container_type::channel_type::const_iterator
                  cur = Channels.names.begin( ),
                  last = Channels.names.end( );
              cur != last;
              ++cur )
        {
            retval->Push( output_frame.GetChannel( cur->old_channel_name ) );
        }

        return retval;
    }

    void
    Catalog::Open( const stream_source_type& Streams )
    {
        for ( stream_source_type::const_iterator cur = Streams.begin( ),
                                                 last = Streams.end( );
              cur != last;
              ++cur )
        {
            if ( streams.find( *cur ) == streams.end( ) )
            {
                streams[ *cur ] = stream( *cur );
                const FrameCPP::FrTOC* toc(
                    dynamic_cast< const FrameCPP::FrTOC* >(
                        streams[ *cur ].toc( ) ) );
                if ( toc )
                {
                    //-------------------------------------------------------------
                    // Loop over the FrAdcData referenced in the table of
                    // contents
                    //-------------------------------------------------------------
                    for ( FrameCPP::FrTOC::MapADC_type::const_iterator
                              cur_adc = toc->GetADC( ).begin( ),
                              last_adc = toc->GetADC( ).end( );
                          cur_adc != last_adc;
                          ++cur_adc )
                    {
                        ( *channels )[ cur_adc->first ] =
                            channel_dict_type::element_type::mapped_type(
                                cur_adc->first );
                    }
                    //-------------------------------------------------------------
                    // Loop over the FrProcData referenced in the table of
                    // contents
                    //-------------------------------------------------------------
                    for ( FrameCPP::FrTOC::MapProc_type::const_iterator
                              cur_proc = toc->GetProc( ).begin( ),
                              last_proc = toc->GetProc( ).end( );
                          cur_proc != last_proc;
                          ++cur_proc )
                    {
                        ( *channels )[ cur_proc->first ] =
                            channel_dict_type::element_type::mapped_type(
                                cur_proc->first );
                    }
                    //-------------------------------------------------------------
                    // Loop over the FrSerData referenced in the table of
                    // contents
                    //-------------------------------------------------------------
                    for ( FrameCPP::FrTOC::MapSer_type::const_iterator
                              cur_ser = toc->GetSer( ).begin( ),
                              last_ser = toc->GetSer( ).end( );
                          cur_ser != last_ser;
                          ++cur_ser )
                    {
                        ( *channels )[ cur_ser->first ] =
                            channel_dict_type::element_type::mapped_type(
                                cur_ser->first );
                    }
                    //-------------------------------------------------------------
                    // Loop over the FrSimData referenced in the table of
                    // contents
                    //-------------------------------------------------------------
                    for ( FrameCPP::FrTOC::MapSim_type::const_iterator
                              cur_sim = toc->GetSim( ).begin( ),
                              last_sim = toc->GetSim( ).end( );
                          cur_sim != last_sim;
                          ++cur_sim )
                    {
                        ( *channels )[ cur_sim->first ] =
                            channel_dict_type::element_type::mapped_type(
                                cur_sim->first );
                    }
                }
            }
            start_type s = streams[ *cur ].start( );
            end_type   e = streams[ *cur ].end( );
            //-----------------------------------------------------------------
            // Determine if the start or end need to be modified.
            //-----------------------------------------------------------------
            if ( ( !start_set ) || ( s < start ) )
            {
                start_set = true;
                start = s;
            }
            if ( ( !end_set ) || ( e > end ) )
            {
                end_set = true;
                end = e;
            }
        }
    }

    void
    Catalog::close_streams( )
    {
        for ( stream_set_type::iterator cur = streams.begin( ),
                                        last = streams.end( );
              cur != last;
              ++cur )
        {
            cur->second.close( );
        }
    }

} // namespace FrameAPI
