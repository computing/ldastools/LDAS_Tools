//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include "genericAPI/Logging.hh"

#include "createRDS.hh"
#include "Frame.hh"
#include "rdsframe.hh"
#include "rdsreduce.hh"
#include "rdsresample.hh"
#include "RDSStreamFile.hh"
#include "RDSStreamMemory.hh"

using GenericAPI::IsLogging;
using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;

#if 0
#define DBGMSG( a ) std::cerr << a << __FILE__ << " " << __LINE__ << std::endl
#else
#define DBGMSG( a )
#endif

#define AT( a ) DBGMSG( a << " " )
#define HERE( ) DBGMSG( "" )

namespace
{
}

namespace FrameAPI
{
    void
    channel_container_type::ParseChannelName(
        std::string const& ChannelNameString )
    {
        static const char* caller =
            "FrameAPI::channel_container_::ParseChannelName";
        std::list< std::string > channel_names;

        if ( boost::filesystem::exists( ChannelNameString ) )
        {
            //-----------------------------------------------------------
            // Read channel name descriptors from a filesystem
            //
            // channel_name_descriptor =>
            //    <old_channel_name> = <new_channel_name>
            //    | <old_channel_name>
            // <old_channel_name> => <channel_name> ! <resample_factor>
            //    | <chanel_name>
            // <new_channel_name> => <channel_name>
            // <resample_factor => 1 | 2 | 4 | 8 | 16
            //-----------------------------------------------------------
            channel_name_file_type< channel_container_type > channel_name_file(
                *this );
            channel_name_file.Read( ChannelNameString );
            channel_names.insert( channel_names.end( ),
                                  input_channels.begin( ),
                                  input_channels.end( ) );
        }
        else
        {
            channel_names.push_back( ChannelNameString );
        }

        for ( auto channel_name : channel_names )
        {
            RDSFrame::channel_name_type::ParseChannelName(
                channel_name, names, resampling );
            QUEUE_LOG_MESSAGE( "name: " << names.back( ).old_channel_name
                                        << " sampleRate: " << resampling.back( )
                                        << " rename: "
                                        << names.back( ).new_channel_name,
                               MT_DEBUG,
                               30,
                               caller,
                               "CMD_CREATE_RDS" );
        }
    }

    void
    channel_container_type::ParseChannelNames(
        std::string const& ChannelNamesString )
    {
        std::vector< std::string > split_channel_names;

        boost::algorithm::split( split_channel_names,
                                 ChannelNamesString,
                                 boost::is_any_of( "\t, " ),
                                 boost::token_compress_on );
        if ( !split_channel_names.empty( ) )
        {
            names.reserve( split_channel_names.size( ) );
            resampling.reserve( split_channel_names.size( ) );
            for ( auto channel_name : split_channel_names )
            {
                ParseChannelName( channel_name );
            }
        }
    }

    void
    channel_container_type::operator( )( std::string const& OldName,
                                         std::string const& Resampling,
                                         std::string const& NewName )
    {
        std::string channel( OldName );

        if ( Resampling != "1" )
        {
            channel += "!" + Resampling;
        }
        if ( NewName != OldName )
        {
            channel += "=" + NewName;
        }
        input_channels.push_back( channel );
    }

    Frame
    createRDSFrame( const frame_file_container_type& FrameFiles,
                    start_type                       Start,
                    end_type                         End,
                    const channel_container_type&    Channels )
    {
        RDS::Options opts( Start, // Start Time
                           End, // End Time
                           "__none__", // Output Type
                           "", // Compression - raw
                           0, // Compression - level
                           false, // Verify Checksum
                           false, // Verify Frame Checksum
                           false, // Verify Time Range
                           false, // Verify Data Valid
                           1, // FramesPerFile
                           End - Start, // SecondsPerFrame
                           true, // AllowShortFrames
                           false, // GenerateFrameChecksum
                           true, // FillMissingDataValidArray
                           false ); // VerifyFilenameMetadata

        auto  output = boost::make_shared< RDSStreamMemory >( );
        Frame retval;

        ReduceRawFrame request( FrameFiles, // Filename list
                                Channels.names, // Channel list
                                opts );
        request.ProcessRequest( output );
        if ( output )
        {
            retval = output->Results( )[ 0 ];
        }

        return retval;
    }

    void
    createRDSSet( const frame_file_container_type&  FrameFiles,
                  const channel_container_type&     Channels,
                  const FrameAPI::RDS::FileOptions& Opts )
    {
        static const char* caller = "FrameAPI::createRDSSet";

        bool need_to_resample = false;

        HERE( );
        for ( auto resampling : Channels.resampling )
        {
            if ( resampling != 1 )
            {
                QUEUE_LOG_MESSAGE(
                    "Have channel requiring resampling: " << resampling,
                    MT_DEBUG,
                    40,
                    caller,
                    "RDSFrame" );
                need_to_resample = true;
                break;
            }
        }
        HERE( );
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        try
        {
            auto stream = boost::make_shared< RDSStreamFile >(
                Opts.DirectoryOutputFrames( ),
                Opts.OutputType( ).c_str( ), // OutputType
                Opts.DirectoryOutputMD5Sum( ),
                Opts.FramesPerFile( ) );
            if ( need_to_resample )
            {
                if ( IsLogging( LogEntryGroup_type::MT_DEBUG, 20 ) )
                {
                    std::ostringstream msg;

                    HERE( );
                    queueLogEntry( "Resampling",
                                   LogEntryGroup_type::MT_DEBUG,
                                   20,
                                   caller,
                                   "CREATE_RDS" );
                }
                //---------------------------------------------------------------
                // Resample
                //---------------------------------------------------------------
                ResampleRawFrame::resample_container_type resampling(
                    ResampleRawFrame::Resample( Channels.names,
                                                Channels.resampling ) );
                ResampleRawFrame request( FrameFiles, // Filename list
                                          Channels.names, // Channel list
                                          resampling, // Resampling
                                          Opts );
                request.ProcessRequest( stream );
            }
            else
            {
                //---------------------------------------------------------------
                // Reduction
                //---------------------------------------------------------------
                HERE( );

                if ( IsLogging( LogEntryGroup_type::MT_DEBUG, 20 ) )
                {
                    HERE( );
                    queueLogEntry( "Reducing",
                                   LogEntryGroup_type::MT_DEBUG,
                                   20,
                                   caller,
                                   "CREATE_RDS" );
                }
                //---------------------------------------------------------------
                // Reduce
                //---------------------------------------------------------------
                HERE( );
                ReduceRawFrame request( FrameFiles, // Filename list
                                        Channels.names, // Channel list
                                        Opts );
                if ( IsLogging( LogEntryGroup_type::MT_DEBUG, 20 ) )
                {
                    queueLogEntry( "Reducing: Created instance",
                                   LogEntryGroup_type::MT_DEBUG,
                                   20,
                                   caller,
                                   "CREATE_RDS" );
                }
                HERE( );
                request.ProcessRequest( stream );
                HERE( );
                if ( IsLogging( LogEntryGroup_type::MT_DEBUG, 20 ) )
                {
                    queueLogEntry( "Reducing: Processed Request",
                                   LogEntryGroup_type::MT_DEBUG,
                                   20,
                                   caller,
                                   "CREATE_RDS" );
                }
            }
        }
        catch ( const std::exception& E )
        {
            HERE( );
            if ( IsLogging( LogEntryGroup_type::MT_DEBUG, 0 ) )
            {
                std::ostringstream msg;

                msg << "Caught excpetion: " << E.what( );
                queueLogEntry( msg.str( ),
                               LogEntryGroup_type::MT_DEBUG,
                               0,
                               caller,
                               "CREATE_RDS" );
            }
            HERE( );
            throw;
        }
        HERE( );
    }
} // namespace FrameAPI
