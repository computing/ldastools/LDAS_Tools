//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

#include <algorithm>
#include <sstream>
#include <memory>

// GenericAPI Header Files
#include "genericAPI/swigexception.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"

// Local Header Files
#include "rdsreduce.hh"
#include "filereader.hh"
#include "rdsutil.hh"

using namespace std;
using namespace FrameCPP::Version;
using LDASTools::AL::GPSTime;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ReduceRawFrame::ReduceRawFrame( const char*              frame_files,
                                const char*              channels,
                                const RDSFrame::Options& UserOptions )
    : RDSFrame( frame_files, channels, UserOptions )
{
    QUEUE_LOG_MESSAGE( "Using ReduceRawFrame algorithm",
                       MT_DEBUG,
                       30,
                       "ReduceRawFrame::ReduceRawFrame_1",
                       "CreateRDS" );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ReduceRawFrame::ReduceRawFrame( const frame_file_container_type& frame_files,
                                const channel_container_type&    channels,
                                const RDSFrame::Options&         UserOptions )
    : RDSFrame( frame_files, channels, UserOptions )
{
    QUEUE_LOG_MESSAGE( "Using ReduceRawFrame algorithm",
                       MT_DEBUG,
                       30,
                       "ReduceRawFrame::ReduceRawFrame_2",
                       "CreateRDS" );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ReduceRawFrame::~ReduceRawFrame( )
{
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void
ReduceRawFrame::processChannel( fr_adc_data_type Adc )
{
    if ( Adc.get( ) && mResultFrame.get( ) && mResultFrame->GetRawData( ) )
    {
        QUEUE_LOG_MESSAGE(
            "Adding Channel: "
                << Adc->GetName( ) << " to frame: " << mResultFrame->GetName( )
                << " containing: "
                << mResultFrame->GetRawData( )->RefFirstAdc( ).size( )
                << " channel(s)",
            MT_DEBUG,
            30,
            "ReduceRawFrame::processChannel",
            "CreateRDS" );
        // Append adc to current frame
        mResultFrame->GetRawData( )->RefFirstAdc( ).append( Adc );
    }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void
ReduceRawFrame::processChannel( fr_proc_data_type Proc )
{
    if ( Proc.get( ) && mResultFrame.get( ) )
    {
        QUEUE_LOG_MESSAGE(
            "Adding Channel(FrProc): '"
                << Proc->GetName( ) << "'"
                << " to frame: " << mResultFrame->GetName( ) << " containing: "
                << mResultFrame->RefProcData( ).size( ) << " channel(s)"
                << " [contains: " << Proc->GetName( ) << " - "
                << ( mResultFrame->RefProcData( ).find( Proc->GetName( ) ) !=
                     mResultFrame->RefProcData( ).end( ) )
                << "]",
            MT_DEBUG,
            30,
            "ReduceRawFrame::processChannel",
            "CreateRDS" );
        // Append deep copy of existing channel
        mResultFrame->RefProcData( ).append( Proc );
    }
}

void
ReduceRawFrame::rangeOptimizer( const time_type& UserStart,
                                const time_type& UserStop,
                                time_type&       DataStart,
                                time_type&       DataStop ) const
{
    if ( DataStart < UserStart )
    {
        DataStart = UserStart;
    }
    if ( DataStop > UserStop )
    {
        DataStop = UserStop;
    }
}

ReduceRawFrame::stop_request_type
ReduceRawFrame::stopRequest( ) const
{
    return STOP_USER;
}

void
ReduceRawFrame::writeFrameToStream( )
{
    if ( ( mResultFrame->GetGTime( ).GetSeconds( ) <
           m_options.OutputTimeStart( ) ) ||
         ( m_options.OutputTimeEnd( ) <
           mResultFrame->GetGTime( ).GetSeconds( ) ) )
    {
        return;
    }
    RDSFrame::writeFrameToStream( );
}
