//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef CONDITION_DATA_HH
#define CONDITION_DATA_HH

#include <list>
#include <memory>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"
#include "ldastoolsal/gpstime.hh"

#include "filters/Resample.hh"

#include "framecpp/Types.hh"
#include "framecpp/FrameH.hh"
#include "framecpp/FrProcData.hh"

//! ignore_begin:

namespace
{
    struct frame_group_info_type;
}

class FileReader;

//! ignore_end:

namespace FrameAPI
{
    //---------------------------------------------------------------------
    //: Create a frame group ilwd
    //
    // This class takes in a list of frame files and channels and
    // produces an frame_group ilwd.
    //---------------------------------------------------------------------
    class ConditionData
    {
    public:
        //-------------------------------------------------------------------
        //: Channel type
        //
        // This enumerated type specifies the type of channel
        //-------------------------------------------------------------------
        enum data_type
        {
            // Raw data
            ADC,
            // Processed data
            PROC,
            // Simulated data
            SIM,
            // Unknown data
            UNKNOWN_DATA
        };

        //-------------------------------------------------------------------
        //: Channel input description
        //
        // This class stores description of channels to extract from the
        // frame files.
        //-------------------------------------------------------------------
        struct channel_input_type
        {
        public:
            enum
            {
                NAME,
                INDEX
            } s_mode;
            // Name of the channel (ex: H2:LSC-AS_Q )
            std::string s_name;

            INT_4U s_index;
            // Down sampling value ( 1, 2, 4, 8, and 16 are currently supported
            // ) The value of 1 means no resampling
            int s_q;
            // The type of Channel
            data_type s_data_type;
            // The offset in the channel<br>
            //:TODO: In the future this should be the offset relative to the<br>
            //:TODO:   start of the frame
            REAL_8 m_offset;
            // The length of the result
            REAL_8 m_delta;
            // Did the user request slicing
            bool m_slice_specified;

            //: Constructor
            channel_input_type( );
            //: Copy Constructor
            channel_input_type( const channel_input_type& Source );
        };

        //-------------------------------------------------------------------
        // Type specifier for the list of input frame files
        //-------------------------------------------------------------------
        typedef std::list< std::string > frame_files_type;

        //-------------------------------------------------------------------
        // Type specifier for continuous segments of data
        //-------------------------------------------------------------------
        class continuous_segment_info_type
        {
        public:
            LDASTools::AL::GPSTime m_start;
            REAL_8                 m_dt;

            inline continuous_segment_info_type( )
                : m_start( ), m_dt( 0.0 ),
                  m_base( static_cast< FrameCPP::Base* >( NULL ) ),
                  m_frameh( static_cast< FrameCPP::FrameH* >( NULL ) )
            {
            }

            inline continuous_segment_info_type(
                FrameCPP::Base*               Base,
                const LDASTools::AL::GPSTime& Start,
                REAL_8                        Delta )
                : m_start( Start ), m_dt( Delta ), m_base( Base ),
                  m_frameh( static_cast< FrameCPP::FrameH* >( NULL ) )
            {
            }

            inline continuous_segment_info_type(
                const continuous_segment_info_type& Source )
                : m_start( Source.m_start ), m_dt( Source.m_dt ),
                  m_base( Source.m_base ),
                  m_frameh(
                      const_cast< continuous_segment_info_type& >( Source )
                          .m_frameh )
            {
            }

            inline const continuous_segment_info_type&
            operator=( const continuous_segment_info_type& Source )
            {
                m_base = Source.m_base;
                m_start = Source.m_start;
                m_dt = Source.m_dt;
                m_frameh = const_cast< continuous_segment_info_type& >( Source )
                               .m_frameh;
                return *this;
            }

            inline FrameCPP::Base*
            GetBase( ) const
            {
                return m_base;
            }

            inline FrameCPP::FrameH*
            ReleaseFrameH( ) const
            {
                FrameCPP::FrameH* retval( m_frameh );
                m_frameh = static_cast< FrameCPP::FrameH* >( NULL );
                return retval;
            }

            inline void
            ResetBase( FrameCPP::Base* NewBase =
                           static_cast< FrameCPP::Base* >( NULL ) ) const
            {
                delete m_base;
                m_base = NewBase;
            }

            inline void
            ResetFrameH( FrameCPP::FrameH* NewFrameH =
                             static_cast< FrameCPP::FrameH* >( NULL ) ) const
            {
                // delete m_frameh;
                m_frameh = NewFrameH;
            }

        private:
            mutable FrameCPP::Base*   m_base;
            mutable FrameCPP::FrameH* m_frameh;
        };

        typedef std::list< continuous_segment_info_type >
            continuous_segment_type;

        //-------------------------------------------------------------------
        //: Constructor
        //
        // This constructor initializes the class with a list of frame files
        //   and a list of channel request. Data validation of the input
        //   parameters is performed.
        //! param: const frame_files_type& FrameFiles - a list of fully
        //+		qualified frame file names.
        //! param: const std::list< channel_input_type>& Channels - List of
        //+		channels as described by struct channel_input_type
        //! param: bool Concatination - true if continuous segments should
        //		be concatinated together.
        //-------------------------------------------------------------------
        ConditionData( const frame_files_type&                FrameFiles,
                       const std::list< channel_input_type >& Channels,
                       bool Concatination = true );
        //-------------------------------------------------------------------
        //: Desstructor
        //
        // Release system resources
        //-------------------------------------------------------------------
        ~ConditionData( );

    protected:
        class channel_info_type;
        friend class channel_info_type;

        struct segment_info_type
        {
            boost::shared_ptr< FrameCPP::Base > m_segment;
            LDASTools::AL::GPSTime              m_start;
            double                              m_dt;

            segment_info_type( );
            segment_info_type( const segment_info_type& Source );
            ~segment_info_type( );
            const segment_info_type&
            operator=( const segment_info_type& Source );

            inline FrameCPP::FrameH*
            GetFrameH( )
            {
                return m_frameh;
            }

            inline FrameCPP::FrameH*
            ReleaseFrameH( )
            {
                FrameCPP::FrameH* retval( m_frameh );
                m_frameh = static_cast< FrameCPP::FrameH* >( NULL );
                return retval;
            }

            inline void
            ResetFrameH( FrameCPP::FrameH* NewFrameH =
                             static_cast< FrameCPP::FrameH* >( NULL ) )
            {
                // delete m_frameh;
                m_frameh = NewFrameH;
            }

        private:
            FrameCPP::FrameH* m_frameh;
        };

        typedef std::list< FrameCPP::FrDetector* >   detector_list_type;
        typedef std::list< FrameCPP::FrHistory* >    history_list_type;
        typedef std::list< segment_info_type >       segment_type;
        typedef std::list< continuous_segment_type > frame_data_groups_type;
        typedef std::list< FrameCPP::FrameH* >       frame_list_type;

        enum
        {
            MODE_FRAME = 0x0001
        };

        class channel_info_type
        {
        public:
            channel_input_type m_info;
            segment_type       m_data;
            // List of FrameCPP objects that will become the m_data
            frame_data_groups_type m_frame_data_groups;
            // The projected start time of the data set
            LDASTools::AL::GPSTime m_start;
            // The projected end time of the data set
            LDASTools::AL::GPSTime m_end;
            // Type of output
            data_type m_output_data_type;
            // The start time of the first frame
            LDASTools::AL::GPSTime m_first_frame_start;

            //: Constructor
            channel_info_type( );
            channel_info_type( const channel_input_type& ChannelInfo );

            //: Destructor
            ~channel_info_type( );

            template < class channel_type >
            void reduce( const channel_type*           Channel,
                         const LDASTools::AL::GPSTime& Start,
                         const FrameCPP::FrameH*       Frame,
                         const LDASTools::AL::GPSTime& ChannelStart,
                         const LDASTools::AL::GPSTime& ChannelEnd,
                         const REAL_8                  StartFrequency,
                         const REAL_8                  DeltaFrequency,
                         const bool                    Concatinate,
                         FrameCPP::FrameH*             FramePtr );

            template < class channel_type,
                       class data_container_type,
                       class data_iterator_type >
            FrameCPP::FrProcData*
            resample( const channel_type&           Channel,
                      data_container_type&          Container,
                      data_iterator_type            Begin,
                      data_iterator_type            End,
                      const LDASTools::AL::GPSTime& FrameStart,
                      LDASTools::AL::GPSTime&       SegmentStart );

        private:
            Filters::ResampleBase* m_resampler;
        };
        typedef std::vector< channel_info_type > channels_type;

        detector_list_type m_detectors;
        history_list_type  m_history;

        channels_type m_channels;
        //-------------------------------------------------------------------
        //: Do most of the work
        //-------------------------------------------------------------------
        void eval( );

        template < class Func >
        void
        foreachFrame( Func Op )
        {
            for ( frame_list_type::iterator f( m_frames.begin( ) );
                  f != m_frames.end( );
                  f++ )
            {
                Op( *( *f ) );
            }
        }

        const LDASTools::AL::GPSTime& getMin( );

        const LDASTools::AL::GPSTime& getMax( );

        frame_list_type releaseFrames( );

        void setMode( int Mode );

    private:
        frame_list_type  m_frames;
        frame_files_type m_frame_files;
        // True if continuous segments should be concatinated together
        bool m_concatinate;

        LDASTools::AL::GPSTime m_min;
        LDASTools::AL::GPSTime m_max;

        int m_mode;

        void process_frame( INT_4U                 Frame,
                            FileReader&            Reader,
                            frame_group_info_type& FrameGroupInfo );

        void process_frame_file( const std::string&     InputFile,
                                 frame_group_info_type& FrameGroupInfo );
    };

    inline const LDASTools::AL::GPSTime&
    ConditionData::getMin( )
    {
        return m_min;
    }

    inline const LDASTools::AL::GPSTime&
    ConditionData::getMax( )
    {
        return m_max;
    }

    inline void
    ConditionData::setMode( int Mode )
    {
        m_mode = Mode;
    }

} // namespace FrameAPI

#endif /* CONDITION_DATA_HH */
