//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <string>
#include <utility>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/range/irange.hpp>
#include <boost/regex.hpp>

#include "ldastoolsal/MemChecker.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrTOC.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrTable.hh"
#include "framecpp/FrVect.hh"
#include "framecpp/GPSTime.hh"
#include "framecpp/IFrameStream.hh"
#include "framecpp/OFrameStream.hh"
#include "framecpp/FrTOC.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "frameAPI/filereader.hh"

#include "CalibrationVector.hh"
#include "Intervals.hh"

using LDASTools::AL::MemChecker;

using GenericAPI::IsLogging;
using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;

typedef boost::program_options::options_description options_type;

class CommandLine
{
public:
    static constexpr int DEFAULT_DEBUG_LEVEL = 0;

    CommandLine( int ArgC, char** ArgV );

    void operator( )( );

    int AnalysisReadyMask( ) const;

    std::string const& AnalysisReadyChannelName( ) const;

    bool Delta( ) const;

    std::string const& GlobPattern( ) const;

    std::string const& IFO( ) const;

    bool SingleEntryPerLine( ) const;

    void Usage( int ExitStatus ) const;

private:
    options_type                          options_visible;
    char* const                           program_name;
    std::string                           ifo;
    std::string                           analysis_ready_channel_name;
    int                                   analysis_ready_mask;
    std::string                           glob_pattern;
    boost::program_options::variables_map options_variable_map;
};

CommandLine::CommandLine( int ArgC, char** ArgV )
    : options_visible( "Allowed options" ), program_name( ArgV[ 0 ] )
{
    //-------------------------------------------------------------------
    // Options visible to help
    //-------------------------------------------------------------------
    options_visible.add_options( ) //
        ( "help,h", "Print help message and exit" ) //
        ( "analysis-ready-channel,c",
          boost::program_options::value< std::string >(
              &analysis_ready_channel_name )
              ->default_value( FrameAPI::LIGO::O4::CHANNEL_NAME ),
          "analysis ready channel name" ) //
        ( "analysis-ready-mask,m",
          boost::program_options::value< int >( &analysis_ready_mask )
              ->default_value( FrameAPI::LIGO::O4::OBS_INTENT |
                               FrameAPI::LIGO::O4::OBS_READY ),
          "analysis ready mask" ) //
        ( "delta",
          "print ranges using delta format [(gps_start_time, delta)]" ) //
        ( "frame-file-pattern",
          boost::program_options::value< std::string >( &glob_pattern )
              ->default_value( "*.gwf" ),
          "analysis ready channel name" ) //
        ( "ifo,i",
          boost::program_options::value< std::string >( &ifo )->default_value(
              "H1" ),
          "analysis ready channel name" ) //
        ( "single-entry-per-line",
          "print each entry on a separate line" ) //
        ;
    //-------------------------------------------------------------------
    // Parse the options
    //-------------------------------------------------------------------
    boost::program_options::store(
        boost::program_options::command_line_parser( ArgC, ArgV )
            .options( options_visible )
            .run( ),
        options_variable_map );
    boost::program_options::notify( options_variable_map );

    if ( options_variable_map.count( "help" ) )
    {
        Usage( 0 );
    }
}

inline int
CommandLine::AnalysisReadyMask( ) const
{
    return analysis_ready_mask;
}

inline std::string const&
CommandLine::AnalysisReadyChannelName( ) const
{
    return analysis_ready_channel_name;
}

inline bool
CommandLine::Delta( ) const
{
    return options_variable_map.count( "delta" );
}

inline std::string const&
CommandLine::GlobPattern( ) const
{
    return glob_pattern;
}

inline std::string const&
CommandLine::IFO( ) const
{
    return ifo;
}

inline bool
CommandLine::SingleEntryPerLine( ) const
{
    return options_variable_map.count( "single-entry-per-line" );
}

inline void
CommandLine::Usage( int ExitStatus ) const
{
    std::cerr << options_visible << std::endl;
    exit( ExitStatus );
}

inline void
CommandLine::operator( )( )
{
    if ( options_variable_map.count( "help" ) )
    {
    }
}

struct glob_type : public std::list< std::string >
{
public:
    typedef std::list< std::string > base_type;

    glob_type( const std::string& pattern );

protected:
    std::string pattern;
};

glob_type::glob_type( const std::string& Pattern )
{
    using namespace boost::filesystem;
    using boost::cmatch;
    using boost::regex;
    using boost::regex_match;
    using boost::regex_replace;

    //-------------------------------------------------------------------
    // transform the glob pattern into regex pattern
    //-------------------------------------------------------------------
    pattern = Pattern;
    pattern = regex_replace( pattern, boost::regex( "[.]" ), "[.]" );
    pattern = regex_replace( pattern, boost::regex( "[*]" ), ".*" );
    pattern = regex_replace( pattern, boost::regex( "[?]" ), "." );
    //-------------------------------------------------------------------
    // Find files in the current directory that match the pattern
    //-------------------------------------------------------------------
    regex pattern_expression( "^" + pattern + "$" );

    for ( auto& directory_entry : directory_iterator( current_path( ) ) )
    {
        if ( regex_match( directory_entry.path( ).filename( ).string( ),
                          pattern_expression ) )
        {
            push_back( directory_entry.path( ).filename( ).string( ) );
        }
    }
    //-------------------------------------------------------------------
    // Sort the list of found files
    //-------------------------------------------------------------------
    sort( );
}

template < typename Type >
void
find_analysis_ready_intervals(
    FrameCPP::FrVect::data_type        Data,
    INT_4U                             Elements,
    Type                               Mask,
    INT_4U                             GPSStartTime,
    INT_4U                             SamplesPerSecond,
    FrameAPI::interval_container_type& ExistingIntervals )
{
    FrameAPI::interval_container_type new_intervals;
    INT_4U                            start = 0;
    INT_4U                            end = 0;

    auto data = reinterpret_cast< Type const* >( Data.get( ) );
    for ( auto offset = INT_4U( 0 ); offset < Elements; )
    {
        if ( ( data[ offset ] & Mask ) != Mask )
        {
            //-----------------------------------------------------------
            // Data is not analysis ready
            //-----------------------------------------------------------
            end = offset / SamplesPerSecond;
            if ( end > start )
            {
                new_intervals += FrameAPI::interval_type( GPSStartTime + start,
                                                          GPSStartTime + end );
            }
            start = end + 1;
            // Advance to the start of the next second
            offset = start * SamplesPerSecond;
            continue;
        }
        ++offset;
    }
    end = Elements / SamplesPerSecond;
    if ( end > start )
    {
        new_intervals.emplace_back( GPSStartTime + start, GPSStartTime + end );
    }

    ExistingIntervals += new_intervals;
}

void
find_analysis_ready_intervals(
    INT_4U                                GPSStartSecond,
    FrameCPP::FrAdcData::data_type const& Data,
    CommandLine const&                    Options,
    FrameAPI::interval_container_type&    ExistingIntervals )
{
    INT_4U samples_per_second = 1.0 / Data[ 0 ]->GetDim( 0 ).GetDx( );
    INT_4U number_of_samples = Data[ 0 ]->GetNData( );
    auto   uncompressed_data = Data[ 0 ]->GetDataUncompressed( );

    switch ( Data[ 0 ]->GetType( ) )
    {
    case FrameCPP::FrVect::FR_VECT_C:
        find_analysis_ready_intervals< CHAR >(
            uncompressed_data,
            number_of_samples,
            CHAR( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_1U:
        find_analysis_ready_intervals< CHAR_U >(
            uncompressed_data,
            number_of_samples,
            CHAR_U( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_2U:
        find_analysis_ready_intervals< INT_2U >(
            uncompressed_data,
            number_of_samples,
            INT_2U( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_2S:
        find_analysis_ready_intervals< INT_2S >(
            uncompressed_data,
            number_of_samples,
            INT_2S( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_4U:
        find_analysis_ready_intervals< INT_4U >(
            uncompressed_data,
            number_of_samples,
            INT_4U( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_4S:
        find_analysis_ready_intervals< INT_4S >(
            uncompressed_data,
            number_of_samples,
            INT_4S( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_8U:
        find_analysis_ready_intervals< INT_8U >(
            uncompressed_data,
            number_of_samples,
            INT_8U( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    case FrameCPP::FrVect::FR_VECT_8S:
        find_analysis_ready_intervals< INT_8S >(
            uncompressed_data,
            number_of_samples,
            INT_8S( Options.AnalysisReadyMask( ) ),
            GPSStartSecond,
            samples_per_second,
            ExistingIntervals );
        break;
    }
}

void
find_analysis_ready_intervals(
    std::string const&                 file_name,
    CommandLine const&                 Options,
    FrameAPI::interval_container_type& ExistingIntervals )
{

    FileReader             frame_stream( file_name.c_str( ) );
    FrameCPP::FrTOC const* toc =
        dynamic_cast< FrameCPP::FrTOC const* >( frame_stream.GetTOC( ) );
    std::ostringstream channel_name;

    channel_name << Options.IFO( ) << ":"
                 << Options.AnalysisReadyChannelName( );

    for ( auto frame_counter : boost::irange(
              decltype( toc->GetNFrame( ) )( 0 ), toc->GetNFrame( ) ) )
    {
        INT_4U gps_start_time_of_frame = toc->GetGTimeS( )[ frame_counter ];
        try
        {
            auto fradc = frame_stream.ReadFrAdcData( frame_counter,
                                                     channel_name.str( ) );
            find_analysis_ready_intervals( gps_start_time_of_frame +
                                               fradc->GetTimeOffset( ),
                                           fradc->RefData( ),
                                           Options,
                                           ExistingIntervals );
        }
        catch ( ... )
        {
            try
            {
                auto frproc = frame_stream.ReadFrProcData(
                    frame_counter, channel_name.str( ) );
                find_analysis_ready_intervals( gps_start_time_of_frame +
                                                   frproc->GetTimeOffset( ),
                                               frproc->RefData( ),
                                               Options,
                                               ExistingIntervals );
                continue;
            }
            catch ( ... )
            {
            }
        }
    }
}

int
main( int ArgC, char** ArgV )
{
    MemChecker::Trigger                      gc_trigger( true );
    static FrameAPI::interval_container_type analysis_ready;

    FrameCPP::Initialize( );

    try
    {
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        //-------------------------------------------------------------------
        // Variables for logging
        //-------------------------------------------------------------------
        static char cwd_buffer[ 2048 ];

        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (const char*)NULL )
        {
            exit( 1 );
        }
        std::string cwd( cwd_buffer );
        GenericAPI::LoggingInfo::LogDirectory( cwd );
        LDASTools::AL::Log::stream_file_type fs(
            new LDASTools::AL::Log::StreamFile );
        LDASTools::AL::Log::stream_type s;

        s = fs;
        GenericAPI::setLogTag( "trend" );
        GenericAPI::setLogDebugLevel( CommandLine::DEFAULT_DEBUG_LEVEL );
        GenericAPI::LogFormatter( )->EntriesMax( 500 );
        GenericAPI::LogFormatter( )->Stream( s );

        CommandLine cl( ArgC, ArgV );

        cl( );

        //---------------------------------------------------------------
        // Collect files to process
        //---------------------------------------------------------------
        glob_type file_names( cl.GlobPattern( ) );

        for ( auto file_name : file_names )
        {
            //-----------------------------------------------------------
            // Process file
            //-----------------------------------------------------------
            find_analysis_ready_intervals( file_name, cl, analysis_ready );
        }
        if ( cl.Delta( ) )
        {
            std::string separator = ", ";
            if ( cl.SingleEntryPerLine( ) )
            {
                std::ostringstream endl_stream;
                endl_stream << std::endl;
                separator = endl_stream.str( );
            }
            bool print_separator = false;
            for ( auto analysis_ready_interval : analysis_ready )
            {
                if ( print_separator )
                {
                    std::cout << separator;
                }
                else
                {
                    print_separator = true;
                }
                std::cout << "( " << analysis_ready_interval.start << ", "
                          << ( analysis_ready_interval.stop -
                               analysis_ready_interval.start )
                          << " )";
            }
            std::cout << std::endl;
        }
        else
        {
            std::cout << analysis_ready << std::endl;
        }
    }
    catch ( const std::exception& Exception )
    {
        std::cerr << "Caught exception: " << Exception.what( ) //
                  << std::endl;
    }
}
