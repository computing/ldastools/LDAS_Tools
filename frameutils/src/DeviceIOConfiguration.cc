//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <sstream>
#include <stdexcept>

#include "ldastoolsal/ErrorLog.hh"

#include "genericAPI/Stat.hh"

#include "DeviceIOConfiguration.hh"
#include "util.hh"

using namespace FrameAPI;
using LDASTools::AL::ReadWriteLock;

using LDASTools::AL::ErrorLog;
using LDASTools::AL::StdErrLog;

namespace
{
    static const char* DEFAULT_FSTYPE_SPECIFIER = "*default*";
} // namespace

SINGLETON_INSTANCE_DEFINITION(
    SingletonHolder< FrameAPI::DeviceIOConfiguration > )

DeviceIOConfiguration::DeviceIOConfiguration( )
{
}

DeviceIOConfiguration::~DeviceIOConfiguration( )
{
}

void
DeviceIOConfiguration::GetConfiguration( const char*  Filename,
                                         size_type&   BufferSize,
                                         bool&        UseMemoryMappedIO,
                                         std::string& FSType )
{
    DeviceIOConfiguration::Instance( ).get_configuration(
        Filename, BufferSize, UseMemoryMappedIO, FSType );
}

std::string
DeviceIOConfiguration::Reset( const descriptions_type& Descriptions )
{
    return DeviceIOConfiguration::Instance( ).reset( Descriptions );
}

void
DeviceIOConfiguration::get_configuration( const char*  Filename,
                                          size_type&   BufferSize,
                                          bool&        UseMemoryMappedIO,
                                          std::string& FSType )
{
    static const char* func = "DeviceIOConfiguration::get_configuration";

    // Let others know that someone is reading the data
    ReadWriteLock lock( m_cache_lock, ReadWriteLock::READ, __FILE__, __LINE__ );

    if ( StdErrLog.IsOpen( ) )
    {
        std::ostringstream msg;

        msg << func << ": Filename: " << Filename;
        StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
    }
    //:TODO: stat the file. If not found, throw meaningful exception
    struct stat stat_buf;

    if ( GenericAPI::Stat( Filename, stat_buf ) != 0 )
    {
        // Try to access just the directory component
        std::string filename( Filename );
        std::string dir( filename.substr( 0, filename.rfind( "/" ) ) );
        if ( dir.length( ) == filename.length( ) )
        {
            // Take care of the special case where only the filename was
            // specified.
            //   This file will end up in the current directory when opened.
            dir = ".";
        }
        if ( ( dir.length( ) <= 0 ) || GenericAPI::Stat( dir, stat_buf ) != 0 )
        {
            std::ostringstream oss;
            oss << "Unable to access file: " << Filename;
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, oss.str( ) );
            throw std::runtime_error( oss.str( ) );
        }
        if ( StdErrLog.IsOpen( ) )
        {
            std::ostringstream msg;

            msg << func << ": filename: " << filename << " dir: " << dir;
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
    }
    if ( StdErrLog.IsOpen( ) )
    {
        std::ostringstream msg;

        msg << func << ": st_dev: " << stat_buf.st_dev;
        StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
    }
    try
    {
        const MountMgr::MountPoint mntpt(
            m_mount_mgr.Lookup( stat_buf.st_dev ) );

        FSType = mntpt.s_type;
    }
    catch ( ... )
    {
        FSType = "unknown";
    }
    const cache_type::const_iterator pos( m_cache.find( stat_buf.st_dev ) );
    if ( pos != m_cache.end( ) )
    {
        // Found doing a cache lookup
        StdErrLog( ErrorLog::DEBUG,
                   __FILE__,
                   __LINE__,
                   "user specified characteristics" );
        BufferSize = ( *pos ).second.s_buffer_size;
        UseMemoryMappedIO = ( *pos ).second.s_use_mmap_io;
    }
    else
    {
        const fstype_cache_type::const_iterator fst_pos(
            m_fstype_cache.find( FSType ) );
        if ( fst_pos != m_fstype_cache.end( ) )
        {
            // Found doing a cache lookup
            BufferSize = ( *fst_pos ).second.s_buffer_size;
            UseMemoryMappedIO = ( *fst_pos ).second.s_use_mmap_io;
        }
        else
        {
            // Not found any of the caches so set to default value
            BufferSize = size_type( StreamBufferSize );
            UseMemoryMappedIO = EnableMemoryMappedIO;
        }
    }
    if ( StdErrLog.IsOpen( ) )
    {
        std::ostringstream msg;

        msg << func << ": BufferSize: " << BufferSize
            << " UseMemoryMappedIO: " << UseMemoryMappedIO;
        StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
    }
}

std::string
DeviceIOConfiguration::reset( const descriptions_type& Descriptions )
{
    static const char* func = "DeviceIOConfiguration::reset";

    std::string warnings;

    // Ensure that only this method is modifying the list
    ReadWriteLock lock(
        m_cache_lock, ReadWriteLock::WRITE, __FILE__, __LINE__ );

    // Erase all elements
    m_cache.erase( m_cache.begin( ), m_cache.end( ) );
    m_fstype_cache.erase( m_fstype_cache.begin( ), m_fstype_cache.end( ) );

    struct stat        stat_buf;
    std::ostringstream missing;

    for ( descriptions_type::const_iterator cur = Descriptions.begin( ),
                                            end = Descriptions.end( );
          cur != end;
          ++cur )
    {
        if ( ( *cur ).s_name[ 0 ] == '/' )
        {
            //-----------------------------------------------------------------
            // These should be files
            //-----------------------------------------------------------------
            if ( GenericAPI::Stat( ( *cur ).s_name, stat_buf ) != 0 )
            {
                if ( StdErrLog.IsOpen( ) )
                {
                    std::ostringstream msg;

                    msg << func << ": Missing file: " << ( *cur ).s_name;
                    StdErrLog(
                        ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
                }
                missing << " " << ( *cur ).s_name;
                continue;
            }
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << func << ": Adding Entry: " << ( *cur ).s_name << "("
                    << stat_buf.st_dev << ")"
                    << " s_buffer_size: "
                    << ( *cur ).s_characteristics.s_buffer_size
                    << " s_use_mmap_io: "
                    << ( *cur ).s_characteristics.s_use_mmap_io;
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
            m_cache[ stat_buf.st_dev ] = ( *cur ).s_characteristics;
        }
        else if ( ::strcmp( ( *cur ).s_name.c_str( ),
                            DEFAULT_FSTYPE_SPECIFIER ) == 0 )
        {
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << func << ": Adding Default: "
                    << " s_buffer_size: "
                    << ( *cur ).s_characteristics.s_buffer_size
                    << " s_use_mmap_io: "
                    << ( *cur ).s_characteristics.s_use_mmap_io;
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
            StreamBufferSize = ( *cur ).s_characteristics.s_buffer_size;
            EnableMemoryMappedIO = ( *cur ).s_characteristics.s_use_mmap_io;
        }
        else
        {
            //-----------------------------------------------------------------
            // These are file system classes
            //-----------------------------------------------------------------
            try
            {
                try
                {
                    m_mount_mgr.IsKnownFSType( ( *cur ).s_name );
                }
                catch ( const std::exception& e )
                {
                    std::ostringstream msg;
                    msg << "WARNING: " << e.what( );
                    warnings += msg.str( );
                }

                m_fstype_cache[ ( *cur ).s_name ] = ( *cur ).s_characteristics;
                if ( StdErrLog.IsOpen( ) )
                {
                    std::ostringstream msg;

                    msg << func << ": Adding FSType: " << ( *cur ).s_name
                        << " s_buffer_size: "
                        << ( *cur ).s_characteristics.s_buffer_size
                        << " s_use_mmap_io: "
                        << ( *cur ).s_characteristics.s_use_mmap_io;
                    StdErrLog(
                        ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
                }
            }
            catch ( ... )
            {
            }

            missing << " " << ( *cur ).s_name;
            continue;
        }
    }
    return warnings;
}
