//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FileReaderHH
#define FileReaderHH

#include <memory>
#include <iosfwd>

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

#include "ldastoolsal/fstream.hh"

#include "framecpp/Common/MD5Sum.hh"
#include "framecpp/FrameCPP.hh"

#include "framecpp/IFrameStream.hh"
#include "framecpp/OFrameStream.hh"

#include "genericAPI/file.hh"
#include "genericAPI/inputfile.hh"
#include "genericAPI/outputfile.hh"

#include "frameAPI/util.hh"

class FrameFile
{
public:
    FrameFile( const std::string& Filename,
               unsigned int       size,
               char*              buffer,
               bool               EnableMemoryMappedIO,
               std::ios::openmode Mode,
               const char*        BufferFilname = (const char*)NULL );

    virtual ~FrameFile( );

    const std::string& GetFilename( ) const;

protected:
    typedef LDASTools::AL::filebuf filebuf;

    // :TODO: This should not have to be dynamically allocated.
    FrameCPP::Common::FrameBuffer< filebuf >* m_frame_buf;

    void finish( );

private:
    boost::shared_array< char > m_buffer;
    const std::string           m_filename;
    bool                        m_finished;

    void check_if_dir( const std::string& Filename ) const;
    void error( ) const;
};

inline FrameFile::FrameFile( const std::string& Filename,
                             unsigned int       size,
                             char*              buffer,
                             bool               EnableMemoryMappedIO,
                             std::ios::openmode Mode,
                             const char*        BufferFilename )
    : m_frame_buf( NULL ), m_filename( Filename ), m_finished( false )
{
    check_if_dir( Filename );
    std::unique_ptr< FrameCPP::Common::FrameBuffer< filebuf > > frame_buf(
        new FrameCPP::Common::FrameBuffer< filebuf >( Mode ) );

    if ( size == FrameCPP::Common::FrameBufferInterface::M_BUFFER_SIZE_SYSTEM )
    {
        // Do nothing
    }
    else if ( size > 0 )
    {
        m_buffer.reset( new char[ size ] );
        frame_buf->pubsetbuf( m_buffer.get( ), size );
    }
    else
    {
        m_buffer.reset( );
        frame_buf->pubsetbuf( m_buffer.get( ), 0 );
    }
    frame_buf->UseMemoryMappedIO( EnableMemoryMappedIO );
    frame_buf->open( ( BufferFilename ) ? BufferFilename : m_filename.c_str( ),
                     Mode | std::ios::binary );
    if ( frame_buf->is_open( ) == false )
    {
        error( );
    }
    m_frame_buf = frame_buf.release( );
}

inline FrameFile::~FrameFile( )
{
    finish( );
}

//-----------------------------------------------------------------------------
//: Retrieve the name of the file
//
//! ret: const std::string& - Name of the file
inline const std::string&
FrameFile::GetFilename( ) const
{
    return m_filename;
}

inline void
FrameFile::finish( )
{
    if ( !m_finished )
    {
        m_finished = true;
        if ( m_frame_buf )
        {
            /// \todo
            ///     Need to understand why this causes instability
            ///     in the frameAPI. Use libumem to quickly raise
            ///     the issue.
            // m_frame_buf->close( );
        }
    }
}

//-----------------------------------------------------------------------------
//
//: Frame File Reader
//
// This class encapsulates a FrameReader object.  It uses the GenericAPI's
// InputFile class to open a file for reading and then passes the resulting
// ifstream to the FrameReader.
//
class FileReader : public FrameFile, public FrameCPP::IFrameStream

{
public:
    /* Constructor/Destructor */
    //! exc: SwigException
    //! exc: bad_alloc - Memory allocation failed.
    FileReader( const char*  filename,
                unsigned int size = FrameAPI::StreamBufferSize,
                char*        buffer = (char*)NULL,
                bool EnableMemoryMappedIO = FrameAPI::EnableMemoryMappedIO,
                bool VerifyFilename = false );
};

//-----------------------------------------------------------------------------
//
//: Frame File Writer
//
// This class encapsulates a FrameWriter object.  It uses the GenericAPI's
// OutputFile class to open a file for writing and then passes the resulting
// ofstream to the FrameWriter.
//
class FileWriter : public FrameFile, public FrameCPP::OFrameStream
{
public:
    typedef INT_4U                                      file_start_time_type;
    typedef INT_4U                                      file_dt_type;
    typedef INT_4U                                      file_frame_count_type;
    typedef FrameCPP::Common::OFrameStream::chkSum_type chkSum_type;
    typedef boost::shared_ptr< FrameCPP::FrTOC >        frtoc_type;

    /* Constructor/Detructor */
    //! exc: SwigException
    FileWriter( const char* filename,
                const char* tmp_filename = (const char*)NULL );
    //! exc: SwigException
    FileWriter( const char*  filename,
                unsigned int size,
                char*        buffer = (char*)NULL,
                bool EnableMemoryMappedIO = FrameAPI::EnableMemoryMappedIO,
                const char* tmp_filename = (const char*)NULL );
    ~FileWriter( );

    file_dt_type Dt( ) const;

    file_frame_count_type FrameCount( ) const;

    void Rename( );

    void ResetMD5Sum( bool CalculateMD5Sum );

    file_start_time_type StartTime( ) const;

    /// \brief Write Frame data to the stream
    void WriteFrame(
        object_type FrameObject,
        chkSum_type FrameChecksumType = FrameCPP::Common::CheckSum::NONE );

    /// \brief Write Frame data to the stream
    void WriteFrame(
        object_type FrameObject,
        INT_2U      CompressionScheme,
        INT_2U      CompressionLevel,
        chkSum_type FrameChecksumType = FrameCPP::Common::CheckSum::NONE );

private:
    file_start_time_type     file_start_time;
    file_dt_type             file_dt;
    file_frame_count_type    file_frame_count;
    mutable std::string      mTmpFilename;
    FrameCPP::Common::MD5Sum m_md5;

    int  rename_file( );
    void update( object_type FrameObject );
};

inline FileWriter::file_dt_type
FileWriter::Dt( ) const
{
    return file_dt;
}

inline FileWriter::file_frame_count_type
FileWriter::FrameCount( ) const
{
    return file_frame_count;
}

inline void
FileWriter::ResetMD5Sum( bool CalculateMD5Sum )
{
    SetMD5Sum( CalculateMD5Sum );
}

inline FileWriter::file_start_time_type
FileWriter::StartTime( ) const
{
    return file_start_time;
}

#endif // FileReaderHH
