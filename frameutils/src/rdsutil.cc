//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// Local Header Files
#include <frameutils_config.h>

#include "rdsutil.hh"

#include <cstdlib>
#include <sstream>
#include <math.h>

// FrameCPP Header Files
//#include <framecpp/frame.hh>
//#include <framecpp/rawdata.hh>
//#include <framecpp/adcdata.hh>

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// General Header Files
#include "ldastoolsal/regexmatch.hh"

using namespace std;
using namespace FrameCPP;

// Static data initialization
const char ListParser::mSeparator( ',' );
const char ListParser::mOpen( '{' );
const char ListParser::mClose( '}' );

//-------------------------------------------------------------------------------
/// This constructor initializes the instance to be ready to parse the supplied
/// string.
//-------------------------------------------------------------------------------
ListParser::ListParser( const char* list )
    : mBuffer( 0 ), mStart( 0 ), mCurrPos( 0 ), mName( "" )
{
    mBuffer = new CHAR[ strlen( list ) + 1 ];
    strcpy( mBuffer, list );

    mStart = mBuffer;
    mCurrPos = mBuffer;
}

ListParser::~ListParser( )
{
    delete[] mBuffer;
    mBuffer = 0;
}

//-------------------------------------------------------------------------------
//
//: Parse out next string token.
//
//! return: const bool - True if token was parsed out, false otherwise (end of
//+        string has been reached).
//
bool
ListParser::getNextToken( )
{
    mStart = mCurrPos;
    while ( isspace( *mStart ) || *mStart == mSeparator || *mStart == mOpen ||
            *mStart == mClose )
    {
        ++mStart;
    }

    // End of string is reached
    if ( *mStart == '\0' )
    {
        return false;
    }

    // find end of token
    for ( mCurrPos = mStart; *mCurrPos != '\0' && !isspace( *mCurrPos ) &&
          *mCurrPos != mSeparator && *mCurrPos != mOpen && *mCurrPos != mClose;
          ++mCurrPos )
    {
    }

    string token( mStart, 0, mCurrPos - mStart );
    mName = token;

#ifdef DEBUG_REDUCE_RAW
    cout << "Got token: " << token << endl;
#endif

    return true;
}

//-------------------------------------------------------------------------------
//
//: Parse out all string tokens.
//
//! return: const std::vector< std::string > - Vector of tokens.
//
const std::vector< std::string >
ListParser::getTokenList( )
{
    vector< string > all;

#ifdef DEBUG_REDUCE_RAW
    cout << "All tokens: " << mBuffer << endl;
#endif
    for ( ;; )
    {
        while ( isspace( *mStart ) || *mStart == mSeparator ||
                *mStart == mOpen || *mStart == mClose )
        {
            ++mStart;
        }

        // End of string is reached
        if ( *mStart == '\0' )
        {
            return all;
        }

        // find end of token
        for ( mCurrPos = mStart; *mCurrPos != '\0' && !isspace( *mCurrPos ) &&
              *mCurrPos != mSeparator && *mCurrPos != mOpen &&
              *mCurrPos != mClose;
              ++mCurrPos )
        {
        }

        all.push_back( string( mStart, 0, mCurrPos - mStart ) );

#ifdef DEBUG_REDUCE_RAW
        cout << "token: \"" << all.back( ) << "\"" << endl;
#endif
        mStart = mCurrPos;
    }

    return all;
}

#if 0
//   
// Regex details for the frame filename:
//
// "^"                    The beginning of the buffer   
// "([^-]+)"              Remember the first match
// "-"                    Delimiter
// "([^-]+)"              Remember the second match    
// "-"                    Delimiter
// "([0-9]+)"             Remember GPS seconds       
// "-"                    Delimiter   
// "([0-9]+)"             Capture frame duration
// "\\.gwf"               File extension
//   
const Regex FrameName::re_name(
   "^([^-]+-)([^-]+)-([0-9]+)-([0-9]+)\\.gwf$" );
   
//   
// Regex details for the name field representing RDS token.
//
// "^"                    The beginning of the buffer   
// "(RDS_)"               RDS_ keyword
// "([^_]+_L)"            File description, followed by "_L"    
// "([0-9]+)"             Level
// "$"                    The end.
//   
const Regex FrameName::re_rds_name(
   "^(RDS_)([^_]+_L)([0-9]+)$" );
   
      

//-------------------------------------------------------------------------------
//   
//: Create file name for output frame.
//   
const std::string FrameName::operator()( const std::string& file,
   const string& dir ) const
{
   string::size_type slash_pos( file.find_last_of( '/' ) );  

   string name;
   if( slash_pos == string::npos )
   {
      // The whole string is a filename:
      name = file;
   }
   else
   { 
      ++slash_pos;
      name.assign( file.begin() + slash_pos, file.end() );
   }

#ifdef DEBUG_REDUCE_RAW      
   cout << "Original file name: " << file << endl;
#endif   
  
   string tmp( dir );
   tmp += '/';
   tmp += createName( name );
   
   return tmp.c_str();
}
   
   
//-------------------------------------------------------------------------------
//   
//: Parses original frame file name and creates next version of the name..
//      
//!param: const std::string& name - Original filename.
//!param: const unsigned int frame_dt - New frame duration.   
//
//!return: const char* - New filename.
//   
const std::string FrameName::createName( const std::string& name ) const
{
   // Parse out file name of format: Type-CODE-GPSsec-Dt.gwf:
   //    if CODE == RDS_xxx_LN ===> increment level: RDS_xxx_LN+1
   //    if CODE == 'one character' ===> CODE will become Type-RDS_CODE_L1-...
   RegexMatch rm( 5 );  
   
   const char* start( name.c_str() );
   if( rm.match( re_name, start ) )
   {
      string source_match( rm.getSubString( 1 ) );
      string description_match( rm.getSubString( 2 ) );   
      string gps_sec_match( rm.getSubString( 3 ) );      
      string dt_match( rm.getSubString( 4 ) );

#ifdef DEBUG_REDUCE_RAW      
      cout << "Parsed out: " << source_match
           << " " << description_match 
           << " " << gps_sec_match 
           << " dt=" << dt_match << endl;
#endif   

      // Frame stop time
      REAL_8 stop_time( mSec );
      stop_time += mNan * 1e-9;
      stop_time += mDt;

      // Calculate frame dt in seconds   
      INT_4S frame_dt( static_cast< INT_4S >( stop_time ) - mSec );
      if( fmod( stop_time, 1.0 ) )
      {
         ++frame_dt;
      }

      // It's RDS frame:
      if( rm.match( re_rds_name, description_match.c_str() ) )
      {
         // It's RDS frame, increment number at the end of the string
         char* end;
         unsigned int level( strtoul( rm.getSubString( 3 ).c_str(), 
                             &end, 10 ) );
         ++level;
   
         ostringstream tmp( source_match );
         tmp << source_match
             << rm.getSubString( 1 ) << rm.getSubString( 2 ) << level
             << '-' << gps_sec_match << '-' << frame_dt << ".gwf";
   
         return tmp.str();
      }
   
      // It's not RDS frame
      ostringstream tmp( source_match );
      tmp << source_match
          << "RDS_" << description_match << "_L1-" 
          << gps_sec_match << '-' << frame_dt << ".gwf";
   
      return tmp.str();
   }
  
   string error( "unknown frame filename format: " );
   error += name;
   
   throw SWIGEXCEPTION( error );
   return "";
}

#endif /* 0 */
