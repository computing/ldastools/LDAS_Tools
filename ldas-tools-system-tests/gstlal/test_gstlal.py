#! /usr/bin/env python

import getopt
import os
import os.path
import re
import subprocess
import sys

sys.path.insert(1, "..")

import testing

Test = testing.UnitTest( )

def mkdir_p( path ):
    try:
        os.makedirs( path )
    except OSError as exc:
        if ( exc.errno == errno.EEXIST and os.path.isdir( path ) ):
                 pass
        else:
            raise

def test_crc( SubTest ):
    #--------------------------------------------------------------------
    # gst-launch-1.0 filesrc location=<file> blocksize=<file size> !
    #    framecpp_channeldemux .<channel_name> ! fakesink sync=false
    #--------------------------------------------------------------------
    channelname = 'H1:LSC-AS_Q'
    filename = '/Users/efm/mnt/scratchdev/frames/Samples/S6/L0/LHO/H-R-9716/H-R-971621856-32.gwf'
    file_size = os.stat( filename ).st_size
    pass

#========================================================================
# OSX - gstreamer1
#   PKG_CONFIG_PATH=/opt/local/lib/pkgconfig
#
#   MacPorts:
#      libgomp
#========================================================================

class project( object ):
    def __init__( self ):
        self.top_src_dir = ''
        self.top_build_dir = ''
        self.prog_configure ='configure'
        self.env = {}
        self.options = []
        if not 'PKG_CONFIG_PATH' in self.env:
            self.env[ 'PKG_CONFIG_PATH' ] = []
        self.env[ 'PKG_CONFIG_PATH' ].extend( [ '/opt/local/lib/pkgconfig', '/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/pkgconfig' ] )
        self.prefix = os.path.join( self.top_build_dir, 'test_install' )

    def compile( self ):
        cmd = [ 'make',
                ]
        self.__eval( 'compile', cmd )

    def configure( self ):
        cmd = [ os.path.join( self.top_src_dir,
                              self.prog_configure )
                ]
        for key in self.env:
            cmd.extend( [ "%s=%s" % ( key, ':'.join( self.env[ key ] ) ) ] )
        cmd.extend( self.options )
        cmd.append( "--prefix=%s" % self.prefix )
        self.__eval( 'configure', cmd )

    def execute( self ):
        pass

    def install( self ):
        cmd = [ 'make',
                'install'
                ]
        self.__eval( 'install', cmd )

    def __eval( self, stage, cmd ):
        cwd = os.getcwd( )
        try:
            if ( not os.path.exists( self.top_build_dir ) ):
                mkdir_p( self.top_build_dir )
            # print( 'DEBUG: cwd: %s top_build_dir: %s' % ( cwd, self.top_build_dir ) )
            os.chdir( self.top_build_dir )
            cmd = ' '.join( cmd )
            cmd = ( '%s > ba_%s 2>&1' % ( cmd, stage ) )
            # print 'DEBUG:', stage, 'cmd:', cmd
            subprocess.check_call( cmd, shell=True )
        except subprocess.CalledProcessError as err:
            print 'FAILED:', stage, 'cmd:', cmd 
            exit( 1 )
        finally:
            #------------------------------------------------------------
            # Ensure that we return to the original directory
            #------------------------------------------------------------
            os.chdir( cwd )


class gstlal( object ):
    class prod( project ):
        def __init__( self ):
            super( self.__class__, self ).__init__( )
            self.top_src_dir = os.path.join( testing.dir_gstlal_src, 'gstlal' )
            self.top_build_dir = os.path.join( testing.dir_gstlal_build, 'gstlal' )
            self.prefix = os.path.join( testing.dir_gstlal_build, 'test_install' )
            self.env[ 'PKG_CONFIG_PATH' ].insert(0, os.path.join( testing.dir_ldas_tools, 'lib', 'pkgconfig' ) )

    class ugly( project ):
        def __init__( self ):
            super( self.__class__, self ).__init__( )
            self.top_src_dir = os.path.join( testing.dir_gstlal_src, 'gstlal-ugly' )
            self.top_build_dir = os.path.join( testing.dir_gstlal_build, 'gstlal-ugly' )
            self.prefix = os.path.join( testing.dir_gstlal_build, 'test_install' )
            self.env[ 'PKG_CONFIG_PATH' ].insert(0, os.path.join( testing.dir_ldas_tools, 'lib', 'pkgconfig' ) )

    class _plugin( object ):
        def __init__( self ):
            out = ""
            n = os.path.join( testing.dir_gstlal_build, 'test_install', 'lib', 'gstreamer-1.0' )
            try:
                e = os.environ['GST_PLUGIN_PATH']
                os.environ['GST_PLUGIN_PATH'] = ( "%s:%s" % ( n, e ) )
            except:
                os.environ['GST_PLUGIN_PATH'] = n
            self.name = None
            cmd = [ 'gst-inspect-1.0',
                    'framecpp'
                    ]
            try:
                cmd = ' '.join( cmd )
                print 'DEBUG: plugin cmd:', cmd
                out = subprocess.check_output( cmd, stderr=subprocess.STDOUT, shell=True )
            except subprocess.CalledProcessError as err:
                out = str( err )
            Test.Message( "DEBUG: Plugin query output: %s" % ( str( out ) ) )
            self.name_expected = 'framecpp'
            self.name = gstlal._plugin.__pattern__( 'Name', out )
            self.path_expected = os.path.join( n, ( 'lib%s_plugin.so' % self.name_expected ) )
            self.path = gstlal._plugin.__pattern__( 'Filename', out )
            self.version_expected = '1.3.1'
            self.version = gstlal._plugin.__pattern__( 'Version', out )

        @staticmethod
        def __pattern__( label, str ):
            retval = None
            p = '\s+%s\s+(\S+)' % label
            print( 'p: %s' % p )
            matches = re.search( p, str, re.MULTILINE )
            if ( matches ):
                retval = matches.group( 1 )
            print( 'retval: %s' % retval )
            return retval

    def __init__( self ):
        self.p = gstlal.prod( )
        self.u = gstlal.ugly( )

    def pipeline( self, frame, file_checksum ):
        global Test
        chksum = 'true'
        if ( not file_checksum ):
            chksum = 'false'
        frame_size = os.path.getsize( frame )
        cmd = [ 'gst-launch-1.0',
                'filesrc',
                'location=%s' % frame,
                'blocksize=%d' % frame_size,
                "!",
                'framecpp_channeldemux',
                'do-file-checksum=%s' % chksum,
                "!",
                'fakesink'
            ]
        try:
            cmd = ' '.join( cmd )
            print( 'DEBUG: plugin cmd:', cmd )
            out = subprocess.check_output( cmd, stderr=subprocess.STDOUT, shell=True )
        except subprocess.CalledProcessError as err:
            out = ( '<output>: %s <err>: %s' % ( err.output, str( err ) ) )
        Test.Message( "DEBUG: Plugin query output: %s" % ( str( out ) ) )

def test_compile( SubTest ):
    global Test
    #return
    #--------------------------------------------------------------------
    #
    #--------------------------------------------------------------------
    g = gstlal( )
    #--------------------------------------------------------------------
    # Build and install production component
    #--------------------------------------------------------------------
    g.p.configure( )
    g.p.compile( )
    g.p.install( )
    #--------------------------------------------------------------------
    # Build and install ugly component
    #--------------------------------------------------------------------
    g.u.configure( )
    g.u.compile( )
    g.u.install( )
    #--------------------------------------------------------------------
    # Verify plugin
    #--------------------------------------------------------------------
    g.plugin = gstlal._plugin( )
    Test.Check( g.plugin.name == g.plugin.name_expected,
                ( 'Verifying plugin name [%s =?= %s]' %
                  ( g.plugin.name, g.plugin.name_expected ) ) )
    Test.Check( g.plugin.path == g.plugin.path_expected,
                ( 'Verifying plugin path [%s =?= %s]' %
                  ( g.plugin.path, g.plugin.path_expected ) ) )
    Test.Check( g.plugin.version == g.plugin.version_expected,
                ( 'Verifying plugin version [%s =?= %s]' %
                  ( g.plugin.version, g.plugin.version_expected ) ) )
    #--------------------------------------------------------------------
    # Verify stream checksum
    #--------------------------------------------------------------------
    g.pipeline( '/hdfs/frames/O2/hoft_C01/H1/H-H1_HOFT_C01-11705/H-H1_HOFT_C01-1170505728-4096.gwf',
                True )

def test_loading( SubTest ):
    #gstlal._plugin.__pattern__( 'Name', '   Name   myName' )
    pass

def main(argv):
    global Test
    retval = 0
    cmd = None
    subcommand = 0

    #--------------------------------------------------------------------
    # Setup for testing
    #--------------------------------------------------------------------
    argv = Test.Init( argv )

    #--------------------------------------------------------------------
    # Run some tests
    #--------------------------------------------------------------------
    try:
        (opts, args) = getopt.getopt( argv, "", \
                                      ["loading=",
                                       "compile="] )
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "--loading":
            cmd = test_loading
            subcommand = int(arg)
        elif opt == "--compile":
            cmd = test_compile
            subcommand = int(arg)
    if ( not cmd == None ):
        cmd( subcommand )
    #--------------------------------------------------------------------
    # All done testing, return with the appropriate exit status
    #--------------------------------------------------------------------
    Test.Exit( )
    return( retval )

if __name__ == "__main__":
    exit( main( sys.argv[1:] ) )
