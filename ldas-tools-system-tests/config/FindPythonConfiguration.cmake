if (NOT DEFINED PYTHONINTERP_FOUND)
  find_package(PythonInterp)
endif (NOT DEFINED PYTHONINTERP_FOUND)

execute_process( COMMAND ${PYTHON_EXECUTABLE} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()"
		 OUTPUT_VARIABLE PYTHON_SITE_PACKAGES
		 OUTPUT_STRIP_TRAILING_WHITESPACE )
message(STATUS "PYTHON_SITE_PACKAGES: ${PYTHON_SITE_PACKAGES}")

