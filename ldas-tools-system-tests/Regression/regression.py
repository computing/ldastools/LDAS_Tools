#! /usr/bin/env python

import getopt
import os
import os.path
import pexpect
import sys

sys.path.insert(1, ".." )

import testing

PROGRAM_FRAMECPP_VERIFY = testing.prog_framecpp_verify

Test = testing.UnitTest( )

FRAME_SET = dict(BAD=1)
PASS = 0
FAIL = 1

def devscratch( frame_set ):
    global FRAME_SET

    d = os.path.join( os.path.sep, testing.dir_test_frames, "test", "frames" )
    if ( frame_set == FRAME_SET['BAD']):
        d = os.path.join( d, "bad" )
    return d

def find_test_file( basename ):
    global FRAME_SET

    paths = [ devscratch( FRAME_SET['BAD'] ),
              os.path.join( os.environ['HOME'], 'tmp' )]
    for p in paths:
        f = os.path.join( p, basename )
        if ( os.path.isfile( f ) ):
            return f
    return None    

def ticket_1774( SubTest ):
    global PROGRAM_FRAMECPP_VERIFY
    DUPLICATE_EXIT_CODE = 11
    status = True
    filename = find_test_file( "H-H1_TEST_ER_C00_AGG_DUPLICATE_CHANNEL-1101606144-256.gwf" )

    #--------------------------------------------------------------------
    # Detect duplicate channel name in the sample file
    #--------------------------------------------------------------------
    Test.Message( 'ticket_1774: filename: %s ' % filename )
    if ( filename ):
        # ===============================================================
        if ( SubTest == 1 ):
            #------------------------------------------------------------
            # Verify that the default action works properly
            #------------------------------------------------------------
            cmd = " ".join( [PROGRAM_FRAMECPP_VERIFY,
                             filename] )
            (command_output, exitstatus) = pexpect.run( cmd, withexitstatus=1 )
            if ( not exitstatus == DUPLICATE_EXIT_CODE ):
                status = False
        # ===============================================================
        elif ( SubTest == 2 ):
            #------------------------------------------------------------
            # Verify that the explicit action works properly
            #------------------------------------------------------------
            cmd = " ".join( [PROGRAM_FRAMECPP_VERIFY,
                             '--check-for-duplicate-channel-names',
                             filename] )
            (command_output, exitstatus) = pexpect.run( cmd, withexitstatus=1 )
            print >> sys.stderr, 'exit status: ' + str(exitstatus) +' for ' + cmd
            if ( not exitstatus == DUPLICATE_EXIT_CODE ):
                status = False
        # ===============================================================
        elif ( SubTest == 3 ):
            #------------------------------------------------------------
            # Verify that the explicit action of disabling check
            #    works properly
            #------------------------------------------------------------
            cmd = " ".join( [PROGRAM_FRAMECPP_VERIFY,
                             '--check-for-duplicate-channel-names', 'no',
                             filename] )
            (command_output, exitstatus) = pexpect.run( cmd, withexitstatus=1 )
            print >> sys.stderr, 'exit status: ' + str(exitstatus) +' for ' + cmd
            if ( not exitstatus == 0 ):
                status = False
    if ( status ):
        status = PASS
    else:
        status = FAIL
    return status

def main(argv):
    cmd = None
    subcommand = 0
    retval = 0

    try:
        (opts, args) = getopt.getopt( argv, "", \
                                      ["1774=",] )
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "--1774":
            cmd = ticket_1774
            subcommand = int(arg)
    if ( not cmd == None ):
        retval = cmd( subcommand )
    Test.Message( ( "Hello: exit code: %d" % ( retval ) ) );
    return( retval )

if __name__ == "__main__":
    exit( main( sys.argv[1:] ) )
