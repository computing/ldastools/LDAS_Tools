#! /usr/bin/env python
import os
import sys

sys.path.insert(1, ".." )

import testing
import dc
import ticket

class test( ticket.ticket ):
    def __init__( self, SubTask, Test ):
        super( self.__class__, self ).__init__( SubTask, '4739', Test = Test )
        self.servers = {}
        self.servers[ 'oasis_cvmfs' ] = '/cvmfs/oasis.opensciencegrid.org/ligo'
        self.servers[ 'osgstorage_cvmfs' ] = '/cvmfs/ligo.osgstorage.org'

        self.base_mount_points = [ '@oasis@/frames/ER8/H1_HOFT_C02',
                                   '@oasis@/frames/ER8/L1_HOFT_C02',
                                   '@oasis@/frames/O1/H1_HOFT_C02',
                                   '@oasis@/frames/O1/L1_HOFT_C02',
                                   '@osgstorage@/frames/ER8/H1_HOFT_C00/H',
                                   '@osgstorage@/frames/ER8/H1_HOFT_C01/H',
                                   '@osgstorage@/frames/ER8/L1_HOFT_C00/L',
                                   '@osgstorage@/frames/ER8/L1_HOFT_C01/L',
                                   '@osgstorage@/frames/O1/H1_HOFT_C00/H',
                                   '@osgstorage@/frames/O1/L1_HOFT_C01/L',
                                   '@osgstorage@/frames/O1/L1_HOFT_C00/L1',
                                   '@osgstorage@/frames/O1/H1_HOFT_C01/H' ]

    def eval( self ):
        super(self.__class__,self).eval( stop_server=True )

    def seed_data( self, outputdir, frame_type = '' ):
        skip = False
        paths = []
        for dir in ( self.base_mount_points ):
            d = dir.replace( '@oasis@', self.servers[ 'oasis_%s' % self._test ] )
            d = d.replace( '@osgstorage@', self.servers[ 'osgstorage_%s' % self._test ] )
            if ( os.path.isdir( d ) ):
                paths.append( d )
            else:
                print( 'DEBUG: SKIPPING because dir does not exist: %s' % ( d ) )
                skip = True
        if ( skip ):
            testing.skip( )
        return paths
