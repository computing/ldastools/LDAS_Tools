#! /usr/bin/env python
import os
import pexpect
import signal
import subprocess
import sys
import time

sys.path.insert(1, ".." )

import testing
import dc
import ticket

class ticket_nfs( ticket.ticket ):
    """
    To simulate nfs hangs, this class mounts directories using
    sshfs (user space mount).
    To unplug the network cable, the process is suspended with
    the STOP signal and resumed with the CONT signal.
    """
    class mount( object ):
        server = "ldas-pcdev4.ligo.caltech.edu"
        base_dir = "/archive/frames"
        """
        Mount the requested directory

        sshfs -o ssh_command=gsissh ldas-pcdev4.ligo.caltech.edu:/archive ~/mnt/archive/
        """
        def __init__( self, source, destination ):
            self.source = source
            self.destination = destination
            self.cmd_mount = " ".join( [ 'sshfs',
                                         '-f',
                                         '-o',
                                         'ssh_command=gsissh',
                                         self.source,
                                         self.destination ] )
            self.cmd_umount = " ".join( [ 'umount',
                                          self.destination ] )
            self.sshfs = None
            if ( not os.path.exists( self.destination ) ):
                os.makedirs( self.destination, 0755 )

        """
        Unmount the directory
        """
        def __del__( self ):
            pass

        def cont( self ):
            if ( self.sshfs ):
                self.sshfs.kill( signal.SIGCONT )

        def mount( self ):
            if ( not self.sshfs ):
                self.sshfs = pexpect.spawn( self.cmd_mount )

        def stop( self ):
            if ( self.sshfs ):
                self.sshfs.kill( signal.SIGSTOP )

        def unmount( self ):
            if ( self.sshfs ):
                self.sshfs.kill( signal.SIGTERM )
            else:
                subprocess.call( self.cmd_umount, shell=True )

    def __init__( self, SubTask, Test ):
        super( self.__class__, self ).__init__( SubTask, 'nfs', Test = Test )
        self._del_dir = None
        self._rds_levels = [ 'R', 'RDS_L1', 'RDS_L2']
        self._times_stride = 100000
        self._times = range( 1117000128, 1117600128, self._times_stride )
        self._nfs_mounts = []
        self.pattern_offline = 'Setting device state to OFFLINE:'
        self.pattern_online = 'Back online:'

    def __def__( self ):
        for mount in self._nfs_mounts:
            mount.unmount( )

    def eval( self ):
        super(self.__class__,self).eval( stop_server=False )
        print( "Evaluating test: %s\n" % ( self._test ) )
        if ( self._test == 'hanging' ):
            self._nfs_mounts[ 0 ].stop( )
            self.server.waitOn( [ self.pattern_offline, ],
                                timeout=60 )
            self._nfs_mounts[ 0 ].cont( )
            self.server.waitOn( [ self.pattern_online, ],
                                timeout=60 )
            

    def setup(self):
        super(self.__class__,self).setup( )
        #----------------------------------------------------------------
        # Add some additional mount points that are mounted via sshfs
        #----------------------------------------------------------------
        self._nfs_mounts.extend( [ ticket_nfs.mount( "localhost:%s" % testing.dir_test_frames,
                                                     os.path.join( self.pseudo_root,
                                                                   'mounted_frames',
                                                                   'scratchdev',
                                                                   'frames',
                                                                   'test' ) ) ] )
        #self._nfs_mounts.extend( mount( testing.dir_archive,
        #                                os.path.join( self.pseudo_root,
        #                                              'mounted_frames',
        #                                              'archive' ) ) )
        #----------------------------------------------------------------
        # Add directories to list of mount points to scan
        #----------------------------------------------------------------
        for mount in self._nfs_mounts:
            self._rsc.mount_points.extend( [ mount.destination ] )
            mount.unmount( ) # Ensure that nothing is left behind
            mount.mount( )  # Make it available
        #----------------------------------------------------------------
        # Update resource file
        #----------------------------------------------------------------
        self._rsc.log_debug_level = 50
        self._rsc.write( self.rsc_name )
