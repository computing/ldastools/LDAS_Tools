import io
import os
import pexpect
import random
import re
import signal
import sys
import time

sys.path.insert(1, ".." )

import testing

def grep( filename, regex, Test ):
    if ( not type(filename) is str ):
        filename = filename[0]
    Test.Message( 'grep: filename type: %s filename: %s regex: %s' % (str(type(filename)), str(filename), str(regex)),
                       Level=10 )
    pattern = re.compile( regex )
    retval = []
    for line in open( filename ):
        if ( pattern.match( line ) ):
            line = line.rstrip()
            Test.Message( 'grep: match: %s' % ( line ),
                          Level=10 )
            retval.append( line )
    Test.Message( 'Found %d matches' % ( len( retval ) ),
                  Level=10 )
    return retval

class tail_f( object ):
    def __init__(self, filename, interval=1.0):
        self.file = open( filename )
        self.interval = interval

    def next(self):
        while True:
            where = self.file.tell( )
            self.line = self.file.readline( )
            if not self.line:
                time.sleep( self.interval )
                self.file.seek( where )
            else:
                return self.line

"""
Create a cache of frame files with length 0

Keyword
times -- array of start times
stride -- distance between times
group -- additional information to be added to the "frame type"
"""
def seed_data( Test, times, stride, group, outputdir, frame_type = '' ):
    paths = []
    for mp in times:
        mdir = ( "%s/Z-%s%s-%d" % ( outputdir, group, frame_type, mp / stride ) )
        Test.Message( 'seed_data: directory: ' + mdir,
                      Level=20 )
        paths.append( mdir );
        try:
            os.makedirs( mdir, 0755 )
        except:
            pass
        for inc in range( 0, 100 ):
            fn = ( "%s/Z-%s%s-%d-256.gwf" % ( mdir, group, frame_type, mp ) )
            mp += 256
            try:
                fh = open( fn, 'a' )
                fh.close( )
            except:
                pass
    return paths

class diskcache:
    class server(object):
        def __init__( self, resource, Test ):
            self.resource = resource
            self.resource.log = 'diskcache_server'
            self.daemon = None
            self.dlogfile = None
            self.re_scanned = re.compile( 'scanned in (\d+) ms' )
            self.re_wrote_cache_file = re.compile( '[*]+ NOTE [*]+ \d+ .* Wrote (ASCII|Binary) Cache File: (.*)(dat|txt) in (\d+(|.\d+)) seconds')
            self.re_purple = re.compile( '[*]+ NOTE [*]+' )
            self.log_tail = None
            self.Test = Test

        def __del__( self ):
            if ( self.dlogfile
                 and self.dlogfile.isalive( ) ):
                self.dlogfile.kill( 9 )

        def isAlive( self ):
            if ( self.daemon
                 and self.daemon.isalive( ) ):
                return True
            return False

        def start( self ):
            #------------------------------------------------------------
            # Construct the server command
            #------------------------------------------------------------
            cmd = " ".join([testing.prog_diskcache,
                            "--log", "diskcache_server",
                            "daemon",
                            "--configuration-file", self.resource.configuration_filename
                            ])
            self.Test.Message( "Starting diskcache server: %s" % ( cmd ), Level=30 )
            self.daemon = pexpect.spawn( cmd )
            log_filename = os.path.join( self.resource.dir_log,
                                         self.resource.log + ".log.txt")
            while not os.path.isfile( log_filename ):
                time.sleep( 1 )
            self.log_tail = tail_f( log_filename )

        def rereadConfiguration( self ):
            self.daemon.kill( signal.SIGHUP )

        def waitOn( self, cond, timeout=300 ):
            if ( isinstance( cond, basestring ) ):
                cond = [ cond, ]
            completed = 0
            recond = []
            for c in cond:
                recond.append( re.compile('^.*' + c + '.*$') )
            done = ( 1 << len( cond ) ) - 1
            self.Test.Message( ( "Looking for these %d conditions: %s" % ( len(cond), str( cond ) ) ),
                               Level=30)
            while ( not completed == done ):
                line = self.log_tail.next( )
                if line:
                    line = line.rstrip( )
                    self.Test.Message( 'Line: ' + line,
                                       Level=25 )
                else:
                    break
                index = 0
                for c in recond:
                    if c.match( line ):
                        self.Test.Message( 'Found matching line(' + str(index) + '): ' + line,
                                           Level=30 )
                        break
                    index = index + 1
                if index == len( cond ):
            	    #----------------------------------------------------
            	    #----------------------------------------------------
                    continue
                else:
                    mask = 1 << index
                    completed = completed | mask
            self.Test.Message( "WaitOn completed",
                               Level=20 )

        def waitOnFirstScan( self, timeout=300 ):
            logfile = os.path.join( self.resource.dir_log,
                                    self.resource.log + ".log.txt" )
            scanned = False
            wrote_cache_file = False
            wait_for_log_file = True
            while( wait_for_log_file ):
                if ( os.path.exists( logfile ) ):
                    break
                sleep( 5 )
            cmd = " ".join([testing.prog_tail, "--follow=name", logfile ])
            self.Test.Message( "waitOnFirstScan: %s" % ( cmd ),
                               Level=20)
            self.dlogfile = pexpect.spawn( cmd )
            expecting = [pexpect.EOF,
                         pexpect.TIMEOUT,
                         self.re_scanned,
                         self.re_wrote_cache_file,
                         ' ADDED ',
                         ' NOTABLE ',
                         self.re_purple]
            ( ID_EOF,
              ID_TIMEOUT,
              ID_SCANNED,
              ID_WROTE_CACHE_FILE,
              ID_ADDED,
              ID_NOTABLE,
              ID_PURPLE ) = range( len( expecting ) )
            while True:
                index = self.dlogfile.expect( expecting,timeout=timeout )
                self.Test.Message( "Log file pexpect index: " + str( index )
                                   + ' [ID_EOF = ' + str( ID_EOF ) + ']',
                                   Level=20)
                if index == ID_EOF:
            	    #----------------------------------------------------
                    # Nothing futher to read from the stream
            	    #----------------------------------------------------
                    break
                elif index == ID_TIMEOUT:
            	    #----------------------------------------------------
                    # Timed out on the stream
            	    #----------------------------------------------------
                    break
                elif ( ( index == ID_ADDED) or
                       ( index == ID_NOTABLE ) or
                       ( index == ID_PURPLE ) ):
            	    #----------------------------------------------------
                    # Still reading new entries
            	    #----------------------------------------------------
                    if ( scanned and wrote_cache_file ):
                        break
                elif index == ID_WROTE_CACHE_FILE:
                    wrote_cache_file = True
                    if ( scanned ):
                        break
                elif index == ID_SCANNED:
            	    #----------------------------------------------------
                    # Completed one round
            	    #----------------------------------------------------
                    scanned = True
                    if ( wrote_cache_file ):
                        break
            self.Test.Message( "Wait completed",
                               Level=20 )

        def stop( self ):
            if ( self.daemon
                 and self.daemon.isalive( ) ):
                #--------------------------------------------------------
                # Send the shutdown command
                #--------------------------------------------------------
                self.daemon.sendintr( )
            #------------------------------------------------------------
            # Wait for the daemon process to exit
	    #------------------------------------------------------------
            self.daemon.expect( pexpect.EOF, timeout=120 )

    class client(object):
        def __init__( self, resource, Test ):
            #------------------------------------------------------------
            # 
            #------------------------------------------------------------
            self.resource = resource
            self.Test = Test

        def server_query( self,
                          start_time,
                          end_time,
                          extension = '.gwf',
                          ifo='Z',
                          ftype='R' ):
            #------------------------------------------------------------
            # Query the server for frame files
            #------------------------------------------------------------
            cmd = " ".join([testing.prog_diskcache,
                            "--host", "localhost",
                            "--port", str( self.resource.arg( 'server_port' ) ),
                            "--log", "diskcache_query",
                            "--log-directory", self.resource.dir_log,
                            "--log-format", "text",
                            'filenames-rds',
                            '--extension', extension,
                            '--ifo', ifo,
                            '--type', ftype,
                            '--start-time', str(start_time),
                            '--end-time', str(end_time)
                            ])
            self.Test.Message( 'About to execute cmd: ' + cmd,
                               Level=10 )
            (output, retval) = pexpect.run( cmd, withexitstatus=True, timeout=30 )
            return (output, retval)

        def server_quit( self ):
            #------------------------------------------------------------
            # Send the server the quit command
            #------------------------------------------------------------
            cmd = " ".join([testing.prog_diskcache,
                            "--host", "localhost",
                            "--port", str( self.resource.arg( 'server_port' ) ),
                            "--log", "diskcache_quit",
                            "--log-directory", self.resource.dir_log,
                            "--log-format", "text",
                            "quit"
                            ])
            (output, retval) = pexpect.run( cmd, withexitstatus=True, timeout=30 )

class diskcache_rsc:
    def __init__( self,
                  dir_log,
                  cache_write_delay = 30,
                  concurrency = 4,
                  data_function = None,
                  excluded_dirs = ["lost+found",],
                  extensions = [".gwf",],
                  hot_directory_age = None,
                  log = 'diskcache',
                  log_debug_level = 10,
                  log_format = 'text',
                  log_rotation = 10000,
                  mount_points = [],
                  output_ascii = "",
                  output_ascii_version = 0x00FF,
                  output_binary = "",
                  output_binary_version = 0x0101,
                  output_data_dir = None,
                  scan_interval = 16000,
                  server_port = 20200 + random.randint( 0, 800 ),
                  size_dir_data = 4,
                  stat_timeout = 5,
                  use_dir_archive = True,
                  use_dir_data = False ):
        self.cache_write_delay = cache_write_delay
        self.concurrency = concurrency
        self.dir_log = dir_log
        self.excluded_dirs = excluded_dirs
        self.extensions = extensions
        self.hot_directory_age = hot_directory_age
        self.log = log,
        self.log_debug_level = log_debug_level,
        self.log_format = log_format,
        self.log_rotation = log_rotation
        self.mount_points = mount_points
        self.output_ascii = output_ascii,
        self.output_ascii_version = output_ascii_version,
        self.output_binary = output_binary,
        self.output_binary_version = output_binary_version,
        self.scan_interval = scan_interval
        self.server_port = server_port
        self.size_dir_data = size_dir_data
        self.stat_timeout = stat_timeout
        self.use_dir_archive = use_dir_archive
        self.use_dir_data = use_dir_data

        if ( len( self.mount_points ) <= 0 ):
            if ( use_dir_data):
                if ( testing.dir_data1 ):
                    stop = min( 100, size_dir_data + 1)
                    for x in range( 1, stop ):
                        self.mount_points.append( os.path.join( os.path.sep, "data", ( "node%d"%(x)),  "frames" ) )
                elif (testing.dir_data001 ):
                    stop = min( 100, size_dir_data + 1)
                    for x in range( 1, stop ):
                        self.mount_points.append( os.path.join( os.path.sep, "data", ( "node%03d"%(x)),  "frames" ) )
                if ( ( size_dir_data > 100 ) and ( testing.dir_data100 ) ):
                    stop = min( 200, size_dir_data + 1)
                    for x in range( 100, stop ):
                        self.mount_points.append( os.path.join( os.path.sep, "data", ( "node%d"%(x)),  "frames" ) )
                if ( ( size_dir_data > 200 ) and ( testing.dir_data200 ) ):
                    stop = min( 300, size_dir_data + 1)
                    for x in range( 200, stop ):
                        self.mount_points.append( os.path.join( os.path.sep, "data", ( "node%d"%(x)),  "frames" ) )
                if ( ( size_dir_data > 300 ) and ( testing.dir_data300 ) ):
                    stop = min( 400, size_dir_data + 1)
                    for x in range( 200, stop ):
                        self.mount_points.append( os.path.join( os.path.sep, "data", ( "node%d"%(x)),  "frames" ) )
                if ( ( size_dir_data > 400 ) and ( testing.dir_data400 ) ):
                    stop = min( 450, size_dir_data + 1)
                    for x in range( 400, stop ):
                        self.mount_points.append( os.path.join( os.path.sep, "data", ( "node%d"%(x)),  "frames" ) )
            if ( use_dir_archive ):
                if ( testing.dir_archive ):
                    for x in [ 'ER6', 'ER5' ]:
                        self.mount_points.append( os.path.join( testing.dir_archive, x ) )
            if ( data_function ):
                self.mount_points = self.mount_points + data_function( output_data_dir )
        self.configuration_filename = None

    def arg( self, var ):
        if ( var == 'concurrency'):
            return str(self.concurrency)
        elif ( var == 'extensions'):
            return ",".join( self.extensions )
        elif ( var == 'mount_points'):
            return ",".join( self.mount_points )
        elif ( var == 'server_port'):
            return str( self.server_port )
        return None

    def write( self, filename ):
        self.configuration_filename = filename
        try:
            ostream = io.open( self.configuration_filename, 'wb' )
            #------------------------------------------------------------
            # General Configuration
            #------------------------------------------------------------
            ostream.writelines( ["CACHE_WRITE_DELAY_SECS=%d\n" % (self.cache_write_delay),
                                 "CONCURRENCY=%d\n" % (self.concurrency),
                                 "SCAN_INTERVAL=%d\n" % (self.scan_interval),
                                 "STAT_TIMEOUT=%d\n" % (self.stat_timeout),
                                 "SERVER_PORT=%d\n" % (self.server_port),
                                 "LOG_DEBUG_LEVEL=%d\n" % (self.log_debug_level),
                                 "LOG_DIRECTORY=%s\n" % (self.dir_log),
                                 "LOG_FORMAT=%s\n" % (self.log_format),
                                 "LOG_ROTATE_ENTRY_COUNT=%d\n" % (self.log_rotation),
                                 "OUTPUT_ASCII=%s\n" % (self.output_ascii),
                                 "OUTPUT_ASCII_VERSION=0x%04x\n" % (self.output_ascii_version),
                                 "OUTPUT_BINARY=%s\n" % (self.output_binary),
                                 "OUTPUT_BINARY_VERSION=0x%04x\n" % (self.output_binary_version),
                                 ] )
            if ( self.hot_directory_age ):
                ostream.write( ( "HOT_DIRECTORY_AGE_SEC=%d\n" % ( self.hot_directory_age) ) )
            #------------------------------------------------------------
            # Extensions
            #------------------------------------------------------------
            ostream.writelines( "\n[EXTENSIONS]\n" )
            for x in self.extensions:
                ostream.writelines( "%s\n" % (x) )
            #------------------------------------------------------------
            # EXCLUDED_DIRECTORIES
            #------------------------------------------------------------
            ostream.writelines( "\n[EXCLUDED_DIRECTORES]\n" )
            for x in self.excluded_dirs:
                ostream.writelines( "%s\n" % (x) )
            #------------------------------------------------------------
            # MOUNT_POINTS
            #------------------------------------------------------------
            if ( self.mount_points ):
                ostream.writelines( "\n[MOUNT_POINTS]\n" )
                for x in self.mount_points:
                    ostream.writelines("%s\n" % (x))
            #------------------------------------------------------------
            # Close the file
            #------------------------------------------------------------
            ostream.close( )
            pass
        except BaseException as e:
            self.Test.Message( "Caught Exception: %s" % (str(e)), Level=0 )

def check_for_seed( LogFile, SeedFile, Test ):
    #--------------------------------------------------------------------
    # Going to use pexpect to process the file
    #--------------------------------------------------------------------
    found = False
    cmd = ' '.join( ['cat', LogFile] )
    Test.Message( ' '.join( ['Checking log:', LogFile,
                             'for seed:', str( SeedFile ),
                             'using cmd: "' + cmd + '"' ] ),
                  Level=30 )
    dlogfile = pexpect.spawn( cmd )
    expecting = [ pexpect.EOF,
                  pexpect.TIMEOUT,
                  'Succeeded to seed the cache with: ([^\r\n]*)\r\n' ]
    ( ID_EOF,
      ID_TIMEOUT,
      ID_SEED_FILE_GENERIC ) = range( len( expecting ) )
    while True:
        index = dlogfile.expect( expecting, timeout=300 )
        if index == ID_EOF:
            Test.Message( 'End of File', Level=30 )
            break
        elif index == ID_TIMEOUT:
            Test.Message( 'Timed out', Level=30 )
            break
        elif index == ID_SEED_FILE_GENERIC:
            Test.Message( 'Checking for pattern in: ' + dlogfile.match.group( 1 ), Level=30 )
            if ( SeedFile ):
                if ( dlogfile.match.group( 1 ) == SeedFile ):
                    found = True
            break
    if ( not found and not SeedFile ):
        #----------------------------------------------------------------
        # This is the case where no seed was found and 
        #----------------------------------------------------------------
        found = True
    return found
