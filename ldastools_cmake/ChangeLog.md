# Release 1.3.1 - December 16, 2024
    - Upgraded to use igwn-cmake-macros 1.6.0

# Release 1.3.0 - December 19, 2023
    - Added new macro cx_check_compile_hardening

# Release 1.2.3 - September 29, 2021
    - Many corrections to support building on RHEL 8
