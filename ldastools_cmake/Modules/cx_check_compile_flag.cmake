#========================================================================
# -*- mode: cmake -*-
#
# cx_check_compile_flag( <flag> )
#
# Options:
#   VERBOSE
#========================================================================
include( CMakeParseArguments )
include( CheckCCompilerFlag )

# include( Autotools/cm_msg_checkiing )
# include( Autotools/cm_msg_result )

function(cx_check_compile_flag flag)
  set(options VERBOSE)
  set(oneValueArgs)
  set(multiValueArgs)

  cmake_parse_arguments(
    ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set(ORIGINAL_CMAKE_REQUIRED_FLAGS ${CMAKE_REQUIRED_FLAGS})
  set(CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS} ${flag}")
  set(ORIGINAL_CMAKE_REQUIRED_QUIET ${CMAKE_REQUIRED_QUIET})
  set(CMAKE_REQUIRED_QUIET TRUE)

  check_cxx_compiler_flag(${OPTION} COMPILER_SUPPORTS_${OPTION})
  if (COMPILER_SUPPORTS_${OPTION})
    set( _result "yes" )
    add_compile_options( "${flag}" )
  else()
    set( _result "no" )
  endif()

  set(CMAKE_REQUIRED_FLAGS ${ORIGINAL_CMAKE_REQUIRED_FLAGS})
  set(CMAKE_REQUIRED_QUIET ${ORIGINAL_CMAKE_REQUIRED_QUIET})

  cm_msg_checking( "if compiler supports '${flag}'")
  cm_msg_result( "${_result}")
endfunction()
