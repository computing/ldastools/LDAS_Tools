set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH True)

include( GNUInstallDirs )

if ( NOT DEFINED IGWN_CMAKE_MODULE_DIRS )
  set(IGWN_CMAKE_MODULE_DIRS
    ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATADIR}/igwn-cmake/cmake/Modules
    ${CMAKE_INSTALL_FULL_DATADIR}/igwn-cmake/cmake/Modules
    )
endif( )

find_package(
  IGWNCMake
  CONFIG
  REQUIRED
  HINTS ${IGWN_CMAKE_MODULE_DIRS}
  PATHS ${IGWN_CMAKE_MODULE_DIRS}
  )

find_package(PkgConfig REQUIRED)
pkg_check_modules(LDASTOOLS REQUIRED ldastoolscmake>=1.1.1)
set(
  LDASTOOLS_PKG_CONFIG_FILE
  ${LDASTOOLS_PREFIX}/share/pkgconfig/ldastoolscmake.pc
  )
pkg_get_variable(
  LDASTOOLS_CMAKE_MODULEDIR ${LDASTOOLS_PKG_CONFIG_FILE} cmakemoduledir
  CACHE FILEPATH "Root directory for LDAS Tools CMake macros"
  )
pkg_get_variable(
  LDASTOOLS_CMAKE_SCRIPTDIR ${LDASTOOLS_PKG_CONFIG_FILE} cmakescriptdir
  CACHE FILEPATH "Root directory for LDAS Tools scripts"
  )
if( LDASTOOLS_CMAKE_MODULEDIR )
  list( INSERT CMAKE_MODULE_PATH 0 "${LDASTOOLS_CMAKE_MODULEDIR}")
endif( )

set(
  LDAS_TOOLS_BUG_REPORT_URL
  "https://git.ligo.org/ldastools/LDAS_Tools/issues"
  CACHE INTERNAL "URL of where to submit problems"
  )
set(
  LDAS_TOOLS_HOMEPAGE_URL
  "https://wiki.ligo.org/Computing/LDASTools"
  CACHE INTERNAL "Homepage URL for LDASTools"
  )
set(
  LDAS_TOOLS_SOURCE_URL
  "https://software.igwn.org/lscsoft/source"
  CACHE INTERNAL "Source URL top level directory for LDASTools"
  )
