#========================================================================
# -*- mode: cmake -*-
#
# cx_check_compile_hardening
#
#========================================================================
include( CMakeParseArguments )

function(cx_check_compile_hardening)
  set(options VERBOSE)
  set(oneValueArgs)
  set(multiValueArgs)

  cmake_parse_arguments(
    ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  if ( CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_GNUCC )
    add_compile_definitions(
      _FORTIFY_SOURCE=2
      _GLIBCXX_ASSERTIONS
      )
    if ( NOT CMAKE_COMPILER_IS_GNUCXX )
      add_compile_options(
        )
    endif( )

    foreach(OPTION 
        "-flto=auto"
        "-ffat-lto-objects"
        "-grecord-gcc-switches"
        "-specs=/usr/lib/rpm/redhat/redhat-hardened-cc1"
        "-specs=/usr/lib/rpm/redhat/redhat-annobin-cc1"
        "-fcf-protection"
        "-g"
        "-DNDEBUG"
        "-O2"
        "-fasynchronous-unwind-tables"
        "-fexceptions"
        "-fstack-clash-protection"
        "-fstack-protector-strong"
        "-Wall"
        "-Wformat"
        "-Werror=format-security"
        "-Wl,-z,defs"
        "-Wl,-z,now"
        "-Wl,-z,relro"
        )
      cx_check_compile_flag("${OPTION}" VERBOSE)
    endforeach( )
  endif( )
endfunction()
